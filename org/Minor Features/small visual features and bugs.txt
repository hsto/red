All diagrams
	keyboard shortcut "DEL" for Delete-action
	keyboard shortcut "ALT+L" to toggle link
	keyboard shortcut "ALT+B" to select linked SE in containment tree
	keyboard shortcut "ALT+S" to toggle sketchiness
	keyboard shortcut "ALT+P" to open properties pop-up menu
	when linked, changes to SE get forwarded to SE, and vice versa
	Properties
	no (snap to) grid
	Drag DE to create SE not possible (replaces "create all" as discussed earlier this week)
	difficult to change name by clicking on element (finding the right spot). Doubleclicking anywhere on a DiagramElement should allow editing. Also selecting (single click) plus "F2" should also give that.
	when linking a DE to a SE, double clicking on the selection should complete the dialog
	Properties dialog should contain also font/size/properties for name, and a "reset to default" button, where default varies by DE.
	stickiness of new elements: uniform, both options available (sticky/non sticky)
	re-attaching a link from one element to another is not possible (not easy?)
	
	Move selected element by cursor not possible
	new generic DiagramElement "Link": has a link to a SpecificationElement, so that double-clicking the link will open the linked SE
	
Context (and others? Ok in  goal diagram)
	Rename palette sections "Activity"
	Unable to link-error
	Drag SE to create DE not possible

When creating a new SpecificationElement
	the first entry of the list of possible files should be preselected (if possible: the file used in the last create-action)
	when placing the new SE in a file, double-clicking on a file should complete the dialog
	spressing CR should have the same effect as pressing the OK-button

Configuration
	in the Relations-panel, adding Parts/Ports/Relations should allow me to select any subset of matching elements (e.g., select, Shift-select, Ctrl-select as usual)
	Low priority: Dragging a context diagram should add all its (linked) elements in the "Relations" panel
	"Long Description" can be smaller (maybe 75% the height), and should be swapped with "Relations"
	kind should gt the values {previous, actual, planned}
	The Heading above the tables in the Relations-panel should be removed
	There should be new columns at the right hand end of the tables in the Relations-panel for load-data
	
	
Navigation
	Backward/Forward-Button to go back/forth in the chain of navigations (=which editor was opened before)
	double clicking on a 