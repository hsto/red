In terms of metrics for a set of requirements:

* number of requirements is a good indication of system complexity (it may seem silly at first as not all requirements are the same "size", but in aggregate it seems to work no worse than counting lines of code does for software complexity)
* number of TBXs both in absolute and percentage of requirements that are not yet complete. It may also help to track the different flavors, as a TBD(etermined) is less mature than a TBR(esolved/reviewed) or a TBS(supplied)
* number of requirements with verification methods and methodologies, as requirements that no one has thought about how to verify probably aren't that good yet
* number of requirements flowed-down to lower-level specifications or similarly the number of childless requirements in top-level specs or orphans in lower-level specs

If you're talking about measuring the efficiency of your requirements management processes:
* hours charged per change request released
* verification burn-down plan vs. actual progress
* percentage of the specifications in the specification tree that are released and under configuration management

