package dk.dtu.imm.red.importing.ui.util;

public class ValidationException extends Exception {

	private static final long serialVersionUID = 1371947062916856995L;
	
	public ValidationException(String message) {
		super(message);
	}

}
