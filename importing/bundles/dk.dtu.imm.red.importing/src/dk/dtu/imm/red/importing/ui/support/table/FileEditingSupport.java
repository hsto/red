package dk.dtu.imm.red.importing.ui.support.table;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.importing.ui.model.FileMatchLine;

public abstract class FileEditingSupport extends EditingSupport {

	private TableViewer viewer;

	protected FileEditingSupport(TableViewer viewer) {
		super(viewer);
		this.viewer = viewer;
	}
	
	protected abstract File getFile(FileMatchLine line);
	protected abstract boolean isUsed(FileMatchLine line);
	protected abstract void setUsed(FileMatchLine line, boolean used);

	@Override
	protected CellEditor getCellEditor(Object element) {
		return new CheckboxCellEditor(null, SWT.CHECK);
	}

	@Override
	protected boolean canEdit(Object element) {
		if (element == null || !(element instanceof FileMatchLine)) {
			return false;
		}
		
		FileMatchLine line = (FileMatchLine) element;
		
		return getFile(line) != null;
	}

	@Override
	protected Object getValue(Object element) {
		if (element == null || !(element instanceof FileMatchLine)) {
			return false;
		}
		
		FileMatchLine line = (FileMatchLine) element;
		
		return isUsed(line);
	}

	@Override
	protected void setValue(Object element, Object value) {
		if (element == null || !(element instanceof FileMatchLine)) {
			return;
		}
		
		FileMatchLine line = (FileMatchLine) element;
		setUsed(line, (Boolean) value);
		viewer.update(element, null);
	}

}
