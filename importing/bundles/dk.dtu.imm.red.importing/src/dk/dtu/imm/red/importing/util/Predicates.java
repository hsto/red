package dk.dtu.imm.red.importing.util;

import java.io.File;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.importing.util.predicate.Predicate;
import dk.dtu.imm.red.importing.util.predicate.ProjectFilePredicate;
import dk.dtu.imm.red.importing.util.predicate.UniqueIdPredicate;

public class Predicates {

	private Predicates() {
	}
	
	public static Predicate<Element> hasUniqueId(String uniqueId) {
		return new UniqueIdPredicate(uniqueId);
	}
	
	public static Predicate<Element> hasUniqueId(Element element) {
		return new UniqueIdPredicate(element.getUniqueID());
	}
	
	public static Predicate<File> isProjectFile = new ProjectFilePredicate();
	
}
