package dk.dtu.imm.red.importing.file.strategy;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.change.Addition;
import dk.dtu.imm.red.importing.change.Deletion;
import dk.dtu.imm.red.importing.change.Move;
import dk.dtu.imm.red.importing.change.Update;
import dk.dtu.imm.red.importing.util.Asserts;
import dk.dtu.imm.red.importing.util.Elements;
import dk.dtu.imm.red.importing.util.Iterables;
import dk.dtu.imm.red.importing.util.Operations;
import dk.dtu.imm.red.importing.util.Predicates;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class NewMergeStrategy implements Strategy<Element, IStatus> {

	private Project currentProject;
	private Project newProject;
	private boolean updatesAllowed;
	private boolean movesAllowed;
	private boolean additionsAllowed;
	private boolean deletionsAllowed;
	private Strategy<Addition, IStatus> additionStrategy;
	private Strategy<Deletion, IStatus> deletionStrategy;
	private Strategy<Move, IStatus> moveStrategy;
	private Strategy<Update, IStatus> updateStrategy;
	
	public NewMergeStrategy(Project currentProject, Project newProject, boolean additionsAllowed,
		boolean deletionsAllowed, boolean movesAllowed, boolean updatesAllowed,
		Strategy<Deletion, IStatus> deletionStrategy, Strategy<Addition, IStatus> additionStrategy,
		Strategy<Move, IStatus> moveStrategy, Strategy<Update, IStatus> updateStrategy) {
		super();
		this.currentProject = currentProject;
		this.newProject = newProject;
		this.additionsAllowed = additionsAllowed;
		this.deletionsAllowed = deletionsAllowed;
		this.movesAllowed = movesAllowed;
		this.updatesAllowed = updatesAllowed;
		this.deletionStrategy = deletionStrategy;
		this.additionStrategy = additionStrategy;
		this.moveStrategy = moveStrategy;
		this.updateStrategy = updateStrategy;
	}
	
	@Override
	public IStatus execute(Element currentElement) {
		Asserts.notNull(currentElement, "currentElement is null");
		Element newElement = Elements.getElement(newProject, currentElement);
		if (newElement != null) {
			merge(currentElement, newElement);
		}
		
		return null;
	}
	
	private void merge(EObject currentEObject, EObject newEObject) {
		EList<EStructuralFeature> features = currentEObject.eClass().getEAllStructuralFeatures();

		for (EStructuralFeature feature : features) {
			EReference reference = null;
			boolean isContainment = false;
			boolean isMany = false;
			boolean isElement = false;

			if (feature instanceof EReference) {
				reference = (EReference) feature;
				isContainment = reference.isContainment();
				isMany = reference.isMany();
				isElement = ElementPackage.eINSTANCE.getElement().isSuperTypeOf(reference.getEReferenceType());
			}

			Object currentValue = currentEObject.eGet(feature, true);
			Object newValue = newEObject.eGet(feature, true);
			boolean update = false;

			if (feature.isTransient()) {
				// Ignore the feature if it's transient.
			} else if (feature instanceof EAttribute) {
				if (currentValue != null) {
					if (!currentValue.equals(newValue)) {
						update = true;
					}
				} else if (newValue != null) {
					update = true;
				}
			} else if (isContainment) {
				if (isMany && isElement) {
					// Ignore warnings, the check was made with isElement.
					@SuppressWarnings("unchecked")
					EList<Element> currentElements = (EList<Element>) currentValue;
					@SuppressWarnings("unchecked")
					EList<Element> newElements = (EList<Element>) newValue;
					merge(currentElements, newElements);
				} else if (!isMany && !isElement && currentValue != null && newValue != null) {
					merge((EObject) currentValue, (EObject) newValue);
				} else {
					// Mostly the case for many-valued containment non-element
					// references.
					update = true;
				}
			} else {
				update = true;
			}

			if (updatesAllowed && update) {
				updateStrategy.execute(new Update(currentEObject, feature, newValue));
			}
		}
	}
	
	private void merge(EList<Element> currentElements, EList<Element> newElements) {
		List<Addition> additions = new ArrayList<>();
		List<Deletion> deletions = new ArrayList<>();
		List<Move> moves = new ArrayList<>();

		for (Element newElement : newElements) {
			Element currentElement = Elements.getElement(currentProject, newElement);

			if (currentElement != null) {
				merge(currentElement, newElement);

				boolean inCurrentElements = Iterables.contains(currentElements, Predicates.hasUniqueId(newElement));
				if (movesAllowed && !inCurrentElements) {
					moves.add(new Move(currentElements, currentElement));
				}
			} else if (additionsAllowed) {
				additions.add(new Addition(currentElements, newElement));
			}
		}

		for (Element currentElement : currentElements) {
			Element newElement = Elements.getElement(newProject, currentElement);
			if (deletionsAllowed && newElement == null) {
				deletions.add(new Deletion(currentElement));
			}
		}

		Operations.iterate(deletions, deletionStrategy);
		Operations.iterate(additions, additionStrategy);
		Operations.iterate(moves, moveStrategy);
	}

}
