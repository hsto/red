package dk.dtu.imm.red.importing.file.strategy;

import java.util.Iterator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.util.Elements;
import dk.dtu.imm.red.importing.util.Iterators;
import dk.dtu.imm.red.importing.util.Operations;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class AddFileStrategy implements Strategy<File, IStatus> {
	
	private Project project;
	private Strategy<Element, IStatus> removeOverlapsStrategy;

	public AddFileStrategy(Project project, Strategy<Element, IStatus> removeOverlapsStrategy) {
		super();
		this.project = project;
		this.removeOverlapsStrategy = removeOverlapsStrategy;
	}

	@Override
	public IStatus execute(File file) {
		System.out.println("Delete: " + file);
		
		URI inputURI = file.eResource().getURI();
		String fileName = inputURI.lastSegment();
		for (File existingFile : project.getListofopenedfilesinproject()) { // Should not override existing file
			if (existingFile.getName() != null && existingFile.getName().equals(file.getName())) {
				fileName = "New " + file.getName();
				file.setName(fileName);
				break;
			}
		}
		String outputPath = Elements.getProjectPath(project) + java.io.File.separator + fileName;
		URI outputURI = URI.createFileURI(outputPath);
		Resource resource = file.eResource();
		resource.setURI(outputURI);
		
		Iterator<Element> elementIterator = Iterators.cast(file.eAllContents(), Element.class);
		Operations.iterate(elementIterator, removeOverlapsStrategy).execute();

		project.addToListOfFilesInProject(file);
		
		return Status.OK_STATUS;
	}

}
