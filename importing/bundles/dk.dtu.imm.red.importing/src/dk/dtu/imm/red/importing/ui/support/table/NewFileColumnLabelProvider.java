package dk.dtu.imm.red.importing.ui.support.table;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.importing.ui.model.FileMatchLine;

public class NewFileColumnLabelProvider extends FileColumnLabelProvider {

	@Override
	protected File getFile(FileMatchLine line) {
		return line.getNewFile();
	}

	@Override
	protected boolean isUsed(FileMatchLine line) {
		return line.getIsNewUsed();
	}

	

}
