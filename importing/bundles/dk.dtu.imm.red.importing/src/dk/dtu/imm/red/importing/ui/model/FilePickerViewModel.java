package dk.dtu.imm.red.importing.ui.model;

import dk.dtu.imm.red.importing.ui.util.ModelObject;

public class FilePickerViewModel extends ModelObject {
	
	public static final String FILE_PATH = "filePath";
	
	private String filePath;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		firePropertyChange(FILE_PATH, this.filePath, this.filePath = filePath);
	}
	
}
