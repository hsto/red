package dk.dtu.imm.red.importing.util.operation;

import java.util.Iterator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class IteratorOperation<T> implements Operation {

	private Iterator<? extends T> iterator;
	private Strategy<T, IStatus> strategy;
	
	public IteratorOperation(Strategy<T, IStatus> strategy, Iterator<? extends T> iterator) {
		super();
		this.strategy = strategy;
		this.iterator = iterator;
	}
	
	public IteratorOperation(Strategy<T, IStatus> strategy, Iterable<? extends T> values) {
		this(strategy, values.iterator());
	}

	@Override
	public IStatus execute() {
		IStatus status = Status.OK_STATUS;
		
		while (iterator.hasNext()) {
			IStatus newStatus = strategy.execute(iterator.next());
			
			if (newStatus != Status.OK_STATUS) {
				status = newStatus;
			}
		}
		
		return status;
	}
	
}
