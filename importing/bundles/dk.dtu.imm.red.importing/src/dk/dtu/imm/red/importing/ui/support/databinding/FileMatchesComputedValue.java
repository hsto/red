package dk.dtu.imm.red.importing.ui.support.databinding;

import java.util.ArrayList;

import org.eclipse.core.databinding.observable.value.ComputedValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.importing.ui.model.FileMatchLine;
import dk.dtu.imm.red.importing.ui.util.Result;
import dk.dtu.imm.red.importing.util.Iterables;

public class FileMatchesComputedValue extends ComputedValue {

	private IObservableValue currentFiles;
	private IObservableValue newFiles;

	public FileMatchesComputedValue(IObservableValue currentFiles, IObservableValue newFiles) {
		this.currentFiles = currentFiles;
		this.newFiles = newFiles;
	}

	@Override
	protected Result<ArrayList<FileMatchLine>> calculate() {
		if (currentFiles.getValue() == null || newFiles.getValue() == null) {
			return null;
		}

		Iterable<File> currentFiles = Iterables.cast((Iterable<?>) this.currentFiles.getValue(), File.class);
		Iterable<File> newFiles = Iterables.cast((Iterable<?>) this.newFiles.getValue(), File.class);

		ArrayList<FileMatchLine> lines = new ArrayList<>();
		ArrayList<FileMatchLine> newFileLines = new ArrayList<>();
		
		for (File currentFile : currentFiles) {
			if (currentFile.getName() != null) {
				lines.add(new FileMatchLine(currentFile, null, true, false));				
			}
		}
		
		for (File newFile : newFiles) {
			FileMatchLine matchingLine = null;
			
			for (FileMatchLine line : lines) {
				if (line.getCurrentFile().getUniqueID().equals(newFile.getUniqueID())) {
					matchingLine = line;
				}
			}
			
			if (matchingLine != null) {
				matchingLine.setNewFile(newFile);
			} else {
				if (newFile.getName() != null) {
					newFileLines.add(new FileMatchLine(null, newFile, true, false));					
				}
			}
		}

		lines.addAll(newFileLines);

		return Result.value(lines);
	}

}
