package dk.dtu.imm.red.importing.util.strategy;

import org.eclipse.emf.ecore.EObject;

import dk.dtu.imm.red.core.element.Element;

public class GetElementStrategy implements Strategy<EObject, Element> {
	
	private String uniqueID;

	public GetElementStrategy(String uniqueID) {
		super();
		this.uniqueID = uniqueID;
	}

	@Override
	public Element execute(EObject eObject) {
		Element element = null;		
		if (eObject.eResource() != null) {
			element = (Element) eObject.eResource().getEObject(uniqueID);			
		}
		
		if (element == null || !(element instanceof Element)) {
			return null;
		}
		
		return (Element) element;
	}

}
