package dk.dtu.imm.red.importing.util.iterable;

import java.util.Iterator;

import dk.dtu.imm.red.importing.util.iterator.MappedIterator;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class MappedIterable<TIn, TOut> implements Iterable<TOut> {

	private Iterable<? extends TIn> iterable;
	private Strategy<TIn, TOut> strategy;
	
	public MappedIterable(Iterable<? extends TIn> iterable, Strategy<TIn, TOut> strategy) {
		super();
		this.iterable = iterable;
		this.strategy = strategy;
	}

	@Override
	public Iterator<TOut> iterator() {
		return new MappedIterator<>(iterable.iterator(), strategy);
	}
	
}
