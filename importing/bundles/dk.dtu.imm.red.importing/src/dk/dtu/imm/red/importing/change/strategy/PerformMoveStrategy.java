package dk.dtu.imm.red.importing.change.strategy;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.imm.red.importing.change.Move;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class PerformMoveStrategy implements Strategy<Move, IStatus> {

	@Override
	public IStatus execute(Move input) {
		System.out.println(input);
		try {
			EcoreUtil.remove(input.getElement());
			input.getContainingList().add(input.getElement());
			return Status.OK_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return Status.CANCEL_STATUS;
		}
	}
	
}
