package dk.dtu.imm.red.importing.ui.support.table;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.importing.ui.model.FileMatchLine;

public abstract class FileColumnLabelProvider extends ColumnLabelProvider {
	
	protected abstract File getFile(FileMatchLine line);
	
	protected abstract boolean isUsed(FileMatchLine line);
	
	@Override
	public String getText(Object element) {
		if (element == null || !(element instanceof FileMatchLine)) {
			return "";
		}
		
		FileMatchLine fileMatch = (FileMatchLine) element;
		File file = getFile(fileMatch);
		
		if (file != null) {
			String prefix = isUsed(fileMatch) ? "✓ " : "";
			return prefix + file.getName();
		}
		return "";
	}
	
}
