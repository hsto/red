package dk.dtu.imm.red.importing.change;

import java.util.ArrayList;
import java.util.Collection;

public class ChangeContainer {

	private final Iterable<Addition> additions;
	private final Iterable<Deletion> deletions;
	private final Iterable<Move> moves;
	private final Iterable<Update> updates;

	private ChangeContainer(Collection<Addition> additions, Collection<Deletion> deletions, Collection<Move> moves,
		Collection<Update> updates) {
		this.additions = additions;
		this.deletions = deletions;
		this.moves = moves;
		this.updates = updates;
	}
	
	public static Builder builder() {
		return new Builder();
	}
	
	public Iterable<Addition> getAdditions() {
		return additions;
	}
	
	public Iterable<Deletion> getDeletions() {
		return deletions;
	}
	
	public Iterable<Move> getMoves() {
		return moves;
	}
	
	public Iterable<Update> getUpdates() {
		return updates;
	}

	public static class Builder {
		private final Collection<Addition> additions = new ArrayList<>();
		private final Collection<Deletion> deletions = new ArrayList<>();
		private final Collection<Move> moves = new ArrayList<>();
		private final Collection<Update> updates = new ArrayList<>();

		private Builder() {
		}

		public Builder add(Addition value) {
			additions.add(value);
			return this;
		}

		public Builder add(Deletion value) {
			deletions.add(value);
			return this;
		}

		public Builder add(Move value) {
			moves.add(value);
			return this;
		}

		public Builder add(Update value) {
			updates.add(value);
			return this;
		}

		public ChangeContainer build() {
			return new ChangeContainer(additions, deletions, moves, updates);
		}
	}

}
