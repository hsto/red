package dk.dtu.imm.red.importing.ui.support.table;

import org.eclipse.jface.viewers.TableViewer;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.importing.ui.model.FileMatchLine;

public class CurrentFileEditingSupport extends FileEditingSupport {

	public CurrentFileEditingSupport(TableViewer viewer) {
		super(viewer);
	}

	@Override
	protected File getFile(FileMatchLine line) {
		return line.getCurrentFile();
	}

	@Override
	protected boolean isUsed(FileMatchLine line) {
		return line.getIsCurrentUsed();
	}

	@Override
	protected void setUsed(FileMatchLine line, boolean used) {
		line.setIsCurrentUsed(used);
	}

}
