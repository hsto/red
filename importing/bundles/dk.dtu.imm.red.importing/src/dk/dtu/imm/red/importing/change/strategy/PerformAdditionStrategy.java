package dk.dtu.imm.red.importing.change.strategy;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.importing.change.Addition;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class PerformAdditionStrategy implements Strategy<Addition, IStatus> {
	
	@Override
	public IStatus execute(Addition input) {
		System.out.println(input);
		try {
			Element element = input.getElement();
			input.getContainingList().add(element);
			return Status.OK_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return Status.CANCEL_STATUS;
		}
	}

}
