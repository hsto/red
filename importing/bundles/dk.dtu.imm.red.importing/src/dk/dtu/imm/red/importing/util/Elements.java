package dk.dtu.imm.red.importing.util;

import java.io.File;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.util.strategy.GetElementStrategy;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class Elements {
	
	private Elements() {
	}
	
	public static Iterable<Element> getElements(Project project, String uniqueID) {
		Asserts.notNull(project, "project is null");
		Asserts.notNull(uniqueID, "uniqueID is null");
		return Iterables.map(project.getListofopenedfilesinproject(), new GetElementStrategy(uniqueID));
	}
	
	public static Iterable<Element> getElements(Project project, Element element) {
		Asserts.notNull(element, "element is null");
		return getElements(project, element.getUniqueID());
	}
	
	public static Element getElement(Project project, String uniqueID) {
		return Iterables.first(getElements(project, uniqueID));
	}

	public static Element getElement(Project project, Element element) {
		Asserts.notNull(element, "element is null");
		return getElement(project, element.getUniqueID());
	}
	
	public static Element getElement(EObject eObject, String uniqueID) {
		Asserts.notNull(eObject, "eObject is null");
		Asserts.notNull(uniqueID, "uniqueID is null");
		Strategy<EObject, Element> strategy = new GetElementStrategy(uniqueID);
		return strategy.execute(eObject);
	}
	
	public static Element getElement(EObject eObject, Element element) {
		Asserts.notNull(element, "element is null");
		return getElement(eObject, element.getUniqueID());
	}
	
	public static String getProjectPath(EObject eObject) {
		Asserts.notNull(eObject, "eObject is null");
		return new File(eObject.eResource().getURI().toFileString()).getParentFile().getAbsolutePath();
	}
	
	public static Project loadProject(dk.dtu.imm.red.core.file.File file) {
		Asserts.notNull(file, "file is null");
		File projectDirectory = new File(getProjectPath(file));
		File projectFile = null;
		
		for (File physicalFile : projectDirectory.listFiles()) {
			if (physicalFile.getName().endsWith(".redproject")) {
				projectFile = physicalFile;
			}
		}
		
		if (projectFile == null) {
			return null;
		}
		
		ResourceSetImpl resourceSet = new ResourceSetImpl();
		Resource projectResource = resourceSet.getResource(URI.createFileURI(projectFile.getAbsolutePath()), true);
		
		return (Project) projectResource.getContents().get(0);
	}
	
	public static dk.dtu.imm.red.core.file.File getFile(Element element) {
		Asserts.notNull(element, "element is null");
		Resource resource = element.eResource();
		if (resource.getContents().size() == 0) {
			return null;
		}
		EObject rootObject = resource.getContents().get(0);
		if (!(rootObject instanceof dk.dtu.imm.red.core.file.File)) {
			return null;
		}
		return (dk.dtu.imm.red.core.file.File) rootObject;
	}
	
}
