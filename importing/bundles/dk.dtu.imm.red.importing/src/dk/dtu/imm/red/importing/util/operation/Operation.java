package dk.dtu.imm.red.importing.util.operation;

import org.eclipse.core.runtime.IStatus;

public interface Operation {

	public IStatus execute();
	
}