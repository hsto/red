package dk.dtu.imm.red.importing.ui.model;

public class Texts {

	public static final String SELECT_PROJECT = "Please select a project.";
	public static final String FILE_NOT_FOUND = "The selected file doesn't exist.";
	public static final String ERROR_OCCURRED = "An error occurred while loading the selected file.";
	public static final String NOT_A_VALID_RED_FILE = "The selected file is not a valid RED-file.";
	
}
