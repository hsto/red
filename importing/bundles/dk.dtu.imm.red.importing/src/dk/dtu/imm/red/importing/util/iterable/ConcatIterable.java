package dk.dtu.imm.red.importing.util.iterable;

import java.util.Iterator;

import dk.dtu.imm.red.importing.util.Iterables;
import dk.dtu.imm.red.importing.util.Iterators;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class ConcatIterable<T> implements Iterable<T> {

	private Iterable<Iterable<T>> iterables;

	public ConcatIterable(Iterable<Iterable<T>> iterables) {
		super();
		this.iterables = iterables;
	}

	@Override
	public Iterator<T> iterator() {
		Strategy<Iterable<T>, Iterator<T>> strategy = new Strategy<Iterable<T>, Iterator<T>>() {
			@Override
			public Iterator<T> execute(Iterable<T> iterable) {
				return iterable.iterator();
			}
		};
		
		return Iterators.concat(Iterables.map(iterables, strategy));
	}
}
