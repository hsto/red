package dk.dtu.imm.red.importing.file.strategy;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.change.Addition;
import dk.dtu.imm.red.importing.change.ChangeContainer;
import dk.dtu.imm.red.importing.change.Deletion;
import dk.dtu.imm.red.importing.change.Move;
import dk.dtu.imm.red.importing.change.Update;
import dk.dtu.imm.red.importing.util.Elements;
import dk.dtu.imm.red.importing.util.Iterables;
import dk.dtu.imm.red.importing.util.Operations;
import dk.dtu.imm.red.importing.util.Pair;
import dk.dtu.imm.red.importing.util.Predicates;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class MergeStrategy implements Strategy<Pair<? extends EObject>, IStatus> {

	private Project currentProject;
	private Project newProject;
	private boolean additionsAllowed;
	private boolean deletionsAllowed;
	private boolean movesAllowed;
	private boolean updatesAllowed;
	private Strategy<Deletion, IStatus> deletionStrategy;
	private Strategy<Addition, IStatus> additionStrategy;
	private Strategy<Move, IStatus> moveStrategy;
	private Strategy<Update, IStatus> updateStrategy;

	public MergeStrategy(Project currentProject, Project newProject, boolean additionsAllowed,
		boolean deletionsAllowed, boolean movesAllowed, boolean updatesAllowed,
		Strategy<Deletion, IStatus> deletionStrategy, Strategy<Addition, IStatus> additionStrategy,
		Strategy<Move, IStatus> moveStrategy, Strategy<Update, IStatus> updateStrategy) {
		super();
		this.currentProject = currentProject;
		this.newProject = newProject;
		this.additionsAllowed = additionsAllowed;
		this.deletionsAllowed = deletionsAllowed;
		this.movesAllowed = movesAllowed;
		this.updatesAllowed = updatesAllowed;
		this.deletionStrategy = deletionStrategy;
		this.additionStrategy = additionStrategy;
		this.moveStrategy = moveStrategy;
		this.updateStrategy = updateStrategy;
	}

	@Override
	public IStatus execute(Pair<? extends EObject> pair) {		
		EObject currentEObject = pair.getFirst();
		EObject newEObject = pair.getSecond();
		
		ChangeContainer.Builder builder = ChangeContainer.builder();

		merge(currentEObject, newEObject, builder);
		
		ChangeContainer changeContainer = builder.build();
		
		Operations.iterate(changeContainer.getDeletions(), deletionStrategy).execute();
		Operations.iterate(changeContainer.getAdditions(), additionStrategy).execute();
		Operations.iterate(changeContainer.getMoves(), moveStrategy).execute();
		Operations.iterate(changeContainer.getUpdates(), updateStrategy).execute();

		return Status.OK_STATUS;
	}

	private void merge(EObject currentEObject, EObject newEObject, ChangeContainer.Builder builder) {
		EList<EStructuralFeature> features = currentEObject.eClass().getEAllStructuralFeatures();

		for (EStructuralFeature feature : features) {
			EReference reference = null;
			boolean isContainment = false;
			boolean isMany = false;
			boolean isElement = false;

			if (feature instanceof EReference) {
				reference = (EReference) feature;
				isContainment = reference.isContainment();
				isMany = reference.isMany();
				isElement = ElementPackage.eINSTANCE.getElement().isSuperTypeOf(reference.getEReferenceType());
			}

			Object currentValue = currentEObject.eGet(feature, true);
			Object newValue = newEObject.eGet(feature, true);
			boolean update = false;

			if (feature.isTransient()) {
				// Ignore the feature if it's transient.
			} else if (feature instanceof EAttribute) {
				if (currentValue != null) {
					if (!currentValue.equals(newValue)) {
						update = true;
					}
				} else if (newValue != null) {
					update = true;
				}
			} else if (isContainment) {
				if (isMany && isElement) {
					// Ignore warnings, the check was made with isElement.
					@SuppressWarnings("unchecked")
					EList<Element> currentElements = (EList<Element>) currentValue;
					@SuppressWarnings("unchecked")
					EList<Element> newElements = (EList<Element>) newValue;
					merge(currentElements, newElements, builder);
				} else if (!isMany && currentValue != null && newValue != null) {
					merge((EObject) currentValue, (EObject) newValue, builder);
				} else {
					// Mostly the case for many-valued containment non-element
					// references.
					update = true;
				}
			} else {
				update = true;
			}

			if (updatesAllowed && update) {
				builder.add(new Update(currentEObject, feature, newValue));
			}
		}
	}

	private void merge(EList<Element> currentElements, EList<Element> newElements, ChangeContainer.Builder builder) {
		for (Element newElement : newElements) {
			Element currentElement = Elements.getElement(currentProject, newElement);

			if (currentElement != null) {
				merge(currentElement, newElement, builder);

				boolean inCurrentElements = Iterables.contains(currentElements, Predicates.hasUniqueId(newElement));
				if (movesAllowed && !inCurrentElements) {
					builder.add(new Move(currentElements, currentElement));
				}
			} else if (additionsAllowed) {
				builder.add(new Addition(currentElements, newElement));
				addChildren(newElement, builder);
			}
		}

		for (Element currentElement : currentElements) {
			Element newElement = Elements.getElement(newProject, currentElement);
			if (deletionsAllowed && newElement == null) {
				builder.add(new Deletion(currentElement));
			}
		}
	}
	
	private void addChildren(Element element, ChangeContainer.Builder builder) {
		if (element instanceof Group) {
			Group group = (Group) element;
			// Copy to avoid concurrent modification
			Collection<Element> newElements = new ArrayList<>(group.getContents());
			for (Element newElement : newElements) {
				EcoreUtil.remove(newElement);
				Element currentElement = Elements.getElement(currentProject, newElement);
				if (currentElement != null) {
					merge(currentElement, newElement, builder);
					if (movesAllowed) {
						builder.add(new Move(group.getContents(), currentElement));
					}
				} else {
					builder.add(new Addition(group.getContents(), newElement));
				}
			}
		}
	}
}
