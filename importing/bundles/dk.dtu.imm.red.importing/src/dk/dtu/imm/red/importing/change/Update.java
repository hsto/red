package dk.dtu.imm.red.importing.change;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class Update {

	private EObject eObject;
	private EStructuralFeature feature;
	private Object newValue;

	public Update(EObject eObject, EStructuralFeature feature, Object newValue) {
		super();
		this.eObject = eObject;
		this.feature = feature;
		this.newValue = newValue;
	}

	public EObject getEObject() {
		return eObject;
	}

	public EStructuralFeature getFeature() {
		return feature;
	}

	public Object getNewValue() {
		return newValue;
	}

}
