package dk.dtu.imm.red.importing.util.iterator;

import java.util.Iterator;

import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class MappedIterator<TIn, TOut> implements Iterator<TOut> {
	
	private Iterator<? extends TIn> iterator;
	private Strategy<TIn, TOut> strategy;
	private TOut next;

	public MappedIterator(Iterator<? extends TIn> iterator, Strategy<TIn, TOut> strategy) {
		this.iterator = iterator;
		this.strategy = strategy;
	}

	@Override
	public boolean hasNext() {
		advance();
		
		return next != null;
	}

	@Override
	public TOut next() {
		advance();
		
		TOut current = next;
		next = null;
		
		return current;
	}

	@Override
	public void remove() {
	}

	private void advance() {
		while (next == null && iterator.hasNext()) {
			next = strategy.execute(iterator.next());
		}
	}
	
}
