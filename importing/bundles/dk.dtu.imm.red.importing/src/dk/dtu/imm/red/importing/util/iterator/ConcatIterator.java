package dk.dtu.imm.red.importing.util.iterator;

import java.util.Iterator;

public class ConcatIterator<T> implements Iterator<T> {

	private Iterator<Iterator<T>> iterators;
	private Iterator<T> current;

	public ConcatIterator(Iterator<Iterator<T>> iterators) {
		super();

		this.iterators = iterators;
		this.current = iterators.next();
	}
	
	public ConcatIterator(Iterable<Iterator<T>> iterators) {
		this(iterators.iterator());
	}

	@Override
	public boolean hasNext() {
		if (!current.hasNext()) {
			advanceIterators();
		}
		
		return current.hasNext();
	}

	@Override
	public T next() {
		if (!current.hasNext()) {
			advanceIterators();
		}
		
		return current.next();
	}

	@Override
	public void remove() {
		throw new IllegalStateException("Operation 'remove' is not implemented.");
	}

	private void advanceIterators() {
		while (!current.hasNext() && iterators.hasNext()) {
			current = iterators.next();
		}
	}

}
