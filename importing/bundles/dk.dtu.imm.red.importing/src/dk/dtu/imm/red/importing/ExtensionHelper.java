package dk.dtu.imm.red.importing;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;

public class ExtensionHelper {

	public static void openElement(final Element element) throws Exception {
		IConfigurationElement[] configurationElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
			"dk.dtu.imm.red.core.element");
		for (IConfigurationElement configurationElement : configurationElements) {

			final Object extension;
			extension = configurationElement.createExecutableExtension("class");

			if (extension instanceof IElementExtensionPoint) {
				ISafeRunnable runnable = new ISafeRunnable() {
					@Override
					public void handleException(final Throwable exception) {
						exception.printStackTrace();
					}

					@Override
					public void run() throws Exception {
						((IElementExtensionPoint) extension).openElement(element);
					}
				};
				runnable.run();
			}
		}
	}
	
	public static void deleteElement(final Element element) throws Exception {
		IConfigurationElement[] configurationElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
			"dk.dtu.imm.red.core.element");
		
		for (IConfigurationElement configurationElement : configurationElements) {

			final Object extension = configurationElement.createExecutableExtension("class");

			if (extension instanceof IElementExtensionPoint) {
				ISafeRunnable runnable = new ISafeRunnable() {

					@Override
					public void handleException(final Throwable exception) {
						exception.printStackTrace();
					}

					@Override
					public void run() throws Exception {
						((IElementExtensionPoint) extension).deleteElement(element);
					}
				};
				runnable.run();
			}
		}
	}

}
