package dk.dtu.imm.red.importing.ui.util;

import org.eclipse.core.databinding.conversion.Converter;

public class ResultConverter extends Converter {

	public ResultConverter() {
		super(null, null);
	}

	@Override
	public Object convert(Object fromObject) {
		Result<?> result = (Result<?>) fromObject;
		
		if (result == null) {
			return null;
		}
		
		return result.getValue();
	}

}
