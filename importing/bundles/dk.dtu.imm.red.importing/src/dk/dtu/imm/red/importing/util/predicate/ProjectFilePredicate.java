package dk.dtu.imm.red.importing.util.predicate;

import java.io.File;

public class ProjectFilePredicate implements Predicate<File> {

	@Override
	public boolean evaluate(File file) {
		return file.getName().endsWith(".redproject");
	}

}
