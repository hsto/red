package dk.dtu.imm.red.importing.ui.views;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.dialog.TitleAreaDialogSupport;
import org.eclipse.jface.databinding.dialog.ValidationMessageProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import dk.dtu.imm.red.importing.ui.model.AddFileDialogModel;
import dk.dtu.imm.red.importing.ui.model.FileMatchLine;
import dk.dtu.imm.red.importing.ui.model.FileMatcherViewModel;
import dk.dtu.imm.red.importing.ui.model.FilePickerViewModel;
import dk.dtu.imm.red.importing.ui.model.OptionsViewModel;
import dk.dtu.imm.red.importing.ui.model.Texts;
import dk.dtu.imm.red.importing.ui.support.databinding.FileMatchesComputedValue;
import dk.dtu.imm.red.importing.ui.support.databinding.FilesComputedValue;
import dk.dtu.imm.red.importing.ui.support.databinding.NewProjectComputedValue;
import dk.dtu.imm.red.importing.ui.support.databinding.ObservableListComputedValue;
import dk.dtu.imm.red.importing.ui.util.ResultUpdateValueStrategy;

public class AddFileDialog extends TitleAreaDialog {

	private final AddFileDialogModel model;
	private final FilePickerViewModel filePickerViewModel;
	private final FileMatcherViewModel fileMatcherViewModel;
	private final OptionsViewModel optionsViewModel;
	private FileMatcherView fileMatcherView;

	public AddFileDialog(Shell parentShell, AddFileDialogModel model, FilePickerViewModel filePickerViewModel,
		FileMatcherViewModel fileMatchViewModel, OptionsViewModel optionsViewModel) {
		super(parentShell);
		this.model = model;
		this.filePickerViewModel = filePickerViewModel;
		this.fileMatcherViewModel = fileMatchViewModel;
		this.optionsViewModel = optionsViewModel;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Import/merge from another project");
		
		setMessage(Texts.SELECT_PROJECT, IMessageProvider.INFORMATION);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		// Create container
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(2, false));
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		// Create file picker control
		Control filePickerView = new FilePickerView(container, SWT.NONE, filePickerViewModel);
		filePickerView.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
		
		// Create file match control
		fileMatcherView = new FileMatcherView(container, SWT.NONE, fileMatcherViewModel);
		GridData fileMatchGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		fileMatchGridData.heightHint = 200;
		fileMatcherView.setLayoutData(fileMatchGridData);

		// Create the options control
		OptionsView optionsView = new OptionsView(container, SWT.NONE, optionsViewModel);
		optionsView.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		
		// Create the log control
//		LogView logView = new LogView(container, SWT.NONE);
//		GridData logGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
//		logGridData.heightHint = 100;
//		logView.setLayoutData(logGridData);

		// Setup data-binding.
		DataBindingContext dbc = new DataBindingContext();

		IObservableValue currentProject = BeansObservables.observeValue(model, AddFileDialogModel.CURRENT_PROJECT);
		IObservableValue currentFiles = new FilesComputedValue(currentProject);
//		fileMatcherView.setCurrentProject(currentProject);
		fileMatcherView.setInitData(currentFiles);

		// Bind to FilePickerViewModel and load the selected file.
		IObservableValue projectPath = BeansObservables.observeValue(filePickerViewModel,
			FilePickerViewModel.FILE_PATH);
		IObservableValue newProjectResult = new NewProjectComputedValue(projectPath);
		IObservableValue newProject = bindResult(dbc, newProjectResult, 
			BeansObservables.observeValue(model, AddFileDialogModel.NEW_PROJECT));
		IObservableValue newFiles = new FilesComputedValue(newProject);

		IObservableValue fileMatchesResult = new FileMatchesComputedValue(currentFiles, newFiles);
		IObservableValue fileMatches = bindResult(dbc, fileMatchesResult,
			BeansObservables.observeValue(model, AddFileDialogModel.FILE_MATCHES));
		IObservableValue observableFileMatches = new ObservableListComputedValue(fileMatches, FileMatchLine.class);

		// Bind to FileMatcherViewModel.
		IObservableValue fileMatcherInput = BeansObservables.observeValue(fileMatcherViewModel,
			FileMatcherViewModel.INPUT);
		dbc.bindValue(observableFileMatches, fileMatcherInput,
			null,
			new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER));

		// Bind dialog message to the latest validation message.
		TitleAreaDialogSupport.create(this, dbc).setValidationMessageProvider(new ValidationMessageProvider());

		return area;
	}

	private IObservableValue bindResult(DataBindingContext dbc, IObservableValue target, IObservableValue model) {
		dbc.bindValue(target, model,
			new ResultUpdateValueStrategy(),
			new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER));
		return model;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		((GridLayout) parent.getLayout()).numColumns++;
		Button buttonApply = new Button(parent, SWT.PUSH);
		buttonApply.setText("Apply");
		buttonApply.setFont(JFaceResources.getDialogFont());
		buttonApply.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				
			}
		});
		buttonApply.setEnabled(false);
		setButtonLayoutData(buttonApply);
		
		//createButton(parent, IDialogConstants.OK_ID, "Apply", true);		
		createButton(parent, IDialogConstants.OK_ID, "Execute", true);		
	    createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}
	
	@Override
	protected void okPressed() {
		String message = fileMatcherView.getCheckReferencesMessage();
		if (message.isEmpty()) {
			super.okPressed();
		} else{
			message = "Warning! You will loose following references if you continue:\n" + message;
			MessageDialog md = new MessageDialog(getShell(), "Warning!", null, message, MessageDialog.CONFIRM, new String[] { "Continue", "Cancel" }, 0);
			int res = md.open();
			if (res == Dialog.OK) {			
				super.okPressed();
			}			
		}
	}

	@Override
	protected boolean isResizable() {
		return true;
	}
}
