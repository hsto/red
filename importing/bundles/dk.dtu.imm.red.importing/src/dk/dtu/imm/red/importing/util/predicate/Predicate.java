package dk.dtu.imm.red.importing.util.predicate;

public interface Predicate<T> {
	
	boolean evaluate(T value);
	
}
