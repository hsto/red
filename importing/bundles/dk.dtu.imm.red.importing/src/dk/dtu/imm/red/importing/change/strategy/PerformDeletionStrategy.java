package dk.dtu.imm.red.importing.change.strategy;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.imm.red.importing.change.Deletion;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class PerformDeletionStrategy implements Strategy<Deletion, IStatus> {

	@Override
	public IStatus execute(Deletion input) {
		System.out.println(input);
		EcoreUtil.remove(input.getCurrentEObject());
		return Status.OK_STATUS;
	}
	
}
