package dk.dtu.imm.red.importing.ui.views;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import dk.dtu.imm.red.importing.ui.model.OptionsViewModel;

public class OptionsView extends Composite {

	private OptionsViewModel model;

	public OptionsView(Composite parent, int style, OptionsViewModel model) {
		super(parent, style);
		this.model = model;
		createContents();
	}

	private void createContents() {
		DataBindingContext dbc = new DataBindingContext();

		// Set layout
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		gridLayout.marginRight = 5;
		gridLayout.verticalSpacing = 0;
		gridLayout.horizontalSpacing = 0;

		setLayout(gridLayout);

		// Create controls
//		Label headerLabel = new Label(this, SWT.NONE);
//		GridData gridData = new GridData(SWT.LEFT, SWT.CENTER,
//			false, false, 1, 1);
//		headerLabel.setLayoutData(gridData);
//		headerLabel.setText("Options:");

		String[] options = { OptionsViewModel.IS_ADDITIONS_ALLOWED, OptionsViewModel.IS_DELETIONS_ALLOWED,
			OptionsViewModel.IS_MOVES_ALLOWED, OptionsViewModel.IS_UPDATES_ALLOWED };
		String[] optionTexts = { "Allow additions", "Allow deletions", "Allow moves", "Allow updates" };

		for (int i = 0; i < options.length; i++) {
			Button checkbox = new Button(this, SWT.CHECK);
			checkbox.setText(optionTexts[i]);

			IObservableValue checkboxSelection = WidgetProperties.selection().observe(checkbox);
			IObservableValue modelValue = BeansObservables.observeValue(model, options[i]);
			dbc.bindValue(checkboxSelection, modelValue);
		}
	}

}
