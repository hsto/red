package dk.dtu.imm.red.importing.file.strategy;

import java.io.IOException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.util.Asserts;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class DeleteFileStrategy implements Strategy<File, IStatus> {

	private Project project;
	
	public DeleteFileStrategy(Project project) {
		super();
		this.project = project;
	}

	@Override
	public IStatus execute(File file) {
		System.out.println("Delete: " + file);
		
		Asserts.notNull(file, "file is null");

		project.getListofopenedfilesinproject().remove(file);
		
		try {
			file.eResource().delete(null);
		} catch (IOException e) {
			e.printStackTrace();
			return Status.CANCEL_STATUS;
		}
		
		return Status.OK_STATUS;
	}

}
