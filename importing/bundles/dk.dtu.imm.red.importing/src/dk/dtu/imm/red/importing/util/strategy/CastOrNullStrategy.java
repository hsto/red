package dk.dtu.imm.red.importing.util.strategy;



public class CastOrNullStrategy<TOut> implements Strategy<Object, TOut> {
	
	private Class<TOut> classT;

	public CastOrNullStrategy(Class<TOut> classT) {
		this.classT = classT;
	}

	@Override
	public TOut execute(Object input) {
		if (classT.isInstance(input)) {
			@SuppressWarnings("unchecked")
			TOut output = (TOut) input;
			return output;
		}
		
		return null;
	}
	
}
