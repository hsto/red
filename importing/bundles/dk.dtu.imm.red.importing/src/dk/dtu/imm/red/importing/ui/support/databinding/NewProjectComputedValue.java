package dk.dtu.imm.red.importing.ui.support.databinding;

import java.util.Arrays;

import org.eclipse.core.databinding.observable.value.ComputedValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.ui.model.Texts;
import dk.dtu.imm.red.importing.ui.util.Result;
import dk.dtu.imm.red.importing.util.Iterables;
import dk.dtu.imm.red.importing.util.Predicates;

public class NewProjectComputedValue extends ComputedValue {

	private IObservableValue projectPath;

	public NewProjectComputedValue(IObservableValue projectPath) {
		super();
		this.projectPath = projectPath;
	}

	@Override
	protected Result<Project> calculate() {
		String projectPath = (String) this.projectPath.getValue();

		if (projectPath == null || projectPath.isEmpty()) {
			return Result.info(Texts.SELECT_PROJECT);
		}

		java.io.File physicalFolder = new java.io.File(projectPath);
		
		if (!physicalFolder.exists()) {
			return Result.error("The selected folder doesn't exist.");
		}
		
		java.io.File projectFile = Iterables.first(Arrays.asList(physicalFolder.listFiles()), Predicates.isProjectFile);

		if (projectFile == null) {
			return Result.error("The selected folder does not contain a RED project.");
		}

		String projectFilePath = projectFile.getAbsolutePath();

		ResourceSet resourceSet = new ResourceSetImpl();
		URI projectFileURI = URI.createFileURI(projectFilePath);
		Resource projectResource = resourceSet.getResource(projectFileURI, true);

		Project project = (Project) projectResource.getContents().get(0);		

		return Result.value(project);
	}
	
}
