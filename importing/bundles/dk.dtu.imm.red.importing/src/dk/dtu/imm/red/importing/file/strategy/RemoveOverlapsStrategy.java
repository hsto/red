package dk.dtu.imm.red.importing.file.strategy;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.util.Elements;
import dk.dtu.imm.red.importing.util.Pair;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class RemoveOverlapsStrategy implements Strategy<Element, IStatus> {
	
	private Project project;
	private boolean movesAllowed;
	private Strategy<Pair<? extends EObject>, IStatus> mergeStrategy;
	
	public RemoveOverlapsStrategy(Project project, boolean movesAllowed, Strategy<Pair<? extends EObject>, IStatus> mergeStrategy) {
		super();
		this.project = project;
		this.movesAllowed = movesAllowed;
		this.mergeStrategy = mergeStrategy;
	}

	@Override
	public IStatus execute(Element newElement) {
		Element currentElement = Elements.getElement(project, newElement.getUniqueID());
		
		if (currentElement != null) {
			mergeStrategy.execute(new Pair<>(currentElement, newElement));
			if (movesAllowed) {
				EcoreUtil.remove(currentElement);
				EcoreUtil.replace(newElement, currentElement);
			} else {
				EcoreUtil.remove(newElement);
			}
		}
		
		return Status.OK_STATUS;
	}	
	
}
