package dk.dtu.imm.red.importing.ui.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.importing.ui.util.ModelObject;

public class FileMatchLine extends ModelObject {

	public static final String CURRENT_FILE = "currentFile";
	public static final String NEW_FILE = "newFile";
	public static final String IS_CURRENT_USED = "isCurrentUsed";
	public static final String IS_NEW_USED = "isNewUsed";
	public static final String CURRENT_REFERENCES = "currentReferences";
	public static final String NEW_REFERENCES = "newReferences";

	private File currentFile;
	private File newFile;
	private List<File> currentReferences;
	private List<File> newReferences;
	private boolean isCurrentUsed;
	private boolean isNewUsed;
	
	public FileMatchLine(File currentFile, File newFile, boolean isCurrentUsed, boolean isNewUsed, List<File> currentReferences, List<File> newReferences) {
		super();
		this.currentFile = currentFile;
		this.newFile = newFile;
		this.isCurrentUsed = isCurrentUsed;
		this.isNewUsed = isNewUsed;
		this.currentReferences = (currentReferences == null)? new ArrayList<File>() : currentReferences;
		this.newReferences = (newReferences == null)? new ArrayList<File>() : newReferences;
	}
	
	public FileMatchLine(File currentFile, File newFile, boolean isCurrentUsed, boolean isNewUsed) {
		super();
		this.currentFile = currentFile;
		this.newFile = newFile;
		this.isCurrentUsed = isCurrentUsed;
		this.isNewUsed = isNewUsed;
		this.currentReferences = new ArrayList<File>();
		this.newReferences = new ArrayList<File>();
		updateReferences();
	}

	public void updateReferences() {
		if (currentFile != null) {
			updateReferences(currentFile, currentReferences);
		}
		if (newFile != null) {
			updateReferences(newFile, newReferences);
		}
	}
	
	private void updateReferences(File file, List<File> references) {
		Map<EObject, Collection<Setting>> referenceMap = EcoreUtil.ExternalCrossReferencer.find(file);
		for (Map.Entry<EObject, Collection<Setting>> entry : referenceMap.entrySet()) {
			if (entry.getKey().eContainer() != null && entry.getKey().eContainer() instanceof File) {
				references.add((File) entry.getKey().eContainer());
			} 
//			else if (entry.getKey().eContainer().eContainer() != null && entry.getKey().eContainer().eContainer() instanceof File) {
//				references.add((File) entry.getKey().eContainer().eContainer());				
//			}
		}
	}

	public File getCurrentFile() {
		return currentFile;
	}

	public void setCurrentFile(File currentFile) {
		firePropertyChange(CURRENT_FILE, this.currentFile, this.currentFile = currentFile);
	}

	public File getNewFile() {
		return newFile;
	}

	public void setNewFile(File newFile) {
		firePropertyChange(NEW_FILE, this.newFile, this.newFile = newFile);
	}

	public boolean getIsCurrentUsed() {
		return isCurrentUsed;
	}

	public void setIsCurrentUsed(boolean useCurrent) {
		firePropertyChange(IS_CURRENT_USED, this.isCurrentUsed, this.isCurrentUsed = useCurrent);
	}

	public boolean getIsNewUsed() {
		return isNewUsed;
	}

	public void setIsNewUsed(boolean useNew) {
		firePropertyChange(IS_NEW_USED, this.isNewUsed, this.isNewUsed = useNew);
	}
	
	public List<File> getCurrentReferences() {
		return currentReferences;
	}
	
	public void setCurrentReferences(List<File> references) {
		if (references == null) references = new ArrayList<File>();
		firePropertyChange(CURRENT_REFERENCES, this.currentReferences, this.currentReferences = references);
	}
	
	public List<File> getNewReferences() {
		return newReferences;
	}
	
	public void setNewReferences(List<File> references) {
		if (references == null) references = new ArrayList<File>();
		firePropertyChange(NEW_REFERENCES, this.newReferences, this.newReferences = references);
	}

	@Override
	public String toString() {
		return "[FileMatch " + IS_CURRENT_USED + ":" + isCurrentUsed + " " + IS_NEW_USED + ":" + isNewUsed + " "
			+ CURRENT_FILE + ":" + currentFile + " " + NEW_FILE + ":" + newFile + "]";
	}

}
