package dk.dtu.imm.red.importing.ui.util;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

public class ResultValidator implements IValidator {

	@Override
	public IStatus validate(Object value) {
		Result<?> result = (Result<?>) value;
		
		if (result == null) {
			return ValidationStatus.ok();
		}
		
		if (result.getStatus() == null) {
			return ValidationStatus.ok();
		}
		
		System.out.println(result.getStatus());
		return result.getStatus();
	}

}
