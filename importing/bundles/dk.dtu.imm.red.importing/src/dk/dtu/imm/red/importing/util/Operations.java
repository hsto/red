package dk.dtu.imm.red.importing.util;

import java.util.Iterator;

import org.eclipse.core.runtime.IStatus;

import dk.dtu.imm.red.importing.util.operation.IteratorOperation;
import dk.dtu.imm.red.importing.util.operation.Operation;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class Operations {

	private Operations() {
	}
	
	public static <T> Operation iterate(Iterable<? extends T> values, Strategy<T, IStatus> strategy) {
		return new IteratorOperation<>(strategy, values);
	}
	
	public static <T> Operation iterate(Iterator<? extends T> iterator, Strategy<T, IStatus> strategy) {
		return new IteratorOperation<>(strategy, iterator);
	}
	
}
