package dk.dtu.imm.red.importing.ui.support.databinding;

import java.util.List;

import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.value.ComputedValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;

public class ObservableListComputedValue extends ComputedValue {
	
	private IObservableValue list;
	private Class<?> elementType;

	public ObservableListComputedValue(IObservableValue list, Class<?> elementType) {
		this.list = list;
		this.elementType = elementType;
	}

	@Override
	protected Object calculate() {
		List<?> list = (List<?>) this.list.getValue();
		
		if (list == null) {
			return null;
		}
		
		return new WritableList(list, elementType);
	}

}
