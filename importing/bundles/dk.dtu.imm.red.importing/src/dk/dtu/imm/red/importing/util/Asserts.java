package dk.dtu.imm.red.importing.util;

public class Asserts {
	
	private Asserts() {
	}
	
	public static void notNull(Object object, String message) {
		if (object == null) {
			throw new IllegalArgumentException(message);
		}
	}
	
}
