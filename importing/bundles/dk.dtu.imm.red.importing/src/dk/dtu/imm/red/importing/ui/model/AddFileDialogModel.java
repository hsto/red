package dk.dtu.imm.red.importing.ui.model;

import java.util.ArrayList;

import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.ui.util.ModelObject;

public class AddFileDialogModel extends ModelObject {

	public static final String CURRENT_PROJECT = "currentProject";
	public static final String NEW_PROJECT = "newProject";
	public static final String FILE_MATCHES = "fileMatches";

	private Project currentProject;
	private Project newProject;
	private ArrayList<FileMatchLine> fileMatches = new ArrayList<>();

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		firePropertyChange(CURRENT_PROJECT, this.currentProject, this.currentProject = currentProject);
	}

	public Project getNewProject() {
		return newProject;
	}

	public void setNewProject(Project newProject) {
		firePropertyChange(NEW_PROJECT, this.newProject, this.newProject = newProject);
	}

	public ArrayList<FileMatchLine> getFileMatches() {
		return fileMatches;
	}

	public void setFileMatches(ArrayList<FileMatchLine> fileMatches) {
		firePropertyChange(FILE_MATCHES, this.fileMatches, this.fileMatches = fileMatches);
	}

}
