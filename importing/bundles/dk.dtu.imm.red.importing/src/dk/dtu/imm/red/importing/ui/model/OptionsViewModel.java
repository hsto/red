package dk.dtu.imm.red.importing.ui.model;

import dk.dtu.imm.red.importing.ui.util.ModelObject;

public class OptionsViewModel extends ModelObject {

	public static final String IS_ADDITIONS_ALLOWED = "isAdditionsAllowed";
	public static final String IS_DELETIONS_ALLOWED = "isDeletionsAllowed";
	public static final String IS_MOVES_ALLOWED = "isMovesAllowed";
	public static final String IS_UPDATES_ALLOWED = "isUpdatesAllowed";

	private boolean isAdditionsAllowed;
	private boolean isDeletionsAllowed;
	private boolean isMovesAllowed;
	private boolean isUpdatesAllowed;

	public boolean getIsAdditionsAllowed() {
		return isAdditionsAllowed;
	}

	public void setIsAdditionsAllowed(boolean isAdditionsAllowed) {
		firePropertyChange(IS_ADDITIONS_ALLOWED, this.isAdditionsAllowed, this.isAdditionsAllowed = isAdditionsAllowed);
	}

	public boolean getIsDeletionsAllowed() {
		return isDeletionsAllowed;
	}

	public void setIsDeletionsAllowed(boolean isDeletionsAllowed) {
		firePropertyChange(IS_DELETIONS_ALLOWED, this.isDeletionsAllowed, this.isDeletionsAllowed = isDeletionsAllowed);
	}

	public boolean getIsMovesAllowed() {
		return isMovesAllowed;
	}

	public void setIsMovesAllowed(boolean isMovesAllowed) {
		firePropertyChange(IS_MOVES_ALLOWED, this.isMovesAllowed, this.isMovesAllowed = isMovesAllowed);
	}

	public boolean getIsUpdatesAllowed() {
		return isUpdatesAllowed;
	}

	public void setIsUpdatesAllowed(boolean isUpdatesAllowed) {
		firePropertyChange(IS_UPDATES_ALLOWED, this.isUpdatesAllowed, this.isUpdatesAllowed = isUpdatesAllowed);
	}

}
