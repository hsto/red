package dk.dtu.imm.red.importing.change;

import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.Element;

public class Move {

	private EList<Element> containingList;
	private Element element;
	
	public Move(EList<Element> containingList, Element element) {
		super();
		this.containingList = containingList;
		this.element = element;
	}

	public Element getElement() {
		return element;
	}

	public EList<Element> getContainingList() {
		return containingList;
	}
	
}
