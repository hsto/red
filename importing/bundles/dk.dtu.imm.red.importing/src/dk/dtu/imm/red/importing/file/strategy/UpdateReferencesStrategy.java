package dk.dtu.imm.red.importing.file.strategy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.util.Elements;
import dk.dtu.imm.red.importing.util.Iterables;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class UpdateReferencesStrategy implements Strategy<File, IStatus> {

	private Project project;
	
	public UpdateReferencesStrategy(Project project) {
		this.project = project;
	}
	
	public IStatus execute(File file) {
		System.out.println("Remove references: " + file);
		Map<EObject, Collection<Setting>> unresolvedProxies = EcoreUtil.ExternalCrossReferencer.find(file);

		for (Entry<EObject, Collection<Setting>> entry : unresolvedProxies.entrySet()) {
			EObject eObject = entry.getKey();
			Collection<Setting> settings = entry.getValue();
			
			URI uri = EcoreUtil.getURI(eObject);
			String uniqueID = uri.fragment();
			System.out.println(uniqueID);
			Iterable<Element> elements = Elements.getElements(project, uniqueID);
			Element element = Iterables.first(elements);
			
			for (Setting setting : settings) {
				try {
					if (element != null) setting.set(element);					
				} catch(Exception e) { // added by Luai for issue #187
					List<Element> list = new ArrayList<>();
					list.add(element);
					setting.set(list);
				}
			}
		}
		
		return Status.OK_STATUS;
	}
	
}
