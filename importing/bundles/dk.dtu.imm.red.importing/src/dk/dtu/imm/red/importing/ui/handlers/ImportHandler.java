package dk.dtu.imm.red.importing.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.element.handlers.OpenElementHandler;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.importing.file.ImportFilesOperation;
import dk.dtu.imm.red.importing.ui.model.AddFileDialogModel;
import dk.dtu.imm.red.importing.ui.model.FileMatcherViewModel;
import dk.dtu.imm.red.importing.ui.model.FilePickerViewModel;
import dk.dtu.imm.red.importing.ui.model.OptionsViewModel;
import dk.dtu.imm.red.importing.ui.views.AddFileDialog;
import dk.dtu.imm.red.importing.util.Elements;

public class ImportHandler extends AbstractHandler implements IHandler {

	@Override
	public boolean isEnabled() {
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();

		return workspace != null && workspace.getCurrentlyOpenedProjects().size() > 0;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveShell(event);
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();

		// only show message dialog when it is not called from "open project"
		MessageDialog md = new MessageDialog(
			shell,
			"Save project",
			null,
//			"You need to save and close your project before adding a file from anther project. Are you sure you want to close your project?",
//			MessageDialog.CONFIRM, new String[] { "Save and close project", "Cancel" }, 0);
			"You need to save your project before adding a file from another project. Exiting the tool will reload your project. Are you sure you want to continue?",
			MessageDialog.CONFIRM, new String[] { "Save project and continue", "Cancel" }, 0);

		int result = md.open();

		if (result == Dialog.OK) {
			
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			page.saveAllEditors(true);
			Project project = workspace.getCurrentlyOpenedProjects().get(0);
			
			for (File file : project.getListofopenedfilesinproject()) {
				file.save();
			}
			
			project.save();
			
			String projectPath = Elements.getProjectPath(project);
			String projectFilePath = project.eResource().getURI().toFileString();

			for (Resource resource : project.eResource().getResourceSet().getResources()) {
				resource.unload();
			}
			
			// Issue #103 Lets the containment panel and editors be in the background
//			workspace.getCurrentlyOpenedProjects().clear();
//			workspace.getCurrentlyOpenedFiles().clear();
			workspace.getContents().clear();
//
//			IWorkbench workbench = PlatformUI.getWorkbench();
//			IWorkbenchPage activePage = workbench.getActiveWorkbenchWindow().getActivePage();
//			activePage.closeEditors(activePage.getEditorReferences(), true);

			ResourceSet resourceSet = new ResourceSetImpl();
			URI projectFileURI = URI.createFileURI(projectFilePath);
			Resource projectResource = resourceSet.getResource(projectFileURI, true);

			project = (Project) projectResource.getContents().get(0);

			AddFileDialogModel addFileDialogModel = new AddFileDialogModel();
			addFileDialogModel.setCurrentProject(project);
			FilePickerViewModel filePickerViewModel = new FilePickerViewModel();
			FileMatcherViewModel fileMatcherViewModel = new FileMatcherViewModel();
			OptionsViewModel optionsViewModel = new OptionsViewModel();
			AddFileDialog addFileDialog = new AddFileDialog(shell, addFileDialogModel, filePickerViewModel, fileMatcherViewModel, optionsViewModel);
			addFileDialog.create();
			int addFileDialogResult = addFileDialog.open();

			// Issue #103 Lets the containment panel and editors be in the background
			workspace.getCurrentlyOpenedProjects().clear();
			workspace.getCurrentlyOpenedFiles().clear();
//			workspace.getContents().clear();

			
			if (addFileDialogResult == Dialog.OK && addFileDialogModel.getNewProject() != null) {
				IWorkbench workbench = PlatformUI.getWorkbench();
				IWorkbenchPage activePage = workbench.getActiveWorkbenchWindow().getActivePage();
				activePage.closeEditors(activePage.getEditorReferences(), true);
				
				try {
					ImportFilesOperation.builder().of(addFileDialogModel).of(optionsViewModel).build().execute();					
				} catch (Exception e) {
					e.printStackTrace(); LogUtil.logError(e);
				}
			}

			for (Resource resource : resourceSet.getResources()) {
				resource.unload();
			}

			OpenElementHandler openElementHandler = new OpenElementHandler();
			openElementHandler.setProjectPath(projectPath);
			openElementHandler.execute(event);
		}

		return null;
	}

}
