package dk.dtu.imm.red.importing.util;

import java.util.Iterator;

import dk.dtu.imm.red.importing.util.iterator.ConcatIterator;
import dk.dtu.imm.red.importing.util.iterator.MappedIterator;
import dk.dtu.imm.red.importing.util.predicate.Predicate;
import dk.dtu.imm.red.importing.util.strategy.CastOrNullStrategy;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class Iterators {

	private Iterators() {
	}

	public static <T> Iterator<T> concat(Iterator<Iterator<T>> iterators) {
		return new ConcatIterator<>(iterators);
	}

	public static <T> Iterator<T> concat(Iterable<Iterator<T>> iterators) {
		return new ConcatIterator<>(iterators);
	}

	public static <TIn, TOut> Iterator<TOut> map(Iterator<? extends TIn> iterator, Strategy<TIn, TOut> strategy) {
		return new MappedIterator<>(iterator, strategy);
	}

	public static <TOut> Iterator<TOut> cast(Iterator<?> iterator, Class<TOut> classT) {
		Strategy<Object, TOut> strategy = new CastOrNullStrategy<>(classT);
		return map(iterator, strategy);
	}

	public static <T> boolean contains(Iterator<T> iterator, Predicate<T> predicate) {
		while (iterator.hasNext()) {
			if (predicate.evaluate(iterator.next())) {
				return true;
			}
		}

		return false;
	}

	public static <T> T first(Iterator<T> iterator, Predicate<T> predicate) {
		while (iterator.hasNext()) {
			T value = iterator.next();
			if (predicate.evaluate(value)) {
				return value;
			}
		}

		return null;
	}

	public static <T> T first(Iterator<T> iterator) {
		if (!iterator.hasNext()) {
			return null;
		}
		
		return iterator.next();
	}
	
	public static <T> int indexOf(Iterator<T> iterator, Predicate<T> predicate) {
		int index = 0;
		while (iterator.hasNext()) {
			if (predicate.evaluate(iterator.next())) {
				return index;
			}
			index += 1;
		}
		return index;
	}

}
