package dk.dtu.imm.red.importing.change.strategy;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.importing.change.Update;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class PerformUpdateStrategy implements Strategy<Update, IStatus> {
	
	@Override
	public IStatus execute(Update input) {
		System.out.println(input);
		input.getEObject().eSet(input.getFeature(), input.getNewValue());
		return Status.OK_STATUS;
	}

}
