package dk.dtu.imm.red.importing.ui.support.databinding;

import java.util.ArrayList;

import org.eclipse.core.databinding.observable.value.ComputedValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;

public class FilesComputedValue extends ComputedValue {
	
	private IObservableValue project;

	public FilesComputedValue(IObservableValue project) {
		this.project = project;
	}

	@Override
	protected Iterable<File> calculate() {
		Project project = (Project) this.project.getValue();
		
		if (project == null) {
			return new ArrayList<>();
		}
		
		return project.getListofopenedfilesinproject();
	}

}
