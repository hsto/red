package dk.dtu.imm.red.importing.ui.model;

import org.eclipse.core.databinding.observable.list.IObservableList;

import dk.dtu.imm.red.importing.ui.util.ModelObject;

public class FileMatcherViewModel extends ModelObject {

	public static final String INPUT = "input";
	
	private IObservableList input;

	public IObservableList getInput() {
		return input;
	}

	public void setInput(IObservableList input) {
		firePropertyChange(INPUT, this.input, this.input = input);
	}
	
}
