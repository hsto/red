package dk.dtu.imm.red.importing.util.predicate;

import dk.dtu.imm.red.core.element.Element;

public class UniqueIdPredicate implements Predicate<Element> {

	private String uniqueId;
	
	public UniqueIdPredicate(String uniqueId) {
		super();
		this.uniqueId = uniqueId;
	}

	@Override
	public boolean evaluate(Element value) {
		return value != null && value.getUniqueID().equals(uniqueId);
	}

}
