package dk.dtu.imm.red.importing.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class Files {

	private Files() {
	}
	
	public static void copy(String inputPath, String outputPath) throws IOException {
		File inputFile = new File(inputPath);

		if (!inputFile.exists()) {
			// Obviously the file we're copying should exist.
			throw new FileNotFoundException(inputPath);
		}

		File outputFile = new File(outputPath);

		if (!outputFile.exists()) {
			outputFile.createNewFile();
		}
		
		// Copy file in an effective way. See the following links:
		// http://stackoverflow.com/a/1707129/113140
		// http://docs.oracle.com/javase/7/docs/api/java/nio/channels/FileChannel.html
		try (FileInputStream inputStream = new FileInputStream(inputFile);
			FileOutputStream outputStream = new FileOutputStream(outputFile);
			FileChannel inputChannel = inputStream.getChannel();
			FileChannel outputChannel = outputStream.getChannel()) {
			inputChannel.transferTo(0, inputChannel.size(), outputChannel);
		}
	}
	
}
