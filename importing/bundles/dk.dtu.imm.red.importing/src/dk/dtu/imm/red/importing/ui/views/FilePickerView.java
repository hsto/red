package dk.dtu.imm.red.importing.ui.views;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.importing.ui.model.FilePickerViewModel;

public class FilePickerView extends Composite {

	private final FilePickerViewModel model;

	public FilePickerView(Composite parent, int style, FilePickerViewModel model) {
		super(parent, style);
		this.model = model;
		createControl();
	}
	
	public FilePickerViewModel getViewModel() {
		return model;
	}

	private void createControl() {
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		
		setLayout(gridLayout);

		final Text filePathText = new Text(this, SWT.BORDER);
		filePathText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Button browseButton = new Button(this, SWT.NONE);
		browseButton.setText("Browse...");
		browseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog fileDialog = new DirectoryDialog(getShell());
				String filePath = fileDialog.open();
				filePathText.setText(filePath);
			}
		});

		// Setup data-binding.
		DataBindingContext dbc = new DataBindingContext();
		IObservableValue filePathTextValue = WidgetProperties.text(SWT.Modify)
				.observe(filePathText);
		IObservableValue modelFilePathValue = BeansObservables.observeValue(
				model, FilePickerViewModel.FILE_PATH);
		dbc.bindValue(filePathTextValue, modelFilePathValue, null, null);
	}

}
