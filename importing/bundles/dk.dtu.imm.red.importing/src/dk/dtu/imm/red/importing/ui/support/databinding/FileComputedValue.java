package dk.dtu.imm.red.importing.ui.support.databinding;

import java.io.IOException;
import java.util.HashMap;

import org.eclipse.core.databinding.observable.value.ComputedValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.importing.ui.model.Texts;
import dk.dtu.imm.red.importing.ui.util.Result;

public class FileComputedValue extends ComputedValue {
	
	private IObservableValue filePath;
	
	public FileComputedValue(IObservableValue filePath) {
		this.filePath = filePath;
	}

	@Override
	protected Result<File> calculate() {
		String filePath = (String) this.filePath.getValue();
		
		if (filePath == null || filePath.isEmpty()) {
			return Result.info(Texts.SELECT_PROJECT);
		}
		
		java.io.File physicalFile = new java.io.File(filePath);
		
		if (!physicalFile.exists()) {
			return Result.error(Texts.FILE_NOT_FOUND);
		}
		
		ResourceSet resourceSet = new ResourceSetImpl();
		URI fileUri = URI.createFileURI(physicalFile.getAbsolutePath());
		Resource resource;

		try {
			resource = resourceSet.getResource(fileUri, true);
			resource.load(new HashMap<Object, Object>());
		} catch (IOException e) {
			return Result.error(Texts.ERROR_OCCURRED);
		} catch (Exception e) {
			return Result.error(Texts.NOT_A_VALID_RED_FILE);
		}
		
		if (resource.getContents().isEmpty()) {
			// File does not contain any objects.
			return Result.error(Texts.NOT_A_VALID_RED_FILE);
		}
		
		EObject rootObject = resource.getContents().get(0);

		if (!(rootObject instanceof File)) {
			// Root object is not a File.
			return Result.error(Texts.NOT_A_VALID_RED_FILE);
		}
		
		File file = (File) rootObject;

		return Result.value(file);
	}

}
