package dk.dtu.imm.red.importing.ui.views;

import java.util.ArrayList;

import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.importing.ui.model.FileMatchLine;
import dk.dtu.imm.red.importing.ui.model.FileMatcherViewModel;
import dk.dtu.imm.red.importing.ui.support.table.CurrentFileColumnLabelProvider;
import dk.dtu.imm.red.importing.ui.support.table.CurrentFileEditingSupport;
import dk.dtu.imm.red.importing.ui.support.table.FileMatchDragListener;
import dk.dtu.imm.red.importing.ui.support.table.FileMatchDropListener;
import dk.dtu.imm.red.importing.ui.support.table.NewFileColumnLabelProvider;
import dk.dtu.imm.red.importing.ui.support.table.NewFileEditingSupport;
import dk.dtu.imm.red.importing.util.Iterables;

public class FileMatcherView extends Composite {

	private final FileMatcherViewModel model;
	
	public FileMatcherView(Composite parent, int style, FileMatcherViewModel model) {
		super(parent, style);
		this.model = model;
		createControl();
	}

	public FileMatcherViewModel getViewModel() {
		return model;
	}

	private void createControl() {
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		
		setLayout(gridLayout);

		final TableViewer tableViewer = new TableViewer(this, SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL);
		tableViewer.setContentProvider(new ObservableListContentProvider());

		Table table = tableViewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setSize(0, 300);

		TableViewerColumn currentFileColumn = new TableViewerColumn(tableViewer, SWT.LEFT);
		currentFileColumn.setLabelProvider(new CurrentFileColumnLabelProvider());
		currentFileColumn.setEditingSupport(new CurrentFileEditingSupport(tableViewer));
		TableColumn currentFileTableColumn = currentFileColumn.getColumn();
		currentFileTableColumn.setWidth(100);
		currentFileTableColumn.setText("Current file");
		currentFileTableColumn.setResizable(true);

		TableViewerColumn newFileColumn = new TableViewerColumn(tableViewer, SWT.LEFT);
		newFileColumn.setLabelProvider(new NewFileColumnLabelProvider());
		newFileColumn.setEditingSupport(new NewFileEditingSupport(tableViewer));
		TableColumn newFileTableColumn = newFileColumn.getColumn();
		newFileTableColumn.setWidth(100);
		newFileTableColumn.setText("New file");
		newFileTableColumn.setResizable(true);
		
		TableViewerColumn referencesColumn = new TableViewerColumn(tableViewer, SWT.LEFT);
		referencesColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
		      public String getText(Object element) {
				StringBuilder sb = new StringBuilder();
				if (element instanceof FileMatchLine) {
					FileMatchLine line = (FileMatchLine) element;
					boolean firstElement = true;
					for (File file : line.getCurrentReferences()) {
						if (file == null) continue;
						if (firstElement) firstElement = false;
						else sb.append(",");
						sb.append(file.getName());
					}
					for (File file : line.getNewReferences()) {
						if (file == null) continue;
						if (firstElement) firstElement = false;
						else sb.append(",");
						sb.append(file.getName());
					}
				}			
				return sb.toString();				
			}
		});
		TableColumn referencesTableColumn = referencesColumn.getColumn();
		referencesTableColumn.setWidth(100);
		referencesTableColumn.setText("References");
		referencesTableColumn.setResizable(true);

		// Setup drag and drop
		int operations = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[] { TextTransfer.getInstance() };
		tableViewer.addDragSupport(operations, transferTypes, new FileMatchDragListener(tableViewer, table));
		tableViewer.addDropSupport(operations, transferTypes, new FileMatchDropListener(tableViewer));

		// Setup data binding
		BeansObservables.observeValue(model, FileMatcherViewModel.INPUT).addChangeListener(new IChangeListener() {
			@Override
			public void handleChange(ChangeEvent event) {
				tableViewer.setInput(model.getInput());
			}

			
		});
		
		Button checkButton = new Button(this, SWT.NONE);
		checkButton.setText("Check");
		checkButton.setToolTipText("Check references");
		checkButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String message = getCheckReferencesMessage();
				if (message.isEmpty()) message = "Corect!";
				else message = "Missing references:\n" + message;
				MessageDialog md = new MessageDialog(getShell(), "Check references", null, message, MessageDialog.CONFIRM, new String[] { "Ok" }, 0);
				md.open();
			}
		});
	}

	public void setInitData(IObservableValue currentFiles) {
		Iterable<File> files = Iterables.cast((Iterable<?>) currentFiles.getValue(), File.class);
		ArrayList<FileMatchLine> lines = new ArrayList<>();
		
		for (File currentFile : files) {
			if (currentFile.getName() != null) {
				lines.add(new FileMatchLine(currentFile, null, true, false));				
			}
		}
		model.setInput(new WritableList(lines, FileMatchLine.class));
	}
	
//	public void setCurrentProject(IObservableValue currentProject) {
//		List<ProjectLine> list = new ArrayList<ProjectLine>();
//		list.add(new ProjectLine((Project)currentProject.getValue(), null));
//		model.setInput(new WritableList(list, ProjectLine.class));
//	}
	
	public String getCheckReferencesMessage() {
		String message = "";
		Iterable<FileMatchLine> lines = Iterables.cast((Iterable<?>) model.getInput(), FileMatchLine.class);
		for (FileMatchLine line : lines) {
			if (!line.getIsCurrentUsed()) continue;
			for (File file : line.getCurrentReferences()) {				
				for (FileMatchLine newLine : lines) {
					if (line.equals(newLine) || line.getCurrentFile() == null) continue;
					if (newLine.getCurrentFile().equals(file) && !newLine.getIsCurrentUsed()) {
						message += line.getCurrentFile().getName() + " points to " + newLine.getCurrentFile().getName() + "\n";
					}
				}				
			}
			if (!line.getIsNewUsed()) continue;
			for (File file : line.getNewReferences()) {
				for (FileMatchLine newLine : lines) {
					if (line.equals(newLine) || newLine.getNewFile() == null) continue;
					if (newLine.getNewFile().equals(file) && !newLine.getIsNewUsed()) {
						message += line.getNewFile().getName() + " points to " + newLine.getCurrentFile().getName() + "\n";
					}
				}
			}
			
		}
		return message;
	}

}
