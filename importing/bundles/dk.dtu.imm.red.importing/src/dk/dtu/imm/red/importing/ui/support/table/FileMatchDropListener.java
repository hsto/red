package dk.dtu.imm.red.importing.ui.support.table;

import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;

import dk.dtu.imm.red.importing.ui.model.FileMatchLine;
import dk.dtu.imm.red.importing.util.Iterables;

public class FileMatchDropListener extends ViewerDropAdapter {

	private final TableViewer viewer;
	private FileMatchLine target;

	public FileMatchDropListener(TableViewer viewer) {
		super(viewer);
		this.viewer = viewer;
	}

	@Override
	public void drop(DropTargetEvent event) {
		Object targetObject = determineTarget(event);
		if (targetObject instanceof FileMatchLine) {
			target = (FileMatchLine) targetObject;
		} else {
			target = null;
		}
		super.drop(event);
	}

	@Override
	public boolean performDrop(Object data) {
		String uniqueID = (String) data;
		if (!uniqueID.equals("empty")) {
			IObservableList input = (IObservableList) viewer.getInput();
			Iterable<FileMatchLine> lines = Iterables.cast((Iterable<?>) input, FileMatchLine.class);
			for (FileMatchLine source : lines) {
				if (source.equals(target)) continue;
				if (source.getNewFile() != null && source.getNewFile().getUniqueID().equals(uniqueID)) {
					if (target == null) {
						FileMatchLine newTarget = new FileMatchLine(null, source.getNewFile(), false, source.getIsNewUsed(), null ,source.getNewReferences());
						input.add(newTarget);
					} else {
						if (target.getNewFile() != null) return false;
						target.setNewFile(source.getNewFile());
						target.setIsNewUsed(true);
						target.setNewReferences(source.getNewReferences());
						viewer.update(target, null);
					}
					
					source.setNewFile(null);
					source.setIsNewUsed(true);
					source.setNewReferences(null);
					
					if (source.getCurrentFile() == null) {
						input.remove(source);
					} else {
						viewer.update(source, null);
					}
					
					return true;
				}
				else if (source.getCurrentFile() != null && source.getCurrentFile().getUniqueID().equals(uniqueID)) { // Added by Luai
					if (target == null) {
						FileMatchLine newTarget = new FileMatchLine(source.getCurrentFile(), null, source.getIsCurrentUsed(), false, source.getCurrentReferences(), null);
						input.add(newTarget);
					} else {
						if (target.getCurrentFile() != null) return false;
						target.setCurrentFile(source.getCurrentFile());
						target.setIsCurrentUsed(true);
						target.setCurrentReferences(source.getCurrentReferences());
						viewer.update(target, null);
					}
					
					source.setCurrentFile(null);
					source.setIsCurrentUsed(true);
					source.setCurrentReferences(null);
					
					if (source.getNewFile() == null) {
						input.remove(source);
					} else {
						viewer.update(source, null);
					}
					
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean validateDrop(Object target, int operation,
			TransferData transferType) {
		return true;
	}

}
