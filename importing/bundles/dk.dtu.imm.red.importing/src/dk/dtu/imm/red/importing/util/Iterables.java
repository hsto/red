package dk.dtu.imm.red.importing.util;

import dk.dtu.imm.red.importing.util.iterable.ConcatIterable;
import dk.dtu.imm.red.importing.util.iterable.MappedIterable;
import dk.dtu.imm.red.importing.util.predicate.Predicate;
import dk.dtu.imm.red.importing.util.strategy.CastOrNullStrategy;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class Iterables {

	private Iterables() {
	}
	
	public static <TIn, TOut> Iterable<TOut> map(Iterable<? extends TIn> iterable, Strategy<TIn, TOut> strategy) {
		return new MappedIterable<>(iterable, strategy);
	}

	public static <T> Iterable<T> cast(Iterable<? extends Object> iterator, Class<T> classT) {
		Strategy<Object, T> strategy = new CastOrNullStrategy<>(classT);
		return map(iterator, strategy);
	}
	
	public static <T> boolean contains(Iterable<T> iterable, Predicate<T> predicate) {
		return Iterators.contains(iterable.iterator(), predicate);
	}
	
	public static <T> T first(Iterable<T> iterable, Predicate<T> predicate) {
		return Iterators.first(iterable.iterator(), predicate);
	}
	
	public static <T> T first(Iterable<T> iterable) {
		return Iterators.first(iterable.iterator());
	}
	
	public static <T> int indexOf(Iterable<T> iterable, Predicate<T> predicate) {
		return Iterators.indexOf(iterable.iterator(), predicate);
	}
	
	public static <T> Iterable<T> concat(Iterable<Iterable<T>> iterables) {
		return new ConcatIterable<>(iterables);
	}
	
}
