package dk.dtu.imm.red.importing.ui.support.table;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import dk.dtu.imm.red.importing.ui.model.FileMatchLine;

public class FileMatchDragListener implements DragSourceListener {

	private final Viewer viewer;
	private int selectedColumn = 1;

	public FileMatchDragListener(Viewer viewer, Table table) {
		this.viewer = viewer;
		addSelectedColumnListener(table);
	}

	@Override
	public void dragStart(DragSourceEvent event) {
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		IStructuredSelection selection = (IStructuredSelection) viewer
				.getSelection();
		FileMatchLine line = (FileMatchLine) selection.getFirstElement();
		
		if (selectedColumn == 1 && line.getNewFile() != null) {
			event.data = line.getNewFile().getUniqueID();
		}
		else if (selectedColumn == 0 && line.getCurrentFile() != null) {
			event.data = line.getCurrentFile().getUniqueID();
		}
		else {
			event.data = "empty";
		}
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
	}
	
	// Added by @Luai
	private void addSelectedColumnListener(final Table table) {
		table.addListener(SWT.MouseDown, new Listener() {			
			@Override
			public void handleEvent(Event event) {
				Point point = new Point(event.x, event.y);
				TableItem item = table.getItem(point);
				if (item == null) return;
				for (int i = 0; i < table.getColumnCount(); i++) {
					Rectangle rect = item.getBounds(i);
					if (rect.contains(point)) {
						selectedColumn = i;
					}
				}				
			}
		});
	}

}
