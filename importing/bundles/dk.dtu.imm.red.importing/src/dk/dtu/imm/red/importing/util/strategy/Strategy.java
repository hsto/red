package dk.dtu.imm.red.importing.util.strategy;

public interface Strategy<TIn, TOut> {

	public TOut execute(TIn input);

}
