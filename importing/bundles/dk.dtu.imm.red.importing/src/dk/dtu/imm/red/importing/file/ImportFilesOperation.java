package dk.dtu.imm.red.importing.file;

import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.importing.change.Deletion;
import dk.dtu.imm.red.importing.change.Move;
import dk.dtu.imm.red.importing.change.Update;
import dk.dtu.imm.red.importing.change.strategy.PerformAdditionStrategy;
import dk.dtu.imm.red.importing.change.strategy.PerformDeletionStrategy;
import dk.dtu.imm.red.importing.change.strategy.PerformMoveStrategy;
import dk.dtu.imm.red.importing.change.strategy.PerformUpdateStrategy;
import dk.dtu.imm.red.importing.file.strategy.AddFileStrategy;
import dk.dtu.imm.red.importing.file.strategy.DeleteFileStrategy;
import dk.dtu.imm.red.importing.file.strategy.MergeStrategy;
import dk.dtu.imm.red.importing.file.strategy.RemoveOverlapsStrategy;
import dk.dtu.imm.red.importing.file.strategy.UpdateReferencesStrategy;
import dk.dtu.imm.red.importing.ui.model.AddFileDialogModel;
import dk.dtu.imm.red.importing.ui.model.FileMatchLine;
import dk.dtu.imm.red.importing.ui.model.OptionsViewModel;
import dk.dtu.imm.red.importing.util.Asserts;
import dk.dtu.imm.red.importing.util.Operations;
import dk.dtu.imm.red.importing.util.Pair;
import dk.dtu.imm.red.importing.util.operation.Operation;
import dk.dtu.imm.red.importing.util.strategy.Strategy;

public class ImportFilesOperation implements Operation {

	private final Project currentProject;
	private final Project newProject;
	private final Iterable<File> filesToAdd;
	private final Iterable<File> filesToDelete;
	private final Iterable<Pair<File>> filesToMerge;
	private final boolean additionsAllowed;
	private final boolean deletionsAllowed;
	private final boolean movesAllowed;
	private final boolean updatesAllowed;

	public static class Builder {
		private Project currentProject;
		private Project newProject;
		private Iterable<File> filesToAdd;
		private Iterable<File> filesToDelete;
		private Iterable<Pair<File>> filesToMerge;
		private boolean additionsAllowed;
		private boolean deletionsAllowed;
		private boolean movesAllowed;
		private boolean updatesAllowed;

		private Builder() {
		}

		public Builder currentProject(Project value) {
			currentProject = value;
			return this;
		}

		public Builder newProject(Project value) {
			newProject = value;
			return this;
		}

		public Builder filesToAdd(Iterable<File> value) {
			filesToAdd = value;
			return this;
		}

		public Builder filesToDelete(Iterable<File> value) {
			filesToDelete = value;
			return this;
		}

		public Builder filesToMerge(Iterable<Pair<File>> value) {
			filesToMerge = value;
			return this;
		}

		public Builder additionsAllowed(boolean value) {
			additionsAllowed = value;
			return this;
		}

		public Builder deletionsAllowed(boolean value) {
			deletionsAllowed = value;
			return this;
		}

		public Builder movesAllowed(boolean value) {
			movesAllowed = value;
			return this;
		}

		public Builder updatesAllowed(boolean value) {
			updatesAllowed = value;
			return this;
		}

		public Builder of(AddFileDialogModel model) {
			ArrayList<File> filesToDelete = new ArrayList<>();
			ArrayList<File> filesToAdd = new ArrayList<>();
			ArrayList<Pair<File>> filesToMerge = new ArrayList<>();

			for (FileMatchLine fileMatch : model.getFileMatches()) {
				File currentFile = fileMatch.getCurrentFile();
				File newFile = fileMatch.getNewFile();
				boolean isCurrentUsed = fileMatch.getIsCurrentUsed();
				boolean isNewUsed = fileMatch.getIsNewUsed();

				if (isNewUsed && newFile != null) {
					if (isCurrentUsed && currentFile != null) {
						filesToMerge.add(new Pair<>(currentFile, newFile));
					} else {
						filesToAdd.add(newFile);
					}
				}

				if (!isCurrentUsed && currentFile != null) {
					filesToDelete.add(currentFile);
				}

			}

			return currentProject(model.getCurrentProject())
				.newProject(model.getNewProject())
				.filesToAdd(filesToAdd)
				.filesToDelete(filesToDelete)
				.filesToMerge(filesToMerge);
		}

		public Builder of(OptionsViewModel model) {
			return additionsAllowed(model.getIsAdditionsAllowed())
				.deletionsAllowed(model.getIsDeletionsAllowed())
				.movesAllowed(model.getIsMovesAllowed())
				.updatesAllowed(model.getIsUpdatesAllowed());
		}

		public ImportFilesOperation build() {
			Asserts.notNull(currentProject, "currentProject is null.");
			Asserts.notNull(newProject, "newProject is null.");
			Asserts.notNull(filesToAdd, "filesToAdd is null.");
			Asserts.notNull(filesToDelete, "filesToDelete is null.");
			Asserts.notNull(filesToMerge, "filesToMerge is null.");
			return new ImportFilesOperation(this);
		}
	}

	private ImportFilesOperation(Builder builder) {
		this.currentProject = builder.currentProject;
		this.newProject = builder.newProject;
		this.filesToAdd = builder.filesToAdd;
		this.filesToDelete = builder.filesToDelete;
		this.filesToMerge = builder.filesToMerge;
		this.additionsAllowed = builder.additionsAllowed;
		this.deletionsAllowed = builder.deletionsAllowed;
		this.movesAllowed = builder.movesAllowed;
		this.updatesAllowed = builder.updatesAllowed;
	}

	public static Builder builder() {
		return new Builder();
	}

	@Override
	public IStatus execute() {
		EList<File> filesInProject = currentProject.getListofopenedfilesinproject();

		PerformAdditionStrategy additionStrategy = new PerformAdditionStrategy();
		Strategy<Deletion, IStatus> deletionStrategy = new PerformDeletionStrategy();
		Strategy<Move, IStatus> moveStrategy = new PerformMoveStrategy();
		Strategy<Update, IStatus> updateStrategy = new PerformUpdateStrategy();
		MergeStrategy mergeStrategy = new MergeStrategy(currentProject, newProject,
			additionsAllowed, deletionsAllowed, movesAllowed, updatesAllowed,
			deletionStrategy, additionStrategy, moveStrategy, updateStrategy);

		DeleteFileStrategy deleteFileStrategy = new DeleteFileStrategy(currentProject);
		RemoveOverlapsStrategy removeOverlapsStrategy = new RemoveOverlapsStrategy(currentProject, movesAllowed,
			mergeStrategy);
		AddFileStrategy addFileStrategy = new AddFileStrategy(currentProject, removeOverlapsStrategy);
		UpdateReferencesStrategy updateReferencesStrategy = new UpdateReferencesStrategy(currentProject);

		Operations.iterate(filesToDelete, deleteFileStrategy).execute();
		Operations.iterate(filesToAdd, addFileStrategy).execute();
		Operations.iterate(filesToMerge, mergeStrategy).execute();
		Operations.iterate(filesInProject, updateReferencesStrategy).execute();

		for (File file : filesInProject) {
			file.save();
		}

		currentProject.save();

		return Status.OK_STATUS;
	}

}
