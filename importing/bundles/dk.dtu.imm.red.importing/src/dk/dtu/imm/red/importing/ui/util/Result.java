package dk.dtu.imm.red.importing.ui.util;

import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

public class Result<T> {
	
	public static final String STATUS = "status";
	public static final String VALUE = "value";

	private IStatus status;
	private T value;

	private Result(IStatus status, T value) {
		super();
		this.status = status;
		this.value = value;
	}
	
	public static <T> Result<T> value(T value) {
		return new Result<T>(null, value);
	}
	
	public static <T> Result<T> cancel(String message, T value) {
		return new Result<T>(ValidationStatus.cancel(message), value);
	}
	
	public static <T> Result<T> cancel(String message) {
		return cancel(message, null);
	}
	
	public static <T> Result<T> error(String message, T value) {
		return new Result<T>(ValidationStatus.error(message), value);
	}
	
	public static <T> Result<T> error(String message) {
		return error(message, null);
	}
	
	public static <T> Result<T> info(String message, T value) {
		return new Result<T>(ValidationStatus.info(message), value);
	}
	
	public static <T> Result<T> info(String message) {
		return info(message, null);
	}
	
	public static <T> Result<T> warning(String message, T value) {
		return new Result<T>(ValidationStatus.warning(message), value);
	}
	
	public static <T> Result<T> warning(String message) {
		return warning(message, null);
	}

	public IStatus getStatus() {
		return status;
	}

	public T getValue() {
		return value;
	}

}
