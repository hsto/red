package dk.dtu.imm.red.importing.ui.util;

import org.eclipse.core.databinding.UpdateValueStrategy;

public class ResultUpdateValueStrategy extends UpdateValueStrategy {

	public ResultUpdateValueStrategy() {
		super();
		setAfterGetValidator(new ResultValidator());
		setConverter(new ResultConverter());
	}
	
}
