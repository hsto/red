package dk.dtu.imm.red.importing.change;

import org.eclipse.emf.ecore.EObject;

public class Deletion {

	private EObject currentEObject;

	public Deletion(EObject currentEObject) {
		super();
		this.currentEObject = currentEObject;
	}

	public EObject getCurrentEObject() {
		return currentEObject;
	}
	
}
