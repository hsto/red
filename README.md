# Welcome

Welcome to our wiki!

See [Build](Build) page for detailed instructions on how to build RED.

See [IDE Setup](https://bitbucket.org/hsto/red/downloads/red-SDK_setup.mp4)
Tips: It seems that Tycho configurator has been removed in catalog v1.4. Therefore you may see that all the plugins cannot be found when you import the main red pom file. One workaround I use is to add this line in the eclipse.ini file to revert the catalog back to v1.3:

-Dm2e.discovery.url=http://download.eclipse.org/technology/m2e/discovery/directory-1.3.xml

See [Tips on using your eclipse](https://bitbucket.org/hsto/red/downloads/EclipseTipsForRED.docx)

See [Adding new specification elements](https://bitbucket.org/hsto/red/downloads/Add%20New%20Specification%20Elements.docx)

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact