/*
 * 
 */
package dk.dtu.imm.red.csv.ui;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.csv.presenter.CSVPresenter;
import dk.dtu.imm.red.csv.presenter.FileCSVPresenter;
import dk.dtu.imm.red.csv.presenter.ImportConflict;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.requirement.RequirementFactory;


// TODO: Auto-generated Javadoc
/**
 * The Class CSVTest.
 * Tests creating a list of Requirements from a CSV file created in Excel,
 * and exporting a CSV from a list of Requirements that can be imported by Excel
 */
public class CSVTest {

	/** The test string. */
	//	private String testString = 
	//			"Name;ID;Type;Requirement;AbstractionLevel;SourceText;RationaleText\n" +
	//			"Shall do something;ReqID1;Feature;Better explained text;Process;Some source text;Some rationale text\n" +
	//			"Shall do another ting;ReqID2;Quality;;Domain;Some other source text;Some other rationale text\n";

	private String testString = 
			"ID;Name;Type;Requirement;AbstractionLevel;SourceText;RationaleText\n" +
					"ReqID1;Shall do something;Feature;Better explained text;Process;Some source text;Some rationale text\n" +
					"ReqID2;Shall do another ting;Quality;;Domain;Some other source text;Some other rationale text\n";


	/**
	 * Test that target can create object list from CSV.
	 */
	@Test
	public void canCreateObjectListFromCSV() {

		EClass ecl = getRequirementEClass(); 

		CSVPresenter<Requirement> presenter = new CSVPresenter<Requirement>()
												  .setEClass(ecl);

		//ACT
		List<Requirement> requirementList =  presenter.getListFromCSV(new StringReader(testString));

		//ASSERT
		assertProperties(requirementList);  
	}  

	/**
	 * Test that target can create CSV from object list.
	 * @throws IOException 
	 */
	@Ignore("Requires maintenance") @Test
	public void canCreateCSVFromObjectList() {

		EClass ecl = getRequirementEClass(); 

		CSVPresenter<Requirement> presenter = new CSVPresenter<Requirement>()
				.setEClass(ecl);

		List<Requirement> reqList = createRequirementList();

		StringWriter writer = new StringWriter();

		//ACT
		presenter.writeCSVFromList(writer, reqList);  

		//ASSERT 
		String writerString = writer.toString();
		List<Requirement> requirementList =  presenter.getListFromCSV(new StringReader(writerString)); 
		assertProperties(requirementList);  
	}

	private List<Requirement> createRequirementList() {
		Requirement rq1 = RequirementFactory.eINSTANCE.createRequirement();
		rq1.setName("Shall do something");
		rq1.setId("ReqID1");	
		rq1.setAbstractionLevel("Process");

		Requirement rq2 = RequirementFactory.eINSTANCE.createRequirement();
		rq2.setName("Shall do another ting");
		rq2.setId("ReqID2");	
		rq2.setAbstractionLevel("Domain");

		List<Requirement> reqList = new ArrayList<Requirement>();
		reqList.add(rq1);
		reqList.add(rq2);
		return reqList;
	}

	private void assertProperties(List<Requirement> requirementList) {

		assertEquals(2, requirementList.size());
		//Assert First Line
		Requirement currentLine = requirementList.get(0);
		assertEquals("Shall do something", currentLine.getName());
		assertEquals("ReqID1", currentLine.getId());	
		assertEquals("Process", currentLine.getAbstractionLevel());

		//Assert First Line
		currentLine = requirementList.get(1);
		assertEquals("Shall do another ting", currentLine.getName());
		assertEquals("ReqID2", currentLine.getId());	
		assertEquals("Domain", currentLine.getAbstractionLevel());
	}

	@Ignore("Requires maintenance") @Test
	public void canMergeList() {
		
		//ARRANGE
		List<Requirement> reqList = createRequirementList();
		
		//Add new list with a Requirement with the same name. Properties should be merged.
		Requirement rq1 = RequirementFactory.eINSTANCE.createRequirement();
		rq1.setName("Shall do something");
		rq1.setId("req3");
		
		Requirement rq2 = RequirementFactory.eINSTANCE.createRequirement();
		rq2.setName("Shall do fourth thing");
		rq2.setId("req4");
		
		List<Requirement> importedReqList = new ArrayList<Requirement>();
		importedReqList.add(rq1);
		importedReqList.add(rq2);
		
		EClass ecl = getRequirementEClass(); 
		
		FileCSVPresenter<Requirement> importPresenter = new FileCSVPresenter<Requirement>(reqList, ecl);
		importPresenter.setTargetList(importedReqList);
		
		//ACT
		importPresenter.mergeIntoSourceList();
		
		//ASSERT
		List<ImportConflict<Requirement>> conflicts = importPresenter.getImportConflicts();
		assertEquals(1, conflicts.size()); 
		
		importPresenter.resolveMergeConflictUseTarget(conflicts.get(0));
		
		assertEquals(0, conflicts.size());
		
		assertEquals(3, importPresenter.getSourceList().size()); 
		
		Requirement req4 = importPresenter.getSourceList().get(0);  
		assertEquals("Shall do something", req4.getName());
		assertEquals("req3", req4.getId());
		
		Requirement req3 = importPresenter.getSourceList().get(2);  
		assertEquals("Shall do fourth thing", req3.getName());
		assertEquals("req4", req3.getId());
	}

	private EClass getRequirementEClass() {
		ExtendedMetaData modelMetaData = new BasicExtendedMetaData(EPackage.Registry.INSTANCE); 
		EClass ecl =  (EClass) modelMetaData.getType("dk.dtu.imm.red.specificationelements.requirement", "Requirement");
		return ecl;
	}

}
