/*
 * @Author Johan Paaske Nielsen
 */
package dk.dtu.imm.red.csv.presenter;

import java.util.List; 
 
public class ImportConflict<T> {
	
	/** The source list. */
	private List<T> sourceList;
	
	/** The target list. */
	private List<T> targetList;
	
	/** The target item. */
	private T targetItem;
	
	/** The source item. */
	private T sourceItem;

	/**
	 * Instantiates a new import conflict.
	 *
	 * @param sourceList the source list
	 * @param targetList the target list
	 * @param sourceItem the source item
	 * @param targetItem the target item
	 */
	public ImportConflict(List<T> sourceList, List<T> targetList, T sourceItem, T targetItem) {
		this.sourceList = sourceList;
		this.targetList = targetList;
		this.sourceItem = sourceItem;
		this.targetItem = targetItem;
	}

	/**
	 * Gets the source list.
	 *
	 * @return the source list
	 */
	public List<T> getSourceList() {
		return sourceList;
	}

	/**
	 * Gets the target list.
	 *
	 * @return the target list
	 */
	public List<T> getTargetList() {
		return targetList;
	}

	/**
	 * Gets the target item.
	 *
	 * @return the target item
	 */
	public T getTargetItem() {
		return targetItem;
	}

	/**
	 * Gets the source item.
	 *
	 * @return the source item
	 */
	public T getSourceItem() {
		return sourceItem;
	}

}
