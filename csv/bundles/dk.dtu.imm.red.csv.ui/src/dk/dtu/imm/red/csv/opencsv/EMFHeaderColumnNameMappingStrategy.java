package dk.dtu.imm.red.csv.opencsv; 
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EPackage;

import au.com.bytecode.opencsv.bean.HeaderColumnNameMappingStrategy;

public class EMFHeaderColumnNameMappingStrategy<T> extends HeaderColumnNameMappingStrategy<T> {  
	
	protected EClass eClass;
	protected PropertyDescriptor[] propertyDesc;

	public EMFHeaderColumnNameMappingStrategy() {
		super();
	}
	
	public Collection<PropertyDescriptor> getDescriptorValues() {
		return descriptorMap.values();
	}
	
	public EMFHeaderColumnNameMappingStrategy(EClass eClass) {
		this.eClass = eClass; 
	}
	
	@SuppressWarnings("unchecked")
	@Override 
	public T createBean() throws InstantiationException, IllegalAccessException {
		
		if(eClass == null) {
			return super.createBean();
		}
		else {
			EFactory f = 			
					(EFactory)EPackage.Registry.INSTANCE.getEFactory(eClass.getEPackage().getNsURI()); 
			return (T)f.create(eClass);
		} 
	}  
	
	@Override
	protected PropertyDescriptor findDescriptor(String name) throws IntrospectionException { 
		if (null == descriptorMap) descriptorMap = loadDescriptorMap(getType()); //lazy load descriptors
        return descriptorMap.get(name.toUpperCase().trim()); 
    } 
	
	@Override
	protected Map<String, PropertyDescriptor> loadDescriptorMap(Class<T> cls) throws IntrospectionException {
        Map<String, PropertyDescriptor> map = new HashMap<String, PropertyDescriptor>();

        PropertyDescriptor[] descriptors;
        descriptors = loadDescriptors(getType());
        for (PropertyDescriptor descriptor : descriptors) {
            map.put(descriptor.getName().toUpperCase().trim(), descriptor);
        }

        return map;
    }
	
	 private PropertyDescriptor[] loadDescriptors(Class<T> cls) throws IntrospectionException {
	        BeanInfo beanInfo = BeanHelper.getBeanInfo(cls);//Introspector.getBeanInfo(cls);  
	        return beanInfo.getPropertyDescriptors();
	 }
}
