/*
 * @author Johan Paaske Nielsen
 */ 
package dk.dtu.imm.red.csv.presenter; 
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List; 
import java.util.Map;

import org.eclipse.emf.ecore.EClass; 

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;

public class FileCSVPresenter<T>  {

	private List<ImportConflict<T>> importConflicts = new ArrayList<ImportConflict<T>>();
	private List<T> sourceList = new ArrayList<T>();
	private List<T> targetList = new ArrayList<T>();
	private CSVPresenter<T> csvPresenter;
	
	/**
	 * Instantiates a new file CSV presenter.
	 *
	 * @param sourceItems the source items
	 * @param eClass the e class that is used for method reflection and initiating new objects.
	 */
	public FileCSVPresenter(List<T> sourceItems, EClass eClass) { 
		this.sourceList = sourceItems;
		csvPresenter = new CSVPresenter<T>()
					       .setEClass(eClass);
	} 
	
	/**
	 * Reads CSV file from the given path, and create a List from the content.
	 *
	 * @param path the path
	 * @return the list from CSV file
	 * @throws FileNotFoundException the file not found exception
	 */
	public List<T> getListFromCSVFile(String path) throws FileNotFoundException {
		FileReader f = new FileReader(path);
		List<T> parsedTargetList = csvPresenter.getListFromCSV(f);
		setTargetList(parsedTargetList);
		return targetList;
	} 
	
	/**
	 * Write the source list to the specified path as a CSV file
	 *
	 * @param path the path
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void writeCSVFromList(String path) throws IOException {
		FileWriter f = new FileWriter(path);
		csvPresenter.writeCSVFromList(f, sourceList);  
	} 

	/**
	 * Resolve merge conflict use target.
	 *
	 * @param importConflict the import conflict
	 */
	@SuppressWarnings("unchecked")
	public void resolveMergeConflictUseTarget(ImportConflict<T> importConflict) {
		if( !(importConflicts.contains(importConflict))) {
			throw new IllegalArgumentException("Merge Conflict could not be found, in the Merge Conflict list");
		} 

		int indexOfExisting =  sourceList.indexOf(importConflict.getSourceItem());
		
		//Overwrite all properties in Source using target
		if(importConflict.getSourceItem() instanceof ElementImpl) {
			
			//TypeCast to Element
			ElementImpl sourceElement = ((ElementImpl) importConflict.getSourceItem());
			ElementImpl targetElement = ((ElementImpl) importConflict.getTargetItem());
			int numOfCols = sourceElement.getColumnRepresentation().length;
		 
			for(int i = 0; i < numOfCols; i++) {
				//Use the Column to read the value from the target, and set it in Source
				
				//Get Column Refs
				ElementColumnDefinition sourceColumn = sourceElement.getColumnRepresentation()[i];
				ElementColumnDefinition targetColumn = targetElement.getColumnRepresentation()[i];
				
				//Get CellHandler Refs
				CellHandler<ElementImpl> tCellHandler =   (CellHandler<ElementImpl>) targetColumn.getCellHandler(); 
				CellHandler<ElementImpl> sCellHandler1 =   (CellHandler<ElementImpl>) sourceColumn.getCellHandler();
				
				//Read Target Value
				Object targetValue = tCellHandler.getAttributeValue((ElementImpl)targetElement);
				
				//Set Source value to target value
				sCellHandler1.setAttributeValue(
						sourceElement, targetValue);
			} 
		} 
		else {
			//Standard. Remove old source element and insert target
			sourceList.add(indexOfExisting, importConflict.getTargetItem());
			sourceList.remove(importConflict.getSourceItem()); 
		} 
		
		//Resolved
		importConflicts.remove(importConflict);
	}

	/**
	 * Resolve merge conflict use source.
	 *
	 * @param importConflict the import conflict
	 */
	public void resolveMergeConflictUseSource(ImportConflict<T> importConflict) {
		if( !(importConflicts.contains(importConflict))) {
			throw new IllegalArgumentException("Merge Conflict could not be found, in the Merge Conflict list");
		}

		importConflicts.remove(importConflict);
	}

	/**
	 * Merge the target list into the source list.
	 * If the target list contains an item with the same unique identifier,
	 * an ImportConflict object will be created, an added to the ImportConflict list.
	 */
	public void mergeIntoSourceList() { 
		
		importConflicts.clear();
		
		//Create a HashMap with Name, Reference for quick comparison 
		Map<String, T> comparisonSet = new HashMap<String, T>();

		for(T item : sourceList) {
			if(item instanceof Element) {
				comparisonSet.put(((Element) item).getName(), item);
			}
		}  

		//Run through all the items in the target list, and see if the name is in the sourceMap
		//If so, Create a Merge Conflict
		for(T item : targetList) {
			if(item instanceof Element) {
				String key = ((Element)item).getName();
				if( comparisonSet.containsKey(key)) {
					ImportConflict<T> con = new ImportConflict<T>(sourceList, targetList, comparisonSet.get(key), item);
					importConflicts.add(con);
					continue;
				} 
				
				if(key == null || key == "") {
					((Element)item).setName("Unnamed Imported Element");
				}
				
			} 
			sourceList.add(item); 
		} 
	}  

	/**
	 * Gets the source list.
	 *
	 * @return the source list
	 */
	public List<T> getSourceList() {
		return sourceList;
	}

	/**
	 * Gets the target list.
	 *
	 * @return the target list
	 */
	public List<T> getTargetList() {
		return targetList;
	}
	
	/**
	 * Sets the target list.
	 *
	 * @param targetList the new target list
	 */
	public void setTargetList(List<T> targetList) { 
		this.targetList = targetList;
	} 
	
	/**
	 * Gets the import conflicts.
	 *
	 * @return the import conflicts
	 */
	public List<ImportConflict<T>> getImportConflicts() {
		return importConflicts;
	}  
}
