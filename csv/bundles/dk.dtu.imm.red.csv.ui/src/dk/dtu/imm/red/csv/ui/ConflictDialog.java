/*
 * @author Johan Paaske Nielsen
 * 
 * A Dialog for resolving import conflicts, when importing CSV files.
 */

package dk.dtu.imm.red.csv.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell; 
 
/**
 * The Class ConflictDialog.
 */
public class ConflictDialog extends Dialog
{
	
	/** The message. */
	private String message = "";
 
	/** The btn left. */
	private Button btnLeft; 
	
	/** The btn right. */
	private Button btnRight;
	
	/** The return code. */
	private int returnCode = 0;

	/**
	 * Instantiates a new conflict dialog.
	 *
	 * @param parent the parent
	 */
	public ConflictDialog(Shell parent)
	{
		// Pass the default styles here
		this(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL); 
	}

	/**
	 * Instantiates a new conflict dialog with style parameter
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public ConflictDialog(Shell parent, int style)
	{
		// Let users override the default styles
		super(parent, style); 
	}

	

	/**
	 * Open the Dialog, and wait for the user, to either press the left or right button
	 * Left button returns 1, right button returns 2
	 *
	 * @return the int
	 */
	public int open()
	{
		Shell shell = new Shell(getParent(), getStyle());
		shell.setText(getText());
		createContents(shell);
		shell.pack();
		shell.open();
		Display display = getParent().getDisplay(); 
		
		createContents(shell);
		
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}

		return returnCode;
	}

	/**
	 * Creates the dialog's contents.
	 *
	 * @param shell            the dialog window
	 */
	 private void createContents(final Shell shell)
	{
		 
		//GridLayout gl = new GridLayout()
		 
		shell.setLayout(new GridLayout(2, true));

		// Show the message
		Label label = new Label(shell, SWT.NONE);
		label.setText(message);
		GridData data = new GridData();
		data.horizontalSpan = 2;
		data.horizontalAlignment = SWT.CENTER;
		label.setLayoutData(data); 
		btnLeft = new Button(shell, SWT.PUSH);
		btnLeft.setText("Keep existing Specification Element");
		data = new GridData(SWT.FILL, SWT.END, true, true);
		btnLeft.setLayoutData(data);
		btnLeft.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				returnCode = 1;
				shell.close();
			}
		});

		btnRight = new Button(shell, SWT.PUSH);
		btnRight.setText("Overwrite existing Specification Element");
		data = new GridData(SWT.FILL, SWT.END, true, true);
		btnRight.setLayoutData(data);
		btnRight.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				returnCode = 2;
				shell.close();
			}
		}); 

		shell.setDefaultButton(btnLeft);
	} 

	 /**
 	 * Gets the btn left
 	 * Returncode is 1.
 	 *
 	 * @return the btn left
 	 */
 	public Button getBtnLeft() {
		 return btnLeft;
	 }

	 /**
 	 * Gets the btn right.
 	 * Returncode is 2.
 	 * @return the btn right
 	 */
 	public Button getBtnRight() {
		 return btnRight;
	 }
 	
 	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}
}