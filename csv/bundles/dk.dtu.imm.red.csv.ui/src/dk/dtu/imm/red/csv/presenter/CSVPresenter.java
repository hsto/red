/*
 * A generic CSV reader and writer.
 * A wrapper around OpenCSV http://opencsv.sourceforge.net/
 * Can read an CSV file, and map to a RED model object. It selects a EMFFactory by EClass type, to create new objects.
 * Uses some helper classes for reflecting the objects correctly. 
 * BeanToCsv was not included in the official distribution, so it was added. 
 * Inherited methods was not being reflected, so BeanHelper was added 
 * 
 *  @author Johan Paaske Nielsen
 */

package dk.dtu.imm.red.csv.presenter; 
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.Reader; 
import java.io.Writer;
import java.util.List;     

import org.eclipse.emf.ecore.EClass; 

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.csv.opencsv.BeanToCsv;
import dk.dtu.imm.red.csv.opencsv.EMFHeaderColumnNameMappingStrategy;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter; 
import au.com.bytecode.opencsv.bean.CsvToBean;

public class CSVPresenter<T> {

	private CsvToBean<T> cvsToBean;
	private BeanToCsv<T> beanToCvs;
	private char seperator = ';';
	private EClass eClass;
	private Class<T> javaClass;  


	public CSVPresenter() { 
		cvsToBean = new CsvToBean<T>();  
		beanToCvs= new BeanToCsv<T>();  
	}

	public CSVPresenter<T> setSeperator(char seperator) { 
		this.seperator = seperator;
		return this;
	}

	/**
	 * Sets the EClass.
	 * The EClass is needed to determine which attributes to write.
	 * Also for creating new object instances when reading CSV file.
	 *
	 * @param eClass the EClass
	 * @return the CSV presenter
	 */
	@SuppressWarnings("unchecked")
	public CSVPresenter<T> setEClass(EClass eClass) {
		this.eClass = eClass;
		javaClass = (Class<T>) eClass.getInstanceClass();  
		return this;
	} 

	/**
	 * Gets the list from CSV.
	 *
	 * @param reader the reader
	 * @return the list from CSV
	 */
	public List<T> getListFromCSV(Reader reader) { 
		// the fields to bind do in your POJO  
		List<T> list = null;
		try {
			CSVReader csvReader = null;  
			try {
				csvReader = new CSVReader(reader,seperator); 
				EMFHeaderColumnNameMappingStrategy<T> strat = new EMFHeaderColumnNameMappingStrategy<T>(eClass); 
				strat.setType(javaClass); 	
				list = cvsToBean.parse(strat, csvReader);
			}
			catch (Exception e) { 
				e.printStackTrace(); LogUtil.logError(e);
			} finally {
				csvReader.close();
			}
		}
		catch (IOException e) { 
			e.printStackTrace(); LogUtil.logError(e);
		}

		if(list == null) {
			throw new IllegalArgumentException("Could not map CSV file to object");
		}

		return list; 
	}

	/**
	 * Write CSV from list content.
	 *
	 * @param writer The writer
	 * @param list The list
	 */
	public void writeCSVFromList(Writer writer, List<T> list) {   
		try {
			CSVWriter csvWriter = null;  
			try {
				csvWriter = new CSVWriter(writer, seperator, CSVWriter.DEFAULT_QUOTE_CHARACTER ); 
				EMFHeaderColumnNameMappingStrategy<T> strat = new EMFHeaderColumnNameMappingStrategy<T>(eClass) {

					@Override
					public PropertyDescriptor findDescriptor(int index) throws IntrospectionException { 

						if (null == descriptorMap) descriptorMap = loadDescriptorMap(getType()); //lazy load descriptors
						if(propertyDesc == null) propertyDesc =  descriptorMap.values().toArray(
								new PropertyDescriptor[descriptorMap.values().size()]);

						if(index <= propertyDesc.length - 1) {
							return propertyDesc[index];
						}
						else {
							return null;
						}  
					} 
				}; 
				strat.setType(javaClass); 	
				beanToCvs.write(strat, csvWriter, list); 
			}
			catch (Exception e) { 
				e.printStackTrace(); LogUtil.logError(e);
			} finally {
				csvWriter.close();
			}
		}
		catch (IOException e) { 
			e.printStackTrace(); LogUtil.logError(e);
		}
	}  
}
