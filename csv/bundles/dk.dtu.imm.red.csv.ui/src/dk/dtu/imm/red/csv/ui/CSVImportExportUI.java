/*
 * @author Johan Paaske Nielsen
 * 
 * Contains UI dialogs for importing and exporting EMF Objects
 */

package dk.dtu.imm.red.csv.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.eclipse.emf.ecore.EClass; 
import org.eclipse.swt.SWT; 
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.csv.presenter.ImportConflict;
import dk.dtu.imm.red.csv.presenter.FileCSVPresenter;

public class CSVImportExportUI<T> {

	private static final int RESOLVE_USING_SOURCE = 1;
	private static final int RESOLVE_USING_TARGET = 2;
	private FileCSVPresenter<T> presenter;
	private Shell parent; 

	/**
	 * Instantiates a new CSV class with methods for importing and exporting CSV files.
	 *
	 * @param parent the parent
	 * @param existingList The list to import CSV content into, or the list to export as CSV file.
	 * @param eClass The EMF type in the existingList. This is needed for Object creation through EMF Factory classes.
	 */
	public CSVImportExportUI(Shell parent, List<T> existingList, EClass eClass) {
		this.parent = parent;
		presenter = new FileCSVPresenter<T>(existingList,eClass); 
	}

	/**
	 * Import CSV file by using a FileDialog.
	 * Uses CSVPresenter for CSV -> Object transformation
	 * @return true, if successful
	 */
	public boolean importCSVFile() {
		
		//Setup FileDialog
		FileDialog fd = new FileDialog(parent, SWT.OPEN);
		fd.setText("Open"); 
		String homefolder = System.getProperty("user.home"); 
		fd.setFilterPath(homefolder);
		String[] filterExt = { "*.csv"};
		fd.setFilterExtensions(filterExt);
		String selected = fd.open();

		if(selected == null) return false; //If Cancel in FileDialog
		
		try {
			presenter.getListFromCSVFile(selected);
		} catch (FileNotFoundException e) { 
			messageBox("File not found",
					"Could not find selected file.", 
					 SWT.ERROR | SWT.OK); 
		}
		catch(Exception e) {
			
			messageBox("Unknown Error!",
					"An unknown error happened during import of the file. \n\n Error Message: \n\n " + e.getMessage(),
					 SWT.ERROR | SWT.OK); 
		}
		
		//Merge the imported target list into the source list
		presenter.mergeIntoSourceList();

		int modifiedCounter = 0; //For tracking number of modifications 
		
		//Use a While loop, because the Collection might get modified, when resolving merge conflicts
		//thus a For Loop will cause an Exception when using an Iterator.
		while(presenter.getImportConflicts().size() > 0) { 
			ImportConflict<T> con = presenter.getImportConflicts().get(0); //Reference to the next conflict
			
			//Setup conflict dialog
			ConflictDialog cDialog = 
					new ConflictDialog(Display.getDefault().getActiveShell());
			
			cDialog.setText("Resolve Conflict");
			cDialog.setMessage(
					"A Specification Element with the Name: " + ((Element)con.getSourceItem()).getName() +
					" is exists. \n"
					+ "Warning. Data will be overwritten.");	
			
			int resultCode = cDialog.open();  
			
			//Resolve according to resultCode
			if(resultCode == RESOLVE_USING_SOURCE) {
				presenter.resolveMergeConflictUseSource(con);
			}
			else if(resultCode == RESOLVE_USING_TARGET) {
				presenter.resolveMergeConflictUseTarget(con);
				modifiedCounter++;
			};   
		} 
		
		//Calculate number of items added
		int numberOfItemsAdded = presenter.getTargetList().size() - modifiedCounter;
		
		messageBox("Import Finished", 
				String.format("%s item(s) was added, %s items(s) was modified", numberOfItemsAdded, modifiedCounter),
				 SWT.ICON_INFORMATION | SWT.OK);  
	 
		return true;
	}

	/**
	 * Export CSV file by using a FileDialog.
	 * Uses CSVPresenter for Object -> CSV transformation
	 *
	 * @param standardFileName The Filename that the FileDialog will set as default
	 */
	public void exportCSVFile(String standardFileName) { 
		
		//Setup FileDialog
		FileDialog fd = new FileDialog(parent, SWT.SAVE);
		fd.setFilterNames(new String[] { ".csv" });
		fd.setFilterExtensions(new String[] { "*.csv" }); 
		String homefolder = System.getProperty("user.home"); 
		fd.setFilterPath(homefolder);
		fd.setFileName(standardFileName + ".csv"); 
		
		//Find file path with FileDialog
		String filePath = fd.open(); 
		
		if(filePath == null) return; //If Cancel in FileDialog
		
		//If File already exists, ask for confirmation
		File f = new File(filePath);
		if(f.exists()) {
			if(messageBox("File already exists",
					String.format(
							"Do you want to overwrite the file %s", filePath), 
							SWT.YES | SWT.NO | SWT.ICON_INFORMATION) == SWT.NO) {
						return;
					};
		}
		
		try {
			presenter.writeCSVFromList(filePath);
			messageBox("File Exported",
					String.format("File successfully exported to %s", filePath), SWT.OK | SWT.ICON_INFORMATION);
			
		} catch (IOException e) {
			messageBox("Could not write file",
					"An error occured when trying to write the CSV file. Either disk is full or there is no write access",
					 SWT.ERROR | SWT.OK); 
		} catch(Exception e) { 
			messageBox("Unknown Error!",
					"An unknown error happened during import of the file. \n\n Error Message: \n\n " + e.getMessage(),
					 SWT.ERROR | SWT.OK); 
		}
	} 
	
	/**
	 * Helper method for displaying Message box.
	 *
	 * @param text the header text
	 * @param message the message
	 */
	private int messageBox(String text, String message, int style) {
		MessageBox dialog = 
				new MessageBox(Display.getDefault().getActiveShell(), style);
		dialog.setText(text);
		dialog.setMessage(
				message);
		return dialog.open();
	}

}
