package dk.dtu.imm.red.csv.opencsv;  

import au.com.bytecode.opencsv.CSVWriter;
import au.com.bytecode.opencsv.bean.MappingStrategy;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set; 
 
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition; 
 
public class BeanToCsv<T> {
	
	//Sets which types are allowed to be displayed in the Agile grid view
	Set<String> allowedTypes = new HashSet<String>(Arrays.asList(
			String.class.getName(), 
			int.class.getName(),
			Integer.class.getName(),
			float.class.getName(),
			double.class.getName(),
			long.class.getName(),
			boolean.class.getName(),
			short.class.getName(),
			byte.class.getName()));

	public BeanToCsv() {
	}

	public boolean write(MappingStrategy<T> mapper, Writer writer,
			List<?> objects) {
		return write(mapper, new CSVWriter(writer), objects);
	}

	public boolean write(MappingStrategy<T> mapper, CSVWriter csv,
			List<?> objects) {
		if (objects == null || objects.isEmpty())
			return false;

		try {
			csv.writeNext(processHeader(mapper, objects));
			List<Method> getters = findGetters(mapper);
			for (Object obj : objects) {
				String[] line = processObject(getters, obj);
				csv.writeNext(line);
			}
			return true;
		} catch (Exception e) {
			throw new RuntimeException("Error writing CSV !", e);
		}
	}

	protected String[] processHeader(MappingStrategy<T> mapper,List<?> objects)
			throws IntrospectionException {
		List<String> values = new ArrayList<String>();
		
		//Use ColumnRepresentation if av. in Element 
		Object firstObject = objects.get(0); // Will always be at least one object.
		if(firstObject instanceof ElementImpl) {
			ElementColumnDefinition[] columnDefs = ((ElementImpl) firstObject).getColumnRepresentation();
			
			for(ElementColumnDefinition c : columnDefs) {
				values.add(c.getAttributeName());
			}
		}
		else { //Default Impl.
			int i = 0;
			PropertyDescriptor prop = mapper.findDescriptor(i);
			while (prop != null) {
				
				Class<?> returnType = prop.getPropertyType(); 
				
				if(allowedTypes.contains(returnType.getName())) {
					values.add(prop.getName()); 
				} 
				
				i++;
				prop = mapper.findDescriptor(i);
			}
		}
	
		return values.toArray(new String[0]);
	}

	protected String[] processObject(List<Method> getters, Object bean)
			throws IntrospectionException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		
		List<String> values = new ArrayList<String>();
		
		if(bean instanceof ElementImpl) {
			ElementColumnDefinition[] columnDefs = ((ElementImpl) bean).getColumnRepresentation();
			
			for(ElementColumnDefinition c : columnDefs) {
				CellHandler<Object> cHandler = (CellHandler<Object>) c.getCellHandler();
				Object value = cHandler.getAttributeValue(bean);
				
				if (value == null) {
					values.add("");
				} else {
					values.add(value.toString());
				}
			} 
		}
		else {
			
			// retrieve bean values
			for (Method getter : getters) {
				
				//If the return type is not in the allowed type hash set, continue the for loop 
				//Types can derive from Enum. Since Enums are allowed, we allow them.
				
				Class<?> returnType = getter.getReturnType(); 
				
				if(!allowedTypes.contains(returnType.getName())) {
					continue;
				}
			
				Object value = getter.invoke(bean, (Object[]) null);
				if (value == null) {
					values.add("null");
				} else {
					values.add(value.toString());
				}
			} 
		}
		
		return values.toArray(new String[0]);
		
	}

	/**
	 * Build getters list from provided mapper.
	 */
	private List<Method> findGetters(MappingStrategy<T> mapper)
			throws IntrospectionException {
		int i = 0;
		PropertyDescriptor prop = mapper.findDescriptor(i);
		// build getters methods list
		List<Method> readers = new ArrayList<Method>();
		while (prop != null) {
			readers.add(prop.getReadMethod());
			i++;
			prop = mapper.findDescriptor(i);
		}
		return readers;
	}
}