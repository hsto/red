package dk.dtu.imm.red.specificationelements.requirement.ui.editors.impl;

import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.requirement.RequirementContribution;
import dk.dtu.imm.red.specificationelements.requirement.RequirementNecessity;
import dk.dtu.imm.red.specificationelements.requirement.RequirementRange;
import dk.dtu.imm.red.specificationelements.requirement.ui.editors.RequirementEditor;
import dk.dtu.imm.red.specificationelements.requirement.ui.editors.RequirementPresenter;

public class RequirementPresenterImpl extends BaseEditorPresenter implements RequirementPresenter {

	private static final int MAX_ID_LENGTH = 12;
	private static final int MAX_NAME_LENGTH = 37;
	protected RequirementEditor editor;

	protected String elaboration;
	protected String level;
	protected String remarks;
	protected String requirementText;
	
	protected RequirementRange range;
	protected RequirementContribution contribution;
	protected RequirementNecessity necessity;

	public RequirementPresenterImpl(RequirementEditor requirementEditor, Requirement element) {
		super(element);
		this.editor = requirementEditor;
		this.element = element;
	}

	@Override
	public void save() {
		super.save();
		Requirement requirement = (Requirement) element;

		requirement.setDetails(TextFactory.eINSTANCE.createText(elaboration));
		requirement.setAbstractionLevel(level);
		requirement.setRemarks(TextFactory.eINSTANCE.createText(remarks));
		
		requirement.setRange(range);
		requirement.setContribution(contribution);
		requirement.setNecessity(necessity);
		
		requirement.save();
	}

	@Override
	public void setElaboration(String text){
		this.elaboration = text;
	}

	@Override
	public void setLevel(String level) {
		this.level = level;
	}

	@Override
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public int getIdMaxLength() {
		return MAX_ID_LENGTH;
	}

	@Override
	public int getNameMaxLength() {
		return MAX_NAME_LENGTH;
	}

	@Override
	public void setRequirementText(String text) {
		this.requirementText = text;
	}

	@Override
	public void setRange(RequirementRange range) {
		this.range = range;		
	}

	@Override
	public void setContribution(RequirementContribution contribution) {
		this.contribution = contribution;
	}

	@Override
	public void setNecessity(RequirementNecessity necessity) {
		this.necessity = necessity;
	}


	

}
