package dk.dtu.imm.red.specificationelements.requirement.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewRequirementWizard extends IBaseWizard {

	String ID = "dk.dtu.imm.red.specificationelements.requirement.newwizard";

	void setRequirementId(String text);
	
}
