package dk.dtu.imm.red.specificationelements.requirement.ui.editors;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface RequirementEditor extends IBaseEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.requirement.requirementeditor";

}
