package dk.dtu.imm.red.specificationelements.requirement.ui.wizards.impl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.specificationelements.requirement.ui.operations.CreateRequirementOperation;
import dk.dtu.imm.red.specificationelements.requirement.ui.wizards.NewRequirementWizard;
import dk.dtu.imm.red.specificationelements.requirement.ui.wizards.NewRequirementWizardPresenter;

public class NewRequirementWizardPresenterImpl extends BaseWizardPresenter 
		implements NewRequirementWizardPresenter {

	public NewRequirementWizardPresenterImpl(
			final NewRequirementWizard newRequirementWizard) {
		super();
	}

	@Override
	public void wizardFinished(String label, String name, String description, Group parent, String path) {
		
		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
				.getOperationSupport();

		CreateRequirementOperation operation = 
				new CreateRequirementOperation(label, name, description, parent, path);

		operation.addContext(operationSupport.getUndoContext());

		IAdaptable info = new IAdaptable() {
			@Override
			public Object getAdapter(final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		try {
			OperationHistoryFactory.getOperationHistory().execute(operation, null,
					info);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

}
