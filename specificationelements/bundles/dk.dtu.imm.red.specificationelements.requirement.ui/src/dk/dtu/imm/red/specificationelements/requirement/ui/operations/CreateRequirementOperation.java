package dk.dtu.imm.red.specificationelements.requirement.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.requirement.RequirementFactory;
import dk.dtu.imm.red.specificationelements.requirement.ui.extensions.RequirementExtension;

public class CreateRequirementOperation extends AbstractOperation{

	protected Requirement requirement;
	protected String label;
	protected String name;
	protected String description;
	protected Group parent;
//	protected String path;

	protected RequirementExtension extension;

	public CreateRequirementOperation(String label, String name, String description, Group parent, String path) {
		super("Create Requirement");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
//		this.path = path;

		extension = new RequirementExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		requirement = RequirementFactory.eINSTANCE.createRequirement();
		requirement.setCreator(PreferenceUtil.getUserPreference_User());

		requirement.setLabel(label);
		requirement.setName(name);
		requirement.setDescription(description);
		requirement.setDetails(TextFactory.eINSTANCE.createText(description));

		if (parent != null) {
			requirement.setParent(parent);
		}

		requirement.save();
		extension.openElement(requirement);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(requirement);
			extension.deleteElement(requirement);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
