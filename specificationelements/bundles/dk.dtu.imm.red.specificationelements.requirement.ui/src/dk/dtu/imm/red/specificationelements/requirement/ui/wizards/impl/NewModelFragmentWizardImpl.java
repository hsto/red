package dk.dtu.imm.red.specificationelements.requirement.ui.wizards.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.ui.wizards.ElementBasicInfoPage;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.requirement.ui.wizards.NewRequirementWizard;
import dk.dtu.imm.red.specificationelements.requirement.ui.wizards.NewRequirementWizardPresenter;

public class NewModelFragmentWizardImpl extends BaseNewWizard 
	implements NewRequirementWizard {

//	protected ElementBasicInfoPage defineRequirementPage;
	
	protected NewRequirementWizardPresenter presenter;

	public NewModelFragmentWizardImpl(){
		super("Requirement", Requirement.class);
		presenter = new NewRequirementWizardPresenterImpl(this);
//		defineRequirementPage = new ElementBasicInfoPage("Requirement");

	}
	
	@Override
	public void addPages() {
//		addPage(defineRequirementPage);
		super.addPages();
	}


	@Override
	public boolean performFinish() {
		
		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription(); 

		Group parent = null;

		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();

			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}
				

		presenter.wizardFinished(label, name, description, parent, "");
		
		return true;
	}

	@Override
	public boolean canFinish() {
		return super.canFinish() 
				&& elementBasicInfoPage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}

	@Override
	public void setRequirementId(final String text) {
		displayInfoPage.setDisplayName(text);
		
	}
}
