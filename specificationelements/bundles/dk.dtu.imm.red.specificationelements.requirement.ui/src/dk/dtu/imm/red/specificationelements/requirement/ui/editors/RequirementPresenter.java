package dk.dtu.imm.red.specificationelements.requirement.ui.editors;

import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.specificationelements.requirement.RequirementContribution;
import dk.dtu.imm.red.specificationelements.requirement.RequirementNecessity;
import dk.dtu.imm.red.specificationelements.requirement.RequirementRange;

public interface RequirementPresenter extends IEditorPresenter {

	void setRemarks(String remarks);

	void setLevel(String level);

	void setElaboration(String text);

	int getIdMaxLength();

	int getNameMaxLength();

	void setRequirementText(String text);

	void setRange(RequirementRange range);
	void setContribution(RequirementContribution contribution);
	void setNecessity(RequirementNecessity necessity);

}
