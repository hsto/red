package dk.dtu.imm.red.specificationelements.requirement.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;

public class RequirementEditorInputImpl extends BaseEditorInput {


	public RequirementEditorInputImpl(Requirement requirement) {
		super(requirement);
	}

	@Override
	public boolean exists() {
		return false;
	}
	
	@Override
	public String getName() {
		return "Requirement editor";
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return "Editor for requirement " + element.getName();
	}
}
