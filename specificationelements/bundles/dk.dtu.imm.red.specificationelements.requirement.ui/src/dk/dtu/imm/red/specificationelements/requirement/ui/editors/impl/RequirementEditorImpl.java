package dk.dtu.imm.red.specificationelements.requirement.ui.editors.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizard;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardPresenter;
import dk.dtu.imm.red.core.ui.element.wizards.impl.CreateElementReferenceWizardImpl;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.requirement.RequirementContribution;
import dk.dtu.imm.red.specificationelements.requirement.RequirementKind;
import dk.dtu.imm.red.specificationelements.requirement.RequirementNecessity;
import dk.dtu.imm.red.specificationelements.requirement.RequirementPackage;
import dk.dtu.imm.red.specificationelements.requirement.RequirementRange;
import dk.dtu.imm.red.specificationelements.requirement.ui.editors.RequirementEditor;
import dk.dtu.imm.red.specificationelements.requirement.ui.editors.RequirementPresenter;

/**
 * An implementation of the editor for requirements.
 * @author Jakob Kragelund
 *
 */
public class RequirementEditorImpl extends BaseEditor implements RequirementEditor {

	protected ElementBasicInfoContainer basicInfoContainer;

	private static final int LEVEL_COMBO_WIDTH_HINT = 100;


	/**
	 * Hint for the height of the description rich text editor.
	 */
	private static final int DESCRIPTION_EDITOR_HEIGHTHINT = 325;

	/**
	 * The standard top and bottom margins for rich text editors.
	 */
	private static final int EDITOR_MARGIN_HEIGHT = 5;

	/**
	 * The standard left and right margins for rich text editors.
	 */
	private static final int EDITOR_MARGIN_WIDTH = 5;

	/**
	 * Hint for the height of the remarks rich text editor.
	 */
	private static final int REMARKS_EDITOR_HEIGHTHINT = 140;

	/**
	 * The rich text editor for description.
	 */
	protected RichTextEditor elaborationEditor;

	/**
	 * Combo for selecting the level of this requirement.
	 */
	protected Combo levelCombo;
	protected Combo rangeCombo;
	protected Combo contributionCombo;
	protected Combo necessityCombo;

	/**
	 * The label for the level combo.
	 */
	protected Label levelLabel;
	protected Label rangeLabel;
	protected Label contributionLabel;
	protected Label necessityLabel;

	/**
	 * Rich text editor for any additional remarks.
	 */
	protected RichTextEditor remarksEditor;

	/**
	 * A cast version of the base presenter.
	 */
	protected RequirementPresenter requirementPresenter;


	/**
	 * Indices of the various pages.
	 */
	protected int overviewIndex = 0;


	@Override
	protected void createPages() {

		Requirement requirement = (Requirement) element;

		overviewIndex = addPage(createRequirementMainPage(getContainer()));
		setPageText(overviewIndex, "Summary");

		super.createPages();

		fillInitialData();
	}



	/**
	 * Create the main tab in the editor, which contains all the main
	 * widgets for editing this requirement.
	 * @param parent The parent editor of this tab
	 * @return the created tab
	 */
	private Composite createRequirementMainPage(final Composite parent) {

		final Composite composite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(3, false);
		composite.setLayout(layout);

		GridData informationGridData = new GridData();
		informationGridData.grabExcessHorizontalSpace = true;
		informationGridData.horizontalAlignment = SWT.FILL;
		informationGridData.horizontalSpan = 3;

		Composite informationContainer = createRequirementInformationContainer(composite, SWT.NONE);
		informationContainer.setLayoutData(informationGridData);

		GridData descriptionGridData = new GridData();
		descriptionGridData.heightHint = DESCRIPTION_EDITOR_HEIGHTHINT;
		descriptionGridData.grabExcessHorizontalSpace = true;
		descriptionGridData.horizontalSpan = 3;
		descriptionGridData.horizontalAlignment = SWT.FILL;

		Composite descriptionContainer = createRequirementDescriptionContainer(composite, SWT.NONE);
		descriptionContainer.setLayoutData(descriptionGridData);

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}


	/**
	 * Creates a container for the general information group, which contains
	 * widgets for specifying the id and name of this requirement, as
	 * well as the help-button.
	 * @param parent The parent composite for this container
	 * @param style the style for this container
	 * @return the created container
	 */
	private Composite createRequirementInformationContainer(final Composite parent, final int style) {
		RequirementPresenter requirementPresenter = (RequirementPresenter) presenter;

		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(4, false);
		composite.setLayout(layout);

		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=4;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (SpecificationElement) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);


		levelLabel = new Label(composite, SWT.NONE);
		levelLabel.setText("Granularity");
		
		rangeLabel = new Label(composite, SWT.NONE);
		rangeLabel.setText("Range");
		
		contributionLabel = new Label(composite, SWT.NONE);
		contributionLabel.setText("Contribution");
		
		necessityLabel = new Label(composite, SWT.NONE);
		necessityLabel.setText("Necessity");

		// Level-widgets below are similiar to type-widgets above
		levelCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
		levelCombo.setLayoutData(new GridData(LEVEL_COMBO_WIDTH_HINT, SWT.DEFAULT));
		levelCombo.addListener(SWT.Selection, modifyListener);
		levelCombo.add("");
		levelCombo.add("Domain");
		levelCombo.add("Process");
		levelCombo.add("Activity");
		
		rangeCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
		rangeCombo.setLayoutData(new GridData(LEVEL_COMBO_WIDTH_HINT, SWT.DEFAULT));
		rangeCombo.addListener(SWT.Selection, modifyListener);
		
		contributionCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
		contributionCombo.setLayoutData(new GridData(LEVEL_COMBO_WIDTH_HINT, SWT.DEFAULT));
		contributionCombo.addListener(SWT.Selection, modifyListener);
		
		necessityCombo = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
		necessityCombo.setLayoutData(new GridData(LEVEL_COMBO_WIDTH_HINT, SWT.DEFAULT));
		necessityCombo.addListener(SWT.Selection, modifyListener);

		return composite;
	}


	/**
	 * Creates a container for the description group, which contains
	 * a rich text editor for describing the requirement.
	 * @param parent The parent composite for this container
	 * @param style the style for this container
	 * @return the created container
	 */
	private Composite createRequirementDescriptionContainer(final Composite parent, final int style) {
		Group container = new Group(parent, SWT.NONE);
		container.setText("Elaboration");

		GridLayout layout = new GridLayout();
		container.setSize(400, 400);
		layout.numColumns = 1;
		layout.marginHeight = EDITOR_MARGIN_HEIGHT;
		layout.marginWidth = EDITOR_MARGIN_WIDTH;
		container.setLayout(layout);

		GridData editorLayoutData = new GridData();
		editorLayoutData.widthHint = 700;
		editorLayoutData.heightHint = 300;
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;

		elaborationEditor = new RichTextEditor(container, SWT.BORDER,
				getEditorSite(), commandListener);
		elaborationEditor.setLayoutData(editorLayoutData);
		elaborationEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});
		
		GridData remarksLayoutData = new GridData();
		remarksLayoutData.grabExcessHorizontalSpace = true;
		remarksLayoutData.horizontalAlignment = SWT.FILL;
		remarksLayoutData.heightHint = REMARKS_EDITOR_HEIGHTHINT;

		Group remarksGroup = new Group(parent, SWT.NONE);
		remarksGroup.setText("Remarks");
		remarksGroup.setLayout(new FillLayout());
		remarksGroup.setLayoutData(remarksLayoutData);

		remarksEditor = new RichTextEditor(remarksGroup, SWT.BORDER,
				getEditorSite(), commandListener);
		remarksEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		return container;
	}


	@Override
	protected void fillInitialData() {
		Requirement requirement = (Requirement) element;

		RequirementKind[] kinds = RequirementKind.values();
		String[] kindStrings = new String[kinds.length];
		for(int i=0;i<kinds.length;i++){
			kindStrings[i] = kinds[i].getName();
		}

		basicInfoContainer.fillInitialData(requirement, kindStrings);


		if (requirement.getDetails() != null) {
			elaborationEditor.setText(requirement.getDetails().toString());
		}

		if (requirement.getAbstractionLevel() != null) {
			for (int i = 0; i < levelCombo.getItemCount(); i++) {
				if (requirement.getAbstractionLevel()
						.equals(levelCombo.getItem(i))) {
					levelCombo.select(i);
					break;
				}
			}
		}
		
		
		RequirementRange[] rangeValues = RequirementRange.values();
		String[] rangeStrings = new String[rangeValues.length];
		for (int i = 0; i < rangeStrings.length; i++) {
			rangeStrings[i] = rangeValues[i].getLiteral();
		}
		rangeCombo.setItems(rangeStrings);
		RequirementRange range = requirement.getRange();
		if (range != null) {
			rangeCombo.select(getComboSelectionIndex(rangeCombo, range.getLiteral()));
		}
		
		RequirementContribution[] contributionValues = RequirementContribution.values();
		String[] contributionStrings = new String[contributionValues.length];
		for (int i = 0; i < contributionStrings.length; i++) {
			contributionStrings[i] = contributionValues[i].getLiteral();
		}
		contributionCombo.setItems(contributionStrings);
		RequirementContribution contribution = requirement.getContribution();
		if (contribution != null) {
			contributionCombo.select(getComboSelectionIndex(contributionCombo, contribution.getLiteral()));
		}
		
		RequirementNecessity[] necessityValues = RequirementNecessity.values();
		String[] necessityStrings = new String[necessityValues.length];
		for (int i = 0; i < necessityStrings.length; i++) {
			necessityStrings[i] = necessityValues[i].getLiteral();
		}
		necessityCombo.setItems(necessityStrings);
		RequirementNecessity necessity = requirement.getNecessity();
		if (necessity != null) {
			necessityCombo.select(getComboSelectionIndex(necessityCombo, necessity.getLiteral()));
		}

		if (requirement.getRemarks() != null) {
			remarksEditor.setText(requirement.getRemarks().toString());
		}

		// Mark the editor as clean initially
		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	private int getComboSelectionIndex(Combo combo, String str) {
		for (int i = 0; i < combo.getItems().length; i++) {
			if (str.equalsIgnoreCase(combo.getItems()[i])) {
				return i;
			}
		}
		return 0;
	}




	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,overviewIndex,basicInfoContainer);

		if (feature == RequirementPackage.Literals.REQUIREMENT__DETAILS) {
			setActivePage(overviewIndex);
			elaborationEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] {"" + startIndex, "" + endIndex});
		} else if (feature == RequirementPackage.Literals.REQUIREMENT__REMARKS) {
			setActivePage(overviewIndex);
			remarksEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] {"" + startIndex, "" + endIndex});
		}
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		// put all the attributes which we want to search through
		// into the attributesToSearchMap
		super.addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);

		attributesToSearch.put(
				RequirementPackage.Literals.REQUIREMENT__DETAILS,
				elaborationEditor.getText());
		attributesToSearch.put(
				RequirementPackage.Literals.REQUIREMENT__REMARKS,
				remarksEditor.getText());



		// Now, search through all attributes, recording all search hits along the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()-1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(), result.getEndIndex());
			}
		}

		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {
		RequirementPresenter presenter = (RequirementPresenter) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		presenter.setElaboration(elaborationEditor.getText());
		presenter.setRemarks(remarksEditor.getText());

		int selectionIndex = levelCombo.getSelectionIndex();
		if (selectionIndex != -1) {
			presenter.setLevel(levelCombo.getItem(selectionIndex));
		} else {
			presenter.setLevel("");
		}
		
		RequirementRange range = RequirementRange.get(rangeCombo.getItem(rangeCombo.getSelectionIndex()));
		presenter.setRange(range);
		
		RequirementContribution contribution = RequirementContribution.get(contributionCombo.getItem(contributionCombo.getSelectionIndex()));
		presenter.setContribution(contribution);
		
		RequirementNecessity necessity = RequirementNecessity.get(necessityCombo.getItem(necessityCombo.getSelectionIndex()));
		presenter.setNecessity(necessity);

		super.doSave(monitor);

		isDirty = false;
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}



	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	/**
	 * Get a reference to a user-selected element. Opens a wizard,
	 * which lets the user pick an element of any type.
	 * @return the selected element.
	 */
	private Element getElementReference() {
		return getElementReference(null);
	}

	/**
	 * Get a reference to a user-selected element. Opens a wizard,
	 * which lets the user pick an element of one of the types in the
	 * allowedTypes parameter.
	 * @param allowedTypes list of allowed types which the user can select.
	 * @return the selected element.
	 */
	private Element getElementReference(final Class[] allowedTypes) {
		CreateElementReferenceWizard wizard =
				new CreateElementReferenceWizardImpl(allowedTypes);
		wizard.init(PlatformUI.getWorkbench(),
				(IStructuredSelection) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection());

		WizardDialog dialog =
				new WizardDialog(Display.getCurrent().getActiveShell(),
						wizard);

		dialog.open();

		Element selectedElement =
				((CreateElementReferenceWizardPresenter) wizard.getPresenter())
				.getselectedElement();

		return selectedElement;
	}

	@Override
	public void init(final IEditorSite site, final IEditorInput input)
			throws PartInitException {
		super.init(site, input);

		if (element == null || !(element instanceof Requirement)) {
			throw new PartInitException("Invalid element supplied. "
					+ "Must be a requirement, and must not be null");
		}

		presenter = new RequirementPresenterImpl(this, (Requirement) element);
		requirementPresenter = (RequirementPresenter) presenter;
	}

	@Override
	public boolean isDirty() {
//		if (fragmentEditor != null && fragmentEditor.isDirty()) {
//			return true;
//		}
		return super.isDirty();
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Requirement.html";
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.requirement.requirementeditor";
	}

}
