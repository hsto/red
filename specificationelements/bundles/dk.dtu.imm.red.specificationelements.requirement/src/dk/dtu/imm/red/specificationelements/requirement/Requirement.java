/**
 */
package dk.dtu.imm.red.specificationelements.requirement;

import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.specificationelements.SpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getAbstractionLevel <em>Abstraction Level</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getDetails <em>Details</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getId <em>Id</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getRemarks <em>Remarks</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getRange <em>Range</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getContribution <em>Contribution</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getNecessity <em>Necessity</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementPackage#getRequirement()
 * @model
 * @generated
 */
public interface Requirement extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Abstraction Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstraction Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstraction Level</em>' attribute.
	 * @see #setAbstractionLevel(String)
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementPackage#getRequirement_AbstractionLevel()
	 * @model
	 * @generated
	 */
	String getAbstractionLevel();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getAbstractionLevel <em>Abstraction Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstraction Level</em>' attribute.
	 * @see #getAbstractionLevel()
	 * @generated
	 */
	void setAbstractionLevel(String value);

	/**
	 * Returns the value of the '<em><b>Details</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Details</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Details</em>' containment reference.
	 * @see #setDetails(Text)
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementPackage#getRequirement_Details()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getDetails();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getDetails <em>Details</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Details</em>' containment reference.
	 * @see #getDetails()
	 * @generated
	 */
	void setDetails(Text value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementPackage#getRequirement_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Remarks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remarks</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remarks</em>' containment reference.
	 * @see #setRemarks(Text)
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementPackage#getRequirement_Remarks()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getRemarks();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getRemarks <em>Remarks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remarks</em>' containment reference.
	 * @see #getRemarks()
	 * @generated
	 */
	void setRemarks(Text value);

	/**
	 * Returns the value of the '<em><b>Range</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.requirement.RequirementRange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementRange
	 * @see #setRange(RequirementRange)
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementPackage#getRequirement_Range()
	 * @model
	 * @generated
	 */
	RequirementRange getRange();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getRange <em>Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementRange
	 * @see #getRange()
	 * @generated
	 */
	void setRange(RequirementRange value);

	/**
	 * Returns the value of the '<em><b>Contribution</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.requirement.RequirementContribution}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contribution</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contribution</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementContribution
	 * @see #setContribution(RequirementContribution)
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementPackage#getRequirement_Contribution()
	 * @model
	 * @generated
	 */
	RequirementContribution getContribution();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getContribution <em>Contribution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contribution</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementContribution
	 * @see #getContribution()
	 * @generated
	 */
	void setContribution(RequirementContribution value);

	/**
	 * Returns the value of the '<em><b>Necessity</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.requirement.RequirementNecessity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Necessity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Necessity</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementNecessity
	 * @see #setNecessity(RequirementNecessity)
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementPackage#getRequirement_Necessity()
	 * @model
	 * @generated
	 */
	RequirementNecessity getNecessity();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.requirement.Requirement#getNecessity <em>Necessity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Necessity</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementNecessity
	 * @see #getNecessity()
	 * @generated
	 */
	void setNecessity(RequirementNecessity value);

} // Requirement
