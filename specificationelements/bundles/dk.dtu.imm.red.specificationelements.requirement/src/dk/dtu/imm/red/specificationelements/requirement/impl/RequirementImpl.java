/**
 */
package dk.dtu.imm.red.specificationelements.requirement.impl;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.requirement.RequirementContribution;
import dk.dtu.imm.red.specificationelements.requirement.RequirementKind;
import dk.dtu.imm.red.specificationelements.requirement.RequirementNecessity;
import dk.dtu.imm.red.specificationelements.requirement.RequirementPackage;
import dk.dtu.imm.red.specificationelements.requirement.RequirementRange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.impl.RequirementImpl#getAbstractionLevel <em>Abstraction Level</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.impl.RequirementImpl#getDetails <em>Details</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.impl.RequirementImpl#getId <em>Id</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.impl.RequirementImpl#getRemarks <em>Remarks</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.impl.RequirementImpl#getRange <em>Range</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.impl.RequirementImpl#getContribution <em>Contribution</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.requirement.impl.RequirementImpl#getNecessity <em>Necessity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequirementImpl extends SpecificationElementImpl implements Requirement {
	/**
	 * The default value of the '{@link #getAbstractionLevel() <em>Abstraction Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractionLevel()
	 * @generated
	 * @ordered
	 */
	protected static final String ABSTRACTION_LEVEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAbstractionLevel() <em>Abstraction Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractionLevel()
	 * @generated
	 * @ordered
	 */
	protected String abstractionLevel = ABSTRACTION_LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDetails() <em>Details</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetails()
	 * @generated
	 * @ordered
	 */
	protected Text details;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRemarks() <em>Remarks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemarks()
	 * @generated
	 * @ordered
	 */
	protected Text remarks;

	/**
	 * The default value of the '{@link #getRange() <em>Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected static final RequirementRange RANGE_EDEFAULT = RequirementRange.UNSPECIFIED;

	/**
	 * The cached value of the '{@link #getRange() <em>Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected RequirementRange range = RANGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getContribution() <em>Contribution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContribution()
	 * @generated
	 * @ordered
	 */
	protected static final RequirementContribution CONTRIBUTION_EDEFAULT = RequirementContribution.UNSPECIFIED;

	/**
	 * The cached value of the '{@link #getContribution() <em>Contribution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContribution()
	 * @generated
	 * @ordered
	 */
	protected RequirementContribution contribution = CONTRIBUTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getNecessity() <em>Necessity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNecessity()
	 * @generated
	 * @ordered
	 */
	protected static final RequirementNecessity NECESSITY_EDEFAULT = RequirementNecessity.UNSPECIFIED;

	/**
	 * The cached value of the '{@link #getNecessity() <em>Necessity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNecessity()
	 * @generated
	 * @ordered
	 */
	protected RequirementNecessity necessity = NECESSITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAbstractionLevel() {
		return abstractionLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractionLevel(String newAbstractionLevel) {
		String oldAbstractionLevel = abstractionLevel;
		abstractionLevel = newAbstractionLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequirementPackage.REQUIREMENT__ABSTRACTION_LEVEL, oldAbstractionLevel, abstractionLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getDetails() {
		if (details != null && details.eIsProxy()) {
			InternalEObject oldDetails = (InternalEObject)details;
			details = (Text)eResolveProxy(oldDetails);
			if (details != oldDetails) {
				InternalEObject newDetails = (InternalEObject)details;
				NotificationChain msgs = oldDetails.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RequirementPackage.REQUIREMENT__DETAILS, null, null);
				if (newDetails.eInternalContainer() == null) {
					msgs = newDetails.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RequirementPackage.REQUIREMENT__DETAILS, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RequirementPackage.REQUIREMENT__DETAILS, oldDetails, details));
			}
		}
		return details;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetDetails() {
		return details;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDetails(Text newDetails, NotificationChain msgs) {
		Text oldDetails = details;
		details = newDetails;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RequirementPackage.REQUIREMENT__DETAILS, oldDetails, newDetails);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDetails(Text newDetails) {
		if (newDetails != details) {
			NotificationChain msgs = null;
			if (details != null)
				msgs = ((InternalEObject)details).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RequirementPackage.REQUIREMENT__DETAILS, null, msgs);
			if (newDetails != null)
				msgs = ((InternalEObject)newDetails).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RequirementPackage.REQUIREMENT__DETAILS, null, msgs);
			msgs = basicSetDetails(newDetails, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequirementPackage.REQUIREMENT__DETAILS, newDetails, newDetails));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequirementPackage.REQUIREMENT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getRemarks() {
		if (remarks != null && remarks.eIsProxy()) {
			InternalEObject oldRemarks = (InternalEObject)remarks;
			remarks = (Text)eResolveProxy(oldRemarks);
			if (remarks != oldRemarks) {
				InternalEObject newRemarks = (InternalEObject)remarks;
				NotificationChain msgs = oldRemarks.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RequirementPackage.REQUIREMENT__REMARKS, null, null);
				if (newRemarks.eInternalContainer() == null) {
					msgs = newRemarks.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RequirementPackage.REQUIREMENT__REMARKS, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RequirementPackage.REQUIREMENT__REMARKS, oldRemarks, remarks));
			}
		}
		return remarks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetRemarks() {
		return remarks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemarks(Text newRemarks, NotificationChain msgs) {
		Text oldRemarks = remarks;
		remarks = newRemarks;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RequirementPackage.REQUIREMENT__REMARKS, oldRemarks, newRemarks);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemarks(Text newRemarks) {
		if (newRemarks != remarks) {
			NotificationChain msgs = null;
			if (remarks != null)
				msgs = ((InternalEObject)remarks).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RequirementPackage.REQUIREMENT__REMARKS, null, msgs);
			if (newRemarks != null)
				msgs = ((InternalEObject)newRemarks).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RequirementPackage.REQUIREMENT__REMARKS, null, msgs);
			msgs = basicSetRemarks(newRemarks, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequirementPackage.REQUIREMENT__REMARKS, newRemarks, newRemarks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementRange getRange() {
		return range;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRange(RequirementRange newRange) {
		RequirementRange oldRange = range;
		range = newRange == null ? RANGE_EDEFAULT : newRange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequirementPackage.REQUIREMENT__RANGE, oldRange, range));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementContribution getContribution() {
		return contribution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContribution(RequirementContribution newContribution) {
		RequirementContribution oldContribution = contribution;
		contribution = newContribution == null ? CONTRIBUTION_EDEFAULT : newContribution;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequirementPackage.REQUIREMENT__CONTRIBUTION, oldContribution, contribution));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementNecessity getNecessity() {
		return necessity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNecessity(RequirementNecessity newNecessity) {
		RequirementNecessity oldNecessity = necessity;
		necessity = newNecessity == null ? NECESSITY_EDEFAULT : newNecessity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequirementPackage.REQUIREMENT__NECESSITY, oldNecessity, necessity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RequirementPackage.REQUIREMENT__DETAILS:
				return basicSetDetails(null, msgs);
			case RequirementPackage.REQUIREMENT__REMARKS:
				return basicSetRemarks(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RequirementPackage.REQUIREMENT__ABSTRACTION_LEVEL:
				return getAbstractionLevel();
			case RequirementPackage.REQUIREMENT__DETAILS:
				if (resolve) return getDetails();
				return basicGetDetails();
			case RequirementPackage.REQUIREMENT__ID:
				return getId();
			case RequirementPackage.REQUIREMENT__REMARKS:
				if (resolve) return getRemarks();
				return basicGetRemarks();
			case RequirementPackage.REQUIREMENT__RANGE:
				return getRange();
			case RequirementPackage.REQUIREMENT__CONTRIBUTION:
				return getContribution();
			case RequirementPackage.REQUIREMENT__NECESSITY:
				return getNecessity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RequirementPackage.REQUIREMENT__ABSTRACTION_LEVEL:
				setAbstractionLevel((String)newValue);
				return;
			case RequirementPackage.REQUIREMENT__DETAILS:
				setDetails((Text)newValue);
				return;
			case RequirementPackage.REQUIREMENT__ID:
				setId((String)newValue);
				return;
			case RequirementPackage.REQUIREMENT__REMARKS:
				setRemarks((Text)newValue);
				return;
			case RequirementPackage.REQUIREMENT__RANGE:
				setRange((RequirementRange)newValue);
				return;
			case RequirementPackage.REQUIREMENT__CONTRIBUTION:
				setContribution((RequirementContribution)newValue);
				return;
			case RequirementPackage.REQUIREMENT__NECESSITY:
				setNecessity((RequirementNecessity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RequirementPackage.REQUIREMENT__ABSTRACTION_LEVEL:
				setAbstractionLevel(ABSTRACTION_LEVEL_EDEFAULT);
				return;
			case RequirementPackage.REQUIREMENT__DETAILS:
				setDetails((Text)null);
				return;
			case RequirementPackage.REQUIREMENT__ID:
				setId(ID_EDEFAULT);
				return;
			case RequirementPackage.REQUIREMENT__REMARKS:
				setRemarks((Text)null);
				return;
			case RequirementPackage.REQUIREMENT__RANGE:
				setRange(RANGE_EDEFAULT);
				return;
			case RequirementPackage.REQUIREMENT__CONTRIBUTION:
				setContribution(CONTRIBUTION_EDEFAULT);
				return;
			case RequirementPackage.REQUIREMENT__NECESSITY:
				setNecessity(NECESSITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RequirementPackage.REQUIREMENT__ABSTRACTION_LEVEL:
				return ABSTRACTION_LEVEL_EDEFAULT == null ? abstractionLevel != null : !ABSTRACTION_LEVEL_EDEFAULT.equals(abstractionLevel);
			case RequirementPackage.REQUIREMENT__DETAILS:
				return details != null;
			case RequirementPackage.REQUIREMENT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case RequirementPackage.REQUIREMENT__REMARKS:
				return remarks != null;
			case RequirementPackage.REQUIREMENT__RANGE:
				return range != RANGE_EDEFAULT;
			case RequirementPackage.REQUIREMENT__CONTRIBUTION:
				return contribution != CONTRIBUTION_EDEFAULT;
			case RequirementPackage.REQUIREMENT__NECESSITY:
				return necessity != NECESSITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (abstractionLevel: ");
		result.append(abstractionLevel);
		result.append(", id: ");
		result.append(id);
		result.append(", range: ");
		result.append(range);
		result.append(", contribution: ");
		result.append(contribution);
		result.append(", necessity: ");
		result.append(necessity);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/requirement.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		// add the attributes of this element to the attributesToSearchMap
		attributesToSearch.put(
				RequirementPackage.Literals.REQUIREMENT__DETAILS,
				this.getDetails()== null ? "" : this.getDetails().toString());
		attributesToSearch.put(
				RequirementPackage.Literals.REQUIREMENT__REMARKS,
				this.getRemarks() == null ? "" : this.getRemarks().toString());


		return super.search(param, attributesToSearch);
	}

	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {
		return new ElementColumnDefinition[] {
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Requirement>() {

					@Override
					public Object getAttributeValue(Requirement item) {
						return item.getName()==null?"":item.getName();
					}

					@Override
					public void setAttributeValue(Requirement item, Object value) {
						item.setName((String) value);
					}
				})
				,
				new ElementColumnDefinition()
				.setHeaderName("Type")
				.setEMFLiteral(RequirementPackage.Literals.REQUIREMENT_KIND)
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setCellItems(new String[] { "Feature", "Quality", "Constraint" })
				.setEditable(true)
				.setCellHandler(new CellHandler<Requirement>() {

					@Override
					public Object getAttributeValue(Requirement item) {
						return item.getElementKind()==null?"":item.getElementKind();
					}

					@Override
					public void setAttributeValue(Requirement item, Object value) {
						item.setElementKind((String) value);;
					}
				})
				,
				new ElementColumnDefinition()
				.setHeaderName("Level")
				.setEMFLiteral(RequirementPackage.Literals.REQUIREMENT__ABSTRACTION_LEVEL)
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setCellItems(new String[] { "Activity", "Process", "Domain" })
				.setEditable(true)
				.setCellHandler(new CellHandler<Requirement>() {

					@Override
					public Object getAttributeValue(Requirement item) {
						return item.getAbstractionLevel()==null?"":item.getAbstractionLevel();
					}

					@Override
					public void setAttributeValue(Requirement item, Object value) {
						item.setAbstractionLevel((String) value);;
					}
				})
				,
				new ElementColumnDefinition()
				.setHeaderName("Requirement")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__DESCRIPTION)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Requirement>() {

					@Override
					public Object getAttributeValue(Requirement item) {
						return item.getDescription()==null?"":item.getDescription();
					}

					@Override
					public void setAttributeValue(Requirement item, Object value) {
						item.setDescription((String) value);
					}
				})
				,
//				new ElementColumnDefinition()
//				.setHeaderName("Rationale")
//				.setEMFLiteral(RequirementPackage.Literals.REQUIREMENT__RATIONALE_TEXT)
//				.setColumnBehaviour(ColumnBehaviour.CellEdit)
//				.setEditable(true)
//				.setCellHandler(new CellHandler<Requirement>() {
//
//					@Override
//					public Object getAttributeValue(Requirement item) {
//						return item.getRationaleText()==null?"":item.getRationaleText();
//					}
//
//					@Override
//					public void setAttributeValue(Requirement item, Object value) {
//						item.setRationaleText((String) value);
//					}
//				})
		};
	}
	
	@Override
	public String getStructeredIdType() {
		if (elementKind != null && elementKind.equalsIgnoreCase(RequirementKind.FEATURE.toString())) {
			return "F";
		}
		else if (elementKind != null && elementKind.equalsIgnoreCase(RequirementKind.QUALITY.toString())) {
			return "Q";
		}
		else {
			return super.getStructeredIdType();
		}
	}

} //RequirementImpl
