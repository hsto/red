/**
 */
package dk.dtu.imm.red.specificationelements.requirement.impl;

import dk.dtu.imm.red.core.text.TextPackage;
import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.requirement.RequirementContribution;
import dk.dtu.imm.red.specificationelements.requirement.RequirementFactory;
import dk.dtu.imm.red.specificationelements.requirement.RequirementKind;
import dk.dtu.imm.red.specificationelements.requirement.RequirementNecessity;
import dk.dtu.imm.red.specificationelements.requirement.RequirementPackage;
import dk.dtu.imm.red.specificationelements.requirement.RequirementRange;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RequirementPackageImpl extends EPackageImpl implements RequirementPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum requirementKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum requirementRangeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum requirementContributionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum requirementNecessityEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.specificationelements.requirement.RequirementPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RequirementPackageImpl() {
		super(eNS_URI, RequirementFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RequirementPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RequirementPackage init() {
		if (isInited) return (RequirementPackage)EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI);

		// Obtain or create and register package
		RequirementPackageImpl theRequirementPackage = (RequirementPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RequirementPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RequirementPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRequirementPackage.createPackageContents();

		// Initialize created meta-data
		theRequirementPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRequirementPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RequirementPackage.eNS_URI, theRequirementPackage);
		return theRequirementPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequirement() {
		return requirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequirement_AbstractionLevel() {
		return (EAttribute)requirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirement_Details() {
		return (EReference)requirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequirement_Id() {
		return (EAttribute)requirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirement_Remarks() {
		return (EReference)requirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequirement_Range() {
		return (EAttribute)requirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequirement_Contribution() {
		return (EAttribute)requirementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequirement_Necessity() {
		return (EAttribute)requirementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRequirementKind() {
		return requirementKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRequirementRange() {
		return requirementRangeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRequirementContribution() {
		return requirementContributionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRequirementNecessity() {
		return requirementNecessityEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementFactory getRequirementFactory() {
		return (RequirementFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		requirementEClass = createEClass(REQUIREMENT);
		createEAttribute(requirementEClass, REQUIREMENT__ABSTRACTION_LEVEL);
		createEReference(requirementEClass, REQUIREMENT__DETAILS);
		createEAttribute(requirementEClass, REQUIREMENT__ID);
		createEReference(requirementEClass, REQUIREMENT__REMARKS);
		createEAttribute(requirementEClass, REQUIREMENT__RANGE);
		createEAttribute(requirementEClass, REQUIREMENT__CONTRIBUTION);
		createEAttribute(requirementEClass, REQUIREMENT__NECESSITY);

		// Create enums
		requirementKindEEnum = createEEnum(REQUIREMENT_KIND);
		requirementRangeEEnum = createEEnum(REQUIREMENT_RANGE);
		requirementContributionEEnum = createEEnum(REQUIREMENT_CONTRIBUTION);
		requirementNecessityEEnum = createEEnum(REQUIREMENT_NECESSITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SpecificationelementsPackage theSpecificationelementsPackage = (SpecificationelementsPackage)EPackage.Registry.INSTANCE.getEPackage(SpecificationelementsPackage.eNS_URI);
		TextPackage theTextPackage = (TextPackage)EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		requirementEClass.getESuperTypes().add(theSpecificationelementsPackage.getSpecificationElement());

		// Initialize classes and features; add operations and parameters
		initEClass(requirementEClass, Requirement.class, "Requirement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRequirement_AbstractionLevel(), ecorePackage.getEString(), "abstractionLevel", null, 0, 1, Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirement_Details(), theTextPackage.getText(), null, "details", null, 0, 1, Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirement_Id(), ecorePackage.getEString(), "id", null, 0, 1, Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirement_Remarks(), theTextPackage.getText(), null, "remarks", null, 0, 1, Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirement_Range(), this.getRequirementRange(), "range", null, 0, 1, Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirement_Contribution(), this.getRequirementContribution(), "contribution", null, 0, 1, Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequirement_Necessity(), this.getRequirementNecessity(), "necessity", null, 0, 1, Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(requirementKindEEnum, RequirementKind.class, "RequirementKind");
		addEEnumLiteral(requirementKindEEnum, RequirementKind.UNSPECIFIED);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.FEATURE);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.QUALITY);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.CONSTRAINT);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.SUITABILITY);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.RELIABILITY);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.EFFICIENCY);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.OPERABILITY);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.SECURITY);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.COMPATIBILITY);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.MAINTAINABILITY);
		addEEnumLiteral(requirementKindEEnum, RequirementKind.TRANSFERABILITY);

		initEEnum(requirementRangeEEnum, RequirementRange.class, "RequirementRange");
		addEEnumLiteral(requirementRangeEEnum, RequirementRange.UNSPECIFIED);
		addEEnumLiteral(requirementRangeEEnum, RequirementRange.LOCAL);
		addEEnumLiteral(requirementRangeEEnum, RequirementRange.RESTRICTED);
		addEEnumLiteral(requirementRangeEEnum, RequirementRange.CROSS_CUTTING);

		initEEnum(requirementContributionEEnum, RequirementContribution.class, "RequirementContribution");
		addEEnumLiteral(requirementContributionEEnum, RequirementContribution.UNSPECIFIED);
		addEEnumLiteral(requirementContributionEEnum, RequirementContribution.ADDITION);
		addEEnumLiteral(requirementContributionEEnum, RequirementContribution.IMPROVEMENT);
		addEEnumLiteral(requirementContributionEEnum, RequirementContribution.CORRECTION);
		addEEnumLiteral(requirementContributionEEnum, RequirementContribution.MODIFICATION);

		initEEnum(requirementNecessityEEnum, RequirementNecessity.class, "RequirementNecessity");
		addEEnumLiteral(requirementNecessityEEnum, RequirementNecessity.UNSPECIFIED);
		addEEnumLiteral(requirementNecessityEEnum, RequirementNecessity.NONE);
		addEEnumLiteral(requirementNecessityEEnum, RequirementNecessity.MINOR);
		addEEnumLiteral(requirementNecessityEEnum, RequirementNecessity.MAJOR);
		addEEnumLiteral(requirementNecessityEEnum, RequirementNecessity.CRITICAL);

		// Create resource
		createResource(eNS_URI);
	}

} //RequirementPackageImpl
