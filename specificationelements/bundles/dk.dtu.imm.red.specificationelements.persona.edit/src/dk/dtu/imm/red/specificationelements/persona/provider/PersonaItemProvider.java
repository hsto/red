/**
 */
package dk.dtu.imm.red.specificationelements.persona.provider;


import dk.dtu.imm.red.core.element.ElementPackage;

import dk.dtu.imm.red.core.text.TextFactory;

import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.PersonaPackage;

import dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardFactory;

import dk.dtu.imm.red.specificationelements.provider.SpecificationElementItemProvider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.specificationelements.persona.Persona} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PersonaItemProvider
	extends SpecificationElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonaItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAgePropertyDescriptor(object);
			addJobPropertyDescriptor(object);
			addPictureURIPropertyDescriptor(object);
			addPictureDataPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Age feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAgePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Persona_age_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Persona_age_feature", "_UI_Persona_type"),
				 PersonaPackage.Literals.PERSONA__AGE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Job feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addJobPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Persona_job_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Persona_job_feature", "_UI_Persona_type"),
				 PersonaPackage.Literals.PERSONA__JOB,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Picture URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPictureURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Persona_pictureURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Persona_pictureURI_feature", "_UI_Persona_type"),
				 PersonaPackage.Literals.PERSONA__PICTURE_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Picture Data feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPictureDataPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Persona_pictureData_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Persona_pictureData_feature", "_UI_Persona_type"),
				 PersonaPackage.Literals.PERSONA__PICTURE_DATA,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(PersonaPackage.Literals.PERSONA__STORYBOARDS);
			childrenFeatures.add(PersonaPackage.Literals.PERSONA__ISSUES);
			childrenFeatures.add(PersonaPackage.Literals.PERSONA__CAPABILITIES);
			childrenFeatures.add(PersonaPackage.Literals.PERSONA__CONSTRAINTS);
			childrenFeatures.add(PersonaPackage.Literals.PERSONA__NARRATIVE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Persona.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Persona"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Persona)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Persona_type") :
			getString("_UI_Persona_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Persona.class)) {
			case PersonaPackage.PERSONA__AGE:
			case PersonaPackage.PERSONA__JOB:
			case PersonaPackage.PERSONA__PICTURE_URI:
			case PersonaPackage.PERSONA__PICTURE_DATA:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case PersonaPackage.PERSONA__STORYBOARDS:
			case PersonaPackage.PERSONA__ISSUES:
			case PersonaPackage.PERSONA__CAPABILITIES:
			case PersonaPackage.PERSONA__CONSTRAINTS:
			case PersonaPackage.PERSONA__NARRATIVE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(PersonaPackage.Literals.PERSONA__STORYBOARDS,
				 StoryboardFactory.eINSTANCE.createStoryboard()));

		newChildDescriptors.add
			(createChildParameter
				(PersonaPackage.Literals.PERSONA__ISSUES,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(PersonaPackage.Literals.PERSONA__CAPABILITIES,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(PersonaPackage.Literals.PERSONA__CONSTRAINTS,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(PersonaPackage.Literals.PERSONA__NARRATIVE,
				 TextFactory.eINSTANCE.createText()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ElementPackage.Literals.ELEMENT__COMMENTLIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CHANGE_LIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CREATOR ||
			childFeature == ElementPackage.Literals.ELEMENT__RESPONSIBLE_USER ||
			childFeature == ElementPackage.Literals.ELEMENT__COST ||
			childFeature == ElementPackage.Literals.ELEMENT__BENEFIT ||
			childFeature == ElementPackage.Literals.ELEMENT__MANAGEMENT_DISCUSSION ||
			childFeature == PersonaPackage.Literals.PERSONA__ISSUES ||
			childFeature == PersonaPackage.Literals.PERSONA__CAPABILITIES ||
			childFeature == PersonaPackage.Literals.PERSONA__CONSTRAINTS ||
			childFeature == PersonaPackage.Literals.PERSONA__NARRATIVE;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return PersonaEditPlugin.INSTANCE;
	}

}
