/**
 */
package dk.dtu.imm.red.specificationelements.usecase.provider;


import dk.dtu.imm.red.core.element.ElementPackage;

import dk.dtu.imm.red.core.properties.PropertiesFactory;

import dk.dtu.imm.red.core.text.TextFactory;

import dk.dtu.imm.red.specificationelements.provider.SpecificationElementItemProvider;

import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.UsecaseFactory;
import dk.dtu.imm.red.specificationelements.usecase.UsecasePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.specificationelements.usecase.UseCase} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UseCaseItemProvider extends SpecificationElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCaseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPreConditionsPropertyDescriptor(object);
			addPostConditionsPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addParameterPropertyDescriptor(object);
			addTriggerPropertyDescriptor(object);
			addOutcomePropertyDescriptor(object);
			addResultPropertyDescriptor(object);
			addPrimaryScenariosPropertyDescriptor(object);
			addPrimaryActorsPropertyDescriptor(object);
			addSecondaryScenariosPropertyDescriptor(object);
			addSecondaryActorsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Pre Conditions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPreConditionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_preConditions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_preConditions_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__PRE_CONDITIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Post Conditions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPostConditionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_postConditions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_postConditions_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__POST_CONDITIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_type_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Parameter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameterPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_parameter_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_parameter_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__PARAMETER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Trigger feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTriggerPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_trigger_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_trigger_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__TRIGGER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Outcome feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutcomePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_outcome_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_outcome_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__OUTCOME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Result feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResultPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_result_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_result_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__RESULT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Primary Scenarios feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrimaryScenariosPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_primaryScenarios_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_primaryScenarios_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__PRIMARY_SCENARIOS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Primary Actors feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrimaryActorsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_primaryActors_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_primaryActors_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__PRIMARY_ACTORS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Secondary Scenarios feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSecondaryScenariosPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_secondaryScenarios_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_secondaryScenarios_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__SECONDARY_SCENARIOS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Secondary Actors feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSecondaryActorsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UseCase_secondaryActors_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UseCase_secondaryActors_feature", "_UI_UseCase_type"),
				 UsecasePackage.Literals.USE_CASE__SECONDARY_ACTORS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UsecasePackage.Literals.USE_CASE__LONG_DESCRIPTION);
			childrenFeatures.add(UsecasePackage.Literals.USE_CASE__INCIDENCE);
			childrenFeatures.add(UsecasePackage.Literals.USE_CASE__DURATION);
			childrenFeatures.add(UsecasePackage.Literals.USE_CASE__INPUT_OUTPUT_LIST);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns UseCase.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UseCase"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((UseCase)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_UseCase_type") :
			getString("_UI_UseCase_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UseCase.class)) {
			case UsecasePackage.USE_CASE__PRE_CONDITIONS:
			case UsecasePackage.USE_CASE__POST_CONDITIONS:
			case UsecasePackage.USE_CASE__TYPE:
			case UsecasePackage.USE_CASE__PARAMETER:
			case UsecasePackage.USE_CASE__TRIGGER:
			case UsecasePackage.USE_CASE__OUTCOME:
			case UsecasePackage.USE_CASE__RESULT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case UsecasePackage.USE_CASE__LONG_DESCRIPTION:
			case UsecasePackage.USE_CASE__INCIDENCE:
			case UsecasePackage.USE_CASE__DURATION:
			case UsecasePackage.USE_CASE__INPUT_OUTPUT_LIST:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UsecasePackage.Literals.USE_CASE__LONG_DESCRIPTION,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(UsecasePackage.Literals.USE_CASE__INCIDENCE,
				 PropertiesFactory.eINSTANCE.createProperty()));

		newChildDescriptors.add
			(createChildParameter
				(UsecasePackage.Literals.USE_CASE__DURATION,
				 PropertiesFactory.eINSTANCE.createProperty()));

		newChildDescriptors.add
			(createChildParameter
				(UsecasePackage.Literals.USE_CASE__INPUT_OUTPUT_LIST,
				 UsecaseFactory.eINSTANCE.createInputOutput()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ElementPackage.Literals.ELEMENT__COMMENTLIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CHANGE_LIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CREATOR ||
			childFeature == ElementPackage.Literals.ELEMENT__RESPONSIBLE_USER ||
			childFeature == ElementPackage.Literals.ELEMENT__COST ||
			childFeature == ElementPackage.Literals.ELEMENT__BENEFIT ||
			childFeature == UsecasePackage.Literals.USE_CASE__INCIDENCE ||
			childFeature == UsecasePackage.Literals.USE_CASE__DURATION ||
			childFeature == ElementPackage.Literals.ELEMENT__MANAGEMENT_DISCUSSION ||
			childFeature == UsecasePackage.Literals.USE_CASE__LONG_DESCRIPTION;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return UseCaseEditPlugin.INSTANCE;
	}

}
