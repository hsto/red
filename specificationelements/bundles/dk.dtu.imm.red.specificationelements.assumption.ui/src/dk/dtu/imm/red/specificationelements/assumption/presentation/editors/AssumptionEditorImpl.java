package dk.dtu.imm.red.specificationelements.assumption.presentation.editors;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;
import dk.dtu.imm.red.specificationelements.assumption.AssumptionKind;

/**
 * The editor for creating and editing an assumption
 *
 * @author Henry Lie
 *
 */
public class AssumptionEditorImpl extends BaseEditor implements AssumptionEditor{

	protected ElementBasicInfoContainer basicInfoContainer;
	protected int assumptionPageIndex = 0;

	@Override
	public void doSaveAs() {

	}

	@Override
	public void doSave(IProgressMonitor monitor) {

		AssumptionPresenterImpl presenter = (AssumptionPresenterImpl) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());

		super.doSave(monitor);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);

		presenter = new AssumptionPresenterImpl(this, (Assumption) element);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	protected void createPages() {
		assumptionPageIndex = addPage(createAssumptionPage(getContainer()));
		setPageText(assumptionPageIndex, "Summary");
		super.createPages();

		fillInitialData();
	}

	private Composite createAssumptionPage(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(1, false);

		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (Assumption) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}


	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Assumption.html";
	}

	@Override
	protected void fillInitialData() {

		Assumption assumption = (Assumption) element;

		AssumptionKind[] assumptionKinds = AssumptionKind.values();
		String[] assumptionKindStrings = new String[assumptionKinds.length];
		for(int i=0;i<assumptionKinds.length;i++){
			assumptionKindStrings[i] = assumptionKinds[i].getName();
		}

		basicInfoContainer.fillInitialData(assumption, assumptionKindStrings);

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}


	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		super.addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);


		// Now, search through all attributes, recording all search hits along the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()-1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(), result.getEndIndex());
			}
		}

		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}

	@Override
	public void selectText(EStructuralFeature feature, int startIndex, int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,assumptionPageIndex,basicInfoContainer);
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.assumption.assumptioneditor";
	}
}
