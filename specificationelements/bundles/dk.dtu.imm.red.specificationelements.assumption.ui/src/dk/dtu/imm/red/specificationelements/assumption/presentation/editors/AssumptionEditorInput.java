package dk.dtu.imm.red.specificationelements.assumption.presentation.editors;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;

public class AssumptionEditorInput extends BaseEditorInput {

	public AssumptionEditorInput(Assumption assumption) {
		super(assumption);
	}

	public boolean exists() {
		return false;
	}

	public String getName() {
		return "Assumption editor";
	}

	public String getToolTipText() {
		return "This is the assumption editor";
	}
}
