package dk.dtu.imm.red.specificationelements.assumption.presentation.editors;

import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface AssumptionEditor extends IEditorPart, IBaseEditor {
	public static final String ID = "dk.dtu.imm.red.specificationelements.assumption.assumptioneditor";
}
