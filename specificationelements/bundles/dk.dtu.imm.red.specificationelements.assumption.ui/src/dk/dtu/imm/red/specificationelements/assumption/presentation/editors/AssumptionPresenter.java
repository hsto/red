package dk.dtu.imm.red.specificationelements.assumption.presentation.editors;


public interface AssumptionPresenter {

	void setAssumption(String text);

	int getTitleMaxLength();

}
