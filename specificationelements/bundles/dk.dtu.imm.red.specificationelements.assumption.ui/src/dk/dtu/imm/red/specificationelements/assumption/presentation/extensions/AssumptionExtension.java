package dk.dtu.imm.red.specificationelements.assumption.presentation.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;
import dk.dtu.imm.red.specificationelements.assumption.presentation.editors.AssumptionEditorImpl;
import dk.dtu.imm.red.specificationelements.assumption.presentation.editors.AssumptionEditorInput;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;

public class AssumptionExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element){
		if (element instanceof Assumption) {
			AssumptionEditorInput editorInput =
					new AssumptionEditorInput((Assumption) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, AssumptionEditorImpl.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}

}
