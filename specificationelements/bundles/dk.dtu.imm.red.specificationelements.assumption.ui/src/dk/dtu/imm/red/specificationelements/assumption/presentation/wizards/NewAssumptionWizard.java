package dk.dtu.imm.red.specificationelements.assumption.presentation.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;

public class NewAssumptionWizard extends BaseNewWizard {

	String ID = "dk.dtu.imm.red.specificationelements.assumption.wizards.newassumption";

//	protected ElementBasicInfoPage defineAssumptionPage;

	protected NewAssumptionWizardPresenter presenter;

	public NewAssumptionWizard() {
		super("Assumption", Assumption.class);
		presenter = new NewAssumptionWizardPresenter(this);
//		defineAssumptionPage = new ElementBasicInfoPage("Assumption");
	}

	@Override
	public void addPages() {
//		addPage(defineAssumptionPage);
		super.addPages();
	}

	@Override
	public boolean performFinish() {
		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription();

		Group parent = null;

		if (!displayInfoPage.getSelection().isEmpty()
			&& displayInfoPage.getSelection() instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) displayInfoPage
					.getSelection();

			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}

		presenter.wizardFinished(label, name, description, parent, "");

		return true;
	}

	public void setName(final String text) {
		displayInfoPage.setDisplayName(text);
	}

	@Override
	public boolean canFinish() {
		return super.canFinish()
				&& elementBasicInfoPage.isPageComplete()
				&& displayInfoPage.isPageComplete()
				&& !displayInfoPage.getSelection().isEmpty();
	}
}
