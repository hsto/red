package dk.dtu.imm.red.specificationelements.assumption.presentation.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;
import dk.dtu.imm.red.specificationelements.assumption.AssumptionFactory;
import dk.dtu.imm.red.specificationelements.assumption.presentation.extensions.AssumptionExtension;

public class CreateAssumptionOperation extends AbstractOperation {

	protected String label;
	protected String name;
	protected String description;
	protected Group parent;
//	protected String path;
	protected Assumption assumption;

	protected AssumptionExtension extension;

	public CreateAssumptionOperation(String label, String name,
			String description, Group parent, String path) {
		super("Create Assumption");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
//		this.path = path;

		extension = new AssumptionExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		assumption = AssumptionFactory.eINSTANCE.createAssumption();
		assumption.setCreator(PreferenceUtil.getUserPreference_User());
//		assumption.setAssumptionName(assumptionName);
//		assumption.setName(assumptionName);
		assumption.setLabel(label);
		assumption.setName(name);
		assumption.setDescription(description);
//		assumption.setAssumptionSentence(TextFactory.eINSTANCE.createText(assumptionDescription));

		if (parent != null) {
			assumption.setParent(parent);
		
		} 

		assumption.save();
		extension.openElement(assumption);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if (parent != null) {
			parent.getContents().remove(assumption);
			extension.deleteElement(assumption);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
