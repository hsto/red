package dk.dtu.imm.red.specificationelements.assumption.presentation.editors;

import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;

public class AssumptionPresenterImpl extends BaseEditorPresenter {

private static final int MAX_TITLE_LENGTH = 40;
	
	protected AssumptionEditor assumptionEditor;
	
	public AssumptionPresenterImpl(AssumptionEditor assumptionEditor, Assumption element) {
		super(element);
		this.assumptionEditor = assumptionEditor;
	}

	@Override
	public void save() {
		super.save();
		Assumption assumption = (Assumption) element;
		assumption.save();
	}

	
	public int getTitleMaxLength() {
		return MAX_TITLE_LENGTH;
	}
}
