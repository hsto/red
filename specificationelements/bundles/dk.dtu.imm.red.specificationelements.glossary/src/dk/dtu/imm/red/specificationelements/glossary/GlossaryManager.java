package dk.dtu.imm.red.specificationelements.glossary;

import java.util.List;

import dk.dtu.imm.red.core.element.group.Group;

public interface GlossaryManager {
	/**
	 * Creates a new glossary with the given name, and optionally a parent
	 * group.
	 *
	 * @param name
	 *            the name of the new glossary
	 * @param parent
	 *            an optional parent group
	 * @param glossaryPath
	 * @return the created glossary
	 */
	Glossary createGlossary(String name, Group parent, String glossaryPath);

	/**
	 * Get the list of existing glossaries.
	 *
	 * @return the list of glossaries
	 */
	List<Glossary> getGlossaries();

	/**
	 * Get the list of glossary entries for a given glossary.
	 *
	 * @param glossary
	 *            the glossary for which all entries should be returned
	 * @return the list of entries contained in the given glossary
	 */
	List<GlossaryEntry> getGlossaryEntries(Glossary glossary);

	/**
	 * Add a glossary entry to an existing glossary.
	 *
	 * @param glossary
	 *            the existing glossary to add the entry to
	 * @param entry
	 *            the entry to add to the given glossary
	 */
	void addGlossaryEntry(Glossary glossary, GlossaryEntry entry);

	/**
	 * Add an existing glossary.
	 * @param glossary existing glossary.
	 */
	void addGlossary(Glossary glossary);

	/**
	 * Deletes an existing glossary entry. This removes it from the containing
	 * glossary.
	 *
	 * @param entry
	 *            the glossary entry to be deleted
	 */
	void deleteGlossaryEntry(GlossaryEntry entry);

	/**
	 * Searches for a glossary with the given name, and returns it if found.
	 *
	 * @param name
	 *            the name of the glossary to look for
	 * @return the result of the search - either a glossary, or null
	 */
	Glossary findGlossaryByName(final String name);

	/**
	 * Delete an existing glossary. This removes it from any containing
	 * glossary.
	 *
	 * @param glossary
	 *            the glossary to be deleted
	 */
	void deleteGlossary(Glossary glossary);

	GlossaryEntry createGlossaryEntry(String label, String name, String description, Glossary parent);
	
	void resetGlossaryManger();
}
