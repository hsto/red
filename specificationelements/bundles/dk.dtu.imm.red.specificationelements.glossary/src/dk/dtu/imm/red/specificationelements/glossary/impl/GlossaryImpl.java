/**
 */
package dk.dtu.imm.red.specificationelements.glossary.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.folder.impl.FolderImpl;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryFactory;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Glossary</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.impl.GlossaryImpl#getDiscussion <em>Discussion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GlossaryImpl extends FolderImpl implements Glossary {
	/**
	 * The cached value of the '{@link #getDiscussion() <em>Discussion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscussion()
	 * @generated
	 * @ordered
	 */
	protected Text discussion;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected GlossaryImpl() {
		super();
		
		GlossaryFactory.eINSTANCE.getGlossaryManager().addGlossary(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GlossaryPackage.Literals.GLOSSARY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getDiscussion() {
		if (discussion != null && discussion.eIsProxy()) {
			InternalEObject oldDiscussion = (InternalEObject)discussion;
			discussion = (Text)eResolveProxy(oldDiscussion);
			if (discussion != oldDiscussion) {
				InternalEObject newDiscussion = (InternalEObject)discussion;
				NotificationChain msgs = oldDiscussion.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GlossaryPackage.GLOSSARY__DISCUSSION, null, null);
				if (newDiscussion.eInternalContainer() == null) {
					msgs = newDiscussion.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GlossaryPackage.GLOSSARY__DISCUSSION, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GlossaryPackage.GLOSSARY__DISCUSSION, oldDiscussion, discussion));
			}
		}
		return discussion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetDiscussion() {
		return discussion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscussion(Text newDiscussion, NotificationChain msgs) {
		Text oldDiscussion = discussion;
		discussion = newDiscussion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GlossaryPackage.GLOSSARY__DISCUSSION, oldDiscussion, newDiscussion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscussion(Text newDiscussion) {
		if (newDiscussion != discussion) {
			NotificationChain msgs = null;
			if (discussion != null)
				msgs = ((InternalEObject)discussion).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GlossaryPackage.GLOSSARY__DISCUSSION, null, msgs);
			if (newDiscussion != null)
				msgs = ((InternalEObject)newDiscussion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GlossaryPackage.GLOSSARY__DISCUSSION, null, msgs);
			msgs = basicSetDiscussion(newDiscussion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GlossaryPackage.GLOSSARY__DISCUSSION, newDiscussion, newDiscussion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY__DISCUSSION:
				return basicSetDiscussion(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY__DISCUSSION:
				if (resolve) return getDiscussion();
				return basicGetDiscussion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY__DISCUSSION:
				setDiscussion((Text)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY__DISCUSSION:
				setDiscussion((Text)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY__DISCUSSION:
				return discussion != null;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/glossary.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean isValidContent(final Element content) {
		return (content instanceof Glossary || content instanceof GlossaryEntry);
	}
	
	/**
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean isValidContent(@SuppressWarnings("rawtypes") Class type) {
		return type.isAssignableFrom(Glossary.class) 
				|| type.isAssignableFrom(GlossaryEntry.class);
	}
	
	@Override
	public String getStructeredIdType() {
		return "GL";
	}

} //GlossaryImpl
