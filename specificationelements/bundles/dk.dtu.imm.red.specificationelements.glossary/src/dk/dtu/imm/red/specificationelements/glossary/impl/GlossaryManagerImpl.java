package dk.dtu.imm.red.specificationelements.glossary.impl;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryFactory;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryManager;

/**
 * @generated NOT
 */
public class GlossaryManagerImpl implements GlossaryManager {

	private List<Glossary> glossaries;

	public GlossaryManagerImpl() {
		glossaries = new ArrayList<Glossary>();
	}

	@Override
	public Glossary createGlossary(String name, Group parent, String path) {
		Glossary glossary = GlossaryFactory.eINSTANCE.createGlossary();
		glossary.setName(name);
		glossary.setParent(parent);
		addGlossary(glossary);

		glossary.save();

		return glossary;
	}

	@Override
	public List<Glossary> getGlossaries() {
		return new ArrayList<Glossary>(glossaries);
	}

	@Override
	public List<GlossaryEntry> getGlossaryEntries(final Glossary glossary) {
		List<GlossaryEntry> entries = new ArrayList<GlossaryEntry>();
		for (Element element : glossary.getContents()) {
			if (element instanceof GlossaryEntry) {
				entries.add((GlossaryEntry) element);
			}
		}

		return entries;
	}

	@Override
	public void addGlossaryEntry(final Glossary glossary,
			final GlossaryEntry entry) {
		glossary.getContents().add(entry);
		glossary.save();
	}

	/**
	 * Find a glossary by a given name.
	 *
	 * @param name
	 *            The name of the glossary
	 * @return The glossary, if it exists. Otherwise null
	 */
	@Override
	public Glossary findGlossaryByName(final String name) {
		for (Glossary glossary : glossaries) {
			if (glossary.getName().equals(name)) {
				return glossary;
			}
		}
		return null;
	}

	@Override
	public void deleteGlossary(final Glossary glossary) {
		if (glossary.getParent() != null) {
			glossary.getParent().getContents().remove(glossary);
			glossary.getParent().save();
		}
		glossaries.remove(glossary);
	}

	@Override
	public void deleteGlossaryEntry(final GlossaryEntry entry) {
		if (entry != null && entry.getParent() != null) {
			Group parent = entry.getParent();
			parent.getContents().remove(entry);
			parent.save();
		}
	}

	@Override
	public GlossaryEntry createGlossaryEntry(String label, String name, String description,
			Glossary parent){
		GlossaryEntry entry = GlossaryFactory.eINSTANCE.createGlossaryEntry();
		entry.setLabel(label);
		entry.setName(name);
		entry.setDescription(description);
//		entry.setTerm(term);
//		entry.setDefinition(definition);

		addGlossaryEntry(parent, entry);

		entry.save();

		return entry;
	}

	@Override
	public void addGlossary(final Glossary glossary) {
		if (!glossaries.contains(glossary)) {
			glossaries.add(glossary);
		}
	}
	
	@Override
	public void resetGlossaryManger() {
		if(!glossaries.isEmpty()) {
			glossaries.clear();
		}
	}

}