/**
 */
package dk.dtu.imm.red.specificationelements.glossary;

import dk.dtu.imm.red.core.folder.Folder;

import dk.dtu.imm.red.core.text.Text;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Glossary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.Glossary#getDiscussion <em>Discussion</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossary()
 * @model
 * @generated
 */
public interface Glossary extends Folder {
	/**
	 * Returns the value of the '<em><b>Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discussion</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discussion</em>' containment reference.
	 * @see #setDiscussion(Text)
	 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossary_Discussion()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getDiscussion();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.glossary.Glossary#getDiscussion <em>Discussion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discussion</em>' containment reference.
	 * @see #getDiscussion()
	 * @generated
	 */
	void setDiscussion(Text value);

} // Glossary
