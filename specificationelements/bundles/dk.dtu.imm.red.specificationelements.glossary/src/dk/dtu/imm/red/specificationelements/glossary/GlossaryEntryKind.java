/**
 */
package dk.dtu.imm.red.specificationelements.glossary;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Entry Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossaryEntryKind()
 * @model
 * @generated
 */
public enum GlossaryEntryKind implements Enumerator {
	/**
	 * The '<em><b>Unspecified</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNSPECIFIED_VALUE
	 * @generated
	 * @ordered
	 */
	UNSPECIFIED(0, "unspecified", "unspecified"),

	/**
	 * The '<em><b>Domain term</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOMAIN_TERM_VALUE
	 * @generated
	 * @ordered
	 */
	DOMAIN_TERM(1, "domain_term", "domain_term"),

	/**
	 * The '<em><b>Technology term</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TECHNOLOGY_TERM_VALUE
	 * @generated
	 * @ordered
	 */
	TECHNOLOGY_TERM(2, "technology_term", "technology_term"),

	/**
	 * The '<em><b>Legal term</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LEGAL_TERM_VALUE
	 * @generated
	 * @ordered
	 */
	LEGAL_TERM(3, "legal_term", "legal_term"),

	/**
	 * The '<em><b>Technical system</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TECHNICAL_SYSTEM_VALUE
	 * @generated
	 * @ordered
	 */
	TECHNICAL_SYSTEM(4, "technical_system", "technical_system"),

	/**
	 * The '<em><b>Proper name</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROPER_NAME_VALUE
	 * @generated
	 * @ordered
	 */
	PROPER_NAME(5, "proper_name", "proper_name"),

	/**
	 * The '<em><b>Activity or process</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACTIVITY_OR_PROCESS_VALUE
	 * @generated
	 * @ordered
	 */
	ACTIVITY_OR_PROCESS(6, "activity_or_process", "activity_or_process"),

	/**
	 * The '<em><b>Domain entity</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOMAIN_ENTITY_VALUE
	 * @generated
	 * @ordered
	 */
	DOMAIN_ENTITY(7, "domain_entity", "domain_entity");

	/**
	 * The '<em><b>Unspecified</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unspecified</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNSPECIFIED
	 * @model name="unspecified"
	 * @generated
	 * @ordered
	 */
	public static final int UNSPECIFIED_VALUE = 0;

	/**
	 * The '<em><b>Domain term</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Domain term</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOMAIN_TERM
	 * @model name="domain_term"
	 * @generated
	 * @ordered
	 */
	public static final int DOMAIN_TERM_VALUE = 1;

	/**
	 * The '<em><b>Technology term</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Technology term</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TECHNOLOGY_TERM
	 * @model name="technology_term"
	 * @generated
	 * @ordered
	 */
	public static final int TECHNOLOGY_TERM_VALUE = 2;

	/**
	 * The '<em><b>Legal term</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Legal term</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LEGAL_TERM
	 * @model name="legal_term"
	 * @generated
	 * @ordered
	 */
	public static final int LEGAL_TERM_VALUE = 3;

	/**
	 * The '<em><b>Technical system</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Technical system</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TECHNICAL_SYSTEM
	 * @model name="technical_system"
	 * @generated
	 * @ordered
	 */
	public static final int TECHNICAL_SYSTEM_VALUE = 4;

	/**
	 * The '<em><b>Proper name</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Proper name</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PROPER_NAME
	 * @model name="proper_name"
	 * @generated
	 * @ordered
	 */
	public static final int PROPER_NAME_VALUE = 5;

	/**
	 * The '<em><b>Activity or process</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Activity or process</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACTIVITY_OR_PROCESS
	 * @model name="activity_or_process"
	 * @generated
	 * @ordered
	 */
	public static final int ACTIVITY_OR_PROCESS_VALUE = 6;

	/**
	 * The '<em><b>Domain entity</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Domain entity</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOMAIN_ENTITY
	 * @model name="domain_entity"
	 * @generated
	 * @ordered
	 */
	public static final int DOMAIN_ENTITY_VALUE = 7;

	/**
	 * An array of all the '<em><b>Entry Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final GlossaryEntryKind[] VALUES_ARRAY =
		new GlossaryEntryKind[] {
			UNSPECIFIED,
			DOMAIN_TERM,
			TECHNOLOGY_TERM,
			LEGAL_TERM,
			TECHNICAL_SYSTEM,
			PROPER_NAME,
			ACTIVITY_OR_PROCESS,
			DOMAIN_ENTITY,
		};

	/**
	 * A public read-only list of all the '<em><b>Entry Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<GlossaryEntryKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Entry Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GlossaryEntryKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			GlossaryEntryKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Entry Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GlossaryEntryKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			GlossaryEntryKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Entry Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GlossaryEntryKind get(int value) {
		switch (value) {
			case UNSPECIFIED_VALUE: return UNSPECIFIED;
			case DOMAIN_TERM_VALUE: return DOMAIN_TERM;
			case TECHNOLOGY_TERM_VALUE: return TECHNOLOGY_TERM;
			case LEGAL_TERM_VALUE: return LEGAL_TERM;
			case TECHNICAL_SYSTEM_VALUE: return TECHNICAL_SYSTEM;
			case PROPER_NAME_VALUE: return PROPER_NAME;
			case ACTIVITY_OR_PROCESS_VALUE: return ACTIVITY_OR_PROCESS;
			case DOMAIN_ENTITY_VALUE: return DOMAIN_ENTITY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private GlossaryEntryKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //GlossaryEntryKind
