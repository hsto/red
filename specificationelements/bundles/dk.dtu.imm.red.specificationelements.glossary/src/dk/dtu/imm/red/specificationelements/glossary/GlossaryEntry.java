/**
 */
package dk.dtu.imm.red.specificationelements.glossary;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.text.Text;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry#getSynonyms <em>Synonyms</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry#getAntonyms <em>Antonyms</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry#getExamples <em>Examples</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry#getDiscussion <em>Discussion</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry#getPartOf <em>Part Of</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry#getKindOf <em>Kind Of</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossaryEntry()
 * @model
 * @generated
 */
public interface GlossaryEntry extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Synonyms</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synonyms</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synonyms</em>' attribute list.
	 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossaryEntry_Synonyms()
	 * @model
	 * @generated
	 */
	EList<String> getSynonyms();

	/**
	 * Returns the value of the '<em><b>Antonyms</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Antonyms</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Antonyms</em>' attribute list.
	 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossaryEntry_Antonyms()
	 * @model
	 * @generated
	 */
	EList<String> getAntonyms();

	/**
	 * Returns the value of the '<em><b>Examples</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Examples</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Examples</em>' attribute list.
	 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossaryEntry_Examples()
	 * @model
	 * @generated
	 */
	EList<String> getExamples();

	/**
	 * Returns the value of the '<em><b>Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discussion</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discussion</em>' containment reference.
	 * @see #setDiscussion(Text)
	 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossaryEntry_Discussion()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getDiscussion();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry#getDiscussion <em>Discussion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discussion</em>' containment reference.
	 * @see #getDiscussion()
	 * @generated
	 */
	void setDiscussion(Text value);

	/**
	 * Returns the value of the '<em><b>Part Of</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part Of</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part Of</em>' reference.
	 * @see #setPartOf(Element)
	 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossaryEntry_PartOf()
	 * @model
	 * @generated
	 */
	Element getPartOf();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry#getPartOf <em>Part Of</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Part Of</em>' reference.
	 * @see #getPartOf()
	 * @generated
	 */
	void setPartOf(Element value);

	/**
	 * Returns the value of the '<em><b>Kind Of</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind Of</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind Of</em>' reference.
	 * @see #setKindOf(Element)
	 * @see dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage#getGlossaryEntry_KindOf()
	 * @model
	 * @generated
	 */
	Element getKindOf();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry#getKindOf <em>Kind Of</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind Of</em>' reference.
	 * @see #getKindOf()
	 * @generated
	 */
	void setKindOf(Element value);

} // GlossaryEntry
