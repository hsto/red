/**
 */
package dk.dtu.imm.red.specificationelements.glossary.impl;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.text.Text;

import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage;

import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.impl.GlossaryEntryImpl#getSynonyms <em>Synonyms</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.impl.GlossaryEntryImpl#getAntonyms <em>Antonyms</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.impl.GlossaryEntryImpl#getExamples <em>Examples</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.impl.GlossaryEntryImpl#getDiscussion <em>Discussion</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.impl.GlossaryEntryImpl#getPartOf <em>Part Of</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.glossary.impl.GlossaryEntryImpl#getKindOf <em>Kind Of</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GlossaryEntryImpl extends SpecificationElementImpl implements GlossaryEntry {
	/**
	 * The cached value of the '{@link #getSynonyms() <em>Synonyms</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSynonyms()
	 * @generated
	 * @ordered
	 */
	protected EList<String> synonyms;

	/**
	 * The cached value of the '{@link #getAntonyms() <em>Antonyms</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAntonyms()
	 * @generated
	 * @ordered
	 */
	protected EList<String> antonyms;

	/**
	 * The cached value of the '{@link #getExamples() <em>Examples</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExamples()
	 * @generated
	 * @ordered
	 */
	protected EList<String> examples;

	/**
	 * The cached value of the '{@link #getDiscussion() <em>Discussion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscussion()
	 * @generated
	 * @ordered
	 */
	protected Text discussion;

	/**
	 * The cached value of the '{@link #getPartOf() <em>Part Of</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartOf()
	 * @generated
	 * @ordered
	 */
	protected Element partOf;

	/**
	 * The cached value of the '{@link #getKindOf() <em>Kind Of</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindOf()
	 * @generated
	 * @ordered
	 */
	protected Element kindOf;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GlossaryEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GlossaryPackage.Literals.GLOSSARY_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSynonyms() {
		if (synonyms == null) {
			synonyms = new EDataTypeUniqueEList<String>(String.class, this, GlossaryPackage.GLOSSARY_ENTRY__SYNONYMS);
		}
		return synonyms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getAntonyms() {
		if (antonyms == null) {
			antonyms = new EDataTypeUniqueEList<String>(String.class, this, GlossaryPackage.GLOSSARY_ENTRY__ANTONYMS);
		}
		return antonyms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getExamples() {
		if (examples == null) {
			examples = new EDataTypeUniqueEList<String>(String.class, this, GlossaryPackage.GLOSSARY_ENTRY__EXAMPLES);
		}
		return examples;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getDiscussion() {
		if (discussion != null && discussion.eIsProxy()) {
			InternalEObject oldDiscussion = (InternalEObject)discussion;
			discussion = (Text)eResolveProxy(oldDiscussion);
			if (discussion != oldDiscussion) {
				InternalEObject newDiscussion = (InternalEObject)discussion;
				NotificationChain msgs = oldDiscussion.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION, null, null);
				if (newDiscussion.eInternalContainer() == null) {
					msgs = newDiscussion.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION, oldDiscussion, discussion));
			}
		}
		return discussion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetDiscussion() {
		return discussion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscussion(Text newDiscussion, NotificationChain msgs) {
		Text oldDiscussion = discussion;
		discussion = newDiscussion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION, oldDiscussion, newDiscussion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscussion(Text newDiscussion) {
		if (newDiscussion != discussion) {
			NotificationChain msgs = null;
			if (discussion != null)
				msgs = ((InternalEObject)discussion).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION, null, msgs);
			if (newDiscussion != null)
				msgs = ((InternalEObject)newDiscussion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION, null, msgs);
			msgs = basicSetDiscussion(newDiscussion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION, newDiscussion, newDiscussion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element getPartOf() {
		if (partOf != null && partOf.eIsProxy()) {
			InternalEObject oldPartOf = (InternalEObject)partOf;
			partOf = (Element)eResolveProxy(oldPartOf);
			if (partOf != oldPartOf) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GlossaryPackage.GLOSSARY_ENTRY__PART_OF, oldPartOf, partOf));
			}
		}
		return partOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element basicGetPartOf() {
		return partOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartOf(Element newPartOf) {
		Element oldPartOf = partOf;
		partOf = newPartOf;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GlossaryPackage.GLOSSARY_ENTRY__PART_OF, oldPartOf, partOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element getKindOf() {
		if (kindOf != null && kindOf.eIsProxy()) {
			InternalEObject oldKindOf = (InternalEObject)kindOf;
			kindOf = (Element)eResolveProxy(oldKindOf);
			if (kindOf != oldKindOf) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GlossaryPackage.GLOSSARY_ENTRY__KIND_OF, oldKindOf, kindOf));
			}
		}
		return kindOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element basicGetKindOf() {
		return kindOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKindOf(Element newKindOf) {
		Element oldKindOf = kindOf;
		kindOf = newKindOf;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GlossaryPackage.GLOSSARY_ENTRY__KIND_OF, oldKindOf, kindOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION:
				return basicSetDiscussion(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY_ENTRY__SYNONYMS:
				return getSynonyms();
			case GlossaryPackage.GLOSSARY_ENTRY__ANTONYMS:
				return getAntonyms();
			case GlossaryPackage.GLOSSARY_ENTRY__EXAMPLES:
				return getExamples();
			case GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION:
				if (resolve) return getDiscussion();
				return basicGetDiscussion();
			case GlossaryPackage.GLOSSARY_ENTRY__PART_OF:
				if (resolve) return getPartOf();
				return basicGetPartOf();
			case GlossaryPackage.GLOSSARY_ENTRY__KIND_OF:
				if (resolve) return getKindOf();
				return basicGetKindOf();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY_ENTRY__SYNONYMS:
				getSynonyms().clear();
				getSynonyms().addAll((Collection<? extends String>)newValue);
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__ANTONYMS:
				getAntonyms().clear();
				getAntonyms().addAll((Collection<? extends String>)newValue);
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__EXAMPLES:
				getExamples().clear();
				getExamples().addAll((Collection<? extends String>)newValue);
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION:
				setDiscussion((Text)newValue);
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__PART_OF:
				setPartOf((Element)newValue);
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__KIND_OF:
				setKindOf((Element)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY_ENTRY__SYNONYMS:
				getSynonyms().clear();
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__ANTONYMS:
				getAntonyms().clear();
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__EXAMPLES:
				getExamples().clear();
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION:
				setDiscussion((Text)null);
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__PART_OF:
				setPartOf((Element)null);
				return;
			case GlossaryPackage.GLOSSARY_ENTRY__KIND_OF:
				setKindOf((Element)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GlossaryPackage.GLOSSARY_ENTRY__SYNONYMS:
				return synonyms != null && !synonyms.isEmpty();
			case GlossaryPackage.GLOSSARY_ENTRY__ANTONYMS:
				return antonyms != null && !antonyms.isEmpty();
			case GlossaryPackage.GLOSSARY_ENTRY__EXAMPLES:
				return examples != null && !examples.isEmpty();
			case GlossaryPackage.GLOSSARY_ENTRY__DISCUSSION:
				return discussion != null;
			case GlossaryPackage.GLOSSARY_ENTRY__PART_OF:
				return partOf != null;
			case GlossaryPackage.GLOSSARY_ENTRY__KIND_OF:
				return kindOf != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (synonyms: ");
		result.append(synonyms);
		result.append(", antonyms: ");
		result.append(antonyms);
		result.append(", examples: ");
		result.append(examples);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/glossaryentry.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		attributesToSearch.put(GlossaryPackage.Literals.GLOSSARY_ENTRY__DISCUSSION,
				this.getDiscussion()==null ? "" : this.getDiscussion().toString());
		
		if(this.getPartOf()!=null){
			attributesToSearch.put(GlossaryPackage.Literals.GLOSSARY_ENTRY__PART_OF,
				this.getPartOf().getLabel());
		}
		if(this.getKindOf()!=null){
			attributesToSearch.put(GlossaryPackage.Literals.GLOSSARY_ENTRY__KIND_OF,
				this.getKindOf().getLabel());
		}
		
		return super.search(param, attributesToSearch);
	}
	
	@Override
	public String getStructeredIdType() {
		return "GE";
	}

} //GlossaryEntryImpl
