package dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.impl;

import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.StakeholderPresenter;

public class StakeholderPresenterImpl extends BaseEditorPresenter implements
		StakeholderPresenter {

	protected StakeholderEditorImpl stakeholderEditorImpl;
	
//	protected String stakeholderName;
	protected String engagement;
	protected String stake;
	protected String longDescription;
//	protected String kind;
	protected int exposure;
	protected int urgency;
	protected int power;
	protected int importance;

	public StakeholderPresenterImpl(StakeholderEditorImpl stakeholderEditorImpl, Stakeholder element) {
		super(element);
		this.stakeholderEditorImpl = stakeholderEditorImpl;
		this.element = element;
	}

	@Override
	public int getNameMaxLength() {
		return 40;
	}

	@Override
	public void save() {
		super.save();
		Stakeholder stakeholder = (Stakeholder) element;
		
//		stakeholder.setName(stakeholderName);
		stakeholder.setEngagement(
				TextFactory.eINSTANCE.createText(engagement));
		stakeholder.setStake(TextFactory.eINSTANCE.createText(stake));
		stakeholder.setLongDescription(
				TextFactory.eINSTANCE.createText(longDescription));
		stakeholder.setExposure(exposure);
		stakeholder.setImportance(importance);
		stakeholder.setPower(power);
		stakeholder.setUrgency(urgency);
//		stakeholder.setElementKind(kind);
		stakeholder.save();
		
	}

	@Override
	public void setEngagement(String text){
		this.engagement = text;
	}
	
	@Override
	public void setStake(String text){
		this.stake = text;
	}
	
//	@Override
//	public void setStakeholder(String name){
//		this.stakeholderName = name;
//	}
//	
	@Override
	public void setLongDescription(String text){
		this.longDescription = text;
	}

//	@Override
//	public void setKind(String item) {
//		this.kind = item;
//	}

	@Override
	public void setExposure(int item) {
		this.exposure = item;
	}

	@Override
	public void setPower(int item) {
		this.power = item;
	}

	@Override
	public void setUrgency(int item) {
		this.urgency = item;
	}

	@Override
	public void setImportance(int item) {
		this.importance = item;
	}
	
	
}
