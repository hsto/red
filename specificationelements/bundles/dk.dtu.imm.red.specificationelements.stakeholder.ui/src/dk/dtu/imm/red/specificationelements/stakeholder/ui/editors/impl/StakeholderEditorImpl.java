package dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.stakeholder.StakeholderKind;
import dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.StakeholderEditor;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.StakeholderPresenter;

/**
 * The editor for creating and editing a stakeholder
 *
 * @author Anders Friis
 *
 */
public class StakeholderEditorImpl extends BaseEditor implements
		StakeholderEditor {

	protected ElementBasicInfoContainer basicInfoContainer;

//	/**
//	 * The label for the name text box.
//	 */
//	Label nameLabel;
//
//	/**
//	 * The text box for the name of the stakeholder.
//	 */
//	Text nameTextBox;
//
//	/**
//	 * The label for the type combo box.
//	 */
//	Label typeLabel;
//
//	/**
//	 * The combo box for type.
//	 */
//	Combo typeComboBox;

	/**
	 * The label for the exposure combo box.
	 */
	Label exposureLabel;

	/**
	 * The combo box for exposure.
	 */
	Combo exposureComboBox;

	/**
	 * The label for the power combo box.
	 */
	Label powerLabel;

	/**
	 * The combo box for power.
	 */
	Combo powerComboBox;

	/**
	 * The label for the urgency combo box.
	 */
	Label urgencyLabel;

	/**
	 * The combo box for urgency.
	 */
	Combo urgencyComboBox;

	/**
	 * The label for the importance combo box.
	 */
	Label importanceLabel;

	/**
	 * The rich text editor for description.
	 */
	RichTextEditor longDescriptionEditor;

	/**
	 * The rich text editor for stake.
	 */
	RichTextEditor stakeEditor;

	/**
	 * The rich text editor for engagement.
	 */
	protected RichTextEditor engagementEditor;

	@Override
	protected void fillInitialData() {
		Stakeholder stakeholder = (Stakeholder) element;

		StakeholderKind[] kinds = StakeholderKind.values();
		String[] kindStrings = new String[kinds.length];
		for(int i=0;i<kinds.length;i++){
			kindStrings[i] = kinds[i].getName();
		}

		basicInfoContainer.fillInitialData(stakeholder, kindStrings);



		if (stakeholder.getLongDescription() != null) {
			longDescriptionEditor.setText(stakeholder.getLongDescription().toString());
		}

		if (stakeholder.getStake() != null) {
			stakeEditor.setText(stakeholder.getStake().toString());
		}

		if (stakeholder.getEngagement() != null) {
			engagementEditor.setText(stakeholder.getEngagement().toString());
		}

		exposureComboBox.select(stakeholder.getExposure() - 1);
		powerComboBox.select(stakeholder.getPower() - 1);
		urgencyComboBox.select(stakeholder.getUrgency() - 1);

		String importanceString = "";
		for (int i = 0; i < stakeholder.getImportance(); i++) {
			importanceString += "*";
		}

		importanceResultLabel.setText(importanceString);

		isDirty = false;
		firePropertyChange(PROP_DIRTY);

	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		StakeholderPresenter presenter =
				(StakeholderPresenter) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
//		presenter.setStakeholder(nameTextBox.getText());
//		presenter.setType(typeComboBox.getItem(
//				typeComboBox.getSelectionIndex()));
		presenter.setExposure(exposureComboBox.getSelectionIndex() + 1);
		presenter.setPower(powerComboBox.getSelectionIndex() + 1);
		presenter.setUrgency(urgencyComboBox.getSelectionIndex() + 1);
		presenter.setImportance(importanceResultLabel.getText().length());
		presenter.setLongDescription(longDescriptionEditor.getText());
		presenter.setStake(stakeEditor.getText());
		presenter.setEngagement(engagementEditor.getText());

		super.doSave(monitor);
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	private Listener comboListener = new Listener() {
		public void handleEvent(Event event) {
			// this is nasty, but we're using indeces, not values
			// as value = index + 1 -> we need to increase the index value by 1
			int power = powerComboBox.getSelectionIndex() + 1;
			int urgency = urgencyComboBox.getSelectionIndex() + 1;

			long importance =
					Math.round((double) (power + urgency) / 2d);
			String importanceString = "";
			for (int i = 0; i < importance; i++) {
				importanceString += "*";
			}

			importanceResultLabel.setText(importanceString);
		};
	};

	private Label importanceResultLabel;

	/**
	 * Index of the stakeholder page.
	 */
	protected int stakeholderPageIndex = 0;

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);

		presenter = new StakeholderPresenterImpl(this, (Stakeholder) element);
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void createPages() {
		stakeholderPageIndex = addPage(createStakeholderPage(getContainer()));
		setPageText(stakeholderPageIndex, "Summary");
		super.createPages();

		fillInitialData();
	}

	private Composite createStakeholderPage(final Composite parent) {

		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (SpecificationElement) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);

		GridData informationGridData = new GridData();
		informationGridData.grabExcessHorizontalSpace = true;
		informationGridData.horizontalAlignment = SWT.FILL;

		Composite informationContainer = createStakeholderInformationContainer(
				composite);
		informationContainer.setLayoutData(informationGridData);


		GridData descriptionGridData = new GridData();
		descriptionGridData.heightHint = 200;
		descriptionGridData.widthHint = 700;
		descriptionGridData.grabExcessHorizontalSpace = true;
		descriptionGridData.horizontalSpan = 2;
		descriptionGridData.horizontalAlignment = SWT.FILL;

		Composite descriptionContainer = createStakeholderDescriptionContainer(
				composite);
		descriptionContainer.setLayoutData(descriptionGridData);

		GridData stakeGridData = new GridData();
		stakeGridData.heightHint = 200;
		stakeGridData.widthHint = 700;
		stakeGridData.grabExcessHorizontalSpace = true;
		stakeGridData.horizontalSpan = 2;
		stakeGridData.horizontalAlignment = SWT.FILL;

		Composite stakeContainer = createStakeholderStakeContainer(composite);
		stakeContainer.setLayoutData(stakeGridData);

		GridData engagementGridData = new GridData();
		engagementGridData.heightHint = 200;
		engagementGridData.widthHint = 700;
		engagementGridData.grabExcessHorizontalSpace = true;
		engagementGridData.horizontalSpan = 2;
		engagementGridData.horizontalAlignment = SWT.FILL;

		Composite engagementContainer =
				createStakeholderEngagementContainer(composite);
		engagementContainer.setLayoutData(engagementGridData);

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	private Composite createStakeholderInformationContainer(
			final Composite parent) {


		Group container = new Group(parent, SWT.NONE);
		container.setText("Classification");

		GridLayout stakeholderInformationLayout = new GridLayout(4, true);
		stakeholderInformationLayout.marginBottom = 3;
		container.setLayout(stakeholderInformationLayout);

		GridData fieldGridData = new GridData();
		fieldGridData.widthHint = 80;

		GridData textGridData = new GridData();
		textGridData.horizontalSpan = 4;
		textGridData.widthHint = 300;

//		nameLabel = new Label(container, SWT.NONE);
//		nameLabel.setText("Stakeholder");
//
//		nameTextBox = new Text(container, SWT.BORDER);
//		nameTextBox.setTextLimit(((StakeholderPresenter) presenter)
//				.getNameMaxLength());
//		nameTextBox.addListener(SWT.Modify, modifyListener);
//		nameTextBox.setLayoutData(textGridData);
//
//		typeLabel = new Label(container, SWT.NONE);
//		typeLabel.setText("Type");
//		typeLabel.setLayoutData(fieldGridData);


		exposureLabel = new Label(container, SWT.NONE);
		exposureLabel.setText("Exposure");

		powerLabel = new Label(container, SWT.NONE);
		powerLabel.setText("Power");

		urgencyLabel = new Label(container, SWT.NONE);
		urgencyLabel.setText("Urgency");

		importanceLabel = new Label(container, SWT.NONE);
		importanceLabel.setText("Importance");


//		typeComboBox = new Combo(container, SWT.READ_ONLY);
//		typeComboBox.add("Internal");
//		typeComboBox.add("External");
//		typeComboBox.addListener(SWT.Modify, modifyListener);
//		typeComboBox.setLayoutData(fieldGridData);

		exposureComboBox = new Combo(container, SWT.READ_ONLY);
		exposureComboBox.add("*");
		exposureComboBox.add("**");
		exposureComboBox.add("***");
		exposureComboBox.addListener(SWT.Selection, comboListener);
		exposureComboBox.addListener(SWT.Modify, modifyListener);
		exposureComboBox.setLayoutData(fieldGridData);


		powerComboBox = new Combo(container, SWT.READ_ONLY);
		powerComboBox.add("*");
		powerComboBox.add("**");
		powerComboBox.add("***");
		powerComboBox.addListener(SWT.Selection, comboListener);
		powerComboBox.addListener(SWT.Modify, modifyListener);
		powerComboBox.setLayoutData(fieldGridData);

		urgencyComboBox = new Combo(container, SWT.READ_ONLY);
		urgencyComboBox.add("*");
		urgencyComboBox.add("**");
		urgencyComboBox.add("***");
		urgencyComboBox.addListener(SWT.Selection, comboListener);
		urgencyComboBox.addListener(SWT.Modify, modifyListener);
		urgencyComboBox.setLayoutData(fieldGridData);

		importanceResultLabel = new Label(container, SWT.BORDER);
		importanceResultLabel.setLayoutData(fieldGridData);

		return container;
	}

	private Composite createStakeholderDescriptionContainer(
			final Composite parent) {
		Group container = new Group(parent, SWT.NONE);
		container.setText("Discussion");

		GridLayout layout = new GridLayout();
		container.setLayout(layout);

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.grabExcessVerticalSpace = true;

		longDescriptionEditor = new RichTextEditor(container, SWT.BORDER,
				getEditorSite(), commandListener);
		longDescriptionEditor.setLayoutData(editorLayoutData);

		longDescriptionEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		return container;
	}

	private Composite createStakeholderStakeContainer(final Composite parent) {
		Group container = new Group(parent, SWT.NONE);
		container.setText("Stake");

		GridLayout layout = new GridLayout();
		container.setLayout(layout);

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.grabExcessVerticalSpace = true;

		stakeEditor = new RichTextEditor(container, SWT.BORDER, getEditorSite(),
				commandListener);
		stakeEditor.setLayoutData(editorLayoutData);

		stakeEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		return container;
	}

	private Composite createStakeholderEngagementContainer(
			final Composite parent) {
		Group container = new Group(parent, SWT.NONE);
		container.setText("Engagement");
		GridLayout layout = new GridLayout();
		container.setLayout(layout);

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.grabExcessVerticalSpace = true;

		engagementEditor = new RichTextEditor(container, SWT.BORDER,
				getEditorSite(), commandListener);
		engagementEditor.setLayoutData(editorLayoutData);

		engagementEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		return container;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Stakeholder.html";
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {
		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		// put all the attributes which we want to search through
		// into the attributesToSearchMap
		super.addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);

		attributesToSearch.put(StakeholderPackage.Literals.STAKEHOLDER__LONG_DESCRIPTION,
				longDescriptionEditor.getText());
		attributesToSearch.put(StakeholderPackage.Literals.STAKEHOLDER__ENGAGEMENT,
				engagementEditor.getText());
		attributesToSearch.put(StakeholderPackage.Literals.STAKEHOLDER__STAKE,
				stakeEditor.getText());


		// Now, search through all attributes, recording all search hits along the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()-1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(), result.getEndIndex());
			}
		}
		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}

	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,stakeholderPageIndex,basicInfoContainer);

		if (feature == StakeholderPackage.Literals.STAKEHOLDER__LONG_DESCRIPTION) {
			setActivePage(stakeholderPageIndex);
			longDescriptionEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] {"" + startIndex, "" + endIndex});
		} else if (feature == StakeholderPackage.Literals.STAKEHOLDER__ENGAGEMENT) {
			setActivePage(stakeholderPageIndex);
			engagementEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] {"" + startIndex, "" + endIndex});
		} else if (feature == StakeholderPackage.Literals.STAKEHOLDER__STAKE) {
			setActivePage(stakeholderPageIndex);
			stakeEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] {"" + startIndex, "" + endIndex});
		}
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.stakeholder.stakeholdereditor";
	}

}
