package dk.dtu.imm.red.specificationelements.stakeholder.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.stakeholder.StakeholderFactory;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.extensions.StakeholderExtension;

public class CreateStakeholderOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected Group parent;
	protected Stakeholder stakeholder;

	protected StakeholderExtension extension;

	public CreateStakeholderOperation(String label, String name, String description, Group parent, String path) {
		super("Create Stakeholder");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;

		extension = new StakeholderExtension();
	}
	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}
	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		stakeholder = StakeholderFactory.eINSTANCE.createStakeholder();
		stakeholder.setCreator(PreferenceUtil.getUserPreference_User());


		stakeholder.setLabel(label);
		stakeholder.setName(name);
		stakeholder.setDescription(description);

		if (parent != null) {
			stakeholder.setParent(parent);
		}

		stakeholder.save();
		extension.openElement(stakeholder);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(stakeholder);
			extension.deleteElement(stakeholder);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
