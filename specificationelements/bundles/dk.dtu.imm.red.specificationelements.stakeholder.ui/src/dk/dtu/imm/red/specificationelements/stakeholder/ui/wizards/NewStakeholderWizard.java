package dk.dtu.imm.red.specificationelements.stakeholder.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewStakeholderWizard extends IBaseWizard{

	String ID = "dk.dtu.imm.red.specificationelements.stakeholder.newwizard";
	
	void setStakeholderName(String text);
}
