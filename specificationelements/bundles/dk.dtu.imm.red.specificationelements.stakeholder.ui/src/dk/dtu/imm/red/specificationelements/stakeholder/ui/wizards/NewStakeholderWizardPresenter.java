package dk.dtu.imm.red.specificationelements.stakeholder.ui.wizards;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;

public interface NewStakeholderWizardPresenter extends IWizardPresenter{

	void wizardFinished(String label, String name, String description, Group parent, String path);
}
