package dk.dtu.imm.red.specificationelements.stakeholder.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.StakeholderEditor;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.impl.StakeholderEditorInputImpl;

public class StakeholderExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element){
		if (element instanceof Stakeholder) {
			StakeholderEditorInputImpl editorInput =
					new StakeholderEditorInputImpl((Stakeholder) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, StakeholderEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
