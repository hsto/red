package dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;

public class StakeholderEditorInputImpl extends BaseEditorInput {

	public StakeholderEditorInputImpl(Stakeholder stakeholder) {
		super(stakeholder);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Stakeholder Editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for stakeholder " + element.getName();
	}
}
