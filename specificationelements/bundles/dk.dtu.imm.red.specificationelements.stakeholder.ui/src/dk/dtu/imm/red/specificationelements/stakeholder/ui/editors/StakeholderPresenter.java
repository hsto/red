package dk.dtu.imm.red.specificationelements.stakeholder.ui.editors;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;

public interface StakeholderPresenter extends IEditorPresenter {

	int getNameMaxLength();

//	void setStakeholder(String text);

//	void setKind(String item);

	void setExposure(int item);

	void setPower(int item);

	void setUrgency(int item);

	void setImportance(int text);

	void setLongDescription(String text);

	void setStake(String text);

	void setEngagement(String text);

}
