package dk.dtu.imm.red.specificationelements.stakeholder.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.wizards.NewStakeholderWizard;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.wizards.NewStakeholderWizardPresenter;

public class NewStakeholderWizardImpl extends BaseNewWizard 
	implements NewStakeholderWizard {

//	protected ElementBasicInfoPage defineStakeholderPage;
	
	protected NewStakeholderWizardPresenter presenter;
	
	public NewStakeholderWizardImpl(){
		super("Stakeholder", Stakeholder.class);
		presenter = new NewStakeholderWizardPresenterImpl(this);
//		defineStakeholderPage = new ElementBasicInfoPage("Stakeholder");
	}
	
	@Override
	public void addPages() {
//		addPage(defineStakeholderPage);
		super.addPages();
	}

	@Override
	public boolean performFinish() {
		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription(); 

		Group parent = null;

		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();

			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}
		
		presenter.wizardFinished(label, name, description, parent, "");

		return true;
	}

	@Override
	public void setStakeholderName(final String text) {
		displayInfoPage.setDisplayName(text);
		
	}
	
	@Override
	public boolean canFinish() {
		return super.canFinish() 
				&& elementBasicInfoPage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}
}
