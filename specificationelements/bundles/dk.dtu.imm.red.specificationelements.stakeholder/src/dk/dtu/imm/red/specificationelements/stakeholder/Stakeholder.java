/**
 */
package dk.dtu.imm.red.specificationelements.stakeholder;

import dk.dtu.imm.red.core.text.Text;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stakeholder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getExposure <em>Exposure</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getPower <em>Power</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getUrgency <em>Urgency</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getImportance <em>Importance</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getStake <em>Stake</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getEngagement <em>Engagement</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getLongDescription <em>Long Description</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage#getStakeholder()
 * @model
 * @generated
 */
public interface Stakeholder extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Exposure</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exposure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exposure</em>' attribute.
	 * @see #setExposure(int)
	 * @see dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage#getStakeholder_Exposure()
	 * @model default="1"
	 * @generated
	 */
	int getExposure();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getExposure <em>Exposure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exposure</em>' attribute.
	 * @see #getExposure()
	 * @generated
	 */
	void setExposure(int value);

	/**
	 * Returns the value of the '<em><b>Power</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Power</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Power</em>' attribute.
	 * @see #setPower(int)
	 * @see dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage#getStakeholder_Power()
	 * @model default="1"
	 * @generated
	 */
	int getPower();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getPower <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Power</em>' attribute.
	 * @see #getPower()
	 * @generated
	 */
	void setPower(int value);

	/**
	 * Returns the value of the '<em><b>Urgency</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Urgency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Urgency</em>' attribute.
	 * @see #setUrgency(int)
	 * @see dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage#getStakeholder_Urgency()
	 * @model default="1"
	 * @generated
	 */
	int getUrgency();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getUrgency <em>Urgency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Urgency</em>' attribute.
	 * @see #getUrgency()
	 * @generated
	 */
	void setUrgency(int value);

	/**
	 * Returns the value of the '<em><b>Importance</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Importance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Importance</em>' attribute.
	 * @see #setImportance(int)
	 * @see dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage#getStakeholder_Importance()
	 * @model default="1"
	 * @generated
	 */
	int getImportance();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getImportance <em>Importance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Importance</em>' attribute.
	 * @see #getImportance()
	 * @generated
	 */
	void setImportance(int value);

	/**
	 * Returns the value of the '<em><b>Stake</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stake</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stake</em>' containment reference.
	 * @see #setStake(Text)
	 * @see dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage#getStakeholder_Stake()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getStake();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getStake <em>Stake</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stake</em>' containment reference.
	 * @see #getStake()
	 * @generated
	 */
	void setStake(Text value);

	/**
	 * Returns the value of the '<em><b>Engagement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Engagement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Engagement</em>' containment reference.
	 * @see #setEngagement(Text)
	 * @see dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage#getStakeholder_Engagement()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getEngagement();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getEngagement <em>Engagement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Engagement</em>' containment reference.
	 * @see #getEngagement()
	 * @generated
	 */
	void setEngagement(Text value);

	/**
	 * Returns the value of the '<em><b>Long Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Long Description</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Long Description</em>' containment reference.
	 * @see #setLongDescription(Text)
	 * @see dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage#getStakeholder_LongDescription()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getLongDescription();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder#getLongDescription <em>Long Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Long Description</em>' containment reference.
	 * @see #getLongDescription()
	 * @generated
	 */
	void setLongDescription(Text value);

} // Stakeholder
