/**
 */
package dk.dtu.imm.red.specificationelements.stakeholder.impl;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage;
/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stakeholder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.impl.StakeholderImpl#getExposure <em>Exposure</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.impl.StakeholderImpl#getPower <em>Power</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.impl.StakeholderImpl#getUrgency <em>Urgency</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.impl.StakeholderImpl#getImportance <em>Importance</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.impl.StakeholderImpl#getStake <em>Stake</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.impl.StakeholderImpl#getEngagement <em>Engagement</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.stakeholder.impl.StakeholderImpl#getLongDescription <em>Long Description</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StakeholderImpl extends SpecificationElementImpl implements Stakeholder {
	/**
	 * The default value of the '{@link #getExposure() <em>Exposure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExposure()
	 * @generated
	 * @ordered
	 */
	protected static final int EXPOSURE_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getExposure() <em>Exposure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExposure()
	 * @generated
	 * @ordered
	 */
	protected int exposure = EXPOSURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPower() <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPower()
	 * @generated
	 * @ordered
	 */
	protected static final int POWER_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getPower() <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPower()
	 * @generated
	 * @ordered
	 */
	protected int power = POWER_EDEFAULT;

	/**
	 * The default value of the '{@link #getUrgency() <em>Urgency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrgency()
	 * @generated
	 * @ordered
	 */
	protected static final int URGENCY_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getUrgency() <em>Urgency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrgency()
	 * @generated
	 * @ordered
	 */
	protected int urgency = URGENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getImportance() <em>Importance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportance()
	 * @generated
	 * @ordered
	 */
	protected static final int IMPORTANCE_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getImportance() <em>Importance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportance()
	 * @generated
	 * @ordered
	 */
	protected int importance = IMPORTANCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStake() <em>Stake</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStake()
	 * @generated
	 * @ordered
	 */
	protected Text stake;

	/**
	 * The cached value of the '{@link #getEngagement() <em>Engagement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEngagement()
	 * @generated
	 * @ordered
	 */
	protected Text engagement;

	/**
	 * The cached value of the '{@link #getLongDescription() <em>Long Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongDescription()
	 * @generated
	 * @ordered
	 */
	protected Text longDescription;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StakeholderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StakeholderPackage.Literals.STAKEHOLDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getExposure() {
		return exposure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExposure(int newExposure) {
		int oldExposure = exposure;
		exposure = newExposure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__EXPOSURE, oldExposure, exposure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPower() {
		return power;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPower(int newPower) {
		int oldPower = power;
		power = newPower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__POWER, oldPower, power));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getUrgency() {
		return urgency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUrgency(int newUrgency) {
		int oldUrgency = urgency;
		urgency = newUrgency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__URGENCY, oldUrgency, urgency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getImportance() {
		return importance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportance(int newImportance) {
		int oldImportance = importance;
		importance = newImportance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__IMPORTANCE, oldImportance, importance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getStake() {
		if (stake != null && stake.eIsProxy()) {
			InternalEObject oldStake = (InternalEObject)stake;
			stake = (Text)eResolveProxy(oldStake);
			if (stake != oldStake) {
				InternalEObject newStake = (InternalEObject)stake;
				NotificationChain msgs = oldStake.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__STAKE, null, null);
				if (newStake.eInternalContainer() == null) {
					msgs = newStake.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__STAKE, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StakeholderPackage.STAKEHOLDER__STAKE, oldStake, stake));
			}
		}
		return stake;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetStake() {
		return stake;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStake(Text newStake, NotificationChain msgs) {
		Text oldStake = stake;
		stake = newStake;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__STAKE, oldStake, newStake);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStake(Text newStake) {
		if (newStake != stake) {
			NotificationChain msgs = null;
			if (stake != null)
				msgs = ((InternalEObject)stake).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__STAKE, null, msgs);
			if (newStake != null)
				msgs = ((InternalEObject)newStake).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__STAKE, null, msgs);
			msgs = basicSetStake(newStake, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__STAKE, newStake, newStake));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getEngagement() {
		if (engagement != null && engagement.eIsProxy()) {
			InternalEObject oldEngagement = (InternalEObject)engagement;
			engagement = (Text)eResolveProxy(oldEngagement);
			if (engagement != oldEngagement) {
				InternalEObject newEngagement = (InternalEObject)engagement;
				NotificationChain msgs = oldEngagement.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__ENGAGEMENT, null, null);
				if (newEngagement.eInternalContainer() == null) {
					msgs = newEngagement.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__ENGAGEMENT, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StakeholderPackage.STAKEHOLDER__ENGAGEMENT, oldEngagement, engagement));
			}
		}
		return engagement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetEngagement() {
		return engagement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEngagement(Text newEngagement, NotificationChain msgs) {
		Text oldEngagement = engagement;
		engagement = newEngagement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__ENGAGEMENT, oldEngagement, newEngagement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEngagement(Text newEngagement) {
		if (newEngagement != engagement) {
			NotificationChain msgs = null;
			if (engagement != null)
				msgs = ((InternalEObject)engagement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__ENGAGEMENT, null, msgs);
			if (newEngagement != null)
				msgs = ((InternalEObject)newEngagement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__ENGAGEMENT, null, msgs);
			msgs = basicSetEngagement(newEngagement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__ENGAGEMENT, newEngagement, newEngagement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getLongDescription() {
		if (longDescription != null && longDescription.eIsProxy()) {
			InternalEObject oldLongDescription = (InternalEObject)longDescription;
			longDescription = (Text)eResolveProxy(oldLongDescription);
			if (longDescription != oldLongDescription) {
				InternalEObject newLongDescription = (InternalEObject)longDescription;
				NotificationChain msgs = oldLongDescription.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION, null, null);
				if (newLongDescription.eInternalContainer() == null) {
					msgs = newLongDescription.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION, oldLongDescription, longDescription));
			}
		}
		return longDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetLongDescription() {
		return longDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLongDescription(Text newLongDescription, NotificationChain msgs) {
		Text oldLongDescription = longDescription;
		longDescription = newLongDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION, oldLongDescription, newLongDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLongDescription(Text newLongDescription) {
		if (newLongDescription != longDescription) {
			NotificationChain msgs = null;
			if (longDescription != null)
				msgs = ((InternalEObject)longDescription).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION, null, msgs);
			if (newLongDescription != null)
				msgs = ((InternalEObject)newLongDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION, null, msgs);
			msgs = basicSetLongDescription(newLongDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION, newLongDescription, newLongDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StakeholderPackage.STAKEHOLDER__STAKE:
				return basicSetStake(null, msgs);
			case StakeholderPackage.STAKEHOLDER__ENGAGEMENT:
				return basicSetEngagement(null, msgs);
			case StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION:
				return basicSetLongDescription(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StakeholderPackage.STAKEHOLDER__EXPOSURE:
				return getExposure();
			case StakeholderPackage.STAKEHOLDER__POWER:
				return getPower();
			case StakeholderPackage.STAKEHOLDER__URGENCY:
				return getUrgency();
			case StakeholderPackage.STAKEHOLDER__IMPORTANCE:
				return getImportance();
			case StakeholderPackage.STAKEHOLDER__STAKE:
				if (resolve) return getStake();
				return basicGetStake();
			case StakeholderPackage.STAKEHOLDER__ENGAGEMENT:
				if (resolve) return getEngagement();
				return basicGetEngagement();
			case StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION:
				if (resolve) return getLongDescription();
				return basicGetLongDescription();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StakeholderPackage.STAKEHOLDER__EXPOSURE:
				setExposure((Integer)newValue);
				return;
			case StakeholderPackage.STAKEHOLDER__POWER:
				setPower((Integer)newValue);
				return;
			case StakeholderPackage.STAKEHOLDER__URGENCY:
				setUrgency((Integer)newValue);
				return;
			case StakeholderPackage.STAKEHOLDER__IMPORTANCE:
				setImportance((Integer)newValue);
				return;
			case StakeholderPackage.STAKEHOLDER__STAKE:
				setStake((Text)newValue);
				return;
			case StakeholderPackage.STAKEHOLDER__ENGAGEMENT:
				setEngagement((Text)newValue);
				return;
			case StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION:
				setLongDescription((Text)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StakeholderPackage.STAKEHOLDER__EXPOSURE:
				setExposure(EXPOSURE_EDEFAULT);
				return;
			case StakeholderPackage.STAKEHOLDER__POWER:
				setPower(POWER_EDEFAULT);
				return;
			case StakeholderPackage.STAKEHOLDER__URGENCY:
				setUrgency(URGENCY_EDEFAULT);
				return;
			case StakeholderPackage.STAKEHOLDER__IMPORTANCE:
				setImportance(IMPORTANCE_EDEFAULT);
				return;
			case StakeholderPackage.STAKEHOLDER__STAKE:
				setStake((Text)null);
				return;
			case StakeholderPackage.STAKEHOLDER__ENGAGEMENT:
				setEngagement((Text)null);
				return;
			case StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION:
				setLongDescription((Text)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StakeholderPackage.STAKEHOLDER__EXPOSURE:
				return exposure != EXPOSURE_EDEFAULT;
			case StakeholderPackage.STAKEHOLDER__POWER:
				return power != POWER_EDEFAULT;
			case StakeholderPackage.STAKEHOLDER__URGENCY:
				return urgency != URGENCY_EDEFAULT;
			case StakeholderPackage.STAKEHOLDER__IMPORTANCE:
				return importance != IMPORTANCE_EDEFAULT;
			case StakeholderPackage.STAKEHOLDER__STAKE:
				return stake != null;
			case StakeholderPackage.STAKEHOLDER__ENGAGEMENT:
				return engagement != null;
			case StakeholderPackage.STAKEHOLDER__LONG_DESCRIPTION:
				return longDescription != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (exposure: ");
		result.append(exposure);
		result.append(", power: ");
		result.append(power);
		result.append(", urgency: ");
		result.append(urgency);
		result.append(", importance: ");
		result.append(importance);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/stakeholder.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		attributesToSearch.put(StakeholderPackage.Literals.STAKEHOLDER__LONG_DESCRIPTION,
				this.getDescription()== null ? "" : this.getDescription().toString());
		attributesToSearch.put(StakeholderPackage.Literals.STAKEHOLDER__ENGAGEMENT,
				this.getEngagement()== null ? "" : this.getEngagement().toString());
		attributesToSearch.put(StakeholderPackage.Literals.STAKEHOLDER__STAKE,
				this.getStake()== null ? "" : this.getStake().toString());

		return super.search(param, attributesToSearch);

	}

	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {
		return new ElementColumnDefinition[] {
				new ElementColumnDefinition()
				.setHeaderName("Stakeholder")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Stakeholder>() {

					@Override
					public Object getAttributeValue(Stakeholder item) {
						return item.getName();
					}

					@Override
					public void setAttributeValue(Stakeholder item, Object value) {
						item.setName((String) value);
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Kind")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__ELEMENT_KIND)
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setCellItems(new String[] { "Internal", "External" } )
				.setEditable(true)
				.setCellHandler(new CellHandler<Stakeholder>() {

					@Override
					public Object getAttributeValue(Stakeholder item) {
						return item.getElementKind()==null?"":item.getElementKind().toString();
					}

					@Override
					public void setAttributeValue(Stakeholder item, Object value) {
						item.setElementKind(value.toString());
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Exposure")
				.setEMFLiteral(StakeholderPackage.Literals.STAKEHOLDER__EXPOSURE)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Stakeholder>() {

					@Override
					public Object getAttributeValue(Stakeholder item) {
						return item.getExposure();
					}

					@Override
					public void setAttributeValue(Stakeholder item, Object value) {
						item.setExposure(Integer.valueOf((String) value.toString().toUpperCase()));
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Power")
				.setEMFLiteral(StakeholderPackage.Literals.STAKEHOLDER__POWER)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Stakeholder>() {

					@Override
					public Object getAttributeValue(Stakeholder item) {
						return item.getPower();
					}

					@Override
					public void setAttributeValue(Stakeholder item, Object value) {
						item.setPower(Integer.valueOf((String) value.toString().toUpperCase()));
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Urgency")
				.setEMFLiteral(StakeholderPackage.Literals.STAKEHOLDER__URGENCY)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Stakeholder>() {

					@Override
					public Object getAttributeValue(Stakeholder item) {
						return item.getUrgency();
					}

					@Override
					public void setAttributeValue(Stakeholder item, Object value) {
						item.setUrgency(Integer.valueOf((String) value.toString().toUpperCase()));
					}
				})
		};
	}
	
	@Override
	public String getStructeredIdType() {
		return "ST";
	}

} //StakeholderImpl
