/**
 */
package dk.dtu.imm.red.specificationelements.stakeholder.impl;

import dk.dtu.imm.red.core.text.TextPackage;
import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.stakeholder.StakeholderFactory;
import dk.dtu.imm.red.specificationelements.stakeholder.StakeholderKind;
import dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StakeholderPackageImpl extends EPackageImpl implements StakeholderPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stakeholderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum stakeholderKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.specificationelements.stakeholder.StakeholderPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StakeholderPackageImpl() {
		super(eNS_URI, StakeholderFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link StakeholderPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StakeholderPackage init() {
		if (isInited) return (StakeholderPackage)EPackage.Registry.INSTANCE.getEPackage(StakeholderPackage.eNS_URI);

		// Obtain or create and register package
		StakeholderPackageImpl theStakeholderPackage = (StakeholderPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof StakeholderPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new StakeholderPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theStakeholderPackage.createPackageContents();

		// Initialize created meta-data
		theStakeholderPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theStakeholderPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(StakeholderPackage.eNS_URI, theStakeholderPackage);
		return theStakeholderPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStakeholder() {
		return stakeholderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStakeholder_Exposure() {
		return (EAttribute)stakeholderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStakeholder_Power() {
		return (EAttribute)stakeholderEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStakeholder_Urgency() {
		return (EAttribute)stakeholderEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStakeholder_Importance() {
		return (EAttribute)stakeholderEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStakeholder_Stake() {
		return (EReference)stakeholderEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStakeholder_Engagement() {
		return (EReference)stakeholderEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStakeholder_LongDescription() {
		return (EReference)stakeholderEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStakeholderKind() {
		return stakeholderKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StakeholderFactory getStakeholderFactory() {
		return (StakeholderFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		stakeholderEClass = createEClass(STAKEHOLDER);
		createEAttribute(stakeholderEClass, STAKEHOLDER__EXPOSURE);
		createEAttribute(stakeholderEClass, STAKEHOLDER__POWER);
		createEAttribute(stakeholderEClass, STAKEHOLDER__URGENCY);
		createEAttribute(stakeholderEClass, STAKEHOLDER__IMPORTANCE);
		createEReference(stakeholderEClass, STAKEHOLDER__STAKE);
		createEReference(stakeholderEClass, STAKEHOLDER__ENGAGEMENT);
		createEReference(stakeholderEClass, STAKEHOLDER__LONG_DESCRIPTION);

		// Create enums
		stakeholderKindEEnum = createEEnum(STAKEHOLDER_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SpecificationelementsPackage theSpecificationelementsPackage = (SpecificationelementsPackage)EPackage.Registry.INSTANCE.getEPackage(SpecificationelementsPackage.eNS_URI);
		TextPackage theTextPackage = (TextPackage)EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		stakeholderEClass.getESuperTypes().add(theSpecificationelementsPackage.getSpecificationElement());

		// Initialize classes and features; add operations and parameters
		initEClass(stakeholderEClass, Stakeholder.class, "Stakeholder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStakeholder_Exposure(), ecorePackage.getEInt(), "exposure", "1", 0, 1, Stakeholder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStakeholder_Power(), ecorePackage.getEInt(), "power", "1", 0, 1, Stakeholder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStakeholder_Urgency(), ecorePackage.getEInt(), "urgency", "1", 0, 1, Stakeholder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStakeholder_Importance(), ecorePackage.getEInt(), "importance", "1", 0, 1, Stakeholder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStakeholder_Stake(), theTextPackage.getText(), null, "stake", null, 0, 1, Stakeholder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStakeholder_Engagement(), theTextPackage.getText(), null, "engagement", null, 0, 1, Stakeholder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStakeholder_LongDescription(), theTextPackage.getText(), null, "longDescription", null, 0, 1, Stakeholder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(stakeholderKindEEnum, StakeholderKind.class, "StakeholderKind");
		addEEnumLiteral(stakeholderKindEEnum, StakeholderKind.UNSPECIFIED);
		addEEnumLiteral(stakeholderKindEEnum, StakeholderKind.INTERNAL);
		addEEnumLiteral(stakeholderKindEEnum, StakeholderKind.EXTERNAL);

		// Create resource
		createResource(eNS_URI);
	}

} //StakeholderPackageImpl
