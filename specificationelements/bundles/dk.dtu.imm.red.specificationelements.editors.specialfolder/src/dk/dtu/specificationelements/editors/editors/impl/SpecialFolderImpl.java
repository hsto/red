/**
 */
package dk.dtu.specificationelements.editors.editors.impl;

import dk.dtu.imm.red.core.folder.impl.FolderImpl;
import dk.dtu.specificationelements.editors.editors.EditorsPackage;
import dk.dtu.specificationelements.editors.editors.SpecialFolder;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Special Folder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link dk.dtu.specificationelements.editors.editors.impl.SpecialFolderImpl#getSpecialType <em>Special Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SpecialFolderImpl extends FolderImpl implements SpecialFolder {
	/**
	 * The default value of the '{@link #getSpecialType() <em>Special Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialType()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIAL_TYPE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getSpecialType() <em>Special Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialType()
	 * @generated
	 * @ordered
	 */
	protected String specialType = SPECIAL_TYPE_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpecialFolderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorsPackage.Literals.SPECIAL_FOLDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecialType() {
		return specialType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialType(String newSpecialType) {
		String oldSpecialType = specialType;
		specialType = newSpecialType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EditorsPackage.SPECIAL_FOLDER__SPECIAL_TYPE, oldSpecialType, specialType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EditorsPackage.SPECIAL_FOLDER__SPECIAL_TYPE:
				return getSpecialType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EditorsPackage.SPECIAL_FOLDER__SPECIAL_TYPE:
				setSpecialType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EditorsPackage.SPECIAL_FOLDER__SPECIAL_TYPE:
				setSpecialType(SPECIAL_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EditorsPackage.SPECIAL_FOLDER__SPECIAL_TYPE:
				return SPECIAL_TYPE_EDEFAULT == null ? specialType != null : !SPECIAL_TYPE_EDEFAULT.equals(specialType);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (specialType: ");
		result.append(specialType);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/editors.specialfolder.png";
	}

} //SpecialFolderImpl
