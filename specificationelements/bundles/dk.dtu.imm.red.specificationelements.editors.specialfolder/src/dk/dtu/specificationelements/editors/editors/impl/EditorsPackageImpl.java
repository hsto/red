/**
 */
package dk.dtu.specificationelements.editors.editors.impl;

import dk.dtu.imm.red.core.folder.FolderPackage;
import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;
import dk.dtu.specificationelements.editors.editors.EditorsFactory;
import dk.dtu.specificationelements.editors.editors.EditorsPackage;
import dk.dtu.specificationelements.editors.editors.SpecialFolder;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EditorsPackageImpl extends EPackageImpl implements EditorsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass specialFolderEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.specificationelements.editors.editors.EditorsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EditorsPackageImpl() {
		super(eNS_URI, EditorsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link EditorsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EditorsPackage init() {
		if (isInited) return (EditorsPackage)EPackage.Registry.INSTANCE.getEPackage(EditorsPackage.eNS_URI);

		// Obtain or create and register package
		EditorsPackageImpl theEditorsPackage = (EditorsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof EditorsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new EditorsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theEditorsPackage.createPackageContents();

		// Initialize created meta-data
		theEditorsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEditorsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EditorsPackage.eNS_URI, theEditorsPackage);
		return theEditorsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSpecialFolder() {
		return specialFolderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSpecialFolder_SpecialType() {
		return (EAttribute)specialFolderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorsFactory getEditorsFactory() {
		return (EditorsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		specialFolderEClass = createEClass(SPECIAL_FOLDER);
		createEAttribute(specialFolderEClass, SPECIAL_FOLDER__SPECIAL_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		FolderPackage theFolderPackage = (FolderPackage)EPackage.Registry.INSTANCE.getEPackage(FolderPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		specialFolderEClass.getESuperTypes().add(theFolderPackage.getFolder());

		// Initialize classes and features; add operations and parameters
		initEClass(specialFolderEClass, SpecialFolder.class, "SpecialFolder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSpecialFolder_SpecialType(), theEcorePackage.getEString(), "specialType", null, 0, 1, SpecialFolder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //EditorsPackageImpl
