/**
 */
package dk.dtu.specificationelements.editors.editors;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.specificationelements.editors.editors.EditorsPackage
 * @generated
 */
public interface EditorsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EditorsFactory eINSTANCE = dk.dtu.specificationelements.editors.editors.impl.EditorsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Special Folder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Special Folder</em>'.
	 * @generated
	 */
	SpecialFolder createSpecialFolder();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EditorsPackage getEditorsPackage();

} //EditorsFactory
