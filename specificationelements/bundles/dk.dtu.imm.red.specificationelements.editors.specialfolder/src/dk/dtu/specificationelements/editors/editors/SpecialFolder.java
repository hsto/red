/**
 */
package dk.dtu.specificationelements.editors.editors;

import dk.dtu.imm.red.core.folder.Folder;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Special Folder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link dk.dtu.specificationelements.editors.editors.SpecialFolder#getSpecialType <em>Special Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see dk.dtu.specificationelements.editors.editors.EditorsPackage#getSpecialFolder()
 * @model
 * @generated
 */
public interface SpecialFolder extends Folder  {
	/**
	 * Returns the value of the '<em><b>Special Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Special Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Special Type</em>' attribute.
	 * @see #setSpecialType(String)
	 * @see dk.dtu.specificationelements.editors.editors.EditorsPackage#getSpecialFolder_SpecialType()
	 * @model
	 * @generated
	 */
	String getSpecialType();

	/**
	 * Sets the value of the '{@link dk.dtu.specificationelements.editors.editors.SpecialFolder#getSpecialType <em>Special Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Special Type</em>' attribute.
	 * @see #getSpecialType()
	 * @generated
	 */
	void setSpecialType(String value);

} // SpecialFolder
