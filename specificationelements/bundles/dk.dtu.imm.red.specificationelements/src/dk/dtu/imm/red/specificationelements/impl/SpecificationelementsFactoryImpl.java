/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.specificationelements.impl;

import dk.dtu.imm.red.specificationelements.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import dk.dtu.imm.red.specificationelements.SpecificationelementsFactory;
import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class SpecificationelementsFactoryImpl extends EFactoryImpl implements
		SpecificationelementsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static SpecificationelementsFactory init() {
		try {
			SpecificationelementsFactory theSpecificationelementsFactory = (SpecificationelementsFactory)EPackage.Registry.INSTANCE.getEFactory(SpecificationelementsPackage.eNS_URI);
			if (theSpecificationelementsFactory != null) {
				return theSpecificationelementsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SpecificationelementsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public SpecificationelementsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SpecificationelementsPackage getSpecificationelementsPackage() {
		return (SpecificationelementsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SpecificationelementsPackage getPackage() {
		return SpecificationelementsPackage.eINSTANCE;
	}

} // SpecificationelementsFactoryImpl
