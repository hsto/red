/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.specificationelements.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;

import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.contribution.impl.ContributionImpl;
import dk.dtu.imm.red.core.element.relationship.ElementReference;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.ui.internal.traceability.util.SpecificationElementType;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Specification Element</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class SpecificationElementImpl extends ContributionImpl implements SpecificationElement {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SpecificationElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpecificationelementsPackage.Literals.SPECIFICATION_ELEMENT;
	}

	@Override
	public void checkStructure() {
		super.checkStructure();
		List<ElementRelationship> relationships = new ArrayList<ElementRelationship>();

		if (getRelatesTo().isEmpty() && getRelatedBy().isEmpty()) {
			addIssueComment(IssueSeverity.MISTAKE, "Element has no relationships");
		} else {

			// check outgoing relationships
			for (ElementRelationship relationship : getRelatesTo()) {
				relationships.add(relationship);
			}

			checkOutgoingRelationships(relationships);
		}
	}

	/**
	 * Recursive approach to check all outgoing relationships of SpecificationElement
	 * 
	 * @param references
	 */
	private void checkOutgoingRelationships(List<ElementRelationship> references) {
		ElementReference pointer;
		Element target;

		if (references.isEmpty()) {
			return;
		} else if (references.get(0).getToElement() == null) {
//			addIssueComment(IssueSeverity.ERROR, "Project data corrupted for relationship: "
//					+ ((ElementReference) references.get(0)).getRelationshipKind().getLiteral());
//			return;
			// Somehow data gets corrupted. Target of relationship missing (probably element not correctly removed?).
			// Issue in RED repository has to be opened.
			references.remove(0);
			checkOutgoingRelationships(references);
		} else {
			pointer = (ElementReference) references.get(0);
			target = pointer.getToElement();
			references.remove(0);
			
			if(validateActivePassivePattern(pointer))
				checkOutgoingRelationships(references);

			// Check all
			System.out.println(this.eClass().getName() + " --<" + pointer.getRelationshipKind().getLiteral() + ">--> "
					+ pointer.getToElement().eClass().getName());

			switch (pointer.getRelationshipKind()) {

			// relationships with specification element as source and target (no
			// restrictions)
			// listed for completeness and for possible future additions
			case CONTRIBUTES_COST_TO:
				;
			case CONTRIBUTES_PERFORMANCE_TO:
				;
			case CONTRIBUTES_RISK_TO:
				;
			case LEADS_TO:
				;
			case PRECEDES:
				;
			case CONTAINS:
				;
			case ELABORATES:
				;
			case COMES_FROM:
				;
			case SUPERSEDES:
				;
			case DERIVES_TO:
				break;

			// Checks specializations
			case REFERS_TO:
				if (!target.eClass().getName().equals(SpecificationElementType.GLOSSARYENTRY.getName()))
					writeTargetErrorMessage(pointer.getRelationshipKind(), target);
				break;

			case FEATURES_IN:
				if (!this.eClass().getName().equals(SpecificationElementType.PERSONA.name())
						&& !this.eClass().getName().equals(SpecificationElementType.ACTOR.name())
						&& !this.eClass().getName().equals(SpecificationElementType.MODELELEMENT.getName()))
					writeSourceErrorMessage(pointer.getRelationshipKind());
				if (!target.eClass().getName().equals(SpecificationElementType.SCENARIO.getName()))
					writeTargetErrorMessage(pointer.getRelationshipKind(), target);
				break;

			case CONFLICTS_WITH:
				if (!this.eClass().getName().equals(SpecificationElementType.ASSUMPTION.getName())
						&& !this.eClass().getName().equals(SpecificationElementType.GOAL.getName()))
					writeSourceErrorMessage(pointer.getRelationshipKind());
				break;

			// Rules with same restrictions listed together
			case INCLUDES:
				;
			case EXTENDS:
				if (!this.eClass().getName().equals(SpecificationElementType.USECASE.getName()))
					writeSourceErrorMessage(pointer.getRelationshipKind());
				if (!target.eClass().getName().equals(SpecificationElementType.USECASE.getName()))
					writeTargetErrorMessage(pointer.getRelationshipKind(), target);
				break;

			case IS_STITCHED_TO:
				if (!this.eClass().getName().equals(SpecificationElementType.DIAGRAM.getName()))
					writeSourceErrorMessage(pointer.getRelationshipKind());
				if (!target.eClass().getName().equals(SpecificationElementType.DIAGRAM.getName()))
					writeTargetErrorMessage(pointer.getRelationshipKind(), target);
				break;
			case ILLUSTRATES:
				if (!this.eClass().getName().equals(SpecificationElementType.DIAGRAM.getName()))
					writeSourceErrorMessage(pointer.getRelationshipKind());
				break;

			case JUSTIFIES:
				if (!this.eClass().getName().equals(SpecificationElementType.ASSUMPTION.getName())
						&& !this.eClass().getName().equals(SpecificationElementType.VISION.getName())
						&& !this.eClass().getName().equals(SpecificationElementType.GOAL.getName()))
					writeSourceErrorMessage(pointer.getRelationshipKind());
				if (this.eClass().getName().equals(SpecificationElementType.VISION.getName())
						|| this.eClass().getName().equals(SpecificationElementType.GOAL.getName())) {
					if (!target.eClass().getName().equals(SpecificationElementType.GOAL.getName())
							&& !target.eClass().getName().equals(SpecificationElementType.REQUIREMENT.getName())) {
						writeTargetErrorMessage(pointer.getRelationshipKind(), target);
					}
				}
				break;

			case REFINES_TO:
				if (!this.eClass().getName().equals(SpecificationElementType.MODELELEMENT.getName()))
					writeSourceErrorMessage(pointer.getRelationshipKind());
				if (!target.eClass().getName().equals(SpecificationElementType.MODELELEMENT.getName()))
					writeTargetErrorMessage(pointer.getRelationshipKind(), target);
				break;

			case SUPPORTS:
				if (!this.eClass().getName().equals(SpecificationElementType.ASSUMPTION.getName())
						&& !this.eClass().getName().equals(SpecificationElementType.GOAL.getName()))
					writeSourceErrorMessage(pointer.getRelationshipKind());

				if (!target.eClass().getName().equals(SpecificationElementType.ASSUMPTION.getName())
						&& !target.eClass().getName().equals(SpecificationElementType.GOAL.getName())) {
					writeTargetErrorMessage(pointer.getRelationshipKind(), target);
				}
				break;
			default:
				System.out.println("Uncovered: " + pointer.getRelationshipKind().getLiteral());
				break;
			}
			
			checkOutgoingRelationships(references);
		}
	}

	private boolean validateActivePassivePattern(ElementReference reference) {
		int value = reference.getRelationshipKind().getValue();

		// Check if reference is not an active type
		if (value % 2 != 1 && value < RelationshipKind.CONFLICTS_WITH_VALUE && value > RelationshipKind.IS_ASSOCIATED_TO_VALUE) {
			// then change source and target, and the relationship type to
			// active
			Element source = reference.getFromElement();
			reference.setFromElement(reference.getToElement());
			reference.setToElement(source);
			reference.setRelationshipKind(RelationshipKind.getReverseRelationshipKind(reference.getRelationshipKind()));
		
			Element target = reference.getToElement();
			source = reference.getFromElement();

			source.getRelatedBy().remove(reference);
			source.getRelatesTo().add(reference);
			
			target.getRelatesTo().remove(reference);
			target.getRelatedBy().add(reference);
			
			WorkspaceFactory.eINSTANCE.getWorkspaceInstance().save();
			
			return true;
		}
		
		return false;
	}
	
	private void writeSourceErrorMessage(RelationshipKind kind) {
		addIssueComment(IssueSeverity.MISTAKE, "Element contains wrong relationship type: " + kind.getLiteral());
	}

	private void writeTargetErrorMessage(RelationshipKind kind, Element target) {
		addIssueComment(IssueSeverity.MISTAKE,
				"Relationship has wrong target type: " + kind.getLiteral() + " " + target.eClass().getName());
	}
	
} // SpecificationElementImpl
