/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.specificationelements;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import dk.dtu.imm.red.core.element.contribution.ContributionPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.SpecificationelementsFactory
 * @model kind="package"
 * @generated
 */
public interface SpecificationelementsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "specificationelements";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.specificationelements";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "specificationelements";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	SpecificationelementsPackage eINSTANCE = dk.dtu.imm.red.specificationelements.impl.SpecificationelementsPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl <em>Specification Element</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl
	 * @see dk.dtu.imm.red.specificationelements.impl.SpecificationelementsPackageImpl#getSpecificationElement()
	 * @generated
	 */
	int SPECIFICATION_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__ICON_URI = ContributionPackage.CONTRIBUTION__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__ICON = ContributionPackage.CONTRIBUTION__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__LABEL = ContributionPackage.CONTRIBUTION__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__NAME = ContributionPackage.CONTRIBUTION__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__ELEMENT_KIND = ContributionPackage.CONTRIBUTION__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__DESCRIPTION = ContributionPackage.CONTRIBUTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__PRIORITY = ContributionPackage.CONTRIBUTION__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__COMMENTLIST = ContributionPackage.CONTRIBUTION__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__TIME_CREATED = ContributionPackage.CONTRIBUTION__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__LAST_MODIFIED = ContributionPackage.CONTRIBUTION__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__LAST_MODIFIED_BY = ContributionPackage.CONTRIBUTION__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__CREATOR = ContributionPackage.CONTRIBUTION__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__VERSION = ContributionPackage.CONTRIBUTION__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__VISIBLE_ID = ContributionPackage.CONTRIBUTION__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__UNIQUE_ID = ContributionPackage.CONTRIBUTION__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__RELATES_TO = ContributionPackage.CONTRIBUTION__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__RELATED_BY = ContributionPackage.CONTRIBUTION__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__PARENT = ContributionPackage.CONTRIBUTION__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__URI = ContributionPackage.CONTRIBUTION__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__WORK_PACKAGE = ContributionPackage.CONTRIBUTION__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__CHANGE_LIST = ContributionPackage.CONTRIBUTION__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__RESPONSIBLE_USER = ContributionPackage.CONTRIBUTION__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__DEADLINE = ContributionPackage.CONTRIBUTION__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__LOCK_STATUS = ContributionPackage.CONTRIBUTION__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__LOCK_PASSWORD = ContributionPackage.CONTRIBUTION__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__ESTIMATED_COMPLEXITY = ContributionPackage.CONTRIBUTION__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__COST = ContributionPackage.CONTRIBUTION__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__BENEFIT = ContributionPackage.CONTRIBUTION__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__RISK = ContributionPackage.CONTRIBUTION__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__LIFE_CYCLE_PHASE = ContributionPackage.CONTRIBUTION__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__STATE = ContributionPackage.CONTRIBUTION__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__MANAGEMENT_DECISION = ContributionPackage.CONTRIBUTION__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__QA_ASSESSMENT = ContributionPackage.CONTRIBUTION__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__MANAGEMENT_DISCUSSION = ContributionPackage.CONTRIBUTION__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__DELETION_LISTENERS = ContributionPackage.CONTRIBUTION__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__QUANTITIES = ContributionPackage.CONTRIBUTION__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT__CUSTOM_ATTRIBUTES = ContributionPackage.CONTRIBUTION__CUSTOM_ATTRIBUTES;

	/**
	 * The number of structural features of the '<em>Specification Element</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIFICATION_ELEMENT_FEATURE_COUNT = ContributionPackage.CONTRIBUTION_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.SpecificationElement <em>Specification Element</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Specification Element</em>'.
	 * @see dk.dtu.imm.red.specificationelements.SpecificationElement
	 * @generated
	 */
	EClass getSpecificationElement();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SpecificationelementsFactory getSpecificationelementsFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl <em>Specification Element</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl
		 * @see dk.dtu.imm.red.specificationelements.impl.SpecificationelementsPackageImpl#getSpecificationElement()
		 * @generated
		 */
		EClass SPECIFICATION_ELEMENT = eINSTANCE.getSpecificationElement();

	}

} // SpecificationelementsPackage
