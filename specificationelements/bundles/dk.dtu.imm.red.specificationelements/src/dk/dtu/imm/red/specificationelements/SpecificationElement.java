/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.specificationelements;

import dk.dtu.imm.red.core.element.contribution.Contribution;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Specification Element</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.specificationelements.SpecificationelementsPackage#getSpecificationElement()
 * @model abstract="true"
 * @generated
 */
public interface SpecificationElement extends Contribution { 
	
} // SpecificationElement
