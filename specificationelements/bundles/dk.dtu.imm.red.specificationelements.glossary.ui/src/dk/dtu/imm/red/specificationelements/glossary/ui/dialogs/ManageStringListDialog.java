package dk.dtu.imm.red.specificationelements.glossary.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Dialog for managing a list of strings. Provides an overview of the list of
 * strings, and options for removing and adding strings.
 * 
 * @author Jakob Kragelund
 * 
 */
public final class ManageStringListDialog extends Dialog {

	private static final int LIST_HEIGHT = 200;

	private static final int LIST_TEXT_WIDTH = 140;

	/**
	 * The presenter for this dialog.
	 */
	ManageStringListDialogPresenter presenter;

	/**
	 * The list widget for displaying the strings.
	 */
	List stringList;

	/**
	 * Button for removing the currently selected string.
	 */
	Button removeStringButton;

	/**
	 * Button for adding a new string.
	 */
	Button addStringButton;

	/**
	 * Text field for the value of a new string.
	 */
	Text newStringText;

	/**
	 * Create the dialog component.
	 * 
	 * @param parentShell
	 *            parent shell
	 * @param stringListContent
	 *            the content to show in the string list
	 */
	public ManageStringListDialog(final Shell parentShell,
			final java.util.List<String> stringListContent) {
		super(parentShell);
		presenter = new ManageStringListDialogPresenter(this, stringListContent);
	}

	@Override
	protected Button getButton(final int id) {
		return super.getButton(id);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
		getButton(IDialogConstants.OK_ID).setEnabled(false);
	}

	@Override
	protected Control createDialogArea(final Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		container.setLayout(layout);

		GridData listLayoutData = new GridData();
		listLayoutData.minimumHeight = LIST_HEIGHT;
		listLayoutData.grabExcessVerticalSpace = true;
		listLayoutData.verticalAlignment = SWT.FILL;
		listLayoutData.minimumWidth = LIST_TEXT_WIDTH;
		listLayoutData.grabExcessHorizontalSpace = true;
		listLayoutData.horizontalAlignment = SWT.FILL;

		GridData newStringLayoutData = new GridData();
		newStringLayoutData.minimumWidth = LIST_TEXT_WIDTH;
		newStringLayoutData.grabExcessHorizontalSpace = true;
		newStringLayoutData.horizontalAlignment = SWT.FILL;

		newStringText = new Text(container, SWT.BORDER);
		newStringText.setLayoutData(newStringLayoutData);
		newStringText.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				if (newStringText.getText().trim().isEmpty()) {
					addStringButton.setEnabled(false);
				} else {
					addStringButton.setEnabled(true);
				}
			}
		});

		addStringButton = new Button(container, SWT.NONE);
		addStringButton.setText("Add");
		addStringButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				presenter.addStringButtonPressed();
			}
		});

		stringList = new List(container, SWT.V_SCROLL | SWT.BORDER);
		stringList.setLayoutData(listLayoutData);
		stringList.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				if (stringList.getSelectionCount() > 0) {
					removeStringButton.setEnabled(true);
				} else {
					removeStringButton.setEnabled(false);
				}
			}
		});

		GridData removeButtonLayoutData = new GridData();
		removeButtonLayoutData.verticalAlignment = SWT.BOTTOM;

		removeStringButton = new Button(container, SWT.NONE);
		removeStringButton.setText("Remove");
		removeStringButton.setLayoutData(removeButtonLayoutData);
		removeStringButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				presenter.removeStringButtonPressed();
			}
		});

		presenter.initialiseDialog();

		return container;
	}

	public ManageStringListDialogPresenter getPresenter() {
		return presenter;
	}

}
