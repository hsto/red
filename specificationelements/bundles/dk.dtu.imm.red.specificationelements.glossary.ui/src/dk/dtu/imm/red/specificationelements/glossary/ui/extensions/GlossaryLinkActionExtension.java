package dk.dtu.imm.red.specificationelements.glossary.ui.extensions;

import org.eclipse.epf.richtext.actions.ILinkActionExtension;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.ui.views.GlossaryView;
import dk.dtu.imm.red.specificationelements.glossary.ui.views.impl.GlossaryViewImpl;

/**
 * Extension for the ILinkActionExtension extension point in the
 * org.eclipse.epf.richtexteditor plugin.
 * 
 * @author Jakob Kragelund
 * 
 */
public class GlossaryLinkActionExtension implements ILinkActionExtension {

	@Override
	public void linkClicked(final String target) {

		Element targetElement = WorkspaceFactory.eINSTANCE
				.getWorkspaceInstance().findDescendantByUniqueID(target);

		try {
			if (targetElement instanceof GlossaryEntry) {
				GlossaryView view = (GlossaryView) PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.showView(GlossaryViewImpl.ID);
				view.glossaryEntrySelected((GlossaryEntry) targetElement);
			}
		} catch (PartInitException e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}
}
