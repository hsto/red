package dk.dtu.imm.red.specificationelements.glossary.ui.wizards;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;

/**
 * Presenter for the new glossary wizard.
 * 
 * @author Jakob Kragelund
 * 
 */
public interface NewGlossaryWizardPresenter extends IWizardPresenter {

	/**
	 * If the user has selected a parent group for this new glossary, this
	 * method is used to access the parent group.
	 * 
	 * @return The parent group, if any is chosen, for the new glossary
	 */
	Group getNewGlossaryParent();

	/**
	 * Returns the name of the new glossary.
	 * 
	 * @return the name of the new glossary
	 */
	String getNewglossaryName();

	/**
	 * Called by the wizard when it finished.
	 * 
	 * @param name
	 *            the name of the new glossary, which the user has entered
	 * @param parent
	 *            an optional parent group, if chosen by the user
	 * @param path 
	 */
	void wizardFinished(String name, Group parent, String path);

}
