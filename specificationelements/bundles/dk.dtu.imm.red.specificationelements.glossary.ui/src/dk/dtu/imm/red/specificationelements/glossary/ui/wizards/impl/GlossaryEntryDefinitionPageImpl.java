package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

/**
 * Wizard page used for defining a glossary entry.
 * 
 * @author Jakob Kragelund
 * 
 */
public class GlossaryEntryDefinitionPageImpl extends WizardPage {

	/**
	 * Label for the Term textbox.
	 */
	protected Label termLabel;

	/**
	 * Text-box for the glossary entry term.
	 */
	protected Text termText;

	/**
	 * Label for the definition editor.
	 */
	protected Label definitionLabel;

	/**
	 * RichTextEditor for the glossary entry definition.
	 */
	protected RichTextEditor definitionEditor;

	public GlossaryEntryDefinitionPageImpl() {
		super("Define Glossary Entry");
		setTitle("Define Glossary Entry");
		setDescription("Use this page to define a glossary entry, by "
				+ "giving it a term, adding abbreviations and synonyms, and "
				+ "adding a definition.");
	}

	@Override
	public void createControl(final Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(2, false);
		container.setLayout(layout);

		GridData labelTwoColumnData = new GridData();
		labelTwoColumnData.horizontalSpan = 2;
		labelTwoColumnData.grabExcessHorizontalSpace = true;
		labelTwoColumnData.grabExcessVerticalSpace = false;

		GridData editorTwoColumnData = new GridData();
		editorTwoColumnData.horizontalSpan = 2;
		editorTwoColumnData.horizontalAlignment = SWT.FILL;
		editorTwoColumnData.verticalAlignment = SWT.FILL;
		editorTwoColumnData.grabExcessHorizontalSpace = true;
		editorTwoColumnData.grabExcessVerticalSpace = true;

		GridData termGridData = new GridData();
		termGridData.horizontalSpan = 1;
		termGridData.verticalAlignment = SWT.FILL;
		termGridData.widthHint = 200;

		termLabel = new Label(container, SWT.NONE);
		termLabel.setText("Term:");

		termText = new Text(container, SWT.BORDER);
		termText.setLayoutData(termGridData);
		termText.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				if (termText.getText().trim().isEmpty()) {
					setPageComplete(false);
				} else {
					setPageComplete(true);
				}
			}
		});

		definitionLabel = new Label(container, SWT.NONE);
		definitionLabel.setText("Definition:");
		definitionLabel.setLayoutData(labelTwoColumnData);

		definitionEditor = new RichTextEditor(container, SWT.BORDER, null, null);
		definitionEditor.setLayoutData(editorTwoColumnData);

		setPageComplete(false);
		setControl(container);
	}

}
