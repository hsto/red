package dk.dtu.imm.red.specificationelements.glossary.ui.views.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import dk.dtu.imm.red.core.ui.views.BaseView;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.ui.views.GlossaryView;

/**
 * View used for displaying glossary entries and browsing through glossaries.
 *
 * @author Jakob Kragelund
 *
 */
public final class GlossaryViewImpl extends BaseView implements GlossaryView {

	/**
	 * The composite containing view elements.
	 */
	protected Group composite;

	/**
	 * The label in front of the term content label.
	 */
	protected Label termLabel;

	/**
	 * The label in front of the previous term content label
	 */
	protected Label previousTermLabel;

	/**
	 * The label in front of the next term content label;
	 */
	protected Label nextTermLabel;

	/**
	 * The label belonging to the active glossary label.
	 */
	protected Label glossaryLabel;

	/**
	 * The label showing the name of the active glossary.
	 */
	protected Label activeGlossaryLabel;

	/**
	 * The currently active glossary
	 */
	protected Glossary activeGlossary;

	/**
	 * The button for showing the previous entry in the glossary
	 */
	protected Button showPreviousEntryButton;

	/**
	 * The button for showing the next entry in the glossary
	 */
	protected Button showNextEntryButton;

	/**
	 * The term content label.
	 */
	protected Label termContentLabel;

	/**
	 * The previous term content label.
	 */
	protected Label previousTermContentLabel;

	/**
	 * The next term content label.
	 */
	protected Label nextTermContentLabel;

	/**
	 * The abbreviations content label.
	 */
	protected Label abbreviationsContentLabel;

	/**
	 * The label in front of the abbreviations content label.
	 */
	protected Label abbreviationsLabel;

	/**
	 * The label in front of the synonyms content label.
	 */
	protected Label synonymsLabel;

	/**
	 * The synonyms content label.
	 */
	protected Label synonymsContentLabel;

	/**
	 * The label in front of the description label.
	 */
	protected Label definitionLabel;

	/**
	 * The description content label.
	 */
	protected Label definitionContentLabel;

	protected Button openEditorButton;

	protected GlossaryEntry currentGlossaryEntry;

	protected int currentEntryPosition;

	/**
	 * Instantiates necessary objects for this view, such as the presenter.
	 */
	public GlossaryViewImpl() {
		presenter = new GlossaryViewPresenter(this);
	}

	@Override
	public void createPartControl(final Composite parent) {
		composite = new Group(parent, SWT.NONE);
		composite.setText("Glossary entry");

		super.createPartControl(parent);
	}

	/**
	 * Informs the presenter that a glossary entry has been selected to be shown
	 * in the view.
	 *
	 * @param entry
	 *            the entry to be shown in the view
	 */
	@Override
	public void glossaryEntrySelected(final GlossaryEntry entry) {
		((GlossaryViewPresenter) presenter).glossaryEntrySelected(entry);
	}

	/**
	 * Shows a given glossary entry in this view.
	 *
	 * @param term
	 *            the term to be shown
	 * @param definition
	 *            the definition of the term
	 * @param abbreviations
	 *            list of abbreviations for the term
	 * @param synonyms
	 *            list of synonyms for the term
	 */
	protected void showGlossaryEntry(final GlossaryEntry entry) {
		currentGlossaryEntry = entry;
		currentEntryPosition = entry.getParent().getContents().indexOf(entry);

		Composite parent = composite.getParent();
		composite.dispose();
		composite = new Group(parent, SWT.NONE);
		composite.setText("Glossary entry");

		// since an entry's parent can only be a glossary:
		activeGlossary = (Glossary) entry.getParent();

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		glossaryLabel = new Label(composite, SWT.NONE);
		glossaryLabel.setText("Currently active glossary: ");

		activeGlossaryLabel = new Label(composite, SWT.NONE);
		activeGlossaryLabel.setText(activeGlossary.getName());

		GridData buttonLayoutData = new GridData();
		buttonLayoutData.horizontalSpan = 2;
		buttonLayoutData.grabExcessHorizontalSpace = true;
		buttonLayoutData.horizontalAlignment = SWT.FILL;
		buttonLayoutData.verticalAlignment = SWT.END;

		showPreviousEntryButton = new Button(composite, SWT.NONE);
		int previousEntry = currentEntryPosition - 1;
		if (previousEntry < 0) {
			previousEntry = activeGlossary.getContents().size() - 1;
		}
		showPreviousEntryButton.setText("<< "
				+ ((GlossaryEntry) activeGlossary.getContents().get(
						previousEntry)).getName());
		showPreviousEntryButton.setLayoutData(buttonLayoutData);
		showPreviousEntryButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				((GlossaryViewPresenter) presenter)
						.showPreviousEntryButtonPressed();
			}
		});

		termLabel = new Label(composite, SWT.NONE);
		termLabel.setText("Term:");

		termContentLabel = new Label(composite, SWT.NONE);
		termContentLabel.setText(entry.getName());

		abbreviationsLabel = new Label(composite, SWT.NONE);
		abbreviationsLabel.setText("Abbreviations:");

		abbreviationsContentLabel = new Label(composite, SWT.NONE);
//		String abbreviationsContent = "";
//		for (int i = 0; i < entry.getAbbreviations().size(); i++) {
//			abbreviationsContent += entry.getAbbreviations().get(i);
//
//			if (i < entry.getAbbreviations().size() - 1) {
//				abbreviationsContent += ", ";
//			}
//		}
		abbreviationsContentLabel.setText(entry.getLabel());

		synonymsLabel = new Label(composite, SWT.NONE);
		synonymsLabel.setText("Synonyms:");

		synonymsContentLabel = new Label(composite, SWT.NONE);
		String synonymsContent = "";
		for (int i = 0; i < entry.getSynonyms().size(); i++) {
			synonymsContent += entry.getSynonyms().get(i);

			if (i < entry.getSynonyms().size() - 1) {
				synonymsContent += ", ";
			}
		}
		synonymsContentLabel.setText(synonymsContent);

		definitionLabel = new Label(composite, SWT.NONE);
		definitionLabel.setText("Description:");

		GridData definitionContentGridData = new GridData();
		definitionContentGridData.horizontalSpan = 2;
		definitionContentGridData.grabExcessHorizontalSpace = true;
		definitionContentGridData.horizontalAlignment = SWT.FILL;
		definitionContentGridData.grabExcessVerticalSpace = true;
		definitionContentGridData.verticalAlignment = SWT.FILL;

		definitionContentLabel = new Label(composite, SWT.WRAP);
		definitionContentLabel.setText(entry.getDescription());
		definitionContentLabel.setLayoutData(definitionContentGridData);

		openEditorButton = new Button(composite, SWT.NONE);
		openEditorButton.setText("Edit entry");
		openEditorButton.setLayoutData(buttonLayoutData);
		openEditorButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				((GlossaryViewPresenter) presenter).openEditorButtonPressed();
			}
		});

		int nextIndex = currentEntryPosition + 1;
		if (nextIndex == activeGlossary.getContents().size()) {
			nextIndex = 0;
		}
		showNextEntryButton = new Button(composite, SWT.NONE);
		showNextEntryButton.setText(((GlossaryEntry) activeGlossary
				.getContents().get(nextIndex)).getName() + " >>");
		showNextEntryButton.setLayoutData(buttonLayoutData);
		showNextEntryButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				((GlossaryViewPresenter) presenter)
						.showNextEntryButtonPressed();
			}
		});

		composite.layout();
		composite.getParent().layout();
	}

}
