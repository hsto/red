package dk.dtu.imm.red.specificationelements.glossary.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryFactory;

public class CreateNewGlossaryOperation extends AbstractOperation {

	protected String glossaryName;
	protected Group glossaryParent;
	protected Glossary newGlossary;
	protected String glossaryPath;

	public CreateNewGlossaryOperation(String glossaryName,
			Group glossaryParent, String path) {
		super("Create new glossary");

		this.glossaryName = glossaryName;
		this.glossaryParent = glossaryParent;
		this.glossaryPath = path;
	}

	@Override
	public IStatus execute(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		return redo(monitor, info);
	}

	@Override
	public IStatus redo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		if (glossaryParent == null) {
			glossaryParent = FileFactory.eINSTANCE.createFile();
			glossaryParent.setUri(glossaryPath);
			glossaryParent.setName(glossaryPath.substring(glossaryPath
					.lastIndexOf(java.io.File.separator) + 1));
			WorkspaceFactory.eINSTANCE.getWorkspaceInstance().addOpenedFile(
					(File) glossaryParent);
		}
		if (newGlossary == null) {
			newGlossary = GlossaryFactory.eINSTANCE.getGlossaryManager()
					.createGlossary(glossaryName, glossaryParent, glossaryPath);
		} else {
			glossaryParent.getContents().add(newGlossary);
		}

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {
		if (glossaryParent != null) {
			glossaryParent.getContents().remove(newGlossary);
		}

		glossaryParent.save();

		return Status.OK_STATUS;
	}

}
