package dk.dtu.imm.red.specificationelements.glossary.ui.editors;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface GlossaryEntryEditor extends IBaseEditor {

	/**
	 * The ID of this editor.
	 */
	String ID = "dk.dtu.imm.red.specificationelements.glossary.entryeditor";

	void setSynonymsString(String synonymsString);
	
	void setAntonymsString(String antonymsString);
	
	void setExamplesString(String examplesString);

}
