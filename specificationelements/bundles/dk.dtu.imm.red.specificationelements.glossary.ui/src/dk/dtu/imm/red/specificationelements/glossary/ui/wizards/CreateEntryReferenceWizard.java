package dk.dtu.imm.red.specificationelements.glossary.ui.wizards;

import org.eclipse.ui.INewWizard;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface CreateEntryReferenceWizard extends INewWizard, IBaseWizard {

}
