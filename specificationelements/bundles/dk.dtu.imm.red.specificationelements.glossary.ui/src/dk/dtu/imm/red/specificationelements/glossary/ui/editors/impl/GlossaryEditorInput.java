package dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;


public final class GlossaryEditorInput extends BaseEditorInput {

	public GlossaryEditorInput(final Glossary glossary) {
		super(glossary);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Glossary";
	}

	@Override
	public String getToolTipText() {
		return "Editor for glossary";
	}
}
