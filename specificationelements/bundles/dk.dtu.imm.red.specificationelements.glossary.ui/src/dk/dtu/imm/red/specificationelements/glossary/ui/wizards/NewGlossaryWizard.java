package dk.dtu.imm.red.specificationelements.glossary.ui.wizards;

import org.eclipse.ui.INewWizard;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

/**
 * Interface for the New Glossary Wizard. This wizard is responsible for helping
 * the user create a new glossary.
 * 
 * It is of type INewWizard, so it shows up when running the
 * org.eclipse.ui.newWizard command.
 * 
 * @author Jakob Kragelund
 * 
 */
public interface NewGlossaryWizard extends INewWizard, IBaseWizard {
	
	String ID = "dk.dtu.imm.red.specificationelements.glossary.newglossarywizard";

	/**
	 * Gets the presenter for this wizard.
	 * 
	 * @return the wizard's presenter
	 */
	NewGlossaryWizardPresenter getPresenter();

}
