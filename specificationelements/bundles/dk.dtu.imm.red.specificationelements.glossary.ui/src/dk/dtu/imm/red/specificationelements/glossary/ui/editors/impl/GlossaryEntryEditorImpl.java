package dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntryKind;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEntryEditor;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEntryEditorPresenter;

/**
 * Editor for editing glossary entries.
 *
 * @author Jakob Kragelund
 * @author Maciej Kucharek
 *
 */
public final class GlossaryEntryEditorImpl extends BaseEditor implements
		GlossaryEntryEditor {

	protected GlossaryEntryBasicInfoContainer basicInfoContainer;


	protected Label synonymsLabel;
	protected Button manageSynonymsButton;
	protected Label antonymsLabel;
	protected Button manageAntonymsButton;
	protected Label examplesLabel;
	protected Button manageExamplesButton;

	protected Label discussionLabel;
	protected RichTextEditor discussionEditor;

	protected Label synonymsListLabel;
	protected Label antonymsListLabel;
	protected Label examplesListLabel;

	protected int glossaryEntryPageIndex = 0;

	@Override
	public void init(final IEditorSite site, final IEditorInput input)
			throws PartInitException {
		super.init(site, input);

		presenter = new GlossaryEntryEditorPresenterImpl(this,
				(GlossaryEntry) element);

	}

	@Override
	public boolean isSaveAsAllowed() {
		// Save As not allowed
		return false;
	}

	@Override
	public void doSaveAs() {
		// do nothing as Save As is not allowed
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {

		GlossaryEntryEditorPresenter presenter = (GlossaryEntryEditorPresenter) this.presenter;

		//set the spec elements basic info into the presenter
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getPartOf(), basicInfoContainer.getKindOf(),
				basicInfoContainer.getDescription());
		presenter.setDiscussion(discussionEditor.getText());
		super.doSave(monitor);
	}

	@Override
	protected void createPages() {
		glossaryEntryPageIndex = addPage(createEntryPage(getContainer()));
		setPageText(glossaryEntryPageIndex, "Summary");
		super.createPages();

		fillInitialData();
	}

	private Composite createEntryPage(final Composite parent) {

		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(3, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=3;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new GlossaryEntryBasicInfoContainer(composite,SWT.NONE, this, (Element) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);

		synonymsLabel = new Label(composite, SWT.NONE);
		synonymsLabel.setText("Synonyms:");
		
		antonymsLabel = new Label(composite, SWT.NONE);
		antonymsLabel.setText("Antonyms:");
		
		examplesLabel = new Label(composite, SWT.NONE);
		examplesLabel.setText("Examples:");

		GridData listGridData = new GridData();
		listGridData.horizontalSpan = 1;
		listGridData.verticalAlignment = SWT.FILL;
		listGridData.horizontalAlignment = SWT.FILL;
		listGridData.widthHint = 290;
		listGridData.heightHint = 60;
		listGridData.grabExcessHorizontalSpace = true;

		synonymsListLabel = new Label(composite, SWT.BORDER | SWT.FLAT | SWT.WRAP);
		synonymsListLabel.setLayoutData(listGridData);
		
		antonymsListLabel = new Label(composite, SWT.BORDER | SWT.FLAT | SWT.WRAP);
		antonymsListLabel.setLayoutData(listGridData);
		
		examplesListLabel = new Label(composite, SWT.BORDER | SWT.FLAT | SWT.WRAP);
		examplesListLabel.setLayoutData(listGridData);

		manageSynonymsButton = new Button(composite, SWT.NONE);
		manageSynonymsButton.setText("Add/Delete Synonyms");
		manageSynonymsButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				((GlossaryEntryEditorPresenter) presenter).manageSynonymsButtonPressed();
			}
		});
		
		manageAntonymsButton = new Button(composite, SWT.NONE);
		manageAntonymsButton.setText("Add/Delete Antonyms");
		manageAntonymsButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				((GlossaryEntryEditorPresenter) presenter).manageAntonymsButtonPressed();
			}
		});
		
		manageExamplesButton = new Button(composite, SWT.NONE);
		manageExamplesButton.setText("Add/Delete Examples");
		manageExamplesButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				((GlossaryEntryEditorPresenter) presenter).manageExamplesButtonPressed();
			}
		});


		Label discussionTextLabel = new Label(composite, SWT.NONE);
		discussionTextLabel.setText("Discussion:");

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalSpan = 3;
		editorLayoutData.widthHint = 400;
		editorLayoutData.heightHint = 200;
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessVerticalSpace = true;

		discussionEditor = new RichTextEditor(composite, SWT.BORDER |
				SWT.WRAP | SWT.V_SCROLL, getEditorSite(), commandListener);
		discussionEditor.setLayoutData(editorLayoutData);

		discussionEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		return SWTUtils.wrapInScrolledComposite(parent, composite);//

	}



	@Override
	public void setSynonymsString(String synonymsString) {
		synonymsListLabel.setText(synonymsString);
	}
	
	@Override
	public void setAntonymsString(String antonymsString) {
		antonymsListLabel.setText(antonymsString);
	}
	
	@Override
	public void setExamplesString(String examplesString) {
		examplesListLabel.setText(examplesString);
	}

	@Override
	public Element getEditorElement() {
		return element;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/GlossaryEntry.html";
	}

	@Override
	protected void fillInitialData() {
		GlossaryEntryEditorPresenter entryPresenter = (GlossaryEntryEditorPresenter) presenter;

		GlossaryEntry entry = (GlossaryEntry) element;

		GlossaryEntryKind[] gKinds = GlossaryEntryKind.values();
		String[] gKindString = new String[gKinds.length];
		for(int i=0;i<gKinds.length;i++){
			gKindString[i] = gKinds[i].getName();
		}

		basicInfoContainer.fillInitialData(element, gKindString);

		if (entryPresenter.getSynonymsList() != null) {
			synonymsListLabel.setText(entryPresenter.getSynonymsList());
		}
		
		if (entryPresenter.getAntonymsList() != null) {
			antonymsListLabel.setText(entryPresenter.getAntonymsList());
		}
		
		if (entryPresenter.getExamplesList() != null) {
			examplesListLabel.setText(entryPresenter.getExamplesList());
		}

		if (entry.getDiscussion() != null) {
			discussionEditor.setText(entry.getDiscussion().toString());
		}

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE
				.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();
		addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);
		attributesToSearch.put(GlossaryPackage.Literals.GLOSSARY_ENTRY__DISCUSSION,
				discussionEditor.getText().toString());
		
		if(basicInfoContainer.getPartOf()!=null){
			attributesToSearch.put(GlossaryPackage.Literals.GLOSSARY_ENTRY__PART_OF,
			basicInfoContainer.getPartOf().getLabel());
		}	
		if(basicInfoContainer.getKindOf()!=null){
			attributesToSearch.put(GlossaryPackage.Literals.GLOSSARY_ENTRY__PART_OF,
			basicInfoContainer.getKindOf().getLabel());
		}
		
		// Now, search through all attributes, recording all search hits along
		// the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch
				.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE
						.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()
						- 1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(),
						result.getEndIndex());
			}
		}
		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}

	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,glossaryEntryPageIndex,basicInfoContainer);
		if (feature == GlossaryPackage.Literals.GLOSSARY_ENTRY__DISCUSSION) {
			setActivePage(glossaryEntryPageIndex);
			discussionEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] { "" + startIndex, "" + endIndex });
		}
		else if (feature == GlossaryPackage.Literals.GLOSSARY_ENTRY__PART_OF) {
			setActivePage(glossaryEntryPageIndex);
			basicInfoContainer.getPartOfTextBox().setSelection(startIndex, endIndex);
		}
		else if (feature == GlossaryPackage.Literals.GLOSSARY_ENTRY__KIND_OF) {
			setActivePage(glossaryEntryPageIndex);
			basicInfoContainer.getKindOfTextBox().setSelection(startIndex, endIndex);
		}
		
	}

	@Override
	public String getEditorID() {
		return ID;
	}

}
