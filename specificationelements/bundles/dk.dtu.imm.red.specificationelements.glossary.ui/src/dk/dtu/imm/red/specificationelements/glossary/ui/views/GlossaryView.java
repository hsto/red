package dk.dtu.imm.red.specificationelements.glossary.ui.views;

import org.eclipse.ui.IViewPart;

import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;

public interface GlossaryView extends IViewPart {

	/**
	 * The ID of this view.
	 */
	final String ID = "dk.dtu.imm.red.specificationelements.glossary.glossaryview";

	void glossaryEntrySelected(final GlossaryEntry entry);

}
