package dk.dtu.imm.red.specificationelements.glossary.ui.views;

import org.eclipse.ui.IViewPart;

import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;

public interface GlossaryViewNew extends IViewPart {

	
	final String ID = "dk.dtu.imm.red.specificationelements.glossary.glossaryviewnew";
	
	void glossaryEntrySelected(final GlossaryEntry entry);
}
