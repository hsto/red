package dk.dtu.imm.red.specificationelements.glossary.ui.editors;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;

public interface GlossaryEntryEditorPresenter extends IEditorPresenter {

	void manageSynonymsButtonPressed();
	
	void manageAntonymsButtonPressed();
	
	void manageExamplesButtonPressed();

	String getSynonymsList();
	
	String getAntonymsList();
	
	String getExamplesList();

	void setDiscussion(String text);

	void setSpecElementBasicInfo(String label, String name, String kind, Element partOf, Element subtypeOf, String description);

}
