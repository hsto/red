package dk.dtu.imm.red.specificationelements.glossary.ui.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;

/**
 * Presenter for the Manage String List dialog.
 * 
 * @author Jakob Kragelund
 * 
 */
public final class ManageStringListDialogPresenter {

	/**
	 * The dialog for this presenter.
	 */
	ManageStringListDialog dialog;

	/**
	 * The strings shown in the list widget.
	 */
	List<String> stringList;

	/**
	 * Create a presenter for the given view.
	 * 
	 * @param dialog
	 *            the dialog for this presenter
	 * @param stringListContent
	 *            the content to show in the list widget
	 */
	protected ManageStringListDialogPresenter(
			final ManageStringListDialog dialog,
			final List<String> stringListContent) {
		this.stringList = stringListContent;
		this.dialog = dialog;
	}

	/**
	 * Initialize the dialog.
	 */
	protected void initialiseDialog() {
		dialog.addStringButton.setEnabled(false);
		dialog.removeStringButton.setEnabled(false);

		for (String string : stringList) {
			dialog.stringList.add(string);
		}
	}

	/**
	 * Called when the Remove String Button is pressed in the dialog.
	 */
	public void removeStringButtonPressed() {
		int selectionIndex = dialog.stringList.getSelectionIndex();
		if (selectionIndex != -1) {
			stringList.remove(selectionIndex);
			dialog.stringList.remove(selectionIndex);
			dialog.stringList.notifyListeners(SWT.Selection, null);
		}
		dialog.getButton(IDialogConstants.OK_ID).setEnabled(true);
	}

	/**
	 * Called when the Add String Button is pressed in the dialog.
	 */
	public void addStringButtonPressed() {
		dialog.stringList.add(dialog.newStringText.getText());
		stringList.add(dialog.newStringText.getText());
		dialog.newStringText.setText("");
		dialog.getButton(IDialogConstants.OK_ID).setEnabled(true);
		dialog.newStringText.notifyListeners(SWT.Modify, null);
	}

	/**
	 * Returns a copy of the string content list.
	 * 
	 * @return copy of the string list
	 */
	public List<String> getStringListContents() {
		return new ArrayList<String>(stringList);
	}

}
