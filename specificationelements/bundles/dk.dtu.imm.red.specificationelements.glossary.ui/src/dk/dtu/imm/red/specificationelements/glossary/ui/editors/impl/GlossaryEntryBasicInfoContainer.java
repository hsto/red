package dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl;

import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wb.swt.ResourceManager;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizard;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardPresenter;
import dk.dtu.imm.red.core.ui.element.wizards.impl.CreateElementReferenceWizardImpl;
import dk.dtu.imm.red.core.ui.internal.element.extensions.ElementLinkActionExtension;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;

public class GlossaryEntryBasicInfoContainer extends ElementBasicInfoContainer {
	private Text txtLabel;
	private Text txtName;
	private Combo cbKind;
	private Text txtPartOf;
	private Element elPartOf;
	private Element elKindOf;
	private RichTextEditor rteDescription;
	private Text txtKindOf;

	/**
	 * Create the composite.
	 *
	 * @param parent
	 * @param style
	 */
	public GlossaryEntryBasicInfoContainer(Composite parent, int style,	final BaseEditor editor, final Element specElement) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		Group grpBasicinfo = new Group(this, SWT.NONE);
		GridData gd_grpBasicinfo = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_grpBasicinfo.heightHint = 317;
		gd_grpBasicinfo.widthHint = 560;
		grpBasicinfo.setLayoutData(gd_grpBasicinfo);
		grpBasicinfo.setText("Basics");
		grpBasicinfo.setLayout(new GridLayout(10, false));

		Label lblLabel = new Label(grpBasicinfo, SWT.NONE);
		lblLabel.setText("Label");

		txtLabel = new Text(grpBasicinfo, SWT.BORDER);
		GridData gd_txtLabel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtLabel.horizontalSpan = 3;
		gd_txtLabel.widthHint = 80;
		gd_txtLabel.minimumWidth = 50;
		txtLabel.setLayoutData(gd_txtLabel);
		txtLabel.addListener(SWT.Modify, editor.getModifyListener());

		Label lblName = new Label(grpBasicinfo, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,	1, 1));
		lblName.setText("Name");

		txtName = new Text(grpBasicinfo, SWT.BORDER);
		GridData gd_txtName = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtName.horizontalSpan = 3;
		gd_txtName.widthHint = 350;
		gd_txtName.minimumWidth = 100;
		txtName.setLayoutData(gd_txtName);
		txtName.addListener(SWT.Modify, editor.getModifyListener());

		Label lblKind = new Label(grpBasicinfo, SWT.NONE);
		lblKind.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblKind.setText("Kind");

		cbKind = new Combo(grpBasicinfo, SWT.NONE);		
		GridData gd_cbKind = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,	1);
		gd_cbKind.widthHint = 20;
		cbKind.setLayoutData(gd_cbKind);
		cbKind.addListener(SWT.Modify, editor.getModifyListener());

		Label lblPartof = new Label(grpBasicinfo, SWT.NONE);
		lblPartof.setText("Part of");

		txtPartOf = new Text(grpBasicinfo, SWT.BORDER);
		txtPartOf.setEditable(false);
		GridData gd_txtPartOf = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_txtPartOf.widthHint = 40;
		txtPartOf.setLayoutData(gd_txtPartOf);
		txtPartOf.addListener(SWT.Modify, editor.getModifyListener());

		Button btnChoosePartOf = new Button(grpBasicinfo, SWT.NONE);
		btnChoosePartOf.setText("...");

		btnChoosePartOf.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				CreateElementReferenceWizard wizard = new CreateElementReferenceWizardImpl(
						new Class[] {});
				wizard.init(PlatformUI.getWorkbench(),
						(IStructuredSelection) PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow()
								.getSelectionService().getSelection());

				WizardDialog dialog = new WizardDialog(Display.getCurrent()
						.getActiveShell(), wizard);

				dialog.open();

				Element selectedElement = (Element) ((CreateElementReferenceWizardPresenter) wizard
						.getPresenter()).getselectedElement();
				if (selectedElement != null) {
					elPartOf = selectedElement;
					txtPartOf.setText(selectedElement.getLabel() + " - "
							+ selectedElement.getName());
				} else {
					elPartOf = null;
					txtPartOf.setText("");
				}
			}
		});

		Button btnOpen = new Button(grpBasicinfo, SWT.NONE);
		btnOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		GridData gd_btnOpen = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnOpen.widthHint = 22;
		btnOpen.setLayoutData(gd_btnOpen);
		btnOpen.setImage(ResourceManager.getPluginImage("dk.dtu.imm.red.core",
				"icons/navigate_forward.png"));

		btnOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (elPartOf != null) {
					ElementLinkActionExtension.elementClicked(elPartOf);
				}
			}
		});

		
		Label lblSubtypeOf = new Label(grpBasicinfo, SWT.NONE);
		lblSubtypeOf.setText("Kind of");

		txtKindOf = new Text(grpBasicinfo, SWT.BORDER);
		txtKindOf.setEditable(false);
		GridData gd_txtSubtypeOf = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		gd_txtSubtypeOf.widthHint = 40;
		txtKindOf.setLayoutData(gd_txtSubtypeOf);
		txtKindOf.addListener(SWT.Modify, editor.getModifyListener());
		
		Button btnChooseSubtypeOf = new Button(grpBasicinfo, SWT.NONE);
		btnChooseSubtypeOf.setText("...");

		btnChooseSubtypeOf.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				CreateElementReferenceWizard wizard = new CreateElementReferenceWizardImpl(
						new Class[] { specElement.getClass() });
				wizard.init(PlatformUI.getWorkbench(),
						(IStructuredSelection) PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow()
								.getSelectionService().getSelection());

				WizardDialog dialog = new WizardDialog(Display.getCurrent()
						.getActiveShell(), wizard);

				dialog.open();

				Element selectedElement = (Element) ((CreateElementReferenceWizardPresenter) wizard
						.getPresenter()).getselectedElement();
				if (selectedElement != null
						&& selectedElement.getClass().isInstance(specElement)) {
					elKindOf = selectedElement;
					txtKindOf.setText(selectedElement.getLabel() + " - "
							+ selectedElement.getName());
				} else {
					elKindOf = null;
					txtKindOf.setText("");
				}
			}
		});

		Button btnOpenSubtypeOf = new Button(grpBasicinfo, SWT.NONE);
		GridData gd_btnOpenSubtypeOf = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_btnOpenSubtypeOf.widthHint = 22;
		btnOpenSubtypeOf.setLayoutData(gd_btnOpenSubtypeOf);
		btnOpenSubtypeOf.setImage(ResourceManager.getPluginImage(
				"dk.dtu.imm.red.core", "icons/navigate_forward.png"));
		btnOpenSubtypeOf.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (elKindOf != null) {
					ElementLinkActionExtension.elementClicked(elKindOf);
				}
			}
		});
		
		new Label(grpBasicinfo, SWT.NONE);
		
		Button btnExplain = new Button(grpBasicinfo, SWT.NONE);
		btnExplain.setText("Explain");
		GridData gd_btnExplain = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_btnExplain.widthHint = 120;
		gd_btnExplain.minimumWidth = 50;
		btnExplain.setLayoutData(gd_btnExplain);
		btnExplain.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				IWorkbench wb = PlatformUI.getWorkbench();
				IWorkbenchWindow window = wb.getActiveWorkbenchWindow();
				IWorkbenchPage page = window.getActivePage();
				IEditorPart editor = page.getActiveEditor();
				PlatformUI.getWorkbench().getHelpSystem().displayHelpResource(((BaseEditor) editor).getHelpResourceURL());
			}
		});
		

		Label lblDescription = new Label(grpBasicinfo, SWT.NONE);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,	false, 1, 1));
		lblDescription.setText("Description");

		rteDescription = new RichTextEditor(grpBasicinfo, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL, editor.getEditorSite() , editor.commandListener);
		GridData gd_txtDescription = new GridData(SWT.FILL, SWT.FILL, true, true, 9, 9);
		gd_txtDescription.widthHint = 215;
		gd_txtDescription.heightHint = 70;
		rteDescription.setLayoutData(gd_txtDescription);
		//rteDescription.addListener(SWT.Modify, editor.getModifyListener());
		ModifyListener modifyList = new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				editor.getModifyListener().handleEvent(null);
			}
		};
		rteDescription.addModifyListener(modifyList);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public void setLabel(String label) {
		txtLabel.setText(label);
	}

	public String getLabel() {
		return txtLabel.getText();
	}

	public Text getLabelTextBox() {
		return txtLabel;
	}

	public void setName(String name) {
		txtName.setText(name);
	}

	public String getName() {
		return txtName.getText();
	}

	public Text getNameTextBox() {
		return txtName;
	}

	public void setKindItems(String[] items) {
		cbKind.setItems(items);
	}

	public void setKind(String item) {
		String[] items = cbKind.getItems();
		for (int i = 0; i < items.length; i++) {
			if (item.equals(items[i])) {
				cbKind.select(i);
			}
		}
	}

	public String getKind() {
		if (cbKind.getSelectionIndex() >= 0) {
			String[] items = cbKind.getItems();
			return items[cbKind.getSelectionIndex()];
		}
		return "";
	}

	public Combo getKindComboBox() {
		return cbKind;
	}

	public void setPartOf(Element el) {
		txtPartOf.setText(el.getLabel() + " - " + el.getName());
		elPartOf = el;
	}

	public Element getPartOf() {
		return elPartOf;
	}

	public Text getPartOfTextBox() {
		return txtPartOf;
	}

	public void setKindOf(Element el) {
		txtKindOf.setText(el.getLabel() + " - " + el.getName());
		elKindOf = el;
	}


	public Element getKindOf() {
		return elKindOf;
	}

	public Text getKindOfTextBox() {
		return txtKindOf;
	}

	public void setDescription(String description) {
		rteDescription.setText(description);
	}

	public String getDescription() {
		return rteDescription.getText();
	}

	public RichTextEditor getDescriptionRTE() {
		return rteDescription;
	}

	public void fillInitialData(Element element, String[] elementKinds) {
		GlossaryEntry glossaryEntry = (GlossaryEntry) element;

		if (elementKinds.length > 0) {
			cbKind.setEnabled(true);
			setKindItems(elementKinds);
		} else {
			setKindItems(new String[] { "unspecified" });
			cbKind.setEnabled(false);
		}

		// fill basic info
		if (glossaryEntry.getLabel() != null) {
			setLabel(glossaryEntry.getLabel());
		}
		if (glossaryEntry.getName() != null) {
			setName(glossaryEntry.getName());
		}
		if (glossaryEntry.getElementKind() != null
				&& !glossaryEntry.getElementKind().isEmpty()) {
			setKind(glossaryEntry.getElementKind());
		} else {
			setKind("unspecified");
		}
		if (glossaryEntry.getPartOf() != null) {
			setPartOf(glossaryEntry.getPartOf());
		}
		if (glossaryEntry.getKindOf() != null) {
			setKindOf(glossaryEntry.getKindOf());
		}
		if (glossaryEntry.getDescription() != null) {
			setDescription(glossaryEntry.getDescription());
		}
	}
}
