package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.element.State;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.ui.handlers.CreateNewGlossaryHandler;
import dk.dtu.imm.red.specificationelements.glossary.ui.operations.CreateGlossaryEntryOperation;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.NewGlossaryEntryWizardPresenter;

public class NewGlossaryEntryWizardPresenterImpl extends BaseWizardPresenter
		implements NewGlossaryEntryWizardPresenter {

	protected String label;
	protected String name;
	protected String description;
//	protected String term;
//	protected Text definition;
	protected List<String> synonyms;
//	protected List<String> abbreviations;
	protected Glossary parent;
	protected State status;

//	@Override
//	public String getNewGlossaryEntryTerm() {
//		return term;
//	}
//
//	@Override
//	public Text getNewGlossaryEntryDefinition() {
//		return definition;
//	}

	@Override
	public List<String> getNewGlossaryEntrySynonyms() {
		return synonyms;
	}

//	@Override
//	public List<String> getNewGlossaryEntryAbbreviations() {
//		return abbreviations;
//	}

	@Override
	public Glossary getNewGlossaryEntryParent() {
		return parent;
	}

//	public void wizardFinished(final String term, final String definition,
//			final Glossary parent) {
//		this.term = term;
//		this.definition = TextFactory.eINSTANCE.createText(definition);
//		this.parent = parent;
//
//		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
//				.getOperationSupport();
//
//		CreateGlossaryEntryOperation operation = new CreateGlossaryEntryOperation(
//				term, this.definition, parent);
//
//		operation.addContext(operationSupport.getUndoContext());
//
//		IAdaptable info = new IAdaptable() {
//			@Override
//			public Object getAdapter(
//					@SuppressWarnings("rawtypes") final Class adapter) {
//				if (Shell.class.equals(adapter)) {
//					return Display.getCurrent().getActiveShell();
//				}
//				return null;
//			}
//		};
//
//		try {
//			OperationHistoryFactory.getOperationHistory().execute(operation,
//					null, info);
//		} catch (ExecutionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace(); LogUtil.logError(e);
//		}
//	}

	@Override
	public void newGlossaryButtonPressed() {
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();

		IHandlerService handlerService = (IHandlerService) window
				.getService(IHandlerService.class);

		try {
			handlerService.executeCommand(CreateNewGlossaryHandler.ID, null);
		} catch (ExecutionException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (NotDefinedException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (NotEnabledException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (NotHandledException e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

	@Override
	public void wizardFinished(String label, String name, String description,
			Group parent, String path) {
		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = (Glossary) parent;

		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
				.getOperationSupport();

		CreateGlossaryEntryOperation operation = new CreateGlossaryEntryOperation(
				label, name, description, this.parent);

		operation.addContext(operationSupport.getUndoContext());

		IAdaptable info = new IAdaptable() {
			@Override
			public Object getAdapter(
					@SuppressWarnings("rawtypes") final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		try {
			OperationHistoryFactory.getOperationHistory().execute(operation,
					null, info);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

}
