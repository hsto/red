package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import java.util.List;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryFactory;
import dk.dtu.imm.red.specificationelements.glossary.ui.handlers.CreateGlossaryEntryHandler;
import dk.dtu.imm.red.specificationelements.glossary.ui.handlers.CreateNewGlossaryHandler;

/**
 * The wizard page used for creating a new reference to a glossary entry. The
 * user can select a glossary from one list, which will show all the entries in
 * that glossary in a second list. When selecting an entry from the second list,
 * the page is completed.
 * 
 * The page contains buttons which open the wizards for creating new glossaries
 * and entries.
 * 
 * @author Jakob Kragelund
 * 
 */
public class CreateEntryReferencePageImpl extends WizardPage {

	protected Label glossaryLabel;
	protected Table glossaryTable;

	protected Label entryLabel;
	protected Table entryTable;

	protected Button createGlossaryButton;
	protected Button createEntryButton;

	protected String descriptionPartOne;
	protected String descriptionPartTwoA;
	protected String descriptionPartTwoB;

	/**
	 * Listener for the Create Glossary/Entry buttons, which executes the
	 * corresponding commands.
	 */
	private Listener createButtonListener = new Listener() {
		@Override
		public void handleEvent(final Event event) {
			if (event.widget != createGlossaryButton
					&& event.widget != createEntryButton) {
				event.doit = false;
				return;
			}

			IHandlerService handlerService = (IHandlerService) PlatformUI
					.getWorkbench().getService(IHandlerService.class);

			try {
				if (event.widget == createGlossaryButton) {
					handlerService.executeCommand(CreateNewGlossaryHandler.ID,
							null);
					updateGlossaryTable();
				} else if (event.widget == createEntryButton) {
					handlerService.executeCommand(
							CreateGlossaryEntryHandler.ID, null);
					updateEntryTable();
				}
			} catch (Exception e) {
				e.printStackTrace(); LogUtil.logError(e);
			}

		}
	};

	/**
	 * Creates the wizard page used for creating a new reference to a glossary
	 * entry. Sets the title of the page and a thorough description.
	 */
	public CreateEntryReferencePageImpl() {
		super("Create Glossary Entry Reference");

		descriptionPartOne = "Use this page to create a reference to "
				+ "an entry in the glossary.\n";

		descriptionPartTwoA = "First, select the correct glossary "
				+ "from the list on the left.";
		descriptionPartTwoB = "Now, select the desired entry from "
				+ "the list on the right";

		setTitle("Create Glossary Entry Reference");

		setDescription(descriptionPartOne + descriptionPartTwoA);
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(2, true);
		container.setLayout(layout);

		glossaryLabel = new Label(container, SWT.NONE);
		glossaryLabel.setText("1st, select a glossary:");

		entryLabel = new Label(container, SWT.NONE);
		entryLabel.setText("2nd, select an entry:");

		GridData listLayoutData = new GridData();
		listLayoutData.grabExcessHorizontalSpace = true;
		listLayoutData.grabExcessVerticalSpace = true;
		listLayoutData.minimumWidth = 200;
		listLayoutData.minimumHeight = 200;

		Composite glossaryTableContainer = new Composite(container, SWT.NONE);
		glossaryTableContainer.setLayoutData(listLayoutData);

		glossaryTable = new Table(glossaryTableContainer, SWT.BORDER
				| SWT.SINGLE | SWT.FULL_SELECTION);
		glossaryTable.setLinesVisible(true);
		glossaryTable.setHeaderVisible(false);
		glossaryTable.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				updateEntryTable();
			}
		});
		TableColumn glossaryColumn = new TableColumn(glossaryTable, SWT.NONE);
		updateGlossaryTable();

		Composite entryTableContainer = new Composite(container, SWT.NONE);
		entryTableContainer.setLayoutData(listLayoutData);

		entryTable = new Table(entryTableContainer, SWT.BORDER | SWT.SINGLE
				| SWT.FULL_SELECTION);
		entryTable.setLinesVisible(true);
		entryTable.setHeaderVisible(false);
		entryTable.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				if (entryTable.getSelectionCount() > 0) {
					setPageComplete(true);
				} else {
					setPageComplete(false);
				}
			}
		});
		TableColumn entryColumn = new TableColumn(entryTable, SWT.NONE);
		updateEntryTable();

		TableColumnLayout tableColumnLayout = new TableColumnLayout();
		tableColumnLayout.setColumnData(glossaryColumn, new ColumnWeightData(
				100));
		tableColumnLayout.setColumnData(entryColumn, new ColumnWeightData(100));

		glossaryTableContainer.setLayout(tableColumnLayout);
		entryTableContainer.setLayout(tableColumnLayout);

		createGlossaryButton = new Button(container, SWT.NONE);
		createGlossaryButton.setText("New Glossary");
		createGlossaryButton.addListener(SWT.Selection, createButtonListener);

		createEntryButton = new Button(container, SWT.NONE);
		createEntryButton.setText("New Entry");
		createEntryButton.addListener(SWT.Selection, createButtonListener);

		setPageComplete(false);
		setControl(container);
	}

	protected void updateGlossaryTable() {
		Glossary selectedGlossary = null;
		if (glossaryTable.getSelectionCount() > 0) {
			selectedGlossary = (Glossary) glossaryTable.getSelection()[0]
					.getData();
		}

		glossaryTable.removeAll();

		List<Glossary> glossaries = GlossaryFactory.eINSTANCE
				.getGlossaryManager().getGlossaries();

		if (glossaries.size() == 0) {
			TableItem item1 = new TableItem(glossaryTable, SWT.NONE);
			item1.setText("No glossaries exist");
			TableItem item2 = new TableItem(glossaryTable, SWT.NONE);
			item2.setText("Create a new glossary");
			glossaryTable.setEnabled(false);
			return;
		}

		for (Glossary glossary : glossaries) {
			TableItem item = new TableItem(glossaryTable, SWT.NONE);
			item.setData(glossary);
			item.setText(glossary.getName());

			if (selectedGlossary != null && glossary == selectedGlossary) {
				glossaryTable.setSelection(item);
			}
		}

		glossaryTable.setEnabled(true);

	}

	protected void updateEntryTable() {
		GlossaryEntry selectedEntry = null;
		Glossary selectedGlossary = null;

		if (entryTable.getSelectionCount() > 0) {
			selectedEntry = (GlossaryEntry) entryTable.getSelection()[0]
					.getData();
		}

		entryTable.removeAll();

		if (glossaryTable.getSelectionCount() == 0) {
			TableItem item1 = new TableItem(entryTable, SWT.NONE);
			item1.setText("No glossary selected");
			TableItem item2 = new TableItem(entryTable, SWT.NONE);
			item2.setText("Select a glossary");
			entryTable.setEnabled(false);
			entryTable.notifyListeners(SWT.Selection, null);
			setDescription(descriptionPartOne + descriptionPartTwoA);
			return;
		}

		selectedGlossary = (Glossary) glossaryTable.getSelection()[0].getData();

		List<GlossaryEntry> entries = GlossaryFactory.eINSTANCE
				.getGlossaryManager().getGlossaryEntries(selectedGlossary);

		if (entries.size() == 0) {
			TableItem item1 = new TableItem(entryTable, SWT.NONE);
			item1.setText("No entries exist");
			TableItem item2 = new TableItem(entryTable, SWT.NONE);
			item2.setText("Create a new entry");
			entryTable.setEnabled(false);
			entryTable.notifyListeners(SWT.Selection, null);
			return;
		}

		for (GlossaryEntry entry : entries) {
			TableItem item = new TableItem(entryTable, SWT.NONE);
			item.setData(entry);
			item.setText(entry.getName());

			if (selectedEntry != null && entry == selectedEntry) {
				entryTable.setSelection(item);
			}
		}
		setDescription(descriptionPartOne + descriptionPartTwoB);
		entryTable.setEnabled(true);
	}

	public GlossaryEntry getSelectedEntry() {
		if (entryTable.getSelectionCount() > 0) {
			return (GlossaryEntry) entryTable.getSelection()[0].getData();
		}
		return null;
	}

}
