package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.specificationelements.glossary.ui.operations.CreateNewGlossaryOperation;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.NewGlossaryWizardPresenter;

/**
 * Implementation of the presenter for the New Glossary Wizard.
 * 
 * This presenter can be used to get the results of the New Glossary Wizard.
 * 
 * @author Jakob Kragelund
 * 
 */
public class NewGlossaryWizardPresenterImpl extends BaseWizardPresenter
		implements NewGlossaryWizardPresenter {

	/**
	 * The name of a new glossary created by the wizard.
	 */
	protected String newGlossaryName;

	/**
	 * An optional parent glossary for the new glossary.
	 */
	protected Group newGlossaryParent;

	protected String newGlossaryPath;

	@Override
	public Group getNewGlossaryParent() {
		return newGlossaryParent;
	}

	@Override
	public String getNewglossaryName() {
		return newGlossaryName;
	}

	@Override
	public void wizardFinished(final String name, final Group parent,
			final String path) {

		this.newGlossaryName = name;
		this.newGlossaryParent = parent;
		this.newGlossaryPath = path;

		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
				.getOperationSupport();

		CreateNewGlossaryOperation operation = new CreateNewGlossaryOperation(
				name, parent, path);

		operation.addContext(operationSupport.getUndoContext());

		IAdaptable info = new IAdaptable() {
			@SuppressWarnings("rawtypes")
			@Override
			public Object getAdapter(final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		try {
			OperationHistoryFactory.getOperationHistory().execute(operation,
					null, info);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

}
