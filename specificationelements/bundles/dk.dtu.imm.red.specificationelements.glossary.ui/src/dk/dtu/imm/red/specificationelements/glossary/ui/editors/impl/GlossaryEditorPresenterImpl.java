package dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEditorPresenter;


public final class GlossaryEditorPresenterImpl extends BaseEditorPresenter
		implements GlossaryEditorPresenter {

	protected String discussion;

	public GlossaryEditorPresenterImpl(Element element) {
		super(element);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void save() {
		super.save();
		Glossary glossary = (Glossary) element;
		glossary.setDiscussion(TextFactory.eINSTANCE.createText(discussion));

		glossary.save();
	}

	@Override
	public void setDiscussion(String text) {
		this.discussion = text;
	}


}
