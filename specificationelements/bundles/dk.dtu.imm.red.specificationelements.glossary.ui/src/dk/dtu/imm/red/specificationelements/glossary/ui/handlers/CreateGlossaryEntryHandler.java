package dk.dtu.imm.red.specificationelements.glossary.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl.NewGlossaryEntryWizardImpl;

public class CreateGlossaryEntryHandler extends AbstractHandler {

	/**
	 * ID of the command that this handler handles.
	 */
	public static final String ID = "dk.dtu.imm.red.specificationelements.glossary.createentry";

	@Override
	public Object execute(final ExecutionEvent event)
			throws ExecutionException {

		NewGlossaryEntryWizardImpl wizard = new NewGlossaryEntryWizardImpl();
		ISelection currentSelection = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection();
		wizard.init(PlatformUI.getWorkbench(),
				(IStructuredSelection) currentSelection);

		WizardDialog dialog = new WizardDialog(
				HandlerUtil.getActiveShell(event), wizard);
		dialog.create();

		dialog.open();

		return null;
	}

}
