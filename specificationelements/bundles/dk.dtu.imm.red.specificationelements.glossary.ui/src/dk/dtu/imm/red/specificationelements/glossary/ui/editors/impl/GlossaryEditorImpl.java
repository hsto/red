package dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryPackage;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEditor;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEditorPresenter;

public final class GlossaryEditorImpl extends BaseEditor implements
		GlossaryEditor {

	protected ElementBasicInfoContainer basicInfoContainer;

	protected Label discussionLabel;
	protected RichTextEditor discussionEditor;

	protected int glossaryPageIndex = 0;

	@Override
	public void init(final IEditorSite site, final IEditorInput input)
			throws PartInitException {
		super.init(site, input);

		presenter = new GlossaryEditorPresenterImpl((Glossary) element);

	}

	@Override
	public boolean isSaveAsAllowed() {
		// Save As not allowed
		return false;
	}

	@Override
	public void doSaveAs() {
		// do nothing as Save As is not allowed
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {

		GlossaryEditorPresenter presenter = (GlossaryEditorPresenter) this.presenter;

		//set the spec elements basic info into the presenter
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		presenter.setDiscussion(discussionEditor.getText());
		super.doSave(monitor);
	}

	@Override
	protected void createPages() {
		glossaryPageIndex = addPage(createSummaryPage(getContainer()));
		setPageText(glossaryPageIndex, "Summary");
		super.createPages();
		fillInitialData();
	}

	private Composite createSummaryPage(final Composite parent) {

		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (Element) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);

		basicInfoContainer.getLabelTextBox().setEnabled(false);

		Label discussionTextLabel = new Label(composite, SWT.NONE);
		discussionTextLabel.setText("Discussion:");

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalSpan = 2;
		editorLayoutData.widthHint = 400;
		editorLayoutData.heightHint = 200;
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessVerticalSpace = true;

		discussionEditor = new RichTextEditor(composite, SWT.BORDER |
				SWT.WRAP | SWT.V_SCROLL, getEditorSite(), commandListener);
		discussionEditor.setLayoutData(editorLayoutData);

		discussionEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		return SWTUtils.wrapInScrolledComposite(parent, composite);//

	}



	@Override
	public Element getEditorElement() {
		return element;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/GlossaryEntry.html";
	}

	@Override
	protected void fillInitialData() {
		Glossary glossary = (Glossary) element;

		basicInfoContainer.fillInitialData(element,  new String[]{});

		if (glossary.getDiscussion() != null) {
			discussionEditor.setText(glossary.getDiscussion().toString());
		}

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE
				.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();
		addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);
		attributesToSearch.put(GlossaryPackage.Literals.GLOSSARY__DISCUSSION,
				discussionEditor.getText().toString());

		// Now, search through all attributes, recording all search hits along
		// the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch
				.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE
						.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()
						- 1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(),
						result.getEndIndex());
			}
		}
		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}

	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,glossaryPageIndex,basicInfoContainer);
		if (feature == GlossaryPackage.Literals.GLOSSARY__DISCUSSION) {
			setActivePage(glossaryPageIndex);
			discussionEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] { "" + startIndex, "" + endIndex });
		}
	}

	@Override
	public String getEditorID() {
		return ID;
	}


}
