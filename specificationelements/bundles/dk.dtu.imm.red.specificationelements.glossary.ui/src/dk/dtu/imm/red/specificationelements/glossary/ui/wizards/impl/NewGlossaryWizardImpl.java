package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ui.IWorkbench;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.NewGlossaryWizardPresenter;

public class NewGlossaryWizardImpl extends BaseNewWizard implements
		dk.dtu.imm.red.specificationelements.glossary.ui.wizards.NewGlossaryWizard {

	/**
	 * The presenter for this wizard.
	 */
	protected NewGlossaryWizardPresenter presenter;

	/**
	 * Constructs a wizard for creating a new glossary.
	 */
	public NewGlossaryWizardImpl() {
		super("Glossary", Glossary.class,true);
		presenter = new NewGlossaryWizardPresenterImpl();
	}
	
	@Override
	public void addPages() {
//		super.addPages();
		//do not need element basic info page
		elementBasicInfoPage.setPageComplete(true);
		addPage(displayInfoPage);
		
		addPage(chooseDirectoryLocation);

	}	

	@Override
	public void init(final IWorkbench workbench,
			final IStructuredSelection selection) {

	}

	@Override
	public boolean canFinish() {
		return super.canFinish()
				&& displayInfoPage.isPageComplete()
				&& !displayInfoPage.getSelection().isEmpty();
	}

	@Override
	public boolean performFinish() {
		Group selectedParent = null;
		ITreeSelection selection = (ITreeSelection) displayInfoPage
				.getSelection();

		if (selection != null && selection.getFirstElement() instanceof Group) {
			selectedParent = (Group) selection.getFirstElement();
		}

		presenter.wizardFinished(displayInfoPage.getDisplayName(),
				selectedParent, "");

		return true;
	}

	@Override
	public NewGlossaryWizardPresenter getPresenter() {
		return presenter;
	}

	@Override
	public void pageOpenedOrClosed(IWizardPage fromPage) {
		// do nothing
	}

}
