package dk.dtu.imm.red.specificationelements.glossary.ui.editors;

import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;

public interface GlossaryEditorPresenter extends IEditorPresenter {

	void setDiscussion(String text);

}
