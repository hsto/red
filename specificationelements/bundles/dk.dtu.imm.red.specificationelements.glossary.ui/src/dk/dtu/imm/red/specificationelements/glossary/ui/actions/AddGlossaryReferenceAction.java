package dk.dtu.imm.red.specificationelements.glossary.ui.actions;

import org.eclipse.epf.richtext.IRichText;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.actions.RichTextAction;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.ui.Activator;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.CreateEntryReferenceWizard;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.CreateEntryReferenceWizardPresenter;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl.CreateEntryReferenceWizardImpl;

public class AddGlossaryReferenceAction extends RichTextAction {

	public AddGlossaryReferenceAction(IRichText richText) {
		super(richText, IAction.AS_PUSH_BUTTON);

		setToolTipText("Create a glossary reference");
		setImageDescriptor(Activator.getImageDescriptor("/icons/AddLink.gif"));
	}

	@Override
	public void execute(IRichText richText) {
		GlossaryEntry entry = getReferenceToEntry();

		if (entry != null) {
			richText.executeCommand(RichTextCommand.ADD_LINK,
					entry.getUniqueID());
		}

	}

	private GlossaryEntry getReferenceToEntry() {

		CreateEntryReferenceWizard wizard = new CreateEntryReferenceWizardImpl();
		wizard.init(PlatformUI.getWorkbench(),
				(IStructuredSelection) PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getSelectionService()
						.getSelection());

		WizardDialog dialog = new WizardDialog(Display.getCurrent()
				.getActiveShell(), wizard);

		if (dialog.open() == WizardDialog.CANCEL) {
			return null;
		}

		return ((CreateEntryReferenceWizardPresenter) wizard.getPresenter())
				.getSelectedEntry();
	}

}
