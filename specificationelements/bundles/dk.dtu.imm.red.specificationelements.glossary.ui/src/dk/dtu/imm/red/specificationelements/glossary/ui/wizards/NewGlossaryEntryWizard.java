package dk.dtu.imm.red.specificationelements.glossary.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

/**
 * Interface for the New Glossary Entry Wizard. This wizard is responsible for
 * helping the user create a new glossary entry.
 *
 * It is of type INewWizard, so it shows up when running the
 * org.eclipse.ui.newWizard command.
 *
 * @author Jakob Kragelund
 *
 */
public interface NewGlossaryEntryWizard extends IBaseWizard {

	String ID = "dk.dtu.imm.red.specificationelements.glossary.newglossaryentrywizard";

	/**
	 * Gets the presenter for this wizard.
	 *
	 * @return the wizard's presenter
	 */
	NewGlossaryEntryWizardPresenter getPresenter();
}
