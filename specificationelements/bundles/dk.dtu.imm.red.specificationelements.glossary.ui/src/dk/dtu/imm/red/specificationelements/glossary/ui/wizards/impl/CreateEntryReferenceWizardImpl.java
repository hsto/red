package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ui.IWorkbench;

import dk.dtu.imm.red.core.ui.wizards.BaseWizard;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.CreateEntryReferenceWizard;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.CreateEntryReferenceWizardPresenter;

public class CreateEntryReferenceWizardImpl extends BaseWizard implements
		CreateEntryReferenceWizard {

	protected CreateEntryReferencePageImpl createEntryReferencePage;

	@Override
	public void init(final IWorkbench workbench,
			final IStructuredSelection selection) {
		createEntryReferencePage = new CreateEntryReferencePageImpl();
		presenter = new CreateEntryReferenceWizardPresenterImpl();
	}

	@Override
	public void addPages() {
		addPage(createEntryReferencePage);
	}

	@Override
	public boolean performFinish() {
		GlossaryEntry selectedEntry = createEntryReferencePage
				.getSelectedEntry();

		if (selectedEntry == null) {
			return false;
		} else {
			((CreateEntryReferenceWizardPresenter) presenter)
					.setSelectedEntry(selectedEntry);
			return true;
		}

	}

	@Override
	public void pageOpenedOrClosed(IWizardPage fromPage) {
		// do nothing
	}

}
