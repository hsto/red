package dk.dtu.imm.red.specificationelements.glossary.ui.views.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.views.BaseViewPresenter;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEntryEditor;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl.GlossaryEntryEditorInput;

/**
 * The presenter for the glossary view.
 *
 * @author Jakob Kragelund
 *
 */
public final class GlossaryViewPresenter extends BaseViewPresenter {

	private final class ContentAdapter extends EContentAdapter {
		@Override
		public void notifyChanged(Notification notification) {
			switch (notification.getEventType()) {
			case Notification.SET:
			case Notification.UNSET:
				if (view != null && currentGlossaryEntry != null) {
					view.showGlossaryEntry(currentGlossaryEntry);
				}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * The view of this presenter.
	 */
	protected GlossaryViewImpl view;

	protected GlossaryEntry currentGlossaryEntry;

	protected Glossary activeGlossary;

	protected int currentEntryPosition;

	private ContentAdapter contentAdapter;

	/**
	 * Creates a presenter for the given view.
	 *
	 * @param view
	 *            view for the presenter
	 */
	public GlossaryViewPresenter(final GlossaryViewImpl view) {
		super();
		this.view = view;
		this.contentAdapter = new ContentAdapter();
	}

	/**
	 * Updates the view by showing the selected glossary entry.
	 *
	 * @param entry
	 *            the selected glossary entry to be shown in the view
	 */
	public void glossaryEntrySelected(final GlossaryEntry entry) {
		if (currentGlossaryEntry != null) {
			currentGlossaryEntry.eAdapters().remove(contentAdapter);
		}

		currentGlossaryEntry = entry;
		activeGlossary = (Glossary) entry.getParent();
		currentEntryPosition = entry.getParent().getContents().indexOf(entry);

		if (entry != null) {
			entry.eAdapters().add(contentAdapter);
		}

		if (entry.getDescription() == null) {
			entry.setDescription(entry.getDescription());
		}

		view.showGlossaryEntry(entry);

	}

	/**
	 * Called when the "edit glossary entry" button in the view is pressed.
	 */
	public void openEditorButtonPressed() {
		GlossaryEntryEditorInput editorInput = new GlossaryEntryEditorInput(
				currentGlossaryEntry);

		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(editorInput, GlossaryEntryEditor.ID);
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

	}

	/**
	 * Called when the "show previous entry" button in the view is pressed
	 */
	public void showPreviousEntryButtonPressed() {
		if (currentEntryPosition == 0) {
			currentEntryPosition = activeGlossary.getContents().size();
		}
		currentGlossaryEntry = (GlossaryEntry) activeGlossary.getContents()
				.get(currentEntryPosition - 1);
		glossaryEntrySelected(currentGlossaryEntry);
	}

	/**
	 * Called when the "show next entry" button in the view is pressed
	 */
	public void showNextEntryButtonPressed() {
		if (currentEntryPosition == activeGlossary.getContents().size() - 1) {
			currentEntryPosition = -1;
		}
		currentGlossaryEntry = (GlossaryEntry) activeGlossary.getContents()
				.get(currentEntryPosition + 1);
		glossaryEntrySelected(currentGlossaryEntry);
	}

}
