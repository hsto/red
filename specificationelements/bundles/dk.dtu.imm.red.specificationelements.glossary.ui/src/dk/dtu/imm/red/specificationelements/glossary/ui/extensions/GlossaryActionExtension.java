package dk.dtu.imm.red.specificationelements.glossary.ui.extensions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.epf.richtext.IRichText;
import org.eclipse.epf.richtext.actions.IActionExtension;
import org.eclipse.epf.richtext.actions.RichTextAction;

import dk.dtu.imm.red.specificationelements.glossary.ui.actions.AddGlossaryReferenceAction;

public class GlossaryActionExtension implements IActionExtension {

	@Override
	public List<RichTextAction> getActions(IRichText richText) {
		List<RichTextAction> actions = new ArrayList<RichTextAction>();
		actions.add(new AddGlossaryReferenceAction(richText));

		return actions;
	}

}
