package dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.ui.dialogs.ManageStringListDialog;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEntryEditor;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEntryEditorPresenter;

/**
 * The presenter for the glossary entry editor.
 *
 * @author Jakob Kragelund
 *
 */
public final class GlossaryEntryEditorPresenterImpl extends BaseEditorPresenter implements GlossaryEntryEditorPresenter {


	GlossaryEntryEditor editor;

	GlossaryEntry glossaryEntry;
	List<String> synonyms;
	List<String> antonyms;
	List<String> examples;
	protected String discussion;


	public GlossaryEntryEditorPresenterImpl( final GlossaryEntryEditor glossaryEntryEditor, final GlossaryEntry glossaryEntry) {
		super(glossaryEntry);

		this.glossaryEntry = glossaryEntry;
		this.editor = glossaryEntryEditor;
		this.element = glossaryEntry;

		synonyms = new ArrayList<String>(glossaryEntry.getSynonyms());
		antonyms = new ArrayList<String>(glossaryEntry.getAntonyms());
		examples = new ArrayList<String>(glossaryEntry.getExamples());
	}


	@Override
	public String getSynonymsList() {
		return getList(synonyms);
	}
	
	@Override
	public String getAntonymsList() {
		return getList(antonyms);
	}
	
	@Override
	public String getExamplesList() {
		return getList(examples);
	}
	
	private String getList(List<String> list) {
		String str = "";
		for (int i = 0; i < list.size(); i++) {
			str += list.get(i);

			if (i < list.size() - 1) {
				str += ", ";
			}
		}
		return str;
	}

	@Override
	public void manageSynonymsButtonPressed() {
		ManageStringListDialog dialog = new ManageStringListDialog(Display.getCurrent().getActiveShell(), new ArrayList<String>(synonyms));
		if (dialog.open() == Window.CANCEL) {
			return;
		}

		synonyms.clear();
		synonyms.addAll(dialog.getPresenter().getStringListContents());
		editor.setSynonymsString(getSynonymsList());
		editor.markAsDirty();
	}
	
	@Override
	public void manageAntonymsButtonPressed() {
		ManageStringListDialog dialog = new ManageStringListDialog(Display.getCurrent().getActiveShell(), new ArrayList<String>(antonyms));
		if (dialog.open() == Window.CANCEL) {
			return;
		}

		antonyms.clear();
		antonyms.addAll(dialog.getPresenter().getStringListContents());
		editor.setAntonymsString(getAntonymsList());
		editor.markAsDirty();
	}
	
	@Override
	public void manageExamplesButtonPressed() {
		ManageStringListDialog dialog = new ManageStringListDialog(Display.getCurrent().getActiveShell(), new ArrayList<String>(examples));
		if (dialog.open() == Window.CANCEL) {
			return;
		}

		examples.clear();
		examples.addAll(dialog.getPresenter().getStringListContents());
		editor.setExamplesString(getExamplesList());
		editor.markAsDirty();
	}

	@Override
	public void save() {
		super.save();
		glossaryEntry.setDiscussion(TextFactory.eINSTANCE.createText(discussion));
		glossaryEntry.getSynonyms().clear();
		glossaryEntry.getSynonyms().addAll(synonyms);
		glossaryEntry.getAntonyms().clear();
		glossaryEntry.getAntonyms().addAll(antonyms);
		glossaryEntry.getExamples().clear();
		glossaryEntry.getExamples().addAll(examples);
		
		glossaryEntry.save();
	}	
	
	@Override
	public void setSpecElementBasicInfo(String label, String name, String kind, Element partOf, Element kindOf, String description) {
		setSpecElementBasicInfo(label, name, kind, description);
		glossaryEntry.setPartOf(partOf);
		glossaryEntry.setKindOf(kindOf);
	}

	@Override
	public void setDiscussion(String text) {
		this.discussion=text;
	}

	public String getDiscussion() {
		return discussion;
	}

}
