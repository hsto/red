package dk.dtu.imm.red.specificationelements.glossary.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.FrameworkUtil;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryFactory;
import dk.dtu.imm.red.specificationelements.glossary.ui.extensions.GlossaryElementExtension;

public class CreateGlossaryEntryOperation extends AbstractOperation {

//	protected String term;
//	protected Text definition;
	protected String label;
	protected String name;
	protected String description;
	protected Glossary parent;
	protected GlossaryEntry newEntry;

	public CreateGlossaryEntryOperation(final String label, final String name,
			final String description, final Glossary parent) {
		super("Create glossary entry");

//		this.term = term;
//		this.definition = definition;
		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;

	}

	@Override
	public IStatus execute(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		// if we haven't created the entry before, create it
		if (newEntry == null) {
			newEntry = GlossaryFactory.eINSTANCE.getGlossaryManager()
					.createGlossaryEntry(label, name, description, parent);
		} else { // otherwise ,just add it
			GlossaryFactory.eINSTANCE.getGlossaryManager().addGlossaryEntry(
					parent, newEntry);
		}
		newEntry.setCreator(PreferenceUtil.getUserPreference_User());
		new GlossaryElementExtension().openElement(newEntry);

		if (newEntry != null) {
			return Status.OK_STATUS;
		} else {
			return new Status(Status.ERROR, FrameworkUtil.getBundle(this.getClass()).getSymbolicName(),
					"Failed to create glossary entry");
		}
	}

	@Override
	public IStatus undo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		GlossaryFactory.eINSTANCE.getGlossaryManager().deleteGlossaryEntry(
				newEntry);

		return Status.OK_STATUS;
	}

}
