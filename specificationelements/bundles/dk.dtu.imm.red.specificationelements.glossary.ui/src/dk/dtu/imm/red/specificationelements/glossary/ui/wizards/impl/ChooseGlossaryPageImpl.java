package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TreeItem;

import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.NewGlossaryEntryWizard;

public class ChooseGlossaryPageImpl extends WizardPage {


	protected NewGlossaryEntryWizard wizard;
	protected ElementTreeViewWidget treeView;
	protected Button newGlossaryButton;
	protected ISelection selection;


	public ChooseGlossaryPageImpl(NewGlossaryEntryWizard wizard) {
		super("Choose Glossary");

		this.wizard = wizard;

		setTitle("Choose Glossary");
		setDescription("Use this page to choose which glossary "
				+ "should contain this entry, or create a new glossary"
				+ "using the \"New Glossary\" button.");
	}

	@Override
	public void createControl(final Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);

		GridLayout gridLayout = new GridLayout(1, false);
		container.setLayout(gridLayout);

		treeView = new ElementTreeViewWidget(true, container, SWT.BORDER);
		treeView.getTree().addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				getContainer().updateButtons();
			}
		});
		treeView.getTree().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(final MouseEvent e) {
				TreeItem item = treeView.getTree().getItem(new Point(e.x, e.y));
				if (item == null) {
					getContainer().updateButtons();
				}
			}
		});

		newGlossaryButton = new Button(container, SWT.NONE);
		newGlossaryButton.setText("New Glossary");
		newGlossaryButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				wizard.getPresenter().newGlossaryButtonPressed();
			}
		});

		setControl(container);

	}

	public ISelection getSelection() {
		return treeView.getSelection();
	}
}
