package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.NewGlossaryEntryWizard;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.NewGlossaryEntryWizardPresenter;

/**
 * Wizard for creating a new glossary entry.
 *
 * @author Jakob Kragelund
 *
 */
public class NewGlossaryEntryWizardImpl extends BaseNewWizard implements
		NewGlossaryEntryWizard {

	/**
	 * The presenter, which handles all non-view logic for this wizard.
	 */
	protected NewGlossaryEntryWizardPresenter presenter;

	/**
	 * Wizard page for defining the glossary entry.
	 */
	protected GlossaryEntryDefinitionPageImpl entryDefinitionPage;

	/**
	 * Wizard page for choosing the glossary parent.
	 */
	protected ChooseGlossaryPageImpl chooseGlossaryPage;

	public NewGlossaryEntryWizardImpl() {
		super("GlossaryEntry", GlossaryEntry.class);
		presenter = new NewGlossaryEntryWizardPresenterImpl();
		chooseGlossaryPage = new ChooseGlossaryPageImpl(this);
	}

//	@Override
//	public void init(final IWorkbench workbench,
//			final IStructuredSelection selection) {
//
////		entryDefinitionPage = new GlossaryEntryDefinitionPageImpl();
////		chooseGlossaryPage = new ChooseGlossaryPageImpl(this);
//	}

	@Override
	public void addPages() {
//		addPage(entryDefinitionPage);
		addPage(elementBasicInfoPage);
		addPage(chooseGlossaryPage);

	}

	@Override
	public NewGlossaryEntryWizardPresenter getPresenter() {
		return presenter;
	}

	@Override
	public boolean performFinish() {
//		String term = entryDefinitionPage.termText.getText();
//		String definition = entryDefinitionPage.definitionEditor.getText();
//		Glossary parent = null;
//		ISelection selection = chooseGlossaryPage.treeView.getSelection();
//		if (selection instanceof ITreeSelection) {
//			parent = (Glossary) ((ITreeSelection) selection).getFirstElement();
//		}
//
//		presenter.wizardFinished(term, definition, parent);
//
//		return true;

		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription();

		Group parent = null;

		if (!chooseGlossaryPage.getSelection().isEmpty()
				&& chooseGlossaryPage.getSelection()
					instanceof IStructuredSelection) {
			IStructuredSelection selection =
					(IStructuredSelection) chooseGlossaryPage.getSelection();

			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}

		presenter.wizardFinished(label, name, description, parent,"");

		return true;
	}

	@Override
	public boolean canFinish() {
//		ISelection selection = chooseGlossaryPage.treeView.getSelection();

//		if (!selection.isEmpty() && selection instanceof ITreeSelection) {
//			Element selectedElement = (Element) ((ITreeSelection) selection)
//					.getFirstElement();
//			return selectedElement instanceof Glossary;
//		}
		if (!chooseGlossaryPage.getSelection().isEmpty()
				&& chooseGlossaryPage.getSelection()
					instanceof IStructuredSelection) {
			IStructuredSelection selection =
					(IStructuredSelection) chooseGlossaryPage.getSelection();

			return(selection.getFirstElement() instanceof Glossary);
		}
		return false;
	}

//	@Override
//	public void pageOpenedOrClosed(IWizardPage fromPage) {
//		// do nothing
//	}

}
