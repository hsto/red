package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.CreateEntryReferenceWizardPresenter;

public class CreateEntryReferenceWizardPresenterImpl extends
		BaseWizardPresenter implements CreateEntryReferenceWizardPresenter {

	protected GlossaryEntry selectedEntry;

	@Override
	public GlossaryEntry getSelectedEntry() {
		return selectedEntry;
	}

	@Override
	public void setSelectedEntry(GlossaryEntry selectedEntry) {
		this.selectedEntry = selectedEntry;
	}

}
