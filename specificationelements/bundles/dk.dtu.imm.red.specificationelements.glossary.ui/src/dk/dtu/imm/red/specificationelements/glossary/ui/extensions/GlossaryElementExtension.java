package dk.dtu.imm.red.specificationelements.glossary.ui.extensions;

import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryFactory;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEditor;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEntryEditor;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl.GlossaryEditorInput;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl.GlossaryEntryEditorInput;

public class GlossaryElementExtension implements IElementExtensionPoint {

	@Override
	public void openElement(final Element element) {
		try {
			if (element instanceof Glossary) {
				GlossaryEditorInput editorInput = new GlossaryEditorInput(
						(Glossary) element);
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getActivePage()
						.openEditor(editorInput, GlossaryEditor.ID);
			}
			else if (element instanceof GlossaryEntry) {
				GlossaryEntryEditorInput editorInput = new GlossaryEntryEditorInput(
						(GlossaryEntry) element);
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getActivePage()
						.openEditor(editorInput, GlossaryEntryEditor.ID);
			}

		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

	}

	@Override
	public void deleteElement(final Element element) {
		if (element instanceof Glossary) {
			GlossaryFactory.eINSTANCE.getGlossaryManager().deleteGlossary(
					(Glossary) element);
		} else if (element instanceof GlossaryEntry) {
			GlossaryFactory.eINSTANCE.getGlossaryManager().deleteGlossaryEntry(
					(GlossaryEntry) element);
		}

		try {
			IWorkbench workbench = PlatformUI.getWorkbench();
			IWorkbenchWindow workbenchWindow = workbench
					.getActiveWorkbenchWindow();
			IWorkbenchPage activePage = workbenchWindow.getActivePage();

			IEditorReference[] editors = activePage.getEditorReferences();
			for (IEditorReference editor : editors) {
				if (editor.getEditorInput() instanceof GlossaryEntryEditorInput) {
					GlossaryEntryEditorInput editorInput = (GlossaryEntryEditorInput) editor
							.getEditorInput();

					if (element == editorInput.getElement()) {
						activePage.closeEditor(editor.getEditor(false), false);
					}
				}
			}
		} catch (PartInitException e) {
			e.printStackTrace(); LogUtil.logError(e);
		}

	}

	@Override
	public void restoreElement(final Element element) {
		if (element instanceof Glossary) {
			GlossaryFactory.eINSTANCE.getGlossaryManager().addGlossary(
					(Glossary) element);
		}

	}

}
