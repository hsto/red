package dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;

/**
 * The editor input for the glossary entry editor. This class serves as a
 * light-weight representation of the model presented in the editor.
 * 
 * It is used to make sure that two editors cannot be open for the same glossary
 * entry at the same time.
 * 
 * @author Jakob Kragelund
 * 
 */
public final class GlossaryEntryEditorInput extends BaseEditorInput {

	/**
	 * Creates an editor input for the glossary entry editor, with a given
	 * existing glossary entry.
	 * 
	 * @param entry
	 *            The glossary entry to show in the editor
	 */
	public GlossaryEntryEditorInput(final GlossaryEntry entry) {
		super(entry);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Glossary entry";
	}

	@Override
	public String getToolTipText() {
		return "Editor for glossary entry";
	}
}
