package dk.dtu.imm.red.specificationelements.glossary.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import dk.dtu.imm.red.specificationelements.glossary.GlossaryFactory;

public class ResetGlossaryManagerHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		GlossaryFactory.eINSTANCE.getGlossaryManager().resetGlossaryManger();
		return null;
	}

}
