package dk.dtu.imm.red.specificationelements.glossary.ui.wizards;

import java.util.List;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;

/**
 * Presenter for the new glossary entry wizard.
 *
 * @author Jakob Kragelund
 *
 */
public interface NewGlossaryEntryWizardPresenter extends IWizardPresenter {

	/**
	 * Returns the term of the new glossary entry.
	 *
	 * @return term
	 */
//	String getNewGlossaryEntryTerm();

	/**
	 * Returns the definition of the new glossary entry.
	 *
	 * @return definition
	 */
//	Text getNewGlossaryEntryDefinition();

	/**
	 * Returns a list of synonyms for the new glossary entry.
	 *
	 * @return list of synonyms
	 */
	List<String> getNewGlossaryEntrySynonyms();

	/**
	 * Returns a list of abbreviations for the new glossary entry.
	 *
	 * @return list of abbreviations
	 */
//	List<String> getNewGlossaryEntryAbbreviations();

	/**
	 * Returns the glossary which will contain this glossary entry.
	 *
	 * @return parent glossary
	 */
	Glossary getNewGlossaryEntryParent();

	/**
	 * Called by the wizard when it is done.
	 *
	 * @param term
	 *            the glossary entry's term
	 * @param definition
	 *            the glossary entry's definition
	 * @param synonyms
	 *            synonyms of the glossary entry
	 * @param abbreviations
	 *            abbreviations of the glossary entry
	 * @param parent
	 *            parent glossary
	 */
//	void wizardFinished(String term, String definition, Glossary parent);

	void wizardFinished(String label, String name, String description, Group parent, String path);

	void newGlossaryButtonPressed();
}
