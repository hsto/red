package dk.dtu.imm.red.specificationelements.glossary.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;

public interface CreateEntryReferenceWizardPresenter extends IWizardPresenter {

	GlossaryEntry getSelectedEntry();

	void setSelectedEntry(GlossaryEntry selectedEntry);

}
