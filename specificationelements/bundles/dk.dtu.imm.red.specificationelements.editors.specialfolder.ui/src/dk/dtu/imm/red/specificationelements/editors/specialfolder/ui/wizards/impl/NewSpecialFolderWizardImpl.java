package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group; 
import dk.dtu.imm.red.core.folder.Folder; 
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard; 
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.NewSpecialFolderWizard;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.NewSpecialFolderWizardPresenter;
 
  
public class NewSpecialFolderWizardImpl extends BaseNewWizard 
	implements NewSpecialFolderWizard {  
	 
	protected NewSpecialFolderWizardPresenter presenter;
	private DefineSpecialFolderPage defineSpecificationElementTypePage;
	
	public NewSpecialFolderWizardImpl() {
		super("Folder", Folder.class, true);
		if (true) throw new IllegalArgumentException("NewSpecialFolderWizardPresenterImpl is not implemented.");
		// presenter = new NewSpecialFolderWizardPresenterImpl();
		defineSpecificationElementTypePage = new DefineSpecialFolderPage(this, presenter);
	}
	
	@Override
	public void addPages() { 
		addPage(defineSpecificationElementTypePage);
		super.addPages();
	}
	
	@Override
	public boolean performFinish() {
		
		String name = displayInfoPage.getDisplayName();
		String specialType = defineSpecificationElementTypePage.getSpecialType();
		try {
			specialType = defineSpecificationElementTypePage.getSpecialType();
		}
		catch(IllegalStateException e) {
			return false;
		}
	

		Group parent = null;
				
		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();
			
			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}
		
		((NewSpecialFolderWizardPresenterImpl) presenter).wizardFinished(name, specialType, parent, "");
		
		return true;
	}

	@Override
	public boolean canFinish() {
		return super.canFinish()  
				&& defineSpecificationElementTypePage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}

}
