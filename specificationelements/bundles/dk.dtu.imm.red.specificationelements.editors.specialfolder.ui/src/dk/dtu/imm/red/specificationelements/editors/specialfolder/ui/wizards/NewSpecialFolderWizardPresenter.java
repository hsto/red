package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards;

import java.util.List;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;
import dk.dtu.imm.red.specificationelements.SpecificationElement;

public interface NewSpecialFolderWizardPresenter extends IWizardPresenter {

	void wizardFinished(String name, String specialType, Group parent, String path); 
	List<String> getSpecificationElementTypes();
}
