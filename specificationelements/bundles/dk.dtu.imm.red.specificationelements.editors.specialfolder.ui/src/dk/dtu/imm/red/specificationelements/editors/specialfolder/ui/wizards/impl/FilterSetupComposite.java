package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.impl;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.layout.GridData;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class FilterSetupComposite extends Composite {
	private Combo cmbSpecificationElementType;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public FilterSetupComposite(Composite parent, 
			int style, final DefineSpecialFolderPage parentPage) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Label lblSelectSpecificationElement = new Label(this, SWT.NONE);
		lblSelectSpecificationElement.setText("Select specification element type");
		
		cmbSpecificationElementType = new Combo(this, SWT.NONE);
		cmbSpecificationElementType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				parentPage.setSelectionChanged();
				
			}
		});
		cmbSpecificationElementType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

	}

	public Combo getCmbSpecificationElementType() {
		return cmbSpecificationElementType;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
