package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.impl.EClassImpl;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.ExtendedMetaData;

public class EMFUtils {

	private final Map<EClass, Collection<EClass>> SUB_CLASSES = new HashMap<EClass, Collection<EClass>>();
	private EClass specificationElementEClass;

	public EMFUtils() {
		super();
		specificationElementEClass = getSpecificationElementEClass();
		loadSubClassesIntoMap(specificationElementEClass);
	}

	public List<String> getSpecificationElementTypes() {

		Collection<EClass> subClasses = loadSubClassesIntoMap(specificationElementEClass);

		List<String> l = new ArrayList<String>();

		// Add all subclasses to the specification element,
		for (EClass ec : subClasses) {
			l.add(ec.getName());
		}

		return l;
	}

	private EClass getSpecificationElementEClass() {
		ExtendedMetaData modelMetaData = new BasicExtendedMetaData(EPackage.Registry.INSTANCE);
		EClass specElementEClass = (EClass) modelMetaData.getType("dk.dtu.imm.red.specificationelements",
				"SpecificationElement");
		return specElementEClass;
	}

	public EClass getSpecialTypesEClassFromFolder(String se) {

		for (EClass sc : SUB_CLASSES.get(specificationElementEClass)) {

			String ecName = sc.getName();
			if (ecName.equals(se)) {
				return sc;
			}
		}

		return null;
	}

	/**
	 * Returns a list of all <em>known</em> sub-classes for the specified class.
	 * 
	 * @param cls
	 *            the super-class
	 * @return list of all sub-classes - possibly <code>null</code>
	 */
	public Collection<EClass> loadSubClassesIntoMap(EClass cls) {
		if (SUB_CLASSES.containsKey(cls))
			return SUB_CLASSES.get(cls);

		Collection<EClass> l = null;
		final Registry registry = EPackage.Registry.INSTANCE;
		for (final Object v : registry.values()) {
			if (!(v instanceof EPackage)) {
				continue;
			}
			final EPackage ep = (EPackage) v;

			for (final EClassifier c : ep.getEClassifiers()) {
				if (!(c instanceof EClass)) {
					continue;
				}
				final EClass cl = (EClass) c;

				EList<EClass> superTypes = cl.getESuperTypes(); 
				
				if (superTypes.contains(cls)) {
					if (l == null) {
						l = new ArrayList<EClass>();
					}
					l.add(cl);
				}
			}
		}
		SUB_CLASSES.put(cls, l);

		return l;
	}

}
