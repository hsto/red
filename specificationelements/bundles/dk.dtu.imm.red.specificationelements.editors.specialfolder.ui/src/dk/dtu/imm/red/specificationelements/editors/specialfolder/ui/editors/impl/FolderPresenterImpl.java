package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.impl;  
 
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.SpecialFolderEditor;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.SpecialFolderPresenter;
 

/**
 * @author Johan Paaske Nielsen
 *
 */ 
public class FolderPresenterImpl extends BaseEditorPresenter implements
		SpecialFolderPresenter { 
	
	
	protected SpecialFolderEditor usecaseEditor;
	private FolderViewModel viewModel;
	private EClass specElementEClass; 
	private Class<?> specialTypeClass; 
	private EMFUtils reflectionProvider; 

	public FolderPresenterImpl(SpecialFolderEditor visionEditor, FolderViewModel viewModel) {
		super(viewModel.getElement());
		this.viewModel = viewModel;
		this.usecaseEditor = visionEditor;   
		this.element = viewModel.getElement();
		this.reflectionProvider = new EMFUtils(); 
		
		if(viewModel.getElement().getSpecialType() != null) {
			updateSpecificationElementType();
		} 
	}

	private void updateSpecificationElementType() { 
			specElementEClass = reflectionProvider.getSpecialTypesEClassFromFolder(
					viewModel.getElement().getSpecialType()); 
			specialTypeClass = specElementEClass.getInstanceClass();   
		 
		reflectionProvider = new EMFUtils();;
	}

	@Override
	public EClass getSpecElementEClass() {
		return specElementEClass;
	}

	@Override
	public Class<?> getSpecialTypeClass() {
		return specialTypeClass;
	} 
	
	public EList<Element> getSpecificationElements() { 
		return viewModel.getElement().getContents(); 
	}
	
	public FolderViewModel getViewModel() { 
		return viewModel;
	}  

	@Override
	public void save() {
		super.save();   
	} 

	public List<String> getSpecificationElementTypes() {
		return reflectionProvider.getSpecificationElementTypes();
	}

	public void setSpecialType(String item) {
		if(item == "" || item == viewModel.getElement().getSpecialType()) return;
		
		viewModel.getElement().setSpecialType(item);
		updateSpecificationElementType();
	}


	@Override
	public Project getFoldersProject() { 
		List<Project> currentlyOpenedProjects = WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects();
		
		return currentlyOpenedProjects.size() > 0 ? currentlyOpenedProjects.get(0) : null; 
	} 
}
