package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors;

import org.eclipse.ui.IEditorPart; 

import dk.dtu.imm.red.core.ui.editors.IBaseEditor; 

public interface SpecialFolderEditor extends IEditorPart, IBaseEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.editors.specialfolder.specialfoldereditor";
	void refreshGrid();
	void updateSelectionSpecialType();
	void importCSV();
	void exportCSV();
	void updateDisplayAttributeFilter(String item); 
}
