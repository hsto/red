package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status; 

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;  
 
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.SpecialFolderEditor;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.SpecialFolderPresenter;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.extensions.SpecialFolderExtension; 
import dk.dtu.specificationelements.editors.editors.EditorsFactory;

public class CreateSpecialFolderOperation extends AbstractOperation{ 
	 
	protected String name;
	protected SpecialFolderExtension extension;
	protected Group parent;
//	protected String path;
	protected String specialType;
	
	private Folder newItem;
	
	public CreateSpecialFolderOperation(String name, String specialType, Group parent, String path) {
		super("Create special folder"); 
		this.name = name; 
		this.parent = parent;
//		this.path = path;
		this.specialType = specialType;
		extension = new SpecialFolderExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		
		newItem = EditorsFactory.eINSTANCE.createSpecialFolder();
		
		newItem.setName(name);		
		newItem.setSpecialType(specialType);
		 
		
		if (parent != null) {
			newItem.setParent(parent);
		} 

		newItem.save();
		extension.openElement(newItem);

		return Status.OK_STATUS;
	}
	
	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(newItem);
			extension.deleteElement(newItem);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}
	
	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
