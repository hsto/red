package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.impl;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;

public class SpecialFolderEditorInputImpl extends BaseEditorInput {

	public SpecialFolderEditorInputImpl(Folder element) {
		super(element);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Special folder editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for folder " + element.getName();
	}
}
