package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.folder.Folder;


import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.SpecialFolderEditor;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.impl.SpecialFolderEditorInputImpl;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;

public class SpecialFolderExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element) {
		if (element instanceof Folder && !(element instanceof Glossary)) {
			SpecialFolderEditorInputImpl editorInput = new SpecialFolderEditorInputImpl(
					(Folder) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, SpecialFolderEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}

}
