package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.SpecialFolderEditor;

public class FolderEditorComposite extends Composite {
	
	private Group gpEditElement;
	private Combo cmbChangeType;
	private Button btnRefresh;
	private SpecialFolderEditor parentEditor;
	private Button btnImport;
	private Button btnExportCsv;
	private Composite composite;
	private Combo cmbAttributeDisplay;
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public FolderEditorComposite(Composite parent, int style) {
		super(parent, style);
		setupControls(); 
	}
	
	public FolderEditorComposite(Composite parent, int style, SpecialFolderEditor parentEditor) {
		super(parent, style);
		this.parentEditor = parentEditor;
		setupControls(); 
	}

	private void setupControls() {
		setLayout(new GridLayout(1, false)); 
		
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;
		
		ElementBasicInfoContainer basicInfoContainer = new ElementBasicInfoContainer(this, SWT.NONE, (FolderEditorImpl) parentEditor, (Element) parentEditor.getEditorElement());
		((FolderEditorImpl) parentEditor).setBasicInfoContainer(basicInfoContainer);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);
		
		gpEditElement = new Group(this, SWT.NONE);
		gpEditElement.setText("Folder Content");
		gpEditElement.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		gpEditElement.setLayout(new GridLayout(2, false));
		
		composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(2, false));
		
		Group gpOptions = new Group(composite, SWT.NONE);
		gpOptions.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 1, 1));
		gpOptions.setSize(418, 53);
		gpOptions.setLayout(new GridLayout(3, false));
		gpOptions.setText("Filter");
		
		cmbChangeType = new Combo(gpOptions, SWT.READ_ONLY);
		cmbChangeType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				parentEditor.updateSelectionSpecialType();
			}
		});
		GridData gd_cmbChangeType = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbChangeType.minimumWidth = 200;
		cmbChangeType.setLayoutData(gd_cmbChangeType);
		cmbChangeType.setBounds(0, 0, 91, 23);
		
		cmbAttributeDisplay = new Combo(gpOptions, SWT.READ_ONLY);
		cmbAttributeDisplay.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				parentEditor.updateDisplayAttributeFilter(cmbAttributeDisplay.getItem(cmbAttributeDisplay.getSelectionIndex()));
			}
		});
		cmbAttributeDisplay.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbAttributeDisplay.setBounds(0, 0, 91, 23);
		
		btnRefresh = new Button(gpOptions, SWT.NONE);
		btnRefresh.setToolTipText("Synchronizes the content with the underlying model");
		btnRefresh.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				parentEditor.refreshGrid();
			}
		});
		btnRefresh.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnRefresh.setText("Refresh");
		
		
		Group grpCsv = new Group(composite, SWT.NONE);
		grpCsv.setText("CSV");
		grpCsv.setLayout(new GridLayout(2, true));
		grpCsv.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		btnImport = new Button(grpCsv, SWT.NONE);
		btnImport.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnImport.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				parentEditor.importCSV();
			}
		});
		btnImport.setText("Import CSV");
		
		btnExportCsv = new Button(grpCsv, SWT.NONE);
		btnExportCsv.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnExportCsv.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				parentEditor.exportCSV();
			}
		});
		btnExportCsv.setText("Export CSV");
	} 

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	public Group getGpEditElement() {
		return gpEditElement;
	}

	public Combo getCmbChangeType() {
		return cmbChangeType;
	}

	public Button getBtnRefresh() {
		return btnRefresh;
	}
	
	public Combo getCombo() {
		return cmbAttributeDisplay;
	}

}
