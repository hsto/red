package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.impl.EClassImpl;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.element.group.Group;
 
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.SpecificationelementsFactory;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.impl.ReflectionProvider;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.operations.CreateSpecialFolderOperation;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.NewSpecialFolderWizard;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.NewSpecialFolderWizardPresenter;

public class NewSpecialFolderWizardPresenterImpl extends BaseWizardPresenter
        implements NewSpecialFolderWizardPresenter { 
    
    private ReflectionProvider reflectionProvider;

    public NewSpecialFolderWizardPresenterImpl() {
        super();
        this.reflectionProvider = new ReflectionProvider();
    }
    
    @Override
    public void wizardFinished(String name, String specialType, Group parent, String path) {

        IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
                .getOperationSupport();

        CreateSpecialFolderOperation operation = 
                new CreateSpecialFolderOperation(name, specialType, parent, path);

        operation.addContext(operationSupport.getUndoContext());

        IAdaptable info = new IAdaptable() {
            @Override
            public Object getAdapter(@SuppressWarnings("rawtypes") final Class adapter) {
                if (Shell.class.equals(adapter)) {
                    return Display.getCurrent().getActiveShell();
                }
                return null;
            }
        };

        try {
            OperationHistoryFactory.getOperationHistory().execute(operation, null,
                    info);
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<String> getSpecificationElementTypes() {
        return reflectionProvider.getSpecificationElementTypes();
    } 
}
