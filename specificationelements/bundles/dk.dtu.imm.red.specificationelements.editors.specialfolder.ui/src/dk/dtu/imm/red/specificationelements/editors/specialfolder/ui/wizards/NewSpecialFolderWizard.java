package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewSpecialFolderWizard extends IBaseWizard {
	
	String ID = "dk.dtu.imm.red.specificationelements.actor.newwizard";

}
