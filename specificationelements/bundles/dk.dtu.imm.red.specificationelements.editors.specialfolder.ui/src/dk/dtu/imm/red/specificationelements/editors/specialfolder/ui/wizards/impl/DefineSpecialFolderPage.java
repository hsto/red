package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.impl;

import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.NewSpecialFolderWizard;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.wizards.NewSpecialFolderWizardPresenter;

public class DefineSpecialFolderPage extends WizardPage implements IWizardPage {

	protected Label actorTitle;
	protected Text titleTextBox;
	protected Label actorLabel;
	protected RichTextEditor actorEditor;
	protected NewSpecialFolderWizard wizard;
	private FilterSetupComposite defType;
	private NewSpecialFolderWizardPresenter presenter;
	
	public DefineSpecialFolderPage(NewSpecialFolderWizard wizard, 
			NewSpecialFolderWizardPresenter presenter) {
		super("Define special folder"); 
		setTitle("Create Special folder");
		setDescription("Use this wizard to create a special folder"); 
		this.wizard = wizard;
		this.presenter = presenter;
	}

	@Override
	public void createControl(Composite parent) {
		defType = new FilterSetupComposite(parent, SWT.NULL, this); 
		setPageComplete(true);
		setControl(defType);
		fillCombobox();
	}
	
	private void fillCombobox() {
		for(String se : presenter.getSpecificationElementTypes()) {
			defType.getCmbSpecificationElementType().add(se);
		}
	}
	@Override
	public IWizardPage getNextPage() { 
		return super.getNextPage();
	} 
	
	@Override
	public boolean isPageComplete() { 
		int index = defType.getCmbSpecificationElementType().getSelectionIndex();
		return index != -1 && defType.getCmbSpecificationElementType().getItem(index) != ""; 
	}  
	
	public String getSpecialType() {
		int index = defType.getCmbSpecificationElementType().getSelectionIndex(); 
		return defType.getCmbSpecificationElementType().getItem(index);
	}

	public void setSelectionChanged() {
		setPageComplete(isPageComplete());
	}
}
