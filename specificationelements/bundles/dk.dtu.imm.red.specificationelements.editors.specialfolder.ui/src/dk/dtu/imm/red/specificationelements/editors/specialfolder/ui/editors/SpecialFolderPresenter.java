package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors; 
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;  
import dk.dtu.imm.red.specificationelements.SpecificationElement;

public interface SpecialFolderPresenter extends IEditorPresenter { 
	List<String> getSpecificationElementTypes();
	EList<Element> getSpecificationElements(); 
	void setSpecialType(String item);
	Class<?> getSpecialTypeClass();
	EClass getSpecElementEClass();
	Project getFoldersProject();
}
