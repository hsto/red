package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.impl; 
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.internal.effortestimation.views.impl.EffortEstimationEditor;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.csv.ui.CSVImportExportUI;
//import dk.dtu.imm.red.csv.ui.CSVImportExportUI;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.SpecialFolderEditor;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.SpecialFolderPresenter;
import dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.impl.FolderViewModel.AttributeFilter; 

public class FolderEditorImpl extends BaseEditor implements SpecialFolderEditor, EffortEstimationEditor { 

	protected ElementBasicInfoContainer basicInfoContainer;
	
	protected int firstPageIndex;  
	private SpecialFolderPresenter specificPresenter; 
	private FolderViewModel viewModel; 
	private EAGComposite<Element> crudComposite;
	private FolderEditorComposite seeComposite;
	private CSVImportExportUI<Element> imp;

	//UI Setup 
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
		viewModel = new FolderViewModel(); 
		viewModel.setElement((Folder) element);
		presenter = new FolderPresenterImpl(this, viewModel); 
		specificPresenter = (FolderPresenterImpl) presenter; // casted presenter 
	}   

	@Override
	protected void createPages() {

		//Create attributes page
		firstPageIndex = addPage(createSpeficationElementEditorPage(getContainer()));
		setPageText(firstPageIndex, "Summary"); 

		super.createPages(); 
		fillInitialData();
	}

	private Composite createSpeficationElementEditorPage(Composite parent) {		
		seeComposite = new FolderEditorComposite(parent, SWT.NONE, this); 
		crudComposite = new EAGComposite<Element>(seeComposite.getGpEditElement(), 
				SWT.NONE, modifyListener)
				.setCanSetRecursive(true)
				.setModifyListener(modifyListener);

		crudComposite.setLayout(new GridLayout(1, false));
		crudComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true)); 
		return SWTUtils.wrapInScrolledComposite(parent, seeComposite); 
	} 

	public void setIsModified() {
		modifyListener.handleEvent(null);
	}


	@Override
	public void doSaveAs() {
		return;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		
		
		super.doSave(monitor);
	}

	//Fill UI widgets with data
	@Override
	protected void fillInitialData() { 	
		isDirty = false;
		firePropertyChange(PROP_DIRTY);  
		
		basicInfoContainer.fillInitialData(viewModel.getElement(), new String[] {});
		
		//Fill data into combobox
		for(String se : specificPresenter.getSpecificationElementTypes()) {
			seeComposite.getCmbChangeType().add(se);
		}		
		
		if(viewModel.getElement().getSpecialType() != null) {
			updateReflectiveAgileGrid();
			seeComposite.getCmbChangeType().select(
					seeComposite.getCmbChangeType().indexOf(
							viewModel.getElement().getSpecialType()));
		} else {
			// Auto select filter. Selects first occurrence.
			seeComposite.getCmbChangeType().select(0);
			String str = getFirstOccurrence(viewModel.getElement());
			int index = seeComposite.getCmbChangeType().indexOf(str); 
			if (index != -1) {
				seeComposite.getCmbChangeType().select(index);
			}
			updateSelectionSpecialType();
		}
		
		for(AttributeFilter a : AttributeFilter.values()) {
			seeComposite.getCombo().add(a.name().replace('_', ' '));
		}
		
		seeComposite.getCombo().select(0);
		
		
	}

	private String getFirstOccurrence(Folder folder) {
		if (folder == null || folder.getContents().isEmpty()) return "";
		
		for (Element element : folder.getContents()) {
			if (element instanceof Folder) {
				return getFirstOccurrence((Folder) element);
			} else {
				return element.getClass().getInterfaces()[0].getSimpleName();
			}
		}		
		return "";
	}

	private void updateReflectiveAgileGrid() { 
		crudComposite.getPresenter().setReflectedClass(specificPresenter.getSpecElementEClass());
	
		if(viewModel.getDisplayAttributes() == AttributeFilter.Element_Attributes) {
			crudComposite.generateDefaultColumns()
			.setDataSource(specificPresenter.getSpecificationElements()); 
		}
		else if(viewModel.getDisplayAttributes() == AttributeFilter.Management_Attributes) {
			crudComposite.generateSpecificColumns(Element.class)
			.setDataSource(specificPresenter.getSpecificationElements()); 
		}
		
		crudComposite.setSumWidget(true);
		Project p = specificPresenter.getFoldersProject();
	} 
	 
	public void setBasicInfoContainer(ElementBasicInfoContainer basicInfoContainer){
		this.basicInfoContainer = basicInfoContainer;
	}
	
	//Properties
	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.editors.specialfolder";
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public String getHelpResourceURL() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void refreshGrid() {
		updateReflectiveAgileGrid();
	}

	@Override
	public void updateSelectionSpecialType() {
		specificPresenter.setSpecialType(seeComposite.getCmbChangeType().getItem(
				seeComposite.getCmbChangeType().getSelectionIndex()));
		updateReflectiveAgileGrid();
		modifyListener.handleEvent(null);
	}
	
	@Override
	public void updateDisplayAttributeFilter(String item) {
		
		String enumName = item.replace(' ', '_');
		
		 if(enumName.equals(AttributeFilter.Element_Attributes.name())) {
			 viewModel.setDisplayAttributes(AttributeFilter.Element_Attributes);
		 }
		 else if (enumName.equals(AttributeFilter.Management_Attributes.name())) {
			 viewModel.setDisplayAttributes(AttributeFilter.Management_Attributes);
		 } 
		 
		 updateReflectiveAgileGrid();
	} 
	 

	@Override
	public List<Element> getEffortEstimationContent() {
		return viewModel.getElement().getContents();
	}

	@Override
	public void importCSV() {  
		Display display = Display.getDefault();
		Shell result = display.getActiveShell();
		imp = new CSVImportExportUI<Element>(result, specificPresenter.getSpecificationElements(), 
				crudComposite.getPresenter().getReflectedEClass());
		
		if(imp.importCSVFile()) {
			updateReflectiveAgileGrid(); 
		};
	}

	@Override
	public void exportCSV() {
		Display display = Display.getDefault();
		Shell result = display.getActiveShell();
		imp = new CSVImportExportUI<Element>(result, crudComposite.getPresenter().getDataSourceView(), 
				crudComposite.getPresenter().getReflectedEClass());
		
		imp.exportCSVFile(viewModel.getElement().getName());  
	}

	
}
