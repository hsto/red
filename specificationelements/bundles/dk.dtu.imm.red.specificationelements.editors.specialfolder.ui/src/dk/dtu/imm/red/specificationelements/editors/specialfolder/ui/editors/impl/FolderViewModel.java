package dk.dtu.imm.red.specificationelements.editors.specialfolder.ui.editors.impl;  
import dk.dtu.imm.red.core.folder.Folder; 
import dk.dtu.imm.red.core.ui.presenters.ElementHandler;

public class FolderViewModel extends ElementHandler<Folder> {
	
	public enum AttributeFilter {
	    Element_Attributes, Management_Attributes
	} 
	 
	private AttributeFilter displayAttributes = AttributeFilter.Element_Attributes;
	
	public AttributeFilter getDisplayAttributes() {
		return displayAttributes;
	}
	public void setDisplayAttributes(AttributeFilter displayAttributes) {
		
		this.displayAttributes = displayAttributes;
	} 
	
	 
	@Override
	public Folder getElement() {
		return getElementBeforeEdit();
	}
}