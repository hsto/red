/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.signature.provider;


import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypeFactory;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityFactory;

import dk.dtu.imm.red.specificationelements.modelelement.provider.ModelElementEditPlugin;

import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SignatureItemItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignatureItemItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addVisibilityPropertyDescriptor(object);
			addAccessModePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SignatureItem_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SignatureItem_name_feature", "_UI_SignatureItem_type"),
				 SignaturePackage.Literals.SIGNATURE_ITEM__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Visibility feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVisibilityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SignatureItem_visibility_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SignatureItem_visibility_feature", "_UI_SignatureItem_type"),
				 SignaturePackage.Literals.SIGNATURE_ITEM__VISIBILITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Access Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAccessModePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SignatureItem_accessMode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SignatureItem_accessMode_feature", "_UI_SignatureItem_type"),
				 SignaturePackage.Literals.SIGNATURE_ITEM__ACCESS_MODE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE);
			childrenFeatures.add(SignaturePackage.Literals.SIGNATURE_ITEM__MULTIPLICITY);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SignatureItem.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SignatureItem"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SignatureItem)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_SignatureItem_type") :
			getString("_UI_SignatureItem_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SignatureItem.class)) {
			case SignaturePackage.SIGNATURE_ITEM__NAME:
			case SignaturePackage.SIGNATURE_ITEM__VISIBILITY:
			case SignaturePackage.SIGNATURE_ITEM__ACCESS_MODE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case SignaturePackage.SIGNATURE_ITEM__TYPE:
			case SignaturePackage.SIGNATURE_ITEM__MULTIPLICITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createBaseType()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createFunctionType()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createDataStructureType()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createTupleType()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createNestedType()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createArrayType()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createChoiceType()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createSignatureReference()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createNoType()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createTypeVariable()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__TYPE,
				 DatatypeFactory.eINSTANCE.createUnparsedType()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createSingleValueMultiplicity()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createRangedMultiplicity()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createCompoundMultiplicity()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createUnparsedMultiplicity()));

		newChildDescriptors.add
			(createChildParameter
				(SignaturePackage.Literals.SIGNATURE_ITEM__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createNoMultiplicity()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ModelElementEditPlugin.INSTANCE;
	}

}
