/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype.provider;


import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypeFactory;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.TupleType;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.specificationelements.modelelement.datatype.TupleType} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TupleTypeItemProvider extends DataTypeItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TupleTypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns TupleType.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TupleType"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_TupleType_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TupleType.class)) {
			case DatatypePackage.TUPLE_TYPE__ELEMENT_TYPES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createBaseType()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createFunctionType()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createDataStructureType()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createTupleType()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createNestedType()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createArrayType()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createChoiceType()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createSignatureReference()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createNoType()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createTypeVariable()));

		newChildDescriptors.add
			(createChildParameter
				(DatatypePackage.Literals.TUPLE_TYPE__ELEMENT_TYPES,
				 DatatypeFactory.eINSTANCE.createUnparsedType()));
	}

}
