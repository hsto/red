package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.tree;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;   
import dk.dtu.imm.red.specificationelements.usecasescenario.Operator;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.UseCaseScenarioEditorImpl;


public class TreeDropListener extends ViewerDropAdapter {
 
  private UseCaseScenarioEditorImpl scenarioEditor;
  private int location;

  public TreeDropListener(Viewer viewer, UseCaseScenarioEditorImpl sc) {
    super(viewer); 
    this.scenarioEditor = sc;
  }

  @Override
  public void drop(DropTargetEvent event) { 
	  location = this.determineLocation(event);  
	  super.drop(event);
  }

  // This method performs the actual drop
  // We simply add the String we receive to the model and trigger a refresh of the 
  // viewer by calling its setInput method.
  @Override
  public boolean performDrop(Object data) { 
	  	scenarioEditor.moveScenarioElement(this.location, getSelection(), (UseCaseScenarioElement) getCurrentTarget()); 
	  	return true;
  }

  @Override
  public boolean validateDrop(Object target, int operation,
      TransferData transferType) {
	 
	  boolean allowedDrop = (getCurrentLocation() == LOCATION_ON && target instanceof Operator) ||
			  				 getCurrentLocation() == LOCATION_AFTER || getCurrentLocation() == LOCATION_BEFORE;
	  
	  return allowedDrop && LocalSelectionTransfer.getTransfer().isSupportedType(transferType); 
  } 
  
  private UseCaseScenarioElement getSelection() {
	  LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
	  IStructuredSelection selection = (IStructuredSelection) transfer.getSelection();
	  return (UseCaseScenarioElement) selection.toList().get(0);
  }
} 
