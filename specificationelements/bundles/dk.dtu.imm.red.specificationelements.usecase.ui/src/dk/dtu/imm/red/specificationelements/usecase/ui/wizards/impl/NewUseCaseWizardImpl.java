package dk.dtu.imm.red.specificationelements.usecase.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.ui.wizards.NewUseCaseWizard;
import dk.dtu.imm.red.specificationelements.usecase.ui.wizards.NewUseCaseWizardPresenter;


public class NewUseCaseWizardImpl extends BaseNewWizard 
	implements NewUseCaseWizard {

//	protected ElementBasicInfoPage defineUsecasePage;

	
	protected NewUseCaseWizardPresenter presenter;
	
	public NewUseCaseWizardImpl() {
		super("UseCase", UseCase.class);
		presenter = new NewUseCaseWizardPresenterImpl(this);
	}
	
	@Override
	public void addPages() {
		super.addPages();
	}
	
	@Override
	public boolean performFinish() {
		
		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription(); 
		
		Group parent = null;
		
		String path = "";
		
		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();
			
			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}
		
		presenter.wizardFinished(label, name, description, parent, path);
		
		return true;
	}

	@Override
	public boolean canFinish() {
		return super.canFinish() 
				&& elementBasicInfoPage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}

}
