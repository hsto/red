package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.tree;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection; 
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;

public class TreeDragListener implements DragSourceListener {

  private final TreeViewer viewer;

  public TreeDragListener(TreeViewer viewer) {
    this.viewer = viewer;
  }

  @Override
  public void dragFinished(DragSourceEvent event) {
	  
  }

  @Override
  public void dragSetData(DragSourceEvent event) {
    // Here you do the conversion to the type which is expected.
    IStructuredSelection selection = (IStructuredSelection) viewer
    .getSelection(); 
    
     LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
     if (transfer.isSupportedType(event.dataType)) {
			transfer.setSelection(selection);
			transfer.setSelectionSetTime(event.time & 0xFFFF);
	}
     
  }

  @Override
  public void dragStart(DragSourceEvent event) {
    
  } 
} 
