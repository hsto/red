package dk.dtu.imm.red.specificationelements.actor.ui.editors.impl;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorEditor;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;

public class ActorDetailsComposite extends Composite { 

	private Text txtCodeContent;
	private Combo cmbCodeType;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ActorDetailsComposite(Composite parent, int style, final ActorEditor editor) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Label lblType = new Label(this, SWT.NONE);
		lblType.setText("Type");
		
		cmbCodeType = new Combo(this, SWT.READ_ONLY);
		cmbCodeType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editor.codeTypeChanged();
			}
		});
		GridData gd_cmbCodeType = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_cmbCodeType.widthHint = 145;
		cmbCodeType.setLayoutData(gd_cmbCodeType);
		
		Label lblContent = new Label(this, SWT.NONE);
		lblContent.setBounds(0, 0, 55, 15);
		lblContent.setText("Content");
		
		txtCodeContent = new Text(this, SWT.BORDER);
		txtCodeContent.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editor.codeContentChanged();
			}
		});
		txtCodeContent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1)); 
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	public Text getTxtCodeContent() {
		return txtCodeContent;
	}

	public Combo getCmbCodeType() {
		return cmbCodeType;
	}

}
