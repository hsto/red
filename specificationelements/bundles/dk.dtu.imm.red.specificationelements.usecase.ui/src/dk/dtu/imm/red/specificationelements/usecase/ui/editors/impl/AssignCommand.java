package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;

public interface AssignCommand<T> {

	void assing(T val);
}
