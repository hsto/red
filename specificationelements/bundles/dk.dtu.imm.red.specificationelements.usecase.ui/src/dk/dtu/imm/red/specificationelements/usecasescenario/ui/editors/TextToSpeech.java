package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors;

import java.io.IOException;
import java.rmi.UnknownHostException;

import javazoom.jl.decoder.JavaLayerException;

public interface TextToSpeech {
	void SpeakText(String text) throws JavaLayerException, UnknownHostException, IOException;
	boolean isSpeaking();
}
