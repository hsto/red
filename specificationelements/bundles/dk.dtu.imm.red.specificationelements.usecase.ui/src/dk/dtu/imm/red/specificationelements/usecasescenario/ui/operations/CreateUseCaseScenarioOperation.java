package dk.dtu.imm.red.specificationelements.usecasescenario.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioFactory;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.extensions.UseCaseScenarioExtension;


public class CreateUseCaseScenarioOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected UseCaseScenarioExtension extension;
	protected Group parent;

	private UseCaseScenario scenario;

	public CreateUseCaseScenarioOperation(String label, String name, String description, Group parent, String path) {
		super("Create Scenario");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;

		extension = new UseCaseScenarioExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		scenario = UsecasescenarioFactory.eINSTANCE.createUseCaseScenario();
		scenario.setCreator(PreferenceUtil.getUserPreference_User());

		scenario.setLabel(label);
		scenario.setName(name);
		scenario.setDescription(description);

		if (parent != null) {
			scenario.setParent(parent);
		}

		scenario.save();
		extension.openElement(scenario);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(scenario);
			extension.deleteElement(scenario);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
