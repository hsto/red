package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ScalableColumnLayoutAdvisor;

public class InputOutputTableLayoutAdviser extends ScalableColumnLayoutAdvisor{


	private static final int INPUT_COLUMN = 0;
	private static final int OUTPUT_COLUMN = 1;
	private static final int NUM_COLUMNS = 2;
	private int rowCount;
	
	public InputOutputTableLayoutAdviser(AgileGrid agileGrid) {
		super(agileGrid);

		rowCount = 0;
	}
	
	@Override
	public int getColumnCount() {
		return NUM_COLUMNS;
	}

	@Override
	public String getTopHeaderLabel(final int col) {
		switch (col) {
		case INPUT_COLUMN:
			return "Input";
		case OUTPUT_COLUMN:
			return "Output";
		default:
			return super.getTopHeaderLabel(col);
		}
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	@Override
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	@Override
	public int getInitialColumnWidth(int column) {
		switch (column) {
			case INPUT_COLUMN:
				return 50;
			case OUTPUT_COLUMN:
				return 50;
			default:
				return 0;
		}
	}
}
