package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl;  
 
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardFactory;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecasescenario.*;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.UseCaseScenarioPresenter;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.UseCaseScenarioEditor;

/**
 * @author Johan Paaske Nielsen
 *
 */

public class UseCaseScenarioPresenterImpl extends BaseEditorPresenter implements
		UseCaseScenarioPresenter { 
	
	private UseCaseScenarioViewModel viewModel;
	private ImageLoader imgSaver = new ImageLoader();
	
	public UseCaseScenarioViewModel getViewModel() {
		return viewModel;
	}

	public UseCaseScenarioPresenterImpl(UseCaseScenarioEditor visionEditor, UseCaseScenarioViewModel viewModel) {
		super(viewModel.getElement());
		this.element = viewModel.getElement();
		this.viewModel = viewModel;
		
		//Create default scenario 
	}

	@Override
	public void save() {
		super.save();  
		viewModel.save(); 
	} 
	
	public void setImageDataForAction(Image imageData) {
		
		//If image has been cleared, just set picturedata to null
 		if(imageData == null) {
 			viewModel.getSelectedAction().setPictureData(null);
 			return;
 		}
		
		imgSaver.data = new ImageData[] {imageData.getImageData()};
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		imgSaver.save(outputStream, SWT.IMAGE_PNG);
		viewModel.getSelectedAction().setPictureData(outputStream.toByteArray()); 
	}

	public Action addNewScenarioAction(ActionType action) {
		if (viewModel.isOperator()) {
			return ((OperatorComponent) getToAddOperator()).createAction("New action", action);
		}
		else {
			 return ((OperatorComponent) getToAddOperator()).createAction("New action", action, getCurrentIndex());
		} 
	}
	
	public Operator addNewScenarioOperator(OperatorType operator) {
		if(viewModel.isOperator()) {
			return ((OperatorComponent)getToAddOperator()).createConnection("New operator", operator);
		}
		else {
			return ((OperatorComponent)getToAddOperator()).createConnection("New operator", operator, getCurrentIndex());
		} 
	}
	
	@Override
	public ScenarioReference addNewReferenceOperator(OperatorType operator) { 
		if(viewModel.isOperator()) {
			return ((OperatorComponent)getToAddOperator()).createScenarioReference("New operator", operator);
		}
		else {
			return ((OperatorComponent)getToAddOperator()).createScenarioReference("New operator", operator, getCurrentIndex());
		}
	
	}
	
	@Override
	public void updateScenarioReference() {
 		Class<?>[] filter =  { UseCase.class, UseCaseScenario.class };
		
		Object se = CreateElementReferenceWizardFactory.createElementReference(filter);
		
		if(se != null) {
			UseCaseReference selection = (UseCaseReference) viewModel.getCurrentScenarioElement(); 
			// Reset values, Added by Luai
			selection.setUseCase(null); 
			selection.setScenario(null);
			
			if(se instanceof UseCase) {
				selection.setUseCase((UseCase)se);
			}
			else if(se instanceof UseCaseScenario) {
				selection.setScenario((UseCaseScenario)se); 
			}
			
			if(selection.getScenario() == null) {
				if (se instanceof UseCase) {
					throw new IllegalArgumentException("The use case that you want to refer to is incomplete (no primary scenario).");
				} else {
					throw new IllegalArgumentException("Reference does not contain any scenario");					
				}
			}
			
			selection.getScenario().getStartOperator().setParent(selection); 
		}
	}
	
	private OperatorComponent getToAddOperator() {
		
		if(viewModel.isAction()) {
			return viewModel.getCurrentScenarioElement().getParent();
		}
		else if(viewModel.isOperator()) {
			return viewModel.getSelectedConnector();
		} 
		
		return null;
	}
	
	private int getCurrentIndex() {
		 int currentIndex = viewModel.getCurrentScenarioElement()
		 			.getParent().getChildElements().indexOf(viewModel.getCurrentScenarioElement());
		 return currentIndex;
	}

	@Override
	public void setScenarioDescription(String string) {
		viewModel.getCurrentScenarioElement().setDescription(string);
	}
	
	@Override
	public void setScenarioElementName(String string) {
		viewModel.getCurrentScenarioElement().setName(string);
	}

	public void setType(ActionType type) {
		Action a = (Action) viewModel.getCurrentScenarioElement();
		a.setType(type);
	}
	
	public void setType(OperatorType type) {
		OperatorComponent c = (OperatorComponent) viewModel.getCurrentScenarioElement();
		c.setType(type);
	} 	
	
	public void removeCurrentScenarioElement() {
		viewModel.getCurrentScenarioElement().remove(); 
	}
	
	public void removeScenarioElement(UseCaseScenarioElement data) {
		data.remove();
	}

	public void moveScenarioElement(int i, UseCaseScenarioElement target, OperatorComponent newParent) { 
		target.move(i, newParent);
	}
	
	public void setGuard(String value) {
		viewModel.getSelectedConnector().setGuard(value);
	}

	public void setDescription(String value) {
		viewModel.getCurrentScenarioElement().setDescription(value);
	}
	
//	public void setNameModified(String text) {
//		viewModel.getElement().setName(text); 
//	}
//
//	@Override
//	public void setIdModified(String text) {
//		viewModel.getElement().setId(text);; 
//	} 
	
	public Image getScenarioImage() {
		Image image;
		ImageLoader imageLoader = new ImageLoader();
		
		// If there is an image stored in the actions picturedata
		// we load that
		if (viewModel.getSelectedAction().getPictureData() != null 
				&& viewModel.getSelectedAction().getPictureData().length > 0) {
			
			ByteArrayInputStream inputStream = 
					new ByteArrayInputStream(viewModel.getSelectedAction().getPictureData());
			ImageData[] imageDataArray = imageLoader.load(inputStream);
			
			if (imageDataArray != null && imageDataArray.length > 0) {
				image = new Image(Display.getCurrent(), imageDataArray[0]);
				return image;
			}
		}
		
		// if loading the image from the persona's picturedata didn't work,
		// we try loading the picture from the persona image path
		if (viewModel.getSelectedAction().getPictureURI() != null) {
			image = new Image(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell().getDisplay(), 
					viewModel.getSelectedAction().getPictureURI());
			return image;
		}
		
		// If loading the image above didn't work, we load the default image.
		image = null; 
		
		return image;
	}

	 
}
