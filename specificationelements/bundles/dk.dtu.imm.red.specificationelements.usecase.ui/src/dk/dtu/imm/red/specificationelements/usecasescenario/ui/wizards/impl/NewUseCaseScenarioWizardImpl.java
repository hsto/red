package dk.dtu.imm.red.specificationelements.usecasescenario.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard; 
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.wizards.NewUseCaseScenarioWizard;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.wizards.NewUseCaseScenarioWizardPresenter;


public class NewUseCaseScenarioWizardImpl extends BaseNewWizard 
	implements NewUseCaseScenarioWizard {

	protected NewUseCaseScenarioWizardPresenter presenter;
	
	public NewUseCaseScenarioWizardImpl() {
		super("UseCaseScenario", UseCaseScenario.class);
		presenter = new NewUseCaseScenarioWizardPresenterImpl(this);
	}
	
	@Override
	public void addPages() {
		super.addPages();
	}
	
	@Override
	public boolean performFinish() {
		
		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription(); 
		
		Group parent = null;
				
		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();
			
			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}

		presenter.wizardFinished(label, name, description, parent, "");
		
		return true;
	}

	@Override
	public boolean canFinish() {
		return super.canFinish() 
				&& elementBasicInfoPage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}

}
