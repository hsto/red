package dk.dtu.imm.red.specificationelements.actor.ui.editors.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.SpecificationElement;

public class ActorComposite extends Composite {

	private Composite cmpTop;   
	private ActorEditorImpl editorParent;
	private Composite compOptional;
	private Composite behaviourColumnContainer; 

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ActorComposite(Composite parent, int style, String groupName) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		createLayout(groupName);
	}
	
	public ActorComposite(Composite parent, int style, ActorEditorImpl editorParent, String groupName) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		this.editorParent = editorParent;
		createLayout(groupName);
	}

	protected void createLayout(String groupName) {
		cmpTop = new Composite(this, SWT.NONE);
		GridData gd_cmpTop = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_cmpTop.heightHint = 342;
		cmpTop.setLayoutData(gd_cmpTop);
		cmpTop.setLayout(new GridLayout(1, false));
		
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;
		
		ElementBasicInfoContainer basicInfoContainer = new ElementBasicInfoContainer(cmpTop, SWT.NONE, editorParent, 
				(SpecificationElement) editorParent.getEditorElement());
		editorParent.setBasicInfoContainer(basicInfoContainer);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);
		
		Group gpActorEditor = new Group(cmpTop, SWT.NONE);
		gpActorEditor.setText(groupName);
		gpActorEditor.setLayout(new GridLayout(1, false));
		gpActorEditor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		SashForm sashForm = new SashForm(gpActorEditor, SWT.NONE);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		//Set up behaviour column
		Composite sashBehaviour = new Composite(sashForm, SWT.NONE);
		sashBehaviour.setLayout(new GridLayout(1, false));
		
		Label lbl = new Label(sashBehaviour, SWT.NONE);
		lbl.setText("Behaviour (e.g. use cases)");
		
		behaviourColumnContainer = new Composite(sashBehaviour, SWT.NONE);
		behaviourColumnContainer.setLayout(new GridLayout(1, false));
		behaviourColumnContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		//Set up optional area
		compOptional = new Composite(sashForm, SWT.NONE);
		
		sashForm.setWeights(new int[] {1, 2});
	}
	
	public Composite getBehaviourColumnContainer() {
		return behaviourColumnContainer;
	}

	public Composite getOptionalComposite() {
		return compOptional;
	}
}
