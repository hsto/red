package dk.dtu.imm.red.specificationelements.usecase.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;


import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.ui.editors.UseCaseEditor;
import dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl.UseCaseEditorInputImpl;


public class UseCaseExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element){
		if (element instanceof UseCase) {
			UseCaseEditorInputImpl editorInput = new UseCaseEditorInputImpl((UseCase) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, UseCaseEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}

}
