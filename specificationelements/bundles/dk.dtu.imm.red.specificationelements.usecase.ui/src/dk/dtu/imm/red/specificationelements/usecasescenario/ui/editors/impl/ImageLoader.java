package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl; 
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor; 
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import dk.dtu.imm.red.specificationelements.usecasescenario.*;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.UseCaseScenarioEditor;

public class ImageLoader {

	private Image sequenceImage = null; 
	private Image actionImage = null;
	private Image optionalImage = null;
	private Image strictImage = null;
	private Image breakImage = null;
	private Image alternateImage = null;
	private Image criticalImage = null;
	private Image loopImage = null;
	private Image parallelImage = null;
	private Image receiveImage = null;
	private Image scenarioImage = null;
	private Image usecaseImage = null;
	private Image startImage = null;
	private Image stopImage = null;
	private Image referenceImage = null;
	private Image sendImage = null;
	private Image textImage = null;
	private Image directionImage = null;

	

	public ImageLoader() {

		//Connector Type images
		startImage = loadImageFile("icons/scenarioeditor/Start.png");
		stopImage = loadImageFile("icons/scenarioeditor/Stop.png");
		sequenceImage = loadImageFile("icons/scenarioeditor/Sequence.png");
		strictImage = loadImageFile("icons/scenarioeditor/Strict.png");
		alternateImage = loadImageFile("icons/scenarioeditor/Alternate.png");
		breakImage = loadImageFile("icons/scenarioeditor/Break.png");
		criticalImage = loadImageFile("icons/scenarioeditor/Critical.png");
		loopImage = loadImageFile("icons/scenarioeditor/Loop.png"); 
		optionalImage = loadImageFile("icons/scenarioeditor/Optional.png");
		parallelImage = loadImageFile("icons/scenarioeditor/Parallel.png"); 

		//Action Type images
		actionImage = loadImageFile("icons/scenarioeditor/Action.png");
		receiveImage = loadImageFile("icons/scenarioeditor/Receive.png");
		sendImage = loadImageFile("icons/scenarioeditor/Send.png");
		textImage = loadImageFile("icons/scenarioeditor/Text.png");
		directionImage = loadImageFile("icons/scenarioeditor/Direction.png");
		
		//Reference
		referenceImage = loadImageFile("icons/scenarioeditor/Reference.png");
		
		//Scenario Reference images
		scenarioImage = loadImageFile("icons/ucscenario.png");
		usecaseImage = loadImageFile("icons/usecase.png");


	}

	private Image loadImageFile(String path) {
		Bundle bundle = FrameworkUtil.getBundle(UseCaseScenarioEditor.class);
		URL url = FileLocator.find(bundle, new Path(path), null);
		ImageDescriptor imageDcr = ImageDescriptor.createFromURL(url);
		return imageDcr.createImage();
	}   

	public Image getByInstance(Object element) { 
		
		if(element instanceof Action) {
			Action e = (Action) element;
			if(e.getType() == ActionType.ACTION) {
				return actionImage;
			} else if(e.getType() == ActionType.RECEIVE) {
				return receiveImage;
			} else if(e.getType() == ActionType.SEND) {
				return sendImage;
			}  else if(e.getType() == ActionType.START) {
				return startImage;
			}   else if(e.getType() == ActionType.STOP) {
				return stopImage;
			}   else if(e.getType() == ActionType.TEXT) {
				return textImage;
			}   else if(e.getType() == ActionType.DIRECTION) {
				return directionImage;
			}
		} else if(element instanceof ScenarioReference) {
			return referenceImage; 
		} else if (element instanceof Operator ) {
			Operator e = (Operator) element;
			
			if(e.getType() == OperatorType.SEQUENCE) {
				return sequenceImage; 
			} else if(e.getType() == OperatorType.STRICT) {
				return strictImage;
			} else if(e.getType() == OperatorType.ALTERNATE) {
				return alternateImage;
			} else if(e.getType() == OperatorType.BREAK) {
				return breakImage;
			} else if(e.getType() == OperatorType.CRITICAL) {
				return criticalImage;
			} else if(e.getType() == OperatorType.LOOP) {
				return loopImage;
			} else if(e.getType() == OperatorType.OPTIONAL) {
				return optionalImage;
			} else if(e.getType() == OperatorType.PARALLEL) {
				return parallelImage;
			} 
		}

		return null;
	}
	
	public Image getSequenceImage() {
		return sequenceImage;
	}

	public Image getActionImage() {
		return actionImage;
	}

	public Image getOptionalImage() {
		return optionalImage;
	}

	public Image getStrictImage() {
		return strictImage;
	}

	public Image getBreakImage() {
		return breakImage;
	}

	public Image getAlternateImage() {
		return alternateImage;
	}

	public Image getCriticalImage() {
		return criticalImage;
	}

	public Image getLoopImage() {
		return loopImage;
	}

	public Image getParallelImage() {
		return parallelImage;
	}

	public Image getMessageImage() {
		return receiveImage;
	}

	public Image getScenarioImage() {
		return scenarioImage;
	}

	public Image getUsecaseImage() {
		return usecaseImage;
	}

	public Image getStartImage() {
		return startImage;
	}

	public Image getStopImage() {
		return stopImage;
	}

	public Image getReferenceImage() {
		return referenceImage;
	} 
	
	public Image getReceiveImage() {
		return receiveImage;
	}

	public Image getSendImage() {
		return sendImage;
	}
	
	public Image getTextImage() {
		return textImage;
	}
	
	public Image getDirectionImage() {
		return directionImage;
	}
} 