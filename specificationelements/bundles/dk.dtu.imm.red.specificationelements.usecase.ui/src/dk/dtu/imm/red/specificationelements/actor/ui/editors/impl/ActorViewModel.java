package dk.dtu.imm.red.specificationelements.actor.ui.editors.impl;  
import dk.dtu.imm.red.core.ui.presenters.ElementHandler;
import dk.dtu.imm.red.specificationelements.actor.Actor;

public class ActorViewModel extends ElementHandler<Actor>{
	
	 
	private int selectedRoleIndex; 
	 

	public void setSelectedRoleIndex(int selectedIndex) {
		this.selectedRoleIndex = selectedIndex; 
	}
	
	public int getSelectedRoleIndex( ) {
		return selectedRoleIndex; 
	} 
}