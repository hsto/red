/*
 * @author Johan Paaske Nielsen
 */
package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.editors.SearchEditorHelper;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.widgets.ImagePanelWidget;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.usecasescenario.*;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.*;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.tree.*;

/**
 * The Class UcscenarioEditorImpl.
 */
public class UseCaseScenarioEditorImpl extends BaseEditor implements UseCaseScenarioEditor {


	protected ElementBasicInfoContainer basicInfoContainer;

	private  int scenarioPageIndex=0;
	private int enactmentPageIndex=1;
	private UseCaseScenarioPresenter useCaseScenarioPresenter;
	private UseCaseScenarioViewModel viewModel;
	private UseCaseScenarioComposite scenarioComposite;
	private boolean isFilling = false;
	private ImagePanelWidget imageContainer;
	private TextToSpeech textToSpeech;
	private EnactmentHandler enactmentHandler;

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);
		viewModel = new UseCaseScenarioViewModel();
		((UseCaseScenario) element).createDefaultIfEmpty();
		viewModel.setElement((UseCaseScenario) element);
		presenter = new UseCaseScenarioPresenterImpl(this, viewModel);
		useCaseScenarioPresenter = (UseCaseScenarioPresenterImpl) presenter; // casted presenter
		textToSpeech = new TextToSpeechManager();
		enactmentHandler = new EnactmentHandler(this, viewModel);
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#createPages()
	 */
	@Override
	protected void createPages() {

		//Create attributes page
		scenarioPageIndex = addPage(createScenarioPage(getContainer()));
		setPageText(scenarioPageIndex, "Summary");

		enactmentPageIndex = addPage(createEnactmentPage(getContainer()));
		setPageText(enactmentPageIndex, "Enactment");


		super.createPages();
		fillInitialData();
	}


	private Composite createScenarioPage(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (SpecificationElement) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);

		return composite;
//		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}
	/**
	 * Creates the scenario page.
	 *
	 * @param parent the parent
	 * @return the composite
	 */
	private Composite createEnactmentPage(Composite parent) {
		scenarioComposite = new UseCaseScenarioComposite(parent, SWT.NONE, this);

		imageContainer = new ImagePanelWidget(scenarioComposite.getImagePlaceholder(),
				SWT.NONE, false, "Picture of action",
				null, "");
		imageContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		imageContainer.addListener(SWT.Modify, modifyListener);
		imageContainer.addListener(SWT.Modify, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if(!isFilling) {
					useCaseScenarioPresenter.setImageDataForAction(imageContainer.getImage());
				}
			}
		});

		return SWTUtils.wrapInScrolledComposite(parent, scenarioComposite);
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.specificationelements.ucscenario.ui.editors.UcscenarioEditor#setIsModified()
	 */
	public void setIsModified() {
		modifyListener.handleEvent(null);
	}

	/**
	 * Default str.
	 *
	 * @param str the str
	 * @return the string
	 */
	public String defaultStr(String str) {
		return str != null ? str : "";
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
		return;
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		UseCaseScenarioPresenter presenter = (UseCaseScenarioPresenter) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		super.doSave(monitor);


	}

	//Fill UI widgets with data
	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#fillInitialData()
	 */
	@Override
	protected void fillInitialData() {

//		scenarioComposite.getTxtName().setText(defaultStr(viewModel.getElement().getName()));
//		scenarioComposite.getTxtID().setText(defaultStr(viewModel.getElement().getId()));

		basicInfoContainer.fillInitialData(viewModel.getElement(), new String[]{});

		fillScenarioTree();

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	/**
	 * Fill scenario tree.
	 */
	private void fillScenarioTree() {
		scenarioComposite.getTreeViewer().setContentProvider(new TreeContentProvider());
		scenarioComposite.getTreeViewer().setLabelProvider(new TreeLabelProvider());
		scenarioComposite.getTreeViewer().setInput(viewModel.getElement().getStartOperator());
		Transfer[] transferTypes = new Transfer[]{ LocalSelectionTransfer.getTransfer()};
		int operations = DND.DROP_COPY| DND.DROP_MOVE;
		scenarioComposite.getTreeViewer().addDragSupport(operations, transferTypes ,
				new TreeDragListener(scenarioComposite.getTreeViewer()));
		scenarioComposite.getTreeViewer().addDropSupport(operations, transferTypes,
				new TreeDropListener(scenarioComposite.getTreeViewer(), this));
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.specificationelements.ucscenario.ui.editors.UcscenarioEditor#selectedScenarioElementChanged(dk.dtu.imm.red.specificationelements.ucScenario.ScenarioElement)
	 */
	public void selectedScenarioElementChanged(UseCaseScenarioElement se) {
		isFilling = true;
		viewModel.setResolvingAutoPlay(false);
		viewModel.setCurrentScenarioElement(se);
		updateComboTypes();

		setComboType();
		setGuardText();
		setDescriptionText();
		setPicture();
		setUIState();
		isFilling = false;
	}

	/**
	 * Sets the ui state.
	 */
	private void setUIState() {

		//Set enabled Menu Options
		boolean currentIsNotNull = viewModel.getCurrentScenarioElement() != null;
		boolean canEdit =  canEdit();
		boolean menuEnabled = currentIsNotNull && canEdit;
		boolean onRootReferenceItem = (viewModel.getCurrentScenarioElement() instanceof ScenarioReference);
		boolean onStartItem = viewModel.isAction() &&
				((Action) viewModel.getCurrentScenarioElement()).getType() == ActionType.START;
		boolean onStopItem = viewModel.isAction() &&
				((Action) viewModel.getCurrentScenarioElement()).getType() == ActionType.STOP;

		scenarioComposite.getBtnDelete().setEnabled(menuEnabled && !(onStartItem || onStopItem));
		scenarioComposite.getMntmNew().setEnabled(menuEnabled && !(onRootReferenceItem) && !onStopItem);
		scenarioComposite.getBtnRename().setEnabled(menuEnabled);
		scenarioComposite.getTxtDescription().setEnabled(menuEnabled);

		if(onRootReferenceItem) {
			UseCaseScenario ref = ((ScenarioReference) viewModel.getCurrentScenarioElement()).getScenario();
			scenarioComposite.getLblScenarioReference().setText(ref != null ? ref.getName() : "None");
		}

		imageContainer.getDefaultImageButton().setEnabled(menuEnabled);
		imageContainer.getChangeImageButton().setEnabled(menuEnabled);

		setExcludedCompositeVisibility(onRootReferenceItem && !viewModel.isEnactmentModeEnabled(),
				getScenarioComposite().getCompositeScenarioRef());
	}

	private boolean canEdit() {
		TreeItem item = getFirstTreeSelection();

		while(item != null) {
			item = item.getParentItem();
			if(item != null && item.getData() instanceof ScenarioReference) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Gets the first tree selection.
	 *
	 * @return the first tree selection
	 */
	private TreeItem getFirstTreeSelection() {
		if(scenarioComposite.getTreeViewer().getTree().getSelection().length > 0) {
			TreeItem sel = scenarioComposite.getTreeViewer().getTree().getSelection()[0];
			return sel;
		}

		return null;
	}

	private int getSelectedTreeItemIndex() {
		if(getFirstTreeSelection() != null) {
			if(getFirstTreeSelection().getParentItem() == null) {
				return getFirstTreeSelection().getParent().indexOf(getFirstTreeSelection());
			}
			else {
				return getFirstTreeSelection().getParentItem().indexOf(getFirstTreeSelection());
			}
		}

		return -1;
	}

	private TreeItem[] getParentTreeItems(TreeItem sel) {

		TreeItem firstSelection = getFirstTreeSelection();

		if(firstSelection != null) {
			TreeItem parent = firstSelection.getParentItem();

			if(parent == null) {
				return firstSelection.getParent().getItems();
			}
			else {
				return firstSelection.getParentItem().getItems();
			}
		}

		return null;
	}

	/**
	 * Sets the tree item manually.
	 *
	 * @param t the new tree item manually
	 */
	private void setTreeItemManually(TreeItem t) {
		scenarioComposite.getTreeViewer().getTree().setSelection(t);
		selectedScenarioElementChanged((UseCaseScenarioElement) t.getData());
	}

	public void selectedTypeChanged(SelectionEvent e) {

		String selectedItem = scenarioComposite.getCmbType().getItem(scenarioComposite.getCmbType().getSelectionIndex());
		if(viewModel.isOperator()) {
			useCaseScenarioPresenter.setType(OperatorType.valueOf(selectedItem.toUpperCase()));
		}
		else if(viewModel.isAction()) {
			useCaseScenarioPresenter.setType(ActionType.valueOf(selectedItem.toUpperCase()));
		}

		refreshTree();
	}

	private void setGuardText() {
		if(viewModel.isOperator()) {
			scenarioComposite.getTxtGuard().setText(defaultStr(viewModel.getSelectedConnector().getGuard()));
		}

		scenarioComposite.getTxtGuard().setVisible(viewModel.isOperator());
		scenarioComposite.getLblGuard().setVisible(viewModel.isOperator());
	}

	private void setDescriptionText() {
		setExcludedCompositeVisibility(viewModel.getCurrentScenarioElement() != null, scenarioComposite.getCmpNameDescription());

		if(viewModel.getCurrentScenarioElement() != null) {
			String description = viewModel.getCurrentScenarioElement().getDescription();
			String name = viewModel.getCurrentScenarioElement().getName();
			scenarioComposite.getTxtDescription().setText(defaultStr(description));
			scenarioComposite.getTxtScenarioComponentName().setText(defaultStr(name));

			if(viewModel.isTextToSpeechEnabled()) {
				String textToSpeek = String.format("%s. %s.",
						name != null ? name : "",
						description != null ? description : "");
				try {
					textToSpeech.SpeakText(textToSpeek);
				} catch (Exception e) {
					e.printStackTrace(); LogUtil.logError(e);
				}
			}

		}
	}


	public void updateGuardText(ModifyEvent e) {
		if(!isFilling) {
			useCaseScenarioPresenter.setGuard(scenarioComposite.getTxtGuard().getText());
			modifyListener.handleEvent(null);
			refreshTree();
		}
	}

	@Override
	public void updateScenarioComponentName() {
		if(!isFilling) {
			useCaseScenarioPresenter.setScenarioElementName(scenarioComposite.getTxtScenarioComponentName().getText());
			modifyListener.handleEvent(null);
			refreshTree();
		}
	}

	@Override
	public void updateScenarioComponentDescription() {
		if(!isFilling) {
			useCaseScenarioPresenter.setDescription(scenarioComposite.getTxtDescription().getText());
			modifyListener.handleEvent(null);
			refreshTree();
		}
	}

	private void updateComboTypes() {
		ArrayList<String> types = new ArrayList<String>();

		if(viewModel.isOperator()) {
			for (OperatorType t : OperatorType.values()) {
				types.add(t.getLiteral());
			}
		}

		if(viewModel.isAction()) {
			for (ActionType t : ActionType.values()) {
				types.add(t.getLiteral());
			}
		}

		scenarioComposite.getCmbType().setItems(types.toArray(new String[types.size()]));
	}

	private void setComboType() {

		scenarioComposite.getCmbType().setVisible(viewModel.getCurrentScenarioElement() != null);
		scenarioComposite.getLblType().setVisible(viewModel.getCurrentScenarioElement() != null);

		if(viewModel.isOperator()) {
			scenarioComposite.getCmbType().select(scenarioComposite.getCmbType().indexOf(
					viewModel.getSelectedConnector().getType().getLiteral()));
		}

		if(viewModel.isAction()) {
			scenarioComposite.getCmbType().select(scenarioComposite.getCmbType().indexOf(
					viewModel.getSelectedAction().getType().getLiteral()));
		}
	}

	private void setPicture() {
		scenarioComposite.getImagePlaceholder().setVisible(viewModel.isAction());

		if(viewModel.isAction()) {
			imageContainer.setImage(useCaseScenarioPresenter.getScenarioImage());
		}
	}

	public void addAct() {
		useCaseScenarioPresenter.addNewScenarioAction(ActionType.ACTION);
		setIsModified();
		refreshTree();
	}

	@Override
	public void addReceive() {
		useCaseScenarioPresenter.addNewScenarioAction(ActionType.RECEIVE);
		setIsModified();
		refreshTree();

	}

	public void addSend() {
		useCaseScenarioPresenter.addNewScenarioAction(ActionType.SEND);
		setIsModified();
		refreshTree();
	}

	public void addText() {
		useCaseScenarioPresenter.addNewScenarioAction(ActionType.TEXT);
		setIsModified();
		refreshTree();
	}


	public void addDirection() {
		useCaseScenarioPresenter.addNewScenarioAction(ActionType.DIRECTION);
		setIsModified();
		refreshTree();
	}


	public void addSeq() {
		useCaseScenarioPresenter.addNewScenarioOperator(OperatorType.SEQUENCE);
		setIsModified();
		refreshTree();
	}

	public void addAlt() {
		useCaseScenarioPresenter.addNewScenarioOperator(OperatorType.ALTERNATE);
		setIsModified();
		refreshTree();
	}

	public void addPar() {
		useCaseScenarioPresenter.addNewScenarioOperator(OperatorType.PARALLEL);
		setIsModified();
		refreshTree();
	}

	public void addStr() {
		useCaseScenarioPresenter.addNewScenarioOperator(OperatorType.STRICT);
		setIsModified();
		refreshTree();
	}

	public void addOpt() {
		useCaseScenarioPresenter.addNewScenarioOperator(OperatorType.OPTIONAL);
		setIsModified();
		refreshTree();
	}

	public void addBrk() {
		useCaseScenarioPresenter.addNewScenarioOperator(OperatorType.BREAK);
		setIsModified();
		refreshTree();
	}


	@Override
	public void addLoop() {
		useCaseScenarioPresenter.addNewScenarioOperator(OperatorType.LOOP);
		setIsModified();
		refreshTree();
	}


	@Override
	public void addReferenceSeq() {
		addScenarioReference(OperatorType.SEQUENCE);
	}

	private void addScenarioReference(OperatorType typ) {
		useCaseScenarioPresenter.addNewReferenceOperator(typ);
		setIsModified();
		refreshTree();
	}

	public void removeSelectedScenarioElement() {

		if(messageBox("Confirm Action",  "Do you want to delete the selected items",
						SWT.YES | SWT.NO | SWT.ICON_INFORMATION) == SWT.NO) {
					return;
		};

		TreeItem[] selection = scenarioComposite.getTreeViewer().getTree().getSelection();

		for(TreeItem ti : selection) {
			useCaseScenarioPresenter.removeScenarioElement((UseCaseScenarioElement)ti.getData());
		}

		useCaseScenarioPresenter.removeCurrentScenarioElement();
		setIsModified();
		refreshTree();
	}

	public void moveScenarioElement(int location, UseCaseScenarioElement selection, UseCaseScenarioElement target) {

		boolean isStartOrStopAction = selection instanceof Action && (((Action) selection).getType() == ActionType.START
												|| ((Action) selection).getType() == ActionType.STOP);

		boolean isInFrontOfStartAction = target instanceof Action && location == 1 && ((Action) target).getType() == ActionType.START;
		boolean isAfterStopAction = target instanceof Action && location == 2 && ((Action) target).getType() == ActionType.STOP;

		//Illegal Places to drop Scenario Element
		if(isParentInNewLocation(selection, target) ||
				isStartOrStopAction ||
				isInFrontOfStartAction ||
				isAfterStopAction)
			return;

		//Before (1) or after (2) target
		if(location == 1 || location == 2) {
			int targetIndex = target.getParent().getChildElements().indexOf(target);
			int selectionIndex = selection.getParent().getChildElements().indexOf(selection);
			int calcPos = -1;

			if(target.getParent() == selection.getParent()) {

				if(targetIndex > selectionIndex) {
					calcPos = location == 1 ? targetIndex - 1 : targetIndex;
				}
				else
				{
					calcPos = location == 1 ? targetIndex : targetIndex + 1  ;
				}
			}
			else {
				calcPos = location == 1 ? targetIndex : targetIndex + 1  ;
			}


			if(selection == target) calcPos = targetIndex;
			useCaseScenarioPresenter.moveScenarioElement(calcPos, selection, target.getParent());
		}

		//On target (3)
		if(location == 3 && !(selection == target)) {
			Operator con = (Operator) target;
			int targetIndex = con.getChildElements().size();
			useCaseScenarioPresenter.moveScenarioElement(targetIndex, selection, (Operator) target);
		}
		setIsModified();
		refreshTree();
	}

	private boolean isParentInNewLocation(UseCaseScenarioElement selection, UseCaseScenarioElement target) {
		boolean isParentInNewLocation = false;
		OperatorComponent parent = target.getParent();

		do {
			if(parent == selection) isParentInNewLocation = true;
			parent = parent.getParent();
		}
		while(parent != null);

		return isParentInNewLocation;
	}

	private void refreshTree() {

		//For remembering expansion state
		Object[] expandedElements = scenarioComposite.getTreeViewer().getExpandedElements();
		TreePath[] expandedTreePaths = scenarioComposite.getTreeViewer().getExpandedTreePaths();

		//Update input
		scenarioComposite.getTreeViewer().setInput(viewModel.getElement().getStartOperator());

		//Restore expansion state
		scenarioComposite.getTreeViewer().setExpandedElements(expandedElements);
		scenarioComposite.getTreeViewer().setExpandedTreePaths(expandedTreePaths);
	}

	//Infrastructure actions
	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(scenarioPageIndex);
			//			visionEditor.executeCommand(RichTextCommand.SELECT_TEXT,
			//					new String[] {"" + startIndex, "" + endIndex});
		} else if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(scenarioPageIndex);
			//titleTextBox.setSelection(startIndex, endIndex);
		}
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();
		UseCaseScenario s = viewModel.getElement();
		attributesToSearch.put(UsecasescenarioPackage.Literals.USE_CASE_SCENARIO__ID,
				s.getName());

		return SearchEditorHelper.search(param, attributesToSearch, element);
	}

	//Properties
	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.ucscenario.ucscenarioeditor";
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/ucscenario.html";
	}

	@Override
	public void selectScenarioReference() {
		try {
			useCaseScenarioPresenter.updateScenarioReference();
		}
		//Use case with no primary Interaction
		catch(IllegalArgumentException a) {
			messageBox("Invalid Reference", a.getMessage(), SWT.ERROR);
			return;
		}

		ScenarioReference se = (ScenarioReference) viewModel.getCurrentScenarioElement();

		//If User selected some scenario
		if(se.getScenario() != null) {
			scenarioComposite.getLblScenarioReference().setText(se.getScenario().getName());
			refreshTree();
		}

	}

	/**
	 * Helper method for displaying Message box.
	 *
	 * @param text the header text
	 * @param message the message
	 */
	private int messageBox(String text, String message, int style) {
		MessageBox dialog =
				new MessageBox(Display.getDefault().getActiveShell(), style);
		dialog.setText(text);
		dialog.setMessage(
				message);
		return dialog.open();
	}


	@Override
	public void EnableEnactment() {
		enactmentHandler.run();
	}

	@Override
	public TextToSpeech getTextToSpeech() {
		return textToSpeech;
	}

	@Override
	public UseCaseScenarioComposite getScenarioComposite() {
		return scenarioComposite;
	}

	@Override
	public void toggleTextToSpeech() {
		viewModel.setTextToSpeechEnabled(!viewModel.isTextToSpeechEnabled());
	}

	@Override
	public void toggleAutoPlay() {
		viewModel.setAutoPlayingEnabled(!viewModel.isAutoPlayingEnabled());

		if(viewModel.isAutoPlayingEnabled()) {
			scenarioComposite.getBtnAutoPlay().setText("Stop ■");
			enactmentHandler.play();
		} else {
			scenarioComposite.getBtnAutoPlay().setText("Play ▶");
			enactmentHandler.stop();
		}

	}

	@Override
	public void goNext() {
		TreeItem item = getParentTreeItems(getFirstTreeSelection())[getSelectedTreeItemIndex() + 1];
		setTreeItemManually(item);
		viewModel.setResolvingAutoPlay(false);
	}

	@Override
	public void goExplore() {
		if(getFirstTreeSelection().getData() instanceof OperatorComponent) {
			OperatorComponent e = (OperatorComponent) getFirstTreeSelection().getData();
			scenarioComposite.getTreeViewer().expandToLevel(e.getChildElements().get(0), 1);
			TreeItem item = getFirstTreeSelection().getItem(0);
			setTreeItemManually(item);
			viewModel.setResolvingAutoPlay(false);
		}
	}

	@Override
	public void setExcludedCompositeVisibility(boolean isVisible, Composite composite) {
		GridData lData = (GridData) composite.getLayoutData();
		lData.exclude = !isVisible;
		composite.setLayoutData(lData);
		composite.setVisible(isVisible);
		getScenarioComposite().redraw();
		getScenarioComposite().layout();
	}

	public void setBasicInfoContainer(ElementBasicInfoContainer basicInfoContainer){
		this.basicInfoContainer = basicInfoContainer;
	}
}
