package dk.dtu.imm.red.specificationelements.actor.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.actor.ui.extensions.ActorExtension;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.ActorFactory;

public class CreateActorOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected ActorExtension extension;
	protected Group parent;
//	protected String path;
	
	private Actor actor;
//	private String id;

	public CreateActorOperation(String label, String name, String description, Group parent, String path) {
		super("Create Actor");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
//		this.path = path;
		extension = new ActorExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		actor = ActorFactory.eINSTANCE.createActor();
		actor.setCreator(PreferenceUtil.getUserPreference_User());

		actor.setLabel(label);
		actor.setName(name);
		actor.setDescription(description);

		if (parent != null) {
			actor.setParent(parent);
		}

		actor.save();
		extension.openElement(actor);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(actor);
			extension.deleteElement(actor);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
