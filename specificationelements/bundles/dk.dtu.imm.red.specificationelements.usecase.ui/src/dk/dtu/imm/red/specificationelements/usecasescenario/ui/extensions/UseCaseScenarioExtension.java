package dk.dtu.imm.red.specificationelements.usecasescenario.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.UseCaseScenarioEditor;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.UseCaseScenarioEditorInputImpl;


public class UseCaseScenarioExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element) {
		if (element instanceof UseCaseScenario) {
			UseCaseScenarioEditorInputImpl editorInput = 
					new UseCaseScenarioEditorInputImpl((UseCaseScenario) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, UseCaseScenarioEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}
}
