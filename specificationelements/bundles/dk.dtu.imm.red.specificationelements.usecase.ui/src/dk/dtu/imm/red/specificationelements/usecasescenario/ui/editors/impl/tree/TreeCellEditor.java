package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.tree;

import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TreeViewer; 
import org.eclipse.swt.widgets.TreeItem;

import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;

public class TreeCellEditor implements ICellModifier {

	private TreeViewer treeViewer;
	
	public TreeCellEditor(TreeViewer viewer) {
		this.treeViewer = viewer;
	}
	
	@Override
	public boolean canModify(Object element, String property) {
		return true;
	}

	@Override
	public Object getValue(Object element, String property) { 
		return ((UseCaseScenarioElement)element).getDescription();
	}

	@Override
	public void modify(Object element, String property, Object value) {
		TreeItem t = (TreeItem) element;
		((UseCaseScenarioElement)t.getData()).setDescription(value.toString());
		this.treeViewer.update(element, null);
	} 
}
