//package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;
// 
// 
//
//import java.io.File;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.Reader;
//import java.io.StringReader;
//import java.io.StringWriter;
//import java.io.UnsupportedEncodingException;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.transform.Transformer;
//import javax.xml.transform.TransformerException;
//import javax.xml.transform.TransformerFactory;
//import javax.xml.transform.dom.DOMSource;
//import javax.xml.transform.stream.StreamResult;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//import org.xml.sax.InputSource;
//
//import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.Scenario;
//import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.ScenarioExtension;
//import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.ScenarioStep;
//import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.Usecase;
//
//public class EnactmentGenerator { 
// 
//	private Usecase usecase;
//	private Document dom;
//	
//	public Document generateEnactmentHTMLModel(Usecase usecase) throws Exception {  
//		this.usecase = usecase;
//	
//		//Create dom. Load JQueryMobile template html
//		InputStream url = EnactmentGenerator.class.getClassLoader().getResourceAsStream("EnactmentTemplate.html");  
//		String text = slurp(url, 1024);  
//		this.dom = loadXMLFromString(text);
//		
//		//Locate body element from template
//		Node bodyElement = dom.getElementsByTagName("body").item(0);
//		
//		//Create JQM page for every scenario and every step
//		for(Scenario se : usecase.getScenarios()) { 
//			createPage(se, bodyElement);
//			
//			for(ScenarioStep ss : se.getSteps()) {
//				createPage(ss, bodyElement);
//			} 
//		}
//		
//		String htmldoc = getStringFromDocument(dom);
//		
//		return dom;
//	}  
//	
//	//Reads a text file, from a given file input path
//	public String slurp(final InputStream is, final int bufferSize)
//	{
//	  final char[] buffer = new char[bufferSize];
//	  final StringBuilder out = new StringBuilder();
//	  try {
//	    final Reader in = new InputStreamReader(is, "UTF-8");
//	    try {
//	      for (;;) {
//	        int rsz = in.read(buffer, 0, buffer.length);
//	        if (rsz < 0)
//	          break;
//	        out.append(buffer, 0, rsz);
//	      }
//	    }
//	    finally {
//	      in.close();
//	    }
//	  }
//	  catch (UnsupportedEncodingException ex) {
//	    /* ... */
//	  }
//	  catch (IOException ex) {
//	      /* ... */
//	  }
//	  return out.toString();
//	}
//	
//	/**
//	 * Reads a Document object, and returns it String representation
//	 * @param doc
//	 * @return
//	 */
//	public String getStringFromDocument(Document doc)
//	{
//	    try
//	    {
//	       DOMSource domSource = new DOMSource(doc);
//	       StringWriter writer = new StringWriter();
//	       StreamResult result = new StreamResult(writer);
//	       TransformerFactory tf = TransformerFactory.newInstance();
//	       Transformer transformer = tf.newTransformer();
//	       transformer.transform(domSource, result);
//	       return writer.toString();
//	    }
//	    catch(TransformerException ex)
//	    {
//	       e.printStackTrace(); LogUtil.logError(e);
//	       return null;
//	    }
//	}
//	
//	private void createPage(Object scenarioItem, Node bodyElement) {
//		
//		boolean isStep = scenarioItem instanceof ScenarioStep; 
//		ScenarioStep nextStep; //Next step in the scenario
//		String currentDescription; //Desc. both for a scenario or a step
//		String currentImageURL = "test.png"; //Image url both for a scenario or a step
//		String currentUniqueID;
//		List<ScenarioExtension> branches = new ArrayList<ScenarioExtension>(); 	  
//		
//		//Setup description variables, based on type
//		//Sets the above variables, either if it's a scenario or a scenario step. 
//		if(isStep) {
//			ScenarioStep step = (ScenarioStep) scenarioItem;
//			//Get next step
//			int nextStepIndex = step.getContainer().getSteps().indexOf(step) + 1;
//			nextStepIndex = nextStepIndex > step.getContainer().getSteps().size() - 1 ? -1 : nextStepIndex; 
//			nextStep =  nextStepIndex != -1 ? step.getContainer().getSteps().get(nextStepIndex) : null;
//			currentDescription = step.getDescription();
//			currentUniqueID = step.getUniqueID();
//			
//			//Get number of branches for current step
//			for(Object o : usecase.getScenarios().toArray()) {
//				if(o instanceof ScenarioExtension) {
//					ScenarioExtension se = (ScenarioExtension) o;
//					if(se.getExtensionStep() == step) {
//						branches.add(se);
//					}
//				}
//			} 
//		}
//		else {
//			Scenario sc = (Scenario) scenarioItem;
//			nextStep = sc.getSteps().size() > 0 ? sc.getSteps().get(0) : null;
//			currentDescription = sc.getName();
//			
//			currentUniqueID = "main_scenario";
//			
//			if(scenarioItem instanceof ScenarioExtension) {
//				ScenarioExtension se = (ScenarioExtension)  scenarioItem;
//				currentUniqueID = se.getUniqueID();
//			}
//		} 
//		
//		//Create Page div
//		Element pageDivNode = dom.createElement("div");
//		pageDivNode.setAttribute("id",currentUniqueID);
//		pageDivNode.setAttribute("data-role", "page");  
//		pageDivNode.setAttribute("data-theme", "b"); 
//		pageDivNode.setAttribute("class", "my-page");
//		bodyElement.appendChild(pageDivNode);
//		
//		//Create content child div
//		Element contentDivNode = dom.createElement("div");  
//		contentDivNode.setAttribute("role", "main");  
//		contentDivNode.setAttribute("class", "ui-content"); 
//		pageDivNode.appendChild(contentDivNode);
//		
//		//Create first image element
//		Element mainImageElement = dom.createElement("img"); 
//		mainImageElement.setAttribute("src", currentImageURL);
//		mainImageElement.setAttribute("style", "display: block; margin-left: auto; margin-right: auto");   
//		contentDivNode.appendChild(mainImageElement);
//		
//		//Create Listview element if needed
//		if(nextStep != null || branches.size() > 0) {
//			
//			//Create Listview element
//			Element listViewElement = dom.createElement("ul"); 
//			listViewElement.setAttribute("data-role", "listview");
//			listViewElement.setAttribute("data-inset", "true");   
//			contentDivNode.appendChild(listViewElement);
//			
//			//Check next step, if present add li element
//			if(nextStep != null) { 
//				appendLiElement(listViewElement, nextStep.getDescription(), nextStep.getUniqueID(), "test.png"); 
//			} 
//			
//			//Append all scenario extensions for current step
//			for(Scenario s : branches) {
//				ScenarioExtension se = (ScenarioExtension) s;
//				appendLiElement(listViewElement, se.calculateID(usecase) + ". " + se.getName(), se.getUniqueID(), "test.png");  
//			} 
//		}	
//	}
//
//	private void appendLiElement(Element parent, String description, String UniqueID, String imageURL) {
//		//First child li element
//		Element liElement = dom.createElement("li");  
//		parent.appendChild(liElement); 
//		
//		//First child anchor element of li
//		Element anchorElement = dom.createElement("a"); 
//		anchorElement.setAttribute("href",  "#" + UniqueID);
//		anchorElement.setAttribute("data-transition", "slide");  
//		liElement.appendChild(anchorElement);
//		
//		//Anchor first child image element of li
//		Element imgElement = dom.createElement("img"); 
//		imgElement.setAttribute("src", imageURL);
//		imgElement.setAttribute("class", "ui-li-thumb");  
//		anchorElement.appendChild(imgElement);
//		
//		//Set description elements
//		Element hElement = dom.createElement("h2"); 
//		hElement.setTextContent("Next step"); 
//		anchorElement.appendChild(hElement);
//		
//		Element pElement = dom.createElement("p");
//		pElement.setTextContent(description);
//		anchorElement.appendChild(pElement);
//		
//		Element pActorElement = dom.createElement("p"); 
//		pActorElement.setTextContent("Actor");
//		pActorElement.setAttribute("class", "ui-li-aside");  
//		anchorElement.appendChild(pActorElement); 
//	}
//
//	/**
//	 * Reads an XML compliant file, and returns the corresponding Document object
//	 * @param xml
//	 * @return
//	 * @throws Exception
//	 */
//	public static Document loadXMLFromString(String xml) throws Exception
//	{
//	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//	    DocumentBuilder builder = factory.newDocumentBuilder();
//	    InputSource is = new InputSource(new StringReader(xml));
//	    return builder.parse(is);
//	}
//	
//	/**
//	 * Writes the DOM object into the given path.
//	 * Will throw exception if DOM has not been initialized yet.
//	 * @param path
//	 * @throws Exception
//	 */
//	public void writeToFile(String path) throws Exception
//	{
//		if(dom == null) throw new Exception("Document has not been created");
//		
//		// write the content into xml file
//		TransformerFactory transformerFactory = TransformerFactory.newInstance();
//		Transformer transformer = transformerFactory.newTransformer();
//		DOMSource source = new DOMSource(dom);
//		StreamResult result = new StreamResult(new File(path)); 
//		transformer.transform(source, result);
//	}
//}
