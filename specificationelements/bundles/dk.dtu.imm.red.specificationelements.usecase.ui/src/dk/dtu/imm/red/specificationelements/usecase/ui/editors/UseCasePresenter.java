package dk.dtu.imm.red.specificationelements.usecase.ui.editors;

 
import java.util.List;

import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecase.InputOutput;

public interface UseCasePresenter extends IEditorPresenter {
	public void setResult(String result);
	public void setOutcome(String outcome);
	public void setTrigger(String trigger); 
	public void setParameter(String value); 
	public void setLongDescription(String value);
	public void setKind(String kind);
	public void setPreCondition(String value); 
	public void setPostCondition(String value);
	public void setInputOutputList(EList<InputOutput> ioList);

	public void updatePrimaryActor(Actor a);
	public void updatePrimaryScenario(UseCaseScenario value);
	public void updateSecondaryScenario(UseCaseScenario value);
	public void updateSecondaryActor(Actor value);
	public EAGPresenter<Actor> getSecondaryActorPresenter();
	public EAGPresenter<UseCaseScenario> getPrimaryScenarioPresenter();
	public EAGPresenter<UseCaseScenario> getSecondaryScenarioPresenter();
	public EAGPresenter<Actor> getPrimaryActorPresenter();
	public EAGPresenter<String> getPreconditionPresenter();
	public EAGPresenter<String> getPostcondtionPresenter();
	
	//Use case associations
	ElementColumnDefinition[] getConditionColumnDefinition();
	ElementColumnDefinition[] getActorColumnDefinition();
	ElementColumnDefinition[] getScenarioColumnDefinition();
	List<InputOutput> getInputOutputList();
	void setInputOutputList(List<InputOutput> ioList);
 
}
