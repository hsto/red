package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.UseCaseScenarioComposite;


public interface UseCaseScenarioEditor extends IEditorPart, IBaseEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.usecasescenario.usecasescenarioeditor"; 
	void setIsModified();
	void selectedScenarioElementChanged(UseCaseScenarioElement data);
	void addAct();
	void addSend();
	void addSeq();
	void addAlt();
	void addOpt();
	void addPar();
	void addStr();
	void addBrk();
	void addLoop();
	void addReferenceSeq();	
	void removeSelectedScenarioElement();	
	void selectedTypeChanged(SelectionEvent e);
	void updateGuardText(ModifyEvent e);
	void updateScenarioComponentName();
	void updateScenarioComponentDescription();
	void selectScenarioReference();
	void EnableEnactment();
	UseCaseScenarioComposite getScenarioComposite();
	TextToSpeech getTextToSpeech();
	void toggleTextToSpeech();
	void toggleAutoPlay();
	void goNext();
	void goExplore();
	void setExcludedCompositeVisibility(boolean isVisible, Composite composite);
	void addReceive(); 
}
