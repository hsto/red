package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl;

import java.io.IOException; 
import java.rmi.UnknownHostException;

import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.TextToSpeech;
import javazoom.jl.decoder.JavaLayerException;

public class TextToSpeechManager implements TextToSpeech {

	private TextToSpeechGoogleImpl tx = new TextToSpeechGoogleImpl();
	private Runnable speakTextThread;
	private Thread speakerThread;

	/**
	 * Speaks text using Google Voice API. Requires internet connection.
	 * If an Error occur using Google Voice API, a library called OpenTTS will be used instead.
	 * This library do not require an internet connection, but the voice quality is much lower.
	 * This call is non blocking.
	 *
	 * @param The text that will be read aloud. 
	 * @throws JavaLayerException the java layer exception
	 * @throws UnknownHostException the unknown host exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void SpeakText(final String text) throws JavaLayerException,
			UnknownHostException, IOException {
		
		//Run in a Thread to avoid blocking UI while speaking
		speakTextThread = new Runnable() { 
			private TextToSpeechTTSImpl fallbackTx;

			@Override
			public void run() {  
				try { 
					tx.SpeakText(text);
				} catch (Exception e) {
					//Lazy initialize fallback TTS
					if(fallbackTx == null ) fallbackTx = new TextToSpeechTTSImpl();
					fallbackTx.SpeakText(text); 
				}   
			};  
		}; 
		
		speakerThread = new Thread(speakTextThread);
		speakerThread.start(); 
	} 
	
	@Override
	public boolean isSpeaking() {
		return speakerThread != null && speakerThread.isAlive();
	}
}
