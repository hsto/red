package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl; 
 
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout; 
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.jface.viewers.TreeViewer; 

import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;

import org.eclipse.wb.swt.SWTResourceManager;

public class UseCaseScenarioComposite extends Composite {
	private Composite cmpTop; 
	
	private SashForm sashForm;
	public SashForm getSashForm() {
		return sashForm;
	}

	private Group grpScenario;
	private UseCaseScenarioEditorImpl editorParent;
	private Composite imagePlaceholder;
	private Combo cmbType;
	private TreeViewer treeViewer; 
	private Text txtGuard;
	private Label lblGuard;
	private Text txtScenarioComponentName;  
	private Label lblScenarioComponentName; 
	private Label lblType;
	private Menu menu;
	private MenuItem mnReference; 
	private MenuItem btnDelete;
	private MenuItem btnRename;
	private Text txtScenarioComponentDescription;
	private Label lblScenarioComponentDescription;
	
	private MenuItem mntmNewSubmenu;
	
	
	private Label lblScenarioReference; 
	private Button btnSelectReference;
	private Composite compositeScenarioRef;
	private Group grpEnactment;
	private Button chkTextToSpeech;
	private Button btnAutoPlay;
	private Button btnEnactment;
	private Composite cmpEnactmentButtons;
	private Composite cmpNameDescription;
	private ImageLoader imageLoader;

	public Composite getCmpNameDescription() {
		return cmpNameDescription;
	}

	public Composite getCmpEnactmentButtons() {
		return cmpEnactmentButtons;
	}

	public Button getBtnEnactment() {
		return btnEnactment;
	}

	public Button getBtnAutoPlay() {
		return btnAutoPlay;
	}

	public Composite getCompositeScenarioRef() {
		return compositeScenarioRef;
	}

	public Button getBtnSelectReference() {
		return btnSelectReference;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public UseCaseScenarioComposite(Composite parent, int style) {
		super(parent, style); 
		createLayout();
	}
	
	public UseCaseScenarioComposite(Composite parent, int style, UseCaseScenarioEditorImpl scenarioEditor) {
		super(parent, style);
		this.editorParent = scenarioEditor;
		
		createLayout();
	}
	
	private void createLayout() {
		setLayout(new GridLayout(1, false));
		setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		
		//Load Images
		this.imageLoader = new ImageLoader();
		
		
		cmpTop = new Composite(this, SWT.NONE);
		GridData gd_cmpTop = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmpTop.widthHint = 302;
		cmpTop.setLayoutData(gd_cmpTop);
		cmpTop.setLayout(new GridLayout(2, false));			
		
		sashForm = new SashForm(this, SWT.NONE);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite composite_1 = new Composite(sashForm, SWT.NONE);
		composite_1.setLayout(new GridLayout(1, false));
		
		grpScenario = new Group(composite_1, SWT.NONE);
		grpScenario.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpScenario.setText("Scenario");
		grpScenario.setLayout(new GridLayout(1, false));
		
		treeViewer = new TreeViewer(grpScenario, SWT.MULTI | SWT.BORDER);
		Tree tvwScenario = treeViewer.getTree();		
		tvwScenario.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.selectedScenarioElementChanged((UseCaseScenarioElement) e.item.getData());
			}
		});
		tvwScenario.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		menu = new Menu(tvwScenario);
		tvwScenario.setMenu(menu);
		
		mntmNewSubmenu = new MenuItem(menu, SWT.CASCADE);
		mntmNewSubmenu.setText("New");
		
		Menu menu_1 = new Menu(mntmNewSubmenu);
		mntmNewSubmenu.setMenu(menu_1);
		
		MenuItem mnAction = new MenuItem(menu_1, SWT.NONE);
		mnAction.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addAct();
			}
		});
		mnAction.setText("Action");
		
		MenuItem mnSend = new MenuItem(menu_1, SWT.NONE);
		mnSend.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addSend();
			}
		});
		mnSend.setText("Send");
		mnSend.setSelection(true);
		
		MenuItem mnReceive = new MenuItem(menu_1, SWT.NONE);
		mnReceive.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addReceive();
			}
		});
		mnReceive.setText("Receive");
		
		MenuItem mnText = new MenuItem(menu_1, SWT.NONE);
		mnText.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addText();
			}
		});
		mnText.setText("Text");
		mnText.setSelection(true);
		
		MenuItem mnDirection = new MenuItem(menu_1, SWT.NONE);
		mnDirection.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addDirection();
			}
		});
		mnDirection.setText("Direction");
		mnDirection.setSelection(true);
		
		new MenuItem(menu_1, SWT.SEPARATOR);
		
		MenuItem mnSequence = new MenuItem(menu_1, SWT.NONE);
		mnSequence.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addSeq();
			}
		});
		mnSequence.setText("Sequence");
		
		MenuItem mnAlternate = new MenuItem(menu_1, SWT.NONE);
		mnAlternate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addAlt();
			}
		});
		mnAlternate.setText("Alternate");
		
		MenuItem mnOptional = new MenuItem(menu_1, SWT.NONE);
		mnOptional.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addOpt();
			}
		});
		mnOptional.setText("Optional");
		
		MenuItem mnParallel = new MenuItem(menu_1, SWT.NONE);
		mnParallel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addPar();
			}
		});
		mnParallel.setText("Parallel");
		
		MenuItem mnStrict = new MenuItem(menu_1, SWT.NONE);
		mnStrict.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addStr();
			}
		});
		mnStrict.setText("Strict");
		
		MenuItem mnBreak = new MenuItem(menu_1, SWT.NONE);
		mnBreak.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addBrk();
			}
		});
		mnBreak.setText("Break");
		
		MenuItem mnLoop = new MenuItem(menu_1, SWT.NONE);
		mnLoop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addLoop();
			}
		});
		mnLoop.setText("Loop");
		
		new MenuItem(menu_1, SWT.SEPARATOR);
		
		mnReference = new MenuItem(menu_1, SWT.CASCADE);
		mnReference.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.addReferenceSeq();
			}
		});
		mnReference.setText("Reference");
		
		new MenuItem(menu, SWT.SEPARATOR);
		
		btnDelete = new MenuItem(menu, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.removeSelectedScenarioElement();
			}
		});
		btnDelete.setText("Delete");
		
		new MenuItem(menu, SWT.SEPARATOR);
		
		btnRename = new MenuItem(menu, SWT.NONE);
		btnRename.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				txtScenarioComponentName.forceFocus();
				txtScenarioComponentName.selectAll();
			}
		});
		btnRename.setText("Rename");
		
		//Load Images
		mnSequence.setImage(imageLoader.getSequenceImage());
		mnAction.setImage(imageLoader.getActionImage());
		mnBreak.setImage(imageLoader.getBreakImage());
		mnLoop.setImage(imageLoader.getLoopImage());
		mnOptional.setImage(imageLoader.getOptionalImage());
		mnParallel.setImage(imageLoader.getParallelImage());
		mnStrict.setImage(imageLoader.getStrictImage());
		mnAlternate.setImage(imageLoader.getAlternateImage());
		
		mnReference.setImage(imageLoader.getReferenceImage());
		mnReceive.setImage(imageLoader.getReceiveImage());
		mnSend.setImage(imageLoader.getSendImage());
		mnText.setImage(imageLoader.getTextImage());
		mnDirection.setImage(imageLoader.getDirectionImage());
		
		grpEnactment = new Group(grpScenario, SWT.NONE);
		grpEnactment.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpEnactment.setText("Enactment");
		grpEnactment.setLayout(new GridLayout(2, false));
		
//		changes to disable enactment, after move it to its own enactment page
//		btnEnactment = new Button(grpEnactment, SWT.NONE);
//		btnEnactment.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				editorParent.EnableEnactment();
//			}
//		});
//		btnEnactment.setText("Enactment Perspective");
		
		chkTextToSpeech = new Button(grpEnactment, SWT.CHECK);
		chkTextToSpeech.setAlignment(SWT.CENTER);
		chkTextToSpeech.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.toggleTextToSpeech();
			}
		});
		chkTextToSpeech.setBounds(0, 0, 93, 16);
		chkTextToSpeech.setText("Text To Speech");
		
		btnAutoPlay = new Button(grpEnactment, SWT.NONE);
		btnAutoPlay.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnAutoPlay.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.toggleAutoPlay();
			}
		});
		btnAutoPlay.setText("Auto Play ▶");
		
		Button btnNext = new Button(grpEnactment, SWT.NONE);
		btnNext.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.goNext();
			}
		});
//		btnNext.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		GridData gd_btnNext = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_btnNext.heightHint = 25;
		btnNext.setLayoutData(gd_btnNext);
//		btnNext.setBounds(0, 0, 75, 25);
		btnNext.setText("Next");
		
		Button btnExplorer = new Button(grpEnactment, SWT.NONE);
		btnExplorer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.goExplore();
			}
		});
//		btnExplorer.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		GridData gd_btnExplorer = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_btnExplorer.heightHint = 25;
		btnExplorer.setLayoutData(gd_btnExplorer);
//		btnExplorer.setBounds(0, 0, 75, 25);
		btnExplorer.setText("Explore");		
		
		Composite composite = new Composite(sashForm, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		
		Group grpOptions = new Group(composite, SWT.NONE);
		grpOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpOptions.setText("Selection");
		grpOptions.setLayout(new GridLayout(1, true));
		
		//here
		cmpNameDescription = new Composite(grpOptions, SWT.NONE);
		cmpNameDescription.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		cmpNameDescription.setLayout(new GridLayout(2, false));

		lblType = new Label(cmpNameDescription, SWT.NONE);
		lblType.setText("Type");
		lblType.setVisible(false);
		
		lblGuard = new Label(cmpNameDescription, SWT.NONE);
		lblGuard.setToolTipText("Guard condition defines a condition that must be satisfied before child scenario nodes can be invoked");
		lblGuard.setText("Guard");
		lblGuard.setVisible(false);
		
		cmbType = new Combo(cmpNameDescription, SWT.READ_ONLY);
		cmbType.setToolTipText("An interaction operator");
		GridData gd_cmbType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbType.widthHint = 50;
		cmbType.setLayoutData(gd_cmbType);
		cmbType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.selectedTypeChanged(e);
			}
		});
		cmbType.setItems(new String[] {"Action", "Message"});
		cmbType.setVisible(false);
		
		txtGuard = new Text(cmpNameDescription, SWT.BORDER);
		GridData gd_txtGuard = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtGuard.widthHint = 100;
		txtGuard.setLayoutData(gd_txtGuard);
		txtGuard.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorParent.updateGuardText(e);
			}
		});
		txtGuard.setVisible(false);


		
		lblScenarioComponentName = new Label(cmpNameDescription, SWT.NONE);
		lblScenarioComponentName.setText("Name");
		lblScenarioComponentName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		 

		txtScenarioComponentName = new Text(cmpNameDescription,SWT.BORDER | SWT.WRAP);
		txtScenarioComponentName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		txtScenarioComponentName.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorParent.updateScenarioComponentName();
			}
		});
		
		//txtScenarioComponentName.setLayoutData(gd_txtScenarioComponentName);
	 
		
		lblScenarioComponentDescription = new Label(cmpNameDescription, SWT.NONE);
		lblScenarioComponentDescription.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblScenarioComponentDescription.setText("Description");
		 
		
		txtScenarioComponentDescription = new Text(cmpNameDescription, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtScenarioComponentDescription = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_txtScenarioComponentDescription.heightHint = 90;
		txtScenarioComponentDescription.setLayoutData(gd_txtScenarioComponentDescription);
		txtScenarioComponentDescription.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				editorParent.updateScenarioComponentDescription();
				
			}
		});

		
		cmpEnactmentButtons = new Composite(grpOptions, SWT.NONE);
		GridData gd_cmpEnactmentButtons = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_cmpEnactmentButtons.exclude = true;
		gd_cmpEnactmentButtons.minimumHeight = 60;
		gd_cmpEnactmentButtons.heightHint = 30;
		cmpEnactmentButtons.setLayoutData(gd_cmpEnactmentButtons);
		cmpEnactmentButtons.setLayout(new GridLayout(2, true));
	 
		
		Button btnGoNext = new Button(cmpEnactmentButtons, SWT.NONE);
		btnGoNext.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.goNext();
			}
		});
		btnGoNext.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		GridData gd_btnGoNext = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_btnGoNext.heightHint = 30;
		btnGoNext.setLayoutData(gd_btnGoNext);
		btnGoNext.setBounds(0, 0, 75, 25);
		btnGoNext.setText("Next");
		
		Button btnExplore = new Button(cmpEnactmentButtons, SWT.NONE);
		btnExplore.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.goExplore();
			}
		});
		btnExplore.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		GridData gd_btnExplore = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_btnExplore.heightHint = 30;
		gd_btnExplore.minimumHeight = 30;
		btnExplore.setLayoutData(gd_btnExplore);
		btnExplore.setBounds(0, 0, 75, 25);
		btnExplore.setText("Explore");
		
		
		compositeScenarioRef = new Composite(composite, SWT.NONE);
		GridData gd_compositeScenarioRef = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_compositeScenarioRef.minimumHeight = -1;
		compositeScenarioRef.setLayoutData(gd_compositeScenarioRef);
		compositeScenarioRef.setLayout(new GridLayout(2, true));
		compositeScenarioRef.setVisible(false);
		
		btnSelectReference = new Button(compositeScenarioRef, SWT.NONE);
		btnSelectReference.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1));
		btnSelectReference.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.selectScenarioReference();
			}
		});
		btnSelectReference.setBounds(0, 0, 75, 25);
		btnSelectReference.setText("Select Reference"); 
		lblScenarioReference = new Label(compositeScenarioRef, SWT.NONE);
		lblScenarioReference.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1));
		lblScenarioReference.setBounds(0, 0, 55, 15);
		lblScenarioReference.setText("None");
		
		imagePlaceholder = new Composite(composite, SWT.NONE);
		imagePlaceholder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		imagePlaceholder.setLayout(new GridLayout(1, false));
		sashForm.setWeights(new int[] {1, 1}); 
	} 
	

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}   

	public Composite getCmpTop() {
		return cmpTop;
	}

	public Composite getImagePlaceholder() {
		return imagePlaceholder;
	}

	public Combo getCmbType() {
		return cmbType;
	}
	
	public TreeViewer getTreeViewer() {
		return treeViewer;
	}
	

	public Text getTxtGuard() {
		return txtGuard;
	}

	public Label getLblGuard() {
		return lblGuard;
	}
	
	public Text getTxtDescription() {
		return txtScenarioComponentDescription;
	} 
	
	public Label getLblDescription() {
		return lblScenarioComponentDescription;
	}

	public void setLblDescription(Label lblDescription) {
		this.lblScenarioComponentName = lblDescription;
	}

	public Label getLblType() {
		return lblType;
	}

	public void setLblType(Label lblType) {
		this.lblType = lblType;
	}

	public void setLblGuard(Label lblGuard) {
		this.lblGuard = lblGuard;
	}
	
	public Text getTxtScenarioComponentName() {
		return txtScenarioComponentName;
	}
	
	public Label getLblScenarioComponentName() {
		return lblScenarioComponentName;
	}
	
	public MenuItem getMntmNew() {
		return mntmNewSubmenu;
	}

	public MenuItem getBtnDelete() {
		return btnDelete;
	}

	public MenuItem getBtnRename() {
		return btnRename;
	}
	
	public Label getLblScenarioReference() {
		return lblScenarioReference;
	}
}
