package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl; 
 
import dk.dtu.imm.red.core.ui.presenters.ElementHandler;
import dk.dtu.imm.red.specificationelements.usecasescenario.*;

public class UseCaseScenarioViewModel extends ElementHandler<UseCaseScenario> { 
	private UseCaseScenarioElement currentScenarioElement;	 

	private boolean enactmentModeEnabled = false;
	private boolean autoPlayingEnabled;
	private boolean textToSpeechEnabled;
	
	public boolean isResolvingAutoPlay() {
		return resolvingAutoPlay;
	}

	public void setResolvingAutoPlay(boolean resolvingAutoPlay) {
		this.resolvingAutoPlay = resolvingAutoPlay;
	}

	private boolean resolvingAutoPlay;

	public boolean isTextToSpeechEnabled() {
		return textToSpeechEnabled;
	}

	public void setTextToSpeechEnabled(boolean textToSpeechEnabled) {
		this.textToSpeechEnabled = textToSpeechEnabled;
	} 

	public void setCurrentScenarioElement(UseCaseScenarioElement se) {
		currentScenarioElement = se;
	}
	
	public UseCaseScenarioElement getCurrentScenarioElement() {
		return currentScenarioElement;
	}
	
	public OperatorComponent getSelectedConnector() {
		OperatorComponent con = (OperatorComponent) currentScenarioElement;
		return con;
	}
	
	public Action getSelectedAction() {
		Action act = (Action) currentScenarioElement;
		return act;
	}
	
	public boolean isOperator() {
		return currentScenarioElement instanceof OperatorComponent;
	}
	
	public boolean isAction() {
		return currentScenarioElement instanceof Action;
	}
	
	public boolean isEnactmentModeEnabled() {
		return enactmentModeEnabled;
	}

	public void setEnactmentModeEnabled(boolean enactmentModeEnabled) {
		this.enactmentModeEnabled = enactmentModeEnabled;
	}

	public boolean isAutoPlayingEnabled() {
		return autoPlayingEnabled;
	}

	public void setAutoPlayingEnabled(boolean autoPlayingEnabled) {
		this.autoPlayingEnabled = autoPlayingEnabled;
	}
}