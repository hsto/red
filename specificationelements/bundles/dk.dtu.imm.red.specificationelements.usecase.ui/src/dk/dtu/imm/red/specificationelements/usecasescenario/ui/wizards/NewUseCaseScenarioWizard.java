package dk.dtu.imm.red.specificationelements.usecasescenario.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewUseCaseScenarioWizard extends IBaseWizard {
	
	String ID = "dk.dtu.imm.red.specificationelements.usecasescenario.newwizard";

}
