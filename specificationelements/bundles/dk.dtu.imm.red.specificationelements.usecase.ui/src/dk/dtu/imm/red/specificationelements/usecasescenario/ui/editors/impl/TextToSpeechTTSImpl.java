package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl;


import java.util.Locale;

import javax.speech.Central;
import javax.speech.synthesis.Synthesizer;
import javax.speech.synthesis.SynthesizerModeDesc;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.TextToSpeech;

public class TextToSpeechTTSImpl implements TextToSpeech {

	private Synthesizer synthesizer;
	private boolean isSpeaking;

	public TextToSpeechTTSImpl() {
		try
		{
			System.setProperty("freetts.voices",
					"com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");

			Central.registerEngineCentral
			("com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");
			
			synthesizer = Central.createSynthesizer(new SynthesizerModeDesc(Locale.US));
			synthesizer.allocate();
		}
		catch(Exception e)
		{
			e.printStackTrace(); LogUtil.logError(e);
		}
	} 
		
	@Override
	public void SpeakText(String text) {
		try {
			synthesizer.resume();
			isSpeaking = true;
			synthesizer.speakPlainText(text, null);
			isSpeaking = false;
			synthesizer.waitEngineState(Synthesizer.QUEUE_EMPTY);  
		} catch (Exception e) {
			// TODO Auto-generated catch block 
		} 
	}

	@Override
	public boolean isSpeaking() {
		return isSpeaking;
	} 
}

