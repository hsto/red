package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;  

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.ActorFactory;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioFactory;
import dk.dtu.imm.red.specificationelements.usecase.InputOutput;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.UsecasePackage;
import dk.dtu.imm.red.specificationelements.usecase.ui.editors.UseCaseEditor;
import dk.dtu.imm.red.specificationelements.usecase.ui.editors.UseCasePresenter;

/**
 * @author Johan Paaske Nielsen
 *
 */
public class UseCasePresenterImpl extends BaseEditorPresenter  implements
		UseCasePresenter { 
	 
	protected UseCaseEditor usecaseEditor;
	private UseCaseViewModel viewModel;
	private List<InputOutput> ioList;
	
	public UseCaseViewModel getViewModel() {
		return viewModel;
	}

	public UseCasePresenterImpl(UseCaseEditor usecaseEditor, UseCaseViewModel viewModel) {
		super(viewModel.getElement());
		this.element = viewModel.getElement();
		this.viewModel = viewModel;
		this.usecaseEditor = usecaseEditor;
		ioList = viewModel.getElement().getInputOutputList();
	}

	@Override
	public void save() {
		super.save();
		viewModel.getElement().getInputOutputList().addAll(ioList);
		viewModel.getElement().save(); 
	}
	 
	@Override
	public List<InputOutput> getInputOutputList(){
		return ioList;
	}
	
	@Override
	public void setInputOutputList(List<InputOutput> ioList){
		this.ioList = ioList;
	}
	
	@Override
	public void setResult(String result) {
		viewModel.getElement().setResult(result);
	}

	@Override
	public void setOutcome(String outcome) {
		viewModel.getElement().setOutcome(outcome);
	}

	@Override
	public void setTrigger(String trigger) {
		viewModel.getElement().setTrigger(trigger); 
	}

	@Override
	public void setParameter(String value) {
		viewModel.getElement().setParameter(value); 
	}

	@Override
	public void setLongDescription(String value) {
		viewModel.getElement().setLongDescription(TextFactory.eINSTANCE.createText(value));
	} 
 
	@Override
	public void setPreCondition(String value) {
		viewModel.getElement().getPreConditions().set(viewModel.getSelectedPreConditionIndex(), value); 
	}  

	@Override
	public void setPostCondition(String value) {
		viewModel.getElement().getPostConditions().set(viewModel.getSelectedPostConditionIndex(), value); 
	}

	 @Override
	public void setKind(String value){
		viewModel.getElement().setElementKind(value);
	}
	 
	 @Override
	 public void setInputOutputList(EList<InputOutput> ioList){
		 viewModel.getElement().getInputOutputList().clear();
		 viewModel.getElement().getInputOutputList().addAll(ioList);
	 }
	 
	@Override
	public void updatePrimaryActor(Actor a) {
		int selectedIndex = viewModel.getSelectedPrimaryActorIndex();
		 viewModel.getElement().getPrimaryActors().set(selectedIndex, a);
	}

	@Override
	public void updatePrimaryScenario(UseCaseScenario value) {
		int selectedIndex = viewModel.getSelectedPrimaryScenarioIndex();
		viewModel.getElement().getPrimaryScenarios().set(selectedIndex, value);
		
	}

	@Override
	public void updateSecondaryScenario(UseCaseScenario value) {
		int selectedIndex = viewModel.getSelectedSecondaryScenarioIndex();
		 viewModel.getElement().getSecondaryScenarios().set(selectedIndex, value);
	}

	@Override
	public void updateSecondaryActor(Actor value) {
		int selectedIndex = viewModel.getSelectedPrimaryActorIndex();
		 viewModel.getElement().getSecondaryActors().set(selectedIndex, value);
	}
	
	@Override
	public EAGPresenter<Actor> getPrimaryActorPresenter() {
		final UseCase useCase = viewModel.getElement();
		EAGPresenter<Actor> crudPresenter = new EAGPresenter<Actor>() { 
			
			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(useCase);
				listener.setHostingFeature(UsecasePackage.eINSTANCE.getUseCase_PrimaryActors());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
				
				super.AddItem(o);
			}

			@Override
			public boolean doSetContentAt(int row, int col, Object value) {
				if(value instanceof Actor) { 
					updatePrimaryActor((Actor)value);
				} 
				return false;
			}
			
			@Override
			public void rowSelectionUpdated(boolean rowSelected,
					int selectedIndex) {
				viewModel.setSelectedPrimaryActorIndex(selectedIndex); 
			}

			@Override
			public Actor getNewItem() {
				return ActorFactory.eINSTANCE.createActor(); 
			}   
		};
		
		crudPresenter.setReflectedClass(Actor.class);
		crudPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.actor", "Actor");
		
		return crudPresenter;
	}

	@Override
	public EAGPresenter<Actor> getSecondaryActorPresenter() {
		final UseCase useCase = viewModel.getElement();
		EAGPresenter<Actor> crudPresenter = new EAGPresenter<Actor>() { 
			
			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(useCase);
				listener.setHostingFeature(UsecasePackage.eINSTANCE.getUseCase_SecondaryActors());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
				
				super.AddItem(o);
			}
			
			@Override
			public boolean doSetContentAt(int row, int col, Object value) {
				if(value instanceof Actor) { 
					updateSecondaryActor((Actor)value);
				} 
				return false;
			}
			
			@Override
			public void rowSelectionUpdated(boolean rowSelected,
					int selectedIndex) {
				viewModel.setSelectedSecondaryActorIndex(selectedIndex); 
			}

			@Override
			public Actor getNewItem() {
				return ActorFactory.eINSTANCE.createActor(); 
			}   
		};
		
		crudPresenter.setReflectedClass(Actor.class);
		crudPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.actor", "Actor");
		
		return crudPresenter;
	}

	@Override
	public EAGPresenter<UseCaseScenario> getPrimaryScenarioPresenter() {
		final UseCase useCase = viewModel.getElement();
		EAGPresenter<UseCaseScenario> crudPresenter =  new EAGPresenter<UseCaseScenario>() { 
			
			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(useCase);
				listener.setHostingFeature(UsecasePackage.eINSTANCE.getUseCase_PrimaryScenarios());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
				
				super.AddItem(o);
			}
			
			@Override
			public boolean doSetContentAt(int row, int col, Object value) {
				if(value instanceof UseCaseScenario) { 
					updatePrimaryScenario((UseCaseScenario)value);
				} 
				return false;
			}
			
			@Override
			public void rowSelectionUpdated(boolean rowSelected,
					int selectedIndex) {
				viewModel.setSelectedPrimaryScenarioIndex(selectedIndex); 
			}

			@Override
			public UseCaseScenario getNewItem() {
				return UsecasescenarioFactory.eINSTANCE.createUseCaseScenario(); 
			}   
		};
		
		crudPresenter.setReflectedClass(UseCaseScenario.class);
		crudPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.usecasescenario", "UseCaseScenario");
		
		return crudPresenter;
	}

	@Override
	public EAGPresenter<UseCaseScenario> getSecondaryScenarioPresenter() {
		final UseCase useCase = viewModel.getElement();
		EAGPresenter<UseCaseScenario> crudPresenter = new EAGPresenter<UseCaseScenario>() { 
			
			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(useCase);
				listener.setHostingFeature(UsecasePackage.eINSTANCE.getUseCase_SecondaryScenarios());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
				
				super.AddItem(o);
			}
			
			@Override
			public boolean doSetContentAt(int row, int col, Object value) {
				if(value instanceof UseCaseScenario) { 
					updateSecondaryScenario((UseCaseScenario)value);
				} 
				return false;
			}
			
			@Override
			public void rowSelectionUpdated(boolean rowSelected,
					int selectedIndex) {
				viewModel.setSelectedSecondaryScenarioIndex(selectedIndex); 
			}

			@Override
			public UseCaseScenario getNewItem() {
				return UsecasescenarioFactory.eINSTANCE.createUseCaseScenario(); 
			}   
		};
		
		crudPresenter.setReflectedClass(UseCaseScenario.class);
		crudPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.usecasescenario", "UseCaseScenario");
		return crudPresenter;
	}

	@Override
	public EAGPresenter<String> getPreconditionPresenter() {
		return createConditionPresenter(viewModel.getElement().getPreConditions()); 
	} 

	@Override
	public EAGPresenter<String> getPostcondtionPresenter() {
		return createConditionPresenter(viewModel.getElement().getPostConditions()); 
	}   
	
	private EAGPresenter<String> createConditionPresenter(List<String> dataSource) {
		EAGPresenter<String> crudPresenter = new EAGPresenter<String>() {  
			 @Override
			public String getNewItem() {
				// TODO Auto-generated method stub
				return "New Condition";
			}
		};
		crudPresenter.setDataSource(dataSource, false);
		crudPresenter.setReflectedClass(String.class);
		return crudPresenter;
	}

	/**
	 * Gets the condition column definition for Pre- and Post-Conditions.
	 *
	 * @return the condition column definition
	 */
	public ElementColumnDefinition[] getConditionColumnDefinition() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("Condition")
				.setColumnType(String.class)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true) };  
	}

	/**
	 * Gets the actor column definition.
	 *
	 * @return the Actor column definition
	 */
	public ElementColumnDefinition[] getActorColumnDefinition() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(6)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {"getIcon"}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(Actor.class)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setEditable(true)
				.setGetterMethod(new String[] {"getName"})};  
	}

	/**
	 * Gets the Scenario column definition.
	 *
	 * @return the Scenario column definition
	 */
	public ElementColumnDefinition[] getScenarioColumnDefinition() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(6)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {"getIcon"}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(UseCaseScenario.class)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setEditable(true)
				.setGetterMethod(new String[] {"getName"})};  
	}
	
	
}
