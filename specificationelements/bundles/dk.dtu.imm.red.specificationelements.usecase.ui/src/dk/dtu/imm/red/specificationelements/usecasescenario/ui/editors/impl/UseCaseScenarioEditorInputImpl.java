package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario; 

public class UseCaseScenarioEditorInputImpl extends BaseEditorInput {

	public UseCaseScenarioEditorInputImpl(UseCaseScenario scenario) {
		super(scenario);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Scenario editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for scenario " + element.getName();
	}
}
