package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;

public class UseCaseEditorInputImpl extends BaseEditorInput {

	public UseCaseEditorInputImpl(UseCase usecase) {
		super(usecase);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Usecase editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for use case " + element.getName();
	}
}
