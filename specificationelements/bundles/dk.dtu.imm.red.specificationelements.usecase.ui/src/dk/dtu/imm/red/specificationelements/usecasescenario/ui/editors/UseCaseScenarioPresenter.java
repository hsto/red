package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors;
import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.specificationelements.usecasescenario.*;

public interface UseCaseScenarioPresenter extends IEditorPresenter {

	void setImageDataForAction(Image image); 
	void setType(OperatorType valueOf);
	void setType(ActionType valueOf);
	void setGuard(String text);
	void setDescription(String text);
	Action addNewScenarioAction(ActionType act);
	Image getScenarioImage();
	Operator addNewScenarioOperator(OperatorType seq);
	void removeScenarioElement(UseCaseScenarioElement data);
	void removeCurrentScenarioElement();
	void moveScenarioElement(int calcPos, UseCaseScenarioElement selection,
			OperatorComponent parent);
	void setScenarioDescription(String string);
	void setScenarioElementName(String string);
	ScenarioReference addNewReferenceOperator(OperatorType con);
	void updateScenarioReference(); 
	 
}
