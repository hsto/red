/*
 * 
 */
package dk.dtu.imm.red.specificationelements.actor.ui.editors.impl;   

import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorEditor;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorPresenter;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.ActorPackage;
import dk.dtu.imm.red.specificationelements.actor.CodeType;
 

/**
 * @author Johan Paaske Nielsen
 *
 */ 
public class ActorPresenterImpl extends BaseEditorPresenter implements ActorPresenter { 
	 
	protected ActorEditor useCaseEditor;
	private ActorViewModel viewModel;
	private EAGPresenter<SpecificationElement> behaviourPresenter;
	
	public ActorViewModel getViewModel() {
		return viewModel;
	}

	public ActorPresenterImpl(ActorEditor visionEditor, ActorViewModel viewModel) {
		super(viewModel.getElement());
		this.element =  viewModel.getElement();
		this.viewModel = viewModel;
		this.useCaseEditor = visionEditor;  
		
		final Actor actor = viewModel.getElement();
		this.behaviourPresenter = new EAGPresenter<SpecificationElement>(){

			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(actor);
				listener.setHostingFeature(ActorPackage.eINSTANCE.getActor_BehaviorElements());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);

				super.AddItem(o);
			}
		};
		behaviourPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements", "SpecificationElement");// For Create,Delete
		behaviourPresenter.setReflectedClass(SpecificationElement.class); // For element selector
	} 

	@Override
	public void save() {
		super.save(); 
		viewModel.save();
	}
	
	@Override
	public void setCodeType(String codeType) {
		 viewModel.getElement().setCodeType(Enum.valueOf(CodeType.class, codeType.toUpperCase()));
	}
	
	@Override
	public void setCodeContent(String codeContent) {
		 viewModel.getElement().setCode(codeContent);
	}

	@Override
	public EAGPresenter<SpecificationElement> getBehaviourPresenter() {
		return behaviourPresenter;
	}
	
	@Override
	public ElementColumnDefinition[] getBehaviourColumnDefinition() {
		return new ElementColumnDefinition[] { 
				//Icon column
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(16)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {"getIcon"}),
				
				//Name column
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setColumnWidth(50)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getName"}),
				
				//Kind column
				new ElementColumnDefinition()
				.setHeaderName("Kind")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getElementKind"})
				 };
	}
}
