package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.tree;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import dk.dtu.imm.red.specificationelements.usecasescenario.Operator;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;

public class TreeContentProvider implements ITreeContentProvider {

	@Override
	public Object[] getChildren(Object parentElement) { 
		OperatorComponent operator = (OperatorComponent) parentElement;
		return operator.getChildElements().toArray(); 
	}

	@Override
	public Object getParent(Object element) {
		UseCaseScenarioElement se = (UseCaseScenarioElement) element;
		return se.getParent();
	}

	@Override
	public boolean hasChildren(Object element) {
		if(element instanceof OperatorComponent) {
			OperatorComponent operator = (OperatorComponent) element;
			return operator.getChildElements().size() > 0; 
		}
		else {
			return false;
		}	
	}

	@Override
	public Object[] getElements(Object inputElement) {
		Operator operator = (Operator) inputElement;
		Object[] objects = operator.getChildElements().toArray();
		return objects;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {

	}

} 