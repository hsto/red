/*
 * @author Johan Paaske Nielsen
 *
 * Editor for the Specification Element Use case
 */

package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.UseCaseKind;
import dk.dtu.imm.red.specificationelements.usecase.UsecasePackage;
import dk.dtu.imm.red.specificationelements.usecase.ui.editors.UseCaseEditor;
import dk.dtu.imm.red.specificationelements.usecase.ui.editors.UseCasePresenter;

/**
 * The Class UsecaseEditorImpl.
 */
public class UseCaseEditorImpl extends BaseEditor implements UseCaseEditor {

	protected ElementBasicInfoContainer basicInfoContainer;

	private RichTextEditor rteDescription;

	private int useCaseAttrPageIndex;
	private int scenarioPageIndex;
	private UseCasePresenter useCasePresenter;
	private UseCaseViewModel viewModel;
	private UseCaseComposite attrComposite;
	private BehaviorComposite behaviorComposite;
	private EAGComposite<Actor> crudPrimaryActor;
	private EAGComposite<Actor> crudSecondaryActor;
	private EAGComposite<String> crudPreConditions;
	private EAGComposite<String> crudPostConditions;
	private EAGComposite<UseCaseScenario> crudPrimaryScenario;
	private EAGComposite<UseCaseScenario> crudSecondaryScenario;

	private InputOutputComposite ioComposite;

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);
		viewModel = new UseCaseViewModel();
		viewModel.setElement((UseCase) element);
		presenter = new UseCasePresenterImpl(this, viewModel);
		useCasePresenter = (UseCasePresenter) presenter; // casted presenter
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#createPages()
	 */
	@Override
	protected void createPages() {

		//Create attributes page
		useCaseAttrPageIndex = addPage(createUseCaseAttrPage(getContainer()));
		setPageText(useCaseAttrPageIndex, "Summary");

		//Create scenario page
		scenarioPageIndex = addPage(createBehaviorPage(getContainer()));
		setPageText(scenarioPageIndex, "Behavior");

		super.createPages();
		fillInitialData();
	}

	@Override
	public dk.dtu.imm.red.core.element.Element getEditorElement() {
		return viewModel.getElement();
	}

	/**
	 * Creates the use case attributes page.
	 *
	 * @param parent the parent
	 * @return the composite
	 */
	private Composite createUseCaseAttrPage(Composite parent) {
		attrComposite = new UseCaseComposite(parent, SWT.NONE, this);

		//Add Pre- and Post-Conditions
		EAGPresenter<String> preconditionPresenter = useCasePresenter.getPreconditionPresenter();
		EAGPresenter<String> postcondtionPresenter = useCasePresenter.getPostcondtionPresenter();

		crudPreConditions = new EAGComposite<String>(attrComposite.getCmpPreConditionsPlaceholder(), SWT.NONE, modifyListener)
				.setModifyListener(modifyListener)
				.setPresenter(preconditionPresenter);
		crudPreConditions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		crudPostConditions = new EAGComposite<String>(attrComposite.getCmpPostCondtionsPlaceholder(), SWT.NONE, modifyListener)
				.setModifyListener(modifyListener)
				.setPresenter(postcondtionPresenter);
		crudPostConditions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		return SWTUtils.wrapInScrolledComposite(parent, attrComposite);
	}

	/**
	 * Creates the references page.
	 *
	 * @param parent the parent
	 * @return the composite
	 */
	private Composite createBehaviorPage(Composite parent) {
		behaviorComposite = new BehaviorComposite(parent, SWT.NONE, this);

		//Create Presenter for reference selectors
		EAGPresenter<UseCaseScenario> primaryScenarioPresenter = useCasePresenter.getPrimaryScenarioPresenter();
		EAGPresenter<UseCaseScenario> secondaryScenarioPresenter = useCasePresenter.getSecondaryScenarioPresenter();
		EAGPresenter<Actor> primaryActorPresenter = useCasePresenter.getPrimaryActorPresenter();
		EAGPresenter<Actor> secondaryActorPresenter = useCasePresenter.getSecondaryActorPresenter();

		//Create Reference selector Composites

		rteDescription = createRTE(behaviorComposite.getGrpLongDescription());

		//Primary Scenario
		crudPrimaryScenario = new EAGComposite<UseCaseScenario>(behaviorComposite.getGrpPrimaryScenario(), SWT.NONE, modifyListener,
				viewModel.getElement(), UsecasePackage.eINSTANCE.getUseCase_PrimaryScenarios())
				.setModifyListener(modifyListener)
				.setElementSelectorOnAdd(true)
				.setPresenter(primaryScenarioPresenter);

		//Secondary Scenario
		crudSecondaryScenario = new EAGComposite<UseCaseScenario>(behaviorComposite.getGrpAlternateScenarios(), SWT.NONE, modifyListener,
				viewModel.getElement(), UsecasePackage.eINSTANCE.getUseCase_SecondaryScenarios())
				.setModifyListener(modifyListener)
				.setElementSelectorOnAdd(true)
				.setPresenter(secondaryScenarioPresenter);


		//Primary Actor
		crudPrimaryActor = new EAGComposite<Actor>(behaviorComposite.getGrpPrimaryActor(), SWT.NONE, modifyListener,
				viewModel.getElement(), UsecasePackage.eINSTANCE.getUseCase_PrimaryActors())
				.setModifyListener(modifyListener)
				.setElementSelectorOnAdd(true)
				.setPresenter(primaryActorPresenter);

		//Secondary Actor
		crudSecondaryActor = new EAGComposite<Actor>(behaviorComposite.getGrpSecondaryActors(), SWT.NONE, modifyListener,
				viewModel.getElement(), UsecasePackage.eINSTANCE.getUseCase_SecondaryActors())
				.setModifyListener(modifyListener)
				.setElementSelectorOnAdd(true)
				.setPresenter(secondaryActorPresenter);

		ioComposite = new InputOutputComposite(behaviorComposite.getTableComposite(), SWT.NONE, this, viewModel.getElement());
		ioComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,1,1));

		return SWTUtils.wrapInScrolledComposite(parent, behaviorComposite);
	}

	/**
	 * Creates the RTE inside the container, and add an modify listener to it .
	 *
	 * @param container the container
	 * @return the rich text editor
	 */
	private RichTextEditor createRTE(Composite container) {

		RichTextEditor rteEditor = new RichTextEditor(container,
				SWT.BORDER | SWT.WRAP | SWT.V_SCROLL,
				getEditorSite(), commandListener);

		rteEditor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		//set event handlers
		rteEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		return rteEditor;
	}

	/**
	 * Helper for set is modified.
	 */
	public void setIsModified() {
		modifyListener.handleEvent(null);
	}


	public void setUsecaseKind(String value){
		useCasePresenter.setKind(value);
		modifyListener.handleEvent(null);
	}

	//Infrastructure actions
	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#selectText(org.eclipse.emf.ecore.EStructuralFeature, int, int)
	 */
	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(useCaseAttrPageIndex);
		} else if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(useCaseAttrPageIndex);
		}
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#search(dk.dtu.imm.red.core.element.SearchParameter)
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		UseCase u = viewModel.getElement();

		// put all the attributes which we want to search through
		// into the attributesToSearchMap
		attributesToSearch.put(UsecasePackage.Literals.USE_CASE__TYPE,
				u.getType().toString());

		attributesToSearch.put(UsecasePackage.Literals.USE_CASE__TRIGGER,
				u.getTrigger());

		attributesToSearch.put(UsecasePackage.Literals.USE_CASE__RESULT,
				u.getResult());

		attributesToSearch.put(UsecasePackage.Literals.USE_CASE__PARAMETER,
				u.getParameter());

		return null;
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
		return;
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {

		UseCasePresenter presenter = (UseCasePresenter) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		presenter.setOutcome(attrComposite.getTxtOutcome().getText());
		presenter.setResult(attrComposite.getTxtResult().getText());
		presenter.setTrigger(attrComposite.getTxtTrigger().getText());
		presenter.setParameter(attrComposite.getTxtParameter().getText());
		presenter.setLongDescription(rteDescription.getText());

		super.doSave(monitor);
	}

	//Fill UI widgets with data
	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#fillInitialData()
	 */
	@Override
	protected void fillInitialData() {

		UseCaseKind[] kinds = UseCaseKind.values();
		String[] kindStrings = new String[kinds.length];
		for(int i=0;i<kinds.length;i++){
			kindStrings[i] = kinds[i].getName();
		}

		basicInfoContainer.fillInitialData(viewModel.getElement(), kindStrings);

		//Load simple widgets
		fillInitialRTE(viewModel.getElement().getLongDescription(), rteDescription);
		fillTextbox(viewModel.getElement().getOutcome(), attrComposite.getTxtOutcome());
		fillTextbox(viewModel.getElement().getResult(), attrComposite.getTxtResult());
		fillTextbox(viewModel.getElement().getTrigger(), attrComposite.getTxtTrigger());
		fillTextbox(viewModel.getElement().getParameter(), attrComposite.getTxtParameter());

		if (!viewModel.getElement().getInputOutputList().isEmpty()) {
			((UseCasePresenter) presenter).getInputOutputList().addAll(viewModel.getElement().getInputOutputList());

		}

		//Load properties
		attrComposite.getDuration()
		.setModifyListener(modifyListener)
		.fillData(viewModel.getElement().getDuration());

		attrComposite.getIncidence()
		.setModifyListener(modifyListener)
		.fillData(viewModel.getElement().getIncidence());

		//Load pre conditions
		crudPreConditions.setColumns(useCasePresenter.getConditionColumnDefinition())
		.setDataSource(viewModel.getElement().getPreConditions());

		//Load post conditions
		crudPostConditions.setColumns(useCasePresenter.getConditionColumnDefinition())
		.setDataSource(viewModel.getElement().getPostConditions());

		//Load primary actors
		crudPrimaryActor.setColumns(useCasePresenter.getActorColumnDefinition())
		.setDataSource(viewModel.getElement().getPrimaryActors());


		//Load secondary actors
		crudSecondaryActor.setColumns(useCasePresenter.getActorColumnDefinition())
		.setDataSource(viewModel.getElement().getSecondaryActors());

		//Load primary scenarios
		crudPrimaryScenario.setColumns(useCasePresenter.getScenarioColumnDefinition())
		.setDataSource(viewModel.getElement().getPrimaryScenarios());


		//Load secondary scenarios
		crudSecondaryScenario.setColumns(useCasePresenter.getScenarioColumnDefinition())
		.setDataSource(viewModel.getElement().getSecondaryScenarios());

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}



	/**
	 * Helper for Fill textbox.
	 *
	 * @param value the value
	 * @param textbox the textbox
	 */
	private void fillTextbox(String value, Text textbox) {
		textbox.setText(value != null ? value : "");
	}

	/**
	 * Helper for Fill initial rte.
	 *
	 * @param value the value
	 * @param rte the rte
	 */
	private void fillInitialRTE(dk.dtu.imm.red.core.text.Text value, RichTextEditor rte) {
		rte.setText(value != null ? value.toString() : "");
	}

	//Properties
	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.BaseEditor#getEditorID()
	 */
	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.usecase.usecaseeditor";
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/* (non-Javadoc)
	 * @see dk.dtu.imm.red.core.ui.editors.IBaseEditor#getHelpResourceURL()
	 */
	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Usecase.html";
	}


	public void setBasicInfoContainer(ElementBasicInfoContainer basicInfoContainer){
		this.basicInfoContainer = basicInfoContainer;
	}
}
