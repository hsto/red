package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;  
import dk.dtu.imm.red.core.ui.presenters.ElementHandler;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;


public class UseCaseViewModel extends ElementHandler<UseCase> {
	
	private int selectedPreConditionIndex;
	private int selectedPostConditionIndex;
	private int selectedPrimaryActorIndex;

	private int selectedPrimaryScenarioIndex;
	private int selectedSecondaryScenarioIndex;
	private int selectedSecondaryActorIndex;
	
	public int getSelectedPrimaryActorIndex() {
		return selectedPrimaryActorIndex;
	}
	 
	public int getSelectedPreConditionIndex() {
		return selectedPreConditionIndex;
	}
	public void setSelectedPreConditionIndex(Integer selectedPreConditionIndex) {
		this.selectedPreConditionIndex = selectedPreConditionIndex;
	}
	public int getSelectedPostConditionIndex() {
		return selectedPostConditionIndex;
	}
	public void setSelectedPostConditionIndex(Integer selectedPostConditionIndex) {
		this.selectedPostConditionIndex = selectedPostConditionIndex;
	} 
	  
	public void setSelectedPrimaryActorIndex(int selectedIndex) {
		 this.selectedPrimaryActorIndex = selectedIndex;
	}
	public void setSelectedPrimaryScenarioIndex(int selectedIndex) {
		 this.selectedPrimaryScenarioIndex = selectedIndex;
		
	}
	public void setSelectedSecondaryScenarioIndex(int selectedIndex) {
		 this.selectedSecondaryScenarioIndex = selectedIndex;
		
	}
	public void setSelectedSecondaryActorIndex(int selectedIndex) {
		 this.selectedSecondaryActorIndex = selectedIndex; 
	}
	
	public int getSelectedPrimaryScenarioIndex() {
		return selectedPrimaryScenarioIndex;
	}
	public int getSelectedSecondaryScenarioIndex() {
		return selectedSecondaryScenarioIndex;
	}
	public int getSelectedSecondaryActorIndex() {
		return selectedSecondaryActorIndex;
	}
}
