/*
 * 
 */
package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl;  
import java.io.IOException;
import java.io.InputStream; 
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javazoom.jl.decoder.JavaLayerException;

import com.gtranslate.Audio;
import com.gtranslate.Language;

import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.TextToSpeech;

public class TextToSpeechGoogleImpl implements TextToSpeech {  
	 
	private boolean isSpeaking;

	@Override
	public void SpeakText(final String text) throws JavaLayerException, IOException {  
	
		Audio audio = Audio.getInstance();
		
		//Google TTS has a limit of 100 characters of each web request.
		//To counter this constraint, split the text into bits of 99 characters, and play them sequentially 
		String newText = text.replaceAll("[\\r\\n]+\\s", "."); //Newlines causes Exception, and are removed
		List<String> max100CharsWordList =  getMax100CharsWordList(newText);
		
		//String[] splittedText = newText.split("(?<=\\G.{" + 99 + "})");
		
		isSpeaking = true;
		
		List<InputStream> sounds = new ArrayList<InputStream>();
		for(String s : max100CharsWordList) {
			sounds.add(audio.getAudio(s, Language.ENGLISH)); 
		}
		
		Enumeration<InputStream> e =  Collections.enumeration(sounds);
		InputStream s = new java.io.SequenceInputStream(e);
		audio.play(s);  
		s.close();
		 
		isSpeaking = false; 
	}
	
	
	/**
	 * Split the input text into a list of complete words/string, that combined has a maximum of 100 characters. 
	 * @param newText the new text
	 * @return the max100 chars word list
	 */
	private List<String> getMax100CharsWordList(String newText) {
		String[] splittedText = newText.split(" ");
		int i = 0;
		List<String> max100CharWords = new ArrayList<String>();
		max100CharWords.add("");
		
		for(String s : splittedText) {
			String cur = max100CharWords.get(i);
			if(cur.length() + s.length() + 1 > 99) {
				cur = "";
				max100CharWords.add(s);
				i++;
			} 
			else {
				max100CharWords.set(i, cur + " " + s); 
			} 
		}
		
		return max100CharWords;
	}

	@Override
	public boolean isSpeaking() {
		return isSpeaking;
	}
	
	
}

