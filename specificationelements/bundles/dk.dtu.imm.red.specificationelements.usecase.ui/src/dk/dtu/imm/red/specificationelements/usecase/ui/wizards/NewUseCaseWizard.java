package dk.dtu.imm.red.specificationelements.usecase.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewUseCaseWizard extends IBaseWizard {
	
	String ID = "dk.dtu.imm.red.specificationelements.usecase.newwizard";

}
