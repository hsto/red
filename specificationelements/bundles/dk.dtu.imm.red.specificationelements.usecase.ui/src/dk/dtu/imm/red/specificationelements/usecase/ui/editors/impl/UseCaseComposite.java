/*
 * @author Johan Paaske Nielsen
 * 
 * This Composite Control is built with Window Builder and can be parsed by this tool - 
 * https://www.eclipse.org/windowbuilder/ 
 * 
 * Contains the GUI for the main Usecase tab
 */

package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl; 
 
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout; 
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Group; 
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;

import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.widgets.PropertySelection;
import dk.dtu.imm.red.specificationelements.SpecificationElement;

import org.eclipse.swt.widgets.Combo; 
 
/**
 * The Class AttributeComposite.
 */
public class UseCaseComposite extends Composite {
	
	/** The txt id. */
//	private Text txtID;
	
	/** The txt name. */
//	private Text txtName;
	
	/** The txt trigger. */
	private Text txtTrigger;
	
	/** The txt parameter. */
	private Text txtParameter;
	
	/** The txt result. */
	private Text txtResult;
	
	/** The usecase editor. */
	private UseCaseEditorImpl usecaseEditor = null;
	
	/** The cmp post condtions placeholder. */
	private Composite cmpPostCondtionsPlaceholder;
	
	/** The cmp pre conditions placeholder. */
	private Composite cmpPreConditionsPlaceholder;
	
	/** The grp description. */
	private Group grpDescription;
	
	/** The cmp top. */
	private Composite cmpTop; 
	
	/** The duration. */
	private PropertySelection duration;
	
	/** The incidence. */
	private PropertySelection incidence;
	private Combo cmbUseCaseType;
	private Composite cmpBottom;
	private Label lblOutcome;
	private Text txtOutcome;

	public Combo getCmbUseCaseType() {
		return cmbUseCaseType;
	}

	/**
	 * Create the composite.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public UseCaseComposite(Composite parent, int style) {
		super(parent, style);
		createLayout();
	}
	
	/**
	 * Instantiates a new attribute composite.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param usecaseEditor the usecase editor
	 */
	public UseCaseComposite(Composite parent, int style, UseCaseEditorImpl usecaseEditor) {
		super(parent, style);
		this.usecaseEditor = usecaseEditor;
		createLayout();
		
	}
	
	/**
	 * Creates the layout.
	 */
	private void createLayout() {
		setLayout(new GridLayout(1, false));
		setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		
		cmpTop = new Composite(this, SWT.NONE);
		cmpTop.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		cmpTop.setLayout(new GridLayout(2, false));
		
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;
		
		ElementBasicInfoContainer basicInfoContainer = new ElementBasicInfoContainer(cmpTop, SWT.NONE, usecaseEditor, (SpecificationElement) usecaseEditor.getEditorElement());
		usecaseEditor.setBasicInfoContainer(basicInfoContainer);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);
		new Label(cmpTop, SWT.NONE);

		SashForm sashForm = new SashForm(this, SWT.NONE);
		GridData gd_sashForm = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_sashForm.heightHint = 338;
		sashForm.setLayoutData(gd_sashForm);
		
		Group grpBefore = new Group(sashForm, SWT.NONE);
		grpBefore.setText("Before");
		grpBefore.setLayout(new GridLayout(1, false));
		grpBefore.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		cmpPreConditionsPlaceholder = new Composite(grpBefore, SWT.NONE);
		cmpPreConditionsPlaceholder.setToolTipText("System states required to trigger this use case");
		GridData gd_cmpPreConditionsPlaceholder = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_cmpPreConditionsPlaceholder.heightHint = 241;
		cmpPreConditionsPlaceholder.setLayoutData(gd_cmpPreConditionsPlaceholder);
		cmpPreConditionsPlaceholder.setLayout(new GridLayout(1, false));	
		gd_cmpPreConditionsPlaceholder.minimumHeight = 120;
		//cmpPreConditionsPlaceholder.setLayoutData(gd_cmpPreConditionsPlaceholder);
	
		
		Label lblTrigger = new Label(grpBefore, SWT.NONE);
		lblTrigger.setToolTipText("An event or command");
		lblTrigger.setText("Trigger");
		
		txtTrigger = new Text(grpBefore, SWT.BORDER);
		txtTrigger.setToolTipText("");
		txtTrigger.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				usecaseEditor.setIsModified();
			}
		});
		txtTrigger.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblParameter = new Label(grpBefore, SWT.NONE);
		lblParameter.setToolTipText("Data needed to trigger this use case");
		lblParameter.setText("Parameter");
		
		txtParameter = new Text(grpBefore, SWT.BORDER);
		txtParameter.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				usecaseEditor.setIsModified();
			}
		});
		txtParameter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Group grpAfter = new Group(sashForm, SWT.NONE);
		grpAfter.setText("After");
		grpAfter.setLayout(new GridLayout(1, false));
		grpAfter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		cmpPostCondtionsPlaceholder = new Composite(grpAfter, SWT.NONE);
		GridData gd_cmpPostCondtionsPlaceholder = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_cmpPostCondtionsPlaceholder.heightHint = 111;
		gd_cmpPostCondtionsPlaceholder.minimumHeight = 120;
		cmpPostCondtionsPlaceholder.setLayoutData(gd_cmpPostCondtionsPlaceholder);
		cmpPostCondtionsPlaceholder.setLayout(new GridLayout(1, false));
		
		lblOutcome = new Label(grpAfter, SWT.NONE);
		lblOutcome.setText("Outcome");
		
		txtOutcome = new Text(grpAfter, SWT.BORDER);
		txtOutcome.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtOutcome.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				usecaseEditor.setIsModified();
			}
		});
		
		Label lblResult = new Label(grpAfter, SWT.NONE);
		lblResult.setToolTipText("Data provided or computed by this use case");
		lblResult.setText("Result");
		
		txtResult = new Text(grpAfter, SWT.BORDER);
		txtResult.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				usecaseEditor.setIsModified();
			}
		});
		txtResult.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		sashForm.setWeights(new int[] {1, 1});
		
		cmpBottom = new Composite(this, SWT.NONE);
		cmpBottom.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		cmpBottom.setLayout(new GridLayout(1, true));
		
		Composite grpMeasurements = new Composite(cmpBottom, SWT.NONE);
		grpMeasurements.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpMeasurements.setSize(621, 98); 
		grpMeasurements.setLayout(new GridLayout(2, true));
		
		Group gpIncidence = new Group(grpMeasurements, SWT.NONE);
		GridData gd_gpIncidence = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_gpIncidence.widthHint = 209;
		gpIncidence.setLayoutData(gd_gpIncidence);
		gpIncidence.setText("Incidence");
		gpIncidence.setLayout(new GridLayout(1, false));
		
		incidence = new PropertySelection(gpIncidence, SWT.NONE);
		((GridData) incidence.getCmbUnit().getLayoutData()).horizontalAlignment = SWT.LEFT;
		GridData gd_incidence = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_incidence.widthHint = 207;
		incidence.setLayoutData(gd_incidence);
		GridData gridData_4 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gridData_4.exclude = true;
		incidence.getLblUnitDescription().setLayoutData(gridData_4);
		GridData gridData = (GridData) incidence.getCombo().getLayoutData();		
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = false;
		gridData.horizontalSpan = 2;
		incidence.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));		
	
		((GridData) incidence.getCmbKind().getLayoutData()).exclude = true;
		GridLayout gl_incidence = (GridLayout) incidence.getLayout();
		gl_incidence.makeColumnsEqualWidth = true;
		
		Group gpDuration= new Group(grpMeasurements, SWT.NONE);
		gpDuration.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		gpDuration.setText("Duration");
		gpDuration.setLayout(new GridLayout(1, false));
		
		duration = new PropertySelection(gpDuration, SWT.NONE);
		GridData gridData_1 = (GridData) duration.getCmbUnit().getLayoutData();
		gridData_1.widthHint = -1;
		gridData_1.grabExcessHorizontalSpace = true;
		gridData_1.horizontalAlignment = SWT.FILL;
		gridData_1.grabExcessVerticalSpace = false;
		gridData_1.horizontalSpan = 2;
		((GridData) duration.getCmbKind().getLayoutData()).exclude = true;
		GridData gd_duration = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_duration.widthHint = 150;
		duration.setLayoutData(gd_duration);
	 
	}
	
	

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Composite#checkSubclass()
	 */
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	} 

	/**
	 * Gets the txt id.
	 *
	 * @return the txt id
	 */
//	public Text getTxtID() {
//		return txtID;
//	}

	/**
	 * Gets the txt name.
	 *
	 * @return the txt name
	 */
//	public Text getTxtName() {
//		return txtName;
//	}

	/**
	 * Gets the txt trigger.
	 *
	 * @return the txt trigger
	 */
	public Text getTxtTrigger() {
		return txtTrigger;
	}

	/**
	 * Gets the txt parameter.
	 *
	 * @return the txt parameter
	 */
	public Text getTxtParameter() {
		return txtParameter;
	}

	/**
	 * Gets the txt result.
	 *
	 * @return the txt result
	 */
	public Text getTxtResult() {
		return txtResult;
	}
	
	/**
	 * Gets the txt outcome.
	 *
	 * @return the txt outcome
	 */	
	public Text getTxtOutcome(){
		return txtOutcome;
	}

	/**
	 * Gets the usecase editor.
	 *
	 * @return the usecase editor
	 */
	public UseCaseEditorImpl getUsecaseEditor() {
		return usecaseEditor;
	}

	/**
	 * Gets the cmp post condtions placeholder.
	 *
	 * @return the cmp post condtions placeholder
	 */
	public Composite getCmpPostCondtionsPlaceholder() {
		return cmpPostCondtionsPlaceholder;
	}

	/**
	 * Gets the cmp pre conditions placeholder.
	 *
	 * @return the cmp pre conditions placeholder
	 */
	public Composite getCmpPreConditionsPlaceholder() {
		return cmpPreConditionsPlaceholder;
	}

	/**
	 * Gets the grp description.
	 *
	 * @return the grp description
	 */
	public Group getGrpDescription() {
		return grpDescription;
	}

	/**
	 * Gets the cmp top.
	 *
	 * @return the cmp top
	 */
	public Composite getCmpTop() {
		return cmpTop;
	}  

	/**
	 * Gets the duration.
	 *
	 * @return the duration
	 */
	public PropertySelection getDuration() {
		return duration;
	}

	/**
	 * Gets the incidence.
	 *
	 * @return the incidence
	 */
	public PropertySelection getIncidence() {
		return incidence;
	}
}
