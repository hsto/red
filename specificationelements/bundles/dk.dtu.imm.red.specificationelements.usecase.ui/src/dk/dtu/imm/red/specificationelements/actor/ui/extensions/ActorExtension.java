package dk.dtu.imm.red.specificationelements.actor.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorEditor;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.impl.ActorEditorInputImpl;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.impl.ActorImpl;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;

public class ActorExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element) {
		if (element instanceof Actor && element.getClass().isAssignableFrom(ActorImpl.class)) {
			ActorEditorInputImpl editorInput = new ActorEditorInputImpl(
					(Actor) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, ActorEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}
}
