package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.tree;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.ImageLoader;

public class TreeLabelProvider extends LabelProvider { 
	
  private ImageLoader imageLoader;

  public TreeLabelProvider() {
	  imageLoader = new ImageLoader(); 
  }  
	
  @Override
  public String getText(Object element) {
	  return element.toString();
  } 
 
  @Override
	public Image getImage(Object element) { 
	  	return imageLoader.getByInstance(element); 
	}
} 