package dk.dtu.imm.red.specificationelements.usecase.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.UsecaseFactory;
import dk.dtu.imm.red.specificationelements.usecase.ui.extensions.UseCaseExtension;

public class CreateUseCaseOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected UseCaseExtension extension;
	protected Group parent;
//	protected String path;
	
	protected UseCase useCase;
	
	public CreateUseCaseOperation(String label, String name, String description, Group parent, String path) {
		super("Create Usecase");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
//		this.path = path;

		extension = new UseCaseExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		
		useCase = UsecaseFactory.eINSTANCE.createUseCase();
		
		useCase.setLabel(label);
		useCase.setName(name);
		useCase.setDescription(description);
		
		if (parent != null) {
			useCase.setParent(parent);
		} 
		

		useCase.save();
		extension.openElement(useCase);

		return Status.OK_STATUS;
	}
	
	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(useCase);
			extension.deleteElement(useCase);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}
	
	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
