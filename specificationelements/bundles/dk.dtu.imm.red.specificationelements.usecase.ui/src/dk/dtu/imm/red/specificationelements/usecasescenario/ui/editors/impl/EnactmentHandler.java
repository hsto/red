/*
 * @author Johan Paaske Nielsen
 */ 
package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl;

 
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.swt.SWTResourceManager;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.UseCaseScenarioEditor;

/**
 * The Class EnactmentHandler.
 */
public class EnactmentHandler {
	
	private UseCaseScenarioViewModel viewModel;
	private UseCaseScenarioEditor editor;
	private Timer timer; 

	/**
	 * Instantiates a new enactment handler.
	 *
	 * @param editor the editor
	 * @param viewModel the view model
	 */
	public EnactmentHandler(UseCaseScenarioEditor editor, UseCaseScenarioViewModel viewModel) {
		this.editor = editor;
		this.viewModel = viewModel;
	}

	public void run() {
		viewModel.setEnactmentModeEnabled(!viewModel.isEnactmentModeEnabled()); 

		if(viewModel.isEnactmentModeEnabled() == false) {    
			setNormalPerspective(); 
		} else { 
			setEnactmentPerspective(); 
		}  
	} 
	
	public void play() {
		
		//Cancel timer if already running
		if(timer != null) timer.cancel();
		
		//Always start at Root node in tree
		final Tree tr = editor.getScenarioComposite().getTreeViewer().getTree(); 
		tr.setSelection(tr.getTopItem());

		TimerTask ta = new TimerTask() {

			@Override
			public void run() {    
				Display.getDefault().asyncExec(new Runnable() { 
					 
					public void run() {

						//Do not run if speaking is in progress
						if(editor.getTextToSpeech().isSpeaking() || viewModel.isResolvingAutoPlay()) {
							return;
						} 
					 
						//Get the current selection
						if( tr.getSelection().length == 0) tr.setSelection(tr.getTopItem());
						TreeItem current = tr.getSelection()[0];
					
						
						TreeItem[] parentItems = current.getParentItem() != null ? 
								current.getParentItem().getItems() : current.getParent().getItems();
								
						int currentIndex = current.getParentItem() != null ? 
								current.getParentItem().indexOf(current): current.getParent().indexOf(current);
								
						//If not at the end of the item list
						if(currentIndex < parentItems.length - 1 && current.getItems().length == 0) { 
							current = parentItems[currentIndex + 1];
							tr.setSelection(current); 
							editor.selectedScenarioElementChanged((UseCaseScenarioElement) current.getData());
							
							//Check if the last node in tree has is selection
							TreeItem lastNodeInTree = current.getParent().getItem(current.getParent().getItemCount() - 1);
							
							if(current.equals(lastNodeInTree)) {
								speakText("Enactment complete!"); 
								editor.toggleAutoPlay();
							}
						
						}  else {
							
							//If there is sub items, let the user handle the next step
							if(current.getItems().length > 0) { 
								current.setExpanded(true);
								editor.getScenarioComposite().getTreeViewer().refresh();
							}
							
							speakText("Please select the next step"); 
							viewModel.setResolvingAutoPlay(true); 
						}
						
					}

					private void speakText(String text) {
						if(viewModel.isTextToSpeechEnabled()) {
							
							//Wait until previous text has been spoken
							while(editor.getTextToSpeech().isSpeaking()) {
								try {
									Thread.sleep(500);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace(); LogUtil.logError(e);
								}
							}
							try {
								editor.getTextToSpeech().SpeakText(text); 
							} catch (Exception e) { }
						}
					}
				}); 
			} 
		};

		//Change every five seconds
		timer = new Timer(true); 
		//Get the current selection
		if( tr.getSelection().length == 0) {
			tr.setSelection(tr.getTopItem());
		}
		else {
			editor.selectedScenarioElementChanged((UseCaseScenarioElement) tr.getSelection()[0].getData());
		}
		
		timer.scheduleAtFixedRate(ta, 5000, 5000); 
	}
	
	public void stop() {
		timer.cancel();
	}
	
	
	
	private void setEnactmentPerspective() {
		LayoutSwitchData lsData = new LayoutSwitchData();
		lsData.editableText = false;
		lsData.excludeScenarioReference = false;
		lsData.excludeTop = false;
		lsData.sashWeights = new int[] { 1,3 };
		lsData.textSize = 14;
		//lsData.editorPartState = IWorkbenchPage.STATE_MAXIMIZED;
		lsData.isFullScreen = true; 
		lsData.perspectiveText = "Normal Perspective";
		lsData.enactmentButtons = true;
		setPerspective(lsData); 
	}
	
	private void setNormalPerspective() {
		LayoutSwitchData lsData = new LayoutSwitchData();
		lsData.editableText = true;
		lsData.excludeScenarioReference = true;
		lsData.excludeTop = true;
		lsData.sashWeights = new int[] { 1,1 };
		lsData.textSize = 9;
		//lsData.editorPartState = IWorkbenchPage.STATE_RESTORED;
		lsData.isFullScreen = false;
		lsData.perspectiveText = "Enactment Perspective";
		lsData.enactmentButtons = false;
		setPerspective(lsData); 
	}
	
	private void setPerspective(LayoutSwitchData lsData) {
		
		//Set Maximized Editor Part
//		IWorkbenchPage activePage = editor.getEditorSite().getWorkbenchWindow().getActivePage();
				
		//Set Normal Editor Part
//		activePage.setPartState(
//				activePage.getActivePartReference(), lsData.editorPartState);  
		
		UseCaseScenarioComposite sc = editor.getScenarioComposite();  
	 
		// Exclude top
		editor.setExcludedCompositeVisibility(lsData.excludeTop, sc.getCmpTop()); 
		
		//Exclude comp scenario ref 
		editor.setExcludedCompositeVisibility(lsData.excludeScenarioReference, sc.getCompositeScenarioRef());
		
		//Set Enactment buttons
		editor.setExcludedCompositeVisibility(lsData.enactmentButtons, 
				editor.getScenarioComposite().getCmpEnactmentButtons());
		
		//Set weight on sash form
		sc.getSashForm().setWeights(lsData.sashWeights); 
		
		//Set Text Layout
		setTextLayout(sc.getTxtScenarioComponentName(), lsData);
		setTextLayout(sc.getTxtDescription(), lsData);  
		
		//Set Switcher Button Text
		editor.getScenarioComposite().getBtnEnactment().setText(lsData.perspectiveText); 
		
		//Set Normal Window
//		Display.getDefault().getActiveShell().setFullScreen(lsData.isFullScreen);  
	}  
	
	private void setTextLayout(Text tx, LayoutSwitchData lsData) {
		// Set text editable
		tx.setEditable(lsData.editableText); 
		tx.setFont(SWTResourceManager.getFont("Segoe UI", lsData.textSize, SWT.NORMAL));  
	}
	
	class LayoutSwitchData {
		public boolean enactmentButtons;
		public boolean isFullScreen;
		public boolean excludeTop;
		public int[] sashWeights;
		public boolean excludeScenarioReference;
		public int textSize;
		public boolean editableText; 
		public int editorPartState;
		public String perspectiveText;
	}
}
