package dk.dtu.imm.red.specificationelements.actor.ui.editors.impl;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorEditor;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorPresenter;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.ActorKind;
import dk.dtu.imm.red.specificationelements.actor.ActorPackage;
import dk.dtu.imm.red.specificationelements.actor.CodeType;

public class ActorEditorImpl extends BaseEditor implements ActorEditor {

	protected ElementBasicInfoContainer basicInfoContainer;

	// Bookkeeping
	/**
	 * Index of the Actor attr page.
	 */
	protected int actorPageIndex;

	// Infrastructure
	private ActorPresenter actorPresenter;
	private ActorViewModel viewModel;
	private EAGComposite<SpecificationElement> gdBehaviours;
	private ActorComposite actorComposite;
	private ActorDetailsComposite detailsComposite;
	private int detailsPageIndex;

	// UI Setup
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {

		super.init(site, input);
		viewModel = new ActorViewModel();
		viewModel.setElement((Actor) element);
		presenter = new ActorPresenterImpl(this, viewModel);
		actorPresenter = (ActorPresenter) presenter; // casted presenter
	}

	@Override
	public dk.dtu.imm.red.core.element.Element getEditorElement() {
		return viewModel.getElement();
	}

	@Override
	protected void createPages() {

		// Create attributes page
		actorPageIndex = addPage(createSummaryPage(getContainer(), "Actor Properties"));
		setPageText(actorPageIndex, "Summary");

		detailsPageIndex = addPage(createDetailsPage(getContainer()));
		setPageText(detailsPageIndex, "Details");

		super.createPages();
		fillInitialData();
	}

	protected Composite createSummaryPage(Composite parent, String groupName) {
		actorComposite = new ActorComposite(parent, SWT.NONE, this, groupName);

		// Setup behavior column
		gdBehaviours = new EAGComposite<SpecificationElement>(actorComposite.getBehaviourColumnContainer(), SWT.NONE,
				modifyListener, viewModel.getElement(), ActorPackage.eINSTANCE.getActor_BehaviorElements())
						.setModifyListener(modifyListener).setPresenter(actorPresenter.getBehaviourPresenter());
		gdBehaviours.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		gdBehaviours.setElementSelectorOnAdd(true);
		gdBehaviours.setAddDialogTitle("Create Reference to a behavioral Element");
		gdBehaviours.setAddDialogDescription("Select a behavioral Element, for example a Scenario.");

		return SWTUtils.wrapInScrolledComposite(parent, actorComposite);
	}

	private Composite createDetailsPage(Composite parent) {
		detailsComposite = new ActorDetailsComposite(parent, SWT.NONE, this);
		return SWTUtils.wrapInScrolledComposite(parent, detailsComposite);
	}

	public void setIsModified() {
		modifyListener.handleEvent(null);
	}

	// Infrastructure actions
	@Override
	public void selectText(EStructuralFeature feature, int startIndex, int endIndex) {
		if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(actorPageIndex);
		} else if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(actorPageIndex);
		}
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {
		return null; // todo remove
	}

	@Override
	public void doSaveAs() {
		return;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		ActorPresenter presenter = (ActorPresenter) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(), basicInfoContainer.getName(),
				basicInfoContainer.getKind(), basicInfoContainer.getDescription());
		String kind = basicInfoContainer.getKind();
		if (kind != null) {
			if (kind.equals(ActorKind.ROLE.getLiteral())) {
				presenter.setElementIconURI("icons/role.png");
			} else if (kind.equals(ActorKind.PERSON.getLiteral())) {
				presenter.setElementIconURI("icons/actor.png");
			} else if (kind.equals(ActorKind.GROUP.getLiteral())) {
				presenter.setElementIconURI("icons/group.png");
			} else if (kind.equals(ActorKind.TEAM.getLiteral())) {
				presenter.setElementIconURI("icons/team.png");
			} else if (kind.equals(ActorKind.ORGANISATION.getLiteral())) {
				presenter.setElementIconURI("icons/organisation.png");
			}
		}

		super.doSave(monitor);
	}

	private String defValue(String s) {
		return s != null ? s : "";
	}

	protected String[] getKindStrings() {
		ActorKind[] kinds = ActorKind.values();
		String[] kindStrings = new String[kinds.length];
		for (int i = 0; i < kinds.length; i++) {
			kindStrings[i] = kinds[i].getName();
		}

		return kindStrings;
	}

	// Fill UI widgets with data
	@Override
	protected void fillInitialData() {

		basicInfoContainer.fillInitialData(viewModel.getElement(), getKindStrings());

		String code = viewModel.getElement().getCode();

		detailsComposite.getTxtCodeContent().setText(defValue(code));

		// Set Table Editors
		gdBehaviours.setColumns(actorPresenter.getBehaviourColumnDefinition())
				.setDataSource(viewModel.getElement().getBehaviorElements());

		// Load Code Types
		for (CodeType a : CodeType.values()) {
			detailsComposite.getCmbCodeType().add(a.name());
		}

		detailsComposite.getCmbCodeType()
				.select(detailsComposite.getCmbCodeType().indexOf(viewModel.getElement().getCodeType().name()));

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	// Properties
	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.actor.actoreditor";
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Actor.html";
	}

	@Override
	public void codeTypeChanged() {
		String selection = detailsComposite.getCmbCodeType()
				.getItem(detailsComposite.getCmbCodeType().getSelectionIndex());
		actorPresenter.setCodeType(selection);
	}

	@Override
	public void codeContentChanged() {
		actorPresenter.setCodeContent(detailsComposite.getTxtCodeContent().getText());
		setIsModified();

	}

	public void setBasicInfoContainer(ElementBasicInfoContainer basicInfoContainer) {
		this.basicInfoContainer = basicInfoContainer;
	}

	protected ActorComposite getActorComposite() {
		return actorComposite;
	}
}
