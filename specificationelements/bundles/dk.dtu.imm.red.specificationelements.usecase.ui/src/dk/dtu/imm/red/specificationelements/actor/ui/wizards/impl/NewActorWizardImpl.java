package dk.dtu.imm.red.specificationelements.actor.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;  
import dk.dtu.imm.red.specificationelements.actor.ui.wizards.NewActorWizard;
import dk.dtu.imm.red.specificationelements.actor.ui.wizards.NewActorWizardPresenter;
import dk.dtu.imm.red.specificationelements.actor.Actor;

public class NewActorWizardImpl extends BaseNewWizard 
	implements NewActorWizard {
	
	protected NewActorWizardPresenter presenter;
	
	public NewActorWizardImpl() {
		super("Actor", Actor.class);
		presenter = new NewActorWizardPresenterImpl(this);
	}
	
	@SuppressWarnings("rawtypes")
	public NewActorWizardImpl(String type, Class clazz) {
		super(type, clazz);
	}
	
	@Override
	public void addPages() {
		super.addPages();
	}
	
	@Override
	public boolean performFinish() {
		
		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription(); 
		
		Group parent = null;
				
		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();
			
			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}
		
		
		presenter.wizardFinished(label, name, description, parent, "");
		
		return true;
	}

	@Override
	public boolean canFinish() {
		return super.canFinish() 
				&& elementBasicInfoPage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}

}
