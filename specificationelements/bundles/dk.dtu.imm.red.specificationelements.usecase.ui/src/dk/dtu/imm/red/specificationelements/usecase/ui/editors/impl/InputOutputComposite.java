package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.Cell;
import org.agilemore.agilegrid.DefaultCellEditorProvider;
import org.agilemore.agilegrid.ISelectionChangedListener;
import org.agilemore.agilegrid.SWTX;
import org.agilemore.agilegrid.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;

import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.specificationelements.usecase.InputOutput;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.UsecaseFactory;
import dk.dtu.imm.red.specificationelements.usecase.ui.editors.UseCasePresenter;

public class InputOutputComposite extends Composite {


	protected AgileGrid ioTable;
	protected InputOutputTableContentProvider ioTableContentProvider;
	protected InputOutputTableLayoutAdviser ioTableLayoutAdviser;
	protected Button btnAddInputOutput ;
	protected Button btnRemoveInputOutput;
	protected UseCase uc;
	protected BaseEditor editor;
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public InputOutputComposite(Composite parent, int style, final BaseEditor ed, UseCase useCase) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		uc = useCase;
		editor = ed;
		Group grpInputOutputbusinessrule = new Group(this, SWT.NONE);
		grpInputOutputbusinessrule.setLayout(new GridLayout(2, false));
		GridData gd_grpInputOutputbusinessrule = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		gd_grpInputOutputbusinessrule.heightHint = 177;
		gd_grpInputOutputbusinessrule.widthHint = 437;
		grpInputOutputbusinessrule.setLayoutData(gd_grpInputOutputbusinessrule);
		grpInputOutputbusinessrule.setText("Functional Specification");

		ioTable = new AgileGrid(grpInputOutputbusinessrule, SWTX.ROW_SELECTION | SWT.V_SCROLL);
		ioTableContentProvider = new InputOutputTableContentProvider(editor);
		ioTableLayoutAdviser = new InputOutputTableLayoutAdviser(ioTable);
		ioTable.setContentProvider(ioTableContentProvider);
		ioTable.setLayoutAdvisor(ioTableLayoutAdviser);
		ioTable.setCellEditorProvider(new DefaultCellEditorProvider(ioTable));
		ioTableContentProvider.setContent(uc.getInputOutputList());

		GridData inputOutputTableLayoutData = new GridData(SWT.FILL, SWT.FILL, true, true,2,1);
		inputOutputTableLayoutData.heightHint = 182;
		inputOutputTableLayoutData.widthHint = 423;
		ioTable.setLayoutData(inputOutputTableLayoutData);
		ioTable.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				if (ioTable.getCellSelection().length > 0) {
					btnRemoveInputOutput.setEnabled(true);
				} else {
					btnRemoveInputOutput.setEnabled(false);
				}
			}
		});


		btnAddInputOutput = new Button(grpInputOutputbusinessrule, SWT.NONE);
		btnAddInputOutput.setText("Add Rule");
		btnAddInputOutput.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				InputOutput io = UsecaseFactory.eINSTANCE.createInputOutput();
				((UseCasePresenter) ed.getPresenter()).getInputOutputList().add(io);
				editor.markAsDirty();

				ioTableLayoutAdviser.setRowCount(((UseCasePresenter) ed.getPresenter()).getInputOutputList().size());

				ioTable.redraw();

			}
		});

		btnRemoveInputOutput = new Button(grpInputOutputbusinessrule, SWT.NONE);
		btnRemoveInputOutput.setText("Remove Rule");
		btnRemoveInputOutput.setEnabled(false);
		btnRemoveInputOutput.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				Cell[] selectedCells = ioTable.getCellSelection();
				System.out.println("Removing input output");
				for (int i = selectedCells.length-1; i >= 0; --i) {
					((UseCasePresenter) ed.getPresenter()).getInputOutputList().remove(selectedCells[i].row);

				}
				editor.markAsDirty();
				ioTableLayoutAdviser.setRowCount(((UseCasePresenter) ed.getPresenter()).getInputOutputList().size());
				ioTable.clearSelection();
				btnRemoveInputOutput.setEnabled(false);
				ioTable.redraw();
			}

		});
		ioTableLayoutAdviser.setRowCount(((UseCasePresenter) ed.getPresenter()).getInputOutputList().size());
		ioTable.redraw();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
