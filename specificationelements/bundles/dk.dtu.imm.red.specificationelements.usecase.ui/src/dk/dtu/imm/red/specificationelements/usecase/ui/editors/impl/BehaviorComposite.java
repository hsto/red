/*
 * @author Johan Paaske Nielsen
 * 
 * This Composite Control is built with Window Builder and can be parsed by this tool - 
 * https://www.eclipse.org/windowbuilder/ 
 * 
 * Contains the GUI for all the Usecase References
 */

package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.GridData;

// TODO: Auto-generated Javadoc
/**
 * The Class ReferencesComposite.
 */
public class BehaviorComposite extends Composite {

	/** The grp primary scenario. */
	private Group grpPrimaryScenario;
	
	/** The grp primary actor. */
	private Group grpPrimaryActor;
	
	/** The grp secondary actors. */
	private Group grpSecondaryActors;
	
	/** The grp alternate scenarios. */
	private Group grpAlternateScenarios;
	private Composite scenariosAndActorsComposite;
	private Composite tableComposite;
	private Group grpLongDescription;

	/**
	 * Create the composite.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param useCaseEditor the use case editor
	 */
	public BehaviorComposite(Composite parent, int style,  UseCaseEditorImpl useCaseEditor) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		grpLongDescription = new Group(this, SWT.NONE);
		grpLongDescription.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpLongDescription.setText("Long Description");
		grpLongDescription.setLayout(new GridLayout(1, false));
		
		scenariosAndActorsComposite = new Composite(this, SWT.NONE);
		GridData gd_scenariosAndActorsComposite = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		gd_scenariosAndActorsComposite.heightHint = 302;
		scenariosAndActorsComposite.setLayoutData(gd_scenariosAndActorsComposite);
		scenariosAndActorsComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		SashForm sashForm = new SashForm(scenariosAndActorsComposite, SWT.HORIZONTAL);
		
		SashForm sashForm_1 = new SashForm(sashForm, SWT.VERTICAL);
		
		grpPrimaryScenario = new Group(sashForm_1, SWT.PUSH);
		grpPrimaryScenario.setText("Primary scenario");
		grpPrimaryScenario.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		grpAlternateScenarios = new Group(sashForm_1, SWT.NONE);
		grpAlternateScenarios.setText("Secondary scenarios");
		grpAlternateScenarios.setLayout(new FillLayout(SWT.HORIZONTAL));
		sashForm_1.setWeights(new int[] {78, 80});
		
		SashForm sashForm_2 = new SashForm(sashForm, SWT.VERTICAL);
		
		grpPrimaryActor = new Group(sashForm_2, SWT.NONE);
		grpPrimaryActor.setText("Primary actor");
		grpPrimaryActor.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		grpSecondaryActors = new Group(sashForm_2, SWT.NONE);
		grpSecondaryActors.setText("Secondary actors");
		grpSecondaryActors.setLayout(new FillLayout(SWT.HORIZONTAL));
		sashForm_2.setWeights(new int[] {77, 81});
		sashForm.setWeights(new int[] {1, 1});
		
		tableComposite = new Composite(this, SWT.NONE);
		tableComposite.setLayout(new GridLayout(1, false));
		tableComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));

	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Composite#checkSubclass()
	 */
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	/**
	 * Gets the Group primary scenario.
	 *
	 * @return the Group primary scenario
	 */
	public Group getGrpPrimaryScenario() {
		return grpPrimaryScenario;
	}

	/**
	 * Gets the Group primary actor.
	 *
	 * @return the Group primary actor
	 */
	public Group getGrpPrimaryActor() {
		return grpPrimaryActor;
	}

	/**
	 * Gets the Group secondary actors.
	 *
	 * @return the Group secondary actors
	 */
	public Group getGrpSecondaryActors() {
		return grpSecondaryActors;
	}

	/**
	 * Gets the Group alternate scenarios.
	 *
	 * @return the Group alternate scenarios
	 */
	public Group getGrpAlternateScenarios() {
		return grpAlternateScenarios;
	}
	
	public Group getGrpLongDescription(){
		return grpLongDescription;
	}
	
	public Composite getTableComposite(){
		return tableComposite;
	}
}
