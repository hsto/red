package dk.dtu.imm.red.specificationelements.actor.ui.editors;

import org.eclipse.ui.IEditorPart; 
import dk.dtu.imm.red.core.ui.editors.IBaseEditor; 

public interface ActorEditor extends IEditorPart, IBaseEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.actor.actoreditor";
	void codeTypeChanged();
	void codeContentChanged();
}
