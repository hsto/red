package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;

import java.util.List;

import org.agilemore.agilegrid.DefaultContentProvider;

import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.specificationelements.usecase.InputOutput;

public class InputOutputTableContentProvider extends DefaultContentProvider {

	private static final int INPUT_COLUMN = 0;
	private static final int OUTPUT_COLUMN = 1;
	
	private BaseEditor editor;
	
	private List<InputOutput> inputOutput;

	public InputOutputTableContentProvider(BaseEditor baseEditor) {
		super();
		this.editor = baseEditor;
	}

	public void setContent(List<InputOutput> inputOutput) {
		this.inputOutput = inputOutput;
	}

	@Override
	public void doSetContentAt(int row, int col, Object value) {
		InputOutput io = inputOutput.get(row);

		if (io == null) {
			super.doSetContentAt(row, col, value);
		}

		switch (col) {
			case INPUT_COLUMN:
				io.setInput((String) value);
				break;
			case OUTPUT_COLUMN:
				io.setOutput((String) value);
				break;
			default:
				doSetContentAt(row, col, value);
		}
		
		editor.markAsDirty();
	}

	@Override
	public Object doGetContentAt(int row, int col) {

		InputOutput io = inputOutput.get(row);

		if (io == null) {
			return super.doGetContentAt(row, col);
		}

		switch (col) {
			case INPUT_COLUMN:
				return io.getInput();
			case OUTPUT_COLUMN:
				return io.getOutput();
			default:
				return doGetContentAt(row, col);
		}
	}
}
