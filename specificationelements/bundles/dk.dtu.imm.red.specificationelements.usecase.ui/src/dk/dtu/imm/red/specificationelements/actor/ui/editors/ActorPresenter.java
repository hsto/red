package dk.dtu.imm.red.specificationelements.actor.ui.editors;  
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.SpecificationElement;

public interface ActorPresenter extends IEditorPresenter {
	void setCodeType(String codeType);
	EAGPresenter<SpecificationElement> getBehaviourPresenter();
	ElementColumnDefinition[] getBehaviourColumnDefinition();
	
	void setCodeContent(String codeContent);
}
