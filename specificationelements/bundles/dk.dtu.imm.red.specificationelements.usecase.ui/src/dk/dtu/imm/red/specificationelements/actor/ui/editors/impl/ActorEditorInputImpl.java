package dk.dtu.imm.red.specificationelements.actor.ui.editors.impl; 
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;  
import dk.dtu.imm.red.specificationelements.actor.Actor;
 

public class ActorEditorInputImpl extends BaseEditorInput {

	public ActorEditorInputImpl(Actor actor) {
		super(actor);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Actor editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for actor " + element.getName();
	}
}
