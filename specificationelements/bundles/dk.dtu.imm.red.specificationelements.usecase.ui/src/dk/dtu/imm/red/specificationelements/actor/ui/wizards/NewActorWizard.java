package dk.dtu.imm.red.specificationelements.actor.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewActorWizard extends IBaseWizard {
	
	String ID = "dk.dtu.imm.red.specificationelements.actor.newwizard";

}
