package dk.dtu.imm.red.specificationelements.persona.ui.editors;

import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface PersonaEditor extends IEditorPart, IBaseEditor {

	final String ID = "dk.dtu.imm.red.specificationelements.persona.personaeditor";

	void refreshPersonaImage();
}
