package dk.dtu.imm.red.specificationelements.persona.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.persona.Persona;

public class PersonaEditorInputImpl extends BaseEditorInput {

	/**
	 * The ID of the Persona Editor.
	 */
	public static final String ID = 
			"dk.dtu.imm.red.specificationelements.persona.personaeditor";

	public PersonaEditorInputImpl(Persona persona) {
		super(persona);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Persona " + element.getName();
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return "Editor for Persona " + element.getName();
	}
}
