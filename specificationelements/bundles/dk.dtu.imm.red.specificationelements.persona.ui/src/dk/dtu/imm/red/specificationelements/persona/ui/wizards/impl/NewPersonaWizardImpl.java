package dk.dtu.imm.red.specificationelements.persona.ui.wizards.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.ui.wizards.ElementBasicInfoPage;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.ui.wizards.NewPersonaWizard;
import dk.dtu.imm.red.specificationelements.persona.ui.wizards.NewPersonaWizardPresenter;

/**
 * Wizard used for creating a new persona.
 * @author Jakob Kragelund
 *
 */
public class NewPersonaWizardImpl extends BaseNewWizard 
	implements NewPersonaWizard {

	/**
	 * Page where the user can enter the basic information about the persona,
	 * such as name, age, job and description.
	 */
	
//	protected ElementBasicInfoPage definePersonaPage;

//	protected DefinePersonaPage definePersonaPage;
	
	/**
	 * The presenter for this wizard.
	 */
	protected NewPersonaWizardPresenter presenter;

	public NewPersonaWizardImpl(){
		super("Persona", Persona.class);
		presenter = new NewPersonaWizardPresenterImpl(this);
//		definePersonaPage = new ElementBasicInfoPage("Persona");
	}
	
	@Override
	public void addPages() {
//		addPage(definePersonaPage);
		super.addPages();
	}

	@Override
	public boolean performFinish() {
		
		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription(); 

		Group parent = null;
				
		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();
			
			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}
		
		presenter.wizardFinished(label, name, description, parent, "");
		
		return true;
	}

	@Override
	public boolean canFinish() {
		return super.canFinish() 
				&& elementBasicInfoPage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}
	
	@Override
	public void setPersonaName(final String text) {
		displayInfoPage.setDisplayName(text);
	}
	
}
