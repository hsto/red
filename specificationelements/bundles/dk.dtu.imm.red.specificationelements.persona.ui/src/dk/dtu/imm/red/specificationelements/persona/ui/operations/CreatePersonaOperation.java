package dk.dtu.imm.red.specificationelements.persona.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.PersonaFactory;
import dk.dtu.imm.red.specificationelements.persona.ui.extensions.PersonaElementExtension;

/**
 * Operation which contains logic for creating a new persona.
 * @author Jakob Kragelund
 *
 */
public class CreatePersonaOperation extends AbstractOperation {

	protected String label;
	protected String name;
	protected String description;
	protected Group parent;
//	protected String path;

	protected Persona persona;

	/**
	 * The Element extension point from the specification plugin.
	 * Used for opening the editor of the newly created persona.
	 */
	protected PersonaElementExtension extension;

	/**
	 * Creates a new Create Persona operation, which, given all the details of
	 * a persona, creates a new persona.
	 * @param name the name of the persona
	 * @param occupation the job/occupation of the persona
	 * @param age the age of the persona
	 * @param description the description of the persona
	 * @param parent the folder in which the persona is placed (may be null)
	 * @param path the path to the file where the persona is saved (may be null)
	 */
	public CreatePersonaOperation(String label, String name, String description, Group parent, String path) {
		super("Create Persona");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
//		this.path = path;

		extension = new PersonaElementExtension();
	}

	@Override
	public IStatus execute(final IProgressMonitor monitor,
			final IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		persona = PersonaFactory.eINSTANCE.createPersona();
		persona.setCreator(PreferenceUtil.getUserPreference_User());


		persona.setLabel(label);
		persona.setName(name);
		persona.setDescription(description);

		// If a parent folder has been chosen, make that parent
		if (parent != null) {
			persona.setParent(parent);
		}

		// save the persona, and open the editor.
		persona.save();
		extension.openElement(persona);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		// Undo the creation of a persona by removing it from its parent.
		if (parent != null) {
			parent.getContents().remove(persona);
			extension.deleteElement(persona);
			persona.delete();
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}

}
