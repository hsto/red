package dk.dtu.imm.red.specificationelements.persona.ui.editors;

import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;

public interface PersonaEditorPresenter extends IEditorPresenter {


	/**
	 * @return the maximum allowed length of the age text field.
	 */
	int getAgeMaxLength();

	/**
	 * @return the max length of the name text field in the Persona editor.
	 */
	int getNameMaxLength();

	/**
	 * @return the maximum allowed length of the occupation text field.
	 */
	int getOccupationMaxLength();

	/**
	 * @return the image for this persona.
	 */
	Image getPersonaImage();
	
	/**
	 * Sets the age for this persona.
	 * @param age age of the persona
	 */
	void setAge(String age);

	/**
	 * Sets the occupation (job) for this persona.
	 * @param text the occupation for this persona
	 */
	void setOccupation(String text);
	
	/**
	 * Sets the image of this persona.
	 * @param image the image of this persona
	 */
	void setPersonaImage(Image image);

	/**
	 * Gets the default image.
	 * @return the default image
	 */
	Image getDefaultImage();

	void setStoryboard(Storyboard storyboard);

	/**
	 * sets the issues text for this persona
	 * @param text
	 */
	void setCapabilities(String text);
	void setConstraints(String text);

}