package dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.widgets.ImagePanelWidget;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardFactory;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.StoryboardEditor;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.StoryboardEditorInput;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.StoryboardPresenter;

public class StoryboardEditorImpl extends BaseEditor implements StoryboardEditor {

	private static final int NUM_PANELS = 5;
	protected Label storyboardLabel;
	protected ImagePanelWidget[] imagePanels;
	protected RichTextEditor introductionEditor;
	protected RichTextEditor conclusionEditor;

	protected Storyboard storyboard;
	
	private Listener panelModifyListener = new Listener() {
		@Override
		public void handleEvent(Event event) {
			markAsDirty();
		}
	};
	
	public StoryboardEditorImpl(BaseEditor parent) {
		super(parent);
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		if (!(input instanceof StoryboardEditorInput)) {
			throw new PartInitException("Invalid input for Storyboard Editor. " 
					+ "Must be of type StoryboardEditorInput");
		}

		setInput(input);
		setSite(site);
		
		imagePanels = new ImagePanelWidget[NUM_PANELS];	

		storyboard = EcoreUtil.copy(((StoryboardEditorInput) input).getStoryboard());

		presenter = new StoryboardPresenterImpl(this, (Storyboard) element);
		
		super.init(site, input);

	}
	
	@Override
	protected void createPages() {
		addPage(createStoryboardPage(getContainer()));
		if (getPageCount() == 1) {
			Composite container = getContainer();
	        if (container instanceof CTabFolder) {
	            ((CTabFolder) container).setTabHeight(0);
	        }
		}
	}
	
	@Override
	protected void fillInitialData() {
		
		while (storyboard.getPanels().size() < NUM_PANELS) {
			storyboard.getPanels().add(StoryboardFactory.eINSTANCE.createStoryboardPanel());
		}
		
		for (int i = 0; i < NUM_PANELS; i++) {
			//imagePanels[i].setImage(storyboard.getPanels().get(i).getImage());
			//somehow this part doesn't work, see Issue #16
			//so it is now moved to the method getStoryboardImage
			//which is loaded directly into ImageWidget
			
			
			imagePanels[i].setCaption(storyboard.getPanels().get(i).getCaption());
		}
		
		if (storyboard.getIntroduction() != null) {
			introductionEditor.setText(storyboard.getIntroduction().toString());
		}
		
		if (storyboard.getConclusion() != null) {
			conclusionEditor.setText(storyboard.getConclusion().toString());
		}
	}
	
	
	private Composite createStoryboardPage(final Composite parent) {
				
		final Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(3, false);

		composite.setLayout(layout);
		
		GridData helpButtonGridData = new GridData();
		helpButtonGridData.horizontalAlignment = SWT.END;
		helpButtonGridData.verticalAlignment = SWT.TOP;
		helpButtonGridData.horizontalSpan = 3;

		Composite helpContainer = createHelpContainer(composite, SWT.BORDER);
		helpContainer.setLayoutData(helpButtonGridData);
		
		GridData introductionGridData = new GridData();
		introductionGridData.verticalAlignment = SWT.FILL;
		introductionGridData.widthHint = 550;
		introductionGridData.grabExcessVerticalSpace = true;
		introductionGridData.horizontalSpan = 2;
		
		Group introductionGroup = new Group(composite, SWT.NONE);
		introductionGroup.setLayout(new FillLayout());
		introductionGroup.setText("Introduction");
		introductionGroup.setLayoutData(introductionGridData);
		
		introductionEditor = new RichTextEditor(
				introductionGroup, SWT.BORDER, getEditorSite(), commandListener);
		introductionEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				markAsDirty();
			}
		});
		
		
		GridData imageGridData = new GridData();
		imageGridData.heightHint = 350;
		imageGridData.widthHint = 275;
		imageGridData.horizontalAlignment = SWT.LEFT;
		
		for (int i = 0; i < NUM_PANELS; i++) {
			ImagePanelWidget panel = new ImagePanelWidget(composite, 
					SWT.NONE, true, "Picture " + (i+1), getStoryboardImage(i), "");
			panel.addListener(SWT.Modify, panelModifyListener);
			panel.setLayoutData(imageGridData);
			composite.layout();
			imagePanels[i] = panel;
		}
		
		GridData conclusionGridData = new GridData();
		conclusionGridData.verticalAlignment = SWT.FILL;
		conclusionGridData.widthHint = 550;
		conclusionGridData.grabExcessVerticalSpace = true;
		conclusionGridData.horizontalSpan = 2;
		
		Group conclusionGroup = new Group(composite, SWT.NONE);
		conclusionGroup.setLayout(new FillLayout());
		conclusionGroup.setText("Conclusion");
		conclusionGroup.setLayoutData(conclusionGridData);
		
		conclusionEditor = new RichTextEditor(
				conclusionGroup, SWT.BORDER, getEditorSite(), commandListener);
		conclusionEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				markAsDirty();
			}
		});
		
		
		fillInitialData();
		
		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}
	
	
	public Image getStoryboardImage(int index){
		
		if(index<storyboard.getPanels().size()){
			byte[] imageData = storyboard.getPanels().get(index).getImageData();
			if (imageData != null 
					&& imageData.length > 0) {
				ImageLoader imageLoader = new ImageLoader();
				ByteArrayInputStream inputStream = 
						new ByteArrayInputStream(imageData);
				ImageData[] imageDataArray = imageLoader.load(inputStream);
				
				if (imageDataArray != null && imageDataArray.length > 0) {
					Image image = new Image(Display.getCurrent(), imageDataArray[0]);
					return image;
				}
			}
		}
		return null;
	}

	@Override
	public void refreshImage() {

	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Storyboard getStoryboard() {
		for (int i = 0; i < NUM_PANELS; i++) {
			//Added to fix the bug of image not getting saved
			Image image = imagePanels[i].getImage();
			ImageLoader imgSaver = new ImageLoader();
			if (image != null && image.getImageData() != null) {
				imgSaver.data = new ImageData[] {image.getImageData()};
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				imgSaver.save(outputStream, SWT.IMAGE_PNG);
				
				storyboard.getPanels().get(i).setImage(imagePanels[i].getImage());
				storyboard.getPanels().get(i).setImageData(outputStream.toByteArray());
			}
			else{
				storyboard.getPanels().get(i).setImage(null);
				storyboard.getPanels().get(i).setImageData(null);
			}
			storyboard.getPanels().get(i).setCaption(imagePanels[i].getCaption());
		}
		storyboard.setIntroduction(TextFactory.eINSTANCE.createText(
				introductionEditor.getText()));
		storyboard.setConclusion(TextFactory.eINSTANCE.createText(
				conclusionEditor.getText()));
		return storyboard;
	}
	
	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Storyboard.html";
	}
	
	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.persona.storyboard.storyboardeditor";
	}

}
