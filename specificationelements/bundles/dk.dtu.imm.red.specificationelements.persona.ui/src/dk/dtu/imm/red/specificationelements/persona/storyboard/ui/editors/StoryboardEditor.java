package dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors;

import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;

public interface StoryboardEditor extends IEditorPart, IBaseEditor{

	public static final String ID = "dk.dtu.imm.red.specificationelements.persona.storyboard.storyboardeditor";

	void refreshImage();
	
	Storyboard getStoryboard();
	
}
