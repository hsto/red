package dk.dtu.imm.red.specificationelements.persona.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.ui.editors.PersonaEditor;
import dk.dtu.imm.red.specificationelements.persona.ui.editors.impl.PersonaEditorInputImpl;

public class PersonaElementExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element) {

		if (element instanceof Persona) {
			PersonaEditorInputImpl editorInput = new PersonaEditorInputImpl(
					(Persona) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, PersonaEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}

}
