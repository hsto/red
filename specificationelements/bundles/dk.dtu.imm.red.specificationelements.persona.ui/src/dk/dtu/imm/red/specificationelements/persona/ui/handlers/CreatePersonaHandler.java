package dk.dtu.imm.red.specificationelements.persona.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.specificationelements.persona.ui.wizards.impl.NewPersonaWizardImpl;

/**
 * Handler for the Create Persona command. Opens the New Persona Wizard, which
 * handles all the logic of creating a new persona, including undo/redo.
 * @author Jakob Kragelund
 *
 */
public class CreatePersonaHandler extends AbstractHandler {

	/**
	 * The ID of the command which this handler handles.
	 */
	public static final String ID = 
			"dk.dtu.imm.red.specificationelements.createpersona";
	
	@Override
	public Object execute(final ExecutionEvent event) 
			throws ExecutionException {
		
		NewPersonaWizardImpl wizard = new NewPersonaWizardImpl();
		ISelection currentSelection = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection();
		wizard.init(PlatformUI.getWorkbench(),
				(IStructuredSelection) currentSelection);

		WizardDialog dialog = new WizardDialog(
				HandlerUtil.getActiveShell(event), wizard);
		dialog.create();
		
		dialog.open();
		
		return null;
	}
}
