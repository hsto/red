package dk.dtu.imm.red.specificationelements.persona.ui.editors.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.eclipse.core.commands.operations.ObjectUndoContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.persona.ui.editors.PersonaEditor;
import dk.dtu.imm.red.specificationelements.persona.ui.editors.PersonaEditorPresenter;

/**
 * The presenter for the persona editor.
 * @author Jakob Kragelund
 *
 */
public class PersonaEditorPresenterImpl extends BaseEditorPresenter implements PersonaEditorPresenter {

	public static final int MAX_OCCUPATION_LENGTH = 40;

	public static final int MAX_NAME_LENGTH = 40;

	public static final int MAX_AGE_LENGTH = 2;
	
	/**
	 * The persona image.
	 */
	protected Image image;
	
	/**
	 * The age of the persona.
	 */
	protected Integer personaAge;
	
	protected String personaCapabilitiesText;
	protected String personaConstraintsText;
	
	/**
	 * The persona editor.
	 */
	protected PersonaEditor personaEditor;
	
	/**
	 * The occupation of the persona.
	 */
	protected String personaOccupation;

	protected Storyboard storyboard;

	/**
	 * Creates a new presenter for a persona editor.
	 * @param personaEditor the editor
	 * @param element the persona
	 */
	public PersonaEditorPresenterImpl(PersonaEditor personaEditor, Persona element) {
		super(element);
		this.personaEditor = personaEditor;
		this.element = element;
		this.setUndoContext(new ObjectUndoContext(personaEditor));
		
	}

	@Override
	public int getAgeMaxLength() {
		return MAX_AGE_LENGTH;
	}

	@Override
	public int getNameMaxLength() {
		return MAX_NAME_LENGTH;
	}

	@Override
	public int getOccupationMaxLength() {
		return MAX_OCCUPATION_LENGTH;
	}
	
	@Override
	public Image getPersonaImage() {
		
		Persona persona = (Persona) element;
		
		// If no image is loaded
		if (image == null) {
			ImageLoader imageLoader = new ImageLoader();
			
			// If there is an image stored in the persona's picturedata
			// we load that
			if (persona.getPictureData() != null 
					&& persona.getPictureData().length > 0) {
				
				ByteArrayInputStream inputStream = 
						new ByteArrayInputStream(persona.getPictureData());
				ImageData[] imageDataArray = imageLoader.load(inputStream);
				
				if (imageDataArray != null && imageDataArray.length > 0) {
					image = new Image(Display.getCurrent(), imageDataArray[0]);
					return image;
				}
			}
			
			// if loading the image from the persona's picturedata didn't work,
			// we try loading the picture from the persona image path
			if (persona.getPictureURI() != null) {
				image = new Image(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell().getDisplay(), 
						persona.getPictureURI());
				return image;
			}
			
			// If loading the image above didn't work, we load the default image.
			image = getDefaultImage();
		}
		
		return image;
	}

	@Override
	public void save() {
		super.save();
		
		Persona persona = (Persona) element;
		
//		persona.setName(personaName);
		
		persona.setCapabilities(TextFactory.eINSTANCE.createText(personaCapabilitiesText));
		persona.setConstraints(TextFactory.eINSTANCE.createText(personaConstraintsText));	
				
		persona.setJob(personaOccupation);
		persona.setAge(personaAge);
		ImageLoader imgSaver = new ImageLoader();
		
		if (image != null && image.getImageData() != null) {
			imgSaver.data = new ImageData[] {image.getImageData()};
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			imgSaver.save(outputStream, SWT.IMAGE_PNG);
			
			persona.setPictureData(outputStream.toByteArray());
		}
		
		persona.getStoryboards().clear();
		persona.getStoryboards().add(storyboard);
		
		persona.save();
		
	}

	@Override
	public void setOccupation(final String text) {
		this.personaOccupation = text;
	}

	@Override
	public void setAge(final String text) {
		try {
			this.personaAge = Integer.parseInt(text);
		} catch (NumberFormatException e) {
			this.personaAge = null;
		}
	}

	
	@Override
	public Image getDefaultImage() {
		return null;
	}

	@Override
	public void setPersonaImage(final Image image) {
		this.image = image;
		personaEditor.refreshPersonaImage();
	}
	
	@Override
	public void setStoryboard(Storyboard storyboard) {
		this.storyboard = storyboard;
		
	}

	@Override
	public void setCapabilities(String text) {
		this.personaCapabilitiesText = text;
		
	}
	
	@Override
	public void setConstraints(String text) {
		this.personaConstraintsText = text;
		
	}
}
