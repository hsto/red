package dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors;

import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;

public interface StoryboardPresenter extends IEditorPresenter {

	void setStoryboard(Storyboard storyboard);
}
