package dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors;

import org.eclipse.ui.IEditorInput;

import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;

public interface StoryboardEditorInput extends IEditorInput{

	Storyboard getStoryboard();

}
