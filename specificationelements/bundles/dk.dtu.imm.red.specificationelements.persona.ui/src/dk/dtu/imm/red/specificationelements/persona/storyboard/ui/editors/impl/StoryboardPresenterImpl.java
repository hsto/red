package dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.StoryboardPresenter;

public class StoryboardPresenterImpl extends BaseEditorPresenter implements StoryboardPresenter {

	protected StoryboardEditorImpl storyboardEditorImpl;
	protected Storyboard storyboard;
	
	public StoryboardPresenterImpl(StoryboardEditorImpl storyboardEditorImpl, Storyboard element) {
		super(element);
		this.storyboardEditorImpl = storyboardEditorImpl;
		this.storyboard = element;
	}
	
	@Override
	public void save() {
		
	}
	
	@Override
	public void setStoryboard(Storyboard storyboard) {
		this.storyboard = storyboard;
	}
}
