package dk.dtu.imm.red.specificationelements.persona.ui.editors.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.widgets.ImagePanelWidget;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.PersonaKind;
import dk.dtu.imm.red.specificationelements.persona.PersonaPackage;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.StoryboardEditor;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.StoryboardEditorInput;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.impl.StoryboardEditorImpl;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.impl.StoryboardEditorInputImpl;
import dk.dtu.imm.red.specificationelements.persona.ui.editors.PersonaEditor;
import dk.dtu.imm.red.specificationelements.persona.ui.editors.PersonaEditorPresenter;

/**
 * An implementation of the editor for personas
 * @author Anders Friis
 *
 */
public class PersonaEditorImpl extends BaseEditor implements PersonaEditor,	IEditorPart {

	protected ElementBasicInfoContainer basicInfoContainer;

	/**
	 * The standard top and bottom margins for rich text editors.
	 */
	private static final int EDITOR_MARGIN_HEIGHT = 5;

	/**
	 * The standard left and right margins for rich text editors.
	 */
	private static final int EDITOR_MARGIN_WIDTH = 5;

	/**
	 * The minimum height of the narrative rich text editor.
	 */

	/**
	 * The label for the image canvas.
	 */
	protected Label imageLabel;

	/**
	 * The label for the rich text editor of description.
	 */
	protected Label longDescriptionLabel;

	/**
	 * The rich text editor describing the issues related
	 * to this persona.
	 */
	protected RichTextEditor capabilitiesEditor;
	protected RichTextEditor constraintsEditor;

	/**
	 * The rich text editor for a story about the persona
	 */
//	Fix issue #37
//	protected RichTextEditor narrativeEditor;

	/**
	 * The labels for the information of the persona (name, occupation and age).
	 */
	protected Label occupationLabel;
	protected Label ageLabel;
	protected Label nameLabel;

	/**
	 * The textboxes for the information of the persona (name, occupation and
	 * age).
	 */
	protected Text nameTextBox;
	protected Text occupationTextBox;
	protected Text ageTextBox;

	/**
	 * The container widget for a persona image
	 */
	protected ImagePanelWidget imageContainer;

	protected StoryboardEditor storyboardEditor;

	protected int personaPageIndex = 0;

	protected int storyboardPageIndex = 1;

	@Override
	public void doSave(final IProgressMonitor monitor) {
		PersonaEditorPresenter presenter =
				(PersonaEditorPresenter) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		presenter.setOccupation(occupationTextBox.getText());
		presenter.setAge(ageTextBox.getText());
		presenter.setCapabilities(capabilitiesEditor.getText());
		presenter.setConstraints(constraintsEditor.getText());
		presenter.setPersonaImage(imageContainer.getImage());
		presenter.setStoryboard(storyboardEditor.getStoryboard());

		super.doSave(monitor);
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	protected void fillInitialData() {

		Persona persona = (Persona) element;

		PersonaKind[] kinds = PersonaKind.values();
		String[] kindStrings = new String[kinds.length];
		for(int i=0;i<kinds.length;i++){
			kindStrings[i] = kinds[i].getName();
		}

		basicInfoContainer.fillInitialData(persona, kindStrings);

		if (persona.getJob() != null) {
			occupationTextBox.setText(persona.getJob());
		}
		if (persona.getAge() != null) {
			ageTextBox.setText(persona.getAge().toString());
		}
		
		if (persona.getCapabilities() != null) {
			capabilitiesEditor.setText(persona.getCapabilities().toString());
		} else {
			capabilitiesEditor.setText("");
		}
		if (persona.getConstraints() != null) {
			constraintsEditor.setText(persona.getConstraints().toString());
		} else {
			constraintsEditor.setText("");
		}

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		super.init(site, input);

		presenter = new PersonaEditorPresenterImpl(this, (Persona) element);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	protected void createPages() {

		personaPageIndex = addPage(createPersonaPage(getContainer()));
		setPageText(personaPageIndex, "Details");

		try {
			Storyboard storyboard = null;
			if (((Persona) element).getStoryboards().size() > 0) {
				storyboard = ((Persona) element).getStoryboards().get(0);
			}
			StoryboardEditorInput storyboardInput =
					new StoryboardEditorInputImpl(storyboard);
			storyboardEditor = new StoryboardEditorImpl(this);
			storyboardPageIndex = addPage(storyboardEditor, storyboardInput);
			setPageText(storyboardPageIndex, "Storyboard");
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

		super.createPages();

		fillInitialData();
	}

	/**
	 * Create the main tab in the editor, which contains all the main
	 * widgets for editing this requirement.
	 * @param parent The parent editor of this tab
	 * @return the created tab
	 */
	private Composite createPersonaPage(final Composite parent) {
		PersonaEditorPresenter personaPresenter = (PersonaEditorPresenter) presenter;

		final Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(2, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=2;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.FILL, this, (SpecificationElement) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);

		
		//Occupation and Age

		Composite OtherInfoComposite = new Composite(composite, SWT.NONE);
		OtherInfoComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		OtherInfoComposite.setLayout(new GridLayout(2, false));


		occupationLabel = new Label(OtherInfoComposite, SWT.NONE);
		occupationLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		occupationLabel.setText("Occupation");

		occupationTextBox = new Text(OtherInfoComposite, SWT.BORDER);
		GridData gd_occupationTextBox = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_occupationTextBox.widthHint = 111;
		occupationTextBox.setLayoutData(gd_occupationTextBox);
		occupationTextBox.setTextLimit(personaPresenter.getOccupationMaxLength());
		occupationTextBox.addListener(SWT.Modify, modifyListener);

		ageLabel = new Label(OtherInfoComposite, SWT.NONE);
		ageLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		ageLabel.setText("Age");

		ageTextBox = new Text(OtherInfoComposite, SWT.BORDER);
		GridData gd_ageTextBox = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_ageTextBox.widthHint = 151;
		ageTextBox.setLayoutData(gd_ageTextBox);
		ageTextBox.setTextLimit(personaPresenter.getAgeMaxLength());
		ageTextBox.addListener(SWT.Modify, modifyListener);
		ageTextBox.addListener(SWT.Verify, new Listener() {
			@Override
			public void handleEvent(Event e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('0' <= chars[i] && chars[i] <= '9')) {
						e.doit = false;
						return;
					}
				}
			}
		});


		//Picture


		GridData imageGridData = new GridData();
		imageGridData.heightHint = 300;
		imageGridData.widthHint = 300;
		imageGridData.horizontalAlignment = SWT.RIGHT;
		imageGridData.grabExcessHorizontalSpace = true;

		imageContainer = new ImagePanelWidget(composite,
				SWT.NONE, false, "Picture of persona",
				((PersonaEditorPresenter) presenter).getPersonaImage(), "");
		imageContainer.setLayoutData(imageGridData);
		imageContainer.addListener(SWT.Modify, modifyListener);
		
		
		ModifyListener modifyList = new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		};

		//Capabilities and Constraints

		Label capabilitiesLabel = new Label(composite, SWT.NONE);
		capabilitiesLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		capabilitiesLabel.setText("Capabilities:");

		Label constraintsLabel = new Label(composite, SWT.NONE);
		constraintsLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		constraintsLabel.setText("Constraints:");

		GridData editorLayoutData2 = new GridData();
		editorLayoutData2.widthHint = 150;
		editorLayoutData2.heightHint = 200;
		editorLayoutData2.horizontalAlignment = SWT.FILL;
		editorLayoutData2.grabExcessHorizontalSpace = true;
		editorLayoutData2.horizontalSpan = 1;
		editorLayoutData2.verticalSpan = 1;

		capabilitiesEditor = new RichTextEditor(composite,
				SWT.BORDER | SWT.WRAP | SWT.V_SCROLL,
				getEditorSite(), null);
		capabilitiesEditor.setLayoutData(editorLayoutData2);
		capabilitiesEditor.addModifyListener(modifyList);

		GridData editorLayoutData3 = new GridData();
		editorLayoutData3.widthHint = 150;
		editorLayoutData3.heightHint = 200;
		editorLayoutData3.horizontalAlignment = SWT.FILL;
		editorLayoutData3.grabExcessHorizontalSpace = true;
		editorLayoutData3.horizontalSpan = 1;
		editorLayoutData3.verticalSpan = 1;

		constraintsEditor = new RichTextEditor(composite,
				SWT.BORDER | SWT.WRAP | SWT.V_SCROLL,
				getEditorSite(), null);
		constraintsEditor.setLayoutData(editorLayoutData3);
		constraintsEditor.addModifyListener(modifyList);

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	@Override
	public void dispose() {
		if (presenter != null) {
			presenter.dispose();
		}
		super.dispose();
	}

	@Override
	public void refreshPersonaImage() {
		imageContainer.refreshImage();
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Persona.html";
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		// put all the attributes which we want to search through
		// into the attributesToSearchMap
		super.addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);
		
		attributesToSearch.put(PersonaPackage.Literals.PERSONA__AGE,
				ageTextBox.getText());
		attributesToSearch.put(PersonaPackage.Literals.PERSONA__JOB,
				occupationTextBox.getText());
		attributesToSearch.put(PersonaPackage.Literals.PERSONA__CAPABILITIES,
				capabilitiesEditor.getText());
		attributesToSearch.put(PersonaPackage.Literals.PERSONA__CONSTRAINTS,
				constraintsEditor.getText());


		// Now, search through all attributes, recording all search hits along the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()-1);

				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(), result.getEndIndex());


			}
		}

		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			System.out.println("NOT EMPTY");
			return wrapper;
		}
	}

	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,personaPageIndex,basicInfoContainer);

		if (feature == PersonaPackage.Literals.PERSONA__AGE) {
			setActivePage(personaPageIndex);
			ageTextBox.setSelection(startIndex, endIndex);
		}
		else if (feature == PersonaPackage.Literals.PERSONA__JOB) {
			setActivePage(personaPageIndex);
			occupationTextBox.setSelection(startIndex, endIndex);
		} else if (feature == PersonaPackage.Literals.PERSONA__CAPABILITIES) {
			setActivePage(personaPageIndex);
			capabilitiesEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] {"" + startIndex, "" + endIndex});
		} else if (feature == PersonaPackage.Literals.PERSONA__CONSTRAINTS) {
			setActivePage(personaPageIndex);
			constraintsEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] {"" + startIndex, "" + endIndex});
		}
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.persona.personaeditor";
	}

}
