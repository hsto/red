package dk.dtu.imm.red.specificationelements.persona.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewPersonaWizard extends IBaseWizard {

	String ID = "dk.dtu.imm.red.specificationelements.persona.newwizard";
	
	void setPersonaName(String text);

}
