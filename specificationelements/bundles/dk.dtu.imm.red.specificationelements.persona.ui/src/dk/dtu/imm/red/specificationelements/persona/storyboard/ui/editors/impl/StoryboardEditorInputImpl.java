package dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardFactory;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.StoryboardEditorInput;

public class StoryboardEditorInputImpl extends BaseEditorInput
	implements StoryboardEditorInput {

	protected Storyboard storyboard;
	
	public StoryboardEditorInputImpl(Storyboard storyboard) {
		super(null);
		if (storyboard == null) {
			storyboard = StoryboardFactory.eINSTANCE.createStoryboard();
		}
		this.storyboard = storyboard;
	}
	
	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Storyboard Editor";
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return "The editor for storyboard";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof StoryboardEditorInputImpl) {
			StoryboardEditorInputImpl other = (StoryboardEditorInputImpl) obj;
			return (other.storyboard.equals(this.storyboard));
		} else {
			return false;
		}
	}

	@Override
	public Storyboard getStoryboard() {
		return storyboard;
	}

}
