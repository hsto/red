/**
 */
package dk.dtu.imm.red.specificationelements.persona.impl;

import dk.dtu.imm.red.core.text.TextPackage;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.PersonaFactory;
import dk.dtu.imm.red.specificationelements.persona.PersonaKind;
import dk.dtu.imm.red.specificationelements.persona.PersonaPackage;

import dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPackage;

import dk.dtu.imm.red.specificationelements.persona.storyboard.impl.StoryboardPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PersonaPackageImpl extends EPackageImpl implements PersonaPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum personaKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PersonaPackageImpl() {
		super(eNS_URI, PersonaFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PersonaPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PersonaPackage init() {
		if (isInited) return (PersonaPackage)EPackage.Registry.INSTANCE.getEPackage(PersonaPackage.eNS_URI);

		// Obtain or create and register package
		PersonaPackageImpl thePersonaPackage = (PersonaPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PersonaPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PersonaPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		StoryboardPackageImpl theStoryboardPackage = (StoryboardPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StoryboardPackage.eNS_URI) instanceof StoryboardPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StoryboardPackage.eNS_URI) : StoryboardPackage.eINSTANCE);

		// Create package meta-data objects
		thePersonaPackage.createPackageContents();
		theStoryboardPackage.createPackageContents();

		// Initialize created meta-data
		thePersonaPackage.initializePackageContents();
		theStoryboardPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePersonaPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PersonaPackage.eNS_URI, thePersonaPackage);
		return thePersonaPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersona() {
		return personaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersona_Age() {
		return (EAttribute)personaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersona_Job() {
		return (EAttribute)personaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersona_PictureURI() {
		return (EAttribute)personaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersona_PictureData() {
		return (EAttribute)personaEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersona_Storyboards() {
		return (EReference)personaEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersona_Issues() {
		return (EReference)personaEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersona_Capabilities() {
		return (EReference)personaEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersona_Constraints() {
		return (EReference)personaEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersona_Narrative() {
		return (EReference)personaEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPersonaKind() {
		return personaKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonaFactory getPersonaFactory() {
		return (PersonaFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		personaEClass = createEClass(PERSONA);
		createEAttribute(personaEClass, PERSONA__AGE);
		createEAttribute(personaEClass, PERSONA__JOB);
		createEAttribute(personaEClass, PERSONA__PICTURE_URI);
		createEAttribute(personaEClass, PERSONA__PICTURE_DATA);
		createEReference(personaEClass, PERSONA__STORYBOARDS);
		createEReference(personaEClass, PERSONA__ISSUES);
		createEReference(personaEClass, PERSONA__CAPABILITIES);
		createEReference(personaEClass, PERSONA__CONSTRAINTS);
		createEReference(personaEClass, PERSONA__NARRATIVE);

		// Create enums
		personaKindEEnum = createEEnum(PERSONA_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StoryboardPackage theStoryboardPackage = (StoryboardPackage)EPackage.Registry.INSTANCE.getEPackage(StoryboardPackage.eNS_URI);
		SpecificationelementsPackage theSpecificationelementsPackage = (SpecificationelementsPackage)EPackage.Registry.INSTANCE.getEPackage(SpecificationelementsPackage.eNS_URI);
		TextPackage theTextPackage = (TextPackage)EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theStoryboardPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		personaEClass.getESuperTypes().add(theSpecificationelementsPackage.getSpecificationElement());

		// Initialize classes and features; add operations and parameters
		initEClass(personaEClass, Persona.class, "Persona", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPersona_Age(), ecorePackage.getEIntegerObject(), "age", null, 0, 1, Persona.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersona_Job(), ecorePackage.getEString(), "job", null, 0, 1, Persona.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersona_PictureURI(), ecorePackage.getEString(), "pictureURI", null, 0, 1, Persona.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersona_PictureData(), ecorePackage.getEByteArray(), "pictureData", null, 0, 1, Persona.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersona_Storyboards(), theStoryboardPackage.getStoryboard(), null, "storyboards", null, 0, -1, Persona.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersona_Issues(), theTextPackage.getText(), null, "issues", null, 0, 1, Persona.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersona_Capabilities(), theTextPackage.getText(), null, "capabilities", null, 0, 1, Persona.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersona_Constraints(), theTextPackage.getText(), null, "constraints", null, 0, 1, Persona.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersona_Narrative(), theTextPackage.getText(), null, "narrative", null, 0, 1, Persona.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(personaKindEEnum, PersonaKind.class, "PersonaKind");
		addEEnumLiteral(personaKindEEnum, PersonaKind.UNSPECIFIED);
		addEEnumLiteral(personaKindEEnum, PersonaKind.ANALYSIS);
		addEEnumLiteral(personaKindEEnum, PersonaKind.DESIGN);

		// Create resource
		createResource(eNS_URI);
	}

} //PersonaPackageImpl
