/**
 */
package dk.dtu.imm.red.specificationelements.persona.storyboard.impl;

import dk.dtu.imm.red.specificationelements.persona.storyboard.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StoryboardFactoryImpl extends EFactoryImpl implements StoryboardFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StoryboardFactory init() {
		try {
			StoryboardFactory theStoryboardFactory = (StoryboardFactory)EPackage.Registry.INSTANCE.getEFactory(StoryboardPackage.eNS_URI);
			if (theStoryboardFactory != null) {
				return theStoryboardFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StoryboardFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StoryboardFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StoryboardPackage.STORYBOARD: return createStoryboard();
			case StoryboardPackage.STORYBOARD_PANEL: return createStoryboardPanel();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Storyboard createStoryboard() {
		StoryboardImpl storyboard = new StoryboardImpl();
		return storyboard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StoryboardPanel createStoryboardPanel() {
		StoryboardPanelImpl storyboardPanel = new StoryboardPanelImpl();
		return storyboardPanel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StoryboardPackage getStoryboardPackage() {
		return (StoryboardPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StoryboardPackage getPackage() {
		return StoryboardPackage.eINSTANCE;
	}

} //StoryboardFactoryImpl
