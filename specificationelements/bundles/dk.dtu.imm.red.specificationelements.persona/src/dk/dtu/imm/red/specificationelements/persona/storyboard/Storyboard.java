/**
 */
package dk.dtu.imm.red.specificationelements.persona.storyboard;

import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storyboard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard#getIntroduction <em>Introduction</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard#getPanels <em>Panels</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard#getConclusion <em>Conclusion</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPackage#getStoryboard()
 * @model
 * @generated
 */
public interface Storyboard extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Introduction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Introduction</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Introduction</em>' containment reference.
	 * @see #setIntroduction(Text)
	 * @see dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPackage#getStoryboard_Introduction()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getIntroduction();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard#getIntroduction <em>Introduction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Introduction</em>' containment reference.
	 * @see #getIntroduction()
	 * @generated
	 */
	void setIntroduction(Text value);

	/**
	 * Returns the value of the '<em><b>Panels</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPanel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Panels</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Panels</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPackage#getStoryboard_Panels()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<StoryboardPanel> getPanels();

	/**
	 * Returns the value of the '<em><b>Conclusion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conclusion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conclusion</em>' containment reference.
	 * @see #setConclusion(Text)
	 * @see dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPackage#getStoryboard_Conclusion()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getConclusion();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard#getConclusion <em>Conclusion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conclusion</em>' containment reference.
	 * @see #getConclusion()
	 * @generated
	 */
	void setConclusion(Text value);

} // Storyboard
