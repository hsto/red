/**
 */
package dk.dtu.imm.red.specificationelements.persona.storyboard.impl;

import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPackage;
import dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPanel;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storyboard</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.storyboard.impl.StoryboardImpl#getIntroduction <em>Introduction</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.storyboard.impl.StoryboardImpl#getPanels <em>Panels</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.storyboard.impl.StoryboardImpl#getConclusion <em>Conclusion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StoryboardImpl extends SpecificationElementImpl implements Storyboard {
	/**
	 * The cached value of the '{@link #getIntroduction() <em>Introduction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntroduction()
	 * @generated
	 * @ordered
	 */
	protected Text introduction;

	/**
	 * The cached value of the '{@link #getPanels() <em>Panels</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPanels()
	 * @generated
	 * @ordered
	 */
	protected EList<StoryboardPanel> panels;

	/**
	 * The cached value of the '{@link #getConclusion() <em>Conclusion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConclusion()
	 * @generated
	 * @ordered
	 */
	protected Text conclusion;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StoryboardImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StoryboardPackage.Literals.STORYBOARD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getIntroduction() {
		if (introduction != null && introduction.eIsProxy()) {
			InternalEObject oldIntroduction = (InternalEObject)introduction;
			introduction = (Text)eResolveProxy(oldIntroduction);
			if (introduction != oldIntroduction) {
				InternalEObject newIntroduction = (InternalEObject)introduction;
				NotificationChain msgs = oldIntroduction.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StoryboardPackage.STORYBOARD__INTRODUCTION, null, null);
				if (newIntroduction.eInternalContainer() == null) {
					msgs = newIntroduction.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StoryboardPackage.STORYBOARD__INTRODUCTION, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StoryboardPackage.STORYBOARD__INTRODUCTION, oldIntroduction, introduction));
			}
		}
		return introduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetIntroduction() {
		return introduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntroduction(Text newIntroduction, NotificationChain msgs) {
		Text oldIntroduction = introduction;
		introduction = newIntroduction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StoryboardPackage.STORYBOARD__INTRODUCTION, oldIntroduction, newIntroduction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntroduction(Text newIntroduction) {
		if (newIntroduction != introduction) {
			NotificationChain msgs = null;
			if (introduction != null)
				msgs = ((InternalEObject)introduction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StoryboardPackage.STORYBOARD__INTRODUCTION, null, msgs);
			if (newIntroduction != null)
				msgs = ((InternalEObject)newIntroduction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StoryboardPackage.STORYBOARD__INTRODUCTION, null, msgs);
			msgs = basicSetIntroduction(newIntroduction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StoryboardPackage.STORYBOARD__INTRODUCTION, newIntroduction, newIntroduction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StoryboardPanel> getPanels() {
		if (panels == null) {
			panels = new EObjectContainmentEList.Resolving<StoryboardPanel>(StoryboardPanel.class, this, StoryboardPackage.STORYBOARD__PANELS);
		}
		return panels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getConclusion() {
		if (conclusion != null && conclusion.eIsProxy()) {
			InternalEObject oldConclusion = (InternalEObject)conclusion;
			conclusion = (Text)eResolveProxy(oldConclusion);
			if (conclusion != oldConclusion) {
				InternalEObject newConclusion = (InternalEObject)conclusion;
				NotificationChain msgs = oldConclusion.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StoryboardPackage.STORYBOARD__CONCLUSION, null, null);
				if (newConclusion.eInternalContainer() == null) {
					msgs = newConclusion.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StoryboardPackage.STORYBOARD__CONCLUSION, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StoryboardPackage.STORYBOARD__CONCLUSION, oldConclusion, conclusion));
			}
		}
		return conclusion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetConclusion() {
		return conclusion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConclusion(Text newConclusion, NotificationChain msgs) {
		Text oldConclusion = conclusion;
		conclusion = newConclusion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StoryboardPackage.STORYBOARD__CONCLUSION, oldConclusion, newConclusion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConclusion(Text newConclusion) {
		if (newConclusion != conclusion) {
			NotificationChain msgs = null;
			if (conclusion != null)
				msgs = ((InternalEObject)conclusion).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StoryboardPackage.STORYBOARD__CONCLUSION, null, msgs);
			if (newConclusion != null)
				msgs = ((InternalEObject)newConclusion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StoryboardPackage.STORYBOARD__CONCLUSION, null, msgs);
			msgs = basicSetConclusion(newConclusion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StoryboardPackage.STORYBOARD__CONCLUSION, newConclusion, newConclusion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StoryboardPackage.STORYBOARD__INTRODUCTION:
				return basicSetIntroduction(null, msgs);
			case StoryboardPackage.STORYBOARD__PANELS:
				return ((InternalEList<?>)getPanels()).basicRemove(otherEnd, msgs);
			case StoryboardPackage.STORYBOARD__CONCLUSION:
				return basicSetConclusion(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StoryboardPackage.STORYBOARD__INTRODUCTION:
				if (resolve) return getIntroduction();
				return basicGetIntroduction();
			case StoryboardPackage.STORYBOARD__PANELS:
				return getPanels();
			case StoryboardPackage.STORYBOARD__CONCLUSION:
				if (resolve) return getConclusion();
				return basicGetConclusion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StoryboardPackage.STORYBOARD__INTRODUCTION:
				setIntroduction((Text)newValue);
				return;
			case StoryboardPackage.STORYBOARD__PANELS:
				getPanels().clear();
				getPanels().addAll((Collection<? extends StoryboardPanel>)newValue);
				return;
			case StoryboardPackage.STORYBOARD__CONCLUSION:
				setConclusion((Text)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StoryboardPackage.STORYBOARD__INTRODUCTION:
				setIntroduction((Text)null);
				return;
			case StoryboardPackage.STORYBOARD__PANELS:
				getPanels().clear();
				return;
			case StoryboardPackage.STORYBOARD__CONCLUSION:
				setConclusion((Text)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StoryboardPackage.STORYBOARD__INTRODUCTION:
				return introduction != null;
			case StoryboardPackage.STORYBOARD__PANELS:
				return panels != null && !panels.isEmpty();
			case StoryboardPackage.STORYBOARD__CONCLUSION:
				return conclusion != null;
		}
		return super.eIsSet(featureID);
	}

} //StoryboardImpl
