/**
 */
package dk.dtu.imm.red.specificationelements.persona.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.text.Text;


import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.PersonaPackage;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Persona</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl#getAge <em>Age</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl#getJob <em>Job</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl#getPictureURI <em>Picture URI</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl#getPictureData <em>Picture Data</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl#getStoryboards <em>Storyboards</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl#getIssues <em>Issues</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl#getCapabilities <em>Capabilities</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl#getNarrative <em>Narrative</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonaImpl extends SpecificationElementImpl implements Persona {
	/**
	 * The default value of the '{@link #getAge() <em>Age</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAge()
	 * @generated
	 * @ordered
	 */
	protected static final Integer AGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAge() <em>Age</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAge()
	 * @generated
	 * @ordered
	 */
	protected Integer age = AGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getJob() <em>Job</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJob()
	 * @generated
	 * @ordered
	 */
	protected static final String JOB_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getJob() <em>Job</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJob()
	 * @generated
	 * @ordered
	 */
	protected String job = JOB_EDEFAULT;

	/**
	 * The default value of the '{@link #getPictureURI() <em>Picture URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPictureURI()
	 * @generated
	 * @ordered
	 */
	protected static final String PICTURE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPictureURI() <em>Picture URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPictureURI()
	 * @generated
	 * @ordered
	 */
	protected String pictureURI = PICTURE_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getPictureData() <em>Picture Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPictureData()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] PICTURE_DATA_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPictureData() <em>Picture Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPictureData()
	 * @generated
	 * @ordered
	 */
	protected byte[] pictureData = PICTURE_DATA_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStoryboards() <em>Storyboards</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStoryboards()
	 * @generated
	 * @ordered
	 */
	protected EList<Storyboard> storyboards;

	/**
	 * The cached value of the '{@link #getIssues() <em>Issues</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIssues()
	 * @generated
	 * @ordered
	 */
	protected Text issues;

	/**
	 * The cached value of the '{@link #getCapabilities() <em>Capabilities</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilities()
	 * @generated
	 * @ordered
	 */
	protected Text capabilities;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected Text constraints;

	/**
	 * The cached value of the '{@link #getNarrative() <em>Narrative</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNarrative()
	 * @generated
	 * @ordered
	 */
	protected Text narrative;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PersonaPackage.Literals.PERSONA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAge(Integer newAge) {
		Integer oldAge = age;
		age = newAge;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__AGE, oldAge, age));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getJob() {
		return job;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJob(String newJob) {
		String oldJob = job;
		job = newJob;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__JOB, oldJob, job));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPictureURI() {
		return pictureURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPictureURI(String newPictureURI) {
		String oldPictureURI = pictureURI;
		pictureURI = newPictureURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__PICTURE_URI, oldPictureURI, pictureURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte[] getPictureData() {
		return pictureData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPictureData(byte[] newPictureData) {
		byte[] oldPictureData = pictureData;
		pictureData = newPictureData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__PICTURE_DATA, oldPictureData, pictureData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Storyboard> getStoryboards() {
		if (storyboards == null) {
			storyboards = new EObjectContainmentEList.Resolving<Storyboard>(Storyboard.class, this, PersonaPackage.PERSONA__STORYBOARDS);
		}
		return storyboards;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getIssues() {
		if (issues != null && issues.eIsProxy()) {
			InternalEObject oldIssues = (InternalEObject)issues;
			issues = (Text)eResolveProxy(oldIssues);
			if (issues != oldIssues) {
				InternalEObject newIssues = (InternalEObject)issues;
				NotificationChain msgs = oldIssues.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__ISSUES, null, null);
				if (newIssues.eInternalContainer() == null) {
					msgs = newIssues.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__ISSUES, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PersonaPackage.PERSONA__ISSUES, oldIssues, issues));
			}
		}
		return issues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetIssues() {
		return issues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIssues(Text newIssues, NotificationChain msgs) {
		Text oldIssues = issues;
		issues = newIssues;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__ISSUES, oldIssues, newIssues);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIssues(Text newIssues) {
		if (newIssues != issues) {
			NotificationChain msgs = null;
			if (issues != null)
				msgs = ((InternalEObject)issues).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__ISSUES, null, msgs);
			if (newIssues != null)
				msgs = ((InternalEObject)newIssues).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__ISSUES, null, msgs);
			msgs = basicSetIssues(newIssues, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__ISSUES, newIssues, newIssues));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getCapabilities() {
		if (capabilities != null && capabilities.eIsProxy()) {
			InternalEObject oldCapabilities = (InternalEObject)capabilities;
			capabilities = (Text)eResolveProxy(oldCapabilities);
			if (capabilities != oldCapabilities) {
				InternalEObject newCapabilities = (InternalEObject)capabilities;
				NotificationChain msgs = oldCapabilities.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__CAPABILITIES, null, null);
				if (newCapabilities.eInternalContainer() == null) {
					msgs = newCapabilities.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__CAPABILITIES, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PersonaPackage.PERSONA__CAPABILITIES, oldCapabilities, capabilities));
			}
		}
		return capabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetCapabilities() {
		return capabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCapabilities(Text newCapabilities, NotificationChain msgs) {
		Text oldCapabilities = capabilities;
		capabilities = newCapabilities;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__CAPABILITIES, oldCapabilities, newCapabilities);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapabilities(Text newCapabilities) {
		if (newCapabilities != capabilities) {
			NotificationChain msgs = null;
			if (capabilities != null)
				msgs = ((InternalEObject)capabilities).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__CAPABILITIES, null, msgs);
			if (newCapabilities != null)
				msgs = ((InternalEObject)newCapabilities).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__CAPABILITIES, null, msgs);
			msgs = basicSetCapabilities(newCapabilities, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__CAPABILITIES, newCapabilities, newCapabilities));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getConstraints() {
		if (constraints != null && constraints.eIsProxy()) {
			InternalEObject oldConstraints = (InternalEObject)constraints;
			constraints = (Text)eResolveProxy(oldConstraints);
			if (constraints != oldConstraints) {
				InternalEObject newConstraints = (InternalEObject)constraints;
				NotificationChain msgs = oldConstraints.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__CONSTRAINTS, null, null);
				if (newConstraints.eInternalContainer() == null) {
					msgs = newConstraints.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__CONSTRAINTS, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PersonaPackage.PERSONA__CONSTRAINTS, oldConstraints, constraints));
			}
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetConstraints() {
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstraints(Text newConstraints, NotificationChain msgs) {
		Text oldConstraints = constraints;
		constraints = newConstraints;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__CONSTRAINTS, oldConstraints, newConstraints);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraints(Text newConstraints) {
		if (newConstraints != constraints) {
			NotificationChain msgs = null;
			if (constraints != null)
				msgs = ((InternalEObject)constraints).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__CONSTRAINTS, null, msgs);
			if (newConstraints != null)
				msgs = ((InternalEObject)newConstraints).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__CONSTRAINTS, null, msgs);
			msgs = basicSetConstraints(newConstraints, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__CONSTRAINTS, newConstraints, newConstraints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getNarrative() {
		if (narrative != null && narrative.eIsProxy()) {
			InternalEObject oldNarrative = (InternalEObject)narrative;
			narrative = (Text)eResolveProxy(oldNarrative);
			if (narrative != oldNarrative) {
				InternalEObject newNarrative = (InternalEObject)narrative;
				NotificationChain msgs = oldNarrative.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__NARRATIVE, null, null);
				if (newNarrative.eInternalContainer() == null) {
					msgs = newNarrative.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__NARRATIVE, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PersonaPackage.PERSONA__NARRATIVE, oldNarrative, narrative));
			}
		}
		return narrative;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetNarrative() {
		return narrative;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNarrative(Text newNarrative, NotificationChain msgs) {
		Text oldNarrative = narrative;
		narrative = newNarrative;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__NARRATIVE, oldNarrative, newNarrative);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNarrative(Text newNarrative) {
		if (newNarrative != narrative) {
			NotificationChain msgs = null;
			if (narrative != null)
				msgs = ((InternalEObject)narrative).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__NARRATIVE, null, msgs);
			if (newNarrative != null)
				msgs = ((InternalEObject)newNarrative).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PersonaPackage.PERSONA__NARRATIVE, null, msgs);
			msgs = basicSetNarrative(newNarrative, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersonaPackage.PERSONA__NARRATIVE, newNarrative, newNarrative));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PersonaPackage.PERSONA__STORYBOARDS:
				return ((InternalEList<?>)getStoryboards()).basicRemove(otherEnd, msgs);
			case PersonaPackage.PERSONA__ISSUES:
				return basicSetIssues(null, msgs);
			case PersonaPackage.PERSONA__CAPABILITIES:
				return basicSetCapabilities(null, msgs);
			case PersonaPackage.PERSONA__CONSTRAINTS:
				return basicSetConstraints(null, msgs);
			case PersonaPackage.PERSONA__NARRATIVE:
				return basicSetNarrative(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PersonaPackage.PERSONA__AGE:
				return getAge();
			case PersonaPackage.PERSONA__JOB:
				return getJob();
			case PersonaPackage.PERSONA__PICTURE_URI:
				return getPictureURI();
			case PersonaPackage.PERSONA__PICTURE_DATA:
				return getPictureData();
			case PersonaPackage.PERSONA__STORYBOARDS:
				return getStoryboards();
			case PersonaPackage.PERSONA__ISSUES:
				if (resolve) return getIssues();
				return basicGetIssues();
			case PersonaPackage.PERSONA__CAPABILITIES:
				if (resolve) return getCapabilities();
				return basicGetCapabilities();
			case PersonaPackage.PERSONA__CONSTRAINTS:
				if (resolve) return getConstraints();
				return basicGetConstraints();
			case PersonaPackage.PERSONA__NARRATIVE:
				if (resolve) return getNarrative();
				return basicGetNarrative();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PersonaPackage.PERSONA__AGE:
				setAge((Integer)newValue);
				return;
			case PersonaPackage.PERSONA__JOB:
				setJob((String)newValue);
				return;
			case PersonaPackage.PERSONA__PICTURE_URI:
				setPictureURI((String)newValue);
				return;
			case PersonaPackage.PERSONA__PICTURE_DATA:
				setPictureData((byte[])newValue);
				return;
			case PersonaPackage.PERSONA__STORYBOARDS:
				getStoryboards().clear();
				getStoryboards().addAll((Collection<? extends Storyboard>)newValue);
				return;
			case PersonaPackage.PERSONA__ISSUES:
				setIssues((Text)newValue);
				return;
			case PersonaPackage.PERSONA__CAPABILITIES:
				setCapabilities((Text)newValue);
				return;
			case PersonaPackage.PERSONA__CONSTRAINTS:
				setConstraints((Text)newValue);
				return;
			case PersonaPackage.PERSONA__NARRATIVE:
				setNarrative((Text)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PersonaPackage.PERSONA__AGE:
				setAge(AGE_EDEFAULT);
				return;
			case PersonaPackage.PERSONA__JOB:
				setJob(JOB_EDEFAULT);
				return;
			case PersonaPackage.PERSONA__PICTURE_URI:
				setPictureURI(PICTURE_URI_EDEFAULT);
				return;
			case PersonaPackage.PERSONA__PICTURE_DATA:
				setPictureData(PICTURE_DATA_EDEFAULT);
				return;
			case PersonaPackage.PERSONA__STORYBOARDS:
				getStoryboards().clear();
				return;
			case PersonaPackage.PERSONA__ISSUES:
				setIssues((Text)null);
				return;
			case PersonaPackage.PERSONA__CAPABILITIES:
				setCapabilities((Text)null);
				return;
			case PersonaPackage.PERSONA__CONSTRAINTS:
				setConstraints((Text)null);
				return;
			case PersonaPackage.PERSONA__NARRATIVE:
				setNarrative((Text)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PersonaPackage.PERSONA__AGE:
				return AGE_EDEFAULT == null ? age != null : !AGE_EDEFAULT.equals(age);
			case PersonaPackage.PERSONA__JOB:
				return JOB_EDEFAULT == null ? job != null : !JOB_EDEFAULT.equals(job);
			case PersonaPackage.PERSONA__PICTURE_URI:
				return PICTURE_URI_EDEFAULT == null ? pictureURI != null : !PICTURE_URI_EDEFAULT.equals(pictureURI);
			case PersonaPackage.PERSONA__PICTURE_DATA:
				return PICTURE_DATA_EDEFAULT == null ? pictureData != null : !PICTURE_DATA_EDEFAULT.equals(pictureData);
			case PersonaPackage.PERSONA__STORYBOARDS:
				return storyboards != null && !storyboards.isEmpty();
			case PersonaPackage.PERSONA__ISSUES:
				return issues != null;
			case PersonaPackage.PERSONA__CAPABILITIES:
				return capabilities != null;
			case PersonaPackage.PERSONA__CONSTRAINTS:
				return constraints != null;
			case PersonaPackage.PERSONA__NARRATIVE:
				return narrative != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (age: ");
		result.append(age);
		result.append(", job: ");
		result.append(job);
		result.append(", pictureURI: ");
		result.append(pictureURI);
		result.append(", pictureData: ");
		result.append(pictureData);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/persona.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

//		Issue #37
//		attributesToSearch.put(PersonaPackage.Literals.PERSONA__ISSUES,
//				this.getIssues()==null ? "" : this.getIssues().toString());
		if (this.getAge() != null) {
			attributesToSearch.put(PersonaPackage.Literals.PERSONA__AGE,
					this.getAge().toString());
		}
		attributesToSearch.put(PersonaPackage.Literals.PERSONA__JOB,
				this.getJob());
//		Issue #37
//		attributesToSearch.put(PersonaPackage.Literals.PERSONA__NARRATIVE,
//				this.getNarrative()==null ? "" : this.getNarrative().toString());
		attributesToSearch.put(PersonaPackage.Literals.PERSONA__CAPABILITIES,
		this.getCapabilities()==null ? "" : this.getCapabilities().toString());
		attributesToSearch.put(PersonaPackage.Literals.PERSONA__CONSTRAINTS,
		this.getConstraints()==null ? "" : this.getConstraints().toString());

		return super.search(param, attributesToSearch);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {
		return new ElementColumnDefinition[] {
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Persona>() {

					@Override
					public Object getAttributeValue(Persona item) {
						return item.getName()==null?"":item.getName();
					}

					@Override
					public void setAttributeValue(Persona item, Object value) {
						item.setName((String) value);
					}
				})
				,
				new ElementColumnDefinition()
				.setHeaderName("Occupation")
				.setEMFLiteral(PersonaPackage.Literals.PERSONA__JOB)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Persona>() {

					@Override
					public Object getAttributeValue(Persona item) {
						return item.getJob()==null?"":item.getJob();
					}

					@Override
					public void setAttributeValue(Persona item, Object value) {
						item.setJob((String) value);
					}
				})
				,
				new ElementColumnDefinition()
				.setHeaderName("Age")
				.setEMFLiteral(PersonaPackage.Literals.PERSONA__AGE)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Persona>() {

					@Override
					public Object getAttributeValue(Persona item) {
						return item.getAge();
					}

					@Override
					public void setAttributeValue(Persona item, Object value) {
						item.setAge(Integer.parseInt(value.toString()));
					}
				})
		};
	}

} //PersonaImpl
