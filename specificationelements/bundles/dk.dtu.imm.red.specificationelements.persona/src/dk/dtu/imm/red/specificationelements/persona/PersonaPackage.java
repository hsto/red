/**
 */
package dk.dtu.imm.red.specificationelements.persona;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.persona.PersonaFactory
 * @model kind="package"
 * @generated
 */
public interface PersonaPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "persona";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.specificationelements.persona";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "persona";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PersonaPackage eINSTANCE = dk.dtu.imm.red.specificationelements.persona.impl.PersonaPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl <em>Persona</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl
	 * @see dk.dtu.imm.red.specificationelements.persona.impl.PersonaPackageImpl#getPersona()
	 * @generated
	 */
	int PERSONA = 0;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__ICON_URI = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__ICON = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__LABEL = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__NAME = SpecificationelementsPackage.SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__ELEMENT_KIND = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__DESCRIPTION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__PRIORITY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__COMMENTLIST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__TIME_CREATED = SpecificationelementsPackage.SPECIFICATION_ELEMENT__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__LAST_MODIFIED = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__LAST_MODIFIED_BY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__CREATOR = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__VERSION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__VISIBLE_ID = SpecificationelementsPackage.SPECIFICATION_ELEMENT__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__UNIQUE_ID = SpecificationelementsPackage.SPECIFICATION_ELEMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__RELATES_TO = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__RELATED_BY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__PARENT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__URI = SpecificationelementsPackage.SPECIFICATION_ELEMENT__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__WORK_PACKAGE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__CHANGE_LIST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__RESPONSIBLE_USER = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__DEADLINE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__LOCK_STATUS = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__LOCK_PASSWORD = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__ESTIMATED_COMPLEXITY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__COST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__BENEFIT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__RISK = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__LIFE_CYCLE_PHASE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__STATE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__MANAGEMENT_DECISION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__QA_ASSESSMENT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__MANAGEMENT_DISCUSSION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__DELETION_LISTENERS = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__QUANTITIES = SpecificationelementsPackage.SPECIFICATION_ELEMENT__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__CUSTOM_ATTRIBUTES = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Age</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__AGE = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Job</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__JOB = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Picture URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__PICTURE_URI = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Picture Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__PICTURE_DATA = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Storyboards</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__STORYBOARDS = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Issues</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__ISSUES = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Capabilities</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__CAPABILITIES = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__CONSTRAINTS = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Narrative</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA__NARRATIVE = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Persona</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONA_FEATURE_COUNT = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 9;


	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.persona.PersonaKind <em>Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaKind
	 * @see dk.dtu.imm.red.specificationelements.persona.impl.PersonaPackageImpl#getPersonaKind()
	 * @generated
	 */
	int PERSONA_KIND = 1;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.persona.Persona <em>Persona</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Persona</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona
	 * @generated
	 */
	EClass getPersona();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getAge <em>Age</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Age</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona#getAge()
	 * @see #getPersona()
	 * @generated
	 */
	EAttribute getPersona_Age();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getJob <em>Job</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Job</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona#getJob()
	 * @see #getPersona()
	 * @generated
	 */
	EAttribute getPersona_Job();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getPictureURI <em>Picture URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Picture URI</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona#getPictureURI()
	 * @see #getPersona()
	 * @generated
	 */
	EAttribute getPersona_PictureURI();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getPictureData <em>Picture Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Picture Data</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona#getPictureData()
	 * @see #getPersona()
	 * @generated
	 */
	EAttribute getPersona_PictureData();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getStoryboards <em>Storyboards</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Storyboards</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona#getStoryboards()
	 * @see #getPersona()
	 * @generated
	 */
	EReference getPersona_Storyboards();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getIssues <em>Issues</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Issues</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona#getIssues()
	 * @see #getPersona()
	 * @generated
	 */
	EReference getPersona_Issues();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getCapabilities <em>Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Capabilities</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona#getCapabilities()
	 * @see #getPersona()
	 * @generated
	 */
	EReference getPersona_Capabilities();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constraints</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona#getConstraints()
	 * @see #getPersona()
	 * @generated
	 */
	EReference getPersona_Constraints();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getNarrative <em>Narrative</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Narrative</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.Persona#getNarrative()
	 * @see #getPersona()
	 * @generated
	 */
	EReference getPersona_Narrative();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.specificationelements.persona.PersonaKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Kind</em>'.
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaKind
	 * @generated
	 */
	EEnum getPersonaKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PersonaFactory getPersonaFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl <em>Persona</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.persona.impl.PersonaImpl
		 * @see dk.dtu.imm.red.specificationelements.persona.impl.PersonaPackageImpl#getPersona()
		 * @generated
		 */
		EClass PERSONA = eINSTANCE.getPersona();

		/**
		 * The meta object literal for the '<em><b>Age</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONA__AGE = eINSTANCE.getPersona_Age();

		/**
		 * The meta object literal for the '<em><b>Job</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONA__JOB = eINSTANCE.getPersona_Job();

		/**
		 * The meta object literal for the '<em><b>Picture URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONA__PICTURE_URI = eINSTANCE.getPersona_PictureURI();

		/**
		 * The meta object literal for the '<em><b>Picture Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONA__PICTURE_DATA = eINSTANCE.getPersona_PictureData();

		/**
		 * The meta object literal for the '<em><b>Storyboards</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONA__STORYBOARDS = eINSTANCE.getPersona_Storyboards();

		/**
		 * The meta object literal for the '<em><b>Issues</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONA__ISSUES = eINSTANCE.getPersona_Issues();

		/**
		 * The meta object literal for the '<em><b>Capabilities</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONA__CAPABILITIES = eINSTANCE.getPersona_Capabilities();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONA__CONSTRAINTS = eINSTANCE.getPersona_Constraints();

		/**
		 * The meta object literal for the '<em><b>Narrative</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONA__NARRATIVE = eINSTANCE.getPersona_Narrative();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.persona.PersonaKind <em>Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.persona.PersonaKind
		 * @see dk.dtu.imm.red.specificationelements.persona.impl.PersonaPackageImpl#getPersonaKind()
		 * @generated
		 */
		EEnum PERSONA_KIND = eINSTANCE.getPersonaKind();

	}

} //PersonaPackage
