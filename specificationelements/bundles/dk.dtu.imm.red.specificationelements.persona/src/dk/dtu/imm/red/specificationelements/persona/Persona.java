/**
 */
package dk.dtu.imm.red.specificationelements.persona;

import dk.dtu.imm.red.core.text.Text;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Persona</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.Persona#getAge <em>Age</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.Persona#getJob <em>Job</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.Persona#getPictureURI <em>Picture URI</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.Persona#getPictureData <em>Picture Data</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.Persona#getStoryboards <em>Storyboards</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.Persona#getIssues <em>Issues</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.Persona#getCapabilities <em>Capabilities</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.Persona#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.persona.Persona#getNarrative <em>Narrative</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona()
 * @model
 * @generated
 */
public interface Persona extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Age</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Age</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Age</em>' attribute.
	 * @see #setAge(Integer)
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona_Age()
	 * @model
	 * @generated
	 */
	Integer getAge();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getAge <em>Age</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Age</em>' attribute.
	 * @see #getAge()
	 * @generated
	 */
	void setAge(Integer value);

	/**
	 * Returns the value of the '<em><b>Job</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Job</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Job</em>' attribute.
	 * @see #setJob(String)
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona_Job()
	 * @model
	 * @generated
	 */
	String getJob();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getJob <em>Job</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Job</em>' attribute.
	 * @see #getJob()
	 * @generated
	 */
	void setJob(String value);

	/**
	 * Returns the value of the '<em><b>Picture URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Picture URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Picture URI</em>' attribute.
	 * @see #setPictureURI(String)
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona_PictureURI()
	 * @model
	 * @generated
	 */
	String getPictureURI();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getPictureURI <em>Picture URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Picture URI</em>' attribute.
	 * @see #getPictureURI()
	 * @generated
	 */
	void setPictureURI(String value);

	/**
	 * Returns the value of the '<em><b>Picture Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Picture Data</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Picture Data</em>' attribute.
	 * @see #setPictureData(byte[])
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona_PictureData()
	 * @model
	 * @generated
	 */
	byte[] getPictureData();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getPictureData <em>Picture Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Picture Data</em>' attribute.
	 * @see #getPictureData()
	 * @generated
	 */
	void setPictureData(byte[] value);

	/**
	 * Returns the value of the '<em><b>Storyboards</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storyboards</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storyboards</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona_Storyboards()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<Storyboard> getStoryboards();

	/**
	 * Returns the value of the '<em><b>Issues</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Issues</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Issues</em>' containment reference.
	 * @see #setIssues(Text)
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona_Issues()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getIssues();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getIssues <em>Issues</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Issues</em>' containment reference.
	 * @see #getIssues()
	 * @generated
	 */
	void setIssues(Text value);

	/**
	 * Returns the value of the '<em><b>Capabilities</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capabilities</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capabilities</em>' containment reference.
	 * @see #setCapabilities(Text)
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona_Capabilities()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getCapabilities();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getCapabilities <em>Capabilities</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capabilities</em>' containment reference.
	 * @see #getCapabilities()
	 * @generated
	 */
	void setCapabilities(Text value);

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference.
	 * @see #setConstraints(Text)
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona_Constraints()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getConstraints();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getConstraints <em>Constraints</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraints</em>' containment reference.
	 * @see #getConstraints()
	 * @generated
	 */
	void setConstraints(Text value);

	/**
	 * Returns the value of the '<em><b>Narrative</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Narrative</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Narrative</em>' containment reference.
	 * @see #setNarrative(Text)
	 * @see dk.dtu.imm.red.specificationelements.persona.PersonaPackage#getPersona_Narrative()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getNarrative();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.persona.Persona#getNarrative <em>Narrative</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Narrative</em>' containment reference.
	 * @see #getNarrative()
	 * @generated
	 */
	void setNarrative(Text value);

} // Persona
