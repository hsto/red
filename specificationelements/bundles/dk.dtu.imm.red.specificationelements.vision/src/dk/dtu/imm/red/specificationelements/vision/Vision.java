/**
 */
package dk.dtu.imm.red.specificationelements.vision;

import dk.dtu.imm.red.core.text.Text;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vision</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.Vision#getVision <em>Vision</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.Vision#getExpectedValueOutcome <em>Expected Value Outcome</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.Vision#getOpportunities <em>Opportunities</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.Vision#getChallenges <em>Challenges</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.Vision#getSponsors <em>Sponsors</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.Vision#getAdversaries <em>Adversaries</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.Vision#getGroupValuesAndCom <em>Group Values And Com</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.Vision#getBeliefs <em>Beliefs</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.vision.VisionPackage#getVision()
 * @model
 * @generated
 */
public interface Vision extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Vision</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vision</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vision</em>' containment reference.
	 * @see #setVision(Text)
	 * @see dk.dtu.imm.red.specificationelements.vision.VisionPackage#getVision_Vision()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getVision();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.vision.Vision#getVision <em>Vision</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vision</em>' containment reference.
	 * @see #getVision()
	 * @generated
	 */
	void setVision(Text value);

	/**
	 * Returns the value of the '<em><b>Expected Value Outcome</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expected Value Outcome</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expected Value Outcome</em>' containment reference.
	 * @see #setExpectedValueOutcome(Text)
	 * @see dk.dtu.imm.red.specificationelements.vision.VisionPackage#getVision_ExpectedValueOutcome()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getExpectedValueOutcome();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.vision.Vision#getExpectedValueOutcome <em>Expected Value Outcome</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expected Value Outcome</em>' containment reference.
	 * @see #getExpectedValueOutcome()
	 * @generated
	 */
	void setExpectedValueOutcome(Text value);

	/**
	 * Returns the value of the '<em><b>Opportunities</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opportunities</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opportunities</em>' containment reference.
	 * @see #setOpportunities(Text)
	 * @see dk.dtu.imm.red.specificationelements.vision.VisionPackage#getVision_Opportunities()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getOpportunities();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.vision.Vision#getOpportunities <em>Opportunities</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opportunities</em>' containment reference.
	 * @see #getOpportunities()
	 * @generated
	 */
	void setOpportunities(Text value);

	/**
	 * Returns the value of the '<em><b>Challenges</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Challenges</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Challenges</em>' containment reference.
	 * @see #setChallenges(Text)
	 * @see dk.dtu.imm.red.specificationelements.vision.VisionPackage#getVision_Challenges()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getChallenges();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.vision.Vision#getChallenges <em>Challenges</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Challenges</em>' containment reference.
	 * @see #getChallenges()
	 * @generated
	 */
	void setChallenges(Text value);

	/**
	 * Returns the value of the '<em><b>Sponsors</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sponsors</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sponsors</em>' containment reference.
	 * @see #setSponsors(Text)
	 * @see dk.dtu.imm.red.specificationelements.vision.VisionPackage#getVision_Sponsors()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getSponsors();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.vision.Vision#getSponsors <em>Sponsors</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sponsors</em>' containment reference.
	 * @see #getSponsors()
	 * @generated
	 */
	void setSponsors(Text value);

	/**
	 * Returns the value of the '<em><b>Adversaries</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adversaries</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adversaries</em>' containment reference.
	 * @see #setAdversaries(Text)
	 * @see dk.dtu.imm.red.specificationelements.vision.VisionPackage#getVision_Adversaries()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getAdversaries();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.vision.Vision#getAdversaries <em>Adversaries</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Adversaries</em>' containment reference.
	 * @see #getAdversaries()
	 * @generated
	 */
	void setAdversaries(Text value);

	/**
	 * Returns the value of the '<em><b>Group Values And Com</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Values And Com</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Values And Com</em>' containment reference.
	 * @see #setGroupValuesAndCom(Text)
	 * @see dk.dtu.imm.red.specificationelements.vision.VisionPackage#getVision_GroupValuesAndCom()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getGroupValuesAndCom();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.vision.Vision#getGroupValuesAndCom <em>Group Values And Com</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Values And Com</em>' containment reference.
	 * @see #getGroupValuesAndCom()
	 * @generated
	 */
	void setGroupValuesAndCom(Text value);

	/**
	 * Returns the value of the '<em><b>Beliefs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Beliefs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Beliefs</em>' containment reference.
	 * @see #setBeliefs(Text)
	 * @see dk.dtu.imm.red.specificationelements.vision.VisionPackage#getVision_Beliefs()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getBeliefs();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.vision.Vision#getBeliefs <em>Beliefs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Beliefs</em>' containment reference.
	 * @see #getBeliefs()
	 * @generated
	 */
	void setBeliefs(Text value);

} // Vision
