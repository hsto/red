/**
 */
package dk.dtu.imm.red.specificationelements.vision.impl;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.specificationelements.vision.VisionPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vision</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.impl.VisionImpl#getVision <em>Vision</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.impl.VisionImpl#getExpectedValueOutcome <em>Expected Value Outcome</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.impl.VisionImpl#getOpportunities <em>Opportunities</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.impl.VisionImpl#getChallenges <em>Challenges</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.impl.VisionImpl#getSponsors <em>Sponsors</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.impl.VisionImpl#getAdversaries <em>Adversaries</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.impl.VisionImpl#getGroupValuesAndCom <em>Group Values And Com</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.vision.impl.VisionImpl#getBeliefs <em>Beliefs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VisionImpl extends SpecificationElementImpl implements Vision {
	/**
	 * The cached value of the '{@link #getVision() <em>Vision</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVision()
	 * @generated
	 * @ordered
	 */
	protected Text vision;

	/**
	 * The cached value of the '{@link #getExpectedValueOutcome() <em>Expected Value Outcome</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedValueOutcome()
	 * @generated
	 * @ordered
	 */
	protected Text expectedValueOutcome;
	/**
	 * The cached value of the '{@link #getOpportunities() <em>Opportunities</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpportunities()
	 * @generated
	 * @ordered
	 */
	protected Text opportunities;
	/**
	 * The cached value of the '{@link #getChallenges() <em>Challenges</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChallenges()
	 * @generated
	 * @ordered
	 */
	protected Text challenges;
	/**
	 * The cached value of the '{@link #getSponsors() <em>Sponsors</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSponsors()
	 * @generated
	 * @ordered
	 */
	protected Text sponsors;
	/**
	 * The cached value of the '{@link #getAdversaries() <em>Adversaries</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdversaries()
	 * @generated
	 * @ordered
	 */
	protected Text adversaries;
	/**
	 * The cached value of the '{@link #getGroupValuesAndCom() <em>Group Values And Com</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupValuesAndCom()
	 * @generated
	 * @ordered
	 */
	protected Text groupValuesAndCom;
	/**
	 * The cached value of the '{@link #getBeliefs() <em>Beliefs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBeliefs()
	 * @generated
	 * @ordered
	 */
	protected Text beliefs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisionPackage.Literals.VISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getVision() {
		if (vision != null && vision.eIsProxy()) {
			InternalEObject oldVision = (InternalEObject)vision;
			vision = (Text)eResolveProxy(oldVision);
			if (vision != oldVision) {
				InternalEObject newVision = (InternalEObject)vision;
				NotificationChain msgs = oldVision.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__VISION, null, null);
				if (newVision.eInternalContainer() == null) {
					msgs = newVision.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__VISION, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisionPackage.VISION__VISION, oldVision, vision));
			}
		}
		return vision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetVision() {
		return vision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVision(Text newVision, NotificationChain msgs) {
		Text oldVision = vision;
		vision = newVision;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__VISION, oldVision, newVision);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVision(Text newVision) {
		if (newVision != vision) {
			NotificationChain msgs = null;
			if (vision != null)
				msgs = ((InternalEObject)vision).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__VISION, null, msgs);
			if (newVision != null)
				msgs = ((InternalEObject)newVision).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__VISION, null, msgs);
			msgs = basicSetVision(newVision, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__VISION, newVision, newVision));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getExpectedValueOutcome() {
		if (expectedValueOutcome != null && expectedValueOutcome.eIsProxy()) {
			InternalEObject oldExpectedValueOutcome = (InternalEObject)expectedValueOutcome;
			expectedValueOutcome = (Text)eResolveProxy(oldExpectedValueOutcome);
			if (expectedValueOutcome != oldExpectedValueOutcome) {
				InternalEObject newExpectedValueOutcome = (InternalEObject)expectedValueOutcome;
				NotificationChain msgs = oldExpectedValueOutcome.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__EXPECTED_VALUE_OUTCOME, null, null);
				if (newExpectedValueOutcome.eInternalContainer() == null) {
					msgs = newExpectedValueOutcome.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__EXPECTED_VALUE_OUTCOME, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisionPackage.VISION__EXPECTED_VALUE_OUTCOME, oldExpectedValueOutcome, expectedValueOutcome));
			}
		}
		return expectedValueOutcome;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetExpectedValueOutcome() {
		return expectedValueOutcome;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpectedValueOutcome(Text newExpectedValueOutcome, NotificationChain msgs) {
		Text oldExpectedValueOutcome = expectedValueOutcome;
		expectedValueOutcome = newExpectedValueOutcome;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__EXPECTED_VALUE_OUTCOME, oldExpectedValueOutcome, newExpectedValueOutcome);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpectedValueOutcome(Text newExpectedValueOutcome) {
		if (newExpectedValueOutcome != expectedValueOutcome) {
			NotificationChain msgs = null;
			if (expectedValueOutcome != null)
				msgs = ((InternalEObject)expectedValueOutcome).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__EXPECTED_VALUE_OUTCOME, null, msgs);
			if (newExpectedValueOutcome != null)
				msgs = ((InternalEObject)newExpectedValueOutcome).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__EXPECTED_VALUE_OUTCOME, null, msgs);
			msgs = basicSetExpectedValueOutcome(newExpectedValueOutcome, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__EXPECTED_VALUE_OUTCOME, newExpectedValueOutcome, newExpectedValueOutcome));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getOpportunities() {
		if (opportunities != null && opportunities.eIsProxy()) {
			InternalEObject oldOpportunities = (InternalEObject)opportunities;
			opportunities = (Text)eResolveProxy(oldOpportunities);
			if (opportunities != oldOpportunities) {
				InternalEObject newOpportunities = (InternalEObject)opportunities;
				NotificationChain msgs = oldOpportunities.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__OPPORTUNITIES, null, null);
				if (newOpportunities.eInternalContainer() == null) {
					msgs = newOpportunities.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__OPPORTUNITIES, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisionPackage.VISION__OPPORTUNITIES, oldOpportunities, opportunities));
			}
		}
		return opportunities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetOpportunities() {
		return opportunities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOpportunities(Text newOpportunities, NotificationChain msgs) {
		Text oldOpportunities = opportunities;
		opportunities = newOpportunities;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__OPPORTUNITIES, oldOpportunities, newOpportunities);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpportunities(Text newOpportunities) {
		if (newOpportunities != opportunities) {
			NotificationChain msgs = null;
			if (opportunities != null)
				msgs = ((InternalEObject)opportunities).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__OPPORTUNITIES, null, msgs);
			if (newOpportunities != null)
				msgs = ((InternalEObject)newOpportunities).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__OPPORTUNITIES, null, msgs);
			msgs = basicSetOpportunities(newOpportunities, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__OPPORTUNITIES, newOpportunities, newOpportunities));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getChallenges() {
		if (challenges != null && challenges.eIsProxy()) {
			InternalEObject oldChallenges = (InternalEObject)challenges;
			challenges = (Text)eResolveProxy(oldChallenges);
			if (challenges != oldChallenges) {
				InternalEObject newChallenges = (InternalEObject)challenges;
				NotificationChain msgs = oldChallenges.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__CHALLENGES, null, null);
				if (newChallenges.eInternalContainer() == null) {
					msgs = newChallenges.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__CHALLENGES, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisionPackage.VISION__CHALLENGES, oldChallenges, challenges));
			}
		}
		return challenges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetChallenges() {
		return challenges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChallenges(Text newChallenges, NotificationChain msgs) {
		Text oldChallenges = challenges;
		challenges = newChallenges;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__CHALLENGES, oldChallenges, newChallenges);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChallenges(Text newChallenges) {
		if (newChallenges != challenges) {
			NotificationChain msgs = null;
			if (challenges != null)
				msgs = ((InternalEObject)challenges).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__CHALLENGES, null, msgs);
			if (newChallenges != null)
				msgs = ((InternalEObject)newChallenges).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__CHALLENGES, null, msgs);
			msgs = basicSetChallenges(newChallenges, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__CHALLENGES, newChallenges, newChallenges));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getSponsors() {
		if (sponsors != null && sponsors.eIsProxy()) {
			InternalEObject oldSponsors = (InternalEObject)sponsors;
			sponsors = (Text)eResolveProxy(oldSponsors);
			if (sponsors != oldSponsors) {
				InternalEObject newSponsors = (InternalEObject)sponsors;
				NotificationChain msgs = oldSponsors.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__SPONSORS, null, null);
				if (newSponsors.eInternalContainer() == null) {
					msgs = newSponsors.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__SPONSORS, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisionPackage.VISION__SPONSORS, oldSponsors, sponsors));
			}
		}
		return sponsors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetSponsors() {
		return sponsors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSponsors(Text newSponsors, NotificationChain msgs) {
		Text oldSponsors = sponsors;
		sponsors = newSponsors;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__SPONSORS, oldSponsors, newSponsors);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSponsors(Text newSponsors) {
		if (newSponsors != sponsors) {
			NotificationChain msgs = null;
			if (sponsors != null)
				msgs = ((InternalEObject)sponsors).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__SPONSORS, null, msgs);
			if (newSponsors != null)
				msgs = ((InternalEObject)newSponsors).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__SPONSORS, null, msgs);
			msgs = basicSetSponsors(newSponsors, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__SPONSORS, newSponsors, newSponsors));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getAdversaries() {
		if (adversaries != null && adversaries.eIsProxy()) {
			InternalEObject oldAdversaries = (InternalEObject)adversaries;
			adversaries = (Text)eResolveProxy(oldAdversaries);
			if (adversaries != oldAdversaries) {
				InternalEObject newAdversaries = (InternalEObject)adversaries;
				NotificationChain msgs = oldAdversaries.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__ADVERSARIES, null, null);
				if (newAdversaries.eInternalContainer() == null) {
					msgs = newAdversaries.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__ADVERSARIES, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisionPackage.VISION__ADVERSARIES, oldAdversaries, adversaries));
			}
		}
		return adversaries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetAdversaries() {
		return adversaries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdversaries(Text newAdversaries, NotificationChain msgs) {
		Text oldAdversaries = adversaries;
		adversaries = newAdversaries;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__ADVERSARIES, oldAdversaries, newAdversaries);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdversaries(Text newAdversaries) {
		if (newAdversaries != adversaries) {
			NotificationChain msgs = null;
			if (adversaries != null)
				msgs = ((InternalEObject)adversaries).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__ADVERSARIES, null, msgs);
			if (newAdversaries != null)
				msgs = ((InternalEObject)newAdversaries).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__ADVERSARIES, null, msgs);
			msgs = basicSetAdversaries(newAdversaries, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__ADVERSARIES, newAdversaries, newAdversaries));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getGroupValuesAndCom() {
		if (groupValuesAndCom != null && groupValuesAndCom.eIsProxy()) {
			InternalEObject oldGroupValuesAndCom = (InternalEObject)groupValuesAndCom;
			groupValuesAndCom = (Text)eResolveProxy(oldGroupValuesAndCom);
			if (groupValuesAndCom != oldGroupValuesAndCom) {
				InternalEObject newGroupValuesAndCom = (InternalEObject)groupValuesAndCom;
				NotificationChain msgs = oldGroupValuesAndCom.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__GROUP_VALUES_AND_COM, null, null);
				if (newGroupValuesAndCom.eInternalContainer() == null) {
					msgs = newGroupValuesAndCom.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__GROUP_VALUES_AND_COM, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisionPackage.VISION__GROUP_VALUES_AND_COM, oldGroupValuesAndCom, groupValuesAndCom));
			}
		}
		return groupValuesAndCom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetGroupValuesAndCom() {
		return groupValuesAndCom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGroupValuesAndCom(Text newGroupValuesAndCom, NotificationChain msgs) {
		Text oldGroupValuesAndCom = groupValuesAndCom;
		groupValuesAndCom = newGroupValuesAndCom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__GROUP_VALUES_AND_COM, oldGroupValuesAndCom, newGroupValuesAndCom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroupValuesAndCom(Text newGroupValuesAndCom) {
		if (newGroupValuesAndCom != groupValuesAndCom) {
			NotificationChain msgs = null;
			if (groupValuesAndCom != null)
				msgs = ((InternalEObject)groupValuesAndCom).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__GROUP_VALUES_AND_COM, null, msgs);
			if (newGroupValuesAndCom != null)
				msgs = ((InternalEObject)newGroupValuesAndCom).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__GROUP_VALUES_AND_COM, null, msgs);
			msgs = basicSetGroupValuesAndCom(newGroupValuesAndCom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__GROUP_VALUES_AND_COM, newGroupValuesAndCom, newGroupValuesAndCom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getBeliefs() {
		if (beliefs != null && beliefs.eIsProxy()) {
			InternalEObject oldBeliefs = (InternalEObject)beliefs;
			beliefs = (Text)eResolveProxy(oldBeliefs);
			if (beliefs != oldBeliefs) {
				InternalEObject newBeliefs = (InternalEObject)beliefs;
				NotificationChain msgs = oldBeliefs.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__BELIEFS, null, null);
				if (newBeliefs.eInternalContainer() == null) {
					msgs = newBeliefs.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__BELIEFS, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisionPackage.VISION__BELIEFS, oldBeliefs, beliefs));
			}
		}
		return beliefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetBeliefs() {
		return beliefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBeliefs(Text newBeliefs, NotificationChain msgs) {
		Text oldBeliefs = beliefs;
		beliefs = newBeliefs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__BELIEFS, oldBeliefs, newBeliefs);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBeliefs(Text newBeliefs) {
		if (newBeliefs != beliefs) {
			NotificationChain msgs = null;
			if (beliefs != null)
				msgs = ((InternalEObject)beliefs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__BELIEFS, null, msgs);
			if (newBeliefs != null)
				msgs = ((InternalEObject)newBeliefs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisionPackage.VISION__BELIEFS, null, msgs);
			msgs = basicSetBeliefs(newBeliefs, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisionPackage.VISION__BELIEFS, newBeliefs, newBeliefs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VisionPackage.VISION__VISION:
				return basicSetVision(null, msgs);
			case VisionPackage.VISION__EXPECTED_VALUE_OUTCOME:
				return basicSetExpectedValueOutcome(null, msgs);
			case VisionPackage.VISION__OPPORTUNITIES:
				return basicSetOpportunities(null, msgs);
			case VisionPackage.VISION__CHALLENGES:
				return basicSetChallenges(null, msgs);
			case VisionPackage.VISION__SPONSORS:
				return basicSetSponsors(null, msgs);
			case VisionPackage.VISION__ADVERSARIES:
				return basicSetAdversaries(null, msgs);
			case VisionPackage.VISION__GROUP_VALUES_AND_COM:
				return basicSetGroupValuesAndCom(null, msgs);
			case VisionPackage.VISION__BELIEFS:
				return basicSetBeliefs(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisionPackage.VISION__VISION:
				if (resolve) return getVision();
				return basicGetVision();
			case VisionPackage.VISION__EXPECTED_VALUE_OUTCOME:
				if (resolve) return getExpectedValueOutcome();
				return basicGetExpectedValueOutcome();
			case VisionPackage.VISION__OPPORTUNITIES:
				if (resolve) return getOpportunities();
				return basicGetOpportunities();
			case VisionPackage.VISION__CHALLENGES:
				if (resolve) return getChallenges();
				return basicGetChallenges();
			case VisionPackage.VISION__SPONSORS:
				if (resolve) return getSponsors();
				return basicGetSponsors();
			case VisionPackage.VISION__ADVERSARIES:
				if (resolve) return getAdversaries();
				return basicGetAdversaries();
			case VisionPackage.VISION__GROUP_VALUES_AND_COM:
				if (resolve) return getGroupValuesAndCom();
				return basicGetGroupValuesAndCom();
			case VisionPackage.VISION__BELIEFS:
				if (resolve) return getBeliefs();
				return basicGetBeliefs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisionPackage.VISION__VISION:
				setVision((Text)newValue);
				return;
			case VisionPackage.VISION__EXPECTED_VALUE_OUTCOME:
				setExpectedValueOutcome((Text)newValue);
				return;
			case VisionPackage.VISION__OPPORTUNITIES:
				setOpportunities((Text)newValue);
				return;
			case VisionPackage.VISION__CHALLENGES:
				setChallenges((Text)newValue);
				return;
			case VisionPackage.VISION__SPONSORS:
				setSponsors((Text)newValue);
				return;
			case VisionPackage.VISION__ADVERSARIES:
				setAdversaries((Text)newValue);
				return;
			case VisionPackage.VISION__GROUP_VALUES_AND_COM:
				setGroupValuesAndCom((Text)newValue);
				return;
			case VisionPackage.VISION__BELIEFS:
				setBeliefs((Text)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisionPackage.VISION__VISION:
				setVision((Text)null);
				return;
			case VisionPackage.VISION__EXPECTED_VALUE_OUTCOME:
				setExpectedValueOutcome((Text)null);
				return;
			case VisionPackage.VISION__OPPORTUNITIES:
				setOpportunities((Text)null);
				return;
			case VisionPackage.VISION__CHALLENGES:
				setChallenges((Text)null);
				return;
			case VisionPackage.VISION__SPONSORS:
				setSponsors((Text)null);
				return;
			case VisionPackage.VISION__ADVERSARIES:
				setAdversaries((Text)null);
				return;
			case VisionPackage.VISION__GROUP_VALUES_AND_COM:
				setGroupValuesAndCom((Text)null);
				return;
			case VisionPackage.VISION__BELIEFS:
				setBeliefs((Text)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisionPackage.VISION__VISION:
				return vision != null;
			case VisionPackage.VISION__EXPECTED_VALUE_OUTCOME:
				return expectedValueOutcome != null;
			case VisionPackage.VISION__OPPORTUNITIES:
				return opportunities != null;
			case VisionPackage.VISION__CHALLENGES:
				return challenges != null;
			case VisionPackage.VISION__SPONSORS:
				return sponsors != null;
			case VisionPackage.VISION__ADVERSARIES:
				return adversaries != null;
			case VisionPackage.VISION__GROUP_VALUES_AND_COM:
				return groupValuesAndCom != null;
			case VisionPackage.VISION__BELIEFS:
				return beliefs != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/vision.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		attributesToSearch.put(VisionPackage.Literals.VISION__VISION,
				this.getVision()== null ? "" : this.getVision().toString());

		return super.search(param, attributesToSearch);

	}

	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {

		return new ElementColumnDefinition[] {
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Vision>() {

					@Override
					public Object getAttributeValue(Vision item) {
						return item.getName()==null?"":item.getName();
					}

					@Override
					public void setAttributeValue(Vision item, Object value) {
						 item.setName((String) value);
					}
				})
		};
	}

} //VisionImpl
