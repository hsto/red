/**
 */
package dk.dtu.imm.red.specificationelements.usecase.impl;

import dk.dtu.imm.red.core.properties.Kind;
import dk.dtu.imm.red.core.properties.PropertiesFactory;
import dk.dtu.imm.red.specificationelements.usecase.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UsecaseFactoryImpl extends EFactoryImpl implements UsecaseFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UsecaseFactory init() {
		try {
			UsecaseFactory theUsecaseFactory = (UsecaseFactory)EPackage.Registry.INSTANCE.getEFactory(UsecasePackage.eNS_URI);
			if (theUsecaseFactory != null) {
				return theUsecaseFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UsecaseFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsecaseFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UsecasePackage.USE_CASE: return createUseCase();
			case UsecasePackage.INPUT_OUTPUT: return createInputOutput();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case UsecasePackage.USE_CASE_KIND:
				return createUseCaseKindFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case UsecasePackage.USE_CASE_KIND:
				return convertUseCaseKindToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputOutput createInputOutput() {
		InputOutputImpl inputOutput = new InputOutputImpl();
		return inputOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCaseKind createUseCaseKindFromString(EDataType eDataType, String initialValue) {
		UseCaseKind result = UseCaseKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUseCaseKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsecasePackage getUsecasePackage() {
		return (UsecasePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UsecasePackage getPackage() {
		return UsecasePackage.eINSTANCE;
	}

	 
	/**
	 * @generated NOT
	 */
	public UseCase createUseCase() {
		UseCaseImpl usecase = new UseCaseImpl();
		
		//Set default values
		if(usecase.getBenefit() == null ) usecase.setBenefit(PropertiesFactory.eINSTANCE.createProperty());
		if(usecase.getDuration() == null ) usecase.setDuration(PropertiesFactory.eINSTANCE.createProperty());
		if(usecase.getIncidence() == null ) usecase.setIncidence(PropertiesFactory.eINSTANCE.createProperty());
		if(usecase.getCost() == null ) usecase.setCost(PropertiesFactory.eINSTANCE.createProperty());
		usecase.getIncidence().setName("Incidence");
		usecase.getIncidence().setKind(Kind.MEASUREMENT);
		
		usecase.getDuration().setName("Duration");
		usecase.getDuration().setKind(Kind.MEASUREMENT);
		
		usecase.getCost().setName("Cost");
		usecase.getCost().setKind(Kind.VALUE);
		
		usecase.getBenefit().setName("Benefit");
		usecase.getBenefit().setKind(Kind.VALUE); 
		
		
		
		return usecase;
	}

} //UsecaseFactoryImpl
