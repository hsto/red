/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.Operator#getChildElements <em>Child Elements</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getOperator()
 * @model
 * @generated
 */
public interface Operator extends OperatorComponent {
	/**
	 * Returns the value of the '<em><b>Child Elements</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Elements</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getOperator_ChildElements()
	 * @model containment="true"
	 * @generated
	 */
	EList<UseCaseScenarioElement> getChildElements();

} // Operator
