/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario.impl;

import dk.dtu.imm.red.specificationelements.usecasescenario.Action;
import dk.dtu.imm.red.specificationelements.usecasescenario.ActionType;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.ActionImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.ActionImpl#getPictureData <em>Picture Data</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.ActionImpl#getPictureURI <em>Picture URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActionImpl extends UseCaseScenarioElementImpl implements Action {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final ActionType TYPE_EDEFAULT = ActionType.ACTION;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ActionType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPictureData() <em>Picture Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPictureData()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] PICTURE_DATA_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPictureData() <em>Picture Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPictureData()
	 * @generated
	 * @ordered
	 */
	protected byte[] pictureData = PICTURE_DATA_EDEFAULT;

	/**
	 * The default value of the '{@link #getPictureURI() <em>Picture URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPictureURI()
	 * @generated
	 * @ordered
	 */
	protected static final String PICTURE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPictureURI() <em>Picture URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPictureURI()
	 * @generated
	 * @ordered
	 */
	protected String pictureURI = PICTURE_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasescenarioPackage.Literals.ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(ActionType newType) {
		ActionType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.ACTION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte[] getPictureData() {
		return pictureData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPictureData(byte[] newPictureData) {
		byte[] oldPictureData = pictureData;
		pictureData = newPictureData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.ACTION__PICTURE_DATA, oldPictureData, pictureData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPictureURI() {
		return pictureURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPictureURI(String newPictureURI) {
		String oldPictureURI = pictureURI;
		pictureURI = newPictureURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.ACTION__PICTURE_URI, oldPictureURI, pictureURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasescenarioPackage.ACTION__TYPE:
				return getType();
			case UsecasescenarioPackage.ACTION__PICTURE_DATA:
				return getPictureData();
			case UsecasescenarioPackage.ACTION__PICTURE_URI:
				return getPictureURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasescenarioPackage.ACTION__TYPE:
				setType((ActionType)newValue);
				return;
			case UsecasescenarioPackage.ACTION__PICTURE_DATA:
				setPictureData((byte[])newValue);
				return;
			case UsecasescenarioPackage.ACTION__PICTURE_URI:
				setPictureURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.ACTION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case UsecasescenarioPackage.ACTION__PICTURE_DATA:
				setPictureData(PICTURE_DATA_EDEFAULT);
				return;
			case UsecasescenarioPackage.ACTION__PICTURE_URI:
				setPictureURI(PICTURE_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.ACTION__TYPE:
				return type != TYPE_EDEFAULT;
			case UsecasescenarioPackage.ACTION__PICTURE_DATA:
				return PICTURE_DATA_EDEFAULT == null ? pictureData != null : !PICTURE_DATA_EDEFAULT.equals(pictureData);
			case UsecasescenarioPackage.ACTION__PICTURE_URI:
				return PICTURE_URI_EDEFAULT == null ? pictureURI != null : !PICTURE_URI_EDEFAULT.equals(pictureURI);
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		 return getType() + " - " + getName();
	} 

} //ActionImpl
