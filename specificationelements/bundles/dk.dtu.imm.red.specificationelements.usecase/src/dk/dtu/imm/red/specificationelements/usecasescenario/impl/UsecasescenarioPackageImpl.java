/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario.impl;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import dk.dtu.imm.red.specificationelements.actor.ActorPackage;

import dk.dtu.imm.red.specificationelements.actor.impl.ActorPackageImpl;
import dk.dtu.imm.red.specificationelements.usecase.UsecasePackage;

import dk.dtu.imm.red.specificationelements.usecase.impl.UsecasePackageImpl;

import dk.dtu.imm.red.specificationelements.usecasescenario.Action;
import dk.dtu.imm.red.specificationelements.usecasescenario.ActionType;
import dk.dtu.imm.red.specificationelements.usecasescenario.Operator;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType;
import dk.dtu.imm.red.specificationelements.usecasescenario.ScenarioReference;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseReference;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioFactory;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UsecasescenarioPackageImpl extends EPackageImpl implements UsecasescenarioPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass useCaseScenarioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass useCaseScenarioElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operatorComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scenarioReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass useCaseReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum actionTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operatorTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UsecasescenarioPackageImpl() {
		super(eNS_URI, UsecasescenarioFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UsecasescenarioPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UsecasescenarioPackage init() {
		if (isInited) return (UsecasescenarioPackage)EPackage.Registry.INSTANCE.getEPackage(UsecasescenarioPackage.eNS_URI);

		// Obtain or create and register package
		UsecasescenarioPackageImpl theUsecasescenarioPackage = (UsecasescenarioPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UsecasescenarioPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UsecasescenarioPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		UsecasePackageImpl theUsecasePackage = (UsecasePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) instanceof UsecasePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) : UsecasePackage.eINSTANCE);
		ActorPackageImpl theActorPackage = (ActorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ActorPackage.eNS_URI) instanceof ActorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ActorPackage.eNS_URI) : ActorPackage.eINSTANCE);

		// Create package meta-data objects
		theUsecasescenarioPackage.createPackageContents();
		theUsecasePackage.createPackageContents();
		theActorPackage.createPackageContents();

		// Initialize created meta-data
		theUsecasescenarioPackage.initializePackageContents();
		theUsecasePackage.initializePackageContents();
		theActorPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUsecasescenarioPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UsecasescenarioPackage.eNS_URI, theUsecasescenarioPackage);
		return theUsecasescenarioPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUseCaseScenario() {
		return useCaseScenarioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseCaseScenario_StartOperator() {
		return (EReference)useCaseScenarioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUseCaseScenario_Id() {
		return (EAttribute)useCaseScenarioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUseCaseScenarioElement() {
		return useCaseScenarioElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUseCaseScenarioElement_Description() {
		return (EAttribute)useCaseScenarioElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseCaseScenarioElement_Parent() {
		return (EReference)useCaseScenarioElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUseCaseScenarioElement_UniqueID() {
		return (EAttribute)useCaseScenarioElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUseCaseScenarioElement_Name() {
		return (EAttribute)useCaseScenarioElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperatorComponent() {
		return operatorComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperatorComponent_Type() {
		return (EAttribute)operatorComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperatorComponent_Guard() {
		return (EAttribute)operatorComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAction() {
		return actionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAction_Type() {
		return (EAttribute)actionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAction_PictureData() {
		return (EAttribute)actionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAction_PictureURI() {
		return (EAttribute)actionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperator() {
		return operatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperator_ChildElements() {
		return (EReference)operatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScenarioReference() {
		return scenarioReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenarioReference_Scenario() {
		return (EReference)scenarioReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUseCaseReference() {
		return useCaseReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseCaseReference_UseCase() {
		return (EReference)useCaseReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getActionType() {
		return actionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperatorType() {
		return operatorTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsecasescenarioFactory getUsecasescenarioFactory() {
		return (UsecasescenarioFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		useCaseScenarioEClass = createEClass(USE_CASE_SCENARIO);
		createEReference(useCaseScenarioEClass, USE_CASE_SCENARIO__START_OPERATOR);
		createEAttribute(useCaseScenarioEClass, USE_CASE_SCENARIO__ID);

		useCaseScenarioElementEClass = createEClass(USE_CASE_SCENARIO_ELEMENT);
		createEAttribute(useCaseScenarioElementEClass, USE_CASE_SCENARIO_ELEMENT__DESCRIPTION);
		createEReference(useCaseScenarioElementEClass, USE_CASE_SCENARIO_ELEMENT__PARENT);
		createEAttribute(useCaseScenarioElementEClass, USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID);
		createEAttribute(useCaseScenarioElementEClass, USE_CASE_SCENARIO_ELEMENT__NAME);

		operatorComponentEClass = createEClass(OPERATOR_COMPONENT);
		createEAttribute(operatorComponentEClass, OPERATOR_COMPONENT__TYPE);
		createEAttribute(operatorComponentEClass, OPERATOR_COMPONENT__GUARD);

		actionEClass = createEClass(ACTION);
		createEAttribute(actionEClass, ACTION__TYPE);
		createEAttribute(actionEClass, ACTION__PICTURE_DATA);
		createEAttribute(actionEClass, ACTION__PICTURE_URI);

		operatorEClass = createEClass(OPERATOR);
		createEReference(operatorEClass, OPERATOR__CHILD_ELEMENTS);

		scenarioReferenceEClass = createEClass(SCENARIO_REFERENCE);
		createEReference(scenarioReferenceEClass, SCENARIO_REFERENCE__SCENARIO);

		useCaseReferenceEClass = createEClass(USE_CASE_REFERENCE);
		createEReference(useCaseReferenceEClass, USE_CASE_REFERENCE__USE_CASE);

		// Create enums
		actionTypeEEnum = createEEnum(ACTION_TYPE);
		operatorTypeEEnum = createEEnum(OPERATOR_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SpecificationelementsPackage theSpecificationelementsPackage = (SpecificationelementsPackage)EPackage.Registry.INSTANCE.getEPackage(SpecificationelementsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		UsecasePackage theUsecasePackage = (UsecasePackage)EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		useCaseScenarioEClass.getESuperTypes().add(theSpecificationelementsPackage.getSpecificationElement());
		operatorComponentEClass.getESuperTypes().add(this.getUseCaseScenarioElement());
		actionEClass.getESuperTypes().add(this.getUseCaseScenarioElement());
		operatorEClass.getESuperTypes().add(this.getOperatorComponent());
		scenarioReferenceEClass.getESuperTypes().add(this.getOperatorComponent());
		useCaseReferenceEClass.getESuperTypes().add(this.getScenarioReference());

		// Initialize classes and features; add operations and parameters
		initEClass(useCaseScenarioEClass, UseCaseScenario.class, "UseCaseScenario", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUseCaseScenario_StartOperator(), this.getOperatorComponent(), null, "startOperator", null, 0, 1, UseCaseScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseCaseScenario_Id(), theEcorePackage.getEString(), "id", null, 0, 1, UseCaseScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(useCaseScenarioElementEClass, UseCaseScenarioElement.class, "UseCaseScenarioElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUseCaseScenarioElement_Description(), theEcorePackage.getEString(), "description", null, 0, 1, UseCaseScenarioElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCaseScenarioElement_Parent(), this.getOperatorComponent(), null, "parent", null, 0, 1, UseCaseScenarioElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseCaseScenarioElement_UniqueID(), theEcorePackage.getEString(), "uniqueID", null, 0, 1, UseCaseScenarioElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUseCaseScenarioElement_Name(), theEcorePackage.getEString(), "name", null, 0, 1, UseCaseScenarioElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operatorComponentEClass, OperatorComponent.class, "OperatorComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOperatorComponent_Type(), this.getOperatorType(), "type", null, 0, 1, OperatorComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperatorComponent_Guard(), ecorePackage.getEString(), "guard", null, 0, 1, OperatorComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(operatorComponentEClass, this.getUseCaseScenarioElement(), "getChildElements", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(actionEClass, Action.class, "Action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAction_Type(), this.getActionType(), "type", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAction_PictureData(), theEcorePackage.getEByteArray(), "pictureData", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAction_PictureURI(), theEcorePackage.getEString(), "pictureURI", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operatorEClass, Operator.class, "Operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperator_ChildElements(), this.getUseCaseScenarioElement(), null, "childElements", null, 0, -1, Operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scenarioReferenceEClass, ScenarioReference.class, "ScenarioReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScenarioReference_Scenario(), this.getUseCaseScenario(), null, "scenario", null, 0, 1, ScenarioReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(useCaseReferenceEClass, UseCaseReference.class, "UseCaseReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUseCaseReference_UseCase(), theUsecasePackage.getUseCase(), null, "useCase", null, 0, 1, UseCaseReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(actionTypeEEnum, ActionType.class, "ActionType");
		addEEnumLiteral(actionTypeEEnum, ActionType.ACTION);
		addEEnumLiteral(actionTypeEEnum, ActionType.RECEIVE);
		addEEnumLiteral(actionTypeEEnum, ActionType.SEND);
		addEEnumLiteral(actionTypeEEnum, ActionType.START);
		addEEnumLiteral(actionTypeEEnum, ActionType.STOP);
		addEEnumLiteral(actionTypeEEnum, ActionType.TEXT);
		addEEnumLiteral(actionTypeEEnum, ActionType.DIRECTION);

		initEEnum(operatorTypeEEnum, OperatorType.class, "OperatorType");
		addEEnumLiteral(operatorTypeEEnum, OperatorType.SEQUENCE);
		addEEnumLiteral(operatorTypeEEnum, OperatorType.PARALLEL);
		addEEnumLiteral(operatorTypeEEnum, OperatorType.OPTIONAL);
		addEEnumLiteral(operatorTypeEEnum, OperatorType.ALTERNATE);
		addEEnumLiteral(operatorTypeEEnum, OperatorType.BREAK);
		addEEnumLiteral(operatorTypeEEnum, OperatorType.LOOP);
		addEEnumLiteral(operatorTypeEEnum, OperatorType.STRICT);
		addEEnumLiteral(operatorTypeEEnum, OperatorType.CRITICAL);

		// Create resource
		createResource(eNS_URI);
	}

} //UsecasescenarioPackageImpl
