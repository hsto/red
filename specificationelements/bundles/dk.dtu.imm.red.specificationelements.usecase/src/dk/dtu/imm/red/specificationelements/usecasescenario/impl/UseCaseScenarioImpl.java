/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario.impl;

import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.specificationelements.usecasescenario.ActionType;
import dk.dtu.imm.red.specificationelements.usecasescenario.Operator;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioFactory;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case Scenario</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioImpl#getStartOperator <em>Start Operator</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioImpl#getId <em>Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UseCaseScenarioImpl extends SpecificationElementImpl implements UseCaseScenario {
	/**
	 * The cached value of the '{@link #getStartOperator() <em>Start Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartOperator()
	 * @generated
	 * @ordered
	 */
	protected OperatorComponent startOperator;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCaseScenarioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasescenarioPackage.Literals.USE_CASE_SCENARIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorComponent getStartOperator() {
		return startOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartOperator(OperatorComponent newStartOperator, NotificationChain msgs) {
		OperatorComponent oldStartOperator = startOperator;
		startOperator = newStartOperator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.USE_CASE_SCENARIO__START_OPERATOR, oldStartOperator, newStartOperator);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartOperator(OperatorComponent newStartOperator) {
		if (newStartOperator != startOperator) {
			NotificationChain msgs = null;
			if (startOperator != null)
				msgs = ((InternalEObject)startOperator).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasescenarioPackage.USE_CASE_SCENARIO__START_OPERATOR, null, msgs);
			if (newStartOperator != null)
				msgs = ((InternalEObject)newStartOperator).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasescenarioPackage.USE_CASE_SCENARIO__START_OPERATOR, null, msgs);
			msgs = basicSetStartOperator(newStartOperator, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.USE_CASE_SCENARIO__START_OPERATOR, newStartOperator, newStartOperator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.USE_CASE_SCENARIO__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_SCENARIO__START_OPERATOR:
				return basicSetStartOperator(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_SCENARIO__START_OPERATOR:
				return getStartOperator();
			case UsecasescenarioPackage.USE_CASE_SCENARIO__ID:
				return getId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_SCENARIO__START_OPERATOR:
				setStartOperator((OperatorComponent)newValue);
				return;
			case UsecasescenarioPackage.USE_CASE_SCENARIO__ID:
				setId((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_SCENARIO__START_OPERATOR:
				setStartOperator((OperatorComponent)null);
				return;
			case UsecasescenarioPackage.USE_CASE_SCENARIO__ID:
				setId(ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_SCENARIO__START_OPERATOR:
				return startOperator != null;
			case UsecasescenarioPackage.USE_CASE_SCENARIO__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public void createDefaultIfEmpty() {
		if(this.getStartOperator() == null) {
			Operator startOperator = UsecasescenarioFactory.eINSTANCE.createOperator("Start operator", OperatorType.SEQUENCE);
			startOperator.createAction("Start", ActionType.START);
			startOperator.createAction("Stop", ActionType.STOP);
			this.setStartOperator(startOperator);
		}
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("ID")
				.setEMFLiteral(UsecasescenarioPackage.Literals.USE_CASE_SCENARIO__ID)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<UseCaseScenario>() {

					@Override
					public Object getAttributeValue(UseCaseScenario item) {
						return item.getId()==null?"":item.getId();
					}

					@Override
					public void setAttributeValue(UseCaseScenario item, Object value) {
						item.setId((String) value);
					} 
				})
				,
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit) 
				.setEditable(true)
				.setCellHandler(new CellHandler<UseCaseScenario>() {

					@Override
					public Object getAttributeValue(UseCaseScenario item) {
						return item.getName()==null?"":item.getName();
					}

					@Override
					public void setAttributeValue(UseCaseScenario item, Object value) {
						item.setName((String) value);
					} 
				}) 
		};  
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/ucscenario.png";
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public List<OperatorComponent> extractAllExtensionOperators() {
		List<OperatorComponent> extractedElements = new ArrayList<OperatorComponent>();
		recExtractOperator(getStartOperator(), extractedElements);
		return extractedElements;
	}
	
	/**
	 * @generated NOT
	 */
	private void recExtractOperator(OperatorComponent operator, List<OperatorComponent> extractedOperators) {
		
		for(UseCaseScenarioElement se : operator.getChildElements() ) {
			if(se instanceof OperatorComponent) {
				extractedOperators.add((OperatorComponent) se);
				recExtractOperator(((OperatorComponent) se), extractedOperators);
			}
		} 
	} 

} //UseCaseScenarioImpl
