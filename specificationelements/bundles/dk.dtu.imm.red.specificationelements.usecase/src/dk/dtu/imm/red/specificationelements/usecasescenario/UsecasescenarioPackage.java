/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioFactory
 * @model kind="package"
 * @generated
 */
public interface UsecasescenarioPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "usecasescenario";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.specificationelements.usecasescenario";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "usecasescenario";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UsecasescenarioPackage eINSTANCE = dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioImpl <em>Use Case Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioImpl
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getUseCaseScenario()
	 * @generated
	 */
	int USE_CASE_SCENARIO = 0;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__ICON_URI = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__ICON = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__LABEL = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__NAME = SpecificationelementsPackage.SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__ELEMENT_KIND = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__DESCRIPTION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__PRIORITY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__COMMENTLIST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__TIME_CREATED = SpecificationelementsPackage.SPECIFICATION_ELEMENT__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__LAST_MODIFIED = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__LAST_MODIFIED_BY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__CREATOR = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__VERSION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__VISIBLE_ID = SpecificationelementsPackage.SPECIFICATION_ELEMENT__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__UNIQUE_ID = SpecificationelementsPackage.SPECIFICATION_ELEMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__RELATES_TO = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__RELATED_BY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__PARENT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__URI = SpecificationelementsPackage.SPECIFICATION_ELEMENT__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__WORK_PACKAGE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__CHANGE_LIST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__RESPONSIBLE_USER = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__DEADLINE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__LOCK_STATUS = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__LOCK_PASSWORD = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__ESTIMATED_COMPLEXITY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__COST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__BENEFIT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__RISK = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__LIFE_CYCLE_PHASE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__STATE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__MANAGEMENT_DECISION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__QA_ASSESSMENT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__MANAGEMENT_DISCUSSION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__DELETION_LISTENERS = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__QUANTITIES = SpecificationelementsPackage.SPECIFICATION_ELEMENT__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__CUSTOM_ATTRIBUTES = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Start Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__START_OPERATOR = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO__ID = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Use Case Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO_FEATURE_COUNT = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioElementImpl <em>Use Case Scenario Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioElementImpl
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getUseCaseScenarioElement()
	 * @generated
	 */
	int USE_CASE_SCENARIO_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO_ELEMENT__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO_ELEMENT__PARENT = 1;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO_ELEMENT__NAME = 3;

	/**
	 * The number of structural features of the '<em>Use Case Scenario Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_SCENARIO_ELEMENT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorComponentImpl <em>Operator Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorComponentImpl
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getOperatorComponent()
	 * @generated
	 */
	int OPERATOR_COMPONENT = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_COMPONENT__DESCRIPTION = USE_CASE_SCENARIO_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_COMPONENT__PARENT = USE_CASE_SCENARIO_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_COMPONENT__UNIQUE_ID = USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_COMPONENT__NAME = USE_CASE_SCENARIO_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_COMPONENT__TYPE = USE_CASE_SCENARIO_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_COMPONENT__GUARD = USE_CASE_SCENARIO_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Operator Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_COMPONENT_FEATURE_COUNT = USE_CASE_SCENARIO_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.ActionImpl
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__DESCRIPTION = USE_CASE_SCENARIO_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__PARENT = USE_CASE_SCENARIO_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__UNIQUE_ID = USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__NAME = USE_CASE_SCENARIO_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__TYPE = USE_CASE_SCENARIO_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Picture Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__PICTURE_DATA = USE_CASE_SCENARIO_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Picture URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__PICTURE_URI = USE_CASE_SCENARIO_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = USE_CASE_SCENARIO_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorImpl <em>Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorImpl
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getOperator()
	 * @generated
	 */
	int OPERATOR = 4;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__DESCRIPTION = OPERATOR_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__PARENT = OPERATOR_COMPONENT__PARENT;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__UNIQUE_ID = OPERATOR_COMPONENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__NAME = OPERATOR_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__TYPE = OPERATOR_COMPONENT__TYPE;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__GUARD = OPERATOR_COMPONENT__GUARD;

	/**
	 * The feature id for the '<em><b>Child Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__CHILD_ELEMENTS = OPERATOR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_FEATURE_COUNT = OPERATOR_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.ScenarioReferenceImpl <em>Scenario Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.ScenarioReferenceImpl
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getScenarioReference()
	 * @generated
	 */
	int SCENARIO_REFERENCE = 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_REFERENCE__DESCRIPTION = OPERATOR_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_REFERENCE__PARENT = OPERATOR_COMPONENT__PARENT;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_REFERENCE__UNIQUE_ID = OPERATOR_COMPONENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_REFERENCE__NAME = OPERATOR_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_REFERENCE__TYPE = OPERATOR_COMPONENT__TYPE;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_REFERENCE__GUARD = OPERATOR_COMPONENT__GUARD;

	/**
	 * The feature id for the '<em><b>Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_REFERENCE__SCENARIO = OPERATOR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Scenario Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_REFERENCE_FEATURE_COUNT = OPERATOR_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseReferenceImpl <em>Use Case Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseReferenceImpl
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getUseCaseReference()
	 * @generated
	 */
	int USE_CASE_REFERENCE = 6;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_REFERENCE__DESCRIPTION = SCENARIO_REFERENCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_REFERENCE__PARENT = SCENARIO_REFERENCE__PARENT;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_REFERENCE__UNIQUE_ID = SCENARIO_REFERENCE__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_REFERENCE__NAME = SCENARIO_REFERENCE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_REFERENCE__TYPE = SCENARIO_REFERENCE__TYPE;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_REFERENCE__GUARD = SCENARIO_REFERENCE__GUARD;

	/**
	 * The feature id for the '<em><b>Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_REFERENCE__SCENARIO = SCENARIO_REFERENCE__SCENARIO;

	/**
	 * The feature id for the '<em><b>Use Case</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_REFERENCE__USE_CASE = SCENARIO_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Use Case Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_REFERENCE_FEATURE_COUNT = SCENARIO_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.ActionType <em>Action Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.ActionType
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getActionType()
	 * @generated
	 */
	int ACTION_TYPE = 7;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType <em>Operator Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getOperatorType()
	 * @generated
	 */
	int OPERATOR_TYPE = 8;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario <em>Use Case Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Case Scenario</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario
	 * @generated
	 */
	EClass getUseCaseScenario();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario#getStartOperator <em>Start Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Start Operator</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario#getStartOperator()
	 * @see #getUseCaseScenario()
	 * @generated
	 */
	EReference getUseCaseScenario_StartOperator();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario#getId()
	 * @see #getUseCaseScenario()
	 * @generated
	 */
	EAttribute getUseCaseScenario_Id();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement <em>Use Case Scenario Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Case Scenario Element</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement
	 * @generated
	 */
	EClass getUseCaseScenarioElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getDescription()
	 * @see #getUseCaseScenarioElement()
	 * @generated
	 */
	EAttribute getUseCaseScenarioElement_Description();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getParent()
	 * @see #getUseCaseScenarioElement()
	 * @generated
	 */
	EReference getUseCaseScenarioElement_Parent();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getUniqueID <em>Unique ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unique ID</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getUniqueID()
	 * @see #getUseCaseScenarioElement()
	 * @generated
	 */
	EAttribute getUseCaseScenarioElement_UniqueID();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getName()
	 * @see #getUseCaseScenarioElement()
	 * @generated
	 */
	EAttribute getUseCaseScenarioElement_Name();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent <em>Operator Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operator Component</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent
	 * @generated
	 */
	EClass getOperatorComponent();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent#getType()
	 * @see #getOperatorComponent()
	 * @generated
	 */
	EAttribute getOperatorComponent_Type();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Guard</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent#getGuard()
	 * @see #getOperatorComponent()
	 * @generated
	 */
	EAttribute getOperatorComponent_Guard();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.Action#getType()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_Type();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action#getPictureData <em>Picture Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Picture Data</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.Action#getPictureData()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_PictureData();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action#getPictureURI <em>Picture URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Picture URI</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.Action#getPictureURI()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_PictureURI();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.usecasescenario.Operator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operator</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.Operator
	 * @generated
	 */
	EClass getOperator();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.specificationelements.usecasescenario.Operator#getChildElements <em>Child Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Child Elements</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.Operator#getChildElements()
	 * @see #getOperator()
	 * @generated
	 */
	EReference getOperator_ChildElements();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.usecasescenario.ScenarioReference <em>Scenario Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scenario Reference</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.ScenarioReference
	 * @generated
	 */
	EClass getScenarioReference();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.specificationelements.usecasescenario.ScenarioReference#getScenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scenario</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.ScenarioReference#getScenario()
	 * @see #getScenarioReference()
	 * @generated
	 */
	EReference getScenarioReference_Scenario();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseReference <em>Use Case Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Case Reference</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseReference
	 * @generated
	 */
	EClass getUseCaseReference();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseReference#getUseCase <em>Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Use Case</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseReference#getUseCase()
	 * @see #getUseCaseReference()
	 * @generated
	 */
	EReference getUseCaseReference_UseCase();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.specificationelements.usecasescenario.ActionType <em>Action Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Action Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.ActionType
	 * @generated
	 */
	EEnum getActionType();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType <em>Operator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operator Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType
	 * @generated
	 */
	EEnum getOperatorType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UsecasescenarioFactory getUsecasescenarioFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioImpl <em>Use Case Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioImpl
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getUseCaseScenario()
		 * @generated
		 */
		EClass USE_CASE_SCENARIO = eINSTANCE.getUseCaseScenario();

		/**
		 * The meta object literal for the '<em><b>Start Operator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE_SCENARIO__START_OPERATOR = eINSTANCE.getUseCaseScenario_StartOperator();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_CASE_SCENARIO__ID = eINSTANCE.getUseCaseScenario_Id();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioElementImpl <em>Use Case Scenario Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioElementImpl
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getUseCaseScenarioElement()
		 * @generated
		 */
		EClass USE_CASE_SCENARIO_ELEMENT = eINSTANCE.getUseCaseScenarioElement();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_CASE_SCENARIO_ELEMENT__DESCRIPTION = eINSTANCE.getUseCaseScenarioElement_Description();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE_SCENARIO_ELEMENT__PARENT = eINSTANCE.getUseCaseScenarioElement_Parent();

		/**
		 * The meta object literal for the '<em><b>Unique ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID = eINSTANCE.getUseCaseScenarioElement_UniqueID();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USE_CASE_SCENARIO_ELEMENT__NAME = eINSTANCE.getUseCaseScenarioElement_Name();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorComponentImpl <em>Operator Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorComponentImpl
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getOperatorComponent()
		 * @generated
		 */
		EClass OPERATOR_COMPONENT = eINSTANCE.getOperatorComponent();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATOR_COMPONENT__TYPE = eINSTANCE.getOperatorComponent_Type();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATOR_COMPONENT__GUARD = eINSTANCE.getOperatorComponent_Guard();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.ActionImpl
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__TYPE = eINSTANCE.getAction_Type();

		/**
		 * The meta object literal for the '<em><b>Picture Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__PICTURE_DATA = eINSTANCE.getAction_PictureData();

		/**
		 * The meta object literal for the '<em><b>Picture URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__PICTURE_URI = eINSTANCE.getAction_PictureURI();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorImpl <em>Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorImpl
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getOperator()
		 * @generated
		 */
		EClass OPERATOR = eINSTANCE.getOperator();

		/**
		 * The meta object literal for the '<em><b>Child Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATOR__CHILD_ELEMENTS = eINSTANCE.getOperator_ChildElements();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.ScenarioReferenceImpl <em>Scenario Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.ScenarioReferenceImpl
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getScenarioReference()
		 * @generated
		 */
		EClass SCENARIO_REFERENCE = eINSTANCE.getScenarioReference();

		/**
		 * The meta object literal for the '<em><b>Scenario</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO_REFERENCE__SCENARIO = eINSTANCE.getScenarioReference_Scenario();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseReferenceImpl <em>Use Case Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseReferenceImpl
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getUseCaseReference()
		 * @generated
		 */
		EClass USE_CASE_REFERENCE = eINSTANCE.getUseCaseReference();

		/**
		 * The meta object literal for the '<em><b>Use Case</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE_REFERENCE__USE_CASE = eINSTANCE.getUseCaseReference_UseCase();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.ActionType <em>Action Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.ActionType
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getActionType()
		 * @generated
		 */
		EEnum ACTION_TYPE = eINSTANCE.getActionType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType <em>Operator Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType
		 * @see dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioPackageImpl#getOperatorType()
		 * @generated
		 */
		EEnum OPERATOR_TYPE = eINSTANCE.getOperatorType();

	}

} //UsecasescenarioPackage
