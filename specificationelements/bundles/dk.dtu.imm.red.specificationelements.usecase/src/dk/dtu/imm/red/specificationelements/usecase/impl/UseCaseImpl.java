/**
 */
package dk.dtu.imm.red.specificationelements.usecase.impl;

import dk.dtu.imm.red.core.element.ComplexityType;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.properties.DurationUnit;
import dk.dtu.imm.red.core.properties.MonetaryUnit;
import dk.dtu.imm.red.core.properties.Property;

import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;

import dk.dtu.imm.red.specificationelements.usecase.InputOutput;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.UseCaseKind;
import dk.dtu.imm.red.specificationelements.usecase.UsecasePackage;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getPreConditions <em>Pre Conditions</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getPostConditions <em>Post Conditions</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getOutcome <em>Outcome</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getResult <em>Result</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getLongDescription <em>Long Description</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getIncidence <em>Incidence</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getInputOutputList <em>Input Output List</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getPrimaryScenarios <em>Primary Scenarios</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getPrimaryActors <em>Primary Actors</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getSecondaryScenarios <em>Secondary Scenarios</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.impl.UseCaseImpl#getSecondaryActors <em>Secondary Actors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UseCaseImpl extends SpecificationElementImpl implements UseCase {
	/**
	 * The cached value of the '{@link #getPreConditions() <em>Pre Conditions</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<String> preConditions;

	/**
	 * The cached value of the '{@link #getPostConditions() <em>Post Conditions</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<String> postConditions;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final UseCaseKind TYPE_EDEFAULT = UseCaseKind.UNSPECIFIED;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected UseCaseKind type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getParameter() <em>Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected static final String PARAMETER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected String parameter = PARAMETER_EDEFAULT;

	/**
	 * The default value of the '{@link #getTrigger() <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrigger()
	 * @generated
	 * @ordered
	 */
	protected static final String TRIGGER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTrigger() <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrigger()
	 * @generated
	 * @ordered
	 */
	protected String trigger = TRIGGER_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutcome() <em>Outcome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutcome()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTCOME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutcome() <em>Outcome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutcome()
	 * @generated
	 * @ordered
	 */
	protected String outcome = OUTCOME_EDEFAULT;

	/**
	 * The default value of the '{@link #getResult() <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected static final String RESULT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResult() <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected String result = RESULT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLongDescription() <em>Long Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongDescription()
	 * @generated
	 * @ordered
	 */
	protected Text longDescription;

	/**
	 * The cached value of the '{@link #getIncidence() <em>Incidence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncidence()
	 * @generated
	 * @ordered
	 */
	protected Property incidence;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected Property duration;

	/**
	 * The cached value of the '{@link #getInputOutputList() <em>Input Output List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputOutputList()
	 * @generated
	 * @ordered
	 */
	protected EList<InputOutput> inputOutputList;

	/**
	 * The cached value of the '{@link #getPrimaryScenarios() <em>Primary Scenarios</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrimaryScenarios()
	 * @generated
	 * @ordered
	 */
	protected EList<UseCaseScenario> primaryScenarios;

	/**
	 * The cached value of the '{@link #getPrimaryActors() <em>Primary Actors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrimaryActors()
	 * @generated
	 * @ordered
	 */
	protected EList<Actor> primaryActors;

	/**
	 * The cached value of the '{@link #getSecondaryScenarios() <em>Secondary Scenarios</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecondaryScenarios()
	 * @generated
	 * @ordered
	 */
	protected EList<UseCaseScenario> secondaryScenarios;

	/**
	 * The cached value of the '{@link #getSecondaryActors() <em>Secondary Actors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecondaryActors()
	 * @generated
	 * @ordered
	 */
	protected EList<Actor> secondaryActors;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasePackage.Literals.USE_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getPreConditions() {
		if (preConditions == null) {
			preConditions = new EDataTypeEList<String>(String.class, this, UsecasePackage.USE_CASE__PRE_CONDITIONS);
		}
		return preConditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getPostConditions() {
		if (postConditions == null) {
			postConditions = new EDataTypeEList<String>(String.class, this, UsecasePackage.USE_CASE__POST_CONDITIONS);
		}
		return postConditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCaseKind getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(UseCaseKind newType) {
		UseCaseKind oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getParameter() {
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameter(String newParameter) {
		String oldParameter = parameter;
		parameter = newParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__PARAMETER, oldParameter, parameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTrigger() {
		return trigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrigger(String newTrigger) {
		String oldTrigger = trigger;
		trigger = newTrigger;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__TRIGGER, oldTrigger, trigger));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutcome() {
		return outcome;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutcome(String newOutcome) {
		String oldOutcome = outcome;
		outcome = newOutcome;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__OUTCOME, oldOutcome, outcome));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResult() {
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResult(String newResult) {
		String oldResult = result;
		result = newResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__RESULT, oldResult, result));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getLongDescription() {
		return longDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLongDescription(Text newLongDescription, NotificationChain msgs) {
		Text oldLongDescription = longDescription;
		longDescription = newLongDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__LONG_DESCRIPTION, oldLongDescription, newLongDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLongDescription(Text newLongDescription) {
		if (newLongDescription != longDescription) {
			NotificationChain msgs = null;
			if (longDescription != null)
				msgs = ((InternalEObject)longDescription).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasePackage.USE_CASE__LONG_DESCRIPTION, null, msgs);
			if (newLongDescription != null)
				msgs = ((InternalEObject)newLongDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasePackage.USE_CASE__LONG_DESCRIPTION, null, msgs);
			msgs = basicSetLongDescription(newLongDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__LONG_DESCRIPTION, newLongDescription, newLongDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getIncidence() {
		return incidence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIncidence(Property newIncidence, NotificationChain msgs) {
		Property oldIncidence = incidence;
		incidence = newIncidence;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__INCIDENCE, oldIncidence, newIncidence);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIncidence(Property newIncidence) {
		if (newIncidence != incidence) {
			NotificationChain msgs = null;
			if (incidence != null)
				msgs = ((InternalEObject)incidence).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasePackage.USE_CASE__INCIDENCE, null, msgs);
			if (newIncidence != null)
				msgs = ((InternalEObject)newIncidence).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasePackage.USE_CASE__INCIDENCE, null, msgs);
			msgs = basicSetIncidence(newIncidence, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__INCIDENCE, newIncidence, newIncidence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDuration(Property newDuration, NotificationChain msgs) {
		Property oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__DURATION, oldDuration, newDuration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(Property newDuration) {
		if (newDuration != duration) {
			NotificationChain msgs = null;
			if (duration != null)
				msgs = ((InternalEObject)duration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasePackage.USE_CASE__DURATION, null, msgs);
			if (newDuration != null)
				msgs = ((InternalEObject)newDuration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasePackage.USE_CASE__DURATION, null, msgs);
			msgs = basicSetDuration(newDuration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasePackage.USE_CASE__DURATION, newDuration, newDuration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InputOutput> getInputOutputList() {
		if (inputOutputList == null) {
			inputOutputList = new EObjectContainmentEList<InputOutput>(InputOutput.class, this, UsecasePackage.USE_CASE__INPUT_OUTPUT_LIST);
		}
		return inputOutputList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UseCaseScenario> getPrimaryScenarios() {
		if (primaryScenarios == null) {
			primaryScenarios = new EObjectResolvingEList<UseCaseScenario>(UseCaseScenario.class, this, UsecasePackage.USE_CASE__PRIMARY_SCENARIOS);
		}
		return primaryScenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Actor> getPrimaryActors() {
		if (primaryActors == null) {
			primaryActors = new EObjectResolvingEList<Actor>(Actor.class, this, UsecasePackage.USE_CASE__PRIMARY_ACTORS);
		}
		return primaryActors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UseCaseScenario> getSecondaryScenarios() {
		if (secondaryScenarios == null) {
			secondaryScenarios = new EObjectResolvingEList<UseCaseScenario>(UseCaseScenario.class, this, UsecasePackage.USE_CASE__SECONDARY_SCENARIOS);
		}
		return secondaryScenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Actor> getSecondaryActors() {
		if (secondaryActors == null) {
			secondaryActors = new EObjectResolvingEList<Actor>(Actor.class, this, UsecasePackage.USE_CASE__SECONDARY_ACTORS);
		}
		return secondaryActors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasePackage.USE_CASE__LONG_DESCRIPTION:
				return basicSetLongDescription(null, msgs);
			case UsecasePackage.USE_CASE__INCIDENCE:
				return basicSetIncidence(null, msgs);
			case UsecasePackage.USE_CASE__DURATION:
				return basicSetDuration(null, msgs);
			case UsecasePackage.USE_CASE__INPUT_OUTPUT_LIST:
				return ((InternalEList<?>)getInputOutputList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasePackage.USE_CASE__PRE_CONDITIONS:
				return getPreConditions();
			case UsecasePackage.USE_CASE__POST_CONDITIONS:
				return getPostConditions();
			case UsecasePackage.USE_CASE__TYPE:
				return getType();
			case UsecasePackage.USE_CASE__PARAMETER:
				return getParameter();
			case UsecasePackage.USE_CASE__TRIGGER:
				return getTrigger();
			case UsecasePackage.USE_CASE__OUTCOME:
				return getOutcome();
			case UsecasePackage.USE_CASE__RESULT:
				return getResult();
			case UsecasePackage.USE_CASE__LONG_DESCRIPTION:
				return getLongDescription();
			case UsecasePackage.USE_CASE__INCIDENCE:
				return getIncidence();
			case UsecasePackage.USE_CASE__DURATION:
				return getDuration();
			case UsecasePackage.USE_CASE__INPUT_OUTPUT_LIST:
				return getInputOutputList();
			case UsecasePackage.USE_CASE__PRIMARY_SCENARIOS:
				return getPrimaryScenarios();
			case UsecasePackage.USE_CASE__PRIMARY_ACTORS:
				return getPrimaryActors();
			case UsecasePackage.USE_CASE__SECONDARY_SCENARIOS:
				return getSecondaryScenarios();
			case UsecasePackage.USE_CASE__SECONDARY_ACTORS:
				return getSecondaryActors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasePackage.USE_CASE__PRE_CONDITIONS:
				getPreConditions().clear();
				getPreConditions().addAll((Collection<? extends String>)newValue);
				return;
			case UsecasePackage.USE_CASE__POST_CONDITIONS:
				getPostConditions().clear();
				getPostConditions().addAll((Collection<? extends String>)newValue);
				return;
			case UsecasePackage.USE_CASE__TYPE:
				setType((UseCaseKind)newValue);
				return;
			case UsecasePackage.USE_CASE__PARAMETER:
				setParameter((String)newValue);
				return;
			case UsecasePackage.USE_CASE__TRIGGER:
				setTrigger((String)newValue);
				return;
			case UsecasePackage.USE_CASE__OUTCOME:
				setOutcome((String)newValue);
				return;
			case UsecasePackage.USE_CASE__RESULT:
				setResult((String)newValue);
				return;
			case UsecasePackage.USE_CASE__LONG_DESCRIPTION:
				setLongDescription((Text)newValue);
				return;
			case UsecasePackage.USE_CASE__INCIDENCE:
				setIncidence((Property)newValue);
				return;
			case UsecasePackage.USE_CASE__DURATION:
				setDuration((Property)newValue);
				return;
			case UsecasePackage.USE_CASE__INPUT_OUTPUT_LIST:
				getInputOutputList().clear();
				getInputOutputList().addAll((Collection<? extends InputOutput>)newValue);
				return;
			case UsecasePackage.USE_CASE__PRIMARY_SCENARIOS:
				getPrimaryScenarios().clear();
				getPrimaryScenarios().addAll((Collection<? extends UseCaseScenario>)newValue);
				return;
			case UsecasePackage.USE_CASE__PRIMARY_ACTORS:
				getPrimaryActors().clear();
				getPrimaryActors().addAll((Collection<? extends Actor>)newValue);
				return;
			case UsecasePackage.USE_CASE__SECONDARY_SCENARIOS:
				getSecondaryScenarios().clear();
				getSecondaryScenarios().addAll((Collection<? extends UseCaseScenario>)newValue);
				return;
			case UsecasePackage.USE_CASE__SECONDARY_ACTORS:
				getSecondaryActors().clear();
				getSecondaryActors().addAll((Collection<? extends Actor>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasePackage.USE_CASE__PRE_CONDITIONS:
				getPreConditions().clear();
				return;
			case UsecasePackage.USE_CASE__POST_CONDITIONS:
				getPostConditions().clear();
				return;
			case UsecasePackage.USE_CASE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case UsecasePackage.USE_CASE__PARAMETER:
				setParameter(PARAMETER_EDEFAULT);
				return;
			case UsecasePackage.USE_CASE__TRIGGER:
				setTrigger(TRIGGER_EDEFAULT);
				return;
			case UsecasePackage.USE_CASE__OUTCOME:
				setOutcome(OUTCOME_EDEFAULT);
				return;
			case UsecasePackage.USE_CASE__RESULT:
				setResult(RESULT_EDEFAULT);
				return;
			case UsecasePackage.USE_CASE__LONG_DESCRIPTION:
				setLongDescription((Text)null);
				return;
			case UsecasePackage.USE_CASE__INCIDENCE:
				setIncidence((Property)null);
				return;
			case UsecasePackage.USE_CASE__DURATION:
				setDuration((Property)null);
				return;
			case UsecasePackage.USE_CASE__INPUT_OUTPUT_LIST:
				getInputOutputList().clear();
				return;
			case UsecasePackage.USE_CASE__PRIMARY_SCENARIOS:
				getPrimaryScenarios().clear();
				return;
			case UsecasePackage.USE_CASE__PRIMARY_ACTORS:
				getPrimaryActors().clear();
				return;
			case UsecasePackage.USE_CASE__SECONDARY_SCENARIOS:
				getSecondaryScenarios().clear();
				return;
			case UsecasePackage.USE_CASE__SECONDARY_ACTORS:
				getSecondaryActors().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasePackage.USE_CASE__PRE_CONDITIONS:
				return preConditions != null && !preConditions.isEmpty();
			case UsecasePackage.USE_CASE__POST_CONDITIONS:
				return postConditions != null && !postConditions.isEmpty();
			case UsecasePackage.USE_CASE__TYPE:
				return type != TYPE_EDEFAULT;
			case UsecasePackage.USE_CASE__PARAMETER:
				return PARAMETER_EDEFAULT == null ? parameter != null : !PARAMETER_EDEFAULT.equals(parameter);
			case UsecasePackage.USE_CASE__TRIGGER:
				return TRIGGER_EDEFAULT == null ? trigger != null : !TRIGGER_EDEFAULT.equals(trigger);
			case UsecasePackage.USE_CASE__OUTCOME:
				return OUTCOME_EDEFAULT == null ? outcome != null : !OUTCOME_EDEFAULT.equals(outcome);
			case UsecasePackage.USE_CASE__RESULT:
				return RESULT_EDEFAULT == null ? result != null : !RESULT_EDEFAULT.equals(result);
			case UsecasePackage.USE_CASE__LONG_DESCRIPTION:
				return longDescription != null;
			case UsecasePackage.USE_CASE__INCIDENCE:
				return incidence != null;
			case UsecasePackage.USE_CASE__DURATION:
				return duration != null;
			case UsecasePackage.USE_CASE__INPUT_OUTPUT_LIST:
				return inputOutputList != null && !inputOutputList.isEmpty();
			case UsecasePackage.USE_CASE__PRIMARY_SCENARIOS:
				return primaryScenarios != null && !primaryScenarios.isEmpty();
			case UsecasePackage.USE_CASE__PRIMARY_ACTORS:
				return primaryActors != null && !primaryActors.isEmpty();
			case UsecasePackage.USE_CASE__SECONDARY_SCENARIOS:
				return secondaryScenarios != null && !secondaryScenarios.isEmpty();
			case UsecasePackage.USE_CASE__SECONDARY_ACTORS:
				return secondaryActors != null && !secondaryActors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (preConditions: ");
		result.append(preConditions);
		result.append(", postConditions: ");
		result.append(postConditions);
		result.append(", type: ");
		result.append(type);
		result.append(", parameter: ");
		result.append(parameter);
		result.append(", trigger: ");
		result.append(trigger);
		result.append(", outcome: ");
		result.append(outcome);
		result.append(", result: ");
		result.append(result);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/usecase.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();
		return super.search(null, attributesToSearch);
		// attributesToSearch.put(GoalPackage.Literals.GOAL__EXPLANATION,
		// this.getExplanation()== null ? "" :
		// this.getExplanation().toString());
		// attributesToSearch.put(GoalPackage.Literals.GOAL__GOAL_ID,
		// this.getGoalID());
		// attributesToSearch.put(GoalPackage.Literals.GOAL__GOAL_SENTENCE,
		// this.getGoalSentence());

		// return super.search(param, attributesToSearWch);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public double computedEffort() {
		return 0;

	}

	/**
	 * @generated NOT
	 */
	@Override
	public dk.dtu.imm.red.core.element.ComplexityType getEstimatedComplexity() {

		// Add all Scenarios
		List<UseCaseScenario> allScenarios = new ArrayList<UseCaseScenario>();
		allScenarios.addAll(getPrimaryScenarios());
		allScenarios.addAll(getSecondaryScenarios());

		Aggregator a = new Aggregator(0);

		for (UseCaseScenario s : allScenarios) {
			getNumberOfStep(s.getStartOperator(), a);
		}

		double stepValue = a.getValue();
		double denominator = 3.0;
		double w = stepValue / denominator; // Use case weight

		ComplexityType ct = w >= 15 ? ComplexityType.LARGE
				: w >= 5 ? ComplexityType.MEDIUM : ComplexityType.SMALL;
		return ct;
	}

	/**
	 * @generated NOT
	 */
	private void getNumberOfStep(OperatorComponent c, Aggregator a) {
		try {
			for (UseCaseScenarioElement e : c.getChildElements()) {
				if (e instanceof OperatorComponent) {
					getNumberOfStep((OperatorComponent) e, a);
				} else {
					a.add(1.0);
				}
			}
		} catch (Exception e) {
		}
	}

	/**
	 * @generated NOT
	 */
	class Aggregator {
		public double value = 0;

		public double getValue() {
			return value;
		}

		public Aggregator(int i) {
			this.value = i;
		}

		public void add(double v) {
			this.value = this.value + v;
		}
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {
		return new ElementColumnDefinition[] {
				new ElementColumnDefinition().setHeaderName("Label")
						.setEMFLiteral(ElementPackage.Literals.ELEMENT__LABEL)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getLabel();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.setLabel((String) value);
							}
						}),
				new ElementColumnDefinition().setHeaderName("Name")
						.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getName();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.setName((String) value);
							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Kind")
						.setEMFLiteral(UsecasePackage.Literals.USE_CASE_KIND)
						.setColumnBehaviour(ColumnBehaviour.ComboSelector)
						.setCellItems(
								ElementColumnDefinition
										.enumToStringArray(UseCaseKind.values()))
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getElementKind();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.setType(UseCaseKind.valueOf(value
										.toString().toUpperCase()));
							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Trigger")
						.setEMFLiteral(UsecasePackage.Literals.USE_CASE__TRIGGER)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getTrigger();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.setTrigger((String) value);
							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Parameter")
						.setEMFLiteral(
								UsecasePackage.Literals.USE_CASE__PARAMETER)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getParameter();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.setParameter((String) value);
							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Outcome")
						.setEMFLiteral(UsecasePackage.Literals.USE_CASE__OUTCOME)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getOutcome();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.setOutcome((String) value);
							}
						}),
				new ElementColumnDefinition().setHeaderName("Result")
						.setEMFLiteral(UsecasePackage.Literals.USE_CASE__RESULT)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getResult();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.setResult((String) value);
							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Estimated Complexity")
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(false)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								String val = item.getEstimatedComplexity()
										.name().toString();
								return val;
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {

							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Incidence Value")
						.setEMFLiteral(
								UsecasePackage.Literals.USE_CASE__INCIDENCE)
						.setColumnType(Double.class)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getIncidence().getValue();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.getIncidence().setValue(
										Double.parseDouble(value.toString()));
							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Incidence Unit")
						.setColumnType(DurationUnit.class)
						.setColumnBehaviour(ColumnBehaviour.ComboSelector)
						.setCellItems(
								ElementColumnDefinition
										.enumToStringArray(DurationUnit
												.values())).setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getIncidence().getDurationUnit()
										.toString();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.getIncidence().setDurationUnit(
										DurationUnit.valueOf(value.toString()
												.toUpperCase()));
							}
						}),
				new ElementColumnDefinition().setHeaderName("Duration Value")
						.setColumnType(Double.class)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getDuration().getValue();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.getDuration().setValue(
										Double.parseDouble(value.toString()));
							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Duration Unit")
						.setColumnType(DurationUnit.class)
						.setColumnBehaviour(ColumnBehaviour.ComboSelector)
						.setCellItems(
								ElementColumnDefinition
										.enumToStringArray(DurationUnit
												.values())).setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getDuration().getDurationUnit();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.getDuration().setDurationUnit(
										DurationUnit.valueOf(value.toString()
												.toUpperCase()));
							}
						}),
				new ElementColumnDefinition().setHeaderName("Cost Value")
						.setColumnType(Double.class)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getCost().getValue();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.getCost().setValue(
										Double.parseDouble(value.toString()));
							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Cost Unit")
						.setColumnType(MonetaryUnit.class)
						.setColumnBehaviour(ColumnBehaviour.ComboSelector)
						.setCellItems(
								ElementColumnDefinition
										.enumToStringArray(MonetaryUnit
												.values())).setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getCost().getMonetaryUnit();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.getCost().setMonetaryUnit(
										MonetaryUnit.valueOf(value.toString()
												.toUpperCase()));
							}
						}),
				new ElementColumnDefinition().setHeaderName("Benefit Value")
						.setColumnType(Double.class)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getBenefit().getValue();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.getBenefit().setValue(
										Double.parseDouble(value.toString()));
							}
						}),
				new ElementColumnDefinition()
						.setHeaderName("Benefit Unit")
						.setColumnType(MonetaryUnit.class)
						.setColumnBehaviour(ColumnBehaviour.ComboSelector)
						.setCellItems(
								ElementColumnDefinition
										.enumToStringArray(MonetaryUnit
												.values())).setEditable(true)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								return item.getBenefit().getMonetaryUnit()
										.toString();
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								item.getBenefit().setMonetaryUnit(
										MonetaryUnit.valueOf(value.toString()
												.toUpperCase()));
							}
						}),
				new ElementColumnDefinition().setHeaderName("Estimated Cost")
						.setColumnType(Double.class)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(false)
						.setCellHandler(new CellHandler<UseCase>() {

							@Override
							public Object getAttributeValue(UseCase item) {
								List<Project> currentlyOpenedProjects = WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects(); 
								Project p =  currentlyOpenedProjects.size() > 0 ? currentlyOpenedProjects.get(0) : null; 
								
								if (p != null) {
									UsecasePoint ucp = p.getEffort();
									if (ucp != null) {
										ArrayList<Element> elements = new ArrayList<Element>();
										elements.add(item);
										Map<String, List<Element>>  elementMap =  ucp.getElementMap(elements); 
										return ucp.getSum(elementMap);										
									}
								}
								
								return 0.0;				
							}

							@Override
							public void setAttributeValue(UseCase item,
									Object value) {
								return;
							}
						}) };
	}

} //UseCaseImpl
