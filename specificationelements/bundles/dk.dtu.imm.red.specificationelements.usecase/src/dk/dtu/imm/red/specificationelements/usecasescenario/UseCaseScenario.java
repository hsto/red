/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario;

import java.util.List;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario#getStartOperator <em>Start Operator</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseScenario()
 * @model
 * @generated
 */
public interface UseCaseScenario extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Start Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Operator</em>' containment reference.
	 * @see #setStartOperator(OperatorComponent)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseScenario_StartOperator()
	 * @model containment="true"
	 * @generated
	 */
	OperatorComponent getStartOperator();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario#getStartOperator <em>Start Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Operator</em>' containment reference.
	 * @see #getStartOperator()
	 * @generated
	 */
	void setStartOperator(OperatorComponent value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseScenario_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * @generated NOT
	 */
	void createDefaultIfEmpty();

	/**
	 * @generated NOT
	 */
	List<OperatorComponent> extractAllExtensionOperators();

} // UseCaseScenario
