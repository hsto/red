/**
 */
package dk.dtu.imm.red.specificationelements.actor;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.actor.Actor#getId <em>Id</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.actor.Actor#getCode <em>Code</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.actor.Actor#getBehaviorElements <em>Behavior Elements</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.actor.Actor#getCodeType <em>Code Type</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.actor.ActorPackage#getActor()
 * @model
 * @generated
 */
public interface Actor extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see dk.dtu.imm.red.specificationelements.actor.ActorPackage#getActor_Id()
	 * @model unique="false"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.actor.Actor#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see dk.dtu.imm.red.specificationelements.actor.ActorPackage#getActor_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.actor.Actor#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Behavior Elements</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.SpecificationElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Behavior Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Behavior Elements</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.actor.ActorPackage#getActor_BehaviorElements()
	 * @model
	 * @generated
	 */
	EList<SpecificationElement> getBehaviorElements();

	/**
	 * Returns the value of the '<em><b>Code Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.actor.CodeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code Type</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.actor.CodeType
	 * @see #setCodeType(CodeType)
	 * @see dk.dtu.imm.red.specificationelements.actor.ActorPackage#getActor_CodeType()
	 * @model
	 * @generated
	 */
	CodeType getCodeType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.actor.Actor#getCodeType <em>Code Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code Type</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.actor.CodeType
	 * @see #getCodeType()
	 * @generated
	 */
	void setCodeType(CodeType value);

} // Actor
