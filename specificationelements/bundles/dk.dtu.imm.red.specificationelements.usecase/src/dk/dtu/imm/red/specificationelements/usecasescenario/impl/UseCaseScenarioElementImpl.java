/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario.impl;

import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent;
import dk.dtu.imm.red.specificationelements.usecasescenario.ScenarioReference;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case Scenario Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioElementImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioElementImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioElementImpl#getUniqueID <em>Unique ID</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseScenarioElementImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class UseCaseScenarioElementImpl extends MinimalEObjectImpl.Container implements UseCaseScenarioElement {
	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected OperatorComponent parent;

	/**
	 * The default value of the '{@link #getUniqueID() <em>Unique ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUniqueID()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIQUE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUniqueID() <em>Unique ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUniqueID()
	 * @generated
	 * @ordered
	 */
	protected String uniqueID = UNIQUE_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCaseScenarioElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasescenarioPackage.Literals.USE_CASE_SCENARIO_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorComponent getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = (OperatorComponent)eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorComponent basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(OperatorComponent newParent) {
		OperatorComponent oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUniqueID() {
		return uniqueID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniqueID(String newUniqueID) {
		String oldUniqueID = uniqueID;
		uniqueID = newUniqueID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID, oldUniqueID, uniqueID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__DESCRIPTION:
				return getDescription();
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID:
				return getUniqueID();
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__PARENT:
				setParent((OperatorComponent)newValue);
				return;
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID:
				setUniqueID((String)newValue);
				return;
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__PARENT:
				setParent((OperatorComponent)null);
				return;
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID:
				setUniqueID(UNIQUE_ID_EDEFAULT);
				return;
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__PARENT:
				return parent != null;
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__UNIQUE_ID:
				return UNIQUE_ID_EDEFAULT == null ? uniqueID != null : !UNIQUE_ID_EDEFAULT.equals(uniqueID);
			case UsecasescenarioPackage.USE_CASE_SCENARIO_ELEMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", uniqueID: ");
		result.append(uniqueID);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public void move(int index, OperatorComponent newParent) { 
		if(this.getParent() != newParent) { 
			getParent().getChildElements().remove(this);
			setParent(newParent);
			getParent().getChildElements().add(boundaryCorrection(index, getParent().getChildElements().size() - 1), this);  
		} 
		else {
			getParent().getChildElements().move(boundaryCorrection(index, getParent().getChildElements().size() - 1), this);  
		} 
	}
	
	private int boundaryCorrection(int index, int maxSize) {
		return Math.min((Math.max(index, 0 )), Math.max(maxSize, 0 ));
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public void remove() { 
		getParent().getChildElements().remove(this); 
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public boolean isFromAReference() {
		UseCaseScenarioElement parent = getParent();
		do {
			if(parent instanceof ScenarioReference) {
				return true;
			}
			parent = parent.getParent();
		} while(parent != null);
		return false;
	}

} //UseCaseScenarioElementImpl
