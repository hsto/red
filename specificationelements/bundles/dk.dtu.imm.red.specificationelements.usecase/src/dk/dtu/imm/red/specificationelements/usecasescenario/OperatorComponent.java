/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operator Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent#getGuard <em>Guard</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getOperatorComponent()
 * @model
 * @generated
 */
public interface OperatorComponent extends UseCaseScenarioElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType
	 * @see #setType(OperatorType)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getOperatorComponent_Type()
	 * @model
	 * @generated
	 */
	OperatorType getType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType
	 * @see #getType()
	 * @generated
	 */
	void setType(OperatorType value);

	/**
	 * Returns the value of the '<em><b>Guard</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Guard</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guard</em>' attribute.
	 * @see #setGuard(String)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getOperatorComponent_Guard()
	 * @model
	 * @generated
	 */
	String getGuard();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent#getGuard <em>Guard</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guard</em>' attribute.
	 * @see #getGuard()
	 * @generated
	 */
	void setGuard(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<UseCaseScenarioElement> getChildElements();

	/**
	 * @generated NOT
	 */
	public Action createAction(String description, ActionType type);
	
	/**
	 * @generated NOT
	 */
	public Action createAction(String description, ActionType type, int index);
	
	/**
	 * @generated NOT
	 */
	public Operator createConnection(String description, OperatorType type);
	
	/**
	 * @generated NOT
	 */
	public Operator createConnection(String description, OperatorType type, int index); 
 
	/**
	 * @generated NOT
	 */
	ScenarioReference createScenarioReference(String description,
			OperatorType type);
	
	/**
	 * @generated NOT
	 */
	ScenarioReference createScenarioReference(String description,
			OperatorType type, int index);

} // OperatorComponent
