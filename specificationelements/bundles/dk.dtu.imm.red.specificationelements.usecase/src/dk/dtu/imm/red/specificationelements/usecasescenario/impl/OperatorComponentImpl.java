/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario.impl;

import dk.dtu.imm.red.specificationelements.usecasescenario.Action;
import dk.dtu.imm.red.specificationelements.usecasescenario.ActionType;
import dk.dtu.imm.red.specificationelements.usecasescenario.Operator;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType;
import dk.dtu.imm.red.specificationelements.usecasescenario.ScenarioReference;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioFactory;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operator Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorComponentImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.OperatorComponentImpl#getGuard <em>Guard</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperatorComponentImpl extends UseCaseScenarioElementImpl implements OperatorComponent {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final OperatorType TYPE_EDEFAULT = OperatorType.SEQUENCE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected OperatorType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getGuard() <em>Guard</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuard()
	 * @generated
	 * @ordered
	 */
	protected static final String GUARD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGuard() <em>Guard</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuard()
	 * @generated
	 * @ordered
	 */
	protected String guard = GUARD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperatorComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasescenarioPackage.Literals.OPERATOR_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(OperatorType newType) {
		OperatorType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.OPERATOR_COMPONENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGuard() {
		return guard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGuard(String newGuard) {
		String oldGuard = guard;
		guard = newGuard;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.OPERATOR_COMPONENT__GUARD, oldGuard, guard));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UseCaseScenarioElement> getChildElements() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasescenarioPackage.OPERATOR_COMPONENT__TYPE:
				return getType();
			case UsecasescenarioPackage.OPERATOR_COMPONENT__GUARD:
				return getGuard();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasescenarioPackage.OPERATOR_COMPONENT__TYPE:
				setType((OperatorType)newValue);
				return;
			case UsecasescenarioPackage.OPERATOR_COMPONENT__GUARD:
				setGuard((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.OPERATOR_COMPONENT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case UsecasescenarioPackage.OPERATOR_COMPONENT__GUARD:
				setGuard(GUARD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.OPERATOR_COMPONENT__TYPE:
				return type != TYPE_EDEFAULT;
			case UsecasescenarioPackage.OPERATOR_COMPONENT__GUARD:
				return GUARD_EDEFAULT == null ? guard != null : !GUARD_EDEFAULT.equals(guard);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", guard: ");
		result.append(guard);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * @generated NOT
	 */
	private Action createActionType(String description, ActionType type) {
		Action act = UsecasescenarioFactory.eINSTANCE.createAction();
		act.setName(description);
		act.setType(type);
		act.setParent(this); 
		return act;
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public Action createAction(String description, ActionType type) {
		Action act = createActionType(description, type); 
		this.getChildElements().add(act);
		return act;
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public Action createAction(String description, ActionType type, int index) {
		Action act = createActionType(description, type); 
		
		if(index >= this.getChildElements().size() - 1) {
			this.getChildElements().add(act); 
		}
		else {
			this.getChildElements().add(index + 1,act); 
		}  
		 
		return act;
	}
	
	/**
	 * @generated NOT
	 */ 
	private Operator createOperatorType(String description, OperatorType type) {
		Operator con = UsecasescenarioFactory.eINSTANCE.createOperator();
		con.setName(description);
		con.setType(type); 
		con.setParent(this);
		return con;
	}
	
	/**
	 * @generated NOT
	 */ 
	private ScenarioReference createOperatorReferenceType(String description, OperatorType type) {
		ScenarioReference con = UsecasescenarioFactory.eINSTANCE.createUseCaseReference();
		con.setName(description);
		con.setType(type);  
		con.setParent(this); 
		return con;
	}
	

	/**
	 * @generated NOT
	 */
	@Override
	public ScenarioReference createScenarioReference(String description,
			OperatorType type) {
		ScenarioReference con = createOperatorReferenceType(description, type);  
		this.getChildElements().add(con); 
		return con;
	}	  
	
	/**
	 * @generated NOT
	 */
	@Override
	public ScenarioReference createScenarioReference(String description, OperatorType type, int index) {
		ScenarioReference con = createOperatorReferenceType(description, type);  
		addToConnections(index, con);   
		return con;
	}	 
	
	/**
	 * @generated NOT
	 */
	@Override
	public Operator createConnection(String description, OperatorType type) {
		Operator con = createOperatorType(description, type); 
		this.getChildElements().add(con);
		return con;
	} 
	
	/**
	 * @generated NOT
	 */
	@Override
	public Operator createConnection(String description, OperatorType type, int index) {
		Operator con = createOperatorType(description, type);  
		addToConnections(index, con);   
		return con;
	}  
	
	/**
	 * @generated NOT
	 */
	private void addToConnections(int index, OperatorComponent con) {
		if(index >= this.getChildElements().size() - 1) {
			this.getChildElements().add(con); 
		}
		else {
			this.getChildElements().add(index + 1, con); 
		}
	} 

} //OperatorComponentImpl
