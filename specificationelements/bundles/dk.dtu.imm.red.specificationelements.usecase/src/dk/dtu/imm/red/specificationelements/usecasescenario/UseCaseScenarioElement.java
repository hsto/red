/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case Scenario Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getDescription <em>Description</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getUniqueID <em>Unique ID</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseScenarioElement()
 * @model abstract="true"
 * @generated
 */
public interface UseCaseScenarioElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseScenarioElement_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(OperatorComponent)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseScenarioElement_Parent()
	 * @model
	 * @generated
	 */
	OperatorComponent getParent();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(OperatorComponent value);

	/**
	 * Returns the value of the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unique ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unique ID</em>' attribute.
	 * @see #setUniqueID(String)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseScenarioElement_UniqueID()
	 * @model id="true"
	 * @generated
	 */
	String getUniqueID();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getUniqueID <em>Unique ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unique ID</em>' attribute.
	 * @see #getUniqueID()
	 * @generated
	 */
	void setUniqueID(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseScenarioElement_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * @generated NOT
	 */
	void move(int index, OperatorComponent newParent);

	/**
	 * @generated NOT
	 */
	void remove();

	/**
	 * @generated NOT
	 */
	boolean isFromAReference();

} // UseCaseScenarioElement
