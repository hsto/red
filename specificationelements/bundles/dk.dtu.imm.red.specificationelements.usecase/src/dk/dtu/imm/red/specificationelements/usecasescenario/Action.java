/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action#getPictureData <em>Picture Data</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action#getPictureURI <em>Picture URI</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getAction()
 * @model
 * @generated
 */
public interface Action extends UseCaseScenarioElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.usecasescenario.ActionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.ActionType
	 * @see #setType(ActionType)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getAction_Type()
	 * @model
	 * @generated
	 */
	ActionType getType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.ActionType
	 * @see #getType()
	 * @generated
	 */
	void setType(ActionType value);

	/**
	 * Returns the value of the '<em><b>Picture Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Picture Data</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Picture Data</em>' attribute.
	 * @see #setPictureData(byte[])
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getAction_PictureData()
	 * @model
	 * @generated
	 */
	byte[] getPictureData();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action#getPictureData <em>Picture Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Picture Data</em>' attribute.
	 * @see #getPictureData()
	 * @generated
	 */
	void setPictureData(byte[] value);

	/**
	 * Returns the value of the '<em><b>Picture URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Picture URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Picture URI</em>' attribute.
	 * @see #setPictureURI(String)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getAction_PictureURI()
	 * @model
	 * @generated
	 */
	String getPictureURI();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.Action#getPictureURI <em>Picture URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Picture URI</em>' attribute.
	 * @see #getPictureURI()
	 * @generated
	 */
	void setPictureURI(String value);

} // Action
