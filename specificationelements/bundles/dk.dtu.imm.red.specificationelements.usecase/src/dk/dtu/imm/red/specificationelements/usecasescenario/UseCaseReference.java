/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario;

import dk.dtu.imm.red.specificationelements.usecase.UseCase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseReference#getUseCase <em>Use Case</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseReference()
 * @model
 * @generated
 */
public interface UseCaseReference extends ScenarioReference {
	/**
	 * Returns the value of the '<em><b>Use Case</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Case</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Case</em>' reference.
	 * @see #setUseCase(UseCase)
	 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage#getUseCaseReference_UseCase()
	 * @model
	 * @generated
	 */
	UseCase getUseCase();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseReference#getUseCase <em>Use Case</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Case</em>' reference.
	 * @see #getUseCase()
	 * @generated
	 */
	void setUseCase(UseCase value);

} // UseCaseReference
