/**
 */
package dk.dtu.imm.red.specificationelements.actor.impl;

import dk.dtu.imm.red.core.element.ComplexityType;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.properties.MonetaryUnit;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.ActorKind;
import dk.dtu.imm.red.specificationelements.actor.ActorPackage;
import dk.dtu.imm.red.specificationelements.actor.CodeType;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.actor.impl.ActorImpl#getId <em>Id</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.actor.impl.ActorImpl#getCode <em>Code</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.actor.impl.ActorImpl#getBehaviorElements <em>Behavior Elements</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.actor.impl.ActorImpl#getCodeType <em>Code Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActorImpl extends SpecificationElementImpl implements Actor {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBehaviorElements() <em>Behavior Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBehaviorElements()
	 * @generated
	 * @ordered
	 */
	protected EList<SpecificationElement> behaviorElements;

	/**
	 * The default value of the '{@link #getCodeType() <em>Code Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCodeType()
	 * @generated
	 * @ordered
	 */
	protected static final CodeType CODE_TYPE_EDEFAULT = CodeType.PROLOG;

	/**
	 * The cached value of the '{@link #getCodeType() <em>Code Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCodeType()
	 * @generated
	 * @ordered
	 */
	protected CodeType codeType = CODE_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActorPackage.Literals.ACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActorPackage.ACTOR__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActorPackage.ACTOR__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SpecificationElement> getBehaviorElements() {
		if (behaviorElements == null) {
			behaviorElements = new EObjectResolvingEList<SpecificationElement>(SpecificationElement.class, this, ActorPackage.ACTOR__BEHAVIOR_ELEMENTS);
		}
		return behaviorElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType getCodeType() {
		return codeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCodeType(CodeType newCodeType) {
		CodeType oldCodeType = codeType;
		codeType = newCodeType == null ? CODE_TYPE_EDEFAULT : newCodeType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActorPackage.ACTOR__CODE_TYPE, oldCodeType, codeType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ActorPackage.ACTOR__ID:
				return getId();
			case ActorPackage.ACTOR__CODE:
				return getCode();
			case ActorPackage.ACTOR__BEHAVIOR_ELEMENTS:
				return getBehaviorElements();
			case ActorPackage.ACTOR__CODE_TYPE:
				return getCodeType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ActorPackage.ACTOR__ID:
				setId((String)newValue);
				return;
			case ActorPackage.ACTOR__CODE:
				setCode((String)newValue);
				return;
			case ActorPackage.ACTOR__BEHAVIOR_ELEMENTS:
				getBehaviorElements().clear();
				getBehaviorElements().addAll((Collection<? extends SpecificationElement>)newValue);
				return;
			case ActorPackage.ACTOR__CODE_TYPE:
				setCodeType((CodeType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ActorPackage.ACTOR__ID:
				setId(ID_EDEFAULT);
				return;
			case ActorPackage.ACTOR__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case ActorPackage.ACTOR__BEHAVIOR_ELEMENTS:
				getBehaviorElements().clear();
				return;
			case ActorPackage.ACTOR__CODE_TYPE:
				setCodeType(CODE_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ActorPackage.ACTOR__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ActorPackage.ACTOR__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case ActorPackage.ACTOR__BEHAVIOR_ELEMENTS:
				return behaviorElements != null && !behaviorElements.isEmpty();
			case ActorPackage.ACTOR__CODE_TYPE:
				return codeType != CODE_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", code: ");
		result.append(code);
		result.append(", codeType: ");
		result.append(codeType);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {

		String kind = getElementKind();//do your own bounds checking
		if(kind!=null){
			if(kind.equals(ActorKind.ROLE.getLiteral())){
	        	return "icons/role.png";
			}
			else if(kind.equals(ActorKind.PERSON.getLiteral())){
	        	return "icons/actor.png";
			}
			else if(kind.equals(ActorKind.GROUP.getLiteral())){
	        	return "icons/group.png";
			}
			else if(kind.equals(ActorKind.TEAM.getLiteral())){
	        	return "icons/team.png";
			}
			else if(kind.equals(ActorKind.ORGANISATION.getLiteral())){
	        	return "icons/organisation.png";
			}
		}
		return "icons/actor.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ComplexityType getEstimatedComplexity() {
		return ComplexityType.LARGE;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {
		return new ElementColumnDefinition[] {
				new ElementColumnDefinition()
				.setHeaderName("ID")
				.setEMFLiteral(ActorPackage.Literals.ACTOR__ID)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Actor>() {

					@Override
					public Object getAttributeValue(Actor item) {
						return item.getId()==null?"":item.getId();
					}

					@Override
					public void setAttributeValue(Actor item, Object value) {
						item.setId((String) value);
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Actor>() {

					@Override
					public Object getAttributeValue(Actor item) {
						return item.getName()==null?"":item.getName();
					}

					@Override
					public void setAttributeValue(Actor item, Object value) {
						item.setName((String) value);
					}
				}),

				new ElementColumnDefinition()
				.setHeaderName("Kind")
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setCellItems(ElementColumnDefinition.enumToStringArray(ActorKind.values()))
				.setEditable(true)
				.setCellHandler(new CellHandler<Actor>() {

					@Override
					public Object getAttributeValue(Actor item) {
						return item.getElementKind()==null?"":item.getElementKind();
					}

					@Override
					public void setAttributeValue(Actor item, Object value) {
						item.setElementKind(value.toString().toUpperCase());
					}
					}
				)
				,
				new ElementColumnDefinition()
				.setHeaderName("Estimated Complexity")
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(false)
				.setCellHandler(new CellHandler<Actor>() {

					@Override
					public Object getAttributeValue(Actor item) {
						return item.getEstimatedComplexity().name()==null?"":item.getEstimatedComplexity().name().toString();
					}

					@Override
					public void setAttributeValue(Actor item, Object value) {

					}
				}
				)
				,
						new ElementColumnDefinition()
						.setHeaderName("Cost Value")
						.setColumnType(Double.class)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<Actor>() {

							@Override
							public Object getAttributeValue(Actor item) {
								return item.getCost().getValue();
							}

							@Override
							public void setAttributeValue(Actor item, Object value) {
								item.getCost().setValue(Double.parseDouble(value.toString()));
							}
						}),
						new ElementColumnDefinition()
						.setHeaderName("Cost Unit")
						.setColumnType(MonetaryUnit.class)
						.setColumnBehaviour(ColumnBehaviour.ComboSelector)
						.setCellItems(ElementColumnDefinition.enumToStringArray(MonetaryUnit.values()))
						.setEditable(true)
						.setCellHandler(new CellHandler<Actor>() {

							@Override
							public Object getAttributeValue(Actor item) {
								return item.getCost()==null?MonetaryUnit.DKK:item.getCost().getMonetaryUnit();
							}

							@Override
							public void setAttributeValue(Actor item, Object value) {
								item.getCost().setMonetaryUnit(MonetaryUnit.valueOf(value.toString().toUpperCase()));
							}
						}),
						new ElementColumnDefinition()
						.setHeaderName("Benefit Value")
						.setColumnType(Double.class)
						.setColumnBehaviour(ColumnBehaviour.CellEdit)
						.setEditable(true)
						.setCellHandler(new CellHandler<Actor>() {

							@Override
							public Object getAttributeValue(Actor item) {
								return item.getBenefit().getValue();
							}

							@Override
							public void setAttributeValue(Actor item, Object value) {
								item.getBenefit().setValue(Double.parseDouble(value.toString()));
							}
						}),
						new ElementColumnDefinition()
						.setHeaderName("Benefit Unit")
						.setColumnType(MonetaryUnit.class)
						.setColumnBehaviour(ColumnBehaviour.ComboSelector)
						.setCellItems(ElementColumnDefinition.enumToStringArray(MonetaryUnit.values()))
						.setEditable(true)
						.setCellHandler(new CellHandler<Actor>() {

							@Override
							public Object getAttributeValue(Actor item) {
								return item.getBenefit().getMonetaryUnit()==null?MonetaryUnit.DKK:item.getBenefit().getMonetaryUnit().toString();
							}

							@Override
							public void setAttributeValue(Actor item, Object value) {
								item.getBenefit().setMonetaryUnit(MonetaryUnit.valueOf(value.toString().toUpperCase()));
							}
						})
				};
	}
	
	@Override
	public String getStructeredIdType() {
		return "AC";
	}

} //ActorImpl
