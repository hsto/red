/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario.impl;

import dk.dtu.imm.red.specificationelements.usecase.UseCase;

import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseReference;
import dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecasescenario.impl.UseCaseReferenceImpl#getUseCase <em>Use Case</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UseCaseReferenceImpl extends ScenarioReferenceImpl implements UseCaseReference {
	/**
	 * The cached value of the '{@link #getUseCase() <em>Use Case</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseCase()
	 * @generated
	 * @ordered
	 */
	protected UseCase useCase;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCaseReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasescenarioPackage.Literals.USE_CASE_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCase getUseCase() {
		if (useCase != null && useCase.eIsProxy()) {
			InternalEObject oldUseCase = (InternalEObject)useCase;
			useCase = (UseCase)eResolveProxy(oldUseCase);
			if (useCase != oldUseCase) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UsecasescenarioPackage.USE_CASE_REFERENCE__USE_CASE, oldUseCase, useCase));
			}
		}
		return useCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCase basicGetUseCase() {
		return useCase;
	}
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setUseCase(UseCase newUsecase) {
		UseCase oldUsecase = useCase;
		useCase = newUsecase;
	 
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasescenarioPackage.USE_CASE_REFERENCE__USE_CASE, oldUsecase, useCase)); 
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_REFERENCE__USE_CASE:
				if (resolve) return getUseCase();
				return basicGetUseCase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_REFERENCE__USE_CASE:
				setUseCase((UseCase)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_REFERENCE__USE_CASE:
				setUseCase((UseCase)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasescenarioPackage.USE_CASE_REFERENCE__USE_CASE:
				return useCase != null;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();
		String placeholder = "Use Case Reference to: %s - %s - %s - %s";
		String name = getUseCase() != null ?  getUseCase().getName() != null ?  getUseCase().getName() : "None" : "None";
		String type = getType().toString();
		String refName = getName() != null ? getName() : "";
		String guard = getGuard() != null ? getGuard() : "";
		
		return String.format(placeholder, name, type, refName , guard);  
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario getScenario() { 
		if(useCase != null && useCase.getPrimaryScenarios().size() >0 ) {
			return useCase.getPrimaryScenarios().get(0);
		} else if (scenario != null) { // Added by Luai Issue #223
			return super.getScenario();
		} else {
			return null;
		} 
	};

} //UseCaseReferenceImpl
