/**
 */
package dk.dtu.imm.red.specificationelements.usecasescenario;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.usecasescenario.UsecasescenarioPackage
 * @generated
 */
public interface UsecasescenarioFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UsecasescenarioFactory eINSTANCE = dk.dtu.imm.red.specificationelements.usecasescenario.impl.UsecasescenarioFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Use Case Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Case Scenario</em>'.
	 * @generated
	 */
	UseCaseScenario createUseCaseScenario();

	/**
	 * Returns a new object of class '<em>Operator Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operator Component</em>'.
	 * @generated
	 */
	OperatorComponent createOperatorComponent();

	/**
	 * Returns a new object of class '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action</em>'.
	 * @generated
	 */
	Action createAction();

	/**
	 * Returns a new object of class '<em>Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operator</em>'.
	 * @generated
	 */
	Operator createOperator();

	/**
	 * Returns a new object of class '<em>Scenario Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scenario Reference</em>'.
	 * @generated
	 */
	ScenarioReference createScenarioReference();

	/**
	 * Returns a new object of class '<em>Use Case Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Case Reference</em>'.
	 * @generated
	 */
	UseCaseReference createUseCaseReference();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UsecasescenarioPackage getUsecasescenarioPackage();

	/**
	 * @generated NOT
	 */
	Operator createOperator(String description, OperatorType type); 

} //UsecasescenarioFactory
