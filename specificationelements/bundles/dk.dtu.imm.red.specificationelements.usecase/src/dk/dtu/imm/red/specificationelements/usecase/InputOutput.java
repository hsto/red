/**
 */
package dk.dtu.imm.red.specificationelements.usecase;

import dk.dtu.imm.red.core.element.Element;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Output</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.InputOutput#getInput <em>Input</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.InputOutput#getOutput <em>Output</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getInputOutput()
 * @model
 * @generated
 */
public interface InputOutput extends Element {
	/**
	 * Returns the value of the '<em><b>Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input</em>' attribute.
	 * @see #setInput(String)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getInputOutput_Input()
	 * @model
	 * @generated
	 */
	String getInput();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.InputOutput#getInput <em>Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input</em>' attribute.
	 * @see #getInput()
	 * @generated
	 */
	void setInput(String value);

	/**
	 * Returns the value of the '<em><b>Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output</em>' attribute.
	 * @see #setOutput(String)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getInputOutput_Output()
	 * @model
	 * @generated
	 */
	String getOutput();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.InputOutput#getOutput <em>Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output</em>' attribute.
	 * @see #getOutput()
	 * @generated
	 */
	void setOutput(String value);

} // InputOutput
