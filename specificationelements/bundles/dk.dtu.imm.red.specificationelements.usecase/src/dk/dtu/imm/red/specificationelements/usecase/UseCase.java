/**
 */
package dk.dtu.imm.red.specificationelements.usecase;

import dk.dtu.imm.red.core.properties.Property;

import dk.dtu.imm.red.core.text.Text;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getPreConditions <em>Pre Conditions</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getPostConditions <em>Post Conditions</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getParameter <em>Parameter</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getOutcome <em>Outcome</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getResult <em>Result</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getLongDescription <em>Long Description</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getIncidence <em>Incidence</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getDuration <em>Duration</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getInputOutputList <em>Input Output List</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getPrimaryScenarios <em>Primary Scenarios</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getPrimaryActors <em>Primary Actors</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getSecondaryScenarios <em>Secondary Scenarios</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getSecondaryActors <em>Secondary Actors</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase()
 * @model
 * @generated
 */
public interface UseCase extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Pre Conditions</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pre Conditions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Conditions</em>' attribute list.
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_PreConditions()
	 * @model unique="false"
	 * @generated
	 */
	EList<String> getPreConditions();

	/**
	 * Returns the value of the '<em><b>Post Conditions</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Post Conditions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Post Conditions</em>' attribute list.
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_PostConditions()
	 * @model unique="false"
	 * @generated
	 */
	EList<String> getPostConditions();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.usecase.UseCaseKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.usecase.UseCaseKind
	 * @see #setType(UseCaseKind)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_Type()
	 * @model
	 * @generated
	 */
	UseCaseKind getType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.usecase.UseCaseKind
	 * @see #getType()
	 * @generated
	 */
	void setType(UseCaseKind value);

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' attribute.
	 * @see #setParameter(String)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_Parameter()
	 * @model
	 * @generated
	 */
	String getParameter();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getParameter <em>Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' attribute.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(String value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' attribute.
	 * @see #setTrigger(String)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_Trigger()
	 * @model
	 * @generated
	 */
	String getTrigger();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getTrigger <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger</em>' attribute.
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(String value);

	/**
	 * Returns the value of the '<em><b>Outcome</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outcome</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outcome</em>' attribute.
	 * @see #setOutcome(String)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_Outcome()
	 * @model
	 * @generated
	 */
	String getOutcome();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getOutcome <em>Outcome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outcome</em>' attribute.
	 * @see #getOutcome()
	 * @generated
	 */
	void setOutcome(String value);

	/**
	 * Returns the value of the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result</em>' attribute.
	 * @see #setResult(String)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_Result()
	 * @model
	 * @generated
	 */
	String getResult();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getResult <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result</em>' attribute.
	 * @see #getResult()
	 * @generated
	 */
	void setResult(String value);

	/**
	 * Returns the value of the '<em><b>Long Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Long Description</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Long Description</em>' containment reference.
	 * @see #setLongDescription(Text)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_LongDescription()
	 * @model containment="true"
	 * @generated
	 */
	Text getLongDescription();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getLongDescription <em>Long Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Long Description</em>' containment reference.
	 * @see #getLongDescription()
	 * @generated
	 */
	void setLongDescription(Text value);

	/**
	 * Returns the value of the '<em><b>Incidence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incidence</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incidence</em>' containment reference.
	 * @see #setIncidence(Property)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_Incidence()
	 * @model containment="true"
	 * @generated
	 */
	Property getIncidence();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getIncidence <em>Incidence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Incidence</em>' containment reference.
	 * @see #getIncidence()
	 * @generated
	 */
	void setIncidence(Property value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' containment reference.
	 * @see #setDuration(Property)
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_Duration()
	 * @model containment="true"
	 * @generated
	 */
	Property getDuration();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.usecase.UseCase#getDuration <em>Duration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' containment reference.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(Property value);

	/**
	 * Returns the value of the '<em><b>Input Output List</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.usecase.InputOutput}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Output List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Output List</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_InputOutputList()
	 * @model containment="true"
	 * @generated
	 */
	EList<InputOutput> getInputOutputList();

	/**
	 * Returns the value of the '<em><b>Primary Scenarios</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primary Scenarios</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primary Scenarios</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_PrimaryScenarios()
	 * @model
	 * @generated
	 */
	EList<UseCaseScenario> getPrimaryScenarios();

	/**
	 * Returns the value of the '<em><b>Primary Actors</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.actor.Actor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primary Actors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primary Actors</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_PrimaryActors()
	 * @model
	 * @generated
	 */
	EList<Actor> getPrimaryActors();

	/**
	 * Returns the value of the '<em><b>Secondary Scenarios</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Secondary Scenarios</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Secondary Scenarios</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_SecondaryScenarios()
	 * @model
	 * @generated
	 */
	EList<UseCaseScenario> getSecondaryScenarios();

	/**
	 * Returns the value of the '<em><b>Secondary Actors</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.actor.Actor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Secondary Actors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Secondary Actors</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.usecase.UsecasePackage#getUseCase_SecondaryActors()
	 * @model
	 * @generated
	 */
	EList<Actor> getSecondaryActors();

} // UseCase
