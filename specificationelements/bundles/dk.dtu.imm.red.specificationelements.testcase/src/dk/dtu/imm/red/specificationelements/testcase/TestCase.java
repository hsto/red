/**
 */
package dk.dtu.imm.red.specificationelements.testcase;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.TestCase#getPreconditions <em>Preconditions</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.TestCase#getInput <em>Input</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.TestCase#getPostconditions <em>Postconditions</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.TestCase#getResult <em>Result</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.TestCase#getActionList <em>Action List</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.testcase.TestcasePackage#getTestCase()
 * @model
 * @generated
 */
public interface TestCase extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Preconditions</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preconditions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preconditions</em>' attribute.
	 * @see #setPreconditions(String)
	 * @see dk.dtu.imm.red.specificationelements.testcase.TestcasePackage#getTestCase_Preconditions()
	 * @model default=""
	 * @generated
	 */
	String getPreconditions();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.testcase.TestCase#getPreconditions <em>Preconditions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preconditions</em>' attribute.
	 * @see #getPreconditions()
	 * @generated
	 */
	void setPreconditions(String value);

	/**
	 * Returns the value of the '<em><b>Input</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input</em>' attribute.
	 * @see #setInput(String)
	 * @see dk.dtu.imm.red.specificationelements.testcase.TestcasePackage#getTestCase_Input()
	 * @model default=""
	 * @generated
	 */
	String getInput();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.testcase.TestCase#getInput <em>Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input</em>' attribute.
	 * @see #getInput()
	 * @generated
	 */
	void setInput(String value);

	/**
	 * Returns the value of the '<em><b>Postconditions</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Postconditions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postconditions</em>' attribute.
	 * @see #setPostconditions(String)
	 * @see dk.dtu.imm.red.specificationelements.testcase.TestcasePackage#getTestCase_Postconditions()
	 * @model default=""
	 * @generated
	 */
	String getPostconditions();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.testcase.TestCase#getPostconditions <em>Postconditions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postconditions</em>' attribute.
	 * @see #getPostconditions()
	 * @generated
	 */
	void setPostconditions(String value);

	/**
	 * Returns the value of the '<em><b>Result</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result</em>' attribute.
	 * @see #setResult(String)
	 * @see dk.dtu.imm.red.specificationelements.testcase.TestcasePackage#getTestCase_Result()
	 * @model default=""
	 * @generated
	 */
	String getResult();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.testcase.TestCase#getResult <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result</em>' attribute.
	 * @see #getResult()
	 * @generated
	 */
	void setResult(String value);

	/**
	 * Returns the value of the '<em><b>Action List</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.testcase.Action}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action List</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.testcase.TestcasePackage#getTestCase_ActionList()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<Action> getActionList();

} // TestCase
