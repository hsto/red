/**
 */
package dk.dtu.imm.red.specificationelements.testcase.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.specificationelements.testcase.Action;
import dk.dtu.imm.red.specificationelements.testcase.TestCase;
import dk.dtu.imm.red.specificationelements.testcase.TestCaseKind;
import dk.dtu.imm.red.specificationelements.testcase.TestcasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.impl.TestCaseImpl#getPreconditions <em>Preconditions</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.impl.TestCaseImpl#getInput <em>Input</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.impl.TestCaseImpl#getPostconditions <em>Postconditions</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.impl.TestCaseImpl#getResult <em>Result</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.testcase.impl.TestCaseImpl#getActionList <em>Action List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestCaseImpl extends SpecificationElementImpl implements TestCase {
	/**
	 * The default value of the '{@link #getPreconditions() <em>Preconditions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreconditions()
	 * @generated
	 * @ordered
	 */
	protected static final String PRECONDITIONS_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getPreconditions() <em>Preconditions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreconditions()
	 * @generated
	 * @ordered
	 */
	protected String preconditions = PRECONDITIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getInput() <em>Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput()
	 * @generated
	 * @ordered
	 */
	protected static final String INPUT_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getInput() <em>Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput()
	 * @generated
	 * @ordered
	 */
	protected String input = INPUT_EDEFAULT;

	/**
	 * The default value of the '{@link #getPostconditions() <em>Postconditions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostconditions()
	 * @generated
	 * @ordered
	 */
	protected static final String POSTCONDITIONS_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getPostconditions() <em>Postconditions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostconditions()
	 * @generated
	 * @ordered
	 */
	protected String postconditions = POSTCONDITIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getResult() <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected static final String RESULT_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getResult() <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected String result = RESULT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getActionList() <em>Action List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionList()
	 * @generated
	 * @ordered
	 */
	protected EList<Action> actionList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestcasePackage.Literals.TEST_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPreconditions() {
		return preconditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreconditions(String newPreconditions) {
		String oldPreconditions = preconditions;
		preconditions = newPreconditions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.TEST_CASE__PRECONDITIONS, oldPreconditions, preconditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInput() {
		return input;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput(String newInput) {
		String oldInput = input;
		input = newInput;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.TEST_CASE__INPUT, oldInput, input));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPostconditions() {
		return postconditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostconditions(String newPostconditions) {
		String oldPostconditions = postconditions;
		postconditions = newPostconditions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.TEST_CASE__POSTCONDITIONS, oldPostconditions, postconditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResult() {
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResult(String newResult) {
		String oldResult = result;
		result = newResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.TEST_CASE__RESULT, oldResult, result));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Action> getActionList() {
		if (actionList == null) {
			actionList = new EObjectContainmentEList.Resolving<Action>(Action.class, this, TestcasePackage.TEST_CASE__ACTION_LIST);
		}
		return actionList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE__ACTION_LIST:
				return ((InternalEList<?>)getActionList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE__PRECONDITIONS:
				return getPreconditions();
			case TestcasePackage.TEST_CASE__INPUT:
				return getInput();
			case TestcasePackage.TEST_CASE__POSTCONDITIONS:
				return getPostconditions();
			case TestcasePackage.TEST_CASE__RESULT:
				return getResult();
			case TestcasePackage.TEST_CASE__ACTION_LIST:
				return getActionList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE__PRECONDITIONS:
				setPreconditions((String)newValue);
				return;
			case TestcasePackage.TEST_CASE__INPUT:
				setInput((String)newValue);
				return;
			case TestcasePackage.TEST_CASE__POSTCONDITIONS:
				setPostconditions((String)newValue);
				return;
			case TestcasePackage.TEST_CASE__RESULT:
				setResult((String)newValue);
				return;
			case TestcasePackage.TEST_CASE__ACTION_LIST:
				getActionList().clear();
				getActionList().addAll((Collection<? extends Action>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE__PRECONDITIONS:
				setPreconditions(PRECONDITIONS_EDEFAULT);
				return;
			case TestcasePackage.TEST_CASE__INPUT:
				setInput(INPUT_EDEFAULT);
				return;
			case TestcasePackage.TEST_CASE__POSTCONDITIONS:
				setPostconditions(POSTCONDITIONS_EDEFAULT);
				return;
			case TestcasePackage.TEST_CASE__RESULT:
				setResult(RESULT_EDEFAULT);
				return;
			case TestcasePackage.TEST_CASE__ACTION_LIST:
				getActionList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE__PRECONDITIONS:
				return PRECONDITIONS_EDEFAULT == null ? preconditions != null : !PRECONDITIONS_EDEFAULT.equals(preconditions);
			case TestcasePackage.TEST_CASE__INPUT:
				return INPUT_EDEFAULT == null ? input != null : !INPUT_EDEFAULT.equals(input);
			case TestcasePackage.TEST_CASE__POSTCONDITIONS:
				return POSTCONDITIONS_EDEFAULT == null ? postconditions != null : !POSTCONDITIONS_EDEFAULT.equals(postconditions);
			case TestcasePackage.TEST_CASE__RESULT:
				return RESULT_EDEFAULT == null ? result != null : !RESULT_EDEFAULT.equals(result);
			case TestcasePackage.TEST_CASE__ACTION_LIST:
				return actionList != null && !actionList.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (preconditions: ");
		result.append(preconditions);
		result.append(", input: ");
		result.append(input);
		result.append(", postconditions: ");
		result.append(postconditions);
		result.append(", result: ");
		result.append(result);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/test_case.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		return super.search(param, attributesToSearch);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {
		return new ElementColumnDefinition[] {
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<TestCase>() {

					@Override
					public Object getAttributeValue(TestCase item) {
						return item.getName()==null?"":item.getName();
					}

					@Override
					public void setAttributeValue(TestCase item, Object value) {
						item.setName((String) value);
					}
				}),
//				new ElementColumnDefinition()
//				.setHeaderName("Test Case Name")
//				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
//				.setColumnBehaviour(ColumnBehaviour.CellEdit)
//				.setEditable(true)
//				.setCellHandler(new CellHandler<TestCase>() {
//
//					@Override
//					public Object getAttributeValue(TestCase item) {
//						return item.getName();
//					}
//
//					@Override
//					public void setAttributeValue(TestCase item, Object value) {
//						item.setName((String) value);
//					}
//				}),
				new ElementColumnDefinition()
				.setHeaderName("Test Case Kind")
				.setEMFLiteral(TestcasePackage.Literals.TEST_CASE_KIND)
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setCellItems(ElementColumnDefinition.enumToStringArray(TestCaseKind.values()))
				.setEditable(true)
				.setCellHandler(new CellHandler<TestCase>() {

					@Override
					public Object getAttributeValue(TestCase item) {
						return item.getElementKind()==null?"":item.getElementKind().toString();
					}

					@Override
					public void setAttributeValue(TestCase item, Object value) {
						item.setElementKind(value.toString().toUpperCase());
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Pre-Conditions")
				.setEMFLiteral(TestcasePackage.Literals.TEST_CASE__PRECONDITIONS)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<TestCase>() {

					@Override
					public Object getAttributeValue(TestCase item) {
						return item.getPreconditions()==null?"":item.getPreconditions();
					}

					@Override
					public void setAttributeValue(TestCase item, Object value) {
						item.setPreconditions((String) value);
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Input")
				.setEMFLiteral(TestcasePackage.Literals.TEST_CASE__INPUT)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<TestCase>() {

					@Override
					public Object getAttributeValue(TestCase item) {
						return item.getInput()==null?"":item.getInput();
					}

					@Override
					public void setAttributeValue(TestCase item, Object value) {
						item.setInput((String) value);
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Post Condition")
				.setEMFLiteral(TestcasePackage.Literals.TEST_CASE__POSTCONDITIONS)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<TestCase>() {

					@Override
					public Object getAttributeValue(TestCase item) {
						return item.getPostconditions()==null?"":item.getPostconditions();
					}

					@Override
					public void setAttributeValue(TestCase item, Object value) {
						item.setPostconditions((String) value);
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Result")
				.setEMFLiteral(TestcasePackage.Literals.TEST_CASE__RESULT)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<TestCase>() {

					@Override
					public Object getAttributeValue(TestCase item) {
						return item.getResult()==null?"":item.getResult();
					}

					@Override
					public void setAttributeValue(TestCase item, Object value) {
						item.setResult((String) value);
					}
				})
		};
	}

} //TestCaseImpl
