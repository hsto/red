package dk.dtu.imm.red.specificationelements.testcase.presentation.editors;

import java.util.List;

import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.testcase.Action;
import dk.dtu.imm.red.specificationelements.testcase.TestCase;

public class TestCasePresenter extends BaseEditorPresenter {

	static final int MAX_GOAL_LENGTH = 69;
	static final int MAX_ID_LENGTH = 5;

	protected String preConditions;
	protected String input;
	protected List<Action> actions;
	protected String postConditions;
	protected String result;

	public TestCasePresenter(TestCase element) {
		super(element);
		this.element = element;
		actions = element.getActionList();
	}
	
	@Override
	public void save() {
		super.save();
		TestCase testCase = (TestCase) element;

		testCase.setPreconditions(preConditions);
		testCase.setInput(input);
		
		testCase.setPostconditions(postConditions);
		testCase.setResult(result);

		testCase.save();
	}

//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}

//	public TestCaseType getType() {
//		return type;
//	}
//
//	public void setType(TestCaseType type) {
//		this.type = type;
//	}


	public String getPreConditions() {
		return preConditions;
	}

	public void setPreConditions(String preConditions) {
		this.preConditions = preConditions;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public String getPostConditions() {
		return postConditions;
	}

	public void setPostConditions(String postConditions) {
		this.postConditions = postConditions;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
}
