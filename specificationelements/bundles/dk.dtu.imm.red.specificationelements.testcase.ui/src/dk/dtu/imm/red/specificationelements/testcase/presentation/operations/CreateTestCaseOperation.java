package dk.dtu.imm.red.specificationelements.testcase.presentation.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.testcase.TestCase;
import dk.dtu.imm.red.specificationelements.testcase.TestcaseFactory;
import dk.dtu.imm.red.specificationelements.testcase.presentation.extensions.TestCaseExtension;

public class CreateTestCaseOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected Group parent;
//	protected String path;
	protected TestCase testCase;

	protected TestCaseExtension extension;

	public CreateTestCaseOperation(String label, String name, String description, Group parent, String path) {
		super("Create Test Case");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
//		this.path = path;

		extension = new TestCaseExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		testCase = TestcaseFactory.eINSTANCE.createTestCase();
		testCase.setCreator(PreferenceUtil.getUserPreference_User());

		testCase.setLabel(label);
		testCase.setName(name);
		testCase.setDescription(description);

		if (parent != null) {
			testCase.setParent(parent);
		}

		testCase.save();
		extension.openElement(testCase);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if (parent != null) {
			parent.getContents().remove(testCase);
			extension.deleteElement(testCase);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}
	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
