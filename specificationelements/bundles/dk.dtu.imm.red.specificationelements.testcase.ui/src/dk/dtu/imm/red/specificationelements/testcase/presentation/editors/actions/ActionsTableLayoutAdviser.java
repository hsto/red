package dk.dtu.imm.red.specificationelements.testcase.presentation.editors.actions;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ScalableColumnLayoutAdvisor;

public class ActionsTableLayoutAdviser extends ScalableColumnLayoutAdvisor{


	private static final int ID_COLUMN = 0;
	private static final int DESCRIPTION_COLUMN = 1;
	private static final int NUM_COLUMNS = 2;
	private int rowCount;
	
	public ActionsTableLayoutAdviser(AgileGrid agileGrid) {
		super(agileGrid);

		rowCount = 0;
	}
	
	@Override
	public int getColumnCount() {
		// Date, author, comment and version
		return NUM_COLUMNS;
	}

	@Override
	public String getTopHeaderLabel(final int col) {
		switch (col) {
		case DESCRIPTION_COLUMN:
			return "Description";
		case ID_COLUMN:
			return "ID";
		default:
			return super.getTopHeaderLabel(col);
		}
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	@Override
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	@Override
	public int getInitialColumnWidth(int column) {
		switch (column) {
			case ID_COLUMN:
				return 10;
			case DESCRIPTION_COLUMN:
				return 90;
			default:
				return 0;
		}
	}
}
