package dk.dtu.imm.red.specificationelements.testcase.presentation.wizards;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.specificationelements.testcase.presentation.operations.CreateTestCaseOperation;

public class NewTestCaseWizardPresenter extends BaseWizardPresenter {

	public NewTestCaseWizardPresenter(final NewTestCaseWizard newTestCaseWizard) {
		super();
	}

	public void wizardFinished(String label, String name, String description, Group parent, String path) {
		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
				.getOperationSupport();
		
		CreateTestCaseOperation operation = 
				new CreateTestCaseOperation(label, name, description, parent, path);

		operation.addContext(operationSupport.getUndoContext());
		
		IAdaptable info = new IAdaptable() {
			@Override
			public Object getAdapter(@SuppressWarnings("rawtypes") final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		try {
			OperationHistoryFactory.getOperationHistory().execute(operation, null,
					info);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

}
