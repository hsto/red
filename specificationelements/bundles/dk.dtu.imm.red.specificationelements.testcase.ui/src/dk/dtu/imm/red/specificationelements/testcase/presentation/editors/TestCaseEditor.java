package dk.dtu.imm.red.specificationelements.testcase.presentation.editors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.Cell;
import org.agilemore.agilegrid.DefaultCellEditorProvider;
import org.agilemore.agilegrid.ISelectionChangedListener;
import org.agilemore.agilegrid.SWTX;
import org.agilemore.agilegrid.SelectionChangedEvent;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.testcase.Action;
import dk.dtu.imm.red.specificationelements.testcase.TestCase;
import dk.dtu.imm.red.specificationelements.testcase.TestCaseKind;
import dk.dtu.imm.red.specificationelements.testcase.TestcaseFactory;
import dk.dtu.imm.red.specificationelements.testcase.presentation.editors.actions.ActionsTableContentProvider;
import dk.dtu.imm.red.specificationelements.testcase.presentation.editors.actions.ActionsTableLayoutAdviser;

/**
 * The editor for creating and editing a test case
 *
 * @author Maciej Kucharek
 *
 */
public class TestCaseEditor extends BaseEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.testcase.editors.testcase";

	protected ElementBasicInfoContainer basicInfoContainer;
	
	private Text preconditionsText;
	private Text inputText;
	private AgileGrid actionsTable;
	private Text postconditionsText;
	private Text resultText;

	private DefaultCellEditorProvider actionsTableCellEditorProvider;
	private ActionsTableContentProvider actionsTableContentProvider;
	private ActionsTableLayoutAdviser actionsTableLayoutAdvisor;

	private Button addActionButton;
	private Button deleteActionButton;

	/**
	 * Index of the goal information page.
	 */
	protected int testCaseInfoPageIndex = 0;

	@Override
	public void doSave(IProgressMonitor monitor) {

		TestCasePresenter testCasePresenter = (TestCasePresenter) presenter;

		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());

		testCasePresenter.setPreConditions(preconditionsText.getText());
		testCasePresenter.setInput(inputText.getText());
		testCasePresenter.setPostConditions(postconditionsText.getText());
		testCasePresenter.setResult(resultText.getText());

		super.doSave(monitor);

	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		super.init(site, input);
		presenter = new TestCasePresenter((TestCase) element);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	protected void createPages() {
		testCaseInfoPageIndex = addPage(createTestCasePage(getContainer()));
		setPageText(testCaseInfoPageIndex, "Summary");
		super.createPages();

		fillInitialData();
	}

	private Composite createTestCasePage(final Composite parent) {

		parent.setLayout(new FillLayout());

		final Composite composite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(2, false);

		composite.setLayout(layout);

		GridData informationGridData = new GridData();
		informationGridData.horizontalAlignment = SWT.FILL;
		informationGridData.grabExcessHorizontalSpace = true;
		informationGridData.horizontalSpan=2;
		informationGridData.verticalSpan=1;

		Composite informationContainer = createTestCaseInformationContainer(
				composite, SWT.BORDER);
		informationContainer.setLayoutData(informationGridData);


		GridData descriptionGridData = new GridData();
		descriptionGridData.heightHint = 120;
		descriptionGridData.minimumWidth = 700;
		descriptionGridData.grabExcessHorizontalSpace = true;
		descriptionGridData.horizontalSpan = 2;
		descriptionGridData.horizontalAlignment = SWT.FILL;

		GridData actionsGridData = new GridData();
		actionsGridData.heightHint = 170;
		actionsGridData.minimumWidth = 700;
		actionsGridData.grabExcessHorizontalSpace = true;
		actionsGridData.horizontalSpan = 2;
		actionsGridData.horizontalAlignment = SWT.FILL;

		GridData otherGridData = new GridData();
		otherGridData.heightHint = 70;
		otherGridData.minimumWidth = 700;
		otherGridData.grabExcessHorizontalSpace = true;
		otherGridData.horizontalSpan = 2;
		otherGridData.horizontalAlignment = SWT.FILL;

		createTestCasePreconditionsContainer(composite, SWT.BORDER).setLayoutData(otherGridData);
		createTestCaseInputContainer(composite, SWT.BORDER).setLayoutData(otherGridData);
		createTestCaseActionsContainer(composite, SWT.BORDER).setLayoutData(actionsGridData);
		createTestCasePostconditionsContainer(composite, SWT.BORDER).setLayoutData(otherGridData);
		createTestCaseResultContainer(composite, SWT.BORDER).setLayoutData(otherGridData);

		GridData settingsGridData = new GridData();
		settingsGridData.heightHint = 200;
		settingsGridData.widthHint = 700;
		settingsGridData.horizontalSpan = 2;

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	private Composite createTestCasePreconditionsContainer(final Composite parent,
			final int style) {

		Group container = new Group(parent, SWT.NONE);
		container.setText("Pre-conditions");

		GridLayout layout = new GridLayout();
		container.setSize(400, 400);
		layout.numColumns = 1;
		layout.marginHeight = 5;
		layout.marginWidth = 5;
		container.setLayout(layout);

		preconditionsText = new Text(container, SWT.BORDER | SWT.MULTI);
		preconditionsText.setLayoutData(textFillAllLayoutData);
		preconditionsText.addListener(SWT.Modify, modifyListener);

		return container;
	}

	private Composite createTestCaseInputContainer(final Composite parent,
			final int style) {

		Group container = new Group(parent, SWT.NONE);
		container.setText("Input");

		GridLayout layout = new GridLayout();
		container.setSize(400, 400);
		layout.numColumns = 1;
		layout.marginHeight = 5;
		layout.marginWidth = 5;
		container.setLayout(layout);

		inputText = new Text(container, SWT.BORDER | SWT.MULTI);
		inputText.setLayoutData(textFillAllLayoutData);
		inputText.addListener(SWT.Modify, modifyListener);

		return container;
	}

	private Composite createTestCaseActionsContainer(final Composite parent,
			final int style) {

		List<Action> actions = ((TestCasePresenter) presenter).getActions();

		Group tcContainer = new Group(parent, SWT.NONE);
		tcContainer.setText("Actions");

		GridLayout tcLayout = new GridLayout(3, false);
		tcContainer.setLayout(tcLayout);

		GridData tableData = new GridData();
		tableData.grabExcessHorizontalSpace = true;
		tableData.grabExcessVerticalSpace = true;
		tableData.verticalAlignment = SWT.FILL;
		tableData.horizontalAlignment = SWT.FILL;
		tableData.horizontalSpan = 3;

		actionsTable = new AgileGrid(tcContainer, SWT.MULTI
				| SWTX.ROW_SELECTION | SWT.V_SCROLL);
		actionsTable.setLayoutData(tableData);

		actionsTableLayoutAdvisor = new ActionsTableLayoutAdviser(
				actionsTable);
		actionsTableContentProvider = new ActionsTableContentProvider(this);
		actionsTableCellEditorProvider = new DefaultCellEditorProvider(
				actionsTable);

		actionsTableContentProvider.setContent(actions);

		actionsTable.setLayoutAdvisor(actionsTableLayoutAdvisor);
		actionsTable.setContentProvider(actionsTableContentProvider);
		actionsTable.setCellEditorProvider(actionsTableCellEditorProvider);

		actionsTableLayoutAdvisor.setRowCount(actions.size());
		actionsTable.redraw();

		actionsTable
				.addSelectionChangedListener(new ISelectionChangedListener() {
					@Override
					public void selectionChanged(
							final SelectionChangedEvent arg0) {
						if (actionsTable.getCellSelection().length > 0) {
							deleteActionButton.setEnabled(true);
						} else {
							deleteActionButton.setEnabled(false);
						}
					}
				});

		addActionButton = new Button(tcContainer, SWT.NONE);
		addActionButton.setText("Add");
		addActionButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				Action action = TestcaseFactory.eINSTANCE.createAction();
				action.setActionId(String.valueOf(((TestCasePresenter) presenter).getActions().size() + 1));
				action.setDescription("");

				((TestCasePresenter) presenter).getActions().add(action);

				markAsDirty();
				actionsTableLayoutAdvisor.setRowCount(((TestCasePresenter) presenter).getActions().size());
				actionsTable.redraw();
			}
		});

		deleteActionButton = new Button(tcContainer, SWT.NONE);
		deleteActionButton.setText("Delete");
		deleteActionButton.setEnabled(false);
		deleteActionButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				Cell[] selectedCells = actionsTable.getCellSelection();

				for (int i = selectedCells.length-1; i >= 0; --i) {
					((TestCasePresenter) presenter).getActions().remove(selectedCells[i].row);
				}

				markAsDirty();
				actionsTableLayoutAdvisor.setRowCount(((TestCasePresenter) presenter).getActions().size());
				actionsTable.clearSelection();
				deleteActionButton.setEnabled(false);
				actionsTable.redraw();
			}

		});
		return tcContainer;
	}

	private Composite createTestCasePostconditionsContainer(final Composite parent,
			final int style) {

		Group container = new Group(parent, SWT.NONE);
		container.setText("Post-conditions");

		GridLayout layout = new GridLayout();
		container.setSize(400, 400);
		layout.numColumns = 1;
		layout.marginHeight = 5;
		layout.marginWidth = 5;
		container.setLayout(layout);

		postconditionsText = new Text(container, SWT.BORDER | SWT.MULTI);
		postconditionsText.setLayoutData(textFillAllLayoutData);
		postconditionsText.addListener(SWT.Modify, modifyListener);

		return container;
	}

	private Composite createTestCaseResultContainer(final Composite parent,
			final int style) {

		Group container = new Group(parent, SWT.NONE);
		container.setText("Result");

		GridLayout layout = new GridLayout();
		container.setSize(400, 400);
		layout.numColumns = 1;
		layout.marginHeight = 5;
		layout.marginWidth = 5;
		container.setLayout(layout);

		resultText = new Text(container, SWT.BORDER | SWT.MULTI);
		resultText.setLayoutData(textFillAllLayoutData);
		resultText.addListener(SWT.Modify, modifyListener);

		return container;
	}

	private Composite createTestCaseInformationContainer(final Composite parent,
			final int style) {

		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (SpecificationElement) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);

//		Group container = new Group(parent, SWT.NONE);
//		container.setText("Information");
//		GridLayout layout = new GridLayout();
//		layout.numColumns = 4;
//		container.setLayout(layout);
//
//		GridData idLayoutData = new GridData();
//		idLayoutData.heightHint = 25;
//		idLayoutData.widthHint = 50;
//
//		Label idLabel = new Label(container, SWT.NONE);
//		idLabel.setText("ID");
//		idLabel.setLayoutData(idLayoutData);
//
//		new Label(container, SWT.NONE);
//
//		GridData testCaseLayoutData = new GridData();
//		testCaseLayoutData.heightHint = 25;
//		testCaseLayoutData.widthHint = 500;
//
//		Label nameLabel = new Label(container, SWT.NONE);
//		nameLabel.setText("Test Case Name");
//		nameLabel.setLayoutData(testCaseLayoutData);
//
//		Label testCaseTypeLabel = new Label(container, SWT.NONE);
//		testCaseTypeLabel.setText("Test Case Type");
//
//		idTextBox = new Text(container, SWT.BORDER);
//		idTextBox.setLayoutData(idLayoutData);
//		idTextBox.setTextLimit(TestCasePresenter.MAX_ID_LENGTH);
//		idTextBox.addListener(SWT.Modify, modifyListener);
//
//		new Label(container, SWT.NONE);
//
//		nameTextBox = new Text(container, SWT.BORDER);
//		nameTextBox.setLayoutData(testCaseLayoutData);
//		nameTextBox.setTextLimit(TestCasePresenter.MAX_GOAL_LENGTH);
//		nameTextBox.addListener(SWT.Modify, modifyListener);
//
//		GridData testCaseTypeLayoutData = new GridData();
//		testCaseLayoutData.horizontalAlignment = SWT.RIGHT;
//
//		testCaseTypeComboBox = new Combo(container, SWT.READ_ONLY);
////		for (TestCaseType tct : TestCaseType.values()){
////			testCaseTypeComboBox.add(tct.getName());
////		}
//
//
//		testCaseTypeComboBox.addListener(SWT.Modify, modifyListener);
//		testCaseTypeComboBox.setLayoutData(testCaseTypeLayoutData);


		return composite;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Goal.html";
	}

	@Override
	protected void fillInitialData() {
		TestCase testCase = (TestCase) element;

		TestCaseKind[] kinds = TestCaseKind.values();
		String[] kindStrings = new String[kinds.length];
		for(int i=0;i<kinds.length;i++){
			kindStrings[i] = kinds[i].getName();
		}

		basicInfoContainer.fillInitialData(testCase, kindStrings);


		if (testCase.getPreconditions() != null) {
			preconditionsText.setText(testCase.getPreconditions());
		}

		if (testCase.getInput() != null) {
			inputText.setText(testCase.getInput());
		}

		if (!testCase.getActionList().isEmpty()) {
			((TestCasePresenter) presenter).setActions(testCase.getActionList());
		}

		if (testCase.getPostconditions() != null) {
			postconditionsText.setText(testCase.getPostconditions());
		}

		if (testCase.getResult() != null) {
			resultText.setText(testCase.getResult());
		}

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();
		// put all the attributes which we want to search through
		// into the attributesToSearchMap
		super.addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);

		// Now, search through all attributes, recording all search hits along the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()-1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(), result.getEndIndex());
			}
		}

		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}

	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,testCaseInfoPageIndex,basicInfoContainer);

	}

	@Override
	public String getEditorID() {
		return ID;
	}

	private final static GridData textFillAllLayoutData;
	static {
		textFillAllLayoutData = new GridData();
		textFillAllLayoutData.widthHint = 700;
		textFillAllLayoutData.horizontalAlignment = SWT.FILL;
		textFillAllLayoutData.verticalAlignment = SWT.FILL;
		textFillAllLayoutData.grabExcessHorizontalSpace = true;
		textFillAllLayoutData.grabExcessVerticalSpace = true;
	}

}
