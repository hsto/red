package dk.dtu.imm.red.specificationelements.testcase.presentation.editors.actions;

import java.util.List;

import org.agilemore.agilegrid.DefaultContentProvider;

import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.specificationelements.testcase.Action;

public class ActionsTableContentProvider extends DefaultContentProvider {

	private static final int DESCRIPTION_COLUMN = 1;
	private static final int ID_COLUMN = 0;

	private List<Action> actionList;
	
	private BaseEditor editor;


	public ActionsTableContentProvider(BaseEditor baseEditor) {
		super();
		this.editor = baseEditor;
	}

	public void setContent(List<Action> actions) {
		this.actionList = actions;
	}

	@Override
	public void doSetContentAt(int row, int col, Object value) {
		Action action = actionList.get(row);

		if (action == null) {
			super.doSetContentAt(row, col, value);
		}

		switch (col) {
			case DESCRIPTION_COLUMN:
				action.setDescription((String) value);
				break;
			case ID_COLUMN:
				action.setActionId((String) value);
				break;
			default:
				doSetContentAt(row, col, value);
		}
		
		editor.markAsDirty();
	}

	@Override
	public Object doGetContentAt(int row, int col) {

		Action action = actionList.get(row);

		if (action == null) {
			return super.doGetContentAt(row, col);
		}

		switch (col) {
			case DESCRIPTION_COLUMN:
				return action.getDescription();
			case ID_COLUMN:
				return action.getActionId();
			default:
				return doGetContentAt(row, col);
		}
	}
}
