package dk.dtu.imm.red.specificationelements.testcase.presentation.editors;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.testcase.TestCase;

public class TestCaseEditorInput extends BaseEditorInput {

	public TestCaseEditorInput(TestCase testCase) {
		super(testCase);
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Test Case editor";
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return "This is the test case editor";
	}
}
