package dk.dtu.imm.red.specificationelements.testcase.presentation.wizards;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.ui.wizards.ElementBasicInfoPage;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.testcase.TestCase;

public class NewTestCaseWizard extends BaseNewWizard {

//	protected ElementBasicInfoPage defineTestCasePage;

	String ID = "dk.dtu.imm.red.specificationelements.testcase.wizards.newtestcase";
	
	protected NewTestCaseWizardPresenter presenter;
	
	public NewTestCaseWizard() {
		super("Test Case", TestCase.class);
		presenter = new NewTestCaseWizardPresenter(this);
//		defineTestCasePage = new ElementBasicInfoPage("Test Case");
	}

	@Override
	public void addPages() {
//		addPage(defineTestCasePage);
		super.addPages();
	}

	@Override
	public boolean performFinish() {
		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription(); 

		Group parent = null;

		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();

			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}

		presenter.wizardFinished(label, name, description, parent, "");

		return true;
	}

	public void setName(final String text) {
		displayInfoPage.setDisplayName(text);
	}
	
	@Override
	public boolean canFinish() {
		return super.canFinish() 
				&& elementBasicInfoPage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}
}
