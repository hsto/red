package dk.dtu.imm.red.specificationelements.vision.ui.editors;

import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;

public interface VisionPresenter extends IEditorPresenter{
	
	void setVision(String text);
	
	void setExpectedValueOutcome(String text);
	
	void setOpportunities(String text);
	
	void setChallenges(String text);
	
	void setSponsors(String text);
	
	void setAdversaries(String text);
	
	void setGroupValuesAndCom(String text);
	
	void setBeliefsEditor(String text);
	
	int getTitleMaxLength();
	
}
