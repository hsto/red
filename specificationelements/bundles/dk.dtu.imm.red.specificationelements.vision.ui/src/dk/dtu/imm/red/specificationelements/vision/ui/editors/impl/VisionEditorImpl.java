package dk.dtu.imm.red.specificationelements.vision.ui.editors.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.specificationelements.vision.VisionPackage;
import dk.dtu.imm.red.specificationelements.vision.ui.editors.VisionEditor;
import dk.dtu.imm.red.specificationelements.vision.ui.editors.VisionPresenter;

public class VisionEditorImpl extends BaseEditor implements VisionEditor {


	protected ElementBasicInfoContainer basicInfoContainer;
	
	/**
	 * The rich text editor for vision
	 */
	protected RichTextEditor visionEditor;
	protected RichTextEditor expectedValueOutcomeEditor;
	protected Text opportunitiesEditor;
	protected Text challengesEditor;
	protected Text sponsorsEditor;
	protected Text adversariesEditor;
	protected Text groupValuesAndComEditor;
	protected Text beliefsEditor;

	/**
	 * Index of the vision page.
	 */
	protected int visionPageIndex = 0;

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doSave(IProgressMonitor monitor) {

		VisionPresenter presenter = (VisionPresenter) this.presenter;
		//set the spec elements basic info into the presenter
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		presenter.setVision(visionEditor.getText());
		presenter.setExpectedValueOutcome(expectedValueOutcomeEditor.getText());
		presenter.setOpportunities(opportunitiesEditor.getText());
		presenter.setChallenges(challengesEditor.getText());
		presenter.setSponsors(sponsorsEditor.getText());
		presenter.setAdversaries(adversariesEditor.getText());
		presenter.setGroupValuesAndCom(groupValuesAndComEditor.getText());
		presenter.setBeliefsEditor(beliefsEditor.getText());

		super.doSave(monitor);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);

		presenter = new VisionPresenterImpl(this, (Vision) element);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	protected void createPages() {
		visionPageIndex = addPage(createVisionPage(getContainer()));
		setPageText(visionPageIndex, "Summary");
		super.createPages();

		fillInitialData();
	}

	private Composite createVisionPage(Composite parent) {

		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(2, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=2;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (SpecificationElement) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);

		
		// Mission
		
		Label visionTextLabel = new Label(composite, SWT.NONE);
		visionTextLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 1));
		visionTextLabel.setText("Mission:");

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalSpan = 2;
		editorLayoutData.widthHint = 400;
		editorLayoutData.heightHint = 110;
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessVerticalSpace = true;

		visionEditor = new RichTextEditor(composite, SWT.BORDER |
				SWT.WRAP | SWT.V_SCROLL, getEditorSite(), commandListener);
		visionEditor.setLayoutData(editorLayoutData);
		ModifyListener modifyList = new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		};
		visionEditor.addModifyListener(modifyList);

		
		
		// Expected Value of Outcome
		
		Label expectedValueOutcomeTextLabel = new Label(composite, SWT.NONE);
		expectedValueOutcomeTextLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 1));
		expectedValueOutcomeTextLabel.setText("Expected Value of Outcome:");
		
		GridData editorLayoutData2 = new GridData();
		editorLayoutData2.widthHint = 400;
		editorLayoutData2.heightHint = 110;
		editorLayoutData2.horizontalAlignment = SWT.FILL;
		editorLayoutData2.grabExcessHorizontalSpace = true;
		editorLayoutData2.horizontalSpan = 2;
		editorLayoutData2.verticalSpan = 1;
		
		expectedValueOutcomeEditor = new RichTextEditor(composite, SWT.BORDER |
				SWT.WRAP | SWT.V_SCROLL, getEditorSite(), null);
		expectedValueOutcomeEditor.setLayoutData(editorLayoutData2);
		expectedValueOutcomeEditor.addModifyListener(modifyList);		
		
		
		// Opportunities + Challenges
		
		Label opportunitiesTextLabel = new Label(composite, SWT.NONE);
		opportunitiesTextLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		opportunitiesTextLabel.setText("Opportunities:");
		
		Label challengesTextLabel = new Label(composite, SWT.NONE);
		challengesTextLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		challengesTextLabel.setText("Challenges:");
		
		
		GridData editorLayoutData3 = new GridData();
		editorLayoutData3.widthHint = 150;
		editorLayoutData3.heightHint = 70;
		editorLayoutData3.horizontalAlignment = SWT.FILL;
		editorLayoutData3.grabExcessHorizontalSpace = true;
		editorLayoutData3.horizontalSpan = 1;
		editorLayoutData3.verticalSpan = 1;
		
		opportunitiesEditor = new Text(composite,SWT.BORDER | SWT.WRAP
				| SWT.V_SCROLL | SWT.MULTI);
		opportunitiesEditor.setLayoutData(editorLayoutData3);
		opportunitiesEditor.addModifyListener(modifyList);
		
		
		GridData editorLayoutData4 = new GridData();
		editorLayoutData4.widthHint = 150;
		editorLayoutData4.heightHint = 70;
		editorLayoutData4.horizontalAlignment = SWT.FILL;
		editorLayoutData4.grabExcessHorizontalSpace = true;
		editorLayoutData4.horizontalSpan = 1;
		editorLayoutData4.verticalSpan = 1;
		
		challengesEditor = new Text(composite, SWT.BORDER | SWT.WRAP
				| SWT.V_SCROLL | SWT.MULTI);
		challengesEditor.setLayoutData(editorLayoutData4);
		challengesEditor.addModifyListener(modifyList);
		
		// Sponsors + Adversaries
		
		Label sponsorsTextLabel = new Label(composite, SWT.NONE);
		sponsorsTextLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		sponsorsTextLabel.setText("Sponsors:");
		
		Label adversariesTextLabel = new Label(composite, SWT.NONE);
		adversariesTextLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		adversariesTextLabel.setText("Adversaries:");
		
		
		GridData editorLayoutData5 = new GridData();
		editorLayoutData5.widthHint = 150;
		editorLayoutData5.heightHint = 70;
		editorLayoutData5.horizontalAlignment = SWT.FILL;
		editorLayoutData5.grabExcessHorizontalSpace = true;
		editorLayoutData5.horizontalSpan = 1;
		editorLayoutData5.verticalSpan = 1;
		
		sponsorsEditor = new Text(composite,SWT.BORDER | SWT.WRAP
				| SWT.V_SCROLL | SWT.MULTI);
		sponsorsEditor.setLayoutData(editorLayoutData5);
		sponsorsEditor.addModifyListener(modifyList);
		
		
		GridData editorLayoutData6 = new GridData();
		editorLayoutData6.widthHint = 150;
		editorLayoutData6.heightHint = 70;
		editorLayoutData6.horizontalAlignment = SWT.FILL;
		editorLayoutData6.grabExcessHorizontalSpace = true;
		editorLayoutData6.horizontalSpan = 1;
		editorLayoutData6.verticalSpan = 1;
		
		adversariesEditor = new Text(composite, SWT.BORDER | SWT.WRAP
				| SWT.V_SCROLL | SWT.MULTI);
		adversariesEditor.setLayoutData(editorLayoutData6);
		adversariesEditor.addModifyListener(modifyList);
		
		
		// Group Values & Committment
		
		Label groupValuesAndComTextLabel = new Label(composite, SWT.NONE);
		groupValuesAndComTextLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 1));
		groupValuesAndComTextLabel.setText("Group Values & Committment:");
		
		GridData editorLayoutData7 = new GridData();
		editorLayoutData7.widthHint = 400;
		editorLayoutData7.heightHint = 70;
		editorLayoutData7.horizontalAlignment = SWT.FILL;
		editorLayoutData7.grabExcessHorizontalSpace = true;
		editorLayoutData7.horizontalSpan = 2;
		editorLayoutData7.verticalSpan = 1;
		
		groupValuesAndComEditor = new Text(composite, SWT.BORDER | SWT.WRAP
				| SWT.V_SCROLL | SWT.MULTI);
		groupValuesAndComEditor.setLayoutData(editorLayoutData7);
		groupValuesAndComEditor.addModifyListener(modifyList);		
		
		
		// Beliefs
		
		Label beliefsTextLabel = new Label(composite, SWT.NONE);
		beliefsTextLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 1));
		beliefsTextLabel.setText("Beliefs:");
		
		GridData editorLayoutData8 = new GridData();
		editorLayoutData8.widthHint = 400;
		editorLayoutData8.heightHint = 70;
		editorLayoutData8.horizontalAlignment = SWT.FILL;
		editorLayoutData8.grabExcessHorizontalSpace = true;
		editorLayoutData8.horizontalSpan = 2;
		editorLayoutData8.verticalSpan = 1;
		
		beliefsEditor = new Text(composite, SWT.BORDER | SWT.WRAP
				| SWT.V_SCROLL | SWT.MULTI);
		beliefsEditor.setLayoutData(editorLayoutData8);
		beliefsEditor.addModifyListener(modifyList);		
		
		
		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Vision.html";
	}

	@Override
	protected void fillInitialData() {

		Vision vision = (Vision) element;

		basicInfoContainer.fillInitialData(vision, new String[]{});

		if (vision.getVision() != null) {
			visionEditor.setText(vision.getVision().toString());
		}
		
		if (vision.getExpectedValueOutcome() != null) {
			expectedValueOutcomeEditor.setText(vision.getExpectedValueOutcome().toString());
		}
		
		if (vision.getOpportunities() != null) {
			opportunitiesEditor.setText(vision.getOpportunities().toString());
		}
		
		if (vision.getChallenges() != null) {
			challengesEditor.setText(vision.getChallenges().toString());			
		}
		
		if (vision.getSponsors() != null) {
			sponsorsEditor.setText(vision.getSponsors().toString());
		}
		
		if (vision.getAdversaries() != null) {
			adversariesEditor.setText(vision.getAdversaries().toString());
		}
		
		if (vision.getGroupValuesAndCom() != null) {
			groupValuesAndComEditor.setText(vision.getGroupValuesAndCom().toString());
		}
		
		if (vision.getBeliefs() != null) {
			beliefsEditor.setText(vision.getBeliefs().toString());
		}

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		// put all the attributes which we want to search through
		// into the attributesToSearchMap
		super.addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);
		
		attributesToSearch.put(VisionPackage.Literals.VISION__VISION,
				visionEditor.getText());
		attributesToSearch.put(VisionPackage.Literals.VISION__EXPECTED_VALUE_OUTCOME,
				expectedValueOutcomeEditor.getText());
		attributesToSearch.put(VisionPackage.Literals.VISION__OPPORTUNITIES,
				opportunitiesEditor.getText());
		attributesToSearch.put(VisionPackage.Literals.VISION__CHALLENGES,
				challengesEditor.getText());
		attributesToSearch.put(VisionPackage.Literals.VISION__SPONSORS,
				sponsorsEditor.getText());
		attributesToSearch.put(VisionPackage.Literals.VISION__ADVERSARIES,
				adversariesEditor.getText());
		attributesToSearch.put(VisionPackage.Literals.VISION__GROUP_VALUES_AND_COM,
				groupValuesAndComEditor.getText());
		attributesToSearch.put(VisionPackage.Literals.VISION__BELIEFS,
				beliefsEditor.getText());


		// Now, search through all attributes, recording all search hits along the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()-1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(), result.getEndIndex());
			}
		}

		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}

	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,visionPageIndex,basicInfoContainer);
		if (feature == VisionPackage.Literals.VISION__VISION) {
			setActivePage(visionPageIndex);
			visionEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] {"" + startIndex, "" + endIndex});
		} else if (feature == VisionPackage.Literals.VISION__EXPECTED_VALUE_OUTCOME) {
			setActivePage(visionPageIndex);
			expectedValueOutcomeEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] {"" + startIndex, "" + endIndex});
		} else if (feature == VisionPackage.Literals.VISION__OPPORTUNITIES) {
			setActivePage(visionPageIndex);
			opportunitiesEditor.setSelection(startIndex, endIndex);
		} else if (feature == VisionPackage.Literals.VISION__CHALLENGES) {
			setActivePage(visionPageIndex);
			challengesEditor.setSelection(startIndex, endIndex);
		} else if (feature == VisionPackage.Literals.VISION__SPONSORS) {
			setActivePage(visionPageIndex);
			sponsorsEditor.setSelection(startIndex, endIndex);
		} else if (feature == VisionPackage.Literals.VISION__ADVERSARIES) {
			setActivePage(visionPageIndex);
			adversariesEditor.setSelection(startIndex, endIndex);
		} else if (feature == VisionPackage.Literals.VISION__GROUP_VALUES_AND_COM) {
			setActivePage(visionPageIndex);
			groupValuesAndComEditor.setSelection(startIndex, endIndex);
		} else if (feature == VisionPackage.Literals.VISION__BELIEFS) {
			setActivePage(visionPageIndex);
			beliefsEditor.setSelection(startIndex, endIndex);
		}
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.vision.visioneditor";
	}
}
