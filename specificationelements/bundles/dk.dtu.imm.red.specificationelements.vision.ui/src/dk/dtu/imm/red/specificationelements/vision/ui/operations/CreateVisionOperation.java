package dk.dtu.imm.red.specificationelements.vision.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.specificationelements.vision.VisionFactory;
import dk.dtu.imm.red.specificationelements.vision.ui.extensions.VisionExtension;

public class CreateVisionOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected VisionExtension extension;
	protected Group parent;
//	protected String path;

	protected Vision vision;

	public CreateVisionOperation(String label, String name, String description, Group parent, String path) {
		super("Create Vision");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
//		this.path = path;

		extension = new VisionExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		vision = VisionFactory.eINSTANCE.createVision();
		vision.setCreator(PreferenceUtil.getUserPreference_User());
		vision.setLabel(label);
		vision.setName(name);
		vision.setDescription(description);

		if (parent != null) {
			vision.setParent(parent);
		}

		vision.save();
		extension.openElement(vision);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(vision);
			extension.deleteElement(vision);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
