package dk.dtu.imm.red.specificationelements.vision.ui.wizards.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.ui.wizards.ElementBasicInfoPage;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.specificationelements.vision.ui.wizards.NewVisionWizard;
import dk.dtu.imm.red.specificationelements.vision.ui.wizards.NewVisionWizardPresenter;

public class NewVisionWizardImpl extends BaseNewWizard
	implements NewVisionWizard {

//	protected ElementBasicInfoPage defineVisionPage;

	protected NewVisionWizardPresenter presenter;

	public NewVisionWizardImpl() {
		super("Vision", Vision.class);
		presenter = new NewVisionWizardPresenterImpl(this);
//		defineVisionPage = new ElementBasicInfoPage("Vision");
	}

	@Override
	public void addPages() {
//		addPage(defineVisionPage);
		super.addPages();
	}

	@Override
	public boolean performFinish() {

		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription();

		Group parent = null;

		if (!displayInfoPage.getSelection().isEmpty()
				&& displayInfoPage.getSelection()
					instanceof IStructuredSelection) {
			IStructuredSelection selection =
					(IStructuredSelection) displayInfoPage.getSelection();

			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}

		presenter.wizardFinished(label, name, description, parent,"");

		return true;
	}

	@Override
	public boolean canFinish() {
		return super.canFinish()
				&& elementBasicInfoPage.isPageComplete()
				&& displayInfoPage.isPageComplete()
				&& !displayInfoPage.getSelection().isEmpty();
	}

}
