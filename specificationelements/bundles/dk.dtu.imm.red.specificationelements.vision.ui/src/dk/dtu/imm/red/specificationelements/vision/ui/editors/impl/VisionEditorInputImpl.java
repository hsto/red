package dk.dtu.imm.red.specificationelements.vision.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.vision.Vision;

public class VisionEditorInputImpl extends BaseEditorInput {

	public VisionEditorInputImpl(Vision vision) {
		super(vision);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Vision editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for vision " + element.getName();
	}
}
