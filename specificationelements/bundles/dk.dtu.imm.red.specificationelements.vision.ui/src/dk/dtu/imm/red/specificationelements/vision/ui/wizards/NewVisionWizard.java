package dk.dtu.imm.red.specificationelements.vision.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewVisionWizard extends IBaseWizard {

	String ID = "dk.dtu.imm.red.specificationelements.vision.newwizard";

}
