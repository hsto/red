package dk.dtu.imm.red.specificationelements.vision.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.specificationelements.vision.ui.editors.VisionEditor;
import dk.dtu.imm.red.specificationelements.vision.ui.editors.impl.VisionEditorInputImpl;

public class VisionExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element) {
		if (element instanceof Vision) {
			VisionEditorInputImpl editorInput = new VisionEditorInputImpl(
					(Vision) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, VisionEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}

}
