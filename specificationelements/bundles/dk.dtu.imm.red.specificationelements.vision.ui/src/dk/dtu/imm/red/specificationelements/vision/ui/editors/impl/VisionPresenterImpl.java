package dk.dtu.imm.red.specificationelements.vision.ui.editors.impl;

import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.specificationelements.vision.ui.editors.VisionEditor;
import dk.dtu.imm.red.specificationelements.vision.ui.editors.VisionPresenter;

public class VisionPresenterImpl extends BaseEditorPresenter implements VisionPresenter {

	private static final int MAX_TITLE_LENGTH = 40;
	
	protected VisionEditor visionEditor;
	
	protected String visionText;
	protected String expectedValueOutcomeText;
	protected String opportunitiesText;
	protected String challengesText;
	protected String sponsorsText;
	protected String adversariesText;
	protected String groupValuesAndComText;
	protected String beliefsText;
	
	public VisionPresenterImpl(VisionEditor visionEditor, Vision element) {
		super(element);
		this.visionEditor = visionEditor;
		visionText = "";
		expectedValueOutcomeText = "";
		opportunitiesText = "";
		challengesText = "";
		sponsorsText = "";
		adversariesText = "";
		groupValuesAndComText = "";
		beliefsText = "";
	}

	@Override
	public void save() {
		super.save();
		Vision vision = (Vision) element;
		vision.setVision(TextFactory.eINSTANCE.createText(visionText));
		vision.setExpectedValueOutcome(TextFactory.eINSTANCE.createText(expectedValueOutcomeText));
		vision.setOpportunities(TextFactory.eINSTANCE.createText(opportunitiesText));
		vision.setChallenges(TextFactory.eINSTANCE.createText(challengesText));
		vision.setSponsors(TextFactory.eINSTANCE.createText(sponsorsText));
		vision.setAdversaries(TextFactory.eINSTANCE.createText(adversariesText));
		vision.setGroupValuesAndCom(TextFactory.eINSTANCE.createText(groupValuesAndComText));
		vision.setBeliefs(TextFactory.eINSTANCE.createText(beliefsText));		
		vision.save();
	}
	
	@Override
	public void setVision(String text) {
		this.visionText = text;
	}
	
	@Override
	public void setExpectedValueOutcome(String text) {
		this.expectedValueOutcomeText = text;
	}

	@Override
	public void setOpportunities(String text) {
		this.opportunitiesText = text;
	}

	@Override
	public void setChallenges(String text) {
		this.challengesText = text;
	}

	@Override
	public void setSponsors(String text) {
		this.sponsorsText = text;
	}

	@Override
	public void setAdversaries(String text) {
		this.adversariesText = text;
	}

	@Override
	public void setGroupValuesAndCom(String text) {
		this.groupValuesAndComText = text;
	}

	@Override
	public void setBeliefsEditor(String text) {
		this.beliefsText = text;
	}
	
	@Override
	public int getTitleMaxLength() {
		return MAX_TITLE_LENGTH;
	}


}
