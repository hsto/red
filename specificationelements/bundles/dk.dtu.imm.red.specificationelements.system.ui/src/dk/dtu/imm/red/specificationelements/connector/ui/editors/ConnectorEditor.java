package dk.dtu.imm.red.specificationelements.connector.ui.editors;

import org.eclipse.ui.IEditorPart; 
import dk.dtu.imm.red.core.ui.editors.IBaseEditor; 

public interface ConnectorEditor extends IEditorPart, IBaseEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.connector.connectoreditor";
	void attitudeChanged(); 
}
