/*
 * 
 */
package dk.dtu.imm.red.specificationelements.configuration.ui.editors.impl;   
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage;
import dk.dtu.imm.red.specificationelements.configuration.ui.editors.ConfigurationEditor;
import dk.dtu.imm.red.specificationelements.configuration.ui.editors.ConfigurationPresenter;
import dk.dtu.imm.red.specificationelements.connector.Connector;
 

/**
 * @author Johan Paaske Nielsen
 *
 */

public class ConfigurationPresenterImpl extends BaseEditorPresenter implements ConfigurationPresenter { 
	 
	protected ConfigurationEditor configurationEditor;
	private ConfigurationViewModel viewModel;
	
	private EAGPresenter<Actor> partsRelationPresenter;   
	private EAGPresenter<Connector> connectorPresenter;
	
	public ConfigurationViewModel getViewModel() {
		return viewModel;
	}

	public ConfigurationPresenterImpl(ConfigurationEditor configurationEditor, ConfigurationViewModel viewModel) {
		super(viewModel.getElement());

		this.element =  viewModel.getElement();
		this.viewModel = viewModel;
		this.configurationEditor = configurationEditor; 
		
		final Configuration configuration = viewModel.getElement();
		partsRelationPresenter = new EAGPresenter<Actor>(){

			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(configuration);
				listener.setHostingFeature(ConfigurationPackage.eINSTANCE.getConfiguration_Parts());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
				
				super.AddItem(o);
			}
		};
		partsRelationPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.actor", "Actor");
		partsRelationPresenter.setReflectedClass(Actor.class); // For element selector
		
		connectorPresenter = new EAGPresenter<Connector>(){

			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(configuration);
				listener.setHostingFeature(ConfigurationPackage.eINSTANCE.getConfiguration_InternalConnectors());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
				
				super.AddItem(o);
			}
		};
		connectorPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.connector", "Connector");
		connectorPresenter.setReflectedClass(Connector.class); // For element selector
		 
	} 

	@Override
	public void save() {
		super.save(); 
		viewModel.save(); 
	}
	
	@Override
	public ElementColumnDefinition[] getPartsColumnDefinition() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(8)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {"getIcon"}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setEditable(false)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getName"}),
				new ElementColumnDefinition()
				.setHeaderName("Kind")
				.setColumnType(String.class)
				.setEditable(false)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getElementKind"})
				 };
	}
	
	@Override
	public ElementColumnDefinition[] getConnectorColumnDefinition() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(8)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {"getIcon"}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(Configuration.class)
				.setEditable(false)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getName"})
				 };
	}
	
	@Override
	public EAGPresenter<Actor> getPartsRelationPresenter() {
		return partsRelationPresenter;
	}
	
	@Override
	public EAGPresenter<Connector> getConnectorPresenter() {
		return connectorPresenter;
	} 
	
	@Override
	public Project getParentProject() {
		
		Element current = viewModel.getElementBeforeEdit().getParent();
		
		while(current != null) {
			if(current instanceof Project) {
				return (Project) current;
			}
			
			current = ((Group) current).getParent();
		}
		
		return null;
	}

	@Override
	public List<Element> getConfigurationElements() {
		 List<Element> elements = new ArrayList<Element>();
		 
		 elements.addAll(collectActorAndUsecases( viewModel.getElement().getParts()));
		 elements.addAll(viewModel.getElement().getInternalConnectors());
		 
		 return elements;
	}

	private List<Element>  collectActorAndUsecases(EList<Actor> eList) {
		List<Element> elements = new ArrayList<Element>();
		
		for(Actor actor : eList) {
			 for(SpecificationElement element : actor.getBehaviorElements()) {
				 elements.add(element);
			 }
			 elements.add(actor);
		 }
		
		return elements;
	}
 
}
