package dk.dtu.imm.red.specificationelements.system.ui.wizards.impl;

import dk.dtu.imm.red.specificationelements.actor.ui.wizards.impl.NewActorWizardImpl;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.system.ui.wizards.NewSystemWizard;

public class NewSystemWizardImpl extends NewActorWizardImpl 
	implements NewSystemWizard {
	
	public NewSystemWizardImpl() {
		super("System", System.class);
		presenter = new NewSystemWizardPresenterImpl(this);
	}

}
