package dk.dtu.imm.red.specificationelements.connector.ui.editors;

import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;

public interface ConnectorPresenter extends IEditorPresenter {
	ElementColumnDefinition[] getScenariosColumnDefinition();
	ElementColumnDefinition[] getSourceColumnDefinition();
	ElementColumnDefinition[] getTargetDefinition();
	EAGPresenter<UseCaseScenario> getScenariosPresenter();
	EAGPresenter<Actor> getSourcePresenter();
	EAGPresenter<Actor> getTargetPresenter();
	void setAttitude(String selection); 

}
