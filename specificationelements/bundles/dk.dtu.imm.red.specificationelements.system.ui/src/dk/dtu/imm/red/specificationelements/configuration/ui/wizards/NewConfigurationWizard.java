package dk.dtu.imm.red.specificationelements.configuration.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewConfigurationWizard extends IBaseWizard {
	
	String ID = "dk.dtu.imm.red.specificationelements.configuration.newconfigurationwizard";

}
