package dk.dtu.imm.red.specificationelements.port.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;
import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;

public interface GetModelElementReferenceWizardPresenter extends IWizardPresenter {
	
	ModelElement getselectedElement();
	
	void setSelectedElement(ModelElement selectedElement);
}
