package dk.dtu.imm.red.specificationelements.system.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.system.ui.editors.SystemEditor;
import dk.dtu.imm.red.specificationelements.system.ui.editors.impl.SystemEditorInputImpl;

public class SystemExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element) {
		if (element instanceof System) {
			SystemEditorInputImpl editorInput = new SystemEditorInputImpl(
					(System) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, SystemEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}
}
