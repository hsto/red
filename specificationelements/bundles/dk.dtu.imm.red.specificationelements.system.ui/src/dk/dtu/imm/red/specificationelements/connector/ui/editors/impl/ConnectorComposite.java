package dk.dtu.imm.red.specificationelements.connector.ui.editors.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.SpecificationElement;

public class ConnectorComposite extends Composite { 

	private Composite cmpTop;  
	private ConnectorEditorImpl editorParent;
	private SashForm sashForm;
	private Composite sashCompProtocol;
	private Composite sashCompEndpoints;
	private Composite cmpProtocol;
	private Composite cmpQuantities;
	private Composite cmpSource;
	private Composite cmpTarget;
	private Combo cmbAttitude;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ConnectorComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		createLayout();
	}
	
	public ConnectorComposite(Composite parent, int style, ConnectorEditorImpl editorParent) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		this.editorParent = editorParent;
		createLayout();
	}

	private void createLayout() {
		cmpTop = new Composite(this, SWT.NONE);
		GridData gd_cmpTop = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_cmpTop.heightHint = 342;
		cmpTop.setLayoutData(gd_cmpTop);
		cmpTop.setLayout(new GridLayout(1, false));
		
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;
		
		ElementBasicInfoContainer basicInfoContainer = new ElementBasicInfoContainer(cmpTop, SWT.NONE, editorParent, (SpecificationElement) editorParent.getEditorElement());
		editorParent.setBasicInfoContainer(basicInfoContainer);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);
		
		Group gpActorEditor = new Group(cmpTop, SWT.NONE);
		gpActorEditor.setText("Connector Properties");
		gpActorEditor.setLayout(new GridLayout(1, false));
		gpActorEditor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		sashForm = new SashForm(gpActorEditor, SWT.NONE);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		// Protocol column
		sashCompProtocol = new Composite(sashForm, SWT.NONE);
		sashCompProtocol.setLayout(new GridLayout(1, false));
		
		Label lblProtocol = new Label(sashCompProtocol, SWT.NONE);
		lblProtocol.setText("Protocol");
		
		cmpProtocol = new Composite(sashCompProtocol, SWT.NONE);
		cmpProtocol.setLayout(new GridLayout(1, false));
		cmpProtocol.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		//Empty middle column
		@SuppressWarnings("unused")
		Composite compEmptyColumn = new Composite(sashForm, SWT.NONE);
		
		// End point column
		sashCompEndpoints = new Composite(sashForm, SWT.NONE);
		sashCompEndpoints.setLayout(new GridLayout(1, false));
		
		Label lblSource = new Label(sashCompEndpoints, SWT.NONE);
		lblSource.setText("Source"); 
		
		cmpSource = new Composite(sashCompEndpoints, SWT.NONE);
		cmpSource.setLayout(new GridLayout(1, false));
		cmpSource.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		new Label(sashCompEndpoints, SWT.SEPARATOR|SWT.HORIZONTAL).setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		Composite compAttitude = new Composite(sashCompEndpoints, SWT.NONE);
		compAttitude.setLayout(new GridLayout(2, false));
		
		Label lblAttitude = new Label(compAttitude, SWT.NONE);
		lblAttitude.setText("Attitude");
		
		cmbAttitude = new Combo(compAttitude, SWT.READ_ONLY);
		cmbAttitude.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.attitudeChanged();
			}
		});
		
		new Label(sashCompEndpoints, SWT.SEPARATOR|SWT.HORIZONTAL).setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		Label lblTarget = new Label(sashCompEndpoints, SWT.NONE);
		lblTarget.setText("Target");
		
		cmpTarget = new Composite(sashCompEndpoints, SWT.NONE);
		cmpTarget.setLayout(new GridLayout(1, false));
		cmpTarget.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		sashForm.setWeights(new int[] {1, 1, 1});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Composite getCmpProtocol() {
		return cmpProtocol;
	}

	public Composite getCmpQuantities() {
		return cmpQuantities;
	}

	public Composite getCmpSource() {
		return cmpSource;
	}

	public Composite getCmpTarget() {
		return cmpTarget;
	}

	public Combo getCmbAttitude() {
		return cmbAttitude;
	}
 
}
