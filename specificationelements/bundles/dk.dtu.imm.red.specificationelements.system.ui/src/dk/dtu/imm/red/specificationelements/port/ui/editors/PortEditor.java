package dk.dtu.imm.red.specificationelements.port.ui.editors;

import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorEditor; 

public interface PortEditor extends ActorEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.port.porteditor";
}
