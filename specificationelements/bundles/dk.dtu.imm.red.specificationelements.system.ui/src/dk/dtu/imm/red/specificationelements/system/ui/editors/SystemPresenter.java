package dk.dtu.imm.red.specificationelements.system.ui.editors;  
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorPresenter;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.ConfigurationReference;
import dk.dtu.imm.red.specificationelements.system.GlueConnector;

public interface SystemPresenter extends ActorPresenter {
	
	public EAGPresenter<ConfigurationReference> getConfigurationPresenter();
	public EAGPresenter<Port> getPortPresenter();
	public EAGPresenter<GlueConnector> getGluePresenter();
	public ElementColumnDefinition[] getConfigurationColumnDefinition();
	public ElementColumnDefinition[] getPortColumnDefinition();
	public ElementColumnDefinition[] getGlueColumnDefinition();
}
