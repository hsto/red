package dk.dtu.imm.red.specificationelements.connector.ui.editors.impl;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude;
import dk.dtu.imm.red.specificationelements.connector.ConnectorKind;
import dk.dtu.imm.red.specificationelements.connector.ConnectorPackage;
import dk.dtu.imm.red.specificationelements.connector.ui.editors.ConnectorEditor;
import dk.dtu.imm.red.specificationelements.connector.ui.editors.ConnectorPresenter;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;

public class ConnectorEditorImpl extends BaseEditor implements ConnectorEditor {

	protected ElementBasicInfoContainer basicInfoContainer;

	/**
	 * Index of the Connector attr page.
	 */
	protected int actorPageIndex;

	//Infrastructure
	private ConnectorPresenter connectorPresenter;
	private ConnectorViewModel viewModel;
	private ConnectorComposite connectorComposite;
	private EAGComposite<UseCaseScenario> gdScenarios;
	private EAGComposite<Actor> gdsource;
	private EAGComposite<Actor> gdTarget;


	//UI Setup
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);
		viewModel = new ConnectorViewModel();
		viewModel.setElement((Connector) element);
		presenter = new ConnectorPresenterImpl(this, viewModel);
		connectorPresenter = (ConnectorPresenter) presenter; // casted presenter
	}

	@Override
	protected void createPages() {

		//Create attributes page
		actorPageIndex = addPage(createFirstPage(getContainer()));
		setPageText(actorPageIndex, "Summary");

		super.createPages();
		fillInitialData();
	}

	private Composite createFirstPage(Composite parent) {

		connectorComposite = new ConnectorComposite(parent, SWT.NONE, this);
			
		//Setup scenario editor
		gdScenarios = new EAGComposite<UseCaseScenario>(
				connectorComposite.getCmpProtocol(), SWT.NONE, modifyListener,
				viewModel.getElement(), ConnectorPackage.eINSTANCE.getConnector_Scenarios())
				.setModifyListener(modifyListener)
				.setPresenter(connectorPresenter.getScenariosPresenter());
		gdScenarios.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		gdScenarios.setElementSelectorOnAdd(true);
		gdScenarios.setAddDialogTitle("Create Reference to a Protocol Element");
		gdScenarios.setAddDialogDescription("Select a protocol element, for example a Scenario.");

		//Setup source Editor
		gdsource = new EAGComposite<Actor>(
				connectorComposite.getCmpSource(), SWT.NONE, modifyListener,
				viewModel.getElement(), ConnectorPackage.eINSTANCE.getConnector_Source())
				.setModifyListener(modifyListener)
				.setPresenter(connectorPresenter.getSourcePresenter());
		gdsource.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		gdsource.setElementSelectorOnAdd(true);
		gdsource.setAddDialogTitle("Create Reference to an Actor Element");
		gdsource.setAddDialogDescription("Select an Actor element, for example a Port.");

		//Setup target Editor
		gdTarget = new EAGComposite<Actor>(
				connectorComposite.getCmpTarget(), SWT.NONE, modifyListener,
				viewModel.getElement(), ConnectorPackage.eINSTANCE.getConnector_Target())
				.setModifyListener(modifyListener)
				.setPresenter(connectorPresenter.getTargetPresenter());
		gdTarget.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		gdTarget.setElementSelectorOnAdd(true);
		gdTarget.setAddDialogTitle("Create Reference to an Actor Element");
		gdTarget.setAddDialogDescription("Select an Actor element, for example a Port.");

		return SWTUtils.wrapInScrolledComposite(parent, connectorComposite);
	}

	public void setIsModified() {
		modifyListener.handleEvent(null);
	}

	//Infrastructure actions
		@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(actorPageIndex);
		} else if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(actorPageIndex);
		}
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {
		return null; //TODO: remove

	}

	@Override
	public void doSaveAs() {
		return;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
			ConnectorPresenter presenter = (ConnectorPresenter) this.presenter;
			presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
					basicInfoContainer.getName(), basicInfoContainer.getKind(),
					basicInfoContainer.getDescription());
			super.doSave(monitor);

		}

	//Fill UI widgets with data
	@Override
	protected void fillInitialData() {

		ConnectorKind[] kinds = ConnectorKind.values();
		String[] kindStrings = new String[kinds.length];
		for(int i=0;i<kinds.length;i++){
			kindStrings[i] = kinds[i].getName();
		}

		basicInfoContainer.fillInitialData(viewModel.getElement(), kindStrings);
		//Set Table Editors
		gdScenarios.setColumns(connectorPresenter.getScenariosColumnDefinition())
			.setDataSource(viewModel.getElement().getScenarios());
		
		gdsource.setColumns(connectorPresenter.getSourceColumnDefinition())
			.setDataSource(viewModel.getElement().getSource());

		gdTarget.setColumns(connectorPresenter.getTargetDefinition())
			.setDataSource(viewModel.getElement().getTarget());

		//Set kind
//		for(ActorRelationKind a : ActorRelationKind.values()) {
//			actorRelationComposite.getCombo().add(a.name());
//		}

//		actorRelationComposite.getCombo().select(
//				actorRelationComposite.getCombo().indexOf(
//						 viewModel.getElement().getKind()));

		//Set attitude
		for(ConnectorAttitude a : ConnectorAttitude.values()) {
			connectorComposite.getCmbAttitude().add(a.name());
		}

		connectorComposite.getCmbAttitude().select(
				connectorComposite.getCmbAttitude().indexOf(
						 viewModel.getElement().getAttitude().name()));



		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	//Properties
	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.configuration.connectoreditor";
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Connector.html";
	}

	@Override
	public void attitudeChanged() {
		String selection = connectorComposite.getCmbAttitude().getItem(
				connectorComposite.getCmbAttitude().getSelectionIndex());
		connectorPresenter.setAttitude(selection);
		modifyListener.handleEvent(null);
	}

	public void setBasicInfoContainer(ElementBasicInfoContainer basicInfoContainer){
		this.basicInfoContainer = basicInfoContainer;
	}
}
