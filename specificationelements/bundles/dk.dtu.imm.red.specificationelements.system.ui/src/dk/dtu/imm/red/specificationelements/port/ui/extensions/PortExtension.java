package dk.dtu.imm.red.specificationelements.port.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.ui.editors.PortEditor;
import dk.dtu.imm.red.specificationelements.port.ui.editors.impl.PortEditorInputImpl;

public class PortExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element) {
		if (element instanceof Port) {
			PortEditorInputImpl editorInput = new PortEditorInputImpl(
					(Port) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, PortEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}
}
