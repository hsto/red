package dk.dtu.imm.red.specificationelements.system.ui.editors.impl;  
import dk.dtu.imm.red.specificationelements.actor.ui.editors.impl.ActorViewModel;
import dk.dtu.imm.red.specificationelements.system.System;

public class SystemViewModel extends ActorViewModel{
	public System getElementAsSystem() {
		return (System) getElement();
	}
}