package dk.dtu.imm.red.specificationelements.port.ui.editors;  
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorPresenter;
import dk.dtu.imm.red.specificationelements.port.RoleTableRow;

public interface PortPresenter extends ActorPresenter {
	
	public EAGPresenter<RoleTableRow> getRoleRowPresenter();
	public ElementColumnDefinition[] getPortItemColumnDefinition();
}
