package dk.dtu.imm.red.specificationelements.port.ui.wizards; 
import org.eclipse.ui.INewWizard;  
import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface GetModelElementReferenceWizard extends INewWizard, IBaseWizard { 
}
