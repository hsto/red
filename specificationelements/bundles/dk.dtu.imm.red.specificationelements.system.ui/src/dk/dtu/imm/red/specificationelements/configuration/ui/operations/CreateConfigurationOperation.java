package dk.dtu.imm.red.specificationelements.configuration.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationFactory;
import dk.dtu.imm.red.specificationelements.configuration.ui.extensions.ConfigurationExtension;


public class CreateConfigurationOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected ConfigurationExtension extension;
	protected Group parent;

	private Configuration configuration;

	public CreateConfigurationOperation(String label, String name, String description, Group parent, String path) {
		super("Create Configuration");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
		extension = new ConfigurationExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		configuration = ConfigurationFactory.eINSTANCE.createConfiguration();
		configuration.setCreator(PreferenceUtil.getUserPreference_User());

		//actor.setId(id);
		configuration.setLabel(label);
		configuration.setName(name);
		configuration.setDescription(description);


		if (parent != null) {
			configuration.setParent(parent);
		}

		configuration.save();
		extension.openElement(configuration);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(configuration);
			extension.deleteElement(configuration);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
