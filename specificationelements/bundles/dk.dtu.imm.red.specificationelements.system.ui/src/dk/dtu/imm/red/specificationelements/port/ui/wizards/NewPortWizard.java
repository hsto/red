package dk.dtu.imm.red.specificationelements.port.ui.wizards;

import dk.dtu.imm.red.specificationelements.actor.ui.wizards.NewActorWizard;

public interface NewPortWizard extends NewActorWizard {
	
	String ID = "dk.dtu.imm.red.specificationelements.port.newportwizard";

}
