package dk.dtu.imm.red.specificationelements.port.ui.wizards.impl;

import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;
import dk.dtu.imm.red.specificationelements.port.ui.wizards.GetModelElementReferenceWizardPresenter;

public class GetModelElementReferenceWizardPresenterImpl
		extends	BaseWizardPresenter  
		implements GetModelElementReferenceWizardPresenter {

	protected ModelElement selectedElement;
	
	@Override
	public ModelElement getselectedElement() {
		return selectedElement;
	}

	@Override
	public void setSelectedElement(ModelElement selectedElement) {
		this.selectedElement = selectedElement;
		
	}

}
