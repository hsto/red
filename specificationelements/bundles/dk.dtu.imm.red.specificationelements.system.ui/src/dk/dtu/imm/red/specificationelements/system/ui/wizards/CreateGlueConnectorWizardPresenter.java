package dk.dtu.imm.red.specificationelements.system.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;
import dk.dtu.imm.red.specificationelements.port.Port;

public interface CreateGlueConnectorWizardPresenter extends IWizardPresenter {
	
	Port getSelectedSource();
	Port getSelectedTarget();
	
	void setSelectedSource(Port source);
	void setSelectedTarget(Port target);
}
