package dk.dtu.imm.red.specificationelements.port.ui.editors.impl; 
import dk.dtu.imm.red.specificationelements.actor.ui.editors.impl.ActorEditorInputImpl;
import dk.dtu.imm.red.specificationelements.port.Port;
 

public class PortEditorInputImpl extends ActorEditorInputImpl {

	public PortEditorInputImpl(Port port) {
		super(port);
	}

	@Override
	public String getName() {
		return "Port editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for port " + element.getName();
	}
}
