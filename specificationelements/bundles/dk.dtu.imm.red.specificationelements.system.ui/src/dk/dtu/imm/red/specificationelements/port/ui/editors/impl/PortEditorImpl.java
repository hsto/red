package dk.dtu.imm.red.specificationelements.port.ui.editors.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.impl.ActorEditorImpl;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortPackage;
import dk.dtu.imm.red.specificationelements.port.RoleTableRow;
import dk.dtu.imm.red.specificationelements.port.ui.editors.PortPresenter;
import dk.dtu.imm.red.specificationelements.system.ui.editors.SystemEditor;

public class PortEditorImpl extends ActorEditorImpl implements SystemEditor {

	private EAGComposite<RoleTableRow> gdRoleRows;
	private PortViewModel viewModel;
	private PortPresenter portPresenter;
	
	//UI Setup
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		
		super.init(site, input);
		viewModel = new PortViewModel();
		viewModel.setElement((Port) element);
		presenter = new PortPresenterImpl(this, viewModel);
		portPresenter = (PortPresenter) presenter; // casted presenter
	}

	@Override
	protected Composite createSummaryPage(Composite parent, String groupName) {
		Composite superComposite = super.createSummaryPage(parent, "Protocol Role");
		 
		 Composite optionalComp = getActorComposite().getOptionalComposite();
		 optionalComp.setLayout(new GridLayout(1, true));

		//Signature column
		 Label signatureLabel = new Label(optionalComp, SWT.NONE);
		 signatureLabel.setText("Role");
		 gdRoleRows = new EAGComposite<RoleTableRow>(
				 optionalComp, SWT.NONE, modifyListener, 
				 viewModel.getElementAsPort(), PortPackage.eINSTANCE.getPort_RoleTable())
				 .setModifyListener(modifyListener)
				 .setPresenter(portPresenter.getRoleRowPresenter());
		 gdRoleRows.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		 return SWTUtils.wrapInScrolledComposite(parent, superComposite);
	}
	
	@Override
	protected String[] getKindStrings() {
		return new String[0];
	}

	//Fill UI widgets with data
	@Override
	protected void fillInitialData() {

		//Set Table Editors
		gdRoleRows.setColumns(portPresenter.getPortItemColumnDefinition())
		   				 .setDataSource( viewModel.getElementAsPort().getRoleTable());
		
		super.fillInitialData();
	}

	//Properties
	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.port.porteditor";
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Port.html";
	}
}
