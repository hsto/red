package dk.dtu.imm.red.specificationelements.system.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.system.SystemFactory;
import dk.dtu.imm.red.specificationelements.system.ui.extensions.SystemExtension;

public class CreateSystemOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected SystemExtension extension;
	protected Group parent;
	
	private System system;

	public CreateSystemOperation(String label, String name, String description, Group parent, String path) {
		super("Create System");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
		extension = new SystemExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		system = SystemFactory.eINSTANCE.createSystem();
		system.setCreator(PreferenceUtil.getUserPreference_User());

		system.setLabel(label);
		system.setName(name);
		system.setDescription(description);

		if (parent != null) {
			system.setParent(parent);
		}

		system.save();
		extension.openElement(system);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(system);
			extension.deleteElement(system);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
