package dk.dtu.imm.red.specificationelements.system.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage; 
import org.eclipse.ui.IWorkbench;

import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.ui.wizards.CreateGlueConnectorWizard;
import dk.dtu.imm.red.specificationelements.system.ui.wizards.CreateGlueConnectorWizardPresenter;

public class CreateGlueConnectorWizardImpl extends BaseNewWizard
		implements CreateGlueConnectorWizard {
	
	protected CreateGlueConnectorPageImpl createGlueConnectorPage;
	
	public CreateGlueConnectorWizardImpl() {
		super("Port", Port.class);
		presenter = new CreateGlueConnectorWizardPresenterImpl();
	} 
	
	@Override
	public boolean performFinish() {

		Port sourcePort = createGlueConnectorPage.getSelectedSource();
		Port targetPort = createGlueConnectorPage.getSelectedTarget();
		
		((CreateGlueConnectorWizardPresenter) presenter).setSelectedSource(sourcePort);
		((CreateGlueConnectorWizardPresenter) presenter).setSelectedTarget(targetPort);
			
		return true;
	}

	@Override
	public void pageOpenedOrClosed(IWizardPage fromPage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		createGlueConnectorPage = new CreateGlueConnectorPageImpl();
		presenter = new CreateGlueConnectorWizardPresenterImpl();
	}
	
	@Override
	public void addPages() {
		addPage(createGlueConnectorPage);
		
		super.addPages();
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		return null;
	}

	@Override
	public boolean canFinish() {
		return createGlueConnectorPage.isPageComplete();
	}

	
}
