package dk.dtu.imm.red.specificationelements.system.ui.editors.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.impl.ActorEditorImpl;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.ConfigurationReference;
import dk.dtu.imm.red.specificationelements.system.GlueConnector;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.system.SystemPackage;
import dk.dtu.imm.red.specificationelements.system.ui.editors.SystemEditor;
import dk.dtu.imm.red.specificationelements.system.ui.editors.SystemPresenter;

public class SystemEditorImpl extends ActorEditorImpl implements SystemEditor {

	private EAGComposite<ConfigurationReference> gdConfiguration;
	private EAGComposite<Port> gdPorts;
	private EAGComposite<GlueConnector> gdGlue;
	private SystemViewModel viewModel;
	private SystemPresenter systemPresenter;
	
	//UI Setup
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);
		viewModel = new SystemViewModel();
		viewModel.setElement((System) element);
		presenter = new SystemPresenterImpl(this, viewModel);
		systemPresenter = (SystemPresenter) presenter; // casted presenter
	}

	@Override
	protected Composite createSummaryPage(Composite parent, String groupName) {
		 Composite superComposite = super.createSummaryPage(parent, "Realization");
		 
		 Composite optionalComp = getActorComposite().getOptionalComposite();
		 optionalComp.setLayout(new GridLayout(2, true));
		 
		 SashForm leftComp = new SashForm(optionalComp, SWT.VERTICAL);
		 leftComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		 Composite rightComp = new Composite(optionalComp, SWT.NONE);
		 rightComp.setLayout(new GridLayout(1, true));
		 rightComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		 
		 Composite upperLeftComp = new Composite(leftComp, SWT.NONE);
		 upperLeftComp.setLayout(new GridLayout(1, true));
		 upperLeftComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		 Composite lowerLeftComp = new Composite(leftComp, SWT.NONE);
		 lowerLeftComp.setLayout(new GridLayout(1, true));
		 lowerLeftComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		 
		 //Configuration composite
		 Label configurationLabel = new Label(upperLeftComp, SWT.NONE);
		 configurationLabel.setText("Configuration");
		 
		 gdConfiguration = new EAGComposite<ConfigurationReference>(
				 upperLeftComp, SWT.NONE, modifyListener,
				 viewModel.getElement(), SystemPackage.eINSTANCE.getSystem_ContainedConfigurations())
				 .setModifyListener(modifyListener)
				 .setPresenter(systemPresenter.getConfigurationPresenter());
		 gdConfiguration.setElementSelectorOnAdd(true);
		 gdConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		 
		 //Ports composite
		 Label portsLabel = new Label(lowerLeftComp, SWT.NONE);
		 portsLabel.setText("External ports");
		 
		 gdPorts = new EAGComposite<Port>(
				 lowerLeftComp, SWT.NONE, modifyListener,
				 viewModel.getElement(), SystemPackage.eINSTANCE.getSystem_Ports())
				 .setModifyListener(modifyListener)
				 .setPresenter(systemPresenter.getPortPresenter());
		 gdPorts.setElementSelectorOnAdd(true);
		 gdPorts.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		 
		 //Glue composite
		 Label glueLabel = new Label(rightComp, SWT.NONE);
		 glueLabel.setText("Glue connectors");
		 
		 gdGlue = new EAGComposite<GlueConnector>(
				 rightComp, SWT.NONE, modifyListener,
				 viewModel.getElement(), SystemPackage.eINSTANCE.getSystem_GlueConnectors())
				 .setModifyListener(modifyListener)
				 .setPresenter(systemPresenter.getGluePresenter());
		 gdGlue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		 return SWTUtils.wrapInScrolledComposite(parent, superComposite);
	}
	
	@Override
	protected String[] getKindStrings() {
		return new String[0];
	}

	//Fill UI widgets with data
	@Override
	protected void fillInitialData() {

		//Set Table Editors
		gdConfiguration.setColumns(systemPresenter.getConfigurationColumnDefinition())
		   				 .setDataSource( viewModel.getElementAsSystem().getContainedConfigurations());
		gdPorts.setColumns(systemPresenter.getPortColumnDefinition())
			 .setDataSource( viewModel.getElementAsSystem().getPorts());
		gdGlue.setColumns(systemPresenter.getGlueColumnDefinition())
			 .setDataSource( viewModel.getElementAsSystem().getGlueConnectors());
		
		super.fillInitialData();
	}

	//Properties
	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.system.systemeditor";
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/System.html";
	}
}
