package dk.dtu.imm.red.specificationelements.configuration.ui.editors.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.configuration.ui.editors.ConfigurationEditor;

public class ConfigurationComposite extends Composite { 

	private Composite cmpTop; 
	private ConfigurationEditorImpl editorParent;
	private Composite actorEditorPlaceholder;
	private SashForm sashForm;
	private Composite sashCompParts;
	private Composite cmpParts;
	private Composite sashCompExternal;
	private Composite cmpPorts;
	private Composite cmpGlue;
	private Composite sashCompInternalConnectors;
	private Composite cmpInternalConnectors;


	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ConfigurationComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		createLayout();
	}
	
	public ConfigurationComposite(Composite parent, int style, ConfigurationEditorImpl editorParent) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		this.editorParent = editorParent;
		createLayout();
	}

	private void createLayout() {
		cmpTop = new Composite(this, SWT.NONE);
		GridData gd_cmpTop = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_cmpTop.heightHint = 342;
		cmpTop.setLayoutData(gd_cmpTop);
		cmpTop.setLayout(new GridLayout(1, false));
		
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;
		
		ElementBasicInfoContainer basicInfoContainer = new ElementBasicInfoContainer(cmpTop, SWT.NONE, editorParent, (SpecificationElement) editorParent.getEditorElement());
		editorParent.setBasicInfoContainer(basicInfoContainer);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);
		
		Group gpActorEditor = new Group(cmpTop, SWT.NONE);
		gpActorEditor.setText("Elements");
		gpActorEditor.setLayout(new GridLayout(1, false));
		gpActorEditor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		sashForm = new SashForm(gpActorEditor, SWT.NONE);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		// Actors column
		sashCompParts = new Composite(sashForm, SWT.NONE);
		sashCompParts.setLayout(new GridLayout(1, false));
		
		Label lblParts = new Label(sashCompParts, SWT.NONE);
		lblParts.setText("Internal actors");
		
		cmpParts = new Composite(sashCompParts, SWT.NONE);
		cmpParts.setLayout(new GridLayout(1, false));
		cmpParts.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		cmpParts.setBounds(0, 0, 64, 64);
		
		new Composite(sashForm, SWT.NONE);
		
		// Internal connectors column
		sashCompInternalConnectors = new Composite(sashForm, SWT.NONE);
		sashCompInternalConnectors.setLayout(new GridLayout(1, false));
		
		Label lblInternalConnectors = new Label(sashCompInternalConnectors, SWT.NONE);
		lblInternalConnectors.setText("Internal connectors");
		
		cmpInternalConnectors = new Composite(sashCompInternalConnectors, SWT.NONE);
		cmpInternalConnectors.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		cmpInternalConnectors.setLayout(new GridLayout(1, false));
		
		
		sashForm.setWeights(new int[] {1, 1, 1});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Composite getActorEditorPlaceholder() {
		return actorEditorPlaceholder;
	} 
	
	public Composite getCmpTop() {
		return cmpTop;
	}

	public ConfigurationEditor getEditorParent() {
		return editorParent;
	}

	public SashForm getSashForm() {
		return sashForm;
	}

	public Composite getSashCompParts() {
		return sashCompParts;
	}

	public Composite getCmpParts() {
		return cmpParts;
	}

	public Composite getSashCompPorts() {
		return sashCompExternal;
	}

	public Composite getCmpPorts() {
		return cmpPorts;
	}

	public Composite getCmpGlue() {
		return cmpGlue;
	}

	public Composite getSashCompRelations() {
		return sashCompInternalConnectors;
	}

	public Composite getCmpRelations() {
		return cmpInternalConnectors;
	}
	
}
