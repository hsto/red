package dk.dtu.imm.red.specificationelements.system.ui.wizards.impl;

import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.ui.wizards.CreateGlueConnectorWizardPresenter;

public class CreateGlueConnectorWizardPresenterImpl
		extends	BaseWizardPresenter  
		implements CreateGlueConnectorWizardPresenter {

	protected Port selectedSource;
	protected Port selectedTarget;
	
	@Override
	public Port getSelectedSource() {
		return selectedSource;
	}

	@Override
	public void setSelectedSource(Port source) {
		this.selectedSource = source;
		
	}

	@Override
	public Port getSelectedTarget() {
		return selectedTarget;
	}

	@Override
	public void setSelectedTarget(Port target) {
		this.selectedTarget = target;
		
	}
}
