package dk.dtu.imm.red.specificationelements.port.ui.wizards.impl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.actor.ui.wizards.impl.NewActorWizardPresenterImpl;
import dk.dtu.imm.red.specificationelements.port.ui.operations.CreatePortOperation;
import dk.dtu.imm.red.specificationelements.port.ui.wizards.NewPortWizard;
import dk.dtu.imm.red.specificationelements.port.ui.wizards.NewPortWizardPresenter;
 

public class NewPortWizardPresenterImpl extends NewActorWizardPresenterImpl
		implements NewPortWizardPresenter {

	public NewPortWizardPresenterImpl(final NewPortWizard newVisionWizard) {
		super(newVisionWizard);
	}
	
	@Override
	public void wizardFinished(String label, String name, String description, Group parent, String path) {

		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
				.getOperationSupport();

		CreatePortOperation operation = 
				new CreatePortOperation(label, name, description, parent, path);

		operation.addContext(operationSupport.getUndoContext());

		IAdaptable info = new IAdaptable() {
			@Override
			public Object getAdapter(@SuppressWarnings("rawtypes") final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		try {
			OperationHistoryFactory.getOperationHistory().execute(operation, null, info);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
	}


}
