/*
 * 
 */
package dk.dtu.imm.red.specificationelements.system.ui.editors.impl;   
import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardFactory;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorEditor;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.impl.ActorPresenterImpl;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.ConfigurationReference;
import dk.dtu.imm.red.specificationelements.system.GlueConnector;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.system.SystemPackage;
import dk.dtu.imm.red.specificationelements.system.ui.editors.SystemPresenter;
import dk.dtu.imm.red.specificationelements.system.ui.wizards.CreateGlueConnectorWizardFactory;
 

public class SystemPresenterImpl extends ActorPresenterImpl implements SystemPresenter { 
	 
	private SystemViewModel viewModel;
	private EAGPresenter<ConfigurationReference> configurationPresenter;
	private EAGPresenter<Port> portPresenter;
	private EAGPresenter<GlueConnector> gluePresenter;

	public SystemPresenterImpl(ActorEditor visionEditor, SystemViewModel viewModel) {
		super(visionEditor, viewModel);
		this.element =  viewModel.getElement();
		this.viewModel = viewModel;
		this.useCaseEditor = visionEditor;  
		
		final System system = viewModel.getElementAsSystem();
		this.configurationPresenter = new EAGPresenter<ConfigurationReference>(){

			@Override
			public void AddItem() {
				Configuration configuration = (Configuration) CreateElementReferenceWizardFactory
						.createElementReference(Configuration.class,
								"Create Reference to a Structural Element",
								"Select a structural element, for example a Configuration.");
				if (configuration != null) {
					ConfigurationReference reference = getNewItem();
					reference.setConfiguration(configuration);
					
					DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
					listener.setHostingObject(system);
					listener.setHostingFeature(SystemPackage.eINSTANCE.getSystem_ContainedConfigurations());
					listener.setHandledObject(reference);
					configuration.getDeletionListeners().add(listener);
					
					super.AddItem(reference);
				}
			}
		};
		configurationPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.system", "ConfigurationReference");// For Create,Delete
		configurationPresenter.setReflectedClass(ConfigurationReference.class); // For element selector
		
		this.portPresenter = new EAGPresenter<Port>(){

			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(system);
				listener.setHostingFeature(SystemPackage.eINSTANCE.getSystem_Ports());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
				
				super.AddItem(element);
			}
		};
		portPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.port", "Port");// For Create,Delete
		portPresenter.setReflectedClass(Port.class); // For element selector
		
		this.gluePresenter = new EAGPresenter<GlueConnector>(){

			@Override
			public void AddItem() {
				GlueConnector connector = CreateGlueConnectorWizardFactory.createGlueConnector();
				
				super.AddItem(connector);
			}
		};
		gluePresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.port", "GlueConnector");// For Create,Delete
		gluePresenter.setReflectedClass(GlueConnector.class); // For element selector
	} 
	
	public SystemViewModel getViewModel() {
		return viewModel;
	}

	@Override
	public EAGPresenter<ConfigurationReference> getConfigurationPresenter() {
		return configurationPresenter;
	}
	
	@Override
	public EAGPresenter<Port> getPortPresenter() {
		return portPresenter;
	}

	@Override
	public EAGPresenter<GlueConnector> getGluePresenter() {
		return gluePresenter;
	}

	@Override
	public ElementColumnDefinition[] getConfigurationColumnDefinition() {
		return new ElementColumnDefinition[] { 
				//Icon column
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(16)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {"getConfiguration", "getIcon"}),
				
				//Name column
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setEditable(false)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setReferenceGetterMethod(new String[] {"getConfiguration"})
				.setGetterMethod(new String[] {"getConfiguration", "getName"}),
				
				//Kind column
				new ElementColumnDefinition()
				.setHeaderName("Label")
				.setColumnType(String.class)
				.setColumnWidth(50)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<ConfigurationReference>() {
					@Override
					public Object getAttributeValue(ConfigurationReference item) {
						return item.getLabel();
					}

					@Override
					public void setAttributeValue(ConfigurationReference item, Object value) {
						item.setLabel(value.toString());
					} 
				})};
	}

	@Override
	public ElementColumnDefinition[] getPortColumnDefinition() {
		return new ElementColumnDefinition[] { 
				//Icon column
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(16)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {"getIcon"}),
				
				//Name column
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setEditable(false)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getName"})};
	}

	@Override
	public ElementColumnDefinition[] getGlueColumnDefinition() {
		return new ElementColumnDefinition[] { 
				//Name column
				new ElementColumnDefinition()
				.setHeaderName("Source")
				.setColumnType(String.class)
				.setEditable(false)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getSource", "getName"}),
				
				//Name column
				new ElementColumnDefinition()
				.setHeaderName("Target")
				.setColumnType(String.class)
				.setEditable(false)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getTarget", "getName"})};
	}
}
