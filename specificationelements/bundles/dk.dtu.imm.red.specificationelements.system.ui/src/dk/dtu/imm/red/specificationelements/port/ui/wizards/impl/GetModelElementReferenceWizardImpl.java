package dk.dtu.imm.red.specificationelements.port.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage; 
import org.eclipse.ui.IWorkbench;

import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;
import dk.dtu.imm.red.specificationelements.port.ui.wizards.GetModelElementReferenceWizard;
import dk.dtu.imm.red.specificationelements.port.ui.wizards.GetModelElementReferenceWizardPresenter;

public class GetModelElementReferenceWizardImpl extends BaseNewWizard
		implements GetModelElementReferenceWizard {
	
	protected GetModelElementReferencePageImpl getModelElementReferencePage;
	
	public GetModelElementReferenceWizardImpl() {
		super("Model Element", ModelElement.class);
		presenter = new GetModelElementReferenceWizardPresenterImpl();
	} 
	
	@Override
	public boolean performFinish() {

		ModelElement selectedElement = 
				getModelElementReferencePage.getSelectedElement();
		
		((GetModelElementReferenceWizardPresenter) presenter)
				.setSelectedElement(selectedElement);
			
		return true;
	}

	@Override
	public void pageOpenedOrClosed(IWizardPage fromPage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		getModelElementReferencePage = new GetModelElementReferencePageImpl();
		presenter = new GetModelElementReferenceWizardPresenterImpl();
	}
	
	@Override
	public void addPages() {
		addPage(getModelElementReferencePage);
		
		super.addPages();
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		return null;
	}

	@Override
	public boolean canFinish() {
		return getModelElementReferencePage.isPageComplete();
	}

	
}
