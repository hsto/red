package dk.dtu.imm.red.specificationelements.connector.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.ConnectorFactory;
import dk.dtu.imm.red.specificationelements.connector.ui.extensions.ConnectorExtension;

public class CreateConnectorOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected ConnectorExtension extension;
	protected Group parent;

	private Connector connector;

	public CreateConnectorOperation(String label, String name, String description, Group parent, String path) {
		super("Create Connector");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
//		this.path = path;
		extension = new ConnectorExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		connector = ConnectorFactory.eINSTANCE.createConnector();
		connector.setCreator(PreferenceUtil.getUserPreference_User());

		connector.setLabel(label);
		connector.setName(name);
		connector.setDescription(description);

		if (parent != null) {
			connector.setParent(parent);
		}

		connector.save();
		extension.openElement(connector);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(connector);
			extension.deleteElement(connector);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}	
}
