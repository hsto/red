package dk.dtu.imm.red.specificationelements.system.ui.wizards; 
import org.eclipse.ui.INewWizard;  
import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface CreateGlueConnectorWizard extends INewWizard, IBaseWizard { 
}
