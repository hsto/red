package dk.dtu.imm.red.specificationelements.system.ui.editors;

import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorEditor; 

public interface SystemEditor extends ActorEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.system.systemeditor";
}
