package dk.dtu.imm.red.specificationelements.port.ui.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;
import dk.dtu.imm.red.specificationelements.port.ui.wizards.impl.GetModelElementReferenceWizardImpl;

public class GetModelElementReferenceWizardFactory { 
	
	public static ModelElement getModelElementReference() {
		GetModelElementReferenceWizard wizard = 
				new GetModelElementReferenceWizardImpl();
		wizard.init(PlatformUI.getWorkbench(), 
				(IStructuredSelection) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection());

		WizardDialog dialog = 
				new WizardDialog(Display.getCurrent().getActiveShell(), 
						wizard);
					
		dialog.open();
		
		ModelElement selectedElement = 
				((GetModelElementReferenceWizardPresenter) wizard.getPresenter())
				.getselectedElement();
		
		return selectedElement;
	}
}
