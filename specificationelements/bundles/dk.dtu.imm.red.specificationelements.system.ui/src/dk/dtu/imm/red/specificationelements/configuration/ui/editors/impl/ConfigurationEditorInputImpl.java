package dk.dtu.imm.red.specificationelements.configuration.ui.editors.impl;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;

public class ConfigurationEditorInputImpl extends BaseEditorInput {

	public ConfigurationEditorInputImpl(Configuration configuration) {
		super(configuration);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Configuration editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for configuration " + element.getName();
	}
}
