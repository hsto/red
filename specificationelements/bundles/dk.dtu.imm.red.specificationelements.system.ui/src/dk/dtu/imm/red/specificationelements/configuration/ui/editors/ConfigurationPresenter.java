package dk.dtu.imm.red.specificationelements.configuration.ui.editors;  
import java.util.List;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.connector.Connector;

public interface ConfigurationPresenter extends IEditorPresenter {
	ElementColumnDefinition[] getPartsColumnDefinition();
	ElementColumnDefinition[] getConnectorColumnDefinition();
	EAGPresenter<Actor> getPartsRelationPresenter();
	EAGPresenter<Connector> getConnectorPresenter();
	Project getParentProject();
	List<Element> getConfigurationElements(); 

}
