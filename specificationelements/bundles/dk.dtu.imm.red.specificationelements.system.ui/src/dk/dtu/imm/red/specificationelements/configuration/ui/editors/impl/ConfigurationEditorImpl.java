package dk.dtu.imm.red.specificationelements.configuration.ui.editors.impl;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.internal.effortestimation.views.impl.EffortEstimationEditor;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage;
import dk.dtu.imm.red.specificationelements.configuration.ui.editors.ConfigurationEditor;
import dk.dtu.imm.red.specificationelements.configuration.ui.editors.ConfigurationPresenter;
import dk.dtu.imm.red.specificationelements.connector.Connector;

public class ConfigurationEditorImpl extends BaseEditor implements ConfigurationEditor, EffortEstimationEditor {

	protected ElementBasicInfoContainer basicInfoContainer;

	// Bookkeeping
	/**
	 * Index of the Configuration attr page.
	 */
	protected int configurationPageIndex;

	// Infrastructure
	private ConfigurationPresenter configurationPresenter;
	private ConfigurationViewModel viewModel;
	private ConfigurationComposite configurationComposite;

	private EAGComposite<Actor> gdPartsRelation;
	private EAGComposite<Connector> gdRelations;

	// UI Setup
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
		viewModel = new ConfigurationViewModel();
		viewModel.setElement((Configuration) element);
		presenter = new ConfigurationPresenterImpl(this, viewModel);
		configurationPresenter = (ConfigurationPresenter) presenter; // casted
																			// presenter
	}

	@Override
	protected void createPages() {

		// Create attributes page
		configurationPageIndex = addPage(createFirstPage(getContainer()));
		setPageText(configurationPageIndex, "Summary");

		super.createPages();
		fillInitialData();
	}

	private Composite createFirstPage(Composite parent) {

		configurationComposite = new ConfigurationComposite(parent, SWT.NONE, this);

		// Setup Parts Editor
		gdPartsRelation = new EAGComposite<Actor>(configurationComposite.getCmpParts(), SWT.NONE, modifyListener,
				viewModel.getElement(), ConfigurationPackage.eINSTANCE.getConfiguration_Parts())
						.setModifyListener(modifyListener)
						.setPresenter(configurationPresenter.getPartsRelationPresenter());
		gdPartsRelation.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		gdPartsRelation.setElementSelectorOnAdd(true);
		gdPartsRelation.setAddDialogTitle("Create Reference to an internal Element");
		gdPartsRelation.setAddDialogDescription("Select an internal element, for example a System.");

		// Setup Internal Connector Editor
		gdRelations = new EAGComposite<Connector>(configurationComposite.getCmpRelations(), SWT.NONE, modifyListener,
				viewModel.getElement(), ConfigurationPackage.eINSTANCE.getConfiguration_InternalConnectors())
						.setModifyListener(modifyListener)
						.setPresenter(configurationPresenter.getConnectorPresenter());
		gdRelations.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		gdRelations.setElementSelectorOnAdd(true);
		gdRelations.setAddDialogTitle("Create Reference to an internal Connector");
		gdRelations.setAddDialogDescription("Select a Connector linking internal elements of this Configuration.");

		return SWTUtils.wrapInScrolledComposite(parent, configurationComposite);
	}

	public void setIsModified() {
		modifyListener.handleEvent(null);
	}

	// Infrastructure actions
	@Override
	public void selectText(EStructuralFeature feature, int startIndex, int endIndex) {
		if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(configurationPageIndex);
		} else if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(configurationPageIndex);
		}
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {
		return null; // todo remove

	}

	@Override
	public void doSaveAs() {
		return;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		ConfigurationPresenter presenter = (ConfigurationPresenter) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(), basicInfoContainer.getName(),
				basicInfoContainer.getKind(), basicInfoContainer.getDescription());
		super.doSave(monitor);

	}

	// Fill UI widgets with data
	@Override
	protected void fillInitialData() {

		basicInfoContainer.fillInitialData(viewModel.getElement(), new String[] {});

		// Set Table Editors
		gdPartsRelation.setColumns(configurationPresenter.getPartsColumnDefinition())
				.setDataSource(viewModel.getElement().getParts());

		gdRelations.setColumns(configurationPresenter.getConnectorColumnDefinition())
				.setDataSource(viewModel.getElement().getInternalConnectors());

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	// Properties
	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.configuration.configurationeditor";
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Configuration.html";
	}

	@Override
	public List<Element> getEffortEstimationContent() {
		return configurationPresenter.getConfigurationElements();
	}

	public void setBasicInfoContainer(ElementBasicInfoContainer basicInfoContainer) {
		this.basicInfoContainer = basicInfoContainer;
	}
}
