package dk.dtu.imm.red.specificationelements.system.ui.wizards.impl;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TreeItem;

import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;
import dk.dtu.imm.red.specificationelements.port.Port;

public class CreateGlueConnectorPageImpl extends WizardPage
		implements IWizardPage{

	protected ElementTreeViewWidget sourceTreeView;
	protected ElementTreeViewWidget targetTreeView;
	
	private Port sourcePort;
	private Port targetPort;
	
	public CreateGlueConnectorPageImpl() {
		super("Create Glue Connector");
		
		setDescription("Select source and target ports for the connector");
		setTitle("Create Glue Connector");
	}

	@Override
	@SuppressWarnings("unchecked")
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		
		GridLayout layout = new GridLayout(2, true);
		composite.setLayout(layout);

		Composite sourceTreeContainer = new Composite(composite, SWT.NONE);

		GridLayout treeContainerLayout = new GridLayout(1, true);
		sourceTreeContainer.setLayout(treeContainerLayout);

		GridData treeViewerLayoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		sourceTreeContainer.setLayoutData(treeViewerLayoutData);

		sourceTreeView = new ElementTreeViewWidget(
				false, sourceTreeContainer, SWT.BORDER, new Class[]{Port.class});

		sourceTreeView.getTree().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(final MouseEvent e) {
				TreeItem item = sourceTreeView.getTree().getItem(new Point(e.x, e.y));
				if (item == null) {
					sourceTreeView.setSelection(null);
				}
			}
		});
		
		sourceTreeView.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				getContainer().updateButtons();
				if (!event.getSelection().isEmpty()) {
					IStructuredSelection selection = (IStructuredSelection) sourceTreeView.getSelection(); 
					sourcePort = (Port) selection.getFirstElement();
				}
				else {
					sourcePort = null;
				}
				
				determinePageComplete();
			}
		});
		sourceTreeView.setSelection(null);

		Composite targetTreeContainer = new Composite(composite, SWT.NONE);
		targetTreeContainer.setLayout(treeContainerLayout);
		targetTreeContainer.setLayoutData(treeViewerLayoutData);

		targetTreeView = new ElementTreeViewWidget(
				false, targetTreeContainer, SWT.BORDER, new Class[]{Port.class});

		targetTreeView.getTree().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(final MouseEvent e) {
				TreeItem item = targetTreeView.getTree().getItem(new Point(e.x, e.y));
				if (item == null) {
					targetTreeView.setSelection(null);
				}
			}
		});
		
		targetTreeView.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				getContainer().updateButtons();
				if (!event.getSelection().isEmpty()) {
					IStructuredSelection selection = (IStructuredSelection) targetTreeView.getSelection(); 
					targetPort = (Port) selection.getFirstElement();
				}
				else {
					targetPort = null;
				}
				
				determinePageComplete();
			}
		});
		targetTreeView.setSelection(null);
		
		setPageComplete(false);
		setControl(composite);
	}
	
	public Port getSelectedSource(){
		return sourcePort;
	}
	
	public Port getSelectedTarget(){
		return targetPort;
	}
	
	private void determinePageComplete() {
		if (sourceTreeView.getSelection().isEmpty() || targetTreeView.getSelection().isEmpty()) {
			setPageComplete(false);
		}
		else {
			setPageComplete(true);
		}
	}
}
