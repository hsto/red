/*
 * 
 */
package dk.dtu.imm.red.specificationelements.port.ui.editors.impl;   



import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.ActorEditor;
import dk.dtu.imm.red.specificationelements.actor.ui.editors.impl.ActorPresenterImpl;
import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.util.MultiplicityParser;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortPackage;
import dk.dtu.imm.red.specificationelements.port.RoleTableRow;
import dk.dtu.imm.red.specificationelements.port.SignatureDirection;
import dk.dtu.imm.red.specificationelements.port.ui.editors.PortPresenter;
import dk.dtu.imm.red.specificationelements.port.ui.wizards.GetModelElementReferenceWizardFactory;
 

public class PortPresenterImpl extends ActorPresenterImpl implements PortPresenter { 
	
	private PortViewModel viewModel;
	private EAGPresenter<RoleTableRow> roleRowPresenter;
	
	public PortPresenterImpl(ActorEditor visionEditor, PortViewModel viewModel) {
		super(visionEditor, viewModel);
		this.element =  viewModel.getElement();
		this.viewModel = viewModel;
		this.useCaseEditor = visionEditor; 
		
		final Port port = viewModel.getElementAsPort();
		this.roleRowPresenter = new EAGPresenter<RoleTableRow>(){

			@Override
			public void AddItem() {
				ModelElement item = GetModelElementReferenceWizardFactory
						.getModelElementReference();
				
				if (item != null) {
					final RoleTableRow row = getNewItem();
					row.setItem((Signature) item);
					
					DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
					listener.setHostingObject(port);
					listener.setHostingFeature(PortPackage.eINSTANCE.getPort_RoleTable());
					listener.setHandledObject(row);
					item.getDeletionListeners().add(listener);

					super.AddItem(row);
				}
			}
			
		};
		roleRowPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.port", "RoleTableRow");
		roleRowPresenter.setReflectedClass(RoleTableRow.class);
	}

	public PortViewModel getViewModel() {
		return viewModel;
	}

	@Override
	public EAGPresenter<RoleTableRow> getRoleRowPresenter() {
		return roleRowPresenter;
	}

	@Override
	public ElementColumnDefinition[] getPortItemColumnDefinition() {
		return new ElementColumnDefinition[] { 
				//Direction column
				new ElementColumnDefinition()
				.setHeaderName("Direction")
				.setColumnType(String.class)
				.setColumnWidth(30)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setCellItems(ElementColumnDefinition.enumToStringArray(SignatureDirection.values()))
				.setCellHandler(new CellHandler<RoleTableRow>() {

					@Override
					public Object getAttributeValue(RoleTableRow item) {
						return item.getDirection().getName();
					}

					@Override
					public void setAttributeValue(RoleTableRow item, Object value) {
						item.setDirection(SignatureDirection.getByName(value.toString()));
					} 
				}),
				
				//Name column
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setColumnWidth(50)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setReferenceGetterMethod(new String[] {"getItem"})
				.setGetterMethod(new String[] {"getItem", "getName"}),
				
				//Multiplicity column
				new ElementColumnDefinition()
				.setHeaderName("Multiplicity")
				.setColumnType(String.class)
				.setColumnWidth(60)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setCellHandler(new CellHandler<RoleTableRow>() {

					@Override
					public Object getAttributeValue(RoleTableRow item) {
						return item.getMultiplicity();
					}

					@Override
					public void setAttributeValue(RoleTableRow item, Object value) {
						item.setMultiplicity(MultiplicityParser.parse(value.toString()));
					} 
				}),
				
				//Description column
				new ElementColumnDefinition()
				.setHeaderName("Description")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setCellHandler(new CellHandler<RoleTableRow>() {

					@Override
					public Object getAttributeValue(RoleTableRow item) {
						return item.getDescription();
					}

					@Override
					public void setAttributeValue(RoleTableRow item, Object value) {
						item.setDescription(value.toString());
					} 
				})
			};
	} 
	
	
}
