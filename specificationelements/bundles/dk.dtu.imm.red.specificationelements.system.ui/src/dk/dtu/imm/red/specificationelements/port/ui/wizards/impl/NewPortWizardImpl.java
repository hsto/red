package dk.dtu.imm.red.specificationelements.port.ui.wizards.impl;

import dk.dtu.imm.red.specificationelements.actor.ui.wizards.impl.NewActorWizardImpl;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.ui.wizards.NewPortWizard;

public class NewPortWizardImpl extends NewActorWizardImpl 
	implements NewPortWizard {
	
	public NewPortWizardImpl() {
		super("Port", Port.class);
		presenter = new NewPortWizardPresenterImpl(this);
	}

}
