package dk.dtu.imm.red.specificationelements.connector.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.ui.editors.ConnectorEditor;
import dk.dtu.imm.red.specificationelements.connector.ui.editors.impl.ConnectorEditorInputImpl;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;

public class ConnectorExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element) {
		if (element instanceof Connector) {
			ConnectorEditorInputImpl editorInput = new ConnectorEditorInputImpl(
					(Connector) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, ConnectorEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);

			}
		}
	}
}
