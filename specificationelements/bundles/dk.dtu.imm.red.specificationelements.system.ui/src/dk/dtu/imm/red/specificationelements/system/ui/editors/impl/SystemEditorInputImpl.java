package dk.dtu.imm.red.specificationelements.system.ui.editors.impl; 
import dk.dtu.imm.red.specificationelements.actor.ui.editors.impl.ActorEditorInputImpl;
import dk.dtu.imm.red.specificationelements.system.System;
 

public class SystemEditorInputImpl extends ActorEditorInputImpl {

	public SystemEditorInputImpl(System system) {
		super(system);
	}

	@Override
	public String getName() {
		return "System editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for system " + element.getName();
	}
}
