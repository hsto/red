package dk.dtu.imm.red.specificationelements.port.ui.editors.impl;  
import dk.dtu.imm.red.specificationelements.actor.ui.editors.impl.ActorViewModel;
import dk.dtu.imm.red.specificationelements.port.Port;

public class PortViewModel extends ActorViewModel{
	public Port getElementAsPort() {
		return (Port) getElement();
	}
}