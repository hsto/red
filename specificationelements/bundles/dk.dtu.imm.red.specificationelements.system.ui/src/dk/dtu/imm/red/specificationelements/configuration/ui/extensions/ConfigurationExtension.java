package dk.dtu.imm.red.specificationelements.configuration.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ui.editors.ConfigurationEditor;
import dk.dtu.imm.red.specificationelements.configuration.ui.editors.impl.ConfigurationEditorInputImpl;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;

public class ConfigurationExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element){
		if (element instanceof Configuration) {
			ConfigurationEditorInputImpl editorInput = new ConfigurationEditorInputImpl(
					(Configuration) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, ConfigurationEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}

}
