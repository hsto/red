package dk.dtu.imm.red.specificationelements.system.ui.wizards;

import dk.dtu.imm.red.specificationelements.actor.ui.wizards.NewActorWizard;

public interface NewSystemWizard extends NewActorWizard {
	
	String ID = "dk.dtu.imm.red.specificationelements.system.newsystemwizard";

}
