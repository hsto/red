package dk.dtu.imm.red.specificationelements.port.ui.wizards.impl;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;
import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;
import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.handlers.CreateSignatureHandler;

public class GetModelElementReferencePageImpl extends WizardPage
		implements IWizardPage{

	protected ISelection selection;
	protected ElementTreeViewWidget treeView;
	
	protected GetModelElementReferencePageImpl() {
		super("Add Signature Item");
		
		setDescription("");
		setTitle("Create Element Reference");
	}

	@Override
	@SuppressWarnings("unchecked")
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		
		GridLayout layout = new GridLayout(2, true);
		composite.setLayout(layout);
		
		GridData descriptionLayoutData = new GridData();
		descriptionLayoutData.horizontalSpan = 2;

		GridData treeViewerLayoutData = new GridData(SWT.FILL, SWT.FILL, true,
				true);
		treeViewerLayoutData.horizontalSpan = 2;

		Composite treeContainer = new Composite(composite, SWT.NONE);

		GridLayout treeContainerLayout = new GridLayout();
		treeContainerLayout.numColumns = 1;

		treeContainer.setLayout(treeContainerLayout);
		treeContainer.setLayoutData(treeViewerLayoutData);
		
		selection = 
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();

		treeView = new ElementTreeViewWidget(
				false, treeContainer, SWT.BORDER, new Class[]{ModelElement.class});
		if (selection != null) {
			treeView.setSelection(selection);
		}

		treeView.getTree().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(final MouseEvent e) {
				TreeItem item = treeView.getTree().getItem(new Point(e.x, e.y));
				if (item == null) {
					treeView.setSelection(null);
				}
			}
		});
		
		treeView.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				getContainer().updateButtons();
				if (!event.getSelection().isEmpty()) {
					setPageComplete(true);
				}
				else {
					setPageComplete(false);
				}
			}
		});
		
		Button newElementButton = new Button(composite, SWT.PUSH);
		newElementButton.setText("New Element");
		newElementButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				IWorkbenchWindow window = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow();

				IHandlerService handlerService = (IHandlerService) window
						.getService(IHandlerService.class);

				try {
					handlerService.executeCommand(CreateSignatureHandler.ID, null);
				} catch (Exception e) {
					e.printStackTrace(); LogUtil.logError(e);
				}
			}
		});
		
		setPageComplete(true);
		setControl(composite);
	}
	
	public ModelElement getSelectedElement(){
		IStructuredSelection selection = (IStructuredSelection) treeView.getSelection(); 
		return (ModelElement) selection.getFirstElement();
	}
}
