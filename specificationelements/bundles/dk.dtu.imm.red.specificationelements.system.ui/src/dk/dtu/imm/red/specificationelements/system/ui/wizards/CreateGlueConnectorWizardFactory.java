package dk.dtu.imm.red.specificationelements.system.ui.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.specificationelements.system.ui.wizards.impl.CreateGlueConnectorWizardImpl;
import dk.dtu.imm.red.specificationelements.system.GlueConnector;
import dk.dtu.imm.red.specificationelements.system.SystemFactory;

public class CreateGlueConnectorWizardFactory { 
	
	public static GlueConnector createGlueConnector() {
		CreateGlueConnectorWizard wizard = 
				new CreateGlueConnectorWizardImpl();
		wizard.init(PlatformUI.getWorkbench(), 
				(IStructuredSelection) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection());

		WizardDialog dialog = 
				new WizardDialog(Display.getCurrent().getActiveShell(), 
						wizard);
					
		dialog.open();
		
		CreateGlueConnectorWizardPresenter presenter = (CreateGlueConnectorWizardPresenter) wizard.getPresenter();
		GlueConnector connector = SystemFactory.eINSTANCE.createGlueConnector();
		connector.setSource(presenter.getSelectedSource());
		connector.setTarget(presenter.getSelectedTarget());
		
		return connector;
	}
}
