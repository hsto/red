/*
 * 
 */
package dk.dtu.imm.red.specificationelements.connector.ui.editors.impl;   

import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude;
import dk.dtu.imm.red.specificationelements.connector.ConnectorPackage;
import dk.dtu.imm.red.specificationelements.connector.ui.editors.ConnectorEditor;
import dk.dtu.imm.red.specificationelements.connector.ui.editors.ConnectorPresenter;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;

/**
 * @author Johan Paaske Nielsen
 *
 */

public class ConnectorPresenterImpl extends BaseEditorPresenter implements ConnectorPresenter { 
	 
	protected ConnectorEditor usecaseEditor;
	private ConnectorViewModel viewModel;  
	private EAGPresenter<UseCaseScenario> scenarioPresenter;
	private EAGPresenter<Actor> sourcePresenter;    
	private EAGPresenter<Actor> targetPresenter; 
	
	public ConnectorViewModel getViewModel() {
		return viewModel;
	}

	public ConnectorPresenterImpl(ConnectorEditor visionEditor, ConnectorViewModel viewModel) {
		super(viewModel.getElement());
		this.element =  viewModel.getElement();
		this.viewModel = viewModel;
		this.usecaseEditor = visionEditor;  

		final Connector connector = viewModel.getElement();
		
		scenarioPresenter = new EAGPresenter<UseCaseScenario>(){

			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(connector);
				listener.setHostingFeature(ConnectorPackage.eINSTANCE.getConnector_Scenarios());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
				
				super.AddItem(o);
			}
		};
		scenarioPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.usecasescenario", "UseCaseScenario");
		scenarioPresenter.setReflectedClass(UseCaseScenario.class); // For element selector
		
		sourcePresenter = new EAGPresenter<Actor>(){

			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(connector);
				listener.setHostingFeature(ConnectorPackage.eINSTANCE.getConnector_Source());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
								
				super.AddItem(o);
			}
		};
		sourcePresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.actor", "Actor");
		sourcePresenter.setReflectedClass(Actor.class); // For element selector
		
		targetPresenter = new EAGPresenter<Actor>(){

			@Override
			public void AddItem(Object o) {
				Element element = (Element) o;
				
				DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
				listener.setHostingObject(connector);
				listener.setHostingFeature(ConnectorPackage.eINSTANCE.getConnector_Target());
				listener.setHandledObject(element);
				element.getDeletionListeners().add(listener);
				
				super.AddItem(o);
			}
		};
		targetPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.actor", "Actor");
		targetPresenter.setReflectedClass(Actor.class); // For element selector 
		 
	} 

	@Override
	public void save() {
		super.save(); 
		
		Connector e = viewModel.getElementBeforeEdit();
		Connector eCopy = viewModel.getElement(); 
		
		e.setName(eCopy.getName());
		e.setTag(eCopy.getTag());
		e.setDescription((eCopy.getDescription()));
		
		e.setElementKind(eCopy.getElementKind());
		e.setAttitude(eCopy.getAttitude());
		
		// TODO find better solution
//		e.getTarget().clear();
//		e.getSource().clear();
		
		e.getTarget().addAll(eCopy.getTarget());
		e.getSource().addAll(eCopy.getSource()); 
		
		e.save();
	}
	
	@Override
	public ElementColumnDefinition[] getScenariosColumnDefinition() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(8)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {"getIcon"}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getName"})
				 };
	}
	
	@Override
	public ElementColumnDefinition[] getSourceColumnDefinition() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(8)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {"getIcon"}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getName"})
				 };
	}
	
	@Override
	public ElementColumnDefinition[] getTargetDefinition() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("")
				.setColumnType(Image.class)
				.setColumnWidth(8)
				.setColumnBehaviour(ColumnBehaviour.IconDisplay)
				.setEditable(false)
				.setGetterMethod(new String[] {  "getIcon"}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] { "getName"})
				 };
	}  
	
	@Override
	public EAGPresenter<UseCaseScenario> getScenariosPresenter() {
		return scenarioPresenter;
	} 
	
	@Override
	public EAGPresenter<Actor> getSourcePresenter() {
		return sourcePresenter;
	}

	@Override
	public EAGPresenter<Actor> getTargetPresenter() {
		return targetPresenter;
	}

	@Override
	public void setAttitude(String selection) {
		 viewModel.getElement().setAttitude(Enum.valueOf(ConnectorAttitude.class, selection.toUpperCase()));  
	}
}
