package dk.dtu.imm.red.specificationelements.connector.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewConnectorWizard extends IBaseWizard {
	
	String ID = "dk.dtu.imm.red.specificationelements.connector.newconnectorwizard";

}
