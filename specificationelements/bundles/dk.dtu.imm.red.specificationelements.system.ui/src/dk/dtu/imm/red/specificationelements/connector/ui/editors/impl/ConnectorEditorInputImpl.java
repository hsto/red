package dk.dtu.imm.red.specificationelements.connector.ui.editors.impl; 
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.connector.Connector; 

public class ConnectorEditorInputImpl extends BaseEditorInput {

	public ConnectorEditorInputImpl(Connector actor) {
		super(actor);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Connector editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for Connector " + element.getName();
	}
}
