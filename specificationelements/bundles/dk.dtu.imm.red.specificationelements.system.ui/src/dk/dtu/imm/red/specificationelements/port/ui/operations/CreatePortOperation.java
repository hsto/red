package dk.dtu.imm.red.specificationelements.port.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortFactory;
import dk.dtu.imm.red.specificationelements.port.ui.extensions.PortExtension;

public class CreatePortOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected PortExtension extension;
	protected Group parent;
	
	private Port port;

	public CreatePortOperation(String label, String name, String description, Group parent, String path) {
		super("Create Port");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
		extension = new PortExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		port = PortFactory.eINSTANCE.createPort();
		port.setCreator(PreferenceUtil.getUserPreference_User());

		port.setLabel(label);
		port.setName(name);
		port.setDescription(description);

		if (parent != null) {
			port.setParent(parent);
		}

		port.save();
		extension.openElement(port);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(port);
			extension.deleteElement(port);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
