package dk.dtu.imm.red.specificationelements.configuration.ui.editors.impl;  
import dk.dtu.imm.red.core.ui.presenters.ElementHandler;
import dk.dtu.imm.red.specificationelements.configuration.Configuration; 

public class ConfigurationViewModel extends ElementHandler<Configuration> {
	
	 
	private int selectedRoleIndex; 
	public void setSelectedRoleIndex(int selectedIndex) {
		this.selectedRoleIndex = selectedIndex; 
	}
	
	public int getSelectedRoleIndex( ) {
		return selectedRoleIndex; 
	} 
}