package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors.impl;

import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.util.DataTypeParser;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.util.MultiplicityParser;
import dk.dtu.imm.red.specificationelements.modelelement.signature.AccessMode;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility;
import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors.SignaturePresenter;

public class SignaturePresenterImpl extends BaseEditorPresenter implements
		SignaturePresenter {

	protected SignatureEditorImpl signatureEditorImpl;
	
	private EAGPresenter<SignatureItem> itemsPresenter;   

	public SignaturePresenterImpl(SignatureEditorImpl signatureEditorImpl, SignatureViewModel viewModel) {
		super(viewModel.getElement());
		this.signatureEditorImpl = signatureEditorImpl;
		this.element = viewModel.getElement();
		
		itemsPresenter = new EAGPresenter<SignatureItem>();
		itemsPresenter.setReflectedEClassByName("dk.dtu.imm.red.specificationelements.modelelement.signature", "SignatureItem");
		itemsPresenter.setReflectedClass(SignatureItem.class); // For element selector
	}

	@Override
	public void save() {
		super.save();
		Signature signature = (Signature) element;

		signature.save();
		
	}
	
	@Override
	public ElementColumnDefinition[] getItemsColumnDefinition() {
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setCellHandler(new CellHandler<SignatureItem>() {

					@Override
					public Object getAttributeValue(SignatureItem item) {
						return item.getName();
					}

					@Override
					public void setAttributeValue(SignatureItem item, Object value) {
						item.setName(value.toString());;
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Type")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setCellHandler(new CellHandler<SignatureItem>() {

					@Override
					public Object getAttributeValue(SignatureItem item) {
						return item.getType().toString();
					}

					@Override
					public void setAttributeValue(SignatureItem item, Object value) {
						DataType newType = DataTypeParser.parse(value.toString());
						item.setType(newType);
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Multiplicity")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setCellHandler(new CellHandler<SignatureItem>() {

					@Override
					public Object getAttributeValue(SignatureItem item) {
						return item.getMultiplicity().toString();
					}

					@Override
					public void setAttributeValue(SignatureItem item, Object value) {
						Multiplicity newMultiplicity = MultiplicityParser.parse(value.toString());
						item.setMultiplicity(newMultiplicity);
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Visibility")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setColumnWidth(40)
				.setCellItems(ElementColumnDefinition.enumToStringArray(Visibility.values(), true))
				.setCellHandler(new CellHandler<SignatureItem>() {

					@Override
					public Object getAttributeValue(SignatureItem item) {
						return item.getVisibility().getLiteral();
					}

					@Override
					public void setAttributeValue(SignatureItem item, Object value) {
						item.setVisibility(Visibility.get(value.toString()));
					}
				}),
				new ElementColumnDefinition()
				.setHeaderName("Writability")
				.setColumnType(String.class)
				.setEditable(true)
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setColumnWidth(40)
				.setCellItems(ElementColumnDefinition.enumToStringArray(AccessMode.values(), true))
				.setCellHandler(new CellHandler<SignatureItem>() {

					@Override
					public Object getAttributeValue(SignatureItem item) {
						return item.getAccessMode().getLiteral();
					}

					@Override
					public void setAttributeValue(SignatureItem item, Object value) {
						item.setAccessMode(AccessMode.get(value.toString()));
					}
				})
				};
	}

	public EAGPresenter<SignatureItem> getItemsPresenter() {
		return itemsPresenter;
	}
}
