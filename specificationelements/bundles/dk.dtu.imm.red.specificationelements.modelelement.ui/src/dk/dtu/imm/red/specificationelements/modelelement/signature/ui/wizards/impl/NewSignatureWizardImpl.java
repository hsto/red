package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.wizards.NewSignatureWizard;
import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.wizards.NewSignatureWizardPresenter;

public class NewSignatureWizardImpl extends BaseNewWizard 
	implements NewSignatureWizard {

	protected NewSignatureWizardPresenter presenter;
	
	public NewSignatureWizardImpl(){
		super("Signature", Signature.class);
		presenter = new NewSignatureWizardPresenterImpl(this);
	}
	
	@Override
	public void addPages() {
		super.addPages();
	}

	@Override
	public boolean performFinish() {
		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription(); 

		Group parent = null;

		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();

			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}
		
		presenter.wizardFinished(label, name, description, parent, "");

		return true;
	}
	
	@Override
	public boolean canFinish() {
		return super.canFinish() 
				&& elementBasicInfoPage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}
}
