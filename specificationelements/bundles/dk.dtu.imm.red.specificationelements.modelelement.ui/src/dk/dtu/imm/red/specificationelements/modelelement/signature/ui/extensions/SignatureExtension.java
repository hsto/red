package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.extensions;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors.SignatureEditor;
import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors.impl.SignatureEditorInputImpl;

public class SignatureExtension extends AbstractSpecificationElementExtension {

	@Override
	public void openConcreteElement(Element element){
		if (element instanceof Signature) {
			SignatureEditorInputImpl editorInput =
					new SignatureEditorInputImpl((Signature) element);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, SignatureEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
