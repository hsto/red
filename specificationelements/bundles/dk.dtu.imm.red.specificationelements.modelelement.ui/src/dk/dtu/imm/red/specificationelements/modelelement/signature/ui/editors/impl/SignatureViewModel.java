package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors.impl;  
import dk.dtu.imm.red.core.ui.presenters.ElementHandler;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;

public class SignatureViewModel extends ElementHandler<Signature>{
	
	 
	private int selectedRoleIndex; 
	 

	public void setSelectedRoleIndex(int selectedIndex) {
		this.selectedRoleIndex = selectedIndex; 
	}
	
	public int getSelectedRoleIndex( ) {
		return selectedRoleIndex; 
	} 
}