package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewSignatureWizard extends IBaseWizard{

	String ID = "dk.dtu.imm.red.specificationelements.modelelement.signature.newwizard";
}
