package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors;

import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem;

public interface SignaturePresenter extends IEditorPresenter {
	ElementColumnDefinition[] getItemsColumnDefinition();
	EAGPresenter<SignatureItem> getItemsPresenter();
}
