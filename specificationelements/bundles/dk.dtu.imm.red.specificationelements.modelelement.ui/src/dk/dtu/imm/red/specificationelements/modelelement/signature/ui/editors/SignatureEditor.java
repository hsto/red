package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors;

import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface SignatureEditor extends IEditorPart, IBaseEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.modelelement.signature.signatureeditor";

}
