package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.wizards.impl.NewSignatureWizardImpl;

public class CreateSignatureHandler extends AbstractHandler {
	
	public static final String ID = "dk.dtu.imm.red.specificationelements.createsignature";

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		NewSignatureWizardImpl wizard = new NewSignatureWizardImpl();
		ISelection currentSelection = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection();
		wizard.init(PlatformUI.getWorkbench(),
				(IStructuredSelection) currentSelection);

		WizardDialog dialog = new WizardDialog(
				HandlerUtil.getActiveShell(event), wizard);
		dialog.create();
		
		dialog.open();
		
		return null;
	}

}
