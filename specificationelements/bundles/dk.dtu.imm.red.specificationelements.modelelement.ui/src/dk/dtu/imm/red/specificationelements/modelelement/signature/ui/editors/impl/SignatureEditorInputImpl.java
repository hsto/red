package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;

public class SignatureEditorInputImpl extends BaseEditorInput {

	public SignatureEditorInputImpl(Signature signature) {
		super(signature);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Signature Editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for signature " + element.getName();
	}
}
