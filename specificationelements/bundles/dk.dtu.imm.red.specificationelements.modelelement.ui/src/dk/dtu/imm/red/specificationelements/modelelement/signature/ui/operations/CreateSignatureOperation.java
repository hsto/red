package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureFactory;
import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.extensions.SignatureExtension;

public class CreateSignatureOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected Group parent;
	protected Signature signature;

	protected SignatureExtension extension;

	public CreateSignatureOperation(String label, String name, String description, Group parent, String path) {
		super("Create Signature");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;

		extension = new SignatureExtension();
	}
	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}
	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		signature = SignatureFactory.eINSTANCE.createSignature();
		signature.setCreator(PreferenceUtil.getUserPreference_User());


		signature.setLabel(label);
		signature.setName(name);
		signature.setDescription(description);

		if (parent != null) {
			signature.setParent(parent);
		}

		signature.save();
		extension.openElement(signature);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(signature);
			extension.deleteElement(signature);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
