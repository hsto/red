package dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors.impl;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureKind;
import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors.SignatureEditor;
import dk.dtu.imm.red.specificationelements.modelelement.signature.ui.editors.SignaturePresenter;


public class SignatureEditorImpl extends BaseEditor implements
		SignatureEditor {
	
	protected int signaturePageIndex;

	protected ElementBasicInfoContainer basicInfoContainer;
	
	private SignatureViewModel viewModel;
	private SignaturePresenter signaturePresenter;
	private SignatureComposite signatureComposite;
	
	EAGComposite<SignatureItem> gdItems;

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);
		viewModel = new SignatureViewModel();
		viewModel.setElement((Signature) element);
		presenter = new SignaturePresenterImpl(this, viewModel);
		signaturePresenter = (SignaturePresenter) presenter;
	}

	@Override
	public dk.dtu.imm.red.core.element.Element getEditorElement() {
		return viewModel.getElement();
	}

	@Override
	protected void createPages() {

		//Create attributes page
		signaturePageIndex = addPage(createSummaryPage(getContainer(), "Signature Properties"));
		setPageText(signaturePageIndex, "Summary");

		super.createPages();
		fillInitialData();
	}

	protected Composite createSummaryPage(Composite parent, String groupName) {
		 signatureComposite = new SignatureComposite(parent, SWT.NONE, this, groupName);

		//Setup items column
		 gdItems = new EAGComposite<SignatureItem>(
				 signatureComposite.getItemsColumnContainer(), SWT.NONE, modifyListener)
				 .setModifyListener(modifyListener)
				 .setPresenter(signaturePresenter.getItemsPresenter());
		 gdItems.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		 return SWTUtils.wrapInScrolledComposite(parent, signatureComposite);
	}

	@Override
	protected void fillInitialData() {
		Signature signature = (Signature) element;

		SignatureKind[] kinds = SignatureKind.values();
		String[] kindStrings = new String[kinds.length];
		for(int i=0;i<kinds.length;i++){
			kindStrings[i] = kinds[i].getName();
		}

		basicInfoContainer.fillInitialData(signature, kindStrings);
		
		gdItems.setColumns(signaturePresenter.getItemsColumnDefinition())
				.setDataSource(viewModel.getElement().getItems());

		isDirty = false;
		firePropertyChange(PROP_DIRTY);

	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		SignaturePresenter presenter =
				(SignaturePresenter) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());

		super.doSave(monitor);
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(signaturePageIndex);
		} else if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(signaturePageIndex);
		}
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {
		return null; //todo remove
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Signature.html";
	}

	public void setBasicInfoContainer(ElementBasicInfoContainer basicInfoContainer){
		this.basicInfoContainer = basicInfoContainer;
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.modelelement.signature.signatureeditor";
	}

}
