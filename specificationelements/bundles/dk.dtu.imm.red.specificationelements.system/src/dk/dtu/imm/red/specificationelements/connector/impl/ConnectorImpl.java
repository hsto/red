/**
 */
package dk.dtu.imm.red.specificationelements.connector.impl;

import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude;
import dk.dtu.imm.red.specificationelements.connector.ConnectorPackage;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.NoMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.SingleValueMultiplicity;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.RoleTableRow;
import dk.dtu.imm.red.specificationelements.port.SignatureDirection;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl#getTag <em>Tag</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl#getAttitude <em>Attitude</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl#getSource <em>Source</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl#getScenarios <em>Scenarios</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnectorImpl extends SpecificationElementImpl implements Connector {
	/**
	 * The default value of the '{@link #getTag() <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTag()
	 * @generated
	 * @ordered
	 */
	protected static final String TAG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTag() <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTag()
	 * @generated
	 * @ordered
	 */
	protected String tag = TAG_EDEFAULT;

	/**
	 * The default value of the '{@link #getAttitude() <em>Attitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttitude()
	 * @generated
	 * @ordered
	 */
	protected static final ConnectorAttitude ATTITUDE_EDEFAULT = ConnectorAttitude.POSITIVE;

	/**
	 * The cached value of the '{@link #getAttitude() <em>Attitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttitude()
	 * @generated
	 * @ordered
	 */
	protected ConnectorAttitude attitude = ATTITUDE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected EList<Actor> source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected EList<Actor> target;

	/**
	 * The cached value of the '{@link #getScenarios() <em>Scenarios</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenarios()
	 * @generated
	 * @ordered
	 */
	protected EList<UseCaseScenario> scenarios;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConnectorPackage.Literals.CONNECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTag(String newTag) {
		String oldTag = tag;
		tag = newTag;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConnectorPackage.CONNECTOR__TAG, oldTag, tag));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectorAttitude getAttitude() {
		return attitude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttitude(ConnectorAttitude newAttitude) {
		ConnectorAttitude oldAttitude = attitude;
		attitude = newAttitude == null ? ATTITUDE_EDEFAULT : newAttitude;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConnectorPackage.CONNECTOR__ATTITUDE, oldAttitude, attitude));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Actor> getSource() {
		if (source == null) {
			source = new EObjectResolvingEList<Actor>(Actor.class, this, ConnectorPackage.CONNECTOR__SOURCE);
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Actor> getTarget() {
		if (target == null) {
			target = new EObjectResolvingEList<Actor>(Actor.class, this, ConnectorPackage.CONNECTOR__TARGET);
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UseCaseScenario> getScenarios() {
		if (scenarios == null) {
			scenarios = new EObjectResolvingEList<UseCaseScenario>(UseCaseScenario.class, this, ConnectorPackage.CONNECTOR__SCENARIOS);
		}
		return scenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConnectorPackage.CONNECTOR__TAG:
				return getTag();
			case ConnectorPackage.CONNECTOR__ATTITUDE:
				return getAttitude();
			case ConnectorPackage.CONNECTOR__SOURCE:
				return getSource();
			case ConnectorPackage.CONNECTOR__TARGET:
				return getTarget();
			case ConnectorPackage.CONNECTOR__SCENARIOS:
				return getScenarios();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConnectorPackage.CONNECTOR__TAG:
				setTag((String)newValue);
				return;
			case ConnectorPackage.CONNECTOR__ATTITUDE:
				setAttitude((ConnectorAttitude)newValue);
				return;
			case ConnectorPackage.CONNECTOR__SOURCE:
				getSource().clear();
				getSource().addAll((Collection<? extends Actor>)newValue);
				return;
			case ConnectorPackage.CONNECTOR__TARGET:
				getTarget().clear();
				getTarget().addAll((Collection<? extends Actor>)newValue);
				return;
			case ConnectorPackage.CONNECTOR__SCENARIOS:
				getScenarios().clear();
				getScenarios().addAll((Collection<? extends UseCaseScenario>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConnectorPackage.CONNECTOR__TAG:
				setTag(TAG_EDEFAULT);
				return;
			case ConnectorPackage.CONNECTOR__ATTITUDE:
				setAttitude(ATTITUDE_EDEFAULT);
				return;
			case ConnectorPackage.CONNECTOR__SOURCE:
				getSource().clear();
				return;
			case ConnectorPackage.CONNECTOR__TARGET:
				getTarget().clear();
				return;
			case ConnectorPackage.CONNECTOR__SCENARIOS:
				getScenarios().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConnectorPackage.CONNECTOR__TAG:
				return TAG_EDEFAULT == null ? tag != null : !TAG_EDEFAULT.equals(tag);
			case ConnectorPackage.CONNECTOR__ATTITUDE:
				return attitude != ATTITUDE_EDEFAULT;
			case ConnectorPackage.CONNECTOR__SOURCE:
				return source != null && !source.isEmpty();
			case ConnectorPackage.CONNECTOR__TARGET:
				return target != null && !target.isEmpty();
			case ConnectorPackage.CONNECTOR__SCENARIOS:
				return scenarios != null && !scenarios.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tag: ");
		result.append(tag);
		result.append(", attitude: ");
		result.append(attitude);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/connector.png";
	}
	
	/**
	 * @generated NOT
	 */
	public ElementColumnDefinition[] getColumnRepresentation() { 
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("ID")
				.setEMFLiteral(ConnectorPackage.Literals.CONNECTOR__TAG) 
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Connector>() {

					@Override
					public Object getAttributeValue(Connector item) {
						return item.getTag();
					}

					@Override
					public void setAttributeValue(Connector item, Object value) {
						item.setTag((String) value);
					} 
				}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Connector>() {

					@Override
					public Object getAttributeValue(Connector item) {
						return item.getName();
					}

					@Override
					public void setAttributeValue(Connector item, Object value) {
						item.setName((String) value);
					} 
				}),   

				new ElementColumnDefinition()
				.setHeaderName("Attitude") 
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setCellItems(ElementColumnDefinition.enumToStringArray(ConnectorAttitude.values()))
				.setEditable(true)
				.setCellHandler(new CellHandler<Connector>() {

					@Override
					public Object getAttributeValue(Connector item) {
						String val = item.getAttitude().name().toString();
						return val;
					}

					@Override
					public void setAttributeValue(Connector item, Object value) {
						item.setAttitude(ConnectorAttitude.valueOf(value.toString().toUpperCase()));
					} 
					}
				)
				,
		};
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getStructeredIdType() {
		return "CO";
	}

	@Override
	public void checkStructure() {
		super.checkStructure();
		
		//Check 5A
		if (getSource().isEmpty() || getTarget().isEmpty()) {
			addIssueComment(IssueSeverity.ERROR, "One or both ends are not attached");
		}
		//Check 5B+C+D
		checkSignaturesAndDirections(getSource(), getTarget());
		checkSignaturesAndDirections(getTarget(), getSource());
	}
	
	/**
	 * @generated NOT
	 */
	private void checkSignaturesAndDirections(List<Actor> sources, List<Actor> targets) {
		for (Actor source : sources) {
			if (source instanceof Port) {
				Port sourcePort = (Port) source;
				
				for (RoleTableRow sourceRow : sourcePort.getRoleTable()) {
					for (Actor target : targets) {
						if (target instanceof Port) {
							Port targetPort = (Port) target;
							SignatureDirection requiredDirection = 
									(sourceRow.getDirection() == SignatureDirection.OUT) ? SignatureDirection.IN : SignatureDirection.OUT;
							boolean foundValidRow = false;
							
							for (RoleTableRow targetRow : targetPort.getRoleTable()) {
								if (targetRow.getItem() == sourceRow.getItem()
										&& targetRow.getDirection() == requiredDirection) {
									foundValidRow = true;
									
									if (sourceRow.getDirection() == SignatureDirection.OUT
											&& !multiplicitiesMatch(sourceRow.getMultiplicity(), targetRow.getMultiplicity())) {
										addIssueComment(IssueSeverity.MISTAKE, "Multiplicity for item '" + sourceRow.getItem().getName()
											+ "' between ports '" + sourcePort.getName() + "' and '" + targetPort.getName() + "'");
									}
									
									break;
								}
							}
							
							if (!foundValidRow) {
								addIssueComment(IssueSeverity.MISTAKE, "Could not find role item in '" + targetPort.getName()
									+ "' matching '" + sourceRow.getItem().getName() + "' in '" + sourcePort.getName() + "'");
							}
						}
					}
				}
			}
		}
	}
	
	private boolean multiplicitiesMatch(Multiplicity source, Multiplicity target) {
		if (source instanceof CompoundMultiplicity) {
			for (Multiplicity part : ((CompoundMultiplicity) source).getParts()) {
				if (!multiplicitiesMatch(part, target)) return false;
			}
			
			return true;
		}
		else if (source instanceof SingleValueMultiplicity) {
			if (((SingleValueMultiplicity) source).isUnbounded()) {
				return target.inRangeUnbounded(0);
			}
			else {
				return target.inRange(((SingleValueMultiplicity) source).getValue());
			}
		}
		else if (source instanceof RangedMultiplicity) {
			RangedMultiplicity rangedSource = (RangedMultiplicity) source;
			if (rangedSource.isUnbounded()) {
				return target.inRangeUnbounded(rangedSource.getMinimum());
			}
			else {
				for (int value = rangedSource.getMinimum(); 
						value <= rangedSource.getMaximum();
						value++) {
					if (!target.inRange(value)) return false;
				}
				
				return true;
			}
		}
		else if (source instanceof NoMultiplicity) {
			return target.inRange(1);
		}
		else {
			return false;
		}
	}

} //ConnectorImpl
