/**
 */
package dk.dtu.imm.red.specificationelements.port;

import dk.dtu.imm.red.specificationelements.actor.ActorPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.port.PortFactory
 * @model kind="package"
 * @generated
 */
public interface PortPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "port";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.specificationelements.port";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "port";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PortPackage eINSTANCE = dk.dtu.imm.red.specificationelements.port.impl.PortPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.port.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.port.impl.PortImpl
	 * @see dk.dtu.imm.red.specificationelements.port.impl.PortPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 0;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ICON_URI = ActorPackage.ACTOR__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ICON = ActorPackage.ACTOR__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__LABEL = ActorPackage.ACTOR__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__NAME = ActorPackage.ACTOR__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ELEMENT_KIND = ActorPackage.ACTOR__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__DESCRIPTION = ActorPackage.ACTOR__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__PRIORITY = ActorPackage.ACTOR__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__COMMENTLIST = ActorPackage.ACTOR__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__TIME_CREATED = ActorPackage.ACTOR__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__LAST_MODIFIED = ActorPackage.ACTOR__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__LAST_MODIFIED_BY = ActorPackage.ACTOR__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__CREATOR = ActorPackage.ACTOR__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__VERSION = ActorPackage.ACTOR__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__VISIBLE_ID = ActorPackage.ACTOR__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__UNIQUE_ID = ActorPackage.ACTOR__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__RELATES_TO = ActorPackage.ACTOR__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__RELATED_BY = ActorPackage.ACTOR__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__PARENT = ActorPackage.ACTOR__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__URI = ActorPackage.ACTOR__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__WORK_PACKAGE = ActorPackage.ACTOR__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__CHANGE_LIST = ActorPackage.ACTOR__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__RESPONSIBLE_USER = ActorPackage.ACTOR__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__DEADLINE = ActorPackage.ACTOR__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__LOCK_STATUS = ActorPackage.ACTOR__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__LOCK_PASSWORD = ActorPackage.ACTOR__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ESTIMATED_COMPLEXITY = ActorPackage.ACTOR__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__COST = ActorPackage.ACTOR__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__BENEFIT = ActorPackage.ACTOR__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__RISK = ActorPackage.ACTOR__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__LIFE_CYCLE_PHASE = ActorPackage.ACTOR__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__STATE = ActorPackage.ACTOR__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__MANAGEMENT_DECISION = ActorPackage.ACTOR__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__QA_ASSESSMENT = ActorPackage.ACTOR__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__MANAGEMENT_DISCUSSION = ActorPackage.ACTOR__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__DELETION_LISTENERS = ActorPackage.ACTOR__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__QUANTITIES = ActorPackage.ACTOR__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__CUSTOM_ATTRIBUTES = ActorPackage.ACTOR__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ID = ActorPackage.ACTOR__ID;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__CODE = ActorPackage.ACTOR__CODE;

	/**
	 * The feature id for the '<em><b>Behavior Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__BEHAVIOR_ELEMENTS = ActorPackage.ACTOR__BEHAVIOR_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Code Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__CODE_TYPE = ActorPackage.ACTOR__CODE_TYPE;

	/**
	 * The feature id for the '<em><b>Role Table</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ROLE_TABLE = ActorPackage.ACTOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = ActorPackage.ACTOR_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.port.impl.RoleTableRowImpl <em>Role Table Row</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.port.impl.RoleTableRowImpl
	 * @see dk.dtu.imm.red.specificationelements.port.impl.PortPackageImpl#getRoleTableRow()
	 * @generated
	 */
	int ROLE_TABLE_ROW = 1;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TABLE_ROW__DIRECTION = 0;

	/**
	 * The feature id for the '<em><b>Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TABLE_ROW__ITEM = 1;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TABLE_ROW__MULTIPLICITY = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TABLE_ROW__DESCRIPTION = 3;

	/**
	 * The number of structural features of the '<em>Role Table Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TABLE_ROW_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.port.SignatureDirection <em>Signature Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.port.SignatureDirection
	 * @see dk.dtu.imm.red.specificationelements.port.impl.PortPackageImpl#getSignatureDirection()
	 * @generated
	 */
	int SIGNATURE_DIRECTION = 2;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.port.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see dk.dtu.imm.red.specificationelements.port.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.specificationelements.port.Port#getRoleTable <em>Role Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role Table</em>'.
	 * @see dk.dtu.imm.red.specificationelements.port.Port#getRoleTable()
	 * @see #getPort()
	 * @generated
	 */
	EReference getPort_RoleTable();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow <em>Role Table Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Table Row</em>'.
	 * @see dk.dtu.imm.red.specificationelements.port.RoleTableRow
	 * @generated
	 */
	EClass getRoleTableRow();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see dk.dtu.imm.red.specificationelements.port.RoleTableRow#getDirection()
	 * @see #getRoleTableRow()
	 * @generated
	 */
	EAttribute getRoleTableRow_Direction();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getItem <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Item</em>'.
	 * @see dk.dtu.imm.red.specificationelements.port.RoleTableRow#getItem()
	 * @see #getRoleTableRow()
	 * @generated
	 */
	EReference getRoleTableRow_Item();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Multiplicity</em>'.
	 * @see dk.dtu.imm.red.specificationelements.port.RoleTableRow#getMultiplicity()
	 * @see #getRoleTableRow()
	 * @generated
	 */
	EReference getRoleTableRow_Multiplicity();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see dk.dtu.imm.red.specificationelements.port.RoleTableRow#getDescription()
	 * @see #getRoleTableRow()
	 * @generated
	 */
	EAttribute getRoleTableRow_Description();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.specificationelements.port.SignatureDirection <em>Signature Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Signature Direction</em>'.
	 * @see dk.dtu.imm.red.specificationelements.port.SignatureDirection
	 * @generated
	 */
	EEnum getSignatureDirection();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PortFactory getPortFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.port.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.port.impl.PortImpl
		 * @see dk.dtu.imm.red.specificationelements.port.impl.PortPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '<em><b>Role Table</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT__ROLE_TABLE = eINSTANCE.getPort_RoleTable();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.port.impl.RoleTableRowImpl <em>Role Table Row</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.port.impl.RoleTableRowImpl
		 * @see dk.dtu.imm.red.specificationelements.port.impl.PortPackageImpl#getRoleTableRow()
		 * @generated
		 */
		EClass ROLE_TABLE_ROW = eINSTANCE.getRoleTableRow();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_TABLE_ROW__DIRECTION = eINSTANCE.getRoleTableRow_Direction();

		/**
		 * The meta object literal for the '<em><b>Item</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_TABLE_ROW__ITEM = eINSTANCE.getRoleTableRow_Item();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_TABLE_ROW__MULTIPLICITY = eINSTANCE.getRoleTableRow_Multiplicity();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_TABLE_ROW__DESCRIPTION = eINSTANCE.getRoleTableRow_Description();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.port.SignatureDirection <em>Signature Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.port.SignatureDirection
		 * @see dk.dtu.imm.red.specificationelements.port.impl.PortPackageImpl#getSignatureDirection()
		 * @generated
		 */
		EEnum SIGNATURE_DIRECTION = eINSTANCE.getSignatureDirection();

	}

} //PortPackage
