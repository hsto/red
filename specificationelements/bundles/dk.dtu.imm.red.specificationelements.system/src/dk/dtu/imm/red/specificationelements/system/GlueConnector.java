/**
 */
package dk.dtu.imm.red.specificationelements.system;

import dk.dtu.imm.red.specificationelements.port.Port;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Glue Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.system.GlueConnector#getSource <em>Source</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.system.GlueConnector#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.system.SystemPackage#getGlueConnector()
 * @model
 * @generated
 */
public interface GlueConnector extends EObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(Port)
	 * @see dk.dtu.imm.red.specificationelements.system.SystemPackage#getGlueConnector_Source()
	 * @model
	 * @generated
	 */
	Port getSource();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.system.GlueConnector#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Port value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Port)
	 * @see dk.dtu.imm.red.specificationelements.system.SystemPackage#getGlueConnector_Target()
	 * @model
	 * @generated
	 */
	Port getTarget();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.system.GlueConnector#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Port value);

} // GlueConnector
