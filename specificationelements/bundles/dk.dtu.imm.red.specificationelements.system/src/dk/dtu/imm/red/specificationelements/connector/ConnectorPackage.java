/**
 */
package dk.dtu.imm.red.specificationelements.connector;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorFactory
 * @model kind="package"
 * @generated
 */
public interface ConnectorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "connector";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.specificationelements.connector";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "connector";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConnectorPackage eINSTANCE = dk.dtu.imm.red.specificationelements.connector.impl.ConnectorPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl <em>Connector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl
	 * @see dk.dtu.imm.red.specificationelements.connector.impl.ConnectorPackageImpl#getConnector()
	 * @generated
	 */
	int CONNECTOR = 0;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__ICON_URI = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__ICON = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__LABEL = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__NAME = SpecificationelementsPackage.SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__ELEMENT_KIND = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__DESCRIPTION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__PRIORITY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__COMMENTLIST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__TIME_CREATED = SpecificationelementsPackage.SPECIFICATION_ELEMENT__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__LAST_MODIFIED = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__LAST_MODIFIED_BY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__CREATOR = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__VERSION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__VISIBLE_ID = SpecificationelementsPackage.SPECIFICATION_ELEMENT__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__UNIQUE_ID = SpecificationelementsPackage.SPECIFICATION_ELEMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__RELATES_TO = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__RELATED_BY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__PARENT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__URI = SpecificationelementsPackage.SPECIFICATION_ELEMENT__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__WORK_PACKAGE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__CHANGE_LIST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__RESPONSIBLE_USER = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__DEADLINE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__LOCK_STATUS = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__LOCK_PASSWORD = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__ESTIMATED_COMPLEXITY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__COST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__BENEFIT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__RISK = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__LIFE_CYCLE_PHASE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__STATE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__MANAGEMENT_DECISION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__QA_ASSESSMENT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__MANAGEMENT_DISCUSSION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__DELETION_LISTENERS = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__QUANTITIES = SpecificationelementsPackage.SPECIFICATION_ELEMENT__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__CUSTOM_ATTRIBUTES = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__TAG = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__ATTITUDE = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__SOURCE = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__TARGET = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Scenarios</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__SCENARIOS = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR_FEATURE_COUNT = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.connector.ConnectorKind <em>Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorKind
	 * @see dk.dtu.imm.red.specificationelements.connector.impl.ConnectorPackageImpl#getConnectorKind()
	 * @generated
	 */
	int CONNECTOR_KIND = 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude <em>Attitude</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude
	 * @see dk.dtu.imm.red.specificationelements.connector.impl.ConnectorPackageImpl#getConnectorAttitude()
	 * @generated
	 */
	int CONNECTOR_ATTITUDE = 2;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.connector.Connector <em>Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connector</em>'.
	 * @see dk.dtu.imm.red.specificationelements.connector.Connector
	 * @generated
	 */
	EClass getConnector();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.connector.Connector#getTag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag</em>'.
	 * @see dk.dtu.imm.red.specificationelements.connector.Connector#getTag()
	 * @see #getConnector()
	 * @generated
	 */
	EAttribute getConnector_Tag();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.connector.Connector#getAttitude <em>Attitude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attitude</em>'.
	 * @see dk.dtu.imm.red.specificationelements.connector.Connector#getAttitude()
	 * @see #getConnector()
	 * @generated
	 */
	EAttribute getConnector_Attitude();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.specificationelements.connector.Connector#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source</em>'.
	 * @see dk.dtu.imm.red.specificationelements.connector.Connector#getSource()
	 * @see #getConnector()
	 * @generated
	 */
	EReference getConnector_Source();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.specificationelements.connector.Connector#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see dk.dtu.imm.red.specificationelements.connector.Connector#getTarget()
	 * @see #getConnector()
	 * @generated
	 */
	EReference getConnector_Target();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.specificationelements.connector.Connector#getScenarios <em>Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Scenarios</em>'.
	 * @see dk.dtu.imm.red.specificationelements.connector.Connector#getScenarios()
	 * @see #getConnector()
	 * @generated
	 */
	EReference getConnector_Scenarios();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.specificationelements.connector.ConnectorKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Kind</em>'.
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorKind
	 * @generated
	 */
	EEnum getConnectorKind();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude <em>Attitude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attitude</em>'.
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude
	 * @generated
	 */
	EEnum getConnectorAttitude();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConnectorFactory getConnectorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl <em>Connector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl
		 * @see dk.dtu.imm.red.specificationelements.connector.impl.ConnectorPackageImpl#getConnector()
		 * @generated
		 */
		EClass CONNECTOR = eINSTANCE.getConnector();

		/**
		 * The meta object literal for the '<em><b>Tag</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONNECTOR__TAG = eINSTANCE.getConnector_Tag();

		/**
		 * The meta object literal for the '<em><b>Attitude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONNECTOR__ATTITUDE = eINSTANCE.getConnector_Attitude();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTOR__SOURCE = eINSTANCE.getConnector_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTOR__TARGET = eINSTANCE.getConnector_Target();

		/**
		 * The meta object literal for the '<em><b>Scenarios</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTOR__SCENARIOS = eINSTANCE.getConnector_Scenarios();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.connector.ConnectorKind <em>Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorKind
		 * @see dk.dtu.imm.red.specificationelements.connector.impl.ConnectorPackageImpl#getConnectorKind()
		 * @generated
		 */
		EEnum CONNECTOR_KIND = eINSTANCE.getConnectorKind();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude <em>Attitude</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude
		 * @see dk.dtu.imm.red.specificationelements.connector.impl.ConnectorPackageImpl#getConnectorAttitude()
		 * @generated
		 */
		EEnum CONNECTOR_ATTITUDE = eINSTANCE.getConnectorAttitude();

	}

} //ConnectorPackage
