/**
 */
package dk.dtu.imm.red.specificationelements.configuration;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.configuration.Configuration#getTag <em>Tag</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.configuration.Configuration#getParts <em>Parts</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.configuration.Configuration#getInternalConnectors <em>Internal Connectors</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage#getConfiguration()
 * @model
 * @generated
 */
public interface Configuration extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag</em>' attribute.
	 * @see #setTag(String)
	 * @see dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage#getConfiguration_Tag()
	 * @model
	 * @generated
	 */
	String getTag();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.configuration.Configuration#getTag <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag</em>' attribute.
	 * @see #getTag()
	 * @generated
	 */
	void setTag(String value);

	/**
	 * Returns the value of the '<em><b>Parts</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.actor.Actor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parts</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage#getConfiguration_Parts()
	 * @model
	 * @generated
	 */
	EList<Actor> getParts();

	/**
	 * Returns the value of the '<em><b>Internal Connectors</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.connector.Connector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Connectors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Connectors</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage#getConfiguration_InternalConnectors()
	 * @model
	 * @generated
	 */
	EList<Connector> getInternalConnectors();

} // Configuration
