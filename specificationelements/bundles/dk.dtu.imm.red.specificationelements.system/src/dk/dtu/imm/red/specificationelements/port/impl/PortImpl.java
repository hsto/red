/**
 */
package dk.dtu.imm.red.specificationelements.port.impl;

import dk.dtu.imm.red.core.element.ComplexityType;
import dk.dtu.imm.red.specificationelements.actor.impl.ActorImpl;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortPackage;
import dk.dtu.imm.red.specificationelements.port.RoleTableRow;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.port.impl.PortImpl#getRoleTable <em>Role Table</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortImpl extends ActorImpl implements Port {
	/**
	 * The cached value of the '{@link #getRoleTable() <em>Role Table</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleTable()
	 * @generated
	 * @ordered
	 */
	protected EList<RoleTableRow> roleTable;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortPackage.Literals.PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoleTableRow> getRoleTable() {
		if (roleTable == null) {
			roleTable = new EObjectContainmentEList<RoleTableRow>(RoleTableRow.class, this, PortPackage.PORT__ROLE_TABLE);
		}
		return roleTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.PORT__ROLE_TABLE:
				return ((InternalEList<?>)getRoleTable()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortPackage.PORT__ROLE_TABLE:
				return getRoleTable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortPackage.PORT__ROLE_TABLE:
				getRoleTable().clear();
				getRoleTable().addAll((Collection<? extends RoleTableRow>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortPackage.PORT__ROLE_TABLE:
				getRoleTable().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortPackage.PORT__ROLE_TABLE:
				return roleTable != null && !roleTable.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ComplexityType getEstimatedComplexity() {
		return ComplexityType.MEDIUM;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {

		return "icons/port.png";
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getStructeredIdType() {
		return "PO";
	}

} //PortImpl
