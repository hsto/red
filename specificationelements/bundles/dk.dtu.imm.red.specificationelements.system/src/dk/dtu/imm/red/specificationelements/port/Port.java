/**
 */
package dk.dtu.imm.red.specificationelements.port;

import dk.dtu.imm.red.specificationelements.actor.Actor;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.port.Port#getRoleTable <em>Role Table</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.port.PortPackage#getPort()
 * @model
 * @generated
 */
public interface Port extends Actor {
	/**
	 * Returns the value of the '<em><b>Role Table</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.port.RoleTableRow}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Table</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Table</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.port.PortPackage#getPort_RoleTable()
	 * @model containment="true"
	 * @generated
	 */
	EList<RoleTableRow> getRoleTable();

} // Port
