/**
 */
package dk.dtu.imm.red.specificationelements.system.impl;

import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.element.ComplexityType;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.impl.ActorImpl;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.ConfigurationReference;
import dk.dtu.imm.red.specificationelements.system.GlueConnector;
import dk.dtu.imm.red.specificationelements.system.SystemPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.system.impl.SystemImpl#getContainedConfigurations <em>Contained Configurations</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.system.impl.SystemImpl#getGlueConnectors <em>Glue Connectors</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.system.impl.SystemImpl#getPorts <em>Ports</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SystemImpl extends ActorImpl implements dk.dtu.imm.red.specificationelements.system.System {
	/**
	 * The cached value of the '{@link #getContainedConfigurations() <em>Contained Configurations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedConfigurations()
	 * @generated
	 * @ordered
	 */
	protected EList<ConfigurationReference> containedConfigurations;

	/**
	 * The cached value of the '{@link #getGlueConnectors() <em>Glue Connectors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlueConnectors()
	 * @generated
	 * @ordered
	 */
	protected EList<GlueConnector> glueConnectors;
	/**
	 * The cached value of the '{@link #getPorts() <em>Ports</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<Port> ports;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemPackage.Literals.SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConfigurationReference> getContainedConfigurations() {
		if (containedConfigurations == null) {
			containedConfigurations = new EObjectContainmentEList<ConfigurationReference>(ConfigurationReference.class, this, SystemPackage.SYSTEM__CONTAINED_CONFIGURATIONS);
		}
		return containedConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GlueConnector> getGlueConnectors() {
		if (glueConnectors == null) {
			glueConnectors = new EObjectContainmentEList<GlueConnector>(GlueConnector.class, this, SystemPackage.SYSTEM__GLUE_CONNECTORS);
		}
		return glueConnectors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Port> getPorts() {
		if (ports == null) {
			ports = new EObjectResolvingEList<Port>(Port.class, this, SystemPackage.SYSTEM__PORTS);
		}
		return ports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemPackage.SYSTEM__CONTAINED_CONFIGURATIONS:
				return ((InternalEList<?>)getContainedConfigurations()).basicRemove(otherEnd, msgs);
			case SystemPackage.SYSTEM__GLUE_CONNECTORS:
				return ((InternalEList<?>)getGlueConnectors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemPackage.SYSTEM__CONTAINED_CONFIGURATIONS:
				return getContainedConfigurations();
			case SystemPackage.SYSTEM__GLUE_CONNECTORS:
				return getGlueConnectors();
			case SystemPackage.SYSTEM__PORTS:
				return getPorts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemPackage.SYSTEM__CONTAINED_CONFIGURATIONS:
				getContainedConfigurations().clear();
				getContainedConfigurations().addAll((Collection<? extends ConfigurationReference>)newValue);
				return;
			case SystemPackage.SYSTEM__GLUE_CONNECTORS:
				getGlueConnectors().clear();
				getGlueConnectors().addAll((Collection<? extends GlueConnector>)newValue);
				return;
			case SystemPackage.SYSTEM__PORTS:
				getPorts().clear();
				getPorts().addAll((Collection<? extends Port>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemPackage.SYSTEM__CONTAINED_CONFIGURATIONS:
				getContainedConfigurations().clear();
				return;
			case SystemPackage.SYSTEM__GLUE_CONNECTORS:
				getGlueConnectors().clear();
				return;
			case SystemPackage.SYSTEM__PORTS:
				getPorts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemPackage.SYSTEM__CONTAINED_CONFIGURATIONS:
				return containedConfigurations != null && !containedConfigurations.isEmpty();
			case SystemPackage.SYSTEM__GLUE_CONNECTORS:
				return glueConnectors != null && !glueConnectors.isEmpty();
			case SystemPackage.SYSTEM__PORTS:
				return ports != null && !ports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ComplexityType getEstimatedComplexity() {
		return ComplexityType.SMALL;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {

		return "icons/system.png";
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getStructeredIdType() {
		return "SY";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void checkStructure() {
		super.checkStructure();
		
		//Check 2A
		findCircularStructure(this);
		//Check 2B
		for (GlueConnector glue : getGlueConnectors()) {
			if (!getPorts().contains(glue.getSource())
					&& !getPorts().contains(glue.getTarget())) {
				addIssueComment(IssueSeverity.ERROR, "Neither of glued ports '" + glue.getSource().getName()
						+ "' and '" + glue.getTarget().getName() + "' are external ports");
			}
		}
		//Check 2F
		if (getPorts().isEmpty()) {
			addIssueComment(IssueSeverity.WARNING, "System has no ports");
		}
	}
	
	/**
	 * @generated NOT
	 */
	private void findCircularStructure(System system) {
		for (ConfigurationReference reference : system.getContainedConfigurations()) {
			Configuration configuration = reference.getConfiguration();
			
			for (Actor part : configuration.getParts()) {
				if (this == part) {
					addIssueComment(IssueSeverity.ERROR, "Circularly defined through configurations '" + configuration.getName() + "'");
				}
				else if (part instanceof System) {
					findCircularStructure((System) part);
				}
			}
		}
	}

} //SystemImpl
