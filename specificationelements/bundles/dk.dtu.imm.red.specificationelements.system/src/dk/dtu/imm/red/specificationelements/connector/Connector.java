/**
 */
package dk.dtu.imm.red.specificationelements.connector;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.Connector#getTag <em>Tag</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.Connector#getAttitude <em>Attitude</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.Connector#getSource <em>Source</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.Connector#getTarget <em>Target</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.connector.Connector#getScenarios <em>Scenarios</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorPackage#getConnector()
 * @model
 * @generated
 */
public interface Connector extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag</em>' attribute.
	 * @see #setTag(String)
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorPackage#getConnector_Tag()
	 * @model
	 * @generated
	 */
	String getTag();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.connector.Connector#getTag <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag</em>' attribute.
	 * @see #getTag()
	 * @generated
	 */
	void setTag(String value);

	/**
	 * Returns the value of the '<em><b>Attitude</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attitude</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attitude</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude
	 * @see #setAttitude(ConnectorAttitude)
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorPackage#getConnector_Attitude()
	 * @model
	 * @generated
	 */
	ConnectorAttitude getAttitude();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.connector.Connector#getAttitude <em>Attitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attitude</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorAttitude
	 * @see #getAttitude()
	 * @generated
	 */
	void setAttitude(ConnectorAttitude value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.actor.Actor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorPackage#getConnector_Source()
	 * @model
	 * @generated
	 */
	EList<Actor> getSource();

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.actor.Actor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorPackage#getConnector_Target()
	 * @model
	 * @generated
	 */
	EList<Actor> getTarget();

	/**
	 * Returns the value of the '<em><b>Scenarios</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenarios</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenarios</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.connector.ConnectorPackage#getConnector_Scenarios()
	 * @model
	 * @generated
	 */
	EList<UseCaseScenario> getScenarios();

} // Connector
