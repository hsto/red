/**
 */
package dk.dtu.imm.red.specificationelements.port;

import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Table Row</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getDirection <em>Direction</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getItem <em>Item</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getDescription <em>Description</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.port.PortPackage#getRoleTableRow()
 * @model
 * @generated
 */
public interface RoleTableRow extends EObject {
	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.port.SignatureDirection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.port.SignatureDirection
	 * @see #setDirection(SignatureDirection)
	 * @see dk.dtu.imm.red.specificationelements.port.PortPackage#getRoleTableRow_Direction()
	 * @model
	 * @generated
	 */
	SignatureDirection getDirection();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.port.SignatureDirection
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(SignatureDirection value);

	/**
	 * Returns the value of the '<em><b>Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item</em>' reference.
	 * @see #setItem(ModelElement)
	 * @see dk.dtu.imm.red.specificationelements.port.PortPackage#getRoleTableRow_Item()
	 * @model
	 * @generated
	 */
	ModelElement getItem();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getItem <em>Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Item</em>' reference.
	 * @see #getItem()
	 * @generated
	 */
	void setItem(ModelElement value);

	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see dk.dtu.imm.red.specificationelements.port.PortPackage#getRoleTableRow_Multiplicity()
	 * @model containment="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see dk.dtu.imm.red.specificationelements.port.PortPackage#getRoleTableRow_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.port.RoleTableRow#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

} // RoleTableRow
