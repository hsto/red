/**
 */
package dk.dtu.imm.red.specificationelements.system;

import dk.dtu.imm.red.specificationelements.actor.Actor;

import dk.dtu.imm.red.specificationelements.port.Port;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.system.System#getContainedConfigurations <em>Contained Configurations</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.system.System#getGlueConnectors <em>Glue Connectors</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.system.System#getPorts <em>Ports</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.system.SystemPackage#getSystem()
 * @model
 * @generated
 */
public interface System extends Actor {
	/**
	 * Returns the value of the '<em><b>Contained Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.system.ConfigurationReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained Configurations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Configurations</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.system.SystemPackage#getSystem_ContainedConfigurations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConfigurationReference> getContainedConfigurations();

	/**
	 * Returns the value of the '<em><b>Glue Connectors</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.system.GlueConnector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Glue Connectors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Glue Connectors</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.system.SystemPackage#getSystem_GlueConnectors()
	 * @model containment="true"
	 * @generated
	 */
	EList<GlueConnector> getGlueConnectors();

	/**
	 * Returns the value of the '<em><b>Ports</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.port.Port}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ports</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ports</em>' reference list.
	 * @see dk.dtu.imm.red.specificationelements.system.SystemPackage#getSystem_Ports()
	 * @model
	 * @generated
	 */
	EList<Port> getPorts();

} // System
