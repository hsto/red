/**
 */
package dk.dtu.imm.red.specificationelements.configuration.impl;

import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.System;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.configuration.impl.ConfigurationImpl#getTag <em>Tag</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.configuration.impl.ConfigurationImpl#getParts <em>Parts</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.configuration.impl.ConfigurationImpl#getInternalConnectors <em>Internal Connectors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConfigurationImpl extends SpecificationElementImpl implements Configuration {
	/**
	 * The default value of the '{@link #getTag() <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTag()
	 * @generated
	 * @ordered
	 */
	protected static final String TAG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTag() <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTag()
	 * @generated
	 * @ordered
	 */
	protected String tag = TAG_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParts() <em>Parts</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParts()
	 * @generated
	 * @ordered
	 */
	protected EList<Actor> parts;

	/**
	 * The cached value of the '{@link #getInternalConnectors() <em>Internal Connectors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalConnectors()
	 * @generated
	 * @ordered
	 */
	protected EList<Connector> internalConnectors;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigurationPackage.Literals.CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTag(String newTag) {
		String oldTag = tag;
		tag = newTag;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIGURATION__TAG, oldTag, tag));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Actor> getParts() {
		if (parts == null) {
			parts = new EObjectResolvingEList<Actor>(Actor.class, this, ConfigurationPackage.CONFIGURATION__PARTS);
		}
		return parts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Connector> getInternalConnectors() {
		if (internalConnectors == null) {
			internalConnectors = new EObjectResolvingEList<Connector>(Connector.class, this, ConfigurationPackage.CONFIGURATION__INTERNAL_CONNECTORS);
		}
		return internalConnectors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigurationPackage.CONFIGURATION__TAG:
				return getTag();
			case ConfigurationPackage.CONFIGURATION__PARTS:
				return getParts();
			case ConfigurationPackage.CONFIGURATION__INTERNAL_CONNECTORS:
				return getInternalConnectors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigurationPackage.CONFIGURATION__TAG:
				setTag((String)newValue);
				return;
			case ConfigurationPackage.CONFIGURATION__PARTS:
				getParts().clear();
				getParts().addAll((Collection<? extends Actor>)newValue);
				return;
			case ConfigurationPackage.CONFIGURATION__INTERNAL_CONNECTORS:
				getInternalConnectors().clear();
				getInternalConnectors().addAll((Collection<? extends Connector>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.CONFIGURATION__TAG:
				setTag(TAG_EDEFAULT);
				return;
			case ConfigurationPackage.CONFIGURATION__PARTS:
				getParts().clear();
				return;
			case ConfigurationPackage.CONFIGURATION__INTERNAL_CONNECTORS:
				getInternalConnectors().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.CONFIGURATION__TAG:
				return TAG_EDEFAULT == null ? tag != null : !TAG_EDEFAULT.equals(tag);
			case ConfigurationPackage.CONFIGURATION__PARTS:
				return parts != null && !parts.isEmpty();
			case ConfigurationPackage.CONFIGURATION__INTERNAL_CONNECTORS:
				return internalConnectors != null && !internalConnectors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tag: ");
		result.append(tag);
		result.append(')');
		return result.toString();
	}
	
	/** 
	 * @generated NOT
	 */
	public ElementColumnDefinition[] getColumnRepresentation() { 
		return new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
				.setHeaderName("Tag")
				.setEMFLiteral(ConfigurationPackage.Literals.CONFIGURATION__TAG) 
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Configuration>() {

					@Override
					public Object getAttributeValue(Configuration item) {
						return item.getTag()==null?"":item.getTag();
					}

					@Override
					public void setAttributeValue(Configuration item, Object value) {
						item.setTag((String) value);
					} 
				}),
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Configuration>() {

					@Override
					public Object getAttributeValue(Configuration item) {
						return item.getName()==null?"":item.getName();
					}

					@Override
					public void setAttributeValue(Configuration item, Object value) {
						item.setName((String) value);
					} 
				}) };
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/configuration.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void checkStructure() {
		super.checkStructure();
		
		//Check 2C
		for (Connector connector : getInternalConnectors()) {
			List<Actor> unfoundEndpoints = new ArrayList<Actor>();
			unfoundEndpoints.addAll(connector.getSource());
			unfoundEndpoints.addAll(connector.getTarget());
			
			for (Actor part : getParts()) {
				unfoundEndpoints.remove(part);
				
				if (part instanceof System) {
					System system = (System) part;
					
					for (Port port : system.getPorts()) {
						unfoundEndpoints.remove(port);
					}
				}
				
				if (unfoundEndpoints.isEmpty()) break;
			}
			
			if (!unfoundEndpoints.isEmpty()) {
				addIssueComment(IssueSeverity.ERROR, "Endpoint(s) of connector '" + connector.getName()
						+ "' not part of this configuration");
			}
		}
	}

} //ConfigurationImpl
