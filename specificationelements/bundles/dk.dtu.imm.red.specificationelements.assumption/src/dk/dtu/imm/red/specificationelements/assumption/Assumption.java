/**
 */
package dk.dtu.imm.red.specificationelements.assumption;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assumption</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.specificationelements.assumption.AssumptionPackage#getAssumption()
 * @model
 * @generated
 */
public interface Assumption extends SpecificationElement {

} // Assumption
