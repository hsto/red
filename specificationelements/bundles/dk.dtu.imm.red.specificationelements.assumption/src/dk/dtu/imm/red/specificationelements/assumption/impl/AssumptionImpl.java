/**
 */
package dk.dtu.imm.red.specificationelements.assumption.impl;

import org.eclipse.emf.ecore.EClass;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;
import dk.dtu.imm.red.specificationelements.assumption.AssumptionPackage;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assumption</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AssumptionImpl extends SpecificationElementImpl implements Assumption {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssumptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssumptionPackage.Literals.ASSUMPTION;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/assumption.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {

		return new ElementColumnDefinition[] {
				new ElementColumnDefinition()
				.setHeaderName("Title")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Assumption>() {

					@Override
					public Object getAttributeValue(Assumption item) {
						return item.getName()==null?"":item.getName();
					}

					@Override
					public void setAttributeValue(Assumption item, Object value) {
						 item.setName((String) value);
					}
				})
		};
	}


} //AssumptionImpl
