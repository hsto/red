/**
 */
package dk.dtu.imm.red.specificationelements.assumption.impl;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import dk.dtu.imm.red.specificationelements.assumption.Assumption;
import dk.dtu.imm.red.specificationelements.assumption.AssumptionFactory;
import dk.dtu.imm.red.specificationelements.assumption.AssumptionKind;
import dk.dtu.imm.red.specificationelements.assumption.AssumptionPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AssumptionPackageImpl extends EPackageImpl implements AssumptionPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assumptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assumptionKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.specificationelements.assumption.AssumptionPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AssumptionPackageImpl() {
		super(eNS_URI, AssumptionFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AssumptionPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AssumptionPackage init() {
		if (isInited) return (AssumptionPackage)EPackage.Registry.INSTANCE.getEPackage(AssumptionPackage.eNS_URI);

		// Obtain or create and register package
		AssumptionPackageImpl theAssumptionPackage = (AssumptionPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AssumptionPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AssumptionPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theAssumptionPackage.createPackageContents();

		// Initialize created meta-data
		theAssumptionPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAssumptionPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AssumptionPackage.eNS_URI, theAssumptionPackage);
		return theAssumptionPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssumption() {
		return assumptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssumptionKind() {
		return assumptionKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssumptionFactory getAssumptionFactory() {
		return (AssumptionFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		assumptionEClass = createEClass(ASSUMPTION);

		// Create enums
		assumptionKindEEnum = createEEnum(ASSUMPTION_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SpecificationelementsPackage theSpecificationelementsPackage = (SpecificationelementsPackage)EPackage.Registry.INSTANCE.getEPackage(SpecificationelementsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		assumptionEClass.getESuperTypes().add(theSpecificationelementsPackage.getSpecificationElement());

		// Initialize classes and features; add operations and parameters
		initEClass(assumptionEClass, Assumption.class, "Assumption", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(assumptionKindEEnum, AssumptionKind.class, "AssumptionKind");
		addEEnumLiteral(assumptionKindEEnum, AssumptionKind.UNSPECIFIED);
		addEEnumLiteral(assumptionKindEEnum, AssumptionKind.TENTATIVE);
		addEEnumLiteral(assumptionKindEEnum, AssumptionKind.UNCONFIRMED);
		addEEnumLiteral(assumptionKindEEnum, AssumptionKind.CONFIRMED);

		// Create resource
		createResource(eNS_URI);
	}

} //AssumptionPackageImpl
