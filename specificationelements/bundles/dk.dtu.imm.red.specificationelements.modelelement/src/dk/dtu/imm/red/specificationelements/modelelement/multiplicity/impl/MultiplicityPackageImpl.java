/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl;

import dk.dtu.imm.red.specificationelements.modelelement.impl.ModelelementPackageImpl;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityFactory;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.NoMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.SingleValueMultiplicity;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.UnparsedMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage;

import dk.dtu.imm.red.specificationelements.modelelement.signature.impl.SignaturePackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MultiplicityPackageImpl extends EPackageImpl implements MultiplicityPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplicityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singleValueMultiplicityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangedMultiplicityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compoundMultiplicityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unparsedMultiplicityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass noMultiplicityEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MultiplicityPackageImpl() {
		super(eNS_URI, MultiplicityFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MultiplicityPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MultiplicityPackage init() {
		if (isInited) return (MultiplicityPackage)EPackage.Registry.INSTANCE.getEPackage(MultiplicityPackage.eNS_URI);

		// Obtain or create and register package
		MultiplicityPackageImpl theMultiplicityPackage = (MultiplicityPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MultiplicityPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MultiplicityPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ModelelementPackageImpl theModelelementPackage = (ModelelementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ModelelementPackage.eNS_URI) instanceof ModelelementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ModelelementPackage.eNS_URI) : ModelelementPackage.eINSTANCE);
		SignaturePackageImpl theSignaturePackage = (SignaturePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SignaturePackage.eNS_URI) instanceof SignaturePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SignaturePackage.eNS_URI) : SignaturePackage.eINSTANCE);
		DatatypePackageImpl theDatatypePackage = (DatatypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) instanceof DatatypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) : DatatypePackage.eINSTANCE);

		// Create package meta-data objects
		theMultiplicityPackage.createPackageContents();
		theModelelementPackage.createPackageContents();
		theSignaturePackage.createPackageContents();
		theDatatypePackage.createPackageContents();

		// Initialize created meta-data
		theMultiplicityPackage.initializePackageContents();
		theModelelementPackage.initializePackageContents();
		theSignaturePackage.initializePackageContents();
		theDatatypePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMultiplicityPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MultiplicityPackage.eNS_URI, theMultiplicityPackage);
		return theMultiplicityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplicity() {
		return multiplicityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSingleValueMultiplicity() {
		return singleValueMultiplicityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSingleValueMultiplicity_Value() {
		return (EAttribute)singleValueMultiplicityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRangedMultiplicity() {
		return rangedMultiplicityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRangedMultiplicity_Minimum() {
		return (EAttribute)rangedMultiplicityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRangedMultiplicity_Maximum() {
		return (EAttribute)rangedMultiplicityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompoundMultiplicity() {
		return compoundMultiplicityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompoundMultiplicity_Parts() {
		return (EReference)compoundMultiplicityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnparsedMultiplicity() {
		return unparsedMultiplicityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnparsedMultiplicity_Definition() {
		return (EAttribute)unparsedMultiplicityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNoMultiplicity() {
		return noMultiplicityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityFactory getMultiplicityFactory() {
		return (MultiplicityFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		multiplicityEClass = createEClass(MULTIPLICITY);

		singleValueMultiplicityEClass = createEClass(SINGLE_VALUE_MULTIPLICITY);
		createEAttribute(singleValueMultiplicityEClass, SINGLE_VALUE_MULTIPLICITY__VALUE);

		rangedMultiplicityEClass = createEClass(RANGED_MULTIPLICITY);
		createEAttribute(rangedMultiplicityEClass, RANGED_MULTIPLICITY__MINIMUM);
		createEAttribute(rangedMultiplicityEClass, RANGED_MULTIPLICITY__MAXIMUM);

		compoundMultiplicityEClass = createEClass(COMPOUND_MULTIPLICITY);
		createEReference(compoundMultiplicityEClass, COMPOUND_MULTIPLICITY__PARTS);

		unparsedMultiplicityEClass = createEClass(UNPARSED_MULTIPLICITY);
		createEAttribute(unparsedMultiplicityEClass, UNPARSED_MULTIPLICITY__DEFINITION);

		noMultiplicityEClass = createEClass(NO_MULTIPLICITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		singleValueMultiplicityEClass.getESuperTypes().add(this.getMultiplicity());
		rangedMultiplicityEClass.getESuperTypes().add(this.getMultiplicity());
		compoundMultiplicityEClass.getESuperTypes().add(this.getMultiplicity());
		unparsedMultiplicityEClass.getESuperTypes().add(this.getMultiplicity());
		noMultiplicityEClass.getESuperTypes().add(this.getMultiplicity());

		// Initialize classes and features; add operations and parameters
		initEClass(multiplicityEClass, Multiplicity.class, "Multiplicity", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(multiplicityEClass, theEcorePackage.getEBoolean(), "inRange", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(multiplicityEClass, theEcorePackage.getEBoolean(), "inRangeUnbounded", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "minimum", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(singleValueMultiplicityEClass, SingleValueMultiplicity.class, "SingleValueMultiplicity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSingleValueMultiplicity_Value(), theEcorePackage.getEInt(), "value", null, 0, 1, SingleValueMultiplicity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(singleValueMultiplicityEClass, theEcorePackage.getEBoolean(), "isUnbounded", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(rangedMultiplicityEClass, RangedMultiplicity.class, "RangedMultiplicity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRangedMultiplicity_Minimum(), theEcorePackage.getEInt(), "minimum", null, 0, 1, RangedMultiplicity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRangedMultiplicity_Maximum(), theEcorePackage.getEInt(), "maximum", null, 0, 1, RangedMultiplicity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(rangedMultiplicityEClass, theEcorePackage.getEBoolean(), "isUnbounded", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(compoundMultiplicityEClass, CompoundMultiplicity.class, "CompoundMultiplicity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompoundMultiplicity_Parts(), this.getMultiplicity(), null, "parts", null, 0, -1, CompoundMultiplicity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unparsedMultiplicityEClass, UnparsedMultiplicity.class, "UnparsedMultiplicity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUnparsedMultiplicity_Definition(), theEcorePackage.getEString(), "definition", null, 0, 1, UnparsedMultiplicity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(noMultiplicityEClass, NoMultiplicity.class, "NoMultiplicity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //MultiplicityPackageImpl
