/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.signature.impl;

import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.NoType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType;
import dk.dtu.imm.red.specificationelements.modelelement.impl.ModelElementImpl;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.UnparsedMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.signature.impl.SignatureImpl#getItems <em>Items</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignatureImpl extends ModelElementImpl implements Signature {
	/**
	 * The cached value of the '{@link #getItems() <em>Items</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItems()
	 * @generated
	 * @ordered
	 */
	protected EList<SignatureItem> items;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignaturePackage.Literals.SIGNATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SignatureItem> getItems() {
		if (items == null) {
			items = new EObjectContainmentEList<SignatureItem>(SignatureItem.class, this, SignaturePackage.SIGNATURE__ITEMS);
		}
		return items;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SignaturePackage.SIGNATURE__ITEMS:
				return ((InternalEList<?>)getItems()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SignaturePackage.SIGNATURE__ITEMS:
				return getItems();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SignaturePackage.SIGNATURE__ITEMS:
				getItems().clear();
				getItems().addAll((Collection<? extends SignatureItem>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SignaturePackage.SIGNATURE__ITEMS:
				getItems().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SignaturePackage.SIGNATURE__ITEMS:
				return items != null && !items.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/signature.png";
	}

	@Override
	public void checkStructure() {
		super.checkStructure();
		
		//Check 3A
		if (getItems().isEmpty()) {
			addIssueComment(IssueSeverity.MISTAKE, "Has no members");
		}
		else {
			for (SignatureItem item : getItems()) {
				//Check 3C
				if (item.getName() == null || item.getName().equals("")) {
					addIssueComment(IssueSeverity.MISTAKE, "A member is missing a name");
				}
				//Check 3B
				if (item.getType() instanceof UnparsedType) {
					addIssueComment(IssueSeverity.MISTAKE, "Type definition of '" + item.getName() + "' could not be parsed");
				}
				else if (item.getMultiplicity() instanceof UnparsedMultiplicity) {
					addIssueComment(IssueSeverity.MISTAKE, "Multiplicity definition of '" + item.getName() + "' could not be parsed");
				}
				//Check 3D
				else if (item.getType() instanceof NoType) {
					addIssueComment(IssueSeverity.MISTAKE, "Member '" + item.getName() + "' has no type");
				}
			}
		}
	}
} //SignatureImpl
