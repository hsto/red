package dk.dtu.imm.red.specificationelements.modelelement.datatype.util;

import java.util.Arrays;
import java.util.List;

import org.codehaus.jparsec.Parser;
import org.codehaus.jparsec.Parsers;
import org.codehaus.jparsec.Scanners;
import org.codehaus.jparsec.Terminals;
import org.codehaus.jparsec.Tokens;
import org.codehaus.jparsec.WithSource;
import org.codehaus.jparsec.error.ParserException;
import org.codehaus.jparsec.functors.Map;
import org.codehaus.jparsec.functors.Map2;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.ArrayType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.BaseType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.ChoiceType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypeFactory;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.FunctionType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.NestedType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.SignatureReference;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.TupleType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.TypeVariable;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType;

public class DataTypeParser {

    // Common parser macros

    /**
     * Read over and discard whitespace, if any exists.
     */
    private static Parser<Void> OPT_WHITESPACE = Scanners.WHITESPACES.optional();

    /**
     * Read over and discard the given string.
     */
    private static Parser<Void> STRING(String str) { return Scanners.string(str); }

    /**
     * Read and return the characters read up until the given string.
     * (Consumes and discards the delimiter)
     */
    private static Parser<String> NAIVE_UNTIL(String delimiter) {
        return Scanners.ANY_CHAR.withSource().until(STRING(delimiter)).map(new Map<List<WithSource<Void>>, String>() {
            public String map(List<WithSource<Void>> withSources) {
                StringBuilder result = new StringBuilder();
                for (WithSource<Void> source : withSources) {
                    result.append(source.getSource());
                }

                return result.toString();
            }
        }).followedBy(STRING(delimiter));
    }

    /**
     * Reference to the top-level parser object.
     */
    private static Parser.Reference<DataType> dataTypeParserRef = Parser.newReference();

    // Prefix parsers
    private static Parser<DataType> parenthesisParser = Parsers.between(
            STRING("("),
            dataTypeParserRef.lazy(),
            OPT_WHITESPACE.next(STRING(")")))
            .map(new Map<DataType, DataType>() {
                public DataType map(DataType dataType) {
                	NestedType type = DatatypeFactory.eINSTANCE.createNestedType();
                	type.setContainedType(dataType);
                    return type;
                }
            });

    private static Parser<DataType> baseTypeParser = Terminals.operators("unit", "bool", "int", "float", "string", "char")
            .tokenizer().map(new Map<Object, DataType>() {
                public DataType map(Object o) {
                	BaseType type = DatatypeFactory.eINSTANCE.createBaseType();
                	type.setName(((Tokens.Fragment) o).text());
                    return type;
                }
            });

    private static Parser<DataType> choiceTypeListParser = OPT_WHITESPACE.next(Scanners.IDENTIFIER).followedBy(OPT_WHITESPACE).sepBy(STRING(",")).map(new Map<List<String>, DataType>() {
        public DataType map(List<String> strings) {
        	ChoiceType type = DatatypeFactory.eINSTANCE.createChoiceType();
        	type.getOptions().addAll(strings);
            return type;
        }
    });

    private static Parser<DataType> choiceTypeParser = Parsers.between(STRING("{"), choiceTypeListParser, STRING("}")).followedBy(OPT_WHITESPACE.followedBy(STRING("choice")));

    private static Parser<DataType> typeVarParser = STRING("'").next(Scanners.IDENTIFIER).map(new Map<String, DataType>() {
        public DataType map(String s) {
        	TypeVariable type = DatatypeFactory.eINSTANCE.createTypeVariable();
        	type.setName(s);
            return type;
        }
    });

    private static Parser<DataType> signatureReferenceParser = OPT_WHITESPACE.next(Scanners.IDENTIFIER).map(new Map<String, DataType>() {
        public DataType map(String s) {
        	SignatureReference type = DatatypeFactory.eINSTANCE.createSignatureReference();
        	type.setSignatureName(s);
        	//TODO: Create reference for existing signature
            return type;
        }
    });

    private static Parser<DataType> prefixParser = OPT_WHITESPACE.next(Parsers.or(
            parenthesisParser,
            baseTypeParser,
            choiceTypeParser,
            typeVarParser,
            signatureReferenceParser,
            Parsers.EOF.map(new Map<Object, DataType>() {
                public DataType map(Object o) {
                    return DatatypeFactory.eINSTANCE.createNoType();
                }
            })));

    // Postfix parsers

    /**
     * Names of valid data structures
     */
    private static List<String> singleValueDataStructureTerminals =
            Arrays.asList("set", "opt", "list");

    private static Parser<String> singleValueDataStructureParser = Scanners.WHITESPACES.next(
            Terminals.operators(singleValueDataStructureTerminals).tokenizer().map(new Map<Object, String>() {
                public String map(Object o) {
                    return o.toString();
                }
            })
    );

    private static Parser<String> arrayParser = Scanners.WHITESPACES.next(
            STRING("array").next(OPT_WHITESPACE.next(STRING("[").next(NAIVE_UNTIL("]"))))).map(new Map<String, String>() {
        public String map(String s) {
            return "array " + s;
        }
    });

    private static Parser<List<Integer>> arrayDimensionParser = Parsers.between(OPT_WHITESPACE, Scanners.INTEGER.map(new Map<String, Integer>() {
        public Integer map(String s) {
            return Integer.valueOf(s);
        }
    }), OPT_WHITESPACE).sepBy(STRING(","));

    private static Parser<DataType> singleDataTypeParser =
            Parsers.sequence(prefixParser,
                    Parsers.or(
                            singleValueDataStructureParser,
                            arrayParser,
                            Parsers.EOF.<DataType>cast()).many(),
                    new Map2<DataType, List<Object>, DataType>() {
                        public DataType map(DataType dataType, List<Object> objects) {
                            DataType resultDataType = dataType;

                            for (Object o: objects) {
                                if (o != null) {
                                    String str = (String) o;

                                    if (singleValueDataStructureTerminals.contains(str)) {
                                    	DataStructureType type = DatatypeFactory.eINSTANCE.createDataStructureType();
                                    	type.setElementType(resultDataType);
                                    	type.setStructureName(str);
                                        resultDataType = type;
                                    }
                                    else if (str.contains("array")) {
                                        List<Integer> dimensions = arrayDimensionParser.parse(str.replace("array", ""));
                                        
                                        ArrayType type = DatatypeFactory.eINSTANCE.createArrayType();
                                        type.setElementType(resultDataType);
                                        type.getDimensions().addAll(dimensions);
                                        resultDataType = type;
                                    }
                                }
                            }

                            return resultDataType;
                        }
                    }).followedBy(OPT_WHITESPACE);

    // Multi-component parsers

    private static Parser<DataType> tupleParser = singleDataTypeParser.sepBy(STRING("*")).map(new Map<List<DataType>, DataType>() {
        public DataType map(List<DataType> dataTypes) {
            if (dataTypes.size() == 1) {
                return dataTypes.get(0);
            }
            else {
            	TupleType type = DatatypeFactory.eINSTANCE.createTupleType();
            	type.getElementTypes().addAll(dataTypes);
                return type;
            }
        }
    });

    private static Parser<DataType> functionParser = tupleParser.sepBy(STRING("->")).map(new Map<List<DataType>, DataType>() {
        public DataType map(List<DataType> dataTypes) {
            DataType rightHandType = dataTypes.get(dataTypes.size() - 1);

            for (int i = dataTypes.size() - 2; 0 <= i; i--) {
            	FunctionType type = DatatypeFactory.eINSTANCE.createFunctionType();
            	type.setInput(dataTypes.get(i));
            	type.setOutput(rightHandType);
                rightHandType = type;
            }

            return rightHandType;
        }
    });

    static {
        // Set the reference to the highest-level grammar rule
        dataTypeParserRef.set(functionParser);
    }
	
    public static DataType parse(String input) {
    	try {
        	return dataTypeParserRef.lazy().parse(input);
    	} catch (ParserException ex) {
    		UnparsedType type = DatatypeFactory.eINSTANCE.createUnparsedType();
    		type.setDefinition(input);
    		return type;
    	}
    }
}
