/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MultiplicityFactoryImpl extends EFactoryImpl implements MultiplicityFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MultiplicityFactory init() {
		try {
			MultiplicityFactory theMultiplicityFactory = (MultiplicityFactory)EPackage.Registry.INSTANCE.getEFactory(MultiplicityPackage.eNS_URI);
			if (theMultiplicityFactory != null) {
				return theMultiplicityFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MultiplicityFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MultiplicityPackage.SINGLE_VALUE_MULTIPLICITY: return createSingleValueMultiplicity();
			case MultiplicityPackage.RANGED_MULTIPLICITY: return createRangedMultiplicity();
			case MultiplicityPackage.COMPOUND_MULTIPLICITY: return createCompoundMultiplicity();
			case MultiplicityPackage.UNPARSED_MULTIPLICITY: return createUnparsedMultiplicity();
			case MultiplicityPackage.NO_MULTIPLICITY: return createNoMultiplicity();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleValueMultiplicity createSingleValueMultiplicity() {
		SingleValueMultiplicityImpl singleValueMultiplicity = new SingleValueMultiplicityImpl();
		return singleValueMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangedMultiplicity createRangedMultiplicity() {
		RangedMultiplicityImpl rangedMultiplicity = new RangedMultiplicityImpl();
		return rangedMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompoundMultiplicity createCompoundMultiplicity() {
		CompoundMultiplicityImpl compoundMultiplicity = new CompoundMultiplicityImpl();
		return compoundMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnparsedMultiplicity createUnparsedMultiplicity() {
		UnparsedMultiplicityImpl unparsedMultiplicity = new UnparsedMultiplicityImpl();
		return unparsedMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoMultiplicity createNoMultiplicity() {
		NoMultiplicityImpl noMultiplicity = new NoMultiplicityImpl();
		return noMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityPackage getMultiplicityPackage() {
		return (MultiplicityPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MultiplicityPackage getPackage() {
		return MultiplicityPackage.eINSTANCE;
	}

} //MultiplicityFactoryImpl
