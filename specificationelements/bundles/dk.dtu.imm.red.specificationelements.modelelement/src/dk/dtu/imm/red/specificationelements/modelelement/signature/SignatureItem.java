/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.signature;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getAccessMode <em>Access Mode</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getMultiplicity <em>Multiplicity</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage#getSignatureItem()
 * @model
 * @generated
 */
public interface SignatureItem extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage#getSignatureItem_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(DataType)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage#getSignatureItem_Type()
	 * @model containment="true"
	 * @generated
	 */
	DataType getType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(DataType value);

	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility
	 * @see #setVisibility(Visibility)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage#getSignatureItem_Visibility()
	 * @model
	 * @generated
	 */
	Visibility getVisibility();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(Visibility value);

	/**
	 * Returns the value of the '<em><b>Access Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.modelelement.signature.AccessMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Mode</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.AccessMode
	 * @see #setAccessMode(AccessMode)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage#getSignatureItem_AccessMode()
	 * @model
	 * @generated
	 */
	AccessMode getAccessMode();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getAccessMode <em>Access Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Mode</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.AccessMode
	 * @see #getAccessMode()
	 * @generated
	 */
	void setAccessMode(AccessMode value);

	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage#getSignatureItem_Multiplicity()
	 * @model containment="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

} // SignatureItem
