/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.NoMultiplicity;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>No Multiplicity</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NoMultiplicityImpl extends MultiplicityImpl implements NoMultiplicity {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NoMultiplicityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MultiplicityPackage.Literals.NO_MULTIPLICITY;
	}
	
	/**
	 * @generated NOT
	 */
	public String toString() {
		return "";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean inRange(int value) {
		return value == 1;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean inRangeUnbounded(int minimum) {
		return false;
	}

} //NoMultiplicityImpl
