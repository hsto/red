/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tuple Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.TupleType#getElementTypes <em>Element Types</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getTupleType()
 * @model
 * @generated
 */
public interface TupleType extends DataType {
	/**
	 * Returns the value of the '<em><b>Element Types</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Types</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getTupleType_ElementTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataType> getElementTypes();

} // TupleType
