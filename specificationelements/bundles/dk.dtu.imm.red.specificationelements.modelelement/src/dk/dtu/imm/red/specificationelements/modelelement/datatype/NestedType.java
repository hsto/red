/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nested Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.NestedType#getContainedType <em>Contained Type</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getNestedType()
 * @model
 * @generated
 */
public interface NestedType extends DataType {
	/**
	 * Returns the value of the '<em><b>Contained Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Type</em>' containment reference.
	 * @see #setContainedType(DataType)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getNestedType_ContainedType()
	 * @model containment="true"
	 * @generated
	 */
	DataType getContainedType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.NestedType#getContainedType <em>Contained Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contained Type</em>' containment reference.
	 * @see #getContainedType()
	 * @generated
	 */
	void setContainedType(DataType value);

} // NestedType
