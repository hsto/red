/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiplicity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage#getMultiplicity()
 * @model abstract="true"
 * @generated
 */
public interface Multiplicity extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean inRange(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean inRangeUnbounded(int minimum);
} // Multiplicity
