/**
 */
package dk.dtu.imm.red.specificationelements.modelelement;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage#getModelElement()
 * @model abstract="true"
 * @generated
 */
public interface ModelElement extends SpecificationElement {
} // ModelElement
