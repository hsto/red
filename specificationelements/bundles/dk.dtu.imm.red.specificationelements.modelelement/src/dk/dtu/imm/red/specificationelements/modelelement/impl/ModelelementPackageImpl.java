/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.impl;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;
import dk.dtu.imm.red.specificationelements.modelelement.ModelelementFactory;
import dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage;
import dk.dtu.imm.red.specificationelements.modelelement.signature.impl.SignaturePackageImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelelementPackageImpl extends EPackageImpl implements ModelelementPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ModelelementPackageImpl() {
		super(eNS_URI, ModelelementFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ModelelementPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ModelelementPackage init() {
		if (isInited) return (ModelelementPackage)EPackage.Registry.INSTANCE.getEPackage(ModelelementPackage.eNS_URI);

		// Obtain or create and register package
		ModelelementPackageImpl theModelelementPackage = (ModelelementPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ModelelementPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ModelelementPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SignaturePackageImpl theSignaturePackage = (SignaturePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SignaturePackage.eNS_URI) instanceof SignaturePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SignaturePackage.eNS_URI) : SignaturePackage.eINSTANCE);
		DatatypePackageImpl theDatatypePackage = (DatatypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) instanceof DatatypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) : DatatypePackage.eINSTANCE);
		MultiplicityPackageImpl theMultiplicityPackage = (MultiplicityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MultiplicityPackage.eNS_URI) instanceof MultiplicityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MultiplicityPackage.eNS_URI) : MultiplicityPackage.eINSTANCE);

		// Create package meta-data objects
		theModelelementPackage.createPackageContents();
		theSignaturePackage.createPackageContents();
		theDatatypePackage.createPackageContents();
		theMultiplicityPackage.createPackageContents();

		// Initialize created meta-data
		theModelelementPackage.initializePackageContents();
		theSignaturePackage.initializePackageContents();
		theDatatypePackage.initializePackageContents();
		theMultiplicityPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theModelelementPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ModelelementPackage.eNS_URI, theModelelementPackage);
		return theModelelementPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelElement() {
		return modelElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelelementFactory getModelelementFactory() {
		return (ModelelementFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		modelElementEClass = createEClass(MODEL_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SignaturePackage theSignaturePackage = (SignaturePackage)EPackage.Registry.INSTANCE.getEPackage(SignaturePackage.eNS_URI);
		DatatypePackage theDatatypePackage = (DatatypePackage)EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI);
		MultiplicityPackage theMultiplicityPackage = (MultiplicityPackage)EPackage.Registry.INSTANCE.getEPackage(MultiplicityPackage.eNS_URI);
		SpecificationelementsPackage theSpecificationelementsPackage = (SpecificationelementsPackage)EPackage.Registry.INSTANCE.getEPackage(SpecificationelementsPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theSignaturePackage);
		getESubpackages().add(theDatatypePackage);
		getESubpackages().add(theMultiplicityPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		modelElementEClass.getESuperTypes().add(theSpecificationelementsPackage.getSpecificationElement());

		// Initialize classes and features; add operations and parameters
		initEClass(modelElementEClass, ModelElement.class, "ModelElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //ModelelementPackageImpl
