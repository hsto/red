/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Typed Data Structure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType#getStructureName <em>Structure Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType#getElementType <em>Element Type</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getDataStructureType()
 * @model
 * @generated
 */
public interface DataStructureType extends DataType {
	/**
	 * Returns the value of the '<em><b>Structure Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Structure Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Structure Name</em>' attribute.
	 * @see #setStructureName(String)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getDataStructureType_StructureName()
	 * @model
	 * @generated
	 */
	String getStructureName();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType#getStructureName <em>Structure Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Structure Name</em>' attribute.
	 * @see #getStructureName()
	 * @generated
	 */
	void setStructureName(String value);

	/**
	 * Returns the value of the '<em><b>Element Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Type</em>' containment reference.
	 * @see #setElementType(DataType)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getDataStructureType_ElementType()
	 * @model containment="true"
	 * @generated
	 */
	DataType getElementType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType#getElementType <em>Element Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Type</em>' containment reference.
	 * @see #getElementType()
	 * @generated
	 */
	void setElementType(DataType value);

} // SingleTypedDataStructure
