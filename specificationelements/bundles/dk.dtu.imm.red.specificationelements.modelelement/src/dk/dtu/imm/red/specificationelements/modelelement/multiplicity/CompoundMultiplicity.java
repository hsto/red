/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compound Multiplicity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity#getParts <em>Parts</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage#getCompoundMultiplicity()
 * @model
 * @generated
 */
public interface CompoundMultiplicity extends Multiplicity {
	/**
	 * Returns the value of the '<em><b>Parts</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parts</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage#getCompoundMultiplicity_Parts()
	 * @model containment="true"
	 * @generated
	 */
	EList<Multiplicity> getParts();

} // CompoundMultiplicity
