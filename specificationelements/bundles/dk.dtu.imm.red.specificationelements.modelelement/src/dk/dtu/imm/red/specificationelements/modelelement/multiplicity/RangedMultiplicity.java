/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ranged Multiplicity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity#getMinimum <em>Minimum</em>}</li>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity#getMaximum <em>Maximum</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage#getRangedMultiplicity()
 * @model
 * @generated
 */
public interface RangedMultiplicity extends Multiplicity {
	/**
	 * Returns the value of the '<em><b>Minimum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minimum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minimum</em>' attribute.
	 * @see #setMinimum(int)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage#getRangedMultiplicity_Minimum()
	 * @model
	 * @generated
	 */
	int getMinimum();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity#getMinimum <em>Minimum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minimum</em>' attribute.
	 * @see #getMinimum()
	 * @generated
	 */
	void setMinimum(int value);

	/**
	 * Returns the value of the '<em><b>Maximum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maximum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maximum</em>' attribute.
	 * @see #setMaximum(int)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage#getRangedMultiplicity_Maximum()
	 * @model
	 * @generated
	 */
	int getMaximum();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity#getMaximum <em>Maximum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum</em>' attribute.
	 * @see #getMaximum()
	 * @generated
	 */
	void setMaximum(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isUnbounded();

} // RangedMultiplicity
