/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage
 * @generated
 */
public interface MultiplicityFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MultiplicityFactory eINSTANCE = dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Single Value Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Single Value Multiplicity</em>'.
	 * @generated
	 */
	SingleValueMultiplicity createSingleValueMultiplicity();

	/**
	 * Returns a new object of class '<em>Ranged Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ranged Multiplicity</em>'.
	 * @generated
	 */
	RangedMultiplicity createRangedMultiplicity();

	/**
	 * Returns a new object of class '<em>Compound Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Compound Multiplicity</em>'.
	 * @generated
	 */
	CompoundMultiplicity createCompoundMultiplicity();

	/**
	 * Returns a new object of class '<em>Unparsed Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unparsed Multiplicity</em>'.
	 * @generated
	 */
	UnparsedMultiplicity createUnparsedMultiplicity();

	/**
	 * Returns a new object of class '<em>No Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>No Multiplicity</em>'.
	 * @generated
	 */
	NoMultiplicity createNoMultiplicity();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MultiplicityPackage getMultiplicityPackage();

} //MultiplicityFactory
