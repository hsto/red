/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unparsed Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType#getDefinition <em>Definition</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getUnparsedType()
 * @model
 * @generated
 */
public interface UnparsedType extends DataType {
	/**
	 * Returns the value of the '<em><b>Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' attribute.
	 * @see #setDefinition(String)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getUnparsedType_Definition()
	 * @model
	 * @generated
	 */
	String getDefinition();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType#getDefinition <em>Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' attribute.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(String value);

} // UnparsedType
