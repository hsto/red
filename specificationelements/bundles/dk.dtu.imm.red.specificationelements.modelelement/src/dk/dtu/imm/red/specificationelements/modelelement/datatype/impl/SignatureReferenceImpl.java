/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype.impl;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.SignatureReference;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signature Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.SignatureReferenceImpl#getSignatureName <em>Signature Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignatureReferenceImpl extends DataTypeImpl implements SignatureReference {
	/**
	 * The default value of the '{@link #getSignatureName() <em>Signature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignatureName()
	 * @generated
	 * @ordered
	 */
	protected static final String SIGNATURE_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getSignatureName() <em>Signature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignatureName()
	 * @generated
	 * @ordered
	 */
	protected String signatureName = SIGNATURE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignatureReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatatypePackage.Literals.SIGNATURE_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSignatureName() {
		return signatureName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignatureName(String newSignatureName) {
		String oldSignatureName = signatureName;
		signatureName = newSignatureName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatatypePackage.SIGNATURE_REFERENCE__SIGNATURE_NAME, oldSignatureName, signatureName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatatypePackage.SIGNATURE_REFERENCE__SIGNATURE_NAME:
				return getSignatureName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatatypePackage.SIGNATURE_REFERENCE__SIGNATURE_NAME:
				setSignatureName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatatypePackage.SIGNATURE_REFERENCE__SIGNATURE_NAME:
				setSignatureName(SIGNATURE_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatatypePackage.SIGNATURE_REFERENCE__SIGNATURE_NAME:
				return SIGNATURE_NAME_EDEFAULT == null ? signatureName != null : !SIGNATURE_NAME_EDEFAULT.equals(signatureName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		return getSignatureName();
	}

} //SignatureReferenceImpl
