package dk.dtu.imm.red.specificationelements.modelelement.multiplicity.util;

import java.util.List;

import org.codehaus.jparsec.Parser;
import org.codehaus.jparsec.Parsers;
import org.codehaus.jparsec.Scanners;
import org.codehaus.jparsec.error.ParserException;
import org.codehaus.jparsec.functors.Map;
import org.codehaus.jparsec.functors.Map3;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityFactory;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.SingleValueMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.UnparsedMultiplicity;

public class MultiplicityParser {

    private static Parser<Integer> integerParser(boolean allowUnbounded) {
    	Parser<Integer> unsignedIntParser = Scanners.INTEGER.map(new Map<String, Integer>() {
            public Integer map(String s) {
                return Math.abs(Integer.parseInt(s));
            }});
    	
    	Parser<Integer> unboundedSymbolParser = 
    			(allowUnbounded) ? 
    					Scanners.string("*").map(new Map<Void, Integer>() {
    						public Integer map(Void arg0) {
    							return -1;
    							}
    						})
    					: Parsers.never().retn(-1); 
    	
    	return Parsers.between(
            Scanners.WHITESPACES.skipMany(),
            Parsers.or(unsignedIntParser, unboundedSymbolParser),
            Scanners.WHITESPACES.skipMany());
    }

    private static Parser<Multiplicity> singleValueMultiplicityParser = integerParser(true).map(new Map<Integer, Multiplicity>() {
    			public Multiplicity map(Integer value) {
    				SingleValueMultiplicity multiplicity = MultiplicityFactory.eINSTANCE.createSingleValueMultiplicity();
    				multiplicity.setValue(value);
    				return multiplicity;
    				}
    			});

    private static Parser<Multiplicity> rangedMultiplicityParser =
            Parsers.sequence(integerParser(false), Scanners.string(".."), integerParser(true), new Map3<Integer, Void, Integer, Multiplicity>() {
                public Multiplicity map(Integer leftHandValue, Void aVoid, Integer rightHandValue) {
                	RangedMultiplicity multiplicity = MultiplicityFactory.eINSTANCE.createRangedMultiplicity();
                	multiplicity.setMinimum(leftHandValue);
                	multiplicity.setMaximum(rightHandValue);
                    return multiplicity;
                }
            });

    private static Parser<Multiplicity> simpleMultiplicityParser = rangedMultiplicityParser.or(singleValueMultiplicityParser);

    private static Parser<Multiplicity> multiplicityParser = Parsers.or(simpleMultiplicityParser.sepBy(Scanners.isChar(','))
    		.followedBy(Scanners.WHITESPACES.optional())
    		.map(new Map<List<Multiplicity>, Multiplicity>() {
        public Multiplicity map(List<Multiplicity> multiplicities) {
        	if (multiplicities.isEmpty()) {
        		return MultiplicityFactory.eINSTANCE.createNoMultiplicity();
        	}
        	else if (multiplicities.size() == 1) {
                return multiplicities.get(0);
            }
            else {
            	CompoundMultiplicity multiplicity = MultiplicityFactory.eINSTANCE.createCompoundMultiplicity();
            	multiplicity.getParts().addAll(multiplicities);
                return multiplicity;
            }
        }
    }), Scanners.WHITESPACES.optional().followedBy(Parsers.EOF).map(new Map<Void, Multiplicity>() {
        public Multiplicity map(Void aVoid) {
            return MultiplicityFactory.eINSTANCE.createNoMultiplicity();
        }
    }));
	
    public static Multiplicity parse(String input) {
    	try {
    		return multiplicityParser.parse(input);
    	} catch (ParserException ex) {
    		UnparsedMultiplicity multiplicity = MultiplicityFactory.eINSTANCE.createUnparsedMultiplicity();
    		multiplicity.setDefinition(input);
    		return multiplicity;
    	}
    	
    }
}
