/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>No Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getNoType()
 * @model
 * @generated
 */
public interface NoType extends DataType {
} // NoType
