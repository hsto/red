/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.impl;

import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;

import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;
import dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ModelElementImpl extends SpecificationElementImpl implements ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelelementPackage.Literals.MODEL_ELEMENT;
	}

} //ModelElementImpl
