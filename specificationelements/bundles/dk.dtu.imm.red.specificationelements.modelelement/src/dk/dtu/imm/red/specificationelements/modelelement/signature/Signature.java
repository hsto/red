/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.signature;

import dk.dtu.imm.red.specificationelements.modelelement.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.signature.Signature#getItems <em>Items</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage#getSignature()
 * @model
 * @generated
 */
public interface Signature extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Items</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Items</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Items</em>' containment reference list.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage#getSignature_Items()
	 * @model containment="true"
	 * @generated
	 */
	EList<SignatureItem> getItems();

} // Signature
