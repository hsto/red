/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Compound Multiplicity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.CompoundMultiplicityImpl#getParts <em>Parts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompoundMultiplicityImpl extends MultiplicityImpl implements CompoundMultiplicity {
	/**
	 * The cached value of the '{@link #getParts() <em>Parts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParts()
	 * @generated
	 * @ordered
	 */
	protected EList<Multiplicity> parts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompoundMultiplicityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MultiplicityPackage.Literals.COMPOUND_MULTIPLICITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Multiplicity> getParts() {
		if (parts == null) {
			parts = new EObjectContainmentEList<Multiplicity>(Multiplicity.class, this, MultiplicityPackage.COMPOUND_MULTIPLICITY__PARTS);
		}
		return parts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MultiplicityPackage.COMPOUND_MULTIPLICITY__PARTS:
				return ((InternalEList<?>)getParts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MultiplicityPackage.COMPOUND_MULTIPLICITY__PARTS:
				return getParts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MultiplicityPackage.COMPOUND_MULTIPLICITY__PARTS:
				getParts().clear();
				getParts().addAll((Collection<? extends Multiplicity>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MultiplicityPackage.COMPOUND_MULTIPLICITY__PARTS:
				getParts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MultiplicityPackage.COMPOUND_MULTIPLICITY__PARTS:
				return parts != null && !parts.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		
		Iterator<Multiplicity> iterator = getParts().iterator();
		if (iterator.hasNext()) {
			result.append(iterator.next().toString());
		}
		
		while (iterator.hasNext()) {
			result.append(", " + iterator.next().toString());
		}
		
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean inRange(int value) {
		for (Multiplicity part : getParts()) {
			if (part.inRange(value)) return true;
		}
		
		return false;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean inRangeUnbounded(int minimum) {
		for (Multiplicity part : getParts()) {
			if (part.inRangeUnbounded(minimum)) return true;
		}
		
		return false;
	}

} //CompoundMultiplicityImpl
