/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.signature.impl;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl;

import dk.dtu.imm.red.specificationelements.modelelement.impl.ModelelementPackageImpl;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl;

import dk.dtu.imm.red.specificationelements.modelelement.signature.AccessMode;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureFactory;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureKind;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SignaturePackageImpl extends EPackageImpl implements SignaturePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signatureItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum signatureKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum visibilityEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum accessModeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SignaturePackageImpl() {
		super(eNS_URI, SignatureFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SignaturePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SignaturePackage init() {
		if (isInited) return (SignaturePackage)EPackage.Registry.INSTANCE.getEPackage(SignaturePackage.eNS_URI);

		// Obtain or create and register package
		SignaturePackageImpl theSignaturePackage = (SignaturePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SignaturePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SignaturePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ModelelementPackageImpl theModelelementPackage = (ModelelementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ModelelementPackage.eNS_URI) instanceof ModelelementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ModelelementPackage.eNS_URI) : ModelelementPackage.eINSTANCE);
		DatatypePackageImpl theDatatypePackage = (DatatypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) instanceof DatatypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) : DatatypePackage.eINSTANCE);
		MultiplicityPackageImpl theMultiplicityPackage = (MultiplicityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MultiplicityPackage.eNS_URI) instanceof MultiplicityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MultiplicityPackage.eNS_URI) : MultiplicityPackage.eINSTANCE);

		// Create package meta-data objects
		theSignaturePackage.createPackageContents();
		theModelelementPackage.createPackageContents();
		theDatatypePackage.createPackageContents();
		theMultiplicityPackage.createPackageContents();

		// Initialize created meta-data
		theSignaturePackage.initializePackageContents();
		theModelelementPackage.initializePackageContents();
		theDatatypePackage.initializePackageContents();
		theMultiplicityPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSignaturePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SignaturePackage.eNS_URI, theSignaturePackage);
		return theSignaturePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignature() {
		return signatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignature_Items() {
		return (EReference)signatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignatureItem() {
		return signatureItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignatureItem_Name() {
		return (EAttribute)signatureItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignatureItem_Type() {
		return (EReference)signatureItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignatureItem_Visibility() {
		return (EAttribute)signatureItemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignatureItem_AccessMode() {
		return (EAttribute)signatureItemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignatureItem_Multiplicity() {
		return (EReference)signatureItemEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSignatureKind() {
		return signatureKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getVisibility() {
		return visibilityEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAccessMode() {
		return accessModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignatureFactory getSignatureFactory() {
		return (SignatureFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		signatureEClass = createEClass(SIGNATURE);
		createEReference(signatureEClass, SIGNATURE__ITEMS);

		signatureItemEClass = createEClass(SIGNATURE_ITEM);
		createEAttribute(signatureItemEClass, SIGNATURE_ITEM__NAME);
		createEReference(signatureItemEClass, SIGNATURE_ITEM__TYPE);
		createEAttribute(signatureItemEClass, SIGNATURE_ITEM__VISIBILITY);
		createEAttribute(signatureItemEClass, SIGNATURE_ITEM__ACCESS_MODE);
		createEReference(signatureItemEClass, SIGNATURE_ITEM__MULTIPLICITY);

		// Create enums
		signatureKindEEnum = createEEnum(SIGNATURE_KIND);
		visibilityEEnum = createEEnum(VISIBILITY);
		accessModeEEnum = createEEnum(ACCESS_MODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ModelelementPackage theModelelementPackage = (ModelelementPackage)EPackage.Registry.INSTANCE.getEPackage(ModelelementPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		DatatypePackage theDatatypePackage = (DatatypePackage)EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI);
		MultiplicityPackage theMultiplicityPackage = (MultiplicityPackage)EPackage.Registry.INSTANCE.getEPackage(MultiplicityPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		signatureEClass.getESuperTypes().add(theModelelementPackage.getModelElement());

		// Initialize classes and features; add operations and parameters
		initEClass(signatureEClass, Signature.class, "Signature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSignature_Items(), this.getSignatureItem(), null, "items", null, 0, -1, Signature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(signatureItemEClass, SignatureItem.class, "SignatureItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSignatureItem_Name(), theEcorePackage.getEString(), "name", null, 0, 1, SignatureItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSignatureItem_Type(), theDatatypePackage.getDataType(), null, "type", null, 0, 1, SignatureItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSignatureItem_Visibility(), this.getVisibility(), "visibility", null, 0, 1, SignatureItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSignatureItem_AccessMode(), this.getAccessMode(), "accessMode", null, 0, 1, SignatureItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSignatureItem_Multiplicity(), theMultiplicityPackage.getMultiplicity(), null, "multiplicity", null, 0, 1, SignatureItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(signatureKindEEnum, SignatureKind.class, "SignatureKind");
		addEEnumLiteral(signatureKindEEnum, SignatureKind.TYPE);
		addEEnumLiteral(signatureKindEEnum, SignatureKind.CLASS);
		addEEnumLiteral(signatureKindEEnum, SignatureKind.INTERFACE);

		initEEnum(visibilityEEnum, Visibility.class, "Visibility");
		addEEnumLiteral(visibilityEEnum, Visibility.UNSPECIFIED);
		addEEnumLiteral(visibilityEEnum, Visibility.PUBLIC);
		addEEnumLiteral(visibilityEEnum, Visibility.PRIVATE);
		addEEnumLiteral(visibilityEEnum, Visibility.PROTECTED);
		addEEnumLiteral(visibilityEEnum, Visibility.PACKAGE);

		initEEnum(accessModeEEnum, AccessMode.class, "AccessMode");
		addEEnumLiteral(accessModeEEnum, AccessMode.READ_WRITE);
		addEEnumLiteral(accessModeEEnum, AccessMode.READ_ONLY);
	}

} //SignaturePackageImpl
