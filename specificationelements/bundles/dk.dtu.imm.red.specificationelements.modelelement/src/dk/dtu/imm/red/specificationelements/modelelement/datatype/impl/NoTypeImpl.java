/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype.impl;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.NoType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>No Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NoTypeImpl extends DataTypeImpl implements NoType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NoTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatatypePackage.Literals.NO_TYPE;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		return "";
	}
} //NoTypeImpl
