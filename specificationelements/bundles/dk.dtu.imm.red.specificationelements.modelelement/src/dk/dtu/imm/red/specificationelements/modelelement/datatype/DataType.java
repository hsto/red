/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getDataType()
 * @model abstract="true"
 * @generated
 */
public interface DataType extends EObject {
} // DataType
