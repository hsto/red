/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>No Multiplicity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage#getNoMultiplicity()
 * @model
 * @generated
 */
public interface NoMultiplicity extends Multiplicity {
} // NoMultiplicity
