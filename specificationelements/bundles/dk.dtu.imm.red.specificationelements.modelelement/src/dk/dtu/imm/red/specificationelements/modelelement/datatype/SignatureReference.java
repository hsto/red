/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signature Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.SignatureReference#getSignatureName <em>Signature Name</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getSignatureReference()
 * @model
 * @generated
 */
public interface SignatureReference extends DataType {
	/**
	 * Returns the value of the '<em><b>Signature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature Name</em>' attribute.
	 * @see #setSignatureName(String)
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage#getSignatureReference_SignatureName()
	 * @model
	 * @generated
	 */
	String getSignatureName();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.SignatureReference#getSignatureName <em>Signature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature Name</em>' attribute.
	 * @see #getSignatureName()
	 * @generated
	 */
	void setSignatureName(String value);

} // SignatureReference
