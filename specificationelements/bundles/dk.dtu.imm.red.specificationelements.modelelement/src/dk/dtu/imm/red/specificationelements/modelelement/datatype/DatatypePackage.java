/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.datatype;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypeFactory
 * @model kind="package"
 * @generated
 */
public interface DatatypePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "datatype";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.specificationelements.modelelement.datatype";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "datatype";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DatatypePackage eINSTANCE = dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DataTypeImpl <em>Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DataTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getDataType()
	 * @generated
	 */
	int DATA_TYPE = 0;

	/**
	 * The number of structural features of the '<em>Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.BaseTypeImpl <em>Base Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.BaseTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getBaseType()
	 * @generated
	 */
	int BASE_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TYPE__NAME = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Base Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.FunctionTypeImpl <em>Function Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.FunctionTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getFunctionType()
	 * @generated
	 */
	int FUNCTION_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Input</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__INPUT = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__OUTPUT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Function Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DataStructureTypeImpl <em>Data Structure Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DataStructureTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getDataStructureType()
	 * @generated
	 */
	int DATA_STRUCTURE_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Structure Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCTURE_TYPE__STRUCTURE_NAME = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Element Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCTURE_TYPE__ELEMENT_TYPE = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Structure Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCTURE_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.TupleTypeImpl <em>Tuple Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.TupleTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getTupleType()
	 * @generated
	 */
	int TUPLE_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Element Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE_TYPE__ELEMENT_TYPES = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Tuple Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.NestedTypeImpl <em>Nested Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.NestedTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getNestedType()
	 * @generated
	 */
	int NESTED_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Contained Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TYPE__CONTAINED_TYPE = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Nested Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.ArrayTypeImpl <em>Array Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.ArrayTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getArrayType()
	 * @generated
	 */
	int ARRAY_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Element Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__ELEMENT_TYPE = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dimensions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__DIMENSIONS = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.ChoiceTypeImpl <em>Choice Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.ChoiceTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getChoiceType()
	 * @generated
	 */
	int CHOICE_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Options</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_TYPE__OPTIONS = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Choice Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.SignatureReferenceImpl <em>Signature Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.SignatureReferenceImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getSignatureReference()
	 * @generated
	 */
	int SIGNATURE_REFERENCE = 8;

	/**
	 * The feature id for the '<em><b>Signature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_REFERENCE__SIGNATURE_NAME = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Signature Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_REFERENCE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.NoTypeImpl <em>No Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.NoTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getNoType()
	 * @generated
	 */
	int NO_TYPE = 9;

	/**
	 * The number of structural features of the '<em>No Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NO_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.TypeVariableImpl <em>Type Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.TypeVariableImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getTypeVariable()
	 * @generated
	 */
	int TYPE_VARIABLE = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_VARIABLE__NAME = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_VARIABLE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.UnparsedTypeImpl <em>Unparsed Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.UnparsedTypeImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getUnparsedType()
	 * @generated
	 */
	int UNPARSED_TYPE = 11;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPARSED_TYPE__DEFINITION = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unparsed Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPARSED_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType
	 * @generated
	 */
	EClass getDataType();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.BaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.BaseType
	 * @generated
	 */
	EClass getBaseType();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.BaseType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.BaseType#getName()
	 * @see #getBaseType()
	 * @generated
	 */
	EAttribute getBaseType_Name();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.FunctionType <em>Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.FunctionType
	 * @generated
	 */
	EClass getFunctionType();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.FunctionType#getInput <em>Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Input</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.FunctionType#getInput()
	 * @see #getFunctionType()
	 * @generated
	 */
	EReference getFunctionType_Input();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.FunctionType#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.FunctionType#getOutput()
	 * @see #getFunctionType()
	 * @generated
	 */
	EReference getFunctionType_Output();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType <em>Data Structure Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Structure Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType
	 * @generated
	 */
	EClass getDataStructureType();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType#getStructureName <em>Structure Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Structure Name</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType#getStructureName()
	 * @see #getDataStructureType()
	 * @generated
	 */
	EAttribute getDataStructureType_StructureName();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType#getElementType <em>Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Element Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType#getElementType()
	 * @see #getDataStructureType()
	 * @generated
	 */
	EReference getDataStructureType_ElementType();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.TupleType <em>Tuple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tuple Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.TupleType
	 * @generated
	 */
	EClass getTupleType();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.TupleType#getElementTypes <em>Element Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Element Types</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.TupleType#getElementTypes()
	 * @see #getTupleType()
	 * @generated
	 */
	EReference getTupleType_ElementTypes();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.NestedType <em>Nested Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nested Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.NestedType
	 * @generated
	 */
	EClass getNestedType();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.NestedType#getContainedType <em>Contained Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Contained Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.NestedType#getContainedType()
	 * @see #getNestedType()
	 * @generated
	 */
	EReference getNestedType_ContainedType();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.ArrayType <em>Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.ArrayType
	 * @generated
	 */
	EClass getArrayType();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.ArrayType#getElementType <em>Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Element Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.ArrayType#getElementType()
	 * @see #getArrayType()
	 * @generated
	 */
	EReference getArrayType_ElementType();

	/**
	 * Returns the meta object for the attribute list '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.ArrayType#getDimensions <em>Dimensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Dimensions</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.ArrayType#getDimensions()
	 * @see #getArrayType()
	 * @generated
	 */
	EAttribute getArrayType_Dimensions();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.ChoiceType <em>Choice Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choice Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.ChoiceType
	 * @generated
	 */
	EClass getChoiceType();

	/**
	 * Returns the meta object for the attribute list '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.ChoiceType#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Options</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.ChoiceType#getOptions()
	 * @see #getChoiceType()
	 * @generated
	 */
	EAttribute getChoiceType_Options();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.SignatureReference <em>Signature Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signature Reference</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.SignatureReference
	 * @generated
	 */
	EClass getSignatureReference();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.SignatureReference#getSignatureName <em>Signature Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signature Name</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.SignatureReference#getSignatureName()
	 * @see #getSignatureReference()
	 * @generated
	 */
	EAttribute getSignatureReference_SignatureName();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.NoType <em>No Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>No Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.NoType
	 * @generated
	 */
	EClass getNoType();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.TypeVariable <em>Type Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Variable</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.TypeVariable
	 * @generated
	 */
	EClass getTypeVariable();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.TypeVariable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.TypeVariable#getName()
	 * @see #getTypeVariable()
	 * @generated
	 */
	EAttribute getTypeVariable_Name();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType <em>Unparsed Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unparsed Type</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType
	 * @generated
	 */
	EClass getUnparsedType();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Definition</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType#getDefinition()
	 * @see #getUnparsedType()
	 * @generated
	 */
	EAttribute getUnparsedType_Definition();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DatatypeFactory getDatatypeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DataTypeImpl <em>Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DataTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getDataType()
		 * @generated
		 */
		EClass DATA_TYPE = eINSTANCE.getDataType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.BaseTypeImpl <em>Base Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.BaseTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getBaseType()
		 * @generated
		 */
		EClass BASE_TYPE = eINSTANCE.getBaseType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_TYPE__NAME = eINSTANCE.getBaseType_Name();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.FunctionTypeImpl <em>Function Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.FunctionTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getFunctionType()
		 * @generated
		 */
		EClass FUNCTION_TYPE = eINSTANCE.getFunctionType();

		/**
		 * The meta object literal for the '<em><b>Input</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_TYPE__INPUT = eINSTANCE.getFunctionType_Input();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_TYPE__OUTPUT = eINSTANCE.getFunctionType_Output();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DataStructureTypeImpl <em>Data Structure Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DataStructureTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getDataStructureType()
		 * @generated
		 */
		EClass DATA_STRUCTURE_TYPE = eINSTANCE.getDataStructureType();

		/**
		 * The meta object literal for the '<em><b>Structure Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_STRUCTURE_TYPE__STRUCTURE_NAME = eINSTANCE.getDataStructureType_StructureName();

		/**
		 * The meta object literal for the '<em><b>Element Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_STRUCTURE_TYPE__ELEMENT_TYPE = eINSTANCE.getDataStructureType_ElementType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.TupleTypeImpl <em>Tuple Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.TupleTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getTupleType()
		 * @generated
		 */
		EClass TUPLE_TYPE = eINSTANCE.getTupleType();

		/**
		 * The meta object literal for the '<em><b>Element Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TUPLE_TYPE__ELEMENT_TYPES = eINSTANCE.getTupleType_ElementTypes();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.NestedTypeImpl <em>Nested Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.NestedTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getNestedType()
		 * @generated
		 */
		EClass NESTED_TYPE = eINSTANCE.getNestedType();

		/**
		 * The meta object literal for the '<em><b>Contained Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NESTED_TYPE__CONTAINED_TYPE = eINSTANCE.getNestedType_ContainedType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.ArrayTypeImpl <em>Array Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.ArrayTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getArrayType()
		 * @generated
		 */
		EClass ARRAY_TYPE = eINSTANCE.getArrayType();

		/**
		 * The meta object literal for the '<em><b>Element Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_TYPE__ELEMENT_TYPE = eINSTANCE.getArrayType_ElementType();

		/**
		 * The meta object literal for the '<em><b>Dimensions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARRAY_TYPE__DIMENSIONS = eINSTANCE.getArrayType_Dimensions();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.ChoiceTypeImpl <em>Choice Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.ChoiceTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getChoiceType()
		 * @generated
		 */
		EClass CHOICE_TYPE = eINSTANCE.getChoiceType();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHOICE_TYPE__OPTIONS = eINSTANCE.getChoiceType_Options();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.SignatureReferenceImpl <em>Signature Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.SignatureReferenceImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getSignatureReference()
		 * @generated
		 */
		EClass SIGNATURE_REFERENCE = eINSTANCE.getSignatureReference();

		/**
		 * The meta object literal for the '<em><b>Signature Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNATURE_REFERENCE__SIGNATURE_NAME = eINSTANCE.getSignatureReference_SignatureName();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.NoTypeImpl <em>No Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.NoTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getNoType()
		 * @generated
		 */
		EClass NO_TYPE = eINSTANCE.getNoType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.TypeVariableImpl <em>Type Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.TypeVariableImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getTypeVariable()
		 * @generated
		 */
		EClass TYPE_VARIABLE = eINSTANCE.getTypeVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE_VARIABLE__NAME = eINSTANCE.getTypeVariable_Name();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.UnparsedTypeImpl <em>Unparsed Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.UnparsedTypeImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.datatype.impl.DatatypePackageImpl#getUnparsedType()
		 * @generated
		 */
		EClass UNPARSED_TYPE = eINSTANCE.getUnparsedType();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPARSED_TYPE__DEFINITION = eINSTANCE.getUnparsedType_Definition();

	}

} //DatatypePackage
