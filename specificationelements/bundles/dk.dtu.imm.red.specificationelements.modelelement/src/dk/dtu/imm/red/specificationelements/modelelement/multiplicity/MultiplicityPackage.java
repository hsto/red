/**
 */
package dk.dtu.imm.red.specificationelements.modelelement.multiplicity;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityFactory
 * @model kind="package"
 * @generated
 */
public interface MultiplicityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "multiplicity";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.specificationelements.modelelement.multiplicity";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "multiplicity";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MultiplicityPackage eINSTANCE = dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityImpl <em>Multiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getMultiplicity()
	 * @generated
	 */
	int MULTIPLICITY = 0;

	/**
	 * The number of structural features of the '<em>Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.SingleValueMultiplicityImpl <em>Single Value Multiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.SingleValueMultiplicityImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getSingleValueMultiplicity()
	 * @generated
	 */
	int SINGLE_VALUE_MULTIPLICITY = 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_VALUE_MULTIPLICITY__VALUE = MULTIPLICITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Single Value Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_VALUE_MULTIPLICITY_FEATURE_COUNT = MULTIPLICITY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.RangedMultiplicityImpl <em>Ranged Multiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.RangedMultiplicityImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getRangedMultiplicity()
	 * @generated
	 */
	int RANGED_MULTIPLICITY = 2;

	/**
	 * The feature id for the '<em><b>Minimum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGED_MULTIPLICITY__MINIMUM = MULTIPLICITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Maximum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGED_MULTIPLICITY__MAXIMUM = MULTIPLICITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ranged Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGED_MULTIPLICITY_FEATURE_COUNT = MULTIPLICITY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.CompoundMultiplicityImpl <em>Compound Multiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.CompoundMultiplicityImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getCompoundMultiplicity()
	 * @generated
	 */
	int COMPOUND_MULTIPLICITY = 3;

	/**
	 * The feature id for the '<em><b>Parts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_MULTIPLICITY__PARTS = MULTIPLICITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Compound Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_MULTIPLICITY_FEATURE_COUNT = MULTIPLICITY_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.UnparsedMultiplicityImpl <em>Unparsed Multiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.UnparsedMultiplicityImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getUnparsedMultiplicity()
	 * @generated
	 */
	int UNPARSED_MULTIPLICITY = 4;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPARSED_MULTIPLICITY__DEFINITION = MULTIPLICITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unparsed Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPARSED_MULTIPLICITY_FEATURE_COUNT = MULTIPLICITY_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.NoMultiplicityImpl <em>No Multiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.NoMultiplicityImpl
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getNoMultiplicity()
	 * @generated
	 */
	int NO_MULTIPLICITY = 5;

	/**
	 * The number of structural features of the '<em>No Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NO_MULTIPLICITY_FEATURE_COUNT = MULTIPLICITY_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplicity</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity
	 * @generated
	 */
	EClass getMultiplicity();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.SingleValueMultiplicity <em>Single Value Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Value Multiplicity</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.SingleValueMultiplicity
	 * @generated
	 */
	EClass getSingleValueMultiplicity();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.SingleValueMultiplicity#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.SingleValueMultiplicity#getValue()
	 * @see #getSingleValueMultiplicity()
	 * @generated
	 */
	EAttribute getSingleValueMultiplicity_Value();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity <em>Ranged Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ranged Multiplicity</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity
	 * @generated
	 */
	EClass getRangedMultiplicity();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity#getMinimum <em>Minimum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minimum</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity#getMinimum()
	 * @see #getRangedMultiplicity()
	 * @generated
	 */
	EAttribute getRangedMultiplicity_Minimum();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity#getMaximum <em>Maximum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity#getMaximum()
	 * @see #getRangedMultiplicity()
	 * @generated
	 */
	EAttribute getRangedMultiplicity_Maximum();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity <em>Compound Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compound Multiplicity</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity
	 * @generated
	 */
	EClass getCompoundMultiplicity();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity#getParts <em>Parts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parts</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity#getParts()
	 * @see #getCompoundMultiplicity()
	 * @generated
	 */
	EReference getCompoundMultiplicity_Parts();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.UnparsedMultiplicity <em>Unparsed Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unparsed Multiplicity</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.UnparsedMultiplicity
	 * @generated
	 */
	EClass getUnparsedMultiplicity();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.UnparsedMultiplicity#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Definition</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.UnparsedMultiplicity#getDefinition()
	 * @see #getUnparsedMultiplicity()
	 * @generated
	 */
	EAttribute getUnparsedMultiplicity_Definition();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.NoMultiplicity <em>No Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>No Multiplicity</em>'.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.NoMultiplicity
	 * @generated
	 */
	EClass getNoMultiplicity();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MultiplicityFactory getMultiplicityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityImpl <em>Multiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getMultiplicity()
		 * @generated
		 */
		EClass MULTIPLICITY = eINSTANCE.getMultiplicity();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.SingleValueMultiplicityImpl <em>Single Value Multiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.SingleValueMultiplicityImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getSingleValueMultiplicity()
		 * @generated
		 */
		EClass SINGLE_VALUE_MULTIPLICITY = eINSTANCE.getSingleValueMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SINGLE_VALUE_MULTIPLICITY__VALUE = eINSTANCE.getSingleValueMultiplicity_Value();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.RangedMultiplicityImpl <em>Ranged Multiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.RangedMultiplicityImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getRangedMultiplicity()
		 * @generated
		 */
		EClass RANGED_MULTIPLICITY = eINSTANCE.getRangedMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Minimum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANGED_MULTIPLICITY__MINIMUM = eINSTANCE.getRangedMultiplicity_Minimum();

		/**
		 * The meta object literal for the '<em><b>Maximum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANGED_MULTIPLICITY__MAXIMUM = eINSTANCE.getRangedMultiplicity_Maximum();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.CompoundMultiplicityImpl <em>Compound Multiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.CompoundMultiplicityImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getCompoundMultiplicity()
		 * @generated
		 */
		EClass COMPOUND_MULTIPLICITY = eINSTANCE.getCompoundMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Parts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOUND_MULTIPLICITY__PARTS = eINSTANCE.getCompoundMultiplicity_Parts();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.UnparsedMultiplicityImpl <em>Unparsed Multiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.UnparsedMultiplicityImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getUnparsedMultiplicity()
		 * @generated
		 */
		EClass UNPARSED_MULTIPLICITY = eINSTANCE.getUnparsedMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPARSED_MULTIPLICITY__DEFINITION = eINSTANCE.getUnparsedMultiplicity_Definition();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.NoMultiplicityImpl <em>No Multiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.NoMultiplicityImpl
		 * @see dk.dtu.imm.red.specificationelements.modelelement.multiplicity.impl.MultiplicityPackageImpl#getNoMultiplicity()
		 * @generated
		 */
		EClass NO_MULTIPLICITY = eINSTANCE.getNoMultiplicity();

	}

} //MultiplicityPackage
