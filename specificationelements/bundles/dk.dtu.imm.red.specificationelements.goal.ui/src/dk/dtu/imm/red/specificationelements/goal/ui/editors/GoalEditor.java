package dk.dtu.imm.red.specificationelements.goal.ui.editors;

import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface GoalEditor extends IEditorPart, IBaseEditor {

	public static final String ID = "dk.dtu.imm.red.specificationelements.goal.goaleditor";

}
