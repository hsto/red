package dk.dtu.imm.red.specificationelements.goal.ui.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.GoalFactory;
import dk.dtu.imm.red.specificationelements.goal.ui.extensions.GoalExtension;

public class CreateGoalOperation extends AbstractOperation{

	protected String label;
	protected String name;
	protected String description;
	protected Group parent;
//	protected String path;
	protected Goal goal;
	
	protected GoalExtension extension;

	public CreateGoalOperation(String label, String name, String description, Group parent, String path) {

		super("Create Goal");

		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
//		this.path = path;

		extension = new GoalExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		goal = GoalFactory.eINSTANCE.createGoal();
		goal.setCreator(PreferenceUtil.getUserPreference_User());

		goal.setName(name);
		goal.setLabel(label);
		goal.setDescription(description);

		if (parent != null) {
			goal.setParent(parent);
		}

		goal.save();		
		extension.openElement(goal);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if (parent != null) {
			parent.getContents().remove(goal);
			extension.deleteElement(goal);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}
	@Override
	public boolean canUndo() {
		return parent != null;
	}
}
