package dk.dtu.imm.red.specificationelements.goal.ui.editors;

import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;

public interface GoalPresenter extends IEditorPresenter {

	void setIsTopLevelGoal(boolean isTopLevel);
	
	/**
	 * @return the max length of the name text field in the Goal editor.
	 */
	int getGoalMaxLength();
	
	int getIdMaxLength();

}
