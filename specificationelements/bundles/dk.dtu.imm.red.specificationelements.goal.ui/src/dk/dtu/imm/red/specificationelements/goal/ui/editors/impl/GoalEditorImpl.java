package dk.dtu.imm.red.specificationelements.goal.ui.editors.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.GoalKind;
import dk.dtu.imm.red.specificationelements.goal.ui.editors.GoalEditor;
import dk.dtu.imm.red.specificationelements.goal.ui.editors.GoalPresenter;

/**
 * The editor for creating and editing a goal
 *
 * @author Anders Friis
 *
 */
public class GoalEditorImpl extends BaseEditor implements GoalEditor {

	protected ElementBasicInfoContainer basicInfoContainer;

	/**
	 * Index of the goal information page.
	 */
	protected int goalInfoPageIndex = 0;

	@Override
	public void doSave(IProgressMonitor monitor) {

		GoalPresenter presenter = (GoalPresenter) this.presenter;

		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());

		super.doSave(monitor);

	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		super.init(site, input);
		presenter = new GoalPresenterImpl(this, (Goal) element);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	protected void createPages() {
		goalInfoPageIndex = addPage(createGoalPage(getContainer()));
		setPageText(goalInfoPageIndex, "Summary");
		super.createPages();

		fillInitialData();
	}

	private Composite createGoalPage(Composite parent) {

		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (Goal) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);

		return SWTUtils.wrapInScrolledComposite(parent, composite);

	}

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Goal.html";
	}

	@Override
	protected void fillInitialData() {
		Goal goal = (Goal) element;

		GoalKind[] goalKinds = GoalKind.values();
		String[] goalKindStrings = new String[goalKinds.length];
		for(int i=0;i<goalKinds.length;i++){
			goalKindStrings[i] = goalKinds[i].getName();
		}

		basicInfoContainer.fillInitialData(goal, goalKindStrings);

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();
		// put all the attributes which we want to search through
		// into the attributesToSearchMap
		super.addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);


		// Now, search through all attributes, recording all search hits along the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()-1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(), result.getEndIndex());
			}
		}

		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}

	@Override
	public void selectText(EStructuralFeature feature, int startIndex, int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,goalInfoPageIndex,basicInfoContainer);
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.specificationelements.goal.goaleditor";
	}

}
