package dk.dtu.imm.red.specificationelements.goal.ui.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewGoalWizard extends IBaseWizard {

	String ID = "dk.dtu.imm.red.specificationelements.goal.newwizard";

	void setGoal(String text);
}
