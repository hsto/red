package dk.dtu.imm.red.specificationelements.goal.ui.wizards.impl;

import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.specificationelements.goal.ui.wizards.NewGoalWizard;

public class DefineGoalPage extends WizardPage implements IWizardPage {

	protected NewGoalWizard wizard;
	Label goalLabel;
	Label idLabel;
	Text goalTextBox;
	Text idNumberTextBox;
	Label explanationLabel;
	RichTextEditor explanationEditor;
	
	public DefineGoalPage(NewGoalWizard wizard) {
		super("Define Goal");

		setTitle("Create New Goal");
		setDescription("Use this wizard to create a new goal");
		
		this.wizard = wizard;
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(4, false);

		container.setLayout(layout);
		
		goalLabel = new Label(container, SWT.NONE);
		goalLabel.setText("Goal");
		goalTextBox = new Text(container, SWT.BORDER);
		
		idLabel = new Label(container, SWT.NONE);
		idLabel.setText("Id");
		idNumberTextBox = new Text(container, SWT.BORDER);
		
		GridData editorGridData = new GridData();
		editorGridData.horizontalSpan = 4;
		editorGridData.grabExcessHorizontalSpace = true;
		editorGridData.horizontalAlignment = SWT.FILL;
		editorGridData.grabExcessVerticalSpace = true;
		editorGridData.verticalAlignment = SWT.FILL;

		GridData labelGridData = new GridData();
		labelGridData.horizontalSpan = 4;
		
		explanationLabel = new Label(container, SWT.NONE);
		explanationLabel.setText("Explanation");
		explanationLabel.setLayoutData(labelGridData);
		
		explanationEditor = new RichTextEditor(container, SWT.BORDER,
				null, null);
		explanationEditor.setLayoutData(editorGridData);
		
		setPageComplete(true);
		setControl(container);

	}
	
	@Override
	public IWizardPage getNextPage() {
		wizard.setGoal(goalTextBox.getText());
		return super.getNextPage();
	}

	public String getGoal() {
		return goalTextBox.getText();
	}
	
	public String getGoalId(){
		return idNumberTextBox.getText();
	}
	
	public String getGoalExplanation(){
		return explanationEditor.getText();
	}
}
