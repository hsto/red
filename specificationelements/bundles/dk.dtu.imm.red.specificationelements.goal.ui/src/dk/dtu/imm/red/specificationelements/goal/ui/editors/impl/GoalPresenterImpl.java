package dk.dtu.imm.red.specificationelements.goal.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.ui.editors.GoalEditor;
import dk.dtu.imm.red.specificationelements.goal.ui.editors.GoalPresenter;

public class GoalPresenterImpl extends BaseEditorPresenter implements
		GoalPresenter {

	private static final int MAX_GOAL_LENGTH = 69;
	private static final int MAX_ID_LENGTH = 5;
	
	protected GoalEditor goalEditor;
	
	protected boolean isTopLevelGoal;

	public GoalPresenterImpl(GoalEditor goalEditor, Goal element) {
		super(element);
		this.goalEditor = goalEditor;
		this.element = element;
	}

	@Override
	public void save() {
		super.save();
		Goal goal = (Goal) element;
		goal.setIsTopLevelGoal(isTopLevelGoal);
		goal.save();
	}
	
	@Override
	public void setIsTopLevelGoal(boolean isTopLevel) {
		isTopLevelGoal = isTopLevel;
	}

	@Override
	public int getGoalMaxLength() {
		return MAX_GOAL_LENGTH;
	}

	@Override
	public int getIdMaxLength() {
		return MAX_ID_LENGTH;
	}

}
