package dk.dtu.imm.red.specificationelements.goal.ui.editors.impl;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.specificationelements.goal.Goal;

public class GoalEditorInputImpl extends BaseEditorInput {
	
	public GoalEditorInputImpl(Goal goal) {
		super(goal);
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Goal editor";
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return "This is the goal editor";
	}
}
