/**
 */
package dk.dtu.imm.red.specificationelements.goal;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.goal.Goal#isIsTopLevelGoal <em>Is Top Level Goal</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.specificationelements.goal.GoalPackage#getGoal()
 * @model
 * @generated
 */
public interface Goal extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Is Top Level Goal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Top Level Goal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Top Level Goal</em>' attribute.
	 * @see #setIsTopLevelGoal(boolean)
	 * @see dk.dtu.imm.red.specificationelements.goal.GoalPackage#getGoal_IsTopLevelGoal()
	 * @model
	 * @generated
	 */
	boolean isIsTopLevelGoal();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.specificationelements.goal.Goal#isIsTopLevelGoal <em>Is Top Level Goal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Top Level Goal</em>' attribute.
	 * @see #isIsTopLevelGoal()
	 * @generated
	 */
	void setIsTopLevelGoal(boolean value);

} // Goal
