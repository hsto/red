/**
 */
package dk.dtu.imm.red.specificationelements.goal.impl;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.GoalPackage;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.specificationelements.goal.impl.GoalImpl#isIsTopLevelGoal <em>Is Top Level Goal</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GoalImpl extends SpecificationElementImpl implements Goal {
	/**
	 * The default value of the '{@link #isIsTopLevelGoal() <em>Is Top Level Goal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsTopLevelGoal()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_TOP_LEVEL_GOAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsTopLevelGoal() <em>Is Top Level Goal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsTopLevelGoal()
	 * @generated
	 * @ordered
	 */
	protected boolean isTopLevelGoal = IS_TOP_LEVEL_GOAL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GoalPackage.Literals.GOAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsTopLevelGoal() {
		return isTopLevelGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsTopLevelGoal(boolean newIsTopLevelGoal) {
		boolean oldIsTopLevelGoal = isTopLevelGoal;
		isTopLevelGoal = newIsTopLevelGoal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GoalPackage.GOAL__IS_TOP_LEVEL_GOAL, oldIsTopLevelGoal, isTopLevelGoal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GoalPackage.GOAL__IS_TOP_LEVEL_GOAL:
				return isIsTopLevelGoal();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GoalPackage.GOAL__IS_TOP_LEVEL_GOAL:
				setIsTopLevelGoal((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GoalPackage.GOAL__IS_TOP_LEVEL_GOAL:
				setIsTopLevelGoal(IS_TOP_LEVEL_GOAL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GoalPackage.GOAL__IS_TOP_LEVEL_GOAL:
				return isTopLevelGoal != IS_TOP_LEVEL_GOAL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isTopLevelGoal: ");
		result.append(isTopLevelGoal);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/goal.png";
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		return super.search(param, attributesToSearch);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation() {
		return new ElementColumnDefinition[] {
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Goal>() {

					@Override
					public Object getAttributeValue(Goal item) {
						return item.getName()==null?"":item.getName();
					}

					@Override
					public void setAttributeValue(Goal item, Object value) {
						item.setName((String) value);
					}
				})
				,
				new ElementColumnDefinition()
				.setHeaderName("Description")
				.setEMFLiteral(ElementPackage.Literals.ELEMENT__DESCRIPTION)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<Goal>() {

					@Override
					public Object getAttributeValue(Goal item) {
						return item.getDescription()==null?"":item.getDescription();
					}

					@Override
					public void setAttributeValue(Goal item, Object value) {
						item.setDescription((String) value);
					}
				})
//				,
//				new ElementColumnDefinition()
//				.setHeaderName("Explanation")
//				.setEMFLiteral(GoalPackage.Literals.GOAL__EXPLANATION)
//				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
//				.setCellItems(ElementColumnDefinition.enumToStringArray(GoalKind.values()))
//				.setEditable(true)
//				.setCellHandler(new CellHandler<Goal>() {
//
//					@Override
//					public Object getAttributeValue(Goal item) {
//						return item.getKind().toString();
//					}
//
//					@Override
//					public void setAttributeValue(Goal item, Object value) {
//						item.setKind(Enum.valueOf(GoalKind.class, value.toString().toUpperCase()));
//					}
//				})
		};
	}

} //GoalImpl
