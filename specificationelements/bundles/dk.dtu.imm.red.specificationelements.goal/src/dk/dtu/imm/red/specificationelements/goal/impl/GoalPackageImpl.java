/**
 */
package dk.dtu.imm.red.specificationelements.goal.impl;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.GoalFactory;
import dk.dtu.imm.red.specificationelements.goal.GoalKind;
import dk.dtu.imm.red.specificationelements.goal.GoalPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GoalPackageImpl extends EPackageImpl implements GoalPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum goalKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.specificationelements.goal.GoalPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GoalPackageImpl() {
		super(eNS_URI, GoalFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GoalPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GoalPackage init() {
		if (isInited) return (GoalPackage)EPackage.Registry.INSTANCE.getEPackage(GoalPackage.eNS_URI);

		// Obtain or create and register package
		GoalPackageImpl theGoalPackage = (GoalPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GoalPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GoalPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationelementsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theGoalPackage.createPackageContents();

		// Initialize created meta-data
		theGoalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGoalPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GoalPackage.eNS_URI, theGoalPackage);
		return theGoalPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoal() {
		return goalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoal_IsTopLevelGoal() {
		return (EAttribute)goalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getGoalKind() {
		return goalKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalFactory getGoalFactory() {
		return (GoalFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		goalEClass = createEClass(GOAL);
		createEAttribute(goalEClass, GOAL__IS_TOP_LEVEL_GOAL);

		// Create enums
		goalKindEEnum = createEEnum(GOAL_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SpecificationelementsPackage theSpecificationelementsPackage = (SpecificationelementsPackage)EPackage.Registry.INSTANCE.getEPackage(SpecificationelementsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		goalEClass.getESuperTypes().add(theSpecificationelementsPackage.getSpecificationElement());

		// Initialize classes and features; add operations and parameters
		initEClass(goalEClass, Goal.class, "Goal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGoal_IsTopLevelGoal(), ecorePackage.getEBoolean(), "isTopLevelGoal", null, 0, 1, Goal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(goalKindEEnum, GoalKind.class, "GoalKind");
		addEEnumLiteral(goalKindEEnum, GoalKind.UNSPECIFIED);
		addEEnumLiteral(goalKindEEnum, GoalKind.STRATEGIC);
		addEEnumLiteral(goalKindEEnum, GoalKind.BUSINESS);
		addEEnumLiteral(goalKindEEnum, GoalKind.SOLUTION);

		// Create resource
		createResource(eNS_URI);
	}

} //GoalPackageImpl
