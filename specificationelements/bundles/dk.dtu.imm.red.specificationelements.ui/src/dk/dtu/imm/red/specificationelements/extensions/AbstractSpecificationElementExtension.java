package dk.dtu.imm.red.specificationelements.extensions;

import org.eclipse.epf.richtext.actions.ILinkActionExtension;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public abstract class AbstractSpecificationElementExtension implements IElementExtensionPoint, ILinkActionExtension {

	protected abstract void openConcreteElement(Element element);

	@Override
	public void openElement(Element element){

			openConcreteElement(element);

	}

	@Override
	public void restoreElement(Element element) {
		// don't really care.
	}

	@Override
	public void deleteElement(Element element) {

		// Remove the element from it's parent, if not done already
		if (element.getParent() != null) {
			element.getParent().getContents().remove(element);
		}

		// Close all editors for this element
		IEditorReference[] editors = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.getEditorReferences();
		for (IEditorReference editor : editors) {
			// if the editor is opened for the given element
			if (editor.getPart(false) instanceof BaseEditor
					&& ((BaseEditor) editor.getPart(false)).getEditorElement() == element) {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getActivePage()
						.closeEditor(editor.getEditor(false), false);
			}
		}
	}

	// ILinkActionExtension
	@Override
	public void linkClicked(String target) {
		Element targetElement = WorkspaceFactory.eINSTANCE
				.getWorkspaceInstance().findDescendantByUniqueID(target);

		if (targetElement != null) {
			openElement(targetElement);
		}
	}

}
