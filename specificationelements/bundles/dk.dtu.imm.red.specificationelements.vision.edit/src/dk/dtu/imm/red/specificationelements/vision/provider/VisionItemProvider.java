/**
 */
package dk.dtu.imm.red.specificationelements.vision.provider;


import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.specificationelements.provider.SpecificationElementItemProvider;
import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.specificationelements.vision.VisionPackage;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.specificationelements.vision.Vision} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class VisionItemProvider
	extends SpecificationElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addVisionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Vision feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVisionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Vision_vision_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Vision_vision_feature", "_UI_Vision_type"),
				 VisionPackage.Literals.VISION__VISION,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VisionPackage.Literals.VISION__EXPECTED_VALUE_OUTCOME);
			childrenFeatures.add(VisionPackage.Literals.VISION__OPPORTUNITIES);
			childrenFeatures.add(VisionPackage.Literals.VISION__CHALLENGES);
			childrenFeatures.add(VisionPackage.Literals.VISION__SPONSORS);
			childrenFeatures.add(VisionPackage.Literals.VISION__ADVERSARIES);
			childrenFeatures.add(VisionPackage.Literals.VISION__GROUP_VALUES_AND_COM);
			childrenFeatures.add(VisionPackage.Literals.VISION__BELIEFS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Vision.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Vision"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Vision)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Vision_type") :
			getString("_UI_Vision_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Vision.class)) {
			case VisionPackage.VISION__VISION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case VisionPackage.VISION__EXPECTED_VALUE_OUTCOME:
			case VisionPackage.VISION__OPPORTUNITIES:
			case VisionPackage.VISION__CHALLENGES:
			case VisionPackage.VISION__SPONSORS:
			case VisionPackage.VISION__ADVERSARIES:
			case VisionPackage.VISION__GROUP_VALUES_AND_COM:
			case VisionPackage.VISION__BELIEFS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VisionPackage.Literals.VISION__EXPECTED_VALUE_OUTCOME,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(VisionPackage.Literals.VISION__OPPORTUNITIES,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(VisionPackage.Literals.VISION__CHALLENGES,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(VisionPackage.Literals.VISION__SPONSORS,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(VisionPackage.Literals.VISION__ADVERSARIES,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(VisionPackage.Literals.VISION__GROUP_VALUES_AND_COM,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(VisionPackage.Literals.VISION__BELIEFS,
				 TextFactory.eINSTANCE.createText()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ElementPackage.Literals.ELEMENT__COMMENTLIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CHANGE_LIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CREATOR ||
			childFeature == ElementPackage.Literals.ELEMENT__RESPONSIBLE_USER ||
			childFeature == ElementPackage.Literals.ELEMENT__COST ||
			childFeature == ElementPackage.Literals.ELEMENT__BENEFIT ||
			childFeature == ElementPackage.Literals.ELEMENT__MANAGEMENT_DISCUSSION ||
			childFeature == VisionPackage.Literals.VISION__EXPECTED_VALUE_OUTCOME ||
			childFeature == VisionPackage.Literals.VISION__OPPORTUNITIES ||
			childFeature == VisionPackage.Literals.VISION__CHALLENGES ||
			childFeature == VisionPackage.Literals.VISION__SPONSORS ||
			childFeature == VisionPackage.Literals.VISION__ADVERSARIES ||
			childFeature == VisionPackage.Literals.VISION__GROUP_VALUES_AND_COM ||
			childFeature == VisionPackage.Literals.VISION__BELIEFS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return VisionEditPlugin.INSTANCE;
	}

}
