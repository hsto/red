package dk.dtu.imm.red.specificationelements.goal.ui.editors.impl;

import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.GoalFactory;
import dk.dtu.imm.red.specificationelements.goal.ui.editors.GoalEditor;

/**
 * Unit test for the goal editor
 *
 * @author Friis
 *
 */
@Ignore("Requires maintenance")
public class GoalEditorTest {

	private Goal goal;
	private GoalEditorInputImpl editorInput;
	private GoalEditorImpl editor;

	@Before
	public void setUp() throws Exception {
		goal = GoalFactory.eINSTANCE.createGoal();
		editorInput = new GoalEditorInputImpl(goal);

		editor = (GoalEditorImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.openEditor(editorInput, GoalEditor.ID);
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Tests that the label for the goal text box exists
	 */
	@Test
	public void testGoalLabelExists() {

//		assertNotNull(editor.goalLabel);
	}

	/**
	 * Tests that there exists a text box for goal
	 */
	@Test
	public void testGoalTextBoxExists() {

//		assertNotNull(editor.goalTextBox);
	}


	/**
	 * Tests that the label for id number exists
	 */
	@Test
	public void testIdNumberLabelExists() {

//		assertNotNull(editor.idNumberLabel);
	}

	/**
	 * Tests that there exists a text box for id number
	 */
	@Test
	public void testIdNumberTextBoxExists() {

//		assertNotNull(editor.idNumberTextBox);
	}

}
