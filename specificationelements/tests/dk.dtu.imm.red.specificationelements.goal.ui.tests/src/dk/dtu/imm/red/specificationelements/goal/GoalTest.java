package dk.dtu.imm.red.specificationelements.goal;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.relationship.ElementReference;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.element.relationship.impl.ElementReferenceImpl;
import dk.dtu.imm.red.core.element.relationship.impl.RelationshipFactoryImpl;
import dk.dtu.imm.red.specificationelements.goal.impl.GoalFactoryImpl;
import dk.dtu.imm.red.specificationelements.goal.impl.GoalImpl;

public class GoalTest {

	private Goal goal;
	
	@Before
	public void setUp() throws Exception {
		goal = new GoalFactoryImpl().createGoal();
		goal.setName("Goal 1");
		goal.setLabel("g1");
	}

	@Test
	public void testEmptyRelationship() {
		goal.checkStructure();
	}

	@Test
	public void testRelationship(){
		
		Goal g2 = new GoalFactoryImpl().createGoal();
		g2.setName("Goal 2");
		g2.setLabel("g2");
		
		ElementReference ref = RelationshipFactoryImpl.eINSTANCE.createElementReference();
		ref.setFromElement(goal);
		ref.setToElement(g2);
		
		goal.getRelatesTo().add(ref);
		g2.getRelatedBy().add(ref);
		
		ref.setRelationshipKind(RelationshipKind.JUSTIFIES);
		
		goal.checkStructure();
	}
}
