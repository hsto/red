package dk.dtu.imm.red.specificationelements.requirement.ui.editors.impl;

import static org.junit.Assert.assertNotNull;

import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.requirement.RequirementFactory;
import dk.dtu.imm.red.specificationelements.requirement.ui.editors.RequirementEditor;
import dk.dtu.imm.red.specificationelements.requirement.ui.editors.impl.RequirementEditorImpl;
import dk.dtu.imm.red.specificationelements.requirement.ui.editors.impl.RequirementEditorInputImpl;

/**
 * Unit test for the requirement editor
 * 
 * @author Anders Friis
 * 
 */
@Ignore("Requires maintenance")
public class RequirementEditorTest {

	private Requirement requirement;
	private RequirementEditorInputImpl editorInput;
	private RequirementEditorImpl editor;

	@Before
	public void setUp() throws Exception {

		requirement = RequirementFactory.eINSTANCE.createRequirement();
		editorInput = new RequirementEditorInputImpl(requirement);

		editor = (RequirementEditorImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.openEditor(editorInput, RequirementEditor.ID);
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Tests that the label for the name text box exists
	 */
	@Test
	public void testNameLabelExists() {

//		assertNotNull(editor.requirementLabel);
	}

	/**
	 * Tests that the text box for name exists
	 */
	@Test
	public void testNameTextBoxExists() {

//		assertNotNull(editor.requirementTextBox);
	}


	/**
	 * Tests that the label for the type text box exists
	 */
	@Test
	public void testTypeLabelExists() {

//		assertNotNull(editor.typeLabel);
	}

	/**
	 * Tests that the rich text editor for description exists
	 */
	@Test
	public void testDescriptionEditorExists() {

		assertNotNull(editor.elaborationEditor);
	}
}
