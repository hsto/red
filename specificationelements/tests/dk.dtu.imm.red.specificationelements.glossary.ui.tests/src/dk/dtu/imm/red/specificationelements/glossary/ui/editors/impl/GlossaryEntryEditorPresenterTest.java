package dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for the glossary entry editor presenter.
 * 
 * @author Jakob Kragelund
 * 
 */
public class GlossaryEntryEditorPresenterTest {

	/**
	 * Sets up any initial things before test cases are run.
	 * 
	 * @throws Exception
	 *             If any exceptions happen during startup.
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Clear up after all tests have run.
	 * 
	 * @throws Exception
	 *             If any exceptions happen during cleanup.
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * 
	 */
	@Test
	public void test() {
	}

}
