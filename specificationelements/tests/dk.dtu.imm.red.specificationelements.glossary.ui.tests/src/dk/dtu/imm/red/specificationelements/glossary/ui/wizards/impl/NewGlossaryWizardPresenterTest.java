package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryFactory;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl.NewGlossaryWizardPresenterImpl;

/**
 * Unit tests for the New Glossary Wizard presenter. Uses a stub New Glossary
 * Wizard.
 * 
 * @author Jakob Kragelund
 * 
 */
public class NewGlossaryWizardPresenterTest {

	/**
	 * The presenter under test.
	 */
	private NewGlossaryWizardPresenterImpl presenter;

	@Before
	public void setUp() throws Exception {
		presenter = new NewGlossaryWizardPresenterImpl();
	}

	@After
	public void tearDown() throws Exception {
		presenter = null;
	}

	/**
	 * Test that when the presenter is notified of a finished wizard with a name
	 * and no parent, the name and parent returned by the presenter are correct.
	 */
	@Ignore("Requires maintenance") @Test
	public void testNewGlossaryNameNoParent() {
		presenter.wizardFinished("newglossary", null, "");

		assertEquals("newglossary", presenter.getNewglossaryName());
		assertNull(presenter.getNewGlossaryParent());
	}

	/**
	 * Test that when the presenter is notified of a finished wizard with a name
	 * and a parent, the name and parent returned by the presenter are correct.
	 */
	@Test
	public void testNewGlossaryNameAndParent() {
		Glossary parentGlossary = GlossaryFactory.eINSTANCE.createGlossary();
		presenter.wizardFinished("glossarynew", parentGlossary, "");

		assertEquals("glossarynew", presenter.getNewglossaryName());
		assertEquals(parentGlossary, presenter.getNewGlossaryParent());
	}

}
