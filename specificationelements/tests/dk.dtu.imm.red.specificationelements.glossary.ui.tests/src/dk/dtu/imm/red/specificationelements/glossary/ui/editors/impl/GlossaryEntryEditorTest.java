package dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryFactory;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.GlossaryEntryEditor;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl.GlossaryEntryEditorImpl;
import dk.dtu.imm.red.specificationelements.glossary.ui.editors.impl.GlossaryEntryEditorInput;

/**
 * Unit tests for the glossary entry editor.
 *
 * @author Jakob Kragelund
 *
 */
public final class GlossaryEntryEditorTest {
//
//	/**
//	 * The editor input for the editor under test.
//	 */
//	private GlossaryEntryEditorInput input;
//
//	/**
//	 * The editor under test.
//	 */
//	private GlossaryEntryEditorImpl editor;
//
//	/**
//	 * Sets up any initial things before test cases are run.
//	 *
//	 * @throws Exception
//	 *             If any exceptions happen during startup.
//	 */
////	@Before
////	public void setUp() throws Exception {
////		GlossaryEntry entry = GlossaryFactory.eINSTANCE.createGlossaryEntry();
////		entry.setLabel("");
////		Text definition = TextFactory.eINSTANCE.createText();
////		entry.setDescription(definition);
////
////		input = new GlossaryEntryEditorInput(entry);
////		editor = (GlossaryEntryEditorImpl) PlatformUI.getWorkbench()
////				.getActiveWorkbenchWindow().getActivePage()
////				.openEditor(input, GlossaryEntryEditor.ID);
////	}
//
//	/**
//	 * Clear up after all tests have run.
//	 *
//	 * @throws Exception
//	 *             If any exceptions happen during cleanup.
//	 */
//	@After
//	public void tearDown() throws Exception {
//		editor.dispose();
//	}
//
//	/**
//	 * Test that the editor contains a term label.
//	 */
//	@Test
//	public void testEditorContainsTermLabel() {
//		assertNotNull(editor.termLabel);
//	}
//
//	/**
//	 * Test the contents of the term label.
//	 */
//	@Test
//	public void testTermLabelContents() {
//		assertEquals("Term:", editor.termLabel.getText());
//	}
//
//	/**
//	 * Test that the editor contains a text-field for the term.
//	 */
//	@Test
//	public void testEditorContainsTermText() {
//		assertNotNull(editor.termText);
//	}
//
//	/**
//	 * Test that the text-field for the term is disabled initially.
//	 */
//	@Test
//	public void testTermTextNotEditable() {
//		//assertFalse(editor.termText.getEditable());
//
//		// changed by Maciej Kucharek
//		// TODO: verify that this field should be enabled on default (the test suggested otherwise, but it probably was not updated after the code change)
//		assertTrue(editor.termText.getEditable());
//	}
//
//	/**
//	 * Test that the editor contains a label for the abbreviations list.
//	 */
//	@Test
//	public void testEditorContainsAbbreviationsLabel() {
//		assertNotNull(editor.abbreviationsLabel);
//	}
//
//	/**
//	 * Test the content of the abbreviations label.
//	 */
//	@Test
//	public void testAbbreviationsLabelContent() {
//		assertEquals("Abbreviations:", editor.abbreviationsLabel.getText());
//	}
//
//	@Test
//	public void testEditorContainsAbbreviationsListLabel() {
//		assertNotNull(editor.abbreviationsListLabel);
//	}
//
//	@Test
//	public void testEditorContainsManageAbbreviationsButton() {
//		assertNotNull(editor.manageAbbreviationsButton);
//	}
//
//	@Test
//	public void testManageAbbreviationsButtonText() {
//		//assertEquals("+/-", editor.manageAbbreviationsButton.getText());
//
//		// changed by Maciej Kucharek
//		// seems like the change has been made in the code, but the tests were not updated
//		assertEquals("Add/Delete Abbreviations", editor.manageAbbreviationsButton.getText());
//	}
//
//	/**
//	 * Test that the editor contains a label for the synonyms list.
//	 */
//	@Test
//	public void testEditorContainsSynonymsLabel() {
//		assertNotNull(editor.synonymsLabel);
//	}
//
//	/**
//	 * Test the content of the synonyms label.
//	 */
//	@Test
//	public void testSynonymsLabelContent() {
//		assertEquals("Synonyms:", editor.synonymsLabel.getText());
//	}
//
//	@Test
//	public void testEditorContainsSynonymsListLabel() {
//		assertNotNull(editor.synonymsListLabel);
//	}
//
//	@Test
//	public void testEditorContainsManageSynonymsButton() {
//		assertNotNull(editor.manageSynonymsButton);
//	}
//
//	@Test
//	public void testAddSynonymButtonText() {
//		//assertEquals("+/-", editor.manageSynonymsButton.getText());
//
//		// changed by Maciej Kucharek
//		// seems like the change has been made in the code, but the tests were not updated
//		assertEquals("Add/Delete Synonyms", editor.manageSynonymsButton.getText());
//	}
//
//	@Test
//	public void testEditorContainsDefinitionLabel() {
//		assertNotNull(editor.definitionLabel);
//	}
//
//	@Test
//	public void testDefinitionLabelContent() {
//		assertEquals("Definition:", editor.definitionLabel.getText());
//	}
//
//	@Test
//	public void testEditorContainsDefinitionEditor() {
//		assertNotNull(editor.definitionEditor);
//	}
//
////	@Test
////	public void testTermHasCorrectContent() throws PartInitException {
////		GlossaryEntry entry = GlossaryFactory.eINSTANCE.createGlossaryEntry();
////		entry.setTerm("testterm");
////
////		GlossaryEntryEditorInput tempInput = new GlossaryEntryEditorInput(entry);
////		GlossaryEntryEditorImpl tempEditor = (GlossaryEntryEditorImpl) PlatformUI
////				.getWorkbench().getActiveWorkbenchWindow().getActivePage()
////				.openEditor(tempInput, GlossaryEntryEditor.ID);
////
////		assertEquals(entry.getTerm(), tempEditor.termText.getText());
////
////	}
////
////	@Test
////	public void testAbbreviationsHasCorrectContentZeroAbbrevs()
////			throws PartInitException {
////		GlossaryEntry entry = GlossaryFactory.eINSTANCE.createGlossaryEntry();
////		entry.setTerm("testterm");
////
////		GlossaryEntryEditorInput tempInput = new GlossaryEntryEditorInput(entry);
////		GlossaryEntryEditorImpl tempEditor = (GlossaryEntryEditorImpl) PlatformUI
////				.getWorkbench().getActiveWorkbenchWindow().getActivePage()
////				.openEditor(tempInput, GlossaryEntryEditor.ID);
////
////		assertEquals("", tempEditor.abbreviationsListLabel.getText());
////	}
////
////	@Test
////	public void testAbbreviationsHasCorrectContentOneAbbrev()
////			throws PartInitException {
////		GlossaryEntry entry = GlossaryFactory.eINSTANCE.createGlossaryEntry();
////		entry.setTerm("testterm");
////
////		entry.getAbbreviations().add("abb1");
////
////		GlossaryEntryEditorInput tempInput = new GlossaryEntryEditorInput(entry);
////		GlossaryEntryEditorImpl tempEditor = (GlossaryEntryEditorImpl) PlatformUI
////				.getWorkbench().getActiveWorkbenchWindow().getActivePage()
////				.openEditor(tempInput, GlossaryEntryEditor.ID);
////
////		assertEquals("abb1", tempEditor.abbreviationsListLabel.getText());
////	}
////
////	@Test
////	public void testAbbreviationsHasCorrectContentThreeAbbrevs()
////			throws PartInitException {
////		GlossaryEntry entry = GlossaryFactory.eINSTANCE.createGlossaryEntry();
////		entry.setTerm("testterm");
////
////		entry.getAbbreviations().add("abb1");
////		entry.getAbbreviations().add("abb2");
////		entry.getAbbreviations().add("abb3");
////
////		GlossaryEntryEditorInput tempInput = new GlossaryEntryEditorInput(entry);
////		GlossaryEntryEditorImpl tempEditor = (GlossaryEntryEditorImpl) PlatformUI
////				.getWorkbench().getActiveWorkbenchWindow().getActivePage()
////				.openEditor(tempInput, GlossaryEntryEditor.ID);
////
////		assertEquals("abb1, abb2, abb3",
////				tempEditor.abbreviationsListLabel.getText());
////	}
////
////	@Test
////	public void testSynonymsHasCorrectContentZeroSynonyms()
////			throws PartInitException {
////		GlossaryEntry entry = GlossaryFactory.eINSTANCE.createGlossaryEntry();
////		entry.setTerm("testterm");
////
////		GlossaryEntryEditorInput tempInput = new GlossaryEntryEditorInput(entry);
////		GlossaryEntryEditorImpl tempEditor = (GlossaryEntryEditorImpl) PlatformUI
////				.getWorkbench().getActiveWorkbenchWindow().getActivePage()
////				.openEditor(tempInput, GlossaryEntryEditor.ID);
////
////		assertEquals("", tempEditor.synonymsListLabel.getText());
////	}
////
////	@Test
////	public void testSynonymsHasCorrectContentOneSynonym()
////			throws PartInitException {
////		GlossaryEntry entry = GlossaryFactory.eINSTANCE.createGlossaryEntry();
////		entry.setTerm("testterm");
////
////		entry.getSynonyms().add("syn1");
////
////		GlossaryEntryEditorInput tempInput = new GlossaryEntryEditorInput(entry);
////		GlossaryEntryEditorImpl tempEditor = (GlossaryEntryEditorImpl) PlatformUI
////				.getWorkbench().getActiveWorkbenchWindow().getActivePage()
////				.openEditor(tempInput, GlossaryEntryEditor.ID);
////
////		assertEquals("syn1", tempEditor.synonymsListLabel.getText());
////	}
////
////	@Test
////	public void testSynonymsHasCorrectContentThreeSynonyms()
////			throws PartInitException {
////		GlossaryEntry entry = GlossaryFactory.eINSTANCE.createGlossaryEntry();
////		entry.setTerm("testterm");
////
////		entry.getSynonyms().add("syn1");
////		entry.getSynonyms().add("syn2");
////		entry.getSynonyms().add("syn3");
////
////		GlossaryEntryEditorInput tempInput = new GlossaryEntryEditorInput(entry);
////		GlossaryEntryEditorImpl tempEditor = (GlossaryEntryEditorImpl) PlatformUI
////				.getWorkbench().getActiveWorkbenchWindow().getActivePage()
////				.openEditor(tempInput, GlossaryEntryEditor.ID);
////
////		assertEquals("syn1, syn2, syn3", tempEditor.synonymsListLabel.getText());
////	}

}
