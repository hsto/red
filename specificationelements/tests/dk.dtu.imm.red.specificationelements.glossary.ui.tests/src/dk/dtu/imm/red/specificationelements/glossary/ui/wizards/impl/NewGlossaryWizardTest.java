package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import static org.junit.Assert.assertNotNull;

import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl.NewGlossaryWizardImpl;

/**
 * Unit tests for the New Glossary Wizard.
 * 
 * @author Jakob Kragelund
 * 
 */
public class NewGlossaryWizardTest {

	/**
	 * The wizard under test.
	 */
	private NewGlossaryWizardImpl wizard;

	/**
	 * Dialog which contains the wizard.
	 */
	WizardDialog dialog;

	@Before
	public void setUp() throws Exception {
		wizard = new NewGlossaryWizardImpl();
		wizard.init(PlatformUI.getWorkbench(), null);
		dialog = new WizardDialog(Display.getCurrent().getActiveShell(), wizard);
		dialog.setBlockOnOpen(false);
		dialog.open();
	}

	@After
	public void tearDown() throws Exception {
		dialog.close();
		dialog = null;
		wizard = null;
	}

	/**
	 * Tests that a presenter gets created for a new wizard.
	 */
	@Test
	public void testPresenterCreated() {
		assertNotNull(wizard.presenter);
	}

}
