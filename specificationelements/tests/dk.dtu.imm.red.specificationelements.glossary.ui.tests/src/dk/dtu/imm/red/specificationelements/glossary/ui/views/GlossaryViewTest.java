package dk.dtu.imm.red.specificationelements.glossary.ui.views;
//package dk.dtu.imm.red.glossary.implementation.views;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.eclipse.ui.PlatformUI;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
///**
// * Unit tests for the glossary viewWithEntry.
// * 
// * @author Jakob Kragelund
// * 
// */
//public final class GlossaryViewTest {
//
//	/**
//	 * The viewWithEntry under test.
//	 */
//	private GlossaryViewImpl view;
//
//	/**
//	 * Sets up any initial things before test cases are run.
//	 * 
//	 * @throws Exception
//	 *             If any exceptions happen during startup.
//	 */
//	@Before
//	public void setUp() throws Exception {
//		view = (GlossaryViewImpl) PlatformUI.getWorkbench()
//				.getActiveWorkbenchWindow().getActivePage()
//				.showView(GlossaryViewImpl.ID);
//
//	}
//
//	/**
//	 * Clear up after all tests have run.
//	 * 
//	 * @throws Exception
//	 *             If any exceptions happen during cleanup.
//	 */
//	@After
//	public void tearDown() throws Exception {
//		view.dispose();
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the view contains a
//	 * label for the term label. Note the difference between the term content
//	 * label itself and the label in front of that label. This test is for the
//	 * label in front of the term content label.
//	 */
//	@Test
//	public void testEntryViewContainsTermLabel() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertNotNull(view.termLabel);
//	}
//
//	/**
//	 * Test that the content of the term label is as expected.
//	 */
//	@Test
//	public void testEntryViewTermLabelContents() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertEquals("Term:", view.termLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the view contains a
//	 * label for the term. Note the difference between the term content label
//	 * itself and the label in front of that label. This test is for the term
//	 * content label
//	 */
//	@Test
//	public void testEntryViewContainsTermContentLabel() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertNotNull(view.termContentLabel);
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the term content
//	 * label is set correctly.
//	 */
//	@Test
//	public void testEntryViewTermContentLabelContent() {
//		view.showGlossaryEntry("librarian", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertEquals("librarian", view.termContentLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the view contains a
//	 * label for the definition label. Note the difference between the
//	 * definition content label itself and the label in front of that label.
//	 * This test is for the label in front of the definition content label.
//	 */
//	@Test
//	public void testEntryViewContainsDefinitionLabel() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertNotNull(view.definitionLabel);
//	}
//
//	/**
//	 * Test that the content of the definition label is as expected.
//	 */
//	@Test
//	public void testEntryViewDefinitionLabelContents() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertEquals("Description:", view.definitionLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the view contains a
//	 * label for the definition. Note the difference between the definition
//	 * content label itself and the label in front of that label. This test is
//	 * for the definition content label
//	 */
//	@Test
//	public void testEntryViewContainsDefinitionContentLabel() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertNotNull(view.definitionContentLabel);
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the definition
//	 * content label is set correctly.
//	 */
//	@Test
//	public void testEntryViewDefinitionContentLabelContent() {
//		String definition = "a librarian is a librarian";
//		view.showGlossaryEntry("", definition, new ArrayList<String>(),
//				new ArrayList<String>());
//		assertEquals(definition, view.definitionContentLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, there is a label
//	 * for the abbreviations list.
//	 */
//	@Test
//	public void testEntryViewContainsAbbreviationsLabel() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertNotNull(view.abbreviationsLabel);
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the abbreviations
//	 * label has the right content.
//	 */
//	@Test
//	public void testEntryViewAbbreviationsLabelContent() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertEquals("Abbreviations:", view.abbreviationsLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, there is an
//	 * abbreviations content label.
//	 */
//	@Test
//	public void testEntryViewContainsAbbreviationsContentLabel() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertNotNull(view.abbreviationsContentLabel);
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the abbreviations
//	 * content label shows the correct content when there is one abbreviation.
//	 */
//	@Test
//	public void testEntryViewAbbreviationsContentLabelOneAbbreviation() {
//		List<String> abbreviations = new ArrayList<String>();
//		abbreviations.add("abb1");
//		view.showGlossaryEntry("", "", abbreviations, new ArrayList<String>());
//		assertEquals("abb1", view.abbreviationsContentLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the abbreviations
//	 * content label shows the correct content when there are two abbreviations.
//	 */
//	@Test
//	public void testEntryViewAbbreviationsContentLabelTwoAbbreviations() {
//		List<String> abbreviations = new ArrayList<String>();
//		abbreviations.add("abb1");
//		abbreviations.add("abb2");
//		view.showGlossaryEntry("", "", abbreviations, new ArrayList<String>());
//		assertEquals("abb1, abb2", view.abbreviationsContentLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the abbreviations
//	 * content label shows the correct content when there are five
//	 * abbreviations.
//	 */
//	@Test
//	public void testEntryViewAbbreviationsContentLabelFiveAbbreviations() {
//		List<String> abbreviations = new ArrayList<String>();
//		abbreviations.add("abb1");
//		abbreviations.add("abb2");
//		abbreviations.add("abb3");
//		abbreviations.add("abb4");
//		abbreviations.add("abb5");
//		view.showGlossaryEntry("", "", abbreviations, new ArrayList<String>());
//		assertEquals("abb1, abb2, abb3, abb4, abb5",
//				view.abbreviationsContentLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the abbreviations
//	 * content label shows the correct content when there are no abbreviations.
//	 */
//	@Test
//	public void testEntryViewAbbreviationsContentLabelZeroAbbreviations() {
//		List<String> abbreviations = new ArrayList<String>();
//
//		view.showGlossaryEntry("", "", abbreviations, new ArrayList<String>());
//		assertEquals("", view.abbreviationsContentLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, there is a label
//	 * for the synonym list.
//	 */
//	@Test
//	public void testEntryViewContainsSynonymsLabel() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertNotNull(view.synonymsLabel);
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the synonyms label
//	 * has the right content.
//	 */
//	@Test
//	public void testEntryViewSynonymsLabelContent() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertEquals("Synonyms:", view.synonymsLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, there is an
//	 * synonyms content label.
//	 */
//	@Test
//	public void testEntryViewContainsSynonymsContentLabel() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertNotNull(view.synonymsContentLabel);
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the synonyms
//	 * content label shows the correct content when there is one synonym.
//	 */
//	@Test
//	public void testEntryViewSynonymsContentLabelOneSynonym() {
//		List<String> synonyms = new ArrayList<String>();
//		synonyms.add("syn1");
//		view.showGlossaryEntry("", "", new ArrayList<String>(), synonyms);
//		assertEquals("syn1", view.synonymsContentLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the synonyms
//	 * content label shows the correct content when there are two synonyms.
//	 */
//	@Test
//	public void testEntryViewSynonymsContentLabelTwoSynonyms() {
//		List<String> synonyms = new ArrayList<String>();
//		synonyms.add("syn1");
//		synonyms.add("syn2");
//		view.showGlossaryEntry("", "", new ArrayList<String>(), synonyms);
//		assertEquals("syn1, syn2", view.synonymsContentLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the synonyms
//	 * content label shows the correct content when there are five synonyms.
//	 */
//	@Test
//	public void testEntryViewSynonymsContentLabelFiveSynonyms() {
//		List<String> synonyms = new ArrayList<String>();
//		synonyms.add("syn1");
//		synonyms.add("syn2");
//		synonyms.add("syn3");
//		synonyms.add("syn4");
//		synonyms.add("syn5");
//		view.showGlossaryEntry("", "", new ArrayList<String>(), synonyms);
//		assertEquals("syn1, syn2, syn3, syn4, syn5",
//				view.synonymsContentLabel.getText());
//	}
//
//	/**
//	 * Tests that when the view is showing a glossary entry, the synonyms
//	 * content label shows the correct content when there are no synonyms.
//	 */
//	@Test
//	public void testEntryViewSynonymsContentLabelZeroSynonyms() {
//		List<String> synonyms = new ArrayList<String>();
//
//		view.showGlossaryEntry("", "", new ArrayList<String>(), synonyms);
//		assertEquals("", view.synonymsContentLabel.getText());
//	}
//
//	@Test
//	public void testEntryViewContainsOpenEditorButton() {
//		view.showGlossaryEntry("", "", new ArrayList<String>(),
//				new ArrayList<String>());
//		assertNotNull(view.openEditorButton);
//	}
//
//}
