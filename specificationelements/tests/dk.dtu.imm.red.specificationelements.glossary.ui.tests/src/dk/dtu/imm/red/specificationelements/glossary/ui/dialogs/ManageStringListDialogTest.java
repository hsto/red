package dk.dtu.imm.red.specificationelements.glossary.ui.dialogs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.glossary.ui.dialogs.ManageStringListDialog;

/**
 * Unit tests for the Manage String List dialog.
 * 
 * @author Jakob Kragelund
 * 
 */
public final class ManageStringListDialogTest {

	/**
	 * The dialog under test.
	 */
	private ManageStringListDialog dialog;

	/**
	 * Sets up any initial things before test cases are run.
	 * 
	 * @throws Exception
	 *             If any exceptions happen during startup.
	 */
	@Before
	public void setUp() throws Exception {
		dialog = new ManageStringListDialog(Display.getCurrent()
				.getActiveShell(), new ArrayList<String>());
		dialog.setBlockOnOpen(false);
		dialog.open();
	}

	/**
	 * Clear up after all tests have run.
	 * 
	 * @throws Exception
	 *             If any exceptions happen during cleanup.
	 */
	@After
	public void tearDown() throws Exception {
		dialog.close();
	}

	/**
	 * Test that the dialog contains the list widget for displaying strings.
	 */
	@Test
	public void testDialogContainsStringList() {
		assertNotNull(dialog.stringList);
	}

	/**
	 * Test that the dialog contains the text-field for specifying a new string.
	 */
	@Test
	public void testDialogContainsNewStringText() {
		assertNotNull(dialog.newStringText);
	}

	/**
	 * Test that the dialog contains a button for removing a string from the
	 * list.
	 */
	@Test
	public void testDialogContainsRemoveStringButton() {
		assertNotNull(dialog.removeStringButton);
	}

	/**
	 * Test that the dialog contains a button for adding a new string to the
	 * list.
	 */
	@Test
	public void testDialogContainsAddStringButton() {
		assertNotNull(dialog.addStringButton);
	}

	/**
	 * Assert that the string list is empty initially.
	 */
	@Test
	public void testStringListInitiallyEmpty() {
		assertEquals(0, dialog.stringList.getItemCount());
	}

	/**
	 * Assert that the new string text field is empty initially.
	 */
	@Test
	public void testNewStringTextInitiallyEmpty() {
		assertTrue(dialog.newStringText.getText().isEmpty());
	}

	/**
	 * Assert that the Add String button is disabled initially.
	 */
	@Test
	public void testAddStringButtonInitiallyDisabled() {
		assertFalse(dialog.addStringButton.isEnabled());
	}

	/**
	 * Assert that the Remove String button is disabled initially.
	 */
	@Test
	public void testRemoveStringButtonInitiallyDisabled() {
		assertFalse(dialog.removeStringButton.isEnabled());
	}

	/**
	 * Assert that the Add String button gets enabled when text is entered in
	 * the New String text field.
	 */
	@Test
	public void testAddStringButtonEnabledWhenNewStringEntered() {
		dialog.newStringText.setText("new text");
		dialog.newStringText.notifyListeners(SWT.Modify, null);

		assertTrue(dialog.addStringButton.isEnabled());
	}

	/**
	 * Assert that the Add String button is disabled if the New String text
	 * field only contains spaces.
	 */
	@Test
	public void testAddStringButtonDisabledWhenNewStringIsSpaces() {
		dialog.newStringText.setText("  ");
		dialog.newStringText.notifyListeners(SWT.Modify, null);

		assertFalse(dialog.addStringButton.isEnabled());
	}

	/**
	 * Assert that when the dialog is opened with a list of strings, those
	 * strings are displayed in the list widget.
	 */
	@Test
	public void testOpenDialogWith3Strings() {
		List<String> stringList = new ArrayList<String>();
		stringList.add("string1");
		stringList.add("string2");
		stringList.add("string3");

		ManageStringListDialog tempDialog = new ManageStringListDialog(Display
				.getCurrent().getActiveShell(), stringList);
		tempDialog.setBlockOnOpen(false);
		tempDialog.open();

		assertEquals(stringList.size(), tempDialog.stringList.getItemCount());
		for (int i = 0; i < stringList.size(); i++) {
			assertEquals(stringList.get(i), tempDialog.stringList.getItem(i));
		}

		tempDialog.close();
	}

	/**
	 * Assert that when a string is selected in the list, the Remove String
	 * button is enabled.
	 */
	@Test
	public void testRemoveStringButtonEnabledWhenStringSelected() {
		List<String> stringList = new ArrayList<String>();
		stringList.add("string1");
		stringList.add("string2");
		stringList.add("string3");

		ManageStringListDialog tempDialog = new ManageStringListDialog(Display
				.getCurrent().getActiveShell(), stringList);
		tempDialog.setBlockOnOpen(false);
		tempDialog.open();

		tempDialog.stringList.select(0);
		tempDialog.stringList.notifyListeners(SWT.Selection, null);
		assertEquals(1, tempDialog.stringList.getSelectionCount());

		assertTrue(tempDialog.removeStringButton.isEnabled());

		tempDialog.close();
	}

	/**
	 * Assert that when a string is selected, and the Remove String button is
	 * pressed, the string is removed from the list widget.
	 */
	@Test
	public void testRemoveButtonRemovesSelectedString() {
		List<String> stringList = new ArrayList<String>();
		stringList.add("string1");
		stringList.add("string2");
		stringList.add("string3");

		ManageStringListDialog tempDialog = new ManageStringListDialog(Display
				.getCurrent().getActiveShell(), stringList);
		tempDialog.setBlockOnOpen(false);
		tempDialog.open();

		tempDialog.stringList.select(1);
		tempDialog.stringList.notifyListeners(SWT.Selection, null);

		assertEquals(1, tempDialog.stringList.getSelectionCount());
		assertEquals("string2", tempDialog.stringList.getSelection()[0]);

		tempDialog.removeStringButton.notifyListeners(SWT.Selection, null);

		assertEquals(0, tempDialog.stringList.getSelectionCount());
		assertFalse(tempDialog.removeStringButton.isEnabled());
		assertEquals(2, tempDialog.stringList.getItemCount());
		assertEquals("string1", tempDialog.stringList.getItem(0));
		assertEquals("string3", tempDialog.stringList.getItem(1));

		tempDialog.close();
	}

	/**
	 * Assert that when a string is entered in the New String text-field, and
	 * the Add String button is pressed, the string is added to the list widget.
	 */
	@Test
	public void testAddButtonAddsNewString() {
		List<String> stringList = new ArrayList<String>();
		stringList.add("string1");
		stringList.add("string2");
		stringList.add("string3");

		ManageStringListDialog tempDialog = new ManageStringListDialog(Display
				.getCurrent().getActiveShell(), stringList);
		tempDialog.setBlockOnOpen(false);
		tempDialog.open();

		tempDialog.newStringText.setText("string4");
		tempDialog.newStringText.notifyListeners(SWT.Modify, null);

		tempDialog.addStringButton.notifyListeners(SWT.Selection, null);

		assertEquals(4, tempDialog.stringList.getItemCount());
		// Check that the original items weren't modified
		assertEquals("string1", tempDialog.stringList.getItem(0));
		assertEquals("string2", tempDialog.stringList.getItem(1));
		assertEquals("string3", tempDialog.stringList.getItem(2));
		assertEquals("string4", tempDialog.stringList.getItem(3));

		// Verify that the text field is emptied
		assertTrue(tempDialog.newStringText.getText().isEmpty());
		assertFalse(tempDialog.addStringButton.isEnabled());

		tempDialog.close();
	}
}
