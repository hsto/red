package dk.dtu.imm.red.specificationelements.glossary.ui.dialogs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Display;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the Manage String List dialog presenter.
 * 
 * @author Jakob Kragelund
 * 
 */
public final class ManageStringListDialogPresenterTest {

	/**
	 * The presenter under test.
	 */
	private ManageStringListDialogPresenter presenter;

	/**
	 * The dialog for the presenter.
	 */
	private ManageStringListDialog dialog;

	/**
	 * Sets up any initial things before test cases are run.
	 * 
	 * @throws Exception
	 *             If any exceptions happen during startup.
	 */
	@Before
	public void setUp() throws Exception {
		dialog = new ManageStringListDialog(Display.getCurrent()
				.getActiveShell(), new ArrayList<String>());
		dialog.setBlockOnOpen(false);
		dialog.open();
		presenter = dialog.getPresenter();
	}

	/**
	 * Clear up after all tests have run.
	 * 
	 * @throws Exception
	 *             If any exceptions happen during cleanup.
	 */
	@After
	public void tearDown() throws Exception {
		dialog.close();
	}

	@Test
	public void testContentsInitiallyEmpty() {
		assertTrue(presenter.stringList.isEmpty());
	}

	@Test
	public void testAddStringToEmptyList() {
		dialog.newStringText.setText("text");
		presenter.addStringButtonPressed();

		assertEquals(1, presenter.stringList.size());
		assertEquals("text", presenter.stringList.get(0));
	}

	@Test
	public void testAddStringToNonEmptyList() {
		presenter.stringList.add("originaltext");

		dialog.newStringText.setText("newtext");
		presenter.addStringButtonPressed();

		assertEquals(2, presenter.stringList.size());
		assertEquals("originaltext", presenter.stringList.get(0));
		assertEquals("newtext", presenter.stringList.get(1));
	}

	@Test
	public void testRemoveLastString() {
		presenter.stringList.add("originaltext");
		dialog.stringList.add("originaltext");
		dialog.stringList.setSelection(0);

		presenter.removeStringButtonPressed();

		assertTrue(presenter.stringList.isEmpty());
	}

	@Test
	public void testRemoveNotLastString() {
		presenter.stringList.add("originaltext");
		dialog.stringList.add("originaltext");
		presenter.stringList.add("originaltext2");
		dialog.stringList.add("originaltext2");

		dialog.stringList.setSelection(0);

		presenter.removeStringButtonPressed();

		assertEquals(1, presenter.stringList.size());
		assertEquals("originaltext2", presenter.stringList.get(0));
	}
}
