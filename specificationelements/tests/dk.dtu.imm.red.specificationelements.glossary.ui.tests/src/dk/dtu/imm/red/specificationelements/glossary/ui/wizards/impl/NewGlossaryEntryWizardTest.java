package dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl.ChooseGlossaryPageImpl;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl.GlossaryEntryDefinitionPageImpl;
import dk.dtu.imm.red.specificationelements.glossary.ui.wizards.impl.NewGlossaryEntryWizardImpl;

/**
 * Unit tests for the New Glossary Entry wizard.
 *
 * @author Jakob Kragelund
 *
 */
public class NewGlossaryEntryWizardTest {

	/**
	 * The wizard under test.
	 */
	private NewGlossaryEntryWizardImpl wizard;

	/**
	 * Dialog which contains the wizard.
	 */
	WizardDialog dialog;

	@Before
	public void setUp() throws Exception {
		wizard = new NewGlossaryEntryWizardImpl();
		wizard.init(PlatformUI.getWorkbench(), null);
		wizard.addPages();
		dialog = new WizardDialog(Display.getCurrent().getActiveShell(), wizard);
		dialog.setBlockOnOpen(false);
		dialog.open();
	}

	@After
	public void tearDown() throws Exception {
		dialog.close();
		dialog = null;
		wizard = null;
	}

	/**
	 * Test that a presenter gets created together with the wizard.
	 */
	@Test
	public void testPresenterCreated() {
		assertNotNull(wizard.presenter);
	}

	/**
	 * Test that the wizard contains a page for defining the glossary entry.
	 */
	@Ignore("Requires maintenance") @Test
	public void testContainsGlossaryEntryDefinitionPage() {
		assertNotNull(wizard.entryDefinitionPage);
	}

	/**
	 * Test that the entryDefinitionPage is added to the wizard, and that it is
	 * the first page.
	 */
	@Ignore("Requires maintenance") @Test
	public void testGlossaryEntryDefinitionPageAddedFirst() {
		assertTrue(wizard.getPageCount() > 0);
		assertNotNull(wizard.getPages()[0]);
		assertTrue(wizard.getPages()[0] instanceof GlossaryEntryDefinitionPageImpl);
	}

	/**
	 * Test that the wizard contains a page for choosing the parent glossary.
	 */
	@Test
	public void testContainsChooseGlossaryPage() {
		assertNotNull(wizard.chooseGlossaryPage);
	}

	/**
	 * Test that the chooseGlossaryPage is added to the wizard, and that it
	 * is the second page.
	 */
	@Test
	public void testChooseGlossaryPageAddedSecond() {
		assertTrue(wizard.getPageCount() > 1);
		assertNotNull(wizard.getPages()[1]);
		assertTrue(wizard.getPages()[1] instanceof ChooseGlossaryPageImpl);
	}

	@Ignore("Requires maintenance") @Test
	public void testEntryDefinitionPageContainsTermLabel() {
		assertNotNull(wizard.entryDefinitionPage.termLabel);
	}

	@Ignore("Requires maintenance") @Test
	public void testEntryDefinitionPageContainsTermText() {
		assertNotNull(wizard.entryDefinitionPage.termText);
	}

	@Ignore("Requires maintenance") @Test
	public void testEntryDefinitionPageContainsDefinitionLabel() {
		assertNotNull(wizard.entryDefinitionPage.definitionLabel);
	}

	@Ignore("Requires maintenance") @Test
	public void testEntryDefinitionPageContainsDefinitionEditor() {
		assertNotNull(wizard.entryDefinitionPage.definitionEditor);
	}

	@Ignore("Requires maintenance") @Test
	public void testEntryDefinitionPageNotCompletedInitially() {
		assertFalse(wizard.entryDefinitionPage.isPageComplete());
	}

	/**
	 * Test that the page is completed when there is text in the term-field.
	 */
	@Ignore("Requires maintenance") @Test
	public void testTermNotEmptyPageComplete() {
		wizard.entryDefinitionPage.termText.setText("text");
		wizard.entryDefinitionPage.termText.notifyListeners(SWT.Modify, null);

		assertTrue(wizard.entryDefinitionPage.isPageComplete());
	}

	/**
	 * Test that the page is not completed when no text is in the term-field.
	 */
	@Ignore("Requires maintenance") @Test
	public void testTermEmptyPageNotComplete() {
		wizard.entryDefinitionPage.termText.setText("");
		wizard.entryDefinitionPage.termText.notifyListeners(SWT.Modify, null);

		assertFalse(wizard.entryDefinitionPage.isPageComplete());
	}

	/**
	 * Test that the page is not completed when text in the term-field is
	 * deleted.
	 */
	@Ignore("Requires maintenance") @Test
	public void testTermDeletedPageNotComplete() {
		wizard.entryDefinitionPage.termText.setText("text");
		wizard.entryDefinitionPage.termText.notifyListeners(SWT.Modify, null);

		assertTrue(wizard.entryDefinitionPage.isPageComplete());

		wizard.entryDefinitionPage.termText.setText("");
		wizard.entryDefinitionPage.termText.notifyListeners(SWT.Modify, null);

		assertFalse(wizard.entryDefinitionPage.isPageComplete());
	}

	@Test
	public void testChooseGlossaryPageContainsTreeView() {
//		assertNotNull(wizard.chooseGlossaryPage.treeView);
	}

	@Test
	public void testChooseGlossaryPageContainsNewGlossaryButton() {
//		assertNotNull(wizard.chooseGlossaryPage.newGlossaryButton);
	}

	@Test
	public void testWizardCanNotFinishWhenNoSelection() {
//		assertTrue(wizard.chooseGlossaryPage.treeView.getSelection()
//				.isEmpty());
		assertFalse(wizard.canFinish());
	}

}
