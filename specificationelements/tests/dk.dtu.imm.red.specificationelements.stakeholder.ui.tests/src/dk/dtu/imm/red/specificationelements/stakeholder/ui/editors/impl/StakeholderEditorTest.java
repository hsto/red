package dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.impl;

import static org.junit.Assert.assertNotNull;

import org.eclipse.ui.PlatformUI;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.stakeholder.StakeholderFactory;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.StakeholderEditor;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.impl.StakeholderEditorImpl;
import dk.dtu.imm.red.specificationelements.stakeholder.ui.editors.impl.StakeholderEditorInputImpl;

/**
 * Unit test for the stakeholder editor
 * 
 * @author Anders Friis
 * 
 */
@Ignore("Requires maintenance") 
public class StakeholderEditorTest {

	private Stakeholder stakeholder;
	private StakeholderEditorInputImpl editorInput;
	private StakeholderEditorImpl editor;

	@Before
	public void setUp() throws Exception {
		stakeholder = StakeholderFactory.eINSTANCE.createStakeholder();
		editorInput = new StakeholderEditorInputImpl(stakeholder);

		editor = (StakeholderEditorImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.openEditor(editorInput, StakeholderEditor.ID);
	}

	/**
	 * Tests that there exists a label for the name text box
	 */
	@Test
	public void testNameLabelExists() {

//		assertNotNull(editor.nameLabel);
	}

	/**
	 * Tests that there exists a text box for the name of the stakeholder
	 */
	@Test
	public void testNameTextBoxExists() {

//		assertNotNull(editor.nameTextBox);
	}

	/**
	 * Tests that there exists a label for the type combo box
	 */
	@Test
	public void testTypeLabelExists() {

//		assertNotNull(editor.typeLabel);
	}

	/**
	 * Tests that there exists a combo box for type
	 */
	@Test
	public void testTypeComboBoxExists() {

//		assertNotNull(editor.typeComboBox);
	}

	/**
	 * Tests that there exists a label for the exposure combo box
	 */
	@Test
	public void testExposureLabelExists() {

		assertNotNull(editor.exposureLabel);
	}

	/**
	 * Tests that there exists a combo box for exposure
	 */
	@Test
	public void testExposureComboBoxExists() {

		assertNotNull(editor.exposureComboBox);
	}

	/**
	 * Tests that there exists a label for the power combo box
	 */
	@Test
	public void testPowerLabelExists() {

		assertNotNull(editor.powerLabel);
	}

	/**
	 * Tests that there exists a combo box for power
	 */
	@Test
	public void testPowerComboBoxExists() {

		assertNotNull(editor.powerComboBox);
	}

	/**
	 * Tests that there exists a label for the urgency combo box
	 */
	@Test
	public void testUrgencyLabelExists() {

		assertNotNull(editor.urgencyLabel);
	}

	/**
	 * Tests that there exists a combo box for urgency
	 */
	@Test
	public void testUrgencyComboBoxExists() {

		assertNotNull(editor.urgencyComboBox);
	}

	/**
	 * Tests that there exists a label for the importance combo box
	 */
	@Test
	public void testImportanceLabelExists() {

		assertNotNull(editor.importanceLabel);
	}

	/**
	 * Tests that there exists an editor for the description rich text box
	 */
	@Test
	public void testDescriptionEditorExists() {

		assertNotNull(editor.longDescriptionEditor);
	}

	/**
	 * Tests that there exists an editor for the stake rich text box
	 */
	@Test
	public void testStakeEditorExists() {

		assertNotNull(editor.stakeEditor);
	}

	/**
	 * Tests that there exists an editor for the engagement rich text box
	 */
	@Test
	public void testEngagementEditorExists() {

		assertNotNull(editor.engagementEditor);
	}
}
