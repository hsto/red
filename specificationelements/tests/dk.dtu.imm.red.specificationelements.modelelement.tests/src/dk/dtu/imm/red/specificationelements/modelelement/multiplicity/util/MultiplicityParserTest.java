package dk.dtu.imm.red.specificationelements.modelelement.multiplicity.util;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.CompoundMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.RangedMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.SingleValueMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.UnparsedMultiplicity;


public class MultiplicityParserTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSingleValue() {
		String input = "1";
		int expected = 1;
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(SingleValueMultiplicity.class));
		assertEquals(expected, ((SingleValueMultiplicity) result).getValue());
	}

	@Test
	public void testSingleValueUnbounded() {
		String input = "*";
		String expected = "*";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(SingleValueMultiplicity.class));
		assertEquals(expected, ((SingleValueMultiplicity) result).toString());
		assertTrue(((SingleValueMultiplicity) result).isUnbounded());
	}
	
	@Test
	public void testSingleValueTrimmed() {
		String input = "  12\n";
		String expected = "12";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(SingleValueMultiplicity.class));
		assertEquals(expected, ((SingleValueMultiplicity) result).toString());
	}
	
	@Test
	public void testSingleValueNotInteger() {
		String input = "24aab";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(UnparsedMultiplicity.class));
		assertEquals(input, ((UnparsedMultiplicity) result).toString());
	}
	
	@Test
	public void testSingleValueNegative() {
		String input = "-5";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(UnparsedMultiplicity.class));
		assertEquals(input, ((UnparsedMultiplicity) result).toString());
	}
	
	@Test
	public void testRanged() {
		String input = "1..2";
		int expectedMin = 1;
		int expectedMax = 2;
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(RangedMultiplicity.class));
		assertEquals(expectedMin, ((RangedMultiplicity) result).getMinimum());
		assertEquals(expectedMax, ((RangedMultiplicity) result).getMaximum());
	}
	
	@Test
	public void testRangedUnbounded() {
		String input = "5..*";
		String expectedString = "5..*";
		int expectedMin = 5;
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(RangedMultiplicity.class));
		assertEquals(expectedString, ((RangedMultiplicity) result).toString());
		assertEquals(expectedMin, ((RangedMultiplicity) result).getMinimum());
		assertTrue(((RangedMultiplicity) result).isUnbounded());
	}
	
	@Test
	public void testRangedTrimmed() {
		String input = "   3\n..\t 14 \r  ";
		String expected = "3..14";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(RangedMultiplicity.class));
		assertEquals(expected, ((RangedMultiplicity) result).toString());
	}
	
	@Test
	public void testRangedLowerUnbounded() {
		String input = "*..3";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(UnparsedMultiplicity.class));
		assertEquals(input, ((UnparsedMultiplicity) result).toString());
	}
	
	@Test
	public void testRangedLowerNegativeInteger() {
		String input = "0..-3";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(UnparsedMultiplicity.class));
		assertEquals(input, ((UnparsedMultiplicity) result).toString());
	}
	
	@Test
	public void testRangedLowerInvalidOperator() {
		String input = "1.,4";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(UnparsedMultiplicity.class));
		assertEquals(input, ((UnparsedMultiplicity) result).toString());
	}
	
	@Test
	public void testCompound() {
		String input = "2, 4..6, 9";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(CompoundMultiplicity.class));
		assertEquals(3, ((CompoundMultiplicity) result).getParts().size());
		
		List<Multiplicity> resultParts = ((CompoundMultiplicity) result).getParts();
		
		assertThat(resultParts.get(0), instanceOf(SingleValueMultiplicity.class));
		assertThat(resultParts.get(1), instanceOf(RangedMultiplicity.class));
		assertThat(resultParts.get(2), instanceOf(SingleValueMultiplicity.class));
	}
	
	@Test
	public void testCompoundInvalidOperator() {
		String input = "1; 3";
		
		Multiplicity result = MultiplicityParser.parse(input);
		
		assertThat(result, instanceOf(UnparsedMultiplicity.class));
		assertEquals(input, ((UnparsedMultiplicity) result).toString());
	}
}


