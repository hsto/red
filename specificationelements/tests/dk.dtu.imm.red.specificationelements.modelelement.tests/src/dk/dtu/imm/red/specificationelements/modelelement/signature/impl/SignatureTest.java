package dk.dtu.imm.red.specificationelements.modelelement.signature.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypeFactory;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.util.DataTypeParser;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.util.MultiplicityParser;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureFactory;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem;


public class SignatureTest {
	private Signature signature;
	private SignatureItem signatureItem;

	@Before
	public void setUp() throws Exception {
		signature = SignatureFactory.eINSTANCE.createSignature();
		signatureItem = SignatureFactory.eINSTANCE.createSignatureItem();
		
		signatureItem.setName("Some name");
		signatureItem.setType(DataTypeParser.parse("int"));
		signatureItem.setMultiplicity(MultiplicityParser.parse("1"));
	}

	@Test
	public void testCheckNoItems() {
		signature.checkStructure();
		int numberofCommentsWithIssue = signature.getCommentlist().getComments().size();
		
		signature.getItems().add(signatureItem);
		signature.checkStructure();
		int numberofCommentsWithoutIssue = signature.getCommentlist().getComments().size();
		
		assertEquals(numberofCommentsWithIssue - 1, numberofCommentsWithoutIssue);
	}

	@Test
	public void testCheckUnparsedType() {
		signature.getItems().add(signatureItem);
		
		signature.checkStructure();
		int numberofCommentsWithoutIssue = signature.getCommentlist().getComments().size();

		signatureItem.setType(DataTypeParser.parse("int list [5]"));
		signature.checkStructure();
		int numberofCommentsWithIssue = signature.getCommentlist().getComments().size();
		
		assertEquals(numberofCommentsWithoutIssue + 1, numberofCommentsWithIssue);
	}

	@Test
	public void testCheckUnparsedMultiplicity() {
		signature.getItems().add(signatureItem);
		
		signature.checkStructure();
		int numberofCommentsWithoutIssue = signature.getCommentlist().getComments().size();

		signatureItem.setMultiplicity(MultiplicityParser.parse("word"));
		signature.checkStructure();
		int numberofCommentsWithIssue = signature.getCommentlist().getComments().size();
		
		assertEquals(numberofCommentsWithoutIssue + 1, numberofCommentsWithIssue);
	}

	@Test
	public void testCheckNoName() {
		signatureItem.setName("");
		signature.getItems().add(signatureItem);
		
		signature.checkStructure();
		int numberofCommentsWithIssue = signature.getCommentlist().getComments().size();
		
		signatureItem.setName("A name");
		signature.checkStructure();
		int numberofCommentsWithoutIssue = signature.getCommentlist().getComments().size();
		
		assertEquals(numberofCommentsWithIssue - 1, numberofCommentsWithoutIssue);
	}

	@Test
	public void testCheckNoType() {
		signatureItem.setType(DatatypeFactory.eINSTANCE.createNoType());
		signature.getItems().add(signatureItem);
		
		signature.checkStructure();
		int numberofCommentsWithIssue = signature.getCommentlist().getComments().size();

		signatureItem.setType(DataTypeParser.parse("bool"));
		signature.checkStructure();
		int numberofCommentsWithoutIssue = signature.getCommentlist().getComments().size();
		
		assertEquals(numberofCommentsWithIssue - 1, numberofCommentsWithoutIssue);
	}
}


