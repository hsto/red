package dk.dtu.imm.red.specificationelements.modelelement.datatype.util;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.ArrayType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.BaseType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.ChoiceType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DataStructureType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.FunctionType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.NestedType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.TupleType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.TypeVariable;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.util.DataTypeParser;


public class DataTypeParserTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testBaseType() {
		String input = "int";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(BaseType.class));
		assertEquals(input, ((BaseType) result).getName());
	}

	@Test
	public void testBaseTypeTrimmed() {
		String input = "  bool \n \t";
		String expected = "bool";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(BaseType.class));
		assertEquals(expected, ((BaseType) result).getName());
	}

	@Ignore @Test //TODO: Fails because unknown identifiers are interpreted as Signature names  
	public void testBaseTypeUnknown() {
		String input = "notvalid";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(UnparsedType.class));
		assertEquals(input, ((UnparsedType) result).getDefinition());
	}

	@Ignore @Test //TODO: Implement when Signature referencing works
	public void testSignature() {
	}

	@Test
	public void testTypeVariable() {
		String input = "'t";
		String expected = "t";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(TypeVariable.class));
		assertEquals(expected, ((TypeVariable) result).getName());
	}

	@Test
	public void testChoice() {
		String input = "{ON,OFF,AUTO} choice";
		String[] expected = {"ON", "OFF", "AUTO"};
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(ChoiceType.class));
		assertArrayEquals(expected, ((ChoiceType) result).getOptions().toArray());
	}

	@Test
	public void testDataStructure() {
		String input = "float list";
		String expectedBaseType = "float";
		String expectedDataStructure = "list";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(DataStructureType.class));
		DataStructureType resultDataStructure = (DataStructureType) result;
		
		assertEquals(expectedDataStructure, resultDataStructure.getStructureName());
		
		assertThat(resultDataStructure.getElementType(), instanceOf(BaseType.class));
		assertEquals(expectedBaseType, ((BaseType)resultDataStructure.getElementType()).getName());
	}

	@Test
	public void testDataStructureNested() {
		String input = "string opt set";
		String expectedInnerDataStructure = "opt";
		String expectedOuterDataStructure = "set";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(DataStructureType.class));
		DataStructureType resultDataStructure = (DataStructureType) result;
		
		assertEquals(expectedOuterDataStructure, resultDataStructure.getStructureName());
		
		assertThat(resultDataStructure.getElementType(), instanceOf(DataStructureType.class));
		assertEquals(expectedInnerDataStructure, ((DataStructureType)resultDataStructure.getElementType()).getStructureName());
	}

	@Test
	public void testArray() {
		String input = "unit array [2,3]";
		String expectedBaseType = "unit";
		Integer[] expectedDimensions = {2, 3};
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(ArrayType.class));
		ArrayType resultArray = (ArrayType) result;
		
		assertArrayEquals(expectedDimensions, resultArray.getDimensions().toArray());
		
		assertThat(resultArray.getElementType(), instanceOf(BaseType.class));
		assertEquals(expectedBaseType, ((BaseType)resultArray.getElementType()).getName());
	}

	@Test
	public void testArrayNoDimensions() {
		String input = "int array";
		
		DataType result = DataTypeParser.parse(input);

		assertThat(result, instanceOf(UnparsedType.class));
		assertEquals(input, ((UnparsedType)result).getDefinition());
	}

	@Test
	public void testTuple() {
		String input = "int * 'f * string list";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(TupleType.class));
		TupleType resultTuple = (TupleType) result;
		
		assertEquals(3, resultTuple.getElementTypes().size());
		assertThat(resultTuple.getElementTypes().get(0), instanceOf(BaseType.class));
		assertThat(resultTuple.getElementTypes().get(1), instanceOf(TypeVariable.class));
		assertThat(resultTuple.getElementTypes().get(2), instanceOf(DataStructureType.class));
	}

	@Test
	public void testFunction() {
		String input = "unit opt -> bool";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(FunctionType.class));
		FunctionType resultFunction = (FunctionType) result;
		
		assertThat(resultFunction.getInput(), instanceOf(DataStructureType.class));
		assertThat(resultFunction.getOutput(), instanceOf(BaseType.class));
	}

	@Test
	public void testFunctionTuplePrecedence() {
		String input = "int * float set -> bool array [4,124,21]";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(FunctionType.class));
		FunctionType resultFunction = (FunctionType) result;
		
		assertThat(resultFunction.getInput(), instanceOf(TupleType.class));
		assertThat(resultFunction.getOutput(), instanceOf(ArrayType.class));
	}

	@Test
	public void testParenthesisWithTupleFunction() {
		String input = "int * (bool -> float)";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(TupleType.class));
		TupleType resultTuple = (TupleType) result;
		
		assertEquals(2, resultTuple.getElementTypes().size());
		assertThat(resultTuple.getElementTypes().get(0), instanceOf(BaseType.class));
		assertThat(resultTuple.getElementTypes().get(1), instanceOf(NestedType.class));
		
		NestedType resultNested = (NestedType) resultTuple.getElementTypes().get(1);
		assertThat(resultNested.getContainedType(), instanceOf(FunctionType.class));
	}

	@Test
	public void testParenthesisWithTupleArray() {
		String input = "(int * float) array [10]";
		
		DataType result = DataTypeParser.parse(input);
		
		assertThat(result, instanceOf(ArrayType.class));
		ArrayType resultArray = (ArrayType) result;
		
		assertThat(resultArray.getElementType(), instanceOf(NestedType.class));
		
		NestedType resultNested = (NestedType) resultArray.getElementType();
		assertThat(resultNested.getContainedType(), instanceOf(TupleType.class));
	}
}


