package dk.dtu.imm.red.specificationelements.modelelement.signature.impl;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.NoType;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.NoMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureFactory;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureKind;


public class SignatureFactoryTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSignatureInitialization() {
		String expected = SignatureKind.TYPE.getName();
		
		Signature signature = SignatureFactory.eINSTANCE.createSignature();
		
		assertEquals(expected, signature.getElementKind());
	}

	@Test
	public void testSignatureItemInitialization() {
		SignatureItem signatureItem = SignatureFactory.eINSTANCE.createSignatureItem();
		
		assertThat(signatureItem.getType(), instanceOf(NoType.class));
		assertThat(signatureItem.getMultiplicity(), instanceOf(NoMultiplicity.class));
	}
}


