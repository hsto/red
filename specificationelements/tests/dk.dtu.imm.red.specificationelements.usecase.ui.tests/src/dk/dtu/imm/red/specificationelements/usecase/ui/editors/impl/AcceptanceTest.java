package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;

import static org.junit.Assert.assertEquals;

import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.UsecaseFactory;
import dk.dtu.imm.red.specificationelements.usecase.UseCaseKind;
import dk.dtu.imm.red.specificationelements.usecase.ui.editors.UseCaseEditor;

public class AcceptanceTest {

	private UseCase usecase;
	private UseCaseEditorInputImpl editorInput;
	private UseCaseEditorImpl editorImpl;
	private UseCasePresenterImpl presenter;

	@Before
	public void setUp() throws Exception {

		
		createTestUseCase();
		editorInput = new UseCaseEditorInputImpl(usecase);

		editorImpl = (UseCaseEditorImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.openEditor(editorInput, UseCaseEditor.ID);
		presenter = (UseCasePresenterImpl) editorImpl.getPresenter();
	}
	
	private UseCase createTestUseCase() {
		usecase = UsecaseFactory.eINSTANCE.createUseCase();
		
		//Some test use case, create user
		usecase.setElementKind(UseCaseKind.SYSTEM_USE_CASE.getLiteral());
		usecase.setName("Test name"); 
		return usecase;
	}

	@After
	public void tearDown() throws Exception {
		
	} 
	
	private void setupTestUsecase() {
		presenter.setKind(UseCaseKind.BUSINESS_PROCESS.getLiteral());
		presenter.setLongDescription("Test description");
		presenter.setTrigger("Test trigger");
		presenter.setParameter("Test parameter");
		presenter.setResult("Test result"); 
		
		//Set preconditions  
		//presenter.addPreCondition();
		presenter.getViewModel().setSelectedPreConditionIndex(0);
		presenter.setPreCondition("Test precondition 1"); 
		
	    //Set and postcondition 
		//presenter.addPostCondition();
		presenter.getViewModel().setSelectedPostConditionIndex(0);
		presenter.setPostCondition("Test postcondition 1");  
	}
	
	@Ignore("Requires maintenance") @Test
	public void canSetupUsecase() { 
		//ARRANGE+ACT
		setupTestUsecase();
		
		//ASSERT  
		assertEquals(UseCaseKind.BUSINESS_PROCESS.getLiteral(), usecase.getElementKind());
		assertEquals("Test name", usecase.getName());
		assertEquals("Test description", usecase.getDescription().toString());
		assertEquals("Test trigger", usecase.getTrigger());
		assertEquals("Test parameter", usecase.getParameter());
		assertEquals("Test result", usecase.getResult());
		assertEquals("Test incidence", usecase.getIncidence());  
		assertEquals("Test precondition 1", usecase.getPreConditions().get(0).toString());  
		assertEquals("Test postcondition 1", usecase.getPostConditions().get(0).toString()); 
	}  	
}
