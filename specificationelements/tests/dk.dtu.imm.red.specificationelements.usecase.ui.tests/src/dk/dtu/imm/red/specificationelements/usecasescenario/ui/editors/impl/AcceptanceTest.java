package dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test; 
import org.w3c.dom.Document;

import dk.dtu.imm.red.core.text.TextFactory; 
import dk.dtu.imm.red.specificationelements.usecasescenario.Action;
import dk.dtu.imm.red.specificationelements.usecasescenario.ActionType;
import dk.dtu.imm.red.specificationelements.usecasescenario.Operator;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorType;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.UseCaseScenarioEditor;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.UseCaseScenarioEditorImpl;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.UseCaseScenarioEditorInputImpl;
import dk.dtu.imm.red.specificationelements.usecasescenario.ui.editors.impl.UseCaseScenarioPresenterImpl;

public class AcceptanceTest {

	private UseCaseScenario scenario;
	private UseCaseScenarioEditorInputImpl editorInput;
	private UseCaseScenarioEditorImpl editorImpl;
	private UseCaseScenarioEditor subject;
	private UseCaseScenarioPresenterImpl presenter;

	@Before
	public void setUp() throws Exception { 
		//createTestUcscenario();
		editorInput = new UseCaseScenarioEditorInputImpl(scenario);

		editorImpl = (UseCaseScenarioEditorImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.openEditor(editorInput, UseCaseScenarioEditor.ID);
		presenter = (UseCaseScenarioPresenterImpl) editorImpl.getPresenter();
	}
	
//	private void createTestUcscenario() {
//		ucscenario = UcScenarioFactory.eINSTANCE.createScenario(); 
//		
//		//Some test scenario
//		ucscenario.setName("Test scenario"); 
//		Connector startConnector = UcScenarioFactory.eINSTANCE.createConnection("Start connector", ConnectorType.Action);
//		 
//		ucscenario.setStartConnector(startConnector);
//		
//		startConnector.createAction("Start", ActionType.STA);
//		startConnector.createAction("First action", ActionType.Action);
//		startConnector.createAction("Second action", ActionType.MSG);
//		
//		//Add alternate activity
//		Connector alternate = startConnector.createConnection("Alternate flow", ConnectorType.ALT);
//		alternate.createAction("Alternate action 1", ActionType.MSG);
//		alternate.createAction("Alternate action 2", ActionType.Action);
//		
//		//Add parallel activity
//		Connector parallel = startConnector.createConnection( "Parallel flows", ConnectorType.PAR);
//		Connector parallelSeqOne = parallel.createConnection("Parallel flow 1", ConnectorType.Action);
//		Connector parallelSeqTwo = parallel.createConnection( "Parallel flow 2", ConnectorType.Action); 
//		
//		parallelSeqOne.createAction("Parallel action 1", ActionType.Action);
//		parallelSeqTwo.createAction("Parallel action 2", ActionType.Action);  
//	}  
	
	@After
	public void tearDown() throws Exception {
		
	} 
	
//	@Test
//	public void canCreateIntialModel() {
//		Connector startConnector = ucscenario.getStartConnector();
//		assertConnector(startConnector, "Start connector", ConnectorType.Action);
//		assertAction((Action)startConnector.getConnections().get(0), "Start", ActionType.STA);
//		assertAction((Action)startConnector.getConnections().get(1), "First action", ActionType.Action);
//		assertAction((Action)startConnector.getConnections().get(2), "Second action", ActionType.MSG);
//		
//		//Alternate flow
//		Connector altConnector = (Connector) startConnector.getConnections().get(3);
//		assertConnector(altConnector, "Alternate flow", ConnectorType.ALT);
//		assertAction((Action)altConnector.getConnections().get(0), "Alternate action 1", ActionType.MSG);
//		assertAction((Action)altConnector.getConnections().get(1), "Alternate action 2", ActionType.Action);
//		
//		//Parallel flow
//		Connector parallelConncetor = (Connector) startConnector.getConnections().get(4);
//		assertConnector(parallelConncetor, "Parallel flows", ConnectorType.PAR);
//		Connector parallelConnector1 = (Connector) parallelConncetor.getConnections().get(0);
//		Connector parallelConnector2 = (Connector) parallelConncetor.getConnections().get(1);
//		assertConnector(parallelConnector1, "Parallel flow 1", ConnectorType.Action);
//		assertConnector(parallelConnector2, "Parallel flow 2", ConnectorType.Action);
//		assertAction((Action)parallelConnector1.getConnections().get(0), "Parallel action 1", ActionType.Action);
//		assertAction((Action)parallelConnector2.getConnections().get(0), "Parallel action 2", ActionType.Action);
//	}
//	
//	@Test
//	public void canAddItem() {
//		ScenarioElement se = ucscenario.getStartConnector().getConnections().get(1);
//		
//		//Add action
//		presenter.getViewModel().setCurrentScenarioElement(se);
//		Action a = presenter.addNewScenarioAction(ActionType.Action);
//		presenter.getViewModel().setCurrentScenarioElement(a);
//		presenter.setScenarioDescription("New action");
//		presenter.setType(ActionType.Action); 
//		assertAction((Action) ucscenario.getStartConnector().getConnections().get(2), "New action", ActionType.Action);
//		
//		//Add connector
//		presenter.getViewModel().setCurrentScenarioElement(a);
//		Connector c = presenter.addNewScenarioConnector(ConnectorType.Action);
//		presenter.getViewModel().setCurrentScenarioElement(c);
//		presenter.setScenarioDescription("New sequence");
//		presenter.setType(ConnectorType.Action); 
//		assertConnector((Connector) ucscenario.getStartConnector().getConnections().get(3), "New sequence", ConnectorType.Action);
//	}
//	
//	@Test
//	public void canDeleteItem() {
//		int startElements = ucscenario.getStartConnector().getConnections().size();
//		Connector altConnector = (Connector) ucscenario.getStartConnector().getConnections().get(3);
//		presenter.getViewModel().setCurrentScenarioElement(altConnector);
//		presenter.removeCurrentScenarioElement();
//		assertEquals(startElements - 1, ucscenario.getStartConnector().getConnections().size());
//	}
//	
//	@Test
//	public void canMoveItem() {
//		ScenarioElement se = ucscenario.getStartConnector().getConnections().get(1);
//		presenter.getViewModel().setCurrentScenarioElement(se);
//		presenter.moveScenarioElement(2, presenter.getViewModel().getCurrentScenarioElement(), 
//				presenter.getViewModel().getCurrentScenarioElement().getParent());
//		assertAction((Action) ucscenario.getStartConnector().getConnections().get(1), "Second action", ActionType.MSG);
//		assertAction((Action) ucscenario.getStartConnector().getConnections().get(2), "First action", ActionType.Action);
//		
//	}
//	
//	private void assertConnector(Connector con, String description, ConnectorType type) {
//		assertEquals(description, con.getDescription());
//		assertEquals(type,con.getType());
//	}
//	
//	private void assertAction(Action act, String description, ActionType type) {
//		assertEquals(description,act.getDescription());
//		assertEquals(type,act.getType());
//	}
//	
	
 
}
