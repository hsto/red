package dk.dtu.imm.red.specificationelements.vision.ui.editors.impl;

import static org.junit.Assert.assertNotNull;

import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.specificationelements.vision.VisionFactory;
import dk.dtu.imm.red.specificationelements.vision.ui.editors.VisionEditor;
import dk.dtu.imm.red.specificationelements.vision.ui.editors.impl.VisionEditorImpl;
import dk.dtu.imm.red.specificationelements.vision.ui.editors.impl.VisionEditorInputImpl;

@Ignore("Requires maintenance") 
public class VisionEditorTest {

	private Vision vision;
	private VisionEditorInputImpl editorInput;
	private VisionEditorImpl editor;

	@Before
	public void setUp() throws Exception {

		vision = VisionFactory.eINSTANCE.createVision();
		editorInput = new VisionEditorInputImpl(vision);

		editor = (VisionEditorImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.openEditor(editorInput, VisionEditor.ID);
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Tests that there exists a rich text editor for vision
	 */
	@Test
	public void testVisionEditorExists() {

		assertNotNull(editor.visionEditor);
	}
}
