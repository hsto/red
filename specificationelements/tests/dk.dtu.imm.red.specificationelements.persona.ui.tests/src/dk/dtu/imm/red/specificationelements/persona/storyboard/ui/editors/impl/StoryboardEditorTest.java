package dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.impl;

import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;

import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardFactory;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.StoryboardEditor;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.StoryboardEditorInput;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.impl.StoryboardEditorImpl;
import dk.dtu.imm.red.specificationelements.persona.storyboard.ui.editors.impl.StoryboardEditorInputImpl;


public class StoryboardEditorTest {

	private Storyboard storyboard;
	private StoryboardEditorInput editorInput;
	private StoryboardEditorImpl editor;
	
	@Before
	public void setUp() throws Exception {
		storyboard = StoryboardFactory.eINSTANCE.createStoryboard();
		editorInput = new StoryboardEditorInputImpl(storyboard);

		editor = (StoryboardEditorImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.openEditor(editorInput, StoryboardEditor.ID);

	}

	@After
	public void tearDown() throws Exception {
	}
	
}
