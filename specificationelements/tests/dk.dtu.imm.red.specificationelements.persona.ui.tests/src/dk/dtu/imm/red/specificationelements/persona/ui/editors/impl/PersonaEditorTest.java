package dk.dtu.imm.red.specificationelements.persona.ui.editors.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.PersonaFactory;
import dk.dtu.imm.red.specificationelements.persona.ui.editors.PersonaEditor;
import dk.dtu.imm.red.specificationelements.persona.ui.editors.impl.PersonaEditorImpl;
import dk.dtu.imm.red.specificationelements.persona.ui.editors.impl.PersonaEditorInputImpl;

/**
 * Unit test for the persona editor
 * 
 * @author Anders Friis
 * 
 */

public class PersonaEditorTest {

	private Persona persona;
	private PersonaEditorInputImpl editorInput;
	private PersonaEditorImpl editor;

	@Before
	public void setUp() throws Exception {
		persona = PersonaFactory.eINSTANCE.createPersona();
		editorInput = new PersonaEditorInputImpl(persona);

		editor = (PersonaEditorImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.openEditor(editorInput, PersonaEditor.ID);
	}

	@After
	public void tearDown() throws Exception {
		PlatformUI.getWorkbench()
		.getActiveWorkbenchWindow().getActivePage().closeEditor(editor, false);
	}

	/**
	 * Tests that there exists a text box for name
	 */
	@Ignore("Requires maintenance") @Test
	public void testNameTextExists() {

		assertNotNull(editor.nameTextBox);
	}

	/**
	 * Tests that there exists a text box to enter name of the persona.
	 */
	@Ignore("Requires maintenance") @Test
	public void testNameMaxLengthReplace() {

		editor.nameTextBox.setText("Below 40");

		assertEquals("Below 40", editor.nameTextBox.getText());
		// //////////////////////////1234567890123456789012345678901234567890123
		editor.nameTextBox
				.setText("This message is above forty characters long");
		// ////////////1234567890123456789012345678901234567890
		assertEquals("This message is above forty characters l",
				editor.nameTextBox.getText());
	}

	/**
	 * Tests that the max length for the nameTextBox is obeyed.
	 */
	@Ignore("Requires maintenance") @Test
	public void testNameMaxLengthInsertLast() {
		String shortMessage = generateStringWithLength(PersonaEditorPresenterImpl.MAX_NAME_LENGTH);
		String append = ". A little longer";

		editor.nameTextBox.setText(shortMessage);
		assertEquals(shortMessage, editor.nameTextBox.getText());
		editor.nameTextBox.append(append);
		assertEquals(shortMessage, editor.nameTextBox.getText());
	}

	/**
	 * Tests that there is a text box to enter occupation of the persona.
	 */
	@Test
	public void testOccupationTextExists() {

		assertNotNull(editor.occupationTextBox);
	}

	/**
	 * Tests that the max length for the occupationTextBox is obeyed.
	 */
	@Test
	public void testOccupationMaxLengthInsertLast() {
		String shortMessage = generateStringWithLength(PersonaEditorPresenterImpl.MAX_OCCUPATION_LENGTH);
		String append = ". A little longer";

		editor.occupationTextBox.setText(shortMessage);
		assertEquals(shortMessage, editor.occupationTextBox.getText());
		
		editor.occupationTextBox.append(append);
		assertEquals(shortMessage, editor.occupationTextBox.getText());
	}
	
	private String generateStringWithLength(int length) {
		StringBuilder outputBuffer = new StringBuilder(length);
		for (int i = 0; i < length; i++){
		   outputBuffer.append("x");
		}
		return outputBuffer.toString();
	}

	/**
	 * Tests that there exists a field to enter age of the persona.
	 */
	@Test
	public void testAgeTextExists() {

		assertNotNull(editor.ageTextBox);
	}

	/**
	 * Tests that it is not possible to enter something other than numbers
	 */
	@Test
	public void testAgeNumberOnlyInsertLast() {
		String numberMessage = "21";
		String letterMessage = "Tw";

		editor.ageTextBox.setText(letterMessage);
		assertEquals("", editor.ageTextBox.getText());
		editor.ageTextBox.setText(numberMessage);
		assertEquals(numberMessage, editor.ageTextBox.getText());

	}

	/**
	 * Tests that the max length of age text box is set to two characters
	 */
	@Test
	public void testAgeMaxLengthInsertLast() {
		String shortNumber = "18";
		String longNumber = "121";

		editor.ageTextBox.setText(shortNumber);
		assertEquals(shortNumber, editor.ageTextBox.getText());
		editor.ageTextBox.setText(longNumber);
		assertEquals("12", editor.ageTextBox.getText());
	}

	// TODO add tests that: tests the size of the canvas, tests that the canvas
	// recieves an image

	/**
	 * Tests that there exists a label for name
	 */
	@Ignore("Requires maintenance") @Test
	public void testNameLabelExists() {
		assertNotNull(editor.nameLabel);
	}

	/**
	 * Tests that the name labels text is set
	 */
	@Ignore("Requires maintenance") @Test
	public void testNameLabelTextSet() {
		assertEquals("Name", editor.nameLabel.getText());
	}

	/**
	 * Tests that there exists a label for occupation
	 */
	@Test
	public void testOccupationLabelExists() {
		assertNotNull(editor.occupationLabel);
	}

	/**
	 * Tests that the occupation labels text is set
	 */
	@Test
	public void testOccupationLabelTextSet() {
		assertEquals("Occupation", editor.occupationLabel.getText());
	}

	/**
	 * Tests that there exists a label for age
	 */
	@Test
	public void testAgeLabelExists() {
		assertNotNull(editor.ageLabel);
	}

	/**
	 * Tests that the age labels text is set
	 */
	@Test
	public void testAgeLabelTextSet() {
		assertEquals("Age", editor.ageLabel.getText());
	}

}
