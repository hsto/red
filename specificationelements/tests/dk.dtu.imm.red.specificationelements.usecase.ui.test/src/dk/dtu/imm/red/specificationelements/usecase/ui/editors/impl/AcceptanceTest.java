package dk.dtu.imm.red.specificationelements.usecase.ui.editors.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test; 
import org.w3c.dom.Document;

import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.specificationelements.businessProcesses.organization.Actor;
import dk.dtu.imm.red.specificationelements.businessProcesses.organization.OrganizationFactory;
import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.ActorRelation;
import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.ActorType;
import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.Scenario;
import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.ScenarioExtension;
import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.ScenarioStep;
import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.Usecase;
import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.UsecaseFactory;
import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.UsecaseType;
import dk.dtu.imm.red.specificationelements.businessProcesses.usecase.impl.UsecaseFactoryImpl;
import dk.dtu.imm.red.specificationelements.usecase.ui.editors.UsecaseEditor;

public class AcceptanceTest {

	private Usecase usecase;
	private UsecaseEditorInputImpl editorInput;
	private UsecaseEditorImpl editorImpl;
	private UsecaseEditor subject;
	private UsecasePresenterImpl presenter;

	@Before
	public void setUp() throws Exception {

		
		createTestUsecase();
		editorInput = new UsecaseEditorInputImpl(usecase);

		editorImpl = (UsecaseEditorImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.openEditor(editorInput, UsecaseEditor.ID);
		presenter = (UsecasePresenterImpl) editorImpl.getPresenter();
	}
	
	private Usecase createTestUsecase() {
		usecase = UsecaseFactory.eINSTANCE.createUsecase();
		
		//Some test usecase, create user
		usecase.setType(UsecaseType.SYSTEM_USE_CASE);
		usecase.setName("Test name"); 
		return usecase;
	}

	@After
	public void tearDown() throws Exception {
		
	} 
	
	private void setupTestUsecase() {
		presenter.setIsBusinessProcess();
		presenter.setDescription("Test description");
		presenter.setTrigger("Test trigger");
		presenter.setParameter("Test parameter");
		presenter.setResult("Test result");
		presenter.setIncidence("Test incidence");
		
		//Set preconditions  
		presenter.addPreCondition();
		presenter.getViewModel().setSelectedPreConditionIndex(0);
		presenter.setPreCondition("Test precondition 1"); 
		
	    //Set and postcondition 
		presenter.addPostCondition();
		presenter.getViewModel().setSelectedPostConditionIndex(0);
		presenter.setPostCondition("Test postcondition 1");  
		
		//Add some steps for the main scenario
		presenter.getViewModel().setSelectedScenarioItem(usecase.getScenarios().get(0));
		ScenarioStep ss = presenter.addNewScenarioStep();
		ss.setDescription("Main step 1");  
		presenter.getViewModel().setSelectedScenarioItem(ss);
		
		//Add an extension to the first scenario step
		ScenarioExtension se = presenter.addNewScenarioExtension();
		se.setName("Extension of main scenario"); 
		presenter.getViewModel().setSelectedScenarioItem(se); 
		
		//Add a step for the extension scenario
		ScenarioStep eStep = presenter.addNewScenarioStep();
		eStep.setDescription("Extension step 1"); 
		presenter.getViewModel().setSelectedScenarioItem(eStep);
		 
		//Add an actor
		presenter.addActor();
		presenter.getViewModel().setSelectedActorIndex(0);
		presenter.setActor("Test actor");
	}
	
	@Test
	public void canSetupUsecase() { 
		//ARRANGE+ACT
		setupTestUsecase();
		
		//ASSERT  
		assertEquals(UsecaseType.BUSINESS_PROCESS, usecase.getType());
		assertEquals("Test name", usecase.getName());
		assertEquals("Test description", usecase.getDescription().toString());
		assertEquals("Test trigger", usecase.getTrigger());
		assertEquals("Test parameter", usecase.getParameter());
		assertEquals("Test result", usecase.getResult());
		assertEquals("Test incidence", usecase.getIncidence());  
		assertEquals("Test precondition 1", usecase.getPreConditions().get(0).toString());  
		assertEquals("Test postcondition 1", usecase.getPostConditions().get(0).toString()); 
		assertEquals("Main scenario", usecase.getScenarios().get(0).getName());  
		assertEquals("Main step 1", usecase.getScenarios().get(0).getSteps().get(0).getDescription());  
		assertEquals("Extension of main scenario", usecase.getScenarios().get(1).getName());  
		assertEquals("Extension step 1", usecase.getScenarios().get(1).getSteps().get(0).getDescription());  
		assertEquals("Test actor", usecase.getOrganization().getActors().get(0).getName());  
	}  
	
	@Test
	public void canRemoveScenarioItem() { 
		//ARRANGE 
		setupTestUsecase();
		
		//ACT    
		presenter.getViewModel().setSelectedScenarioItem(usecase.getScenarios().get(0).getSteps().get(0));
		presenter.removeScenarioItem();
		
		//ASSERT
		assertEquals(1, usecase.getScenarios().size()); //Should only be main scenario
		assertEquals(0, usecase.getScenarios().get(0).getSteps().size()); //Should't have steps 
	}  
	
	@Test
	public void canNumberScenarioItems() {
		//ACT+ARRANGE 
		setupTestUsecase();
		ScenarioExtension se = (ScenarioExtension) usecase.getScenarios().get(1);
		
		//ASSERT
		assertEquals("1", usecase.getScenarios().get(0).getSteps().get(0).calculateID()); 
		assertEquals("1a", se.calculateID(usecase));  
		assertEquals("1", usecase.getScenarios().get(1).getSteps().get(0).calculateID());  
	}
	
	@Test
	public void canValidateValidUsecase() {
		//ACT+ARRANGE 
		setupTestUsecase();
		
		//ASSERT
		assertEquals(true, usecase.isValid());
		assertEquals(0, usecase.getValidationErrors().size());  
	 
	}
	
	@Test
	public void canValidateInvalidUsecase() {
		//ARRANGE 
		setupTestUsecase();
		
		//ACT    
		presenter.getViewModel().setSelectedScenarioItem(usecase.getScenarios().get(0).getSteps().get(0));
		presenter.removeScenarioItem();
		presenter.removePreCondition();
		presenter.setIncidence(null);
		presenter.setParameter(null);
		presenter.setTrigger(null);
		presenter.removePostCondition();
		presenter.setResult(null);
		
		//ASSERT
		assertEquals(false, usecase.isValid());
		
		//Should have three errors (missing scenario step, and missing pre and post condition attributes)
		assertEquals(3, usecase.getValidationErrors().size());  
	}
	
	@Test
	public void canGenerateScenarioEnactmentHTMLModel() {
		//ARRANGE 
		setupTestUsecase();
		
		//ACT
		Document htmlModel = null;
		
		try {
			 htmlModel = presenter.generateEnactmentHTMLModel();
		}
		catch(Exception e) {
			assertEquals(true, false);
		}
		
	 
		//ASSERT
		
		//Assert first page div
		org.w3c.dom.Node firstStepPage = htmlModel.getChildNodes().item(0); 
		ScenarioStep firstStep = usecase.getScenarios().get(0).getSteps().get(0);
		assertPage(firstStepPage, firstStep);
		
		//Assert scenario extension step page div
		org.w3c.dom.Node firstExtensionStepPage = htmlModel.getChildNodes().item(0); 
		ScenarioStep firstExtensionStep = usecase.getScenarios().get(0).getSteps().get(0);
		assertPage(firstExtensionStepPage, firstExtensionStep); 
	}
	
	private void assertPage(org.w3c.dom.Node node, ScenarioStep step) {
		 
		//Assert page div
		assertEquals("div", node.getNodeName());
		assertEquals(step.getUniqueID(), node.getAttributes().getNamedItem("id").getNodeValue());
		assertEquals("page", node.getAttributes().getNamedItem("data-role").getNodeValue());
		assertEquals("b", node.getAttributes().getNamedItem("data-theme").getNodeValue());
		
		//Assert page div first child element
		org.w3c.dom.Node contentNode = node.getFirstChild();
		assertEquals("div", contentNode.getNodeName());
		assertEquals("main", contentNode.getAttributes().getNamedItem("role").getNodeValue());
		assertEquals("ui-content", contentNode.getAttributes().getNamedItem("class").getNodeValue());
		
		//Assert first image element
		org.w3c.dom.Node imageNode = contentNode.getFirstChild();
		assertEquals("img", contentNode.getNodeName());
		assertEquals("", contentNode.getAttributes().getNamedItem("src").getNodeValue());
		String style = "display: block; margin-left: auto; margin-right: auto";
		assertEquals(style, node.getAttributes().getNamedItem("style").getNodeValue());
		
		//Get next step
		int nextStepIndex = step.getContainer().getSteps().indexOf(step) + 1;
		nextStepIndex = nextStepIndex > step.getContainer().getSteps().size() - 1 ? -1 : nextStepIndex;
		
		//Get number of branches for current step  
		List<ScenarioExtension> branches = new ArrayList<ScenarioExtension>();
		
		for(Object o : usecase.getScenarios().toArray()) {
			if(o instanceof ScenarioExtension) {
				ScenarioExtension se = (ScenarioExtension) o;
				if(se.getExtensionStep() == step) {
					branches.add(se);
				}
			}
		} 
		
		if(nextStepIndex == -1 && branches.size() == 0) {
			//No more steps, shouldn't display listview
			assertEquals(1, contentNode.getChildNodes().getLength());
		}
		else {
			//Assert listview element
			org.w3c.dom.Node listViewNode = contentNode.getChildNodes().item(1);
			assertEquals("ul ", listViewNode.getNodeName());
			assertEquals("listview", listViewNode.getAttributes().getNamedItem("data-role").getNodeValue());
			assertEquals("true", listViewNode.getAttributes().getNamedItem("data-inset").getNodeValue()); 
			
			//Number of li elements should be the next step + number of branches
			assertEquals(branches.size() + (nextStepIndex != -1 ? 1 : 0), listViewNode.getChildNodes().getLength());
			
			if(nextStepIndex != -1) {
				//Check next step li element
				ScenarioStep nextStep =  step.getContainer().getSteps().get(nextStepIndex);
				org.w3c.dom.Node nextStepNode = listViewNode.getFirstChild();
				assertLiNode(nextStep.getDescription(), "", nextStep.getUniqueID(), nextStepNode);
			} 
			
			//Check all branches
			int branchCounter = 1;
			for(ScenarioExtension se : branches) {
				org.w3c.dom.Node branchNode = listViewNode.getChildNodes().item(branchCounter);
				assertLiNode(se.getName(), "", se.getUniqueID(), branchNode);
				branchCounter++;
			} 
		} 
	}
	
	private void assertLiNode(String description, String imageURL, String nextID, org.w3c.dom.Node node) {
		assertEquals("li ", node.getNodeName());
		
		//First child anchor node
		org.w3c.dom.Node anchorNode = node.getFirstChild();
		assertEquals("a", anchorNode.getNodeName());
		assertEquals(nextID, anchorNode.getAttributes().getNamedItem("href").getNodeValue());
		assertEquals("slide", anchorNode.getAttributes().getNamedItem("data-transition").getNodeValue());
		
		//Anchor first child image node
		org.w3c.dom.Node imageNode = anchorNode.getFirstChild();
		assertEquals("img", imageNode.getNodeName());
		assertEquals(imageURL, anchorNode.getAttributes().getNamedItem("src").getNodeValue());
		assertEquals("ui-li-thumb", anchorNode.getAttributes().getNamedItem("class").getNodeValue());
		
		//Check description
		org.w3c.dom.Node titleNode = anchorNode.getChildNodes().item(1);
		assertEquals("h2", titleNode.getNodeName());
		assertEquals("Next step", titleNode.getNodeValue());
		
		org.w3c.dom.Node descriptionNode = anchorNode.getChildNodes().item(2);
		assertEquals("p", descriptionNode.getNodeName());
		assertEquals(description, descriptionNode.getNodeValue());
		
		org.w3c.dom.Node actorNode = anchorNode.getChildNodes().item(3);
		assertEquals("p", actorNode.getNodeName());
		assertEquals("Actor", actorNode.getNodeValue());
		assertEquals("ui-li-aside", actorNode.getAttributes().getNamedItem("class").getNodeValue()); 
	}
	

}
