package dk.dtu.imm.red.specificationelements.connector.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.ConnectorFactory;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.util.MultiplicityParser;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureFactory;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortFactory;
import dk.dtu.imm.red.specificationelements.port.RoleTableRow;
import dk.dtu.imm.red.specificationelements.port.SignatureDirection;


public class ConnectorTest {
	private Connector connector;
	private Port port1, port2;

	@Before
	public void setUp() throws Exception {
		connector = ConnectorFactory.eINSTANCE.createConnector();
		port1 = PortFactory.eINSTANCE.createPort();
		port2 = PortFactory.eINSTANCE.createPort();
	}

	@Test
	public void testConnectorAttached() {
		connector.getSource().add(port1);
		connector.checkStructure();
		int numberOfCommentsWithIssue = connector.getCommentlist().getComments().size();

		connector.getTarget().add(port2);
		connector.checkStructure();
		int numberOfCommentsWithoutIssue = connector.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithIssue - 1, numberOfCommentsWithoutIssue);
	}

	@Test
	public void testConnectorSignatureMatches() {
		Signature signature = SignatureFactory.eINSTANCE.createSignature();
		RoleTableRow row1 = PortFactory.eINSTANCE.createRoleTableRow();
		row1.setDirection(SignatureDirection.OUT);
		row1.setItem(signature);
		RoleTableRow row2 = PortFactory.eINSTANCE.createRoleTableRow();
		row2.setDirection(SignatureDirection.IN);
		row2.setItem(signature);
		
		connector.getSource().add(port1);
		connector.getTarget().add(port2);

		port1.getRoleTable().add(row1);
		connector.checkStructure();
		int numberOfCommentsWithIssue = connector.getCommentlist().getComments().size();

		port2.getRoleTable().add(row2);
		connector.checkStructure();
		int numberOfCommentsWithoutIssue = connector.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithIssue - 1, numberOfCommentsWithoutIssue);
	}

	@Test
	public void testConnectorDirectionMatches() {
		Signature signature = SignatureFactory.eINSTANCE.createSignature();
		RoleTableRow row1 = PortFactory.eINSTANCE.createRoleTableRow();
		row1.setDirection(SignatureDirection.OUT);
		row1.setItem(signature);
		RoleTableRow row2 = PortFactory.eINSTANCE.createRoleTableRow();
		row2.setDirection(SignatureDirection.OUT);
		row2.setItem(signature);
		
		connector.getSource().add(port1);
		connector.getTarget().add(port2);
		port1.getRoleTable().add(row1);
		port2.getRoleTable().add(row2);

		connector.checkStructure();
		int numberOfCommentsWithIssue = connector.getCommentlist().getComments().size();

		row2.setDirection(SignatureDirection.IN);
		connector.checkStructure();
		int numberOfCommentsWithoutIssue = connector.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithIssue - 2, numberOfCommentsWithoutIssue);
	}

	@Test
	public void testConnectorMultiplicityMatches() {
		Signature signature = SignatureFactory.eINSTANCE.createSignature();
		RoleTableRow row1 = PortFactory.eINSTANCE.createRoleTableRow();
		row1.setDirection(SignatureDirection.OUT);
		row1.setItem(signature);
		row1.setMultiplicity(MultiplicityParser.parse("2..3, 6, 8..*"));
		RoleTableRow row2 = PortFactory.eINSTANCE.createRoleTableRow();
		row2.setDirection(SignatureDirection.IN);
		row2.setItem(signature);
		row2.setMultiplicity(MultiplicityParser.parse("2..3, 5"));
		
		connector.getSource().add(port1);
		connector.getTarget().add(port2);
		port1.getRoleTable().add(row1);
		port2.getRoleTable().add(row2);

		connector.checkStructure();
		int numberOfCommentsWithIssue = connector.getCommentlist().getComments().size();

		row2.setMultiplicity(MultiplicityParser.parse("1..4, 6, 7..*"));
		connector.checkStructure();
		int numberOfCommentsWithoutIssue = connector.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithIssue - 1, numberOfCommentsWithoutIssue);
	}
}


