package dk.dtu.imm.red.specificationelements.system.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.ComplexityType;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationFactory;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortFactory;
import dk.dtu.imm.red.specificationelements.system.ConfigurationReference;
import dk.dtu.imm.red.specificationelements.system.GlueConnector;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.system.SystemFactory;


public class SystemTest {
	private System system;

	@Before
	public void setUp() throws Exception {
		system = SystemFactory.eINSTANCE.createSystem();
	}

	@Test
	public void testSystemInitialization() {
		System system = SystemFactory.eINSTANCE.createSystem();
		
		assertEquals(ComplexityType.SMALL, system.getEstimatedComplexity());
	}

	@Test
	public void testCheckCircularStructure() {
		Configuration configuration = ConfigurationFactory.eINSTANCE.createConfiguration();
		ConfigurationReference reference = SystemFactory.eINSTANCE.createConfigurationReference();
		reference.setConfiguration(configuration);
		system.getContainedConfigurations().add(reference);
		
		system.checkStructure();
		int numberOfCommentsWithoutIssue = system.getCommentlist().getComments().size();

		configuration.getParts().add(system);
		system.checkStructure();
		int numberOfCommentsWithIssue = system.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithoutIssue + 1, numberOfCommentsWithIssue);
	}

	@Test
	public void testCheckGlueNotAttachedToExternal() {
		Port port1 = PortFactory.eINSTANCE.createPort();
		Port port2 = PortFactory.eINSTANCE.createPort();
		GlueConnector glue = SystemFactory.eINSTANCE.createGlueConnector();
		glue.setSource(port1);
		glue.setTarget(port2);
		system.getGlueConnectors().add(glue);
		
		system.checkStructure();
		int numberOfCommentsWithIssue = system.getCommentlist().getComments().size();

		system.getPorts().add(port1);
		system.checkStructure();
		int numberOfCommentsWithoutIssue = system.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithIssue - 2, numberOfCommentsWithoutIssue);
	}

	@Test
	public void testCheckNoPorts() {
		Port port = PortFactory.eINSTANCE.createPort();
		
		system.checkStructure();
		int numberOfCommentsWithIssue = system.getCommentlist().getComments().size();

		system.getPorts().add(port);
		system.checkStructure();
		int numberOfCommentsWithoutIssue = system.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithIssue - 1, numberOfCommentsWithoutIssue);
	}
}


