package dk.dtu.imm.red.specificationelements.configuration.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.ActorFactory;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationFactory;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.ConnectorFactory;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortFactory;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.system.SystemFactory;


public class ConfigurationTest {
	private Configuration configuration;

	@Before
	public void setUp() throws Exception {
		configuration = ConfigurationFactory.eINSTANCE.createConfiguration();
	}

	@Test
	public void testCheckConnectorConfinement() {
		Actor actor = ActorFactory.eINSTANCE.createActor();
		configuration.getParts().add(actor);
		System system = SystemFactory.eINSTANCE.createSystem();
		configuration.getParts().add(system);
		Port port = PortFactory.eINSTANCE.createPort();
		Connector connector = ConnectorFactory.eINSTANCE.createConnector();
		connector.getSource().add(actor);
		connector.getTarget().add(port);
		configuration.getInternalConnectors().add(connector);
		
		configuration.checkStructure();
		int numberOfCommentsWithIssue = configuration.getCommentlist().getComments().size();

		system.getPorts().add(port);
		configuration.checkStructure();
		int numberOfCommentsWithoutIssue = configuration.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithIssue - 1, numberOfCommentsWithoutIssue);
	}
}


