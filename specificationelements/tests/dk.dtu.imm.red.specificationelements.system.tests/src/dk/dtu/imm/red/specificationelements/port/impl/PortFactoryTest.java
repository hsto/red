package dk.dtu.imm.red.specificationelements.port.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortFactory;


public class PortFactoryTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPortInitialization() {
		Port port = PortFactory.eINSTANCE.createPort();
		
		assertEquals("port", port.getElementKind().toLowerCase());
	}
}


