package dk.dtu.imm.red.specificationelements.system.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.system.SystemFactory;


public class SystemFactoryTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPortInitialization() {
		System system = SystemFactory.eINSTANCE.createSystem();
		
		assertEquals("system", system.getElementKind().toLowerCase());
	}
}


