package dk.dtu.imm.red.specificationelements.port.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.ComplexityType;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortFactory;


public class PortTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPortDefaultComplexity() {
		Port port = PortFactory.eINSTANCE.createPort();
		
		assertEquals(ComplexityType.MEDIUM, port.getEstimatedComplexity());
	}
}


