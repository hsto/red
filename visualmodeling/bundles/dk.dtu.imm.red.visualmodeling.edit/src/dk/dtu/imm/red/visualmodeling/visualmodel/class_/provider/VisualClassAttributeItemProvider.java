/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_.provider;


import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypeFactory;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;

import dk.dtu.imm.red.visualmodeling.visualmodel.provider.VisualElementItemProvider;
import dk.dtu.imm.red.visualmodeling.visualmodel.provider.VisualmodelingEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class VisualClassAttributeItemProvider
	extends VisualElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualClassAttributeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addVisibilityPropertyDescriptor(object);
			addReadOnlyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Visibility feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVisibilityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_VisualClassAttribute_Visibility_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_VisualClassAttribute_Visibility_feature", "_UI_VisualClassAttribute_type"),
				 ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__VISIBILITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Read Only feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReadOnlyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_VisualClassAttribute_ReadOnly_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_VisualClassAttribute_ReadOnly_feature", "_UI_VisualClassAttribute_type"),
				 ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__READ_ONLY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE);
			childrenFeatures.add(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns VisualClassAttribute.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/VisualClassAttribute"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((VisualClassAttribute)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_VisualClassAttribute_type") :
			getString("_UI_VisualClassAttribute_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(VisualClassAttribute.class)) {
			case ClassPackage.VISUAL_CLASS_ATTRIBUTE__VISIBILITY:
			case ClassPackage.VISUAL_CLASS_ATTRIBUTE__READ_ONLY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ClassPackage.VISUAL_CLASS_ATTRIBUTE__TYPE:
			case ClassPackage.VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createBaseType()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createFunctionType()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createDataStructureType()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createTupleType()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createNestedType()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createArrayType()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createChoiceType()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createSignatureReference()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createNoType()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createTypeVariable()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__TYPE,
				 DatatypeFactory.eINSTANCE.createUnparsedType()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createSingleValueMultiplicity()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createRangedMultiplicity()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createCompoundMultiplicity()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createUnparsedMultiplicity()));

		newChildDescriptors.add
			(createChildParameter
				(ClassPackage.Literals.VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY,
				 MultiplicityFactory.eINSTANCE.createNoMultiplicity()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return VisualmodelingEditPlugin.INSTANCE;
	}

}
