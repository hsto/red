/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.provider;


import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalFactory;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructureFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecaseFactory;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class VisualDiagramItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualDiagramItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLocationPropertyDescriptor(object);
			addBoundsPropertyDescriptor(object);
			addParentPropertyDescriptor(object);
			addDiagramPropertyDescriptor(object);
			addConnectionsPropertyDescriptor(object);
			addIsSketchyPropertyDescriptor(object);
			addValidationMessagePropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addVisualIDPropertyDescriptor(object);
			addStitchToPropertyDescriptor(object);
			addDiagramTypePropertyDescriptor(object);
			addIsStitchOutputPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Location feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_Location_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_Location_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__LOCATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bounds feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBoundsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_Bounds_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_Bounds_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__BOUNDS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Parent feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_Parent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_Parent_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__PARENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Diagram feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDiagramPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_Diagram_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_Diagram_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__DIAGRAM,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Connections feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConnectionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_Connections_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_Connections_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__CONNECTIONS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Sketchy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsSketchyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_IsSketchy_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_IsSketchy_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__IS_SKETCHY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Validation Message feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValidationMessagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_ValidationMessage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_ValidationMessage_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__VALIDATION_MESSAGE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_Name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_Name_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Visual ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVisualIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_visualID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_visualID_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__VISUAL_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Stitch To feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStitchToPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualElement_stitchTo_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualElement_stitchTo_feature", "_UI_IVisualElement_type"),
				 visualmodelPackage.Literals.IVISUAL_ELEMENT__STITCH_TO,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Diagram Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDiagramTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_VisualDiagram_DiagramType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_VisualDiagram_DiagramType_feature", "_UI_VisualDiagram_type"),
				 visualmodelPackage.Literals.VISUAL_DIAGRAM__DIAGRAM_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Stitch Output feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsStitchOutputPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_VisualDiagram_isStitchOutput_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_VisualDiagram_isStitchOutput_feature", "_UI_VisualDiagram_type"),
				 visualmodelPackage.Literals.VISUAL_DIAGRAM__IS_STITCH_OUTPUT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS);
			childrenFeatures.add(visualmodelPackage.Literals.IVISUAL_ELEMENT__STITCH_FROM);
			childrenFeatures.add(visualmodelPackage.Literals.VISUAL_DIAGRAM__DIAGRAM_CONNECTIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns VisualDiagram.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/VisualDiagram"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((VisualDiagram)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_VisualDiagram_type") :
			getString("_UI_VisualDiagram_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(VisualDiagram.class)) {
			case visualmodelPackage.VISUAL_DIAGRAM__LOCATION:
			case visualmodelPackage.VISUAL_DIAGRAM__BOUNDS:
			case visualmodelPackage.VISUAL_DIAGRAM__IS_SKETCHY:
			case visualmodelPackage.VISUAL_DIAGRAM__VALIDATION_MESSAGE:
			case visualmodelPackage.VISUAL_DIAGRAM__NAME:
			case visualmodelPackage.VISUAL_DIAGRAM__VISUAL_ID:
			case visualmodelPackage.VISUAL_DIAGRAM__DIAGRAM_TYPE:
			case visualmodelPackage.VISUAL_DIAGRAM__IS_STITCH_OUTPUT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case visualmodelPackage.VISUAL_DIAGRAM__ELEMENTS:
			case visualmodelPackage.VISUAL_DIAGRAM__STITCH_FROM:
			case visualmodelPackage.VISUAL_DIAGRAM__DIAGRAM_CONNECTIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 visualmodelFactory.eINSTANCE.createVisualDiagram()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 visualmodelFactory.eINSTANCE.createVisualElement()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 visualmodelFactory.eINSTANCE.createNote()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 visualmodelFactory.eINSTANCE.createVisualGenericElement()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 GoalFactory.eINSTANCE.createVisualGoalElement()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 GoalFactory.eINSTANCE.createVisualGoalLayer()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 UsecaseFactory.eINSTANCE.createVisualActorElement()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 UsecaseFactory.eINSTANCE.createVisualUseCaseElement()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 UsecaseFactory.eINSTANCE.createVisualSystemBoundaryElement()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ClassFactory.eINSTANCE.createVisualClassElement()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ClassFactory.eINSTANCE.createVisualClassAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ClassFactory.eINSTANCE.createVisualClassOperation()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ClassFactory.eINSTANCE.createVisualEnumerationElement()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ClassFactory.eINSTANCE.createVisualEnumerationLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ClassFactory.eINSTANCE.createVisualPackageElement()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ActivityFactory.eINSTANCE.createVisualActionNode()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ActivityFactory.eINSTANCE.createVisualInitialNode()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ActivityFactory.eINSTANCE.createVisualActivityFinalNode()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ActivityFactory.eINSTANCE.createVisualFlowFinalNode()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ActivityFactory.eINSTANCE.createVisualDecisionMergeNode()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ActivityFactory.eINSTANCE.createVisualForkJoinNode()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ActivityFactory.eINSTANCE.createVisualSendSignalNode()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ActivityFactory.eINSTANCE.createVisualReceiveSignalNode()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 ActivityFactory.eINSTANCE.createVisualObjectNode()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 StatemachineFactory.eINSTANCE.createVisualState()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 StatemachineFactory.eINSTANCE.createVisualInitialState()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 StatemachineFactory.eINSTANCE.createVisualFinalState()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 StatemachineFactory.eINSTANCE.createVisualDecisionMergeState()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 StatemachineFactory.eINSTANCE.createVisualForkJoinState()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 StatemachineFactory.eINSTANCE.createVisualSendState()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 StatemachineFactory.eINSTANCE.createVisualReceiveState()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 StatemachineFactory.eINSTANCE.createVisualShallowHistoryState()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 StatemachineFactory.eINSTANCE.createVisualDeepHistoryState()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 SystemstructureFactory.eINSTANCE.createVisualSystemStructureActor()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 SystemstructureFactory.eINSTANCE.createVisualSystem()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__ELEMENTS,
				 SystemstructureFactory.eINSTANCE.createVisualPort()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_ELEMENT__STITCH_FROM,
				 visualmodelFactory.eINSTANCE.createStitchElementRelationship()));

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.VISUAL_DIAGRAM__DIAGRAM_CONNECTIONS,
				 visualmodelFactory.eINSTANCE.createVisualConnection()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return VisualmodelingEditPlugin.INSTANCE;
	}

}
