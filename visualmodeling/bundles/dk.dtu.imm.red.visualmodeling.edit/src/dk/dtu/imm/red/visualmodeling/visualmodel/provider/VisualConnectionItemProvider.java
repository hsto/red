/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.provider;


import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class VisualConnectionItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualConnectionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourcePropertyDescriptor(object);
			addTargetPropertyDescriptor(object);
			addDirectionPropertyDescriptor(object);
			addBendpointsPropertyDescriptor(object);
			addLineStylePropertyDescriptor(object);
			addSourceDecorationPropertyDescriptor(object);
			addTargetDecorationPropertyDescriptor(object);
			addValidationMessagePropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addSourceMultiplicityPropertyDescriptor(object);
			addTargetMultiplicityPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addRountingPropertyDescriptor(object);
			addVisualIDPropertyDescriptor(object);
			addStitchToPropertyDescriptor(object);
			addSpecificationElementPropertyDescriptor(object);
			addIsLinkedToElementPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_Source_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_Source_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__SOURCE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_Target_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_Target_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__TARGET,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Direction feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_Direction_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_Direction_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__DIRECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bendpoints feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBendpointsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_Bendpoints_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_Bendpoints_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__BENDPOINTS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Line Style feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLineStylePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_LineStyle_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_LineStyle_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__LINE_STYLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Decoration feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceDecorationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_SourceDecoration_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_SourceDecoration_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__SOURCE_DECORATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Decoration feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetDecorationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_TargetDecoration_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_TargetDecoration_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__TARGET_DECORATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Validation Message feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValidationMessagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_ValidationMessage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_ValidationMessage_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__VALIDATION_MESSAGE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_Name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_Name_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Multiplicity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceMultiplicityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_SourceMultiplicity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_SourceMultiplicity_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__SOURCE_MULTIPLICITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Multiplicity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetMultiplicityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_TargetMultiplicity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_TargetMultiplicity_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__TARGET_MULTIPLICITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_Type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_Type_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Rounting feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRountingPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_Rounting_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_Rounting_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__ROUNTING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Visual ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVisualIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_visualID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_visualID_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__VISUAL_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Stitch To feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStitchToPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IVisualConnection_stitchTo_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IVisualConnection_stitchTo_feature", "_UI_IVisualConnection_type"),
				 visualmodelPackage.Literals.IVISUAL_CONNECTION__STITCH_TO,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Specification Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSpecificationElementPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_VisualConnection_SpecificationElement_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_VisualConnection_SpecificationElement_feature", "_UI_VisualConnection_type"),
				 visualmodelPackage.Literals.VISUAL_CONNECTION__SPECIFICATION_ELEMENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Linked To Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsLinkedToElementPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_VisualConnection_IsLinkedToElement_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_VisualConnection_IsLinkedToElement_feature", "_UI_VisualConnection_type"),
				 visualmodelPackage.Literals.VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(visualmodelPackage.Literals.IVISUAL_CONNECTION__STITCH_FROM);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns VisualConnection.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/VisualConnection"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((VisualConnection)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_VisualConnection_type") :
			getString("_UI_VisualConnection_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(VisualConnection.class)) {
			case visualmodelPackage.VISUAL_CONNECTION__DIRECTION:
			case visualmodelPackage.VISUAL_CONNECTION__BENDPOINTS:
			case visualmodelPackage.VISUAL_CONNECTION__LINE_STYLE:
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_DECORATION:
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_DECORATION:
			case visualmodelPackage.VISUAL_CONNECTION__VALIDATION_MESSAGE:
			case visualmodelPackage.VISUAL_CONNECTION__NAME:
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_MULTIPLICITY:
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_MULTIPLICITY:
			case visualmodelPackage.VISUAL_CONNECTION__TYPE:
			case visualmodelPackage.VISUAL_CONNECTION__ROUNTING:
			case visualmodelPackage.VISUAL_CONNECTION__VISUAL_ID:
			case visualmodelPackage.VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_FROM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(visualmodelPackage.Literals.IVISUAL_CONNECTION__STITCH_FROM,
				 visualmodelFactory.eINSTANCE.createStitchConnectionRelationship()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return VisualmodelingEditPlugin.INSTANCE;
	}

}
