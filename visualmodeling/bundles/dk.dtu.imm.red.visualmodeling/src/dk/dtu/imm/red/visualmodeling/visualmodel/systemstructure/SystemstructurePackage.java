/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructureFactory
 * @model kind="package"
 * @generated
 */
public interface SystemstructurePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "systemstructure";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "systemstructure";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SystemstructurePackage eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualSystemStructureActorImpl <em>Visual System Structure Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualSystemStructureActorImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getVisualSystemStructureActor()
	 * @generated
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR = 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__LOCATION = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__BOUNDS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__PARENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__DIAGRAM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__ELEMENTS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__CONNECTIONS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__IS_SKETCHY = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__NAME = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__VISUAL_ID = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__STITCH_FROM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__STITCH_TO = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__SPECIFICATION_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__IS_LINKED_TO_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT;

	/**
	 * The feature id for the '<em><b>Actor Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR__ACTOR_KIND = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Visual System Structure Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_STRUCTURE_ACTOR_FEATURE_COUNT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualSystemImpl <em>Visual System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualSystemImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getVisualSystem()
	 * @generated
	 */
	int VISUAL_SYSTEM = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__LOCATION = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__BOUNDS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__PARENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__DIAGRAM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__ELEMENTS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__CONNECTIONS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__IS_SKETCHY = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__NAME = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__VISUAL_ID = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__STITCH_FROM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__STITCH_TO = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__SPECIFICATION_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM__IS_LINKED_TO_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT;

	/**
	 * The number of structural features of the '<em>Visual System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_FEATURE_COUNT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualPortImpl <em>Visual Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualPortImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getVisualPort()
	 * @generated
	 */
	int VISUAL_PORT = 2;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__LOCATION = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__BOUNDS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__PARENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__DIAGRAM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__ELEMENTS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__CONNECTIONS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__IS_SKETCHY = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__NAME = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__VISUAL_ID = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__STITCH_FROM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__STITCH_TO = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__SPECIFICATION_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT__IS_LINKED_TO_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT;

	/**
	 * The number of structural features of the '<em>Visual Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PORT_FEATURE_COUNT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind <em>System Structure Actor Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getSystemStructureActorKind()
	 * @generated
	 */
	int SYSTEM_STRUCTURE_ACTOR_KIND = 3;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind <em>System Structure Diagram Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getSystemStructureDiagramKind()
	 * @generated
	 */
	int SYSTEM_STRUCTURE_DIAGRAM_KIND = 4;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor <em>Visual System Structure Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual System Structure Actor</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor
	 * @generated
	 */
	EClass getVisualSystemStructureActor();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor#getActorKind <em>Actor Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Actor Kind</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor#getActorKind()
	 * @see #getVisualSystemStructureActor()
	 * @generated
	 */
	EAttribute getVisualSystemStructureActor_ActorKind();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem <em>Visual System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual System</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem
	 * @generated
	 */
	EClass getVisualSystem();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort <em>Visual Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Port</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort
	 * @generated
	 */
	EClass getVisualPort();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind <em>System Structure Actor Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>System Structure Actor Kind</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind
	 * @generated
	 */
	EEnum getSystemStructureActorKind();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind <em>System Structure Diagram Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>System Structure Diagram Kind</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind
	 * @generated
	 */
	EEnum getSystemStructureDiagramKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SystemstructureFactory getSystemstructureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualSystemStructureActorImpl <em>Visual System Structure Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualSystemStructureActorImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getVisualSystemStructureActor()
		 * @generated
		 */
		EClass VISUAL_SYSTEM_STRUCTURE_ACTOR = eINSTANCE.getVisualSystemStructureActor();

		/**
		 * The meta object literal for the '<em><b>Actor Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_SYSTEM_STRUCTURE_ACTOR__ACTOR_KIND = eINSTANCE.getVisualSystemStructureActor_ActorKind();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualSystemImpl <em>Visual System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualSystemImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getVisualSystem()
		 * @generated
		 */
		EClass VISUAL_SYSTEM = eINSTANCE.getVisualSystem();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualPortImpl <em>Visual Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualPortImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getVisualPort()
		 * @generated
		 */
		EClass VISUAL_PORT = eINSTANCE.getVisualPort();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind <em>System Structure Actor Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getSystemStructureActorKind()
		 * @generated
		 */
		EEnum SYSTEM_STRUCTURE_ACTOR_KIND = eINSTANCE.getSystemStructureActorKind();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind <em>System Structure Diagram Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl#getSystemStructureDiagramKind()
		 * @generated
		 */
		EEnum SYSTEM_STRUCTURE_DIAGRAM_KIND = eINSTANCE.getSystemStructureDiagramKind();

	}

} //SystemstructurePackage
