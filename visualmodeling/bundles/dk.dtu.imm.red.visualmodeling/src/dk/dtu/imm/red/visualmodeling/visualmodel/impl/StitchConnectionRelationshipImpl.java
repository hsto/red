/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stitch Connection Relationship</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchConnectionRelationshipImpl#getStitchOrigin <em>Stitch Origin</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchConnectionRelationshipImpl#getStitchOutput <em>Stitch Output</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StitchConnectionRelationshipImpl extends EObjectImpl implements StitchConnectionRelationship {
	/**
	 * The cached value of the '{@link #getStitchOrigin() <em>Stitch Origin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStitchOrigin()
	 * @generated
	 * @ordered
	 */
	protected IVisualConnection stitchOrigin;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StitchConnectionRelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return visualmodelPackage.Literals.STITCH_CONNECTION_RELATIONSHIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualConnection getStitchOrigin() {
		if (stitchOrigin != null && stitchOrigin.eIsProxy()) {
			InternalEObject oldStitchOrigin = (InternalEObject)stitchOrigin;
			stitchOrigin = (IVisualConnection)eResolveProxy(oldStitchOrigin);
			if (stitchOrigin != oldStitchOrigin) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN, oldStitchOrigin, stitchOrigin));
			}
		}
		return stitchOrigin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualConnection basicGetStitchOrigin() {
		return stitchOrigin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStitchOrigin(IVisualConnection newStitchOrigin, NotificationChain msgs) {
		IVisualConnection oldStitchOrigin = stitchOrigin;
		stitchOrigin = newStitchOrigin;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN, oldStitchOrigin, newStitchOrigin);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStitchOrigin(IVisualConnection newStitchOrigin) {
		if (newStitchOrigin != stitchOrigin) {
			NotificationChain msgs = null;
			if (stitchOrigin != null)
				msgs = ((InternalEObject)stitchOrigin).eInverseRemove(this, visualmodelPackage.IVISUAL_CONNECTION__STITCH_TO, IVisualConnection.class, msgs);
			if (newStitchOrigin != null)
				msgs = ((InternalEObject)newStitchOrigin).eInverseAdd(this, visualmodelPackage.IVISUAL_CONNECTION__STITCH_TO, IVisualConnection.class, msgs);
			msgs = basicSetStitchOrigin(newStitchOrigin, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN, newStitchOrigin, newStitchOrigin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualConnection getStitchOutput() {
		if (eContainerFeatureID() != visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT) return null;
		return (IVisualConnection)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualConnection basicGetStitchOutput() {
		if (eContainerFeatureID() != visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT) return null;
		return (IVisualConnection)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStitchOutput(IVisualConnection newStitchOutput, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newStitchOutput, visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStitchOutput(IVisualConnection newStitchOutput) {
		if (newStitchOutput != eInternalContainer() || (eContainerFeatureID() != visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT && newStitchOutput != null)) {
			if (EcoreUtil.isAncestor(this, newStitchOutput))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newStitchOutput != null)
				msgs = ((InternalEObject)newStitchOutput).eInverseAdd(this, visualmodelPackage.IVISUAL_CONNECTION__STITCH_FROM, IVisualConnection.class, msgs);
			msgs = basicSetStitchOutput(newStitchOutput, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT, newStitchOutput, newStitchOutput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN:
				if (stitchOrigin != null)
					msgs = ((InternalEObject)stitchOrigin).eInverseRemove(this, visualmodelPackage.IVISUAL_CONNECTION__STITCH_TO, IVisualConnection.class, msgs);
				return basicSetStitchOrigin((IVisualConnection)otherEnd, msgs);
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetStitchOutput((IVisualConnection)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN:
				return basicSetStitchOrigin(null, msgs);
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT:
				return basicSetStitchOutput(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT:
				return eInternalContainer().eInverseRemove(this, visualmodelPackage.IVISUAL_CONNECTION__STITCH_FROM, IVisualConnection.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN:
				if (resolve) return getStitchOrigin();
				return basicGetStitchOrigin();
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT:
				if (resolve) return getStitchOutput();
				return basicGetStitchOutput();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN:
				setStitchOrigin((IVisualConnection)newValue);
				return;
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT:
				setStitchOutput((IVisualConnection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN:
				setStitchOrigin((IVisualConnection)null);
				return;
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT:
				setStitchOutput((IVisualConnection)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN:
				return stitchOrigin != null;
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT:
				return basicGetStitchOutput() != null;
		}
		return super.eIsSet(featureID);
	}

} //StitchConnectionRelationshipImpl
