/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualInitialState;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Initial State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualInitialStateImpl extends VisualElementImpl implements VisualInitialState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualInitialStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.VISUAL_INITIAL_STATE;
	}

} //VisualInitialStateImpl
