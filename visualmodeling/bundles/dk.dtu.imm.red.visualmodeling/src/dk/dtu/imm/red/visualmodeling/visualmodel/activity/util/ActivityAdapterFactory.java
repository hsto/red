/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity.util;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage
 * @generated
 */
public class ActivityAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ActivityPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ActivityPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivitySwitch<Adapter> modelSwitch =
		new ActivitySwitch<Adapter>() {
			@Override
			public Adapter caseVisualActionNode(VisualActionNode object) {
				return createVisualActionNodeAdapter();
			}
			@Override
			public Adapter caseVisualInitialNode(VisualInitialNode object) {
				return createVisualInitialNodeAdapter();
			}
			@Override
			public Adapter caseVisualActivityFinalNode(VisualActivityFinalNode object) {
				return createVisualActivityFinalNodeAdapter();
			}
			@Override
			public Adapter caseVisualFlowFinalNode(VisualFlowFinalNode object) {
				return createVisualFlowFinalNodeAdapter();
			}
			@Override
			public Adapter caseVisualDecisionMergeNode(VisualDecisionMergeNode object) {
				return createVisualDecisionMergeNodeAdapter();
			}
			@Override
			public Adapter caseVisualForkJoinNode(VisualForkJoinNode object) {
				return createVisualForkJoinNodeAdapter();
			}
			@Override
			public Adapter caseVisualSendSignalNode(VisualSendSignalNode object) {
				return createVisualSendSignalNodeAdapter();
			}
			@Override
			public Adapter caseVisualReceiveSignalNode(VisualReceiveSignalNode object) {
				return createVisualReceiveSignalNodeAdapter();
			}
			@Override
			public Adapter caseVisualObjectNode(VisualObjectNode object) {
				return createVisualObjectNodeAdapter();
			}
			@Override
			public Adapter caseIVisualElement(IVisualElement object) {
				return createIVisualElementAdapter();
			}
			@Override
			public Adapter caseVisualElement(VisualElement object) {
				return createVisualElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActionNode <em>Visual Action Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActionNode
	 * @generated
	 */
	public Adapter createVisualActionNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualInitialNode <em>Visual Initial Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualInitialNode
	 * @generated
	 */
	public Adapter createVisualInitialNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActivityFinalNode <em>Visual Activity Final Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActivityFinalNode
	 * @generated
	 */
	public Adapter createVisualActivityFinalNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualFlowFinalNode <em>Visual Flow Final Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualFlowFinalNode
	 * @generated
	 */
	public Adapter createVisualFlowFinalNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualDecisionMergeNode <em>Visual Decision Merge Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualDecisionMergeNode
	 * @generated
	 */
	public Adapter createVisualDecisionMergeNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualForkJoinNode <em>Visual Fork Join Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualForkJoinNode
	 * @generated
	 */
	public Adapter createVisualForkJoinNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualSendSignalNode <em>Visual Send Signal Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualSendSignalNode
	 * @generated
	 */
	public Adapter createVisualSendSignalNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualReceiveSignalNode <em>Visual Receive Signal Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualReceiveSignalNode
	 * @generated
	 */
	public Adapter createVisualReceiveSignalNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode <em>Visual Object Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode
	 * @generated
	 */
	public Adapter createVisualObjectNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement <em>IVisual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement
	 * @generated
	 */
	public Adapter createIVisualElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement <em>Visual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement
	 * @generated
	 */
	public Adapter createVisualElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ActivityAdapterFactory
