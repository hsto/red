/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getVisualDiagram <em>Visual Diagram</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getMergeLog <em>Merge Log</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getStitchState <em>Stitch State</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getDiagram()
 * @model
 * @generated
 */
public interface Diagram extends SpecificationElement {
	/**
	 * Returns the value of the '<em><b>Visual Diagram</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramElement <em>Diagram Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visual Diagram</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visual Diagram</em>' containment reference.
	 * @see #setVisualDiagram(VisualDiagram)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getDiagram_VisualDiagram()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramElement
	 * @model opposite="diagramElement" containment="true" resolveProxies="true"
	 * @generated
	 */
	VisualDiagram getVisualDiagram();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getVisualDiagram <em>Visual Diagram</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visual Diagram</em>' containment reference.
	 * @see #getVisualDiagram()
	 * @generated
	 */
	void setVisualDiagram(VisualDiagram value);

	/**
	 * Returns the value of the '<em><b>Merge Log</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merge Log</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merge Log</em>' attribute.
	 * @see #setMergeLog(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getDiagram_MergeLog()
	 * @model
	 * @generated
	 */
	String getMergeLog();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getMergeLog <em>Merge Log</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merge Log</em>' attribute.
	 * @see #getMergeLog()
	 * @generated
	 */
	void setMergeLog(String value);

	/**
	 * Returns the value of the '<em><b>Stitch State</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stitch State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stitch State</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchState
	 * @see #setStitchState(StitchState)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getDiagram_StitchState()
	 * @model transient="true"
	 * @generated
	 */
	StitchState getStitchState();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getStitchState <em>Stitch State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stitch State</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchState
	 * @see #getStitchState()
	 * @generated
	 */
	void setStitchState(StitchState value);

} // Diagram
