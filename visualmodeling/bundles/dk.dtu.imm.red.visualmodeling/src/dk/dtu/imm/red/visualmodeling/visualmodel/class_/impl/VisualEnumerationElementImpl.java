/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Enumeration Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualEnumerationElementImpl extends VisualElementImpl implements VisualEnumerationElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualEnumerationElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassPackage.Literals.VISUAL_ENUMERATION_ELEMENT;
	}


	/**
	 * @generated NOT
	 */
	List<VisualEnumerationLiteral> literalsList = new ArrayList<VisualEnumerationLiteral>();

	/**
	 * @generated NOT
	 */
	public List<VisualEnumerationLiteral> getLiterals(){
		if(literalsList.isEmpty()){
			List<IVisualElement> elementList = getElements();
			for(int i=0;i<elementList.size();i++){
				IVisualElement el = elementList.get(i);
				if(el instanceof VisualEnumerationLiteral){
					literalsList.add((VisualEnumerationLiteral)el);
				}
			}
		}
		return literalsList;
	}

} //VisualEnumerationElementImpl
