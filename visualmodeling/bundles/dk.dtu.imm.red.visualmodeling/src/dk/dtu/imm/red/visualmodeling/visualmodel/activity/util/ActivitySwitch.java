/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity.util;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage
 * @generated
 */
public class ActivitySwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ActivityPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivitySwitch() {
		if (modelPackage == null) {
			modelPackage = ActivityPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ActivityPackage.VISUAL_ACTION_NODE: {
				VisualActionNode visualActionNode = (VisualActionNode)theEObject;
				T result = caseVisualActionNode(visualActionNode);
				if (result == null) result = caseVisualElement(visualActionNode);
				if (result == null) result = caseIVisualElement(visualActionNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ActivityPackage.VISUAL_INITIAL_NODE: {
				VisualInitialNode visualInitialNode = (VisualInitialNode)theEObject;
				T result = caseVisualInitialNode(visualInitialNode);
				if (result == null) result = caseVisualElement(visualInitialNode);
				if (result == null) result = caseIVisualElement(visualInitialNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ActivityPackage.VISUAL_ACTIVITY_FINAL_NODE: {
				VisualActivityFinalNode visualActivityFinalNode = (VisualActivityFinalNode)theEObject;
				T result = caseVisualActivityFinalNode(visualActivityFinalNode);
				if (result == null) result = caseVisualElement(visualActivityFinalNode);
				if (result == null) result = caseIVisualElement(visualActivityFinalNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ActivityPackage.VISUAL_FLOW_FINAL_NODE: {
				VisualFlowFinalNode visualFlowFinalNode = (VisualFlowFinalNode)theEObject;
				T result = caseVisualFlowFinalNode(visualFlowFinalNode);
				if (result == null) result = caseVisualElement(visualFlowFinalNode);
				if (result == null) result = caseIVisualElement(visualFlowFinalNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ActivityPackage.VISUAL_DECISION_MERGE_NODE: {
				VisualDecisionMergeNode visualDecisionMergeNode = (VisualDecisionMergeNode)theEObject;
				T result = caseVisualDecisionMergeNode(visualDecisionMergeNode);
				if (result == null) result = caseVisualElement(visualDecisionMergeNode);
				if (result == null) result = caseIVisualElement(visualDecisionMergeNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ActivityPackage.VISUAL_FORK_JOIN_NODE: {
				VisualForkJoinNode visualForkJoinNode = (VisualForkJoinNode)theEObject;
				T result = caseVisualForkJoinNode(visualForkJoinNode);
				if (result == null) result = caseVisualElement(visualForkJoinNode);
				if (result == null) result = caseIVisualElement(visualForkJoinNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ActivityPackage.VISUAL_SEND_SIGNAL_NODE: {
				VisualSendSignalNode visualSendSignalNode = (VisualSendSignalNode)theEObject;
				T result = caseVisualSendSignalNode(visualSendSignalNode);
				if (result == null) result = caseVisualElement(visualSendSignalNode);
				if (result == null) result = caseIVisualElement(visualSendSignalNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ActivityPackage.VISUAL_RECEIVE_SIGNAL_NODE: {
				VisualReceiveSignalNode visualReceiveSignalNode = (VisualReceiveSignalNode)theEObject;
				T result = caseVisualReceiveSignalNode(visualReceiveSignalNode);
				if (result == null) result = caseVisualElement(visualReceiveSignalNode);
				if (result == null) result = caseIVisualElement(visualReceiveSignalNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ActivityPackage.VISUAL_OBJECT_NODE: {
				VisualObjectNode visualObjectNode = (VisualObjectNode)theEObject;
				T result = caseVisualObjectNode(visualObjectNode);
				if (result == null) result = caseVisualElement(visualObjectNode);
				if (result == null) result = caseIVisualElement(visualObjectNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Action Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Action Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualActionNode(VisualActionNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Initial Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Initial Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualInitialNode(VisualInitialNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Activity Final Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Activity Final Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualActivityFinalNode(VisualActivityFinalNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Flow Final Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Flow Final Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualFlowFinalNode(VisualFlowFinalNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Decision Merge Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Decision Merge Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualDecisionMergeNode(VisualDecisionMergeNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Fork Join Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Fork Join Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualForkJoinNode(VisualForkJoinNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Send Signal Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Send Signal Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualSendSignalNode(VisualSendSignalNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Receive Signal Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Receive Signal Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualReceiveSignalNode(VisualReceiveSignalNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Object Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Object Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualObjectNode(VisualObjectNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IVisual Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IVisual Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIVisualElement(IVisualElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualElement(VisualElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ActivitySwitch
