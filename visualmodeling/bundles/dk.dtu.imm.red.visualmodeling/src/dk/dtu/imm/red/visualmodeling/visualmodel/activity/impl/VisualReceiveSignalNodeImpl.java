/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualReceiveSignalNode;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Receive Signal Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualReceiveSignalNodeImpl extends VisualElementImpl implements VisualReceiveSignalNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualReceiveSignalNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActivityPackage.Literals.VISUAL_RECEIVE_SIGNAL_NODE;
	}

} //VisualReceiveSignalNodeImpl
