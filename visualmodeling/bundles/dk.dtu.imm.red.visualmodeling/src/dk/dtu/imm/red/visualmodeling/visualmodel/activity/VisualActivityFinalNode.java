/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Activity Final Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage#getVisualActivityFinalNode()
 * @model
 * @generated
 */
public interface VisualActivityFinalNode extends VisualElement {
} // VisualActivityFinalNode
