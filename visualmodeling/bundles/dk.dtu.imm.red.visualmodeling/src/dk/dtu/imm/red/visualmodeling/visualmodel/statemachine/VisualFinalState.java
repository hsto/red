/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Final State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage#getVisualFinalState()
 * @model
 * @generated
 */
public interface VisualFinalState extends VisualElement {
} // VisualFinalState
