/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Object Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage#getVisualObjectNode()
 * @model
 * @generated
 */
public interface VisualObjectNode extends VisualElement {
} // VisualObjectNode
