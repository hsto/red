/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.usecase;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Actor Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage#getVisualActorElement()
 * @model
 * @generated
 */
public interface VisualActorElement extends VisualSpecificationElement {
} // VisualActorElement
