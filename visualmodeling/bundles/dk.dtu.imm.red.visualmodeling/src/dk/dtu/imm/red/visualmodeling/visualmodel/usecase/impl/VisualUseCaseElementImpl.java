/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.UsecaseFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Use Case Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualUseCaseElementImpl extends VisualSpecificationElementImpl implements VisualUseCaseElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualUseCaseElementImpl() {
		super();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public Class<?> getSpecificationElementType() {
		return UseCase.class;
	}
	
	@Override 
	public SpecificationElement createSpecificationElement(Group parent) {
		UseCase usecase = UsecaseFactory.eINSTANCE.createUseCase();
		usecase.setCreator(PreferenceUtil.getUserPreference_User());
		
		String str = (this.name == null) ? "" : this.name; 
		
		usecase.setName(str);
		usecase.setLabel(str);
		usecase.setDescription("");
		
		if (parent != null) {
			usecase.setParent(parent);
		}
		usecase.save();
		
		return usecase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasePackage.Literals.VISUAL_USE_CASE_ELEMENT;
	}

} //VisualUseCaseElementImpl
