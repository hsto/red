/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualReceiveState;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Receive State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualReceiveStateImpl extends VisualElementImpl implements VisualReceiveState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualReceiveStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.VISUAL_RECEIVE_STATE;
	}

} //VisualReceiveStateImpl
