/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.usecase;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecaseFactory
 * @model kind="package"
 * @generated
 */
public interface UsecasePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "usecase";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.visualmodeling.visualmodel.usecase";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "usecase";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UsecasePackage eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualActorElementImpl <em>Visual Actor Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualActorElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl#getVisualActorElement()
	 * @generated
	 */
	int VISUAL_ACTOR_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__LOCATION = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__BOUNDS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__PARENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__DIAGRAM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__ELEMENTS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__CONNECTIONS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__IS_SKETCHY = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__NAME = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__VISUAL_ID = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__STITCH_FROM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__STITCH_TO = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__SPECIFICATION_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT__IS_LINKED_TO_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT;

	/**
	 * The number of structural features of the '<em>Visual Actor Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTOR_ELEMENT_FEATURE_COUNT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualUseCaseElementImpl <em>Visual Use Case Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualUseCaseElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl#getVisualUseCaseElement()
	 * @generated
	 */
	int VISUAL_USE_CASE_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__LOCATION = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__BOUNDS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__PARENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__DIAGRAM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__ELEMENTS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__CONNECTIONS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__IS_SKETCHY = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__NAME = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__VISUAL_ID = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__STITCH_FROM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__STITCH_TO = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__SPECIFICATION_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT__IS_LINKED_TO_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT;

	/**
	 * The number of structural features of the '<em>Visual Use Case Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_USE_CASE_ELEMENT_FEATURE_COUNT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualSystemBoundaryElementImpl <em>Visual System Boundary Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualSystemBoundaryElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl#getVisualSystemBoundaryElement()
	 * @generated
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual System Boundary Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SYSTEM_BOUNDARY_ELEMENT_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualActorElement <em>Visual Actor Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Actor Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualActorElement
	 * @generated
	 */
	EClass getVisualActorElement();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement <em>Visual Use Case Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Use Case Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement
	 * @generated
	 */
	EClass getVisualUseCaseElement();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualSystemBoundaryElement <em>Visual System Boundary Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual System Boundary Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualSystemBoundaryElement
	 * @generated
	 */
	EClass getVisualSystemBoundaryElement();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UsecaseFactory getUsecaseFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualActorElementImpl <em>Visual Actor Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualActorElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl#getVisualActorElement()
		 * @generated
		 */
		EClass VISUAL_ACTOR_ELEMENT = eINSTANCE.getVisualActorElement();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualUseCaseElementImpl <em>Visual Use Case Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualUseCaseElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl#getVisualUseCaseElement()
		 * @generated
		 */
		EClass VISUAL_USE_CASE_ELEMENT = eINSTANCE.getVisualUseCaseElement();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualSystemBoundaryElementImpl <em>Visual System Boundary Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.VisualSystemBoundaryElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl#getVisualSystemBoundaryElement()
		 * @generated
		 */
		EClass VISUAL_SYSTEM_BOUNDARY_ELEMENT = eINSTANCE.getVisualSystemBoundaryElement();

	}

} //UsecasePackage
