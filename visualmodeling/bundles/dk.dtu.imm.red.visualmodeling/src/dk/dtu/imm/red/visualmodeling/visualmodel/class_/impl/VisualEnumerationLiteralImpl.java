/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Enumeration Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualEnumerationLiteralImpl extends VisualElementImpl implements VisualEnumerationLiteral {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualEnumerationLiteralImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassPackage.Literals.VISUAL_ENUMERATION_LITERAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		return "  " + getName();
	}

} //VisualEnumerationLiteralImpl
