/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualSystemBoundaryElement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual System Boundary Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualSystemBoundaryElementImpl extends VisualElementImpl implements VisualSystemBoundaryElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualSystemBoundaryElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasePackage.Literals.VISUAL_SYSTEM_BOUNDARY_ELEMENT;
	}

} //VisualSystemBoundaryElementImpl
