/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.ActorFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualActorElement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Actor Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualActorElementImpl extends VisualSpecificationElementImpl implements VisualActorElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualActorElementImpl() {
		super();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public Class<?> getSpecificationElementType() {
		return Actor.class;
	}

	@Override
	public SpecificationElement createSpecificationElement(Group parent) {		
		Actor actor = ActorFactory.eINSTANCE.createActor();
		actor.setCreator(PreferenceUtil.getUserPreference_User());
		
		String str = (this.name == null) ? "" : this.name; 
		
		actor.setName(str);
		actor.setLabel(str);
		actor.setDescription("");
		
		if (parent != null) {
			actor.setParent(parent);
		}
		actor.save();
		
		
		return actor;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasePackage.Literals.VISUAL_ACTOR_ELEMENT;
	}

} //VisualActorElementImpl
