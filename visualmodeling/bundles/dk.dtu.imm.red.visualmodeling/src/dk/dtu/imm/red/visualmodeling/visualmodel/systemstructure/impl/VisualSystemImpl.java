/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.impl.FileImpl;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.impl.WorkspaceFactoryImpl;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationFactory;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.ConfigurationReference;
import dk.dtu.imm.red.specificationelements.system.GlueConnector;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.system.SystemFactory;
import dk.dtu.imm.red.specificationelements.system.SystemPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual System</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualSystemImpl extends VisualSpecificationElementImpl implements VisualSystem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualSystemImpl() {
		super();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public Class<?> getSpecificationElementType() {
		return System.class;
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public SpecificationElement createSpecificationElement(Group parent) {
		System system = SystemFactory.eINSTANCE.createSystem();
		system.setCreator(PreferenceUtil.getUserPreference_User());
		
		String str = (this.name == null) ? "" : this.name; 
		
		system.setName(str);
		system.setLabel(str);
		system.setDescription("");
		
		if (parent != null) {
			system.setParent(parent);
		}
		
		system.save();
		
		return system;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemstructurePackage.Literals.VISUAL_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void handleInternalConnection(VisualConnection connection) {
		if (!(connection.getSource() instanceof VisualSpecificationElement)
				|| !(connection.getTarget() instanceof VisualSpecificationElement)) {
			return;
		}
		
		VisualSpecificationElement source = (VisualSpecificationElement) connection.getSource();
		VisualSpecificationElement target = (VisualSpecificationElement) connection.getTarget();

		if (isGlueConnector(connection)) {
			System system = (System) getSpecificationElement();

			// Check if glue connector exists
			for (GlueConnector glueConnector : system.getGlueConnectors()) {
				if ((glueConnector.getSource() == source.getSpecificationElement()
						|| glueConnector.getSource() == target.getSpecificationElement())
						&& (glueConnector.getTarget() == source.getSpecificationElement()
								|| glueConnector.getTarget() == target.getSpecificationElement())) {
					return;
				}
			}

			GlueConnector glueConnector = SystemFactory.eINSTANCE.createGlueConnector();
			glueConnector.setSource((Port) source.getSpecificationElement());
			glueConnector.setTarget((Port) target.getSpecificationElement());

			system.getGlueConnectors().add(glueConnector);
			
			return;
		}
		
		if (getSpecificationElement() != null
				&& getSpecificationElement() instanceof System) {
			Configuration configuration = getRelevantConfiguration((System) getSpecificationElement());

			if (configuration != null
					&& connection.getSpecificationElement() instanceof Connector) {
				if (!configuration.getInternalConnectors().contains(connection.getSpecificationElement())) {
					Connector connector = (Connector) connection.getSpecificationElement();
					
					configuration.getInternalConnectors().add(connector);
					
					DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
					listener.setHostingObject(configuration);
					listener.setHostingFeature(ConfigurationPackage.eINSTANCE.getConfiguration_InternalConnectors());
					listener.setHandledObject(connector);
					connector.getDeletionListeners().add(listener);
				}
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isGlueConnector(VisualConnection connection) {
		if (!(connection.getSource() instanceof VisualSpecificationElement)
				|| !(connection.getTarget() instanceof VisualSpecificationElement)) {
			return false;
		}
		
		VisualSpecificationElement source = (VisualSpecificationElement) connection.getSource();
		VisualSpecificationElement target = (VisualSpecificationElement) connection.getTarget();
		
		if (getSpecificationElement() != null
				&& getSpecificationElement() instanceof System) {
			System system = (System) getSpecificationElement();
			
			if (system.getPorts().contains(source.getSpecificationElement())
					|| system.getPorts().contains(target.getSpecificationElement())) {
				
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateInternals() {
		if (getSpecificationElement() == null
				|| !(getSpecificationElement() instanceof System)) {
			return;
		}
		
		System system = (System) getSpecificationElement();
		
		for (IVisualElement element : getElements()) {
			if (element instanceof VisualSpecificationElement
					&& ((VisualSpecificationElement) element).getSpecificationElement() != null) {
				SpecificationElement childSpecificationElement = ((VisualSpecificationElement) element).getSpecificationElement();
				Configuration configuration = null;
				
				if (childSpecificationElement instanceof Port) {
					system.getPorts().add((Port) childSpecificationElement);
					
					DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
					listener.setHostingObject(system);
					listener.setHostingFeature(SystemPackage.eINSTANCE.getSystem_Ports());
					listener.setHandledObject(childSpecificationElement);
					childSpecificationElement.getDeletionListeners().add(listener);
				}
				else if (childSpecificationElement instanceof Actor) {
					if (configuration == null) {
						configuration = getRelevantConfiguration(system);
					}
					
					Actor childActor = (Actor) childSpecificationElement;
					configuration.getParts().add(childActor);
					
					DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
					listener.setHostingObject(configuration);
					listener.setHostingFeature(ConfigurationPackage.eINSTANCE.getConfiguration_Parts());
					listener.setHandledObject(childActor);
					childActor.getDeletionListeners().add(listener);
				}
			}
		}
		
		system.save();
	}

	/**
	 * @generated NOT
	 */
	private Configuration getRelevantConfiguration(System system) {
		if (system != null) {
			String diagramName = getDiagramName();
			
			for (ConfigurationReference configurationRef : system.getContainedConfigurations()) {
				if (configurationRef.getLabel() != null
						&& diagramName != null
						&& configurationRef.getLabel().equals(diagramName)) {
					return configurationRef.getConfiguration();
				}
			}
			
			Configuration newConfiguration = createNewConfiguration();
			
			ConfigurationReference configurationRef = SystemFactory.eINSTANCE.createConfigurationReference();
			configurationRef.setConfiguration(newConfiguration);
			configurationRef.setLabel(getDiagramName());
			
			system.getContainedConfigurations().add(configurationRef);
			
			DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
			listener.setHostingObject(system);
			listener.setHostingFeature(SystemPackage.eINSTANCE.getSystem_ContainedConfigurations());
			listener.setHandledObject(configurationRef);
			newConfiguration.getDeletionListeners().add(listener);
			
			return newConfiguration;
		}
		
		return null;
	}
	
	private Configuration createNewConfiguration() {
		Configuration configuration = ConfigurationFactory.eINSTANCE.createConfiguration();
		configuration.setCreator(PreferenceUtil.getUserPreference_User());
		
		String str = (this.name == null) ? "New configuration" : this.name + " " + getDiagramName(); 
		
		configuration.setName(str);
		configuration.setLabel(str);
		configuration.setDescription("");
		configuration.setElementKind(SystemStructureDiagramKind.INTERNAL_STRUCTURE.getLiteral());
		configuration.setParent(getFirstFile(WorkspaceFactoryImpl.eINSTANCE.getWorkspaceInstance()));
		configuration.save();
		
		return configuration;
	}
	
	/**
	 * @generated NOT
	 */
	private String getDiagramName() {
		IVisualElement parent = getParent();
		
		while (!(parent instanceof VisualDiagram)) {
			parent = parent.getParent();
		}
		
		return parent.getName();
	}
	
	/**
	 * @generated NOT
	 */
	private Group getFirstFile(Workspace workspace) {
		Group parent = null;
		for (Element e : workspace.getContents()) {
			if (e instanceof FileImpl) {
				parent = (Group) e;
				break;
			}
		}
		return parent;		
	}

} //VisualSystemImpl
