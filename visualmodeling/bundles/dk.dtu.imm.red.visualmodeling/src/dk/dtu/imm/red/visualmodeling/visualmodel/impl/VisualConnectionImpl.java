/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.impl;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.ConnectorFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Connection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getSource <em>Source</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getDirection <em>Direction</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getBendpoints <em>Bendpoints</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getLineStyle <em>Line Style</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getSourceDecoration <em>Source Decoration</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getTargetDecoration <em>Target Decoration</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getValidationMessage <em>Validation Message</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getSourceMultiplicity <em>Source Multiplicity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getTargetMultiplicity <em>Target Multiplicity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getRounting <em>Rounting</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getVisualID <em>Visual ID</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getStitchFrom <em>Stitch From</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getStitchTo <em>Stitch To</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#getSpecificationElement <em>Specification Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl#isIsLinkedToElement <em>Is Linked To Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VisualConnectionImpl extends EObjectImpl implements VisualConnection {
	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected IVisualElement source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected IVisualElement target;

	/**
	 * The default value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected static final ConnectionDirection DIRECTION_EDEFAULT = ConnectionDirection.NONE;

	/**
	 * The cached value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected ConnectionDirection direction = DIRECTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBendpoints() <em>Bendpoints</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBendpoints()
	 * @generated
	 * @ordered
	 */
	protected EList<Point> bendpoints;

	/**
	 * The default value of the '{@link #getLineStyle() <em>Line Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineStyle()
	 * @generated
	 * @ordered
	 */
	protected static final ConnectionLineStyle LINE_STYLE_EDEFAULT = ConnectionLineStyle.DEFAULT;

	/**
	 * The cached value of the '{@link #getLineStyle() <em>Line Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineStyle()
	 * @generated
	 * @ordered
	 */
	protected ConnectionLineStyle lineStyle = LINE_STYLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceDecoration() <em>Source Decoration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDecoration()
	 * @generated
	 * @ordered
	 */
	protected static final ConnectionDecoration SOURCE_DECORATION_EDEFAULT = ConnectionDecoration.NONE;

	/**
	 * The cached value of the '{@link #getSourceDecoration() <em>Source Decoration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDecoration()
	 * @generated
	 * @ordered
	 */
	protected ConnectionDecoration sourceDecoration = SOURCE_DECORATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetDecoration() <em>Target Decoration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDecoration()
	 * @generated
	 * @ordered
	 */
	protected static final ConnectionDecoration TARGET_DECORATION_EDEFAULT = ConnectionDecoration.NONE;

	/**
	 * The cached value of the '{@link #getTargetDecoration() <em>Target Decoration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDecoration()
	 * @generated
	 * @ordered
	 */
	protected ConnectionDecoration targetDecoration = TARGET_DECORATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getValidationMessage() <em>Validation Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidationMessage()
	 * @generated
	 * @ordered
	 */
	protected static final String VALIDATION_MESSAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValidationMessage() <em>Validation Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidationMessage()
	 * @generated
	 * @ordered
	 */
	protected String validationMessage = VALIDATION_MESSAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceMultiplicity() <em>Source Multiplicity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_MULTIPLICITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceMultiplicity() <em>Source Multiplicity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected String sourceMultiplicity = SOURCE_MULTIPLICITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetMultiplicity() <em>Target Multiplicity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_MULTIPLICITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetMultiplicity() <em>Target Multiplicity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected String targetMultiplicity = TARGET_MULTIPLICITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRounting() <em>Rounting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRounting()
	 * @generated
	 * @ordered
	 */
	protected static final ConnectionRouting ROUNTING_EDEFAULT = ConnectionRouting.NONE;

	/**
	 * The cached value of the '{@link #getRounting() <em>Rounting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRounting()
	 * @generated
	 * @ordered
	 */
	protected ConnectionRouting rounting = ROUNTING_EDEFAULT;

	/**
	 * The default value of the '{@link #getVisualID() <em>Visual ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisualID()
	 * @generated
	 * @ordered
	 */
	protected static final String VISUAL_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVisualID() <em>Visual ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisualID()
	 * @generated
	 * @ordered
	 */
	protected String visualID = VISUAL_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStitchFrom() <em>Stitch From</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStitchFrom()
	 * @generated
	 * @ordered
	 */
	protected EList<StitchConnectionRelationship> stitchFrom;

	/**
	 * The cached value of the '{@link #getStitchTo() <em>Stitch To</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStitchTo()
	 * @generated
	 * @ordered
	 */
	protected EList<StitchConnectionRelationship> stitchTo;

	/**
	 * The cached value of the '{@link #getSpecificationElement() <em>Specification Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecificationElement()
	 * @generated
	 * @ordered
	 */
	protected SpecificationElement specificationElement;

	/**
	 * The default value of the '{@link #isIsLinkedToElement() <em>Is Linked To Element</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsLinkedToElement()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_LINKED_TO_ELEMENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsLinkedToElement() <em>Is Linked To Element</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsLinkedToElement()
	 * @generated
	 * @ordered
	 */
	protected boolean isLinkedToElement = IS_LINKED_TO_ELEMENT_EDEFAULT;

	/**
	 * This is true if the Is Linked To Element attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isLinkedToElementESet;
	
	/**
	 * @generated NOT
	 */
	private DeletionListener specificationElementDeletionListener;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualConnectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return visualmodelPackage.Literals.VISUAL_CONNECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = (IVisualElement)eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, visualmodelPackage.VISUAL_CONNECTION__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(IVisualElement newSource) {
		IVisualElement oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (IVisualElement)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, visualmodelPackage.VISUAL_CONNECTION__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(IVisualElement newTarget) {
		IVisualElement oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionDirection getDirection() {
		return direction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirection(ConnectionDirection newDirection) {
		ConnectionDirection oldDirection = direction;
		direction = newDirection == null ? DIRECTION_EDEFAULT : newDirection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__DIRECTION, oldDirection, direction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Point> getBendpoints() {
		if (bendpoints == null) {
			bendpoints = new EDataTypeUniqueEList<Point>(Point.class, this, visualmodelPackage.VISUAL_CONNECTION__BENDPOINTS);
		}
		return bendpoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionLineStyle getLineStyle() {
		return lineStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLineStyle(ConnectionLineStyle newLineStyle) {
		ConnectionLineStyle oldLineStyle = lineStyle;
		lineStyle = newLineStyle == null ? LINE_STYLE_EDEFAULT : newLineStyle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__LINE_STYLE, oldLineStyle, lineStyle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionDecoration getSourceDecoration() {
		return sourceDecoration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceDecoration(ConnectionDecoration newSourceDecoration) {
		ConnectionDecoration oldSourceDecoration = sourceDecoration;
		sourceDecoration = newSourceDecoration == null ? SOURCE_DECORATION_EDEFAULT : newSourceDecoration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__SOURCE_DECORATION, oldSourceDecoration, sourceDecoration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionDecoration getTargetDecoration() {
		return targetDecoration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetDecoration(ConnectionDecoration newTargetDecoration) {
		ConnectionDecoration oldTargetDecoration = targetDecoration;
		targetDecoration = newTargetDecoration == null ? TARGET_DECORATION_EDEFAULT : newTargetDecoration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__TARGET_DECORATION, oldTargetDecoration, targetDecoration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValidationMessage() {
		return validationMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValidationMessage(String newValidationMessage) {
		String oldValidationMessage = validationMessage;
		validationMessage = newValidationMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__VALIDATION_MESSAGE, oldValidationMessage, validationMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceMultiplicity() {
		return sourceMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceMultiplicity(String newSourceMultiplicity) {
		String oldSourceMultiplicity = sourceMultiplicity;
		sourceMultiplicity = newSourceMultiplicity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__SOURCE_MULTIPLICITY, oldSourceMultiplicity, sourceMultiplicity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTargetMultiplicity() {
		return targetMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetMultiplicity(String newTargetMultiplicity) {
		String oldTargetMultiplicity = targetMultiplicity;
		targetMultiplicity = newTargetMultiplicity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__TARGET_MULTIPLICITY, oldTargetMultiplicity, targetMultiplicity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionRouting getRounting() {
		return rounting;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRounting(ConnectionRouting newRounting) {
		ConnectionRouting oldRounting = rounting;
		rounting = newRounting == null ? ROUNTING_EDEFAULT : newRounting;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__ROUNTING, oldRounting, rounting));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVisualID() {
		return visualID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisualID(String newVisualID) {
		String oldVisualID = visualID;
		visualID = newVisualID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__VISUAL_ID, oldVisualID, visualID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StitchConnectionRelationship> getStitchFrom() {
		if (stitchFrom == null) {
			stitchFrom = new EObjectContainmentWithInverseEList.Resolving<StitchConnectionRelationship>(StitchConnectionRelationship.class, this, visualmodelPackage.VISUAL_CONNECTION__STITCH_FROM, visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT);
		}
		return stitchFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StitchConnectionRelationship> getStitchTo() {
		if (stitchTo == null) {
			stitchTo = new EObjectWithInverseResolvingEList<StitchConnectionRelationship>(StitchConnectionRelationship.class, this, visualmodelPackage.VISUAL_CONNECTION__STITCH_TO, visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN);
		}
		return stitchTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecificationElement getSpecificationElement() {
		if (specificationElement != null && specificationElement.eIsProxy()) {
			InternalEObject oldSpecificationElement = (InternalEObject)specificationElement;
			specificationElement = (SpecificationElement)eResolveProxy(oldSpecificationElement);
			if (specificationElement != oldSpecificationElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, visualmodelPackage.VISUAL_CONNECTION__SPECIFICATION_ELEMENT, oldSpecificationElement, specificationElement));
			}
		}
		return specificationElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecificationElement basicGetSpecificationElement() {
		return specificationElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setSpecificationElement(SpecificationElement newSpecificationElement) {
		
		if(newSpecificationElement == null)
		{
			setName("");
			setIsLinkedToElement(false);
		}
		else
		{
			setName(newSpecificationElement.getName());
			setIsLinkedToElement(true);
		}
		
		SpecificationElement oldSpecificationElement = specificationElement;
		specificationElement = newSpecificationElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__SPECIFICATION_ELEMENT, oldSpecificationElement, specificationElement));

		if (specificationElement != null) {
			final SpecificationElement listenedElement = specificationElement;
			
			specificationElementDeletionListener = ElementFactory.eINSTANCE.createDeletionListener();
			specificationElementDeletionListener.setHostingObject(this);
			specificationElementDeletionListener.setHostingFeature(visualmodelPackage.eINSTANCE.getVisualConnection_SpecificationElement());
			specificationElementDeletionListener.setHandledObject(listenedElement);
			listenedElement.getDeletionListeners().add(specificationElementDeletionListener);
			
			// Delete any old listeners to avoid unexpected behavior
			Iterator<DeletionListener> deletionListenerIterator = specificationElement.getDeletionListeners().iterator();
			while (deletionListenerIterator.hasNext()) {
				DeletionListener listener = deletionListenerIterator.next();
				if (listener.getHostingObject() == this) 
					deletionListenerIterator.remove();
			}
			
			specificationElement.getDeletionListeners().add(specificationElementDeletionListener);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsLinkedToElement() {
		return isLinkedToElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsLinkedToElement(boolean newIsLinkedToElement) {
		boolean oldIsLinkedToElement = isLinkedToElement;
		isLinkedToElement = newIsLinkedToElement;
		boolean oldIsLinkedToElementESet = isLinkedToElementESet;
		isLinkedToElementESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT, oldIsLinkedToElement, isLinkedToElement, !oldIsLinkedToElementESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsLinkedToElement() {
		boolean oldIsLinkedToElement = isLinkedToElement;
		boolean oldIsLinkedToElementESet = isLinkedToElementESet;
		isLinkedToElement = IS_LINKED_TO_ELEMENT_EDEFAULT;
		isLinkedToElementESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, visualmodelPackage.VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT, oldIsLinkedToElement, IS_LINKED_TO_ELEMENT_EDEFAULT, oldIsLinkedToElementESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsLinkedToElement() {
		return isLinkedToElementESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SpecificationElement createSpecificationConnection(Group parent, SpecificationElement source, SpecificationElement target) {
		Connector connector = ConnectorFactory.eINSTANCE.createConnector();
		connector.setCreator(PreferenceUtil.getUserPreference_User());

		String name = (this.name == null || this.name.isEmpty()) ? 
				getSource().getName() + "-" + getTarget().getName() + " connector" 
				: this.name; 
		
		connector.setLabel(name);
		connector.setName(name);
		connector.setDescription("");

		if (parent != null) {
			connector.setParent(parent);
		}
		
		connector.getSource().add((Actor) source);
		connector.getTarget().add((Actor) target);
		
		connector.save();
		
		return connector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_FROM:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStitchFrom()).basicAdd(otherEnd, msgs);
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_TO:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStitchTo()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_FROM:
				return ((InternalEList<?>)getStitchFrom()).basicRemove(otherEnd, msgs);
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_TO:
				return ((InternalEList<?>)getStitchTo()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case visualmodelPackage.VISUAL_CONNECTION__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case visualmodelPackage.VISUAL_CONNECTION__DIRECTION:
				return getDirection();
			case visualmodelPackage.VISUAL_CONNECTION__BENDPOINTS:
				return getBendpoints();
			case visualmodelPackage.VISUAL_CONNECTION__LINE_STYLE:
				return getLineStyle();
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_DECORATION:
				return getSourceDecoration();
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_DECORATION:
				return getTargetDecoration();
			case visualmodelPackage.VISUAL_CONNECTION__VALIDATION_MESSAGE:
				return getValidationMessage();
			case visualmodelPackage.VISUAL_CONNECTION__NAME:
				return getName();
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_MULTIPLICITY:
				return getSourceMultiplicity();
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_MULTIPLICITY:
				return getTargetMultiplicity();
			case visualmodelPackage.VISUAL_CONNECTION__TYPE:
				return getType();
			case visualmodelPackage.VISUAL_CONNECTION__ROUNTING:
				return getRounting();
			case visualmodelPackage.VISUAL_CONNECTION__VISUAL_ID:
				return getVisualID();
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_FROM:
				return getStitchFrom();
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_TO:
				return getStitchTo();
			case visualmodelPackage.VISUAL_CONNECTION__SPECIFICATION_ELEMENT:
				if (resolve) return getSpecificationElement();
				return basicGetSpecificationElement();
			case visualmodelPackage.VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT:
				return isIsLinkedToElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE:
				setSource((IVisualElement)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__TARGET:
				setTarget((IVisualElement)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__DIRECTION:
				setDirection((ConnectionDirection)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__BENDPOINTS:
				getBendpoints().clear();
				getBendpoints().addAll((Collection<? extends Point>)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__LINE_STYLE:
				setLineStyle((ConnectionLineStyle)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_DECORATION:
				setSourceDecoration((ConnectionDecoration)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_DECORATION:
				setTargetDecoration((ConnectionDecoration)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__VALIDATION_MESSAGE:
				setValidationMessage((String)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__NAME:
				setName((String)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_MULTIPLICITY:
				setSourceMultiplicity((String)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_MULTIPLICITY:
				setTargetMultiplicity((String)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__TYPE:
				setType((String)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__ROUNTING:
				setRounting((ConnectionRouting)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__VISUAL_ID:
				setVisualID((String)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_FROM:
				getStitchFrom().clear();
				getStitchFrom().addAll((Collection<? extends StitchConnectionRelationship>)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_TO:
				getStitchTo().clear();
				getStitchTo().addAll((Collection<? extends StitchConnectionRelationship>)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__SPECIFICATION_ELEMENT:
				setSpecificationElement((SpecificationElement)newValue);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT:
				setIsLinkedToElement((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE:
				setSource((IVisualElement)null);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__TARGET:
				setTarget((IVisualElement)null);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__DIRECTION:
				setDirection(DIRECTION_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__BENDPOINTS:
				getBendpoints().clear();
				return;
			case visualmodelPackage.VISUAL_CONNECTION__LINE_STYLE:
				setLineStyle(LINE_STYLE_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_DECORATION:
				setSourceDecoration(SOURCE_DECORATION_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_DECORATION:
				setTargetDecoration(TARGET_DECORATION_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__VALIDATION_MESSAGE:
				setValidationMessage(VALIDATION_MESSAGE_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_MULTIPLICITY:
				setSourceMultiplicity(SOURCE_MULTIPLICITY_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_MULTIPLICITY:
				setTargetMultiplicity(TARGET_MULTIPLICITY_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__ROUNTING:
				setRounting(ROUNTING_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__VISUAL_ID:
				setVisualID(VISUAL_ID_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_FROM:
				getStitchFrom().clear();
				return;
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_TO:
				getStitchTo().clear();
				return;
			case visualmodelPackage.VISUAL_CONNECTION__SPECIFICATION_ELEMENT:
				setSpecificationElement((SpecificationElement)null);
				return;
			case visualmodelPackage.VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT:
				unsetIsLinkedToElement();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE:
				return source != null;
			case visualmodelPackage.VISUAL_CONNECTION__TARGET:
				return target != null;
			case visualmodelPackage.VISUAL_CONNECTION__DIRECTION:
				return direction != DIRECTION_EDEFAULT;
			case visualmodelPackage.VISUAL_CONNECTION__BENDPOINTS:
				return bendpoints != null && !bendpoints.isEmpty();
			case visualmodelPackage.VISUAL_CONNECTION__LINE_STYLE:
				return lineStyle != LINE_STYLE_EDEFAULT;
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_DECORATION:
				return sourceDecoration != SOURCE_DECORATION_EDEFAULT;
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_DECORATION:
				return targetDecoration != TARGET_DECORATION_EDEFAULT;
			case visualmodelPackage.VISUAL_CONNECTION__VALIDATION_MESSAGE:
				return VALIDATION_MESSAGE_EDEFAULT == null ? validationMessage != null : !VALIDATION_MESSAGE_EDEFAULT.equals(validationMessage);
			case visualmodelPackage.VISUAL_CONNECTION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case visualmodelPackage.VISUAL_CONNECTION__SOURCE_MULTIPLICITY:
				return SOURCE_MULTIPLICITY_EDEFAULT == null ? sourceMultiplicity != null : !SOURCE_MULTIPLICITY_EDEFAULT.equals(sourceMultiplicity);
			case visualmodelPackage.VISUAL_CONNECTION__TARGET_MULTIPLICITY:
				return TARGET_MULTIPLICITY_EDEFAULT == null ? targetMultiplicity != null : !TARGET_MULTIPLICITY_EDEFAULT.equals(targetMultiplicity);
			case visualmodelPackage.VISUAL_CONNECTION__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case visualmodelPackage.VISUAL_CONNECTION__ROUNTING:
				return rounting != ROUNTING_EDEFAULT;
			case visualmodelPackage.VISUAL_CONNECTION__VISUAL_ID:
				return VISUAL_ID_EDEFAULT == null ? visualID != null : !VISUAL_ID_EDEFAULT.equals(visualID);
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_FROM:
				return stitchFrom != null && !stitchFrom.isEmpty();
			case visualmodelPackage.VISUAL_CONNECTION__STITCH_TO:
				return stitchTo != null && !stitchTo.isEmpty();
			case visualmodelPackage.VISUAL_CONNECTION__SPECIFICATION_ELEMENT:
				return specificationElement != null;
			case visualmodelPackage.VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT:
				return isSetIsLinkedToElement();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Direction: ");
		result.append(direction);
		result.append(", Bendpoints: ");
		result.append(bendpoints);
		result.append(", LineStyle: ");
		result.append(lineStyle);
		result.append(", SourceDecoration: ");
		result.append(sourceDecoration);
		result.append(", TargetDecoration: ");
		result.append(targetDecoration);
		result.append(", ValidationMessage: ");
		result.append(validationMessage);
		result.append(", Name: ");
		result.append(name);
		result.append(", SourceMultiplicity: ");
		result.append(sourceMultiplicity);
		result.append(", TargetMultiplicity: ");
		result.append(targetMultiplicity);
		result.append(", Type: ");
		result.append(type);
		result.append(", Rounting: ");
		result.append(rounting);
		result.append(", visualID: ");
		result.append(visualID);
		result.append(", IsLinkedToElement: ");
		if (isLinkedToElementESet) result.append(isLinkedToElement); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //VisualConnectionImpl
