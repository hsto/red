/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualFlowFinalNode;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Flow Final Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualFlowFinalNodeImpl extends VisualElementImpl implements VisualFlowFinalNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualFlowFinalNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActivityPackage.Literals.VISUAL_FLOW_FINAL_NODE;
	}

} //VisualFlowFinalNodeImpl
