/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IVisitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisitor()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void visit(EObject element);

} // IVisitor
