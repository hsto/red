/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Decision Merge Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage#getVisualDecisionMergeNode()
 * @model
 * @generated
 */
public interface VisualDecisionMergeNode extends VisualElement {
} // VisualDecisionMergeNode
