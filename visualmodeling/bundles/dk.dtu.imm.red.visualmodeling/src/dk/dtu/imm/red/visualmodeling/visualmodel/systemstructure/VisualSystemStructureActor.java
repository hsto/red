/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual System Structure Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor#getActorKind <em>Actor Kind</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage#getVisualSystemStructureActor()
 * @model
 * @generated
 */
public interface VisualSystemStructureActor extends VisualSpecificationElement {
	/**
	 * Returns the value of the '<em><b>Actor Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actor Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actor Kind</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind
	 * @see #setActorKind(SystemStructureActorKind)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage#getVisualSystemStructureActor_ActorKind()
	 * @model
	 * @generated
	 */
	SystemStructureActorKind getActorKind();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor#getActorKind <em>Actor Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actor Kind</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind
	 * @see #getActorKind()
	 * @generated
	 */
	void setActorKind(SystemStructureActorKind value);

} // VisualSystemStructureActor
