/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Package Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualPackageElement()
 * @model
 * @generated
 */
public interface VisualPackageElement extends VisualElement {
} // VisualPackageElement
