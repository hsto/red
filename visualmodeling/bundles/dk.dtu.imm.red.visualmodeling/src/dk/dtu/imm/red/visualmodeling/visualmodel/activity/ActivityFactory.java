/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage
 * @generated
 */
public interface ActivityFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ActivityFactory eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Visual Action Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Action Node</em>'.
	 * @generated
	 */
	VisualActionNode createVisualActionNode();

	/**
	 * Returns a new object of class '<em>Visual Initial Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Initial Node</em>'.
	 * @generated
	 */
	VisualInitialNode createVisualInitialNode();

	/**
	 * Returns a new object of class '<em>Visual Activity Final Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Activity Final Node</em>'.
	 * @generated
	 */
	VisualActivityFinalNode createVisualActivityFinalNode();

	/**
	 * Returns a new object of class '<em>Visual Flow Final Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Flow Final Node</em>'.
	 * @generated
	 */
	VisualFlowFinalNode createVisualFlowFinalNode();

	/**
	 * Returns a new object of class '<em>Visual Decision Merge Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Decision Merge Node</em>'.
	 * @generated
	 */
	VisualDecisionMergeNode createVisualDecisionMergeNode();

	/**
	 * Returns a new object of class '<em>Visual Fork Join Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Fork Join Node</em>'.
	 * @generated
	 */
	VisualForkJoinNode createVisualForkJoinNode();

	/**
	 * Returns a new object of class '<em>Visual Send Signal Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Send Signal Node</em>'.
	 * @generated
	 */
	VisualSendSignalNode createVisualSendSignalNode();

	/**
	 * Returns a new object of class '<em>Visual Receive Signal Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Receive Signal Node</em>'.
	 * @generated
	 */
	VisualReceiveSignalNode createVisualReceiveSignalNode();

	/**
	 * Returns a new object of class '<em>Visual Object Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Object Node</em>'.
	 * @generated
	 */
	VisualObjectNode createVisualObjectNode();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ActivityPackage getActivityPackage();

} //ActivityFactory
