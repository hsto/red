/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Class Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualClassOperation()
 * @model
 * @generated
 */
public interface VisualClassOperation extends VisualElement {
	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility
	 * @see #setVisibility(Visibility)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualClassOperation_Visibility()
	 * @model
	 * @generated
	 */
	Visibility getVisibility();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(Visibility value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(DataType)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualClassOperation_Type()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	DataType getType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(DataType value);

	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualClassOperation_Multiplicity()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference.
	 * @see #setParameters(DataType)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualClassOperation_Parameters()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	DataType getParameters();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getParameters <em>Parameters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameters</em>' containment reference.
	 * @see #getParameters()
	 * @generated
	 */
	void setParameters(DataType value);

	/**
	 * @generated NOT
	 */
	public void setValues(VisualClassOperation newValue);
} // VisualClassOperation
