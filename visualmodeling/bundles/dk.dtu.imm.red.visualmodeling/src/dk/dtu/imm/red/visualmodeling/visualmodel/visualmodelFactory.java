/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage
 * @generated
 */
public interface visualmodelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	visualmodelFactory eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Diagram</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Diagram</em>'.
	 * @generated
	 */
	Diagram createDiagram();

	/**
	 * Returns a new object of class '<em>Visual Diagram</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Diagram</em>'.
	 * @generated
	 */
	VisualDiagram createVisualDiagram();

	/**
	 * Returns a new object of class '<em>Visual Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Element</em>'.
	 * @generated
	 */
	VisualElement createVisualElement();

	/**
	 * Returns a new object of class '<em>Visual Connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Connection</em>'.
	 * @generated
	 */
	VisualConnection createVisualConnection();

	/**
	 * Returns a new object of class '<em>Note</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Note</em>'.
	 * @generated
	 */
	Note createNote();

	/**
	 * Returns a new object of class '<em>Visual Generic Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Generic Element</em>'.
	 * @generated
	 */
	VisualGenericElement createVisualGenericElement();

	/**
	 * Returns a new object of class '<em>Stitch Element Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stitch Element Relationship</em>'.
	 * @generated
	 */
	StitchElementRelationship createStitchElementRelationship();

	/**
	 * Returns a new object of class '<em>Stitch Connection Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stitch Connection Relationship</em>'.
	 * @generated
	 */
	StitchConnectionRelationship createStitchConnectionRelationship();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	visualmodelPackage getvisualmodelPackage();

} //visualmodelFactory
