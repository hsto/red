/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl;

import dk.dtu.imm.red.specificationelements.goal.GoalPackage;

import dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;

import dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.ImporttypesPackage;

import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.impl.ImporttypesPackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructureFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor;

import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage;

import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SystemstructurePackageImpl extends EPackageImpl implements SystemstructurePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualSystemStructureActorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum systemStructureActorKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum systemStructureDiagramKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SystemstructurePackageImpl() {
		super(eNS_URI, SystemstructureFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SystemstructurePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SystemstructurePackage init() {
		if (isInited) return (SystemstructurePackage)EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI);

		// Obtain or create and register package
		SystemstructurePackageImpl theSystemstructurePackage = (SystemstructurePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SystemstructurePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SystemstructurePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		GoalPackage.eINSTANCE.eClass();
		ModelelementPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		visualmodelPackageImpl thevisualmodelPackage = (visualmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI) instanceof visualmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI) : visualmodelPackage.eINSTANCE);
		ImporttypesPackageImpl theImporttypesPackage = (ImporttypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) instanceof ImporttypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) : ImporttypesPackage.eINSTANCE);
		GoalPackageImpl theGoalPackage_1 = (GoalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage.eNS_URI) instanceof GoalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage.eNS_URI) : dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage.eINSTANCE);
		UsecasePackageImpl theUsecasePackage = (UsecasePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) instanceof UsecasePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) : UsecasePackage.eINSTANCE);
		ClassPackageImpl theClassPackage = (ClassPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI) instanceof ClassPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI) : ClassPackage.eINSTANCE);
		ActivityPackageImpl theActivityPackage = (ActivityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI) instanceof ActivityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI) : ActivityPackage.eINSTANCE);
		StatemachinePackageImpl theStatemachinePackage = (StatemachinePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) instanceof StatemachinePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) : StatemachinePackage.eINSTANCE);

		// Create package meta-data objects
		theSystemstructurePackage.createPackageContents();
		thevisualmodelPackage.createPackageContents();
		theImporttypesPackage.createPackageContents();
		theGoalPackage_1.createPackageContents();
		theUsecasePackage.createPackageContents();
		theClassPackage.createPackageContents();
		theActivityPackage.createPackageContents();
		theStatemachinePackage.createPackageContents();

		// Initialize created meta-data
		theSystemstructurePackage.initializePackageContents();
		thevisualmodelPackage.initializePackageContents();
		theImporttypesPackage.initializePackageContents();
		theGoalPackage_1.initializePackageContents();
		theUsecasePackage.initializePackageContents();
		theClassPackage.initializePackageContents();
		theActivityPackage.initializePackageContents();
		theStatemachinePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSystemstructurePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SystemstructurePackage.eNS_URI, theSystemstructurePackage);
		return theSystemstructurePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualSystemStructureActor() {
		return visualSystemStructureActorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualSystemStructureActor_ActorKind() {
		return (EAttribute)visualSystemStructureActorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualSystem() {
		return visualSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualPort() {
		return visualPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSystemStructureActorKind() {
		return systemStructureActorKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSystemStructureDiagramKind() {
		return systemStructureDiagramKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemstructureFactory getSystemstructureFactory() {
		return (SystemstructureFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		visualSystemStructureActorEClass = createEClass(VISUAL_SYSTEM_STRUCTURE_ACTOR);
		createEAttribute(visualSystemStructureActorEClass, VISUAL_SYSTEM_STRUCTURE_ACTOR__ACTOR_KIND);

		visualSystemEClass = createEClass(VISUAL_SYSTEM);

		visualPortEClass = createEClass(VISUAL_PORT);

		// Create enums
		systemStructureActorKindEEnum = createEEnum(SYSTEM_STRUCTURE_ACTOR_KIND);
		systemStructureDiagramKindEEnum = createEEnum(SYSTEM_STRUCTURE_DIAGRAM_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		visualmodelPackage thevisualmodelPackage = (visualmodelPackage)EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		visualSystemStructureActorEClass.getESuperTypes().add(thevisualmodelPackage.getVisualSpecificationElement());
		visualSystemEClass.getESuperTypes().add(thevisualmodelPackage.getVisualSpecificationElement());
		visualPortEClass.getESuperTypes().add(thevisualmodelPackage.getVisualSpecificationElement());

		// Initialize classes and features; add operations and parameters
		initEClass(visualSystemStructureActorEClass, VisualSystemStructureActor.class, "VisualSystemStructureActor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVisualSystemStructureActor_ActorKind(), this.getSystemStructureActorKind(), "actorKind", null, 0, 1, VisualSystemStructureActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(visualSystemEClass, VisualSystem.class, "VisualSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(visualSystemEClass, null, "handleInternalConnection", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thevisualmodelPackage.getVisualConnection(), "connection", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(visualSystemEClass, theEcorePackage.getEBoolean(), "isGlueConnector", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thevisualmodelPackage.getVisualConnection(), "connection", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(visualSystemEClass, null, "updateInternals", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(visualPortEClass, VisualPort.class, "VisualPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(systemStructureActorKindEEnum, SystemStructureActorKind.class, "SystemStructureActorKind");
		addEEnumLiteral(systemStructureActorKindEEnum, SystemStructureActorKind.PERSON);
		addEEnumLiteral(systemStructureActorKindEEnum, SystemStructureActorKind.ROLE);
		addEEnumLiteral(systemStructureActorKindEEnum, SystemStructureActorKind.GROUP);
		addEEnumLiteral(systemStructureActorKindEEnum, SystemStructureActorKind.TEAM);
		addEEnumLiteral(systemStructureActorKindEEnum, SystemStructureActorKind.ORGANISATION);

		initEEnum(systemStructureDiagramKindEEnum, SystemStructureDiagramKind.class, "SystemStructureDiagramKind");
		addEEnumLiteral(systemStructureDiagramKindEEnum, SystemStructureDiagramKind.SYSTEM_CONTEXT);
		addEEnumLiteral(systemStructureDiagramKindEEnum, SystemStructureDiagramKind.INTERNAL_STRUCTURE);
		addEEnumLiteral(systemStructureDiagramKindEEnum, SystemStructureDiagramKind.DOMAIN_ARCHITECTURE);
	}

} //SystemstructurePackageImpl
