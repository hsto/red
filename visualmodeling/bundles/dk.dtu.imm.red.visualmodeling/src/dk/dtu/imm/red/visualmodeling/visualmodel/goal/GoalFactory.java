/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.goal;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage
 * @generated
 */
public interface GoalFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GoalFactory eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Visual Goal Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Goal Element</em>'.
	 * @generated
	 */
	VisualGoalElement createVisualGoalElement();

	/**
	 * Returns a new object of class '<em>Visual Goal Layer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Goal Layer</em>'.
	 * @generated
	 */
	VisualGoalLayer createVisualGoalLayer();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GoalPackage getGoalPackage();

} //GoalFactory
