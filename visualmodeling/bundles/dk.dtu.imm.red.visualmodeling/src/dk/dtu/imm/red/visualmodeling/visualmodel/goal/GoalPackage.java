/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.goal;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalFactory
 * @model kind="package"
 * @generated
 */
public interface GoalPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "goal";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.visualmodeling.visualmodel.goal";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "goal";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GoalPackage eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalElementImpl <em>Visual Goal Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl#getVisualGoalElement()
	 * @generated
	 */
	int VISUAL_GOAL_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__LOCATION = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__BOUNDS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__PARENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__DIAGRAM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__ELEMENTS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__CONNECTIONS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__IS_SKETCHY = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__NAME = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__VISUAL_ID = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__STITCH_FROM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__STITCH_TO = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__SPECIFICATION_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT__IS_LINKED_TO_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT;

	/**
	 * The number of structural features of the '<em>Visual Goal Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_ELEMENT_FEATURE_COUNT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalLayerImpl <em>Visual Goal Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalLayerImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl#getVisualGoalLayer()
	 * @generated
	 */
	int VISUAL_GOAL_LAYER = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Below Layer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__BELOW_LAYER = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Above Layer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__ABOVE_LAYER = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Layer Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER__LAYER_KIND = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Visual Goal Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GOAL_LAYER_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalElement <em>Visual Goal Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Goal Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalElement
	 * @generated
	 */
	EClass getVisualGoalElement();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer <em>Visual Goal Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Goal Layer</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer
	 * @generated
	 */
	EClass getVisualGoalLayer();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getBelowLayer <em>Below Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Below Layer</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getBelowLayer()
	 * @see #getVisualGoalLayer()
	 * @generated
	 */
	EReference getVisualGoalLayer_BelowLayer();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getAboveLayer <em>Above Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Above Layer</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getAboveLayer()
	 * @see #getVisualGoalLayer()
	 * @generated
	 */
	EReference getVisualGoalLayer_AboveLayer();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getLayerKind <em>Layer Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Layer Kind</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getLayerKind()
	 * @see #getVisualGoalLayer()
	 * @generated
	 */
	EAttribute getVisualGoalLayer_LayerKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GoalFactory getGoalFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalElementImpl <em>Visual Goal Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl#getVisualGoalElement()
		 * @generated
		 */
		EClass VISUAL_GOAL_ELEMENT = eINSTANCE.getVisualGoalElement();
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalLayerImpl <em>Visual Goal Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalLayerImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl#getVisualGoalLayer()
		 * @generated
		 */
		EClass VISUAL_GOAL_LAYER = eINSTANCE.getVisualGoalLayer();
		/**
		 * The meta object literal for the '<em><b>Below Layer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_GOAL_LAYER__BELOW_LAYER = eINSTANCE.getVisualGoalLayer_BelowLayer();
		/**
		 * The meta object literal for the '<em><b>Above Layer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_GOAL_LAYER__ABOVE_LAYER = eINSTANCE.getVisualGoalLayer_AboveLayer();
		/**
		 * The meta object literal for the '<em><b>Layer Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_GOAL_LAYER__LAYER_KIND = eINSTANCE.getVisualGoalLayer_LayerKind();

	}

} //GoalPackage
