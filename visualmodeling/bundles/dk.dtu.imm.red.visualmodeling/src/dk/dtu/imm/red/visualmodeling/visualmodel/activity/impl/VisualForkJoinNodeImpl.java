/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualForkJoinNode;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Fork Join Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualForkJoinNodeImpl extends VisualElementImpl implements VisualForkJoinNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualForkJoinNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActivityPackage.Literals.VISUAL_FORK_JOIN_NODE;
	}

} //VisualForkJoinNodeImpl
