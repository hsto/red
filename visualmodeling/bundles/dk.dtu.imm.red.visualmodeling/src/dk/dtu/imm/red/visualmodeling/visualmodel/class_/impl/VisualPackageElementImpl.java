/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualPackageElement;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Package Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualPackageElementImpl extends VisualElementImpl implements VisualPackageElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualPackageElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassPackage.Literals.VISUAL_PACKAGE_ELEMENT;
	}

} //VisualPackageElementImpl
