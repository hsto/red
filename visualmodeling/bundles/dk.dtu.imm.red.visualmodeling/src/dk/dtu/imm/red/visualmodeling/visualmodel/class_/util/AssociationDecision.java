package dk.dtu.imm.red.visualmodeling.visualmodel.class_.util;

public enum AssociationDecision {
	IGNORE,
	EMBED,
	INLINE,
	LINK_SOURCE_TARGET,
	LINK_TARGET_SOURCE
}
