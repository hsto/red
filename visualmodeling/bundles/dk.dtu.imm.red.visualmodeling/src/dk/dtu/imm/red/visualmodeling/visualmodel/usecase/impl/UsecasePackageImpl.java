/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl;

import dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.ImporttypesPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.impl.ImporttypesPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecaseFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualActorElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualSystemBoundaryElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UsecasePackageImpl extends EPackageImpl implements UsecasePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualActorElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualUseCaseElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualSystemBoundaryElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UsecasePackageImpl() {
		super(eNS_URI, UsecaseFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UsecasePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UsecasePackage init() {
		if (isInited) return (UsecasePackage)EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI);

		// Obtain or create and register package
		UsecasePackageImpl theUsecasePackage = (UsecasePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UsecasePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UsecasePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		dk.dtu.imm.red.specificationelements.goal.GoalPackage.eINSTANCE.eClass();
		ModelelementPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		visualmodelPackageImpl thevisualmodelPackage = (visualmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI) instanceof visualmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI) : visualmodelPackage.eINSTANCE);
		ImporttypesPackageImpl theImporttypesPackage = (ImporttypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) instanceof ImporttypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) : ImporttypesPackage.eINSTANCE);
		GoalPackageImpl theGoalPackage_1 = (GoalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GoalPackage.eNS_URI) instanceof GoalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GoalPackage.eNS_URI) : GoalPackage.eINSTANCE);
		ClassPackageImpl theClassPackage = (ClassPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI) instanceof ClassPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI) : ClassPackage.eINSTANCE);
		ActivityPackageImpl theActivityPackage = (ActivityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI) instanceof ActivityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI) : ActivityPackage.eINSTANCE);
		StatemachinePackageImpl theStatemachinePackage = (StatemachinePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) instanceof StatemachinePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) : StatemachinePackage.eINSTANCE);
		SystemstructurePackageImpl theSystemstructurePackage = (SystemstructurePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI) instanceof SystemstructurePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI) : SystemstructurePackage.eINSTANCE);

		// Create package meta-data objects
		theUsecasePackage.createPackageContents();
		thevisualmodelPackage.createPackageContents();
		theImporttypesPackage.createPackageContents();
		theGoalPackage_1.createPackageContents();
		theClassPackage.createPackageContents();
		theActivityPackage.createPackageContents();
		theStatemachinePackage.createPackageContents();
		theSystemstructurePackage.createPackageContents();

		// Initialize created meta-data
		theUsecasePackage.initializePackageContents();
		thevisualmodelPackage.initializePackageContents();
		theImporttypesPackage.initializePackageContents();
		theGoalPackage_1.initializePackageContents();
		theClassPackage.initializePackageContents();
		theActivityPackage.initializePackageContents();
		theStatemachinePackage.initializePackageContents();
		theSystemstructurePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUsecasePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UsecasePackage.eNS_URI, theUsecasePackage);
		return theUsecasePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualActorElement() {
		return visualActorElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualUseCaseElement() {
		return visualUseCaseElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualSystemBoundaryElement() {
		return visualSystemBoundaryElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsecaseFactory getUsecaseFactory() {
		return (UsecaseFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		visualActorElementEClass = createEClass(VISUAL_ACTOR_ELEMENT);

		visualUseCaseElementEClass = createEClass(VISUAL_USE_CASE_ELEMENT);

		visualSystemBoundaryElementEClass = createEClass(VISUAL_SYSTEM_BOUNDARY_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		visualmodelPackage thevisualmodelPackage = (visualmodelPackage)EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		visualActorElementEClass.getESuperTypes().add(thevisualmodelPackage.getVisualSpecificationElement());
		visualUseCaseElementEClass.getESuperTypes().add(thevisualmodelPackage.getVisualSpecificationElement());
		visualSystemBoundaryElementEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());

		// Initialize classes and features; add operations and parameters
		initEClass(visualActorElementEClass, VisualActorElement.class, "VisualActorElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualUseCaseElementEClass, VisualUseCaseElement.class, "VisualUseCaseElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualSystemBoundaryElementEClass, VisualSystemBoundaryElement.class, "VisualSystemBoundaryElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //UsecasePackageImpl
