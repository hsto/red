/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage#getVisualPort()
 * @model
 * @generated
 */
public interface VisualPort extends VisualSpecificationElement {
} // VisualPort
