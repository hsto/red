/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory
 * @model kind="package"
 * @generated
 */
public interface visualmodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "visualmodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.visualmodeling";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "visualmodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	visualmodelPackage eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.DiagramImpl <em>Diagram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.DiagramImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getDiagram()
	 * @generated
	 */
	int DIAGRAM = 0;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__ICON_URI = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__ICON = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__LABEL = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__NAME = SpecificationelementsPackage.SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__ELEMENT_KIND = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__DESCRIPTION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__PRIORITY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__COMMENTLIST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__TIME_CREATED = SpecificationelementsPackage.SPECIFICATION_ELEMENT__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__LAST_MODIFIED = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__LAST_MODIFIED_BY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__CREATOR = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__VERSION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__VISIBLE_ID = SpecificationelementsPackage.SPECIFICATION_ELEMENT__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__UNIQUE_ID = SpecificationelementsPackage.SPECIFICATION_ELEMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__RELATES_TO = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__RELATED_BY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__PARENT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__URI = SpecificationelementsPackage.SPECIFICATION_ELEMENT__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__WORK_PACKAGE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__CHANGE_LIST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__RESPONSIBLE_USER = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__DEADLINE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__LOCK_STATUS = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__LOCK_PASSWORD = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__ESTIMATED_COMPLEXITY = SpecificationelementsPackage.SPECIFICATION_ELEMENT__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__COST = SpecificationelementsPackage.SPECIFICATION_ELEMENT__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__BENEFIT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__RISK = SpecificationelementsPackage.SPECIFICATION_ELEMENT__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__LIFE_CYCLE_PHASE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__STATE = SpecificationelementsPackage.SPECIFICATION_ELEMENT__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__MANAGEMENT_DECISION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__QA_ASSESSMENT = SpecificationelementsPackage.SPECIFICATION_ELEMENT__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__MANAGEMENT_DISCUSSION = SpecificationelementsPackage.SPECIFICATION_ELEMENT__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__DELETION_LISTENERS = SpecificationelementsPackage.SPECIFICATION_ELEMENT__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__QUANTITIES = SpecificationelementsPackage.SPECIFICATION_ELEMENT__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__CUSTOM_ATTRIBUTES = SpecificationelementsPackage.SPECIFICATION_ELEMENT__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Visual Diagram</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__VISUAL_DIAGRAM = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Merge Log</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__MERGE_LOG = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Stitch State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__STITCH_STATE = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_FEATURE_COUNT = SpecificationelementsPackage.SPECIFICATION_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement <em>IVisual Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getIVisualElement()
	 * @generated
	 */
	int IVISUAL_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__LOCATION = 0;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__BOUNDS = 1;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__PARENT = 2;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__DIAGRAM = 3;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__ELEMENTS = 4;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__CONNECTIONS = 5;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__IS_SKETCHY = 6;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__VALIDATION_MESSAGE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__NAME = 8;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__VISUAL_ID = 9;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__STITCH_FROM = 10;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT__STITCH_TO = 11;

	/**
	 * The number of structural features of the '<em>IVisual Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT_FEATURE_COUNT = 12;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection <em>IVisual Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getIVisualConnection()
	 * @generated
	 */
	int IVISUAL_CONNECTION = 2;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__TARGET = 1;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__DIRECTION = 2;

	/**
	 * The feature id for the '<em><b>Bendpoints</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__BENDPOINTS = 3;

	/**
	 * The feature id for the '<em><b>Line Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__LINE_STYLE = 4;

	/**
	 * The feature id for the '<em><b>Source Decoration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__SOURCE_DECORATION = 5;

	/**
	 * The feature id for the '<em><b>Target Decoration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__TARGET_DECORATION = 6;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__VALIDATION_MESSAGE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__NAME = 8;

	/**
	 * The feature id for the '<em><b>Source Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__SOURCE_MULTIPLICITY = 9;

	/**
	 * The feature id for the '<em><b>Target Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__TARGET_MULTIPLICITY = 10;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__TYPE = 11;

	/**
	 * The feature id for the '<em><b>Rounting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__ROUNTING = 12;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__VISUAL_ID = 13;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__STITCH_FROM = 14;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION__STITCH_TO = 15;

	/**
	 * The number of structural features of the '<em>IVisual Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION_FEATURE_COUNT = 16;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualDiagramImpl <em>Visual Diagram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualDiagramImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualDiagram()
	 * @generated
	 */
	int VISUAL_DIAGRAM = 3;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__LOCATION = IVISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__BOUNDS = IVISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__PARENT = IVISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__DIAGRAM = IVISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__ELEMENTS = IVISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__CONNECTIONS = IVISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__IS_SKETCHY = IVISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__VALIDATION_MESSAGE = IVISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__NAME = IVISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__VISUAL_ID = IVISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__STITCH_FROM = IVISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__STITCH_TO = IVISUAL_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Diagram Connections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__DIAGRAM_CONNECTIONS = IVISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Diagram Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__DIAGRAM_TYPE = IVISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Diagram Element</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__DIAGRAM_ELEMENT = IVISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Stitch Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM__IS_STITCH_OUTPUT = IVISUAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Visual Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DIAGRAM_FEATURE_COUNT = IVISUAL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl <em>Visual Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualElement()
	 * @generated
	 */
	int VISUAL_ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__LOCATION = IVISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__BOUNDS = IVISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__PARENT = IVISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__DIAGRAM = IVISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__ELEMENTS = IVISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__CONNECTIONS = IVISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__IS_SKETCHY = IVISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__VALIDATION_MESSAGE = IVISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__NAME = IVISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__VISUAL_ID = IVISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__STITCH_FROM = IVISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT__STITCH_TO = IVISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ELEMENT_FEATURE_COUNT = IVISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl <em>Visual Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualConnection()
	 * @generated
	 */
	int VISUAL_CONNECTION = 5;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__SOURCE = IVISUAL_CONNECTION__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__TARGET = IVISUAL_CONNECTION__TARGET;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__DIRECTION = IVISUAL_CONNECTION__DIRECTION;

	/**
	 * The feature id for the '<em><b>Bendpoints</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__BENDPOINTS = IVISUAL_CONNECTION__BENDPOINTS;

	/**
	 * The feature id for the '<em><b>Line Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__LINE_STYLE = IVISUAL_CONNECTION__LINE_STYLE;

	/**
	 * The feature id for the '<em><b>Source Decoration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__SOURCE_DECORATION = IVISUAL_CONNECTION__SOURCE_DECORATION;

	/**
	 * The feature id for the '<em><b>Target Decoration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__TARGET_DECORATION = IVISUAL_CONNECTION__TARGET_DECORATION;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__VALIDATION_MESSAGE = IVISUAL_CONNECTION__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__NAME = IVISUAL_CONNECTION__NAME;

	/**
	 * The feature id for the '<em><b>Source Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__SOURCE_MULTIPLICITY = IVISUAL_CONNECTION__SOURCE_MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Target Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__TARGET_MULTIPLICITY = IVISUAL_CONNECTION__TARGET_MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__TYPE = IVISUAL_CONNECTION__TYPE;

	/**
	 * The feature id for the '<em><b>Rounting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__ROUNTING = IVISUAL_CONNECTION__ROUNTING;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__VISUAL_ID = IVISUAL_CONNECTION__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__STITCH_FROM = IVISUAL_CONNECTION__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__STITCH_TO = IVISUAL_CONNECTION__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__SPECIFICATION_ELEMENT = IVISUAL_CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT = IVISUAL_CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Visual Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CONNECTION_FEATURE_COUNT = IVISUAL_CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.NoteImpl <em>Note</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.NoteImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getNote()
	 * @generated
	 */
	int NOTE = 6;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__LOCATION = VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__BOUNDS = VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__PARENT = VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__DIAGRAM = VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__ELEMENTS = VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__CONNECTIONS = VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__IS_SKETCHY = VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__VALIDATION_MESSAGE = VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__NAME = VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__VISUAL_ID = VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__STITCH_FROM = VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__STITCH_TO = VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__TEXT = VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Note</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE_FEATURE_COUNT = VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl <em>Visual Specification Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualSpecificationElement()
	 * @generated
	 */
	int VISUAL_SPECIFICATION_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__LOCATION = VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__BOUNDS = VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__PARENT = VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__DIAGRAM = VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__ELEMENTS = VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__CONNECTIONS = VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__IS_SKETCHY = VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__VALIDATION_MESSAGE = VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__NAME = VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__VISUAL_ID = VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__STITCH_FROM = VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__STITCH_TO = VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT = VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT = VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Visual Specification Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT = VISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualGenericElementImpl <em>Visual Generic Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualGenericElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualGenericElement()
	 * @generated
	 */
	int VISUAL_GENERIC_ELEMENT = 8;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__LOCATION = VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__BOUNDS = VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__PARENT = VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__DIAGRAM = VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__ELEMENTS = VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__CONNECTIONS = VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__IS_SKETCHY = VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__VALIDATION_MESSAGE = VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__NAME = VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__VISUAL_ID = VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__STITCH_FROM = VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__STITCH_TO = VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Generic Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT__GENERIC_TYPE = VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Visual Generic Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_GENERIC_ELEMENT_FEATURE_COUNT = VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchElementRelationshipImpl <em>Stitch Element Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchElementRelationshipImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getStitchElementRelationship()
	 * @generated
	 */
	int STITCH_ELEMENT_RELATIONSHIP = 9;

	/**
	 * The feature id for the '<em><b>Stitch Origin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN = 0;

	/**
	 * The feature id for the '<em><b>Stitch Output</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT = 1;

	/**
	 * The number of structural features of the '<em>Stitch Element Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STITCH_ELEMENT_RELATIONSHIP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchConnectionRelationshipImpl <em>Stitch Connection Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchConnectionRelationshipImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getStitchConnectionRelationship()
	 * @generated
	 */
	int STITCH_CONNECTION_RELATIONSHIP = 10;

	/**
	 * The feature id for the '<em><b>Stitch Origin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN = 0;

	/**
	 * The feature id for the '<em><b>Stitch Output</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT = 1;

	/**
	 * The number of structural features of the '<em>Stitch Connection Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STITCH_CONNECTION_RELATIONSHIP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection <em>Connection Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionDirection()
	 * @generated
	 */
	int CONNECTION_DIRECTION = 11;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType <em>Diagram Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getDiagramType()
	 * @generated
	 */
	int DIAGRAM_TYPE = 12;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration <em>Connection Decoration</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionDecoration()
	 * @generated
	 */
	int CONNECTION_DECORATION = 13;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle <em>Connection Line Style</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionLineStyle()
	 * @generated
	 */
	int CONNECTION_LINE_STYLE = 14;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.GenericType <em>Generic Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.GenericType
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getGenericType()
	 * @generated
	 */
	int GENERIC_TYPE = 15;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType <em>Connection Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionType()
	 * @generated
	 */
	int CONNECTION_TYPE = 16;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting <em>Connection Routing</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionRouting()
	 * @generated
	 */
	int CONNECTION_ROUTING = 17;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchState <em>Stitch State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchState
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getStitchState()
	 * @generated
	 */
	int STITCH_STATE = 18;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagram</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.Diagram
	 * @generated
	 */
	EClass getDiagram();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getVisualDiagram <em>Visual Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Visual Diagram</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getVisualDiagram()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_VisualDiagram();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getMergeLog <em>Merge Log</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merge Log</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getMergeLog()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_MergeLog();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getStitchState <em>Stitch State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stitch State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getStitchState()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_StitchState();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement <em>IVisual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IVisual Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement
	 * @generated
	 */
	EClass getIVisualElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getLocation()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EAttribute getIVisualElement_Location();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getBounds <em>Bounds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bounds</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getBounds()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EAttribute getIVisualElement_Bounds();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getParent()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EReference getIVisualElement_Parent();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getDiagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Diagram</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getDiagram()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EReference getIVisualElement_Diagram();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getElements()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EReference getIVisualElement_Elements();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getConnections <em>Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Connections</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getConnections()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EReference getIVisualElement_Connections();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#isIsSketchy <em>Is Sketchy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Sketchy</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#isIsSketchy()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EAttribute getIVisualElement_IsSketchy();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getValidationMessage <em>Validation Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Validation Message</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getValidationMessage()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EAttribute getIVisualElement_ValidationMessage();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getName()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EAttribute getIVisualElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getVisualID <em>Visual ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visual ID</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getVisualID()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EAttribute getIVisualElement_VisualID();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchFrom <em>Stitch From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Stitch From</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchFrom()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EReference getIVisualElement_StitchFrom();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchTo <em>Stitch To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Stitch To</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchTo()
	 * @see #getIVisualElement()
	 * @generated
	 */
	EReference getIVisualElement_StitchTo();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection <em>IVisual Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IVisual Connection</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection
	 * @generated
	 */
	EClass getIVisualConnection();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSource()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EReference getIVisualConnection_Source();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTarget()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EReference getIVisualConnection_Target();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getDirection()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_Direction();

	/**
	 * Returns the meta object for the attribute list '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getBendpoints <em>Bendpoints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Bendpoints</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getBendpoints()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_Bendpoints();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getLineStyle <em>Line Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Line Style</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getLineStyle()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_LineStyle();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSourceDecoration <em>Source Decoration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Decoration</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSourceDecoration()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_SourceDecoration();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTargetDecoration <em>Target Decoration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Decoration</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTargetDecoration()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_TargetDecoration();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getValidationMessage <em>Validation Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Validation Message</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getValidationMessage()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_ValidationMessage();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getName()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_Name();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSourceMultiplicity <em>Source Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Multiplicity</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSourceMultiplicity()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_SourceMultiplicity();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTargetMultiplicity <em>Target Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Multiplicity</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTargetMultiplicity()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_TargetMultiplicity();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getType()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_Type();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getRounting <em>Rounting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rounting</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getRounting()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_Rounting();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getVisualID <em>Visual ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visual ID</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getVisualID()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EAttribute getIVisualConnection_VisualID();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchFrom <em>Stitch From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Stitch From</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchFrom()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EReference getIVisualConnection_StitchFrom();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchTo <em>Stitch To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Stitch To</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchTo()
	 * @see #getIVisualConnection()
	 * @generated
	 */
	EReference getIVisualConnection_StitchTo();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram <em>Visual Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Diagram</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram
	 * @generated
	 */
	EClass getVisualDiagram();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramConnections <em>Diagram Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Diagram Connections</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramConnections()
	 * @see #getVisualDiagram()
	 * @generated
	 */
	EReference getVisualDiagram_DiagramConnections();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramType <em>Diagram Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Diagram Type</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramType()
	 * @see #getVisualDiagram()
	 * @generated
	 */
	EAttribute getVisualDiagram_DiagramType();

	/**
	 * Returns the meta object for the container reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramElement <em>Diagram Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Diagram Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramElement()
	 * @see #getVisualDiagram()
	 * @generated
	 */
	EReference getVisualDiagram_DiagramElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#isIsStitchOutput <em>Is Stitch Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Stitch Output</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#isIsStitchOutput()
	 * @see #getVisualDiagram()
	 * @generated
	 */
	EAttribute getVisualDiagram_IsStitchOutput();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement <em>Visual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement
	 * @generated
	 */
	EClass getVisualElement();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection <em>Visual Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Connection</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection
	 * @generated
	 */
	EClass getVisualConnection();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection#getSpecificationElement <em>Specification Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Specification Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection#getSpecificationElement()
	 * @see #getVisualConnection()
	 * @generated
	 */
	EReference getVisualConnection_SpecificationElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection#isIsLinkedToElement <em>Is Linked To Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Linked To Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection#isIsLinkedToElement()
	 * @see #getVisualConnection()
	 * @generated
	 */
	EAttribute getVisualConnection_IsLinkedToElement();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Note <em>Note</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Note</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.Note
	 * @generated
	 */
	EClass getNote();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Note#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.Note#getText()
	 * @see #getNote()
	 * @generated
	 */
	EAttribute getNote_Text();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement <em>Visual Specification Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Specification Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement
	 * @generated
	 */
	EClass getVisualSpecificationElement();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#getSpecificationElement <em>Specification Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Specification Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#getSpecificationElement()
	 * @see #getVisualSpecificationElement()
	 * @generated
	 */
	EReference getVisualSpecificationElement_SpecificationElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#isIsLinkedToElement <em>Is Linked To Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Linked To Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#isIsLinkedToElement()
	 * @see #getVisualSpecificationElement()
	 * @generated
	 */
	EAttribute getVisualSpecificationElement_IsLinkedToElement();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement <em>Visual Generic Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Generic Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement
	 * @generated
	 */
	EClass getVisualGenericElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement#getGenericType <em>Generic Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generic Type</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement#getGenericType()
	 * @see #getVisualGenericElement()
	 * @generated
	 */
	EAttribute getVisualGenericElement_GenericType();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship <em>Stitch Element Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stitch Element Relationship</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship
	 * @generated
	 */
	EClass getStitchElementRelationship();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOrigin <em>Stitch Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Stitch Origin</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOrigin()
	 * @see #getStitchElementRelationship()
	 * @generated
	 */
	EReference getStitchElementRelationship_StitchOrigin();

	/**
	 * Returns the meta object for the container reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOutput <em>Stitch Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Stitch Output</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOutput()
	 * @see #getStitchElementRelationship()
	 * @generated
	 */
	EReference getStitchElementRelationship_StitchOutput();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship <em>Stitch Connection Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stitch Connection Relationship</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship
	 * @generated
	 */
	EClass getStitchConnectionRelationship();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOrigin <em>Stitch Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Stitch Origin</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOrigin()
	 * @see #getStitchConnectionRelationship()
	 * @generated
	 */
	EReference getStitchConnectionRelationship_StitchOrigin();

	/**
	 * Returns the meta object for the container reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOutput <em>Stitch Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Stitch Output</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOutput()
	 * @see #getStitchConnectionRelationship()
	 * @generated
	 */
	EReference getStitchConnectionRelationship_StitchOutput();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection <em>Connection Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Connection Direction</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection
	 * @generated
	 */
	EEnum getConnectionDirection();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType <em>Diagram Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Diagram Type</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType
	 * @generated
	 */
	EEnum getDiagramType();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration <em>Connection Decoration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Connection Decoration</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration
	 * @generated
	 */
	EEnum getConnectionDecoration();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle <em>Connection Line Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Connection Line Style</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle
	 * @generated
	 */
	EEnum getConnectionLineStyle();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.GenericType <em>Generic Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Generic Type</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.GenericType
	 * @generated
	 */
	EEnum getGenericType();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType <em>Connection Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Connection Type</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType
	 * @generated
	 */
	EEnum getConnectionType();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting <em>Connection Routing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Connection Routing</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting
	 * @generated
	 */
	EEnum getConnectionRouting();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchState <em>Stitch State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Stitch State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchState
	 * @generated
	 */
	EEnum getStitchState();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	visualmodelFactory getvisualmodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.DiagramImpl <em>Diagram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.DiagramImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getDiagram()
		 * @generated
		 */
		EClass DIAGRAM = eINSTANCE.getDiagram();

		/**
		 * The meta object literal for the '<em><b>Visual Diagram</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIAGRAM__VISUAL_DIAGRAM = eINSTANCE.getDiagram_VisualDiagram();

		/**
		 * The meta object literal for the '<em><b>Merge Log</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__MERGE_LOG = eINSTANCE.getDiagram_MergeLog();

		/**
		 * The meta object literal for the '<em><b>Stitch State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__STITCH_STATE = eINSTANCE.getDiagram_StitchState();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement <em>IVisual Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getIVisualElement()
		 * @generated
		 */
		EClass IVISUAL_ELEMENT = eINSTANCE.getIVisualElement();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_ELEMENT__LOCATION = eINSTANCE.getIVisualElement_Location();

		/**
		 * The meta object literal for the '<em><b>Bounds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_ELEMENT__BOUNDS = eINSTANCE.getIVisualElement_Bounds();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_ELEMENT__PARENT = eINSTANCE.getIVisualElement_Parent();

		/**
		 * The meta object literal for the '<em><b>Diagram</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_ELEMENT__DIAGRAM = eINSTANCE.getIVisualElement_Diagram();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_ELEMENT__ELEMENTS = eINSTANCE.getIVisualElement_Elements();

		/**
		 * The meta object literal for the '<em><b>Connections</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_ELEMENT__CONNECTIONS = eINSTANCE.getIVisualElement_Connections();

		/**
		 * The meta object literal for the '<em><b>Is Sketchy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_ELEMENT__IS_SKETCHY = eINSTANCE.getIVisualElement_IsSketchy();

		/**
		 * The meta object literal for the '<em><b>Validation Message</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_ELEMENT__VALIDATION_MESSAGE = eINSTANCE.getIVisualElement_ValidationMessage();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_ELEMENT__NAME = eINSTANCE.getIVisualElement_Name();

		/**
		 * The meta object literal for the '<em><b>Visual ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_ELEMENT__VISUAL_ID = eINSTANCE.getIVisualElement_VisualID();

		/**
		 * The meta object literal for the '<em><b>Stitch From</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_ELEMENT__STITCH_FROM = eINSTANCE.getIVisualElement_StitchFrom();

		/**
		 * The meta object literal for the '<em><b>Stitch To</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_ELEMENT__STITCH_TO = eINSTANCE.getIVisualElement_StitchTo();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection <em>IVisual Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getIVisualConnection()
		 * @generated
		 */
		EClass IVISUAL_CONNECTION = eINSTANCE.getIVisualConnection();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_CONNECTION__SOURCE = eINSTANCE.getIVisualConnection_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_CONNECTION__TARGET = eINSTANCE.getIVisualConnection_Target();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__DIRECTION = eINSTANCE.getIVisualConnection_Direction();

		/**
		 * The meta object literal for the '<em><b>Bendpoints</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__BENDPOINTS = eINSTANCE.getIVisualConnection_Bendpoints();

		/**
		 * The meta object literal for the '<em><b>Line Style</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__LINE_STYLE = eINSTANCE.getIVisualConnection_LineStyle();

		/**
		 * The meta object literal for the '<em><b>Source Decoration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__SOURCE_DECORATION = eINSTANCE.getIVisualConnection_SourceDecoration();

		/**
		 * The meta object literal for the '<em><b>Target Decoration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__TARGET_DECORATION = eINSTANCE.getIVisualConnection_TargetDecoration();

		/**
		 * The meta object literal for the '<em><b>Validation Message</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__VALIDATION_MESSAGE = eINSTANCE.getIVisualConnection_ValidationMessage();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__NAME = eINSTANCE.getIVisualConnection_Name();

		/**
		 * The meta object literal for the '<em><b>Source Multiplicity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__SOURCE_MULTIPLICITY = eINSTANCE.getIVisualConnection_SourceMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Target Multiplicity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__TARGET_MULTIPLICITY = eINSTANCE.getIVisualConnection_TargetMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__TYPE = eINSTANCE.getIVisualConnection_Type();

		/**
		 * The meta object literal for the '<em><b>Rounting</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__ROUNTING = eINSTANCE.getIVisualConnection_Rounting();

		/**
		 * The meta object literal for the '<em><b>Visual ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION__VISUAL_ID = eINSTANCE.getIVisualConnection_VisualID();

		/**
		 * The meta object literal for the '<em><b>Stitch From</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_CONNECTION__STITCH_FROM = eINSTANCE.getIVisualConnection_StitchFrom();

		/**
		 * The meta object literal for the '<em><b>Stitch To</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_CONNECTION__STITCH_TO = eINSTANCE.getIVisualConnection_StitchTo();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualDiagramImpl <em>Visual Diagram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualDiagramImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualDiagram()
		 * @generated
		 */
		EClass VISUAL_DIAGRAM = eINSTANCE.getVisualDiagram();

		/**
		 * The meta object literal for the '<em><b>Diagram Connections</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_DIAGRAM__DIAGRAM_CONNECTIONS = eINSTANCE.getVisualDiagram_DiagramConnections();

		/**
		 * The meta object literal for the '<em><b>Diagram Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_DIAGRAM__DIAGRAM_TYPE = eINSTANCE.getVisualDiagram_DiagramType();

		/**
		 * The meta object literal for the '<em><b>Diagram Element</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_DIAGRAM__DIAGRAM_ELEMENT = eINSTANCE.getVisualDiagram_DiagramElement();

		/**
		 * The meta object literal for the '<em><b>Is Stitch Output</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_DIAGRAM__IS_STITCH_OUTPUT = eINSTANCE.getVisualDiagram_IsStitchOutput();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl <em>Visual Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualElement()
		 * @generated
		 */
		EClass VISUAL_ELEMENT = eINSTANCE.getVisualElement();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl <em>Visual Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualConnection()
		 * @generated
		 */
		EClass VISUAL_CONNECTION = eINSTANCE.getVisualConnection();

		/**
		 * The meta object literal for the '<em><b>Specification Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_CONNECTION__SPECIFICATION_ELEMENT = eINSTANCE.getVisualConnection_SpecificationElement();

		/**
		 * The meta object literal for the '<em><b>Is Linked To Element</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT = eINSTANCE.getVisualConnection_IsLinkedToElement();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.NoteImpl <em>Note</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.NoteImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getNote()
		 * @generated
		 */
		EClass NOTE = eINSTANCE.getNote();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOTE__TEXT = eINSTANCE.getNote_Text();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl <em>Visual Specification Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualSpecificationElement()
		 * @generated
		 */
		EClass VISUAL_SPECIFICATION_ELEMENT = eINSTANCE.getVisualSpecificationElement();

		/**
		 * The meta object literal for the '<em><b>Specification Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT = eINSTANCE.getVisualSpecificationElement_SpecificationElement();

		/**
		 * The meta object literal for the '<em><b>Is Linked To Element</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT = eINSTANCE.getVisualSpecificationElement_IsLinkedToElement();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualGenericElementImpl <em>Visual Generic Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualGenericElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getVisualGenericElement()
		 * @generated
		 */
		EClass VISUAL_GENERIC_ELEMENT = eINSTANCE.getVisualGenericElement();

		/**
		 * The meta object literal for the '<em><b>Generic Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_GENERIC_ELEMENT__GENERIC_TYPE = eINSTANCE.getVisualGenericElement_GenericType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchElementRelationshipImpl <em>Stitch Element Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchElementRelationshipImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getStitchElementRelationship()
		 * @generated
		 */
		EClass STITCH_ELEMENT_RELATIONSHIP = eINSTANCE.getStitchElementRelationship();

		/**
		 * The meta object literal for the '<em><b>Stitch Origin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN = eINSTANCE.getStitchElementRelationship_StitchOrigin();

		/**
		 * The meta object literal for the '<em><b>Stitch Output</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT = eINSTANCE.getStitchElementRelationship_StitchOutput();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchConnectionRelationshipImpl <em>Stitch Connection Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchConnectionRelationshipImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getStitchConnectionRelationship()
		 * @generated
		 */
		EClass STITCH_CONNECTION_RELATIONSHIP = eINSTANCE.getStitchConnectionRelationship();

		/**
		 * The meta object literal for the '<em><b>Stitch Origin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN = eINSTANCE.getStitchConnectionRelationship_StitchOrigin();

		/**
		 * The meta object literal for the '<em><b>Stitch Output</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT = eINSTANCE.getStitchConnectionRelationship_StitchOutput();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection <em>Connection Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionDirection()
		 * @generated
		 */
		EEnum CONNECTION_DIRECTION = eINSTANCE.getConnectionDirection();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType <em>Diagram Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getDiagramType()
		 * @generated
		 */
		EEnum DIAGRAM_TYPE = eINSTANCE.getDiagramType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration <em>Connection Decoration</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionDecoration()
		 * @generated
		 */
		EEnum CONNECTION_DECORATION = eINSTANCE.getConnectionDecoration();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle <em>Connection Line Style</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionLineStyle()
		 * @generated
		 */
		EEnum CONNECTION_LINE_STYLE = eINSTANCE.getConnectionLineStyle();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.GenericType <em>Generic Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.GenericType
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getGenericType()
		 * @generated
		 */
		EEnum GENERIC_TYPE = eINSTANCE.getGenericType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType <em>Connection Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionType()
		 * @generated
		 */
		EEnum CONNECTION_TYPE = eINSTANCE.getConnectionType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting <em>Connection Routing</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getConnectionRouting()
		 * @generated
		 */
		EEnum CONNECTION_ROUTING = eINSTANCE.getConnectionRouting();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchState <em>Stitch State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchState
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl#getStitchState()
		 * @generated
		 */
		EEnum STITCH_STATE = eINSTANCE.getStitchState();

	}

} //visualmodelPackage
