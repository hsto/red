/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Generic Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement#getGenericType <em>Generic Type</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualGenericElement()
 * @model
 * @generated
 */
public interface VisualGenericElement extends VisualElement {

	/**
	 * Returns the value of the '<em><b>Generic Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.visualmodeling.visualmodel.GenericType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Type</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.GenericType
	 * @see #setGenericType(GenericType)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualGenericElement_GenericType()
	 * @model
	 * @generated
	 */
	GenericType getGenericType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement#getGenericType <em>Generic Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Type</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.GenericType
	 * @see #getGenericType()
	 * @generated
	 */
	void setGenericType(GenericType value);
} // VisualGenericElement
