/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl;

import dk.dtu.imm.red.specificationelements.goal.GoalPackage;
import dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypePackage;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityPackage;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignaturePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualPackageElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.ImporttypesPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.impl.ImporttypesPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassPackageImpl extends EPackageImpl implements ClassPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualClassElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualClassAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualClassOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualEnumerationElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualEnumerationLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualPackageElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ClassPackageImpl() {
		super(eNS_URI, ClassFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ClassPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ClassPackage init() {
		if (isInited) return (ClassPackage)EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI);

		// Obtain or create and register package
		ClassPackageImpl theClassPackage = (ClassPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ClassPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ClassPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		GoalPackage.eINSTANCE.eClass();
		ModelelementPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		visualmodelPackageImpl thevisualmodelPackage = (visualmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI) instanceof visualmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI) : visualmodelPackage.eINSTANCE);
		ImporttypesPackageImpl theImporttypesPackage = (ImporttypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) instanceof ImporttypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) : ImporttypesPackage.eINSTANCE);
		GoalPackageImpl theGoalPackage_1 = (GoalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage.eNS_URI) instanceof GoalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage.eNS_URI) : dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage.eINSTANCE);
		UsecasePackageImpl theUsecasePackage = (UsecasePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) instanceof UsecasePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) : UsecasePackage.eINSTANCE);
		ActivityPackageImpl theActivityPackage = (ActivityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI) instanceof ActivityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI) : ActivityPackage.eINSTANCE);
		StatemachinePackageImpl theStatemachinePackage = (StatemachinePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) instanceof StatemachinePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) : StatemachinePackage.eINSTANCE);
		SystemstructurePackageImpl theSystemstructurePackage = (SystemstructurePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI) instanceof SystemstructurePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI) : SystemstructurePackage.eINSTANCE);

		// Create package meta-data objects
		theClassPackage.createPackageContents();
		thevisualmodelPackage.createPackageContents();
		theImporttypesPackage.createPackageContents();
		theGoalPackage_1.createPackageContents();
		theUsecasePackage.createPackageContents();
		theActivityPackage.createPackageContents();
		theStatemachinePackage.createPackageContents();
		theSystemstructurePackage.createPackageContents();

		// Initialize created meta-data
		theClassPackage.initializePackageContents();
		thevisualmodelPackage.initializePackageContents();
		theImporttypesPackage.initializePackageContents();
		theGoalPackage_1.initializePackageContents();
		theUsecasePackage.initializePackageContents();
		theActivityPackage.initializePackageContents();
		theStatemachinePackage.initializePackageContents();
		theSystemstructurePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClassPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClassPackage.eNS_URI, theClassPackage);
		return theClassPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualClassElement() {
		return visualClassElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualClassElement_Abstract() {
		return (EAttribute)visualClassElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualClassElement_Interface() {
		return (EAttribute)visualClassElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualClassAttribute() {
		return visualClassAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualClassAttribute_Visibility() {
		return (EAttribute)visualClassAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVisualClassAttribute_Type() {
		return (EReference)visualClassAttributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVisualClassAttribute_Multiplicity() {
		return (EReference)visualClassAttributeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualClassAttribute_ReadOnly() {
		return (EAttribute)visualClassAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualClassOperation() {
		return visualClassOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualClassOperation_Visibility() {
		return (EAttribute)visualClassOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVisualClassOperation_Type() {
		return (EReference)visualClassOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVisualClassOperation_Multiplicity() {
		return (EReference)visualClassOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVisualClassOperation_Parameters() {
		return (EReference)visualClassOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualEnumerationElement() {
		return visualEnumerationElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualEnumerationLiteral() {
		return visualEnumerationLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualPackageElement() {
		return visualPackageElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassFactory getClassFactory() {
		return (ClassFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		visualClassElementEClass = createEClass(VISUAL_CLASS_ELEMENT);
		createEAttribute(visualClassElementEClass, VISUAL_CLASS_ELEMENT__ABSTRACT);
		createEAttribute(visualClassElementEClass, VISUAL_CLASS_ELEMENT__INTERFACE);

		visualClassAttributeEClass = createEClass(VISUAL_CLASS_ATTRIBUTE);
		createEAttribute(visualClassAttributeEClass, VISUAL_CLASS_ATTRIBUTE__VISIBILITY);
		createEAttribute(visualClassAttributeEClass, VISUAL_CLASS_ATTRIBUTE__READ_ONLY);
		createEReference(visualClassAttributeEClass, VISUAL_CLASS_ATTRIBUTE__TYPE);
		createEReference(visualClassAttributeEClass, VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY);

		visualClassOperationEClass = createEClass(VISUAL_CLASS_OPERATION);
		createEAttribute(visualClassOperationEClass, VISUAL_CLASS_OPERATION__VISIBILITY);
		createEReference(visualClassOperationEClass, VISUAL_CLASS_OPERATION__TYPE);
		createEReference(visualClassOperationEClass, VISUAL_CLASS_OPERATION__MULTIPLICITY);
		createEReference(visualClassOperationEClass, VISUAL_CLASS_OPERATION__PARAMETERS);

		visualEnumerationElementEClass = createEClass(VISUAL_ENUMERATION_ELEMENT);

		visualEnumerationLiteralEClass = createEClass(VISUAL_ENUMERATION_LITERAL);

		visualPackageElementEClass = createEClass(VISUAL_PACKAGE_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		visualmodelPackage thevisualmodelPackage = (visualmodelPackage)EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		SignaturePackage theSignaturePackage = (SignaturePackage)EPackage.Registry.INSTANCE.getEPackage(SignaturePackage.eNS_URI);
		DatatypePackage theDatatypePackage = (DatatypePackage)EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI);
		MultiplicityPackage theMultiplicityPackage = (MultiplicityPackage)EPackage.Registry.INSTANCE.getEPackage(MultiplicityPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		visualClassElementEClass.getESuperTypes().add(thevisualmodelPackage.getVisualSpecificationElement());
		visualClassAttributeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualClassOperationEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualEnumerationElementEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualEnumerationLiteralEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualPackageElementEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());

		// Initialize classes and features; add operations and parameters
		initEClass(visualClassElementEClass, VisualClassElement.class, "VisualClassElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVisualClassElement_Abstract(), theEcorePackage.getEBoolean(), "Abstract", null, 0, 1, VisualClassElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVisualClassElement_Interface(), theEcorePackage.getEBoolean(), "Interface", null, 0, 1, VisualClassElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(visualClassAttributeEClass, VisualClassAttribute.class, "VisualClassAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVisualClassAttribute_Visibility(), theSignaturePackage.getVisibility(), "Visibility", null, 0, 1, VisualClassAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVisualClassAttribute_ReadOnly(), theEcorePackage.getEBoolean(), "ReadOnly", null, 0, 1, VisualClassAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVisualClassAttribute_Type(), theDatatypePackage.getDataType(), null, "Type", null, 0, 1, VisualClassAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVisualClassAttribute_Multiplicity(), theMultiplicityPackage.getMultiplicity(), null, "Multiplicity", null, 0, 1, VisualClassAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(visualClassOperationEClass, VisualClassOperation.class, "VisualClassOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVisualClassOperation_Visibility(), theSignaturePackage.getVisibility(), "Visibility", null, 0, 1, VisualClassOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVisualClassOperation_Type(), theDatatypePackage.getDataType(), null, "Type", null, 0, 1, VisualClassOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVisualClassOperation_Multiplicity(), theMultiplicityPackage.getMultiplicity(), null, "Multiplicity", null, 0, 1, VisualClassOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVisualClassOperation_Parameters(), theDatatypePackage.getDataType(), null, "Parameters", null, 0, 1, VisualClassOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(visualEnumerationElementEClass, VisualEnumerationElement.class, "VisualEnumerationElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualEnumerationLiteralEClass, VisualEnumerationLiteral.class, "VisualEnumerationLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualPackageElementEClass, VisualPackageElement.class, "VisualPackageElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //ClassPackageImpl
