/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypeFactory;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.MultiplicityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassFactoryImpl extends EFactoryImpl implements ClassFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ClassFactory init() {
		try {
			ClassFactory theClassFactory = (ClassFactory)EPackage.Registry.INSTANCE.getEFactory(ClassPackage.eNS_URI);
			if (theClassFactory != null) {
				return theClassFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ClassFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ClassPackage.VISUAL_CLASS_ELEMENT: return createVisualClassElement();
			case ClassPackage.VISUAL_CLASS_ATTRIBUTE: return createVisualClassAttribute();
			case ClassPackage.VISUAL_CLASS_OPERATION: return createVisualClassOperation();
			case ClassPackage.VISUAL_ENUMERATION_ELEMENT: return createVisualEnumerationElement();
			case ClassPackage.VISUAL_ENUMERATION_LITERAL: return createVisualEnumerationLiteral();
			case ClassPackage.VISUAL_PACKAGE_ELEMENT: return createVisualPackageElement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualClassElement createVisualClassElement() {
		VisualClassElementImpl visualClassElement = new VisualClassElementImpl();
		return visualClassElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public VisualClassAttribute createVisualClassAttribute() {
		VisualClassAttributeImpl visualClassAttribute = new VisualClassAttributeImpl();
		
		visualClassAttribute.setType(DatatypeFactory.eINSTANCE.createNoType());
		visualClassAttribute.setMultiplicity(MultiplicityFactory.eINSTANCE.createNoMultiplicity());
		
		return visualClassAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualClassOperation createVisualClassOperation() {
		VisualClassOperationImpl visualClassOperation = new VisualClassOperationImpl();
		return visualClassOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualEnumerationElement createVisualEnumerationElement() {
		VisualEnumerationElementImpl visualEnumerationElement = new VisualEnumerationElementImpl();
		return visualEnumerationElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualEnumerationLiteral createVisualEnumerationLiteral() {
		VisualEnumerationLiteralImpl visualEnumerationLiteral = new VisualEnumerationLiteralImpl();
		return visualEnumerationLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualPackageElement createVisualPackageElement() {
		VisualPackageElementImpl visualPackageElement = new VisualPackageElementImpl();
		return visualPackageElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassPackage getClassPackage() {
		return (ClassPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ClassPackage getPackage() {
		return ClassPackage.eINSTANCE;
	}

} //ClassFactoryImpl
