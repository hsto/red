package dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.GoalFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Goal Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualGoalElementImpl extends VisualSpecificationElementImpl implements VisualGoalElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualGoalElementImpl() {
		super();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public Class<?> getSpecificationElementType() {
		return Goal.class;
	}
	
	@Override 
	public SpecificationElement createSpecificationElement(Group parent) {
		Goal goal = GoalFactory.eINSTANCE.createGoal();		
		goal.setCreator(PreferenceUtil.getUserPreference_User());
		
		String str = (this.name == null) ? "" : this.name; 
		
		goal.setName(str);
		goal.setLabel(str);
		goal.setDescription("");
		
		if (parent != null) {
			goal.setParent(parent);
		}
		goal.save();
		
		return goal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GoalPackage.Literals.VISUAL_GOAL_ELEMENT;
	}

} //VisualGoalElementImpl
