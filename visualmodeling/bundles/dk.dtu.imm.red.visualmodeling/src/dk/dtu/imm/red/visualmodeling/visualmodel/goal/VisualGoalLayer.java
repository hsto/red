/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.goal;

import dk.dtu.imm.red.specificationelements.goal.GoalKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Goal Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getBelowLayer <em>Below Layer</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getAboveLayer <em>Above Layer</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getLayerKind <em>Layer Kind</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage#getVisualGoalLayer()
 * @model
 * @generated
 */
public interface VisualGoalLayer extends VisualElement {
	/**
	 * Returns the value of the '<em><b>Below Layer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Below Layer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Below Layer</em>' reference.
	 * @see #setBelowLayer(VisualGoalLayer)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage#getVisualGoalLayer_BelowLayer()
	 * @model
	 * @generated
	 */
	VisualGoalLayer getBelowLayer();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getBelowLayer <em>Below Layer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Below Layer</em>' reference.
	 * @see #getBelowLayer()
	 * @generated
	 */
	void setBelowLayer(VisualGoalLayer value);

	/**
	 * Returns the value of the '<em><b>Above Layer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Above Layer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Above Layer</em>' reference.
	 * @see #setAboveLayer(VisualGoalLayer)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage#getVisualGoalLayer_AboveLayer()
	 * @model
	 * @generated
	 */
	VisualGoalLayer getAboveLayer();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getAboveLayer <em>Above Layer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Above Layer</em>' reference.
	 * @see #getAboveLayer()
	 * @generated
	 */
	void setAboveLayer(VisualGoalLayer value);

	/**
	 * Returns the value of the '<em><b>Layer Kind</b></em>' attribute.
	 * The default value is <code>"unspecified"</code>.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.specificationelements.goal.GoalKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layer Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layer Kind</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.goal.GoalKind
	 * @see #setLayerKind(GoalKind)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage#getVisualGoalLayer_LayerKind()
	 * @model default="unspecified"
	 * @generated
	 */
	GoalKind getLayerKind();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer#getLayerKind <em>Layer Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layer Kind</em>' attribute.
	 * @see dk.dtu.imm.red.specificationelements.goal.GoalKind
	 * @see #getLayerKind()
	 * @generated
	 */
	void setLayerKind(GoalKind value);

} // VisualGoalLayer
