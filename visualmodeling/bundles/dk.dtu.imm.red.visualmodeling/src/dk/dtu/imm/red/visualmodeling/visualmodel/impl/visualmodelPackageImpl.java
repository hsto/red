/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.impl;

import dk.dtu.imm.red.core.element.group.GroupPackage;
import dk.dtu.imm.red.specificationelements.SpecificationelementsPackage;
import dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.GenericType;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.Note;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchState;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.ImporttypesPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.impl.ImporttypesPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class visualmodelPackageImpl extends EPackageImpl implements visualmodelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagramEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iVisualElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iVisualConnectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualDiagramEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualConnectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass noteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualSpecificationElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualGenericElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stitchElementRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stitchConnectionRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum connectionDirectionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum diagramTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum connectionDecorationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum connectionLineStyleEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum genericTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum connectionTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum connectionRoutingEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum stitchStateEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private visualmodelPackageImpl() {
		super(eNS_URI, visualmodelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link visualmodelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static visualmodelPackage init() {
		if (isInited) return (visualmodelPackage)EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI);

		// Obtain or create and register package
		visualmodelPackageImpl thevisualmodelPackage = (visualmodelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof visualmodelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new visualmodelPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		dk.dtu.imm.red.specificationelements.goal.GoalPackage.eINSTANCE.eClass();
		ModelelementPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ImporttypesPackageImpl theImporttypesPackage = (ImporttypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) instanceof ImporttypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) : ImporttypesPackage.eINSTANCE);
		GoalPackageImpl theGoalPackage_1 = (GoalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GoalPackage.eNS_URI) instanceof GoalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GoalPackage.eNS_URI) : GoalPackage.eINSTANCE);
		UsecasePackageImpl theUsecasePackage = (UsecasePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) instanceof UsecasePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) : UsecasePackage.eINSTANCE);
		ClassPackageImpl theClassPackage = (ClassPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI) instanceof ClassPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI) : ClassPackage.eINSTANCE);
		ActivityPackageImpl theActivityPackage = (ActivityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI) instanceof ActivityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI) : ActivityPackage.eINSTANCE);
		StatemachinePackageImpl theStatemachinePackage = (StatemachinePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) instanceof StatemachinePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) : StatemachinePackage.eINSTANCE);
		SystemstructurePackageImpl theSystemstructurePackage = (SystemstructurePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI) instanceof SystemstructurePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI) : SystemstructurePackage.eINSTANCE);

		// Create package meta-data objects
		thevisualmodelPackage.createPackageContents();
		theImporttypesPackage.createPackageContents();
		theGoalPackage_1.createPackageContents();
		theUsecasePackage.createPackageContents();
		theClassPackage.createPackageContents();
		theActivityPackage.createPackageContents();
		theStatemachinePackage.createPackageContents();
		theSystemstructurePackage.createPackageContents();

		// Initialize created meta-data
		thevisualmodelPackage.initializePackageContents();
		theImporttypesPackage.initializePackageContents();
		theGoalPackage_1.initializePackageContents();
		theUsecasePackage.initializePackageContents();
		theClassPackage.initializePackageContents();
		theActivityPackage.initializePackageContents();
		theStatemachinePackage.initializePackageContents();
		theSystemstructurePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thevisualmodelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(visualmodelPackage.eNS_URI, thevisualmodelPackage);
		return thevisualmodelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDiagram() {
		return diagramEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDiagram_VisualDiagram() {
		return (EReference)diagramEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDiagram_MergeLog() {
		return (EAttribute)diagramEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDiagram_StitchState() {
		return (EAttribute)diagramEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIVisualElement() {
		return iVisualElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualElement_Location() {
		return (EAttribute)iVisualElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualElement_Bounds() {
		return (EAttribute)iVisualElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualElement_Parent() {
		return (EReference)iVisualElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualElement_Diagram() {
		return (EReference)iVisualElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualElement_Elements() {
		return (EReference)iVisualElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualElement_Connections() {
		return (EReference)iVisualElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualElement_IsSketchy() {
		return (EAttribute)iVisualElementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualElement_ValidationMessage() {
		return (EAttribute)iVisualElementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualElement_Name() {
		return (EAttribute)iVisualElementEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualElement_VisualID() {
		return (EAttribute)iVisualElementEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualElement_StitchFrom() {
		return (EReference)iVisualElementEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualElement_StitchTo() {
		return (EReference)iVisualElementEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIVisualConnection() {
		return iVisualConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualConnection_Source() {
		return (EReference)iVisualConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualConnection_Target() {
		return (EReference)iVisualConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_Direction() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_Bendpoints() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_LineStyle() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_SourceDecoration() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_TargetDecoration() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_ValidationMessage() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_Name() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_SourceMultiplicity() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_TargetMultiplicity() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_Type() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_Rounting() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnection_VisualID() {
		return (EAttribute)iVisualConnectionEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualConnection_StitchFrom() {
		return (EReference)iVisualConnectionEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualConnection_StitchTo() {
		return (EReference)iVisualConnectionEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualDiagram() {
		return visualDiagramEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVisualDiagram_DiagramConnections() {
		return (EReference)visualDiagramEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualDiagram_DiagramType() {
		return (EAttribute)visualDiagramEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVisualDiagram_DiagramElement() {
		return (EReference)visualDiagramEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualDiagram_IsStitchOutput() {
		return (EAttribute)visualDiagramEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualElement() {
		return visualElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualConnection() {
		return visualConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVisualConnection_SpecificationElement() {
		return (EReference)visualConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualConnection_IsLinkedToElement() {
		return (EAttribute)visualConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNote() {
		return noteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNote_Text() {
		return (EAttribute)noteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualSpecificationElement() {
		return visualSpecificationElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVisualSpecificationElement_SpecificationElement() {
		return (EReference)visualSpecificationElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualSpecificationElement_IsLinkedToElement() {
		return (EAttribute)visualSpecificationElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualGenericElement() {
		return visualGenericElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisualGenericElement_GenericType() {
		return (EAttribute)visualGenericElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStitchElementRelationship() {
		return stitchElementRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStitchElementRelationship_StitchOrigin() {
		return (EReference)stitchElementRelationshipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStitchElementRelationship_StitchOutput() {
		return (EReference)stitchElementRelationshipEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStitchConnectionRelationship() {
		return stitchConnectionRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStitchConnectionRelationship_StitchOrigin() {
		return (EReference)stitchConnectionRelationshipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStitchConnectionRelationship_StitchOutput() {
		return (EReference)stitchConnectionRelationshipEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConnectionDirection() {
		return connectionDirectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDiagramType() {
		return diagramTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConnectionDecoration() {
		return connectionDecorationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConnectionLineStyle() {
		return connectionLineStyleEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getGenericType() {
		return genericTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConnectionType() {
		return connectionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConnectionRouting() {
		return connectionRoutingEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStitchState() {
		return stitchStateEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public visualmodelFactory getvisualmodelFactory() {
		return (visualmodelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		diagramEClass = createEClass(DIAGRAM);
		createEReference(diagramEClass, DIAGRAM__VISUAL_DIAGRAM);
		createEAttribute(diagramEClass, DIAGRAM__MERGE_LOG);
		createEAttribute(diagramEClass, DIAGRAM__STITCH_STATE);

		iVisualElementEClass = createEClass(IVISUAL_ELEMENT);
		createEAttribute(iVisualElementEClass, IVISUAL_ELEMENT__LOCATION);
		createEAttribute(iVisualElementEClass, IVISUAL_ELEMENT__BOUNDS);
		createEReference(iVisualElementEClass, IVISUAL_ELEMENT__PARENT);
		createEReference(iVisualElementEClass, IVISUAL_ELEMENT__DIAGRAM);
		createEReference(iVisualElementEClass, IVISUAL_ELEMENT__ELEMENTS);
		createEReference(iVisualElementEClass, IVISUAL_ELEMENT__CONNECTIONS);
		createEAttribute(iVisualElementEClass, IVISUAL_ELEMENT__IS_SKETCHY);
		createEAttribute(iVisualElementEClass, IVISUAL_ELEMENT__VALIDATION_MESSAGE);
		createEAttribute(iVisualElementEClass, IVISUAL_ELEMENT__NAME);
		createEAttribute(iVisualElementEClass, IVISUAL_ELEMENT__VISUAL_ID);
		createEReference(iVisualElementEClass, IVISUAL_ELEMENT__STITCH_FROM);
		createEReference(iVisualElementEClass, IVISUAL_ELEMENT__STITCH_TO);

		iVisualConnectionEClass = createEClass(IVISUAL_CONNECTION);
		createEReference(iVisualConnectionEClass, IVISUAL_CONNECTION__SOURCE);
		createEReference(iVisualConnectionEClass, IVISUAL_CONNECTION__TARGET);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__DIRECTION);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__BENDPOINTS);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__LINE_STYLE);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__SOURCE_DECORATION);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__TARGET_DECORATION);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__VALIDATION_MESSAGE);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__NAME);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__SOURCE_MULTIPLICITY);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__TARGET_MULTIPLICITY);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__TYPE);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__ROUNTING);
		createEAttribute(iVisualConnectionEClass, IVISUAL_CONNECTION__VISUAL_ID);
		createEReference(iVisualConnectionEClass, IVISUAL_CONNECTION__STITCH_FROM);
		createEReference(iVisualConnectionEClass, IVISUAL_CONNECTION__STITCH_TO);

		visualDiagramEClass = createEClass(VISUAL_DIAGRAM);
		createEReference(visualDiagramEClass, VISUAL_DIAGRAM__DIAGRAM_CONNECTIONS);
		createEAttribute(visualDiagramEClass, VISUAL_DIAGRAM__DIAGRAM_TYPE);
		createEReference(visualDiagramEClass, VISUAL_DIAGRAM__DIAGRAM_ELEMENT);
		createEAttribute(visualDiagramEClass, VISUAL_DIAGRAM__IS_STITCH_OUTPUT);

		visualElementEClass = createEClass(VISUAL_ELEMENT);

		visualConnectionEClass = createEClass(VISUAL_CONNECTION);
		createEReference(visualConnectionEClass, VISUAL_CONNECTION__SPECIFICATION_ELEMENT);
		createEAttribute(visualConnectionEClass, VISUAL_CONNECTION__IS_LINKED_TO_ELEMENT);

		noteEClass = createEClass(NOTE);
		createEAttribute(noteEClass, NOTE__TEXT);

		visualSpecificationElementEClass = createEClass(VISUAL_SPECIFICATION_ELEMENT);
		createEReference(visualSpecificationElementEClass, VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT);
		createEAttribute(visualSpecificationElementEClass, VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT);

		visualGenericElementEClass = createEClass(VISUAL_GENERIC_ELEMENT);
		createEAttribute(visualGenericElementEClass, VISUAL_GENERIC_ELEMENT__GENERIC_TYPE);

		stitchElementRelationshipEClass = createEClass(STITCH_ELEMENT_RELATIONSHIP);
		createEReference(stitchElementRelationshipEClass, STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN);
		createEReference(stitchElementRelationshipEClass, STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT);

		stitchConnectionRelationshipEClass = createEClass(STITCH_CONNECTION_RELATIONSHIP);
		createEReference(stitchConnectionRelationshipEClass, STITCH_CONNECTION_RELATIONSHIP__STITCH_ORIGIN);
		createEReference(stitchConnectionRelationshipEClass, STITCH_CONNECTION_RELATIONSHIP__STITCH_OUTPUT);

		// Create enums
		connectionDirectionEEnum = createEEnum(CONNECTION_DIRECTION);
		diagramTypeEEnum = createEEnum(DIAGRAM_TYPE);
		connectionDecorationEEnum = createEEnum(CONNECTION_DECORATION);
		connectionLineStyleEEnum = createEEnum(CONNECTION_LINE_STYLE);
		genericTypeEEnum = createEEnum(GENERIC_TYPE);
		connectionTypeEEnum = createEEnum(CONNECTION_TYPE);
		connectionRoutingEEnum = createEEnum(CONNECTION_ROUTING);
		stitchStateEEnum = createEEnum(STITCH_STATE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ImporttypesPackage theImporttypesPackage = (ImporttypesPackage)EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI);
		GoalPackage theGoalPackage_1 = (GoalPackage)EPackage.Registry.INSTANCE.getEPackage(GoalPackage.eNS_URI);
		UsecasePackage theUsecasePackage = (UsecasePackage)EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI);
		ClassPackage theClassPackage = (ClassPackage)EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI);
		ActivityPackage theActivityPackage = (ActivityPackage)EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI);
		StatemachinePackage theStatemachinePackage = (StatemachinePackage)EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI);
		SystemstructurePackage theSystemstructurePackage = (SystemstructurePackage)EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI);
		SpecificationelementsPackage theSpecificationelementsPackage = (SpecificationelementsPackage)EPackage.Registry.INSTANCE.getEPackage(SpecificationelementsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		GroupPackage theGroupPackage = (GroupPackage)EPackage.Registry.INSTANCE.getEPackage(GroupPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theImporttypesPackage);
		getESubpackages().add(theGoalPackage_1);
		getESubpackages().add(theUsecasePackage);
		getESubpackages().add(theClassPackage);
		getESubpackages().add(theActivityPackage);
		getESubpackages().add(theStatemachinePackage);
		getESubpackages().add(theSystemstructurePackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		diagramEClass.getESuperTypes().add(theSpecificationelementsPackage.getSpecificationElement());
		visualDiagramEClass.getESuperTypes().add(this.getIVisualElement());
		visualElementEClass.getESuperTypes().add(this.getIVisualElement());
		visualConnectionEClass.getESuperTypes().add(this.getIVisualConnection());
		noteEClass.getESuperTypes().add(this.getVisualElement());
		visualSpecificationElementEClass.getESuperTypes().add(this.getVisualElement());
		visualGenericElementEClass.getESuperTypes().add(this.getVisualElement());

		// Initialize classes and features; add operations and parameters
		initEClass(diagramEClass, Diagram.class, "Diagram", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDiagram_VisualDiagram(), this.getVisualDiagram(), this.getVisualDiagram_DiagramElement(), "VisualDiagram", null, 0, 1, Diagram.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDiagram_MergeLog(), theEcorePackage.getEString(), "mergeLog", null, 0, 1, Diagram.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDiagram_StitchState(), this.getStitchState(), "stitchState", null, 0, 1, Diagram.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iVisualElementEClass, IVisualElement.class, "IVisualElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIVisualElement_Location(), theImporttypesPackage.getPoint(), "Location", null, 0, 1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualElement_Bounds(), theImporttypesPackage.getDimension(), "Bounds", null, 0, 1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIVisualElement_Parent(), this.getIVisualElement(), null, "Parent", null, 0, 1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIVisualElement_Diagram(), this.getVisualDiagram(), null, "Diagram", null, 0, 1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIVisualElement_Elements(), this.getIVisualElement(), null, "Elements", null, 0, -1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIVisualElement_Connections(), this.getIVisualConnection(), null, "Connections", null, 0, -1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualElement_IsSketchy(), theEcorePackage.getEBoolean(), "IsSketchy", null, 0, 1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualElement_ValidationMessage(), theEcorePackage.getEString(), "ValidationMessage", null, 0, 1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualElement_Name(), theEcorePackage.getEString(), "Name", null, 0, 1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualElement_VisualID(), theEcorePackage.getEString(), "visualID", null, 0, 1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIVisualElement_StitchFrom(), this.getStitchElementRelationship(), this.getStitchElementRelationship_StitchOutput(), "stitchFrom", null, 0, -1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIVisualElement_StitchTo(), this.getStitchElementRelationship(), this.getStitchElementRelationship_StitchOrigin(), "stitchTo", null, 0, -1, IVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iVisualConnectionEClass, IVisualConnection.class, "IVisualConnection", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIVisualConnection_Source(), this.getIVisualElement(), null, "Source", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIVisualConnection_Target(), this.getIVisualElement(), null, "Target", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_Direction(), this.getConnectionDirection(), "Direction", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_Bendpoints(), theImporttypesPackage.getPoint(), "Bendpoints", null, 0, -1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_LineStyle(), this.getConnectionLineStyle(), "LineStyle", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_SourceDecoration(), this.getConnectionDecoration(), "SourceDecoration", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_TargetDecoration(), this.getConnectionDecoration(), "TargetDecoration", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_ValidationMessage(), theEcorePackage.getEString(), "ValidationMessage", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_Name(), theEcorePackage.getEString(), "Name", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_SourceMultiplicity(), theEcorePackage.getEString(), "SourceMultiplicity", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_TargetMultiplicity(), theEcorePackage.getEString(), "TargetMultiplicity", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_Type(), theEcorePackage.getEString(), "Type", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_Rounting(), this.getConnectionRouting(), "Rounting", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnection_VisualID(), theEcorePackage.getEString(), "visualID", null, 0, 1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIVisualConnection_StitchFrom(), this.getStitchConnectionRelationship(), this.getStitchConnectionRelationship_StitchOutput(), "stitchFrom", null, 0, -1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIVisualConnection_StitchTo(), this.getStitchConnectionRelationship(), this.getStitchConnectionRelationship_StitchOrigin(), "stitchTo", null, 0, -1, IVisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(visualDiagramEClass, VisualDiagram.class, "VisualDiagram", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVisualDiagram_DiagramConnections(), this.getIVisualConnection(), null, "DiagramConnections", null, 0, -1, VisualDiagram.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVisualDiagram_DiagramType(), this.getDiagramType(), "DiagramType", null, 0, 1, VisualDiagram.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVisualDiagram_DiagramElement(), this.getDiagram(), this.getDiagram_VisualDiagram(), "diagramElement", null, 0, 1, VisualDiagram.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVisualDiagram_IsStitchOutput(), theEcorePackage.getEBoolean(), "isStitchOutput", null, 0, 1, VisualDiagram.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(visualElementEClass, VisualElement.class, "VisualElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualConnectionEClass, VisualConnection.class, "VisualConnection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVisualConnection_SpecificationElement(), theSpecificationelementsPackage.getSpecificationElement(), null, "SpecificationElement", null, 0, 1, VisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVisualConnection_IsLinkedToElement(), theEcorePackage.getEBoolean(), "IsLinkedToElement", null, 0, 1, VisualConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(visualConnectionEClass, theSpecificationelementsPackage.getSpecificationElement(), "createSpecificationConnection", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theGroupPackage.getGroup(), "parent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theSpecificationelementsPackage.getSpecificationElement(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theSpecificationelementsPackage.getSpecificationElement(), "target", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(noteEClass, Note.class, "Note", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNote_Text(), theEcorePackage.getEString(), "Text", null, 0, 1, Note.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(visualSpecificationElementEClass, VisualSpecificationElement.class, "VisualSpecificationElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVisualSpecificationElement_SpecificationElement(), theSpecificationelementsPackage.getSpecificationElement(), null, "SpecificationElement", null, 0, 1, VisualSpecificationElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVisualSpecificationElement_IsLinkedToElement(), theEcorePackage.getEBoolean(), "IsLinkedToElement", null, 0, 1, VisualSpecificationElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(visualSpecificationElementEClass, theSpecificationelementsPackage.getSpecificationElement(), "createSpecificationElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theGroupPackage.getGroup(), "parent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(visualSpecificationElementEClass, null, "getSpecificationElementType", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(theEcorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEClass(visualGenericElementEClass, VisualGenericElement.class, "VisualGenericElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVisualGenericElement_GenericType(), this.getGenericType(), "GenericType", null, 0, 1, VisualGenericElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stitchElementRelationshipEClass, StitchElementRelationship.class, "StitchElementRelationship", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStitchElementRelationship_StitchOrigin(), this.getIVisualElement(), this.getIVisualElement_StitchTo(), "stitchOrigin", null, 1, 1, StitchElementRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStitchElementRelationship_StitchOutput(), this.getIVisualElement(), this.getIVisualElement_StitchFrom(), "stitchOutput", null, 1, 1, StitchElementRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stitchConnectionRelationshipEClass, StitchConnectionRelationship.class, "StitchConnectionRelationship", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStitchConnectionRelationship_StitchOrigin(), this.getIVisualConnection(), this.getIVisualConnection_StitchTo(), "stitchOrigin", null, 1, 1, StitchConnectionRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStitchConnectionRelationship_StitchOutput(), this.getIVisualConnection(), this.getIVisualConnection_StitchFrom(), "stitchOutput", null, 1, 1, StitchConnectionRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(connectionDirectionEEnum, ConnectionDirection.class, "ConnectionDirection");
		addEEnumLiteral(connectionDirectionEEnum, ConnectionDirection.NONE);
		addEEnumLiteral(connectionDirectionEEnum, ConnectionDirection.SOURCE_TARGET);
		addEEnumLiteral(connectionDirectionEEnum, ConnectionDirection.TARGET_SOURCE);
		addEEnumLiteral(connectionDirectionEEnum, ConnectionDirection.BIDIRECTIONAL);

		initEEnum(diagramTypeEEnum, DiagramType.class, "DiagramType");
		addEEnumLiteral(diagramTypeEEnum, DiagramType.GENERIC);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.ACTIVITY);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.CLASS);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.GOAL);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.STATE_MACHINE);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.USE_CASE);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.USE_CASE_MAP);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.SYSTEM_STRUCTURE);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.STITCH);

		initEEnum(connectionDecorationEEnum, ConnectionDecoration.class, "ConnectionDecoration");
		addEEnumLiteral(connectionDecorationEEnum, ConnectionDecoration.NONE);
		addEEnumLiteral(connectionDecorationEEnum, ConnectionDecoration.ARROW_HEAD);
		addEEnumLiteral(connectionDecorationEEnum, ConnectionDecoration.ARROW_HEAD_SOLID);
		addEEnumLiteral(connectionDecorationEEnum, ConnectionDecoration.ARROW_HEAD_SOLID_WHITE);
		addEEnumLiteral(connectionDecorationEEnum, ConnectionDecoration.DIAMOND_WHITE);
		addEEnumLiteral(connectionDecorationEEnum, ConnectionDecoration.DIAMOND_BLACK);

		initEEnum(connectionLineStyleEEnum, ConnectionLineStyle.class, "ConnectionLineStyle");
		addEEnumLiteral(connectionLineStyleEEnum, ConnectionLineStyle.DEFAULT);
		addEEnumLiteral(connectionLineStyleEEnum, ConnectionLineStyle.DASHED);

		initEEnum(genericTypeEEnum, GenericType.class, "GenericType");
		addEEnumLiteral(genericTypeEEnum, GenericType.RECTANGLE);
		addEEnumLiteral(genericTypeEEnum, GenericType.ELLIPSE);
		addEEnumLiteral(genericTypeEEnum, GenericType.CIRCLE);
		addEEnumLiteral(genericTypeEEnum, GenericType.TRIANGLE);
		addEEnumLiteral(genericTypeEEnum, GenericType.DIAMOND);

		initEEnum(connectionTypeEEnum, ConnectionType.class, "ConnectionType");
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.SUPPORTING);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.OBSTRUCTING);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.EXTEND);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.INCLUDE);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.ASSOCIATION);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.GENERALIZATION);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.AGGREGATION);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.COMPOSITION);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.CONTROL_FLOW);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.OBJECT_FLOW);
		addEEnumLiteral(connectionTypeEEnum, ConnectionType.STATE_TRANSITION);

		initEEnum(connectionRoutingEEnum, ConnectionRouting.class, "ConnectionRouting");
		addEEnumLiteral(connectionRoutingEEnum, ConnectionRouting.NONE);
		addEEnumLiteral(connectionRoutingEEnum, ConnectionRouting.MANHATTEN);
		addEEnumLiteral(connectionRoutingEEnum, ConnectionRouting.SHORTEST);
		addEEnumLiteral(connectionRoutingEEnum, ConnectionRouting.FAN);

		initEEnum(stitchStateEEnum, StitchState.class, "StitchState");
		addEEnumLiteral(stitchStateEEnum, StitchState.INITIAL);
		addEEnumLiteral(stitchStateEEnum, StitchState.MARKED_AS_INPUT);
		addEEnumLiteral(stitchStateEEnum, StitchState.COMPLETED);
		addEEnumLiteral(stitchStateEEnum, StitchState.ERROR);

		// Create resource
		createResource(eNS_URI);
	}

} //visualmodelPackageImpl
