/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.util;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage
 * @generated
 */
public class StatemachineSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static StatemachinePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineSwitch() {
		if (modelPackage == null) {
			modelPackage = StatemachinePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case StatemachinePackage.VISUAL_STATE: {
				VisualState visualState = (VisualState)theEObject;
				T result = caseVisualState(visualState);
				if (result == null) result = caseVisualElement(visualState);
				if (result == null) result = caseIVisualElement(visualState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachinePackage.VISUAL_INITIAL_STATE: {
				VisualInitialState visualInitialState = (VisualInitialState)theEObject;
				T result = caseVisualInitialState(visualInitialState);
				if (result == null) result = caseVisualElement(visualInitialState);
				if (result == null) result = caseIVisualElement(visualInitialState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachinePackage.VISUAL_FINAL_STATE: {
				VisualFinalState visualFinalState = (VisualFinalState)theEObject;
				T result = caseVisualFinalState(visualFinalState);
				if (result == null) result = caseVisualElement(visualFinalState);
				if (result == null) result = caseIVisualElement(visualFinalState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachinePackage.VISUAL_DECISION_MERGE_STATE: {
				VisualDecisionMergeState visualDecisionMergeState = (VisualDecisionMergeState)theEObject;
				T result = caseVisualDecisionMergeState(visualDecisionMergeState);
				if (result == null) result = caseVisualElement(visualDecisionMergeState);
				if (result == null) result = caseIVisualElement(visualDecisionMergeState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachinePackage.VISUAL_FORK_JOIN_STATE: {
				VisualForkJoinState visualForkJoinState = (VisualForkJoinState)theEObject;
				T result = caseVisualForkJoinState(visualForkJoinState);
				if (result == null) result = caseVisualElement(visualForkJoinState);
				if (result == null) result = caseIVisualElement(visualForkJoinState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachinePackage.VISUAL_SEND_STATE: {
				VisualSendState visualSendState = (VisualSendState)theEObject;
				T result = caseVisualSendState(visualSendState);
				if (result == null) result = caseVisualElement(visualSendState);
				if (result == null) result = caseIVisualElement(visualSendState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachinePackage.VISUAL_RECEIVE_STATE: {
				VisualReceiveState visualReceiveState = (VisualReceiveState)theEObject;
				T result = caseVisualReceiveState(visualReceiveState);
				if (result == null) result = caseVisualElement(visualReceiveState);
				if (result == null) result = caseIVisualElement(visualReceiveState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachinePackage.VISUAL_SHALLOW_HISTORY_STATE: {
				VisualShallowHistoryState visualShallowHistoryState = (VisualShallowHistoryState)theEObject;
				T result = caseVisualShallowHistoryState(visualShallowHistoryState);
				if (result == null) result = caseVisualElement(visualShallowHistoryState);
				if (result == null) result = caseIVisualElement(visualShallowHistoryState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachinePackage.VISUAL_DEEP_HISTORY_STATE: {
				VisualDeepHistoryState visualDeepHistoryState = (VisualDeepHistoryState)theEObject;
				T result = caseVisualDeepHistoryState(visualDeepHistoryState);
				if (result == null) result = caseVisualElement(visualDeepHistoryState);
				if (result == null) result = caseIVisualElement(visualDeepHistoryState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualState(VisualState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Initial State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Initial State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualInitialState(VisualInitialState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Final State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Final State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualFinalState(VisualFinalState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Decision Merge State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Decision Merge State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualDecisionMergeState(VisualDecisionMergeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Fork Join State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Fork Join State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualForkJoinState(VisualForkJoinState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Send State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Send State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualSendState(VisualSendState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Receive State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Receive State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualReceiveState(VisualReceiveState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Shallow History State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Shallow History State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualShallowHistoryState(VisualShallowHistoryState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Deep History State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Deep History State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualDeepHistoryState(VisualDeepHistoryState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IVisual Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IVisual Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIVisualElement(IVisualElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualElement(VisualElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //StatemachineSwitch
