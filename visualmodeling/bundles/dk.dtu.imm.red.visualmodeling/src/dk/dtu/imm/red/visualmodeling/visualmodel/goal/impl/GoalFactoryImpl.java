/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.goal.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GoalFactoryImpl extends EFactoryImpl implements GoalFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static GoalFactory init() {
		try {
			GoalFactory theGoalFactory = (GoalFactory)EPackage.Registry.INSTANCE.getEFactory(GoalPackage.eNS_URI);
			if (theGoalFactory != null) {
				return theGoalFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GoalFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case GoalPackage.VISUAL_GOAL_ELEMENT: return createVisualGoalElement();
			case GoalPackage.VISUAL_GOAL_LAYER: return createVisualGoalLayer();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualGoalElement createVisualGoalElement() {
		VisualGoalElementImpl visualGoalElement = new VisualGoalElementImpl();
		return visualGoalElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualGoalLayer createVisualGoalLayer() {
		VisualGoalLayerImpl visualGoalLayer = new VisualGoalLayerImpl();
		return visualGoalLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalPackage getGoalPackage() {
		return (GoalPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static GoalPackage getPackage() {
		return GoalPackage.eINSTANCE;
	}

} //GoalFactoryImpl
