/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.importtypes;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.ImporttypesPackage
 * @generated
 */
public interface ImporttypesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ImporttypesFactory eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.impl.ImporttypesFactoryImpl.init();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ImporttypesPackage getImporttypesPackage();

} //ImporttypesFactory
