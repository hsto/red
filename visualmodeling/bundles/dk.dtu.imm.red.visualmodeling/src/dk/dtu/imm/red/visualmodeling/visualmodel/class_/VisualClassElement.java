/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_;

import java.util.List;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Class Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement#isInterface <em>Interface</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualClassElement()
 * @model
 * @generated
 */
public interface VisualClassElement extends VisualSpecificationElement {
	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualClassElement_Abstract()
	 * @model
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' attribute.
	 * @see #setInterface(boolean)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualClassElement_Interface()
	 * @model
	 * @generated
	 */
	boolean isInterface();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement#isInterface <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' attribute.
	 * @see #isInterface()
	 * @generated
	 */
	void setInterface(boolean value);

	/**
	 * @generated NOT
	 */
	List<VisualClassAttribute> getAttributes();

	/**
	 * @generated NOT
	 */
	List<VisualClassOperation> getOperations();
} // VisualClassElement
