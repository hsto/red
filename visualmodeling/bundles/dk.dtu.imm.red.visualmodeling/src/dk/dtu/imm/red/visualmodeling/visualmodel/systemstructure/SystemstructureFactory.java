/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage
 * @generated
 */
public interface SystemstructureFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SystemstructureFactory eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructureFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Visual System Structure Actor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual System Structure Actor</em>'.
	 * @generated
	 */
	VisualSystemStructureActor createVisualSystemStructureActor();

	/**
	 * Returns a new object of class '<em>Visual System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual System</em>'.
	 * @generated
	 */
	VisualSystem createVisualSystem();

	/**
	 * Returns a new object of class '<em>Visual Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Port</em>'.
	 * @generated
	 */
	VisualPort createVisualPort();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SystemstructurePackage getSystemstructurePackage();

} //SystemstructureFactory
