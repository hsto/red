/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SystemstructureFactoryImpl extends EFactoryImpl implements SystemstructureFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SystemstructureFactory init() {
		try {
			SystemstructureFactory theSystemstructureFactory = (SystemstructureFactory)EPackage.Registry.INSTANCE.getEFactory(SystemstructurePackage.eNS_URI);
			if (theSystemstructureFactory != null) {
				return theSystemstructureFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SystemstructureFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemstructureFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SystemstructurePackage.VISUAL_SYSTEM_STRUCTURE_ACTOR: return createVisualSystemStructureActor();
			case SystemstructurePackage.VISUAL_SYSTEM: return createVisualSystem();
			case SystemstructurePackage.VISUAL_PORT: return createVisualPort();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case SystemstructurePackage.SYSTEM_STRUCTURE_ACTOR_KIND:
				return createSystemStructureActorKindFromString(eDataType, initialValue);
			case SystemstructurePackage.SYSTEM_STRUCTURE_DIAGRAM_KIND:
				return createSystemStructureDiagramKindFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case SystemstructurePackage.SYSTEM_STRUCTURE_ACTOR_KIND:
				return convertSystemStructureActorKindToString(eDataType, instanceValue);
			case SystemstructurePackage.SYSTEM_STRUCTURE_DIAGRAM_KIND:
				return convertSystemStructureDiagramKindToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualSystemStructureActor createVisualSystemStructureActor() {
		VisualSystemStructureActorImpl visualSystemStructureActor = new VisualSystemStructureActorImpl();
		return visualSystemStructureActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualSystem createVisualSystem() {
		VisualSystemImpl visualSystem = new VisualSystemImpl();
		return visualSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualPort createVisualPort() {
		VisualPortImpl visualPort = new VisualPortImpl();
		return visualPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemStructureActorKind createSystemStructureActorKindFromString(EDataType eDataType, String initialValue) {
		SystemStructureActorKind result = SystemStructureActorKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSystemStructureActorKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemStructureDiagramKind createSystemStructureDiagramKindFromString(EDataType eDataType, String initialValue) {
		SystemStructureDiagramKind result = SystemStructureDiagramKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSystemStructureDiagramKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemstructurePackage getSystemstructurePackage() {
		return (SystemstructurePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SystemstructurePackage getPackage() {
		return SystemstructurePackage.eINSTANCE;
	}

} //SystemstructureFactoryImpl
