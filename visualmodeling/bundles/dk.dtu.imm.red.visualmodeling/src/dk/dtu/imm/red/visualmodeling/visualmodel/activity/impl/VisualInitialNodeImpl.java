/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualInitialNode;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Initial Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualInitialNodeImpl extends VisualElementImpl implements VisualInitialNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualInitialNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActivityPackage.Literals.VISUAL_INITIAL_NODE;
	}

} //VisualInitialNodeImpl
