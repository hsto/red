/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.usecase;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Use Case Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage#getVisualUseCaseElement()
 * @model
 * @generated
 */
public interface VisualUseCaseElement extends VisualSpecificationElement {
} // VisualUseCaseElement
