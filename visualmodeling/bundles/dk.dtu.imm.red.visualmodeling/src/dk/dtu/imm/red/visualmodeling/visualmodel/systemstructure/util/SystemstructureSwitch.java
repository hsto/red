/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.util;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage
 * @generated
 */
public class SystemstructureSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SystemstructurePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemstructureSwitch() {
		if (modelPackage == null) {
			modelPackage = SystemstructurePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case SystemstructurePackage.VISUAL_SYSTEM_STRUCTURE_ACTOR: {
				VisualSystemStructureActor visualSystemStructureActor = (VisualSystemStructureActor)theEObject;
				T result = caseVisualSystemStructureActor(visualSystemStructureActor);
				if (result == null) result = caseVisualSpecificationElement(visualSystemStructureActor);
				if (result == null) result = caseVisualElement(visualSystemStructureActor);
				if (result == null) result = caseIVisualElement(visualSystemStructureActor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SystemstructurePackage.VISUAL_SYSTEM: {
				VisualSystem visualSystem = (VisualSystem)theEObject;
				T result = caseVisualSystem(visualSystem);
				if (result == null) result = caseVisualSpecificationElement(visualSystem);
				if (result == null) result = caseVisualElement(visualSystem);
				if (result == null) result = caseIVisualElement(visualSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SystemstructurePackage.VISUAL_PORT: {
				VisualPort visualPort = (VisualPort)theEObject;
				T result = caseVisualPort(visualPort);
				if (result == null) result = caseVisualSpecificationElement(visualPort);
				if (result == null) result = caseVisualElement(visualPort);
				if (result == null) result = caseIVisualElement(visualPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual System Structure Actor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual System Structure Actor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualSystemStructureActor(VisualSystemStructureActor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualSystem(VisualSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualPort(VisualPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IVisual Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IVisual Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIVisualElement(IVisualElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualElement(VisualElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visual Specification Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visual Specification Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisualSpecificationElement(VisualSpecificationElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SystemstructureSwitch
