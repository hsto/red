/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_;

import java.util.List;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Enumeration Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualEnumerationElement()
 * @model
 * @generated
 */
public interface VisualEnumerationElement extends VisualElement {

	/**
	 * @generated NOT
	 */
	List<VisualEnumerationLiteral> getLiterals();
} // VisualEnumerationElement
