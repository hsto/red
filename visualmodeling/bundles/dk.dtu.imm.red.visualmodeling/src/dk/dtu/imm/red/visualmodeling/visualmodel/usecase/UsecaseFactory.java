/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.usecase;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage
 * @generated
 */
public interface UsecaseFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UsecaseFactory eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecaseFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Visual Actor Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Actor Element</em>'.
	 * @generated
	 */
	VisualActorElement createVisualActorElement();

	/**
	 * Returns a new object of class '<em>Visual Use Case Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Use Case Element</em>'.
	 * @generated
	 */
	VisualUseCaseElement createVisualUseCaseElement();

	/**
	 * Returns a new object of class '<em>Visual System Boundary Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual System Boundary Element</em>'.
	 * @generated
	 */
	VisualSystemBoundaryElement createVisualSystemBoundaryElement();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UsecasePackage getUsecasePackage();

} //UsecaseFactory
