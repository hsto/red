/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDecisionMergeState;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Decision Merge State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualDecisionMergeStateImpl extends VisualElementImpl implements VisualDecisionMergeState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualDecisionMergeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.VISUAL_DECISION_MERGE_STATE;
	}

} //VisualDecisionMergeStateImpl
