/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Action Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage#getVisualActionNode()
 * @model
 * @generated
 */
public interface VisualActionNode extends VisualElement {
} // VisualActionNode
