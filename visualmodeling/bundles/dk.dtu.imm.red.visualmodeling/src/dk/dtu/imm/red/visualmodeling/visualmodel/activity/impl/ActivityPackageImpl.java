/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl;

import dk.dtu.imm.red.specificationelements.goal.GoalPackage;

import dk.dtu.imm.red.specificationelements.modelelement.ModelelementPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActionNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActivityFinalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualDecisionMergeNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualFlowFinalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualForkJoinNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualInitialNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualReceiveSignalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualSendSignalNode;

import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;

import dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.GoalPackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelPackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.ImporttypesPackage;

import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.impl.ImporttypesPackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.SystemstructurePackageImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage;

import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.impl.UsecasePackageImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActivityPackageImpl extends EPackageImpl implements ActivityPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualActionNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualInitialNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualActivityFinalNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualFlowFinalNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualDecisionMergeNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualForkJoinNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualSendSignalNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualReceiveSignalNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visualObjectNodeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ActivityPackageImpl() {
		super(eNS_URI, ActivityFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ActivityPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ActivityPackage init() {
		if (isInited) return (ActivityPackage)EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI);

		// Obtain or create and register package
		ActivityPackageImpl theActivityPackage = (ActivityPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ActivityPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ActivityPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		GoalPackage.eINSTANCE.eClass();
		ModelelementPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		visualmodelPackageImpl thevisualmodelPackage = (visualmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI) instanceof visualmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI) : visualmodelPackage.eINSTANCE);
		ImporttypesPackageImpl theImporttypesPackage = (ImporttypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) instanceof ImporttypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ImporttypesPackage.eNS_URI) : ImporttypesPackage.eINSTANCE);
		GoalPackageImpl theGoalPackage_1 = (GoalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage.eNS_URI) instanceof GoalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage.eNS_URI) : dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage.eINSTANCE);
		UsecasePackageImpl theUsecasePackage = (UsecasePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) instanceof UsecasePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UsecasePackage.eNS_URI) : UsecasePackage.eINSTANCE);
		ClassPackageImpl theClassPackage = (ClassPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI) instanceof ClassPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClassPackage.eNS_URI) : ClassPackage.eINSTANCE);
		StatemachinePackageImpl theStatemachinePackage = (StatemachinePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) instanceof StatemachinePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI) : StatemachinePackage.eINSTANCE);
		SystemstructurePackageImpl theSystemstructurePackage = (SystemstructurePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI) instanceof SystemstructurePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemstructurePackage.eNS_URI) : SystemstructurePackage.eINSTANCE);

		// Create package meta-data objects
		theActivityPackage.createPackageContents();
		thevisualmodelPackage.createPackageContents();
		theImporttypesPackage.createPackageContents();
		theGoalPackage_1.createPackageContents();
		theUsecasePackage.createPackageContents();
		theClassPackage.createPackageContents();
		theStatemachinePackage.createPackageContents();
		theSystemstructurePackage.createPackageContents();

		// Initialize created meta-data
		theActivityPackage.initializePackageContents();
		thevisualmodelPackage.initializePackageContents();
		theImporttypesPackage.initializePackageContents();
		theGoalPackage_1.initializePackageContents();
		theUsecasePackage.initializePackageContents();
		theClassPackage.initializePackageContents();
		theStatemachinePackage.initializePackageContents();
		theSystemstructurePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theActivityPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ActivityPackage.eNS_URI, theActivityPackage);
		return theActivityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualActionNode() {
		return visualActionNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualInitialNode() {
		return visualInitialNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualActivityFinalNode() {
		return visualActivityFinalNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualFlowFinalNode() {
		return visualFlowFinalNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualDecisionMergeNode() {
		return visualDecisionMergeNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualForkJoinNode() {
		return visualForkJoinNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualSendSignalNode() {
		return visualSendSignalNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualReceiveSignalNode() {
		return visualReceiveSignalNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisualObjectNode() {
		return visualObjectNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityFactory getActivityFactory() {
		return (ActivityFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		visualActionNodeEClass = createEClass(VISUAL_ACTION_NODE);

		visualInitialNodeEClass = createEClass(VISUAL_INITIAL_NODE);

		visualActivityFinalNodeEClass = createEClass(VISUAL_ACTIVITY_FINAL_NODE);

		visualFlowFinalNodeEClass = createEClass(VISUAL_FLOW_FINAL_NODE);

		visualDecisionMergeNodeEClass = createEClass(VISUAL_DECISION_MERGE_NODE);

		visualForkJoinNodeEClass = createEClass(VISUAL_FORK_JOIN_NODE);

		visualSendSignalNodeEClass = createEClass(VISUAL_SEND_SIGNAL_NODE);

		visualReceiveSignalNodeEClass = createEClass(VISUAL_RECEIVE_SIGNAL_NODE);

		visualObjectNodeEClass = createEClass(VISUAL_OBJECT_NODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		visualmodelPackage thevisualmodelPackage = (visualmodelPackage)EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		visualActionNodeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualInitialNodeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualActivityFinalNodeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualFlowFinalNodeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualDecisionMergeNodeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualForkJoinNodeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualSendSignalNodeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualReceiveSignalNodeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());
		visualObjectNodeEClass.getESuperTypes().add(thevisualmodelPackage.getVisualElement());

		// Initialize classes and features; add operations and parameters
		initEClass(visualActionNodeEClass, VisualActionNode.class, "VisualActionNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualInitialNodeEClass, VisualInitialNode.class, "VisualInitialNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualActivityFinalNodeEClass, VisualActivityFinalNode.class, "VisualActivityFinalNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualFlowFinalNodeEClass, VisualFlowFinalNode.class, "VisualFlowFinalNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualDecisionMergeNodeEClass, VisualDecisionMergeNode.class, "VisualDecisionMergeNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualForkJoinNodeEClass, VisualForkJoinNode.class, "VisualForkJoinNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualSendSignalNodeEClass, VisualSendSignalNode.class, "VisualSendSignalNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualReceiveSignalNodeEClass, VisualReceiveSignalNode.class, "VisualReceiveSignalNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(visualObjectNodeEClass, VisualObjectNode.class, "VisualObjectNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //ActivityPackageImpl
