/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Connection Decoration</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getConnectionDecoration()
 * @model
 * @generated
 */
public enum ConnectionDecoration implements Enumerator {
	/**
	 * The '<em><b>None</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NONE_VALUE
	 * @generated
	 * @ordered
	 */
	NONE(0, "None", "None"), /**
	 * The '<em><b>Arrow Head</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARROW_HEAD_VALUE
	 * @generated
	 * @ordered
	 */
	ARROW_HEAD(1, "ArrowHead", "ArrowHead"), /**
	 * The '<em><b>Arrow Head Solid</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARROW_HEAD_SOLID_VALUE
	 * @generated
	 * @ordered
	 */
	ARROW_HEAD_SOLID(2, "ArrowHeadSolid", "ArrowHeadSolid"), /**
	 * The '<em><b>Arrow Head Solid White</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARROW_HEAD_SOLID_WHITE_VALUE
	 * @generated
	 * @ordered
	 */
	ARROW_HEAD_SOLID_WHITE(3, "ArrowHeadSolidWhite", "ArrowHeadSolidWhite"), /**
	 * The '<em><b>Diamond White</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIAMOND_WHITE_VALUE
	 * @generated
	 * @ordered
	 */
	DIAMOND_WHITE(4, "DiamondWhite", "DiamondWhite"), /**
	 * The '<em><b>Diamond Black</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIAMOND_BLACK_VALUE
	 * @generated
	 * @ordered
	 */
	DIAMOND_BLACK(5, "DiamondBlack", "DiamondBlack");

	/**
	 * The '<em><b>None</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>None</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NONE
	 * @model name="None"
	 * @generated
	 * @ordered
	 */
	public static final int NONE_VALUE = 0;

	/**
	 * The '<em><b>Arrow Head</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Arrow Head</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ARROW_HEAD
	 * @model name="ArrowHead"
	 * @generated
	 * @ordered
	 */
	public static final int ARROW_HEAD_VALUE = 1;

	/**
	 * The '<em><b>Arrow Head Solid</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Arrow Head Solid</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ARROW_HEAD_SOLID
	 * @model name="ArrowHeadSolid"
	 * @generated
	 * @ordered
	 */
	public static final int ARROW_HEAD_SOLID_VALUE = 2;

	/**
	 * The '<em><b>Arrow Head Solid White</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Arrow Head Solid White</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ARROW_HEAD_SOLID_WHITE
	 * @model name="ArrowHeadSolidWhite"
	 * @generated
	 * @ordered
	 */
	public static final int ARROW_HEAD_SOLID_WHITE_VALUE = 3;

	/**
	 * The '<em><b>Diamond White</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diamond White</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIAMOND_WHITE
	 * @model name="DiamondWhite"
	 * @generated
	 * @ordered
	 */
	public static final int DIAMOND_WHITE_VALUE = 4;

	/**
	 * The '<em><b>Diamond Black</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diamond Black</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIAMOND_BLACK
	 * @model name="DiamondBlack"
	 * @generated
	 * @ordered
	 */
	public static final int DIAMOND_BLACK_VALUE = 5;

	/**
	 * An array of all the '<em><b>Connection Decoration</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ConnectionDecoration[] VALUES_ARRAY =
		new ConnectionDecoration[] {
			NONE,
			ARROW_HEAD,
			ARROW_HEAD_SOLID,
			ARROW_HEAD_SOLID_WHITE,
			DIAMOND_WHITE,
			DIAMOND_BLACK,
		};

	/**
	 * A public read-only list of all the '<em><b>Connection Decoration</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ConnectionDecoration> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Connection Decoration</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ConnectionDecoration get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ConnectionDecoration result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Connection Decoration</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ConnectionDecoration getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ConnectionDecoration result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Connection Decoration</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ConnectionDecoration get(int value) {
		switch (value) {
			case NONE_VALUE: return NONE;
			case ARROW_HEAD_VALUE: return ARROW_HEAD;
			case ARROW_HEAD_SOLID_VALUE: return ARROW_HEAD_SOLID;
			case ARROW_HEAD_SOLID_WHITE_VALUE: return ARROW_HEAD_SOLID_WHITE;
			case DIAMOND_WHITE_VALUE: return DIAMOND_WHITE;
			case DIAMOND_BLACK_VALUE: return DIAMOND_BLACK;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ConnectionDecoration(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ConnectionDecoration
