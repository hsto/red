/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualSendState;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Send State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualSendStateImpl extends VisualElementImpl implements VisualSendState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualSendStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.VISUAL_SEND_STATE;
	}

} //VisualSendStateImpl
