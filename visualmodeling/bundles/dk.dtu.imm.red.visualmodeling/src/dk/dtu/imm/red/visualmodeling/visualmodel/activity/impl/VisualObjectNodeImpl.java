/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode;

import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Object Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualObjectNodeImpl extends VisualElementImpl implements VisualObjectNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualObjectNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActivityPackage.Literals.VISUAL_OBJECT_NODE;
	}

} //VisualObjectNodeImpl
