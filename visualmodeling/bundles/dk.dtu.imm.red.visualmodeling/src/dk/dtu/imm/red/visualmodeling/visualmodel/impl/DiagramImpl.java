/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.Diagnostician;

import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.specificationelements.impl.SpecificationElementImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchState;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Diagram</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.DiagramImpl#getVisualDiagram <em>Visual Diagram</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.DiagramImpl#getMergeLog <em>Merge Log</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.DiagramImpl#getStitchState <em>Stitch State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DiagramImpl extends SpecificationElementImpl implements Diagram {
	/**
	 * The cached value of the '{@link #getVisualDiagram() <em>Visual Diagram</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisualDiagram()
	 * @generated
	 * @ordered
	 */
	protected VisualDiagram visualDiagram;

	/**
	 * The default value of the '{@link #getMergeLog() <em>Merge Log</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergeLog()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGE_LOG_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getMergeLog() <em>Merge Log</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergeLog()
	 * @generated
	 * @ordered
	 */
	protected String mergeLog = MERGE_LOG_EDEFAULT;

	/**
	 * The default value of the '{@link #getStitchState() <em>Stitch State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStitchState()
	 * @generated
	 * @ordered
	 */
	protected static final StitchState STITCH_STATE_EDEFAULT = StitchState.INITIAL;

	/**
	 * The cached value of the '{@link #getStitchState() <em>Stitch State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStitchState()
	 * @generated
	 * @ordered
	 */
	protected StitchState stitchState = STITCH_STATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiagramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return visualmodelPackage.Literals.DIAGRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualDiagram getVisualDiagram() {
		if (visualDiagram != null && visualDiagram.eIsProxy()) {
			InternalEObject oldVisualDiagram = (InternalEObject)visualDiagram;
			visualDiagram = (VisualDiagram)eResolveProxy(oldVisualDiagram);
			if (visualDiagram != oldVisualDiagram) {
				InternalEObject newVisualDiagram = (InternalEObject)visualDiagram;
				NotificationChain msgs =  oldVisualDiagram.eInverseRemove(this, visualmodelPackage.VISUAL_DIAGRAM__DIAGRAM_ELEMENT, VisualDiagram.class, null);
				if (newVisualDiagram.eInternalContainer() == null) {
					msgs =  newVisualDiagram.eInverseAdd(this, visualmodelPackage.VISUAL_DIAGRAM__DIAGRAM_ELEMENT, VisualDiagram.class, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM, oldVisualDiagram, visualDiagram));
			}
		}
		return visualDiagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualDiagram basicGetVisualDiagram() {
		return visualDiagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVisualDiagram(VisualDiagram newVisualDiagram, NotificationChain msgs) {
		VisualDiagram oldVisualDiagram = visualDiagram;
		visualDiagram = newVisualDiagram;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM, oldVisualDiagram, newVisualDiagram);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisualDiagram(VisualDiagram newVisualDiagram) {
		if (newVisualDiagram != visualDiagram) {
			NotificationChain msgs = null;
			if (visualDiagram != null)
				msgs = ((InternalEObject)visualDiagram).eInverseRemove(this, visualmodelPackage.VISUAL_DIAGRAM__DIAGRAM_ELEMENT, VisualDiagram.class, msgs);
			if (newVisualDiagram != null)
				msgs = ((InternalEObject)newVisualDiagram).eInverseAdd(this, visualmodelPackage.VISUAL_DIAGRAM__DIAGRAM_ELEMENT, VisualDiagram.class, msgs);
			msgs = basicSetVisualDiagram(newVisualDiagram, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM, newVisualDiagram, newVisualDiagram));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergeLog() {
		return mergeLog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergeLog(String newMergeLog) {
		String oldMergeLog = mergeLog;
		mergeLog = newMergeLog;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.DIAGRAM__MERGE_LOG, oldMergeLog, mergeLog));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StitchState getStitchState() {
		return stitchState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStitchState(StitchState newStitchState) {
		StitchState oldStitchState = stitchState;
		stitchState = newStitchState == null ? STITCH_STATE_EDEFAULT : newStitchState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.DIAGRAM__STITCH_STATE, oldStitchState, stitchState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM:
				if (visualDiagram != null)
					msgs = ((InternalEObject)visualDiagram).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM, null, msgs);
				return basicSetVisualDiagram((VisualDiagram)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM:
				return basicSetVisualDiagram(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM:
				if (resolve) return getVisualDiagram();
				return basicGetVisualDiagram();
			case visualmodelPackage.DIAGRAM__MERGE_LOG:
				return getMergeLog();
			case visualmodelPackage.DIAGRAM__STITCH_STATE:
				return getStitchState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM:
				setVisualDiagram((VisualDiagram)newValue);
				return;
			case visualmodelPackage.DIAGRAM__MERGE_LOG:
				setMergeLog((String)newValue);
				return;
			case visualmodelPackage.DIAGRAM__STITCH_STATE:
				setStitchState((StitchState)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM:
				setVisualDiagram((VisualDiagram)null);
				return;
			case visualmodelPackage.DIAGRAM__MERGE_LOG:
				setMergeLog(MERGE_LOG_EDEFAULT);
				return;
			case visualmodelPackage.DIAGRAM__STITCH_STATE:
				setStitchState(STITCH_STATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case visualmodelPackage.DIAGRAM__VISUAL_DIAGRAM:
				return visualDiagram != null;
			case visualmodelPackage.DIAGRAM__MERGE_LOG:
				return MERGE_LOG_EDEFAULT == null ? mergeLog != null : !MERGE_LOG_EDEFAULT.equals(mergeLog);
			case visualmodelPackage.DIAGRAM__STITCH_STATE:
				return stitchState != STITCH_STATE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mergeLog: ");
		result.append(mergeLog);
		result.append(", stitchState: ");
		result.append(stitchState);
		result.append(')');
		return result.toString();
	}

	/*
	 * @generated NOT
	 */
	@Override
	public void delete() {
		recursiveRemoveTraces(getVisualDiagram());
		super.delete();
	 }

	private void recursiveRemoveTraces(IVisualElement element){
		element.getStitchFrom().clear();
		element.getStitchTo().clear();

		for(IVisualConnection conn : element.getConnections()){
			conn.getStitchFrom().clear();
			conn.getStitchTo().clear();
		}

		for(IVisualElement child : element.getElements()){
			recursiveRemoveTraces(child);
		}
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getIconURI() {
		return "icons/diagram_16x16.png";
	}

	/**
	 * @generated NOT
	 */
	public void save()
	{
		System.out.println("SAVING!!!");
		System.out.println(this + " rel size is " + getRelatedBy().size());
		for(ElementRelationship rel: getRelatedBy()){
			System.out.println("Relationship is:" + rel);
			System.out.println(Diagnostician.INSTANCE.validate(rel));
		}
		super.save();

	}

	@Override
	public void checkStructure() {
		super.checkStructure();
		
		//Check 17C
		if (visualDiagram.getDiagramType() == DiagramType.SYSTEM_STRUCTURE
				&& getElementKind().equals(SystemStructureDiagramKind.DOMAIN_ARCHITECTURE.getLiteral())) {
			searchForPorts(visualDiagram);
		}
	}
	
	public void searchForPorts(IVisualElement parent) {
		for (IVisualElement element : parent.getElements()) {
			if (element instanceof VisualPort) {
				addIssueComment(IssueSeverity.WEAKNESS, "Domain architecture diagrams may not contain ports");
				break;
			}
			
			searchForPorts(element);
		}
	}
} //DiagramImpl
