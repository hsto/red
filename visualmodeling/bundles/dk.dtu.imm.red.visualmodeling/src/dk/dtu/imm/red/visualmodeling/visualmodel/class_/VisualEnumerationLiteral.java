/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Enumeration Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage#getVisualEnumerationLiteral()
 * @model
 * @generated
 */
public interface VisualEnumerationLiteral extends VisualElement {
} // VisualEnumerationLiteral
