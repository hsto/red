/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.specificationelements.SpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Specification Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#getSpecificationElement <em>Specification Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#isIsLinkedToElement <em>Is Linked To Element</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualSpecificationElement()
 * @model abstract="true"
 * @generated
 */
public interface VisualSpecificationElement extends VisualElement {
	/**
	 * Returns the value of the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification Element</em>' reference.
	 * @see #setSpecificationElement(SpecificationElement)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualSpecificationElement_SpecificationElement()
	 * @model
	 * @generated
	 */
	SpecificationElement getSpecificationElement();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#getSpecificationElement <em>Specification Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification Element</em>' reference.
	 * @see #getSpecificationElement()
	 * @generated
	 */
	void setSpecificationElement(SpecificationElement value);

	/**
	 * Returns the value of the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Linked To Element</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Linked To Element</em>' attribute.
	 * @see #isSetIsLinkedToElement()
	 * @see #unsetIsLinkedToElement()
	 * @see #setIsLinkedToElement(boolean)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualSpecificationElement_IsLinkedToElement()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isIsLinkedToElement();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#isIsLinkedToElement <em>Is Linked To Element</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Linked To Element</em>' attribute.
	 * @see #isSetIsLinkedToElement()
	 * @see #unsetIsLinkedToElement()
	 * @see #isIsLinkedToElement()
	 * @generated
	 */
	void setIsLinkedToElement(boolean value);

	/**
	 * Unsets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#isIsLinkedToElement <em>Is Linked To Element</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsLinkedToElement()
	 * @see #isIsLinkedToElement()
	 * @see #setIsLinkedToElement(boolean)
	 * @generated
	 */
	void unsetIsLinkedToElement();

	/**
	 * Returns whether the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement#isIsLinkedToElement <em>Is Linked To Element</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Linked To Element</em>' attribute is set.
	 * @see #unsetIsLinkedToElement()
	 * @see #isIsLinkedToElement()
	 * @see #setIsLinkedToElement(boolean)
	 * @generated
	 */
	boolean isSetIsLinkedToElement();

	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification Element Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Class<?> getSpecificationElementType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SpecificationElement createSpecificationElement(Group parent);
} // VisualSpecificationElement
