/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.port.PortFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VisualPortImpl extends VisualSpecificationElementImpl implements VisualPort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualPortImpl() {
		super();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public Class<?> getSpecificationElementType() {
		return Port.class;
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public SpecificationElement createSpecificationElement(Group parent) {
		Port port = PortFactory.eINSTANCE.createPort();
		port.setCreator(PreferenceUtil.getUserPreference_User());
		
		String str = (this.name == null) ? "" : this.name; 
		
		port.setName(str);
		port.setLabel(str);
		port.setDescription("");
		
		if (parent != null) {
			port.setParent(parent);
		}
		
		port.save();
		
		
		return port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemstructurePackage.Literals.VISUAL_PORT;
	}

} //VisualPortImpl
