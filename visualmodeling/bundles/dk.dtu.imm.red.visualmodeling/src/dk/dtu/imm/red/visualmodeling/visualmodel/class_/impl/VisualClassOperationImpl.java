/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.NoType;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.Multiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.NoMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Class Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassOperationImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassOperationImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassOperationImpl#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassOperationImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VisualClassOperationImpl extends VisualElementImpl implements VisualClassOperation {
	/**
	 * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected static final Visibility VISIBILITY_EDEFAULT = Visibility.UNSPECIFIED;

	/**
	 * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected Visibility visibility = VISIBILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected DataType type;

	/**
	 * The cached value of the '{@link #getMultiplicity() <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected Multiplicity multiplicity;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected DataType parameters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualClassOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassPackage.Literals.VISUAL_CLASS_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Visibility getVisibility() {
		return visibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibility(Visibility newVisibility) {
		Visibility oldVisibility = visibility;
		visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassPackage.VISUAL_CLASS_OPERATION__VISIBILITY, oldVisibility, visibility));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (DataType)eResolveProxy(oldType);
			if (type != oldType) {
				InternalEObject newType = (InternalEObject)type;
				NotificationChain msgs = oldType.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__TYPE, null, null);
				if (newType.eInternalContainer() == null) {
					msgs = newType.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__TYPE, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClassPackage.VISUAL_CLASS_OPERATION__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetType(DataType newType, NotificationChain msgs) {
		DataType oldType = type;
		type = newType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClassPackage.VISUAL_CLASS_OPERATION__TYPE, oldType, newType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(DataType newType) {
		if (newType != type) {
			NotificationChain msgs = null;
			if (type != null)
				msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__TYPE, null, msgs);
			if (newType != null)
				msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__TYPE, null, msgs);
			msgs = basicSetType(newType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassPackage.VISUAL_CLASS_OPERATION__TYPE, newType, newType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity getMultiplicity() {
		if (multiplicity != null && multiplicity.eIsProxy()) {
			InternalEObject oldMultiplicity = (InternalEObject)multiplicity;
			multiplicity = (Multiplicity)eResolveProxy(oldMultiplicity);
			if (multiplicity != oldMultiplicity) {
				InternalEObject newMultiplicity = (InternalEObject)multiplicity;
				NotificationChain msgs = oldMultiplicity.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY, null, null);
				if (newMultiplicity.eInternalContainer() == null) {
					msgs = newMultiplicity.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY, oldMultiplicity, multiplicity));
			}
		}
		return multiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity basicGetMultiplicity() {
		return multiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMultiplicity(Multiplicity newMultiplicity, NotificationChain msgs) {
		Multiplicity oldMultiplicity = multiplicity;
		multiplicity = newMultiplicity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY, oldMultiplicity, newMultiplicity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiplicity(Multiplicity newMultiplicity) {
		if (newMultiplicity != multiplicity) {
			NotificationChain msgs = null;
			if (multiplicity != null)
				msgs = ((InternalEObject)multiplicity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY, null, msgs);
			if (newMultiplicity != null)
				msgs = ((InternalEObject)newMultiplicity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY, null, msgs);
			msgs = basicSetMultiplicity(newMultiplicity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY, newMultiplicity, newMultiplicity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType getParameters() {
		if (parameters != null && parameters.eIsProxy()) {
			InternalEObject oldParameters = (InternalEObject)parameters;
			parameters = (DataType)eResolveProxy(oldParameters);
			if (parameters != oldParameters) {
				InternalEObject newParameters = (InternalEObject)parameters;
				NotificationChain msgs = oldParameters.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS, null, null);
				if (newParameters.eInternalContainer() == null) {
					msgs = newParameters.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS, oldParameters, parameters));
			}
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType basicGetParameters() {
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameters(DataType newParameters, NotificationChain msgs) {
		DataType oldParameters = parameters;
		parameters = newParameters;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS, oldParameters, newParameters);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameters(DataType newParameters) {
		if (newParameters != parameters) {
			NotificationChain msgs = null;
			if (parameters != null)
				msgs = ((InternalEObject)parameters).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS, null, msgs);
			if (newParameters != null)
				msgs = ((InternalEObject)newParameters).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS, null, msgs);
			msgs = basicSetParameters(newParameters, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS, newParameters, newParameters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassPackage.VISUAL_CLASS_OPERATION__TYPE:
				return basicSetType(null, msgs);
			case ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY:
				return basicSetMultiplicity(null, msgs);
			case ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS:
				return basicSetParameters(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassPackage.VISUAL_CLASS_OPERATION__VISIBILITY:
				return getVisibility();
			case ClassPackage.VISUAL_CLASS_OPERATION__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY:
				if (resolve) return getMultiplicity();
				return basicGetMultiplicity();
			case ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS:
				if (resolve) return getParameters();
				return basicGetParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassPackage.VISUAL_CLASS_OPERATION__VISIBILITY:
				setVisibility((Visibility)newValue);
				return;
			case ClassPackage.VISUAL_CLASS_OPERATION__TYPE:
				setType((DataType)newValue);
				return;
			case ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY:
				setMultiplicity((Multiplicity)newValue);
				return;
			case ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS:
				setParameters((DataType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassPackage.VISUAL_CLASS_OPERATION__VISIBILITY:
				setVisibility(VISIBILITY_EDEFAULT);
				return;
			case ClassPackage.VISUAL_CLASS_OPERATION__TYPE:
				setType((DataType)null);
				return;
			case ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY:
				setMultiplicity((Multiplicity)null);
				return;
			case ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS:
				setParameters((DataType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassPackage.VISUAL_CLASS_OPERATION__VISIBILITY:
				return visibility != VISIBILITY_EDEFAULT;
			case ClassPackage.VISUAL_CLASS_OPERATION__TYPE:
				return type != null;
			case ClassPackage.VISUAL_CLASS_OPERATION__MULTIPLICITY:
				return multiplicity != null;
			case ClassPackage.VISUAL_CLASS_OPERATION__PARAMETERS:
				return parameters != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(" ");
		if(visibility!=Visibility.UNSPECIFIED){
			result.append(visibility.getUMLSyntax());
			result.append(" ");
		}
		result.append(name);
		result.append("(");
		result.append(parameters);
		result.append(")");
		if(type!=null && !(type instanceof NoType)){
			result.append(" : "+type.toString());
		}
		if(!(multiplicity instanceof NoMultiplicity)){
			result.append(" [");
			result.append(multiplicity.toString());
			result.append("]");
		}
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	public void setValues(VisualClassOperation newValue){
		setName(newValue.getName());
		setType(newValue.getType());
		setMultiplicity(newValue.getMultiplicity());
		setVisibility(newValue.getVisibility());
		setParameters(newValue.getParameters());
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisualClassOperationImpl other = (VisualClassOperationImpl) obj;

		if (name != other.name)
			return false;
		if (multiplicity != other.multiplicity)
			return false;
		if (parameters == null) {
			if (other.parameters != null)
				return false;
		} else if (!parameters.equals(other.parameters))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (visibility != other.visibility)
			return false;
		return true;
	}

} //VisualClassOperationImpl
