/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.*;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ImporttypesFactoryImpl extends EFactoryImpl implements ImporttypesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ImporttypesFactory init() {
		try {
			ImporttypesFactory theImporttypesFactory = (ImporttypesFactory)EPackage.Registry.INSTANCE.getEFactory(ImporttypesPackage.eNS_URI);
			if (theImporttypesFactory != null) {
				return theImporttypesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ImporttypesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImporttypesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ImporttypesPackage.POINT:
				return createPointFromString(eDataType, initialValue);
			case ImporttypesPackage.RECTANGLE:
				return createRectangleFromString(eDataType, initialValue);
			case ImporttypesPackage.DIMENSION:
				return createDimensionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ImporttypesPackage.POINT:
				return convertPointToString(eDataType, instanceValue);
			case ImporttypesPackage.RECTANGLE:
				return convertRectangleToString(eDataType, instanceValue);
			case ImporttypesPackage.DIMENSION:
				return convertDimensionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Point createPointFromString(EDataType eDataType, String initialValue) {
		
		initialValue.replaceAll("\\s", "");
		String[] values = initialValue.split(",");		
		return new Point(Integer.parseInt(values[0]),Integer.parseInt(values[1]));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertPointToString(EDataType eDataType, Object instanceValue) {
		
		if(instanceValue == null) {
		    return null;
		}
		
		Point point = (Point)instanceValue;
		
		return String.format("%d,%d", point.x, point.y);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Rectangle createRectangleFromString(EDataType eDataType, String initialValue) {
		
		if(initialValue == null) {
		    return null;
		  }
		  
		  initialValue.replaceAll("\\s", "");
		  String[] values = initialValue.split(",");
		  
		  if(values.length != 4) {
		    return null;
		  }
		 
		  Rectangle rect = new Rectangle();
		  
		  try {
		    rect.setLocation(Integer.parseInt(values[0]), Integer.parseInt(values[1]));
		    rect.setSize(Integer.parseInt(values[2]), Integer.parseInt(values[3]));
		  } catch(NumberFormatException e) {
		    EcorePlugin.INSTANCE.log(e);
		    rect = null;
		  }
		  
		  return rect;	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertRectangleToString(EDataType eDataType, Object instanceValue) {
		if(instanceValue == null) {
		    return null;
		  }
		  Rectangle rect = (Rectangle) instanceValue;
		  
		  return rect.x+","+rect.y+","+rect.width+","+rect.height;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Dimension createDimensionFromString(EDataType eDataType, String initialValue) {
		initialValue.replaceAll("\\s", "");
		String[] values = initialValue.split(",");		
		return new Dimension(Integer.parseInt(values[0]),Integer.parseInt(values[1]));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertDimensionToString(EDataType eDataType, Object instanceValue) {
		
		if(instanceValue == null) {
		    return null;
		}
		
		Dimension dimension = (Dimension)instanceValue;
		
		return String.format("%d,%d", dimension.width, dimension.height);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImporttypesPackage getImporttypesPackage() {
		return (ImporttypesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ImporttypesPackage getPackage() {
		return ImporttypesPackage.eINSTANCE;
	}

} //ImporttypesFactoryImpl
