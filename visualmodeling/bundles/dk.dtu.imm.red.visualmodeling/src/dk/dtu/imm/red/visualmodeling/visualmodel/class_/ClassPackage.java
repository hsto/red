/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassFactory
 * @model kind="package"
 * @generated
 */
public interface ClassPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "class";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.visualmodeling.visualmodel.class";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "class";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassPackage eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassElementImpl <em>Visual Class Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualClassElement()
	 * @generated
	 */
	int VISUAL_CLASS_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__LOCATION = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__BOUNDS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__PARENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__DIAGRAM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__ELEMENTS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__CONNECTIONS = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__IS_SKETCHY = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__NAME = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__VISUAL_ID = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__STITCH_FROM = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__STITCH_TO = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Specification Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__SPECIFICATION_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Linked To Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__IS_LINKED_TO_ELEMENT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__ABSTRACT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT__INTERFACE = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Visual Class Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ELEMENT_FEATURE_COUNT = visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassAttributeImpl <em>Visual Class Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassAttributeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualClassAttribute()
	 * @generated
	 */
	int VISUAL_CLASS_ATTRIBUTE = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__VISIBILITY = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__READ_ONLY = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__TYPE = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Visual Class Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_ATTRIBUTE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassOperationImpl <em>Visual Class Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassOperationImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualClassOperation()
	 * @generated
	 */
	int VISUAL_CLASS_OPERATION = 2;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__VISIBILITY = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__TYPE = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__MULTIPLICITY = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION__PARAMETERS = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Visual Class Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_CLASS_OPERATION_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualEnumerationElementImpl <em>Visual Enumeration Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualEnumerationElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualEnumerationElement()
	 * @generated
	 */
	int VISUAL_ENUMERATION_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Enumeration Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_ELEMENT_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualEnumerationLiteralImpl <em>Visual Enumeration Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualEnumerationLiteralImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualEnumerationLiteral()
	 * @generated
	 */
	int VISUAL_ENUMERATION_LITERAL = 4;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Enumeration Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ENUMERATION_LITERAL_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualPackageElementImpl <em>Visual Package Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualPackageElementImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualPackageElement()
	 * @generated
	 */
	int VISUAL_PACKAGE_ELEMENT = 5;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Package Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_PACKAGE_ELEMENT_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement <em>Visual Class Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Class Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement
	 * @generated
	 */
	EClass getVisualClassElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement#isAbstract()
	 * @see #getVisualClassElement()
	 * @generated
	 */
	EAttribute getVisualClassElement_Abstract();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement#isInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interface</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement#isInterface()
	 * @see #getVisualClassElement()
	 * @generated
	 */
	EAttribute getVisualClassElement_Interface();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute <em>Visual Class Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Class Attribute</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute
	 * @generated
	 */
	EClass getVisualClassAttribute();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute#getVisibility()
	 * @see #getVisualClassAttribute()
	 * @generated
	 */
	EAttribute getVisualClassAttribute_Visibility();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute#getType()
	 * @see #getVisualClassAttribute()
	 * @generated
	 */
	EReference getVisualClassAttribute_Type();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Multiplicity</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute#getMultiplicity()
	 * @see #getVisualClassAttribute()
	 * @generated
	 */
	EReference getVisualClassAttribute_Multiplicity();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute#isReadOnly <em>Read Only</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Read Only</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute#isReadOnly()
	 * @see #getVisualClassAttribute()
	 * @generated
	 */
	EAttribute getVisualClassAttribute_ReadOnly();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation <em>Visual Class Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Class Operation</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation
	 * @generated
	 */
	EClass getVisualClassOperation();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getVisibility()
	 * @see #getVisualClassOperation()
	 * @generated
	 */
	EAttribute getVisualClassOperation_Visibility();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getType()
	 * @see #getVisualClassOperation()
	 * @generated
	 */
	EReference getVisualClassOperation_Type();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Multiplicity</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getMultiplicity()
	 * @see #getVisualClassOperation()
	 * @generated
	 */
	EReference getVisualClassOperation_Multiplicity();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation#getParameters()
	 * @see #getVisualClassOperation()
	 * @generated
	 */
	EReference getVisualClassOperation_Parameters();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement <em>Visual Enumeration Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Enumeration Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement
	 * @generated
	 */
	EClass getVisualEnumerationElement();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral <em>Visual Enumeration Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Enumeration Literal</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral
	 * @generated
	 */
	EClass getVisualEnumerationLiteral();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualPackageElement <em>Visual Package Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Package Element</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualPackageElement
	 * @generated
	 */
	EClass getVisualPackageElement();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClassFactory getClassFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassElementImpl <em>Visual Class Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualClassElement()
		 * @generated
		 */
		EClass VISUAL_CLASS_ELEMENT = eINSTANCE.getVisualClassElement();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_CLASS_ELEMENT__ABSTRACT = eINSTANCE.getVisualClassElement_Abstract();

		/**
		 * The meta object literal for the '<em><b>Interface</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_CLASS_ELEMENT__INTERFACE = eINSTANCE.getVisualClassElement_Interface();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassAttributeImpl <em>Visual Class Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassAttributeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualClassAttribute()
		 * @generated
		 */
		EClass VISUAL_CLASS_ATTRIBUTE = eINSTANCE.getVisualClassAttribute();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_CLASS_ATTRIBUTE__VISIBILITY = eINSTANCE.getVisualClassAttribute_Visibility();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_CLASS_ATTRIBUTE__TYPE = eINSTANCE.getVisualClassAttribute_Type();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_CLASS_ATTRIBUTE__MULTIPLICITY = eINSTANCE.getVisualClassAttribute_Multiplicity();

		/**
		 * The meta object literal for the '<em><b>Read Only</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_CLASS_ATTRIBUTE__READ_ONLY = eINSTANCE.getVisualClassAttribute_ReadOnly();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassOperationImpl <em>Visual Class Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassOperationImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualClassOperation()
		 * @generated
		 */
		EClass VISUAL_CLASS_OPERATION = eINSTANCE.getVisualClassOperation();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_CLASS_OPERATION__VISIBILITY = eINSTANCE.getVisualClassOperation_Visibility();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_CLASS_OPERATION__TYPE = eINSTANCE.getVisualClassOperation_Type();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_CLASS_OPERATION__MULTIPLICITY = eINSTANCE.getVisualClassOperation_Multiplicity();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VISUAL_CLASS_OPERATION__PARAMETERS = eINSTANCE.getVisualClassOperation_Parameters();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualEnumerationElementImpl <em>Visual Enumeration Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualEnumerationElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualEnumerationElement()
		 * @generated
		 */
		EClass VISUAL_ENUMERATION_ELEMENT = eINSTANCE.getVisualEnumerationElement();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualEnumerationLiteralImpl <em>Visual Enumeration Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualEnumerationLiteralImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualEnumerationLiteral()
		 * @generated
		 */
		EClass VISUAL_ENUMERATION_LITERAL = eINSTANCE.getVisualEnumerationLiteral();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualPackageElementImpl <em>Visual Package Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualPackageElementImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassPackageImpl#getVisualPackageElement()
		 * @generated
		 */
		EClass VISUAL_PACKAGE_ELEMENT = eINSTANCE.getVisualPackageElement();

	}

} //ClassPackage
