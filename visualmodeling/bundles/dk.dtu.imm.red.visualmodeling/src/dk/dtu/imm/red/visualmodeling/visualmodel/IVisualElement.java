/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IVisual Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getLocation <em>Location</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getBounds <em>Bounds</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getDiagram <em>Diagram</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getElements <em>Elements</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getConnections <em>Connections</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#isIsSketchy <em>Is Sketchy</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getValidationMessage <em>Validation Message</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getVisualID <em>Visual ID</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchFrom <em>Stitch From</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchTo <em>Stitch To</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IVisualElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see #setLocation(Point)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_Location()
	 * @model dataType="dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.Point"
	 * @generated
	 */
	Point getLocation();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getLocation <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' attribute.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(Point value);

	/**
	 * Returns the value of the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bounds</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bounds</em>' attribute.
	 * @see #setBounds(Dimension)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_Bounds()
	 * @model dataType="dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.Dimension"
	 * @generated
	 */
	Dimension getBounds();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getBounds <em>Bounds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bounds</em>' attribute.
	 * @see #getBounds()
	 * @generated
	 */
	void setBounds(Dimension value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(IVisualElement)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_Parent()
	 * @model
	 * @generated
	 */
	IVisualElement getParent();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(IVisualElement value);

	/**
	 * Returns the value of the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram</em>' reference.
	 * @see #setDiagram(VisualDiagram)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_Diagram()
	 * @model
	 * @generated
	 */
	VisualDiagram getDiagram();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getDiagram <em>Diagram</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diagram</em>' reference.
	 * @see #getDiagram()
	 * @generated
	 */
	void setDiagram(VisualDiagram value);

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_Elements()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<IVisualElement> getElements();

	/**
	 * Returns the value of the '<em><b>Connections</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connections</em>' reference list.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_Connections()
	 * @model
	 * @generated
	 */
	EList<IVisualConnection> getConnections();

	/**
	 * Returns the value of the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Sketchy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Sketchy</em>' attribute.
	 * @see #setIsSketchy(boolean)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_IsSketchy()
	 * @model
	 * @generated
	 */
	boolean isIsSketchy();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#isIsSketchy <em>Is Sketchy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Sketchy</em>' attribute.
	 * @see #isIsSketchy()
	 * @generated
	 */
	void setIsSketchy(boolean value);

	/**
	 * Returns the value of the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validation Message</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validation Message</em>' attribute.
	 * @see #setValidationMessage(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_ValidationMessage()
	 * @model
	 * @generated
	 */
	String getValidationMessage();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getValidationMessage <em>Validation Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validation Message</em>' attribute.
	 * @see #getValidationMessage()
	 * @generated
	 */
	void setValidationMessage(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visual ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visual ID</em>' attribute.
	 * @see #setVisualID(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_VisualID()
	 * @model id="true"
	 * @generated
	 */
	String getVisualID();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getVisualID <em>Visual ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visual ID</em>' attribute.
	 * @see #getVisualID()
	 * @generated
	 */
	void setVisualID(String value);

	/**
	 * Returns the value of the '<em><b>Stitch From</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship}.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOutput <em>Stitch Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stitch From</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stitch From</em>' containment reference list.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_StitchFrom()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOutput
	 * @model opposite="stitchOutput" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<StitchElementRelationship> getStitchFrom();

	/**
	 * Returns the value of the '<em><b>Stitch To</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship}.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOrigin <em>Stitch Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stitch To</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stitch To</em>' reference list.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualElement_StitchTo()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOrigin
	 * @model opposite="stitchOrigin"
	 * @generated
	 */
	EList<StitchElementRelationship> getStitchTo();

} // IVisualElement
