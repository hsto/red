/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import org.eclipse.draw2d.geometry.Point;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IVisual Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSource <em>Source</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTarget <em>Target</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getDirection <em>Direction</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getBendpoints <em>Bendpoints</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getLineStyle <em>Line Style</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSourceDecoration <em>Source Decoration</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTargetDecoration <em>Target Decoration</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getValidationMessage <em>Validation Message</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSourceMultiplicity <em>Source Multiplicity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTargetMultiplicity <em>Target Multiplicity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getRounting <em>Rounting</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getVisualID <em>Visual ID</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchFrom <em>Stitch From</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchTo <em>Stitch To</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IVisualConnection extends EObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(IVisualElement)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_Source()
	 * @model
	 * @generated
	 */
	IVisualElement getSource();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(IVisualElement value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(IVisualElement)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_Target()
	 * @model
	 * @generated
	 */
	IVisualElement getTarget();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(IVisualElement value);

	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection
	 * @see #setDirection(ConnectionDirection)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_Direction()
	 * @model
	 * @generated
	 */
	ConnectionDirection getDirection();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(ConnectionDirection value);

	/**
	 * Returns the value of the '<em><b>Bendpoints</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.draw2d.geometry.Point}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bendpoints</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bendpoints</em>' attribute list.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_Bendpoints()
	 * @model dataType="dk.dtu.imm.red.visualmodeling.visualmodel.importtypes.Point"
	 * @generated
	 */
	EList<Point> getBendpoints();

	/**
	 * Returns the value of the '<em><b>Line Style</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Line Style</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Line Style</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle
	 * @see #setLineStyle(ConnectionLineStyle)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_LineStyle()
	 * @model
	 * @generated
	 */
	ConnectionLineStyle getLineStyle();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getLineStyle <em>Line Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Line Style</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle
	 * @see #getLineStyle()
	 * @generated
	 */
	void setLineStyle(ConnectionLineStyle value);

	/**
	 * Returns the value of the '<em><b>Source Decoration</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Decoration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Decoration</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration
	 * @see #setSourceDecoration(ConnectionDecoration)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_SourceDecoration()
	 * @model
	 * @generated
	 */
	ConnectionDecoration getSourceDecoration();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSourceDecoration <em>Source Decoration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Decoration</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration
	 * @see #getSourceDecoration()
	 * @generated
	 */
	void setSourceDecoration(ConnectionDecoration value);

	/**
	 * Returns the value of the '<em><b>Target Decoration</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Decoration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Decoration</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration
	 * @see #setTargetDecoration(ConnectionDecoration)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_TargetDecoration()
	 * @model
	 * @generated
	 */
	ConnectionDecoration getTargetDecoration();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTargetDecoration <em>Target Decoration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Decoration</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration
	 * @see #getTargetDecoration()
	 * @generated
	 */
	void setTargetDecoration(ConnectionDecoration value);

	/**
	 * Returns the value of the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validation Message</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validation Message</em>' attribute.
	 * @see #setValidationMessage(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_ValidationMessage()
	 * @model
	 * @generated
	 */
	String getValidationMessage();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getValidationMessage <em>Validation Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validation Message</em>' attribute.
	 * @see #getValidationMessage()
	 * @generated
	 */
	void setValidationMessage(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Source Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Multiplicity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Multiplicity</em>' attribute.
	 * @see #setSourceMultiplicity(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_SourceMultiplicity()
	 * @model
	 * @generated
	 */
	String getSourceMultiplicity();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getSourceMultiplicity <em>Source Multiplicity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Multiplicity</em>' attribute.
	 * @see #getSourceMultiplicity()
	 * @generated
	 */
	void setSourceMultiplicity(String value);

	/**
	 * Returns the value of the '<em><b>Target Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Multiplicity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Multiplicity</em>' attribute.
	 * @see #setTargetMultiplicity(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_TargetMultiplicity()
	 * @model
	 * @generated
	 */
	String getTargetMultiplicity();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getTargetMultiplicity <em>Target Multiplicity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Multiplicity</em>' attribute.
	 * @see #getTargetMultiplicity()
	 * @generated
	 */
	void setTargetMultiplicity(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Rounting</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rounting</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rounting</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting
	 * @see #setRounting(ConnectionRouting)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_Rounting()
	 * @model
	 * @generated
	 */
	ConnectionRouting getRounting();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getRounting <em>Rounting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rounting</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting
	 * @see #getRounting()
	 * @generated
	 */
	void setRounting(ConnectionRouting value);

	/**
	 * Returns the value of the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visual ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visual ID</em>' attribute.
	 * @see #setVisualID(String)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_VisualID()
	 * @model id="true"
	 * @generated
	 */
	String getVisualID();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getVisualID <em>Visual ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visual ID</em>' attribute.
	 * @see #getVisualID()
	 * @generated
	 */
	void setVisualID(String value);

	/**
	 * Returns the value of the '<em><b>Stitch From</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship}.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOutput <em>Stitch Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stitch From</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stitch From</em>' containment reference list.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_StitchFrom()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOutput
	 * @model opposite="stitchOutput" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<StitchConnectionRelationship> getStitchFrom();

	/**
	 * Returns the value of the '<em><b>Stitch To</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship}.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOrigin <em>Stitch Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stitch To</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stitch To</em>' reference list.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getIVisualConnection_StitchTo()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOrigin
	 * @model opposite="stitchOrigin"
	 * @generated
	 */
	EList<StitchConnectionRelationship> getStitchTo();

} // IVisualConnection
