/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramConnections <em>Diagram Connections</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramType <em>Diagram Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramElement <em>Diagram Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#isIsStitchOutput <em>Is Stitch Output</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualDiagram()
 * @model
 * @generated
 */
public interface VisualDiagram extends IVisualElement {
	/**
	 * Returns the value of the '<em><b>Diagram Connections</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram Connections</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram Connections</em>' containment reference list.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualDiagram_DiagramConnections()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<IVisualConnection> getDiagramConnections();

	/**
	 * Returns the value of the '<em><b>Diagram Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram Type</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType
	 * @see #setDiagramType(DiagramType)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualDiagram_DiagramType()
	 * @model
	 * @generated
	 */
	DiagramType getDiagramType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramType <em>Diagram Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diagram Type</em>' attribute.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType
	 * @see #getDiagramType()
	 * @generated
	 */
	void setDiagramType(DiagramType value);

	/**
	 * Returns the value of the '<em><b>Diagram Element</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getVisualDiagram <em>Visual Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram Element</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram Element</em>' container reference.
	 * @see #setDiagramElement(Diagram)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualDiagram_DiagramElement()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.Diagram#getVisualDiagram
	 * @model opposite="VisualDiagram" transient="false"
	 * @generated
	 */
	Diagram getDiagramElement();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#getDiagramElement <em>Diagram Element</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diagram Element</em>' container reference.
	 * @see #getDiagramElement()
	 * @generated
	 */
	void setDiagramElement(Diagram value);

	/**
	 * Returns the value of the '<em><b>Is Stitch Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Stitch Output</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Stitch Output</em>' attribute.
	 * @see #setIsStitchOutput(boolean)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualDiagram_IsStitchOutput()
	 * @model
	 * @generated
	 */
	boolean isIsStitchOutput();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram#isIsStitchOutput <em>Is Stitch Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Stitch Output</em>' attribute.
	 * @see #isIsStitchOutput()
	 * @generated
	 */
	void setIsStitchOutput(boolean value);

} // VisualDiagram
