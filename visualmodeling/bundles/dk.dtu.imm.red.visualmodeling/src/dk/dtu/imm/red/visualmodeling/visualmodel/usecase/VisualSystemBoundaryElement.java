/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.usecase;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual System Boundary Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecasePackage#getVisualSystemBoundaryElement()
 * @model
 * @generated
 */
public interface VisualSystemBoundaryElement extends VisualElement {
} // VisualSystemBoundaryElement
