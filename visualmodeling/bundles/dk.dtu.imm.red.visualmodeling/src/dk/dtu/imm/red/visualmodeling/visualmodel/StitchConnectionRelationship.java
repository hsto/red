/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stitch Connection Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOrigin <em>Stitch Origin</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOutput <em>Stitch Output</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getStitchConnectionRelationship()
 * @model
 * @generated
 */
public interface StitchConnectionRelationship extends EObject {
	/**
	 * Returns the value of the '<em><b>Stitch Origin</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchTo <em>Stitch To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stitch Origin</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stitch Origin</em>' reference.
	 * @see #setStitchOrigin(IVisualConnection)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getStitchConnectionRelationship_StitchOrigin()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchTo
	 * @model opposite="stitchTo" required="true"
	 * @generated
	 */
	IVisualConnection getStitchOrigin();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOrigin <em>Stitch Origin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stitch Origin</em>' reference.
	 * @see #getStitchOrigin()
	 * @generated
	 */
	void setStitchOrigin(IVisualConnection value);

	/**
	 * Returns the value of the '<em><b>Stitch Output</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchFrom <em>Stitch From</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stitch Output</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stitch Output</em>' container reference.
	 * @see #setStitchOutput(IVisualConnection)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getStitchConnectionRelationship_StitchOutput()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection#getStitchFrom
	 * @model opposite="stitchFrom" required="true" transient="false"
	 * @generated
	 */
	IVisualConnection getStitchOutput();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship#getStitchOutput <em>Stitch Output</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stitch Output</em>' container reference.
	 * @see #getStitchOutput()
	 * @generated
	 */
	void setStitchOutput(IVisualConnection value);

} // StitchConnectionRelationship
