/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getVisualElement()
 * @model
 * @generated
 */
public interface VisualElement extends IVisualElement {

} // VisualElement
