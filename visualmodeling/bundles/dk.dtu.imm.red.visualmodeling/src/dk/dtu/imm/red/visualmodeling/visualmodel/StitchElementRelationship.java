/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stitch Element Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOrigin <em>Stitch Origin</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOutput <em>Stitch Output</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getStitchElementRelationship()
 * @model
 * @generated
 */
public interface StitchElementRelationship extends EObject {
	/**
	 * Returns the value of the '<em><b>Stitch Origin</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchTo <em>Stitch To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stitch Origin</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stitch Origin</em>' reference.
	 * @see #setStitchOrigin(IVisualElement)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getStitchElementRelationship_StitchOrigin()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchTo
	 * @model opposite="stitchTo" required="true"
	 * @generated
	 */
	IVisualElement getStitchOrigin();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOrigin <em>Stitch Origin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stitch Origin</em>' reference.
	 * @see #getStitchOrigin()
	 * @generated
	 */
	void setStitchOrigin(IVisualElement value);

	/**
	 * Returns the value of the '<em><b>Stitch Output</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchFrom <em>Stitch From</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stitch Output</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stitch Output</em>' container reference.
	 * @see #setStitchOutput(IVisualElement)
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage#getStitchElementRelationship_StitchOutput()
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement#getStitchFrom
	 * @model opposite="stitchFrom" required="true" transient="false"
	 * @generated
	 */
	IVisualElement getStitchOutput();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship#getStitchOutput <em>Stitch Output</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stitch Output</em>' container reference.
	 * @see #getStitchOutput()
	 * @generated
	 */
	void setStitchOutput(IVisualElement value);

} // StitchElementRelationship
