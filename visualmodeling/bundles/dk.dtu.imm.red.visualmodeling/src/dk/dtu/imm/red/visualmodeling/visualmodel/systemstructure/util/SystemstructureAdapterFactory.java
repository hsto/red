/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.util;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage
 * @generated
 */
public class SystemstructureAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SystemstructurePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemstructureAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SystemstructurePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemstructureSwitch<Adapter> modelSwitch =
		new SystemstructureSwitch<Adapter>() {
			@Override
			public Adapter caseVisualSystemStructureActor(VisualSystemStructureActor object) {
				return createVisualSystemStructureActorAdapter();
			}
			@Override
			public Adapter caseVisualSystem(VisualSystem object) {
				return createVisualSystemAdapter();
			}
			@Override
			public Adapter caseVisualPort(VisualPort object) {
				return createVisualPortAdapter();
			}
			@Override
			public Adapter caseIVisualElement(IVisualElement object) {
				return createIVisualElementAdapter();
			}
			@Override
			public Adapter caseVisualElement(VisualElement object) {
				return createVisualElementAdapter();
			}
			@Override
			public Adapter caseVisualSpecificationElement(VisualSpecificationElement object) {
				return createVisualSpecificationElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor <em>Visual System Structure Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor
	 * @generated
	 */
	public Adapter createVisualSystemStructureActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem <em>Visual System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem
	 * @generated
	 */
	public Adapter createVisualSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort <em>Visual Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort
	 * @generated
	 */
	public Adapter createVisualPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement <em>IVisual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement
	 * @generated
	 */
	public Adapter createIVisualElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement <em>Visual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement
	 * @generated
	 */
	public Adapter createVisualElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement <em>Visual Specification Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement
	 * @generated
	 */
	public Adapter createVisualSpecificationElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SystemstructureAdapterFactory
