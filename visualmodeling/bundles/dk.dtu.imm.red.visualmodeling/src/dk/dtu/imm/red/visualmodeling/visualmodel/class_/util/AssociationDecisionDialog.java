package dk.dtu.imm.red.visualmodeling.visualmodel.class_.util;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

public class AssociationDecisionDialog extends Dialog {

	private IVisualConnection connection;
	private VisualElement subject;
	private AssociationDecision decision = AssociationDecision.IGNORE;

	public AssociationDecisionDialog(Shell parentShell, IVisualConnection connection, VisualElement subject) {
		super(parentShell);

		this.connection = connection;
		this.subject = subject;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);

		if (connection.getType().equals(ConnectionType.GENERALIZATION.getName())) {
			createGeneralizationDialogArea(container);
		}
		else if (connection.getType().equals(ConnectionType.COMPOSITION.getName())) {
			createCompositionDialogArea(container);
		}
		else if (connection.getType().equals(ConnectionType.ASSOCIATION.getName())) {
			createAssociationDialogArea(container);
		}
		else {
			throw new IllegalArgumentException(
					"This dialog does not support connections of type: " + connection.getType());
		}

		return container;
	}

	private void createGeneralizationDialogArea(Composite container) {
		Label label = new Label(container, SWT.NONE);
		label.setText(String.format("%s is a subclass of %s. Choose a method for implementing this:", 
				subject.getName(), connection.getTarget().getName()));
		
		Button ignoreRadio = new Button(container, SWT.RADIO);
		ignoreRadio.setSelection(true);
		ignoreRadio.setText("Do nothing");
		ignoreRadio.addSelectionListener(new DecisionSelectedListener(AssociationDecision.IGNORE));

		Button embedRadio = new Button(container, SWT.RADIO);
		embedRadio.setText(String.format("Embed (Create member %s: %s inside %s)",
				nonCapitalized(connection.getTarget().getName()), connection.getTarget().getName(), subject.getName()));
		embedRadio.addSelectionListener(new DecisionSelectedListener(AssociationDecision.EMBED));

		Button inlineRadio = new Button(container, SWT.RADIO);
		inlineRadio.setText(String.format("Inline (Add members of %s to %s)",
				connection.getTarget().getName(), subject.getName()));
		inlineRadio.addSelectionListener(new DecisionSelectedListener(AssociationDecision.INLINE));

		/*
		 * Button linkRadio = new Button(container, SWT.RADIO);
		 * linkRadio.setText("Ignore");
		 * linkRadio.addSelectionListener(new
		 * DecisionSelectedListener(AssociationDecision.LINK_SOURCE_TARGET));
		 */
	}

	private void createCompositionDialogArea(Composite container) {
		Label label = new Label(container, SWT.NONE);
		label.setText(String.format("%s has a composition of %s. Choose a method for implementing this:", 
				subject.getName(), connection.getTarget().getName()));
		
		Button ignoreRadio = new Button(container, SWT.RADIO);
		ignoreRadio.setSelection(true);
		ignoreRadio.setText("Do nothing");
		ignoreRadio.addSelectionListener(new DecisionSelectedListener(AssociationDecision.IGNORE));

		Button embedRadio = new Button(container, SWT.RADIO);
		embedRadio.setText(String.format("Embed (Create member %s: %s inside %s)",
				nonCapitalized(connection.getTarget().getName()), connection.getTarget().getName(), subject.getName()));
		embedRadio.addSelectionListener(new DecisionSelectedListener(AssociationDecision.EMBED));

		Button inlineRadio = new Button(container, SWT.RADIO);
		inlineRadio.setText(String.format("Inline (Add members of %s to %s)",
				connection.getTarget().getName(), subject.getName()));
		inlineRadio.addSelectionListener(new DecisionSelectedListener(AssociationDecision.INLINE));

		/*
		 * Button linkRadio = new Button(container, SWT.RADIO);
		 * linkRadio.setText("Ignore");
		 * linkRadio.addSelectionListener(new
		 * DecisionSelectedListener(AssociationDecision.LINK_SOURCE_TARGET));
		 */
	}

	private void createAssociationDialogArea(Composite container) {
		IVisualElement otherElement = (connection.getSource() == subject) ? connection.getTarget() : connection.getSource();
		
		Label label = new Label(container, SWT.NONE);
		label.setText(String.format("%s has an association with %s. Choose a method for implementing this:", 
				subject.getName(), otherElement.getName(), subject.getName()));
		
		Button ignoreRadio = new Button(container, SWT.RADIO);
		ignoreRadio.setSelection(true);
		ignoreRadio.setText("Do nothing");
		ignoreRadio.addSelectionListener(new DecisionSelectedListener(AssociationDecision.IGNORE));

		Button embedRadio = new Button(container, SWT.RADIO);
		embedRadio.setText(String.format("Reference (Create member %s: %s inside %s)",
				nonCapitalized(otherElement.getName()), otherElement.getName(), subject.getName()));
		embedRadio.addSelectionListener(new DecisionSelectedListener(AssociationDecision.EMBED));
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Element selection dialog");
	}

	@Override
	protected Point getInitialSize() {
		return new Point(500, 250);
	}

	public AssociationDecision getDecision() {
		return decision;
	}

	class DecisionSelectedListener implements SelectionListener {
		private AssociationDecision outputDecision;

		public DecisionSelectedListener(AssociationDecision outputDecision) {
			this.outputDecision = outputDecision;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			decision = outputDecision;
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			decision = outputDecision;
		}
	}

	private String nonCapitalized(String string) {
		if (string == null || string.length() == 0) {
			return string;
		}
		char c[] = string.toCharArray();
		c[0] = Character.toLowerCase(c[0]);
		return new String(c);
	}
}
