/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl;

import dk.dtu.imm.red.specificationelements.goal.GoalKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Goal Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalLayerImpl#getBelowLayer <em>Below Layer</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalLayerImpl#getAboveLayer <em>Above Layer</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.goal.impl.VisualGoalLayerImpl#getLayerKind <em>Layer Kind</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VisualGoalLayerImpl extends VisualElementImpl implements VisualGoalLayer {
	/**
	 * The cached value of the '{@link #getBelowLayer() <em>Below Layer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBelowLayer()
	 * @generated
	 * @ordered
	 */
	protected VisualGoalLayer belowLayer;

	/**
	 * The cached value of the '{@link #getAboveLayer() <em>Above Layer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAboveLayer()
	 * @generated
	 * @ordered
	 */
	protected VisualGoalLayer aboveLayer;

	/**
	 * The default value of the '{@link #getLayerKind() <em>Layer Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayerKind()
	 * @generated
	 * @ordered
	 */
	protected static final GoalKind LAYER_KIND_EDEFAULT = GoalKind.UNSPECIFIED;

	/**
	 * The cached value of the '{@link #getLayerKind() <em>Layer Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayerKind()
	 * @generated
	 * @ordered
	 */
	protected GoalKind layerKind = LAYER_KIND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualGoalLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GoalPackage.Literals.VISUAL_GOAL_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualGoalLayer getBelowLayer() {
		if (belowLayer != null && belowLayer.eIsProxy()) {
			InternalEObject oldBelowLayer = (InternalEObject)belowLayer;
			belowLayer = (VisualGoalLayer)eResolveProxy(oldBelowLayer);
			if (belowLayer != oldBelowLayer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GoalPackage.VISUAL_GOAL_LAYER__BELOW_LAYER, oldBelowLayer, belowLayer));
			}
		}
		return belowLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualGoalLayer basicGetBelowLayer() {
		return belowLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBelowLayer(VisualGoalLayer newBelowLayer) {
		VisualGoalLayer oldBelowLayer = belowLayer;
		belowLayer = newBelowLayer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GoalPackage.VISUAL_GOAL_LAYER__BELOW_LAYER, oldBelowLayer, belowLayer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualGoalLayer getAboveLayer() {
		if (aboveLayer != null && aboveLayer.eIsProxy()) {
			InternalEObject oldAboveLayer = (InternalEObject)aboveLayer;
			aboveLayer = (VisualGoalLayer)eResolveProxy(oldAboveLayer);
			if (aboveLayer != oldAboveLayer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GoalPackage.VISUAL_GOAL_LAYER__ABOVE_LAYER, oldAboveLayer, aboveLayer));
			}
		}
		return aboveLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualGoalLayer basicGetAboveLayer() {
		return aboveLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAboveLayer(VisualGoalLayer newAboveLayer) {
		VisualGoalLayer oldAboveLayer = aboveLayer;
		aboveLayer = newAboveLayer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GoalPackage.VISUAL_GOAL_LAYER__ABOVE_LAYER, oldAboveLayer, aboveLayer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalKind getLayerKind() {
		return layerKind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLayerKind(GoalKind newLayerKind) {
		GoalKind oldLayerKind = layerKind;
		layerKind = newLayerKind == null ? LAYER_KIND_EDEFAULT : newLayerKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GoalPackage.VISUAL_GOAL_LAYER__LAYER_KIND, oldLayerKind, layerKind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GoalPackage.VISUAL_GOAL_LAYER__BELOW_LAYER:
				if (resolve) return getBelowLayer();
				return basicGetBelowLayer();
			case GoalPackage.VISUAL_GOAL_LAYER__ABOVE_LAYER:
				if (resolve) return getAboveLayer();
				return basicGetAboveLayer();
			case GoalPackage.VISUAL_GOAL_LAYER__LAYER_KIND:
				return getLayerKind();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GoalPackage.VISUAL_GOAL_LAYER__BELOW_LAYER:
				setBelowLayer((VisualGoalLayer)newValue);
				return;
			case GoalPackage.VISUAL_GOAL_LAYER__ABOVE_LAYER:
				setAboveLayer((VisualGoalLayer)newValue);
				return;
			case GoalPackage.VISUAL_GOAL_LAYER__LAYER_KIND:
				setLayerKind((GoalKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GoalPackage.VISUAL_GOAL_LAYER__BELOW_LAYER:
				setBelowLayer((VisualGoalLayer)null);
				return;
			case GoalPackage.VISUAL_GOAL_LAYER__ABOVE_LAYER:
				setAboveLayer((VisualGoalLayer)null);
				return;
			case GoalPackage.VISUAL_GOAL_LAYER__LAYER_KIND:
				setLayerKind(LAYER_KIND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GoalPackage.VISUAL_GOAL_LAYER__BELOW_LAYER:
				return belowLayer != null;
			case GoalPackage.VISUAL_GOAL_LAYER__ABOVE_LAYER:
				return aboveLayer != null;
			case GoalPackage.VISUAL_GOAL_LAYER__LAYER_KIND:
				return layerKind != LAYER_KIND_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (LayerKind: ");
		result.append(layerKind);
		result.append(')');
		return result.toString();
	}

} //VisualGoalLayerImpl
