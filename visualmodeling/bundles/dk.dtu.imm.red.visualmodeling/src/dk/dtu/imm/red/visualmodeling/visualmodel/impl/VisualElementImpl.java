/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.impl;

import java.util.Collection;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getBounds <em>Bounds</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getDiagram <em>Diagram</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getConnections <em>Connections</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#isIsSketchy <em>Is Sketchy</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getValidationMessage <em>Validation Message</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getVisualID <em>Visual ID</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getStitchFrom <em>Stitch From</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualElementImpl#getStitchTo <em>Stitch To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VisualElementImpl extends EObjectImpl implements VisualElement {
	/**
	 * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected static final Point LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected Point location = LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getBounds() <em>Bounds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBounds()
	 * @generated
	 * @ordered
	 */
	protected static final Dimension BOUNDS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBounds() <em>Bounds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBounds()
	 * @generated
	 * @ordered
	 */
	protected Dimension bounds = BOUNDS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected IVisualElement parent;

	/**
	 * The cached value of the '{@link #getDiagram() <em>Diagram</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagram()
	 * @generated
	 * @ordered
	 */
	protected VisualDiagram diagram;

	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<IVisualElement> elements;

	/**
	 * The cached value of the '{@link #getConnections() <em>Connections</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnections()
	 * @generated
	 * @ordered
	 */
	protected EList<IVisualConnection> connections;

	/**
	 * The default value of the '{@link #isIsSketchy() <em>Is Sketchy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsSketchy()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_SKETCHY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsSketchy() <em>Is Sketchy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsSketchy()
	 * @generated
	 * @ordered
	 */
	protected boolean isSketchy = IS_SKETCHY_EDEFAULT;

	/**
	 * The default value of the '{@link #getValidationMessage() <em>Validation Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidationMessage()
	 * @generated
	 * @ordered
	 */
	protected static final String VALIDATION_MESSAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValidationMessage() <em>Validation Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidationMessage()
	 * @generated
	 * @ordered
	 */
	protected String validationMessage = VALIDATION_MESSAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVisualID() <em>Visual ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisualID()
	 * @generated
	 * @ordered
	 */
	protected static final String VISUAL_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVisualID() <em>Visual ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisualID()
	 * @generated
	 * @ordered
	 */
	protected String visualID = VISUAL_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStitchFrom() <em>Stitch From</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStitchFrom()
	 * @generated
	 * @ordered
	 */
	protected EList<StitchElementRelationship> stitchFrom;

	/**
	 * The cached value of the '{@link #getStitchTo() <em>Stitch To</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStitchTo()
	 * @generated
	 * @ordered
	 */
	protected EList<StitchElementRelationship> stitchTo;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected VisualElementImpl() {
		super();
		// Default starting size of all elements
		this.setBounds(new Dimension(150, 50));
		this.setVisualID(EcoreUtil.generateUUID());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return visualmodelPackage.Literals.VISUAL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Point getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(Point newLocation) {
		Point oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_ELEMENT__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dimension getBounds() {
		return bounds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBounds(Dimension newBounds) {
		Dimension oldBounds = bounds;
		bounds = newBounds;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_ELEMENT__BOUNDS, oldBounds, bounds));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = (IVisualElement)eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, visualmodelPackage.VISUAL_ELEMENT__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(IVisualElement newParent) {
		IVisualElement oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_ELEMENT__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualDiagram getDiagram() {
		if (diagram != null && diagram.eIsProxy()) {
			InternalEObject oldDiagram = (InternalEObject)diagram;
			diagram = (VisualDiagram)eResolveProxy(oldDiagram);
			if (diagram != oldDiagram) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, visualmodelPackage.VISUAL_ELEMENT__DIAGRAM, oldDiagram, diagram));
			}
		}
		return diagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualDiagram basicGetDiagram() {
		return diagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiagram(VisualDiagram newDiagram) {
		VisualDiagram oldDiagram = diagram;
		diagram = newDiagram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_ELEMENT__DIAGRAM, oldDiagram, diagram));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IVisualElement> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList.Resolving<IVisualElement>(IVisualElement.class, this, visualmodelPackage.VISUAL_ELEMENT__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IVisualConnection> getConnections() {
		if (connections == null) {
			connections = new EObjectResolvingEList<IVisualConnection>(IVisualConnection.class, this, visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS);
		}
		return connections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsSketchy() {
		return isSketchy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsSketchy(boolean newIsSketchy) {
		boolean oldIsSketchy = isSketchy;
		isSketchy = newIsSketchy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY, oldIsSketchy, isSketchy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValidationMessage() {
		return validationMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValidationMessage(String newValidationMessage) {
		String oldValidationMessage = validationMessage;
		validationMessage = newValidationMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE, oldValidationMessage, validationMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_ELEMENT__NAME, oldName, name));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVisualID() {
		return visualID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisualID(String newVisualID) {
		String oldVisualID = visualID;
		visualID = newVisualID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID, oldVisualID, visualID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StitchElementRelationship> getStitchFrom() {
		if (stitchFrom == null) {
			stitchFrom = new EObjectContainmentWithInverseEList.Resolving<StitchElementRelationship>(StitchElementRelationship.class, this, visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM, visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT);
		}
		return stitchFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StitchElementRelationship> getStitchTo() {
		if (stitchTo == null) {
			stitchTo = new EObjectWithInverseResolvingEList<StitchElementRelationship>(StitchElementRelationship.class, this, visualmodelPackage.VISUAL_ELEMENT__STITCH_TO, visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN);
		}
		return stitchTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStitchFrom()).basicAdd(otherEnd, msgs);
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_TO:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStitchTo()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_ELEMENT__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM:
				return ((InternalEList<?>)getStitchFrom()).basicRemove(otherEnd, msgs);
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_TO:
				return ((InternalEList<?>)getStitchTo()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_ELEMENT__LOCATION:
				return getLocation();
			case visualmodelPackage.VISUAL_ELEMENT__BOUNDS:
				return getBounds();
			case visualmodelPackage.VISUAL_ELEMENT__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case visualmodelPackage.VISUAL_ELEMENT__DIAGRAM:
				if (resolve) return getDiagram();
				return basicGetDiagram();
			case visualmodelPackage.VISUAL_ELEMENT__ELEMENTS:
				return getElements();
			case visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS:
				return getConnections();
			case visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY:
				return isIsSketchy();
			case visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE:
				return getValidationMessage();
			case visualmodelPackage.VISUAL_ELEMENT__NAME:
				return getName();
			case visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID:
				return getVisualID();
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM:
				return getStitchFrom();
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_TO:
				return getStitchTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_ELEMENT__LOCATION:
				setLocation((Point)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__BOUNDS:
				setBounds((Dimension)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__PARENT:
				setParent((IVisualElement)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__DIAGRAM:
				setDiagram((VisualDiagram)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends IVisualElement>)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS:
				getConnections().clear();
				getConnections().addAll((Collection<? extends IVisualConnection>)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY:
				setIsSketchy((Boolean)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE:
				setValidationMessage((String)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__NAME:
				setName((String)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID:
				setVisualID((String)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM:
				getStitchFrom().clear();
				getStitchFrom().addAll((Collection<? extends StitchElementRelationship>)newValue);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_TO:
				getStitchTo().clear();
				getStitchTo().addAll((Collection<? extends StitchElementRelationship>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_ELEMENT__LOCATION:
				setLocation(LOCATION_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__BOUNDS:
				setBounds(BOUNDS_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__PARENT:
				setParent((IVisualElement)null);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__DIAGRAM:
				setDiagram((VisualDiagram)null);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__ELEMENTS:
				getElements().clear();
				return;
			case visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS:
				getConnections().clear();
				return;
			case visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY:
				setIsSketchy(IS_SKETCHY_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE:
				setValidationMessage(VALIDATION_MESSAGE_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID:
				setVisualID(VISUAL_ID_EDEFAULT);
				return;
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM:
				getStitchFrom().clear();
				return;
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_TO:
				getStitchTo().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_ELEMENT__LOCATION:
				return LOCATION_EDEFAULT == null ? location != null : !LOCATION_EDEFAULT.equals(location);
			case visualmodelPackage.VISUAL_ELEMENT__BOUNDS:
				return BOUNDS_EDEFAULT == null ? bounds != null : !BOUNDS_EDEFAULT.equals(bounds);
			case visualmodelPackage.VISUAL_ELEMENT__PARENT:
				return parent != null;
			case visualmodelPackage.VISUAL_ELEMENT__DIAGRAM:
				return diagram != null;
			case visualmodelPackage.VISUAL_ELEMENT__ELEMENTS:
				return elements != null && !elements.isEmpty();
			case visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS:
				return connections != null && !connections.isEmpty();
			case visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY:
				return isSketchy != IS_SKETCHY_EDEFAULT;
			case visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE:
				return VALIDATION_MESSAGE_EDEFAULT == null ? validationMessage != null : !VALIDATION_MESSAGE_EDEFAULT.equals(validationMessage);
			case visualmodelPackage.VISUAL_ELEMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID:
				return VISUAL_ID_EDEFAULT == null ? visualID != null : !VISUAL_ID_EDEFAULT.equals(visualID);
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM:
				return stitchFrom != null && !stitchFrom.isEmpty();
			case visualmodelPackage.VISUAL_ELEMENT__STITCH_TO:
				return stitchTo != null && !stitchTo.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Location: ");
		result.append(location);
		result.append(", Bounds: ");
		result.append(bounds);
		result.append(", IsSketchy: ");
		result.append(isSketchy);
		result.append(", ValidationMessage: ");
		result.append(validationMessage);
		result.append(", Name: ");
		result.append(name);
		result.append(", visualID: ");
		result.append(visualID);
		result.append(')');
		return result.toString();
	}

} //VisualElementImpl
