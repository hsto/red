/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory
 * @model kind="package"
 * @generated
 */
public interface StatemachinePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "statemachine";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.visualmodeling.visualmodel.statemachine";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "statemachine";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StatemachinePackage eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualStateImpl <em>Visual State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualStateImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualState()
	 * @generated
	 */
	int VISUAL_STATE = 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__ENTRY = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Exit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__EXIT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Do</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE__DO = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Visual State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_STATE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualInitialStateImpl <em>Visual Initial State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualInitialStateImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualInitialState()
	 * @generated
	 */
	int VISUAL_INITIAL_STATE = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Initial State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_STATE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualFinalStateImpl <em>Visual Final State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualFinalStateImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualFinalState()
	 * @generated
	 */
	int VISUAL_FINAL_STATE = 2;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Final State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FINAL_STATE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualDecisionMergeStateImpl <em>Visual Decision Merge State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualDecisionMergeStateImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualDecisionMergeState()
	 * @generated
	 */
	int VISUAL_DECISION_MERGE_STATE = 3;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Decision Merge State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_STATE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualForkJoinStateImpl <em>Visual Fork Join State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualForkJoinStateImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualForkJoinState()
	 * @generated
	 */
	int VISUAL_FORK_JOIN_STATE = 4;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Fork Join State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_STATE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualSendStateImpl <em>Visual Send State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualSendStateImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualSendState()
	 * @generated
	 */
	int VISUAL_SEND_STATE = 5;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Send State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_STATE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualReceiveStateImpl <em>Visual Receive State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualReceiveStateImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualReceiveState()
	 * @generated
	 */
	int VISUAL_RECEIVE_STATE = 6;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Receive State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_STATE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualShallowHistoryStateImpl <em>Visual Shallow History State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualShallowHistoryStateImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualShallowHistoryState()
	 * @generated
	 */
	int VISUAL_SHALLOW_HISTORY_STATE = 7;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Shallow History State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SHALLOW_HISTORY_STATE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualDeepHistoryStateImpl <em>Visual Deep History State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualDeepHistoryStateImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualDeepHistoryState()
	 * @generated
	 */
	int VISUAL_DEEP_HISTORY_STATE = 8;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Deep History State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DEEP_HISTORY_STATE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState <em>Visual State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState
	 * @generated
	 */
	EClass getVisualState();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Entry</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState#getEntry()
	 * @see #getVisualState()
	 * @generated
	 */
	EAttribute getVisualState_Entry();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState#getExit <em>Exit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exit</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState#getExit()
	 * @see #getVisualState()
	 * @generated
	 */
	EAttribute getVisualState_Exit();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState#getDo <em>Do</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState#getDo()
	 * @see #getVisualState()
	 * @generated
	 */
	EAttribute getVisualState_Do();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualInitialState <em>Visual Initial State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Initial State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualInitialState
	 * @generated
	 */
	EClass getVisualInitialState();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualFinalState <em>Visual Final State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Final State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualFinalState
	 * @generated
	 */
	EClass getVisualFinalState();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDecisionMergeState <em>Visual Decision Merge State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Decision Merge State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDecisionMergeState
	 * @generated
	 */
	EClass getVisualDecisionMergeState();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualForkJoinState <em>Visual Fork Join State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Fork Join State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualForkJoinState
	 * @generated
	 */
	EClass getVisualForkJoinState();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualSendState <em>Visual Send State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Send State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualSendState
	 * @generated
	 */
	EClass getVisualSendState();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualReceiveState <em>Visual Receive State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Receive State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualReceiveState
	 * @generated
	 */
	EClass getVisualReceiveState();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualShallowHistoryState <em>Visual Shallow History State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Shallow History State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualShallowHistoryState
	 * @generated
	 */
	EClass getVisualShallowHistoryState();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDeepHistoryState <em>Visual Deep History State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Deep History State</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDeepHistoryState
	 * @generated
	 */
	EClass getVisualDeepHistoryState();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StatemachineFactory getStatemachineFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualStateImpl <em>Visual State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualStateImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualState()
		 * @generated
		 */
		EClass VISUAL_STATE = eINSTANCE.getVisualState();

		/**
		 * The meta object literal for the '<em><b>Entry</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_STATE__ENTRY = eINSTANCE.getVisualState_Entry();

		/**
		 * The meta object literal for the '<em><b>Exit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_STATE__EXIT = eINSTANCE.getVisualState_Exit();

		/**
		 * The meta object literal for the '<em><b>Do</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISUAL_STATE__DO = eINSTANCE.getVisualState_Do();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualInitialStateImpl <em>Visual Initial State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualInitialStateImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualInitialState()
		 * @generated
		 */
		EClass VISUAL_INITIAL_STATE = eINSTANCE.getVisualInitialState();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualFinalStateImpl <em>Visual Final State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualFinalStateImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualFinalState()
		 * @generated
		 */
		EClass VISUAL_FINAL_STATE = eINSTANCE.getVisualFinalState();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualDecisionMergeStateImpl <em>Visual Decision Merge State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualDecisionMergeStateImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualDecisionMergeState()
		 * @generated
		 */
		EClass VISUAL_DECISION_MERGE_STATE = eINSTANCE.getVisualDecisionMergeState();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualForkJoinStateImpl <em>Visual Fork Join State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualForkJoinStateImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualForkJoinState()
		 * @generated
		 */
		EClass VISUAL_FORK_JOIN_STATE = eINSTANCE.getVisualForkJoinState();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualSendStateImpl <em>Visual Send State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualSendStateImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualSendState()
		 * @generated
		 */
		EClass VISUAL_SEND_STATE = eINSTANCE.getVisualSendState();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualReceiveStateImpl <em>Visual Receive State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualReceiveStateImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualReceiveState()
		 * @generated
		 */
		EClass VISUAL_RECEIVE_STATE = eINSTANCE.getVisualReceiveState();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualShallowHistoryStateImpl <em>Visual Shallow History State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualShallowHistoryStateImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualShallowHistoryState()
		 * @generated
		 */
		EClass VISUAL_SHALLOW_HISTORY_STATE = eINSTANCE.getVisualShallowHistoryState();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualDeepHistoryStateImpl <em>Visual Deep History State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.VisualDeepHistoryStateImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachinePackageImpl#getVisualDeepHistoryState()
		 * @generated
		 */
		EClass VISUAL_DEEP_HISTORY_STATE = eINSTANCE.getVisualDeepHistoryState();

	}

} //StatemachinePackage
