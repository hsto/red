/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class visualmodelFactoryImpl extends EFactoryImpl implements visualmodelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static visualmodelFactory init() {
		try {
			visualmodelFactory thevisualmodelFactory = (visualmodelFactory)EPackage.Registry.INSTANCE.getEFactory(visualmodelPackage.eNS_URI);
			if (thevisualmodelFactory != null) {
				return thevisualmodelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new visualmodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public visualmodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case visualmodelPackage.DIAGRAM: return createDiagram();
			case visualmodelPackage.VISUAL_DIAGRAM: return createVisualDiagram();
			case visualmodelPackage.VISUAL_ELEMENT: return createVisualElement();
			case visualmodelPackage.VISUAL_CONNECTION: return createVisualConnection();
			case visualmodelPackage.NOTE: return createNote();
			case visualmodelPackage.VISUAL_GENERIC_ELEMENT: return createVisualGenericElement();
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP: return createStitchElementRelationship();
			case visualmodelPackage.STITCH_CONNECTION_RELATIONSHIP: return createStitchConnectionRelationship();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case visualmodelPackage.CONNECTION_DIRECTION:
				return createConnectionDirectionFromString(eDataType, initialValue);
			case visualmodelPackage.DIAGRAM_TYPE:
				return createDiagramTypeFromString(eDataType, initialValue);
			case visualmodelPackage.CONNECTION_DECORATION:
				return createConnectionDecorationFromString(eDataType, initialValue);
			case visualmodelPackage.CONNECTION_LINE_STYLE:
				return createConnectionLineStyleFromString(eDataType, initialValue);
			case visualmodelPackage.GENERIC_TYPE:
				return createGenericTypeFromString(eDataType, initialValue);
			case visualmodelPackage.CONNECTION_TYPE:
				return createConnectionTypeFromString(eDataType, initialValue);
			case visualmodelPackage.CONNECTION_ROUTING:
				return createConnectionRoutingFromString(eDataType, initialValue);
			case visualmodelPackage.STITCH_STATE:
				return createStitchStateFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case visualmodelPackage.CONNECTION_DIRECTION:
				return convertConnectionDirectionToString(eDataType, instanceValue);
			case visualmodelPackage.DIAGRAM_TYPE:
				return convertDiagramTypeToString(eDataType, instanceValue);
			case visualmodelPackage.CONNECTION_DECORATION:
				return convertConnectionDecorationToString(eDataType, instanceValue);
			case visualmodelPackage.CONNECTION_LINE_STYLE:
				return convertConnectionLineStyleToString(eDataType, instanceValue);
			case visualmodelPackage.GENERIC_TYPE:
				return convertGenericTypeToString(eDataType, instanceValue);
			case visualmodelPackage.CONNECTION_TYPE:
				return convertConnectionTypeToString(eDataType, instanceValue);
			case visualmodelPackage.CONNECTION_ROUTING:
				return convertConnectionRoutingToString(eDataType, instanceValue);
			case visualmodelPackage.STITCH_STATE:
				return convertStitchStateToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Diagram createDiagram() {
		DiagramImpl diagram = new DiagramImpl();
		return diagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualDiagram createVisualDiagram() {
		VisualDiagramImpl visualDiagram = new VisualDiagramImpl();
		return visualDiagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualElement createVisualElement() {
		VisualElementImpl visualElement = new VisualElementImpl();
		return visualElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualConnection createVisualConnection() {
		VisualConnectionImpl visualConnection = new VisualConnectionImpl();
		return visualConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Note createNote() {
		NoteImpl note = new NoteImpl();
		return note;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualGenericElement createVisualGenericElement() {
		VisualGenericElementImpl visualGenericElement = new VisualGenericElementImpl();
		return visualGenericElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StitchElementRelationship createStitchElementRelationship() {
		StitchElementRelationshipImpl stitchElementRelationship = new StitchElementRelationshipImpl();
		return stitchElementRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StitchConnectionRelationship createStitchConnectionRelationship() {
		StitchConnectionRelationshipImpl stitchConnectionRelationship = new StitchConnectionRelationshipImpl();
		return stitchConnectionRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionDirection createConnectionDirectionFromString(EDataType eDataType, String initialValue) {
		ConnectionDirection result = ConnectionDirection.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConnectionDirectionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiagramType createDiagramTypeFromString(EDataType eDataType, String initialValue) {
		DiagramType result = DiagramType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDiagramTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionDecoration createConnectionDecorationFromString(EDataType eDataType, String initialValue) {
		ConnectionDecoration result = ConnectionDecoration.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConnectionDecorationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionLineStyle createConnectionLineStyleFromString(EDataType eDataType, String initialValue) {
		ConnectionLineStyle result = ConnectionLineStyle.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConnectionLineStyleToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericType createGenericTypeFromString(EDataType eDataType, String initialValue) {
		GenericType result = GenericType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGenericTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionType createConnectionTypeFromString(EDataType eDataType, String initialValue) {
		ConnectionType result = ConnectionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConnectionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionRouting createConnectionRoutingFromString(EDataType eDataType, String initialValue) {
		ConnectionRouting result = ConnectionRouting.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConnectionRoutingToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StitchState createStitchStateFromString(EDataType eDataType, String initialValue) {
		StitchState result = StitchState.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStitchStateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public visualmodelPackage getvisualmodelPackage() {
		return (visualmodelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static visualmodelPackage getPackage() {
		return visualmodelPackage.eINSTANCE;
	}

} //visualmodelFactoryImpl
