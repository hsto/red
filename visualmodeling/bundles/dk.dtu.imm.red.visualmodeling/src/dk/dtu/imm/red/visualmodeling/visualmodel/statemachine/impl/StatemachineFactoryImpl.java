/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StatemachineFactoryImpl extends EFactoryImpl implements StatemachineFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StatemachineFactory init() {
		try {
			StatemachineFactory theStatemachineFactory = (StatemachineFactory)EPackage.Registry.INSTANCE.getEFactory(StatemachinePackage.eNS_URI);
			if (theStatemachineFactory != null) {
				return theStatemachineFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StatemachineFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StatemachinePackage.VISUAL_STATE: return createVisualState();
			case StatemachinePackage.VISUAL_INITIAL_STATE: return createVisualInitialState();
			case StatemachinePackage.VISUAL_FINAL_STATE: return createVisualFinalState();
			case StatemachinePackage.VISUAL_DECISION_MERGE_STATE: return createVisualDecisionMergeState();
			case StatemachinePackage.VISUAL_FORK_JOIN_STATE: return createVisualForkJoinState();
			case StatemachinePackage.VISUAL_SEND_STATE: return createVisualSendState();
			case StatemachinePackage.VISUAL_RECEIVE_STATE: return createVisualReceiveState();
			case StatemachinePackage.VISUAL_SHALLOW_HISTORY_STATE: return createVisualShallowHistoryState();
			case StatemachinePackage.VISUAL_DEEP_HISTORY_STATE: return createVisualDeepHistoryState();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualState createVisualState() {
		VisualStateImpl visualState = new VisualStateImpl();
		return visualState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualInitialState createVisualInitialState() {
		VisualInitialStateImpl visualInitialState = new VisualInitialStateImpl();
		return visualInitialState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualFinalState createVisualFinalState() {
		VisualFinalStateImpl visualFinalState = new VisualFinalStateImpl();
		return visualFinalState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualDecisionMergeState createVisualDecisionMergeState() {
		VisualDecisionMergeStateImpl visualDecisionMergeState = new VisualDecisionMergeStateImpl();
		return visualDecisionMergeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualForkJoinState createVisualForkJoinState() {
		VisualForkJoinStateImpl visualForkJoinState = new VisualForkJoinStateImpl();
		return visualForkJoinState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualSendState createVisualSendState() {
		VisualSendStateImpl visualSendState = new VisualSendStateImpl();
		return visualSendState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualReceiveState createVisualReceiveState() {
		VisualReceiveStateImpl visualReceiveState = new VisualReceiveStateImpl();
		return visualReceiveState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualShallowHistoryState createVisualShallowHistoryState() {
		VisualShallowHistoryStateImpl visualShallowHistoryState = new VisualShallowHistoryStateImpl();
		return visualShallowHistoryState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualDeepHistoryState createVisualDeepHistoryState() {
		VisualDeepHistoryStateImpl visualDeepHistoryState = new VisualDeepHistoryStateImpl();
		return visualDeepHistoryState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachinePackage getStatemachinePackage() {
		return (StatemachinePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StatemachinePackage getPackage() {
		return StatemachinePackage.eINSTANCE;
	}

} //StatemachineFactoryImpl
