/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stitch Element Relationship</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchElementRelationshipImpl#getStitchOrigin <em>Stitch Origin</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.StitchElementRelationshipImpl#getStitchOutput <em>Stitch Output</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StitchElementRelationshipImpl extends EObjectImpl implements StitchElementRelationship {
	/**
	 * The cached value of the '{@link #getStitchOrigin() <em>Stitch Origin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStitchOrigin()
	 * @generated
	 * @ordered
	 */
	protected IVisualElement stitchOrigin;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StitchElementRelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return visualmodelPackage.Literals.STITCH_ELEMENT_RELATIONSHIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement getStitchOrigin() {
		if (stitchOrigin != null && stitchOrigin.eIsProxy()) {
			InternalEObject oldStitchOrigin = (InternalEObject)stitchOrigin;
			stitchOrigin = (IVisualElement)eResolveProxy(oldStitchOrigin);
			if (stitchOrigin != oldStitchOrigin) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN, oldStitchOrigin, stitchOrigin));
			}
		}
		return stitchOrigin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement basicGetStitchOrigin() {
		return stitchOrigin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStitchOrigin(IVisualElement newStitchOrigin, NotificationChain msgs) {
		IVisualElement oldStitchOrigin = stitchOrigin;
		stitchOrigin = newStitchOrigin;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN, oldStitchOrigin, newStitchOrigin);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStitchOrigin(IVisualElement newStitchOrigin) {
		if (newStitchOrigin != stitchOrigin) {
			NotificationChain msgs = null;
			if (stitchOrigin != null)
				msgs = ((InternalEObject)stitchOrigin).eInverseRemove(this, visualmodelPackage.IVISUAL_ELEMENT__STITCH_TO, IVisualElement.class, msgs);
			if (newStitchOrigin != null)
				msgs = ((InternalEObject)newStitchOrigin).eInverseAdd(this, visualmodelPackage.IVISUAL_ELEMENT__STITCH_TO, IVisualElement.class, msgs);
			msgs = basicSetStitchOrigin(newStitchOrigin, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN, newStitchOrigin, newStitchOrigin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement getStitchOutput() {
		if (eContainerFeatureID() != visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT) return null;
		return (IVisualElement)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVisualElement basicGetStitchOutput() {
		if (eContainerFeatureID() != visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT) return null;
		return (IVisualElement)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStitchOutput(IVisualElement newStitchOutput, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newStitchOutput, visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStitchOutput(IVisualElement newStitchOutput) {
		if (newStitchOutput != eInternalContainer() || (eContainerFeatureID() != visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT && newStitchOutput != null)) {
			if (EcoreUtil.isAncestor(this, newStitchOutput))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newStitchOutput != null)
				msgs = ((InternalEObject)newStitchOutput).eInverseAdd(this, visualmodelPackage.IVISUAL_ELEMENT__STITCH_FROM, IVisualElement.class, msgs);
			msgs = basicSetStitchOutput(newStitchOutput, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT, newStitchOutput, newStitchOutput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN:
				if (stitchOrigin != null)
					msgs = ((InternalEObject)stitchOrigin).eInverseRemove(this, visualmodelPackage.IVISUAL_ELEMENT__STITCH_TO, IVisualElement.class, msgs);
				return basicSetStitchOrigin((IVisualElement)otherEnd, msgs);
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetStitchOutput((IVisualElement)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN:
				return basicSetStitchOrigin(null, msgs);
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT:
				return basicSetStitchOutput(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT:
				return eInternalContainer().eInverseRemove(this, visualmodelPackage.IVISUAL_ELEMENT__STITCH_FROM, IVisualElement.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN:
				if (resolve) return getStitchOrigin();
				return basicGetStitchOrigin();
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT:
				if (resolve) return getStitchOutput();
				return basicGetStitchOutput();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN:
				setStitchOrigin((IVisualElement)newValue);
				return;
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT:
				setStitchOutput((IVisualElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN:
				setStitchOrigin((IVisualElement)null);
				return;
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT:
				setStitchOutput((IVisualElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_ORIGIN:
				return stitchOrigin != null;
			case visualmodelPackage.STITCH_ELEMENT_RELATIONSHIP__STITCH_OUTPUT:
				return basicGetStitchOutput() != null;
		}
		return super.eIsSet(featureID);
	}

} //StitchElementRelationshipImpl
