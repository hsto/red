/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory
 * @model kind="package"
 * @generated
 */
public interface ActivityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "activity";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.visualmodeling.visualmodel.activity";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "activity";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ActivityPackage eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualActionNodeImpl <em>Visual Action Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualActionNodeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualActionNode()
	 * @generated
	 */
	int VISUAL_ACTION_NODE = 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Action Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTION_NODE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualInitialNodeImpl <em>Visual Initial Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualInitialNodeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualInitialNode()
	 * @generated
	 */
	int VISUAL_INITIAL_NODE = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Initial Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_INITIAL_NODE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualActivityFinalNodeImpl <em>Visual Activity Final Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualActivityFinalNodeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualActivityFinalNode()
	 * @generated
	 */
	int VISUAL_ACTIVITY_FINAL_NODE = 2;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Activity Final Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_ACTIVITY_FINAL_NODE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualFlowFinalNodeImpl <em>Visual Flow Final Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualFlowFinalNodeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualFlowFinalNode()
	 * @generated
	 */
	int VISUAL_FLOW_FINAL_NODE = 3;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Flow Final Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FLOW_FINAL_NODE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualDecisionMergeNodeImpl <em>Visual Decision Merge Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualDecisionMergeNodeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualDecisionMergeNode()
	 * @generated
	 */
	int VISUAL_DECISION_MERGE_NODE = 4;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Decision Merge Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_DECISION_MERGE_NODE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualForkJoinNodeImpl <em>Visual Fork Join Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualForkJoinNodeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualForkJoinNode()
	 * @generated
	 */
	int VISUAL_FORK_JOIN_NODE = 5;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Fork Join Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_FORK_JOIN_NODE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualSendSignalNodeImpl <em>Visual Send Signal Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualSendSignalNodeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualSendSignalNode()
	 * @generated
	 */
	int VISUAL_SEND_SIGNAL_NODE = 6;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Send Signal Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_SEND_SIGNAL_NODE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualReceiveSignalNodeImpl <em>Visual Receive Signal Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualReceiveSignalNodeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualReceiveSignalNode()
	 * @generated
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE = 7;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Receive Signal Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_RECEIVE_SIGNAL_NODE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualObjectNodeImpl <em>Visual Object Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualObjectNodeImpl
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualObjectNode()
	 * @generated
	 */
	int VISUAL_OBJECT_NODE = 8;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__LOCATION = visualmodelPackage.VISUAL_ELEMENT__LOCATION;

	/**
	 * The feature id for the '<em><b>Bounds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__BOUNDS = visualmodelPackage.VISUAL_ELEMENT__BOUNDS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__PARENT = visualmodelPackage.VISUAL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__DIAGRAM = visualmodelPackage.VISUAL_ELEMENT__DIAGRAM;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__ELEMENTS = visualmodelPackage.VISUAL_ELEMENT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__CONNECTIONS = visualmodelPackage.VISUAL_ELEMENT__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Is Sketchy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__IS_SKETCHY = visualmodelPackage.VISUAL_ELEMENT__IS_SKETCHY;

	/**
	 * The feature id for the '<em><b>Validation Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__VALIDATION_MESSAGE = visualmodelPackage.VISUAL_ELEMENT__VALIDATION_MESSAGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__NAME = visualmodelPackage.VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visual ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__VISUAL_ID = visualmodelPackage.VISUAL_ELEMENT__VISUAL_ID;

	/**
	 * The feature id for the '<em><b>Stitch From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__STITCH_FROM = visualmodelPackage.VISUAL_ELEMENT__STITCH_FROM;

	/**
	 * The feature id for the '<em><b>Stitch To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE__STITCH_TO = visualmodelPackage.VISUAL_ELEMENT__STITCH_TO;

	/**
	 * The number of structural features of the '<em>Visual Object Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISUAL_OBJECT_NODE_FEATURE_COUNT = visualmodelPackage.VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActionNode <em>Visual Action Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Action Node</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActionNode
	 * @generated
	 */
	EClass getVisualActionNode();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualInitialNode <em>Visual Initial Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Initial Node</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualInitialNode
	 * @generated
	 */
	EClass getVisualInitialNode();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActivityFinalNode <em>Visual Activity Final Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Activity Final Node</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActivityFinalNode
	 * @generated
	 */
	EClass getVisualActivityFinalNode();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualFlowFinalNode <em>Visual Flow Final Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Flow Final Node</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualFlowFinalNode
	 * @generated
	 */
	EClass getVisualFlowFinalNode();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualDecisionMergeNode <em>Visual Decision Merge Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Decision Merge Node</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualDecisionMergeNode
	 * @generated
	 */
	EClass getVisualDecisionMergeNode();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualForkJoinNode <em>Visual Fork Join Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Fork Join Node</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualForkJoinNode
	 * @generated
	 */
	EClass getVisualForkJoinNode();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualSendSignalNode <em>Visual Send Signal Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Send Signal Node</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualSendSignalNode
	 * @generated
	 */
	EClass getVisualSendSignalNode();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualReceiveSignalNode <em>Visual Receive Signal Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Receive Signal Node</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualReceiveSignalNode
	 * @generated
	 */
	EClass getVisualReceiveSignalNode();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode <em>Visual Object Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visual Object Node</em>'.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode
	 * @generated
	 */
	EClass getVisualObjectNode();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ActivityFactory getActivityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualActionNodeImpl <em>Visual Action Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualActionNodeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualActionNode()
		 * @generated
		 */
		EClass VISUAL_ACTION_NODE = eINSTANCE.getVisualActionNode();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualInitialNodeImpl <em>Visual Initial Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualInitialNodeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualInitialNode()
		 * @generated
		 */
		EClass VISUAL_INITIAL_NODE = eINSTANCE.getVisualInitialNode();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualActivityFinalNodeImpl <em>Visual Activity Final Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualActivityFinalNodeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualActivityFinalNode()
		 * @generated
		 */
		EClass VISUAL_ACTIVITY_FINAL_NODE = eINSTANCE.getVisualActivityFinalNode();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualFlowFinalNodeImpl <em>Visual Flow Final Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualFlowFinalNodeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualFlowFinalNode()
		 * @generated
		 */
		EClass VISUAL_FLOW_FINAL_NODE = eINSTANCE.getVisualFlowFinalNode();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualDecisionMergeNodeImpl <em>Visual Decision Merge Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualDecisionMergeNodeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualDecisionMergeNode()
		 * @generated
		 */
		EClass VISUAL_DECISION_MERGE_NODE = eINSTANCE.getVisualDecisionMergeNode();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualForkJoinNodeImpl <em>Visual Fork Join Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualForkJoinNodeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualForkJoinNode()
		 * @generated
		 */
		EClass VISUAL_FORK_JOIN_NODE = eINSTANCE.getVisualForkJoinNode();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualSendSignalNodeImpl <em>Visual Send Signal Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualSendSignalNodeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualSendSignalNode()
		 * @generated
		 */
		EClass VISUAL_SEND_SIGNAL_NODE = eINSTANCE.getVisualSendSignalNode();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualReceiveSignalNodeImpl <em>Visual Receive Signal Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualReceiveSignalNodeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualReceiveSignalNode()
		 * @generated
		 */
		EClass VISUAL_RECEIVE_SIGNAL_NODE = eINSTANCE.getVisualReceiveSignalNode();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualObjectNodeImpl <em>Visual Object Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.VisualObjectNodeImpl
		 * @see dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl.ActivityPackageImpl#getVisualObjectNode()
		 * @generated
		 */
		EClass VISUAL_OBJECT_NODE = eINSTANCE.getVisualObjectNode();

	}

} //ActivityPackage
