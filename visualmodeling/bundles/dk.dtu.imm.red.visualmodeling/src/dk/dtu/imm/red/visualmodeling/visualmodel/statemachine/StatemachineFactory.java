/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage
 * @generated
 */
public interface StatemachineFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StatemachineFactory eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.impl.StatemachineFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Visual State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual State</em>'.
	 * @generated
	 */
	VisualState createVisualState();

	/**
	 * Returns a new object of class '<em>Visual Initial State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Initial State</em>'.
	 * @generated
	 */
	VisualInitialState createVisualInitialState();

	/**
	 * Returns a new object of class '<em>Visual Final State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Final State</em>'.
	 * @generated
	 */
	VisualFinalState createVisualFinalState();

	/**
	 * Returns a new object of class '<em>Visual Decision Merge State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Decision Merge State</em>'.
	 * @generated
	 */
	VisualDecisionMergeState createVisualDecisionMergeState();

	/**
	 * Returns a new object of class '<em>Visual Fork Join State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Fork Join State</em>'.
	 * @generated
	 */
	VisualForkJoinState createVisualForkJoinState();

	/**
	 * Returns a new object of class '<em>Visual Send State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Send State</em>'.
	 * @generated
	 */
	VisualSendState createVisualSendState();

	/**
	 * Returns a new object of class '<em>Visual Receive State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Receive State</em>'.
	 * @generated
	 */
	VisualReceiveState createVisualReceiveState();

	/**
	 * Returns a new object of class '<em>Visual Shallow History State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Shallow History State</em>'.
	 * @generated
	 */
	VisualShallowHistoryState createVisualShallowHistoryState();

	/**
	 * Returns a new object of class '<em>Visual Deep History State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Deep History State</em>'.
	 * @generated
	 */
	VisualDeepHistoryState createVisualDeepHistoryState();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StatemachinePackage getStatemachinePackage();

} //StatemachineFactory
