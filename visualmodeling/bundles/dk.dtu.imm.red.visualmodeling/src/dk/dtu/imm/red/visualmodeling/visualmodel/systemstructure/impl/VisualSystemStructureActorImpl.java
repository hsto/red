/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.ActorFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;

import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual System Structure Actor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.impl.VisualSystemStructureActorImpl#getActorKind <em>Actor Kind</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VisualSystemStructureActorImpl extends VisualSpecificationElementImpl implements VisualSystemStructureActor {
	/**
	 * The default value of the '{@link #getActorKind() <em>Actor Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActorKind()
	 * @generated
	 * @ordered
	 */
	protected static final SystemStructureActorKind ACTOR_KIND_EDEFAULT = SystemStructureActorKind.PERSON;

	/**
	 * The cached value of the '{@link #getActorKind() <em>Actor Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActorKind()
	 * @generated
	 * @ordered
	 */
	protected SystemStructureActorKind actorKind = ACTOR_KIND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualSystemStructureActorImpl() {
		super();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public Class<?> getSpecificationElementType() {
		return Actor.class;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SpecificationElement createSpecificationElement(Group parent) {
		Actor actor = ActorFactory.eINSTANCE.createActor();
		actor.setCreator(PreferenceUtil.getUserPreference_User());
		
		String str = (this.name == null) ? "" : this.name; 
		
		actor.setName(str);
		actor.setLabel(str);
		actor.setDescription("");
		
		if (parent != null) {
			actor.setParent(parent);
		}
		actor.setElementKind(this.getActorKind().getName().toLowerCase());
		
		actor.save();
		
		
		return actor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemstructurePackage.Literals.VISUAL_SYSTEM_STRUCTURE_ACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemStructureActorKind getActorKind() {
		return actorKind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActorKind(SystemStructureActorKind newActorKind) {
		SystemStructureActorKind oldActorKind = actorKind;
		actorKind = newActorKind == null ? ACTOR_KIND_EDEFAULT : newActorKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemstructurePackage.VISUAL_SYSTEM_STRUCTURE_ACTOR__ACTOR_KIND, oldActorKind, actorKind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemstructurePackage.VISUAL_SYSTEM_STRUCTURE_ACTOR__ACTOR_KIND:
				return getActorKind();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemstructurePackage.VISUAL_SYSTEM_STRUCTURE_ACTOR__ACTOR_KIND:
				setActorKind((SystemStructureActorKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemstructurePackage.VISUAL_SYSTEM_STRUCTURE_ACTOR__ACTOR_KIND:
				setActorKind(ACTOR_KIND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemstructurePackage.VISUAL_SYSTEM_STRUCTURE_ACTOR__ACTOR_KIND:
				return actorKind != ACTOR_KIND_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (actorKind: ");
		result.append(actorKind);
		result.append(')');
		return result.toString();
	}

} //VisualSystemStructureActorImpl
