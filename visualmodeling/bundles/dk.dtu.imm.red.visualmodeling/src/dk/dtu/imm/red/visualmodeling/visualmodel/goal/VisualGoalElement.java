/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.goal;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visual Goal Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage#getVisualGoalElement()
 * @model
 * @generated
 */
public interface VisualGoalElement extends VisualSpecificationElement {
} // VisualGoalElement
