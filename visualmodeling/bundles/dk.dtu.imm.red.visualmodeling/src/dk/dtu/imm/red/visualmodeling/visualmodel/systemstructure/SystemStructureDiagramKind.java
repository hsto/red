/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>System Structure Diagram Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructurePackage#getSystemStructureDiagramKind()
 * @model
 * @generated
 */
public enum SystemStructureDiagramKind implements Enumerator {
	/**
	 * The '<em><b>System Context</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYSTEM_CONTEXT_VALUE
	 * @generated
	 * @ordered
	 */
	SYSTEM_CONTEXT(0, "SystemContext", "System Context"),

	/**
	 * The '<em><b>Internal Structure</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERNAL_STRUCTURE_VALUE
	 * @generated
	 * @ordered
	 */
	INTERNAL_STRUCTURE(1, "InternalStructure", "Internal Structure"),

	/**
	 * The '<em><b>Domain Architecture</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOMAIN_ARCHITECTURE_VALUE
	 * @generated
	 * @ordered
	 */
	DOMAIN_ARCHITECTURE(2, "DomainArchitecture", "Domain Architecture");

	/**
	 * The '<em><b>System Context</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>System Context</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SYSTEM_CONTEXT
	 * @model name="SystemContext" literal="System Context"
	 * @generated
	 * @ordered
	 */
	public static final int SYSTEM_CONTEXT_VALUE = 0;

	/**
	 * The '<em><b>Internal Structure</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Internal Structure</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTERNAL_STRUCTURE
	 * @model name="InternalStructure" literal="Internal Structure"
	 * @generated
	 * @ordered
	 */
	public static final int INTERNAL_STRUCTURE_VALUE = 1;

	/**
	 * The '<em><b>Domain Architecture</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Domain Architecture</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOMAIN_ARCHITECTURE
	 * @model name="DomainArchitecture" literal="Domain Architecture"
	 * @generated
	 * @ordered
	 */
	public static final int DOMAIN_ARCHITECTURE_VALUE = 2;

	/**
	 * An array of all the '<em><b>System Structure Diagram Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SystemStructureDiagramKind[] VALUES_ARRAY =
		new SystemStructureDiagramKind[] {
			SYSTEM_CONTEXT,
			INTERNAL_STRUCTURE,
			DOMAIN_ARCHITECTURE,
		};

	/**
	 * A public read-only list of all the '<em><b>System Structure Diagram Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SystemStructureDiagramKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>System Structure Diagram Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SystemStructureDiagramKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SystemStructureDiagramKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>System Structure Diagram Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SystemStructureDiagramKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SystemStructureDiagramKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>System Structure Diagram Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SystemStructureDiagramKind get(int value) {
		switch (value) {
			case SYSTEM_CONTEXT_VALUE: return SYSTEM_CONTEXT;
			case INTERNAL_STRUCTURE_VALUE: return INTERNAL_STRUCTURE;
			case DOMAIN_ARCHITECTURE_VALUE: return DOMAIN_ARCHITECTURE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SystemStructureDiagramKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SystemStructureDiagramKind
