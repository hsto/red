/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.impl;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.specificationelements.SpecificationElement;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Specification Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl#getSpecificationElement <em>Specification Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl#isIsLinkedToElement <em>Is Linked To Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class VisualSpecificationElementImpl extends VisualElementImpl implements VisualSpecificationElement {
	/**
	 * The cached value of the '{@link #getSpecificationElement() <em>Specification Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecificationElement()
	 * @generated
	 * @ordered
	 */
	protected SpecificationElement specificationElement;

	/**
	 * The default value of the '{@link #isIsLinkedToElement() <em>Is Linked To Element</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsLinkedToElement()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_LINKED_TO_ELEMENT_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isIsLinkedToElement() <em>Is Linked To Element</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsLinkedToElement()
	 * @generated
	 * @ordered
	 */
	protected boolean isLinkedToElement = IS_LINKED_TO_ELEMENT_EDEFAULT;
	/**
	 * This is true if the Is Linked To Element attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isLinkedToElementESet;
	
	/**
	 * @generated NOT
	 */
	private DeletionListener specificationElementDeletionListener;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualSpecificationElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return visualmodelPackage.Literals.VISUAL_SPECIFICATION_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecificationElement getSpecificationElement() {
		if (specificationElement != null && specificationElement.eIsProxy()) {
			InternalEObject oldSpecificationElement = (InternalEObject)specificationElement;
			specificationElement = (SpecificationElement)eResolveProxy(oldSpecificationElement);
			if (specificationElement != oldSpecificationElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT, oldSpecificationElement, specificationElement));
			}
		}
		return specificationElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecificationElement basicGetSpecificationElement() {
		return specificationElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setSpecificationElement(SpecificationElement newSpecificationElement) {
		
		if(newSpecificationElement == null)
		{
			setIsLinkedToElement(false);
		}
		else
		{
			setName(newSpecificationElement.getName());
			setIsLinkedToElement(true);
		}

		SpecificationElement oldSpecificationElement = specificationElement;
		specificationElement = newSpecificationElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT, oldSpecificationElement, specificationElement));
		
		if (specificationElement != null) {
			final SpecificationElement listenedElement = specificationElement;
			
			specificationElementDeletionListener = ElementFactory.eINSTANCE.createDeletionListener();
			specificationElementDeletionListener.setHostingObject(this);
			specificationElementDeletionListener.setHostingFeature(visualmodelPackage.eINSTANCE.getVisualSpecificationElement_SpecificationElement());
			specificationElementDeletionListener.setHandledObject(listenedElement);
			listenedElement.getDeletionListeners().add(specificationElementDeletionListener);
			
			// Delete any old listeners to avoid unexpected behavior
			Iterator<DeletionListener> deletionListenerIterator = specificationElement.getDeletionListeners().iterator();
			while (deletionListenerIterator.hasNext()) {
				DeletionListener listener = deletionListenerIterator.next();
				if (listener.getHostingObject() == this) 
					deletionListenerIterator.remove();
			}
			
			specificationElement.getDeletionListeners().add(specificationElementDeletionListener);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsLinkedToElement() {
		return isLinkedToElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsLinkedToElement(boolean newIsLinkedToElement) {
		boolean oldIsLinkedToElement = isLinkedToElement;
		isLinkedToElement = newIsLinkedToElement;
		boolean oldIsLinkedToElementESet = isLinkedToElementESet;
		isLinkedToElementESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT, oldIsLinkedToElement, isLinkedToElement, !oldIsLinkedToElementESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsLinkedToElement() {
		boolean oldIsLinkedToElement = isLinkedToElement;
		boolean oldIsLinkedToElementESet = isLinkedToElementESet;
		isLinkedToElement = IS_LINKED_TO_ELEMENT_EDEFAULT;
		isLinkedToElementESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT, oldIsLinkedToElement, IS_LINKED_TO_ELEMENT_EDEFAULT, oldIsLinkedToElementESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsLinkedToElement() {
		return isLinkedToElementESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public abstract Class<?> getSpecificationElementType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public abstract SpecificationElement createSpecificationElement(Group parent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT:
				if (resolve) return getSpecificationElement();
				return basicGetSpecificationElement();
			case visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT:
				return isIsLinkedToElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT:
				setSpecificationElement((SpecificationElement)newValue);
				return;
			case visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT:
				setIsLinkedToElement((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT:
				setSpecificationElement((SpecificationElement)null);
				return;
			case visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT:
				unsetIsLinkedToElement();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT:
				return specificationElement != null;
			case visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__IS_LINKED_TO_ELEMENT:
				return isSetIsLinkedToElement();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (IsLinkedToElement: ");
		if (isLinkedToElementESet) result.append(isLinkedToElement); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //VisualSpecificationElementImpl
