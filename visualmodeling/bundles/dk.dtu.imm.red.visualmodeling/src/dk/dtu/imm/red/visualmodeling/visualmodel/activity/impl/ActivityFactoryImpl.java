/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.activity.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.activity.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActivityFactoryImpl extends EFactoryImpl implements ActivityFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ActivityFactory init() {
		try {
			ActivityFactory theActivityFactory = (ActivityFactory)EPackage.Registry.INSTANCE.getEFactory(ActivityPackage.eNS_URI);
			if (theActivityFactory != null) {
				return theActivityFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ActivityFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ActivityPackage.VISUAL_ACTION_NODE: return createVisualActionNode();
			case ActivityPackage.VISUAL_INITIAL_NODE: return createVisualInitialNode();
			case ActivityPackage.VISUAL_ACTIVITY_FINAL_NODE: return createVisualActivityFinalNode();
			case ActivityPackage.VISUAL_FLOW_FINAL_NODE: return createVisualFlowFinalNode();
			case ActivityPackage.VISUAL_DECISION_MERGE_NODE: return createVisualDecisionMergeNode();
			case ActivityPackage.VISUAL_FORK_JOIN_NODE: return createVisualForkJoinNode();
			case ActivityPackage.VISUAL_SEND_SIGNAL_NODE: return createVisualSendSignalNode();
			case ActivityPackage.VISUAL_RECEIVE_SIGNAL_NODE: return createVisualReceiveSignalNode();
			case ActivityPackage.VISUAL_OBJECT_NODE: return createVisualObjectNode();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualActionNode createVisualActionNode() {
		VisualActionNodeImpl visualActionNode = new VisualActionNodeImpl();
		return visualActionNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualInitialNode createVisualInitialNode() {
		VisualInitialNodeImpl visualInitialNode = new VisualInitialNodeImpl();
		return visualInitialNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualActivityFinalNode createVisualActivityFinalNode() {
		VisualActivityFinalNodeImpl visualActivityFinalNode = new VisualActivityFinalNodeImpl();
		return visualActivityFinalNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualFlowFinalNode createVisualFlowFinalNode() {
		VisualFlowFinalNodeImpl visualFlowFinalNode = new VisualFlowFinalNodeImpl();
		return visualFlowFinalNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualDecisionMergeNode createVisualDecisionMergeNode() {
		VisualDecisionMergeNodeImpl visualDecisionMergeNode = new VisualDecisionMergeNodeImpl();
		return visualDecisionMergeNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualForkJoinNode createVisualForkJoinNode() {
		VisualForkJoinNodeImpl visualForkJoinNode = new VisualForkJoinNodeImpl();
		return visualForkJoinNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualSendSignalNode createVisualSendSignalNode() {
		VisualSendSignalNodeImpl visualSendSignalNode = new VisualSendSignalNodeImpl();
		return visualSendSignalNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualReceiveSignalNode createVisualReceiveSignalNode() {
		VisualReceiveSignalNodeImpl visualReceiveSignalNode = new VisualReceiveSignalNodeImpl();
		return visualReceiveSignalNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualObjectNode createVisualObjectNode() {
		VisualObjectNodeImpl visualObjectNode = new VisualObjectNodeImpl();
		return visualObjectNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityPackage getActivityPackage() {
		return (ActivityPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ActivityPackage getPackage() {
		return ActivityPackage.eINSTANCE;
	}

} //ActivityFactoryImpl
