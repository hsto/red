/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.util;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachinePackage
 * @generated
 */
public class StatemachineAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static StatemachinePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = StatemachinePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StatemachineSwitch<Adapter> modelSwitch =
		new StatemachineSwitch<Adapter>() {
			@Override
			public Adapter caseVisualState(VisualState object) {
				return createVisualStateAdapter();
			}
			@Override
			public Adapter caseVisualInitialState(VisualInitialState object) {
				return createVisualInitialStateAdapter();
			}
			@Override
			public Adapter caseVisualFinalState(VisualFinalState object) {
				return createVisualFinalStateAdapter();
			}
			@Override
			public Adapter caseVisualDecisionMergeState(VisualDecisionMergeState object) {
				return createVisualDecisionMergeStateAdapter();
			}
			@Override
			public Adapter caseVisualForkJoinState(VisualForkJoinState object) {
				return createVisualForkJoinStateAdapter();
			}
			@Override
			public Adapter caseVisualSendState(VisualSendState object) {
				return createVisualSendStateAdapter();
			}
			@Override
			public Adapter caseVisualReceiveState(VisualReceiveState object) {
				return createVisualReceiveStateAdapter();
			}
			@Override
			public Adapter caseVisualShallowHistoryState(VisualShallowHistoryState object) {
				return createVisualShallowHistoryStateAdapter();
			}
			@Override
			public Adapter caseVisualDeepHistoryState(VisualDeepHistoryState object) {
				return createVisualDeepHistoryStateAdapter();
			}
			@Override
			public Adapter caseIVisualElement(IVisualElement object) {
				return createIVisualElementAdapter();
			}
			@Override
			public Adapter caseVisualElement(VisualElement object) {
				return createVisualElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState <em>Visual State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState
	 * @generated
	 */
	public Adapter createVisualStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualInitialState <em>Visual Initial State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualInitialState
	 * @generated
	 */
	public Adapter createVisualInitialStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualFinalState <em>Visual Final State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualFinalState
	 * @generated
	 */
	public Adapter createVisualFinalStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDecisionMergeState <em>Visual Decision Merge State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDecisionMergeState
	 * @generated
	 */
	public Adapter createVisualDecisionMergeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualForkJoinState <em>Visual Fork Join State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualForkJoinState
	 * @generated
	 */
	public Adapter createVisualForkJoinStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualSendState <em>Visual Send State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualSendState
	 * @generated
	 */
	public Adapter createVisualSendStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualReceiveState <em>Visual Receive State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualReceiveState
	 * @generated
	 */
	public Adapter createVisualReceiveStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualShallowHistoryState <em>Visual Shallow History State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualShallowHistoryState
	 * @generated
	 */
	public Adapter createVisualShallowHistoryStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDeepHistoryState <em>Visual Deep History State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDeepHistoryState
	 * @generated
	 */
	public Adapter createVisualDeepHistoryStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement <em>IVisual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement
	 * @generated
	 */
	public Adapter createIVisualElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement <em>Visual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement
	 * @generated
	 */
	public Adapter createVisualElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //StatemachineAdapterFactory
