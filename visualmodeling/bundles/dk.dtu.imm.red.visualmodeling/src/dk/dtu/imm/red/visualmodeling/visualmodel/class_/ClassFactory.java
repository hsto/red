/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage
 * @generated
 */
public interface ClassFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassFactory eINSTANCE = dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.ClassFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Visual Class Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Class Element</em>'.
	 * @generated
	 */
	VisualClassElement createVisualClassElement();

	/**
	 * Returns a new object of class '<em>Visual Class Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Class Attribute</em>'.
	 * @generated
	 */
	VisualClassAttribute createVisualClassAttribute();

	/**
	 * Returns a new object of class '<em>Visual Class Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Class Operation</em>'.
	 * @generated
	 */
	VisualClassOperation createVisualClassOperation();

	/**
	 * Returns a new object of class '<em>Visual Enumeration Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Enumeration Element</em>'.
	 * @generated
	 */
	VisualEnumerationElement createVisualEnumerationElement();

	/**
	 * Returns a new object of class '<em>Visual Enumeration Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Enumeration Literal</em>'.
	 * @generated
	 */
	VisualEnumerationLiteral createVisualEnumerationLiteral();

	/**
	 * Returns a new object of class '<em>Visual Package Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visual Package Element</em>'.
	 * @generated
	 */
	VisualPackageElement createVisualPackageElement();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ClassPackage getClassPackage();

} //ClassFactory
