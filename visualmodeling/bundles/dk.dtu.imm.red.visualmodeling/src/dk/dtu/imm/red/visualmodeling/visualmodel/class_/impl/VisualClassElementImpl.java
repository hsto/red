/**
 */
package dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DataType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.DatatypeFactory;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.FunctionType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.NestedType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.UnparsedType;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.util.DataTypeParser;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.util.MultiplicityParser;
import dk.dtu.imm.red.specificationelements.modelelement.signature.AccessMode;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureFactory;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureItem;
import dk.dtu.imm.red.specificationelements.modelelement.signature.SignatureKind;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.util.AssociationDecision;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.util.AssociationDecisionDialog;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visual Class Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassElementImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassElementImpl#isInterface <em>Interface</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VisualClassElementImpl extends VisualSpecificationElementImpl implements VisualClassElement {
	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The default value of the '{@link #isInterface() <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInterface()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INTERFACE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInterface() <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInterface()
	 * @generated
	 * @ordered
	 */
	protected boolean interface_ = INTERFACE_EDEFAULT;
	
	/**
	 * @generated NOT
	 */
	private boolean elementCreationInProgress = false;

	/**
	 * @generated NOT
	 */
	private EcoreUtil.EqualityHelper equalityHelper = new EcoreUtil.EqualityHelper();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualClassElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassPackage.Literals.VISUAL_CLASS_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassPackage.VISUAL_CLASS_ELEMENT__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInterface() {
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterface(boolean newInterface) {
		boolean oldInterface = interface_;
		interface_ = newInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassPackage.VISUAL_CLASS_ELEMENT__INTERFACE, oldInterface, interface_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassPackage.VISUAL_CLASS_ELEMENT__ABSTRACT:
				return isAbstract();
			case ClassPackage.VISUAL_CLASS_ELEMENT__INTERFACE:
				return isInterface();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassPackage.VISUAL_CLASS_ELEMENT__ABSTRACT:
				setAbstract((Boolean)newValue);
				return;
			case ClassPackage.VISUAL_CLASS_ELEMENT__INTERFACE:
				setInterface((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassPackage.VISUAL_CLASS_ELEMENT__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case ClassPackage.VISUAL_CLASS_ELEMENT__INTERFACE:
				setInterface(INTERFACE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassPackage.VISUAL_CLASS_ELEMENT__ABSTRACT:
				return abstract_ != ABSTRACT_EDEFAULT;
			case ClassPackage.VISUAL_CLASS_ELEMENT__INTERFACE:
				return interface_ != INTERFACE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Abstract: ");
		result.append(abstract_);
		result.append(", Interface: ");
		result.append(interface_);
		result.append(')');
		return result.toString();
	}


	/**
	 * @generated NOT
	 * crude hack to have attributes and operations list taken from the elements
	 * because containment within elements are required for visual weaving code
	 */
	List<VisualClassAttribute> attributesList = new ArrayList<VisualClassAttribute>();
	List<VisualClassOperation> operationsList = new ArrayList<VisualClassOperation>();

	/**
	 * @generated NOT
	 */
	public List<VisualClassAttribute> getAttributes(){
		if (getSpecificationElement() != null) {
			List<VisualClassAttribute> resultList = new ArrayList<VisualClassAttribute>();
			Signature signature = (Signature) getSpecificationElement();
			for (SignatureItem item : signature.getItems()) {
				DataType outerType = item.getType();
				
				while (outerType instanceof NestedType) {
					outerType = ((NestedType) outerType).getContainedType();
				}
				
				if (!(outerType instanceof FunctionType)) {
					VisualClassAttribute newAttribute = ClassFactory.eINSTANCE.createVisualClassAttribute();
					newAttribute.setName(item.getName());
					newAttribute.setType(EcoreUtil.copy(item.getType()));
					newAttribute.setMultiplicity(EcoreUtil.copy(item.getMultiplicity()));
					newAttribute.setReadOnly(item.getAccessMode() == AccessMode.READ_ONLY);
					newAttribute.setVisibility(item.getVisibility());
					
					resultList.add(newAttribute);
				}
			}
			
			return resultList;
		}
		else {
			if(attributesList.isEmpty()){
				List<IVisualElement> elementList = getElements();
				for(int i=0;i<elementList.size();i++){
					IVisualElement el = elementList.get(i);
					if(el instanceof VisualClassAttribute){
						attributesList.add((VisualClassAttribute)el);
					}
				}
			}
			return attributesList;
		}
	}

	/**
	 * @generated NOT
	 */
	public List<VisualClassOperation> getOperations(){
		if (getSpecificationElement() != null) {
			List<VisualClassOperation> resultList = new ArrayList<VisualClassOperation>();
			Signature signature = (Signature) getSpecificationElement();
			for (SignatureItem item : signature.getItems()) {
				DataType outerType = item.getType();
				
				while (outerType instanceof NestedType) {
					outerType = ((NestedType) outerType).getContainedType();
				}
				
				if (outerType instanceof FunctionType) {
					VisualClassOperation newOperation = ClassFactory.eINSTANCE.createVisualClassOperation();
					newOperation.setName(item.getName());
					newOperation.setType(EcoreUtil.copy(((FunctionType) outerType).getOutput()));
					newOperation.setParameters(EcoreUtil.copy(((FunctionType) outerType).getInput()));
					newOperation.setMultiplicity(EcoreUtil.copy(item.getMultiplicity()));
					newOperation.setVisibility(item.getVisibility());
					
					resultList.add(newOperation);
				}
			}
			
			return resultList;
		}
		else {
			if(operationsList.isEmpty()){
				List<IVisualElement> elementList = getElements();
				for(int i=0;i<elementList.size();i++){
					IVisualElement el = elementList.get(i);
					if(el instanceof VisualClassOperation){
						operationsList.add((VisualClassOperation)el);
					}
				}
			}
			return operationsList;
		}
	}

	/**
	 * @generated NOT
	 */
	@Override
	public Class<?> getSpecificationElementType() {
		return Signature.class;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public SpecificationElement createSpecificationElement(Group parent) {
		
		if (elementCreationInProgress) {
			String message = "Some elements could not be created due to a circular definition.";
			MessageBox messageBox = new MessageBox(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), SWT.ICON_WARNING | SWT.OK);
			messageBox.setText("Could not create elements");
			messageBox.setMessage(message);
			messageBox.open();
			throw new IllegalStateException(message);
		}
		else {
			elementCreationInProgress = true;
		}
		
		List<VisualClassElement> classesToEmbed = new ArrayList<VisualClassElement>();
		List<VisualClassElement> classesToReference = new ArrayList<VisualClassElement>();
		List<Signature> superSignaturesToInline = new ArrayList<Signature>();
		List<Signature> compositionSignaturesToInline = new ArrayList<Signature>();
		
		for (IVisualConnection connection : getConnections()) {
			if (connection.getType().equals(ConnectionType.GENERALIZATION.getName()) 
					&& connection.getSource() == this
					&& connection.getTarget() instanceof VisualClassElement) {
				AssociationDecision decision = requestAssociationDecision(connection);
				if (decision == null) {
					return null;
				}
				else if (decision == AssociationDecision.EMBED) {
					classesToEmbed.add((VisualClassElement) connection.getTarget());
				}
				else if (decision == AssociationDecision.INLINE) {
					if (((VisualClassElement) connection.getTarget()).getSpecificationElement() == null) {
						SpecificationElement element = ((VisualClassElement) connection.getTarget()).createSpecificationElement(parent);
						((VisualClassElement) connection.getTarget()).setSpecificationElement(element);
					}
					
					Signature signature = (Signature) ((VisualClassElement) connection.getTarget()).getSpecificationElement();
					
					superSignaturesToInline.add(signature);
				}
			}
			else if (connection.getType().equals(ConnectionType.COMPOSITION.getName()) 
					&& connection.getSource() == this
					&& connection.getTarget() instanceof VisualClassElement) {
				AssociationDecision decision = requestAssociationDecision(connection);
				if (decision == null) {
					return null;
				}
				else if (decision == AssociationDecision.EMBED) {
					classesToEmbed.add((VisualClassElement) connection.getTarget());
				}
				else if (decision == AssociationDecision.INLINE) {
					if (((VisualClassElement) connection.getTarget()).getSpecificationElement() == null) {
						SpecificationElement element = ((VisualClassElement) connection.getTarget()).createSpecificationElement(parent);
						((VisualClassElement) connection.getTarget()).setSpecificationElement(element);
					}
					
					Signature signature = (Signature) ((VisualClassElement) connection.getTarget()).getSpecificationElement();
					
					compositionSignaturesToInline.add(signature);
				}
			}
			else if (connection.getType().equals(ConnectionType.ASSOCIATION.getName())
					&& connection.getSource() instanceof VisualClassElement
					&& connection.getTarget() instanceof VisualClassElement) {
				if (connection.getDirection() == ConnectionDirection.BIDIRECTIONAL ||
						(connection.getDirection() == ConnectionDirection.SOURCE_TARGET
						&& connection.getSource() == this)) {
					AssociationDecision decision = requestAssociationDecision(connection);
					if (decision == null) {
						return null;
					}
					else if (decision == AssociationDecision.EMBED) {
						classesToReference.add((VisualClassElement) 
								((connection.getSource() == this) ? connection.getTarget() : connection.getSource()));
					}
				}
			}
		}
		
		Signature signature = SignatureFactory.eINSTANCE.createSignature();
		signature.setCreator(PreferenceUtil.getUserPreference_User());
		
		String str = (this.name == null) ? "" : this.name; 
		
		signature.setName(str);
		signature.setLabel(str);
		signature.setDescription("");
		
		if (parent != null) {
			signature.setParent(parent);
		}
		
		if (isInterface()) {
			signature.setElementKind(SignatureKind.INTERFACE.getName());
		}
		
		signature.getItems().addAll(resolveAttributes(classesToEmbed, classesToReference, 
				superSignaturesToInline, compositionSignaturesToInline));
		
		signature.getItems().addAll(resolveMethods(superSignaturesToInline, compositionSignaturesToInline));
		
		signature.save();
		
		elementCreationInProgress = false;
		
		return signature;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void setSpecificationElement(SpecificationElement newSpecificationElement) {
		super.setSpecificationElement(newSpecificationElement);
	}
	
	private AssociationDecision requestAssociationDecision(IVisualConnection connection) {
		AssociationDecisionDialog dialog = new AssociationDecisionDialog(null, connection, this);
		dialog.create();
		int result = dialog.open();
		if (result == AssociationDecisionDialog.CANCEL) {
			return null;
		}
		else {
			return dialog.getDecision();
		}
	}
	
	/**
	 * @generated NOT
	 */
	private List<SignatureItem> resolveAttributes(List<VisualClassElement> classesToEmbed, 
			List<VisualClassElement> classesToReference, 
			List<Signature> superSignaturesToInline,
			List<Signature> compositionSignaturesToInline) {
		
		List<SignatureItem> resultItems = new ArrayList<SignatureItem>();
		List<SignatureItem> ignoredItems = new ArrayList<SignatureItem>();
		
		for (VisualClassAttribute attribute : getAttributes()) {
			for (Signature signatureToInline : superSignaturesToInline) {
				SignatureItem redefinedItem = findAttribute(signatureToInline.getItems(), attribute.getName());
				
				if (redefinedItem != null) {
					ignoredItems.add(redefinedItem);
				}
			}
			
			SignatureItem item = SignatureFactory.eINSTANCE.createSignatureItem();
			
			item.setName(attribute.getName());
			item.setType(EcoreUtil.copy(attribute.getType()));
			item.setVisibility(attribute.getVisibility());
			item.setMultiplicity(EcoreUtil.copy(attribute.getMultiplicity()));

			item.setAccessMode((attribute.isReadOnly()) ? AccessMode.READ_ONLY : AccessMode.READ_WRITE);
			
			resultItems.add(item);
		}
		
		List<Signature> signaturesToInline = new ArrayList<Signature>(superSignaturesToInline);
		signaturesToInline.addAll(compositionSignaturesToInline);
		
		for (Signature signatureToInline : signaturesToInline) {
			for (SignatureItem item : signatureToInline.getItems()) {
				if (item.getType() instanceof FunctionType
						|| ignoredItems.contains(item)) continue;
				
				resultItems.add(item);
			}
		}
		
		for (VisualClassElement classToEmbed : classesToEmbed) {
			SignatureItem item = SignatureFactory.eINSTANCE.createSignatureItem();
			
			item.setName(nonCapitalized(classToEmbed.getName()));
			item.setType(DataTypeParser.parse(classToEmbed.getName()));
			item.setVisibility(Visibility.PRIVATE);
			item.setMultiplicity(MultiplicityParser.parse("1"));
			
			resultItems.add(item);
		}
		
		for (VisualClassElement classToReference : classesToReference) {
			SignatureItem item = SignatureFactory.eINSTANCE.createSignatureItem();
			
			item.setName(nonCapitalized(classToReference.getName()));
			item.setType(DataTypeParser.parse(classToReference.getName()));
			
			resultItems.add(item);
		}
		
		return resultItems;
	}
	
	/**
	 * @generated NOT
	 */
	private List<SignatureItem> resolveMethods(List<Signature> superSignaturesToInline,
			List<Signature> compositionSignaturesToInline) {
		
		List<SignatureItem> resultItems = new ArrayList<SignatureItem>();
		List<SignatureItem> ignoredItems = new ArrayList<SignatureItem>();
		
		for (VisualClassOperation operation : getOperations()) {
			for (Signature signatureToInline : superSignaturesToInline) {
				SignatureItem redefinedItem = findFunction(signatureToInline.getItems(), 
						operation.getName(), operation.getParameters());
				
				if (redefinedItem != null) {
					ignoredItems.add(redefinedItem);
				}
			}
			
			SignatureItem item = SignatureFactory.eINSTANCE.createSignatureItem();
			
			item.setName(operation.getName());
			item.setVisibility(operation.getVisibility());
			item.setMultiplicity(EcoreUtil.copy(operation.getMultiplicity()));
			
			if (operation.getParameters() instanceof UnparsedType
					|| operation.getType() instanceof UnparsedType) {
				item.setType(DataTypeParser.parse(operation.getParameters().toString() 
						+ " -> " + operation.getType().toString()));
			}
			else {
				FunctionType type = DatatypeFactory.eINSTANCE.createFunctionType();
				type.setInput(EcoreUtil.copy(operation.getParameters()));
				type.setOutput(EcoreUtil.copy(operation.getType()));
				item.setType(type);
			}
			
			resultItems.add(item);
		}
		
		List<Signature> signaturesToInline = new ArrayList<Signature>(superSignaturesToInline);
		signaturesToInline.addAll(compositionSignaturesToInline);
		
		for (Signature signatureToInline : signaturesToInline) {
			for (SignatureItem item : signatureToInline.getItems()) {
				if (!(item.getType() instanceof FunctionType)
						|| ignoredItems.contains(item)) continue;
				
				resultItems.add(item);
			}
		}
		
		return resultItems;
	}
	
	/**
	 * @generated NOT
	 */
	private SignatureItem findAttribute(List<SignatureItem> items, String attributeName) {
		for (SignatureItem item : items) {
			if (item.getName().equals(attributeName)
					&& !(item.getType() instanceof FunctionType)) {
				return item;
			}
		}
		
		return null;
	}
	
	/**
	 * @generated NOT
	 */
	private SignatureItem findFunction(List<SignatureItem> items, String functionName, DataType parameters) {
		for (SignatureItem item : items) {
			if (item.getName().equals(functionName)
					&& item.getType() instanceof FunctionType) {
				DataType itemParamters = ((FunctionType) item.getType()).getInput();
				
				if (equalityHelper.equals(parameters, itemParamters)) {
					return item;
				}
			}
		}
		
		return null;
	}

	private String nonCapitalized(String string) {
		if (string == null || string.length() == 0) {
			return string;
		}
		char c[] = string.toCharArray();
		c[0] = Character.toLowerCase(c[0]);
		return new String(c);
	}
} //VisualClassElementImpl
