package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral;

public class VisualEnumerationFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(125,100);

	private Label stereotype = new Label();
	//private RectangleFigure formalFigure;

	//private SVGFigure sketchyFigure;
	private String sketchyFigureURL = svgPath + "Class/Enumeration.svg";

	private CompartmentFigure literalsCompartmentFigure = new CompartmentFigure();

	public VisualEnumerationFigure(IVisualElement model)
	{
		super(model);

		ToolbarLayout layout = new ToolbarLayout();

		formalFigure = new RectangleFigure();
		formalFigure.setBorder(new LineBorder(ColorConstants.black, 1));
	    formalFigure.setFill(true);
	    formalFigure.setBackgroundColor(ColorHelper.classDiagramDefaultColor);
	    formalFigure.setLayoutManager(layout);

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);
		sketchyFigure.setLayoutManager(layout);

		stereotype.setText("<<enumeration>>");
		refreshVisuals();
	}

	@Override
	protected void paintFigure(Graphics graphics) {

		Rectangle r = getBounds().getCopy();
		boundingRectangle.setVisible(false);

		if(this.getChildren().contains(sketchyFigure)){
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		}
		if(this.getChildren().contains(formalFigure)){
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
		}
	};

	@Override
	public void refreshVisuals() {

		VisualEnumerationElement model = (VisualEnumerationElement)super.model;
		this.removeAll();
		literalsCompartmentFigure.removeAll();

		//adding all of them back in the right z order
		//toggling between sketchy and non sketchy
		if(model.isIsSketchy()){
			add(sketchyFigure);
			stereotype.setFont(SKETCHY_FONT);
			nameLabel.setFont(SKETCHY_FONT);

			sketchyFigure.add(stereotype);
			sketchyFigure.add(nameLabel);
			sketchyFigure.add(literalsCompartmentFigure);
		}
		else{
			add(formalFigure);
			stereotype.setFont(DEFAULT_FONT);
			nameLabel.setFont(DEFAULT_FONT);

			formalFigure.add(stereotype);
			formalFigure.add(nameLabel);
			formalFigure.add(literalsCompartmentFigure);
		}


		for(VisualEnumerationLiteral literal : model.getLiterals())
		{
			Label literalLabel = new Label();
			if(model.isIsSketchy())
				literalLabel.setFont(SKETCHY_FONT);
			else
				literalLabel.setFont(DEFAULT_FONT);
			literalLabel.setText(literal.toString());
			literalsCompartmentFigure.add(literalLabel);
		}


		nameLabel.setText(this.model.getName());

//		System.out.println("THIS:"+this.getClass() + ":" +
//				this.getBounds().x +"," +
//				this.getBounds().y +"," +
//				this.getBounds().width +"," +
//				this.getBounds().height
//					);
//
//		for(Object child : this.getChildren()){
//			System.out.println("CHILD:"+child.getClass() + ":" +
//				((Figure)child).getBounds().x +"," +
//				((Figure)child).getBounds().y +"," +
//				((Figure)child).getBounds().width +"," +
//				((Figure)child).getBounds().height
//					);
//		}
//		System.out.println("----");
	}


	@Override
	public void add(IFigure figure, Object constraint, int index) {
		if(figure instanceof EmptyFigure)
			//special handling to skip adding the empty figure
			;
		else
			super.add(figure, constraint, index);
	}

	@Override
	public void remove(IFigure figure) {
		if(figure instanceof EmptyFigure)
			//special handling to skip adding the empty figure
			;
		else
			super.remove(figure);
	}

}