package dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric;

import org.eclipse.gef.EditPart;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualGenericElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.factories.EditPartFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement;

public class GenericEditPartFactory extends EditPartFactoryBase{

	public GenericEditPartFactory(RuleCollection ruleCollection) {
		super(ruleCollection);		
	}

	@Override
	protected EditPart createEditPartInternal(EditPart context, Object model) {
		
		if(model instanceof VisualGenericElement)
		{
			return new VisualGenericElementEditPart(ruleCollection);
		}	
		
		return null;
	}

}
