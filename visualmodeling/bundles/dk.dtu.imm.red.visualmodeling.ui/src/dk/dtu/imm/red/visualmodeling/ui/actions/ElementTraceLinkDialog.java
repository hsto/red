package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.Set;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import dk.dtu.imm.red.visualmodeling.ui.editors.DiagramExtension;
import dk.dtu.imm.red.visualmodeling.ui.editors.impl.VisualModelEditorImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

public class ElementTraceLinkDialog extends Dialog {

	private IVisualElement element;
	private  Set<IVisualElement> backwardTraces;
	private  Set<IVisualElement> forwardTraces;


	private IVisualConnection conn;
	private  Set<IVisualConnection> connBackwardTraces;
	private  Set<IVisualConnection> connForwardTraces;

	private Table tblBackwardTraces;
	private TableViewer tvBackwardTraces;
	private TableViewerColumn tvBackwardClmnDiagramName;
	private TableViewerColumn tvBackwardClmnDiagramType;
	private TableViewerColumn tvBackwardClmnElementName;

	private Table tblForwardTraces;
	private TableViewer tvForwardTraces;
	private TableViewerColumn tvForwardClmnDiagramName;
	private TableViewerColumn tvForwardClmnDiagramType;
	private TableViewerColumn tvForwardClmnElementName;
	/**
	 * Create the dialog.
	 * @param parentShell
	 */
	public ElementTraceLinkDialog(Shell parentShell, IVisualElement element,
			Set<IVisualElement> backwardTraces, Set<IVisualElement> forwardTraces) {
		super(parentShell);
		this.element = element;
		this.backwardTraces = backwardTraces;
		this.forwardTraces = forwardTraces;
	}


	public ElementTraceLinkDialog(Shell parentShell, IVisualConnection conn,
			Set<IVisualConnection> connBackwardTraces, Set<IVisualConnection> connForwardTraces) {
		super(parentShell);
		this.conn = conn;
		this.connBackwardTraces = connBackwardTraces;
		this.connForwardTraces = connForwardTraces;
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);

		Button btnBackToCurrent = new Button(container, SWT.NONE);
		btnBackToCurrent.setText("Back to current element");

		btnBackToCurrent.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Diagram d=null;
				if(element!=null){
					 d = element.getDiagram().getDiagramElement();
					 DiagramExtension ext = new DiagramExtension();
					 ext.openConcreteElement(d);
					//get graphical viewer of that editor, find the edit part for the model and select the edit part
					VisualModelEditorImpl editor = (VisualModelEditorImpl) ext.getOpenedEditor();
					GraphicalViewer viewer = editor.getVisualEditor().getGraphicalViewer();
					EditPart elementEditPart = (EditPart) viewer.getEditPartRegistry().get(element);
					viewer.select(elementEditPart);
				}
				else if(conn!=null){
					 d = conn.getSource().getDiagram().getDiagramElement();
					 DiagramExtension ext = new DiagramExtension();
					 ext.openConcreteElement(d);
					//get graphical viewer of that editor, find the edit part for the model and select the edit part
					VisualModelEditorImpl editor = (VisualModelEditorImpl) ext.getOpenedEditor();
					GraphicalViewer viewer = editor.getVisualEditor().getGraphicalViewer();
					EditPart connEditPart = (EditPart) viewer.getEditPartRegistry().get(conn);
					viewer.select(connEditPart);
				}

			}
		});

		Label lblBackwardTraces = new Label(container, SWT.NONE);
		lblBackwardTraces.setText("Backward Traces:");

		tvBackwardTraces = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		tblBackwardTraces = tvBackwardTraces.getTable();
		tblBackwardTraces.setHeaderVisible(true);
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_table.heightHint = 100;
		gd_table.minimumHeight = 100;
		gd_table.widthHint = 312;
		tblBackwardTraces.setLayoutData(gd_table);

		tvBackwardClmnDiagramName = new TableViewerColumn(tvBackwardTraces, SWT.NONE);
		TableColumn tblBackwardClmnBDiagramname = tvBackwardClmnDiagramName.getColumn();
		tblBackwardClmnBDiagramname.setWidth(147);
		tblBackwardClmnBDiagramname.setText("DiagramName");

		tvBackwardClmnDiagramType = new TableViewerColumn(tvBackwardTraces, SWT.NONE);
		TableColumn tblBackwardClmnDiagramtype = tvBackwardClmnDiagramType.getColumn();
		tblBackwardClmnDiagramtype.setWidth(124);
		tblBackwardClmnDiagramtype.setText("DiagramType");

		tvBackwardClmnElementName = new TableViewerColumn(tvBackwardTraces, SWT.NONE);
		TableColumn tblBackwardClmnElementname = tvBackwardClmnElementName.getColumn();
		tblBackwardClmnElementname.setWidth(77);
		tblBackwardClmnElementname.setText("ElementName");

		Label lblForwardTraces = new Label(container, SWT.NONE);
		lblForwardTraces.setText("Forward Traces:");

		tvForwardTraces = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		tblForwardTraces = tvForwardTraces.getTable();
		tblForwardTraces.setHeaderVisible(true);
		GridData gd_table2 = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_table2.heightHint = 100;
		gd_table2.minimumHeight = 100;
		gd_table2.widthHint = 312;
		tblForwardTraces.setLayoutData(gd_table2);

		tvForwardClmnDiagramName = new TableViewerColumn(tvForwardTraces, SWT.NONE);
		TableColumn tblForwardClmnDiagramname = tvForwardClmnDiagramName.getColumn();
		tblForwardClmnDiagramname.setWidth(147);
		tblForwardClmnDiagramname.setText("DiagramName");

		tvForwardClmnDiagramType = new TableViewerColumn(tvForwardTraces, SWT.NONE);
		TableColumn tblForwardClmnDiagramtype = tvForwardClmnDiagramType.getColumn();
		tblForwardClmnDiagramtype.setWidth(124);
		tblForwardClmnDiagramtype.setText("DiagramType");

		tvForwardClmnElementName = new TableViewerColumn(tvForwardTraces, SWT.NONE);
		TableColumn tblForwardClmnElementname = tvForwardClmnElementName.getColumn();
		tblForwardClmnElementname.setWidth(77);
		tblForwardClmnElementname.setText("ElementName");

		IDoubleClickListener doubleClickListener = new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event
						.getSelection();
				Object firstSelection = selection.getFirstElement();
				IVisualElement selectedElement=null;
				IVisualConnection selectedConn=null;
				Diagram d=null;
				if(firstSelection instanceof IVisualElement){
					selectedElement = (IVisualElement) firstSelection;
					d = selectedElement.getDiagram().getDiagramElement();
					DiagramExtension ext = new DiagramExtension();
					ext.openConcreteElement(d);
					//get graphical viewer of that editor, find the edit part for the model and select the edit part
					VisualModelEditorImpl editor = (VisualModelEditorImpl) ext.getOpenedEditor();
					GraphicalViewer viewer = editor.getVisualEditor().getGraphicalViewer();
					EditPart elementEditPart = (EditPart) viewer.getEditPartRegistry().get(selectedElement);
					viewer.select(elementEditPart);
				}

				else if(firstSelection instanceof IVisualConnection){
					selectedConn = (IVisualConnection) firstSelection;
					d = selectedConn.getSource().getDiagram().getDiagramElement();
					DiagramExtension ext = new DiagramExtension();
					ext.openConcreteElement(d);
					//get graphical viewer of that editor, find the edit part for the model and select the edit part
					VisualModelEditorImpl editor = (VisualModelEditorImpl) ext.getOpenedEditor();
					GraphicalViewer viewer = editor.getVisualEditor().getGraphicalViewer();
					EditPart connEditPart = (EditPart) viewer.getEditPartRegistry().get(selectedConn);
					viewer.select(connEditPart);
				}
			}
		};
		tvBackwardTraces.addDoubleClickListener(doubleClickListener);
		tvForwardTraces.addDoubleClickListener(doubleClickListener);

		setTableViewerProperties();
		loadInitialData();
		refreshTables();

		return container;
	}

	private void setTableViewerProperties() {
		ColumnLabelProvider diagramLabelProvider = new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if(element instanceof IVisualElement){
					if(((IVisualElement) element).getDiagram()!=null){
						VisualDiagram d = ((IVisualElement) element).getDiagram();
						if(d.getDiagramElement()!=null)
							return d.getDiagramElement().getLabel();
					}
					return "";
				}
				else
					return ((IVisualConnection) element).getSource().getDiagram().getDiagramElement().getLabel();
			}
		};
		tvBackwardClmnDiagramName.setLabelProvider(diagramLabelProvider);
		tvForwardClmnDiagramName.setLabelProvider(diagramLabelProvider);

		ColumnLabelProvider diagramTypeProvider = new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if(element instanceof IVisualElement){
					if(((IVisualElement) element).getDiagram()!=null){
						VisualDiagram d = ((IVisualElement) element).getDiagram();
						return d.getDiagramType().getName();
					}
					return "";
				}
				else
					return ((IVisualConnection) element).getSource().getDiagram().getDiagramType().getName();
			}
		};
		tvBackwardClmnDiagramType.setLabelProvider(diagramTypeProvider);
		tvForwardClmnDiagramType.setLabelProvider(diagramTypeProvider);

		ColumnLabelProvider elementNameProvider = new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if(element instanceof IVisualElement)
					return ((IVisualElement) element).getName();
				else
					return ((IVisualConnection) element).getName();
			}
		};
		tvBackwardClmnElementName.setLabelProvider(elementNameProvider);
		tvForwardClmnElementName.setLabelProvider(elementNameProvider);

	}

	private void loadInitialData() {
		if(element!=null){
			tvBackwardTraces.setContentProvider(new ArrayContentProvider());
			tvBackwardTraces.setInput(backwardTraces);
			tvForwardTraces.setContentProvider(new ArrayContentProvider());
			tvForwardTraces.setInput(forwardTraces);
		}
		else if(conn!=null){
			tvBackwardTraces.setContentProvider(new ArrayContentProvider());
			tvBackwardTraces.setInput(connBackwardTraces);
			tvForwardTraces.setContentProvider(new ArrayContentProvider());
			tvForwardTraces.setInput(connForwardTraces);
		}
	}

	private void refreshTables() {
		tvBackwardTraces.refresh();
		tvForwardTraces.refresh();

	}
	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
//		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
//				true);
//		createButton(parent, IDialogConstants.CANCEL_ID,
//				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(422, 615);
	}

	@Override
	protected void setShellStyle(int newShellStyle) {
		// make the dialog non modal
		super.setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
		setBlockOnOpen(false);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if(element!=null)
			shell.setText("Traces of " + element.getName());
		else if(conn!=null)
			shell.setText("Traces of " + conn.getName());
	}

}
