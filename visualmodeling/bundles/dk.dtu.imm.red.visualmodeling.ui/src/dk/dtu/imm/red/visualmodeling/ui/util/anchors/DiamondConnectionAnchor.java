package dk.dtu.imm.red.visualmodeling.ui.util.anchors;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;

public class DiamondConnectionAnchor extends AbstractConnectionAnchor{

	public DiamondConnectionAnchor(IFigure owner)
	{
		super(owner);
	}
	
	@Override
	public Point getLocation(Point reference) {
		
		Point origin = getOwner().getBounds().getCenter();
		// Translates to absolute to allow for scrolling
		getOwner().translateToAbsolute(origin);
		
		int Ax = Math.abs(reference.x - origin.x);
		int Ay = Math.abs(reference.y - origin.y);
		
		int divisor = Ax + Ay;
		if(divisor == 0)
			return origin;
		
		int radiusX = getOwner().getBounds().width / 2;
		int radiusY = getOwner().getBounds().height / 2;
		
		int x = (radiusX * Ax) / divisor;
		int y = (radiusY * Ay) / divisor;
		
		if(reference.x < origin.x)
			x = -x;
		if(reference.y < origin.y)
			y = -y;
		
		return new Point(origin.x + x, origin.y + y);
	}

}
