package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualDecisionMergeNodeFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualDecisionMergeNode;

public class VisualDecisionMergeNodeFactory implements CreationFactory {
	public Object getNewObject() {

		VisualDecisionMergeNode element = ActivityFactory.eINSTANCE.
				createVisualDecisionMergeNode();
		element.setBounds(VisualDecisionMergeNodeFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualDecisionMergeNode.class;
	}
}
