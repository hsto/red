package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts;

import org.eclipse.draw2d.IFigure;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.figures.VisualGoalElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualGoalElementEditPart extends VisualElementEditPart{

	public VisualGoalElementEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	@Override
	protected IFigure createFigure() {
		return new VisualGoalElementFigure((VisualSpecificationElement)getModel());
	}

}
