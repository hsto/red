package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualReceiveStateFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualReceiveState;

public class VisualReceiveStateFactory implements CreationFactory {
	public Object getNewObject() {

		VisualReceiveState element = StatemachineFactory.eINSTANCE
				.createVisualReceiveState();
		element.setBounds(VisualReceiveStateFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualReceiveState.class;
	}
}
