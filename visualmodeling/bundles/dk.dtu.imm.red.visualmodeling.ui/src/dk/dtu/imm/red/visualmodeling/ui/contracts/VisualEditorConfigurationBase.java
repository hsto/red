package dk.dtu.imm.red.visualmodeling.ui.contracts;

import java.util.HashMap;

import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.ui.factories.EditPartFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;

public abstract class VisualEditorConfigurationBase implements IVisualEditorConfiguration{

	protected PaletteFactoryBase paletteFactory;
	protected EditPartFactoryBase editPartFactory;
	protected RuleCollection ruleCollection;
	protected VisualContextMenuProviderBase contextMenuProvider;
	
	@Override
	public IPaletteFactory getPaletteFactory() {
		return paletteFactory;
	}

	@Override
	public EditPartFactory getEditPartFactory() {
		return editPartFactory;
	}

	@Override
	public HashMap<Class<? extends SpecificationElement>, Class<? extends CreationFactory>> getSpecificationElementMapping() {
		
		return null;
	}
	
	@Override
	public IVisualContextMenuProvider getContextMenuProvider() {
		return contextMenuProvider;
	}
}
