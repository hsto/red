package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;
import org.eclipse.jface.resource.ImageDescriptor;

import dk.dtu.imm.red.visualmodeling.ui.directedit.CreationAndDirectEditTool;
import dk.dtu.imm.red.visualmodeling.ui.factories.DiagramConnectionFactory;
import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalElementFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalLayerFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;

public class GoalPaletteFactory extends PaletteFactoryBase {

	@Override
	public void populatePaletteInternal(PaletteRoot root) {

		PaletteDrawer group = new PaletteDrawer("Goal");

		CreationToolEntry entry;
		entry = new CreationToolEntry("Goal", "Create a new goal",
				new VisualGoalElementFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Goal Layer", "Create a new goal layer",
				new VisualGoalLayerFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		group.add(new PaletteSeparator());

		ConnectionCreationToolEntry connectionEntry = new ConnectionCreationToolEntry(
				"Supporting", "Creates a new support connection",
				new DiagramConnectionFactory(ConnectionType.SUPPORTING.getName(),
						ConnectionType.SUPPORTING.getName(),
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);


		ImageDescriptor img = null;

		try {
			img = ImageDescriptor.createFromURL(new URL("platform:/plugin/dk.dtu.imm.red.visualmodeling.ui/icons/lightning_32x32.png"));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		connectionEntry = new ConnectionCreationToolEntry("Obstructing",
				"Creates a new obstruction connection",
				new DiagramConnectionFactory(ConnectionType.OBSTRUCTING.getName(),
						ConnectionType.OBSTRUCTING.getName(),
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), img, img);
		group.add(connectionEntry);

		root.add(group);
	}
}
