package dk.dtu.imm.red.visualmodeling.ui.validation;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public interface IRule {
	
	boolean canConnect(IVisualElement source, IVisualElement target, IVisualConnection connection);
	
	boolean canAccept(IVisualElement parent, IVisualElement child);
}
