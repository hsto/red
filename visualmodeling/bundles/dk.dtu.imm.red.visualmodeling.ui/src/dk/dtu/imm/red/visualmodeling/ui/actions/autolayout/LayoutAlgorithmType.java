package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.zest.layouts.LayoutAlgorithm;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.CompositeLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.DirectedGraphLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.GridLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.HorizontalLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.HorizontalShift;
import org.eclipse.zest.layouts.algorithms.HorizontalTreeLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.VerticalLayoutAlgorithm;

public class LayoutAlgorithmType {

	public static final String DIRECTED_GRAPH_LAYOUT = "Directed Graph Layout";
	public static final String GRID_LAYOUT = "Grid Layout";
	public static final String HORIZONTAL_LAYOUT = "Horizontal Layout";
	public static final String HORIZONTAL_TREE_LAYOUT = "Horizontal Tree Layout";
	public static final String RADIAL_LAYOUT = "Radial Layout";
	public static final String SPRING_LAYOUT = "Spring Layout";
	public static final String TREE_LAYOUT = "Tree Layout";
	public static final String VERTICAL_LAYOUT = "Vertical Layout";

	public static final LayoutAlgorithm directedGraphAlgo =
//			new	DirectedGraphLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		new CompositeLayoutAlgorithm(
			LayoutStyles.NO_LAYOUT_NODE_RESIZING, new LayoutAlgorithm[] {
			new	DirectedGraphLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING),
			new	HorizontalShift(LayoutStyles.NO_LAYOUT_NODE_RESIZING) });

	public static final LayoutAlgorithm gridLayoutAlgo =
			new	GridLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
//		new CompositeLayoutAlgorithm(
//			LayoutStyles.NO_LAYOUT_NODE_RESIZING, new LayoutAlgorithm[] {
//			new	GridLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING),
//			new	HorizontalShift(LayoutStyles.NO_LAYOUT_NODE_RESIZING) });

	public static final LayoutAlgorithm horizontalLayoutAlgo =
//			new	HorizontalLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		new CompositeLayoutAlgorithm(
			LayoutStyles.NO_LAYOUT_NODE_RESIZING, new LayoutAlgorithm[] {
			new	HorizontalLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING),
			new	HorizontalShift(LayoutStyles.NO_LAYOUT_NODE_RESIZING) });

	public static final LayoutAlgorithm horizontalTreeLayoutAlgo =
			new HorizontalTreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
//		new CompositeLayoutAlgorithm(
//			LayoutStyles.NO_LAYOUT_NODE_RESIZING, new LayoutAlgorithm[] {
//			new	HorizontalTreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING),
//			new	HorizontalShift(LayoutStyles.NO_LAYOUT_NODE_RESIZING) });

	public static final LayoutAlgorithm radialLayoutAlgo =
//			new	RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		new CompositeLayoutAlgorithm(
			LayoutStyles.NO_LAYOUT_NODE_RESIZING, new LayoutAlgorithm[] {
			new	RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING),
			new	HorizontalShift(LayoutStyles.NO_LAYOUT_NODE_RESIZING) });

	public static final LayoutAlgorithm springLayoutAlgo =
			new	SpringLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
//		new CompositeLayoutAlgorithm(
//			LayoutStyles.NO_LAYOUT_NODE_RESIZING, new LayoutAlgorithm[] {
//			new	SpringLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING),
//			new	HorizontalShift(LayoutStyles.NO_LAYOUT_NODE_RESIZING) });

	public static final LayoutAlgorithm treeLayoutAlgo =
			new	TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
//		new CompositeLayoutAlgorithm(
//			LayoutStyles.NO_LAYOUT_NODE_RESIZING, new LayoutAlgorithm[] {
//			new	TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING),
//			new	HorizontalShift(LayoutStyles.NO_LAYOUT_NODE_RESIZING) });

	public static final LayoutAlgorithm verticalLayoutAlgo =
		new CompositeLayoutAlgorithm(
			LayoutStyles.NO_LAYOUT_NODE_RESIZING, new LayoutAlgorithm[] {
			new	VerticalLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING),
			new	HorizontalShift(LayoutStyles.NO_LAYOUT_NODE_RESIZING) });

	public static LayoutAlgorithm getLayoutAlgorithm(String type){
		LayoutAlgorithm algorithm = treeLayoutAlgo;
		switch(type){
			case DIRECTED_GRAPH_LAYOUT:
				algorithm = directedGraphAlgo;
				break;

			case GRID_LAYOUT:
				algorithm = gridLayoutAlgo;
				break;

			case HORIZONTAL_LAYOUT:
				algorithm = horizontalLayoutAlgo;
				break;

			case HORIZONTAL_TREE_LAYOUT:
				algorithm = horizontalTreeLayoutAlgo;
				break;

			case RADIAL_LAYOUT:
				algorithm = radialLayoutAlgo;
				break;

			case SPRING_LAYOUT:
				algorithm = springLayoutAlgo;
				break;

			case TREE_LAYOUT:
				algorithm = treeLayoutAlgo;
				break;

			case VERTICAL_LAYOUT:
				algorithm = verticalLayoutAlgo;
				break;

		}
		return algorithm;

	}
}
