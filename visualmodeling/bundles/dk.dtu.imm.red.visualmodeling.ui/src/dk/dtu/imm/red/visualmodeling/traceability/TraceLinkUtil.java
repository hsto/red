package dk.dtu.imm.red.visualmodeling.traceability;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship;

public class TraceLinkUtil {

	public static Set<IVisualElement> findBackwardTraces(IVisualElement visualElement){
		LinkedHashSet<IVisualElement> backwardTraces = new LinkedHashSet<IVisualElement>();

		if(visualElement.getStitchFrom().size()>0){
			Set<IVisualElement> unvisitedElements = new HashSet<IVisualElement>();
			Set<IVisualElement> visitedElements = new HashSet<IVisualElement>();
			for(StitchElementRelationship stitchRel : visualElement.getStitchFrom()){
				unvisitedElements.add(stitchRel.getStitchOrigin());
			}

			while(!unvisitedElements.isEmpty()){
				Iterator<IVisualElement> iter = unvisitedElements.iterator();
				Set<IVisualElement> toAdd = new HashSet<IVisualElement>();
				while(iter.hasNext()){
					IVisualElement trace = iter.next();
					if(trace!=null && trace.getStitchFrom()!=null && trace.getStitchFrom().size()>0){
						for(StitchElementRelationship traceRel : trace.getStitchFrom()){
							toAdd.add(traceRel.getStitchOrigin());
						}
					}
					visitedElements.add(trace);
					backwardTraces.add(trace);
				}
				unvisitedElements.addAll(toAdd);
				toAdd.clear();
				unvisitedElements.removeAll(visitedElements);
				visitedElements.clear();
			}

			unvisitedElements.clear();
			visitedElements.clear();
		}

		return backwardTraces;
	}

	public static Set<IVisualConnection> findBackwardTraces(IVisualConnection visualConnection){
		LinkedHashSet<IVisualConnection> backwardTraces = new LinkedHashSet<IVisualConnection>();

		if(visualConnection.getStitchFrom().size()>0){
			Set<IVisualConnection> unvisitedConnections = new HashSet<IVisualConnection>();
			Set<IVisualConnection> visitedConnections = new HashSet<IVisualConnection>();
			for(StitchConnectionRelationship stitchRel : visualConnection.getStitchFrom()){
				unvisitedConnections.add(stitchRel.getStitchOrigin());
			}

			while(!unvisitedConnections.isEmpty()){
				Iterator<IVisualConnection> iter = unvisitedConnections.iterator();
				Set<IVisualConnection> toAdd = new HashSet<IVisualConnection>();
				while(iter.hasNext()){
					IVisualConnection trace = iter.next();
					if(trace!=null && trace.getStitchFrom()!=null && trace.getStitchFrom().size()>0){
						for(StitchConnectionRelationship traceRel : trace.getStitchFrom()){
							toAdd.add(traceRel.getStitchOrigin());
						}
					}
					visitedConnections.add(trace);
					backwardTraces.add(trace);
				}
				unvisitedConnections.addAll(toAdd);
				toAdd.clear();
				unvisitedConnections.removeAll(visitedConnections);
				visitedConnections.clear();
			}

			unvisitedConnections.clear();
			visitedConnections.clear();
		}

		return backwardTraces;
	}

	public static Set<IVisualElement> findForwardTraces(IVisualElement visualElement){
		LinkedHashSet<IVisualElement> forwardTraces = new LinkedHashSet<IVisualElement>();

		if(visualElement.getStitchTo().size()>0){
			Set<IVisualElement> unvisitedElements = new HashSet<IVisualElement>();
			Set<IVisualElement> visitedElements = new HashSet<IVisualElement>();
			for(StitchElementRelationship stitchRel : visualElement.getStitchTo()){
				unvisitedElements.add(stitchRel.getStitchOutput());
			}

			while(!unvisitedElements.isEmpty()){
				Iterator<IVisualElement> iter = unvisitedElements.iterator();
				Set<IVisualElement> toAdd = new HashSet<IVisualElement>();
				while(iter.hasNext()){
					IVisualElement trace = iter.next();
					if(trace!=null && trace.getStitchTo()!=null && trace.getStitchTo().size()>0){
						for(StitchElementRelationship traceRel : trace.getStitchTo()){
							toAdd.add(traceRel.getStitchOutput());
						}
					}
					visitedElements.add(trace);
					forwardTraces.add(trace);
				}
				unvisitedElements.addAll(toAdd);
				toAdd.clear();
				unvisitedElements.removeAll(visitedElements);
				visitedElements.clear();
			}

			unvisitedElements.clear();
			visitedElements.clear();
		}

		return forwardTraces;

	}

	public static Set<IVisualConnection> findForwardTraces(IVisualConnection visualConnection){
		LinkedHashSet<IVisualConnection> forwardTraces = new LinkedHashSet<IVisualConnection>();

		if(visualConnection.getStitchTo().size()>0){
			Set<IVisualConnection> unvisitedConnections = new HashSet<IVisualConnection>();
			Set<IVisualConnection> visitedConnections = new HashSet<IVisualConnection>();
			for(StitchConnectionRelationship stitchRel : visualConnection.getStitchTo()){
				unvisitedConnections.add(stitchRel.getStitchOutput());
			}

			while(!unvisitedConnections.isEmpty()){
				Iterator<IVisualConnection> iter = unvisitedConnections.iterator();
				Set<IVisualConnection> toAdd = new HashSet<IVisualConnection>();
				while(iter.hasNext()){
					IVisualConnection trace = iter.next();
					if(trace!=null && trace.getStitchTo()!=null && trace.getStitchTo().size()>0){
						for(StitchConnectionRelationship traceRel : trace.getStitchTo()){
							toAdd.add(traceRel.getStitchOutput());
						}
					}
					visitedConnections.add(trace);
					forwardTraces.add(trace);
				}
				unvisitedConnections.addAll(toAdd);
				toAdd.clear();
				unvisitedConnections.removeAll(visitedConnections);
				visitedConnections.clear();
			}

			unvisitedConnections.clear();
			visitedConnections.clear();
		}

		return forwardTraces;
	}


	public static Set<IVisualElement> findAllTrace(IVisualElement visualElement){
		Set<IVisualElement> allTraces = new LinkedHashSet<IVisualElement>();
		Set<IVisualElement> backwardTraces = findBackwardTraces(visualElement);
		Set<IVisualElement> forwardTraces = findForwardTraces(visualElement);
		if(backwardTraces!=null && !backwardTraces.isEmpty())
			allTraces.addAll(backwardTraces);
		if(forwardTraces!=null && !forwardTraces.isEmpty())
			allTraces.addAll(forwardTraces);
		return allTraces;
	}

	public static Set<IVisualConnection> findAllTrace(IVisualConnection visualConn){
		Set<IVisualConnection> allTraces = new LinkedHashSet<IVisualConnection>();
		Set<IVisualConnection> backwardTraces = findBackwardTraces(visualConn);
		Set<IVisualConnection> forwardTraces = findForwardTraces(visualConn);
		if(backwardTraces!=null && !backwardTraces.isEmpty())
			allTraces.addAll(backwardTraces);
		if(forwardTraces!=null && !forwardTraces.isEmpty())
			allTraces.addAll(forwardTraces);
		return allTraces;
	}

}
