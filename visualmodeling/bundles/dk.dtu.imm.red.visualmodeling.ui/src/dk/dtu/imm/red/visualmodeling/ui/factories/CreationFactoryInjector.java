package dk.dtu.imm.red.visualmodeling.ui.factories;

import org.eclipse.gef.requests.CreationFactory;
import org.picocontainer.behaviors.Intercepted;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class CreationFactoryInjector implements CreationFactory {

	private Intercepted.Controller controller;
	private SpecificationElement specificationElement;

	public CreationFactoryInjector(Intercepted.Controller controller, SpecificationElement specificationElement) {
		this.controller = controller;
		this.specificationElement = specificationElement;
	}

	@Override
	public Object getNewObject() {
		
		Object obj = controller.getOriginalRetVal();
		if(obj instanceof VisualSpecificationElement)
		{
			VisualSpecificationElement element = (VisualSpecificationElement)obj;
			element.setSpecificationElement(specificationElement);
		}
		
		return null; // Ignored
	}

	@Override
	public Object getObjectType() {
		// Do nothing
		return null; // Ignored
	}

}
