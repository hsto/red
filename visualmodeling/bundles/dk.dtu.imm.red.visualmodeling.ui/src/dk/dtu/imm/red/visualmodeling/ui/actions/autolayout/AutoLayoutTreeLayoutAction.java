package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.ui.IEditorPart;

public class AutoLayoutTreeLayoutAction extends AutoLayoutAction {

    public AutoLayoutTreeLayoutAction(IEditorPart editor) {
		super(editor, LayoutAlgorithmType.TREE_LAYOUT);
	}

}
