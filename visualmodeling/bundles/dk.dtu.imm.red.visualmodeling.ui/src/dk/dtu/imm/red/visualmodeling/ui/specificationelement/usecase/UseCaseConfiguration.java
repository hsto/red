package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualEditorConfigurationBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericPaletteFactory;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;

public class UseCaseConfiguration extends VisualEditorConfigurationBase{

	public UseCaseConfiguration()
	{
		this.ruleCollection = new RuleCollection();
		this.ruleCollection.addRule(new UseCaseRule());
		
		this.paletteFactory = new UseCasePaletteFactory();	
		this.paletteFactory.setNext(new GenericPaletteFactory());
		
		this.editPartFactory = new UseCaseEditPartFactory(ruleCollection);
		this.editPartFactory.setNext(new GenericEditPartFactory(ruleCollection));
		
		this.contextMenuProvider = new UseCaseContextMenuProvider();
	}
}
