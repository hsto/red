package dk.dtu.imm.red.visualmodeling.ui;

import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.eclipse.gef.EditPart;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualConnectionEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualPortEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemStructureActorEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualClassEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualActorElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualSystemBoundaryElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualUseCaseElementEditPart;

// TODO extend search and element type filter for easy navigation of treeview if possible
// TODO user should have a correct selected specification element selected to click ok, else cancel.
public class SpecificationElementSelectionDialog extends Dialog{

	private ElementTreeViewWidget treeView;

	private Class<Element>[] typesShown;
	private Class<Element>[] typesExcluded;
	private SpecificationElement selectedElement;
	
	public SpecificationElementSelectionDialog(Shell parentShell, Class<Element>[] typesShown) {
		super(parentShell);
		
		this.typesShown = typesShown;
	}

	@SuppressWarnings("unchecked")
	public SpecificationElementSelectionDialog(Shell parentShell, EditPart sourceEditPart) {
		super(parentShell);
		
		if(sourceEditPart instanceof VisualGoalElementEditPart){
			this.typesShown = new Class[]{Goal.class};
		}
		else if(sourceEditPart instanceof VisualActorElementEditPart || sourceEditPart instanceof VisualSystemStructureActorEditPart || 
				 sourceEditPart instanceof VisualSystemBoundaryElementEditPart){
			this.typesShown = new Class[]{Actor.class};
			this.typesExcluded = new Class[]{System.class, Port.class};
		}
		else if (sourceEditPart instanceof VisualPortEditPart) {
			this.typesShown = new Class[]{Port.class};
		}
		else if (sourceEditPart instanceof VisualSystemEditPart) {
			this.typesShown = new Class[]{System.class};
		}
		else if (sourceEditPart instanceof VisualUseCaseElementEditPart){
			this.typesShown = new Class[]{UseCase.class};
		}
		else if (sourceEditPart instanceof VisualClassEditPart) {
			this.typesShown = new Class[]{Signature.class};
		}
		// For connections
		else if (sourceEditPart instanceof VisualConnectionEditPart) {
			this.typesShown = new Class[]{Connector.class};
		}
		else {
			throw new NotImplementedException("Linking not implemented for edit parts of type '" + sourceEditPart.getClass() + "'.");
		}
	}

	@Override
	  protected Control createDialogArea(Composite parent) {
	    Composite container = (Composite) super.createDialogArea(parent);

	    treeView = new ElementTreeViewWidget(false, container, SWT.BORDER, typesShown, typesExcluded);

	    treeView.addSelectionChangedListener(
	    		new ISelectionChangedListener() {
					@Override
					@SuppressWarnings("unchecked")
					public void selectionChanged(SelectionChangedEvent event) {
						ITreeSelection selection = (ITreeSelection) event.getSelection();
						List<Element> selectedElements = selection.toList();
						if(selectedElements.size() > 0)
						{
							Element element = selectedElements.get(0);
							if(element instanceof SpecificationElement)
							{
								selectedElement = (SpecificationElement)element;
							}else
							{
								selectedElement = null;
							}
						}
					}
				}
	    		);
	    return container;
	  }

	  @Override
	  protected void configureShell(Shell newShell) {
	    super.configureShell(newShell);
	    newShell.setText("Element selection dialog");
	  }

	  @Override
	  protected Point getInitialSize() {
	    return new Point(450, 300);
	  }

	  public SpecificationElement getSelection()
	  {
		  return selectedElement;
	  }
}
