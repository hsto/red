package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.Set;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.traceability.TraceLinkUtil;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;

public class VisualConnectionLinkCommand extends Command{

	private VisualConnection connection;
	private SpecificationElement specificationElement;
	private String oldName = "";

		public VisualConnectionLinkCommand(VisualConnection connection,	SpecificationElement specificationElement) {
			this.connection = connection;
			this.specificationElement = specificationElement;
			oldName = (connection.getName() == null) ? "" : connection.getName();
		}

		@Override public void execute() {
	    	linkConnection(connection);

	    	if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
				Set<IVisualConnection> allTraces = TraceLinkUtil.findAllTrace(connection);
				for(IVisualConnection e : allTraces){
			    	linkConnection(e);
				}
			}
	    }

	    private void linkConnection(IVisualConnection connection){
	    	if(connection instanceof VisualConnection){
	    		((VisualConnection)connection).setSpecificationElement(specificationElement);
	    		if(specificationElement!=null && specificationElement.getName()!=null)
	    			connection.setName(specificationElement.getName());
	    	}
	    }

	    @Override public void undo() {
	    	unlinkElement(connection);

	    	if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
				Set<IVisualConnection> allTraces = TraceLinkUtil.findAllTrace(connection);
				for(IVisualConnection e : allTraces){
					unlinkElement(e);
				}
			}
	    }

	    private void unlinkElement(IVisualConnection connection){
	    	if(connection instanceof VisualConnection){
	    		((VisualConnection)connection).setSpecificationElement(null);
		    	connection.setName(oldName);
	    	}
	    }
}
