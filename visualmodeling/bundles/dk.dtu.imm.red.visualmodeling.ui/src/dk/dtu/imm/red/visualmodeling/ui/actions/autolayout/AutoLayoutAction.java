package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.gef.Request;
import org.eclipse.gef.ui.actions.EditorPartAction;
import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.visualmodeling.ui.VisualEditor;
import dk.dtu.imm.red.visualmodeling.ui.commands.AutoLayoutCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

public abstract class AutoLayoutAction extends EditorPartAction {

	protected VisualEditor editor;
	protected Diagram diagram;
	protected VisualDiagram visualDiagram;
	protected Request request;
	public String algorithmType;

	public AutoLayoutAction(IEditorPart editor, String algorithmType) {
		super(editor);

		if(editor instanceof VisualEditor){
			this.editor = (VisualEditor) editor;
			diagram = this.editor.getDiagram();
			visualDiagram = diagram.getVisualDiagram();
			this.algorithmType = algorithmType;
		}

        setId(algorithmType);
        setText(algorithmType);
        request = new Request(algorithmType);
	}

	public AutoLayoutAction(IEditorPart editor, int style) {
		super(editor, style);
	}

	@Override
	protected boolean calculateEnabled() {
		return true;
	}

	@Override
	public void run() {
		 AutoLayoutCommand autoLayoutCommand = new AutoLayoutCommand(diagram, algorithmType);
		 execute(autoLayoutCommand);

	}

}