package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualSendSignalNodeFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualSendSignalNode;

public class VisualSendSignalNodeFactory implements CreationFactory {
	public Object getNewObject() {

		VisualSendSignalNode element = ActivityFactory.eINSTANCE
				.createVisualSendSignalNode();
		element.setBounds(VisualSendSignalNodeFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualSendSignalNode.class;
	}
}
