package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures;

import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PolylineShape;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PrecisionPoint;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualFlowFinalNodeFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(30,30);

	private Ellipse formalFigureEllipse;
	private PolylineShape formalFigureSlash;
	private PolylineShape formalFigureBackwardSlash;

	//private SVGFigure sketchyFigure;

	private String sketchyFigureURL = svgPath + "Activity/FlowFinal.svg";

	public VisualFlowFinalNodeFigure(IVisualElement model)
	{
		super(model);

		formalFigureEllipse = new Ellipse();
		formalFigureEllipse.setFill(true);
		formalFigureEllipse.setBackgroundColor(ColorHelper.activityDiagramDefaultColor);
		formalFigureSlash = new PolylineShape();
		formalFigureBackwardSlash = new PolylineShape();

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);

	    add(this.nameLabel);
	    refreshVisuals();
	}

	@Override
	  protected void paintFigure(Graphics graphics) {
		Rectangle r = getBounds().getCopy();

		boundingRectangle.setVisible(false);


		if (this.getChildren().contains(sketchyFigure))
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(formalFigureEllipse)) {
			setConstraint(formalFigureEllipse, new Rectangle(0, 0, r.width, r.height));

			formalFigureSlash.removeAllPoints();
			double radius = formalFigureEllipse.getBounds().width/2;
			double distCornerToCircle = radius * Math.sqrt(2) - radius;
			double xDistCornerToCircle = distCornerToCircle/Math.sqrt(2);

			formalFigureSlash.setStart(new PrecisionPoint(xDistCornerToCircle,xDistCornerToCircle));
			formalFigureSlash.addPoint(
					new PrecisionPoint(
							2*radius-xDistCornerToCircle,
							2*radius-xDistCornerToCircle)
					);
			setConstraint(formalFigureSlash, new Rectangle(0, 0, formalFigureEllipse.getBounds().width, formalFigureEllipse.getBounds().height));

			formalFigureBackwardSlash.removeAllPoints();
			formalFigureBackwardSlash.setStart(
					new PrecisionPoint(
							xDistCornerToCircle,
							2*radius-xDistCornerToCircle));
			formalFigureBackwardSlash.addPoint(
					new PrecisionPoint(
							2*radius-xDistCornerToCircle,
							xDistCornerToCircle)
					);
			setConstraint(formalFigureBackwardSlash,
					new Rectangle(0, 0, formalFigureEllipse.getBounds().width, formalFigureEllipse.getBounds().height));
		}

		setConstraint(nameLabel, new Rectangle(0, 0, r.width, 30));
	  }

	@Override
	public void refreshVisuals()
	{
		if (this.getChildren().contains(formalFigureEllipse)) {
			remove(formalFigureEllipse);
			remove(formalFigureSlash);
			remove(formalFigureBackwardSlash);
		}
		if (this.getChildren().contains(sketchyFigure)) {
			remove(sketchyFigure);
		}

		// toggling between sketchy and non sketchy
		if (model.isIsSketchy()) {
			add(sketchyFigure, 0);
			nameLabel.setFont(SKETCHY_FONT);
		} else {
			add(formalFigureEllipse, 0);
			add(formalFigureSlash, 1);
			add(formalFigureBackwardSlash, 2);
			nameLabel.setFont(DEFAULT_FONT);
		}
		nameLabel.setText(this.model.getName());
	}
}
