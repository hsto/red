package dk.dtu.imm.red.visualmodeling.ui.editparts;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

public abstract class VisualSpecificationElementEditPart extends VisualElementEditPart{

	private Adapter adapter;
	
	public VisualSpecificationElementEditPart(RuleCollection ruleCollection)
	{		
		super(ruleCollection);
		
		adapter = new SpecificationElementAdapter();
	}
	
	@Override
	public void activate() {
		if (!isActive()) {
			((IVisualElement) getModel()).eAdapters().add(adapter);
		}
		super.activate();
	}

	@Override
	public void deactivate() {
		if (isActive()) {
			((IVisualElement) getModel()).eAdapters().remove(adapter);
		}

		super.deactivate();
	}
		
	@Override
	public void setModel(Object model) {	
		super.setModel(model);

		// TODO delete or figure out if its needed
		/*
		if(model instanceof VisualSpecificationElement)
		{
			VisualSpecificationElement element = (VisualSpecificationElement)model;
		
			if(element.getSpecificationElement() == null)
			{
				SpecificationElementFactory factory = new SpecificationElementFactory();		
				factory.OpenCreateNewWizard();
								
				element.setSpecificationElement(null); // UNDONE get created specification element and set it in the element
			}
		}
		*/
	}
	
	/*
	 * Inner class for notifications
	 */
	public class SpecificationElementAdapter implements Adapter {

		public void notifyChanged(Notification notification) {

			if (notification.getEventType() == Notification.SET && notification.getFeatureID(VisualSpecificationElement.class) == visualmodelPackage.VISUAL_SPECIFICATION_ELEMENT__SPECIFICATION_ELEMENT) {
				
			}

			refresh();
			
		}

		public Notifier getTarget() {
			return (IVisualElement) getModel();
		}

		public void setTarget(Notifier newTarget) {
			// Do nothing.
		}

		public boolean isAdapterForType(Object type) {
			return type.equals(IVisualElement.class);
		}
	}
}
