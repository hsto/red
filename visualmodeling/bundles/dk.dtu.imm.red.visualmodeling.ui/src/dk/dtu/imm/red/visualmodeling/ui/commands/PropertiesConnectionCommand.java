package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;

public class PropertiesConnectionCommand extends Command {

	private VisualConnection visualConnection;
	private Connection figure;
	private org.eclipse.draw2d.Label connectionLabel;

	private String name;
	private String nameOld;
	
	// Font
	private Color labelColor;
	private Color labelColorOld;
	private Font font;
	private Font fontOld;

	// Line
	private int lineWidth = -1;
	private int lineWidthOld = -1;
	private Color lineColor;
	private Color lineColorOld;

	public PropertiesConnectionCommand(VisualConnection visualConnection, Connection figure, org.eclipse.draw2d.Label connectionLabel) {
		this.visualConnection = visualConnection;
		this.figure = figure;
		this.connectionLabel = connectionLabel;
		
		nameOld = visualConnection.getName();
		labelColorOld = connectionLabel.getForegroundColor();
		fontOld = connectionLabel.getFont();
		if (figure instanceof PolylineConnection) {
			lineWidthOld = ((PolylineConnection)figure).getLineWidth();
		}
		lineColorOld = figure.getForegroundColor();
	}

	@Override
	public void execute() {		
		if (visualConnection != null) {
			if (name != null) visualConnection.setName(name);
		}
		
		if (figure != null) {
			if (lineWidth != -1 && figure instanceof PolylineConnection) {
				((PolylineConnection)figure).setLineWidth(lineWidth);
			}
			if (lineColor != null) figure.setForegroundColor(lineColor); 
		}
		
		if (connectionLabel != null) {
			if (labelColor != null) connectionLabel.setForegroundColor(labelColor);
			if (font != null) connectionLabel.setFont(font);
		}
	}

	@Override
	public void undo() {
		if (visualConnection != null) {
			if (nameOld != null && name != null) visualConnection.setName(nameOld);
		}
		
		if (figure != null) {
			if (lineWidthOld != -1 && figure instanceof PolylineConnection && lineWidth != -1) ((PolylineConnection)figure).setLineWidth(lineWidthOld);
			if (lineColorOld != null) figure.setForegroundColor(lineColorOld);
		}
		
		if (connectionLabel != null) {
			if (labelColorOld != null && labelColor != null) connectionLabel.setForegroundColor(labelColorOld);
			if (fontOld != null && font != null) connectionLabel.setFont(fontOld);
		}
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLabelColor(Color color) {
		this.labelColor = color;
	}
	
	public void setFont(Font font) {
		this.font = font;
	}
	
	public void setLineWidth(int width) {
		this.lineWidth = width;
	}
	
	public void setLineColor(Color color) {
		this.lineColor = color;
	}

}
