package dk.dtu.imm.red.visualmodeling.zestlayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.zest.layouts.LayoutEntity;
import org.eclipse.zest.layouts.LayoutGraph;
import org.eclipse.zest.layouts.LayoutRelationship;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualLayoutGraph implements LayoutGraph{

	Map<IVisualElement,LayoutEntity> visualElToLayoutEntities;
	List<LayoutRelationship> relationships;


	public VisualLayoutGraph() {
		visualElToLayoutEntities = new HashMap<IVisualElement,LayoutEntity>();
		relationships = new ArrayList<LayoutRelationship>();
	}

	@Override
	public void addEntity(LayoutEntity node) {
		if (node instanceof VisualLayoutEntity) {
			visualElToLayoutEntities.put(((VisualLayoutEntity) node).getVisualElement(), node);
		}
	}

	@Override
	public void addRelationship(LayoutRelationship relationship) {
		relationships.add(relationship);
	}

	@Override
	@SuppressWarnings({"rawtypes","unchecked"})
	public List getEntities() {
		return new ArrayList(visualElToLayoutEntities.values());
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getRelationships() {
		return relationships;

	}

	@Override
	@SuppressWarnings("rawtypes")
	public boolean isBidirectional() {
		boolean isBidirectional = true;
		for (Iterator iter = relationships.iterator(); iter.hasNext();) {
			VisualLayoutRelationship rel = (VisualLayoutRelationship) iter.next();
			if (!rel.isBidirectionalInLayout()) {
				isBidirectional = false;
				break;
			}
		}
		return isBidirectional;
	}

	public Map<IVisualElement, LayoutEntity> getVisualElToLayoutEntities() {
		return visualElToLayoutEntities;
	}

	public void printGraph(){
		for(LayoutEntity ent : visualElToLayoutEntities.values()){
			VisualLayoutEntity entity = (VisualLayoutEntity) ent;
			System.out.println(entity.getVisualElement().getName());
			System.out.println((int)entity.getXInLayout() + "," + (int)entity.getYInLayout() + ","
					+ (int)entity.getWidthInLayout() + "," + (int)entity.getHeightInLayout());
		}
		for(LayoutRelationship rel : relationships){
			System.out.println(rel);
		}
	}
}
