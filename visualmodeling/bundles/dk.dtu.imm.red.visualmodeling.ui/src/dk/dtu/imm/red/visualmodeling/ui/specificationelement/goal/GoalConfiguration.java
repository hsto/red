package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualEditorConfigurationBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericPaletteFactory;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;


public class GoalConfiguration extends VisualEditorConfigurationBase{

	public GoalConfiguration()
	{
		this.ruleCollection = new RuleCollection();
		this.ruleCollection.addRule(new GoalRule());

		this.paletteFactory = new GoalPaletteFactory();
		this.paletteFactory.setNext(new GenericPaletteFactory());

		this.editPartFactory = new GoalEditPartFactory(ruleCollection);
		this.editPartFactory.setNext(new GenericEditPartFactory(ruleCollection));
	}
}
