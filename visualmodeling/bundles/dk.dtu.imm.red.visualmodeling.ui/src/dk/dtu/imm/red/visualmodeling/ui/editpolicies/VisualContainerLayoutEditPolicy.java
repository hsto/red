package dk.dtu.imm.red.visualmodeling.ui.editpolicies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.editpolicies.ResizableEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.handles.AbstractHandle;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl;
import dk.dtu.imm.red.visualmodeling.ui.commands.MultipleCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionCreateCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualContainerAddCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementCreateCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementMoveCommand;
import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.CoordinateTranslation;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;

public class VisualContainerLayoutEditPolicy extends XYLayoutEditPolicy {

	private RuleCollection ruleCollection;
	private IFigure activeArea;

	public VisualContainerLayoutEditPolicy(RuleCollection ruleCollection) {
		this.ruleCollection = ruleCollection;
		this.activeArea = null;
	}

	public VisualContainerLayoutEditPolicy(RuleCollection ruleCollection,
			IFigure activeArea) {
		this.ruleCollection = ruleCollection;
		this.activeArea = activeArea;
	}

	@Override
	protected Command createChangeConstraintCommand(EditPart child,
			Object constraint) {

		if (child.getParent() instanceof VisualElementEditPart) {
			VisualElementEditPart parent = (VisualElementEditPart) child.getParent();
			VisualElementFigure figure = (VisualElementFigure) parent.getFigure();

			IFigure activeArea = figure.getActiveArea();
			Point point = ((Rectangle) constraint).getLocation();
			point.translate(activeArea.getBounds().getLocation());

			// If outside the active area we do now allow the drop
			if (activeArea != null && !activeArea.containsPoint(point)) {
				return null;
			}
		}

		VisualElementMoveCommand command = new VisualElementMoveCommand(
				(IVisualElement) child.getModel(), (Rectangle) constraint);

		return command;
	}

	@Override
	@SuppressWarnings("unchecked")
	protected Command getCreateCommand(CreateRequest request) {
		System.out.println("VisualContainerLayoutEditPolicy.getCreateCommand()");
		List<Command> commands = new ArrayList<Command>();
		List<Object> objects = new ArrayList<Object>();
		
		if (request.getNewObject() instanceof List) {
			objects = (List<Object>) request.getNewObject();
		} else {
			objects.add(request.getNewObject());
		}

		int locationIndex = 0;
		for (Object obj : objects) {
			if (IVisualElement.class.isAssignableFrom(obj.getClass())) {
				Command command = getCreateVisualElementCommand(request, obj, locationIndex);
				commands.add(command);
				locationIndex++;
			}
		}
		
		for (Object obj : objects) { // Relations have to be added last
			if (IVisualConnection.class.isAssignableFrom(obj.getClass())) {
				Command command = getCreateVisualConnectionCommand(request, obj, commands);
				commands.add(command);
			}
		}
		
		return new MultipleCommand(commands);
	}
	
	private Command getCreateVisualElementCommand(CreateRequest request, Object newObject, int locationIndex) {
		IVisualElement parent = (IVisualElement) getHost().getModel();
		VisualDiagram diagram = parent.getDiagram();
		IVisualElement child = (IVisualElement) newObject;

		Point createLocation = request.getLocation().getCopy();
		// Translates the absolute mouseclick location to the models relative position
		this.getHostFigure().translateToRelative(createLocation);

		// Offset the model parents nesting
		Point parentLocation = CoordinateTranslation.getAbsoluteLocation(parent);
		createLocation.translate(parentLocation.negate());
		
		// For adding more elements
		int spaceX = 120, spaceY = 80, rowLength = 4;
		createLocation.x += (locationIndex % 4) * spaceX;
		createLocation.y += (locationIndex / rowLength) *spaceY;
		

		VisualElementCreateCommand command = new VisualElementCreateCommand(diagram, parent, child, createLocation);
		return command;
	}
	
	private Command getCreateVisualConnectionCommand(CreateRequest request, Object newObject, List<Command> draggedCommands) {
		VisualConnection child = (VisualConnection) newObject;
		ConnectorImpl actorRelation = (ConnectorImpl) child.getSpecificationElement();
		if (actorRelation.getSource().isEmpty() || actorRelation.getTarget().isEmpty()) {
			MessageDialog md = new MessageDialog(Display
					.getCurrent().getActiveShell(), "Missing!", null,
					 "Dragged Actor Relation missing either source element or target element.",
					MessageDialog.INFORMATION,
					new String[] { "ok" }, 0);
			md.open();
			return null;
		}
		
		
		IVisualElement source = null, target = null;
		List<IVisualElement> sources = new ArrayList<IVisualElement>();
		List<IVisualElement> targets = new ArrayList<IVisualElement>();
		IVisualElement parent = (IVisualElement) getHost().getModel();
		for (IVisualElement e : parent.getElements()) { // Find all possible sources and targets
			
			if (e instanceof VisualSpecificationElementImpl) {
				VisualSpecificationElementImpl visualElement = (VisualSpecificationElementImpl)e;
				
				if (actorRelation.getSource().get(0).equals(visualElement.getSpecificationElement())) {
					sources.add(visualElement);
				}
				if (actorRelation.getTarget().get(0).equals(visualElement.getSpecificationElement())) {
					targets.add(visualElement);
				}					
			}
		}
		// Adds visual elements from the current drag selection
		IVisualElement temp = getConnectionSourceOrTargetInDraggedCommands(actorRelation.getSource().get(0),draggedCommands);
		if (temp != null) sources.add(temp);
		temp = getConnectionSourceOrTargetInDraggedCommands(actorRelation.getTarget().get(0),draggedCommands);
		if (temp != null) targets.add(temp);
		
		if (!sources.isEmpty()) source = sources.get(0); // Sets default values
		if (!targets.isEmpty()) target = targets.get(0);
		for (IVisualConnection e : parent.getDiagram().getDiagramConnections()) { // Filter already connected sources and targets
			if (e instanceof VisualConnection) {
				VisualConnection con = (VisualConnection) e;
				if (sources.contains(con.getSource())) sources.remove(con.getSource());
				if (targets.contains(con.getTarget())) targets.remove(con.getTarget());
			}
		}
		if (!sources.isEmpty()) source = sources.get(0);
		if (!targets.isEmpty()) target = targets.get(0);
		
		
		if (source == null || target == null) {
			MessageDialog md = new MessageDialog(Display
					.getCurrent().getActiveShell(), "Missing!", null,
					"Dragged Actor Relation missing either source element or  target element on the editor.",
					MessageDialog.INFORMATION,
					new String[] { "ok" }, 0);				
			md.open();
			return null;
		}
		
		VisualConnectionCreateCommand command = new VisualConnectionCreateCommand();
		
		VisualDiagram diagram = (VisualDiagram) getHost().getRoot().getContents().getModel();
		VisualConnection connection = child;
		// Link it
		connection.setSpecificationElement(actorRelation);
		connection.setName(actorRelation.getName());
		
		command.setDiagram(diagram);
		command.setSource(source);
		command.setTarget(target);
		command.setConnection(connection);
		command.setLabel(actorRelation.getLabel() + " - " + actorRelation.getName());
		return command;
	}

	/**
	 * Called when adding an element to another element residing in the root
	 * diagram.
	 */
	@Override
	protected Command createAddCommand(ChangeBoundsRequest request, EditPart child, Object constraint)
	{
		System.out.println("VisualContainerLayoutEditPolicy.createAddCommand()");

		List<IVisualElement> elements = new ArrayList<IVisualElement>();

		for (Object obj : request.getEditParts()) {
			EditPart part = (EditPart) obj;
			elements.add((IVisualElement) part.getModel());
		}

		IVisualElement newParent = (IVisualElement) getHost().getModel();

		for (IVisualElement childElement : elements) {
			if (!ruleCollection.canAccept(newParent, childElement)) {
				return null;
			}
		}

		// Translates the absolute mouseclick location to the models relative position
		Point dropLocation = request.getLocation().getCopy();
		this.getHostFigure().translateToRelative(dropLocation);

		// If outside the active area we do now allow the drop
		if (activeArea != null
				&& !activeArea.containsPoint(dropLocation)) {
			return null;
		}

		// Translate the move delta to models relative position
		Point offset = request.getMoveDelta().getCopy();
		double zoom = ((ScalableFreeformRootEditPart)this.getHost().getRoot()).getZoomManager().getZoom();
		offset.scale(1.0/zoom);

		Point[] newLocations = new Point[elements.size()];
		for (int i = 0; i < elements.size(); ++i) {

			Point f = CoordinateTranslation.getAbsoluteLocation(elements.get(i));
			f.translate(offset);
			newLocations[i] = CoordinateTranslation.getRelativeLocation((IVisualElement)getHost().getModel(), f);
		}

		VisualContainerAddCommand command = new VisualContainerAddCommand(elements, newParent, newLocations);

		return command;
	}

	@Override
	protected EditPolicy createChildEditPolicy(EditPart child) {
		//customize the bounding box of the selected element to red color
		return new ResizableEditPolicy() {
			@Override
			@SuppressWarnings("rawtypes")
			protected List createSelectionHandles() {
				List l = super.createSelectionHandles();
				for (Object obj : l) {
					if (obj instanceof AbstractHandle) {
						AbstractHandle handle = (AbstractHandle) obj;
						Border border = handle.getBorder();
						if (border instanceof LineBorder) {
							LineBorder lborder = (LineBorder) border;
							lborder.setColor(ColorConstants.red);
						}
					}
				}

				return l;
			}
		};
	}
	
	private IVisualElement getConnectionSourceOrTargetInDraggedCommands(SpecificationElement source , List<Command> draggedCommands) {
		for (Command command : draggedCommands) {
			if (command instanceof VisualElementCreateCommand) {
				VisualElementCreateCommand cmd = (VisualElementCreateCommand) command;
				if (cmd.getElement() instanceof VisualSpecificationElement) {
					VisualSpecificationElement visualElement = (VisualSpecificationElement) cmd.getElement();
					if (visualElement.getSpecificationElement() != null &&  source.equals(visualElement.getSpecificationElement())) {
						return visualElement;
					}					
				}
			}
		}
		return null;
	}
}
