package dk.dtu.imm.red.visualmodeling.ui;

import java.util.HashMap;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

public interface TypeMapping {

	public HashMap<Class<? extends SpecificationElement>, Class<? extends CreationFactory>> getDragDropMapping();
	
}
