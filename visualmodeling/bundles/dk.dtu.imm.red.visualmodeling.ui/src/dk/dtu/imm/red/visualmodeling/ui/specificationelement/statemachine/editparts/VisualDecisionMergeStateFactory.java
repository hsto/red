package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualDecisionMergeStateFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualDecisionMergeNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDecisionMergeState;

public class VisualDecisionMergeStateFactory implements CreationFactory {
	public Object getNewObject() {

		VisualDecisionMergeState element = StatemachineFactory.eINSTANCE.
				createVisualDecisionMergeState();
		element.setBounds(VisualDecisionMergeStateFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualDecisionMergeNode.class;
	}
}
