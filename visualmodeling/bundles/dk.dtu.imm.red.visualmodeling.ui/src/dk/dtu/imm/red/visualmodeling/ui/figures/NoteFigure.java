package dk.dtu.imm.red.visualmodeling.ui.figures;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;



public class NoteFigure extends VisualElementFigure {

	  private RectangleFigure rectangle;

	  public NoteFigure(IVisualElement model) {
		super(model);
		setLayoutManager(new XYLayout());

		rectangle = new RectangleFigure();
	    add(rectangle);
	    nameLabel = new Label();
	    add(nameLabel);

	    connectionAnchor = new ChopboxAnchor(this);
	  }


	  @Override
	  protected void paintFigure(Graphics graphics) {

		  Rectangle r = getBounds().getCopy();
		  setConstraint(rectangle, new Rectangle(0, 0, r.width, r.height));
		  setConstraint(nameLabel, new Rectangle(0, 0, r.width, r.height));

		  graphics.setLineStyle(SWT.LINE_DOT);
		  graphics.drawLine(0,-10,10,0);
	  }

}
