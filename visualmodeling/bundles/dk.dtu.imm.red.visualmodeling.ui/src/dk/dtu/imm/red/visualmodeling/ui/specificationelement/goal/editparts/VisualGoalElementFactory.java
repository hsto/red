package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.figures.VisualGoalElementFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalElement;

public class VisualGoalElementFactory implements CreationFactory {

	public Object getNewObject() {

		VisualGoalElement element = GoalFactory.eINSTANCE.createVisualGoalElement();
		element.setBounds(VisualGoalElementFigure.initialBounds);
	    return element;
	  }


	  public Object getObjectType() {
	    return VisualGoalElement.class;
	  }
}
