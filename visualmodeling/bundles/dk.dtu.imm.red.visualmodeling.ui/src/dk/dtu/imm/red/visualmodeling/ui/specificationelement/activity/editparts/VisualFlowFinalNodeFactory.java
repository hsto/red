package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualFlowFinalNodeFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualFlowFinalNode;

public class VisualFlowFinalNodeFactory implements CreationFactory {
	public Object getNewObject() {

		VisualFlowFinalNode element = ActivityFactory.eINSTANCE.
				createVisualFlowFinalNode();
		element.setBounds(VisualFlowFinalNodeFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualFlowFinalNode.class;
	}
}
