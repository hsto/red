package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.figures.VisualSystemFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructureFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;

public class VisualSystemFactory implements CreationFactory {
	public Object getNewObject() {

		VisualSystem element = SystemstructureFactory.eINSTANCE.createVisualSystem();
		element.setBounds(VisualSystemFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualSystem.class;
	}
}
