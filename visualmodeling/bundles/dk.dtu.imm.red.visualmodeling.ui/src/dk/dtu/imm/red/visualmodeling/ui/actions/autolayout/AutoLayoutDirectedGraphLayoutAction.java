package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.ui.IEditorPart;

public class AutoLayoutDirectedGraphLayoutAction extends AutoLayoutAction {

    public AutoLayoutDirectedGraphLayoutAction(IEditorPart editor) {
		super(editor, LayoutAlgorithmType.DIRECTED_GRAPH_LAYOUT);
	}

}
