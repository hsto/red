package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualEditorConfigurationBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericPaletteFactory;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;

public class ClassConfiguration extends VisualEditorConfigurationBase{

	public ClassConfiguration()
	{
		this.ruleCollection = new RuleCollection();
		this.ruleCollection.addRule(new ClassRule());

		this.paletteFactory = new ClassPaletteFactory();
		this.paletteFactory.setNext(new GenericPaletteFactory());

		this.editPartFactory = new ClassEditPartFactory(ruleCollection);
		this.editPartFactory.setNext(new GenericEditPartFactory(ruleCollection));

		this.contextMenuProvider = null;
	}
}
