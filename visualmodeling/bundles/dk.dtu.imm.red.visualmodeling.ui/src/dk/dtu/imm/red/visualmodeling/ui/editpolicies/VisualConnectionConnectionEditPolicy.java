package dk.dtu.imm.red.visualmodeling.ui.editpolicies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ConnectionEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionDeleteCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;

public class VisualConnectionConnectionEditPolicy extends ConnectionEditPolicy{

	@Override
	protected Command getDeleteCommand(GroupRequest request) {		
		VisualConnectionDeleteCommand command = new VisualConnectionDeleteCommand((IVisualConnection) getHost().getModel());			    			
		return command;
	}
	
}
