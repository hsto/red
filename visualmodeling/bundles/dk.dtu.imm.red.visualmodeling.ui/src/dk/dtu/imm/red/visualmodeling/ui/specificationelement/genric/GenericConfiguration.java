package dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualEditorConfigurationBase;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;

public class GenericConfiguration extends VisualEditorConfigurationBase{

	public GenericConfiguration()
	{
		this.ruleCollection = new RuleCollection();
		this.paletteFactory = new GenericPaletteFactory();
		this.editPartFactory = new GenericEditPartFactory(ruleCollection);
	}
	
}
