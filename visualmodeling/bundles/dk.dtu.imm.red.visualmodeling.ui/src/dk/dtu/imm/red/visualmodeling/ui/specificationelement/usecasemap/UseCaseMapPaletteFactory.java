package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecasemap;

import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;

import dk.dtu.imm.red.visualmodeling.ui.directedit.CreationAndDirectEditTool;
import dk.dtu.imm.red.visualmodeling.ui.factories.DiagramConnectionFactory;
import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualActorElementFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualSystemBoundaryElementFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;

public class UseCaseMapPaletteFactory extends PaletteFactoryBase {

	@Override
	public void populatePaletteInternal(PaletteRoot root) {

		PaletteDrawer group = new PaletteDrawer("Use Case");

		CreationToolEntry entry;

		entry = new CreationToolEntry("Use Case", "Create a new use case",
				new VisualUseCaseMapElementFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Actor", "Create a new actor",
				new VisualActorElementFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("System Boundary",
				"Create a new system boundary",
				new VisualSystemBoundaryElementFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		group.add(new PaletteSeparator());

		ConnectionCreationToolEntry connectionEntry = new ConnectionCreationToolEntry(
				"Association", "Creates a new Association connection",
				new DiagramConnectionFactory(), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry("Extend",
				"Creates a new extend connection",
				new DiagramConnectionFactory("Extend", "Extend",
						ConnectionLineStyle.DASHED, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry("Include",
				"Creates a new include connection",
				new DiagramConnectionFactory("Include", "Include",
						ConnectionLineStyle.DASHED, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		root.add(group);
	}
}
