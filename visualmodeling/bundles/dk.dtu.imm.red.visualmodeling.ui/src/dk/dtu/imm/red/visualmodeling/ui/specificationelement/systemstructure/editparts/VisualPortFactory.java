package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.figures.VisualPortFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructureFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;

public class VisualPortFactory implements CreationFactory {
	public Object getNewObject() {

		VisualPort element = SystemstructureFactory.eINSTANCE.
				createVisualPort();
		element.setBounds(VisualPortFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualPort.class;
	}
}
