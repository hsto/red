package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

public class VisualConnectionDeleteCommand extends Command {
	  
	  private IVisualElement source;
	  private IVisualElement target;
	  private IVisualConnection connection;
	  private VisualDiagram diagram;
	 
	  public VisualConnectionDeleteCommand(IVisualConnection connection) {
		  this.connection = connection;
		  this.source = connection.getSource();
		  this.target = connection.getTarget();
		  if (this.source != null)
			  diagram = this.source.getDiagram();
	  }
	  
	  @Override
	  public boolean canExecute() {
	    return connection != null;
	  }
	   
	  @Override public void execute() {
		System.out.println("VisualConnectionDeleteCommand");
		 if (source != null) source.getConnections().remove(connection);
		 if (target != null) target.getConnections().remove(connection);
		 if (diagram != null) diagram.getDiagramConnections().remove(connection);
	  }
	 
	  @Override public void undo() {
		  if (source != null) source.getConnections().add(connection);
		  if (target != null) target.getConnections().add(connection);
		  if (diagram != null) diagram.getDiagramConnections().add(connection);
	  }
	 
	  public void setTarget(VisualElement target) {
	    this.target = target;
	  }
	   
	  public void setSource(VisualElement source) {
	    this.source = source;
	  }
	   
	  public void setConnection(VisualConnection connection) {
	    this.connection = connection;
	  }
	   
	  public void setDiagram(VisualDiagram diagram) {
	    this.diagram = diagram;
	  }
}
