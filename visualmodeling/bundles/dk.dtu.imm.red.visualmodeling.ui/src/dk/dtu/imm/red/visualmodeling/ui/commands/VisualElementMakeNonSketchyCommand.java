package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualElementMakeNonSketchyCommand extends Command {

	private boolean[] isSketchyArr;
	private IVisualElement[] elements;

	public VisualElementMakeNonSketchyCommand(IVisualElement[] elements) {
		this.elements = elements;
		isSketchyArr = new boolean[elements.length];
		for(int i=0;i<elements.length;i++){
			isSketchyArr[i]=elements[i].isIsSketchy();
		}
	}

	@Override
	public boolean canExecute() {
		return elements != null && elements.length>0;
	}

	@Override
	public void execute() {
		for(int i=0;i<elements.length;i++){
			elements[i].setIsSketchy(false);
		}
	}

	@Override
	public void undo() {
		for(int i=0;i<elements.length;i++){
			elements[i].setIsSketchy(isSketchyArr[i]);
		}
	}

}
