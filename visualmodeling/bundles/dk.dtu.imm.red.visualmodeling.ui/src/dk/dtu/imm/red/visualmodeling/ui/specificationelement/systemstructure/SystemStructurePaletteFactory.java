package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure;

import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;

import dk.dtu.imm.red.visualmodeling.ui.directedit.CreationAndDirectEditTool;
import dk.dtu.imm.red.visualmodeling.ui.factories.DiagramConnectionFactory;
import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualPortFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemStructureActorFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind;

public class SystemStructurePaletteFactory extends PaletteFactoryBase{

	@Override
	public void populatePaletteInternal(PaletteRoot root) {
		PaletteDrawer group = new PaletteDrawer("Activity");

		CreationToolEntry entry;
		entry = new CreationToolEntry("Person", "Create a new person",
				new VisualSystemStructureActorFactory(SystemStructureActorKind.PERSON), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Role", "Create a new role",
				new VisualSystemStructureActorFactory(SystemStructureActorKind.ROLE), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Group", "Create a new group",
				new VisualSystemStructureActorFactory(SystemStructureActorKind.GROUP), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Team", "Create a new team",
				new VisualSystemStructureActorFactory(SystemStructureActorKind.TEAM), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Organisation", "Create a new organisation",
				new VisualSystemStructureActorFactory(SystemStructureActorKind.ORGANISATION), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("System", "Create a new system",
				new VisualSystemFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Port", "Create a new port",
				new VisualPortFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);
		group.add(new PaletteSeparator());

		ConnectionCreationToolEntry connectionEntry = new ConnectionCreationToolEntry(
				"Association", "Creates a new association connection",
				new DiagramConnectionFactory(ConnectionType.ASSOCIATION.getName(),
						"",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.NONE,
						ConnectionDirection.BIDIRECTIONAL), null, null);
		group.add(connectionEntry);

		root.add(group);
	}
}
