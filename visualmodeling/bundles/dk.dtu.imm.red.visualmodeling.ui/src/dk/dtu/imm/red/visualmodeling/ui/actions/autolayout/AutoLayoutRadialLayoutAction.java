package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.ui.IEditorPart;

public class AutoLayoutRadialLayoutAction extends AutoLayoutAction {

    public AutoLayoutRadialLayoutAction(IEditorPart editor) {
		super(editor, LayoutAlgorithmType.RADIAL_LAYOUT);
	}

}
