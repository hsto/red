package dk.dtu.imm.red.visualmodeling.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.impl.FileImpl;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.impl.WorkspaceFactoryImpl;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationFactory;
import dk.dtu.imm.red.specificationelements.configuration.ConfigurationPackage;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionLinkCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualSpecificationElementLinkCommand;
import dk.dtu.imm.red.visualmodeling.ui.editors.impl.VisualModelEditorImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;

/**
 * 
 * @author Luai
 *
 */

public class CreateElementsFromDiagram extends AbstractHandler {
	
	private VisualDiagram diagram;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		if (HandlerUtil.getActiveEditor(event) instanceof VisualModelEditorImpl) {
			VisualModelEditorImpl editor = (VisualModelEditorImpl) HandlerUtil.getActiveEditor(event);	
			diagram = editor.getVisualEditor().getDiagram().getVisualDiagram();
			Configuration configuration = null;
			
			if (diagram.getDiagramType() == DiagramType.SYSTEM_STRUCTURE) {
				configuration = getDiagramConfigurationElement();
			}
			
			EList<IVisualElement> diagramElements = diagram.getElements();
			EList<IVisualConnection> diagramConnections = diagram.getDiagramConnections();
			
			for (IVisualElement e : diagramElements) { // Link or create elements
				processElementImpl(e);
				
				if (configuration != null) {
					addElementToConfiguration(e, configuration);
				}
			}
			for (IVisualConnection con : diagramConnections) { // Link or create connections
				processConnectionImpl(con);
				
				if (configuration != null) {
					addConnectionToConfiguration(con, configuration);
				}
			}
			
		} else {
			MessageDialog md = new MessageDialog(Display
					.getCurrent().getActiveShell(), "Diagram not found", null,
					"A diagram has to be open!",
					MessageDialog.INFORMATION,
					new String[] { "ok" }, 0);			
			md.open();
		}
		
		return null;
	}
	
	private Configuration getDiagramConfigurationElement() {
		// Look for existing configuration
		Workspace workspace = WorkspaceFactoryImpl.eINSTANCE.getWorkspaceInstance();
		for (Element e : workspace.getContents()) {
			if (e instanceof FileImpl) {
				for (Element element : ((FileImpl) e).getContents()) {
					
					if (element instanceof Configuration) { 
						if (element.getName() != null && diagram.getName() != null && 
								element.getName().equals(diagram.getName())) {
							return (Configuration) element;
						}
					}	
				}
			}
		}
		
		// Create new configuration
		Configuration configuration = ConfigurationFactory.eINSTANCE.createConfiguration();
		configuration.setCreator(PreferenceUtil.getUserPreference_User());
		
		configuration.setLabel(diagram.getName());
		configuration.setName(diagram.getName());
		configuration.setDescription("");
		configuration.setElementKind(SystemStructureDiagramKind.SYSTEM_CONTEXT.getLiteral());
		
		configuration.setParent(getFirstFile(workspace));
		
		configuration.save();
		
		return configuration;
	}
	
	private void processElementImpl(IVisualElement diagramElement) {
		// Recursive call on child elements
		for (IVisualElement	 e : diagramElement.getElements()) { 
			processElementImpl(e);
		}
		
		if (!(diagramElement instanceof VisualSpecificationElementImpl)) {
			return;
		}
		
		VisualSpecificationElementImpl visualElement = (VisualSpecificationElementImpl) diagramElement;
		if (visualElement.getSpecificationElement() != null) { // Is already linked
			
			processInternals(visualElement);
			
			return;
		}
		
		
		/**
		 * Link unlinked diagram elements with specification element with the same name.
		 */
		Workspace workspace = WorkspaceFactoryImpl.eINSTANCE.getWorkspaceInstance();
		for (Element e : workspace.getContents()) {
			if (e instanceof FileImpl) {
				for (Element element : ((FileImpl) e).getContents()) {
					
					if (visualElement.getSpecificationElementType().isAssignableFrom(element.getClass())) { // Checks the corresponding Specification element
						if (element.getName() != null && diagramElement.getName() != null && 
								element.getName().equals(diagramElement.getName())) {
							linkElementCommand(visualElement, (SpecificationElement) element);
							
							processInternals(visualElement);
							
							return;
						}
					}	
				}
			}
		}
		
		/**
		 * Create new specification element
		 */
		// Factory call
		Group parent = getFirstFile(workspace);
		SpecificationElement se = visualElement.createSpecificationElement(parent);
		if (se != null) {
			linkElementCommand(visualElement, se);			
		}
		
		processInternals(visualElement);
	}
	
	private void processInternals(VisualSpecificationElement visualElement) {
		if (visualElement instanceof VisualSystem) {
			((VisualSystem) visualElement).updateInternals();
		}
	}
	
	private void addElementToConfiguration(IVisualElement element, Configuration configuration) {
		if (element instanceof VisualSpecificationElement) {
			SpecificationElement specificationElement = ((VisualSpecificationElement) element).getSpecificationElement();
			
			if (specificationElement instanceof Actor) {
				if (!configuration.getParts().contains(specificationElement)) {
					Actor actor = (Actor) specificationElement;
					
					configuration.getParts().add(actor);
					
					DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
					listener.setHostingObject(configuration);
					listener.setHostingFeature(ConfigurationPackage.eINSTANCE.getConfiguration_Parts());
					listener.setHandledObject(actor);
					actor.getDeletionListeners().add(listener);
				}
			}
		}
	}
	
	private void processConnectionImpl(IVisualConnection diagramConnection) {
		VisualConnectionImpl visualConnectionImpl = (VisualConnectionImpl)diagramConnection;
		
		if (isGlueConnector(visualConnectionImpl)) {
			return;
		}
		
		if (visualConnectionImpl.getSpecificationElement() != null) { // Is already linked
			return;
		}
		
		VisualSpecificationElementImpl sourceSE = null, targetSE = null;
		IVisualElement sourceElement = diagramConnection.getSource();
		IVisualElement targetElement = diagramConnection.getTarget();
		if (sourceElement != null) sourceSE = (VisualSpecificationElementImpl)sourceElement;
		if (targetElement != null) targetSE = (VisualSpecificationElementImpl)targetElement;
		if (sourceSE == null || sourceSE.getSpecificationElement() == null || targetSE == null || targetSE.getSpecificationElement() == null) return;
		
		if ( !(sourceSE.getSpecificationElement() instanceof Actor) || !(targetSE.getSpecificationElement() instanceof Actor) ) return;
		
		
		Workspace workspace = WorkspaceFactoryImpl.eINSTANCE.getWorkspaceInstance();
		for (Element e : workspace.getContents()) {
			if (e instanceof FileImpl) {
				for (Element element : ((FileImpl) e).getContents()) {
					
					if (element instanceof Connector) {
						ConnectorImpl actorRelation = (ConnectorImpl) element;
						if ( (actorRelation.getSource().get(0).equals(sourceSE.getSpecificationElement()) &&
								actorRelation.getTarget().get(0).equals(targetSE.getSpecificationElement()) )
								||
								(actorRelation.getSource().get(0).equals(targetSE.getSpecificationElement()) &&
										actorRelation.getTarget().get(0).equals(sourceSE.getSpecificationElement()) ) ) { // Relation already exists
							linkConnectionCommand(visualConnectionImpl, (SpecificationElement)element);
							return;
						}
					}
					
				}
			}
		}
		
		/**
		 *  Create Connection
		 */	
		Group parent = getFirstFile(workspace);
		SpecificationElement relation = visualConnectionImpl.createSpecificationConnection(parent, sourceSE.getSpecificationElement(), targetSE.getSpecificationElement());
		if (relation != null) {
			linkConnectionCommand(visualConnectionImpl, relation);
		}
		
	}
	
	private boolean isGlueConnector(VisualConnection connection) {
		if (getConnectionContext(connection) instanceof VisualSystem) {
			return ((VisualSystem) getConnectionContext(connection)).isGlueConnector(connection);
		}
		else {
			return false;
		}
	}
	
	private void addConnectionToConfiguration(IVisualConnection connection, Configuration configuration) {
		if (connection instanceof VisualConnection
				&& connection.getSource() instanceof VisualSpecificationElement
				&& connection.getTarget() instanceof VisualSpecificationElement) {
			
			VisualConnection visualConnection = (VisualConnection) connection;
			IVisualElement connectionContext = getConnectionContext(visualConnection); 
			
			if (connectionContext == null) {
				return;
			}
			else if (connectionContext == diagram) {
				SpecificationElement specificationElement = visualConnection.getSpecificationElement();
				
				if (specificationElement instanceof Connector
						&& !configuration.getInternalConnectors().contains(specificationElement)) {
					Connector connector = (Connector) specificationElement;
					
					configuration.getInternalConnectors().add(connector);
					
					DeletionListener listener = ElementFactory.eINSTANCE.createDeletionListener();
					listener.setHostingObject(configuration);
					listener.setHostingFeature(ConfigurationPackage.eINSTANCE.getConfiguration_InternalConnectors());
					listener.setHandledObject(connector);
					connector.getDeletionListeners().add(listener);
				}
			}
			else if (connectionContext instanceof VisualSystem) {
				((VisualSystem) connectionContext).handleInternalConnection(visualConnection);
			}
		}
	}
	
	private IVisualElement getConnectionContext(IVisualConnection connection) {
		IVisualElement source = connection.getSource();
		IVisualElement target = connection.getTarget();
		
		if (source.getParent() == target.getParent()) {
			return connection.getSource().getParent();
		}
		else if (source instanceof VisualPort
				&& source.getParent().getParent() != null 
				&& source.getParent().getParent() == target.getParent()) {
			return target.getParent();
		}
		else if (target instanceof VisualPort
				&& target.getParent().getParent() != null 
				&& target.getParent().getParent() == source.getParent()) {
			return source.getParent();
		}
		else if (source instanceof VisualPort && target instanceof VisualPort
				&& source.getParent().getParent() != null 
				&& target.getParent().getParent() != null 
				&& source.getParent().getParent() == target.getParent().getParent()) {
			return source.getParent().getParent();
		}
		else {
			return null;
		}
	}

	private void linkElementCommand(VisualSpecificationElement visualElement, SpecificationElement specificationElement) {
		Command command = new VisualSpecificationElementLinkCommand(visualElement, specificationElement);
		if (command.canExecute()) command.execute();
	}
	
	private void linkConnectionCommand(VisualConnection connection,	SpecificationElement specificationElement) {
		Command command = new VisualConnectionLinkCommand(connection, specificationElement);
		if (command.canExecute()) command.execute();
	}
	
	/**
	 * Gets the first file of the project
	 * @return
	 */
	private Group getFirstFile(Workspace workspace) {
		Group parent = null;
		for (Element e : workspace.getContents()) {
			if (e instanceof FileImpl) {
				parent = (Group) e;
				break;
			}
		}
		return parent;		
	}
}
