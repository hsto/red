package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity;

import org.eclipse.gef.EditPart;
import org.eclipse.ui.part.EditorPart;

import dk.dtu.imm.red.visualmodeling.ui.factories.EditPartFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualActionNodeEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualActivityFinalNodeEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualDecisionMergeNodeEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualFlowFinalNodeEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualForkJoinNodeEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualInitialNodeEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualObjectNodeEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualReceiveSignalNodeEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualSendSignalNodeEditPart;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActionNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActivityFinalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualDecisionMergeNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualFlowFinalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualForkJoinNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualInitialNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualReceiveSignalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualSendSignalNode;

public class ActivityEditPartFactory extends EditPartFactoryBase {

	RuleCollection ruleCollection;

	public ActivityEditPartFactory(RuleCollection ruleCollection) {
		super(ruleCollection);
		this.ruleCollection = ruleCollection;
	}

	public EditPart createEditPartInternal(EditPart context, Object model) {
		EditPart part = null;

		if (model instanceof VisualActionNode) {
			part = new VisualActionNodeEditPart(ruleCollection);
		}
		else if (model instanceof VisualActivityFinalNode) {
			part = new VisualActivityFinalNodeEditPart(ruleCollection);
		}
		else if (model instanceof VisualDecisionMergeNode) {
			part = new VisualDecisionMergeNodeEditPart(ruleCollection);
		}
		else if (model instanceof VisualFlowFinalNode) {
			part = new VisualFlowFinalNodeEditPart(ruleCollection);
		}
		else if (model instanceof VisualForkJoinNode) {
			part = new VisualForkJoinNodeEditPart(ruleCollection);
		}
		else if (model instanceof VisualInitialNode) {
			part = new VisualInitialNodeEditPart(ruleCollection);
		}
		else if (model instanceof VisualObjectNode) {
			part = new VisualObjectNodeEditPart(ruleCollection);
		}
		else if (model instanceof VisualReceiveSignalNode) {
			part = new VisualReceiveSignalNodeEditPart(ruleCollection);
		}
		else if (model instanceof VisualSendSignalNode) {
			part = new VisualSendSignalNodeEditPart(ruleCollection);
		}


		return part;
	}
}
