package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualReceiveSignalNodeFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualReceiveSignalNode;

public class VisualReceiveSignalNodeFactory implements CreationFactory {
	public Object getNewObject() {

		VisualReceiveSignalNode element = ActivityFactory.eINSTANCE
				.createVisualReceiveSignalNode();
		element.setBounds(VisualReceiveSignalNodeFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualReceiveSignalNode.class;
	}
}
