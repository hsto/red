package dk.dtu.imm.red.visualmodeling.ui.factories;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

/*
 * Proxy creation factory that sets a given specification element before returning a new object.
 * */
public class VisualSpecificationElementCreationFactory implements CreationFactory {

	private SpecificationElement specificationElement;
	private CreationFactory creationFactory;
	
	public VisualSpecificationElementCreationFactory(CreationFactory creationFactory, SpecificationElement specificationElement)
	{				
		this.specificationElement = specificationElement;
		this.creationFactory = creationFactory;
	}
	
	@Override
	public Object getNewObject() {	
		VisualSpecificationElement element = (VisualSpecificationElement)creationFactory.getNewObject();
		element.setSpecificationElement(specificationElement);
		return element;
	}

	@Override
	public Object getObjectType() {	
		return creationFactory.getObjectType();
	}
}
