package dk.dtu.imm.red.visualmodeling.ui.editors;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.visualmodeling.ui.editors.impl.NewDiagramWizardImpl;

/**
 * 
 * Creates a new diagram element.
 *
 */
public class CreateDiagramHandler extends AbstractHandler {
	
	
	public Object execute(ExecutionEvent event) throws ExecutionException {		
		 
		NewDiagramWizardImpl wizard = new NewDiagramWizardImpl();
				
		ISelection currentSelection = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection();
		
		wizard.init(PlatformUI.getWorkbench(), (IStructuredSelection) currentSelection);

		WizardDialog dialog = new WizardDialog(HandlerUtil.getActiveShell(event), wizard);
		dialog.create();
		
		dialog.open();
		
		return null;
	}
}
