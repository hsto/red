package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.ui.PropertiesConnectionDialog;
import dk.dtu.imm.red.visualmodeling.ui.PropertiesElementDialog;
import dk.dtu.imm.red.visualmodeling.ui.commands.MultipleCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * 
 * @author Luai
 *
 */
public class ElementPropertiesAction extends SelectionAction {
	
	public static final String ELEMENT_PROPERTIES = "ElementPropertiesAction";
	public static final String REQ_ELEMENT_PROPERTIES = "ElementPropertiesAction";
	
	Request request;
	
	public ElementPropertiesAction(IWorkbenchPart part) {
		super(part);
		setId(ELEMENT_PROPERTIES);
		setText("Element properties");
		request = new Request(REQ_ELEMENT_PROPERTIES);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void run() {
		
		Display display = Display.getDefault();
	    Shell shell = display.getActiveShell();
		
	    EditPart selectedEditPart = getSelectedEditPart();
	    
		Object selectedElement = getSelectedElement(selectedEditPart);
		if (selectedElement instanceof IVisualElement) {
			IVisualElement visualElement  = (IVisualElement) selectedElement;
			PropertiesElementDialog dialog = new PropertiesElementDialog(shell, SWT.APPLICATION_MODAL, visualElement, selectedEditPart);
			Object result = dialog.open();
			if(result != null && result instanceof List)
			{
				List<Command> commands = (List<Command>) result;
				if (!commands.isEmpty()) {
					this.getCommandStack().execute(new MultipleCommand(commands));
					selectedEditPart.refresh();
				}
			}
		} 
		else if (selectedElement instanceof VisualConnection) {
			VisualConnection visualConnection = (VisualConnection) selectedElement;
			PropertiesConnectionDialog dialog = new PropertiesConnectionDialog(shell, SWT.APPLICATION_MODAL, visualConnection, selectedEditPart);
			Object result = dialog.open();
			if(result != null)
			{
				List<Command> commands = (List<Command>) result;
				if (!commands.isEmpty()) {
					this.getCommandStack().execute(new MultipleCommand(commands));
//					selectedEditPart.refresh();
				}
				
			}
		}
		
		
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	protected boolean calculateEnabled() {
		List selectionList = getSelectedObjects();

    	if(selectionList.size() != 1) // Only allow 1 element to be selected for simplicity
    		return false;

    	Object selection = ((EditPart)selectionList.get(0)).getModel();

    	if(!(selection instanceof VisualElement) && !(selection instanceof VisualConnection))
	    	return false;

        return true;
	}
	
	private Object getSelectedElement(EditPart editPart) {
		Object model = editPart.getModel();

		if(model instanceof IVisualElement)
		{
			model =  (IVisualElement) model;
		}
		else if (model instanceof IVisualConnection) {
			model =  (IVisualConnection) model;
		}
		
		return model;
	}
	
	@SuppressWarnings("unchecked")
	private EditPart getSelectedEditPart() {
		List<EditPart> selection = this.getSelectedObjects();
		if (selection.isEmpty()) return null;
		
		EditPart editPart = selection.get(0);
		
		return editPart;
	}
	
}
