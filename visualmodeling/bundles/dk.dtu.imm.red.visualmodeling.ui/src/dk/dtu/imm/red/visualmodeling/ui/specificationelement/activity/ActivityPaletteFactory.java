package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity;

import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;

import dk.dtu.imm.red.visualmodeling.ui.directedit.CreationAndDirectEditTool;
import dk.dtu.imm.red.visualmodeling.ui.factories.DiagramConnectionFactory;
import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualActionNodeFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualActivityFinalNodeFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualDecisionMergeNodeFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualFlowFinalNodeFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualForkJoinNodeFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualInitialNodeFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualObjectNodeFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualReceiveSignalNodeFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts.VisualSendSignalNodeFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;

public class ActivityPaletteFactory extends PaletteFactoryBase{

	@Override
	public void populatePaletteInternal(PaletteRoot root) {
		PaletteDrawer group = new PaletteDrawer("Activity");

		CreationToolEntry entry;
		entry = new CreationToolEntry("Action Node", "Create a new action node",
				new VisualActionNodeFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Initial Node", "Create a new initial node",
				new VisualInitialNodeFactory(), null, null);
		group.add(entry);

		entry = new CreationToolEntry("Activity Final Node", "Create a new activity final node",
				new VisualActivityFinalNodeFactory(), null, null);
		group.add(entry);

		entry = new CreationToolEntry("Flow Final Node", "Create a new flow final node",
				new VisualFlowFinalNodeFactory(), null, null);
		group.add(entry);

		entry = new CreationToolEntry("Decision/Merge Node", "Create a new decision/merge node",
				new VisualDecisionMergeNodeFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);


		entry = new CreationToolEntry("Fork/Join Node", "Create a new fork/join node",
				new VisualForkJoinNodeFactory(), null, null);
		group.add(entry);


		entry = new CreationToolEntry("Object Node", "Create a new object node",
				new VisualObjectNodeFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Send Signal Node", "Create a new send signal node",
				new VisualSendSignalNodeFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Receive Signal Node", "Create a new receive signal node",
				new VisualReceiveSignalNodeFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);


		group.add(new PaletteSeparator());

		ConnectionCreationToolEntry connectionEntry = new ConnectionCreationToolEntry(
				"Control Flow", "Creates a new control flow connection",
				new DiagramConnectionFactory(ConnectionType.CONTROL_FLOW.getName(),
						"",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry(
				"Object Flow", "Creates a new object flow connection",
				new DiagramConnectionFactory(ConnectionType.OBJECT_FLOW.getName(),
						"",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		root.add(group);
	}
}
