package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase;

import dk.dtu.imm.red.visualmodeling.ui.validation.IRule;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualActorElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualSystemBoundaryElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement;

public class UseCaseRule implements IRule {

	@Override
	public boolean canConnect(IVisualElement source, IVisualElement target,
			IVisualConnection connection) {

		switch(connection.getType()){
			case "Generalization":
				if(source instanceof VisualUseCaseElement && target instanceof VisualUseCaseElement)
					return true;
				if(source instanceof VisualActorElement && target instanceof VisualActorElement)
					return true;
				break;
			case "Extend":
				if(source instanceof VisualUseCaseElement && target instanceof VisualUseCaseElement)
					return true;
				break;
			case "Include":
				if(source instanceof VisualUseCaseElement && target instanceof VisualUseCaseElement)
					return true;
				break;
			case "Association":
				if(source instanceof VisualSystemBoundaryElement || target instanceof VisualSystemBoundaryElement)
					return false;
				else
					return true;
		}
		return false;
	}

	@Override
	public boolean canAccept(IVisualElement parent, IVisualElement child) {

		if(parent instanceof VisualSystemBoundaryElement) {

			if(child instanceof VisualUseCaseElement || child instanceof VisualSystemBoundaryElement)
			{
				return true;
			}
		}else if(parent instanceof VisualDiagram)
		{
			return true;
		}

		return false;
	}



}
