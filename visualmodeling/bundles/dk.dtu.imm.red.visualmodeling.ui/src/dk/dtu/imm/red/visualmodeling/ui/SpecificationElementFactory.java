package dk.dtu.imm.red.visualmodeling.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;

import dk.dtu.imm.red.core.ui.internal.LogUtil;

public class SpecificationElementFactory {

	// UNDONE hardcoded to goal only
	public void OpenCreateNewWizard()
	{
		try {
			getCreateNewWizardRunnable("dk.dtu.imm.red.specificationelements.creategoal").run();
		} catch (Exception e) {			
			e.printStackTrace(); LogUtil.logError(e);
		}
	}
	
	/*
	 * Gets the AbstractHandler used for creating a new SpecificationElement and opening its creation wizard.
	 * */
	public ISafeRunnable getCreateNewWizardRunnable(String id) {
		  
		IConfigurationElement[] configurationElements = 
					Platform.getExtensionRegistry().getConfigurationElementsFor("org.eclipse.ui.commands");
			try {
				for (IConfigurationElement configurationElement : configurationElements) {

					if(configurationElement.getAttribute("id").equalsIgnoreCase(id))
					{
						final Object extension = configurationElement.createExecutableExtension("defaultHandler");
						
						if (extension instanceof AbstractHandler) {
							
							ISafeRunnable runnable = new ISafeRunnable() {

								@Override
								public void handleException(final Throwable exception) {
									exception.printStackTrace();
								}

								@Override
								public void run() throws Exception {							
									((AbstractHandler) extension).execute(new ExecutionEvent());			
								}
							};
							
							return runnable;
						}
					}
				}
			} catch (CoreException e) {
				e.printStackTrace(); LogUtil.logError(e);
			} catch (Exception e) {
				e.printStackTrace(); LogUtil.logError(e);
			}
			
			return null;
	  }
}
