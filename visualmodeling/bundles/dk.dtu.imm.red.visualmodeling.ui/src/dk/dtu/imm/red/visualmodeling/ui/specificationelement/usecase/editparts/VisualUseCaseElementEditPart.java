package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts;

import org.eclipse.draw2d.IFigure;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.figures.VisualUseCaseElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualUseCaseElementEditPart extends VisualElementEditPart{

	public VisualUseCaseElementEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);
			
	}
	
	@Override
	protected IFigure createFigure() {
		return new VisualUseCaseElementFigure((VisualSpecificationElement)getModel());
	}
}
