package dk.dtu.imm.red.visualmodeling.ui.editparts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.TextCellEditor;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import dk.dtu.imm.red.visualmodeling.ui.SpecificationElementSelectionDialog;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualSpecificationElementLinkCommand;
import dk.dtu.imm.red.visualmodeling.ui.directedit.VisualElementCellEditorLocator;
import dk.dtu.imm.red.visualmodeling.ui.directedit.VisualElementDirectEditManager;
import dk.dtu.imm.red.visualmodeling.ui.editpolicies.VisualContainerContainerEditPolicy;
import dk.dtu.imm.red.visualmodeling.ui.editpolicies.VisualContainerLayoutEditPolicy;
import dk.dtu.imm.red.visualmodeling.ui.editpolicies.VisualElementDirectEditPolicy;
import dk.dtu.imm.red.visualmodeling.ui.editpolicies.VisualElementGraphicalNodeEditPolicy;
import dk.dtu.imm.red.visualmodeling.ui.editpolicies.VisualElementComponentEditPolicy;
import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;

public abstract class VisualElementEditPart extends AbstractGraphicalEditPart
		implements NodeEditPart {

	private DiagramElementAdapter adapter;
	protected RuleCollection ruleCollection;

	public VisualElementEditPart(RuleCollection ruleCollection) {
		adapter = new DiagramElementAdapter();
		this.ruleCollection = ruleCollection;
	}

	@Override
	public void setModel(Object model) {
		super.setModel(model);
	}


	@Override
	public void performRequest(Request req) {
		
		if (req.getType() == RequestConstants.REQ_DIRECT_EDIT) {
			performDirectEditing();
		}
		else if (req.getType() == RequestConstants.REQ_OPEN) {
			performDoubleClick();
		}

	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new VisualElementGraphicalNodeEditPolicy(ruleCollection));
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new VisualElementComponentEditPolicy());

		IFigure activeArea = ((VisualElementFigure) getFigure())
				.getActiveArea();
		installEditPolicy(EditPolicy.CONTAINER_ROLE, new VisualContainerContainerEditPolicy(ruleCollection, activeArea));
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new VisualContainerLayoutEditPolicy(ruleCollection, activeArea));


		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new VisualElementDirectEditPolicy());
		// installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, new
		// VisualElementSelectionFeedbackEditPolicy());
	}

	private void performDirectEditing() {
		Label label = ((VisualElementFigure) getFigure()).getLabel();
		VisualElementDirectEditManager manager = new VisualElementDirectEditManager(
				this, TextCellEditor.class, new VisualElementCellEditorLocator(
						label), label);
		manager.show();
	}
	
	private void performDoubleClick() {
		
		if (this.getModel() == null) {
			return; 
		}
		else if (this.getModel() instanceof VisualSpecificationElement) {
			VisualSpecificationElement element = (VisualSpecificationElement) this.getModel();
			
			if (element.getSpecificationElement() == null) {
				this.linkModelToElement();
			}
			else {
				this.openElement();
			}
		}
	}

	/*
	 * Return connections where this element is the source
	 */
	@Override
	protected List<IVisualConnection> getModelSourceConnections() {
		List<IVisualConnection> result = new ArrayList<IVisualConnection>();
		IVisualElement model = (IVisualElement) getModel();

		for (IVisualConnection c : model.getConnections()) {
			if (c.getSource() != null && c.getSource().equals(model)) {
				result.add(c);
			}
		}
		return result;
	}

	/*
	 * Return connections where this element is the target
	 */
	@Override
	protected List<IVisualConnection> getModelTargetConnections() {
		List<IVisualConnection> result = new ArrayList<IVisualConnection>();
		IVisualElement model = (IVisualElement) getModel();

		for (IVisualConnection c : model.getConnections()) {
			if (c.getTarget() != null && c.getTarget().equals(model)) {
				result.add(c);
			}
		}
		return result;
	}

	@Override
	@SuppressWarnings({"rawtypes","unchecked"})
	protected void refreshVisuals() {

		VisualElementFigure figure = (VisualElementFigure) getFigure();
		IVisualElement model = (IVisualElement) getModel();
		AbstractGraphicalEditPart parent = (AbstractGraphicalEditPart) getParent();

		Rectangle layout = new Rectangle(model.getLocation(), model.getBounds());


		// TODO fix this hack and find out why GEF reorders the figures in the children collection.
		// Possibly factor them out completely from the parent figure and nest them in there own.
		List x = new ArrayList();
		for(Object f : parent.getFigure().getChildren())
		{
			if(f instanceof VisualElementFigure)
				x.add(f);
		}

		parent.getFigure().getChildren().removeAll(x);
		parent.getFigure().getChildren().addAll(x);

		//parent.getFigure().add(figure);

		if(figure.getParent()!=null)
			parent.setLayoutConstraint(this, figure, layout);

		figure.refreshVisuals();
	}

	@Override
	public void activate() {
		if (!isActive()) {
			((IVisualElement) getModel()).eAdapters().add(adapter);
		}
		super.activate();
	}

	@Override
	public void deactivate() {
		if (isActive()) {
			((IVisualElement) getModel()).eAdapters().remove(adapter);
		}

		super.deactivate();
	}

	@Override
	protected List<IVisualElement> getModelChildren() {
		IVisualElement element = (IVisualElement) getModel();
		return Collections.unmodifiableList(element.getElements());
	}

	public ConnectionAnchor getSourceConnectionAnchor(ConnectionEditPart arg0) {
		// TODO Auto-generated method stub
		return ((VisualElementFigure) getFigure()).getConnectionAnchor();
	}

	@Override
	public ConnectionAnchor getSourceConnectionAnchor(Request arg0) {
		// TODO Auto-generated method stub
		return ((VisualElementFigure) getFigure()).getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(ConnectionEditPart arg0) {
		// TODO Auto-generated method stub
		return ((VisualElementFigure) getFigure()).getConnectionAnchor();
	}

	@Override
	public ConnectionAnchor getTargetConnectionAnchor(Request arg0) {
		// TODO Auto-generated method stub
		return ((VisualElementFigure) getFigure()).getConnectionAnchor();
	}

	/*
	 * Override to extend the context menu on selection.
	 */
	public void createContextMenu(IMenuManager menu) {

	}

	/*
	 * Inner class for notifications
	 */
	public class DiagramElementAdapter implements Adapter {

		public void notifyChanged(Notification notification) {

			if (notification.getEventType() == Notification.ADD && notification.getFeatureID(IVisualElement.class) == visualmodelPackage.IVISUAL_ELEMENT__ELEMENTS) {

			}

			refresh();

		}

		public Notifier getTarget() {
			return (IVisualElement) getModel();
		}

		public void setTarget(Notifier newTarget) {
			// Do nothing.
		}

		public boolean isAdapterForType(Object type) {
			return type.equals(IVisualElement.class);
		}
	}
	
	public void openElement() {
		if (getModel() == null || !(getModel() instanceof VisualSpecificationElement)) return;
		
		VisualSpecificationElement visualElement = (VisualSpecificationElement)getModel();
		final Element element = visualElement.getSpecificationElement(); 
		
		if (element == null) return;
		
		IConfigurationElement[] configurationElements = 
				Platform.getExtensionRegistry().getConfigurationElementsFor("dk.dtu.imm.red.core.element");
		try {
			for (IConfigurationElement configurationElement : configurationElements) {

				final Object extension = configurationElement.createExecutableExtension("class");

				if (extension instanceof IElementExtensionPoint) {
					
					ISafeRunnable runnable = new ISafeRunnable() {

						@Override
						public void handleException(final Throwable exception) {
							exception.printStackTrace();
						}

						@Override
						public void run() throws Exception {							
							((IElementExtensionPoint) extension).openElement(element);						
						}
					};
					runnable.run();
				}
			}
		} catch (CoreException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (Exception e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}
	
	public void linkModelToElement() {
		Object model = this.getModel();
		
		if(model instanceof VisualSpecificationElement) {
			
			SpecificationElementSelectionDialog dialog =
					new SpecificationElementSelectionDialog(null, this);
			dialog.create();
			int result = dialog.open();

			if(result == SpecificationElementSelectionDialog.CANCEL)
			{
				return;
			}

			SpecificationElement element = dialog.getSelection();
			
			VisualSpecificationElement visualElement = (VisualSpecificationElement)getModel();

	    	Command command = new VisualSpecificationElementLinkCommand(visualElement, element);
	    	getViewer().getEditDomain().getCommandStack().execute(command);

	    	this.refresh();
		}
	}
}
