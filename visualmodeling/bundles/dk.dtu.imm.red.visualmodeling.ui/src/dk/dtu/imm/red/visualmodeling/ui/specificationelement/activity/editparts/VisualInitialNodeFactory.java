package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualInitialNodeFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualInitialNode;

public class VisualInitialNodeFactory implements CreationFactory {
	public Object getNewObject() {

		VisualInitialNode element = ActivityFactory.eINSTANCE
				.createVisualInitialNode();
		element.setBounds(VisualInitialNodeFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualInitialNode.class;
	}
}
