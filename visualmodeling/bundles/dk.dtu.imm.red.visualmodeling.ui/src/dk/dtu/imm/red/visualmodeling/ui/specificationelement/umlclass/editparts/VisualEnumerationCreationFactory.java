package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures.VisualEnumerationFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement;

public class VisualEnumerationCreationFactory implements CreationFactory {

	public VisualEnumerationCreationFactory()
	{

	}

	@Override
	public Object getNewObject() {
		VisualEnumerationElement element = ClassFactory.eINSTANCE.createVisualEnumerationElement();
		element.setBounds(VisualEnumerationFigure.initialBounds);

		return element;
	}

	@Override
	public Object getObjectType() {
		return VisualEnumerationElement.class;
	}



}
