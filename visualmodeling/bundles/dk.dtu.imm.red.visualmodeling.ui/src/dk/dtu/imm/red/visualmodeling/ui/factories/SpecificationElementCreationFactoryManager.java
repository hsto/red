package dk.dtu.imm.red.visualmodeling.ui.factories;

import java.util.HashMap;

import org.eclipse.gef.requests.CreationFactory;
import org.picocontainer.PicoContainer;

import dk.dtu.imm.red.specificationelements.SpecificationElement;


public final class SpecificationElementCreationFactoryManager {

	private PicoContainer container;
	private HashMap<Class<? extends SpecificationElement>, Class<? extends CreationFactory>> mapping;

	public SpecificationElementCreationFactoryManager(PicoContainer container, HashMap<Class<? extends SpecificationElement>, Class<? extends CreationFactory>> mapping)
	{
		this.container = container;
		this.mapping = mapping;
	}
	
	public CreationFactory getCreationFactory(Class<? extends SpecificationElement> key) {

		// TODO check key exists
		Class<? extends CreationFactory> value = mapping.get(key);
		CreationFactory factory = container.getComponent(value);

		return factory;
	}
}
