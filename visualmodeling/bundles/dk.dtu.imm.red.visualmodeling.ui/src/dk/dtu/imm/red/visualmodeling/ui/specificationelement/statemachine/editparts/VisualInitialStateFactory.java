package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualInitialStateFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualInitialState;

public class VisualInitialStateFactory implements CreationFactory {
	public Object getNewObject() {

		VisualInitialState element = StatemachineFactory.eINSTANCE
				.createVisualInitialState();
		element.setBounds(VisualInitialStateFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualInitialState.class;
	}
}
