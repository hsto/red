package dk.dtu.imm.red.visualmodeling.ui.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import dk.dtu.imm.red.visualmodeling.ui.editpolicies.VisualContainerContainerEditPolicy;
import dk.dtu.imm.red.visualmodeling.ui.editpolicies.VisualContainerLayoutEditPolicy;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

public class DiagramEditPart extends AbstractGraphicalEditPart {

	private DiagramAdapter adapter;
	protected RuleCollection ruleCollection;

	public DiagramEditPart(RuleCollection ruleCollection) {
		adapter = new DiagramAdapter();
		this.ruleCollection = ruleCollection;
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new VisualContainerLayoutEditPolicy(ruleCollection));
		installEditPolicy(EditPolicy.CONTAINER_ROLE, new VisualContainerContainerEditPolicy(ruleCollection));
	}

	@Override
	protected IFigure createFigure() {
		// TODO Auto-generated method stub

		FreeformLayer layer = new FreeformLayer();
		layer.setLayoutManager(new FreeformLayout());
		//layer.setBorder(new LineBorder(1));
		return layer;

	}

	@Override
	public List<IVisualElement> getModelChildren() {
		List<IVisualElement> children = new ArrayList<IVisualElement>();
		VisualDiagram diagram = (VisualDiagram) getModel();
		children.addAll(diagram.getElements());
		return children;
	}

	@Override
	public void activate() {
		if (!isActive()) {
			((VisualDiagram) getModel()).eAdapters().add(adapter);
		}
		super.activate();
	}

	@Override
	public void deactivate() {
		if (isActive()) {
			((VisualDiagram) getModel()).eAdapters().remove(adapter);
		}
		super.deactivate();
	}

	public class DiagramAdapter implements Adapter {

		public void notifyChanged(Notification notification) {
			refreshChildren();
		}

		public Notifier getTarget() {
			return (VisualDiagram) getModel();
		}

		public void setTarget(Notifier newTarget) {
			// Do nothing.
		}

		public boolean isAdapterForType(Object type) {
			return type.equals(VisualDiagram.class);
		}
	}
}
