package dk.dtu.imm.red.visualmodeling.ui.actions;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.SWTGraphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.LayerManager;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.ui.ExportDiagramDialog;
import dk.dtu.imm.red.visualmodeling.ui.VisualEditor;

public class ExportDiagramAction extends WorkbenchPartAction{

  	public static final String EXPORT_DIAGRAM = "ExportDiagram";
   	public static final String REQ_EXPORT_DIAGRAM = "ExportDiagram";

	private VisualEditor editor;
	
	public ExportDiagramAction(IWorkbenchPart part) {
		super(part);
		
		setId(EXPORT_DIAGRAM);
        setText("Export Diagram");
        
		this.editor = (VisualEditor)part;
		
	}

	@Override
	protected boolean calculateEnabled() {		
		return true;
	}
	
	@Override
	public void run() {
				
			
			String saveLocation = "C:\\Users\\Jeppe\\Test.jpeg";			
			int format = SWT.IMAGE_JPEG;
			
			Display display = Display.getDefault();
		    Shell shell = display.getActiveShell();
			
		    ExportDiagramDialog dialog = new ExportDiagramDialog(shell, SWT.APPLICATION_MODAL);
			if(dialog.open() != null)
			{
				saveLocation = dialog.exportFilePath;
				format = dialog.imageFormat;
				
				performExport(saveLocation, format);
			}
	}
	
	public void performExport(String filePath, int imageFormat)
	{
		GraphicalViewer graphicalViewer = editor.getGraphicalViewer();
		ScalableFreeformRootEditPart rootEditPart = (ScalableFreeformRootEditPart)graphicalViewer.getEditPartRegistry().get(LayerManager.ID);
		IFigure rootFigure = ((LayerManager)rootEditPart).getLayer(LayerConstants.PRINTABLE_LAYERS);
		Rectangle rootFigureBounds = rootFigure.getBounds();		
		
		Control figureCanvas = graphicalViewer.getControl();				
		GC figureCanvasGC = new GC(figureCanvas);		
		
		Image img = new Image(null, rootFigureBounds.width, rootFigureBounds.height);
		GC imageGC = new GC(img);
		imageGC.setBackground(figureCanvasGC.getBackground());
		imageGC.setForeground(figureCanvasGC.getForeground());
		imageGC.setFont(figureCanvasGC.getFont());
		imageGC.setLineStyle(figureCanvasGC.getLineStyle());
		imageGC.setLineWidth(figureCanvasGC.getLineWidth());
		
		Graphics imgGraphics = new SWTGraphics(imageGC);					
		rootFigure.paint(imgGraphics);
						
		ImageData[] imgData = new ImageData[1];
		imgData[0] = img.getImageData();
		
		ImageLoader imgLoader = new ImageLoader();
		imgLoader.data = imgData;
		imgLoader.save(filePath, imageFormat);
		
		figureCanvasGC.dispose();
		imageGC.dispose();
		img.dispose();
	}
}

