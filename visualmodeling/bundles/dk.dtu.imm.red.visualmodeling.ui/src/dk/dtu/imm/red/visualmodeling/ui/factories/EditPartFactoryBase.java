package dk.dtu.imm.red.visualmodeling.ui.factories;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import dk.dtu.imm.red.visualmodeling.ui.editparts.DiagramEditPart;
import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualConnectionEditPart;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

/**
 * Base class for EditPartFactory. Override {@link #createEditPartInternal(EditPart, Object) createEditPartInternal}
 * to provide own {@link #org.eclipse.gef.EditPart EditParts}. Use {@link #setNext(EditPartFactoryBase) setNext} method to
 * create a chain of factories.
 */
public abstract class EditPartFactoryBase implements EditPartFactory {

	protected EditPartFactoryBase nextFactory;
	protected RuleCollection ruleCollection;

	public EditPartFactoryBase(RuleCollection ruleCollection) {
		this.ruleCollection = ruleCollection;
	}

	@Override
	public final EditPart createEditPart(EditPart context, Object model) {
		
		EditPart part = createEditPartNext(context, model);
						
		if (part == null) {
			if (model instanceof VisualDiagram) {
				part = new DiagramEditPart(ruleCollection);			
			} else if (model instanceof VisualConnection) {
				part = new VisualConnectionEditPart();
			}
		}		
		
		if (part != null && part.getModel() == null) {
			part.setModel(model);			
		}else if(part == null)
		{
			// Should not add the illegal element in the diagram
			IVisualElement element = (IVisualElement) model;
			VisualDiagram diagram = (VisualDiagram) element.getDiagram();
			diagram.getElements().remove(element);
			element.setParent(null);;
			element.setDiagram(null);
						
			throw new IllegalArgumentException(String.format("EditPartFactory could not handle the model type %s in the context of %s"
					, model.getClass().toString()
					, context.getClass().toString()));
		}

		return part;
	}
	
	/**
	 * Recursively loops through chained factories.
	 * @param context
	 * @param model
	 * @return
	 */
	private final EditPart createEditPartNext(EditPart context, Object model)
	{			
		EditPart part = createEditPartInternal(context, model);
		
		if(part == null && nextFactory != null)
			return nextFactory.createEditPartNext(context, model);
		else
			return part;
	}
	
	public final EditPartFactoryBase getNext()
	{
		return nextFactory;
	}

	/**
	 * Sets the next factory in the chain to try.
	 * @param nextfactory
	 */
	public final EditPartFactoryBase setNext(EditPartFactoryBase nextfactory)
	{
		this.nextFactory = nextfactory;
		return nextfactory;
	}
	
	/**
	 * Create and return an EditPart based on the context and model.
	 * Return null if can't handle creation of EditPart for the context or model.
	 * @param context
	 * @param model
	 * @return
	 */
	protected abstract EditPart createEditPartInternal(EditPart context, Object model);
}
