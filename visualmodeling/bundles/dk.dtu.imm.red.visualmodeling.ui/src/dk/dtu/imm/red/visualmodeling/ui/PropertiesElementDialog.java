package dk.dtu.imm.red.visualmodeling.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.actor.ActorKind;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.GoalKind;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.usecase.UseCaseKind;
import dk.dtu.imm.red.visualmodeling.ui.commands.PropertiesElementCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementMakeNonSketchyCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementMakeSketchyCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementRenameCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualSpecificationElementLinkCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualSpecificationElementUnlinkCommand;
import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualPortEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemStructureActorEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualActorElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualSystemBoundaryElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualUseCaseElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorLabelMouseListener;
import dk.dtu.imm.red.visualmodeling.visualmodel.GenericType;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualActorElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement;

/**
 * 
 * @author Luai
 *
 * Issue #123
 */

public class PropertiesElementDialog extends Dialog {

	private IVisualElement visualElement;
	private VisualSpecificationElement visualSpecificationElement;
	private VisualElementEditPart editPart;
	private VisualElementFigure figure;
	
	protected Shell shellProperties;
	
	private List<Command> commands;
	
	private Text textName;
	private Combo comboKind;
	private Combo comboFontType;
	private Combo comboFontStyle;
	private Combo comboFontSize;
	private Label labelColor;
	private Combo comboOutlineWeight;
	private Combo comboOutlineDashes;
	private Label labelLineColor;
	private Label labelFillColor;
	private Combo comboTransparency;
	private Button checkBoxSketchiness;
	
	
	
	private SpecificationElement linkedElement = null;
	private boolean unlinkElement = false;
	
	private String defaultFormalFontType = "Tahoma";
	private String dafultSketchyFontType = "Bradley Hand ITC";
	private String defaultFormalFontSize = "10";
	private String dafultSketchyFontSize = "10";
	
	public PropertiesElementDialog(Shell parent, int style, IVisualElement visualElement, EditPart selectedEditPart) {
		super(parent, style);
		setText("SWT Dialog");
		this.visualElement = visualElement;
		if (visualElement instanceof VisualSpecificationElement) {
			this.visualSpecificationElement = (VisualSpecificationElement) visualElement;
			
		}
		this.editPart = (VisualElementEditPart) selectedEditPart;
		this.figure = (VisualElementFigure) editPart.getFigure();
	}
	

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shellProperties.open();
		shellProperties.layout();
		Display display = getParent().getDisplay();
		while (!shellProperties.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return commands;
	}
	
	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shellProperties = new Shell(getParent(), SWT.DIALOG_TRIM);
		shellProperties.setSize(350, 400);
		shellProperties.setText("Visual Element Properties");
		
		Monitor primary = getParent().getDisplay().getPrimaryMonitor();
	    Rectangle bounds = primary.getBounds();
	    Rectangle rect = shellProperties.getBounds();	    
	    int x = bounds.x + (bounds.width - rect.width) / 2;
	    int y = bounds.y + (bounds.height - rect.height) / 2;	    
	    shellProperties.setLocation(x, y);
	    
	    int sepOffset = 16;
	    
	    Label labelName = new Label(shellProperties, SWT.NONE);
	    labelName.setBounds(10, 10, 40, 15);
	    labelName.setText("Name");
	    
	    textName = new Text(shellProperties, SWT.BORDER);
	    textName.setBounds(10, labelName.getLocation().y + labelName.getSize().y, 200, 24);
	    if (visualElement.getName() != null) textName.setText(visualElement.getName());
	    
	    Label labelKind = new Label (shellProperties, SWT.NONE);
	    labelKind.setBounds(textName.getLocation().x + textName.getSize().x + 15, labelName.getLocation().y, 30, 15);
	    labelKind.setText("Kind");
	    
	    comboKind = new Combo(shellProperties, SWT.READ_ONLY);
	    comboKind.setBounds(labelKind.getLocation().x, textName.getLocation().y, 100, 24);
	    fillComboKind(comboKind);
	    
	    
	    
	    Label labelFont = new Label(shellProperties, SWT.NONE);
	    labelFont.setBounds(10, textName.getLocation().y + textName.getSize().y + sepOffset, 40, 15);
	    labelFont.setText("Font");
	    
	    
	    String fontHeight = "" + figure.getLabel().getFont().getFontData()[0].getHeight();
	    String fontName = figure.getLabel().getFont().getFontData()[0].getName();
	    int fontStyle = figure.getLabel().getFont().getFontData()[0].getStyle();
	    
	    comboFontType = new Combo(shellProperties, SWT.READ_ONLY);
	    comboFontType.setBounds(10, labelFont.getLocation().y + labelFont.getSize().y, 100, 24);
	    comboFontType.add("Arial");
	    comboFontType.add("Bradley Hand ITC");
	    comboFontType.add("Calibri");
	    comboFontType.add("Times New Roman");
	    comboFontType.add("Tahoma");
	    comboFontType.add("Segoe UI");
	    comboFontType.select(getComboSelectionIndex(comboFontType, fontName));
	    
	    comboFontStyle = new Combo(shellProperties, SWT.READ_ONLY);
	    comboFontStyle.setBounds(comboFontType.getLocation().x + comboFontType.getSize().x + 10, comboFontType.getLocation().y, 100, 24);
	    comboFontStyle.add("Regular");
	    comboFontStyle.add("Bold");
	    comboFontStyle.add("Italic");
	    comboFontStyle.select(fontStyle);
	    
	    comboFontSize = new Combo(shellProperties, SWT.READ_ONLY);
		comboFontSize.setBounds(comboFontStyle.getLocation().x + comboFontStyle.getSize().x + 10, comboFontType.getLocation().y, 40, 24);
		comboFontSize.add("9");
		comboFontSize.add("10");
		comboFontSize.add("11");
		comboFontSize.add("12");
		comboFontSize.add("14");
		comboFontSize.add("16");
		comboFontSize.add("18");
		comboFontSize.add("20");
		comboFontSize.select(getComboSelectionIndex(comboFontSize, fontHeight));
	    
		labelColor = new Label(shellProperties, SWT.BORDER);
		labelColor.setBounds(comboFontSize.getLocation().x + comboFontSize.getSize().x + 10, comboFontType.getLocation().y, 25, 23);
		//labelColor.setBackground(new Color(shellProperties.getDisplay(), new RGB(0, 255, 0)));
		labelColor.setBackground(figure.getLabel().getForegroundColor());
		labelColor.addMouseListener(new ColorLabelMouseListener(labelColor, shellProperties));
		
		Label labelSELink = new Label(shellProperties, SWT.NONE);
		labelSELink.setBounds(10, comboFontType.getLocation().y + comboFontType.getSize().y + sepOffset, 180, 15);
		labelSELink.setText("Linked Specification Element");
		
		final CLabel labeSElLinkValue = new CLabel(shellProperties, SWT.BORDER);
		labeSElLinkValue.setBounds(10, labelSELink.getLocation().y + labelSELink.getSize().y, 240, 24);
		if (visualSpecificationElement != null && visualSpecificationElement.getSpecificationElement() != null) {
			labeSElLinkValue.setImage(visualSpecificationElement.getSpecificationElement().getIcon());
			labeSElLinkValue.setText(visualSpecificationElement.getSpecificationElement().getLabel() + " - " + visualSpecificationElement.getSpecificationElement().getName());
		}
		if (visualSpecificationElement == null) labeSElLinkValue.setEnabled(false);
		//labeSElLinkValue.setBackground(new Color(shellProperties.getDisplay(), new RGB(255, 255, 255)));
		
	    final Button buttonLink = new Button(shellProperties, SWT.NONE);
	    buttonLink.setBounds(labeSElLinkValue.getLocation().x + labeSElLinkValue.getSize().x + 4, labeSElLinkValue.getLocation().y, 24, 24);
	    buttonLink.setText("+");
	    if (visualSpecificationElement == null) buttonLink.setEnabled(false);
	    buttonLink.addSelectionListener(new SelectionAdapter() {
			@Override
			@SuppressWarnings("unchecked")
			public void widgetSelected(SelectionEvent e) {
				Class<Element>[] typesShown = new Class[]{};
				if(editPart instanceof VisualGoalElementEditPart){
					typesShown = new Class[]{Goal.class};
				}
				else if(editPart instanceof VisualActorElementEditPart || editPart instanceof VisualSystemStructureActorEditPart || 
						editPart instanceof VisualPortEditPart || editPart instanceof VisualSystemEditPart || editPart instanceof VisualSystemBoundaryElementEditPart){
					typesShown = new Class[]{Actor.class};
				}
				else if(editPart instanceof VisualUseCaseElementEditPart){
					typesShown = new Class[]{UseCase.class};
				}
				SpecificationElementSelectionDialog dialog = new SpecificationElementSelectionDialog(shellProperties, typesShown);
				dialog.create();
				int result = dialog.open();
				if(result != SpecificationElementSelectionDialog.CANCEL)
				{
					linkedElement = dialog.getSelection();
					unlinkElement = false;
					labeSElLinkValue.setImage(linkedElement.getIcon());
		    		labeSElLinkValue.setText(linkedElement.getLabel() + " - " + linkedElement.getName());
		    		if (linkedElement.getElementKind() != null)
		    			comboKind.select(getComboSelectionIndex(comboKind, linkedElement.getElementKind()));
		    		else
		    			comboKind.select(0);
				}
			}
	    	
		});
	    
	    Button buttonUnlink = new Button(shellProperties, SWT.NONE);
	    buttonUnlink.setBounds(buttonLink.getLocation().x + buttonLink.getSize().x, labeSElLinkValue.getLocation().y, 24, 24);
	    buttonUnlink.setText("-");
	    if (visualSpecificationElement == null) buttonUnlink.setEnabled(false);
	    buttonUnlink.addSelectionListener(new SelectionAdapter() {
	    	@Override
	    	public void widgetSelected(SelectionEvent e) {
	    		linkedElement = null;
	    		unlinkElement = true;
	    		labeSElLinkValue.setImage(null);
	    		labeSElLinkValue.setText("");
	    		//buttonLink.setEnabled(true);
	    	}
		});
	    
	    Label labelLine = new Label(shellProperties, SWT.NONE);
	    labelLine.setBounds(10, labeSElLinkValue.getLocation().y + labeSElLinkValue.getSize().y + sepOffset, 40, 15);
	    labelLine.setText("Line");
	    
	    
	    comboOutlineWeight = new Combo(shellProperties, SWT.READ_ONLY);
	    comboOutlineWeight.setBounds(10, labelLine.getLocation().y + labelLine.getSize().y, 80, 23);
	    comboOutlineWeight.add("1");
	    comboOutlineWeight.add("2");
	    comboOutlineWeight.add("3");
	    comboOutlineWeight.add("4");
	    if (figure.getFormalFigure() != null) comboOutlineWeight.select(figure.getFormalFigure().getLineWidth() - 1);
	    else comboOutlineWeight.setEnabled(false); 
	    //String width = "" + figure.getFormalFigure().getLineWidth();
	    //comboOutlineWeight.select(getComboSelectionIndex(comboOutlineWeight, width));
	    
	    comboOutlineDashes = new Combo(shellProperties, SWT.READ_ONLY);
	    comboOutlineDashes.setBounds(comboOutlineWeight.getLocation().x + comboOutlineWeight.getSize().x + 10, comboOutlineWeight.getLocation().y, 80, 24);
	    comboOutlineDashes.add("Solid");
	    comboOutlineDashes.add("Dash");
	    comboOutlineDashes.add("Dot");
	    comboOutlineDashes.add("Dash Dot");
	    comboOutlineDashes.add("Dash Dot Dot");
	    if (figure.getFormalFigure() != null) comboOutlineDashes.select(figure.getFormalFigure().getLineStyle() - 1);
	    else comboOutlineDashes.setEnabled(false);
	    
	    labelLineColor = new Label(shellProperties, SWT.BORDER);
	    labelLineColor.setBounds(comboOutlineDashes.getLocation().x + comboOutlineDashes.getSize().x + 10, comboOutlineWeight.getLocation().y, 25, 24);
	    labelLineColor.addMouseListener(new ColorLabelMouseListener(labelLineColor, shellProperties));
	    if (figure.getFormalFigure() != null) labelLineColor.setBackground(figure.getFormalFigure().getForegroundColor());
	    //labelLineColor.setBackground(new Color(shellProperties.getDisplay(), new RGB(0, 255, 0)));
	    
	    Label labelFill = new Label(shellProperties, SWT.NONE);
	    labelFill.setBounds(10, comboOutlineWeight.getLocation().y + comboOutlineWeight.getSize().y + sepOffset, 35, 15);
	    labelFill.setText("Fill");

	    labelFillColor = new Label(shellProperties, SWT.BORDER);
	    labelFillColor.setBounds(10, labelFill.getLocation().y + labelFill.getSize().y, 25, 24);
	    //labelFillColor.setBackground(new Color(shellProperties.getDisplay(), new RGB(0, 255, 0)));
	    labelFillColor.addMouseListener(new ColorLabelMouseListener(labelFillColor, shellProperties));
	    if (figure.getFormalFigure() != null) labelFillColor.setBackground(figure.getFormalFigure().getBackgroundColor());
		
	    
	    
	    Label labelTransparency = new Label(shellProperties, SWT.NONE);
	    labelTransparency.setBounds(labelFillColor.getLocation().x + labelFillColor.getSize().x + 10, labelFill.getLocation().y, 80, 15);
	    labelTransparency.setText("Transparency");
	    
	    comboTransparency = new Combo(shellProperties, SWT.READ_ONLY);
	    comboTransparency.setBounds(labelTransparency.getLocation().x, labelFillColor.getLocation().y, 70, 24);
	    comboTransparency.add("100 %");
	    comboTransparency.add("75 %");
	    comboTransparency.add("50 %");
	    comboTransparency.add("15 %");
	    comboTransparency.add("1 %");
	    fillComboTransparency(comboTransparency);
	    
	    if (visualElement.isIsSketchy() && !(visualElement instanceof VisualGenericElement)) { // Temp Sketchy element is a picture, these settings not possible
			comboOutlineWeight.setEnabled(false);
			comboOutlineDashes.setEnabled(false);
			labelLineColor.setEnabled(false);
			labelFillColor.setEnabled(false);
		}
	    checkBoxSketchiness = new Button(shellProperties, SWT.CHECK);
	    checkBoxSketchiness.setSelection(visualElement.isIsSketchy());
	    checkBoxSketchiness.setBounds(10, labelFillColor.getLocation().y + labelFillColor.getSize().y + sepOffset, 80, 15);
	    checkBoxSketchiness.setText("Sketchy");
	    checkBoxSketchiness.setSelection(visualElement.isIsSketchy());
	    checkBoxSketchiness.addSelectionListener(new SelectionAdapter() { // Temp Sketchy element is a picture, these settings not possible
	    	@Override
			public void widgetSelected(SelectionEvent e) {
	    		if (checkBoxSketchiness.getSelection()) comboFontType.select(getComboSelectionIndex(comboFontType, dafultSketchyFontType));
	    		else comboFontType.select(getComboSelectionIndex(comboFontType, defaultFormalFontType));
	    			
	    		if (checkBoxSketchiness.getSelection() && /*Temp*/ !(visualElement instanceof VisualGenericElement)) {
	    			comboOutlineWeight.setEnabled(false);
	    			comboOutlineDashes.setEnabled(false);
	    			labelLineColor.setEnabled(false);
	    			labelFillColor.setEnabled(false);
	    		} else {
	    			comboOutlineWeight.setEnabled(true);
	    			comboOutlineDashes.setEnabled(true);
	    			labelLineColor.setEnabled(true);
	    			labelFillColor.setEnabled(true);
	    		}
	    	}
		});
	    
	    Button buttonCancel = new Button(shellProperties, SWT.NONE);
	    buttonCancel.setBounds(10, shellProperties.getSize().y - 64, 75, 24);  
	    buttonCancel.setText("Cancel");
	    buttonCancel.addSelectionListener(new SelectionAdapter() {
	    	@Override
			public void widgetSelected(SelectionEvent e) {
				commands = null;
				shellProperties.close();
			}
		});
	    
	    Button buttonDefault = new Button(shellProperties, SWT.NONE);	    
	    buttonDefault.setBounds(85, shellProperties.getSize().y - 64, 75, 24);
	    buttonDefault.setText("Defaults");
	    buttonDefault.addSelectionListener(new SelectionAdapter() {
	    	@Override
			public void widgetSelected(SelectionEvent e) {
	    		textName.setText("");
	    		if (checkBoxSketchiness.getSelection()) {
	    			comboFontType.select(getComboSelectionIndex(comboFontType, dafultSketchyFontType));
		    		comboFontSize.select(getComboSelectionIndex(comboFontSize, dafultSketchyFontSize));
		    		comboFontStyle.select(0);
	    		} else {
	    			comboFontType.select(getComboSelectionIndex(comboFontType, defaultFormalFontType));
		    		comboFontSize.select(getComboSelectionIndex(comboFontSize, defaultFormalFontSize));
		    		comboFontStyle.select(0);
	    		}
	    		
	    		labelColor.setBackground(ColorHelper.black);
	    		comboOutlineWeight.select(2 - 1);
	    		comboOutlineDashes.select(0);
	    		labelLineColor.setBackground(ColorHelper.black);
	    		labelFillColor.setBackground(ColorHelper.black);
	    		comboTransparency.select(0);
	    	}
	    });
	    
	    Button buttonOk = new Button(shellProperties, SWT.NONE);
	    buttonOk.setBounds(shellProperties.getSize().x - 90 - 10, shellProperties.getSize().y - 64, 90, 24);
	    buttonOk.setText("Ok");
	    buttonOk.addSelectionListener(new SelectionAdapter() {
	    	@Override
			public void widgetSelected(SelectionEvent e) {
				commands = finish();
				shellProperties.close();
			}
		});
	    
	}
	
	
	private List<Command> finish() {
		List<Command> commands = new ArrayList<Command>();
		PropertiesElementCommand propertiesCommand = new PropertiesElementCommand(figure, linkedElement, visualSpecificationElement, visualElement);
		
		if (visualElement.getName() != null && textName.getText() != null && !visualElement.getName().equals(textName.getText())) {
			commands.add(new VisualElementRenameCommand(visualElement, textName.getText()));			
		}		
		
		String selectedKind = comboKind.getItem(comboKind.getSelectionIndex());		
		if (visualSpecificationElement != null) {
			if (linkedElement != null && linkedElement.getElementKind() != null && !linkedElement.getElementKind().equals(selectedKind)) {
				propertiesCommand.setLinkedElementKind(selectedKind);
			} else if (visualSpecificationElement.getSpecificationElement() != null && visualSpecificationElement.getSpecificationElement().getElementKind() != null &&
					!visualSpecificationElement.getSpecificationElement().getElementKind().equals(selectedKind) ) {
				propertiesCommand.setVisualSpecificationKind(selectedKind);
			}			
			
			if (visualElement instanceof VisualSystemStructureActor) {
				VisualSystemStructureActor actor = (VisualSystemStructureActor) visualElement;
				if (!actor.getActorKind().toString().equals(selectedKind)) {
					propertiesCommand.setKindSystemStructureActor(SystemStructureActorKind.getByName(selectedKind));					
				}
			}
		}
		
		if (visualElement instanceof VisualGenericElement) {
			VisualGenericElement generic = (VisualGenericElement) visualElement;
			if (!generic.getGenericType().equals(selectedKind))
				propertiesCommand.setKindGeneric(GenericType.getByName(selectedKind));		
		}
		
		FontData[] fd = figure.getLabel().getFont().getFontData();
		fd[0].setHeight(Integer.parseInt(comboFontSize.getItem(comboFontSize.getSelectionIndex())));
		fd[0].setName(comboFontType.getItem(comboFontType.getSelectionIndex()));;
	    fd[0].setStyle(comboFontStyle.getSelectionIndex());	    
	    
	    propertiesCommand.setLabelColor(labelColor.getBackground());
		
		if (visualElement.isIsSketchy()) {
			propertiesCommand.setFontSketchy(new Font(shellProperties.getDisplay(), fd[0]));
		} else {
			propertiesCommand.setFontDefault(new Font(shellProperties.getDisplay(), fd[0]));
		}
		
		
		// Linking
		if (linkedElement != null && !linkedElement.equals(visualSpecificationElement.getSpecificationElement())) {
			commands.add(new VisualSpecificationElementLinkCommand(visualSpecificationElement, linkedElement));
		} else if (unlinkElement && visualSpecificationElement.getSpecificationElement() != null) {
			commands.add(new VisualSpecificationElementUnlinkCommand(visualSpecificationElement));
		}
		
		// Line
		if ( (!checkBoxSketchiness.getSelection() && figure.getFormalFigure() != null) || 
		/*Temp*/ (checkBoxSketchiness.getSelection() && visualElement instanceof VisualGenericElement) ) {
			
			if (figure.getFormalFigure().getLineWidth() != (comboOutlineWeight.getSelectionIndex() + 1)) {
				propertiesCommand.setLineWidth(comboOutlineWeight.getSelectionIndex() + 1);
			}
			
			if (figure.getFormalFigure().getLineStyle() != (comboOutlineDashes.getSelectionIndex() + 1)) {
				propertiesCommand.setLineStyle(comboOutlineDashes.getSelectionIndex() + 1);
			}
			
			if (figure.getFormalFigure().getForegroundColor() != null && !figure.getFormalFigure().getForegroundColor().equals(labelLineColor.getBackground())) {
				propertiesCommand.setLineColor(labelLineColor.getBackground());
			}
			
			if (!figure.getFormalFigure().getBackgroundColor().equals(labelFillColor.getBackground())) {
				propertiesCommand.setFillColor(labelFillColor.getBackground());
			}
			
		}
		
		
		// Transparency
		int selectedAlpha = percentToAlpha(Integer.parseInt(percentValue(comboTransparency.getItem(comboTransparency.getSelectionIndex()))));
		if (checkBoxSketchiness.getSelection() && figure.getSketchyFigure() != null && (figure.getSketchyFigure().getAlpha() != selectedAlpha) ) {
			propertiesCommand.setAlphaSketchy(selectedAlpha);
		} else if (!checkBoxSketchiness.getSelection() && figure.getFormalFigure() != null) {			
			if (figure.getFormalFigure().getAlpha() == null) { // Alpha has not been set before
				propertiesCommand.setAlphaFormal(selectedAlpha);
			} else if(figure.getFormalFigure().getAlpha() != selectedAlpha) {
				propertiesCommand.setAlphaFormal(selectedAlpha);
			}			
		}
		
		commands.add(propertiesCommand);
		
		// Sketchiness
		if (!visualElement.isIsSketchy() && checkBoxSketchiness.getSelection()) {
			commands.add(new VisualElementMakeSketchyCommand(new IVisualElement[] {visualElement}));
		} else if (visualElement.isIsSketchy() && !checkBoxSketchiness.getSelection()) {
			commands.add(new VisualElementMakeNonSketchyCommand(new IVisualElement[] {visualElement}));
		} 
		
		return commands;
	}
	
	/**
	 * TODO Should be done generic
	 * 
	 */
	private void fillComboKind(Combo combo) {
		
		String[] elementKinds = null;
		
		if (visualElement instanceof VisualGoalElement || visualElement instanceof VisualGoalLayer) {
			GoalKind[] goalKinds = GoalKind.values();
			elementKinds = new String[goalKinds.length];
			for(int i=0;i<goalKinds.length;i++){
				elementKinds[i] = goalKinds[i].getName();
			}
		} else if (visualElement instanceof VisualSystemStructureActor) {
			SystemStructureActorKind[] systemStructureActorKinds = SystemStructureActorKind.values();
			elementKinds = new String[systemStructureActorKinds.length];
			for(int i=0;i<systemStructureActorKinds.length;i++){
				elementKinds[i] = systemStructureActorKinds[i].getName().toLowerCase();
			}
		} else if (visualElement instanceof VisualActorElement) {
			UseCaseKind[] usecaseKinds = UseCaseKind.values();
			elementKinds = new String[usecaseKinds.length];
			for(int i=0;i<usecaseKinds.length;i++){
				elementKinds[i] = usecaseKinds[i].getName();
			}
		} else if (visualElement instanceof VisualUseCaseElement) {
			ActorKind[] actorKinds = ActorKind.values();
			elementKinds = new String[actorKinds.length];
			for(int i=0;i<actorKinds.length;i++){
				elementKinds[i] = actorKinds[i].getName();
			}
		} else if (visualElement instanceof VisualGenericElement) {
			GenericType[] genericTypes = GenericType.values();
			elementKinds = new String[genericTypes.length];
			for(int i=0;i<genericTypes.length;i++){
				elementKinds[i] = genericTypes[i].getName();
			}
		}
		
		if (elementKinds != null && elementKinds.length > 0) {
			combo.setEnabled(true);
			combo.setItems(elementKinds);
		} else {
			combo.add("unspecified");
			combo.setEnabled(false);			
		}
		
		if (visualSpecificationElement == null && /*Temp*/ !(visualElement instanceof VisualGenericElement)) {
			combo.select(0);
			combo.setEnabled(false);
			return;
		}
		
		if (visualElement instanceof VisualSystemStructureActor) {
			VisualSystemStructureActor systemStructureActor = (VisualSystemStructureActor) visualElement;
			String str = systemStructureActor.getActorKind().toString();
			combo.select(getComboSelectionIndex(combo, str));
		} else if (visualElement instanceof VisualGenericElement) {
			VisualGenericElement generic = (VisualGenericElement) visualElement;
			String str = generic.getGenericType().toString();
			combo.select(getComboSelectionIndex(combo, str));
		} else if (visualSpecificationElement.getSpecificationElement() != null && visualSpecificationElement.getSpecificationElement().getElementKind() != null && !visualSpecificationElement.getSpecificationElement().getElementKind().isEmpty()) {
			String str = visualSpecificationElement.getSpecificationElement().getElementKind();
			combo.select(getComboSelectionIndex(combo, str));
		} else {
			combo.select(0);
		}
		
	}
	
	private void fillComboTransparency(Combo combo) {
		int alpha = 255;
	    if(visualElement.isIsSketchy() && figure.getSketchyFigure() != null) {
	    	alpha = figure.getSketchyFigure().getAlpha();
	    } else {
	    	if (figure.getFormalFigure() != null && figure.getFormalFigure().getAlpha() != null) {
	    		alpha = figure.getFormalFigure().getAlpha();	    		
	    	}
	    }
	    String str = "" + alphaToPercent(alpha);
	    for (int i = 0; i < combo.getItems().length; i++) {	    	
			if (str.equalsIgnoreCase(percentValue(combo.getItems()[i]))) {
				combo.select(i);
			}
		}
	}
	
	private int getComboSelectionIndex(Combo combo, String str) {
		for (int i = 0; i < combo.getItems().length; i++) {
			if (str.equalsIgnoreCase(combo.getItems()[i])) {
				return i;
			}
		}
		return 0;
	}
	
	private int alphaToPercent(int alpha) {
		return (int) Math.ceil((alpha / 255.0) * 100);
	}
	
	private int percentToAlpha(int percent) {
		return (int) (2.55f * percent);
	}
	
	private String percentValue(String percent) {
		String[] split = percent.split(" ");
		return split[0];
	}

}
