package dk.dtu.imm.red.visualmodeling.ui.contracts;

import org.eclipse.gef.requests.CreationFactory;

public abstract class AbstractCreationFactory {

	protected AbstractCreationFactory nextFactory;
	
	@SuppressWarnings("rawtypes")
	public final CreationFactory getCreationFactory(Class key)
	{
		CreationFactory returnValue = getCreationFactoryInternal(key);
		
		if(returnValue == null)
			returnValue = this.nextFactory.getCreationFactory(key);
		
		return returnValue;
	}
		
	/*
	 * Override to return CreationFactory.
	 * */	
	@SuppressWarnings("rawtypes")
	protected CreationFactory getCreationFactoryInternal(Class key)
	{
		return null;
	}
	
	public final AbstractCreationFactory setNext(AbstractCreationFactory next)
	{
		if(this.nextFactory == null)
			this.nextFactory = next;		
		return next;
	}	
}
