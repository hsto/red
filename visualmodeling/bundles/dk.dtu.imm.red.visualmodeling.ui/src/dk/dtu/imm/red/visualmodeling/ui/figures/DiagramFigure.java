package dk.dtu.imm.red.visualmodeling.ui.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Rectangle;

public class DiagramFigure extends Figure {

		
	  private Label label;
	  
	  FreeformLayer layer;
	  
	  public DiagramFigure() {
	    //setLayoutManager(new XYLayout());
	    	  	
	  	this.setBorder(new LineBorder(5));
		label = new Label();
		add(label);
	  }
	  

	  @Override 
	  protected void paintFigure(Graphics graphics) {
		  setConstraint(label, new Rectangle(5, 5, 100, 100));
	  }
	  
	  public Label getLabel() {
		    return label;
		  }

}
