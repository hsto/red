package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts;

import org.eclipse.draw2d.IFigure;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.figures.VisualSystemBoundaryFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualSystemBoundaryElementEditPart extends VisualElementEditPart{

	public VisualSystemBoundaryElementEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	@Override
	protected IFigure createFigure() {
		return new VisualSystemBoundaryFigure((IVisualElement)getModel());
	}
}
