package dk.dtu.imm.red.visualmodeling.ui.editors;


import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;

public interface INewDiagramWizardPresenter extends IWizardPresenter {

	void wizardFinished(String label, String name, String description, Group parent, String path, DiagramType type);
}
