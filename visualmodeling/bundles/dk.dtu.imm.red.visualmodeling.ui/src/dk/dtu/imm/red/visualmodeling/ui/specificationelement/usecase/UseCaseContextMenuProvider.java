package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.jface.action.IMenuManager;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualContextMenuProviderBase;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class UseCaseContextMenuProvider extends VisualContextMenuProviderBase{

	@Override
	protected void buildContextMenuInternal(IMenuManager menu, EditPartViewer viewer) {
		
		IVisualElement diagram = (IVisualElement)viewer.getContents().getModel();
		
		menu.add(new EffortAction(diagram));
		
	}

}
