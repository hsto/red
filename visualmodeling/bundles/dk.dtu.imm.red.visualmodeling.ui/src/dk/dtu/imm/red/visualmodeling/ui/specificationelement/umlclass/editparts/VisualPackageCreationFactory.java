package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures.VisualPackageFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualPackageElement;

public class VisualPackageCreationFactory implements CreationFactory {

	public VisualPackageCreationFactory()
	{

	}

	@Override
	public Object getNewObject() {
		VisualPackageElement element = ClassFactory.eINSTANCE.createVisualPackageElement();
		element.setBounds(VisualPackageFigure.initialBounds);

		return element;
	}

	@Override
	public Object getObjectType() {
		return VisualPackageElement.class;
	}



}
