package dk.dtu.imm.red.visualmodeling.ui;

import org.eclipse.gef.ui.actions.ActionBarContributor;
import org.eclipse.gef.ui.actions.DeleteRetargetAction;
import org.eclipse.gef.ui.actions.RedoRetargetAction;
import org.eclipse.gef.ui.actions.UndoRetargetAction;
import org.eclipse.gef.ui.actions.ZoomComboContributionItem;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.ui.actions.ActionFactory;
 
public class VisualModelingActionBarContributor extends ActionBarContributor {
 
  @Override
  protected void buildActions() {
    addRetargetAction(new UndoRetargetAction());
    addRetargetAction(new RedoRetargetAction());
    addRetargetAction(new DeleteRetargetAction());
  }
   
  @Override
  public void contributeToToolBar(IToolBarManager toolBarManager) {
    super.contributeToToolBar(toolBarManager);
    toolBarManager.add(getAction(ActionFactory.UNDO.getId()));
    toolBarManager.add(getAction(ActionFactory.REDO.getId()));
    toolBarManager.add(getAction(ActionFactory.DELETE.getId()));

    toolBarManager.add(new ZoomComboContributionItem(getPage()));
    
  }
 
  @Override
  protected void declareGlobalActionKeys() {

	  
  }
  
  @Override
	public void contributeToCoolBar(ICoolBarManager coolBarManager) {	
		super.contributeToCoolBar(coolBarManager);
		
		coolBarManager.add(getAction(ActionFactory.UNDO.getId()));
		coolBarManager.add(getAction(ActionFactory.REDO.getId()));
		coolBarManager.add(getAction(ActionFactory.DELETE.getId()));
	}
}