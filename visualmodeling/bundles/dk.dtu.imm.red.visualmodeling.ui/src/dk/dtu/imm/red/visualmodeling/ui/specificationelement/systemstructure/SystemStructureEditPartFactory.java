package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure;

import org.eclipse.gef.EditPart;

import dk.dtu.imm.red.visualmodeling.ui.factories.EditPartFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualPortEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemStructureActorEditPart;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;

public class SystemStructureEditPartFactory extends EditPartFactoryBase {

	RuleCollection ruleCollection;

	public SystemStructureEditPartFactory(RuleCollection ruleCollection) {
		super(ruleCollection);
		this.ruleCollection = ruleCollection;
	}

	public EditPart createEditPartInternal(EditPart context, Object model) {
		EditPart part = null;

		if (model instanceof VisualSystemStructureActor) {
			part = new VisualSystemStructureActorEditPart(ruleCollection);
		}
		else if (model instanceof VisualSystem) {
			part = new VisualSystemEditPart(ruleCollection);
		}
		else if (model instanceof VisualPort) {
			part = new VisualPortEditPart(ruleCollection);
		}


		return part;
	}
}
