package dk.dtu.imm.red.visualmodeling.ui.editpolicies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementRenameCommand;
import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;


public class VisualElementDirectEditPolicy extends DirectEditPolicy{

	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		VisualElementRenameCommand command = new VisualElementRenameCommand((IVisualElement)getHost().getModel(), (String)request.getCellEditor().getValue());
		
		return command;
	}

	@Override
	protected void showCurrentEditValue(DirectEditRequest request) {
		String value = (String)request.getCellEditor().getValue();
		((VisualElementFigure)getHostFigure()).getLabel().setText(value);
	}

}
