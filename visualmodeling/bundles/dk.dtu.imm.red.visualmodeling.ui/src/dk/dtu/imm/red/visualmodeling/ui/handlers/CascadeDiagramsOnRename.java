package dk.dtu.imm.red.visualmodeling.ui.handlers;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.impl.WorkspaceFactoryImpl;
import dk.dtu.imm.red.visualmodeling.ui.util.VisualModelHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;


/**
 * 
 * @author Luai
 *
 *	Issue #119
 *
 */
public class CascadeDiagramsOnRename extends AbstractHandler {
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		Workspace workspace = WorkspaceFactoryImpl.eINSTANCE.getWorkspaceInstance();		
		List<Diagram> diagrams = VisualModelHelper.getAllDiagrams(workspace);
		for (Diagram diagram : diagrams) {
			
			List<IVisualElement> diagramElements = VisualModelHelper.getAllDecendents(diagram.getVisualDiagram());
			EList<IVisualConnection> diagramConnections = diagram.getVisualDiagram().getDiagramConnections();
			
			for (IVisualElement e : diagramElements) {
				if (e instanceof VisualSpecificationElementImpl)
					updateVisualElement((VisualSpecificationElementImpl)e);
			}
			for (IVisualConnection con : diagramConnections) {
				if (con instanceof VisualConnectionImpl)
					updateVisualConnection((VisualConnectionImpl)con);
			}
		}
		
		
		return null;
	}
	
	
	private static void updateVisualElement(VisualSpecificationElementImpl e) {
		if (e.getSpecificationElement() != null && !e.getName().equals(e.getSpecificationElement().getName())) {
			e.setName(e.getSpecificationElement().getName());
		}
	}
	
	private static void updateVisualConnection(VisualConnectionImpl con) {
		if (con.getSpecificationElement() != null && !con.getName().equals(con.getSpecificationElement().getName()) ) {
			con.setName(con.getSpecificationElement().getName());
		}
	}

}
