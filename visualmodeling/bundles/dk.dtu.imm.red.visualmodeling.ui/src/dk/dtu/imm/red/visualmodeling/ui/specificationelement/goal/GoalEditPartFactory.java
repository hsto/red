package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal;

import org.eclipse.gef.EditPart;

import dk.dtu.imm.red.visualmodeling.ui.factories.EditPartFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalLayerEditPart;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer;

public class GoalEditPartFactory extends EditPartFactoryBase {

	public GoalEditPartFactory(RuleCollection ruleCollection) {
		super(ruleCollection);		
	}
	
	public EditPart createEditPartInternal(EditPart context, Object model) {

		EditPart part = null;

		if (model instanceof VisualGoalLayer) {
			part = new VisualGoalLayerEditPart(ruleCollection);
		} else if(model instanceof VisualGoalElement){
			return new VisualGoalElementEditPart(ruleCollection);
		}
		
		return part;			
	}
}