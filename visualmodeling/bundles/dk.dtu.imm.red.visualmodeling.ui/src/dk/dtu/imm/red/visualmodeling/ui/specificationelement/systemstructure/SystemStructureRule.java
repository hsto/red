package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure;

import dk.dtu.imm.red.visualmodeling.ui.validation.IRule;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;

public class SystemStructureRule implements IRule{

	@Override
	public boolean canConnect(IVisualElement source, IVisualElement target,
			IVisualConnection connection) {
		return true;
	}

	@Override
	public boolean canAccept(IVisualElement parent, IVisualElement child) {
		if(parent instanceof VisualSystem)
			return true;
		return false;
	}

}
