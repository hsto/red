package dk.dtu.imm.red.visualmodeling.ui.figures;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ImageFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.visualmodeling.ui.Activator;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public abstract class VisualElementFigure extends Figure implements MouseListener {

	protected IVisualElement model;
	protected ConnectionAnchor connectionAnchor;
	protected Label nameLabel;
	protected RectangleFigure boundingRectangle;
	protected IFigure activeArea;

	// Added by Luai
	protected Shape formalFigure;
	protected SVGFigure sketchyFigure;

	protected ImageFigure linkedIcon = new ImageFigure(Activator.getImageFromRegistry(Activator.LINKED_ICON));

	// protected static Font DEFAULT_FONT = new Font( Display.getCurrent(), new
	// FontData( "Tahoma", 10, SWT.NORMAL));
	// protected static Font SKETCHY_FONT = new Font( Display.getCurrent(),
	// "Bradley Hand ITC", 10, SWT.NORMAL);
	public Font DEFAULT_FONT = new Font(Display.getCurrent(), new FontData("Tahoma", 10, SWT.NORMAL));
	public Font SKETCHY_FONT = new Font(Display.getCurrent(), "Bradley Hand ITC", 10, SWT.NORMAL);

	protected static String svgPath = "platform:/plugin/dk.dtu.imm.red.visualmodeling.ui/icons/svg/";

	public VisualElementFigure(IVisualElement model) {
		this.model = model;

		boundingRectangle = new RectangleFigure();
		nameLabel = new Label();

		setLayoutManager(new XYLayout());
		this.setBounds(new Rectangle(model.getLocation(), model.getBounds()));

		add(nameLabel);
		connectionAnchor = new ChopboxAnchor(this);

		activeArea = this;

		super.addMouseListener(this);
	}

	public IFigure getActiveArea() {
		return activeArea;
	}

	public Shape getFormalFigure() {
		return formalFigure;
	}

	public SVGFigure getSketchyFigure() {
		return sketchyFigure;
	}

	@Override
	protected void paintFigure(Graphics graphics) {

		Rectangle r = getBounds().getCopy();

		if (this.getChildren().contains(nameLabel)) {
			setConstraint(nameLabel, new Rectangle(0, 0, r.width, r.height));
		}

		if (this.getChildren().contains(linkedIcon)) {
			setConstraint(linkedIcon, new Rectangle(0, 0, 15, 15));
		}

	}

	public Label getLabel() {
		return nameLabel;
	}

	public ConnectionAnchor getConnectionAnchor() {
		return connectionAnchor;
	}

	public void refreshVisuals() {
		nameLabel.setText(model.getName());
		
		if(isLinked()) {
			add(linkedIcon);
		}
		else {
			if(this.getChildren().contains(linkedIcon))
				remove(linkedIcon);
		}
	}
	
	protected boolean isLinked() {
		if (model instanceof VisualSpecificationElement) {
			if (((VisualSpecificationElement)model).getSpecificationElement() != null) {
				return true;
			}
		}
		
		return false;
	}

	public void mousePressed(MouseEvent me) {
	}

	public void mouseReleased(MouseEvent me) {
	}

	public void mouseDoubleClicked(MouseEvent me) {
	}

}
