package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts;

import org.eclipse.draw2d.IFigure;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.figures.VisualSystemStructureActorFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualSystemStructureActorEditPart extends VisualElementEditPart{

	public VisualSystemStructureActorEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	@Override
	protected IFigure createFigure() {
		return new VisualSystemStructureActorFigure((VisualSpecificationElement)getModel());
	}

}
