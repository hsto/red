package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.ui.IEditorPart;

public class AutoLayoutGridLayoutAction extends AutoLayoutAction {

    public AutoLayoutGridLayoutAction(IEditorPart editor) {
		super(editor, LayoutAlgorithmType.GRID_LAYOUT);
	}

}
