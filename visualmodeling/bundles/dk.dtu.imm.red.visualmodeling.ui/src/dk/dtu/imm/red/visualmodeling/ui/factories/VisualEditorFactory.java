package dk.dtu.imm.red.visualmodeling.ui.factories;

import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.PicoContainer;
import org.picocontainer.behaviors.Intercepting;
import org.picocontainer.parameters.ComponentParameter;
import org.picocontainer.parameters.ConstantParameter;

import dk.dtu.imm.red.visualmodeling.ui.VisualEditor;
import dk.dtu.imm.red.visualmodeling.ui.contracts.IVisualEditorConfiguration;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.ActivityConfiguration;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericConfiguration;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.GoalConfiguration;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.StateMachineConfiguration;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.stitch.StitchConfiguration;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.SystemStructureConfiguration;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.ClassConfiguration;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.UseCaseConfiguration;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecasemap.UseCaseMapConfiguration;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;

public class VisualEditorFactory {

	private MutablePicoContainer pico;

	public VisualEditor createVisualEditor(DiagramType diagramType) {
		pico = new DefaultPicoContainer(new Intercepting());

		pico.addComponent(VisualEditor.class);

		switch (diagramType) {
		case GENERIC:
			pico.addComponent(IVisualEditorConfiguration.class,
					GenericConfiguration.class);
			break;
		case GOAL:
			pico.addComponent(IVisualEditorConfiguration.class,
					GoalConfiguration.class);
			break;
		case USE_CASE:
			pico.addComponent(IVisualEditorConfiguration.class,
					UseCaseConfiguration.class);
			break;
		case USE_CASE_MAP:
			pico.addComponent(IVisualEditorConfiguration.class,
					UseCaseMapConfiguration.class);
			break;
		case CLASS:
			pico.addComponent(IVisualEditorConfiguration.class,
					ClassConfiguration.class);
			break;
		case ACTIVITY:
			pico.addComponent(IVisualEditorConfiguration.class,
					ActivityConfiguration.class);
			break;
		case STATE_MACHINE:
			pico.addComponent(IVisualEditorConfiguration.class,
					StateMachineConfiguration.class);
			break;
		case SYSTEM_STRUCTURE:
			pico.addComponent(IVisualEditorConfiguration.class,
					SystemStructureConfiguration.class);
			break;
		case STITCH:
			pico.addComponent(IVisualEditorConfiguration.class,
					StitchConfiguration.class);
			break;
		default:
			// TODO throw some exception
			break;
		}

		PicoContainer child = pico.makeChildContainer();

		pico.addComponent(SpecificationElementCreationFactoryManager.class,
				SpecificationElementCreationFactoryManager.class,
				new ConstantParameter(child), new ComponentParameter());

		return pico.getComponent(VisualEditor.class);
	}
}
