package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.figures;

import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.EllipseAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;
import org.eclipse.swt.graphics.Pattern;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualUseCaseElementFigure extends VisualElementFigure{

	public static Dimension initialBounds = new Dimension(150,50);

	private String sketchyFigureURL = svgPath + "Usecase/UseCase.svg";

	public VisualUseCaseElementFigure(VisualSpecificationElement model)
	{
		super(model);

		this.connectionAnchor = new EllipseAnchor(this);

		setLayoutManager(new XYLayout());

		formalFigure = new Ellipse() {
	    	@Override
	    	public void paintFigure(Graphics graphics) {
	    		Rectangle r = getBounds();
	    		graphics.setBackgroundPattern(new Pattern(Display.getCurrent(), r.x, r.y, r.x + r.width, r.y + r.height,
	    				ColorHelper.white, ColorHelper.useCaseDiagramDefaultColor));
	    		super.paintFigure(graphics);
	    	}
	    };
		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);

	    refreshVisuals();
	}

	@Override public boolean containsPoint(int x, int y) {
		return getBounds().contains(x, y);
	};

	@Override
	  protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		
		Rectangle r = getBounds().getCopy();

		formalFigure.setFill(true);
		formalFigure.setOutline(true);

		boundingRectangle.setVisible(false);

		if(this.getChildren().contains(sketchyFigure))
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		if(this.getChildren().contains(formalFigure))
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
	  }



	@Override
	public void refreshVisuals()
	{
		super.refreshVisuals();
		
		//first remove the main figure
		if(this.getChildren().contains(formalFigure))
			remove(formalFigure);
		if(this.getChildren().contains(sketchyFigure))
			remove(sketchyFigure);

		//toggling between sketchy and non sketchy
		if(model.isIsSketchy()){
			add(sketchyFigure,0);
			//the main figure always needs to be added in front (index 0 so that it won't block the children)
			nameLabel.setFont(SKETCHY_FONT);
		}
		else{
			add(formalFigure,0);
			//the main figure always needs to be added in front (index 0 so that it won't block the children)
			nameLabel.setFont(DEFAULT_FONT);
		}
	}
}
