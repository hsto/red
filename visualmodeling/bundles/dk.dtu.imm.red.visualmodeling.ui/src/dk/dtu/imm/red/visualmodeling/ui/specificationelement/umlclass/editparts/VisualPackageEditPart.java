package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures.VisualPackageFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualPackageEditPart extends VisualElementEditPart {

	private Adapter adapter;

	public VisualPackageEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

		adapter = new VisualPackageAdapter();
	}

	@Override
	protected IFigure createFigure() {
		return new VisualPackageFigure((IVisualElement) getModel());
	}

	@Override
	public void activate() {
		if (!isActive()) {
			((IVisualElement) getModel()).eAdapters().add(adapter);
		}
		super.activate();
	}

	@Override
	public void deactivate() {
		if (isActive()) {
			((IVisualElement) getModel()).eAdapters().remove(adapter);
		}

		super.deactivate();
	}

	@Override
	public void setModel(Object model) {
		super.setModel(model);
	};

	/*
	 * Inner class for notifications
	 */
	public class VisualPackageAdapter implements Adapter {

		// TODO should happen in the EMF package layer model

		public void notifyChanged(Notification notification) {

		}

		public Notifier getTarget() {
			return (IVisualElement) getModel();
		}

		public void setTarget(Notifier newTarget) {
			// Do nothing.
		}

		public boolean isAdapterForType(Object type) {
			return type.equals(IVisualElement.class);
		}

	}
}
