package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.ui.IEditorPart;

public class AutoLayoutVerticalLayoutAction extends AutoLayoutAction {

    public AutoLayoutVerticalLayoutAction(IEditorPart editor) {
		super(editor, LayoutAlgorithmType.VERTICAL_LAYOUT);
	}

}
