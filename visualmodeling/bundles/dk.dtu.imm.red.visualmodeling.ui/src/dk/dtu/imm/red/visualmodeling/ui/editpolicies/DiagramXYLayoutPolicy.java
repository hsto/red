package dk.dtu.imm.red.visualmodeling.ui.editpolicies;

import java.util.List;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ResizableEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.handles.AbstractHandle;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementCreateCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementMoveCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

// UNDONE delete if not used
public class DiagramXYLayoutPolicy extends XYLayoutEditPolicy {

	@Override
	protected Command createChangeConstraintCommand(
			ChangeBoundsRequest request, EditPart child, Object constraint) {
		return createChangeConstraintCommand(child, constraint);
	}

	@Override
	protected Command createChangeConstraintCommand(EditPart child,
			Object constraint) {
		VisualElementMoveCommand command = new VisualElementMoveCommand(
				(IVisualElement) child.getModel(), (Rectangle) constraint);
		return command;
	}

	@Override
	protected Command getCreateCommand(CreateRequest request) {

		Command retVal = null;
		if (request.getNewObjectType().equals(VisualElement.class)) {
			IVisualElement parent = (IVisualElement) getHost().getModel();
			VisualDiagram diagram = parent.getDiagram();
			IVisualElement newElement = (IVisualElement) request.getNewObject();
			VisualElementCreateCommand command = new VisualElementCreateCommand(
					diagram, parent, newElement, request.getLocation());
			retVal = command;
		}

		return retVal;
	}

	@Override
	@SuppressWarnings("rawtypes")
	protected EditPolicy createChildEditPolicy(EditPart child) {
		return new ResizableEditPolicy() {
			@Override
			protected List createSelectionHandles() {
				List l = super.createSelectionHandles();
				for (Object obj : l) {
					if (obj instanceof AbstractHandle) {
						AbstractHandle handle = (AbstractHandle) obj;
						Border border = handle.getBorder();
						if (border instanceof LineBorder) {
							LineBorder lborder = (LineBorder) border;
							lborder.setColor(ColorConstants.red);
						}
					}
				}

				return l;
			}
		};
	}

}
