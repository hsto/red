package dk.dtu.imm.red.visualmodeling.ui.editors.impl;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;
import dk.dtu.imm.red.visualmodeling.ui.editors.INewDiagramWizard;
import dk.dtu.imm.red.visualmodeling.ui.editors.INewDiagramWizardPresenter;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;

public class NewDiagramWizardImpl extends BaseNewWizard implements INewDiagramWizard {

	private INewDiagramWizardPresenter presenter;
	private DiagramOptionWizardPage diagramTypePage;

	public NewDiagramWizardImpl() {
		super("Diagram", Diagram.class);
		presenter = new NewDiagramWizardPresenter(this);
	}

	@Override
	public IWizardPresenter getPresenter() {
		return presenter;
	}

	@Override
	public void addPages() {

		diagramTypePage = new DiagramOptionWizardPage("Diagram Type");

		this.addPage(diagramTypePage); // Add it first
		super.addPages();
	}

	@Override
	public boolean performFinish() {

		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription();
		DiagramType type = diagramTypePage.getDiagramType();

		Group parent = null;

		if (!displayInfoPage.getSelection().isEmpty() && displayInfoPage.getSelection() instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) displayInfoPage.getSelection();

			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}

		presenter.wizardFinished(label, name, description, parent,"", type);

		return true;
	}

	@Override
	public boolean canFinish() {
		return super.canFinish()
				&& elementBasicInfoPage.isPageComplete()
				&& displayInfoPage.isPageComplete()
				&& !displayInfoPage.getSelection().isEmpty();
	}
}
