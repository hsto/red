package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.List;

import org.eclipse.gef.commands.Command;

public class MultipleCommand extends Command {
	
	private List<Command> commands;
	
	public MultipleCommand(List<Command> commands) {
		this.commands = commands;
	}
	
	@Override
	public void execute() {
		for (Command command : commands) {
			try { 
				command.execute();				
			} catch (Exception e) { // Used so creation SE into Diagrams doesn't stop
//				MessageDialog md = new MessageDialog(Display
//						.getCurrent().getActiveShell(), "", null,
//						e.getMessage(),
//						MessageDialog.ERROR,
//						new String[] { "ok" }, 0);			
//				md.open();
			}
		}
	}
	
	@Override
	public void undo() {
		for (Command command : commands) {
			try {
				command.undo();				
			} catch (Exception e) {
				
			}
		}
	}

}
