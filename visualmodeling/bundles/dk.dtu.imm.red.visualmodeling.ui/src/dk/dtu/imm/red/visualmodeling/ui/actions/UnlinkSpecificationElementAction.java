package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionUnlinkCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualSpecificationElementUnlinkCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class UnlinkSpecificationElementAction extends SelectionAction{

  	public static final String SPECIFICATION_ELEMENT_UNLINK = "SpecificationElementUnlink";
   	public static final String REQ_SPECIFICATION_ELEMENT_UNLINK = "SpecificationElementUnlink";
	
	public UnlinkSpecificationElementAction(IWorkbenchPart part) {
		super(part);
		
        setId(SPECIFICATION_ELEMENT_UNLINK);
        setText("Unlink Element");
	}

	@Override
	@SuppressWarnings("rawtypes")
	protected boolean calculateEnabled() {
		List selection = getSelectedObjects();
		
		if(selection.size() > 0)
		{
			Object model = ((EditPart)selection.get(0)).getModel();
	    	
			if(model instanceof VisualSpecificationElement)
	    	{
				VisualSpecificationElement element = (VisualSpecificationElement)model;
	    		return element.getSpecificationElement() != null;
	    	}
			else if (model instanceof VisualConnection) {
				VisualConnection connection = (VisualConnection) model;
				return connection.getSpecificationElement() != null;
			}
		}
		return false;
	}
	
    @Override
    public void run() {
    	EditPart editPart = (EditPart)getSelectedObjects().get(0);
    	Object model = editPart.getModel();
    	
    	if (model instanceof VisualSpecificationElement) {    	
    		VisualSpecificationElement element = (VisualSpecificationElement)model;
        	Command command = new VisualSpecificationElementUnlinkCommand(element);
        	
        	this.execute(command);
        	
    	} else if (model instanceof VisualConnection) {
    		VisualConnection connection = (VisualConnection)model;
        	Command command = new VisualConnectionUnlinkCommand(connection);
        	
        	this.execute(command);
    	}
    	
    	update();
    	
    }
}
