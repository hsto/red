package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualActivityFinalNodeFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActivityFinalNode;

public class VisualActivityFinalNodeFactory implements CreationFactory {
	public Object getNewObject() {

		VisualActivityFinalNode element = ActivityFactory.eINSTANCE.
				createVisualActivityFinalNode();
		element.setBounds(VisualActivityFinalNodeFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualActivityFinalNode.class;
	}
}
