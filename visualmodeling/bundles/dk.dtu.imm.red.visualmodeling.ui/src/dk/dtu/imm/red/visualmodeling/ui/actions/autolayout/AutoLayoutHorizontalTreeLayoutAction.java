package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.ui.IEditorPart;

public class AutoLayoutHorizontalTreeLayoutAction extends AutoLayoutAction {

    public AutoLayoutHorizontalTreeLayoutAction(IEditorPart editor) {
		super(editor, LayoutAlgorithmType.HORIZONTAL_TREE_LAYOUT);
	}

}
