package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualGoalElementFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(150,50);
	//private PolygonShape formalFigure;

	//private SVGFigure sketchyFigure;

	private String sketchyFigureURL = svgPath + "Goal/Goal.svg";


	public VisualGoalElementFigure(VisualSpecificationElement model)
	{
		super(model);

	    formalFigure = new PolygonShape();
	    formalFigure.setFill(true);
	    formalFigure.setBackgroundColor(ColorHelper.goalDiagramDefaultColor);
	    formalFigure.setForegroundColor(ColorHelper.lightGreen);

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);

		add(this.nameLabel);
	    refreshVisuals();
	}

	@Override public boolean containsPoint(int x, int y) {
		return getBounds().contains(x, y);
	};

	@Override
	  protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		
		Rectangle r = getBounds().getCopy();

//		boundingRectangle.setVisible(false);

		if(this.getChildren().contains(sketchyFigure)){
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		}
		if(this.getChildren().contains(formalFigure)){
			((PolygonShape)formalFigure).removeAllPoints();
			((PolygonShape)formalFigure).setStart(new Point(15, 0));
			((PolygonShape)formalFigure).addPoint(new Point(r.width-1, 0));
			((PolygonShape)formalFigure).addPoint(new Point(r.width - 15, r.height-1));
			((PolygonShape)formalFigure).addPoint(new Point(0, r.height-1));
			((PolygonShape)formalFigure).setEnd(new Point(0, r.height-1));
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
		}
	  }

	@Override
	public void refreshVisuals()
	{
		super.refreshVisuals();
		
		if(this.getChildren().contains(formalFigure))
			remove(formalFigure);
		if(this.getChildren().contains(sketchyFigure))
			remove(sketchyFigure);

		//toggling between sketchy and non sketchy
		if(model.isIsSketchy()){
			add(sketchyFigure,0);
			nameLabel.setFont(SKETCHY_FONT);
		}
		else{
			add(formalFigure,0);
			nameLabel.setFont(DEFAULT_FONT);
		}
	}
}
