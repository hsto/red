package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine;

import dk.dtu.imm.red.visualmodeling.ui.validation.IRule;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState;

public class StateMachineRule implements IRule{

	@Override
	public boolean canConnect(IVisualElement source, IVisualElement target,
			IVisualConnection connection) {
		return true;
	}

	@Override
	public boolean canAccept(IVisualElement parent, IVisualElement child) {
		if(parent instanceof VisualState){
			return true;
		}
		else if (parent instanceof VisualDiagram) {
			return true;
		}
		return false;
	}

}
