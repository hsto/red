package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualForkJoinStateFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualForkJoinState;

public class VisualForkJoinStateFactory implements CreationFactory {
	public Object getNewObject() {

		VisualForkJoinState element = StatemachineFactory.eINSTANCE.
				createVisualForkJoinState();
		element.setBounds(VisualForkJoinStateFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualForkJoinState.class;
	}
}
