package dk.dtu.imm.red.visualmodeling.ui.factories;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;



public class DiagramElementFactory implements CreationFactory {

	public Object getNewObject() {
	    return visualmodelFactory.eINSTANCE.createVisualElement();
	  }
	 

	  public Object getObjectType() {
	    return VisualElement.class;	   
	  }	
}
