package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualSendStateFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualSendState;

public class VisualSendStateFactory implements CreationFactory {
	public Object getNewObject() {

		VisualSendState element = StatemachineFactory.eINSTANCE
				.createVisualSendState();
		element.setBounds(VisualSendStateFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualSendState.class;
	}
}
