package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;


public class VisualElementDeleteCommand extends Command{

	protected IVisualElement parent;
	protected IVisualElement element;
	protected VisualDiagram diagram;
	protected List<Command> commands;
	
	public VisualElementDeleteCommand(IVisualElement element)
	{		
		this.element = element;
		this.parent = element.getParent();
		this.diagram = element.getDiagram();
		commands = new ArrayList<Command>();
	}
		
	@Override
	public boolean canExecute() {
		return true;
	}
	
	@Override
	  public void execute() {
		
			List<IVisualConnection> connections = new ArrayList<IVisualConnection>(element.getConnections());
			
			for(IVisualConnection connection : connections)
			{
				Command command = new VisualConnectionDeleteCommand(connection);
				command.execute();
				commands.add(command);
			}
			
			element.setDiagram(null);
			element.setParent(null);	
			parent.getElements().remove(element);			
	  }
	 
	  @Override
	  public void undo() {		  
		  parent.getElements().add(element);
		  element.setParent(parent);
		  element.setDiagram(diagram);
		  
		  for(Command command : commands)
			{				
				command.undo();
			}
	  }
	
}
