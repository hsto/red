package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualShallowHistoryStateFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualShallowHistoryState;

public class VisualShallowHistoryStateFactory implements CreationFactory {
	public Object getNewObject() {

		VisualShallowHistoryState element = StatemachineFactory.eINSTANCE.
				createVisualShallowHistoryState();
		element.setBounds(VisualShallowHistoryStateFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualShallowHistoryState.class;
	}
}
