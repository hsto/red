package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.draw2d.IFigure;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualDeepHistoryStateFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualDeepHistoryStateEditPart extends VisualElementEditPart{

	public VisualDeepHistoryStateEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	@Override
	protected IFigure createFigure() {
		return new VisualDeepHistoryStateFigure((IVisualElement)getModel());
	}

}
