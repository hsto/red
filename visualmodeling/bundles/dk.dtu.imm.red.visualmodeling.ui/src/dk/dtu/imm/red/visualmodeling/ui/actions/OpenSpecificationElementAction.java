package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualConnectionEditPart;
import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

/*
 * Opens the editor for a selected specification element.
 * Finds all extensions to the extension point "dk.dtu.imm.red.core.element".
 * */
public class OpenSpecificationElementAction extends SelectionAction{

  	public static final String SPECIFICATION_ELEMENT_OPEN = "SpecificationElementOpenAction";
   	public static final String REQ_SPECIFICATION_ELEMENT_OPEN = "SpecificationElementOpenAction";
 
    Request request;
 
    public OpenSpecificationElementAction(IWorkbenchPart part) {
        super(part);
        setId(SPECIFICATION_ELEMENT_OPEN);
        setText("Open Element");
        request = new Request(REQ_SPECIFICATION_ELEMENT_OPEN);
    }
 

    @Override
    public void run() {
    	Object selected = getSelectedObjects().get(0);
    	
    	if(selected instanceof VisualElementEditPart)
    	{
    		((VisualElementEditPart)selected).openElement();
    	}	    
    	else if (selected instanceof VisualConnectionEditPart) {
    		((VisualConnectionEditPart)selected).openElement();
    	}
    }
 

    @Override
	@SuppressWarnings("rawtypes")
    protected boolean calculateEnabled() {	
    	List selectionList = getSelectedObjects();
    	
    	if(selectionList.size() != 1)
    		return false;
    	
    	Object selection = ((EditPart)selectionList.get(0)).getModel();
    	
    	if (selection instanceof VisualSpecificationElement)
    	{
    		return ((VisualSpecificationElement)selection).getSpecificationElement() != null;	    		
    	}
    	else if (selection instanceof VisualConnection) 
    	{
    		return ((VisualConnection)selection).getSpecificationElement() != null;
    	}
	    		    	
        return false;
    }
}
