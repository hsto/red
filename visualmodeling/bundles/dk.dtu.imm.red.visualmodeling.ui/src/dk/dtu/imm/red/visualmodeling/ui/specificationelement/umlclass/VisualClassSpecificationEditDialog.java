package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.specificationelements.modelelement.datatype.util.DataTypeParser;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.util.MultiplicityParser;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.commands.VisualClassUpdateCommand;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualClassEditPart;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.impl.VisualClassElementImpl;

public class VisualClassSpecificationEditDialog extends Dialog {
	private VisualClassElement model;
	private VisualClassElement modelCopy;

	private VisualClassEditPart editPart;

	private Button btnIsInterface;
	private Button btnIsAbstract;

	private Table tableAttributes;
	private Table tableOperations;
	private TableViewer tableViewerAttributes;
	private TableViewer tableViewerOperations;

	private TableViewerColumn tvClmnAttrVisibility;
	private TableViewerColumn tvClmnAttrName;
	private TableViewerColumn tvClmnAttrType;
	private TableViewerColumn tvClmnAttrMultiplicity;
	private TableViewerColumn tvClmnAttrReadOnly;

	private TableViewerColumn tvClmnOpVisibility;
	private TableViewerColumn tvClmnOpName;
	private TableViewerColumn tvClmnOpParameters;
	private TableViewerColumn tvClmnOpReturnType;
	private TableViewerColumn tvClmnOpMultiplicity;
	private Text textName;

	/**
	 * Create the dialog.
	 *
	 * @param parentShell
	 */
	public VisualClassSpecificationEditDialog(Shell parentShell,
			Object inputModel, VisualClassEditPart editPart) {
		super(parentShell);
		setShellStyle(SWT.MAX | SWT.RESIZE);
		if (inputModel instanceof VisualClassElementImpl) {
			this.model = (VisualClassElement) inputModel;
		} else {
			this.model = ClassFactory.eINSTANCE.createVisualClassElement();
		}

		//copy without references
		EcoreUtil.Copier copier = new EcoreUtil.Copier(true,false);
		this.modelCopy = (VisualClassElement) copier.copy(this.model);
		this.editPart = editPart;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Class Specification");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, false));

		Label lblName = new Label(container, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
				1, 1));
		lblName.setText("Name");

		textName = new Text(container, SWT.BORDER);
		GridData gd_textName = new GridData(SWT.LEFT, SWT.CENTER, true, false,
				1, 1);
		gd_textName.widthHint = 297;
		textName.setLayoutData(gd_textName);
		textName.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent fe) {
			}

			@Override
			public void focusLost(FocusEvent fe) {
				modelCopy.setName(textName.getText());
			}

		});

		btnIsInterface = new Button(container, SWT.CHECK);
		btnIsInterface.setText("Is Interface");

		btnIsAbstract = new Button(container, SWT.CHECK);
		btnIsAbstract.setText("Is Abstract");

		btnIsInterface.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Button btn = (Button) event.getSource();
				if (btn.getSelection()) {
					modelCopy.setInterface(true);
				} else {
					modelCopy.setInterface(false);
				}
			}
		});
		btnIsAbstract.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Button btn = (Button) event.getSource();
				if (btn.getSelection()) {
					modelCopy.setAbstract(true);

				} else {
					modelCopy.setAbstract(false);
				}
			}
		});

		Label label = new Label(container, SWT.NONE);
		label.setText("New Label");
		new Label(container, SWT.NONE);

		// ---------------- ATTRIBUTES SECTION ------------------------------
		Label lblAttributes = new Label(container, SWT.NONE);
		lblAttributes.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false,
				false, 1, 1));
		lblAttributes.setText("Attributes:");
		new Label(container, SWT.NONE);

		// ScrolledComposite scrolledCompAttributes = new ScrolledComposite(
		// container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		// scrolledCompAttributes.setLayoutData(new GridData(SWT.FILL,
		// SWT.CENTER,
		// true, true, 2, 1));
		// scrolledCompAttributes.setExpandHorizontal(true);
		// scrolledCompAttributes.setExpandVertical(true);

		tableViewerAttributes = new TableViewer(container, SWT.BORDER
				| SWT.FULL_SELECTION | SWT.MULTI);
		tableAttributes = tableViewerAttributes.getTable();
		GridData gd_tableAttributes = new GridData(SWT.FILL, SWT.CENTER, true,
				true, 2, 1);
		gd_tableAttributes.heightHint = 81;
		tableAttributes.setLayoutData(gd_tableAttributes);
		tableAttributes.setSize(new Point(341, 100));
		tableAttributes.setLinesVisible(true);
		tableAttributes.setHeaderVisible(true);

		tvClmnAttrVisibility = new TableViewerColumn(tableViewerAttributes,
				SWT.NONE);
		TableColumn tblclmnAttrVisibility = tvClmnAttrVisibility.getColumn();
		tblclmnAttrVisibility.setWidth(80);
		tblclmnAttrVisibility.setText("Visibility");

		tvClmnAttrName = new TableViewerColumn(tableViewerAttributes, SWT.NONE);
		TableColumn tblclmnAttrName = tvClmnAttrName.getColumn();
		tblclmnAttrName.setWidth(150);
		tblclmnAttrName.setText("Name");

		tvClmnAttrType = new TableViewerColumn(tableViewerAttributes, SWT.NONE);
		TableColumn tblclmnAttrType = tvClmnAttrType.getColumn();
		tblclmnAttrType.setWidth(100);
		tblclmnAttrType.setText("Type");

		tvClmnAttrMultiplicity = new TableViewerColumn(tableViewerAttributes,
				SWT.NONE);
		TableColumn tblclmnAttrMultiplicity = tvClmnAttrMultiplicity
				.getColumn();
		tblclmnAttrMultiplicity.setWidth(80);
		tblclmnAttrMultiplicity.setText("Multiplicity");

		tvClmnAttrReadOnly = new TableViewerColumn(tableViewerAttributes,
				SWT.NONE);
		TableColumn tblclmnReadonly = tvClmnAttrReadOnly.getColumn();
		tblclmnReadonly.setWidth(80);
		tblclmnReadonly.setText("readOnly");

		// scrolledCompAttributes.setContent(tableAttributes);
		// scrolledCompAttributes.setMinSize(new Point(341, 100));

		Button btnAddAttribute = new Button(container, SWT.NONE);
		btnAddAttribute.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				createNewAttribute();
				tableViewerAttributes.refresh();
				editPart.refresh();
			}

		});
		btnAddAttribute.setText("Add Attribute");

		Button btnDeleteAttribute = new Button(container, SWT.NONE);
		btnDeleteAttribute.setText("Delete Attribute");
		btnDeleteAttribute.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				int[] selections = tableAttributes.getSelectionIndices();
				IVisualElement[] attrToRemove = new VisualClassAttribute[selections.length];
				for (int i = 0; i < selections.length; i++) {
					attrToRemove[i] = modelCopy.getAttributes().get(selections[i]);

				}
				for (int i = 0; i < attrToRemove.length; i++) {
					modelCopy.getElements().remove(attrToRemove[i]);
					modelCopy.getAttributes().remove(attrToRemove[i]);
				}
				tableViewerAttributes.refresh();
				editPart.refresh();
			}
		});

		// ---------------- OPERATIONS SECTION ------------------------------
		Label lblOperations = new Label(container, SWT.NONE);
		lblOperations.setText("Operations:");
		new Label(container, SWT.NONE);

		// ScrolledComposite scrolledCompOperations = new ScrolledComposite(
		// container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		// scrolledCompOperations.setLayoutData(new GridData(SWT.FILL,
		// SWT.CENTER,
		// true, true, 2, 1));
		// scrolledCompOperations.setExpandHorizontal(true);
		// scrolledCompOperations.setExpandVertical(true);

		tableViewerOperations = new TableViewer(container, SWT.BORDER
				| SWT.FULL_SELECTION | SWT.MULTI);
		tableOperations = tableViewerOperations.getTable();
		GridData gd_tableOperations = new GridData(SWT.FILL, SWT.CENTER, true,
				true, 2, 1);
		gd_tableOperations.heightHint = 102;
		tableOperations.setLayoutData(gd_tableOperations);
		tableOperations.setLinesVisible(true);
		tableOperations.setHeaderVisible(true);

		tvClmnOpVisibility = new TableViewerColumn(tableViewerOperations,
				SWT.NONE);
		TableColumn tblclmnOpVisibility = tvClmnOpVisibility.getColumn();
		tblclmnOpVisibility.setWidth(80);
		tblclmnOpVisibility.setText("Visibility");

		tvClmnOpName = new TableViewerColumn(tableViewerOperations, SWT.NONE);
		TableColumn tblclmnOpName = tvClmnOpName.getColumn();
		tblclmnOpName.setWidth(150);
		tblclmnOpName.setText("Name");

		tvClmnOpParameters = new TableViewerColumn(tableViewerOperations,
				SWT.NONE);
		TableColumn tblclmnOpParameters = tvClmnOpParameters.getColumn();
		tblclmnOpParameters.setWidth(300);
		tblclmnOpParameters.setText("Parameters");

		tvClmnOpReturnType = new TableViewerColumn(tableViewerOperations,
				SWT.NONE);
		TableColumn tblclmnOpReturntype = tvClmnOpReturnType.getColumn();
		tblclmnOpReturntype.setWidth(100);
		tblclmnOpReturntype.setText("ReturnType");

		tvClmnOpMultiplicity = new TableViewerColumn(tableViewerOperations,
				SWT.NONE);
		TableColumn tblclmnOpMultiplicity = tvClmnOpMultiplicity.getColumn();
		tblclmnOpMultiplicity.setWidth(80);
		tblclmnOpMultiplicity.setText("Multiplicity");
		// scrolledCompOperations.setContent(tableOperations);
		// scrolledCompOperations.setMinSize(new Point(641, 100));

		Button btnAddOperation = new Button(container, SWT.NONE);
		btnAddOperation.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				createNewOperation();
				tableViewerOperations.refresh();
				editPart.refresh();
			}
		});
		btnAddOperation.setText("Add Operation");

		Button btnRemoveOperation = new Button(container, SWT.NONE);
		btnRemoveOperation.setText("Remove Operation");
		btnRemoveOperation.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				int[] selections = tableOperations.getSelectionIndices();
				IVisualElement[] opToRemove = new VisualClassOperation[selections.length];
				for (int i = 0; i < selections.length; i++) {
					opToRemove[i] = modelCopy.getOperations().get(selections[i]);

				}
				for (int i = 0; i < opToRemove.length; i++) {
					modelCopy.getElements().remove(opToRemove[i]);
					modelCopy.getOperations().remove(opToRemove[i]);
				}
				tableViewerOperations.refresh();
				editPart.refresh();
			}
		});

		setTableAttributeCellEditorAndViewer();
		setTableOperationCellEditorAndViewer();
		loadInitialData();

		return container;
	}

	private void setTableAttributeCellEditorAndViewer() {
		// Attribute Table - Column Visibility
		tvClmnAttrVisibility.setEditingSupport(new EditingSupport(
				tableViewerAttributes) {
			
			ComboBoxCellEditor combo = new ComboBoxCellEditor(tableAttributes,
					getComboItems());

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return combo;
			}

			protected Object getValue(Object element) {
				// use index, so can just use the enum value
				return ((VisualClassAttribute) element).getVisibility()
						.getValue();
			}

			protected void setValue(Object element, Object value) {
				VisualClassAttribute attr = (VisualClassAttribute) element;
				attr.setVisibility(Visibility.get((int) value));
				getViewer().update(element, null);
				editPart.refresh();
			}
			
			private String[] getComboItems() {
				String[] comboItems = new String[Visibility.values().length];
				for (int i = 0; i < Visibility.values().length; i++) {
					comboItems[i] = Visibility.values()[i].getLiteral();
				}
				
				return comboItems;
			}
		});
		tvClmnAttrVisibility.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VisualClassAttribute attr = ((VisualClassAttribute) element);
				if (element == null || attr.getVisibility() == null) {
					attr.getVisibility();
					return Visibility.get(0).getName();
				} else
					return attr.getVisibility().getName();
			}
		});

		// Attribute Table - Column Name
		tvClmnAttrName.setEditingSupport(new EditingSupport(
				tableViewerAttributes) {
			private TextCellEditor cellEditor = new TextCellEditor(
					tableAttributes);

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return cellEditor;
			}

			protected Object getValue(Object element) {
				return ((VisualClassAttribute) element).getName();
			}

			protected void setValue(Object element, Object value) {
				VisualClassAttribute attr = (VisualClassAttribute) element;
				attr.setName(value.toString());
				getViewer().update(element, null);
				editPart.refresh();
			}
		});
		tvClmnAttrName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VisualClassAttribute attr = ((VisualClassAttribute) element);
				if (element == null || attr.getName() == null)
					return "";
				else
					return attr.getName();
			}
		});

		// Attribute Table - Column Type
		tvClmnAttrType.setEditingSupport(new EditingSupport(
				tableViewerAttributes) {
			private TextCellEditor cellEditor = new TextCellEditor(
					tableAttributes);

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return cellEditor;
			}

			protected Object getValue(Object element) {
				return ((VisualClassAttribute) element).getType().toString();
			}

			protected void setValue(Object element, Object value) {
				VisualClassAttribute attr = (VisualClassAttribute) element;
				attr.setType(DataTypeParser.parse(value.toString()));
				getViewer().update(element, null);
				editPart.refresh();
			}
		});
		tvClmnAttrType.setLabelProvider(new ColumnLabelProvider() {
			public String getText(Object element) {
				VisualClassAttribute attr = ((VisualClassAttribute) element);
				if (element == null || attr.getType() == null)
					return "";
				else
					return attr.getType().toString();
			}
		});

		// Attribute Table - Column Multiplicity
		tvClmnAttrMultiplicity.setEditingSupport(new EditingSupport(
				tableViewerAttributes) {
			private TextCellEditor cellEditor = new TextCellEditor(
					tableAttributes);

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return cellEditor;
			}

			protected Object getValue(Object element) {
				return ((VisualClassAttribute) element).getMultiplicity().toString();
			}

			protected void setValue(Object element, Object value) {
				VisualClassAttribute attr = (VisualClassAttribute) element;
				attr.setMultiplicity(MultiplicityParser.parse( value.toString()));
				getViewer().update(element, null);
				editPart.refresh();
			}
		});
		tvClmnAttrMultiplicity.setLabelProvider(new ColumnLabelProvider() {
			public String getText(Object element) {
				VisualClassAttribute attr = ((VisualClassAttribute) element);
				if (element == null || attr.getMultiplicity() == null)
					return "";
				else
					return attr.getMultiplicity().toString();
			}
		});

		// Attribute Table - Column ReadOnly
		tvClmnAttrReadOnly.setEditingSupport(new EditingSupport(
				tableViewerAttributes) {
			CheckboxCellEditor checkBox = new CheckboxCellEditor(
					tableAttributes);

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return checkBox;
			}

			protected Object getValue(Object element) {
				// use index, so can just use the enum value
				return ((VisualClassAttribute) element).isReadOnly();
			}

			protected void setValue(Object element, Object value) {
				VisualClassAttribute attr = (VisualClassAttribute) element;
				attr.setReadOnly((boolean) value);
				getViewer().update(element, null);
				editPart.refresh();
			}
		});

		tvClmnAttrReadOnly.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VisualClassAttribute attr = ((VisualClassAttribute) element);
				return attr.isReadOnly() ? "Y" : "N";
			}
		});
	}

	private void setTableOperationCellEditorAndViewer() {
		// Operation Table - Column Visibility
		tvClmnOpVisibility.setEditingSupport(new EditingSupport(
				tableViewerOperations) {
			ComboBoxCellEditor combo = new ComboBoxCellEditor(tableOperations,
					getComboItems());

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return combo;
			}

			protected Object getValue(Object element) {
				// use index, so can just use the enum value
				return ((VisualClassOperation) element).getVisibility()
						.getValue();
			}

			protected void setValue(Object element, Object value) {
				VisualClassOperation op = (VisualClassOperation) element;
				op.setVisibility(Visibility.get((int) value));
				getViewer().update(element, null);
				editPart.refresh();
			}
			
			private String[] getComboItems() {
				String[] comboItems = new String[Visibility.values().length];
				for (int i = 0; i < Visibility.values().length; i++) {
					comboItems[i] = Visibility.values()[i].getLiteral();
				}
				
				return comboItems;
			}
		});
		tvClmnOpVisibility.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VisualClassOperation op = ((VisualClassOperation) element);
				if (element == null || op.getVisibility() == null)
					return Visibility.get(0).getName();
				else
					return op.getVisibility().getName();
			}
		});

		// Operation Table - Column Name
		tvClmnOpName
				.setEditingSupport(new EditingSupport(tableViewerOperations) {
					private TextCellEditor cellEditor = new TextCellEditor(
							tableOperations);

					protected boolean canEdit(Object element) {
						return true;
					}

					protected CellEditor getCellEditor(Object element) {
						return cellEditor;
					}

					protected Object getValue(Object element) {
						return ((VisualClassOperation) element).getName();
					}

					protected void setValue(Object element, Object value) {
						VisualClassOperation op = (VisualClassOperation) element;
						op.setName(value.toString());
						getViewer().update(element, null);
						editPart.refresh();
					}
				});
		tvClmnOpName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VisualClassOperation op = ((VisualClassOperation) element);
				if (element == null || op.getName() == null)
					return "";
				else
					return op.getName();
			}
		});

		// Operation Table - Column Parameters
		tvClmnOpParameters.setEditingSupport(new EditingSupport(
				tableViewerOperations) {
			private TextCellEditor cellEditor = new TextCellEditor(
					tableOperations);

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return cellEditor;
			}

			protected Object getValue(Object element) {
				return ((VisualClassOperation) element).getParameters().toString();
			}

			protected void setValue(Object element, Object value) {
				VisualClassOperation op = (VisualClassOperation) element;
				op.setParameters(DataTypeParser.parse(value.toString()));
				getViewer().update(element, null);
				editPart.refresh();
			}
		});
		tvClmnOpParameters.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VisualClassOperation op = ((VisualClassOperation) element);
				if (element == null || op.getParameters() == null)
					return "";
				else
					return op.getParameters().toString();
			}
		});

		// Operation Table - Column Return Type
		tvClmnOpReturnType.setEditingSupport(new EditingSupport(
				tableViewerOperations) {
			private TextCellEditor cellEditor = new TextCellEditor(
					tableOperations);

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return cellEditor;
			}

			protected Object getValue(Object element) {
				return ((VisualClassOperation) element).getType().toString();
			}

			protected void setValue(Object element, Object value) {
				VisualClassOperation op = (VisualClassOperation) element;
				op.setType(DataTypeParser.parse(value.toString()));
				getViewer().update(element, null);
				editPart.refresh();
			}
		});
		tvClmnOpReturnType.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VisualClassOperation op = ((VisualClassOperation) element);
				if (element == null || op.getType() == null)
					return "";
				else
					return op.getType().toString();
			}
		});

		// Operation Table - Column Multiplicity
		tvClmnOpMultiplicity.setEditingSupport(new EditingSupport(
				tableViewerOperations) {
			private TextCellEditor cellEditor = new TextCellEditor(
					tableOperations);

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return cellEditor;
			}

			protected Object getValue(Object element) {
				return ((VisualClassOperation) element).getMultiplicity().toString();
			}

			protected void setValue(Object element, Object value) {
				VisualClassOperation op = (VisualClassOperation) element;
				op.setMultiplicity(MultiplicityParser.parse(value.toString()));
				getViewer().update(element, null);
				editPart.refresh();
			}
		});
		tvClmnOpMultiplicity.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VisualClassOperation op = ((VisualClassOperation) element);
				if (element == null || op.getMultiplicity() == null)
					return "";
				else
					return op.getMultiplicity().toString();
			}
		});
	}

	private void loadInitialData() {
		if (modelCopy.getName() != null && !modelCopy.getName().isEmpty()) {
			textName.setText(modelCopy.getName());
		}

		if (modelCopy.isInterface())
			btnIsInterface.setSelection(true);
		else
			btnIsInterface.setSelection(false);

		if (modelCopy.isAbstract())
			btnIsAbstract.setSelection(true);
		else
			btnIsAbstract.setSelection(false);

		tableViewerAttributes.setContentProvider(new ArrayContentProvider());
		tableViewerAttributes.setInput(modelCopy.getAttributes());

		tableViewerOperations.setContentProvider(new ArrayContentProvider());
		tableViewerOperations.setInput(modelCopy.getOperations());

	}

	/**
	 * Create contents of the button bar.
	 *
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button OKButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		OKButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				VisualClassUpdateCommand updateClass = new VisualClassUpdateCommand(model,modelCopy,editPart);
				editPart.getRoot().getViewer().getEditDomain().getCommandStack().execute(updateClass);
			}
		});
		// createButton(parent, IDialogConstants.CANCEL_ID,
		// IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(760, 466);
	}

	private VisualClassAttribute createNewAttribute() {
		VisualClassAttribute newAttr = ClassFactory.eINSTANCE
				.createVisualClassAttribute();
		newAttr.setLocation(new org.eclipse.draw2d.geometry.Point(0, 0));
		newAttr.setBounds(new Dimension(0, 0));
		newAttr.setVisibility(Visibility.UNSPECIFIED);
		newAttr.setName("newAttr");

		newAttr.setDiagram(modelCopy.getDiagram());
	    newAttr.setParent(modelCopy);
	    modelCopy.getElements().add(newAttr);
		if (!modelCopy.getAttributes().contains(newAttr)) {
			modelCopy.getAttributes().add(newAttr);
		}
		return newAttr;
	}

	private VisualClassOperation createNewOperation() {
		VisualClassOperation newOp = ClassFactory.eINSTANCE
				.createVisualClassOperation();
		newOp.setLocation(new org.eclipse.draw2d.geometry.Point(0, 0));
		newOp.setBounds(new Dimension(0, 0));
		newOp.setVisibility(Visibility.UNSPECIFIED);
		newOp.setName("newOperation");

		newOp.setDiagram(modelCopy.getDiagram());
		newOp.setParent(modelCopy);
		modelCopy.getElements().add(newOp);
		if (!modelCopy.getOperations().contains(newOp)) {
			modelCopy.getOperations().add(newOp);
		}
		return newOp;
	}

}
