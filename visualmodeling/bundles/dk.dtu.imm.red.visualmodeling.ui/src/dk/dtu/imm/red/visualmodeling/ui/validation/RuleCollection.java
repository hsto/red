package dk.dtu.imm.red.visualmodeling.ui.validation;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

public class RuleCollection {
	
	private List<IRule> ruleList;
	
	public RuleCollection()
	{
		this.ruleList = new ArrayList<IRule>();
	}
	
	public RuleCollection(List<IRule> ruleList)
	{
		this.ruleList = ruleList;
	}
	
	public void addRule(IRule rule)
	{
		this.ruleList.add(rule);
	}
	
	public void ValidateDiagram(VisualDiagram diagram)
	{
		// TODO iterate all elements and connections and apply the rules to them and get any errors or warnings
	}	
	
	public boolean canConnect(IVisualElement source, IVisualElement target, IVisualConnection connection)
	{		
		for(IRule rule : ruleList)
		{
			if(!rule.canConnect(source, target, connection))
			{
				return false;
			}
		}
		
		return true;
	}
	
	public boolean canAccept(IVisualElement parent, IVisualElement child)
	{
		for(IRule rule : ruleList)
		{
			if(!rule.canAccept(parent, child))
			{
				return false;
			}
		}
		
		return true;
	}
}
