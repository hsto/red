package dk.dtu.imm.red.visualmodeling.ui.specificationelement.stitch;

import org.eclipse.gef.palette.PaletteRoot;

import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;

public class StitchPaletteFactory extends PaletteFactoryBase{

	@Override
	public void populatePaletteInternal(PaletteRoot root) {
		//do nothing (no need for any palette for weave diagram)
	}
}
