package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementDeleteCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

public class ElementDeleteAction extends SelectionAction {

    public static final String ELEMENT_DELETE = "ElementDeleteAction";
    public static final String REQ_ELEMENT_DELETE = "ElementDeleteAction";

    Request request;

    public ElementDeleteAction(IWorkbenchPart part) {
        super(part);
        setId(ELEMENT_DELETE);
        setText("Delete Element");
        request = new Request(REQ_ELEMENT_DELETE);
    }


    @Override
    public void run() {
    	Object model = ((EditPart)getSelection()).getModel();

    	VisualElement element = (VisualElement)model;

        VisualElementDeleteCommand command = new VisualElementDeleteCommand(element);
        execute(command);
    }


	@Override
    @SuppressWarnings("rawtypes")
    protected boolean calculateEnabled() {

    	List selectionList = getSelectedObjects();

    	if(selectionList.size() != 1)
    		return false;

    	Object selection = ((EditPart)selectionList.get(0)).getModel();

    	if(!(selection instanceof VisualElement))
	    	return false;

        return true;
    }
}