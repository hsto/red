package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.figures.VisualSystemStructureActorFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructureFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor;

public class VisualSystemStructureActorFactory implements CreationFactory {

	SystemStructureActorKind kind = SystemStructureActorKind.PERSON;

	public VisualSystemStructureActorFactory(SystemStructureActorKind kind) {
		this.kind = kind;
	}


	public Object getNewObject() {

		VisualSystemStructureActor element = SystemstructureFactory.eINSTANCE.createVisualSystemStructureActor();
		element.setActorKind(kind);
		element.setBounds(VisualSystemStructureActorFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualSystemStructureActor.class;
	}
}
