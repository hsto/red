package dk.dtu.imm.red.visualmodeling.ui.editpolicies;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.BendpointEditPolicy;
import org.eclipse.gef.requests.BendpointRequest;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionBendpointCreateCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionBendpointDeleteCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionBendpointMoveCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;

public class VisualConnectionBenpointEditPolicy extends BendpointEditPolicy{

	@Override
	protected Command getCreateBendpointCommand(BendpointRequest request) {
		
		Point location = request.getLocation();
		getHostFigure().translateToRelative(location);
				
		return new VisualConnectionBendpointCreateCommand(
				request.getIndex()
				, location
				, (VisualConnection)request.getSource().getModel()
			);	
	}

	@Override
	protected Command getDeleteBendpointCommand(BendpointRequest request) {
		return new VisualConnectionBendpointDeleteCommand(
				request.getIndex()
				, (VisualConnection)request.getSource().getModel());
	}

	/**
	 * Moves and deletes a bendpoint if its dragged close to bendpoints before or after it.
	 * **/
	@Override
	protected Command getMoveBendpointCommand(BendpointRequest request) {		
		VisualConnection connection = (VisualConnection)request.getSource().getModel();
		int index = request.getIndex();
		int size = connection.getBendpoints().size();
		Point location = request.getLocation();
		getHostFigure().translateToRelative(location);
		Point bendpoint = location.getCopy();
		
		if(index > 0)
		{
			Point previousBendpoint = connection.getBendpoints().get(request.getIndex() - 1);
			if(bendpoint.getDistance(previousBendpoint) < 5.0)
				return getDeleteBendpointCommand(request);
		}			
		
		if(index < size - 1)
		{
			Point nextBendpoint = connection.getBendpoints().get(request.getIndex() + 1);
			if(bendpoint.getDistance(nextBendpoint) < 5.0)
				return getDeleteBendpointCommand(request);
		}
			
		
		return new VisualConnectionBendpointMoveCommand(request.getIndex()
				, location
				, (VisualConnection)request.getSource().getModel());
	}
	
}
