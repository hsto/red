package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;

public class VisualConnectionBendpointCreateCommand extends Command{
		
    private int index;    
    private Point location;    
    private VisualConnection connection;
 
    public VisualConnectionBendpointCreateCommand(int index, Point location, VisualConnection connection)
    {
    	this.index = index;
		this.location = location;
		this.connection = connection;
    }
    
    @Override public void execute() {
    	connection.getBendpoints().add(index, location);
    }
 
    @Override public void undo() {
    	connection.getBendpoints().remove(index);
    }
}
