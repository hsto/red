package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.ui.handlers.NavigateToElementTree;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

/**
 * 
 * @author Luai
 *
 */
public class NavigateToElementTreeAction extends SelectionAction {

	public static final String NAVIGATE_TO_ELEMENT = "NavigateToElementTreeAction";
	public static final String REQ_NAVIGATE_TO_ELEMENT = "NavigateToElementTreeAction";

	Request request;

	public NavigateToElementTreeAction(IWorkbenchPart part) {
		super(part);
		setId(NAVIGATE_TO_ELEMENT);
		setText("Navigate To Element in Explorer");
		setActionDefinitionId("dk.dtu.imm.red.visualmodeling.ui.navigateToTree");
		request = new Request(REQ_NAVIGATE_TO_ELEMENT);
	}

	@Override
	public void run() {
		NavigateToElementTree handler = new NavigateToElementTree();
		handler.execute(getSelectedObjects());
	}

	@Override
	@SuppressWarnings("rawtypes")
	protected boolean calculateEnabled() {

    	List selectionList = getSelectedObjects();
    	
    	if(selectionList.size() != 1)
    		return false;
    	
    	Object selection = ((EditPart)selectionList.get(0)).getModel();
    	
    	if (selection instanceof VisualSpecificationElement)
    	{
    		return ((VisualSpecificationElement)selection).getSpecificationElement() != null;	    		
    	}
    	else if (selection instanceof VisualConnection) 
    	{
    		return ((VisualConnection)selection).getSpecificationElement() != null;
    	}
	    		    	
        return false;
	}

}
