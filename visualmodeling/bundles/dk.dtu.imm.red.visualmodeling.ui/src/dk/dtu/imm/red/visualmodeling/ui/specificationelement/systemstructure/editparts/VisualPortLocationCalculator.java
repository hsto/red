package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts;

import org.eclipse.draw2d.geometry.Point;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;

public class VisualPortLocationCalculator {

	public static void recalculatePortStickingLocation(VisualPort port) {
		IVisualElement parent = port.getParent();
		if(!(parent instanceof VisualSystem))
			return;

		if (parent instanceof VisualSystem) {

			if(isAlignLeft(port)){
        		port.setLocation(new Point(0,port.getLocation().y));
			}
			else if(isAlignTop(port)){
        		port.setLocation(new Point(port.getLocation().x,0));
			}
			else if(isAlignRight(port)){
				port.setLocation(new Point(parent.getBounds().width-port.getBounds().width,port.getLocation().y));
			}
			else{//align bottom
				port.setLocation(new Point(port.getLocation().x,parent.getBounds().height-port.getBounds().height));
			}
		}
	}

	public static boolean isAlignLeft(VisualPort port) {
		IVisualElement parent = port.getParent();

		int parentWidth = 0;
		int parentHeight = 0;

		int distToLeftBorder = port.getLocation().x();
		int distToTopBorder = port.getLocation().y();

		if (parent instanceof VisualSystem) {
			parentWidth = parent.getBounds().width();
			parentHeight = parent.getBounds().height();

			int distToRightBorder = parentWidth - distToLeftBorder;
			int distToBottomBorder = parentHeight - distToTopBorder;

		return (distToLeftBorder <= distToTopBorder
				&& distToLeftBorder <= distToRightBorder && distToLeftBorder <= distToBottomBorder);
		}

		return false;
	}

	public static boolean isAlignTop(VisualPort port) {

		IVisualElement parent = port.getParent();

		int parentWidth = 0;
		int parentHeight = 0;

		int distToLeftBorder = port.getLocation().x();
		int distToTopBorder = port.getLocation().y();

		if (parent instanceof VisualSystem) {
			parentWidth = parent.getBounds().width();
			parentHeight = parent.getBounds().height();

			int distToRightBorder = parentWidth - distToLeftBorder;
			int distToBottomBorder = parentHeight - distToTopBorder;

		return (distToTopBorder <= distToLeftBorder
				&& distToTopBorder <= distToRightBorder && distToTopBorder <= distToBottomBorder);
		}
		return false;
	}

	public static boolean isAlignRight(VisualPort port) {
		IVisualElement parent = port.getParent();

		int parentWidth = 0;
		int parentHeight = 0;

		int distToLeftBorder = port.getLocation().x();
		int distToTopBorder = port.getLocation().y();

		if (parent instanceof VisualSystem) {
			parentWidth = parent.getBounds().width();
			parentHeight = parent.getBounds().height();

			int distToRightBorder = parentWidth - distToLeftBorder;
			int distToBottomBorder = parentHeight - distToTopBorder;

			return (distToRightBorder <= distToTopBorder
				&& distToRightBorder <= distToLeftBorder && distToRightBorder <= distToBottomBorder);
		}
		return false;

	}

	public static boolean isAlignBottom(VisualPort port) {
		IVisualElement parent = port.getParent();

		int parentWidth = 0;
		int parentHeight = 0;

		int distToLeftBorder = port.getLocation().x();
		int distToTopBorder = port.getLocation().y();

		if (parent instanceof VisualSystem) {
			parentWidth = parent.getBounds().width();
			parentHeight = parent.getBounds().height();

			int distToRightBorder = parentWidth - distToLeftBorder;
			int distToBottomBorder = parentHeight - distToTopBorder;

			return (distToBottomBorder <= distToTopBorder
				&& distToBottomBorder <= distToLeftBorder && distToBottomBorder <= distToRightBorder);
		}
		return false;

	}
}
