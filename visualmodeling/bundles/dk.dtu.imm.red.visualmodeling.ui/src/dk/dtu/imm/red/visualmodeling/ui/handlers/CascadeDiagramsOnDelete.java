package dk.dtu.imm.red.visualmodeling.ui.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.impl.WorkspaceFactoryImpl;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionDeleteCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementDeleteCommand;
import dk.dtu.imm.red.visualmodeling.ui.util.VisualModelHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;


/**
 * 
 * @author Luai
 *
 *	Issue #119
 *
 */
public class CascadeDiagramsOnDelete extends AbstractHandler {
	
	@Override
	@SuppressWarnings("unchecked")
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		List<Element> selectedElements = (List<Element>) event.getTrigger();
		List<VisualElementDeleteCommand> elementCommands = new ArrayList<VisualElementDeleteCommand>();
		List<VisualConnectionDeleteCommand> connectionCommands = new ArrayList<VisualConnectionDeleteCommand>();
		
		
		Workspace workspace = WorkspaceFactoryImpl.eINSTANCE.getWorkspaceInstance();
		List<Diagram> diagrams = VisualModelHelper.getAllDiagrams(workspace);
		for (Diagram diagram : diagrams) {
			//EList<IVisualElement> diagramElements = diagram.getVisualDiagram().getElements();
			List<IVisualElement> diagramElements = VisualModelHelper.getAllDecendents(diagram.getVisualDiagram());
			for (IVisualElement e : diagramElements) {
				if (e instanceof VisualSpecificationElementImpl) {
					VisualSpecificationElementImpl visualElement = (VisualSpecificationElementImpl)e;
					for (Element selectedElement : selectedElements) {
						if (visualElement.getSpecificationElement() != null &&
								visualElement.getSpecificationElement().equals(selectedElement)) {
							elementCommands.add(new VisualElementDeleteCommand(visualElement));
							break;
						}
					}
				}
			}
			//executeElementCommands(elementCommands);
			
			EList<IVisualConnection> diagramConnections = diagram.getVisualDiagram().getDiagramConnections();						
			for (IVisualConnection con : diagramConnections) {
				if (con instanceof VisualConnectionImpl) {
					VisualConnectionImpl visualConnection = (VisualConnectionImpl) con;
					for (Element selectedElement : selectedElements) {
						if (visualConnection.getSpecificationElement() != null && 
								visualConnection.getSpecificationElement().equals(selectedElement)) {
							connectionCommands.add(new VisualConnectionDeleteCommand(visualConnection));
						}
						break;
					}
				}
			}
			//executeConnectionCommands(connectionCommands);	
		}
		
		
		List<Command> commands = new ArrayList<Command>();
		commands.addAll(elementCommands);
		commands.addAll(connectionCommands);
		return commands;
	}
}
