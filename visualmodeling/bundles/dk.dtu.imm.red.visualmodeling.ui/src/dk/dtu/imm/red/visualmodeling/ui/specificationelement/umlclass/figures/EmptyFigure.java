package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures;

import org.eclipse.draw2d.geometry.Rectangle;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class EmptyFigure extends VisualElementFigure{

	public EmptyFigure(IVisualElement model) {
		super(model);
		setBounds(new Rectangle(0, 0, 0, 0));
	}

}
