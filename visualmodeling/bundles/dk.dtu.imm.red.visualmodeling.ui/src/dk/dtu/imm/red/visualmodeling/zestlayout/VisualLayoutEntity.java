package dk.dtu.imm.red.visualmodeling.zestlayout;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.zest.layouts.NestedLayoutEntity;
import org.eclipse.zest.layouts.constraints.BasicEntityConstraint;
import org.eclipse.zest.layouts.constraints.EntityPriorityConstraint;
import org.eclipse.zest.layouts.constraints.LabelLayoutConstraint;
import org.eclipse.zest.layouts.constraints.LayoutConstraint;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualLayoutEntity implements NestedLayoutEntity, Cloneable{

	protected double x, y, width, height;
	protected IVisualElement visualElement;
	private boolean ignoreInLayout = false;
	private Object internalNode;

	private NestedLayoutEntity parent;
	private List<NestedLayoutEntity> children;



	//translate visual element to layout entity
	public VisualLayoutEntity(IVisualElement visualElement){
		this.x = visualElement.getLocation().x;
		this.y = visualElement.getLocation().y;
		this.width = visualElement.getBounds().width;
		this.height = visualElement.getBounds().height;
		this.visualElement = visualElement;
	}

	@Override
	public int compareTo(Object arg0) {
		return 0;
	}

	@Override
	public void setGraphData(Object o) {
	}

	@Override
	public Object getGraphData() {
		return null;
	}

	@Override
	public void setLocationInLayout(double x, double y) {
		if (!ignoreInLayout) {
			this.x = x;
			this.y = y;
		}
	}

	@Override
	public void setSizeInLayout(double width, double height) {
		if (!ignoreInLayout) {
			this.width = width;
			this.height = height;
		}
	}

	@Override
	public double getXInLayout() {
		return this.x;
	}

	@Override
	public double getYInLayout() {
		return this.y;
	}

	@Override
	public double getWidthInLayout() {
		return this.width;
	}

	@Override
	public double getHeightInLayout() {
		return this.height;
	}

	@Override
	public Object getLayoutInformation() {
		return internalNode;
	}

	@Override
	public void setLayoutInformation(Object internalEntity) {
		this.internalNode = internalEntity;
	}

	@Override
	public void populateLayoutConstraint(LayoutConstraint constraint) {
		if (constraint instanceof LabelLayoutConstraint) {
			LabelLayoutConstraint labelConstraint = (LabelLayoutConstraint) constraint;
			labelConstraint.label = visualElement.toString();
			labelConstraint.pointSize = 18;
		} else if (constraint instanceof BasicEntityConstraint) {
			// noop
		} else if (constraint instanceof EntityPriorityConstraint) {
			EntityPriorityConstraint priorityConstraint = (EntityPriorityConstraint) constraint;
			priorityConstraint.priority = Math.random() * 10 + 1;
		}
	}

	public IVisualElement getVisualElement() {
		return visualElement;
	}

	@Override
	public NestedLayoutEntity getParent() {
		return parent;
	}

	public void setParent(VisualLayoutEntity parent) {
		this.parent = parent;
		//also add children when parent is set
		parent.addChildren(this);
	}

	@Override
	public List<NestedLayoutEntity> getChildren() {
		if(children==null)
			children = new ArrayList<NestedLayoutEntity>();
		return children;
	}

	public void addChildren(NestedLayoutEntity child) {
		if(children==null)
			children = new ArrayList<NestedLayoutEntity>();
		children.add(child);
	}

	@Override
	public boolean hasChildren() {
		return children.size()>0;
	}


	@Override
	public VisualLayoutEntity clone(){
		VisualLayoutEntity cl = new VisualLayoutEntity(this.visualElement);
		return cl;
	}
}
