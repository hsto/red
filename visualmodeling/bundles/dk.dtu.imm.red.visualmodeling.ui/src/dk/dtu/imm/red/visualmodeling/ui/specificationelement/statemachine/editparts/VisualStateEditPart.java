package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualStateFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualStateEditPart extends VisualElementEditPart{

	public VisualStateEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	@Override
	public void performRequest(Request req) {
		super.performRequest(req);
		if(req.getType() == RequestConstants.REQ_OPEN) {
			VisualStateSpecificationEditDialog dialog = new
					VisualStateSpecificationEditDialog(Display.getDefault().getActiveShell(), getModel(), this);
			dialog.open();
	    }

	}

	@Override
	protected IFigure createFigure() {
		return new VisualStateFigure((IVisualElement)getModel());
	}

}
