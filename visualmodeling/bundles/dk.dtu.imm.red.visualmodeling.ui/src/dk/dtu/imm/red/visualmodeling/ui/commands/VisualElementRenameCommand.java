package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.Set;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.visualmodeling.traceability.TraceLinkUtil;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualElementRenameCommand extends Command {

	protected String oldName, newName;
	protected IVisualElement model;

	public VisualElementRenameCommand(IVisualElement model, String newName) {
		this.model = model;
		this.newName = newName;
	}

	@Override
	public void execute() {

		updateName(model,newName);

		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
			Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(model);
			for(IVisualElement element : allTraces){
				updateName(element,newName);
			}
		}


	}

	private void updateName(IVisualElement model, String name){
		if(model==null)
			return;

		oldName = model.getName();

		if(model instanceof VisualSpecificationElement)
		{
			VisualSpecificationElement element = (VisualSpecificationElement)model;
			if(element.isIsLinkedToElement()){
				oldName = element.getSpecificationElement().getName();
				element.getSpecificationElement().setName(newName);
			}
		}
		model.setName(newName);
	}


	@Override
	public void undo() {
		undoUpdateName(model);
		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
			Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(model);
			for(IVisualElement element : allTraces){
				undoUpdateName(element);
			}
		}
	}

	private void undoUpdateName(IVisualElement model){
		model.setName(oldName);

		if(model instanceof VisualSpecificationElement)
		{
			VisualSpecificationElement element = (VisualSpecificationElement)model;
			if(element.isIsLinkedToElement())
				element.getSpecificationElement().setName(oldName);
		}
		model.setName(oldName);
	}
}
