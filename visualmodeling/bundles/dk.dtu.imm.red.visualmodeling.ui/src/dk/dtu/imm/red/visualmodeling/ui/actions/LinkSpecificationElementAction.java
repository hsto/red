package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualConnectionEditPart;
import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class LinkSpecificationElementAction extends SelectionAction {

  	public static final String SPECIFICATION_ELEMENT_LINK = "SpecificationElementLink";
   	public static final String REQ_SPECIFICATION_ELEMENT_LINK = "SpecificationElementLink";

	public LinkSpecificationElementAction(IWorkbenchPart part) {
		super(part);

        setId(SPECIFICATION_ELEMENT_LINK);
        setText("Link Element");
	}

	@Override
	@SuppressWarnings("rawtypes")
	protected boolean calculateEnabled() {
		List selection = getSelectedObjects();

		if(selection.size() > 0)
		{
			Object model = ((EditPart)selection.get(0)).getModel();
			if(model instanceof VisualSpecificationElement)
	    	{
				VisualSpecificationElement element = (VisualSpecificationElement)model;
	    		return element.getSpecificationElement() == null;
	    	}
			else if (model instanceof VisualConnection) 
			{
				VisualConnection connection = (VisualConnection) model;
				return connection.getSpecificationElement() == null;
				//return connection.getName() == ConnectionType.ASSOCIATION.getName();
			}
		}
		return false;
	}

	@Override
    public void run() {

		Object selection = this.getSelectedObjects().get(0);
		EditPart editPart = (EditPart)selection;

		if(editPart instanceof VisualElementEditPart)
		{
			((VisualElementEditPart)editPart).linkModelToElement();
		}
		else if (editPart.getModel() instanceof VisualConnection) {
			
			((VisualConnectionEditPart)editPart).linkModelToElement();
		}

    }
}
