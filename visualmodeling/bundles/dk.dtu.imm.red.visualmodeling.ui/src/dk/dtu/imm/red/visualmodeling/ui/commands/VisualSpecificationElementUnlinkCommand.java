package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.Set;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.traceability.TraceLinkUtil;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualSpecificationElementUnlinkCommand extends Command{

	private VisualSpecificationElement element;
	private SpecificationElement specificationElement;

	  public VisualSpecificationElementUnlinkCommand(VisualSpecificationElement element)
	    {
	    	this.element = element;
	    }

	    @Override public void execute() {
	    	unlinkElement(element);
	    	if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
				Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(element);
				for(IVisualElement e : allTraces){
					unlinkElement(e);
				}
			}
	    }

	    private void unlinkElement(IVisualElement element){
	    	if(element instanceof VisualSpecificationElement){
	    		specificationElement = ((VisualSpecificationElement) element).getSpecificationElement();
	    		((VisualSpecificationElement)element).setSpecificationElement(null);
	    	}
	    }

	    @Override public void undo() {
	    	linkElement(element);
	    	if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
				Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(element);
				for(IVisualElement e : allTraces){
					linkElement(e);
				}
			}
	    }

	    private void linkElement(IVisualElement element){
	    	if(element instanceof VisualSpecificationElement){
	    		((VisualSpecificationElement) element).setSpecificationElement(specificationElement);
	    	}
	    }
}
