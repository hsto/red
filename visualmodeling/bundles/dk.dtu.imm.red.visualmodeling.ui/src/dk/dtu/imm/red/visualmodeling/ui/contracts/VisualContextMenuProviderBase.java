package dk.dtu.imm.red.visualmodeling.ui.contracts;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.jface.action.IMenuManager;

public abstract class VisualContextMenuProviderBase implements IVisualContextMenuProvider{

	protected VisualContextMenuProviderBase nextProvider;
	
	@Override
	public final void buildContextMenu(IMenuManager menu, EditPartViewer viewer)
	{
		buildContextMenuInternal(menu, viewer);
		
		if(nextProvider != null)
			nextProvider.buildContextMenu(menu, viewer);
	}
	
	public final VisualContextMenuProviderBase setNext(VisualContextMenuProviderBase nextProvider)
	{
		this.nextProvider = nextProvider;
		return nextProvider;
	}
	
	public final VisualContextMenuProviderBase getNext()
	{
		return this.nextProvider;
	}
	
	protected abstract void buildContextMenuInternal(IMenuManager menu, EditPartViewer viewer);
}
