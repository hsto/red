package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualEditorConfigurationBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericPaletteFactory;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;


public class SystemStructureConfiguration extends VisualEditorConfigurationBase{

	public SystemStructureConfiguration()
	{
		this.ruleCollection = new RuleCollection();
		this.ruleCollection.addRule(new SystemStructureRule());

		this.paletteFactory = new SystemStructurePaletteFactory();
		this.paletteFactory.setNext(new GenericPaletteFactory());

		this.editPartFactory = new SystemStructureEditPartFactory(ruleCollection);
		this.editPartFactory.setNext(new GenericEditPartFactory(ruleCollection));
	}
}
