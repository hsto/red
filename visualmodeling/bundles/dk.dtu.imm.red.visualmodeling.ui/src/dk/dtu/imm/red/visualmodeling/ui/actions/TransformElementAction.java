package dk.dtu.imm.red.visualmodeling.ui.actions;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.ui.SpecificationElementSelectionDialog;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementReplaceCommand;
import dk.dtu.imm.red.visualmodeling.ui.factories.VisualElementCreationFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

/*
 * Allows a user to transform a generic element into a specific type of element.
 * */
public class TransformElementAction extends SelectionAction{

  	public static final String SPECIFICATION_ELEMENT_TRANSFORM = "SpecificationElementTransformAction";
   	public static final String REQ_SPECIFICATION_ELEMENT_TRANSFORM = "SpecificationElemenTransformAction";

    Request request;

    public TransformElementAction(IWorkbenchPart part) {
        super(part);
        setId(SPECIFICATION_ELEMENT_TRANSFORM);
        setText("Transform Element");
        request = new Request(REQ_SPECIFICATION_ELEMENT_TRANSFORM);
    }


	@Override
	protected boolean calculateEnabled() {
		if(this.getSelectedObjects().size() != 1 || getSelectedElement() == null)
		{
			return false;
		}

		return true;
	}


	@Override
	@SuppressWarnings("unchecked")
	public void run() {
		super.run();

		SpecificationElementSelectionDialog dialog =
				new SpecificationElementSelectionDialog(null, new Class[]{});
		dialog.create();
		int result = dialog.open();

		if(result == SpecificationElementSelectionDialog.CANCEL)
		{
			return;
		}

		SpecificationElement element = dialog.getSelection();
		IVisualElement visualElement = getSelectedElement();

		VisualElementCreationFactory elementFactory = new VisualElementCreationFactory();
		elementFactory.SetCreationFactory(element);
		elementFactory.setSpecificationElement(element);

		IVisualElement replacementElement = (IVisualElement)elementFactory.getNewObject();

		VisualElementReplaceCommand command = new VisualElementReplaceCommand(
				visualElement.getDiagram()
				, visualElement.getParent()
				, visualElement
				, replacementElement);

		command.execute();
	}

	private IVisualElement getSelectedElement()
	{
		Object selection = this.getSelectedObjects().get(0);
		EditPart editPart = (EditPart)selection;
		Object model = editPart.getModel();

		if(model instanceof IVisualElement)
		{
			return (IVisualElement)model;
		}

		return null;
	}
}
