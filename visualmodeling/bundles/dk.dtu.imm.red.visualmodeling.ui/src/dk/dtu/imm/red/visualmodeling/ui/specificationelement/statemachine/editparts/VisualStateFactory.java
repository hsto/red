package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualStateFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState;

public class VisualStateFactory implements CreationFactory {
	public Object getNewObject() {

		VisualState element = StatemachineFactory.eINSTANCE.
				createVisualState();
		element.setBounds(VisualStateFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualState.class;
	}
}
