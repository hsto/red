package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionCascadeDeleteCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementCascadeDeleteCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualMultiCascadeDeleteCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

/**
 * 
 * @author Luai
 *
 */
public class ElementCascadeDeleteAction extends SelectionAction {

	public static final String ELEMENT_CASCADE_DELETE = "ElementCascadeDeleteAction";
	public static final String REQ_ELEMENT_CASCADE_DELETE = "ElementCascadeDeleteAction";

	Request request;

	public ElementCascadeDeleteAction(IWorkbenchPart part) {
		super(part);
		setId(ELEMENT_CASCADE_DELETE);
		setText("Cascaded Delete Element");
		request = new Request(REQ_ELEMENT_CASCADE_DELETE);
	}

	@Override
	public void run() {
		
		Object[] selectedElement = getSelectedElements();
		
		List<VisualElementCascadeDeleteCommand> elementCommands = new ArrayList<VisualElementCascadeDeleteCommand>();
		List<VisualConnectionCascadeDeleteCommand> connectionCommands = new ArrayList<VisualConnectionCascadeDeleteCommand>();
		for (int i = 0; i < selectedElement.length; i++) {
			if (selectedElement[i] instanceof IVisualElement) {
				elementCommands.add(new VisualElementCascadeDeleteCommand((IVisualElement) selectedElement[i]));
			}
			else if (selectedElement[i] instanceof IVisualConnection) {
				connectionCommands.add(new VisualConnectionCascadeDeleteCommand((IVisualConnection) selectedElement[i]));
			}
		}
		
		VisualMultiCascadeDeleteCommand command = new VisualMultiCascadeDeleteCommand(elementCommands, connectionCommands);
		this.getCommandStack().execute(command);		
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	protected boolean calculateEnabled() {
		List selectionList = getSelectedObjects();

		//TODO: This doesn't seem right. Should "selection" not be current object of the iteration, instead of element 0?
		for (Object obj : selectionList) {
			Object selection = ((EditPart)selectionList.get(0)).getModel();
			
			if(!(selection instanceof VisualElement) && !(selection instanceof VisualConnection))
				return false;			
		}		

        return true;
	}
	
	@SuppressWarnings("unchecked")
	private Object[] getSelectedElements() {
		List<EditPart> selection = getSelectedObjects();

		Vector<Object> vector = new Vector<Object>();
		for(EditPart editpart : selection){
			Object model = editpart.getModel();

			if(model instanceof IVisualElement)
			{
				vector.add((IVisualElement) model);
			}
			else if (model instanceof IVisualConnection) {
				vector.add((IVisualConnection) model);
			}
		}

		Object[] result = vector.toArray(new Object[vector.size()]);
		vector.clear();
		return result;
	}

}
