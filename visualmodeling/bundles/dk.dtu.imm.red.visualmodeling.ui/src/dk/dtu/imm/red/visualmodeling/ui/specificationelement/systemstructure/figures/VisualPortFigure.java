package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualPortFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(30,30);

	// private RectangleFigure rectFigure;	

	public VisualPortFigure(IVisualElement model) {
		super(model);

		formalFigure = new RectangleFigure();
		formalFigure.setSize(30, 30);
		formalFigure.setBackgroundColor(ColorHelper.lightGray);

	    add(this.nameLabel);
	    refreshVisuals();
	}

	@Override
	  protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		
		Rectangle r = getBounds().getCopy();

		boundingRectangle.setVisible(false);

		if(this.getChildren().contains(formalFigure)){
			setConstraint(formalFigure,new Rectangle(0,0,r.width,r.height));
		}
		setConstraint(nameLabel, new Rectangle(0,0,r.width,r.height));

	  }


	@Override
	public void refreshVisuals()
	{
		super.refreshVisuals();
		
		if (this.getChildren().contains(formalFigure))
			remove(formalFigure);
		add(formalFigure,0);

		//toggling between sketchy and non sketchy
		if(model.isIsSketchy()){
			nameLabel.setFont(SKETCHY_FONT);
		}
		else{
			nameLabel.setFont(DEFAULT_FONT);
		}
	}


}
