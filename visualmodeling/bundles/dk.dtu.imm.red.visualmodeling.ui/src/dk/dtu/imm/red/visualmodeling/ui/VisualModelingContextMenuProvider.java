package dk.dtu.imm.red.visualmodeling.ui;

import java.util.List;

import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.ui.actions.ActionFactory;

import dk.dtu.imm.red.visualmodeling.ui.actions.ElementCascadeDeleteAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.ElementFindAllTraceAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.ElementPropertiesAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.ExportDiagramAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.LinkSpecificationElementAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.MakeNonSketchyAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.MakeSketchyAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.NavigateToElementTreeAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.OpenSpecificationElementAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.TransformElementAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.UnlinkSpecificationElementAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.LayoutAlgorithmType;
import dk.dtu.imm.red.visualmodeling.ui.contracts.IVisualContextMenuProvider;
import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;

public class VisualModelingContextMenuProvider extends ContextMenuProvider {

	private ActionRegistry actionRegistry;
	private IVisualContextMenuProvider visualContextMenuProvider;

    public VisualModelingContextMenuProvider(EditPartViewer viewer, final ActionRegistry actionRegistry, IVisualContextMenuProvider visualContextMenuProvider) {
        super(viewer);
        setActionRegistry(actionRegistry);
        this.visualContextMenuProvider = visualContextMenuProvider;
    }


    @Override
    public void buildContextMenu(IMenuManager menu) {
    	
    	if(visualContextMenuProvider != null)
    		visualContextMenuProvider.buildContextMenu(menu, getViewer());

    	editPartContextMenu(menu);

    	GEFActionConstants.addStandardActionGroups(menu);

        IAction action;

        action = getActionRegistry().getAction(ActionFactory.UNDO.getId());
        menu.appendToGroup(GEFActionConstants.GROUP_UNDO, action);
        action = getActionRegistry().getAction(ActionFactory.REDO.getId());
        menu.appendToGroup(GEFActionConstants.GROUP_UNDO, action);
        action = getActionRegistry().getAction(ActionFactory.DELETE.getId());
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
        
        action = getActionRegistry().getAction(ElementCascadeDeleteAction.ELEMENT_CASCADE_DELETE);
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
        action = getActionRegistry().getAction(OpenSpecificationElementAction.SPECIFICATION_ELEMENT_OPEN);
        ((SelectionAction) action).update(); 
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
        action = getActionRegistry().getAction(NavigateToElementTreeAction.NAVIGATE_TO_ELEMENT);
        ((SelectionAction) action).update(); 
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
        action = getActionRegistry().getAction(TransformElementAction.SPECIFICATION_ELEMENT_TRANSFORM);
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);


        action = getActionRegistry().getAction(MakeSketchyAction.ELEMENT_MAKE_SKETCHY);
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
        action = getActionRegistry().getAction(MakeNonSketchyAction.ELEMENT_MAKE_NONSKETCHY);
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);

        action = getActionRegistry().getAction(LinkSpecificationElementAction.SPECIFICATION_ELEMENT_LINK);
        ((SelectionAction) action).update(); 
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
        action = getActionRegistry().getAction(UnlinkSpecificationElementAction.SPECIFICATION_ELEMENT_UNLINK);
        ((SelectionAction) action).update(); 
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);

        action = getActionRegistry().getAction(ElementFindAllTraceAction.ELEMENT_FIND_ALL_TRACE);
        menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);

        createAlignmentMenu(menu);
        createResizeMenu(menu);

        createAutoAlignMenu(menu);

        action = getActionRegistry().getAction(GEFActionConstants.ZOOM_IN);
        menu.appendToGroup(GEFActionConstants.GROUP_VIEW, action);
        action = getActionRegistry().getAction(GEFActionConstants.ZOOM_OUT);
        menu.appendToGroup(GEFActionConstants.GROUP_VIEW, action);

        action = getActionRegistry().getAction(ExportDiagramAction.EXPORT_DIAGRAM);
        menu.appendToGroup(GEFActionConstants.GROUP_VIEW, action);
        
        action = getActionRegistry().getAction(ElementPropertiesAction.ELEMENT_PROPERTIES);
        menu.appendToGroup(GEFActionConstants.GROUP_REST, action);
    }

    /*
     * Get the context menus provided by editparts
     * */
    @SuppressWarnings("rawtypes")
    private void editPartContextMenu(IMenuManager menu)
    {
    	List editparts = this.getViewer().getSelectedEditParts();

    	if(editparts.size() == 1)
    	{
    		EditPart editpart = (EditPart)editparts.get(0);
    		if(editpart instanceof VisualElementEditPart)
    		{
    			VisualElementEditPart elementEditPart = (VisualElementEditPart)editpart;
    			elementEditPart.createContextMenu(menu);
    		}
    	}
    }

    private void createAlignmentMenu(IMenuManager menu)
    {
    	MenuManager subMenu = new MenuManager("Align", null);

    	IAction action;

    	action = getActionRegistry().getAction(GEFActionConstants.ALIGN_LEFT);
        subMenu.add(action);
        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_RIGHT);
        subMenu.add(action);
        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_TOP);
        subMenu.add(action);
        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_BOTTOM);
        subMenu.add(action);
        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_CENTER);
        subMenu.add(action);
        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_MIDDLE);
        subMenu.add(action);
        menu.add(subMenu);
    }

    private void createResizeMenu(IMenuManager menu)
    {
    	MenuManager subMenu = new MenuManager("Resize", null);
    	IAction action;

    	action = getActionRegistry().getAction(GEFActionConstants.MATCH_HEIGHT);
        subMenu.add(action);
        action = getActionRegistry().getAction(GEFActionConstants.MATCH_WIDTH);
        subMenu.add(action);
        action = getActionRegistry().getAction(GEFActionConstants.MATCH_SIZE);
        subMenu.add(action);

        menu.add(subMenu);
    }


    private void createAutoAlignMenu(IMenuManager menu)
    {
    	MenuManager subMenu = new MenuManager("Auto Layout", null);
    	IAction action;

    	action = getActionRegistry().getAction(LayoutAlgorithmType.DIRECTED_GRAPH_LAYOUT);
        subMenu.add(action);

        action = getActionRegistry().getAction(LayoutAlgorithmType.GRID_LAYOUT);
        subMenu.add(action);

//        action = getActionRegistry().getAction(LayoutAlgorithmType.HORIZONTAL_LAYOUT);
//        subMenu.add(action);
//
//    	action = getActionRegistry().getAction(LayoutAlgorithmType.HORIZONTAL_TREE_LAYOUT);
//        subMenu.add(action);

    	action = getActionRegistry().getAction(LayoutAlgorithmType.RADIAL_LAYOUT);
        subMenu.add(action);

    	action = getActionRegistry().getAction(LayoutAlgorithmType.SPRING_LAYOUT);
        subMenu.add(action);

    	action = getActionRegistry().getAction(LayoutAlgorithmType.TREE_LAYOUT);
        subMenu.add(action);

    	action = getActionRegistry().getAction(LayoutAlgorithmType.VERTICAL_LAYOUT);
        subMenu.add(action);

        menu.add(subMenu);
    }

    private ActionRegistry getActionRegistry() {
        return actionRegistry;
    }

    private void setActionRegistry(final ActionRegistry actionRegistry) {
        this.actionRegistry = actionRegistry;
    }

}
