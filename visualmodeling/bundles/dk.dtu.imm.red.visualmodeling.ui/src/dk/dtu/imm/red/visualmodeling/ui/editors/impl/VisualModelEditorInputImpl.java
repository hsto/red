package dk.dtu.imm.red.visualmodeling.ui.editors.impl;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;

public class VisualModelEditorInputImpl extends BaseEditorInput {

	public VisualModelEditorInputImpl(Element element) {
		super(element);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Visual model editor";
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return "This is the visual model editor";
	}
	
}
