package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.ExtensionHelper;

/**
 * 
 * @author Luai
 *
 */
public class VisualMultiCascadeDeleteCommand extends Command {
	
	private List<VisualElementCascadeDeleteCommand> elementCommands;
	private List<VisualConnectionCascadeDeleteCommand> connectionCommands;
	
	private List<Element> specificationElements;
	protected List<Command> commands; // Commands trigered by the cascading when deleting the SE's
	
	public VisualMultiCascadeDeleteCommand(List<VisualElementCascadeDeleteCommand> elementCommands, List<VisualConnectionCascadeDeleteCommand> connectionCommands) {
		this.elementCommands = elementCommands;
		this.connectionCommands = connectionCommands;
		this.specificationElements = new ArrayList<Element>();
		commands = new ArrayList<Command>();
	}
	
	@Override
	public boolean canExecute() {
		return true;
	}

	@Override
	public void execute() {
		commands = new ArrayList<Command>();
		
		for (VisualConnectionCascadeDeleteCommand command : connectionCommands) {
			command.execute();
			if (command.selectedSE != null) {
				specificationElements.add(command.selectedSE);
			}
		}
		
		for (VisualElementCascadeDeleteCommand command : elementCommands) {
			command.execute();
			if (command.selectedSE != null) {
				specificationElements.add(command.selectedSE);
			}
		}
		
		if (!specificationElements.isEmpty()) {
			commands.addAll(ExtensionHelper.runCommand("dk.dtu.imm.red.core.cascadeDelete", specificationElements));
		}
	}

	@Override
	public void undo() {
		
		for (VisualConnectionCascadeDeleteCommand command : connectionCommands) {
			command.undo();			
		}
		
		for (VisualElementCascadeDeleteCommand command : elementCommands) {
			command.undo();
		}
		
		for (Command command : commands) {
			command.undo();
		}
	}
	
	

}
