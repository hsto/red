package dk.dtu.imm.red.visualmodeling.ui.util;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.ScalableRootEditPart;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class CoordinateTranslation {

	public static Point getRelativeLocation(IVisualElement parent, Point absolutePoint)
	{
		Point parentLocation = getAbsoluteLocation(parent);
		return absolutePoint.getTranslated(parentLocation.getNegated());
	}
	
	/*
	 * Returns the absolute coordinate for an IVisualElement in relation to its VisualDiagram.
	 * */
	public static Point getAbsoluteLocation(IVisualElement element)
	{
		IVisualElement current = element;

		Point offset = new Point(0,0);
		
		while(current != null && current != current.getDiagram())
		{								
			offset.translate(current.getLocation());
			
			current = current.getParent();
		}
		
		return offset;
	}
	
	/*
	 * Returns the zoom. If no zoom is found 1.0 is returned.
	 * 50% = 0.5, 200% = 2.0 so reciproc value should be used for scaling translations.
	 * */
	public static double getZoom(EditPart part)
	{
		if(part.getRoot() instanceof ScalableRootEditPart)
		{
			double zoom = ((ScalableRootEditPart)part.getRoot()).getZoomManager().getZoom();
			return zoom;
		}
		else
		{
			return 1.0;
		}		
	}
}
