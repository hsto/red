package dk.dtu.imm.red.visualmodeling.ui.factories;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;

public class DiagramConnectionFactory implements CreationFactory {

	private String type;
	private String name;
	private ConnectionLineStyle lineStyle;
	private ConnectionDecoration sourceDecoration;
	private ConnectionDecoration targetDecoration;
	private ConnectionDirection direction;

	public DiagramConnectionFactory() {

	}

	public DiagramConnectionFactory(String type, String name,
			ConnectionLineStyle linestyle,
			ConnectionDecoration sourceDecoration,
			ConnectionDecoration targetDecoration, ConnectionDirection direction) {
		this.type = type;
		this.name = name;
		this.lineStyle = linestyle;
		this.sourceDecoration = sourceDecoration;
		this.targetDecoration = targetDecoration;
		this.direction = direction;
	}

	@Override
	public Object getNewObject() {
		IVisualConnection connection = visualmodelFactory.eINSTANCE.createVisualConnection();

		if (type != null)
			connection.setType(type);
		if (name != null)
			connection.setName(name);
		if (lineStyle != null)
			connection.setLineStyle(lineStyle);
		if (sourceDecoration != null)
			connection.setSourceDecoration(sourceDecoration);
		if (targetDecoration != null)
			connection.setTargetDecoration(targetDecoration);
		if (direction != null)
			connection.setDirection(direction);
		
		return connection;
	}

	@Override
	public Object getObjectType() {
		return VisualConnection.class;
	}
}
