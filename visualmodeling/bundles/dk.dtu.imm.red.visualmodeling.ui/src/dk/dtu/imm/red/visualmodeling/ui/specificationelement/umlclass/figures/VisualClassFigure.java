package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;

public class VisualClassFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(200,150);
	private Label stereotype = new Label();
	//private RectangleFigure formalFigure;

	//private SVGFigure sketchyFigure;
	private String sketchyFigureURL = svgPath + "Class/Class.svg";

	private CompartmentFigure attrCompartmentFigure = new CompartmentFigure();
	private CompartmentFigure opCompartmentFigure = new CompartmentFigure();


	public VisualClassFigure(IVisualElement model)
	{
		super(model);

		ToolbarLayout layout = new ToolbarLayout();

		formalFigure = new RectangleFigure();
		formalFigure.setBorder(new LineBorder(ColorConstants.black, 1));
	    formalFigure.setFill(true);
	    formalFigure.setBackgroundColor(ColorHelper.classDiagramDefaultColor);
	    formalFigure.setLayoutManager(layout);

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);
		sketchyFigure.setLayoutManager(layout);

		refreshVisuals();
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);

		Rectangle r = getBounds().getCopy();
		boundingRectangle.setVisible(false);

		if(this.getChildren().contains(sketchyFigure)){
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		}
		if(this.getChildren().contains(formalFigure)){
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
		}
	};

	@Override
	public void refreshVisuals() {
		VisualClassElement model = (VisualClassElement)super.model;
		this.removeAll();
		attrCompartmentFigure.removeAll();
		opCompartmentFigure.removeAll();

		//adding all of them back in the right z order
		//toggling between sketchy and non sketchy
		if(model.isIsSketchy()){
			add(sketchyFigure);
			stereotype.setFont(SKETCHY_FONT);
			nameLabel.setFont(SKETCHY_FONT);

			sketchyFigure.add(stereotype);
			sketchyFigure.add(nameLabel);
			sketchyFigure.add(attrCompartmentFigure);
			sketchyFigure.add(opCompartmentFigure);
		}
		else{
			add(formalFigure);
			stereotype.setFont(DEFAULT_FONT);
			nameLabel.setFont(DEFAULT_FONT);

			formalFigure.add(stereotype);
			formalFigure.add(nameLabel);
			formalFigure.add(attrCompartmentFigure);
			formalFigure.add(opCompartmentFigure);
		}


		for(VisualClassAttribute attribute : model.getAttributes())
		{
			Label attrLabel = new Label();
			if(model.isIsSketchy())
				attrLabel.setFont(SKETCHY_FONT);
			else
				attrLabel.setFont(DEFAULT_FONT);
			attrLabel.setText(attribute.toString());
			attrCompartmentFigure.add(attrLabel);

		}

		for(VisualClassOperation operation : model.getOperations())
		{
			Label opLabel = new Label();
			if(model.isIsSketchy())
				opLabel.setFont(SKETCHY_FONT);
			else
				opLabel.setFont(DEFAULT_FONT);
			opLabel.setText(operation.toString());
			opCompartmentFigure.add(opLabel);
		}

		if(model.isInterface()){
			stereotype.setText("<<interface>>");
		}
		else if(model.isAbstract()){
			stereotype.setText("<<abstract>>");
		}
		else{
			stereotype.setText("");
		}

		super.refreshVisuals();
	}


	@Override
	public void add(IFigure figure, Object constraint, int index) {
		if(figure instanceof EmptyFigure)
			//special handling to skip adding the empty figure
			;
		else
			super.add(figure, constraint, index);
	}

	@Override
	public void remove(IFigure figure) {
		if(figure instanceof EmptyFigure)
			//special handling to skip adding the empty figure
			;
		else
			super.remove(figure);
	}

}