package dk.dtu.imm.red.visualmodeling.ui.editors.impl;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;

public class DiagramOptionWizardPage extends WizardPage{

	private Composite container;
	private Combo diagramTypeCombo;
	
	protected DiagramOptionWizardPage(String pageName) {
		super(pageName);	
		
		this.setTitle("Diagram Options");
		this.setDescription("Select options for the diagram to create.");
	}
				
	@Override
	public void createControl(Composite parent) {

		container = new Composite(parent, SWT.NONE);
		
		RowLayout rowLayout = new RowLayout();
	    rowLayout.spacing = 15;
	    rowLayout.marginWidth = 15;
	    rowLayout.marginHeight = 15;
		
	    container.setLayout(rowLayout);
		
	    Label diagramTypeLabel = new Label(container, SWT.NONE);
	    diagramTypeLabel.setText("Diagram type:");
	    
	    diagramTypeCombo = new Combo(container, SWT.DROP_DOWN);

	    for(DiagramType type : DiagramType.VALUES)
	    {	    	
	    	diagramTypeCombo.add(type.getLiteral());	    
	    }
	    							
		setControl(container);
	}
	
	public DiagramType getDiagramType()
	{
		String text = diagramTypeCombo.getText();
		return DiagramType.get(text);
	}

}
