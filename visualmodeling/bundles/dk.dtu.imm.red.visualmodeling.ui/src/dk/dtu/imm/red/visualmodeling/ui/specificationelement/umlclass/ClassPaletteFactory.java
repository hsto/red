package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass;

import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;

import dk.dtu.imm.red.visualmodeling.ui.directedit.CreationAndDirectEditTool;
import dk.dtu.imm.red.visualmodeling.ui.factories.DiagramConnectionFactory;
import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualClassCreationFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualEnumerationCreationFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualPackageCreationFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;

public class ClassPaletteFactory  extends PaletteFactoryBase {

	@Override
	public void populatePaletteInternal(PaletteRoot root) {

		PaletteDrawer group = new PaletteDrawer("Class");

		CreationToolEntry entry;
		entry = new CreationToolEntry("Class", "Create a new class",
				new VisualClassCreationFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Enumeration", "Create a new enumeration",
				new VisualEnumerationCreationFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Package", "Create a new package",
				new VisualPackageCreationFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);
		group.add(new PaletteSeparator());

		ConnectionCreationToolEntry connectionEntry = new ConnectionCreationToolEntry(
				"Association", "Creates a new association connection",
				new DiagramConnectionFactory(ConnectionType.ASSOCIATION.getName(), "",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.NONE,
						ConnectionDirection.BIDIRECTIONAL), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry(
				"Directed Association", "Creates a new directed association connection",
				new DiagramConnectionFactory(ConnectionType.ASSOCIATION.getName(), "",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry(
				"Generalization", "Creates a new generalization connection",
				new DiagramConnectionFactory(ConnectionType.GENERALIZATION.getName(), "",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD_SOLID_WHITE,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry(
				"Composition", "Creates a new composition connection",
				new DiagramConnectionFactory(ConnectionType.COMPOSITION.getName(), "",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.DIAMOND_BLACK,
						ConnectionDecoration.NONE,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry(
				"Aggregation", "Creates a new aggregation connection",
				new DiagramConnectionFactory(ConnectionType.AGGREGATION.getName(), "",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.DIAMOND_WHITE,
						ConnectionDecoration.NONE,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);
		// TODO add composition and aggregation

		root.add(group);
	}

}
