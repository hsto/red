package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualActorFigure extends VisualElementFigure{

	public static Dimension initialBounds = new Dimension(80,150);

	private SVGFigure formalFigure;
//	private SVGFigure sketchyFigure;

	private String formalFigureURL = svgPath + "Usecase/Actor-formal.svg";
	private String sketchyFigureURL = svgPath + "Usecase/Actor.svg";

	public VisualActorFigure(VisualSpecificationElement model)
	{
		super(model);

		formalFigure = new SVGFigure();
		formalFigure.setURI(formalFigureURL);

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);

	    add(this.nameLabel);
	    refreshVisuals();
	}

	@Override
	  protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		
		Rectangle r = getBounds().getCopy();

		boundingRectangle.setVisible(false);

		if(this.getChildren().contains(sketchyFigure))
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height-20));
		if(this.getChildren().contains(formalFigure))
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height-20));
	  }

	@Override
	public void refreshVisuals()
	{
		super.refreshVisuals();
		
		if(this.getChildren().contains(formalFigure))
			remove(formalFigure);
		if(this.getChildren().contains(sketchyFigure))
			remove(sketchyFigure);

		//toggling between sketchy and non sketchy
		if(model.isIsSketchy()){
			add(sketchyFigure,0);
			nameLabel.setFont(SKETCHY_FONT);
		}
		else{

			add(formalFigure,0);
			nameLabel.setFont(DEFAULT_FONT);
		}
	}
}
