package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState;

public class VisualStateFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(100,100);
	//private RoundedRectangle formalFigure;

	//private SVGFigure sketchyFigure;
	private String sketchyFigureURL = svgPath + "Statemachine/State.svg";

	private StateCompartmentFigure stateCompartmentFigure = new StateCompartmentFigure();

	protected Label entryLabel;
	protected Label exitLabel;
	protected Label doLabel;

	public VisualStateFigure(IVisualElement model)
	{
		super(model);

		ToolbarLayout layout = new ToolbarLayout();

		formalFigure = new RoundedRectangle();
		((RoundedRectangle)formalFigure).setCornerDimensions(new Dimension(20,20));
	    formalFigure.setFill(true);
	    formalFigure.setBackgroundColor(ColorHelper.stateMachineDiagramDefaultColor);
	    formalFigure.setLayoutManager(layout);

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);
		sketchyFigure.setLayoutManager(layout);

//	    add(this.nameLabel);
	    entryLabel = new Label();
	    exitLabel = new Label();
	    doLabel = new Label();
//	    add(this.entryLabel);
//	    add(this.exitLabel);
//	    add(this.doLabel);

	    refreshVisuals();
	}

	@Override
	  protected void paintFigure(Graphics graphics) {
		Rectangle r = getBounds().getCopy();

		boundingRectangle.setVisible(false);

		if (this.getChildren().contains(sketchyFigure))
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(formalFigure))
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));

//		setConstraint(nameLabel, new Rectangle(0, 0, r.width, 30));
	  }


	@Override
	public void refreshVisuals()
	{
		VisualState model = (VisualState)super.model;

		if (this.getChildren().contains(formalFigure))
			remove(formalFigure);
		if (this.getChildren().contains(sketchyFigure))
			remove(sketchyFigure);
		if(stateCompartmentFigure.getChildren().contains(entryLabel))
			stateCompartmentFigure.remove(entryLabel);
		if(stateCompartmentFigure.getChildren().contains(exitLabel))
			stateCompartmentFigure.remove(exitLabel);
		if(stateCompartmentFigure.getChildren().contains(doLabel))
			stateCompartmentFigure.remove(doLabel);

		// toggling between sketchy and non sketchy
		if (model.isIsSketchy()) {
			add(sketchyFigure,0);
			sketchyFigure.add(nameLabel);
			sketchyFigure.add(stateCompartmentFigure);
			nameLabel.setFont(SKETCHY_FONT);
			entryLabel.setFont(SKETCHY_FONT);
			exitLabel.setFont(SKETCHY_FONT);
			doLabel.setFont(SKETCHY_FONT);
		} else {
			add(formalFigure,0);
			formalFigure.add(nameLabel);
			formalFigure.add(stateCompartmentFigure);
			nameLabel.setFont(DEFAULT_FONT);
			entryLabel.setFont(DEFAULT_FONT);
			exitLabel.setFont(DEFAULT_FONT);
			doLabel.setFont(DEFAULT_FONT);
		}

		stateCompartmentFigure.add(entryLabel);
		stateCompartmentFigure.add(exitLabel);
		stateCompartmentFigure.add(doLabel);

		nameLabel.setText(model.getName());
		if(model.getEntry()!=null && !model.getEntry().isEmpty())
			entryLabel.setText(" Entry/"+model.getEntry());
		if(model.getExit()!=null && !model.getExit().isEmpty())
			exitLabel.setText(" Exit/"+model.getExit());
		if(model.getDo()!=null && !model.getDo().isEmpty())
			doLabel.setText(" Do/"+model.getDo());


//		System.out.println("THIS:"+this.getClass() + ":" +
//		this.getBounds().x +"," +
//		this.getBounds().y +"," +
//		this.getBounds().width +"," +
//		this.getBounds().height
//			);
//
//for(Object child : this.getChildren()){
//	System.out.println("CHILD:"+child.getClass() + ":" +
//		((Figure)child).getBounds().x +"," +
//		((Figure)child).getBounds().y +"," +
//		((Figure)child).getBounds().width +"," +
//		((Figure)child).getBounds().height
//			);
//}
//System.out.println("----");
	}
}
