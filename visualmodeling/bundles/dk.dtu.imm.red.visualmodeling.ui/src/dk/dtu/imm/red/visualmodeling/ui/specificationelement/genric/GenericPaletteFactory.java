package dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric;

import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;

import dk.dtu.imm.red.visualmodeling.ui.directedit.CreationAndDirectEditTool;
import dk.dtu.imm.red.visualmodeling.ui.factories.DiagramConnectionFactory;
import dk.dtu.imm.red.visualmodeling.ui.factories.GenericCreationFactory;
import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;
import dk.dtu.imm.red.visualmodeling.visualmodel.GenericType;

public class GenericPaletteFactory extends PaletteFactoryBase {

	@Override
	public void populatePaletteInternal(PaletteRoot root) {
		addGenericGroup(root);
	}

	private void addGenericGroup(PaletteRoot root) {
		PaletteDrawer group = new PaletteDrawer("Generic");
		root.add(group);

		CreationToolEntry entry;

		entry = new CreationToolEntry("Rectangle", "Create a new generic rectangle",
				new GenericCreationFactory(GenericType.RECTANGLE), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Circle", "Create a new generic circle",
				new GenericCreationFactory(GenericType.CIRCLE), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Ellipse", "Create a new generic ellipse",
				new GenericCreationFactory(GenericType.ELLIPSE), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Diamond", "Create a new generic diamond",
				new GenericCreationFactory(GenericType.DIAMOND), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Triangle", "Create a new generic triangle",
				new GenericCreationFactory(GenericType.TRIANGLE), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		ConnectionCreationToolEntry connectionEntry = new ConnectionCreationToolEntry(
				"Connection", "Creates a new connection",
				new DiagramConnectionFactory(), null, null);
		group.add(connectionEntry);
	}
}
