package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualConnectionImpl;

/**
 * 
 * @author Luai
 *
 */
public class VisualConnectionCascadeDeleteCommand extends Command {

	protected IVisualConnection connection;
	protected Command command;
	protected List<Command> commands;
	
	protected Element selectedSE;
	protected Group selectedSEParent;

	public VisualConnectionCascadeDeleteCommand(IVisualConnection connection) {
		this.connection = connection;
		if (connection instanceof VisualConnectionImpl) {
			VisualConnectionImpl con = (VisualConnectionImpl) connection;
			selectedSE = con.getSpecificationElement();
			selectedSEParent = con.getSpecificationElement().getParent();
		}
	}

	@Override
	public boolean canExecute() {
		return true;
	}

	@Override
	public void execute() {
		
		command = new VisualConnectionDeleteCommand(connection);
		command.execute();
	}
	
	@Override
	public void undo() {
		// Create SE
		if (connection instanceof VisualConnectionImpl) {
			final Element element = selectedSE;
			final Group parent = selectedSEParent;
			if (parent != null) {
				parent.getContents().add(element);
			}
			
			IConfigurationElement[] configurationElements = Platform
					.getExtensionRegistry().getConfigurationElementsFor(
							"dk.dtu.imm.red.core.element");

			try {
				for (IConfigurationElement configurationElement : configurationElements) {

					final Object extension = configurationElement
							.createExecutableExtension("class");

					if (extension instanceof IElementExtensionPoint) {
						ISafeRunnable runnable = new ISafeRunnable() {

							@Override
							public void handleException(final Throwable exception) {
								exception.printStackTrace();
							}

							@Override
							public void run() throws Exception {
								((IElementExtensionPoint) extension)
										.restoreElement(element);
							}
						};
						runnable.run();
					}
				}
			} catch (CoreException e) {
				e.printStackTrace(); LogUtil.logError(e);
			} catch (Exception e) {
				e.printStackTrace(); LogUtil.logError(e);
			}
		}
		
		command.undo();
		for (Command command : commands) {
			command.undo();
		}
	}

}
