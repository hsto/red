package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.figures.VisualSystemBoundaryFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecaseFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualSystemBoundaryElement;

public class VisualSystemBoundaryElementFactory implements CreationFactory {
	public Object getNewObject() {

		VisualSystemBoundaryElement element = UsecaseFactory.eINSTANCE
				.createVisualSystemBoundaryElement();
		element.setBounds(VisualSystemBoundaryFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualSystemBoundaryElement.class;
	}
}
