package dk.dtu.imm.red.visualmodeling.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.dnd.AbstractTransferDropTargetListener;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.ui.factories.VisualCreationFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;

/*
 * Supports dragging specification elements from ElementExplore to the diagram.
 * Creates a GEF CreateRequest and sets the CreationFactory with the dropped element.
 * */
public class SpecificationElementDropListener extends AbstractTransferDropTargetListener  
{		
		private VisualCreationFactory creationFactory;
		private List<SpecificationElement> droppedElements;
		private DiagramType diagramType;
		
	    public SpecificationElementDropListener(EditPartViewer viewer, Transfer transfer, DiagramType diagramType) {        
	    	super(viewer, transfer);
	    	this.diagramType = diagramType;
	    	this.creationFactory = new VisualCreationFactory();
	    	this.droppedElements = new ArrayList<SpecificationElement>();
	    }

	    @Override
	    protected Request createTargetRequest() {	        	        
	        CreateRequest request = new CreateRequest();
	        request.setFactory(creationFactory);
	        return request;
	    }
	    
	    protected void handleDragOver() {
	        super.handleDragOver();
	    }

	    @Override
	    public void drop(DropTargetEvent event) {
	    	if (LocalSelectionTransfer.getTransfer().isSupportedType(event.currentDataType))
	    	{
	    		ISelection selection = LocalSelectionTransfer.getTransfer().getSelection();	    		
	    		if(selection instanceof StructuredSelection)
	    		{
	    			this.droppedElements = new ArrayList<SpecificationElement>();
	    			List<?> selectionList = ((StructuredSelection)selection).toList();
	    			for (Object obj : selectionList) {
						if (obj instanceof SpecificationElement) {
							droppedElements.add((SpecificationElement) obj);
						}
					}
	    			if (droppedElements.isEmpty()) {
	    				event.detail = DND.DROP_NONE;
	    			}
	    		}
	    	}	    	
	    	super.drop(event);
	    }
	    
	    @Override
	    protected void handleDrop() {	    	
	    	creationFactory.setDiagramType(diagramType);
	        creationFactory.setSpecificationElements(droppedElements);
	    	CreateRequest request = (CreateRequest)getTargetRequest();
    		
	    	request.setFactory(creationFactory);

	        super.handleDrop();
	    }
	    
	    @Override
	    protected void updateTargetRequest() {
	        CreateRequest request = (CreateRequest)getTargetRequest();
    		request.setLocation(getDropLocation());	        	        
	    }

}
