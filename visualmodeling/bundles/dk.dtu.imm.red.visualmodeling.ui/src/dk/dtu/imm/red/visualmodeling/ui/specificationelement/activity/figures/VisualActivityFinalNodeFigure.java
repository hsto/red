package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures;

import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualActivityFinalNodeFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(30, 30);
	private Ellipse formalFigureOuterEllipse, formalFigureInnerEllipse;

	//private SVGFigure sketchyFigure;
	private String sketchyFigureURL = svgPath + "Activity/ActivityFinal.svg";

	public VisualActivityFinalNodeFigure(IVisualElement model) {
		super(model);

		formalFigureOuterEllipse = new Ellipse();
		formalFigureOuterEllipse.setSize(initialBounds);

		formalFigureInnerEllipse = new Ellipse();
		formalFigureInnerEllipse.setSize(initialBounds);
		formalFigureInnerEllipse.setFill(true);
		formalFigureInnerEllipse.setBackgroundColor(ColorHelper.activityDiagramDefaultColor);


		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);

		add(this.nameLabel);
		refreshVisuals();
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		Rectangle r = getBounds().getCopy();

		boundingRectangle.setVisible(false);

		if (this.getChildren().contains(sketchyFigure))
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(formalFigureOuterEllipse)) {
			setConstraint(formalFigureOuterEllipse, new Rectangle(0, 0,
					r.width, r.height));
			setConstraint(formalFigureInnerEllipse, new Rectangle(r.width / 6,
					r.height / 6, r.width * 2 / 3, r.height * 2 / 3));
		}

		setConstraint(nameLabel, new Rectangle(0, 0, r.width, 30));
	}

	@Override
	public void refreshVisuals() {
		if (this.getChildren().contains(formalFigureOuterEllipse)) {
			remove(formalFigureOuterEllipse);
			remove(formalFigureInnerEllipse);
		}
		if (this.getChildren().contains(sketchyFigure)) {
			remove(sketchyFigure);
		}

		// toggling between sketchy and non sketchy
		if (model.isIsSketchy()) {
			add(sketchyFigure, 0);
			nameLabel.setFont(SKETCHY_FONT);
		} else {
			add(formalFigureOuterEllipse, 0);
			add(formalFigureInnerEllipse, 1);
			nameLabel.setFont(DEFAULT_FONT);
		}
		nameLabel.setText(this.model.getName());
	}

}
