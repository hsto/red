package dk.dtu.imm.red.visualmodeling.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.visualmodeling.ui.commands.PropertiesConnectionCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionLinkCommand;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionUnlinkCommand;
import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualConnectionEditPart;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorLabelMouseListener;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;

public class PropertiesConnectionDialog extends Dialog {

	private VisualConnection visualConnection;
	private Connection figure;
	private org.eclipse.draw2d.Label connectionLabel;
	private VisualConnectionEditPart connectionEditPart;
	
	protected Shell shellProperties;
	
	private List<Command> commands;
	
	private Text textName;
	private Combo comboKind;
	private Combo comboFontType;
	private Combo comboFontStyle;
	private Combo comboFontSize;
	private Label labelColor;
	private Combo comboOutlineWeight;
	private Label labelLineColor;

	private SpecificationElement linkedElement = null;
	private boolean unlinkElement = false;
	
	public PropertiesConnectionDialog(Shell parent, int style, VisualConnection visualConnection, EditPart selectedEditPart) {
		super(parent, style);
		setText("SWT Dialog");
		this.visualConnection = visualConnection;
		connectionEditPart = (VisualConnectionEditPart) selectedEditPart;
		this.figure = (Connection) connectionEditPart.getFigure();
		this.connectionLabel = connectionEditPart.getNameLabel();
		
	}
	
	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shellProperties.open();
		shellProperties.layout();
		Display display = getParent().getDisplay();
		while (!shellProperties.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return commands;
	}

	private void createContents() {
		shellProperties = new Shell(getParent(), SWT.DIALOG_TRIM);
		shellProperties.setSize(350, 400);
		shellProperties.setText("Visual Element Properties");
		
		Monitor primary = getParent().getDisplay().getPrimaryMonitor();
	    Rectangle bounds = primary.getBounds();
	    Rectangle rect = shellProperties.getBounds();	    
	    int x = bounds.x + (bounds.width - rect.width) / 2;
	    int y = bounds.y + (bounds.height - rect.height) / 2;	    
	    shellProperties.setLocation(x, y);
	    
	    int sepOffset = 16;
	    
	    Label labelName = new Label(shellProperties, SWT.NONE);
	    labelName.setBounds(10, 10, 40, 15);
	    labelName.setText("Name");
	    
	    textName = new Text(shellProperties, SWT.BORDER);
	    textName.setBounds(10, labelName.getLocation().y + labelName.getSize().y, 200, 24);
	    if (visualConnection.getName() != null) textName.setText(visualConnection.getName());
	    
	    Label labelKind = new Label (shellProperties, SWT.NONE);
	    labelKind.setBounds(textName.getLocation().x + textName.getSize().x + 15, labelName.getLocation().y, 30, 15);
	    labelKind.setText("Kind");
	    
	    comboKind = new Combo(shellProperties, SWT.READ_ONLY);
	    comboKind.setBounds(labelKind.getLocation().x, textName.getLocation().y, 100, 24);
	    if (visualConnection.getType() != null) {
	    	comboKind.add(visualConnection.getType());
	    	comboKind.select(0);	    	
	    } else {
	    	comboKind.setEnabled(false);
	    }
	    
	    
	    Label labelFont = new Label(shellProperties, SWT.NONE);
	    labelFont.setBounds(10, textName.getLocation().y + textName.getSize().y + sepOffset, 40, 15);
	    labelFont.setText("Font");
	    
	    String fontHeight = "" + connectionLabel.getFont().getFontData()[0].getHeight();
	    String fontName = connectionLabel.getFont().getFontData()[0].getName();
	    int fontStyle = connectionLabel.getFont().getFontData()[0].getStyle();
	    
	    comboFontType = new Combo(shellProperties, SWT.READ_ONLY);
	    comboFontType.setBounds(10, labelFont.getLocation().y + labelFont.getSize().y, 100, 24);
	    comboFontType.add("Arial");
	    comboFontType.add("Bradley Hand ITC");
	    comboFontType.add("Calibri");
	    comboFontType.add("Times New Roman");
	    comboFontType.add("Tahoma");
	    comboFontType.add("Segoe UI");
	    comboFontType.select(getComboSelectionIndex(comboFontType, fontName));
	    
	    comboFontStyle = new Combo(shellProperties, SWT.READ_ONLY);
	    comboFontStyle.setBounds(comboFontType.getLocation().x + comboFontType.getSize().x + 10, comboFontType.getLocation().y, 100, 24);
	    comboFontStyle.add("Regular");
	    comboFontStyle.add("Bold");
	    comboFontStyle.add("Italic");
	    comboFontStyle.select(fontStyle);
	    
	    comboFontSize = new Combo(shellProperties, SWT.READ_ONLY);
		comboFontSize.setBounds(comboFontStyle.getLocation().x + comboFontStyle.getSize().x + 10, comboFontType.getLocation().y, 40, 24);
		comboFontSize.add("9");
		comboFontSize.add("10");
		comboFontSize.add("11");
		comboFontSize.add("12");
		comboFontSize.add("14");
		comboFontSize.add("16");
		comboFontSize.add("18");
		comboFontSize.add("20");
		comboFontSize.select(getComboSelectionIndex(comboFontSize, fontHeight));
	    
		labelColor = new Label(shellProperties, SWT.BORDER);
		labelColor.setBounds(comboFontSize.getLocation().x + comboFontSize.getSize().x + 10, comboFontType.getLocation().y, 25, 23);
		//labelColor.setBackground(new Color(shellProperties.getDisplay(), new RGB(0, 255, 0)));
		labelColor.setBackground(connectionLabel.getForegroundColor());
		labelColor.addMouseListener(new ColorLabelMouseListener(labelColor, shellProperties));
		
		Label labelSELink = new Label(shellProperties, SWT.NONE);
		labelSELink.setBounds(10, comboFontType.getLocation().y + comboFontType.getSize().y + sepOffset, 180, 15);
		labelSELink.setText("Linked Specification Element");
		
		final CLabel labeSElLinkValue = new CLabel(shellProperties, SWT.BORDER);
		labeSElLinkValue.setBounds(10, labelSELink.getLocation().y + labelSELink.getSize().y, 240, 24);
		if (visualConnection != null && visualConnection.getSpecificationElement() != null) {
			labeSElLinkValue.setImage(visualConnection.getSpecificationElement().getIcon());
			labeSElLinkValue.setText(visualConnection.getSpecificationElement().getLabel() + " - " + visualConnection.getSpecificationElement().getName());
		}
		//labeSElLinkValue.setBackground(new Color(shellProperties.getDisplay(), new RGB(255, 255, 255)));
		
	    final Button buttonLink = new Button(shellProperties, SWT.NONE);
	    buttonLink.setBounds(labeSElLinkValue.getLocation().x + labeSElLinkValue.getSize().x + 4, labeSElLinkValue.getLocation().y, 24, 24);
	    buttonLink.setText("+");
	    buttonLink.addSelectionListener(new SelectionAdapter() {
			@Override
			@SuppressWarnings("unchecked")
			public void widgetSelected(SelectionEvent e) {				
				SpecificationElementSelectionDialog dialog = new SpecificationElementSelectionDialog(shellProperties, new Class[]{Connector.class});
				dialog.create();
				int result = dialog.open();
				if(result != SpecificationElementSelectionDialog.CANCEL)
				{
					linkedElement = dialog.getSelection();
					unlinkElement = false;
					labeSElLinkValue.setImage(linkedElement.getIcon());
		    		labeSElLinkValue.setText(linkedElement.getLabel() + " - " + linkedElement.getName());
		    		comboKind.select(getComboSelectionIndex(comboKind, linkedElement.getElementKind()));
				}
			}
	    	
		});
	    
	    Button buttonUnlink = new Button(shellProperties, SWT.NONE);
	    buttonUnlink.setBounds(buttonLink.getLocation().x + buttonLink.getSize().x, labeSElLinkValue.getLocation().y, 24, 24);
	    buttonUnlink.setText("-");
	    buttonUnlink.addSelectionListener(new SelectionAdapter() {
	    	@Override
	    	public void widgetSelected(SelectionEvent e) {
	    		linkedElement = null;
	    		unlinkElement = true;
	    		labeSElLinkValue.setImage(null);
	    		labeSElLinkValue.setText("");
	    	}
		});
	    if (visualConnection.getType() == null) { // The generic connection should not be linked
	    	buttonLink.setEnabled(false);
	    	buttonUnlink.setEnabled(false);
	    }
	    
	    
	    Label labelLine = new Label(shellProperties, SWT.NONE);
	    labelLine.setBounds(10, labeSElLinkValue.getLocation().y + labeSElLinkValue.getSize().y + sepOffset, 40, 15);
	    labelLine.setText("Line");
	    
	    PolylineConnection line = (PolylineConnection) figure;
	    
	    comboOutlineWeight = new Combo(shellProperties, SWT.READ_ONLY);
	    comboOutlineWeight.setBounds(10, labelLine.getLocation().y + labelLine.getSize().y, 80, 23);
	    comboOutlineWeight.add("1");
	    comboOutlineWeight.add("2");
	    comboOutlineWeight.add("3");
	    comboOutlineWeight.add("4");
	    comboOutlineWeight.select(line.getLineWidth() - 1);
	    	    
	    labelLineColor = new Label(shellProperties, SWT.BORDER);
	    labelLineColor.setBounds(comboOutlineWeight.getLocation().x + comboOutlineWeight.getSize().x + 10, comboOutlineWeight.getLocation().y, 25, 24);
	    labelLineColor.addMouseListener(new ColorLabelMouseListener(labelLineColor, shellProperties));
	    labelLineColor.setBackground(line.getForegroundColor());
	    //labelLineColor.setBackground(new Color(shellProperties.getDisplay(), new RGB(0, 255, 0)));
	    
	    Button buttonCancel = new Button(shellProperties, SWT.NONE);
	    buttonCancel.setBounds(10, shellProperties.getSize().y - 64, 75, 24);  
	    buttonCancel.setText("Cancel");
	    buttonCancel.addSelectionListener(new SelectionAdapter() {
	    	@Override
			public void widgetSelected(SelectionEvent e) {
				commands = null;
				shellProperties.close();
			}
		});
	    
	    Button buttonDefault = new Button(shellProperties, SWT.NONE);
	    buttonDefault.setBounds(85, shellProperties.getSize().y - 64, 75, 24);
	    buttonDefault.setText("Defaults");
	    buttonDefault.addSelectionListener(new SelectionAdapter() {
	    	@Override
			public void widgetSelected(SelectionEvent e) {
	    		textName.setText("");
	    		comboFontType.select(getComboSelectionIndex(comboFontType, "Segoe UI"));
	    		comboFontSize.select(getComboSelectionIndex(comboFontSize, "9"));
	    		comboFontStyle.select(0);
	    		labelColor.setBackground(ColorHelper.black);
	    		comboOutlineWeight.select(2 - 1);
	    		labelLineColor.setBackground(ColorHelper.black);
	    	}
		});
	    
	    Button buttonOk = new Button(shellProperties, SWT.NONE);
	    buttonOk.setBounds(shellProperties.getSize().x - 90 - 10, shellProperties.getSize().y - 64, 90, 24);
	    buttonOk.setText("Ok");
	    buttonOk.addSelectionListener(new SelectionAdapter() {
	    	@Override
			public void widgetSelected(SelectionEvent e) {
				commands = finish();
				shellProperties.close();
			}
		});
		
	}
	
	private List<Command> finish() {
		List<Command> commands = new ArrayList<Command>();
		PropertiesConnectionCommand propertiesCommand = new PropertiesConnectionCommand(visualConnection, figure, connectionLabel);
		
		if (visualConnection.getName() != null && textName.getText() != null && !visualConnection.getName().equals(textName.getText())) {
			propertiesCommand.setName(textName.getText());
		}
		
		// TODO Kind 
		
		FontData[] fd = connectionLabel.getFont().getFontData();
		fd[0].setHeight(Integer.parseInt(comboFontSize.getItem(comboFontSize.getSelectionIndex())));
		fd[0].setName(comboFontType.getItem(comboFontType.getSelectionIndex()));;
	    fd[0].setStyle(comboFontStyle.getSelectionIndex());
	    
	    propertiesCommand.setFont(new Font(shellProperties.getDisplay(), fd));
	    propertiesCommand.setLabelColor(labelColor.getBackground());
	    
	    // Linking
	    if (linkedElement != null && !linkedElement.equals(visualConnection.getSpecificationElement())) {
	    	commands.add(new VisualConnectionLinkCommand(visualConnection, linkedElement));
	    } else if (unlinkElement && visualConnection.getSpecificationElement() != null) {
	    	commands.add(new VisualConnectionUnlinkCommand(visualConnection));
	    }
	    
	    // Line
	    if (figure != null ) {
	    	if (figure instanceof PolylineConnection && ((PolylineConnection)figure).getLineWidth() != comboOutlineWeight.getSelectionIndex() + 1 ) {
	    		propertiesCommand.setLineWidth(comboOutlineWeight.getSelectionIndex() + 1);
	    	}
	    	if (figure.getForegroundColor() !=  labelLineColor.getBackground()) {
	    		propertiesCommand.setLineColor(labelLineColor.getBackground());
	    	}
	    }
	    
	    commands.add(propertiesCommand);
		
		return commands;
	}
	
	private int getComboSelectionIndex(Combo combo, String str) {
		for (int i = 0; i < combo.getItems().length; i++) {
			if (str.equalsIgnoreCase(combo.getItems()[i])) {
				return i;
			}
		}
		return 0;
	}

}
