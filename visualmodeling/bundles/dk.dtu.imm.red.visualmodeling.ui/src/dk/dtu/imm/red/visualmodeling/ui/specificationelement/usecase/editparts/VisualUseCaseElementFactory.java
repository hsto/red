package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.figures.VisualUseCaseElementFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecaseFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement;

public class VisualUseCaseElementFactory implements CreationFactory {
	public Object getNewObject() {

			VisualUseCaseElement element = UsecaseFactory.eINSTANCE.createVisualUseCaseElement();
			element.setBounds(VisualUseCaseElementFigure.initialBounds);
		    return element;
		  }


		  public Object getObjectType() {
		    return VisualUseCaseElement.class;
		  }
}


