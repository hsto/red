package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.zest.layouts.InvalidLayoutConfiguration;

import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.zestlayout.AutoLayoutProcessor;


public class AutoLayoutCommand extends Command{

	protected Diagram diagram;
	protected String algorithmType;
	AutoLayoutProcessor autoLayoutProc;

	public AutoLayoutCommand(Diagram diagram, String algorithmType)
	{
		this.diagram = diagram;
		this.algorithmType = algorithmType;

	}

	@Override
	public boolean canExecute() {
		return true;
	}

	@Override
	  public void execute() {

		autoLayoutProc = new AutoLayoutProcessor(diagram, algorithmType);
		 try {
			autoLayoutProc.autoLayoutGraph();
		} catch (InvalidLayoutConfiguration e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }

	  @Override
	  public void undo() {
		  try {
			autoLayoutProc.undoAutoLayoutGraph();
		} catch (InvalidLayoutConfiguration e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }

}
