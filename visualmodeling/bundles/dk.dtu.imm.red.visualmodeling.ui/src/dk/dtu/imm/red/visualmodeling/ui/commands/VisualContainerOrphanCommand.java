package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.List;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;


public class VisualContainerOrphanCommand extends Command{
	
	private List<IVisualElement> elements;
	private IVisualElement oldParent;
	
	public VisualContainerOrphanCommand(List<IVisualElement> elements, IVisualElement oldParent)
	{
		this.elements = elements;
		this.oldParent = oldParent;
	}
	
	@Override
	public void execute() {
		System.out.println("VisualContainerOrphanCommand");
		for(IVisualElement element : elements)
		{
			element.setParent(null);
		}
		
		oldParent.getElements().removeAll(elements);
	}
	
	//UNDONE undo
}
