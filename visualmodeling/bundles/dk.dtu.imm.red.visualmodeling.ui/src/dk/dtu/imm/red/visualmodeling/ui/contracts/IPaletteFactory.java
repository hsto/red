package dk.dtu.imm.red.visualmodeling.ui.contracts;

import org.eclipse.gef.palette.PaletteRoot;

public interface IPaletteFactory {

	void populatePalette(PaletteRoot root);
	
}
