package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualActionNodeFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActionNode;

public class VisualActionNodeFactory implements CreationFactory {
	public Object getNewObject() {

		VisualActionNode element = ActivityFactory.eINSTANCE.
				createVisualActionNode();
		element.setBounds(VisualActionNodeFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualActionNode.class;
	}
}
