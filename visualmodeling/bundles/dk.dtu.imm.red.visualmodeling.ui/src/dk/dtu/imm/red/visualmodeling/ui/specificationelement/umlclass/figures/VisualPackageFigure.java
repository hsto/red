package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualPackageFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(400, 400);

	private IVisualElement model;
	private SVGFigure formalFigure;
//	private SVGFigure sketchyFigure;

	private String sketchyFigureURL = svgPath + "Class/Package.svg";
	private String formalFigureURL = svgPath + "Class/Package-formal.svg";
	public VisualPackageFigure(IVisualElement model) {
		super(model);

		this.model = model;

		formalFigure = new SVGFigure();
		formalFigure.setURI(formalFigureURL);

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);

		add(nameLabel);

	}

	@Override
	protected void paintFigure(Graphics graphics) {

		Rectangle r = getBounds().getCopy();

		// boundingRectangle.setVisible(false);

//		nameLabel.setText(model.getName());

		if (this.getChildren().contains(sketchyFigure))
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(formalFigure))
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));

		setConstraint(nameLabel, new Rectangle(15,8, r.width, 20));
	}

	@Override
	public void refreshVisuals() {

		if(this.getChildren().contains(formalFigure)){
			remove(formalFigure);
		}
		if(this.getChildren().contains(sketchyFigure)){
			remove(sketchyFigure);
		}

		//toggling between sketchy and non sketchy
		if(model.isIsSketchy()){
			add(sketchyFigure,0);
			nameLabel.setFont(SKETCHY_FONT);
		}
		else{
			add(formalFigure,0);
			nameLabel.setFont(DEFAULT_FONT);
		}

		nameLabel.setLabelAlignment(PositionConstants.LEFT);
		nameLabel.setText(this.model.getName());

	}
}
