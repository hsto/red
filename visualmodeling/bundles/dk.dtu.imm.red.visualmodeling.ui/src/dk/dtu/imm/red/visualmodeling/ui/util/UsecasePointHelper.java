package dk.dtu.imm.red.visualmodeling.ui.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.specificationelements.SpecificationElement;

public class UsecasePointHelper {

	private UsecasePoint up;
	private Map<String, List<Element>> elementMap;
	
	public UsecasePointHelper()
	{
		List<Project> currentlyOpenedProjects = WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects(); 
		Project p =  currentlyOpenedProjects.size() > 0 ? currentlyOpenedProjects.get(0) : null; 				
		up = p.getEffort();		
	}
	
	public void calculate(SpecificationElement element)
	{
		ArrayList<SpecificationElement> list = new ArrayList<SpecificationElement>();
		list.add(element);
		calculate(list);
	}
	
	public void calculate(List<SpecificationElement> elements)
	{
		List<Element> list = new ArrayList<Element>(elements);
		elementMap = up.getElementMap(list);
		up.getSum(elementMap); // Must be called before anything else to ensure correct calculation
	}
	
	public double getRequirementFactor()
	{
		return up.getRequirementFactor().getSum(elementMap);
	}
	
	public double getManagementFactor()
	{
		return up.getManagementFactor().getSum();
	}	
	
	public double getTechnologyFactor()
	{
		return up.getTechnologyFactor().getSum();
	}
	
	public double getProductivityFactor()
	{
		return up.getProductivityFactor().getSum();
	}
	
	public double getEffort()
	{
		return up.getSum(elementMap);
	}
	
	public double getUsecasePoints()
	{
		return up.getUsecasePoints();
	}
}
