package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.draw2d.IFigure;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualObjectNodeFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualObjectNodeEditPart extends VisualElementEditPart{

	public VisualObjectNodeEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	@Override
	protected IFigure createFigure() {
		return new VisualObjectNodeFigure((IVisualElement)getModel());
	}

}
