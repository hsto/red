package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.commands.VisualEnumerationUpdateCommand;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualEnumerationEditPart;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral;

public class VisualEnumSpecificationEditDialog extends Dialog {
	private VisualEnumerationElement model;
	private VisualEnumerationElement modelCopy;

	private VisualEnumerationEditPart editPart;
	private Table tableLiterals;
	private TableViewer tableViewerLiterals;

	private TableViewerColumn tvClmnLiteralName;
	private Text textName;

	/**
	 * Create the dialog.
	 *
	 * @param parentShell
	 */
	public VisualEnumSpecificationEditDialog(Shell parentShell, Object inputModel,
			VisualEnumerationEditPart editPart) {
		super(parentShell);
		setShellStyle(SWT.MAX | SWT.RESIZE);
		if (inputModel instanceof VisualEnumerationElement) {
			this.model = (VisualEnumerationElement) inputModel;
		} else {
			this.model = ClassFactory.eINSTANCE.createVisualEnumerationElement();
		}
		//copy without references
		EcoreUtil.Copier copier = new EcoreUtil.Copier(true,false);
		this.modelCopy = (VisualEnumerationElement) copier.copy(this.model);
		this.editPart = editPart;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Enum Specification");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, false));

		Label lblName = new Label(container, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name");

		textName = new Text(container, SWT.BORDER);
		GridData gd_textName = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_textName.widthHint = 243;
		textName.setLayoutData(gd_textName);
		textName.addFocusListener(new FocusListener(){
			@Override
			public void focusGained(FocusEvent fe) {
			}

			@Override
			public void focusLost(FocusEvent fe) {
				modelCopy.setName(textName.getText());
			}

		});


		// ---------------- Literals SECTION ------------------------------
		Label lblAttributes = new Label(container, SWT.NONE);
		lblAttributes.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false,
				false, 1, 1));
		lblAttributes.setText("Enum Literals:");
		new Label(container, SWT.NONE);

//		ScrolledComposite scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
//		GridData gd_scrolledComposite = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
//		gd_scrolledComposite.heightHint = 133;
//		gd_scrolledComposite.widthHint = 348;
//		scrolledComposite.setLayoutData(gd_scrolledComposite);
//		scrolledComposite.setExpandHorizontal(true);
//		scrolledComposite.setExpandVertical(true);

		tableViewerLiterals = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		tableLiterals = tableViewerLiterals.getTable();
		GridData gd_tableLiterals = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_tableLiterals.heightHint = 136;
		tableLiterals.setLayoutData(gd_tableLiterals);
		tableLiterals.setLinesVisible(true);
		tableLiterals.setHeaderVisible(true);

		tvClmnLiteralName = new TableViewerColumn(tableViewerLiterals, SWT.NONE);
		TableColumn tblclmnEnumLiteral = tvClmnLiteralName.getColumn();
		tblclmnEnumLiteral.setWidth(363);
		tblclmnEnumLiteral.setText("Enum Literal");
//		scrolledComposite.setContent(tableLiterals);
//		scrolledComposite.setMinSize(tableLiterals.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		Button btnAddLiteral = new Button(container, SWT.NONE);
		btnAddLiteral.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				createNewLiteral();
				tableViewerLiterals.refresh();
				editPart.refresh();
			}

		});
		btnAddLiteral.setText("Add Literal");

		Button btnDeleteLiteral = new Button(container, SWT.NONE);
		btnDeleteLiteral.setText("Delete Literal");
		btnDeleteLiteral.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				int[] selections = tableLiterals.getSelectionIndices();
				IVisualElement[] literalsToRemove = new VisualEnumerationLiteral[selections.length];
				for (int i = 0; i < selections.length; i++) {
					literalsToRemove[i] = modelCopy.getLiterals().get(selections[i]);
				}
				for (int i = 0; i < literalsToRemove.length; i++) {
					modelCopy.getElements().remove(literalsToRemove[i]);
					modelCopy.getLiterals().remove(literalsToRemove[i]);
				}
				tableViewerLiterals.refresh();
				editPart.refresh();
			}
		});



		setTableLiteralsEditorAndViewer();
		loadInitialData();

		return container;
	}

	private void setTableLiteralsEditorAndViewer() {
		// Attribute Table - Column Name
		tvClmnLiteralName.setEditingSupport(new EditingSupport(
				tableViewerLiterals) {
			private TextCellEditor cellEditor = new TextCellEditor(
					tableLiterals);

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				return cellEditor;
			}

			protected Object getValue(Object element) {
				return ((VisualEnumerationLiteral) element).getName();
			}

			protected void setValue(Object element, Object value) {
				VisualEnumerationLiteral attr = (VisualEnumerationLiteral) element;
				attr.setName(value.toString());
				getViewer().update(element, null);
				editPart.refresh();
			}
		});
		tvClmnLiteralName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VisualEnumerationLiteral attr = ((VisualEnumerationLiteral) element);
				if (element == null || attr.getName() == null)
					return "";
				else
					return attr.getName();
			}
		});
	}


	private void loadInitialData() {
		if (modelCopy.getName()!=null && !modelCopy.getName().isEmpty()){
			textName.setText(modelCopy.getName());
		}
		tableViewerLiterals.setContentProvider(new ArrayContentProvider());
		tableViewerLiterals.setInput(modelCopy.getLiterals());
	}

	/**
	 * Create contents of the button bar.
	 *
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button OKButton = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		OKButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				VisualEnumerationUpdateCommand updateEnum = new VisualEnumerationUpdateCommand(model,modelCopy,editPart);
				editPart.getRoot().getViewer().getEditDomain().getCommandStack().execute(updateEnum);
			}
		});
		// createButton(parent, IDialogConstants.CANCEL_ID,
		// IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 328);
	}

	private VisualEnumerationLiteral createNewLiteral() {
		VisualEnumerationLiteral newLiteral = ClassFactory.eINSTANCE.createVisualEnumerationLiteral();
		newLiteral.setLocation(new org.eclipse.draw2d.geometry.Point(0, 0));
		newLiteral.setBounds(new Dimension(0, 0));
		newLiteral.setName("newLiteral");

		newLiteral.setDiagram(modelCopy.getDiagram());
		newLiteral.setParent(modelCopy);
		modelCopy.getElements().add(newLiteral);
		if (!modelCopy.getLiterals().contains(newLiteral)) {
			modelCopy.getLiterals().add(newLiteral);
		}
		return newLiteral;
	}


}
