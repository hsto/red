package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.visualmodeling.traceability.TraceLinkUtil;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualClassEditPart;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;

public class VisualClassUpdateCommand extends Command {

	protected VisualClassEditPart editPart;

	protected VisualClassElement model;
	protected VisualClassElement oldModel;
	protected VisualClassElement newModel;

	private List<IVisualElement> modifiedChildrenList = new ArrayList<IVisualElement>();
	private List<IVisualElement> addedChildrenList = new ArrayList<IVisualElement>();
	private List<IVisualElement> deletedChildrenList = new ArrayList<IVisualElement>();


	public VisualClassUpdateCommand(VisualClassElement model, VisualClassElement newModel, VisualClassEditPart editPart) {
		this.model = model;
		EcoreUtil.Copier copier = new EcoreUtil.Copier(false,false);
		this.oldModel = (VisualClassElement) copier.copy(model);
		this.newModel = newModel;
		this.editPart = editPart;

		for(IVisualElement child : model.getElements()){
			String visID = child.getVisualID();
			boolean found=false;
			for(IVisualElement newModelChild : newModel.getElements()){
				if(visID.equals(newModelChild.getVisualID())){
					found=true;
					if(!child.equals(newModelChild)){
						modifiedChildrenList.add(child);
					}
				}
			}
			if(!found)
				deletedChildrenList.add(child);
		}


		for(IVisualElement newModelChild : newModel.getElements()){
			boolean found = false;
			for(IVisualElement child : model.getElements()){
				if(child.getVisualID().equals(newModelChild.getVisualID())){
					found=true;
				}
			}
			if(!found)
				addedChildrenList.add(newModelChild);
		}


		System.out.println("MOD:"+modifiedChildrenList);
		System.out.println("INS:"+addedChildrenList);
		System.out.println("DEL:"+deletedChildrenList);

	}

	@Override
	public void execute() {
		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
			applyUpdates(model, newModel,true);
		}
		else{
			applyUpdates(model, newModel,false);
		}
		applyInsertDelete(model);
		editPart.refresh();
	}


	private void applyUpdates(VisualClassElement modelTobeChanged, VisualClassElement modelChanges, boolean cascadeUpdates){

		for(IVisualElement child : modifiedChildrenList){
			for(VisualClassAttribute attr : modelChanges.getAttributes()){
				if(child.getVisualID().equals(attr.getVisualID())){
					//put the updates towards all traces
					Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(child);
//					System.out.println("TRACES OF ATTRIBUTE:");
//					for(IVisualElement trace : allTraces){
//						System.out.println(trace.getDiagram().getDiagramElement().getLabel());
//					}
					for(IVisualElement element : allTraces){
						((VisualClassAttribute)element).setValues(attr);
					}
					((VisualClassAttribute)child).setValues(attr);

					break;
				}
			}
			for(VisualClassOperation op : modelChanges.getOperations()){
				if(child.getVisualID().equals(op.getVisualID())){
					//put the updates towards all traces
					Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(child);
//					System.out.println("TRACES OF OPERATION");
//					for(IVisualElement trace : allTraces){
//						System.out.println(trace.getDiagram().getDiagramElement().getLabel());
//					}
					for(IVisualElement element : allTraces){
						((VisualClassOperation)element).setValues(op);
					}
					((VisualClassOperation)child).setValues(op);

					break;
				}
			}
		}


		if(cascadeUpdates){
			//put the updates towards all traces
			Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(modelTobeChanged);
			for(IVisualElement element : allTraces){
				updateClassValues((VisualClassElement)element, modelChanges.getName(), modelChanges.isInterface(), modelChanges.isAbstract());
			}
		}
		updateClassValues(modelTobeChanged, modelChanges.getName(), modelChanges.isInterface(), modelChanges.isAbstract());

	}


	public void applyInsertDelete(VisualClassElement modelTobeChanged){
		for(IVisualElement child : addedChildrenList){
			if(child instanceof VisualClassAttribute)
				modelTobeChanged.getAttributes().add((VisualClassAttribute)child);
			if(child instanceof VisualClassOperation)
				modelTobeChanged.getOperations().add((VisualClassOperation)child);
			modelTobeChanged.getElements().add(child);
			child.setParent(modelTobeChanged);
			child.setDiagram(modelTobeChanged.getDiagram());

		}
		for(IVisualElement child : deletedChildrenList){
			child.setParent(null);
			child.setDiagram(null);
			if(child instanceof VisualClassAttribute)
				modelTobeChanged.getAttributes().remove((VisualClassAttribute)child);
			if(child instanceof VisualClassOperation)
				modelTobeChanged.getOperations().remove((VisualClassOperation)child);
			modelTobeChanged.getElements().remove(child);
		}
	}


	public void undoInsertDelete(VisualClassElement modelTobeChanged){
		for(IVisualElement child : deletedChildrenList){
			if(child instanceof VisualClassAttribute)
				modelTobeChanged.getAttributes().add((VisualClassAttribute)child);
			if(child instanceof VisualClassOperation)
				modelTobeChanged.getOperations().add((VisualClassOperation)child);
			modelTobeChanged.getElements().add(child);
			child.setParent(modelTobeChanged);
			child.setDiagram(modelTobeChanged.getDiagram());

		}

		for(IVisualElement child : addedChildrenList){
			child.setParent(null);
			child.setDiagram(null);
			if(child instanceof VisualClassAttribute)
				modelTobeChanged.getAttributes().remove((VisualClassAttribute)child);
			if(child instanceof VisualClassOperation)
				modelTobeChanged.getOperations().remove((VisualClassOperation)child);
			modelTobeChanged.getElements().remove(child);
		}
	}



	public void updateClassValues(VisualClassElement element, String name, boolean isInterface, boolean isAbstract){
		element.setName(name);
		element.setInterface(isInterface);
		element.setAbstract(isAbstract);
	}



	@Override
	public void undo() {

		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
			applyUpdates(model, oldModel,true);
		}
		else{
			applyUpdates(model, oldModel,false);
		}
		undoInsertDelete(model);
		editPart.refresh();
	}

}
