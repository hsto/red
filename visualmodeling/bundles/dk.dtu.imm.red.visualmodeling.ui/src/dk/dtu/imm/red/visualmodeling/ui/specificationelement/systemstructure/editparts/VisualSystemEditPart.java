package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts;

import org.eclipse.draw2d.IFigure;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.figures.VisualSystemFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualSystemEditPart extends VisualElementEditPart{

	public VisualSystemEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	@Override
	protected IFigure createFigure() {
		return new VisualSystemFigure((IVisualElement)getModel());
	}

}
