package dk.dtu.imm.red.visualmodeling.ui.util;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

public class ColorHelper {

	public static Color white = ColorConstants.white;
	public static Color lightRed = getColor(255,166,166);
	public static Color lightOrange = getColor(255,165,74);
	public static Color lightYellow = getColor(248,248,200);
	public static Color lightGreen =  getColor(138,255,138);
	public static Color lightNavy = getColor(170,255,255);
	public static Color lightBlue = getColor(193,193,255);
	public static Color lightPurple = getColor(210,166,255);
	public static Color lightPink = getColor(255,159,255);
	public static Color lightGray = getColor(200,200,200);
	public static Color red = ColorConstants.red;
	public static Color black = ColorConstants.black;
	public static Color blue = ColorConstants.blue;
	public static Color yellow = ColorConstants.yellow;

	public static Color useCaseDiagramDefaultColor = lightGreen;
	public static Color classDiagramDefaultColor = lightYellow;
	public static Color goalDiagramDefaultColor = lightPurple;
	public static Color stateMachineDiagramDefaultColor = lightBlue;
	public static Color activityDiagramDefaultColor = lightGray;



	public static Color getColor(int r, int g, int b) {
		Color color = new Color(Display.getCurrent(), r,g,b);
		return color;
	}
}
