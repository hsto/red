package dk.dtu.imm.red.visualmodeling.ui.editors;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface INewDiagramWizard extends IBaseWizard{

	String ID = "dk.dtu.imm.red.visualmodeling.newwizard";
}
