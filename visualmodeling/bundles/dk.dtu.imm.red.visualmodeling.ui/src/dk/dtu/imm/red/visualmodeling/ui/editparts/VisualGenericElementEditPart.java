package dk.dtu.imm.red.visualmodeling.ui.editparts;

import org.eclipse.draw2d.IFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualGenericElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement;

public class VisualGenericElementEditPart extends VisualElementEditPart{

	public VisualGenericElementEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}
	
	@Override
	protected IFigure createFigure() {
		
		return new VisualGenericElementFigure((VisualGenericElement)getModel());
	}

}
