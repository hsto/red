package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.commands.VisualStateUpdateCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState;

public class VisualStateSpecificationEditDialog extends Dialog {
	private VisualState model;
	private VisualState modelCopy;

	private VisualStateEditPart editPart;
	private Text textName;
	private Text textEntry;
	private Text textExit;
	private Text textDo;

	/**
	 * Create the dialog.
	 *
	 * @param parentShell
	 */
	public VisualStateSpecificationEditDialog(Shell parentShell, Object inputModel,
			VisualStateEditPart editPart) {
		super(parentShell);
		setShellStyle(SWT.MAX | SWT.RESIZE);
		if (inputModel instanceof VisualState) {
			this.model = (VisualState) inputModel;
		} else {
			this.model = StatemachineFactory.eINSTANCE.createVisualState();
		}
		//copy without references
		EcoreUtil.Copier copier = new EcoreUtil.Copier(true,false);
		this.modelCopy = (VisualState) copier.copy(this.model);
		this.editPart = editPart;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("State Specification");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, false));

		Label lblName = new Label(container, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name");

		textName = new Text(container, SWT.BORDER);
		GridData gd_textName = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_textName.widthHint = 243;
		textName.setLayoutData(gd_textName);
		textName.addFocusListener(new FocusListener(){
			@Override
			public void focusGained(FocusEvent fe) {
			}

			@Override
			public void focusLost(FocusEvent fe) {
				modelCopy.setName(textName.getText());
			}

		});

		Label lblEntry = new Label(container, SWT.NONE);
		lblEntry.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEntry.setText("Entry");

		textEntry = new Text(container, SWT.BORDER);
		textEntry.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		textEntry.addFocusListener(new FocusListener(){
			@Override
			public void focusGained(FocusEvent fe) {
			}

			@Override
			public void focusLost(FocusEvent fe) {
				modelCopy.setEntry(textEntry.getText());
			}

		});

		Label lblExit = new Label(container, SWT.NONE);
		lblExit.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblExit.setText("Exit");

		textExit = new Text(container, SWT.BORDER);
		textExit.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		textExit.addFocusListener(new FocusListener(){
			@Override
			public void focusGained(FocusEvent fe) {
			}

			@Override
			public void focusLost(FocusEvent fe) {
				modelCopy.setExit(textExit.getText());
			}

		});

		Label lblDo = new Label(container, SWT.NONE);
		lblDo.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDo.setText("Do");

		textDo = new Text(container, SWT.BORDER);
		textDo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		textDo.addFocusListener(new FocusListener(){
			@Override
			public void focusGained(FocusEvent fe) {
			}

			@Override
			public void focusLost(FocusEvent fe) {
				modelCopy.setDo(textDo.getText());
			}

		});

		loadInitialData();

		return container;
	}



	private void loadInitialData() {
		if (modelCopy.getName()!=null && !modelCopy.getName().isEmpty()){
			textName.setText(modelCopy.getName());
		}
		if (modelCopy.getEntry()!=null && !modelCopy.getEntry().isEmpty()){
			textEntry.setText(modelCopy.getEntry());
		}
		if (modelCopy.getExit()!=null && !modelCopy.getExit().isEmpty()){
			textExit.setText(modelCopy.getExit());
		}
		if (modelCopy.getDo()!=null && !modelCopy.getDo().isEmpty()){
			textDo.setText(modelCopy.getDo());
		}
	}

	/**
	 * Create contents of the button bar.
	 *
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button OKButton = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		OKButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				VisualStateUpdateCommand updateState = new VisualStateUpdateCommand(model,modelCopy,editPart);
				editPart.getRoot().getViewer().getEditDomain().getCommandStack().execute(updateState);
			}
		});
		// createButton(parent, IDialogConstants.CANCEL_ID,
		// IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 207);
	}


}
