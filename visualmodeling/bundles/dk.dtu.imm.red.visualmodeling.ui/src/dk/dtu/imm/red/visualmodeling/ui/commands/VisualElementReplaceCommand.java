package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

public class VisualElementReplaceCommand extends Command{
	 
	  private IVisualElement parent;
	  private IVisualElement element;
	  private IVisualElement replacement;
	 
	  private Command add;
	  private Command delete;
	  
	  public VisualElementReplaceCommand(VisualDiagram diagram, IVisualElement parent, IVisualElement element, IVisualElement replacement)
	  {
		  this.parent = parent;
		  this.element = element;
		  this.replacement = replacement;
	  }
	  
	  @Override public void execute() {
		replacement.setLocation(element.getLocation());
		replacement.setBounds(element.getBounds()); 
		
		List<IVisualElement> replacementList = new ArrayList<IVisualElement>();
		replacementList.add(replacement);
		
		add = new VisualContainerAddCommand(replacementList,parent, new Point[] {element.getLocation()});		
		delete = new VisualElementDeleteCommand(element);
		
		// Manual handling of connections so we reuse them otherwise delete would clean them up
		for(IVisualConnection connection : element.getConnections())
		{
			IVisualElement source = connection.getSource();
			IVisualElement target = connection.getTarget();
			
			if(source == element)
			{
				connection.setSource(replacement);
			}
			if(target == element)
			{
				connection.setTarget(replacement);
			}			
			
			replacement.getConnections().add(connection);
		}
		
		element.getConnections().clear();
						
		add.execute();
		delete.execute();
	  }
	 
	  @Override public void undo() {		  
		  // TODO implement undo
	  }
}
