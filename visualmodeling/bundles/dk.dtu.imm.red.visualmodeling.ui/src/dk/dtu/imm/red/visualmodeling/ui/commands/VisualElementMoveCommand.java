package dk.dtu.imm.red.visualmodeling.ui.commands;


import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualPortLocationCalculator;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;

public class VisualElementMoveCommand extends Command {

	protected Rectangle oldConstraint;
	protected Rectangle newConstraint;
	protected IVisualElement element;

  public VisualElementMoveCommand(IVisualElement element, Rectangle newConstraint)
  {
	  this.element = element;
	  this.newConstraint = newConstraint;
  }

  @Override
  public void execute() {

	  if(oldConstraint == null) {
		  oldConstraint = new Rectangle(element.getLocation(), element.getBounds());
    }

	element.setLocation(newConstraint.getLocation());
    element.setBounds(newConstraint.getSize());

	// only for port visual element, need to overwrite the location
    IVisualElement parent = element.getParent();
    if (element instanceof VisualPort && parent!=null && parent instanceof VisualSystem)
		VisualPortLocationCalculator.recalculatePortStickingLocation((VisualPort) element);
  }

  @Override public void undo() {

	element.setLocation(oldConstraint.getLocation());
    element.setBounds(oldConstraint.getSize());
  }
}