package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualEditorConfigurationBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericPaletteFactory;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;


public class StateMachineConfiguration extends VisualEditorConfigurationBase{

	public StateMachineConfiguration()
	{
		this.ruleCollection = new RuleCollection();
		this.ruleCollection.addRule(new StateMachineRule());

		this.paletteFactory = new StateMachinePaletteFactory();
		this.paletteFactory.setNext(new GenericPaletteFactory());

		this.editPartFactory = new StateMachineEditPartFactory(ruleCollection);
		this.editPartFactory.setNext(new GenericEditPartFactory(ruleCollection));
	}
}
