package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase;

import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;

import dk.dtu.imm.red.visualmodeling.ui.directedit.CreationAndDirectEditTool;
import dk.dtu.imm.red.visualmodeling.ui.factories.DiagramConnectionFactory;
import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualActorElementFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualSystemBoundaryElementFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualUseCaseElementFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;

public class UseCasePaletteFactory extends PaletteFactoryBase {

	@Override
	public void populatePaletteInternal(PaletteRoot root) {

		PaletteDrawer group = new PaletteDrawer("Use Case");

		CreationToolEntry entry;
		entry = new CreationToolEntry("Use Case", "Create a new use case",
				new VisualUseCaseElementFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Actor", "Create a new actor",
				new VisualActorElementFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("System Boundary",
				"Create a new system boundary",
				new VisualSystemBoundaryElementFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		group.add(new PaletteSeparator());

		ConnectionCreationToolEntry connectionEntry = new ConnectionCreationToolEntry(
				"Association", "Creates a new Association connection",
				new DiagramConnectionFactory(ConnectionType.ASSOCIATION.getName(), "",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.NONE, ConnectionDirection.BIDIRECTIONAL), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry(
				"Generalization", "Creates a new generalization connection",
				new DiagramConnectionFactory(ConnectionType.GENERALIZATION.getName(), "",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD_SOLID_WHITE,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry("Extend",
				"Creates a new extend connection",
				new DiagramConnectionFactory(ConnectionType.EXTEND.getName(),
						ConnectionType.EXTEND.getName(),
						ConnectionLineStyle.DASHED, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		connectionEntry = new ConnectionCreationToolEntry("Include",
				"Creates a new include connection",
				new DiagramConnectionFactory(
						ConnectionType.INCLUDE.getName(),
						ConnectionType.INCLUDE.getName(),
						ConnectionLineStyle.DASHED, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), null, null);
		group.add(connectionEntry);

		root.add(group);
	}

}
