package dk.dtu.imm.red.visualmodeling.ui.editors.impl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.visualmodeling.ui.editors.*;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;

public class NewDiagramWizardPresenter extends BaseWizardPresenter implements INewDiagramWizardPresenter {

	
	
	public NewDiagramWizardPresenter(NewDiagramWizardImpl newDiagramWizardImpl) {
		

	}

	@Override
	public void wizardFinished(String label, String name, String description, Group parent, String path, DiagramType diagramType) {

		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
				.getOperationSupport();

		CreateDiagramOperation operation = 
				new CreateDiagramOperation(label, name, description, parent, diagramType);

		operation.addContext(operationSupport.getUndoContext());

		IAdaptable info = new IAdaptable() {
			@Override
			public Object getAdapter(@SuppressWarnings("rawtypes") final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		try {
			OperationHistoryFactory.getOperationHistory().execute(operation, null,
					info);
		} catch (ExecutionException e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}


}
