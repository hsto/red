package dk.dtu.imm.red.visualmodeling.ui.editors.impl;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.ui.VisualEditor;
import dk.dtu.imm.red.visualmodeling.ui.editors.IVisualModelEditor;
import dk.dtu.imm.red.visualmodeling.ui.factories.VisualEditorFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind;


public class VisualModelEditorImpl extends BaseEditor implements IVisualModelEditor {

	protected IEditorInput input;
	protected ElementBasicInfoContainer basicInfoContainer;
	protected VisualEditor editor;
	private Text mergeLogTextbox;

	protected Diagram diagram;

	public String getHelpResourceURL() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
		this.input = input;
		presenter = new VisualModelPresenterImpl(element);
		diagram = (Diagram)element;

		try
		{
			VisualEditorFactory factory = new VisualEditorFactory();
			editor = factory.createVisualEditor(diagram.getVisualDiagram().getDiagramType());
		}catch(Exception e)
		{
			throw e;
		}
	}

	@Override
	public String getEditorID() {
		// TODO Auto-generated method stub
		return ID;
	}

	@Override
	protected void fillInitialData() {
		
		String[] kindStrings = new String[]{};
		
		if (((Diagram)element).getVisualDiagram().getDiagramType() == DiagramType.SYSTEM_STRUCTURE) {
			SystemStructureDiagramKind[] kinds = SystemStructureDiagramKind.values();
			kindStrings = new String[kinds.length];
			for (int i = 0; i < kinds.length; i++) {
				kindStrings[i] = kinds[i].getLiteral();
			}
		}

		basicInfoContainer.fillInitialData(element, kindStrings);

		if(element instanceof Diagram){
			String mergeLogString = ((Diagram)element).getMergeLog();
			if(mergeLogString!=null){
				mergeLogTextbox.setText(mergeLogString);
			}
		}

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override public boolean isDirty() {
		return editor.isDirty() || this.isDirty;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		editor.doSave(monitor);
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		super.doSave(monitor);
	};

	@Override
	protected void createPages() {

		try {
			Composite parent = getContainer();

			int visualEditorindex = addPage(editor, input);
			setPageText(visualEditorindex, "Visual Diagram Editor");

			int editorIndex = addPage(createEditorPage(parent));
			setPageText(editorIndex, "Diagram Editor");

			super.createPages();

			fillInitialData();

		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}

	private Composite createEditorPage(Composite parent)
	{
		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (SpecificationElement) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);


		Label lblMergeLog = new Label(composite, SWT.NONE);
		lblMergeLog.setText("Merge Log:");

		mergeLogTextbox = new Text(composite, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_text.heightHint = 350;
		gd_text.widthHint = 415;
		mergeLogTextbox.setLayoutData(gd_text);
		mergeLogTextbox.setEditable(false);

		/*
		Label diagramTextLabel = new Label(composite, SWT.NONE);
		diagramTextLabel.setText("Diagram Text:");

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalSpan = 2;
		editorLayoutData.widthHint = 400;
		editorLayoutData.heightHint = 200;
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessVerticalSpace = true;

		visionEditor = new RichTextEditor(composite, SWT.BORDER |
				SWT.WRAP | SWT.V_SCROLL, getEditorSite(), commandListener);
		visionEditor.setLayoutData(editorLayoutData);

		visionEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});
		*/
		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	public VisualEditor getVisualEditor(){
		return editor;
	}
}
