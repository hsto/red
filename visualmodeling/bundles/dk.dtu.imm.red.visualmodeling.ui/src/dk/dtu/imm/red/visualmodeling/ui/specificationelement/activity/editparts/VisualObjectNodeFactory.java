package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualObjectNodeFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode;

public class VisualObjectNodeFactory implements CreationFactory {
	public Object getNewObject() {

		VisualObjectNode element = ActivityFactory.eINSTANCE
				.createVisualObjectNode();
		element.setBounds(VisualObjectNodeFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualObjectNode.class;
	}
}
