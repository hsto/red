package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.Set;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.traceability.TraceLinkUtil;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;

public class VisualConnectionUnlinkCommand extends Command{

	private VisualConnection conncetion;
	private SpecificationElement specificationElement;

	    public VisualConnectionUnlinkCommand(VisualConnection connection) {
	    	this.conncetion = connection;
	    }

		@Override public void execute() {
	    	unlinkElement(conncetion);
	    	if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
				Set<IVisualConnection> allTraces = TraceLinkUtil.findAllTrace(conncetion);
				for(IVisualConnection e : allTraces){
					unlinkElement(e);
				}
			}
	    }

	    private void unlinkElement(IVisualConnection connection){
	    	if(connection instanceof VisualConnection){
	    		specificationElement = ((VisualConnection) connection).getSpecificationElement();
	    		((VisualConnection)connection).setSpecificationElement(null);
	    		if (connection.getName() != null)
	    			connection.setName("");
	    	}
	    }

	    @Override public void undo() {
	    	linkElement(conncetion);
	    	if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
				Set<IVisualConnection> allTraces = TraceLinkUtil.findAllTrace(conncetion);
				for(IVisualConnection e : allTraces){
					linkElement(e);
				}
			}
	    }

	    private void linkElement(IVisualConnection connection){
	    	if(connection instanceof VisualConnection){
	    		((VisualConnection) connection).setSpecificationElement(specificationElement);
	    		if(specificationElement!=null && specificationElement.getName()!=null)
	    			connection.setName(specificationElement.getName());
	    	}
	    }
}
