package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase;

import org.eclipse.gef.EditPart;

import dk.dtu.imm.red.visualmodeling.ui.factories.EditPartFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualActorElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualSystemBoundaryElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualUseCaseElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualActorElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualSystemBoundaryElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement;


public class UseCaseEditPartFactory extends EditPartFactoryBase {

	public UseCaseEditPartFactory(RuleCollection ruleCollection) {
		super(ruleCollection);		
	}
	
	public EditPart createEditPartInternal(EditPart context, Object model) {

		EditPart part = null;

		if (model instanceof VisualUseCaseElement) {
			part = new VisualUseCaseElementEditPart(ruleCollection);
		} else if (model instanceof VisualActorElement) {
			part = new VisualActorElementEditPart(ruleCollection);
		} else if (model instanceof VisualSystemBoundaryElement) {
			part = new VisualSystemBoundaryElementEditPart(ruleCollection);
		}

		return part;
	}
}