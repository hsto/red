package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.ui.IEditorPart;

public class AutoLayoutSpringLayoutAction extends AutoLayoutAction {

    public AutoLayoutSpringLayoutAction(IEditorPart editor) {
		super(editor, LayoutAlgorithmType.SPRING_LAYOUT);
	}

}
