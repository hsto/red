package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine;

import org.eclipse.gef.EditPart;

import dk.dtu.imm.red.visualmodeling.ui.factories.EditPartFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualDecisionMergeStateEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualDeepHistoryStateEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualFinalStateEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualForkJoinStateEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualInitialStateEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualReceiveStateEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualSendStateEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualShallowHistoryStateEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualStateEditPart;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDecisionMergeState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDeepHistoryState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualFinalState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualForkJoinState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualInitialState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualReceiveState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualSendState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualShallowHistoryState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState;

public class StateMachineEditPartFactory extends EditPartFactoryBase {

	RuleCollection ruleCollection;

	public StateMachineEditPartFactory(RuleCollection ruleCollection) {
		super(ruleCollection);
		this.ruleCollection = ruleCollection;
	}

	public EditPart createEditPartInternal(EditPart context, Object model) {
		EditPart part = null;

		if (model instanceof VisualDecisionMergeState) {
			part = new VisualDecisionMergeStateEditPart(ruleCollection);
		}
		else if (model instanceof VisualDeepHistoryState) {
			part = new VisualDeepHistoryStateEditPart(ruleCollection);
		}
		else if (model instanceof VisualFinalState) {
			part = new VisualFinalStateEditPart(ruleCollection);
		}
		else if (model instanceof VisualForkJoinState) {
			part = new VisualForkJoinStateEditPart(ruleCollection);
		}
		else if (model instanceof VisualInitialState) {
			part = new VisualInitialStateEditPart(ruleCollection);
		}
		else if (model instanceof VisualReceiveState) {
			part = new VisualReceiveStateEditPart(ruleCollection);
		}
		else if (model instanceof VisualSendState) {
			part = new VisualSendStateEditPart(ruleCollection);
		}
		else if (model instanceof VisualShallowHistoryState) {
			part = new VisualShallowHistoryStateEditPart(ruleCollection);
		}
		else if (model instanceof VisualState) {
			part = new VisualStateEditPart(ruleCollection);
		}


		return part;
	}
}
