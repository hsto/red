package dk.dtu.imm.red.visualmodeling.ui.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gef.EditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.ui.internal.navigation.ElementExplorer;
import dk.dtu.imm.red.core.ui.internal.navigation.FileExplorer;
import dk.dtu.imm.red.core.ui.internal.navigation.implementation.ElementExplorerImpl;
import dk.dtu.imm.red.core.ui.internal.navigation.implementation.FileExplorerImpl;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.ui.editors.impl.VisualModelEditorImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class NavigateToElementTree extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		if (HandlerUtil.getActiveEditor(event) instanceof VisualModelEditorImpl) {
			VisualModelEditorImpl editor = (VisualModelEditorImpl) HandlerUtil.getActiveEditor(event);
			ISelection visualSelection = editor.getSite().getSelectionProvider().getSelection();
			List<?> selectionList = ((StructuredSelection)visualSelection).toList();			
			execute(selectionList);			
		}
		
		return null;
	}
	
	public void execute(List<?> selectionList) {
		List<SpecificationElement> elements = new ArrayList<SpecificationElement>();
		
		for (Object obj : selectionList) {
			if (obj instanceof EditPart) {
				EditPart editPart = (EditPart) obj;
				if (editPart.getModel() != null && editPart.getModel() instanceof VisualSpecificationElement) {
					VisualSpecificationElement visualSE = (VisualSpecificationElement) editPart.getModel();
					if (visualSE.getSpecificationElement() != null) {
						elements.add(visualSE.getSpecificationElement());
					}
				} else if (editPart.getModel() != null && editPart.getModel() instanceof VisualConnection) {
					VisualConnection visualConnection = (VisualConnection) editPart.getModel();
					if (visualConnection.getSpecificationElement() != null) {
						elements.add(visualConnection.getSpecificationElement());
					}
				}
			}
		}
		if (elements.isEmpty()) return;
		
		StructuredSelection selection = new StructuredSelection(elements);
		
		
		// Sets selection in tree view
		boolean isElementViewVisible = false;
		boolean isFileViewVisible = false;
		IViewPart elementView = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(ElementExplorer.ID);
		IViewPart fileView = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(FileExplorer.ID);
		if (elementView != null) {
			isElementViewVisible = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().isPartVisible(elementView);				
		}
		if (fileView != null) {
			isFileViewVisible = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().isPartVisible(fileView);				
		}
		
		if (isElementViewVisible && elementView instanceof ElementExplorerImpl) {
			ElementExplorerImpl elementExplorer = (ElementExplorerImpl) elementView;				
			elementExplorer.getTreeView().setSelection(selection);
		}
		if (isFileViewVisible && fileView instanceof FileExplorerImpl) {
			FileExplorerImpl fileExplorer = (FileExplorerImpl) fileView;
			fileExplorer.getTreeView().setSelection(selection);
		}
	}

}
