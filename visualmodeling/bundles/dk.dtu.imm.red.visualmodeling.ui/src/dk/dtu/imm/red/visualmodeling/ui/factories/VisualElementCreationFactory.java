package dk.dtu.imm.red.visualmodeling.ui.factories;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalElementFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualPortFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemStructureActorFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualClassCreationFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualActorElementFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualUseCaseElementFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind;

public class VisualElementCreationFactory implements CreationFactory{

	private CreationFactory selectedFactory;
	private SpecificationElement specificationElement;
	private DiagramType diagramType;

	public void SetCreationFactory(SpecificationElementEnum specificationElementType)
	{
		switch(specificationElementType)
		{
			case Goal:
				selectedFactory = new VisualGoalElementFactory();
				break;
			case Actor:
			case System:
			case Port:
				if (diagramType != null && diagramType == DiagramType.SYSTEM_STRUCTURE) {
					switch (specificationElementType) 
					{
						case Actor:
							selectedFactory = new VisualSystemStructureActorFactory(SystemStructureActorKind.getByName(specificationElement.getElementKind()));
							break;
						case System:
							selectedFactory = new VisualSystemFactory();
							break;
						case Port:
							selectedFactory = new VisualPortFactory();
							break;
						default: // TODO throw not implemented exception
					}
				}
				else 
				{
					selectedFactory = new VisualActorElementFactory();
				}
				break;
			case UseCase:
				selectedFactory = new VisualUseCaseElementFactory();
				break;
			case Signature:
				selectedFactory = new VisualClassCreationFactory();
			default: // TODO throw not implemented exception
				break;
		}
	}
	
	public void SetCreationFactory(SpecificationElement specificationElement)
	{
		
		if(specificationElement instanceof Goal)
		{
			SetCreationFactory(SpecificationElementEnum.Goal);
		}
		else if(specificationElement instanceof UseCase)
		{
			SetCreationFactory(SpecificationElementEnum.UseCase);
		}
		else if(specificationElement instanceof System)
		{
			SetCreationFactory(SpecificationElementEnum.System);
		}
		else if(specificationElement instanceof Port)
		{
			SetCreationFactory(SpecificationElementEnum.Port);
		}
		else if(specificationElement instanceof Actor)
		{
			SetCreationFactory(SpecificationElementEnum.Actor);
		}
		else if(specificationElement instanceof Signature) 
		{
			SetCreationFactory(SpecificationElementEnum.Signature);
		}
		else
		{
			// TODO throw not implemented exception			
		}
	}
	
	public void setSpecificationElement(SpecificationElement specificationElement)
	{
		this.specificationElement = specificationElement;
	}
	
	public void setDiagramType (DiagramType diagramType)
	{
		this.diagramType = diagramType;
	}
	
	@Override
	public Object getNewObject() {
		VisualSpecificationElement visualElement = (VisualSpecificationElement)selectedFactory.getNewObject();
		visualElement.setSpecificationElement(specificationElement);
		specificationElement = null;
		return visualElement;
	}

	@Override
	public Object getObjectType() {
		return selectedFactory.getObjectType();
	}

}
