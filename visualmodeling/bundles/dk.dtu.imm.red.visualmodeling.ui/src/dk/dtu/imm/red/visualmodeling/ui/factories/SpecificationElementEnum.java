package dk.dtu.imm.red.visualmodeling.ui.factories;

public enum SpecificationElementEnum {
	Goal,
	UseCase,
	Actor,
	System,
	Port,
	Connection,
	Signature
}
