package dk.dtu.imm.red.visualmodeling.zestlayout;

import org.eclipse.zest.layouts.LayoutBendPoint;
import org.eclipse.zest.layouts.LayoutEntity;
import org.eclipse.zest.layouts.LayoutRelationship;
import org.eclipse.zest.layouts.constraints.BasicEdgeConstraints;
import org.eclipse.zest.layouts.constraints.LabelLayoutConstraint;
import org.eclipse.zest.layouts.constraints.LayoutConstraint;
import org.eclipse.zest.layouts.dataStructures.BendPoint;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;

public class VisualLayoutRelationship implements LayoutRelationship, Cloneable{


	private IVisualConnection realObject;
	protected LayoutEntity sourceEntity;
	protected LayoutEntity destinationEntity;
	protected boolean bidirectional;
	private Object internalRelationship;
	private LayoutBendPoint[] bendPoints;
	private String label;

//	private Map attributes;
//	private static int DEFAULT_REL_LINE_WIDTH = 1;
//	private int lineWidth = DEFAULT_REL_LINE_WIDTH;
//	private static Object DEFAULT_RELATIONSHIP_COLOR;
//	private Object color = DEFAULT_RELATIONSHIP_COLOR;


	public VisualLayoutRelationship(IVisualConnection visualConnection, LayoutEntity sourceEntity, LayoutEntity destinationEntity, boolean bidirectional) {
		this.realObject = visualConnection;
		this.destinationEntity = destinationEntity;
		this.sourceEntity = sourceEntity;
		this.bidirectional = bidirectional;
//		this.attributes = new HashMap();
//		this.lineWidth = DEFAULT_REL_LINE_WIDTH;
//		this.color = DEFAULT_RELATIONSHIP_COLOR;
	}

	@Override
	public void setGraphData(Object o) {
	}

	@Override
	public Object getGraphData() {
		return null;
	}

	@Override
	public LayoutEntity getSourceInLayout() {
		return sourceEntity;
	}

	@Override
	public LayoutEntity getDestinationInLayout() {
		return destinationEntity;
	}

	@Override
	public void setLayoutInformation(Object layoutInformation) {
		this.internalRelationship = layoutInformation;

	}

	@Override
	public Object getLayoutInformation() {
		return internalRelationship;
	}

	@Override
	public void setBendPoints(LayoutBendPoint[] bendPoints) {
		this.bendPoints = bendPoints;
	}

	public LayoutBendPoint[] getBendPoints(){
		return this.bendPoints;
	}

	@Override
	public void clearBendPoints() {
		this.bendPoints = new BendPoint[0];
	}

	@Override
	public void populateLayoutConstraint(LayoutConstraint constraint) {
		if (constraint instanceof LabelLayoutConstraint) {
			LabelLayoutConstraint labelConstraint = (LabelLayoutConstraint) constraint;
			labelConstraint.label = this.label;
			labelConstraint.pointSize = 18;
		} else if (constraint instanceof BasicEdgeConstraints) {
			// noop

		}
	}

	public boolean isBidirectionalInLayout() {
		return bidirectional;
	}

	public IVisualConnection getRealObject() {
		return realObject;
	}

	public VisualLayoutRelationship clone(){
		VisualLayoutRelationship cl = new VisualLayoutRelationship(realObject,sourceEntity,destinationEntity,bidirectional);
		if(this.bendPoints!=null){
			cl.bendPoints = new LayoutBendPoint[this.bendPoints.length];
			for(int i=0;i<bendPoints.length;i++){
				cl.bendPoints[i] = new BendPoint(this.bendPoints[i].getX(), this.bendPoints[i].getY(), this.bendPoints[i].getIsControlPoint());
			}
		}
		return cl;
	}
}
