package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.List;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualPortLocationCalculator;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;

public class VisualContainerAddCommand extends Command{

	private List<IVisualElement> elements;
	private IVisualElement newParent;
	private Point[] newLocations;

	public VisualContainerAddCommand(List<IVisualElement> elements, IVisualElement newParent, Point[] newLocations)
	{
		this.elements = elements;
		this.newParent = newParent;
		this.newLocations = newLocations;
	}

	@Override
	public boolean canExecute() {
		return true;
	};

	@Override
	public void execute() {
		System.out.println("VisualContainerAddCommand");
		for(int i = 0; i < elements.size(); i++)
		{
			elements.get(i).setLocation(newLocations[i]);
			elements.get(i).setParent(newParent);

			if (elements.get(i) instanceof VisualPort && newParent instanceof VisualSystem)
				VisualPortLocationCalculator.recalculatePortStickingLocation((VisualPort) elements.get(i));
		}

		newParent.getElements().addAll(elements);
	}

	//UNDONE undo
}
