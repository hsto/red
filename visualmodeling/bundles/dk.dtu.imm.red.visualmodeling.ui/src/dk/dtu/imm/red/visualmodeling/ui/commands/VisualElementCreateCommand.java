package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualPortLocationCalculator;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;

public class VisualElementCreateCommand extends Command {

	private VisualDiagram diagram;
	private IVisualElement parent;
	private IVisualElement element;
	private Rectangle constraints;

	public VisualElementCreateCommand(VisualDiagram diagram,
			IVisualElement parent, IVisualElement element, Point location) {
		this.constraints = new Rectangle(location, element.getBounds());
		this.parent = parent;
		this.element = element;
		// default sketchy
		this.element.setIsSketchy(true);
		this.diagram = diagram;
	}

	@Override
	public void execute() {
		System.out.println("VisualElementCreateCommand");
		if (constraints != null) {
			element.setLocation(constraints.getLocation());
			element.setBounds(constraints.getSize());
		}

		element.setDiagram(diagram);
		element.setParent(parent);
		parent.getElements().add(element);

		// only for port visual element, need to overwrite the location
		//can't help but put it here otherwise have to create another command and
		//another edit policy :(
		if (element instanceof VisualPort && parent instanceof VisualSystem)
			VisualPortLocationCalculator.recalculatePortStickingLocation((VisualPort) element);
	}

	@Override
	public void undo() {
		parent.getElements().remove(element);
		element.setParent(parent);
		element.setDiagram(null);
	}

	public IVisualElement getElement() {
		return element;
	}
}
