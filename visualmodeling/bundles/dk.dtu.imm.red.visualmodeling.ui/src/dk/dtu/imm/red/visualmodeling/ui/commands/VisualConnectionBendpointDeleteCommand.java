package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;

public class VisualConnectionBendpointDeleteCommand extends Command{
	private int index;    
    private Point location;    
    private VisualConnection connection;
 
    public VisualConnectionBendpointDeleteCommand(int index, VisualConnection connection)
    {
    	this.index = index;
		this.connection = connection;
    }
    
    @Override public void execute() {
    	location = connection.getBendpoints().get(index);
    	connection.getBendpoints().remove(index);
    }
 
    @Override public void undo() {
    	connection.getBendpoints().add(index, location);
    }
}
