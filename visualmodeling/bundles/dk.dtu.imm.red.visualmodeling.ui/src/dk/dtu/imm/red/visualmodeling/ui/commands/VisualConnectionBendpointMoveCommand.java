package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;

public class VisualConnectionBendpointMoveCommand extends Command{
	
	  private int index;	 
	  private Point oldLocation;	
	  private Point newLocation;	  	  
	  private VisualConnection connection;
	   
	  public VisualConnectionBendpointMoveCommand(int index, Point newLocation, VisualConnection connection)
	  {
		  this.index = index;
		  this.newLocation = newLocation;
		  this.connection = connection;
	  }
	  
	  public void execute() {
	    if(oldLocation == null) {
	      oldLocation = connection.getBendpoints().get(index);	      
	    }
	    connection.getBendpoints().set(index, newLocation);
	  }
	   
	  
	  @Override public void undo() {
		  connection.getBendpoints().set(index, oldLocation);
	  }
}
