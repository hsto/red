package dk.dtu.imm.red.visualmodeling.ui.editpolicies;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ContainerEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.requests.GroupRequest;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualContainerOrphanCommand;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualContainerContainerEditPolicy extends ContainerEditPolicy{

	//private RuleCollection ruleCollection;
	//private IFigure activeArea;
	
	public VisualContainerContainerEditPolicy(RuleCollection ruleCollection)
	{		
		//this.ruleCollection = ruleCollection;
		//this.activeArea = null;
	}
	
	public VisualContainerContainerEditPolicy(RuleCollection ruleCollection, IFigure activeArea)
	{		
		//this.ruleCollection = ruleCollection;
		//this.activeArea = activeArea;
	}
	
	@Override
	protected Command getCreateCommand(CreateRequest request) {	
		/*
		System.out.println("VisualContainerContainerEditPolicy.getCreateCommand()");
		
		Command retVal = null;
		if (IVisualElement.class.isAssignableFrom((Class) request
				.getNewObjectType())) {
			IVisualElement parent = (IVisualElement) getHost().getModel();
			VisualDiagram diagram = parent.getDiagram();
			IVisualElement child = (IVisualElement) request.getNewObject();
			// TODO clean all this up
			Point newParentAbsLocation = CoordinateTranslation
					.getAbsoluteCoordinates(parent);
			Point createLocation = request.getLocation().getTranslated(
					newParentAbsLocation.getNegated());

			VisualElementCreateCommand command = new VisualElementCreateCommand(diagram, parent, child, createLocation);
			retVal = command;
		}

		return retVal;
		*/
		return null;
	}
	
	/**
	 * Called when dragging an element into another element.
	 */
	@Override
	protected Command getAddCommand(GroupRequest request) {
		/*
		System.out.println("VisualContainerContainerEditPolicy.getAddCommand()");
		
		List<IVisualElement> elements = new ArrayList<IVisualElement>();
		
		for(Object obj : request.getEditParts()){
			EditPart part = (EditPart)obj;
			elements.add((IVisualElement)part.getModel());
		}
		
		IVisualElement newParent = (IVisualElement)getHost().getModel();
		
		for(IVisualElement child :elements)
		{
			if(!ruleCollection.canAccept(newParent, child))
			{
				return null;
			}
		}		
		
		if(request instanceof ChangeBoundsRequest)
		{		
			ChangeBoundsRequest cbRequest = (ChangeBoundsRequest)request;
			Point mouseDropPoint = cbRequest.getLocation();
		
			// If outside the active area we do now allow the drop
			if(activeArea != null && !activeArea.containsPoint(mouseDropPoint))
			{
				return null;
			}
			
			Point newParentAbsLocation = CoordinateTranslation.getAbsoluteCoordinates(newParent);			
			Point offset = cbRequest.getMoveDelta().getTranslated(newParentAbsLocation.getNegated());
			
			
			Point[] newLocations = new Point[elements.size()];
			for(int i = 0; i < elements.size(); ++i)
			{
				newLocations[i] = CoordinateTranslation.getAbsoluteCoordinates(elements.get(i)).getTranslated(offset);	
			}
			
			VisualContainerAddCommand command = new VisualContainerAddCommand(elements, newParent, newLocations);
			return command;
		}
		*/		
		return null;
	}
	
	/**
	 * Called to remove children being dragged into a new parent.
	 */
	@Override
	protected Command getOrphanChildrenCommand(GroupRequest request) {
		System.out.println("VisualContainerContainerEditPolicy.getOrphanChildrenCommand()");
		
		List<IVisualElement> elements = new ArrayList<IVisualElement>();
		
		for(Object obj : request.getEditParts()){
			EditPart part = (EditPart)obj;
			elements.add((IVisualElement)part.getModel());
		}
		
		VisualContainerOrphanCommand command = new VisualContainerOrphanCommand(elements, (IVisualElement)getHost().getModel());
				
		return command;
	}
}
