package dk.dtu.imm.red.visualmodeling.ui.editors;

import java.io.FileOutputStream;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.SWTGraphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.LayerManager;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.extensions.AbstractSpecificationElementExtension;
import dk.dtu.imm.red.visualmodeling.ui.ExportDiagramDialog;
import dk.dtu.imm.red.visualmodeling.ui.VisualEditor;
import dk.dtu.imm.red.visualmodeling.ui.editors.impl.VisualModelEditorImpl;
import dk.dtu.imm.red.visualmodeling.ui.editors.impl.VisualModelEditorInputImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;


public class DiagramExtension extends AbstractSpecificationElementExtension {

	private IEditorPart openedEditor;
	/**
	 *Opens the editor for the Diagram specificationelement.
	 */
	@Override
	public void openConcreteElement(Element element){
		if (element instanceof Diagram) {
			VisualModelEditorInputImpl editorInput =
					new VisualModelEditorInputImpl((Diagram) element);
			try {
				openedEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, IVisualModelEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
	}
	
	public String saveDiagram(Element element,String path)
	{
		if (element instanceof Diagram) {
			VisualModelEditorInputImpl editorInput =
					new VisualModelEditorInputImpl((Diagram) element);
			try {
				openedEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.openEditor(editorInput, IVisualModelEditor.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}
		}
		int format = SWT.IMAGE_PNG;
		System.out.println(PlatformUI.getWorkbench().getWorkbenchWindowCount());
		//((IWorkbenchPart)openedEditor);
		VisualEditor e =((VisualModelEditorImpl)openedEditor).getVisualEditor();
		if(openedEditor instanceof IWorkbenchPart)
			System.out.println("yes2");
		if(openedEditor instanceof VisualModelEditorImpl)
			System.out.println("yes3");
		if(openedEditor instanceof VisualEditor)
			System.out.println("yes4");
		if(PlatformUI.getWorkbench().getWorkbenchWindows()[0] instanceof IWorkbenchPart)
			System.out.println("yes");
		if(PlatformUI.getWorkbench().getWorkbenchWindows()[0] instanceof VisualModelEditorImpl)
			System.out.println("yes");
		if(PlatformUI.getWorkbench().getWorkbenchWindows()[0] instanceof VisualEditor)
			System.out.println("yes");
		
		GraphicalViewer graphicalViewer = e.getGraphicalViewer();
		ScalableFreeformRootEditPart rootEditPart = (ScalableFreeformRootEditPart)graphicalViewer.getEditPartRegistry().get(LayerManager.ID);
		IFigure rootFigure = ((LayerManager)rootEditPart).getLayer(LayerConstants.PRINTABLE_LAYERS);
		Rectangle rootFigureBounds = rootFigure.getBounds();		
		
		Display display = Display.getDefault();
	    Shell shell = display.getActiveShell();
		
	    ExportDiagramDialog dialog = new ExportDiagramDialog(shell, SWT.APPLICATION_MODAL);
		if(dialog.open() != null)
		{
			String enteredPath = dialog.exportFilePath;
			if(!enteredPath.isEmpty())
			{
				path=enteredPath;
			}
		}
		
		Control figureCanvas = graphicalViewer.getControl();				
		GC figureCanvasGC = new GC(figureCanvas);
		int width,height;
		try
		{
			System.out.println(rootFigureBounds.width);
			System.out.println(rootFigureBounds.height);
			if(rootFigureBounds.width==0 ||rootFigureBounds.height==0)
			{
				width=2000;
				height=2000;
			}
			else
			{
				width=rootFigureBounds.width;
				height=rootFigureBounds.height;
			}
		}catch(Exception exc){
			System.out.println("width and height diagramextension method savediagram");
			width=1000;
			height=1000;
		}
		System.out.println(width);
		System.out.println(height);
		Image img = new Image(null,width, height);
		GC imageGC = new GC(img);
		imageGC.setBackground(figureCanvasGC.getBackground());
		imageGC.setForeground(figureCanvasGC.getForeground());
		imageGC.setFont(figureCanvasGC.getFont());
		imageGC.setLineStyle(figureCanvasGC.getLineStyle());
		imageGC.setLineWidth(figureCanvasGC.getLineWidth());
		
		Graphics imgGraphics = new SWTGraphics(imageGC);					
		rootFigure.paint(imgGraphics);
						
		ImageData[] imgData = new ImageData[1];
		imgData[0] = img.getImageData();
		try {
			FileOutputStream fos = new FileOutputStream(path);
			fos.write(0);
			fos.close();
		} catch (Exception ex) {
			ex.printStackTrace(); LogUtil.logError(ex);
		}
		ImageLoader imgLoader = new ImageLoader();
		imgLoader.data = imgData;
		imgLoader.save(path, format);
		
		figureCanvasGC.dispose();
		imageGC.dispose();
		img.dispose();
		System.out.println(path);
		return path;
	}

	public IEditorPart getOpenedEditor(){
		return openedEditor;
	}
}
