package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecasemap;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualEditorConfigurationBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericPaletteFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.UseCaseContextMenuProvider;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.UseCaseEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.UseCaseRule;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;

public class UseCaseMapConfiguration extends VisualEditorConfigurationBase{

	public UseCaseMapConfiguration()
	{
		this.ruleCollection = new RuleCollection();
		this.ruleCollection.addRule(new UseCaseRule());
		
		this.paletteFactory = new UseCaseMapPaletteFactory();	
		this.paletteFactory.setNext(new GenericPaletteFactory());
		
		this.editPartFactory = new UseCaseEditPartFactory(ruleCollection);
		this.editPartFactory.setNext(new GenericEditPartFactory(ruleCollection));
		
		this.contextMenuProvider = new UseCaseContextMenuProvider();
	}
}
