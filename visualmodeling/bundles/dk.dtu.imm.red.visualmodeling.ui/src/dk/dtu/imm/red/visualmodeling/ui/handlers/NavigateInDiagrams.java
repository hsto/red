package dk.dtu.imm.red.visualmodeling.ui.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.RootEditPart;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.workspace.impl.WorkspaceFactoryImpl;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.visualmodeling.ui.editors.DiagramExtension;
import dk.dtu.imm.red.visualmodeling.ui.util.VisualModelHelper;
import dk.dtu.imm.red.visualmodeling.ui.widget.NavigateToMessageDialog;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;



/**
 * 
 * @author Luai
 *
 */
public class NavigateInDiagrams extends AbstractHandler implements IHandler {

	private List<Diagram> resultDiagrams;
	private List<IVisualElement> visualElements;
	private List<IVisualConnection> visualConnections;
	
	public NavigateInDiagrams() {
		super();
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		Element selectedElement = null;
		ISelection selectedObject = HandlerUtil.getCurrentSelection(event);
		if (selectedObject instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) selectedObject;
			selectedElement = (Element) selection.getFirstElement();
		}
		if (selectedElement == null) return null;
		
		String errorMessage = "";
		if (selectedElement instanceof Diagram) {
			errorMessage = "Element connot be a diagram!";
		} else if (!(selectedElement instanceof SpecificationElement)) {
			errorMessage = "Selected Element cannot be a " + selectedElement.getClass().getSimpleName() + ".";
		}
		if (!errorMessage.isEmpty()) {
			MessageDialog.open(MessageDialog.INFORMATION, Display.getCurrent().getActiveShell(), "Invalid input!", errorMessage, SWT.SHELL_TRIM);
			return null;
		}
		
		findLinkedVisualElements((SpecificationElement) selectedElement);
		
		if (resultDiagrams.isEmpty()) {
			MessageDialog.open(MessageDialog.INFORMATION, Display.getCurrent().getActiveShell(), "Not Found!", "No occurrences found!", SWT.SHELL_TRIM);
		} else {
			NavigateToMessageDialog md = new NavigateToMessageDialog(resultDiagrams, Display
					.getCurrent().getActiveShell(), "Navigate to diagram occurrences", null,
					"Occurrences found in " + resultDiagrams.size() + " diagram(s). Choose diagram to navigate to!", MessageDialog.QUESTION,
					new String[] { "Go to", "Cancel"}, 0);
			
			if (md.open() == 0) { // Go to
				selectDiagramElement((Diagram)md.getSelectedDiagram());
			}			
		}
		
		return null;
	}
	
	private void findLinkedVisualElements(SpecificationElement element) {
		resultDiagrams = new ArrayList<Diagram>();
		visualElements = new ArrayList<IVisualElement>();
		visualConnections = new ArrayList<IVisualConnection>();
				
		List<Diagram> diagrams = VisualModelHelper.getAllDiagrams(WorkspaceFactoryImpl.eINSTANCE.getWorkspaceInstance());
		for (Diagram diagram : diagrams) {
			boolean exist = false;
			
			if (element instanceof Connector) {
				EList<IVisualConnection> diagramConnections = diagram.getVisualDiagram().getDiagramConnections();
				for (IVisualConnection con : diagramConnections) {
					if (con instanceof VisualConnection) {
						SpecificationElement se = ((VisualConnection) con).getSpecificationElement();
						if (se != null && se.equals(element)) {
							visualConnections.add(con);
							exist = true;
						}
					}
				}
				
			} 
			
			else { // All elements
				List<IVisualElement> ve = VisualModelHelper.getAllDecendents(diagram.getVisualDiagram());
				for (IVisualElement visualElement : ve) {
					if (visualElement instanceof VisualSpecificationElement) {
						SpecificationElement se = ((VisualSpecificationElement) visualElement).getSpecificationElement();
						if (se != null && se.equals(element)) {
							visualElements.add(visualElement);
							exist = true;
						}						
					}
				}
				
			}
			
			if (exist && !resultDiagrams.contains(diagram)) {
				resultDiagrams.add(diagram);
				exist = false;
			}
		}
	}
	
	private void selectDiagramElement(Diagram diagram) {		
		DiagramExtension diagramExtension = new DiagramExtension();
		diagramExtension.openConcreteElement(diagram);
		
		
		
		List<IVisualElement> visualElementsInDiagram = new ArrayList<IVisualElement>();
		List<IVisualConnection> visualConnectionsInDiagram = new ArrayList<IVisualConnection>();
		for (IVisualElement element : visualElements) {
			if (element.getDiagram().equals(diagram.getVisualDiagram())) visualElementsInDiagram.add(element);
		}
		EList<IVisualConnection> diagramConnections = diagram.getVisualDiagram().getDiagramConnections();
		for (IVisualConnection connection : visualConnections) {
			if (diagramConnections.contains(connection)) visualConnectionsInDiagram.add(connection);
		}
		
		List<Object> selectedList = new ArrayList<Object>();
		RootEditPart editPartRoot = (RootEditPart) diagramExtension.getOpenedEditor().getAdapter(EditPart.class);
		List<EditPart> editParts = getAllEditParts(editPartRoot.getContents());
		for (EditPart editPart : editParts) {
			if (visualElementsInDiagram.contains(editPart.getModel()) || visualConnectionsInDiagram.contains(editPart.getModel())) {
				selectedList.add(editPart);
			}
		}
		
		StructuredSelection selection = new StructuredSelection(selectedList);
		
		
		diagramExtension.getOpenedEditor().getSite().getSelectionProvider().setSelection(selection);
	}
	
	@SuppressWarnings("unchecked")
	private List<EditPart> getAllEditParts(EditPart editPart) { // TODO EditPart connections is not added!
		List<EditPart> list = new ArrayList<EditPart>();
		List<EditPart> children = (List<EditPart>) editPart.getChildren();
		list.addAll(children);
		
		for (EditPart child : children) {
			list.addAll(getAllEditParts(child));
		}
		
		return list;
	}

}
