package dk.dtu.imm.red.visualmodeling.ui.editpolicies;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.editpolicies.GraphicalEditPolicy;
import org.eclipse.gef.requests.LocationRequest;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;


public class VisualElementSelectionFeedbackEditPolicy extends GraphicalEditPolicy{

	private IFigure fig;
	
	@Override 
	public boolean understandsRequest(Request request) {
		
		if(request.getType() == RequestConstants.REQ_SELECTION_HOVER)
		{
			return true;
		}
		
		return false;
	};
	
	@Override
	@SuppressWarnings("unused")
	public void showTargetFeedback(Request request) 
	{
		if(request.getType() == RequestConstants.REQ_SELECTION_HOVER)
		{
			if(request instanceof LocationRequest)
			{
				System.out.println("SHOW");
				LocationRequest selectionRequest = (LocationRequest)request;
				
				Ellipse e = new Ellipse();
				e.setBounds(new Rectangle(0,0,50,50));
				e.setBackgroundColor(ColorConstants.green);
				e.setOutline(true);
				fig = e;
				IFigure layer = getFeedbackLayer();
				layer.add(e);
				layer.setConstraint(e, new Rectangle(50,50,50,50));
				
				// Creates a context menu and displays it
				Control control = this.getHost().getViewer().getControl();								
				Menu menu = new Menu(control);
				MenuItem refreshItem = new MenuItem(menu, SWT.NONE);
			    refreshItem.setText("Refresh");
			    
			    menu.setLocation(control.toDisplay(20, 20));
			    menu.setVisible(true);			    
			}
		}
		
	}
	
	@Override
	public void eraseTargetFeedback(Request request)
	{	
		if(request.getType() == RequestConstants.REQ_SELECTION_HOVER && fig != null)
		{
			System.out.println("ERASE");
			removeFeedback(fig);
			fig = null;
		}
	}
}
