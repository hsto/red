package dk.dtu.imm.red.visualmodeling.ui.specificationelement.stitch;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualEditorConfigurationBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericPaletteFactory;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;


public class StitchConfiguration extends VisualEditorConfigurationBase{

	public StitchConfiguration()
	{
		this.ruleCollection = new RuleCollection();
		this.ruleCollection.addRule(new StitchRule());

		this.paletteFactory = new StitchPaletteFactory();
		this.paletteFactory.setNext(new GenericPaletteFactory());

		this.editPartFactory = new StitchEditPartFactory(ruleCollection);
		this.editPartFactory.setNext(new GenericEditPartFactory(ruleCollection));
	}
}
