package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualForkJoinStateFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(100,5);

	//private RectangleFigure formalFigure;

	//private SVGFigure sketchyFigure;
	private String sketchyFigureURL = svgPath + "Statemachine/ForkJoinState.svg";

	public VisualForkJoinStateFigure(IVisualElement model)
	{
		super(model);

		formalFigure = new RectangleFigure();
		formalFigure.setFill(true);
		formalFigure.setBackgroundColor(ColorHelper.stateMachineDiagramDefaultColor);

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);

	    add(this.nameLabel);
	    refreshVisuals();
	}

	@Override
	  protected void paintFigure(Graphics graphics) {
		Rectangle r = getBounds().getCopy();

		boundingRectangle.setVisible(false);

		if (this.getChildren().contains(sketchyFigure))
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(formalFigure)){
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
		}
		setConstraint(nameLabel, new Rectangle(0, 0, r.width, 30));
	  }

	@Override
	public void refreshVisuals()
	{
		if (this.getChildren().contains(formalFigure))
			remove(formalFigure);
		if (this.getChildren().contains(sketchyFigure))
			remove(sketchyFigure);

		// toggling between sketchy and non sketchy
		if (model.isIsSketchy()) {
			add(sketchyFigure, 0);
			nameLabel.setFont(SKETCHY_FONT);
		} else {
			add(formalFigure, 0);
			nameLabel.setFont(DEFAULT_FONT);
		}
		nameLabel.setText(this.model.getName());
	}
}
