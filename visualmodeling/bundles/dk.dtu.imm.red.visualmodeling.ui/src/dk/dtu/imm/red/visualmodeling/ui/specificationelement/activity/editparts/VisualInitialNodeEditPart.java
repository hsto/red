package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.draw2d.IFigure;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualInitialNodeFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualInitialNodeEditPart extends VisualElementEditPart{

	public VisualInitialNodeEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	@Override
	protected IFigure createFigure() {
		return new VisualInitialNodeFigure((IVisualElement)getModel());
	}

}
