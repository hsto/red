package dk.dtu.imm.red.visualmodeling.ui.editors;


import org.eclipse.ui.IEditorPart;


import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface IVisualModelEditor extends IEditorPart, IBaseEditor {

	public static final String ID = "dk.dtu.imm.red.visualmodeling.ui.editors.editor";

}