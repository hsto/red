package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecasemap;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.visualmodel.GenericType;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecaseFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement;

public class VisualUseCaseMapElementFactory implements CreationFactory {
	public Object getNewObject() {
			
			VisualUseCaseElement element = UsecaseFactory.eINSTANCE.createVisualUseCaseElement();
			VisualGenericElement generic = visualmodelFactory.eINSTANCE.createVisualGenericElement();
			generic.setGenericType(GenericType.CIRCLE);
			generic.setLocation(new Point(10,10));
			generic.setBounds(new Dimension(20, 20));
			element.getElements().add(generic);
		    return element;
		  }
		 
		  public Object getObjectType() {
		    return VisualUseCaseElement.class;	   
		  }	
}


