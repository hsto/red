package dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor;

public class VisualSystemStructureActorFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(60,70);
	//private RoundedRectangle formalFigure;

	private SVGFigure personFigure;
	private String personFigureURL = svgPath + "Context/Person.svg";
	private SVGFigure roleFigure;
	private String roleFigureURL = svgPath + "Context/Role.svg";
	private SVGFigure teamFigure;
	private String teamFigureURL = svgPath + "Context/Team.svg";
	private SVGFigure groupFigure;
	private String groupFigureURL = svgPath + "Context/Group.svg";
	private SVGFigure organisationFigure;
	private String organisationFigureURL = svgPath + "Context/Organisation.svg";

	public VisualSystemStructureActorFigure(VisualSpecificationElement model)
	{
		super(model);

		formalFigure = new RoundedRectangle();
		((RoundedRectangle)formalFigure).setCornerDimensions(new Dimension(20,20));
	    formalFigure.setFill(true);
	    formalFigure.setBackgroundColor(ColorHelper.activityDiagramDefaultColor);

	    personFigure = new SVGFigure();
	    personFigure.setURI(personFigureURL);
	    roleFigure = new SVGFigure();
	    roleFigure.setURI(roleFigureURL);
	    teamFigure = new SVGFigure();
	    teamFigure.setURI(teamFigureURL);
	    groupFigure = new SVGFigure();
	    groupFigure.setURI(groupFigureURL);
	    organisationFigure = new SVGFigure();
	    organisationFigure.setURI(organisationFigureURL);

	    add(this.nameLabel);
	    refreshVisuals();
	}

	@Override
	  protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		
		Rectangle r = getBounds().getCopy();

		boundingRectangle.setVisible(false);

		if (this.getChildren().contains(personFigure))
			setConstraint(personFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(roleFigure))
			setConstraint(roleFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(teamFigure))
			setConstraint(teamFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(groupFigure))
			setConstraint(groupFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(organisationFigure))
			setConstraint(organisationFigure, new Rectangle(0, 0, r.width, r.height));
		setConstraint(nameLabel, new Rectangle(0, r.height - 20, r.width, 20));
	  }


	@Override
	public void refreshVisuals()
	{
		super.refreshVisuals();
		
		if (this.getChildren().contains(personFigure))
			remove(personFigure);
		if (this.getChildren().contains(roleFigure))
			remove(roleFigure);
		if (this.getChildren().contains(teamFigure))
			remove(teamFigure);
		if (this.getChildren().contains(groupFigure))
			remove(groupFigure);
		if (this.getChildren().contains(organisationFigure))
			remove(organisationFigure);

		VisualSystemStructureActor actor = (VisualSystemStructureActor)model;
		// toggling between sketchy and non sketchy
		if(actor.getActorKind()==SystemStructureActorKind.PERSON) {
			add(personFigure, 0);
			sketchyFigure = personFigure;
		} else if(actor.getActorKind()==SystemStructureActorKind.ROLE) {
			add(roleFigure, 0);
			sketchyFigure = roleFigure;
		} else if(actor.getActorKind()==SystemStructureActorKind.TEAM) {
			add(teamFigure, 0);
			sketchyFigure = teamFigure;
		} else if(actor.getActorKind()==SystemStructureActorKind.GROUP) {
			add(groupFigure, 0);
			sketchyFigure = groupFigure;
		} else {
			add(organisationFigure, 0);
			sketchyFigure = organisationFigure;
		}

		if (model.isIsSketchy()) {
			nameLabel.setFont(SKETCHY_FONT);
		} else {
			nameLabel.setFont(DEFAULT_FONT);
		}
	}
}
