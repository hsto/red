package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.commands;

import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.visualmodeling.traceability.TraceLinkUtil;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualStateEditPart;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState;

public class VisualStateUpdateCommand extends Command {

	protected VisualStateEditPart editPart;

	protected VisualState model;
	protected VisualState oldModel;
	protected VisualState newModel;

	public VisualStateUpdateCommand(VisualState model, VisualState newModel, VisualStateEditPart editPart) {
		this.model = model;
		EcoreUtil.Copier copier = new EcoreUtil.Copier(false,false);
		this.oldModel = (VisualState) copier.copy(model);
		this.newModel = newModel;
		this.editPart = editPart;
	}

	@Override
	public void execute() {
		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
			applyUpdates(model, newModel,true);
		}
		else{
			applyUpdates(model, newModel,false);
		}
		editPart.refresh();
	}


	private void applyUpdates(VisualState modelTobeChanged, VisualState modelChanges, boolean cascadeUpdates){
		if(cascadeUpdates){
			//put the updates towards all traces
			Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(modelTobeChanged);
			for(IVisualElement element : allTraces){
				updateStateValues((VisualState)element, modelChanges.getName(), modelChanges.getEntry(), modelChanges.getExit(), modelChanges.getDo());
			}
		}
		updateStateValues(modelTobeChanged, modelChanges.getName(), modelChanges.getEntry(), modelChanges.getExit(), modelChanges.getDo());

	}

	public void updateStateValues(VisualState element, String name, String entry, String exit, String doText){
		element.setName(name);
		element.setEntry(entry);
		element.setExit(exit);
		element.setDo(doText);
	}



	@Override
	public void undo() {

		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
			applyUpdates(model, oldModel,true);
		}
		else{
			applyUpdates(model, oldModel,false);
		}
		editPart.refresh();
	}

}
