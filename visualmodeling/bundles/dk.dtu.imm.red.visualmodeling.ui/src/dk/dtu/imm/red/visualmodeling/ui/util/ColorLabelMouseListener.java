package dk.dtu.imm.red.visualmodeling.ui.util;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class ColorLabelMouseListener implements MouseListener {

	private Label label;
	private Shell shellProperties;
	
	public ColorLabelMouseListener(Label label, Shell shellProperties) {
		this.label = label;
		this.shellProperties = shellProperties;
	}
	
	@Override
	public void mouseDoubleClick(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDown(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseUp(MouseEvent e) {
		ColorDialog dialog = new ColorDialog(shellProperties);
		dialog.setText("Choose a color");
		RGB rgb = dialog.open();
		if (rgb != null) {
			label.setBackground(new Color(shellProperties.getDisplay(), rgb));
		}
		
	}
	
}
