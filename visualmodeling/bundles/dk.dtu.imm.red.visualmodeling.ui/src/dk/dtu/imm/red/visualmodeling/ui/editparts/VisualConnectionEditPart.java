package dk.dtu.imm.red.visualmodeling.ui.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.draw2d.AbsoluteBendpoint;
import org.eclipse.draw2d.BendpointConnectionRouter;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionEndpointLocator;
import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.FanRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.ManhattanConnectionRouter;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.gef.editpolicies.ConnectionEndpointEditPolicy;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.connector.impl.ConnectorImpl;
import dk.dtu.imm.red.visualmodeling.ui.SpecificationElementSelectionDialog;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionLinkCommand;
import dk.dtu.imm.red.visualmodeling.ui.editpolicies.VisualConnectionBenpointEditPolicy;
import dk.dtu.imm.red.visualmodeling.ui.editpolicies.VisualConnectionConnectionEditPolicy;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionRouting;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;

public class VisualConnectionEditPart extends AbstractConnectionEditPart {

	private IVisualConnection model;
	private Connection connection;
	private Label targetMultiplicity;
	private Label sourceMultiplicity;
	private Label name;

	private VisualConnectionAdapter adapter;

	 public VisualConnectionEditPart() {
		    super();
		    adapter = new VisualConnectionAdapter();

		    targetMultiplicity = new Label();
		    sourceMultiplicity = new Label();
		    name = new Label();
	 }

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE, new ConnectionEndpointEditPolicy());
		installEditPolicy(EditPolicy.CONNECTION_ROLE, new VisualConnectionConnectionEditPolicy());
		installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE, new VisualConnectionBenpointEditPolicy());
	}

	 @Override
	 protected IFigure createFigure() {
	    connection = new PolylineConnection();
	    connection.setConnectionRouter(new BendpointConnectionRouter());
	    connection.add(name);
	    connection.add(targetMultiplicity);
	    connection.add(sourceMultiplicity);
	    return connection;
	}

	 @Override
	 public void setModel(Object model) {
		 super.setModel(model);
		 this.model = (IVisualConnection)model;
	 };

	 @Override
	 protected void refreshVisuals() {

	    IVisualConnection model = (IVisualConnection)getModel();
	    PolylineConnection polyline = ((PolylineConnection)connection);

		// Refresh the bendpoints from the model
		List<Point> modelConstraint = ((VisualConnection)getModel()).getBendpoints();
	    List<AbsoluteBendpoint> figureConstraint = new ArrayList<AbsoluteBendpoint>();
	    for (Point p : modelConstraint) {
	      figureConstraint.add(new AbsoluteBendpoint(p));
	    }
	    connection.setRoutingConstraint(figureConstraint);

	    switch(model.getLineStyle())
	    {
	    case DEFAULT:
	    	polyline.setLineStyle(SWT.LINE_SOLID);
	    	polyline.setLineWidth(2);
	    	break;
	    case DASHED:
	    	//polyline.setLineStyle(SWT.LINE_DASH);
	    	polyline.setLineStyle(SWT.LINE_CUSTOM);
	    	polyline.setLineDash(new float[] {5f, 5f});
	    	polyline.setLineWidth(2);
	    	break;
	    }

	    RotatableDecoration sourceDecoration = null;
	    RotatableDecoration targetDecoration = null;

	    switch(model.getSourceDecoration())
	    {
		case ARROW_HEAD:
			PolylineDecoration deco = new PolylineDecoration();
			deco.setScale(5f, 5f);
			sourceDecoration = deco;
			break;
		case NONE:
			// Nothing
			break;
		case ARROW_HEAD_SOLID:
			PolygonDecoration polygonDecoration = new PolygonDecoration();
			polygonDecoration.setScale(5f, 5f);
			polygonDecoration.setBackgroundColor(ColorConstants.black);
			sourceDecoration = polygonDecoration;
			break;
		case ARROW_HEAD_SOLID_WHITE:
			PolygonDecoration polygonDecorationWhite = new PolygonDecoration();
			polygonDecorationWhite.setScale(5f, 5f);
			polygonDecorationWhite.setBackgroundColor(ColorConstants.white);
			sourceDecoration = polygonDecorationWhite;
			break;
		case DIAMOND_WHITE:
			PolygonDecoration pdDiamondWhite = new PolygonDecoration();
			PointList pdDiamondWhitePointList = new PointList();
			pdDiamondWhitePointList.addPoint(0,0);
			pdDiamondWhitePointList.addPoint(-2,2);
			pdDiamondWhitePointList.addPoint(-4,0);
			pdDiamondWhitePointList.addPoint(-2,-2);
			pdDiamondWhite.setTemplate(pdDiamondWhitePointList);
			pdDiamondWhite.setScale(5f, 5f);
			pdDiamondWhite.setBackgroundColor(ColorConstants.white);
			sourceDecoration = pdDiamondWhite;
			break;
		case DIAMOND_BLACK:
			PolygonDecoration pdDiamondBlack = new PolygonDecoration();
			PointList pdDiamondBlackPointList = new PointList();
			pdDiamondBlackPointList.addPoint(0,0);
			pdDiamondBlackPointList.addPoint(-2,2);
			pdDiamondBlackPointList.addPoint(-4,0);
			pdDiamondBlackPointList.addPoint(-2,-2);
			pdDiamondBlack.setTemplate(pdDiamondBlackPointList);
			pdDiamondBlack.setScale(5f, 5f);
			pdDiamondBlack.setBackgroundColor(ColorConstants.black);
			sourceDecoration = pdDiamondBlack;
			break;
	    }

	    switch(model.getTargetDecoration())
	    {
		case ARROW_HEAD:
			PolylineDecoration deco = new PolylineDecoration();
			deco.setScale(10f, 10f);
			targetDecoration = deco;
			break;
		case NONE:
			// Nothing
			break;
		case ARROW_HEAD_SOLID:
			PolygonDecoration polygonDecoration = new PolygonDecoration();
			polygonDecoration.setScale(10f, 10f);
			polygonDecoration.setBackgroundColor(ColorConstants.black);
			targetDecoration = polygonDecoration;
			break;
		case ARROW_HEAD_SOLID_WHITE:
			PolygonDecoration polygonDecorationWhite = new PolygonDecoration();
			polygonDecorationWhite.setScale(10f, 10f);
			polygonDecorationWhite.setBackgroundColor(ColorConstants.white);
			targetDecoration = polygonDecorationWhite;
			break;
		case DIAMOND_WHITE:
			PolygonDecoration pdDiamondWhite = new PolygonDecoration();
			PointList pdDiamondWhitePointList = new PointList();
			pdDiamondWhitePointList.addPoint(0,0);
			pdDiamondWhitePointList.addPoint(-1,1);
			pdDiamondWhitePointList.addPoint(-2,0);
			pdDiamondWhitePointList.addPoint(-1,-1);
			pdDiamondWhite.setTemplate(pdDiamondWhitePointList);
			pdDiamondWhite.setBackgroundColor(ColorConstants.white);
			sourceDecoration = pdDiamondWhite;
			break;
		case DIAMOND_BLACK:
			PolygonDecoration pdDiamondBlack = new PolygonDecoration();
			PointList pdDiamondBlackPointList = new PointList();
			pdDiamondBlackPointList.addPoint(0,0);
			pdDiamondBlackPointList.addPoint(-1,1);
			pdDiamondBlackPointList.addPoint(-2,0);
			pdDiamondBlackPointList.addPoint(-1,-1);
			pdDiamondBlack.setTemplate(pdDiamondBlackPointList);
			pdDiamondBlack.setBackgroundColor(ColorConstants.black);
			sourceDecoration = pdDiamondBlack;
			break;
	    }

    	polyline.setTargetDecoration(targetDecoration);
    	polyline.setSourceDecoration(sourceDecoration);

	    name.setText(model.getName());
	    ConnectionLocator locator = new ConnectionLocator(connection, ConnectionLocator.MIDDLE);
	    locator.setRelativePosition(PositionConstants.NSEW);
	    locator.setGap(5);
	    connection.remove(name);
	    connection.add(name, locator);


	    ConnectionEndpointLocator  endpointLocator;

	    sourceMultiplicity.setText(model.getSourceMultiplicity());
	    endpointLocator = new ConnectionEndpointLocator(connection, false);
	    endpointLocator.setUDistance(10);
	    endpointLocator.setVDistance(10);
	    connection.remove(sourceMultiplicity);
	    connection.add(sourceMultiplicity, endpointLocator);

	    targetMultiplicity.setText(model.getTargetMultiplicity());
	    endpointLocator = new ConnectionEndpointLocator(connection, true);
	    endpointLocator.setUDistance(10);
	    endpointLocator.setVDistance(10);
	    connection.remove(targetMultiplicity);
	    connection.add(targetMultiplicity, endpointLocator);

	  }


	@Override
	public void performRequest(Request req) {
		if (req.getType() == RequestConstants.REQ_OPEN) {
			
			if (getModel() == null) {
				return;
			}
			else if (getModel() instanceof VisualConnection) {
				VisualConnection element = (VisualConnection) this.getModel();
				
				if (element.getSpecificationElement() == null) {
					linkModelToElement();
				}
				else {
					openElement();
				}
			}
		}

	}


	 @Override
	 public void activate() {
		    if(!isActive()) {
		      ((VisualConnection)getModel()).eAdapters().add(adapter);
		    }
		    super.activate();
		  }

	  @Override
	  public void deactivate() {
	    if(isActive()) {
	      ((VisualConnection)getModel()).eAdapters().remove(adapter);
	    }
	    super.deactivate();
	  }


	 public class VisualConnectionAdapter implements Adapter
	 {

		@Override
		public void notifyChanged(Notification notification) {

			if(notification.getEventType() == Notification.SET && notification.getFeatureID(VisualConnection.class) == visualmodelPackage.VISUAL_CONNECTION__ROUNTING)
			{
				model.getBendpoints().clear();
			    switch((ConnectionRouting)notification.getNewValue())
			    {
				case FAN:
					connection.setConnectionRouter(new FanRouter());
					break;
				case MANHATTEN:
					connection.setConnectionRouter(new ManhattanConnectionRouter());
					break;
				case NONE:
					connection.setConnectionRouter(ConnectionRouter.NULL);
					break;
				case SHORTEST:
					connection.setConnectionRouter(new BendpointConnectionRouter());
					break;
			    }
			}

			refresh();
		}

		@Override
		public Notifier getTarget() {
				return (VisualConnection)getModel();
		}

		@Override
		public void setTarget(Notifier newTarget) {
			// Nothing
		}

		@Override
		public boolean isAdapterForType(Object type) {
			return type.equals(VisualConnection.class);
		}

	 }
	 
	 public Label getNameLabel() {
		 return this.name;
	 }
	 
	public void openElement() {
		if (getModel() == null || !(getModel() instanceof VisualConnection)) return;
		
		VisualConnection visualConnection = (VisualConnection)getModel();
		final Element element = visualConnection.getSpecificationElement(); 
		
		if (element == null) return;
		
		IConfigurationElement[] configurationElements = 
				Platform.getExtensionRegistry().getConfigurationElementsFor("dk.dtu.imm.red.core.element");
		try {
			for (IConfigurationElement configurationElement : configurationElements) {

				final Object extension = configurationElement.createExecutableExtension("class");

				if (extension instanceof IElementExtensionPoint) {
					
					ISafeRunnable runnable = new ISafeRunnable() {

						@Override
						public void handleException(final Throwable exception) {
							exception.printStackTrace();
						}

						@Override
						public void run() throws Exception {							
							((IElementExtensionPoint) extension).openElement(element);						
						}
					};
					runnable.run();
				}
			}
		} catch (CoreException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (Exception e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}
	
	public void linkModelToElement() {
		Object model = this.getModel();
		
		if(model instanceof VisualConnection) {
			
			SpecificationElementSelectionDialog dialog =
					new SpecificationElementSelectionDialog(null, this);
			dialog.create();
			int result = dialog.open();

			if(result == SpecificationElementSelectionDialog.CANCEL)
			{
				return;
			}

			SpecificationElement element = dialog.getSelection();

			VisualConnection connection = (VisualConnection)this.getModel();
			if (isLinkValid(connection, element)) {
				
				Command command = new VisualConnectionLinkCommand(connection, element);
				getViewer().getEditDomain().getCommandStack().execute(command);
				
				this.refresh();					
			}
			else {
				messagePopUp();
			}
		}
	}
	
	private boolean isLinkValid (VisualConnection connection, SpecificationElement element) {
    	
    	VisualSpecificationElementImpl sourceSE = null, targetSE = null;
		IVisualElement sourceElement = connection.getSource();
		IVisualElement targetElement = connection.getTarget();
		if (sourceElement != null) sourceSE = (VisualSpecificationElementImpl)sourceElement;
		if (targetElement != null) targetSE = (VisualSpecificationElementImpl)targetElement;
		if (sourceSE == null || sourceSE.getSpecificationElement() == null || targetSE == null || targetSE.getSpecificationElement() == null) return false;
    	
    	if (element instanceof Connector) {
    		ConnectorImpl actorRelation = (ConnectorImpl) element;
			if ( (actorRelation.getSource().get(0).equals(sourceSE.getSpecificationElement()) &&
					actorRelation.getTarget().get(0).equals(targetSE.getSpecificationElement()) )
					||
					(actorRelation.getSource().get(0).equals(targetSE.getSpecificationElement()) &&
							actorRelation.getTarget().get(0).equals(sourceSE.getSpecificationElement()) ) ) { // Relation already exists
				
				return true;
			}
    	}
    	
    	return false;
    }
    
    private void messagePopUp() {
    	MessageDialog md = new MessageDialog(Display
				.getCurrent().getActiveShell(), "Invalid link command", null,
				"Selected element does not have the same source and target references as the connection!",
				MessageDialog.INFORMATION,
				new String[] { "OK" }, 0);			
		md.open();
    }
	 
}
