package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.visualmodeling.traceability.TraceLinkUtil;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualEnumerationEditPart;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral;

public class VisualEnumerationUpdateCommand extends Command {

	protected VisualEnumerationEditPart editPart;

	protected VisualEnumerationElement model;
	protected VisualEnumerationElement oldModel;
	protected VisualEnumerationElement newModel;

	private List<IVisualElement> modifiedChildrenList = new ArrayList<IVisualElement>();
	private List<IVisualElement> addedChildrenList = new ArrayList<IVisualElement>();
	private List<IVisualElement> deletedChildrenList = new ArrayList<IVisualElement>();


	public VisualEnumerationUpdateCommand(VisualEnumerationElement model, VisualEnumerationElement newModel,
			VisualEnumerationEditPart editPart) {
		this.model = model;
		EcoreUtil.Copier copier = new EcoreUtil.Copier(false,false);
		this.oldModel = (VisualEnumerationElement) copier.copy(model);
		this.newModel = newModel;
		this.editPart = editPart;

		for(IVisualElement child : model.getElements()){
			String visID = child.getVisualID();
			boolean found=false;
			for(IVisualElement newModelChild : newModel.getElements()){
				if(visID.equals(newModelChild.getVisualID())){
					found=true;
					if(!child.equals(newModelChild)){
						modifiedChildrenList.add(child);
					}
				}
			}
			if(!found)
				deletedChildrenList.add(child);
		}


		for(IVisualElement newModelChild : newModel.getElements()){
			boolean found = false;
			for(IVisualElement child : model.getElements()){
				if(child.getVisualID().equals(newModelChild.getVisualID())){
					found=true;
				}
			}
			if(!found)
				addedChildrenList.add(newModelChild);
		}


		System.out.println("MOD:"+modifiedChildrenList);
		System.out.println("INS:"+addedChildrenList);
		System.out.println("DEL:"+deletedChildrenList);

	}

	@Override
	public void execute() {
		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
			applyUpdates(model, newModel,true);
		}
		else{
			applyUpdates(model, newModel,false);
		}
		applyInsertDelete(model);
		editPart.refresh();
	}


	private void applyUpdates(VisualEnumerationElement modelTobeChanged, VisualEnumerationElement modelChanges, boolean cascadeUpdates){

		for(IVisualElement child : modifiedChildrenList){
			for(VisualEnumerationLiteral literal : modelChanges.getLiterals()){
				if(child.getVisualID().equals(literal.getVisualID())){
					//put the updates towards all traces
					Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(child);
					for(IVisualElement element : allTraces){
						((VisualEnumerationLiteral)element).setName(literal.getName());
					}
					((VisualEnumerationLiteral)child).setName(literal.getName());

					break;
				}
			}
		}


		if(cascadeUpdates){
			//put the updates towards all traces
			Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(modelTobeChanged);
			for(IVisualElement element : allTraces){
				element.setName(modelChanges.getName());
			}
		}
		modelTobeChanged.setName(modelChanges.getName());
	}


	public void applyInsertDelete(VisualEnumerationElement modelTobeChanged){
		for(IVisualElement child : addedChildrenList){
			if(child instanceof VisualEnumerationLiteral)
				modelTobeChanged.getLiterals().add((VisualEnumerationLiteral)child);
			modelTobeChanged.getElements().add(child);
			child.setParent(modelTobeChanged);
			child.setDiagram(modelTobeChanged.getDiagram());

		}
		for(IVisualElement child : deletedChildrenList){
			child.setParent(null);
			child.setDiagram(null);
			if(child instanceof VisualEnumerationLiteral)
				modelTobeChanged.getLiterals().remove((VisualEnumerationLiteral)child);
			modelTobeChanged.getElements().remove(child);
		}
	}


	public void undoInsertDelete(VisualEnumerationElement modelTobeChanged){
		for(IVisualElement child : deletedChildrenList){
			if(child instanceof VisualEnumerationLiteral)
				modelTobeChanged.getLiterals().add((VisualEnumerationLiteral)child);
			modelTobeChanged.getElements().add(child);
			child.setParent(modelTobeChanged);
			child.setDiagram(modelTobeChanged.getDiagram());

		}

		for(IVisualElement child : addedChildrenList){
			child.setParent(null);
			child.setDiagram(null);
			if(child instanceof VisualEnumerationLiteral)
				modelTobeChanged.getLiterals().remove((VisualEnumerationLiteral)child);
			modelTobeChanged.getElements().remove(child);
		}
	}


	@Override
	public void undo() {

		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
			applyUpdates(model, oldModel,true);
		}
		else{
			applyUpdates(model, oldModel,false);
		}
		undoInsertDelete(model);
		editPart.refresh();
	}

}
