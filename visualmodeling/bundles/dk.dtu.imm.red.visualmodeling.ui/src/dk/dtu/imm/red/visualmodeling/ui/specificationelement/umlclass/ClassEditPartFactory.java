package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass;

import org.eclipse.gef.EditPart;

import dk.dtu.imm.red.visualmodeling.ui.factories.EditPartFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.EmptyEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualClassEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualEnumerationEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualPackageEditPart;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualPackageElement;

public class ClassEditPartFactory extends EditPartFactoryBase {

	public ClassEditPartFactory(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	public EditPart createEditPartInternal(EditPart context, Object model) {

		EditPart part = null;

		if (model instanceof VisualClassElement) {
			part = new VisualClassEditPart(ruleCollection);
		}
		if (model instanceof VisualEnumerationElement) {
			part = new VisualEnumerationEditPart(ruleCollection);
		}
		if (model instanceof VisualPackageElement) {
			part = new VisualPackageEditPart(ruleCollection);
		}
		if (model instanceof VisualClassAttribute ||
			model instanceof VisualClassOperation ||
			model instanceof VisualEnumerationLiteral
			){
			part = new EmptyEditPart(new RuleCollection());
		}

		return part;
	}
}
