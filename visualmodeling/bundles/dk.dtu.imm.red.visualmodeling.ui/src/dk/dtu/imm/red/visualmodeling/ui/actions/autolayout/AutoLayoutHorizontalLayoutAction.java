package dk.dtu.imm.red.visualmodeling.ui.actions.autolayout;

import org.eclipse.ui.IEditorPart;

public class AutoLayoutHorizontalLayoutAction extends AutoLayoutAction {

    public AutoLayoutHorizontalLayoutAction(IEditorPart editor) {
		super(editor, LayoutAlgorithmType.HORIZONTAL_LAYOUT);
	}

}
