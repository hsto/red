package dk.dtu.imm.red.visualmodeling.ui.editpolicies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementDeleteCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualElementComponentEditPolicy extends ComponentEditPolicy {

	  @Override 
	  protected Command createDeleteCommand(GroupRequest deleteRequest) {		  	
		  	IVisualElement element = (IVisualElement)getHost().getModel();
		    VisualElementDeleteCommand command = new VisualElementDeleteCommand(element);		    
		    return command;		    
		  }
}
