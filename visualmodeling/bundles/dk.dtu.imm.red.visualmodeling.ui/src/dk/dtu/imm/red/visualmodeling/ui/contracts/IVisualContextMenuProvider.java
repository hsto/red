package dk.dtu.imm.red.visualmodeling.ui.contracts;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.jface.action.IMenuManager;

public interface IVisualContextMenuProvider {

	 public void buildContextMenu(IMenuManager menu, EditPartViewer viewer);
    
}
