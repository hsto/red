package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal;

import dk.dtu.imm.red.visualmodeling.ui.validation.IRule;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalElement;

public class GoalRule implements IRule{

	@Override
	public boolean canConnect(IVisualElement source, IVisualElement target,
			IVisualConnection connection) {
		
		if(source == target && source instanceof VisualGoalElement)
		{
			return false;
		}
		
		return true;
	}

	@Override
	public boolean canAccept(IVisualElement parent, IVisualElement child) {
		
		if(parent instanceof VisualGoalElement)
		{
			return false;
		}
		
		return true;
	}
}
