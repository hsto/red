package dk.dtu.imm.red.visualmodeling.zestlayout;

import java.util.Map;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.EList;
import org.eclipse.zest.layouts.InvalidLayoutConfiguration;
import org.eclipse.zest.layouts.LayoutAlgorithm;
import org.eclipse.zest.layouts.LayoutBendPoint;
import org.eclipse.zest.layouts.LayoutEntity;

import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.LayoutAlgorithmType;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

public class AutoLayoutProcessor {
	String algorithmType;
	LayoutAlgorithm algorithm;
	Diagram diagram;
	VisualDiagram visualDiagram;

	VisualLayoutGraph rootGraph = new VisualLayoutGraph();

	VisualLayoutEntity[] entitiesArr;
	VisualLayoutRelationship[] relationshipArr;

	VisualLayoutEntity[] beforeStateEntitiesArr;
	VisualLayoutRelationship[] beforeStateRelationshipArr;

	@SuppressWarnings("unchecked")
	public AutoLayoutProcessor(Diagram diagram, String algorithmType){
		this.diagram = diagram;
		this.visualDiagram = diagram.getVisualDiagram();
		this.algorithmType = algorithmType;
		//convert visual elements to layout elements
		recursiveFlattenElements(null, visualDiagram.getElements());
		flattenConnections(visualDiagram.getDiagramConnections());

		algorithm = LayoutAlgorithmType.getLayoutAlgorithm(algorithmType);

		entitiesArr =	new VisualLayoutEntity[rootGraph.getEntities().size()];
		entitiesArr =  (VisualLayoutEntity[]) rootGraph.getEntities().toArray(entitiesArr);

		beforeStateEntitiesArr = new VisualLayoutEntity[rootGraph.getEntities().size()];
		for(int i=0;i<beforeStateEntitiesArr.length;i++){
			beforeStateEntitiesArr[i] = entitiesArr[i].clone();
		}

		relationshipArr = new VisualLayoutRelationship[rootGraph.getRelationships().size()];
		relationshipArr =  (VisualLayoutRelationship[]) rootGraph.getRelationships().toArray(relationshipArr);

		beforeStateRelationshipArr = new VisualLayoutRelationship[rootGraph.getRelationships().size()];
		for(int i=0;i<beforeStateRelationshipArr.length;i++){
			beforeStateRelationshipArr[i] = relationshipArr[i].clone();
		}
	}

	public void autoLayoutGraph() throws InvalidLayoutConfiguration{
		Dimension newBounds = getCalculatedBounds(entitiesArr);

		//run layout algorithm
		algorithm.applyLayout(
				entitiesArr,
				relationshipArr,
				0.d, 0.d, newBounds.width, newBounds.height,
				false, false);

		updateLayoutToVisualElement(entitiesArr,relationshipArr);

	}

	public void undoAutoLayoutGraph() throws InvalidLayoutConfiguration{
		updateLayoutToVisualElement(beforeStateEntitiesArr, beforeStateRelationshipArr);

	}


	public void updateLayoutToVisualElement(VisualLayoutEntity[] entitiesArr, VisualLayoutRelationship[] relationshipArr){
		//update the actual layout in the visual elements
		for(VisualLayoutEntity entity : entitiesArr){
			if(entity.getParent()==null){
				System.out.println("SETTING LOCATION OF "+ entity.getVisualElement().getName() +
						" TO: "+ new Point((int)entity.x,(int)entity.y));
				entity.getVisualElement().setLocation(new Point((int)entity.x,(int)entity.y));
				entity.getVisualElement().setBounds(new Dimension((int)entity.width,(int)entity.height));
			}
			else{
				double containerWidth = entity.getParent().getWidthInLayout();
				double containerHeight = entity.getParent().getWidthInLayout();

				if(entity.x > containerWidth){
					entity.x = 0;
				}
				if(entity.y > containerHeight){
					entity.y = 0;
				}
			}
		}

		for(VisualLayoutRelationship relationship : relationshipArr){
			IVisualConnection conn = relationship.getRealObject();
			conn.getBendpoints().clear();
			if(relationship.getBendPoints()!=null){
				for(LayoutBendPoint bendPoint : relationship.getBendPoints()){
					conn.getBendpoints().add(new Point((int)bendPoint.getX(),(int)bendPoint.getY()));
				}
			}
		}
	}


	public Dimension getCalculatedBounds(VisualLayoutEntity[] entitiesArr){
		double maxXValue=0.0d;
		double maxWidth=0.0d;

		double maxYValue=0.0d;
		double maxHeight=0.0d;

		for(VisualLayoutEntity entity: entitiesArr){
			double x = entity.getXInLayout();
			if(x>maxXValue)
				maxXValue = x;

			double width = entity.getWidthInLayout();
			if(width>maxWidth)
				maxWidth = width;

			double y = entity.getYInLayout();
			if(y>maxYValue)
				maxYValue = y;

			double height = entity.getHeightInLayout();
			if(height>maxHeight)
				maxHeight = height;
		}
		int calculatedWidth = (int) ((maxXValue + maxWidth));
		int calculatedHeight = (int)  ((maxHeight + maxYValue));
		return new Dimension(calculatedWidth,calculatedHeight);
	}

	public void recursiveFlattenElements(VisualLayoutEntity parent, EList<IVisualElement> visualElementList){
		for(IVisualElement visualElement: visualElementList){
			VisualLayoutEntity entity = new VisualLayoutEntity(visualElement);
			rootGraph.addEntity(entity);
			if(parent!=null)
				entity.setParent(parent);
			if(visualElement.getElements().size()>0){
				recursiveFlattenElements(entity, visualElement.getElements());
			}
		}
	}

	public void flattenConnections(EList<IVisualConnection> connectionList){
		Map<IVisualElement,LayoutEntity> mapping = rootGraph.getVisualElToLayoutEntities();
		for(IVisualConnection connection : connectionList){
			IVisualElement sourceElement = connection.getSource();
			IVisualElement targetElement = connection.getTarget();

			if(sourceElement==null || targetElement==null)
				continue;

			LayoutEntity sourceEntity = mapping.get(sourceElement);
			LayoutEntity destinationEntity = mapping.get(targetElement);
			boolean bidirectional = true;
			if(connection.getDirection()==ConnectionDirection.SOURCE_TARGET
					|| connection.getDirection()==ConnectionDirection.TARGET_SOURCE)
				bidirectional = false;
			VisualLayoutRelationship relationship = new VisualLayoutRelationship(connection,
					sourceEntity, destinationEntity, bidirectional);
			rootGraph.addRelationship(relationship);
		}
	}

	public void printGraph(){
		rootGraph.printGraph();
	}
}
