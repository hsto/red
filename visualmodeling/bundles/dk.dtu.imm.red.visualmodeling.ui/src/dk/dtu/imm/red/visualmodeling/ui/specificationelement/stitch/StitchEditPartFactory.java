package dk.dtu.imm.red.visualmodeling.ui.specificationelement.stitch;

import org.eclipse.gef.EditPart;

import dk.dtu.imm.red.visualmodeling.ui.factories.EditPartFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.ActivityEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.GoalEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.StateMachineEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.SystemStructureEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.ClassEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.UseCaseEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;

public class StitchEditPartFactory extends EditPartFactoryBase {

	RuleCollection ruleCollection;

	public StitchEditPartFactory(RuleCollection ruleCollection) {
		super(ruleCollection);
		this.ruleCollection = ruleCollection;
	}

	public EditPart createEditPartInternal(EditPart context, Object model) {
		EditPart part = null;

		GoalEditPartFactory goalEditPartFactory = new GoalEditPartFactory(ruleCollection);
		part = goalEditPartFactory.createEditPartInternal(context, model);
		if(part!=null){
			return part;
		}

		UseCaseEditPartFactory useCaseEditPartFactory = new UseCaseEditPartFactory(ruleCollection);
		part = useCaseEditPartFactory.createEditPartInternal(context, model);
		if(part!=null)
			return part;

		ClassEditPartFactory classEditPartFactory = new ClassEditPartFactory(ruleCollection);
		part = classEditPartFactory.createEditPartInternal(context, model);
		if(part!=null)
			return part;

		ActivityEditPartFactory activityEditPartFactory = new ActivityEditPartFactory(ruleCollection);
		part = activityEditPartFactory.createEditPartInternal(context, model);
		if(part!=null)
			return part;

		StateMachineEditPartFactory stateMachineEditPartFactory = new StateMachineEditPartFactory(ruleCollection);
		part = stateMachineEditPartFactory.createEditPartInternal(context, model);
		if(part!=null)
			return part;

		SystemStructureEditPartFactory contextEditPartFactory = new SystemStructureEditPartFactory(ruleCollection);
		part = contextEditPartFactory.createEditPartInternal(context, model);
		if(part!=null)
			return part;

		return part;
	}
}
