package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.traceability.TraceLinkUtil;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;

public class ElementFindAllTraceAction extends SelectionAction {

    public static final String ELEMENT_FIND_ALL_TRACE = "ElementFindAllTrace";
    public static final String REQ_ELEMENT_FIND_ALL_TRACE = "ElementFindAllTrace";

    Request request;

    public ElementFindAllTraceAction(IWorkbenchPart part) {
        super(part);
        setId(ELEMENT_FIND_ALL_TRACE);
        setText("Tracing");
        request = new Request(ELEMENT_FIND_ALL_TRACE);
    }


    @Override
    public void run() {
    	IVisualElement[] selections = getSelectedElements();
    	IVisualConnection[] connSelections = getSelectedConnections();

    	if(selections.length>0){
    		VisualElement element = (VisualElement)selections[0];

    		Set<IVisualElement> backwardTraces = TraceLinkUtil.findBackwardTraces(element);
    		Set<IVisualElement>forwardTraces = TraceLinkUtil.findForwardTraces(element);
    		ElementTraceLinkDialog traceDialog = new ElementTraceLinkDialog(
					Display.getDefault().getActiveShell(),
					element,
					backwardTraces, forwardTraces);
    		traceDialog.open();
    	}
    	else if(connSelections.length>0){
    		VisualConnection conn = (VisualConnection)connSelections[0];

    		Set<IVisualConnection> backwardTraces = TraceLinkUtil.findBackwardTraces(conn);
    		Set<IVisualConnection>forwardTraces = TraceLinkUtil.findForwardTraces(conn);
    		ElementTraceLinkDialog traceDialog = new ElementTraceLinkDialog(
					Display.getDefault().getActiveShell(),
					conn,
					backwardTraces, forwardTraces);
    		traceDialog.open();
    	}

    }

    @SuppressWarnings("unchecked")
	private IVisualElement[] getSelectedElements()
   	{
   		List<EditPart> selection = this.getSelectedObjects();

   		Vector<IVisualElement> vector = new Vector<IVisualElement>();
   		for(EditPart editpart : selection){
   			Object model = editpart.getModel();

   			if(model instanceof IVisualElement)
   			{
   				vector.add((IVisualElement) model);
   			}
   		}

   		IVisualElement[] result = vector.toArray(new IVisualElement[vector.size()]);
   		vector.clear();
   		return result;
   	}

    @SuppressWarnings("unchecked")
	private IVisualConnection[] getSelectedConnections()
   	{
   		List<EditPart> selection = this.getSelectedObjects();

   		Vector<IVisualConnection> vector = new Vector<IVisualConnection>();
   		for(EditPart editpart : selection){
   			Object model = editpart.getModel();

   			if(model instanceof IVisualConnection)
   			{
   				vector.add((IVisualConnection) model);
   			}
   		}

   		IVisualConnection[] result = vector.toArray(new IVisualConnection[vector.size()]);
   		vector.clear();
   		return result;
   	}

	@Override
    @SuppressWarnings("rawtypes")
    protected boolean calculateEnabled() {

    	List selectionList = getSelectedObjects();

    	if(selectionList.size() != 1)
    		return false;

        return true;
    }
}