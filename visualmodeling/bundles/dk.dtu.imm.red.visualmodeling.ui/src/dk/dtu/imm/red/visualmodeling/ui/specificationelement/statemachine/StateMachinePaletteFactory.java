package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine;

import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;

import dk.dtu.imm.red.visualmodeling.ui.directedit.CreationAndDirectEditTool;
import dk.dtu.imm.red.visualmodeling.ui.factories.DiagramConnectionFactory;
import dk.dtu.imm.red.visualmodeling.ui.factories.PaletteFactoryBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualDecisionMergeStateFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualDeepHistoryStateFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualFinalStateFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualForkJoinStateFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualInitialStateFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualReceiveStateFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualSendStateFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualShallowHistoryStateFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts.VisualStateFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;

public class StateMachinePaletteFactory extends PaletteFactoryBase{

	@Override
	public void populatePaletteInternal(PaletteRoot root) {
		PaletteDrawer group = new PaletteDrawer("Activity");

		CreationToolEntry entry;

		entry = new CreationToolEntry("State", "Create a new state",
				new VisualStateFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Initial Pseudostate", "Create a new initial pseudostate",
				new VisualInitialStateFactory(), null, null);
		group.add(entry);

		entry = new CreationToolEntry("Final Pseudostate", "Create a new final pseudostate",
				new VisualFinalStateFactory(), null, null);
		group.add(entry);

		entry = new CreationToolEntry("Decision/Merge State", "Create a new decision/merge state",
				new VisualDecisionMergeStateFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Fork/Join State", "Create a new fork/join state",
				new VisualForkJoinStateFactory(), null, null);
		group.add(entry);

		entry = new CreationToolEntry("Receive State", "Create a new receive state",
				new VisualReceiveStateFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Send State", "Create a new send state",
				new VisualSendStateFactory(), null, null);
		entry.setToolClass(CreationAndDirectEditTool.class);
		group.add(entry);

		entry = new CreationToolEntry("Deep History State", "Create a new deep history state",
				new VisualDeepHistoryStateFactory(), null, null);
		group.add(entry);


		entry = new CreationToolEntry("Shallow History State", "Create a new shallow history state",
				new VisualShallowHistoryStateFactory(), null, null);
		group.add(entry);


		group.add(new PaletteSeparator());

		ConnectionCreationToolEntry connectionEntry = new ConnectionCreationToolEntry(
				"State Transition Flow", "Creates a new state transition flow connection",
				new DiagramConnectionFactory(ConnectionType.STATE_TRANSITION.getName(),
						"",
						ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
						ConnectionDecoration.ARROW_HEAD,
						ConnectionDirection.SOURCE_TARGET), null, null);
//		connectionEntry.setToolClass(CreationAndDirectEditTool.class);

		group.add(connectionEntry);


		root.add(group);
	}
}
