package dk.dtu.imm.red.visualmodeling.ui.figures;

import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.EllipseAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import dk.dtu.imm.red.visualmodeling.ui.util.anchors.DiamondConnectionAnchor;
import dk.dtu.imm.red.visualmodeling.visualmodel.GenericType;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement;

public class VisualGenericElementFigure extends VisualElementFigure {

	private VisualGenericElement model;
	//private Figure figure;
	//private PolygonShape polygon;

	public VisualGenericElementFigure(VisualGenericElement model) {
		super(model);

		this.model = model;
		init();
		add(this.nameLabel);
		refreshVisuals();
	}
	
	private void init() {
		Rectangle r = new Rectangle(0, 0, 100, 30);

		if (model.getGenericType() == GenericType.RECTANGLE) {

			formalFigure = new RectangleFigure();
			this.add(formalFigure);
		} else if (model.getGenericType() == GenericType.CIRCLE
				|| model.getGenericType() == GenericType.ELLIPSE) {

			formalFigure = new Ellipse();
			this.connectionAnchor = new EllipseAnchor(this);
			this.add(formalFigure);
		} else if (model.getGenericType() == GenericType.DIAMOND) {


			this.connectionAnchor = new DiamondConnectionAnchor(this);
			formalFigure = new PolygonShape();

			((PolygonShape)formalFigure).setStart(r.getTop());
			((PolygonShape)formalFigure).addPoint(r.getRight());
			((PolygonShape)formalFigure).addPoint(r.getBottom());
			((PolygonShape)formalFigure).addPoint(r.getLeft());
			((PolygonShape)formalFigure).setEnd(r.getLeft());

			// polygon.setFill(true);
			// polygon.setBackgroundColor(ColorConstants.lightGray);
			((PolygonShape)formalFigure).setPreferredSize(50, 50);
			add(formalFigure,
					new Rectangle(((PolygonShape)formalFigure).getStart(), formalFigure
							.getPreferredSize()));
		} else if (model.getGenericType() == GenericType.TRIANGLE) {
			formalFigure = new PolygonShape();

			((PolygonShape)formalFigure).setStart(r.getTop());
			((PolygonShape)formalFigure).addPoint(r.getBottomRight());
			((PolygonShape)formalFigure).addPoint(r.getBottomLeft());
			((PolygonShape)formalFigure).setEnd(r.getBottomLeft());

			formalFigure.setPreferredSize(50, 50);
			add(formalFigure,
					new Rectangle(((PolygonShape)formalFigure).getStart(), formalFigure
							.getPreferredSize()));
		}
	}

	@Override
	protected void paintFigure(Graphics graphics) {

		Rectangle r = getBounds().getCopy();

		if (model.getGenericType() == GenericType.RECTANGLE) {
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
		} else if (model.getGenericType() == GenericType.CIRCLE
				|| model.getGenericType() == GenericType.ELLIPSE) {
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
		} else if (model.getGenericType() == GenericType.DIAMOND) {
			((PolygonShape)formalFigure).removeAllPoints();
			((PolygonShape)formalFigure).setStart(new Point(r.width / 2, 0));
			((PolygonShape)formalFigure).addPoint(new Point(r.width, r.height / 2));
			((PolygonShape)formalFigure).addPoint(new Point(r.width / 2, r.height));
			((PolygonShape)formalFigure).addPoint(new Point(0, r.height / 2));
			((PolygonShape)formalFigure).setEnd(new Point(0, r.height / 2));
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
		} else if (model.getGenericType() == GenericType.TRIANGLE) {
			((PolygonShape)formalFigure).removeAllPoints();
			((PolygonShape)formalFigure).setStart(new Point(r.width / 2, 0));
			((PolygonShape)formalFigure).addPoint(new Point(r.width, r.height - 1));
			((PolygonShape)formalFigure).addPoint(new Point(0, r.height - 1));
			((PolygonShape)formalFigure).setEnd(new Point(0, r.height - 1));
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
		}

		setConstraint(nameLabel, new Rectangle(0, 0, r.width, r.height));

	}

	@Override
	public void refreshVisuals() {
		super.refreshVisuals();
		
		if(this.getChildren().contains(formalFigure))
			remove(formalFigure);
		
		init();
		nameLabel.setText(model.getName());
	}
}
