package dk.dtu.imm.red.visualmodeling.ui.specificationelement.stitch;

import dk.dtu.imm.red.visualmodeling.ui.validation.IRule;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class StitchRule implements IRule{

	@Override
	public boolean canConnect(IVisualElement source, IVisualElement target,
			IVisualConnection connection) {
		return false;
	}

	@Override
	public boolean canAccept(IVisualElement parent, IVisualElement child) {
		return false;
	}

}
