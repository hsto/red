package dk.dtu.imm.red.visualmodeling.ui.factories;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.visualmodel.GenericType;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;

public class GenericCreationFactory implements CreationFactory{

	private GenericType type;
	
	public GenericCreationFactory(GenericType type)
	{
		this.type = type;
	}
	
	@Override
	public Object getNewObject() {
		VisualGenericElement element = visualmodelFactory.eINSTANCE.createVisualGenericElement();
		element.setGenericType(type);
		return element;
	}

	@Override
	public Object getObjectType() {		
		return VisualGenericElement.class;
	}

	
	
}
