package dk.dtu.imm.red.visualmodeling.ui.editpolicies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualConnectionCreateCommand;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

public class VisualElementGraphicalNodeEditPolicy extends GraphicalNodeEditPolicy {
	 
	private RuleCollection ruleCollection;
		
	public VisualElementGraphicalNodeEditPolicy(RuleCollection ruleCollection)
	{
		this.ruleCollection = ruleCollection;
	}
	
	  @Override 
	  protected Command getConnectionCompleteCommand(CreateConnectionRequest request){
	    VisualConnectionCreateCommand command = (VisualConnectionCreateCommand) request.getStartCommand();
	    command.setTarget((IVisualElement)getHost().getModel());
	    
	    if(!ruleCollection.canConnect(command.getSource(), command.getTarget(), command.getConnection()))
	    {
	    	return null;
	    }
	    
	    return command;
	  }
	 
	  @Override 
	  protected Command getConnectionCreateCommand(CreateConnectionRequest request) {
		
		VisualConnectionCreateCommand command = new VisualConnectionCreateCommand();
		VisualDiagram diagram = (VisualDiagram) getHost().getRoot().getContents().getModel();
		
		IVisualElement source = (IVisualElement)getHost().getModel(); 
		IVisualConnection connection = (VisualConnection) request.getNewObject();
		
		command.setSource(source);
	    command.setConnection(connection);
	    command.setDiagram(diagram);
	    request.setStartCommand(command);
	    return command;
	  }
	 
	  @Override protected Command getReconnectTargetCommand(ReconnectRequest request) {
	    return null;
	  }
	 
	  @Override protected Command getReconnectSourceCommand(ReconnectRequest request) {
	    return null;
	  }
}
