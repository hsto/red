package dk.dtu.imm.red.visualmodeling.ui.factories;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Signature;
import dk.dtu.imm.red.specificationelements.port.Port;
import dk.dtu.imm.red.specificationelements.system.System;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalElementFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualPortFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.systemstructure.editparts.VisualSystemStructureActorFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts.VisualClassCreationFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualActorElementFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts.VisualUseCaseElementFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDecoration;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionLineStyle;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind;

/**
 * 
 * @author Luai
 *	Used for dropping multiple specification elements into a diagram
 */
public class VisualCreationFactory implements CreationFactory{

	private List<SpecificationElement> specificationElements;
	private DiagramConnectionFactory creationConnectionFactory;
	private DiagramType diagramType;
	
	public VisualCreationFactory() {
		creationConnectionFactory = new DiagramConnectionFactory(ConnectionType.ASSOCIATION.getName(), "",
				ConnectionLineStyle.DEFAULT, ConnectionDecoration.NONE,
				ConnectionDecoration.NONE, ConnectionDirection.BIDIRECTIONAL);
	}
	
	@Override
	public Object getNewObject() {
		List<Object> visuals = new ArrayList<Object>();
		
		for (SpecificationElement element : specificationElements) {
			if (element instanceof Connector) { // VisualConection
				VisualConnection visualConnection = (VisualConnection)creationConnectionFactory.getNewObject();
				visualConnection.setSpecificationElement(element);
				visuals.add(visualConnection);
			}
			else { // VisualElements
				CreationFactory factory = getCreationFactory(element);
				if (factory != null) {
					VisualSpecificationElement visualElement = (VisualSpecificationElement)factory.getNewObject();
					visualElement.setSpecificationElement(element);
					visuals.add(visualElement);
				}
			}
		}
		
		specificationElements = null;
		return visuals;
	}

	@Override
	public Object getObjectType() {
		List<Object> types = new ArrayList<Object>();
		
		for (SpecificationElement element : specificationElements) {
			if (element instanceof Connector) {
				types.add(creationConnectionFactory.getObjectType());
			}
			else { // VisualElements
				CreationFactory factory = getCreationFactory(element);
				if (factory != null) {
					types.add(factory.getObjectType());
				}
			}
		}
		return types;
	}
	
	private CreationFactory getCreationFactory(SpecificationElement specificationElement)
	{
		CreationFactory selectedFactory = null;
		
		if(specificationElement instanceof Goal)
		{
			selectedFactory = new VisualGoalElementFactory();
		}
		else if(specificationElement instanceof UseCase)
		{
			selectedFactory = new VisualUseCaseElementFactory();
		}
		else if(specificationElement instanceof Actor)
		{
			if (diagramType != null && diagramType == DiagramType.SYSTEM_STRUCTURE) {
				if (specificationElement instanceof System)
				{
					selectedFactory = new VisualSystemFactory();
				}
				else if (specificationElement instanceof Port)
				{
					
					selectedFactory = new VisualPortFactory();
				}
				else 
				{
					selectedFactory = new VisualSystemStructureActorFactory(SystemStructureActorKind.getByName(specificationElement.getElementKind()));
				}
			}
			else {
				selectedFactory = new VisualActorElementFactory();
			}
		}
		else if(specificationElement instanceof Signature) {
			selectedFactory = new VisualClassCreationFactory();
		}
		else
		{
			throw new IllegalArgumentException("Cannot find creation factory for element: " + specificationElement.getClass());	
		}
		
		return selectedFactory;
	}
	
	public void setSpecificationElements(List<SpecificationElement> specificationElements)
	{
		this.specificationElements = specificationElements;
	}
	
	public void setDiagramType (DiagramType diagramType)
	{
		this.diagramType = diagramType;
	}

}
