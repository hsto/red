package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity;

import dk.dtu.imm.red.visualmodeling.ui.contracts.VisualEditorConfigurationBase;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericEditPartFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.genric.GenericPaletteFactory;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;


public class ActivityConfiguration extends VisualEditorConfigurationBase{

	public ActivityConfiguration()
	{
		this.ruleCollection = new RuleCollection();
		this.ruleCollection.addRule(new ActivityRule());

		this.paletteFactory = new ActivityPaletteFactory();
		this.paletteFactory.setNext(new GenericPaletteFactory());

		this.editPartFactory = new ActivityEditPartFactory(ruleCollection);
		this.editPartFactory.setNext(new GenericEditPartFactory(ruleCollection));
	}
}
