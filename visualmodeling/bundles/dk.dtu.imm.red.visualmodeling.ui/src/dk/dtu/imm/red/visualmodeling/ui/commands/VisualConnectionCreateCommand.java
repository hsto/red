package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

public class VisualConnectionCreateCommand extends Command {

	  private IVisualElement source;
	  private IVisualElement target;
	  private IVisualConnection connection;
	  private VisualDiagram diagram;

	  @Override
	  public boolean canExecute() {
	    return source != null && target != null && connection != null;
	  }

	  @Override public void execute() {
			System.out.println("VisualConnectionCreateCommand");
		  connection.setSource(source);
		  connection.setTarget(target);
		  source.getConnections().add(connection);
		  target.getConnections().add(connection);

		  //self connection
		  if(source==target){
			  Point elementLocation = source.getLocation();
			  Dimension elementBounds = source.getBounds();
			  connection.getBendpoints().add(new Point(elementLocation.x+elementBounds.width,
					  elementLocation.y+elementBounds.height+30));
			  connection.getBendpoints().add(new Point(elementLocation.x+elementBounds.width+30,
					  elementLocation.y+elementBounds.height));
		  }
		  diagram.getDiagramConnections().add(connection);
	  }

	  @Override public void undo() {
		  connection.setSource(null);
		  connection.setTarget(null);
	  }

	  public void setTarget(IVisualElement target) {
	    this.target = target;
	  }

	  public void setSource(IVisualElement source) {
	    this.source = source;
	  }

	  public void setConnection(IVisualConnection connection) {
	    this.connection = connection;
	  }

	  public void setDiagram(VisualDiagram diagram) {
	    this.diagram = diagram;
	  }

	  public IVisualElement getTarget() {
		    return target;
		  }

		  public IVisualElement getSource() {
		    return source;
		  }

		  public IVisualConnection getConnection() {
		    return connection;
		  }

		  public VisualDiagram getDiagram() {
		    return diagram;
		  }
}
