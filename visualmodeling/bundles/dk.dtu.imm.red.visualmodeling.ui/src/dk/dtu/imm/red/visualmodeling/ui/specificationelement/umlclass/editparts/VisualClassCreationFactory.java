package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures.VisualClassFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.ClassFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement;

public class VisualClassCreationFactory implements CreationFactory {

	public VisualClassCreationFactory()
	{

	}

	@Override
	public Object getNewObject() {
		VisualClassElement element = ClassFactory.eINSTANCE.createVisualClassElement();
		element.setBounds(VisualClassFigure.initialBounds);
		return element;
	}

	@Override
	public Object getObjectType() {
		return VisualClassElement.class;
	}



}
