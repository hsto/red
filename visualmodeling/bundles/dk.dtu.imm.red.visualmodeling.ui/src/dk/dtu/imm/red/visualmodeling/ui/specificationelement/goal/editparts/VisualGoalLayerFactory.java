package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.figures.VisualGoalLayerFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer;

public class VisualGoalLayerFactory implements CreationFactory{

	public Object getNewObject() {

		VisualGoalLayer element = GoalFactory.eINSTANCE.createVisualGoalLayer();
		element.setBounds(VisualGoalLayerFigure.initialBounds);
	    return element;
	  }

	  public Object getObjectType() {
	    return VisualGoalLayer.class;
	  }

}
