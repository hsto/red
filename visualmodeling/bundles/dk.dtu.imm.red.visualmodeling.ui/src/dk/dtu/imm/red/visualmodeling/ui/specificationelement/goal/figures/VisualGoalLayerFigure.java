package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualGoalLayerFigure extends VisualElementFigure{

	public static Dimension initialBounds = new Dimension(500,220);

	private IVisualElement model;
	private RectangleFigure formalFigure;
	private SVGFigure sketchyFigure;

	//for content area, just use normal rectangle, no need for sketchy
	private RectangleFigure formalContentArea;
	private SVGFigure sketchyContentArea;

	private String sketchyFigureURL = svgPath + "Goal/VisualGoalLayer.svg";
	private String sketchyContentAreaURL = svgPath + "Goal/VisualGoalLayerContent.svg";

	public VisualGoalLayerFigure(IVisualElement model) {
		super(model);

		this.model = model;

		formalFigure = new RectangleFigure();
		formalContentArea = new RectangleFigure();

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);

		sketchyContentArea = new SVGFigure();
		sketchyContentArea.setURI(sketchyContentAreaURL);

		// TODO remove this code and remove label from superclass so not to mess up z-order
//		this.remove(super.nameLabel);

//		this.add(rectangleFigure);
		this.add(nameLabel);
//		this.add(contentArea);


	}

	  @Override
	  protected void paintFigure(Graphics graphics) {

		  Rectangle r = getBounds().getCopy();

		  //boundingRectangle.setVisible(false);

		  nameLabel.setText(model.getName());

		if(this.getChildren().contains(sketchyFigure)){
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
			setConstraint(sketchyContentArea, new Rectangle(0, 25, r.width, r.height-25));
		}
		if(this.getChildren().contains(formalFigure)){
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
			setConstraint(formalContentArea, new Rectangle(0, 25, r.width, r.height-25));
		}

		  setConstraint(nameLabel, new Rectangle(5, 5, r.width, 20));
	  }

	  @Override
		public void refreshVisuals()
		{
			if(this.getChildren().contains(formalFigure)){
				remove(formalContentArea);
				remove(formalFigure);
			}
			if(this.getChildren().contains(sketchyFigure)){
				remove(sketchyContentArea);
				remove(sketchyFigure);
			}

			//toggling between sketchy and non sketchy
			if(model.isIsSketchy()){
				add(sketchyFigure,0);
				add(sketchyContentArea,1);
				this.activeArea = sketchyContentArea;

				nameLabel.setFont(SKETCHY_FONT);
			}
			else{
				add(formalFigure,0);
				add(formalContentArea,1);
				this.activeArea = formalContentArea;
				nameLabel.setFont(DEFAULT_FONT);
			}

			nameLabel.setText(this.model.getName());


//			System.out.println(this.getClass() + ":" +
//					this.getBounds().x +"," +
//					this.getBounds().y +"," +
//					this.getBounds().width +"," +
//					this.getBounds().height
//						);
//
//			for(Object child : this.getChildren()){
//				System.out.println("CHILD"+child.getClass() + ":" +
//					((Figure)child).getBounds().x +"," +
//					((Figure)child).getBounds().y +"," +
//					((Figure)child).getBounds().width +"," +
//					((Figure)child).getBounds().height
//						);
//			}
//			System.out.println("----");
		}
}
