package dk.dtu.imm.red.visualmodeling.ui;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;

public class VisualModelingSelectionProvider implements ISelectionProvider, ISelectionChangedListener{

	private VisualEditor editor;
	private ISelection selection;
	
	public VisualModelingSelectionProvider(VisualEditor part)
	{
		editor = part;
	}
	
	@Override
	public void addSelectionChangedListener(ISelectionChangedListener arg0) {
	}

	@Override
	public ISelection getSelection() { 
		return selection;
	}

	@Override
	public void removeSelectionChangedListener(ISelectionChangedListener arg0) {
		
	}

	@Override
	public void setSelection(ISelection newSelection) { 
		selection = newSelection;
	}

	@Override
	public void selectionChanged(SelectionChangedEvent arg0) {
		
		selection = arg0.getSelection();
		editor.UpdateActions();
	}
}
