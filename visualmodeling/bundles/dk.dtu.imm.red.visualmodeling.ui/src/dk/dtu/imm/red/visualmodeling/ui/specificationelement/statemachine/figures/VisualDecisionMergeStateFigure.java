package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.lite.svg.SVGFigure;

import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualDecisionMergeStateFigure extends VisualElementFigure {

	public static Dimension initialBounds = new Dimension(100,80);

	//private PolygonShape formalFigure;

	//private SVGFigure sketchyFigure;
	private String sketchyFigureURL = svgPath + "Statemachine/DecisionMergeState.svg";


	public VisualDecisionMergeStateFigure(IVisualElement model)
	{
		super(model);

		formalFigure = new PolygonShape();
	    formalFigure.setFill(true);
	    formalFigure.setBackgroundColor(ColorHelper.stateMachineDiagramDefaultColor);

		sketchyFigure = new SVGFigure();
		sketchyFigure.setURI(sketchyFigureURL);

		add(this.nameLabel);
	    refreshVisuals();
	}

	@Override
	  protected void paintFigure(Graphics graphics) {

		Rectangle r = getBounds().getCopy();
		boundingRectangle.setVisible(false);

		if (this.getChildren().contains(sketchyFigure))
			setConstraint(sketchyFigure, new Rectangle(0, 0, r.width, r.height));
		if (this.getChildren().contains(formalFigure)){
			((PolygonShape)formalFigure).removeAllPoints();
			((PolygonShape)formalFigure).setStart(new Point(r.width / 2, 0));
			((PolygonShape)formalFigure).addPoint(new Point(r.width, r.height / 2));
			((PolygonShape)formalFigure).addPoint(new Point(r.width / 2, r.height));
			((PolygonShape)formalFigure).addPoint(new Point(0, r.height / 2));
			((PolygonShape)formalFigure).setEnd(new Point(0, r.height / 2));
			setConstraint(formalFigure, new Rectangle(0, 0, r.width, r.height));
		}

		setConstraint(nameLabel, new Rectangle(0, 0, r.width, r.height));
	  }

	@Override
	public void refreshVisuals()
	{
		if (this.getChildren().contains(formalFigure))
			remove(formalFigure);
		if (this.getChildren().contains(sketchyFigure))
			remove(sketchyFigure);

		// toggling between sketchy and non sketchy
		if (model.isIsSketchy()) {
			add(sketchyFigure, 0);
			nameLabel.setFont(SKETCHY_FONT);
		} else {
			add(formalFigure, 0);
			nameLabel.setFont(DEFAULT_FONT);
		}
		nameLabel.setText(this.model.getName());
	}
}
