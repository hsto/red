package dk.dtu.imm.red.visualmodeling.ui.actions;

import java.util.List;
import java.util.Vector;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementMakeSketchyCommand;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class MakeSketchyAction extends SelectionAction{

  	public static final String ELEMENT_MAKE_SKETCHY = "ElementMakeSketchy";
   	public static final String REQ_ELEMENT_MAKE_SKETCHY = "ElementMakeSketchy";

	public MakeSketchyAction(IWorkbenchPart part) {
		super(part);

        setId(ELEMENT_MAKE_SKETCHY);
        setText("Make element sketchy");
	}

	@Override
	@SuppressWarnings("unchecked")
	protected boolean calculateEnabled() {
		List<EditPart> selection = this.getSelectedObjects();

		for(EditPart editpart : selection){
			Object model = editpart.getModel();

			if(model instanceof IVisualElement)
			{
				if(!((IVisualElement)model).isIsSketchy()){
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public void run() {
        VisualElementMakeSketchyCommand command =
        		new VisualElementMakeSketchyCommand(getSelectedElements());
        execute(command);

        for(Object object : this.getSelectedObjects()){
        	((EditPart)object).refresh();
        }
	}

	@SuppressWarnings("unchecked")
    private IVisualElement[] getSelectedElements()
	{
		List<EditPart> selection = this.getSelectedObjects();

		Vector<IVisualElement> vector = new Vector<IVisualElement>();
		for(EditPart editpart : selection){
			Object model = editpart.getModel();

			if(model instanceof IVisualElement)
			{
				vector.add((IVisualElement) model);
			}
		}

		IVisualElement[] result = vector.toArray(new IVisualElement[vector.size()]);
		vector.clear();
		return result;
	}

}
