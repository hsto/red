package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.VisualSpecificationElementImpl;

/**
 * 
 * @author Luai
 *
 */
public class VisualElementCascadeDeleteCommand extends Command {

	protected IVisualElement parent;
	protected IVisualElement element;
	protected VisualDiagram diagram;
	protected List<Command> commands;
	
	protected Element selectedSE;
	protected Group selectedSEParent;

	public VisualElementCascadeDeleteCommand(IVisualElement element) {
		this.element = element;
		this.parent = element.getParent();
		this.diagram = element.getDiagram();
		commands = new ArrayList<Command>();
		
		if (element instanceof VisualSpecificationElementImpl) {
			VisualSpecificationElementImpl se = (VisualSpecificationElementImpl) element;
			selectedSE = se.getSpecificationElement();
			
			if (selectedSE != null) {
				selectedSEParent = selectedSE.getParent();
			}
		}
	}

	@Override
	public boolean canExecute() {
		return true;
	}

	@Override
	public void execute() {
		
		commands = new ArrayList<Command>();
		List<IVisualConnection> connections = new ArrayList<IVisualConnection>(element.getConnections());

		for (IVisualConnection connection : connections) {
			Command command = new VisualConnectionDeleteCommand(connection);
			command.execute();
			commands.add(command);
		}

		element.setDiagram(null);
		element.setParent(null);
		parent.getElements().remove(element);		
	}

	@Override
	public void undo() {
		// Creates SE
		if (selectedSE != null) {
			final Element element = selectedSE;
			final Group parent = selectedSEParent;
			if (parent != null) {
				parent.getContents().add(element);
			}
			
			IConfigurationElement[] configurationElements = Platform
					.getExtensionRegistry().getConfigurationElementsFor(
							"dk.dtu.imm.red.core.element");

			try {
				for (IConfigurationElement configurationElement : configurationElements) {

					final Object extension = configurationElement
							.createExecutableExtension("class");

					if (extension instanceof IElementExtensionPoint) {
						ISafeRunnable runnable = new ISafeRunnable() {

							@Override
							public void handleException(final Throwable exception) {
								exception.printStackTrace();
							}

							@Override
							public void run() throws Exception {
								((IElementExtensionPoint) extension)
										.restoreElement(element);
							}
						};
						runnable.run();
					}
				}
			} catch (CoreException e) {
				e.printStackTrace(); LogUtil.logError(e);
			} catch (Exception e) {
				e.printStackTrace(); LogUtil.logError(e);
			}
		}
		
		// Adds element
		parent.getElements().add(element);
		element.setParent(parent);
		element.setDiagram(diagram);

		for (Command command : commands) {
			command.undo();
		}
	}

}
