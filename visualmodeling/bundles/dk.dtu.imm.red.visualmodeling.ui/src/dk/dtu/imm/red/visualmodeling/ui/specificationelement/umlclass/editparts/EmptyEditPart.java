package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures.EmptyFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class EmptyEditPart extends VisualElementEditPart{

	//empty edit part used by class attributes and operations which
	//do not need edit part to work because it should be edited from
	//the class specification edit dialog, however they are required
	//to be added into the attribute "elements" for visual weaving
	public EmptyEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected IFigure createFigure() {
		return new EmptyFigure((IVisualElement) getModel());
	}

	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		//do nothing
	}

}
