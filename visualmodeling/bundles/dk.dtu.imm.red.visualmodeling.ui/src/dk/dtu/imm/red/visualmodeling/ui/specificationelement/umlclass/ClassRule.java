package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass;

import dk.dtu.imm.red.visualmodeling.ui.validation.IRule;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualPackageElement;

public class ClassRule implements IRule {

	@Override
	public boolean canConnect(IVisualElement source, IVisualElement target,
			IVisualConnection connection) {

		if (source instanceof VisualPackageElement
				|| target instanceof VisualPackageElement) {
			return false;
		}

		return true;
	}

	@Override
	public boolean canAccept(IVisualElement parent, IVisualElement child) {
		if (parent instanceof VisualPackageElement) {
			return true;
		} else if (parent instanceof VisualDiagram) {
			return true;
		}
		return false;
	}

}
