package dk.dtu.imm.red.visualmodeling.ui.widget;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;

public class NavigateToMessageDialog extends MessageDialog {

	private List<Diagram> diagrams;
	private Element selectedDiagram; 
	public NavigateToMessageDialog(List<Diagram> diagrams, Shell parentShell, String dialogTitle, Image dialogTitleImage, String dialogMessage, int dialogImageType, String[] dialogButtonLabels, int defaultIndex) {
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, dialogButtonLabels, defaultIndex);
		this.diagrams = diagrams;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
		GridLayout layout = (GridLayout) container.getLayout();
		layout.marginWidth = 25;
		
		boolean firstElement = true;
		for (final Diagram diagram : diagrams) {
			Button button = new Button(container, SWT.RADIO);
			button.setText(diagram.getLabel() + " - " + diagram.getName());
			button.setImage(diagram.getIcon());
			button.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					super.widgetSelected(e);
					selectedDiagram = diagram;
				}
			});
			if (firstElement) { // Init
				selectedDiagram = diagram;
				button.setSelection(true);
				firstElement = false;
			}
		}		
		
		return container;

	}
	
	public Element getSelectedDiagram() {
		return selectedDiagram;
	}

}
