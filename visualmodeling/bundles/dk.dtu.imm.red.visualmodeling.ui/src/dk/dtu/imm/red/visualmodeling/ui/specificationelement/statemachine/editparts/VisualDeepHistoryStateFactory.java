package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualDeepHistoryStateFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDeepHistoryState;

public class VisualDeepHistoryStateFactory implements CreationFactory {
	public Object getNewObject() {

		VisualDeepHistoryState element = StatemachineFactory.eINSTANCE
				.createVisualDeepHistoryState();
		element.setBounds(VisualDeepHistoryStateFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualDeepHistoryState.class;
	}
}
