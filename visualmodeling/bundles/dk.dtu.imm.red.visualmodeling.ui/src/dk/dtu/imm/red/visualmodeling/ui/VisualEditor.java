package dk.dtu.imm.red.visualmodeling.ui;

import java.util.EventObject;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor.PropertyValueWrapper;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.actions.AlignmentAction;
import org.eclipse.gef.ui.actions.EditorPartAction;
import org.eclipse.gef.ui.actions.MatchHeightAction;
import org.eclipse.gef.ui.actions.MatchSizeAction;
import org.eclipse.gef.ui.actions.MatchWidthAction;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.gef.ui.actions.ZoomInAction;
import org.eclipse.gef.ui.actions.ZoomOutAction;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;
import org.eclipse.gef.ui.properties.UndoablePropertySheetEntry;
import org.eclipse.gef.ui.properties.UndoablePropertySheetPage;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.IPropertySourceProvider;
import org.eclipse.ui.views.properties.PropertySheetPage;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.visualmodeling.ui.actions.ElementCascadeDeleteAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.ElementDeleteAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.ElementFindAllTraceAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.ElementPropertiesAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.ExportDiagramAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.LinkSpecificationElementAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.MakeNonSketchyAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.MakeSketchyAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.NavigateToElementTreeAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.OpenSpecificationElementAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.TransformElementAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.UnlinkSpecificationElementAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.AutoLayoutDirectedGraphLayoutAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.AutoLayoutGridLayoutAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.AutoLayoutHorizontalLayoutAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.AutoLayoutHorizontalTreeLayoutAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.AutoLayoutRadialLayoutAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.AutoLayoutSpringLayoutAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.AutoLayoutTreeLayoutAction;
import dk.dtu.imm.red.visualmodeling.ui.actions.autolayout.AutoLayoutVerticalLayoutAction;
import dk.dtu.imm.red.visualmodeling.ui.contracts.IPaletteFactory;
import dk.dtu.imm.red.visualmodeling.ui.contracts.IVisualContextMenuProvider;
import dk.dtu.imm.red.visualmodeling.ui.contracts.IVisualEditorConfiguration;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.provider.visualmodelItemProviderAdapterFactory;

public class VisualEditor extends GraphicalEditorWithFlyoutPalette {

	protected IPaletteFactory paletteFactory;
	protected EditPartFactory editPartFactory;
	protected IVisualContextMenuProvider visualContextMenuProvider;

	protected boolean isDirty;
	protected VisualModelingSelectionProvider selectionProvider;
	protected Diagram diagram;
	protected PropertySheetPage propertyPage;

	public VisualEditor(IVisualEditorConfiguration config) {
		this.paletteFactory = config.getPaletteFactory();
		this.editPartFactory = config.getEditPartFactory();
		this.visualContextMenuProvider = config.getContextMenuProvider();
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		// TODO javadoc advices against storing models in IEditorInput, they can
		// stay in memory long after the editor itself has closed. Use
		// constructor instead.
		diagram = (Diagram) ((BaseEditorInput) input).getElement();

		setEditDomain(new DefaultEditDomain(this));

		super.init(site, input);
	}

	@Override
	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();

		getGraphicalViewer().setContents(diagram.getVisualDiagram());
		getGraphicalViewer().addDropTargetListener(
				new SpecificationElementDropListener(getGraphicalViewer(),
						LocalSelectionTransfer.getTransfer(), diagram.getVisualDiagram().getDiagramType()));
	}

	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setEditPartFactory(editPartFactory);
		viewer.setContextMenu(new VisualModelingContextMenuProvider(
				getGraphicalViewer(), getActionRegistry(), visualContextMenuProvider));

		selectionProvider = new VisualModelingSelectionProvider(this);
		viewer.addSelectionChangedListener(selectionProvider);

		// TODO move this to the action setup method
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());
		ScalableFreeformRootEditPart rootEditPart = (ScalableFreeformRootEditPart)viewer.getRootEditPart();
		IAction zoomIn = new ZoomInAction(rootEditPart.getZoomManager());
		IAction zoomOut = new ZoomOutAction(rootEditPart.getZoomManager());
		getActionRegistry().registerAction(zoomIn);
		getActionRegistry().registerAction(zoomOut);
	}

	public GraphicalViewer getGraphicalViewer()
	{
		return super.getGraphicalViewer();
	}

	public void UpdateActions() {
		this.updateActions(getSelectionActions());
	}

	protected PaletteRoot getPaletteRoot() {
		PaletteRoot root = new PaletteRoot();
		paletteFactory.populatePalette(root);
		return root;
	}

	public void doSave(IProgressMonitor monitor) {

		isDirty = false;
	}

	@Override
	public boolean isDirty() {
		return isDirty;
	}

	@Override
	public void commandStackChanged(EventObject event) {
		isDirty = true;
		this.firePropertyChange(EditorPart.PROP_DIRTY);
		super.commandStackChanged(event);
	}

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class type) {
		if (type.equals(IPropertySheetPage.class)) {
			if (propertyPage == null) {
				propertyPage = (UndoablePropertySheetPage) super
						.getAdapter(type);

				IPropertySourceProvider sourceProvider = new IPropertySourceProvider() {
					IPropertySourceProvider modelPropertySourceProvider = new AdapterFactoryContentProvider(
							new visualmodelItemProviderAdapterFactory());

					@Override
					public IPropertySource getPropertySource(Object object) {
						IPropertySource source = null;
						if (object instanceof EditPart) {
							source = modelPropertySourceProvider
									.getPropertySource(((EditPart) object)
											.getModel());
						} else {
							source = modelPropertySourceProvider
									.getPropertySource(object);
						}

						if (source != null) {
							return new UnwrappingPropertySource(source);
						} else {
							return null;
						}
					}

				};
				UndoablePropertySheetEntry root = new UndoablePropertySheetEntry(
						getCommandStack());
				root.setPropertySourceProvider(sourceProvider);
				propertyPage.setRootEntry(root);
			}
			return propertyPage;
		}

		if (type == ZoomManager.class)
		{
			return getGraphicalViewer().getProperty(ZoomManager.class.toString());
		}

		return super.getAdapter(type);
	}

	public class UnwrappingPropertySource implements IPropertySource {
		private IPropertySource source;

		public UnwrappingPropertySource(final IPropertySource source) {
			this.source = source;
		}

		@Override
		public Object getEditableValue() {
			Object value = source.getEditableValue();
			if (value instanceof PropertyValueWrapper) {
				PropertyValueWrapper wrapper = (PropertyValueWrapper) value;
				return wrapper.getEditableValue(null);
			} else {
				return source.getEditableValue();
			}
		}

		@Override
		public IPropertyDescriptor[] getPropertyDescriptors() {
			return source.getPropertyDescriptors();
		}

		@Override
		public Object getPropertyValue(Object id) {
			Object value = source.getPropertyValue(id);
			if (value instanceof PropertyValueWrapper) {
				PropertyValueWrapper wrapper = (PropertyValueWrapper) value;
				return wrapper.getEditableValue(null);
			} else {
				return source.getPropertyValue(id);
			}
		}

		@Override
		public boolean isPropertySet(Object id) {
			return source.isPropertySet(id);
		}

		@Override
		public void resetPropertyValue(Object id) {
			source.resetPropertyValue(id);
		}

		@Override
		public void setPropertyValue(Object id, Object value) {
			source.setPropertyValue(id, value);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void createActions() {
		super.createActions();

		Action exportDiagramAction = new ExportDiagramAction(this);
		getActionRegistry().registerAction(exportDiagramAction);

		SelectionAction action;

		action = new OpenSpecificationElementAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());
		
		action = new NavigateToElementTreeAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new TransformElementAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new ElementDeleteAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new ElementCascadeDeleteAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());
		
		action = new ElementFindAllTraceAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new MakeSketchyAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new MakeNonSketchyAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new LinkSpecificationElementAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new UnlinkSpecificationElementAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.LEFT);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.RIGHT);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.TOP);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.BOTTOM);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.MIDDLE);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new AlignmentAction((IWorkbenchPart) this,
				PositionConstants.CENTER);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new MatchSizeAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new MatchWidthAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		action = new MatchHeightAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());

		// all the different auto layout
		EditorPartAction layout = new AutoLayoutDirectedGraphLayoutAction(this);
		getActionRegistry().registerAction(layout);

		layout = new AutoLayoutGridLayoutAction(this);
		getActionRegistry().registerAction(layout);

		layout = new AutoLayoutHorizontalLayoutAction(this);
		getActionRegistry().registerAction(layout);

		layout = new AutoLayoutHorizontalTreeLayoutAction(this);
		getActionRegistry().registerAction(layout);

		layout = new AutoLayoutRadialLayoutAction(this);
		getActionRegistry().registerAction(layout);

		layout = new AutoLayoutSpringLayoutAction(this);
		getActionRegistry().registerAction(layout);

		layout = new AutoLayoutTreeLayoutAction(this);
		getActionRegistry().registerAction(layout);

		layout = new AutoLayoutVerticalLayoutAction(this);
		getActionRegistry().registerAction(layout);

		action = new ElementPropertiesAction(this);
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());
		
		List<String> selectionActions = getSelectionActions();

		// Fix for the IWorkbenchPart not being wired right, so selection event
		// don't get routed. This custom selectionprovider fix it.
		for (String actionid : selectionActions) {
			SelectionAction selectionAction = (SelectionAction) getActionRegistry()
					.getAction(actionid);
			selectionAction.setSelectionProvider(selectionProvider);
		}

	}

	public Diagram getDiagram() {
		return diagram;
	}

}
