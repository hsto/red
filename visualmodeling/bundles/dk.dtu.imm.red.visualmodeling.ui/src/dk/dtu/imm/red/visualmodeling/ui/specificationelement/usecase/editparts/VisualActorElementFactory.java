package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase.figures.VisualActorFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.UsecaseFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualActorElement;

public class VisualActorElementFactory implements CreationFactory {
	public Object getNewObject() {

		VisualActorElement element = UsecaseFactory.eINSTANCE
				.createVisualActorElement();
		element.setBounds(VisualActorFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualActorElement.class;
	}
}
