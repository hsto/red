package dk.dtu.imm.red.visualmodeling.ui.contracts;

import java.util.HashMap;

import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.specificationelements.SpecificationElement;

public interface IVisualEditorConfiguration {
	
	IPaletteFactory getPaletteFactory();
	EditPartFactory getEditPartFactory();
	IVisualContextMenuProvider getContextMenuProvider();
	
	/*
	 * Maps supported specification elements to there corresponding CreationFactory.	
	 * */
	HashMap<Class<? extends SpecificationElement>, Class<? extends CreationFactory>> getSpecificationElementMapping();	
}