package dk.dtu.imm.red.visualmodeling.ui.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.ui.figures.VisualElementFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.GenericType;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualGenericElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureActorKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor;

/**
 * 
 * @author Luai
 *
 */
public class PropertiesElementCommand extends Command{
	
	private VisualElementFigure figure;
	private SpecificationElement linkedElement;
	private VisualSpecificationElement visualSpecificationElement;
	private IVisualElement visualElement;
	
	// Kind
	private String linkedElementKind;
	private String linkedElementKindOld;
	private String visualSpecificationKind;
	private String visualSpecificationKindOld;
	
	private SystemStructureActorKind systemStructureActorKind;
	private SystemStructureActorKind systemStructureActorKindOld;
	private GenericType kindGeneric;
	private GenericType kindGenericOld;
	
	// Font
	private Color labelColor;
	private Color labelColorOld;
	private Font fontSketchy;
	private Font fontSketchyOld;
	private Font fontDefault;
	private Font fontDefaultOld;
	
	// Line
	private int lineWidth = -1;
	private int lineWidthOld = -1;
	private int lineStyle = -1;
	private int lineStyleOld = -1;
	private Color lineColor;
	private Color lineColorOld;
	
	private Color fillColor;
	private Color fillColorOld;
	private int alphaFormal = -1;
	private int alphaFormalOld = -1;
	private int alphaSketchy = -1;
	private int alphaSketchyOld = -1;
	
	
	public PropertiesElementCommand(VisualElementFigure figure, SpecificationElement linkedElement, VisualSpecificationElement visualSpecificationElement, IVisualElement visualElement) {
		setFigure(figure);
		setLinkedElement(linkedElement);
		setVisualSpecificationElement(visualSpecificationElement);
		this.visualElement = visualElement;
	}

	@Override
	public void execute() {
		if (linkedElement != null) {			
			if (linkedElementKind != null) {
				linkedElement.setElementKind(linkedElementKind);
			}
		}
		if (visualSpecificationElement != null && visualSpecificationElement.getSpecificationElement() != null) {
			if (visualSpecificationKind != null) {
				visualSpecificationElement.getSpecificationElement().setElementKind(visualSpecificationKind);
			}
		}
		
		if (visualElement != null) {
			String updateKind = null;
			if (kindGeneric != null) {
				VisualGenericElement element = (VisualGenericElement) visualElement;
				element.setGenericType(kindGeneric);
			}
			else if (systemStructureActorKind != null) {
				VisualSystemStructureActor actor = (VisualSystemStructureActor) visualElement;
				actor.setActorKind(systemStructureActorKind);
				updateKind = systemStructureActorKind.getLiteral();
			}
			updateIcon(updateKind);
		}
		
		if (figure != null) {

			if (labelColor != null) {
				figure.getLabel().setForegroundColor(labelColor);
			}			
			if (fontSketchy != null) {
				figure.SKETCHY_FONT = fontSketchy;
			}			
			if (fontDefault != null) {
				figure.DEFAULT_FONT = fontDefault;
			}
			if (lineWidth != -1) {
				figure.getFormalFigure().setLineWidth(lineWidth);
			}
			if (lineStyle != -1) {
				figure.getFormalFigure().setLineStyle(lineStyle);
			}
			if (lineColor != null) {
				figure.getFormalFigure().setForegroundColor(lineColor);
			}
			if (fillColor != null) {
				figure.getFormalFigure().setBackgroundColor(fillColor);
			}
			if (alphaFormal != -1) {
				figure.getFormalFigure().setAlpha(alphaFormal);
			}
			if (alphaSketchy != -1) {
				figure.getSketchyFigure().setAlpha(alphaSketchy);
			}
			figure.refreshVisuals();
		}
	}
	
	@Override
	public void undo() {
		if (linkedElement != null) {			
			if (linkedElementKindOld != null && linkedElementKind != null) {
				linkedElement.setElementKind(linkedElementKindOld);
			}
		}
		if (visualSpecificationElement != null && visualSpecificationElement.getSpecificationElement() != null) {
			if (visualSpecificationKindOld != null && visualSpecificationKind != null) {
				this.visualSpecificationElement.getSpecificationElement().setElementKind(visualSpecificationKindOld);
			}
		}
		
		if (visualElement != null) {
			String updateKind = null;
			if (kindGenericOld != null && kindGeneric != null) {
				VisualGenericElement element = (VisualGenericElement) visualElement;
				element.setGenericType(kindGenericOld);
			}
			else if (systemStructureActorKindOld != null && systemStructureActorKind != null) {
				VisualSystemStructureActor actor = (VisualSystemStructureActor) visualElement;
				actor.setActorKind(systemStructureActorKindOld);
				updateKind = systemStructureActorKindOld.getLiteral();
			}
			updateIcon(updateKind);
			
		}
		
		if (figure != null) {

			if (labelColorOld != null && labelColor != null) {
				figure.getLabel().setForegroundColor(labelColorOld);
			}			
			if (fontSketchyOld != null && fontSketchy != null) {
				figure.SKETCHY_FONT = fontSketchyOld;
			}			
			if (fontDefaultOld != null && fontDefault != null) {
				figure.DEFAULT_FONT = fontDefaultOld;
			}
			if (lineWidthOld != -1 && lineWidth != -1) {
				figure.getFormalFigure().setLineWidth(lineWidthOld);
			}
			if (lineStyleOld != -1 && lineStyle != -1) {
				figure.getFormalFigure().setLineStyle(lineStyleOld);
			}
			if (lineColorOld != null && lineColor != null) {
				figure.getFormalFigure().setForegroundColor(lineColorOld);
			}
			if (fillColorOld != null && fillColor != null) {
				figure.getFormalFigure().setBackgroundColor(fillColorOld);
			}
			if (alphaFormalOld != -1 && alphaFormal != -1) {
				figure.getFormalFigure().setAlpha(alphaFormalOld);
			}
			if (alphaSketchyOld != -1 && alphaSketchy != -1) {
				figure.getSketchyFigure().setAlpha(alphaSketchyOld);
			}
			figure.refreshVisuals();
		}
	}
	
	private void setFigure(VisualElementFigure figure) {
		this.figure = figure;
		labelColorOld = figure.getLabel().getForegroundColor();
		fontSketchyOld = figure.SKETCHY_FONT;
		fontDefaultOld = figure.DEFAULT_FONT;
		if (figure.getFormalFigure() != null) {
			lineWidthOld = figure.getFormalFigure().getLineWidth();
			lineStyleOld = figure.getFormalFigure().getLineStyle();
			lineColorOld = figure.getFormalFigure().getForegroundColor();
			fillColorOld = figure.getFormalFigure().getBackgroundColor();			
		}
		if (figure.getFormalFigure() != null && figure.getFormalFigure().getAlpha() == null) {
			alphaFormalOld = 255;
		} else if (figure.getFormalFigure() != null){			
			alphaFormalOld = figure.getFormalFigure().getAlpha();
		}
		if (figure.getSketchyFigure() != null) {
			alphaSketchyOld = figure.getSketchyFigure().getAlpha();	
		}
	}
	
	private void setLinkedElement(SpecificationElement element) {
		this.linkedElement = element;
		if (element != null) linkedElementKindOld = element.getElementKind();
	}
	
	private void setVisualSpecificationElement(VisualSpecificationElement visualSpecificationElement) {
		this.visualSpecificationElement = visualSpecificationElement;
		if (visualSpecificationElement != null && visualSpecificationElement.getSpecificationElement() != null) {
			visualSpecificationKindOld = visualSpecificationElement.getSpecificationElement().getElementKind();			
		}
		
	}
	
	private void updateIcon(String updateKind) {
		if (updateKind != null) {
			if (linkedElement != null) {
				linkedElement.setIcon(null);
				linkedElement.setIconURI("icons/" + updateKind + ".png");
			} else if (visualSpecificationElement != null && visualSpecificationElement.getSpecificationElement() != null) {
				visualSpecificationElement.getSpecificationElement().setIcon(null);
				visualSpecificationElement.getSpecificationElement().setIconURI("icons/" + updateKind + ".png");
			}
		}
	}
	
	public void setLinkedElementKind(String kind) {
		this.linkedElementKind = kind;
	}
	
	public void setVisualSpecificationKind(String kind) {
		this.visualSpecificationKind = kind;
	}
	
	public void setKindGeneric(GenericType type) {
		if (visualElement != null && visualElement instanceof VisualGenericElement) {
			this.kindGeneric = type;
			VisualGenericElement genericElement = (VisualGenericElement) visualElement;
			this.kindGenericOld = genericElement.getGenericType();
		}
	}
	
	public void setKindSystemStructureActor(SystemStructureActorKind actorKind) {
		if (visualElement != null && visualElement instanceof VisualSystemStructureActor) {
			this.systemStructureActorKind = actorKind;
			VisualSystemStructureActor actor = (VisualSystemStructureActor) visualElement;
			this.systemStructureActorKindOld = actor.getActorKind();
		}
	}
	
	public void setLabelColor(Color color) {
		this.labelColor = color;
	}
	
	public void setFontSketchy(Font font) {
		this.fontSketchy = font;
	}
	
	public void setFontDefault(Font font) {
		this.fontDefault = font;
	}
	
	public void setLineWidth(int width) {
		this.lineWidth = width;
	}
	
	public void setLineStyle(int style) {
		this.lineStyle = style;
	}
	
	public void setLineColor(Color color) {
		this.lineColor = color;
	}
	
	public void setFillColor(Color color) {
		this.fillColor = color;
	}
	
	public void setAlphaFormal(int alpha) {
		this.alphaFormal = alpha;
	}
	
	public void setAlphaSketchy(int alpha) {
		this.alphaSketchy = alpha;
	}

}
