package dk.dtu.imm.red.visualmodeling.ui.editors;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.draw2d.geometry.Dimension;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelFactoryImpl;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind;

/**
 * 
 * Creates a new Diagram specification elememnt.
 *
 */
public class CreateDiagramOperation extends AbstractOperation {

	private Diagram diagram;
	private DiagramExtension extension;
	
	private DiagramType diagramType;
	private String label;
	private String name;
	private String description;
	private Group parent;
	
	public CreateDiagramOperation(String label, String name, String description, Group parent, DiagramType diagramType) {
		super(label);
		
		this.label = label;
		this.name = name;
		this.description = description;
		this.parent = parent;
		this.diagramType = diagramType;
		extension = new DiagramExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable adapter)
			throws ExecutionException {	
		return redo(monitor, adapter);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable adapter)
			throws ExecutionException {
		
		diagram = visualmodelFactoryImpl.eINSTANCE.createDiagram();
		diagram.setLabel(label);
		diagram.setName(name);
		diagram.setDescription(description);
		
		VisualDiagram visualDiagram = visualmodelFactoryImpl.eINSTANCE.createVisualDiagram();
		visualDiagram.setDiagramType(diagramType);
		
		if (diagramType == DiagramType.SYSTEM_STRUCTURE) {
			diagram.setElementKind(SystemStructureDiagramKind.get(0).getLiteral());
		}
		
		diagram.setVisualDiagram(visualDiagram);
		
		Dimension diagramSize = new Dimension(0, 0);  // TODO not implemented
		diagram.getVisualDiagram().setBounds(diagramSize);
		if (parent != null) {
			diagram.setParent(parent);
		} 

		diagram.save();
		extension.openElement(diagram);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor arg0, IAdaptable arg1)
			throws ExecutionException {		
		return null;
	}

}
