package dk.dtu.imm.red.visualmodeling.ui;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Label;

public class ExportDiagramDialog extends Dialog {

	protected Object result;
	protected Shell shlExportDiagram;
	private Text txtFilePath;

	public String exportFilePath;
	public int imageFormat;
	
	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public ExportDiagramDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shlExportDiagram.open();
		shlExportDiagram.layout();
		Display display = getParent().getDisplay();
		while (!shlExportDiagram.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlExportDiagram = new Shell(getParent(), SWT.DIALOG_TRIM);
		shlExportDiagram.setSize(450, 300);
		shlExportDiagram.setText("Export Diagram");
		
		Button btnExport = new Button(shlExportDiagram, SWT.NONE);
		btnExport.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				exportFilePath = txtFilePath.getText();
				result = true;
				shlExportDiagram.close();
			}
		});
		btnExport.setBounds(343, 236, 91, 25);
		btnExport.setText("Export");
		
		final Combo cboImageFormat = new Combo(shlExportDiagram, SWT.READ_ONLY);		
		cboImageFormat.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				switch(cboImageFormat.getText())
				{
					case "JPEG": imageFormat = SWT.IMAGE_JPEG;					
				}
			}
		});
		cboImageFormat.setBounds(343, 51, 91, 23);
		cboImageFormat.add("JPEG");
		cboImageFormat.select(0);
		
		
		Button btnBrowse = new Button(shlExportDiagram, SWT.NONE);
		btnBrowse.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(shlExportDiagram, SWT.SAVE);
			    dialog.setFilterNames(new String[] { "JPEG", "All Files (*.*)" });
			    dialog.setFilterExtensions(new String[] { "*.jpeg", "*.*" });
			    //dialog.setFilterPath("c:\\");
			    //dialog.setFileName("fred.bat");
			    String filepath = dialog.open();
			    if(filepath != null)
			    {
			    	txtFilePath.setText(filepath);
			    }
			}
		});
		btnBrowse.setBounds(343, 10, 91, 25);
		btnBrowse.setText("Browse");
		
		txtFilePath = new Text(shlExportDiagram, SWT.BORDER);
		txtFilePath.setBounds(64, 10, 273, 25);
		
		Button btnNewButton = new Button(shlExportDiagram, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = null;
				shlExportDiagram.close();
			}
		});
		btnNewButton.setBounds(262, 236, 75, 25);
		btnNewButton.setText("Cancel");
		
		Label lblFilePath = new Label(shlExportDiagram, SWT.NONE);
		lblFilePath.setBounds(10, 15, 55, 15);
		lblFilePath.setText("File path:");

	}
}
