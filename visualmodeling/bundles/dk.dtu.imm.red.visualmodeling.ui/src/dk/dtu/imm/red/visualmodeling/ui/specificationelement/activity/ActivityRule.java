package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity;

import dk.dtu.imm.red.visualmodeling.ui.validation.IRule;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode;

public class ActivityRule implements IRule{

	@Override
	public boolean canConnect(IVisualElement source, IVisualElement target,
			IVisualConnection connection) {
		if(connection.getType().equals(ConnectionType.OBJECT_FLOW.getName())){
			if(!(source instanceof VisualObjectNode) && !(target instanceof VisualObjectNode)){
				return false;
			}
		}
		else if(connection.getType().equals(ConnectionType.CONTROL_FLOW.getName())){
			if((source instanceof VisualObjectNode) || (target instanceof VisualObjectNode)){
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean canAccept(IVisualElement parent, IVisualElement child) {
		return false;
	}

}
