package dk.dtu.imm.red.visualmodeling.ui.commands;

import java.util.Set;

import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.traceability.TraceLinkUtil;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualSpecificationElementLinkCommand extends Command{

	private VisualSpecificationElement element;
	private SpecificationElement specificationElement;
	private String oldName = "";

	  public VisualSpecificationElementLinkCommand(VisualSpecificationElement element, SpecificationElement specificationElement)
	    {
	    	this.element = element;
	    	this.specificationElement = specificationElement;
	    	oldName = element.getName();
	    }

	    @Override public void execute() {
	    	linkElement(element);

	    	if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
				Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(element);
				for(IVisualElement e : allTraces){
			    	linkElement(e);
				}
			}
	    }

	    private void linkElement(IVisualElement element){
	    	if(element instanceof VisualSpecificationElement){
	    		((VisualSpecificationElement)element).setSpecificationElement(specificationElement);
	    		if(specificationElement!=null && specificationElement.getName()!=null)
	    			element.setName(specificationElement.getName());
	    	}
	    }

	    @Override public void undo() {
	    	unlinkElement(element);

	    	if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS)){
				Set<IVisualElement> allTraces = TraceLinkUtil.findAllTrace(element);
				for(IVisualElement e : allTraces){
					unlinkElement(e);
				}
			}
	    }

	    private void unlinkElement(IVisualElement element){
	    	if(element instanceof VisualSpecificationElement){
	    		((VisualSpecificationElement)element).setSpecificationElement(null);
		    	element.setName(oldName);
	    	}
	    }
}
