package dk.dtu.imm.red.visualmodeling.ui.specificationelement.usecase;

import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.action.Action;

import dk.dtu.imm.red.visualmodeling.ui.util.UsecasePointHelper;
import dk.dtu.imm.red.visualmodeling.ui.util.VisualModelHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class EffortAction extends Action {

	private IVisualElement diagram;
	private UsecasePointHelper helper;

	public EffortAction(IVisualElement diagram)
	{
		this.setText("Calculate Effort");
		this.diagram = diagram;
		this.helper = new UsecasePointHelper();
	}

	@Override
	public void run() {
		super.run();

		List<IVisualElement> leafs = VisualModelHelper.getLeafElements((IVisualElement)diagram);
		HashMap<IVisualElement,Double> values = new HashMap<IVisualElement,Double>();

		for(IVisualElement leaf : leafs)
		{
			if(leaf instanceof VisualSpecificationElement)
			{
				VisualSpecificationElement visualElement = (VisualSpecificationElement)leaf;
				if(visualElement.getSpecificationElement() != null)
				{
					helper.calculate(visualElement.getSpecificationElement());
					double point = helper.getUsecasePoints();
					values.put(leaf, point);
				}
			}

			test(leaf, values);
		}
	}

	private void test(IVisualElement element, HashMap<IVisualElement,Double> values)
	{
		// Get intrinsic point value
		double point = 0;

		if(element instanceof VisualSpecificationElement)
		{
			VisualSpecificationElement visualSpecElement = (VisualSpecificationElement)element;
			if(visualSpecElement.getSpecificationElement() != null)
			{
				helper.calculate(visualSpecElement.getSpecificationElement());
				point = helper.getUsecasePoints();
			}
		}

		// Assign intrinsic point value
		double p = values.get(element) != null ? values.get(element) :0.0;
		values.put(element, p + point);

		List<IVisualElement> connected = VisualModelHelper.getConnectedElementsDirectional(element);

		for(IVisualElement connectee : connected)
		{

			test(connectee, values);
		}
	}
}
