package dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;

import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.GoalKind;
import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.figures.VisualGoalLayerFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalPackage;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.VisualGoalLayer;

public class VisualGoalLayerEditPart extends VisualElementEditPart {

	private VisualGoalLayer goalLayer;

	private Adapter adapter;

	public VisualGoalLayerEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

		adapter = new GoalLayerAdapter();
	}

	@Override
	protected IFigure createFigure() {
		return new VisualGoalLayerFigure((IVisualElement) getModel());
	}

	@Override
	public void activate() {
		if (!isActive()) {
			((IVisualElement) getModel()).eAdapters().add(adapter);
		}
		super.activate();
	}

	@Override
	public void deactivate() {
		if (isActive()) {
			((IVisualElement) getModel()).eAdapters().remove(adapter);
		}

		super.deactivate();
	}

	@Override
	public void setModel(Object model) {
		super.setModel(model);
		goalLayer = (VisualGoalLayer) model;
	};

	@Override
	public void createContextMenu(IMenuManager menu) {

		MenuManager subMenu = new MenuManager("Change Kind", null);

		IAction action;

		final VisualGoalLayer goalLayer = (VisualGoalLayer) this.getModel();

		for (final GoalKind kind : GoalKind.values()) {
			action = new Action("Set " + kind.getName()) {

				@Override
				public void run() {
					super.run();

					goalLayer.setName(kind.getName());
					goalLayer.setLayerKind(kind);
				}

			};

			subMenu.add(action);
		}

		menu.add(subMenu);
	}

	/*
	 * Inner class for notifications
	 */
	public class GoalLayerAdapter implements Adapter {

		// TODO should happen in the EMF goal layer model

		public void notifyChanged(Notification notification) {

			if (notification.getEventType() == Notification.ADD) {

				IVisualElement element = (IVisualElement) notification
						.getNewValue();
				if (element instanceof VisualGoalElement) {
					Goal goal = (Goal) ((VisualGoalElement) element)
							.getSpecificationElement();
					if (goal != null) {

						goal.setElementKind(goalLayer.getLayerKind().getLiteral());
					}
				}
			} else if (notification.getEventType() == Notification.SET
					&& notification.getFeatureID(VisualGoalLayer.class) == GoalPackage.VISUAL_GOAL_LAYER__LAYER_KIND) {

				for(IVisualElement element : goalLayer.getElements())
				{
					if(element instanceof VisualGoalElement)
					{
						Goal goal = (Goal)((VisualGoalElement)element).getSpecificationElement();
						if(goal != null)
						{
							goal.setElementKind(((GoalKind)notification.getNewValue()).getLiteral());
						}
					}
				}
			}
		}

		public Notifier getTarget() {
			return (IVisualElement) getModel();
		}

		public void setTarget(Notifier newTarget) {
			// Do nothing.
		}

		public boolean isAdapterForType(Object type) {
			return type.equals(IVisualElement.class);
		}

	}
}
