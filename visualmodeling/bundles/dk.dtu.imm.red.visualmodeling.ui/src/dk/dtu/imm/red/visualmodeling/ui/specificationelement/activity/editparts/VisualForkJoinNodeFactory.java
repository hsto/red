package dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.activity.figures.VisualForkJoinNodeFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.ActivityFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualForkJoinNode;

public class VisualForkJoinNodeFactory implements CreationFactory {
	public Object getNewObject() {

		VisualForkJoinNode element = ActivityFactory.eINSTANCE.
				createVisualForkJoinNode();
		element.setBounds(VisualForkJoinNodeFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualForkJoinNode.class;
	}
}
