package dk.dtu.imm.red.visualmodeling.ui.factories;

import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.SelectionToolEntry;

import dk.dtu.imm.red.visualmodeling.ui.contracts.IPaletteFactory;

/*
 * Constructs the base PaletteRoot for generic diagrams.
 * Subclass to implement custom PaletteRoot for diagrams.
 * */
public abstract class PaletteFactoryBase implements IPaletteFactory {

	protected PaletteFactoryBase nextFactory;
	
	public final void populatePalette(PaletteRoot root) {
		
		addSelectionTool(root);
		
		populatePaletteNext(root);
	}
	
	/**
	 * Adds the selection group of tools to the palette.
	 * */
	private final void addSelectionTool(PaletteRoot root) {
		PaletteDrawer group = new PaletteDrawer("Tools");
		root.add(group);

		SelectionToolEntry entry = new SelectionToolEntry();
		group.add(entry);
		root.setDefaultEntry(entry);

		MarqueeToolEntry marqEntry = new MarqueeToolEntry();
		group.add(marqEntry);
	}

	/**
	 * Recursively loops through chained factories.
	 * @param root
	 */
	private final void populatePaletteNext(PaletteRoot root)
	{
		populatePaletteInternal(root);
		
		if(nextFactory != null)
		{
			nextFactory.populatePaletteNext(root);
		}
	}
	
	public final PaletteFactoryBase getNext()
	{
		return nextFactory;
	}

	/**
	 * Sets the next factory in the chain to try.
	 * @param nextfactory
	 */
	public final PaletteFactoryBase setNext(PaletteFactoryBase nextfactory)
	{
		this.nextFactory = nextfactory;
		return nextfactory;
	}
	
	/**
	 * Override to add to the palette.
	 * @param root
	 */
	protected abstract void populatePaletteInternal(PaletteRoot root);
}
