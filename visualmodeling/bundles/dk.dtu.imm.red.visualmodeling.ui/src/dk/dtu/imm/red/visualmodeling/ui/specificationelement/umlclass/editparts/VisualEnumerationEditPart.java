package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.VisualEnumSpecificationEditDialog;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures.VisualEnumerationFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

public class VisualEnumerationEditPart extends VisualElementEditPart {

	// TODO fix
	public VisualEnumerationEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);

	}

	@Override
	public void performRequest(Request req) {
		super.performRequest(req);
		if(req.getType() == RequestConstants.REQ_OPEN) {
			VisualEnumSpecificationEditDialog dialog = new
					VisualEnumSpecificationEditDialog(Display.getDefault().getActiveShell(), getModel(), this);
			dialog.open();
	    }

	}

//	@Override
//	protected void createEditPolicies() {
//		super.createEditPolicies();
//
//		// Remove these as we won't we needing them
//		removeEditPolicy(EditPolicy.CONTAINER_ROLE);
//	}

	@Override
	protected IFigure createFigure() {

		VisualEnumerationFigure figure = new VisualEnumerationFigure((IVisualElement) getModel());

		return figure;
	}

	@Override
	public void setLayoutConstraint(EditPart child, IFigure childFigure,
			Object constraint) {
		//do nothing
	}

//	@Override
//	protected void refreshVisuals() {
//		((VisualElementFigure) this.getFigure()).refreshVisuals();
//	}
}
