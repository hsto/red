package dk.dtu.imm.red.visualmodeling.ui.util;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionDirection;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;

public class VisualModelHelper {

	public static List<IVisualElement> getAllDecendents(IVisualElement element) {
		List<IVisualElement> list = new ArrayList<IVisualElement>();
		list.addAll(element.getElements());

		for (IVisualElement child : element.getElements()) {
			list.addAll(getAllDecendents(child));
		}

		return list;
	}

	public static List<SpecificationElement> getAllDecendentElements(
			IVisualElement parent) {
		List<IVisualElement> list = getAllDecendents(parent);
		final List<SpecificationElement> elementList = new ArrayList<SpecificationElement>();

		for (IVisualElement visualElement : list) {
			if (visualElement instanceof VisualSpecificationElement) {
				SpecificationElement element = ((VisualSpecificationElement) visualElement)
						.getSpecificationElement();
				if (element != null)
					elementList.add(element);
			}
		}

		return elementList;
	}
	
	// Added by @Luai
	public static List<Diagram> getAllDiagrams(Group workspace) {
		List<Diagram> diagrams = new ArrayList<Diagram>();
		List<Group> allGroups = getAllGroups(workspace);
		
		for (Group group : allGroups) {
			for (Element element : group.getContents()) {
				if (element instanceof Diagram) {
					diagrams.add((Diagram) element);
				}
			}
		}
		return diagrams;
	}
	
	// Added by @Luai
	private static List<Group> getAllGroups(Group element) {
		List<Group> list = new ArrayList<Group>();
		
		for (Element child : element.getContents()) {
			if (child instanceof Group) {
				list.add((Group) child);
				list.addAll(getAllGroups((Group) child));				
			}
		}
		return list;
	}

	public static List<IVisualElement> getConnectedElementsDirectional(IVisualElement element) {
		return getConnectedElementsDirectional(element, false);
	}

	public static List<IVisualElement> getConnectedElementsDirectional(IVisualElement element, boolean includeBidirectional) {
		List<IVisualElement> connectedElements = new ArrayList<IVisualElement>(5);

		for (IVisualConnection connection : element.getConnections()) {
			if (connection.getSource() == element && connection.getDirection() == ConnectionDirection.SOURCE_TARGET) {
				connectedElements.add(connection.getTarget());
			} else if (connection.getTarget() == element && connection.getDirection() == ConnectionDirection.TARGET_SOURCE) {
				connectedElements.add(connection.getSource());
			}else if(includeBidirectional && connection.getDirection() == ConnectionDirection.BIDIRECTIONAL)
			{
				if(connection.getTarget() == element)
				{
					connectedElements.add(connection.getSource());
				}else
				{
					connectedElements.add(connection.getTarget());
				}
			}
		}
		return connectedElements;
	}

	public static List<IVisualElement> getConnectedElements(
			IVisualElement element) {
		List<IVisualElement> connectedElements = new ArrayList<IVisualElement>(5);

		for (IVisualConnection connection : element.getConnections()) {
			if (connection.getSource() == element) {
				connectedElements.add(connection.getTarget());
			} else if (connection.getTarget() == element) {
				connectedElements.add(connection.getSource());
			}
		}
		return connectedElements;
	}

	public static List<IVisualElement> getLeafElements(IVisualElement root) {
		List<IVisualElement> allElements = getAllDecendents(root);
		List<IVisualElement> leafElements = new ArrayList<IVisualElement>(10);

		for (IVisualElement element : allElements) {
			boolean isLeaf = true;
			for (IVisualConnection connection : element.getConnections()) {
				if (connection.getDirection() == ConnectionDirection.SOURCE_TARGET
						&& connection.getSource() != element
						|| connection.getDirection() == ConnectionDirection.TARGET_SOURCE
						&& connection.getTarget() != element
						|| connection.getDirection() == ConnectionDirection.BIDIRECTIONAL
						|| connection.getDirection() == ConnectionDirection.NONE) {
					isLeaf = false;
				}
			}

			if (isLeaf)
				leafElements.add(element);
		}
		return leafElements;
	}
}
