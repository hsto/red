package dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.visualmodeling.ui.editparts.VisualElementEditPart;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.VisualClassSpecificationEditDialog;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.umlclass.figures.VisualClassFigure;
import dk.dtu.imm.red.visualmodeling.ui.validation.RuleCollection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

public class VisualClassEditPart extends VisualElementEditPart {
	
	private Adapter specificationElementNotifier;

	// TODO fix
	public VisualClassEditPart(RuleCollection ruleCollection) {
		super(ruleCollection);
	}

	@Override
	public void performRequest(Request req) {
		if(req.getType() == RequestConstants.REQ_OPEN) {
			if (this.getModel() == null) {
				return; 
			}
			else if (this.getModel() instanceof VisualSpecificationElement) {
				VisualSpecificationElement element = (VisualSpecificationElement) this.getModel();
				
				if (element.getSpecificationElement() == null) {
					VisualClassSpecificationEditDialog dialog = new
							VisualClassSpecificationEditDialog(Display.getDefault().getActiveShell(), getModel(), this);
					dialog.open();
				}
				else {
					this.openElement();
				}
			}
	    }

	}

//	@Override
//	protected void createEditPolicies() {
//		super.createEditPolicies();
//
//		// Remove these as we won't we needing them
//
//		removeEditPolicy(EditPolicy.CONTAINER_ROLE);
//	}

	@Override
	public void setModel(Object model) {
		super.setModel(model);
		
		// Setup notification system, so changes to a linked Signature object results in a graphics refresh
		if (this.getModel() != null && this.getModel() instanceof VisualSpecificationElement) {
			VisualSpecificationElement element = (VisualSpecificationElement) this.getModel();
			
			if (element.getSpecificationElement() != null) {
				addNotifierToEObject(element.getSpecificationElement());
			}
			
			element.eAdapters().add(new EContentAdapter() {
				@Override
				public void notifyChanged(Notification notification) {
					super.notifyChanged(notification);

					EReference specificationElementFeature = visualmodelPackage.eINSTANCE.getVisualSpecificationElement_SpecificationElement();
					
					if (notification.getFeature() != null && notification.getFeature().equals(specificationElementFeature)) {
						if (notification.getOldValue() != null) {
							((EObject)notification.getOldValue()).eAdapters().remove(specificationElementNotifier);
						}
						if (notification.getNewValue() != null) {
							
							addNotifierToEObject((EObject) notification.getNewValue());
						}
					}
				}
				
			});
		}
	}

	@Override
	protected IFigure createFigure() {

		VisualClassFigure figure = new VisualClassFigure((IVisualElement) getModel());

		return figure;
	}

	@Override
	public void setLayoutConstraint(EditPart child, IFigure childFigure,
			Object constraint) {
		//do nothing
	}
	
	private void addNotifierToEObject(EObject obj) {
		specificationElementNotifier = new EContentAdapter() {

			@Override
			public void notifyChanged(Notification notification) {
				super.notifyChanged(notification);
				
				if (notification.getEventType() == Notification.SET) {
					refreshVisuals();
				}
			}
		};
		
		obj.eAdapters().add(specificationElementNotifier);
	}
}
