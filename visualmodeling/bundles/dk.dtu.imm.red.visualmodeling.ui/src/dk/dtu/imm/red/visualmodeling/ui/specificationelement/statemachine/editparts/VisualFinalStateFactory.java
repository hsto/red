package dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.editparts;

import org.eclipse.gef.requests.CreationFactory;

import dk.dtu.imm.red.visualmodeling.ui.specificationelement.statemachine.figures.VisualFinalStateFigure;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.StatemachineFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualFinalState;

public class VisualFinalStateFactory implements CreationFactory {
	public Object getNewObject() {

		VisualFinalState element = StatemachineFactory.eINSTANCE.
				createVisualFinalState();
		element.setBounds(VisualFinalStateFigure.initialBounds);
		return element;
	}

	public Object getObjectType() {
		return VisualFinalState.class;
	}
}
