package dk.dtu.imm.red.visualmodeling.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemStructureDiagramKind;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.SystemstructureFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;



public class DiagramTest {
	Diagram diagram;
	VisualDiagram visualDiagram;

	@Before
	public void setUp() throws Exception {
		diagram = visualmodelFactory.eINSTANCE.createDiagram();
		visualDiagram = visualmodelFactory.eINSTANCE.createVisualDiagram();
	}

	@Test
	public void testPortNotPresent() {
		visualDiagram.setDiagramType(DiagramType.SYSTEM_STRUCTURE);
		diagram.setVisualDiagram(visualDiagram);
		diagram.setElementKind(SystemStructureDiagramKind.DOMAIN_ARCHITECTURE.getLiteral());
		VisualSystem system = SystemstructureFactory.eINSTANCE.createVisualSystem();
		VisualPort port = SystemstructureFactory.eINSTANCE.createVisualPort();
		
		visualDiagram.getElements().add(system);
		diagram.checkStructure();
		int numberOfCommentsWithoutIssue = diagram.getCommentlist().getComments().size();
		
		system.getElements().add(port);
		diagram.checkStructure();
		int numberOfCommentsWithIssue = diagram.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithoutIssue + 1, numberOfCommentsWithIssue);
	}

	@Test
	public void testPortNotPresentOtherDiagramType() {
		visualDiagram.setDiagramType(DiagramType.CLASS);
		diagram.setVisualDiagram(visualDiagram);
		VisualSystem system = SystemstructureFactory.eINSTANCE.createVisualSystem();
		VisualPort port = SystemstructureFactory.eINSTANCE.createVisualPort();
		
		visualDiagram.getElements().add(system);
		diagram.checkStructure();
		int numberOfCommentsWithoutIssue = diagram.getCommentlist().getComments().size();
		
		system.getElements().add(port);
		diagram.checkStructure();
		int numberOfCommentsWithIssue = diagram.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithoutIssue, numberOfCommentsWithIssue);
	}

	@Test
	public void testPortNotPresentOtherDiagramKind() {
		visualDiagram.setDiagramType(DiagramType.SYSTEM_STRUCTURE);
		diagram.setVisualDiagram(visualDiagram);
		diagram.setElementKind(SystemStructureDiagramKind.SYSTEM_CONTEXT.getLiteral());
		VisualSystem system = SystemstructureFactory.eINSTANCE.createVisualSystem();
		VisualPort port = SystemstructureFactory.eINSTANCE.createVisualPort();
		
		visualDiagram.getElements().add(system);
		diagram.checkStructure();
		int numberOfCommentsWithoutIssue = diagram.getCommentlist().getComments().size();
		
		system.getElements().add(port);
		diagram.checkStructure();
		int numberOfCommentsWithIssue = diagram.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithoutIssue, numberOfCommentsWithIssue);
	}
}


