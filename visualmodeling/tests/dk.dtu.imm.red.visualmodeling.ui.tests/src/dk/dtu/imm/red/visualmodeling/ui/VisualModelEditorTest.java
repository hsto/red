package dk.dtu.imm.red.visualmodeling.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.goal.GoalFactory;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementCreateCommand;
import dk.dtu.imm.red.visualmodeling.ui.editors.IVisualModelEditor;
import dk.dtu.imm.red.visualmodeling.ui.editors.impl.VisualModelEditorImpl;
import dk.dtu.imm.red.visualmodeling.ui.editors.impl.VisualModelEditorInputImpl;
import dk.dtu.imm.red.visualmodeling.ui.factories.VisualCreationFactory;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalElementFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelFactoryImpl;

@Ignore("Requires maintenance") 
public class VisualModelEditorTest {
	
	private Diagram diagram;
	private VisualModelEditorImpl editor;
	
	@Before
	public void setUp() throws Exception {
		VisualDiagram visualDiagram = visualmodelFactoryImpl.eINSTANCE.createVisualDiagram();
		visualDiagram.setDiagramType(DiagramType.GOAL);
		
		diagram = visualmodelFactoryImpl.eINSTANCE.createDiagram();
		diagram.setLabel("Test label");
		diagram.setName("Test name");
		diagram.setDescription("Test description");
		diagram.setVisualDiagram(visualDiagram);
		
		VisualModelEditorInputImpl editorInput = new VisualModelEditorInputImpl(diagram);
		
		editor = (VisualModelEditorImpl) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(editorInput, IVisualModelEditor.ID);
		
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testVisualModelEditorExists() {
		
		// Test editor exists
		assertNotNull(editor.getVisualEditor());
	}
	
	@Test
	public void testAddVisualElement() { // Adding one visual goal
		
		VisualGoalElementFactory factory = new VisualGoalElementFactory();
		VisualElement goal = (VisualElement) factory.getNewObject();
	
		// The diagram is empty to begin with
		assertTrue(diagram.getVisualDiagram().getElements().isEmpty());
		
		VisualElementCreateCommand command = new VisualElementCreateCommand(diagram.getVisualDiagram(), diagram.getVisualDiagram(), goal, new Point(1,1));
		command.execute();
		
		assertEquals(diagram.getVisualDiagram().getElements().size(), 1);
		IVisualElement addedGoal = diagram.getVisualDiagram().getElements().get(0);
		assertEquals(goal, addedGoal);
		assertEquals(diagram.getVisualDiagram(), addedGoal.getDiagram());
		assertEquals(diagram.getVisualDiagram(), addedGoal.getParent());		
		
		command.undo();
		assertTrue(diagram.getVisualDiagram().getElements().isEmpty());
	}
	
	@Test
	public void testAddVisualElementsFromSE() {
		
		SpecificationElement goal1 =  GoalFactory.eINSTANCE.createGoal();
		SpecificationElement goal2 =  GoalFactory.eINSTANCE.createGoal();
		goal1.setName("Goal 1");
		goal1.setLabel("Label 1");
		goal1.setDescription("Des 1");
		goal2.setName("Goal 2");
		goal2.setLabel("Label 2");
		goal2.setDescription("Des 2");
		List<SpecificationElement> goals =  new ArrayList<SpecificationElement>();
		goals.add(goal1);
		goals.add(goal2);
		
		VisualCreationFactory factory = new VisualCreationFactory();
		factory.setSpecificationElements(goals);
		
		
		
		
		VisualSpecificationElement visualGoal1, visualGoal2;
		List<Object> visualElements = (List<Object>) factory.getNewObject();
		visualGoal1 = (VisualSpecificationElement) visualElements.get(0);
		visualGoal2 = (VisualSpecificationElement) visualElements.get(1);
		assertEquals(goal1, visualGoal1.getSpecificationElement());
		assertEquals(goal2, visualGoal2.getSpecificationElement());
		assertEquals(Goal.class, visualGoal1.getSpecificationElementType());
		assertEquals(Goal.class, visualGoal2.getSpecificationElementType());
		
		
		assertTrue(diagram.getVisualDiagram().getElements().isEmpty());
		VisualElementCreateCommand command1 = new VisualElementCreateCommand(diagram.getVisualDiagram(), diagram.getVisualDiagram(), visualGoal1, new Point(1,1));
		VisualElementCreateCommand command2 = new VisualElementCreateCommand(diagram.getVisualDiagram(), diagram.getVisualDiagram(), visualGoal2, new Point(1,1));
		command1.execute();
		command2.execute();
		
		assertEquals(diagram.getVisualDiagram().getElements().size(), 2);
		IVisualElement addedGoal1 = diagram.getVisualDiagram().getElements().get(0);
		IVisualElement addedGoal2 = diagram.getVisualDiagram().getElements().get(1);
		assertEquals(visualGoal1, addedGoal1);
		assertEquals(visualGoal2, addedGoal2);
		assertEquals(diagram.getVisualDiagram(), addedGoal1.getDiagram());
		assertEquals(diagram.getVisualDiagram(), addedGoal1.getParent());
		assertEquals(diagram.getVisualDiagram(), addedGoal2.getDiagram());
		assertEquals(diagram.getVisualDiagram(), addedGoal2.getParent());
		assertEquals(goal1, ((VisualSpecificationElement) addedGoal1).getSpecificationElement());
		assertEquals(goal2, ((VisualSpecificationElement) addedGoal2).getSpecificationElement());
		
		command1.undo();
		command2.undo();
		assertTrue(diagram.getVisualDiagram().getElements().isEmpty());
	}

}
