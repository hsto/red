package dk.dtu.imm.red.visualmodeling.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.visualmodeling.ui.commands.VisualElementCreateCommand;
import dk.dtu.imm.red.visualmodeling.ui.editors.IVisualModelEditor;
import dk.dtu.imm.red.visualmodeling.ui.editors.impl.VisualModelEditorImpl;
import dk.dtu.imm.red.visualmodeling.ui.editors.impl.VisualModelEditorInputImpl;
import dk.dtu.imm.red.visualmodeling.ui.specificationelement.goal.editparts.VisualGoalElementFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelFactoryImpl;

@Ignore("Requires maintenance") 
public class CreateElementsFromDiagramTest {
	
	private Diagram diagram;
	private VisualModelEditorImpl editor;
	
	@Before
	public void setUp() throws Exception {
		VisualDiagram visualDiagram = visualmodelFactoryImpl.eINSTANCE.createVisualDiagram();
		visualDiagram.setDiagramType(DiagramType.GOAL);
		
		diagram = visualmodelFactoryImpl.eINSTANCE.createDiagram();
		diagram.setLabel("Test label");
		diagram.setName("Test name");
		diagram.setDescription("Test description");
		diagram.setVisualDiagram(visualDiagram);
		
		VisualModelEditorInputImpl editorInput = new VisualModelEditorInputImpl(diagram);
		
		editor = (VisualModelEditorImpl) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(editorInput, IVisualModelEditor.ID);
		
		assertNotNull(editor.getVisualEditor());
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void test() {
		VisualGoalElementFactory factory = new VisualGoalElementFactory();
		VisualElement visualGoal = (VisualElement) factory.getNewObject();
		visualGoal.setName("Goal");
		
		new VisualElementCreateCommand(diagram.getVisualDiagram(), diagram.getVisualDiagram(), visualGoal, new Point(1,1)).execute();
		assertEquals(diagram.getVisualDiagram().getElements().size(), 1);
		VisualSpecificationElement addedGoal = (VisualSpecificationElement) diagram.getVisualDiagram().getElements().get(0);
		
		Group parent = FileFactory.eINSTANCE.createFile();
		
		SpecificationElement goal = addedGoal.createSpecificationElement(parent);
		assertEquals(visualGoal.getName(), goal.getName());
		assertEquals(visualGoal.getName(), goal.getLabel());
		assertEquals("", goal.getDescription());
		assertEquals(parent, goal.getParent());
		
	}

}
