<?xml version="1.0" encoding="UTF-8"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:activity="dk.dtu.imm.red.visualmodeling.visualmodel.activity" xmlns:class="dk.dtu.imm.red.visualmodeling.visualmodel.class" xmlns:comment="dk.dtu.imm.red.core.comment" xmlns:datatype="dk.dtu.imm.red.specificationelements.modelelement.datatype" xmlns:file="dk.dtu.imm.red.core.file" xmlns:folder="dk.dtu.imm.red.core.folder" xmlns:multiplicity="dk.dtu.imm.red.specificationelements.modelelement.multiplicity" xmlns:requirement="dk.dtu.imm.red.specificationelements.requirement" xmlns:statemachine="dk.dtu.imm.red.visualmodeling.visualmodel.statemachine" xmlns:text="dk.dtu.imm.red.core.text" xmlns:usecase="dk.dtu.imm.red.visualmodeling.visualmodel.usecase" xmlns:visualmodel="dk.dtu.imm.red.visualmodeling" name="REQ.red" timeCreated="2014-09-25T11:11:09.891+0200" lastModified="2017-01-17T19:34:42.872+0100" uniqueID="ae8367e6-e6c2-4107-b296-b0a704cc943e" license="Apache 2.0 (c) Harald Stoerrle">
  <commentlist>
    <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:32.323+0100" uniqueID="18d4ca70-530d-4840-b36c-b780e5c73958" severity="Warning">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <text>
        <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
      </text>
    </comments>
  </commentlist>
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="folder:Folder" name="Requirements" timeCreated="2011-11-18T10:07:23.270+0100" lastModified="2017-01-17T19:34:42.872+0100" uniqueID="3bcd6f72-901a-462c-a81b-7529569c3271">
    <commentlist>
      <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:34.872+0100" uniqueID="a67b0e80-9088-4a19-ad2d-12e8b1923d0c" severity="Warning">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <text>
          <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
        </text>
      </comments>
    </commentlist>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="folder:Folder" name="Corpus" timeCreated="2011-12-09T15:09:23.409+0100" lastModified="2017-01-17T19:34:35.988+0100" uniqueID="5ddef5ac-6045-47b0-958f-ef7fa895ddcd">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.249+0100" uniqueID="7ccc6279-2810-4687-b19f-4ccc38221f6c" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Medium Life Cycle" timeCreated="2011-12-07T15:29:54.893+0100" lastModified="2017-01-17T19:34:35.664+0100" uniqueID="01a489a4-c58a-4b57-aa27-9f16534fd98b">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.453+0100" uniqueID="55eca880-6444-4784-ad19-3993cc6dc8ad" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="" name="MLC1" elementKind="" description="Media follow a defined lifecycle from suggested, via acquired, incorporated, to removed." timeCreated="2011-11-25T15:32:46.357+0100" lastModified="2017-01-17T19:34:35.464+0100" uniqueID="d9c4ff89-f5ad-472c-8006-78bc1a090776" workPackage="" abstractionLevel="" id="MLC1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.463+0100" uniqueID="4c6a6279-95ac-49b5-98bc-467de3c79bb7" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-11-25T15:43:01.465+0100" uniqueID="7d3559f9-bce0-4df9-94e5-d698c2326392">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-11-25T15:43:01.469+0100" uniqueID="5bf9d520-c1cf-40bd-879d-ebea4723fbac">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC1sm1" name="MLC1sm1" elementKind="unspecified" description="" timeCreated="2015-04-18T21:25:13.074+0200" lastModified="2017-01-17T19:34:35.474+0100" uniqueID="776f219a-47bd-436b-994a-36fb59b812b3" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.473+0100" uniqueID="9d3afd92-55a3-495c-8d7c-5fdf6d1842cd" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-18T21:29:23.785+0200" uniqueID="80d46aa0-a601-46eb-b6ce-ba8a377f9805">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-18T21:29:23.788+0200" uniqueID="a04a2c03-c739-4710-bfed-f52d9863faf8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram">
            <Elements xsi:type="statemachine:VisualInitialState" Location="25,115" Bounds="30,30" Parent="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" visualID="_9YYV4P5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="129,111" Bounds="109,36" Parent="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="suggested" visualID="_9YY88P5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="117,190" Bounds="105,35" Parent="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="acquired" visualID="_9YY88f5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="118,264" Bounds="97,30" Parent="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.3 //@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.5" IsSketchy="true" Name="incorporated" visualID="_9YY88v5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="127,327" Bounds="90,26" Parent="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.3 //@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.4" IsSketchy="true" Name="removed" visualID="_9YY88_5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualFinalState" Location="80,365" Bounds="30,30" Parent="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram/@DiagramConnections.4" IsSketchy="true" visualID="_9YY89P5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="visualmodel:VisualGenericElement" Location="99,46" Bounds="50,50" Parent="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.1/@VisualDiagram" IsSketchy="true" Name="Medium" visualID="_9YZkAP5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YYV4P5YEeSzkfJMf5_daA" Target="_9YY88P5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="suggest" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YY88P5YEeSzkfJMf5_daA" Target="_9YY88f5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="acquire" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YY88f5YEeSzkfJMf5_daA" Target="_9YY88v5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="catalog" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YY88v5YEeSzkfJMf5_daA" Target="_9YY88_5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="remove" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YY88_5YEeSzkfJMf5_daA" Target="_9YY89P5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YY88v5YEeSzkfJMf5_daA" Target="_9YY88v5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="restrict" Type="StateTransition">
              <Bendpoints>259,261</Bendpoints>
              <Bendpoints>258,286</Bendpoints>
              <Bendpoints>253,308</Bendpoints>
            </DiagramConnections>
          </VisualDiagram>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC1sm2" name="MLC1sm2" elementKind="unspecified" description="" timeCreated="2015-04-19T12:21:03.560+0200" lastModified="2017-01-17T19:34:35.488+0100" uniqueID="c793f40c-7646-4762-a958-9b23678f65bb" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.487+0100" uniqueID="b4eddbc3-64c9-4ded-b5a3-fea64908e7e6" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T12:22:11.356+0200" uniqueID="42324963-9d00-42d5-a67a-95347e0f51e9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T12:22:11.358+0200" uniqueID="ca988114-67f9-485c-b014-b026e99a3a06">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram">
            <Elements xsi:type="statemachine:VisualState" Location="105,44" Bounds="276,303" Parent="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.5 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.6 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.7 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.8 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.9 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.10" IsSketchy="true" Name="Available" visualID="_9YayIP5YEeSzkfJMf5_daA">
              <Elements xsi:type="statemachine:VisualState" Location="78,77" Bounds="86,34" Parent="_9YayIP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="ready" visualID="_9YbZMP5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="statemachine:VisualState" Location="73,176" Bounds="87,29" Parent="_9YayIP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.3 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.4" IsSketchy="true" Name="lease" visualID="_9YbZMf5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="statemachine:VisualInitialState" Location="30,28" Bounds="30,30" Parent="_9YayIP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" visualID="_9YbZMv5YEeSzkfJMf5_daA"/>
            </Elements>
            <Elements xsi:type="statemachine:VisualInitialState" Location="33,62" Bounds="30,30" Parent="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" visualID="_9YbZM_5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="480,69" Bounds="247,312" Parent="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" IsSketchy="true" Name="Unavalailable" visualID="_9YbZNP5YEeSzkfJMf5_daA">
              <Elements xsi:type="statemachine:VisualState" Location="43,33" Bounds="86,27" Parent="_9YbZNP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.5 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.6" IsSketchy="true" Name="lost" visualID="_9YbZNf5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="statemachine:VisualState" Location="93,161" Bounds="93,28" Parent="_9YbZNP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.7 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.8 //@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.9" IsSketchy="true" Name="damaged" visualID="_9YcAQP5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="statemachine:VisualState" Location="35,248" Bounds="96,27" Parent="_9YbZNP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.10" IsSketchy="true" Name="removed" visualID="_9YcAQf5YEeSzkfJMf5_daA"/>
            </Elements>
            <Elements xsi:type="visualmodel:VisualGenericElement" Location="397,16" Bounds="50,50" Parent="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.2/@VisualDiagram" IsSketchy="true" Name="Copy" visualID="_9YcAQv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YbZM_5YEeSzkfJMf5_daA" Target="_9YayIP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="obtain" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YbZMv5YEeSzkfJMf5_daA" Target="_9YbZMP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YbZMP5YEeSzkfJMf5_daA" Target="_9YbZMf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="lease" Type="StateTransition">
              <Bendpoints>281,188</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YbZMf5YEeSzkfJMf5_daA" Target="_9YbZMP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="return" Type="StateTransition">
              <Bendpoints>185,178</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YbZMf5YEeSzkfJMf5_daA" Target="_9YbZMf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="prolong" Type="StateTransition">
              <Bendpoints>332,249</Bendpoints>
              <Bendpoints>303,291</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YayIP5YEeSzkfJMf5_daA" Target="_9YbZNf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="lose" Type="StateTransition">
              <Bendpoints>426,101</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YbZNf5YEeSzkfJMf5_daA" Target="_9YayIP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="find" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YayIP5YEeSzkfJMf5_daA" Target="_9YcAQP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="damage" Type="StateTransition">
              <Bendpoints>502,186</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YcAQP5YEeSzkfJMf5_daA" Target="_9YayIP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="replace" Type="StateTransition">
              <Bendpoints>496,212</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YcAQP5YEeSzkfJMf5_daA" Target="_9YayIP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="restore" Type="StateTransition">
              <Bendpoints>470,244</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YayIP5YEeSzkfJMf5_daA" Target="_9YcAQf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="remove" Type="StateTransition"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="" name="MLC2" elementKind="" description="Librarians may add, update, and delete corpus items manually." timeCreated="2011-11-25T15:47:54.636+0100" lastModified="2017-01-17T19:34:35.499+0100" uniqueID="ce4a3259-f77d-4d12-93ed-5319436eff22" workPackage="" abstractionLevel="Activity" id="MLC2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.498+0100" uniqueID="8fbe5778-0520-40ab-be96-ccd057bb4f4d" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-11-25T15:48:46.130+0100" uniqueID="07c330a5-6045-425b-844b-d886db1381ea">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-11-25T15:48:46.151+0100" uniqueID="345b21e8-1b21-4375-bdf6-542ff72a5666">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC2uc" name="MLC2uc" elementKind="unspecified" description="" timeCreated="2015-04-18T21:58:37.816+0200" lastModified="2017-01-17T19:34:35.509+0100" uniqueID="7e0606b2-1970-49b5-b267-ee11cfa98aa4" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.508+0100" uniqueID="05314b74-2954-4fb6-96db-838d0644b53e" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-18T21:59:59.029+0200" uniqueID="ec3a9a62-c104-4861-828c-1c7b8b30306a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-18T21:59:59.029+0200" uniqueID="2e33a353-3ada-4808-812e-613cc90fc562">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="127,158" Bounds="80,150" Parent="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram/@DiagramConnections.2" Name="The New Librarian" visualID="_9YdOYP5YEeSzkfJMf5_daA" IsLinkedToElement="true"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="304,143" Bounds="150,50" Parent="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram/@DiagramConnections.0" Name="Add Corpus Item" visualID="_9Yd1cP5YEeSzkfJMf5_daA" IsLinkedToElement="true"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="316,207" Bounds="150,50" Parent="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="update corpus item" visualID="_9Yd1cf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="324,269" Bounds="150,50" Parent="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.4/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="delete corpus item" visualID="_9Yd1cv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YdOYP5YEeSzkfJMf5_daA" Target="_9Yd1cP5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YdOYP5YEeSzkfJMf5_daA" Target="_9Yd1cf5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YdOYP5YEeSzkfJMf5_daA" Target="_9Yd1cv5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MLC3" name="MLC3" elementKind="" description="On creating a new catalog item, the LMS creates a new globally unique identifier for the item." timeCreated="2011-12-07T14:08:12.923+0100" lastModified="2017-01-17T19:34:35.521+0100" uniqueID="6bb0c7ff-3583-44df-92ea-5bc909682c56" workPackage="" abstractionLevel="" id="MLC3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.520+0100" uniqueID="64fa17a4-9340-4f61-838c-7cb503f91c9e" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-07T14:09:42.289+0100" uniqueID="44a1236b-7a64-463f-a4bb-42844f1992aa">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T14:09:42.291+0100" uniqueID="45eb942e-b19c-4bf2-90a9-2f8599911f5f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC3cd" name="MLC3cd" elementKind="unspecified" description="" timeCreated="2015-04-19T13:00:07.005+0200" lastModified="2017-01-17T19:34:35.531+0100" uniqueID="666c1279-ed48-4483-aad9-b54092f6b616" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.530+0100" uniqueID="5b6159ae-6f9d-4ede-8f6a-83b436ffb4c3" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T13:00:44.816+0200" uniqueID="38a23bf0-9786-4958-88b7-af88638935e2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T13:00:44.817+0200" uniqueID="2618da37-05e7-4ffb-91c5-5f599a723068">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.6/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="192,104" Bounds="100,58" Parent="//@contents.0/@contents.0/@contents.0/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.6/@VisualDiagram" IsSketchy="true" Name="Catalog item" visualID="_9YfDkP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9YfDkP5YEeSzkfJMf5_daA" Name="catalogID" visualID="_9YfqoP5YEeSzkfJMf5_daA" ReadOnly="true">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MLC4" name="MLC4" elementKind="" description="Librarians and Readers may post and inspect media they think should be acquired by the library to a public “wish list” indicating the status of the wish and the originator. " timeCreated="2011-12-07T14:40:09.009+0100" lastModified="2017-01-17T19:34:35.541+0100" uniqueID="aee159a9-3461-4739-a52f-5e7ef25bcb4c" workPackage="" abstractionLevel="" id="MLC4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.540+0100" uniqueID="c09ae87a-d4bf-48c4-a8cf-3923e2cd2fb9" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-07T14:42:10.846+0100" uniqueID="c98012cd-c341-49e4-8fe9-a153222f93a8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T14:42:10.847+0100" uniqueID="a21f83cf-0973-4edc-a92f-17539490055c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC4uc" name="MLC4uc" elementKind="unspecified" description="" timeCreated="2015-04-19T14:41:30.121+0200" lastModified="2017-01-17T19:34:35.551+0100" uniqueID="9267e7d1-58cb-4d76-ba2b-f80f5b6b45ae" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.550+0100" uniqueID="49af9ea7-b22f-4842-aab1-1ad4df273c4f" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T14:42:05.482+0200" uniqueID="329606cf-0141-43b1-9c97-ae68713c084d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T14:42:05.484+0200" uniqueID="d16887f8-215f-468c-94aa-2d0649c09487">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="125,105" Bounds="80,150" Parent="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="reader" visualID="_9YgRsP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="276,116" Bounds="200,200" Parent="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" IsSketchy="true" Name="Acquisitions" visualID="_9YgRsf5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="33,61" Bounds="150,50" Parent="_9YgRsf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="suggest acquisitions" visualID="_9YgRsv5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="24,121" Bounds="150,50" Parent="_9YgRsf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="comment suggestion" visualID="_9YgRs_5YEeSzkfJMf5_daA"/>
            </Elements>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="275,338" Bounds="200,200" Parent="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" IsSketchy="true" Name="wishlist" visualID="_9YgRtP5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="20,52" Bounds="150,50" Parent="_9YgRtP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.4" IsSketchy="true" Name="suggest acquisition" visualID="_9YgRtf5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="28,116" Bounds="150,50" Parent="_9YgRtP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.3 //@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.5" IsSketchy="true" Name="inspect suggestion" visualID="_9Yg4wP5YEeSzkfJMf5_daA"/>
            </Elements>
            <Elements xsi:type="usecase:VisualActorElement" Location="496,362" Bounds="80,150" Parent="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.4 //@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.5" IsSketchy="true" Name="librarian" visualID="_9Yg4wf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualActorElement" Location="136,369" Bounds="80,150" Parent="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.0/@contents.0/@contents.8/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="reader" visualID="_9Yg4wv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YgRsP5YEeSzkfJMf5_daA" Target="_9YgRsv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YgRsP5YEeSzkfJMf5_daA" Target="_9YgRs_5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yg4wv5YEeSzkfJMf5_daA" Target="_9YgRtf5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yg4wv5YEeSzkfJMf5_daA" Target="_9Yg4wP5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yg4wf5YEeSzkfJMf5_daA" Target="_9YgRtf5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yg4wf5YEeSzkfJMf5_daA" Target="_9Yg4wP5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC4cl" name="MLC4cl" elementKind="unspecified" description="" timeCreated="2015-04-19T14:47:30.059+0200" lastModified="2017-01-17T19:34:35.561+0100" uniqueID="1dd8ecd4-1c6a-4f47-8f35-4d55e8f17561" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.561+0100" uniqueID="ecab8823-b441-4060-9dfa-15d714b567e2" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T14:48:36.788+0200" uniqueID="f52fe3ef-4c79-4758-9b33-f29cd26d0987">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T14:48:36.789+0200" uniqueID="49f58963-551b-4ba2-94d2-90859cf6000a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="49,191" Bounds="104,41" Parent="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Wishlist" visualID="_9Yhf0P5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="class:VisualClassElement" Location="203,163" Bounds="200,112" Parent="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="Wish" visualID="_9Yhf0f5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Yhf0f5YEeSzkfJMf5_daA" Name="originator" visualID="_9Yhf0v5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Yhf0f5YEeSzkfJMf5_daA" Name="medium description" visualID="_9Yhf0_5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Yhf0f5YEeSzkfJMf5_daA" Name="justification" visualID="_9Yhf1P5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Yhf0f5YEeSzkfJMf5_daA" Name="status" visualID="_9Yhf1f5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
            <Elements xsi:type="class:VisualClassElement" Location="449,166" Bounds="200,89" Parent="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.9/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="Comment" visualID="_9Yhf1v5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Yhf1v5YEeSzkfJMf5_daA" Name="originator" visualID="_9Yhf1_5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Yhf1v5YEeSzkfJMf5_daA" Name="body" visualID="_9YiG4P5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Yhf1v5YEeSzkfJMf5_daA" Name="support" visualID="_9YiG4f5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yhf0P5YEeSzkfJMf5_daA" Target="_9Yhf0f5YEeSzkfJMf5_daA" Direction="SourceTarget" SourceDecoration="DiamondBlack" Name="" TargetMultiplicity="*" Type="Composition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yhf0f5YEeSzkfJMf5_daA" Target="_9Yhf1v5YEeSzkfJMf5_daA" Direction="SourceTarget" SourceDecoration="DiamondBlack" Name="" TargetMultiplicity="*" Type="Composition"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MLC5" name="MLC5" elementKind="" description="Postings to the wishlist may be publicly commented by other users" timeCreated="2011-12-07T15:26:25.726+0100" lastModified="2017-01-17T19:34:35.571+0100" uniqueID="244af1d4-32b7-4964-9514-74b278b06777" workPackage="" abstractionLevel="" id="m">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.571+0100" uniqueID="5ecaaafc-a042-4fc7-bbe7-5bb8c99f0393" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-07T15:28:56.415+0100" uniqueID="ce673a9a-40c2-4aa7-83a1-0ec0c0a9cd84">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T15:28:56.417+0100" uniqueID="08c52fb2-2500-4b0c-a10a-baebedb7e7c2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MLC6" name="MLC6" elementKind="" description="Librarians may remove or deactivate entries to the wishlist." timeCreated="2011-12-07T15:30:34.012+0100" lastModified="2017-01-17T19:34:35.581+0100" uniqueID="767854c6-e241-467f-9032-b33b7ec589db" workPackage="" abstractionLevel="" id="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.580+0100" uniqueID="d6c17fe3-0e60-454d-9e3d-60969856d49d" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-07T15:31:47.414+0100" uniqueID="c8df4845-5164-4ae2-8289-f456f32e7f4b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T15:31:47.415+0100" uniqueID="81b5812a-551a-44ca-9052-b8c853c497a8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC6uc" name="MLC6uc" elementKind="unspecified" description="" timeCreated="2015-04-19T14:50:52.755+0200" lastModified="2017-01-17T19:34:35.592+0100" uniqueID="ce9081ac-fd22-44c6-afdf-bb87fd61ba18" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.591+0100" uniqueID="6cb39c54-fc7c-4435-a13e-3713a9795f78" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T14:51:35.903+0200" uniqueID="efd1fafc-f649-444b-bac3-857f0b61bdb8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T14:51:35.904+0200" uniqueID="29b79038-49a2-4393-b5d2-008190956db7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="98,178" Bounds="80,150" Parent="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="Librarian" visualID="_9Yit8P5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="226,95" Bounds="322,297" Parent="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram" IsSketchy="true" Name="Acquisitions" visualID="_9YjVAP5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="46,56" Bounds="150,50" Parent="_9YjVAP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="process suggestion" visualID="_9YjVAf5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="43,132" Bounds="150,50" Parent="_9YjVAP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="close suggestion" visualID="_9YjVAv5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="70,225" Bounds="150,50" Parent="_9YjVAP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.12/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="acquire copy" visualID="_9YjVA_5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yit8P5YEeSzkfJMf5_daA" Target="_9YjVAf5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yit8P5YEeSzkfJMf5_daA" Target="_9YjVAv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yit8P5YEeSzkfJMf5_daA" Target="_9YjVA_5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MLC7" name="MLC7" elementKind="" description="If a suggestion changes its state (see previous requirement), the user who originally posted it is notified electronically." timeCreated="2011-12-07T15:33:47.454+0100" lastModified="2017-01-17T19:34:35.602+0100" uniqueID="9ca79e2b-b450-48a4-a184-257dc57205a7" workPackage="" abstractionLevel="" id="MLC7">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.601+0100" uniqueID="93b8e019-60a1-4481-846a-0c15745a9d99" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-07T15:34:56.496+0100" uniqueID="50999170-30bb-42f9-870e-7aed28382107">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T15:34:56.497+0100" uniqueID="2f5f0aef-e7a8-47dd-a8fa-c01638035732">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC7sm" name="MLC7sm" elementKind="unspecified" description="" timeCreated="2015-04-19T14:52:14.005+0200" lastModified="2017-01-17T19:34:35.612+0100" uniqueID="bb36d4ec-9bdc-41ab-a3fb-d8f22ad24be2" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.611+0100" uniqueID="d5f8ab42-65b4-4c69-b32f-86d331f596ce" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T14:54:35.028+0200" uniqueID="fe0e9427-82c4-4165-89a7-76086fd92138">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T14:54:35.030+0200" uniqueID="ba465984-bf60-49ad-9e9e-a9e38b66e99b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram">
            <Elements xsi:type="statemachine:VisualState" Location="194,145" Bounds="84,31" Parent="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.5 //@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.7" IsSketchy="true" Name="proposed" visualID="_9Yj8EP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualInitialState" Location="106,137" Bounds="30,30" Parent="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" visualID="_9Yj8Ef5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualForkJoinState" Location="386,132" Bounds="8,125" Parent="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" visualID="_9YkjIP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="466,130" Bounds="98,24" Parent="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="notifying" visualID="_9YkjIf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="497,215" Bounds="85,27" Parent="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.3 //@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.4" IsSketchy="true" Name="closed" visualID="_9YkjIv5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="263,312" Bounds="85,26" Parent="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.5 //@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.6" IsSketchy="true" Name="pending" visualID="_9YkjI_5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualFinalState" Location="424,301" Bounds="30,30" Parent="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.4 //@contents.0/@contents.0/@contents.0/@contents.14/@VisualDiagram/@DiagramConnections.6" IsSketchy="true" visualID="_9YkjJP5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yj8Ef5YEeSzkfJMf5_daA" Target="_9Yj8EP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="propose" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yj8EP5YEeSzkfJMf5_daA" Target="_9YkjIP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="reject" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YkjIP5YEeSzkfJMf5_daA" Target="_9YkjIf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YkjIP5YEeSzkfJMf5_daA" Target="_9YkjIv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YkjIv5YEeSzkfJMf5_daA" Target="_9YkjJP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yj8EP5YEeSzkfJMf5_daA" Target="_9YkjI_5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="accept" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YkjI_5YEeSzkfJMf5_daA" Target="_9YkjJP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yj8EP5YEeSzkfJMf5_daA" Target="_9Yj8EP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="comment" Type="StateTransition">
              <Bendpoints>249,110</Bendpoints>
              <Bendpoints>294,120</Bendpoints>
            </DiagramConnections>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MLC8" name="MLC8" elementKind="" description="Guests have the capabilities to use the catalog except the features requiring personalization." timeCreated="2011-12-07T15:37:15.353+0100" lastModified="2017-01-17T19:34:35.622+0100" uniqueID="3ccddefb-2f1c-4d0c-a2ed-b305789a4947" workPackage="" abstractionLevel="" id="MLC8">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.622+0100" uniqueID="89168af6-c2f2-445a-b90b-ce7f0333480e" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:23:34.669+0100" uniqueID="d6814062-c12c-4be8-89db-d03c5caaca98">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:23:34.672+0100" uniqueID="7a2575df-070e-4a9d-80d4-4131d596da9c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MLC9" name="MLC9" elementKind="" description="Guest readers may inspect suggestions." timeCreated="2011-12-09T09:24:02.147+0100" lastModified="2017-01-17T19:34:35.632+0100" uniqueID="fc938ff1-a668-4c7a-bd63-5632cb32c859" workPackage="" abstractionLevel="" id="MLC9">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.632+0100" uniqueID="d8e34dbd-1a1f-446e-9c51-0a32b80fcbd0" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:24:31.211+0100" uniqueID="aa71aafd-c452-4511-a76c-27bbc49136b5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:24:31.218+0100" uniqueID="c9a1ba3a-2686-4d16-bf1c-c7e5ba3c1acf">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC9uc" name="MLC9uc" elementKind="unspecified" description="" timeCreated="2015-04-19T14:55:15.422+0200" lastModified="2017-01-17T19:34:35.642+0100" uniqueID="4531cf51-2a82-4650-ade4-c1abe9a5148c" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.642+0100" uniqueID="f22ad3d4-7a53-497f-85bb-470dbd075835" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T14:55:45.119+0200" uniqueID="312fad4c-fa9a-482d-a0dc-65483eaaef91">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T14:55:45.120+0200" uniqueID="24758eab-6edf-4103-a0bf-107ae8f3d526">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.17/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="54,74" Bounds="80,150" Parent="//@contents.0/@contents.0/@contents.0/@contents.17/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.17/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.17/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Guest" visualID="_9YlxQP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="180,63" Bounds="200,125" Parent="//@contents.0/@contents.0/@contents.0/@contents.17/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.17/@VisualDiagram" IsSketchy="true" Name="wishlist" visualID="_9YlxQf5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="26,39" Bounds="150,50" Parent="_9YlxQf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.0/@contents.0/@contents.17/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.17/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="inspect suggestion" visualID="_9YlxQv5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YlxQP5YEeSzkfJMf5_daA" Target="_9YlxQv5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MLC10" name="MLC10" elementKind="" description="A librarian can do all a reader can do; a reader can do all a guest reader can do." timeCreated="2011-12-09T09:24:46.293+0100" lastModified="2017-01-17T19:34:35.653+0100" uniqueID="a0c43a61-f5df-43f7-bdaf-e75f2e8eaf64" workPackage="" abstractionLevel="" id="MLC10">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.652+0100" uniqueID="4f980c98-2ec4-4337-ad40-3713dc5c71df" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:26:31.981+0100" uniqueID="86745771-f300-489f-82ec-e276a308c4e2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:26:31.986+0100" uniqueID="7dcc03ae-d530-4ffa-964a-0115115f18b9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="MLC10uc" name="MLC10uc" elementKind="unspecified" description="" timeCreated="2015-04-19T14:56:12.288+0200" lastModified="2017-01-17T19:34:35.664+0100" uniqueID="8de7eab6-665d-4e3f-9052-0644d602079c" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.663+0100" uniqueID="b1dad53b-ccc4-42ec-8397-e9a836d3348a" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T14:56:48.989+0200" uniqueID="4b7e7ee3-4af8-4613-b49e-f5d53df04b3d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T14:56:48.990+0200" uniqueID="fb69a05b-2127-4612-8420-77f717f7f600">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="72,74" Bounds="80,150" Parent="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Guest" visualID="_9YnmcP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualActorElement" Location="243,76" Bounds="80,150" Parent="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="Reader" visualID="_9Ynmcf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualActorElement" Location="426,74" Bounds="80,150" Parent="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.0/@contents.19/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="Librarian" visualID="_9Ynmcv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Ynmcf5YEeSzkfJMf5_daA" Target="_9YnmcP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHeadSolidWhite" Name="" Type="Generalization"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Ynmcv5YEeSzkfJMf5_daA" Target="_9Ynmcf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHeadSolidWhite" Name="" Type="Generalization"/>
          </VisualDiagram>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Advanced Medium Delivery Service" timeCreated="2011-12-07T15:38:10.730+0100" lastModified="2017-01-17T19:34:35.877+0100" uniqueID="7298855b-607f-4624-9a76-5a389f0a7cc3">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.776+0100" uniqueID="8dcfc058-42d0-4d0e-8215-8b4ec2b8dbd6" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="AMDS1" name="AMDS1" elementKind="" description="The AMDS delivers both electronic and physical media across both physical and online channels." timeCreated="2011-12-09T09:32:05.986+0100" lastModified="2017-01-17T19:34:35.786+0100" uniqueID="2ee06d2f-380f-4623-a0e5-1647923f7629" workPackage="" abstractionLevel="" id="AMDS1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.786+0100" uniqueID="6727b8f9-1585-4273-8279-14326fb687e4" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:32:41.948+0100" uniqueID="f0a7312e-1db3-48c0-a6b1-1e7d77442576">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:32:41.949+0100" uniqueID="7f4feaa0-3287-4133-aa35-b93009c1b483">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="AMDS1cl" name="AMDS1cl" elementKind="unspecified" description="" timeCreated="2015-04-19T14:58:31.898+0200" lastModified="2017-01-17T19:34:35.797+0100" uniqueID="77b328af-98b3-46bd-a17c-aa2efadad337" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.796+0100" uniqueID="164a555c-6e85-458a-a507-a77477f2d693" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T14:58:45.308+0200" uniqueID="e3bdc01d-c071-4076-85e4-cbae46feab2a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T14:58:45.311+0200" uniqueID="1dd8d995-c7d7-44b6-96a2-0af01ba73b5e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.1/@contents.1/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualEnumerationElement" Location="74,81" Bounds="163,83" Parent="//@contents.0/@contents.0/@contents.1/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.1/@contents.1/@VisualDiagram" IsSketchy="true" Name="MediaKind" visualID="_9YpboP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_9YpboP5YEeSzkfJMf5_daA" Name="media type = physical" visualID="_9Ypbof5YEeSzkfJMf5_daA">
                <stitchTo href="StitchTempFile.red#//@contents.0/@VisualDiagram/@Elements.1/@Elements.0/@stitchFrom.0"/>
              </Elements>
              <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_9YpboP5YEeSzkfJMf5_daA" Name="delivery = online" visualID="_9YqCsP5YEeSzkfJMf5_daA">
                <stitchTo href="StitchTempFile.red#//@contents.0/@VisualDiagram/@Elements.1/@Elements.1/@stitchFrom.0"/>
              </Elements>
              <stitchTo href="StitchTempFile.red#//@contents.0/@VisualDiagram/@Elements.1/@stitchFrom.0"/>
            </Elements>
            <Elements xsi:type="class:VisualEnumerationElement" Location="309,79" Bounds="125,100" Parent="//@contents.0/@contents.0/@contents.1/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.1/@contents.1/@VisualDiagram" IsSketchy="true" Name="DeliveryKind" visualID="_9YqCsf5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_9YqCsf5YEeSzkfJMf5_daA" Name="media type = physical" visualID="_9YqCsv5YEeSzkfJMf5_daA">
                <stitchTo href="StitchTempFile.red#//@contents.0/@VisualDiagram/@Elements.2/@Elements.0/@stitchFrom.0"/>
              </Elements>
              <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_9YqCsf5YEeSzkfJMf5_daA" Name="delivery = online" visualID="_9YqCs_5YEeSzkfJMf5_daA">
                <stitchTo href="StitchTempFile.red#//@contents.0/@VisualDiagram/@Elements.2/@Elements.1/@stitchFrom.0"/>
              </Elements>
              <stitchTo href="StitchTempFile.red#//@contents.0/@VisualDiagram/@Elements.2/@stitchFrom.0"/>
            </Elements>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="AMDS2" name="AMDS2" elementKind="" description="Readers must sign up to the AMDS in order to use it." timeCreated="2011-12-09T09:33:52.604+0100" lastModified="2017-01-17T19:34:35.806+0100" uniqueID="7d77b7d8-c459-45b8-b5f3-c341429e74a8" workPackage="" abstractionLevel="" id="AMDS2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.806+0100" uniqueID="88e776ec-34eb-4d4c-bcab-31f04c9445ca" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:36:05.611+0100" uniqueID="4b844b08-65d4-4cba-b60a-3a4ce6bf3939">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:36:05.612+0100" uniqueID="44f142d6-bda4-49f2-ab34-dc0d8d53106d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="AMDS3" name="AMDS3" elementKind="" description="Participants of the AMDS must document consent with its specific usage regulations." timeCreated="2011-12-09T09:37:30.620+0100" lastModified="2017-01-17T19:34:35.816+0100" uniqueID="91aca4be-bb60-4f24-a6b2-49abd810d51e" workPackage="" abstractionLevel="" id="AMDS3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.816+0100" uniqueID="16fd7355-0e09-41bc-9eac-5502f591bef5" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:38:07.585+0100" uniqueID="9162ed7c-a74a-4b01-ba04-b5be0a01f929">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:38:07.592+0100" uniqueID="aaab6b3c-1b14-477f-934d-3bdfdaf2b00c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="AMDS3ac" name="AMDS3ac" elementKind="unspecified" description="" timeCreated="2015-04-19T15:04:40.240+0200" lastModified="2017-01-17T19:34:35.826+0100" uniqueID="140f7dca-0b4a-4765-b478-9eb6cf3135c5" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.826+0100" uniqueID="4096e05d-ee9c-4fc3-a740-5e97cad1571a" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T15:05:01.568+0200" uniqueID="e540006c-3d92-4f34-b0e5-31204feec023">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T15:05:01.569+0200" uniqueID="1cc06b8a-e3ec-46f2-bb71-51642da1cede">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.1/@contents.4/@VisualDiagram" DiagramType="Activity">
            <Elements xsi:type="activity:VisualActionNode" Location="64,108" Bounds="100,50" Parent="//@contents.0/@contents.0/@contents.1/@contents.4/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.1/@contents.4/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.1/@contents.4/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="document consent" visualID="_9YtGAP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="activity:VisualActionNode" Location="230,109" Bounds="100,50" Parent="//@contents.0/@contents.0/@contents.1/@contents.4/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.1/@contents.4/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.1/@contents.4/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="access AMDS" visualID="_9YttEP5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YtGAP5YEeSzkfJMf5_daA" Target="_9YttEP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="ControlFlow"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="AMDS4" name="AMDS4" elementKind="" description="Electronic media may be delivered to readers online." timeCreated="2011-12-09T09:39:33.597+0100" lastModified="2017-01-17T19:34:35.836+0100" uniqueID="8d934b6f-d5e6-4c99-ae22-05373427e6fe" workPackage="" abstractionLevel="" id="AMDS4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.835+0100" uniqueID="a9b14317-e1c4-4ea0-bdea-a04e967b3e04" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:43:33.166+0100" uniqueID="3e6b349e-53a2-4d51-ae05-31cfe53e1208">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:43:33.168+0100" uniqueID="9371cc72-7f74-48e6-b517-4a85d2755b13">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="AMDS4cl" name="AMDS4cl" elementKind="unspecified" description="" timeCreated="2015-04-19T15:07:47.769+0200" lastModified="2017-01-17T19:34:35.846+0100" uniqueID="00fc0d2e-bf3f-4ecc-bf19-803b4d664595" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.845+0100" uniqueID="92378984-74ff-42c7-91ec-9fae967656e8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T15:09:42.376+0200" uniqueID="8d8e8a96-4f23-4bb8-831d-c8aa54de50a6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T15:09:42.377+0200" uniqueID="7ac31170-b056-433e-8738-eca96e9f52a5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.1/@contents.6/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="82,109" Bounds="200,150" Parent="//@contents.0/@contents.0/@contents.1/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.1/@contents.6/@VisualDiagram" IsSketchy="true" Name="CopyOrder" visualID="_9Yu7MP5YEeSzkfJMf5_daA">
              <stitchTo href="StitchTempFile.red#//@contents.0/@VisualDiagram/@Elements.0/@stitchFrom.0"/>
            </Elements>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="AMDS5" name="AMDS5" elementKind="" description="Physical media may be delivered using the book station." timeCreated="2011-12-09T09:44:15.936+0100" lastModified="2017-01-17T19:34:35.856+0100" uniqueID="829ea81b-69d5-4e7f-a133-ee9dc80f323e" workPackage="" abstractionLevel="" id="AMDS5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.856+0100" uniqueID="7184e9e4-32ab-404f-8294-3a23762bc086" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:45:41.834+0100" uniqueID="f526656e-e6d6-4d82-8287-c2cc9ab884b7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:45:41.836+0100" uniqueID="062391a6-dc1f-4f71-8af6-f75aac27cd19">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="AMDS6" name="AMDS6" elementKind="" description="If a leased copy is taken from or returned to the book station, the delivery date to/from the reader is relevant for the beginning/end of the lease." timeCreated="2011-12-09T09:52:41.626+0100" lastModified="2017-01-17T19:34:35.866+0100" uniqueID="d37b04ef-3dee-43bd-91ee-3f560e57c99d" workPackage="" abstractionLevel="" id="AMDS6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.866+0100" uniqueID="ed929409-10d3-4b67-a887-f533fc9fc348" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:54:54.937+0100" uniqueID="aaada215-ed44-48a9-a384-137593a67101">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:54:54.938+0100" uniqueID="6d86fbee-d1a3-4a86-8d78-cb9b92a6fa41">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="AMDS6cl" name="AMDS6cl" elementKind="unspecified" description="" timeCreated="2015-04-19T15:18:34.779+0200" lastModified="2017-01-17T19:34:35.877+0100" uniqueID="557b0cfb-7b0c-4bc6-9911-b633d10a36ef" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.876+0100" uniqueID="97f8ec6b-7458-4c11-ae6e-b26533ca18fb" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T15:19:21.270+0200" uniqueID="0c2e4d5f-0808-4bdf-abe6-a90e15033484">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T15:19:21.271+0200" uniqueID="1113d8d8-d0b5-403f-a4e4-bc04c9add3ad">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.1/@contents.9/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="117,131" Bounds="200,150" Parent="//@contents.0/@contents.0/@contents.1/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.1/@contents.9/@VisualDiagram" IsSketchy="true" Name="LeaseCopy" visualID="_9YwwYP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9YwwYP5YEeSzkfJMf5_daA" Name="mediumID" visualID="_9YwwYf5YEeSzkfJMf5_daA">
                <stitchTo href="StitchTempFile.red#//@contents.1/@VisualDiagram/@Elements.0/@Elements.0/@stitchFrom.0"/>
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9YwwYP5YEeSzkfJMf5_daA" Name="issueDate" visualID="_9YwwYv5YEeSzkfJMf5_daA">
                <stitchTo href="StitchTempFile.red#//@contents.1/@VisualDiagram/@Elements.0/@Elements.1/@stitchFrom.0"/>
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9YwwYP5YEeSzkfJMf5_daA" Name="deliverDate" visualID="_9YwwY_5YEeSzkfJMf5_daA">
                <stitchTo href="StitchTempFile.red#//@contents.1/@VisualDiagram/@Elements.0/@Elements.2/@stitchFrom.0"/>
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9YwwYP5YEeSzkfJMf5_daA" Name="reader" visualID="_9YwwZP5YEeSzkfJMf5_daA">
                <stitchTo href="StitchTempFile.red#//@contents.1/@VisualDiagram/@Elements.0/@Elements.3/@stitchFrom.0"/>
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <stitchTo href="StitchTempFile.red#//@contents.1/@VisualDiagram/@Elements.0/@stitchFrom.0"/>
            </Elements>
          </VisualDiagram>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Identification Tags" timeCreated="2011-12-07T15:39:02.504+0100" lastModified="2017-01-17T19:34:35.988+0100" uniqueID="2f107ae2-2b58-44c6-80b4-8835c1c872de">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.935+0100" uniqueID="749a0030-b9ef-4d05-9706-8374a4bffef8" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="IDTAG1" name="IDTAG1" elementKind="unspecified" description="Every copy can be identified by the scanner attached to every terminal." timeCreated="2011-12-09T09:58:36.183+0100" lastModified="2017-01-17T19:34:35.945+0100" uniqueID="7324ba2a-e538-4627-a0e4-5194447c736b" workPackage="" abstractionLevel="" id="IDTAG1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.945+0100" uniqueID="77df4f80-36d6-4602-9fa2-0c947c227021" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T09:58:45.382+0100" uniqueID="9f5254d7-fee0-4a02-9569-9f67642d6e93">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:58:45.383+0100" uniqueID="c7d049b5-c0d7-4cce-9939-19b35fcc5947">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="IDTAG2" name="IDTAG2" elementKind="unspecified" description="Scanning the id badge of a medium displays a picture of the cover at the terminal." timeCreated="2011-12-09T10:01:13.652+0100" lastModified="2017-01-17T19:34:35.955+0100" uniqueID="006282e6-7444-4633-8e47-4f9517adadc8" workPackage="" abstractionLevel="" id="IDTAG2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.955+0100" uniqueID="dc6ccb6e-fe84-491e-82fb-f0b36e957559" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:01:16.200+0100" uniqueID="41e8e78a-4147-4c5f-bcf7-ea2a16332229">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:01:16.203+0100" uniqueID="45173dca-89f3-42d4-98c6-40c436e60863">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="IDTAG2sm" name="IDTAG2sm" elementKind="unspecified" description="" timeCreated="2015-04-19T15:38:23.247+0200" lastModified="2017-01-17T19:34:35.965+0100" uniqueID="7acd6f8f-861d-44bc-a997-ea2ae60f4717" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.965+0100" uniqueID="da6a0a07-a5fc-4ca0-b209-fc4c913b69c1" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T15:39:56.183+0200" uniqueID="79143fc2-6af7-4004-ba42-eb6db9f81fa2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T15:39:56.184+0200" uniqueID="2a6cc73a-ddbc-49ab-af03-b7185cbc64db">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram">
            <Elements xsi:type="statemachine:VisualInitialState" Location="106,18" Bounds="30,30" Parent="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" visualID="_9YylkP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="75,85" Bounds="92,26" Parent="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="scanned" visualID="_9Yylkf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="70,162" Bounds="97,37" Parent="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="displayed" visualID="_9Yylkv5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualFinalState" Location="105,233" Bounds="30,30" Parent="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.0/@contents.2/@contents.2/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" visualID="_9Yylk_5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9YylkP5YEeSzkfJMf5_daA" Target="_9Yylkf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="medium ID badge" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yylkf5YEeSzkfJMf5_daA" Target="_9Yylkv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="displaying cover" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Yylkv5YEeSzkfJMf5_daA" Target="_9Yylk_5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="IDTAG3" name="IDTAG3" elementKind="unspecified" description="Scanning the id of a reader card displays and identifies the reader." timeCreated="2011-12-09T10:02:48.052+0100" lastModified="2017-01-17T19:34:35.978+0100" uniqueID="050e25ee-7926-4270-9ed9-b2e8f6790dec" workPackage="" abstractionLevel="" id="IDTAG3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.977+0100" uniqueID="62d0e0b7-3e78-4990-8c53-9dfd39d1a05a" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:03:13.000+0100" uniqueID="158fc73c-4146-4d71-b045-459d270bb5f9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:03:13.001+0100" uniqueID="14ed9a14-46ba-44fd-8c83-8ffd6e32edd1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="IDTAG4" name="IDTAG4" elementKind="unspecified" description="Online access to a reader account is only granted to persons who can identify themselves as the owner of the account or legitimate proxies." timeCreated="2011-12-09T10:03:36.187+0100" lastModified="2017-01-17T19:34:35.988+0100" uniqueID="a33b1879-09ec-438c-b4e2-cbdd29b0b810" workPackage="" abstractionLevel="" id="IDTAG4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:35.987+0100" uniqueID="cbc65a40-4dca-49ae-bb3e-a8bd8f672b4c" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:06:14.065+0100" uniqueID="ad699e51-9a04-47dc-94c4-9099f749f041">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:06:14.066+0100" uniqueID="1764408b-f775-4fb4-bab2-8976e6cfa0e3">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Catalog" timeCreated="2011-12-09T15:10:16.357+0100" lastModified="2017-01-17T19:34:37.432+0100" uniqueID="b8e2fc9c-f730-4727-8f85-aea58589c89d">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.471+0100" uniqueID="916e782c-a840-46c5-bc5c-bc1b21e73a84" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Catalog Data" timeCreated="2011-12-07T15:43:50.580+0100" lastModified="2017-01-17T19:34:36.733+0100" uniqueID="fcbd04f1-9c7f-4026-9e26-e4b8f4b1eda6">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.600+0100" uniqueID="af92cf65-21f6-41d7-94d8-94ae16f8d5b8" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="CDAT1" name="CDAT1" elementKind="" description="A catalog provides information about media and copies in a corpus." timeCreated="2011-12-09T10:06:53.259+0100" lastModified="2017-01-17T19:34:36.613+0100" uniqueID="4b3e61e6-3f0c-46c0-9d41-25ce3f9e49f5" workPackage="" abstractionLevel="" id="CDAT1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.612+0100" uniqueID="0bf8c53e-7bde-469b-bc35-d374aade6cae" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:07:43.429+0100" uniqueID="708d2f66-742a-43fb-a2c0-dbda35055a75">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:07:43.430+0100" uniqueID="a030d818-9294-4326-916d-a466815f5bf5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CDAT2" name="CDAT2" elementKind="" description="Multiple catalogs shall be made available to users in a unified and seamless way." timeCreated="2011-12-09T10:08:16.306+0100" lastModified="2017-01-17T19:34:36.623+0100" uniqueID="6d61d2e4-9009-4ef2-9fc8-775317017d80" workPackage="" abstractionLevel="" id="CDAT2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.622+0100" uniqueID="1ef5d00f-98e3-46f2-a249-de00a09ecc00" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:08:44.571+0100" uniqueID="f32879fb-8dd9-4583-bfbe-783e4d203893">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:08:44.573+0100" uniqueID="786ade98-6f58-4b86-9163-216eb20410a1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CDAT2cl" name="CDAT2cl" elementKind="unspecified" description="" timeCreated="2015-04-19T15:41:19.700+0200" lastModified="2017-01-17T19:34:36.633+0100" uniqueID="a381ce69-9abc-4571-ab8f-92e06b7c65f5" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.632+0100" uniqueID="5b98d469-f536-44d2-a6bd-63bd6b9c436e" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T15:42:25.741+0200" uniqueID="99d3c789-1c55-4c4c-ab8c-042f4028d4e2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T15:42:25.742+0200" uniqueID="bc20420b-e969-4fef-bb76-d327f824acbb">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.0/@contents.2/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="54,45" Bounds="122,83" Parent="//@contents.0/@contents.1/@contents.0/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Users" visualID="_9Y1o4P5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y1o4P5YEeSzkfJMf5_daA" Name="userName" visualID="_9Y1o4f5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y1o4P5YEeSzkfJMf5_daA" Name="userID" visualID="_9Y1o4v5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
            <Elements xsi:type="class:VisualClassElement" Location="248,58" Bounds="141,60" Parent="//@contents.0/@contents.1/@contents.0/@contents.2/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.2/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.2/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Catalog" visualID="_9Y1o4_5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y1o4_5YEeSzkfJMf5_daA" Name="catalogName" visualID="_9Y2P8P5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y1o4P5YEeSzkfJMf5_daA" Target="_9Y1o4_5YEeSzkfJMf5_daA" Direction="Bidirectional" Name="" SourceMultiplicity="1" TargetMultiplicity="*" Type="Association"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CDAT3" name="CDAT3" elementKind="" description="Each medium is stored with the full bibliographic data according to DanMarc2 and a list of the copies of this medium in the corpus." timeCreated="2011-12-09T10:09:00.514+0100" lastModified="2017-01-17T19:34:36.643+0100" uniqueID="e7ac3734-1fe7-470c-9f07-e04b07c94dc1" workPackage="" abstractionLevel="" id="CDAT3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.642+0100" uniqueID="e37d5c30-cec6-48aa-9ba3-f72821b37e26" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:10:24.172+0100" uniqueID="0bd24f62-e984-41e2-b8d1-64e7a6b58867">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:10:24.175+0100" uniqueID="71509d44-8f7c-4d51-88d0-101ba8d407ab">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CDAT4" name="CDAT4" elementKind="" description="Each copy is stored in the catalog with a picture of the cover, and a state indicating its degree of damage." timeCreated="2011-12-09T10:10:38.137+0100" lastModified="2017-01-17T19:34:36.653+0100" uniqueID="1a8eb053-febd-4448-a202-84435855ed0a" workPackage="" abstractionLevel="" id="CDAT4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.653+0100" uniqueID="6ce3208b-9ff2-4ade-b0f5-40d3db045aea" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:11:13.247+0100" uniqueID="9689be43-a094-4689-85c6-afa9c049776f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:11:13.253+0100" uniqueID="b1466596-3a6a-4a3f-bf07-20b075c3c648">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CDAT4cl" name="CDAT4cl" elementKind="unspecified" description="" timeCreated="2015-04-19T15:43:10.336+0200" lastModified="2017-01-17T19:34:36.663+0100" uniqueID="a729febd-d1bf-4820-af18-2ce615320f65" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.663+0100" uniqueID="1cfdafa6-6d8f-421b-bf20-ba49d7d5b888" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T15:46:57.829+0200" uniqueID="0c824e8a-3d4c-467f-9b23-547bfda2915d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T15:46:57.830+0200" uniqueID="b82fa657-cbd7-4da8-85ff-119db812c091">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="117,75" Bounds="136,48" Parent="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="Catalog" visualID="_9Y3eEP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y3eEP5YEeSzkfJMf5_daA" Name="name" visualID="_9Y3eEf5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
            <Elements xsi:type="class:VisualClassElement" Location="27,203" Bounds="130,112" Parent="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="Corpus" visualID="_9Y3eEv5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y3eEv5YEeSzkfJMf5_daA" Name="name" visualID="_9Y4FIP5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y3eEv5YEeSzkfJMf5_daA" Name="location" visualID="_9Y4FIf5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y3eEv5YEeSzkfJMf5_daA" Name="policy" visualID="_9Y4FIv5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y3eEv5YEeSzkfJMf5_daA" Name="owner" visualID="_9Y4FI_5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
            <Elements xsi:type="class:VisualClassElement" Location="229,310" Bounds="151,93" Parent="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="Medium" visualID="_9Y4FJP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y4FJP5YEeSzkfJMf5_daA" Name="signature" visualID="_9Y4FJf5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y4FJP5YEeSzkfJMf5_daA" Name="classification" visualID="_9Y4FJv5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y4FJP5YEeSzkfJMf5_daA" Name="details" visualID="_9Y4FJ_5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
            <Elements xsi:type="class:VisualClassElement" Location="361,132" Bounds="102,116" Parent="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.5/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="Copy" visualID="_9Y4FKP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y4FKP5YEeSzkfJMf5_daA" Name="number" visualID="_9Y4FKf5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y4FKP5YEeSzkfJMf5_daA" Name="lendingState" visualID="_9Y4sMP5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y4FKP5YEeSzkfJMf5_daA" Name="degreeOfWear" visualID="_9Y4sMf5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9Y4FKP5YEeSzkfJMf5_daA" Name="cover" visualID="_9Y4sMv5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y3eEv5YEeSzkfJMf5_daA" Target="_9Y3eEP5YEeSzkfJMf5_daA" Direction="Bidirectional" Name="" SourceMultiplicity="1..*" TargetMultiplicity="*" Type="Association"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y3eEP5YEeSzkfJMf5_daA" Target="_9Y4FJP5YEeSzkfJMf5_daA" Direction="Bidirectional" Name="" SourceMultiplicity="*" TargetMultiplicity="*" Type="Association"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y3eEv5YEeSzkfJMf5_daA" Target="_9Y4FJP5YEeSzkfJMf5_daA" Direction="Bidirectional" Name="" SourceMultiplicity="1..*" TargetMultiplicity="*" Type="Association"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y4FJP5YEeSzkfJMf5_daA" Target="_9Y4FKP5YEeSzkfJMf5_daA" Direction="Bidirectional" Name="" SourceMultiplicity="1" TargetMultiplicity="1..*" Type="Association"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CDAT5" name="CDAT5" elementKind="" description="The catalog may be updated semi-automatically by a librarian using electronic sources." timeCreated="2011-12-09T10:11:48.024+0100" lastModified="2017-01-17T19:34:36.673+0100" uniqueID="9a3ec2bc-ef72-47ad-a08f-9be32c94738a" workPackage="" abstractionLevel="" id="CDAT5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.673+0100" uniqueID="751e6287-d1ee-4d58-907c-7750487aa598" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:12:10.183+0100" uniqueID="3756f2a5-2cf6-40f9-8155-8f542a716091">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:12:10.185+0100" uniqueID="1306d4f6-3a82-4a26-abfd-17efa2f7e85f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CDAT5uc" name="CDAT5uc" elementKind="unspecified" description="" timeCreated="2015-04-19T15:47:30.896+0200" lastModified="2017-01-17T19:34:36.684+0100" uniqueID="2e3c2a86-e584-4d0c-92c7-d01a2a4fa37d" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.683+0100" uniqueID="4b91df3a-ac6c-4cac-81a0-d1126b7c67e2" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T15:48:06.173+0200" uniqueID="c911791f-c295-4c1f-8b17-b941a5fd70b6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T15:48:06.175+0200" uniqueID="fc27359a-979b-49ae-a941-48e64495b7ff">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="41,60" Bounds="80,150" Parent="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Librarian" visualID="_9Y6hYP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="177,40" Bounds="200,230" Parent="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram" IsSketchy="true" Name="Catalog" visualID="_9Y6hYf5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="25,61" Bounds="150,50" Parent="_9Y6hYf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="update" visualID="_9Y6hYv5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="29,148" Bounds="150,50" Parent="_9Y6hYf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.7/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="semi automatic update" visualID="_9Y6hY_5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y6hYP5YEeSzkfJMf5_daA" Target="_9Y6hYv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y6hY_5YEeSzkfJMf5_daA" Target="_9Y6hYv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHeadSolidWhite" Name="" Type="Generalization"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CDAT6" name="CDAT6" elementKind="" description="The catalog may be updated by a librarian manually." timeCreated="2011-12-09T10:12:31.664+0100" lastModified="2017-01-17T19:34:36.694+0100" uniqueID="c1056659-8b01-410c-9a53-3682714c1fe8" workPackage="" abstractionLevel="" id="CDAT6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.693+0100" uniqueID="621031f2-23db-4da8-9562-3759ef0a2535" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:12:45.669+0100" uniqueID="3d411480-919c-4316-8f19-4039cdcc35f9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:12:45.672+0100" uniqueID="87752de3-7978-4015-970c-e83720e6c632">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CDAT6uc" name="CDAT6uc" elementKind="unspecified" description="" timeCreated="2015-04-19T15:48:56.749+0200" lastModified="2017-01-17T19:34:36.704+0100" uniqueID="bab8e575-1858-46c0-b9c7-8fd0443feec9" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.703+0100" uniqueID="bd980bc7-07a5-46ba-8dd5-6dfb4e8ba096" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T15:51:08.218+0200" uniqueID="eec18902-4c34-4acd-b09e-c11295c38aa0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T15:51:08.220+0200" uniqueID="964f1c83-976a-4a33-915e-02aba7e665eb">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="42,40" Bounds="80,150" Parent="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="Librarian" visualID="_9Y8WkP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="160,34" Bounds="200,200" Parent="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram" IsSketchy="true" Name="Catalog" visualID="_9Y8Wkf5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="23,46" Bounds="150,50" Parent="_9Y8Wkf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="update" visualID="_9Y8Wkv5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="43,127" Bounds="150,50" Parent="_9Y8Wkf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.9/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="manual update" visualID="_9Y8Wk_5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y8Wk_5YEeSzkfJMf5_daA" Target="_9Y8Wkv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHeadSolidWhite" Name="" Type="Generalization"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y8WkP5YEeSzkfJMf5_daA" Target="_9Y8Wkv5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CDAT7" name="CDAT7" elementKind="" description="The LMS shall be able to batch update the local cache of all remote catalogs it offers to the reader." timeCreated="2011-12-09T10:13:10.464+0100" lastModified="2017-01-17T19:34:36.714+0100" uniqueID="47c4422f-67b4-4b47-bbb7-f454111bcb53" workPackage="" abstractionLevel="" id="CDAT7">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.713+0100" uniqueID="351f6e4e-ca9e-42fe-af0f-05604caf13ad" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:13:35.218+0100" uniqueID="872ad5b9-a859-4cb7-a08e-19aecfd91137">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:13:35.219+0100" uniqueID="cb1c536f-a6ca-4b30-87d3-eced409f0090">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CDAT7uc" name="CDAT7uc" elementKind="unspecified" description="" timeCreated="2015-04-19T16:10:54.452+0200" lastModified="2017-01-17T19:34:36.733+0100" uniqueID="5aaaa988-b45c-4c19-8358-549bd9790fdd" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.732+0100" uniqueID="d14c3e50-a2f6-4384-8511-be14873995c7" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:11:25.934+0200" uniqueID="26a3db5f-05fe-43fc-9014-864c49e9ee42">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:11:25.936+0200" uniqueID="71ae3a54-18f7-445b-bed2-39817ea35e88">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.0/@contents.11/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="86,96" Bounds="94,150" Parent="//@contents.0/@contents.1/@contents.0/@contents.11/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.11/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Remote catalog" visualID="_9Y-LwP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="220,96" Bounds="200,136" Parent="//@contents.0/@contents.1/@contents.0/@contents.11/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.0/@contents.11/@VisualDiagram" IsSketchy="true" Name="LMS" visualID="_9Y-Lwf5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="14,51" Bounds="150,50" Parent="_9Y-Lwf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.0/@contents.11/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="update catalog (batch)" visualID="_9Y-Lwv5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9Y-LwP5YEeSzkfJMf5_daA" Target="_9Y-Lwv5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Catalog Search (General)" timeCreated="2011-12-07T15:44:19.774+0100" lastModified="2017-01-17T19:34:37.099+0100" uniqueID="6fbb5a9b-f661-414d-baae-05b8a70b06c9">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.917+0100" uniqueID="7dd4071e-315f-4566-b75e-92d16d4482c7" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="CSG1" name="CSG1" elementKind="" description="Users may search the catalog from local or remote terminals " timeCreated="2011-12-09T10:14:47.390+0100" lastModified="2017-01-17T19:34:36.928+0100" uniqueID="ff9f69a3-7315-4c79-9e61-c85388e2650e" workPackage="" abstractionLevel="" id="CSG1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.928+0100" uniqueID="d75a2b23-c1f3-45f1-b44a-a2a7fcf8ed64" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:15:05.035+0100" uniqueID="98e3bbb9-1990-4c7e-a0bc-0761b0a526f6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:15:05.036+0100" uniqueID="8bfb1a25-e390-47bd-9433-a1297156ad15">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CSG1uc" name="CSG1uc" elementKind="unspecified" description="" timeCreated="2015-04-19T16:16:32.935+0200" lastModified="2017-01-17T19:34:36.939+0100" uniqueID="7eeec9cd-78dc-4a48-8f07-4de23edadd8d" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.938+0100" uniqueID="d9b2e855-1feb-4218-8953-b50b6fc61729" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:31:07.826+0200" uniqueID="b9627728-9875-4899-946d-0aa7eea1e614">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:31:07.828+0200" uniqueID="b4121403-01d8-4357-880f-b0b1c0bae09c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="64,112" Bounds="80,150" Parent="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="user" visualID="_9ZAA8P5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="192,85" Bounds="200,200" Parent="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram" IsSketchy="true" Name="Catalog" visualID="_9ZAA8f5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="21,47" Bounds="150,50" Parent="_9ZAA8f5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="search" visualID="_9ZAA8v5YEeSzkfJMf5_daA"/>
            </Elements>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="204,299" Bounds="200,132" Parent="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram" IsSketchy="true" Name="remote terminal" visualID="_9ZAA8_5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="35,45" Bounds="150,50" Parent="_9ZAA8_5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.1/@contents.1/@contents.1/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="use" visualID="_9ZAA9P5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZAA8P5YEeSzkfJMf5_daA" Target="_9ZAA8v5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZAA8P5YEeSzkfJMf5_daA" Target="_9ZAA9P5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZAA8v5YEeSzkfJMf5_daA" Target="_9ZAA9P5YEeSzkfJMf5_daA" Direction="SourceTarget" LineStyle="Dashed" TargetDecoration="ArrowHead" Name="Extend" Type="Extend"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG2" name="CSG2" elementKind="" description="Users may search the catalog using two modes for experts and novices, respectively, called “Expert Search” and “Simple Search”." timeCreated="2011-12-09T10:15:20.390+0100" lastModified="2017-01-17T19:34:36.949+0100" uniqueID="b3170a9b-a38f-4219-9c6a-4f28e6b82653" workPackage="" abstractionLevel="" id="CSG2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.949+0100" uniqueID="2cc4f9b8-6fd6-4fa6-84ce-13d1df96dbaa" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:16:34.730+0100" uniqueID="f2bfa5eb-9a1c-463f-8f8d-5ce38aa0b185">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:16:34.733+0100" uniqueID="8ecf59c6-1ecb-4bce-b7df-c9c1e50cb1f0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CSG2uc" name="CSG2uc" elementKind="unspecified" description="" timeCreated="2015-04-19T16:31:57.375+0200" lastModified="2017-01-17T19:34:36.959+0100" uniqueID="b9a325ba-5b64-4c12-b81f-9dbe02f042f8" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.959+0100" uniqueID="0c2421f1-146f-413e-8925-e99e9f339b80" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:34:26.713+0200" uniqueID="7e4e4026-2b84-4c60-8da3-5af274d6c786">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:34:26.713+0200" uniqueID="738c5139-6967-4911-bfb3-e2160146b380">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="39,58" Bounds="80,150" Parent="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="reader" visualID="_9ZBPEP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="157,60" Bounds="365,339" Parent="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram" IsSketchy="true" Name="Catalog" visualID="_9ZBPEf5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="44,51" Bounds="150,50" Parent="_9ZBPEf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="search catalog" visualID="_9ZBPEv5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="145,127" Bounds="150,50" Parent="_9ZBPEf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="search catalog(complex)" visualID="_9ZBPE_5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="143,194" Bounds="150,50" Parent="_9ZBPEf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="search catalog(simple)" visualID="_9ZBPFP5YEeSzkfJMf5_daA"/>
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="152,247" Bounds="150,50" Parent="_9ZBPEf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.3/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="identify copy/medium" visualID="_9ZBPFf5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZBPE_5YEeSzkfJMf5_daA" Target="_9ZBPEv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHeadSolidWhite" Name="" Type="Generalization">
              <Bendpoints>275,222</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZBPFP5YEeSzkfJMf5_daA" Target="_9ZBPEv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHeadSolidWhite" Name="" Type="Generalization">
              <Bendpoints>274,267</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZBPFf5YEeSzkfJMf5_daA" Target="_9ZBPEv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHeadSolidWhite" Name="" Type="Generalization">
              <Bendpoints>269,312</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZBPEP5YEeSzkfJMf5_daA" Target="_9ZBPEv5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG3" name="CSG3" elementKind="" description="A search yields a list of overview results that may each be expanded to detail results." timeCreated="2011-12-09T10:20:27.561+0100" lastModified="2017-01-17T19:34:36.970+0100" uniqueID="6d4e7081-a879-4f3f-a162-15044923df64" workPackage="" abstractionLevel="" id="CSG3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.969+0100" uniqueID="ab375f75-1143-4cef-a8bd-a53f9a71dac5" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:20:46.190+0100" uniqueID="450f06fc-60ac-4bba-8845-d11836500952">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:20:46.194+0100" uniqueID="9ab4f7c0-a4b8-4d83-b3e1-277bf7ac506f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CSG3cl" name="CSG3cl" elementKind="unspecified" description="" timeCreated="2015-04-19T16:34:52.073+0200" lastModified="2017-01-17T19:34:36.981+0100" uniqueID="a2590b72-54f7-48d7-8e1c-043fd3e83fb3" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.981+0100" uniqueID="c962fd9d-eb9f-4489-8424-72b09972b6f8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:36:04.293+0200" uniqueID="ab170234-21b9-4f39-9ebf-d769e80efc47">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:36:04.294+0200" uniqueID="8f84e43b-80e1-4e4f-9291-9962411a2610">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="61,76" Bounds="90,65" Parent="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Catalog" visualID="_9ZDEQP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassOperation" Location="0,0" Bounds="0,0" Parent="_9ZDEQP5YEeSzkfJMf5_daA" Name="search" visualID="_9ZDrUP5YEeSzkfJMf5_daA"/>
            </Elements>
            <Elements xsi:type="class:VisualClassElement" Location="217,62" Bounds="106,63" Parent="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="OverviewResult" visualID="_9ZESYP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="class:VisualClassElement" Location="142,193" Bounds="135,52" Parent="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.1/@contents.5/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="DetailedResult" visualID="_9ZESYf5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZDEQP5YEeSzkfJMf5_daA" Target="_9ZESYP5YEeSzkfJMf5_daA" Direction="Bidirectional" Name="" SourceMultiplicity="*" TargetMultiplicity="0..*" Type="Association"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZESYP5YEeSzkfJMf5_daA" Target="_9ZESYf5YEeSzkfJMf5_daA" Direction="Bidirectional" Name="" SourceMultiplicity="1" TargetMultiplicity="*" Type="Association"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG4" name="CSG4" elementKind="" description="An overview search result consists of the medium category, author, title, and publication year." timeCreated="2011-12-09T10:21:05.518+0100" lastModified="2017-01-17T19:34:36.992+0100" uniqueID="698441bb-7043-4cea-910b-f4aad0dfc6fb" workPackage="" abstractionLevel="" id="CSG4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:36.992+0100" uniqueID="a57c22f0-29a9-4938-9015-5d4e7e6322bb" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:21:18.004+0100" uniqueID="5e0f55db-a32f-4a64-b276-2f1716ed666d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:21:18.006+0100" uniqueID="4f961d98-36bf-43cc-9d12-e6a36ddd3868">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CSG4cl" name="CSG4cl" elementKind="unspecified" description="" timeCreated="2015-04-19T16:36:24.647+0200" lastModified="2017-01-17T19:34:37.003+0100" uniqueID="de272dc8-ff7b-471c-8c26-520035ffc86e" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.001+0100" uniqueID="ff184821-3bcc-4dd0-b734-61f45a232dfb" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:37:06.393+0200" uniqueID="9d237acf-a386-438b-af6a-5fe7e1cfba97">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:37:06.395+0200" uniqueID="04b4966b-b87f-4968-b3b4-5acb9a9d825d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.1/@contents.7/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="81,57" Bounds="143,130" Parent="//@contents.0/@contents.1/@contents.1/@contents.7/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.7/@VisualDiagram" IsSketchy="true" Name="OverviewResult" visualID="_9ZFggP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZFggP5YEeSzkfJMf5_daA" Name="mediumType" visualID="_9ZFggf5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZFggP5YEeSzkfJMf5_daA" Name="author" visualID="_9ZFggv5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZFggP5YEeSzkfJMf5_daA" Name="title" visualID="_9ZFgg_5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZFggP5YEeSzkfJMf5_daA" Name="publicationYear" visualID="_9ZFghP5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG5" name="CSG5" elementKind="" description="A detailed search result consists of the full bibliographic details, a list of all copies and their availability, and all comments on the medium." timeCreated="2011-12-09T10:21:39.733+0100" lastModified="2017-01-17T19:34:37.013+0100" uniqueID="bad992de-56f7-47b6-96fc-32602ecc0e5a" workPackage="" abstractionLevel="" id="CSG5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.013+0100" uniqueID="ee60b1ed-ab99-4ed1-aa9b-f8b8d5bc6943" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:21:45.492+0100" uniqueID="aea739f3-833c-48bd-80e5-77d8805630bd">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:21:45.493+0100" uniqueID="d29dd120-07f4-42ef-af77-ef8f7720d8a9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="CSG5cl" name="CSG5cl" elementKind="unspecified" description="" timeCreated="2015-04-19T16:37:26.412+0200" lastModified="2017-01-17T19:34:37.023+0100" uniqueID="929d496f-10e6-4b6c-8480-1d511428281b" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.023+0100" uniqueID="b7e9d968-7bd4-454e-af10-bdb5793fee15" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:38:02.448+0200" uniqueID="938112ee-d25f-4971-b7d2-a446c973c1fd">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:38:02.450+0200" uniqueID="2e62f115-cf94-44df-b481-290e06fba3b1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.1/@contents.9/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="55,47" Bounds="161,124" Parent="//@contents.0/@contents.1/@contents.1/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.1/@contents.9/@VisualDiagram" IsSketchy="true" Name="DetailedResult" visualID="_9ZHVsP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZHVsP5YEeSzkfJMf5_daA" Name="bibliographicDetails" visualID="_9ZHVsf5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZHVsP5YEeSzkfJMf5_daA" Name="numberOfCopies" visualID="_9ZHVsv5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZHVsP5YEeSzkfJMf5_daA" Name="availability" visualID="_9ZHVs_5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZHVsP5YEeSzkfJMf5_daA" Name="comments" visualID="_9ZHVtP5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG6" name="CSG6" elementKind="" description="There is a special function to quickly identify copies and return their overview data and the cover picture." timeCreated="2011-12-09T10:22:08.156+0100" lastModified="2017-01-17T19:34:37.036+0100" uniqueID="4d16d1c7-90d3-4256-b801-48433d50ef11" workPackage="" abstractionLevel="" id="CSG6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.036+0100" uniqueID="68a6c473-fbbf-4c95-96af-d610c7d03577" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:22:19.492+0100" uniqueID="4ea0462f-3357-48f1-a13a-02585e54170a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:22:19.493+0100" uniqueID="b2bd41e5-64fe-49e4-922c-43fdf0f86e3b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG7" name="CSG7" elementKind="" description="Identifying a copy takes less than 0.5s in 99% of all cases." timeCreated="2011-12-09T10:23:18.441+0100" lastModified="2017-01-17T19:34:37.047+0100" uniqueID="4d181500-03bf-47e8-9092-c2f4c5ce5310" workPackage="" abstractionLevel="" id="CSG7">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.046+0100" uniqueID="cb2de918-b937-40e4-888f-c870466576e8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:23:33.869+0100" uniqueID="46e5b4b9-9022-4957-b745-2944b09ee2df">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:23:33.870+0100" uniqueID="3f521dc2-3663-4c24-815b-93fd5d05773b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG8" name="CSG8" elementKind="" description="If a copy cannot be identified, an adequate error notification is issued within 1s." timeCreated="2011-12-09T10:23:55.979+0100" lastModified="2017-01-17T19:34:37.058+0100" uniqueID="0e33e5f4-7372-444e-8016-a265b8ed3cff" workPackage="" abstractionLevel="" id="CSG8">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.057+0100" uniqueID="718aa288-a6da-4919-8a52-e813f2904b47" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:24:06.940+0100" uniqueID="5f3b90d2-aa06-4abf-a5fd-5d5943c45e9e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:24:06.943+0100" uniqueID="3e256a3d-5784-4295-8d16-88cfd49df2a5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG9" name="CSG9" elementKind="" description="All search queries, their results, and data on which of the results were chosen by the user are archived in line with the applicable privacy reg¬ul¬ations." timeCreated="2011-12-09T10:24:32.603+0100" lastModified="2017-01-17T19:34:37.068+0100" uniqueID="8de0b7fc-6b81-4498-ac57-9c6abc969e2a" workPackage="" abstractionLevel="" id="CSG9">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.068+0100" uniqueID="c187ce92-e1da-4013-8955-dac40c4152b8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:24:57.527+0100" uniqueID="d94771da-89b2-47b9-a852-46729ec71e01">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:24:57.530+0100" uniqueID="b4a016aa-9785-475e-a466-79d1a81dced2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG10" name="CSG10" elementKind="" description="If a search query involves only the local catalog and yields at least one answer, all LMS will provide the first/next 10 answers to the search query within 5 seconds." timeCreated="2011-12-09T10:25:29.977+0100" lastModified="2017-01-17T19:34:37.079+0100" uniqueID="c2d677ff-894f-4c0b-88c1-90fc6706e152" workPackage="" abstractionLevel="" id="CSG10">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.078+0100" uniqueID="899aaa12-f6b8-400f-8dee-6f417923c083" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:25:41.579+0100" uniqueID="180780dd-f1d1-4143-b9c1-af3167c03711">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:25:41.582+0100" uniqueID="2b8eb90d-2fd9-4d18-b5c9-cfec5f1516a5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG11" name="CSG11" elementKind="" description="Retrieving the detail results of 10 media takes less than 5s." timeCreated="2011-12-09T10:26:10.739+0100" lastModified="2017-01-17T19:34:37.089+0100" uniqueID="8395caba-8170-452a-9b59-47b8c9f7e59e" workPackage="" abstractionLevel="" id="CSG11">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.089+0100" uniqueID="224099d5-c739-46e2-b2a5-662399b9efe7" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:27:12.082+0100" uniqueID="4347ee47-115b-406a-8bb6-84163122aaa6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:27:12.083+0100" uniqueID="c50158a5-c27e-4992-900a-c1769a14e287">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="CSG12" name="CSG12" elementKind="" description="When a medium is scanned, the respective catalog entry is displayed within .3s." timeCreated="2011-12-09T10:27:26.683+0100" lastModified="2017-01-17T19:34:37.099+0100" uniqueID="569cb9d0-f6c1-4e12-91d0-023b88524da0" workPackage="" abstractionLevel="" id="CSG12">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.099+0100" uniqueID="858d2781-c2c9-4642-831a-9bf7e8c5be21" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:27:55.640+0100" uniqueID="2f932e21-e8bb-44ec-9de4-d48b6afc2acd">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:27:55.641+0100" uniqueID="d35b3dff-78c4-4cdb-a637-8ecc332cde3b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Expert Search Mode" timeCreated="2011-12-07T15:44:42.205+0100" lastModified="2017-01-17T19:34:37.256+0100" uniqueID="f9344502-a2e3-4dde-9501-b41f99bab383">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.180+0100" uniqueID="31e8f6e3-f27c-4042-90a4-5115de973e83" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="ESM1" name="ESM1" elementKind="" description="In the expert search mode, users may specify any set of bibliographic details or catalog information, and the catalog to be used." timeCreated="2011-12-09T10:28:10.762+0100" lastModified="2017-01-17T19:34:37.191+0100" uniqueID="bb0ef371-1bea-4691-9e9d-ea193ba67243" workPackage="" abstractionLevel="" id="ESM1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.190+0100" uniqueID="99c162fc-a85f-431b-b76c-6836d31fe599" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:28:17.065+0100" uniqueID="64e63741-7cf3-4171-a4ea-530a2ef546f7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:28:17.068+0100" uniqueID="32387c9d-6bdf-4249-98a9-17b3d089b289">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="ESM1uc" name="ESM1uc" elementKind="unspecified" description="" timeCreated="2015-04-19T16:38:55.378+0200" lastModified="2017-01-17T19:34:37.201+0100" uniqueID="20e1de3d-311a-4039-881a-59ce15474971" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.201+0100" uniqueID="350f6c09-ab9a-4547-aaa7-712a28ff16fa" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:39:23.404+0200" uniqueID="64efb0b9-f22e-4e02-acbb-5305e49efa07">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:39:23.406+0200" uniqueID="cf095ab4-6775-4ede-957f-7c9281c01ad9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.2/@contents.1/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="29,74" Bounds="80,150" Parent="//@contents.0/@contents.1/@contents.2/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.2/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.2/@contents.1/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Reader" visualID="_9ZLnIP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="159,75" Bounds="209,145" Parent="//@contents.0/@contents.1/@contents.2/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.2/@contents.1/@VisualDiagram" IsSketchy="true" Name="Catalog" visualID="_9ZLnIf5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="32,62" Bounds="150,50" Parent="_9ZLnIf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.2/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.2/@contents.1/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="search catalog(complex)" visualID="_9ZLnIv5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZLnIP5YEeSzkfJMf5_daA" Target="_9ZLnIv5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="ESM2" name="ESM2" elementKind="" description="The expert search provides connectors to combine any available information item into complex searches. " timeCreated="2011-12-09T10:29:08.954+0100" lastModified="2017-01-17T19:34:37.211+0100" uniqueID="8c8b549b-7362-4b89-8076-bd2eb1337378" workPackage="" abstractionLevel="" id="ESM2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.211+0100" uniqueID="2bfa61f6-ea20-4fde-8f3e-d7a4c844bc47" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:29:26.060+0100" uniqueID="fb84265b-ff6b-425f-bcc0-8acebbb3f109">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:29:26.064+0100" uniqueID="dae02d19-08b9-4cb2-a28d-328403f162d4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="ESM3" name="ESM3" elementKind="" description="An expert search allows to adjust the length of the result set to 5/10/20/50/all." timeCreated="2011-12-09T10:29:39.658+0100" lastModified="2017-01-17T19:34:37.222+0100" uniqueID="9f49df16-4fa8-400a-af6b-bfb9e930267c" workPackage="" abstractionLevel="" id="ESM3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.222+0100" uniqueID="0c0ec14e-3aac-46c0-93ce-b6f80f009a44" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:29:57.806+0100" uniqueID="8571bbfb-217b-4e02-99da-047955b632e4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:29:57.810+0100" uniqueID="07a33c07-1121-4ba8-89cd-1011e6fa98b1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="ESM4" name="ESM4" elementKind="" description="Expert search queries and (parts of their) results may be saved, loaded, and forwarded to an email address." timeCreated="2011-12-09T10:30:19.593+0100" lastModified="2017-01-17T19:34:37.232+0100" uniqueID="4c20bd3b-d4e2-40a8-a4e6-192cd738f687" workPackage="" abstractionLevel="" id="ESM4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.232+0100" uniqueID="780b2d13-e7ee-4862-879c-0f105c65e6a2" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:30:38.468+0100" uniqueID="48d8cda7-224e-4b72-8427-c954371207d9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:30:38.468+0100" uniqueID="579f7071-4a50-4d50-b522-6be92f5ebc04">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="ESM5" name="ESM5" elementKind="" description="Saved expert search queries may be loaded and executed automatically at preset times and with preset actions for the result." timeCreated="2011-12-09T10:30:51.744+0100" lastModified="2017-01-17T19:34:37.245+0100" uniqueID="f7497150-3b46-4724-8067-02a389b5cac4" workPackage="" abstractionLevel="" id="ESM5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.245+0100" uniqueID="61b42b01-4fcf-492b-b1ed-8696231d7a72" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:31:11.446+0100" uniqueID="18a1a19f-0746-4e9c-b95c-3a888c1bf86c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:31:11.449+0100" uniqueID="cb1b14da-f515-4a7d-be0a-f1eed3eeb453">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="ESM5sm" name="ESM5sm" elementKind="unspecified" description="" timeCreated="2015-04-19T16:43:01.787+0200" lastModified="2017-01-17T19:34:37.256+0100" uniqueID="cbb6a8f9-f0bd-45dc-9a65-d6f9cb4cf536" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.255+0100" uniqueID="72c90939-a543-4afd-9783-142931776f8a" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:43:57.953+0200" uniqueID="f09741d1-e75a-450a-b731-de58ac77e946">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:43:57.955+0200" uniqueID="f8fed4af-c12c-4bba-a385-371dc02cb432">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram">
            <Elements xsi:type="statemachine:VisualState" Location="42,56" Bounds="82,36" Parent="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="saved" visualID="_9ZNcUP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="38,123" Bounds="87,32" Parent="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="loaded" visualID="_9ZNcUf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="33,187" Bounds="92,32" Parent="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.2/@contents.6/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="executed" visualID="_9ZNcUv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZNcUP5YEeSzkfJMf5_daA" Target="_9ZNcUf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZNcUf5YEeSzkfJMf5_daA" Target="_9ZNcUv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
          </VisualDiagram>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Novice Search Mode" timeCreated="2011-12-07T15:47:34.444+0100" lastModified="2017-01-17T19:34:37.432+0100" uniqueID="74fbf5c2-c0ef-4634-8414-d826a4a281ac">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.347+0100" uniqueID="dd510cea-8c3f-4625-b91b-12ad15ab2035" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="NSM1" name="NSM1" elementKind="" description="The novice search mode offers only have a single text field for entering search terms." timeCreated="2011-12-09T10:31:35.318+0100" lastModified="2017-01-17T19:34:37.359+0100" uniqueID="cdee9ea8-6423-4ba5-8c42-dcd89f830cee" workPackage="" abstractionLevel="" id="NSM1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.358+0100" uniqueID="90e5bc6f-ad0e-4927-b088-9369150eee18" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:31:57.072+0100" uniqueID="9954a3f5-f84f-436c-bcbf-a928b5953a64">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:31:57.075+0100" uniqueID="d0598c71-e669-4de2-ba97-9571df2e1693">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="NSM1uc" name="NSM1uc" elementKind="unspecified" description="" timeCreated="2015-04-19T16:44:23.031+0200" lastModified="2017-01-17T19:34:37.369+0100" uniqueID="5fd52f36-0e5c-47e9-a95d-1ba4a04e336d" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.369+0100" uniqueID="5da854fd-1984-4074-a86f-cfcedb52ec52" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:44:48.232+0200" uniqueID="32df60d4-df0a-46a9-8ee2-4126921f86d1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:44:48.234+0200" uniqueID="2b9030c4-ca27-43a2-b3ce-35427f4260d0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.3/@contents.1/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="52,57" Bounds="80,150" Parent="//@contents.0/@contents.1/@contents.3/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.1/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Reader" visualID="_9ZOqcP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="172,51" Bounds="200,125" Parent="//@contents.0/@contents.1/@contents.3/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.1/@VisualDiagram" IsSketchy="true" Name="Catalog" visualID="_9ZOqcf5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="17,47" Bounds="150,50" Parent="_9ZOqcf5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.3/@contents.1/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.1/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="search catalog (simple)" visualID="_9ZOqcv5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZOqcP5YEeSzkfJMf5_daA" Target="_9ZOqcv5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NSM2" name="NSM2" elementKind="" description="Query sessions initiated from a terminal in the library and running in the simplified search mode offer spelling help and query-completion-as-you-type." timeCreated="2011-12-09T10:32:27.238+0100" lastModified="2017-01-17T19:34:37.380+0100" uniqueID="281ecb72-4233-4e95-9611-d7a8d3b50f10" workPackage="" abstractionLevel="" id="NSM2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.379+0100" uniqueID="682c2792-5b3a-4845-8329-98ac6ac1b725" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:32:45.434+0100" uniqueID="152f758d-69bb-4271-a5a9-b18f5e3cc1c5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:32:45.437+0100" uniqueID="9b3c078d-e681-4565-870e-bcbae2ee5a42">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NSM3" name="NSM3" elementKind="" description="Users using the basic search mode will be presented with reading suggestions that may be interesting to them based on the heuristics implemented in the BookTip™-system." timeCreated="2011-12-09T10:32:56.054+0100" lastModified="2017-01-17T19:34:37.390+0100" uniqueID="d996b788-1a2c-4e70-9da1-82e7155e1305" workPackage="" abstractionLevel="" id="NSM3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.390+0100" uniqueID="a6dc3e75-a601-4227-9951-eb380d4ecd7d" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:33:22.953+0100" uniqueID="cd380bee-cdd7-4edc-84c4-73e4a4798db8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:33:22.955+0100" uniqueID="2b5630f8-e830-4b91-aad7-f050a3b799b9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NSM4" name="NSM4" elementKind="" description="LMS will provide between 3 and 5 reading suggestions within 5s after issuing the query." timeCreated="2011-12-09T10:33:39.232+0100" lastModified="2017-01-17T19:34:37.401+0100" uniqueID="56dbf1fa-ab3a-4ce7-b845-1c5c6c72f9d4" workPackage="" abstractionLevel="" id="NSM4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.400+0100" uniqueID="e6123bcd-f263-494b-8ba3-aee93462a044" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:34:08.419+0100" uniqueID="d2c09029-154e-4363-a8f9-8a6d150a0e77">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:34:08.423+0100" uniqueID="f1c8207e-8f97-46ba-b5b9-4dbafc0d8bf3">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="NSM4uc" name="NSM4uc" elementKind="unspecified" description="" timeCreated="2015-04-19T16:46:00.693+0200" lastModified="2017-01-17T19:34:37.411+0100" uniqueID="56405a9b-6e2f-4306-b0a7-052139fb7118" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.411+0100" uniqueID="cdd8b06e-cbce-4c11-85f0-7f66ca55186c" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T16:47:25.153+0200" uniqueID="c47c7964-c284-405b-8d37-016798bc01d9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T16:47:25.155+0200" uniqueID="68a18280-3d02-4045-9df9-3b0e7c96676b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="37,88" Bounds="80,150" Parent="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="Reader" visualID="_9ZQfoP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="168,94" Bounds="189,112" Parent="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" IsSketchy="true" Name="Catalog" visualID="_9ZRGsP5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="23,49" Bounds="150,50" Parent="_9ZRGsP5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="search catalog" visualID="_9ZRGsf5YEeSzkfJMf5_daA"/>
            </Elements>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="149,285" Bounds="221,103" Parent="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" IsSketchy="true" Name="Booktip" visualID="_9ZRGsv5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="20,32" Bounds="150,50" Parent="_9ZRGsv5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="add data" visualID="_9ZRGs_5YEeSzkfJMf5_daA"/>
            </Elements>
            <Elements xsi:type="usecase:VisualActorElement" Location="383,103" Bounds="80,150" Parent="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="Reader" visualID="_9ZRGtP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="497,145" Bounds="150,50" Parent="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="setup account" visualID="_9ZRGtf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="495,251" Bounds="150,50" Parent="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.5/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="use registration guide" visualID="_9ZRGtv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZRGsf5YEeSzkfJMf5_daA" Target="_9ZRGs_5YEeSzkfJMf5_daA" Direction="SourceTarget" LineStyle="Dashed" TargetDecoration="ArrowHead" Name="Include" Type="Include">
              <Bendpoints>320,265</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZQfoP5YEeSzkfJMf5_daA" Target="_9ZRGsf5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZRGtv5YEeSzkfJMf5_daA" Target="_9ZRGtf5YEeSzkfJMf5_daA" Direction="SourceTarget" LineStyle="Dashed" TargetDecoration="ArrowHead" Name="Extend" Type="Extend"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZRGtP5YEeSzkfJMf5_daA" Target="_9ZRGtf5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NSM5" name="NSM5" elementKind="" description="Librarians may define preferred media manually or by automatically created queries to appear as suggestions to readers when doing their search. " timeCreated="2011-12-09T10:34:25.214+0100" lastModified="2017-01-17T19:34:37.422+0100" uniqueID="e9b02eba-eba5-4d60-82b1-f8885c39fe64" workPackage="" abstractionLevel="" id="NSM5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.421+0100" uniqueID="8af73bb8-3217-46b5-ad49-751571c536ad" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:34:36.341+0100" uniqueID="40015501-b4ce-4c80-bf49-a9294377138e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:34:36.342+0100" uniqueID="321ab487-f318-4dcd-8910-62b49f29ac50">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="NSM5uc" name="NSM5uc" elementKind="unspecified" description="" timeCreated="2015-04-19T16:48:57.385+0200" lastModified="2017-01-17T19:34:37.432+0100" uniqueID="96ff38ad-c38b-41cd-a4c0-5fb53435afa9" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.432+0100" uniqueID="e65d130f-dca9-4b8e-8c75-640a00b31e83" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T17:12:35.124+0200" uniqueID="d86ca494-4f58-449d-b7e7-c89f86826f37">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T17:12:35.126+0200" uniqueID="d1e10ea3-2c4d-484a-a858-86e2ce35e885">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.1/@contents.3/@contents.7/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="67,82" Bounds="80,150" Parent="//@contents.0/@contents.1/@contents.3/@contents.7/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.7/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.7/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Librarian" visualID="_9ZTi8P5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="239,83" Bounds="200,200" Parent="//@contents.0/@contents.1/@contents.3/@contents.7/@VisualDiagram" Diagram="//@contents.0/@contents.1/@contents.3/@contents.7/@VisualDiagram" IsSketchy="true" Name="Catalog" visualID="_9ZTi8f5YEeSzkfJMf5_daA">
              <Elements xsi:type="usecase:VisualUseCaseElement" Location="21,59" Bounds="150,50" Parent="_9ZTi8f5YEeSzkfJMf5_daA" Diagram="//@contents.0/@contents.1/@contents.3/@contents.7/@VisualDiagram" Connections="//@contents.0/@contents.1/@contents.3/@contents.7/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="add suggestions" visualID="_9ZTi8v5YEeSzkfJMf5_daA"/>
            </Elements>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZTi8P5YEeSzkfJMf5_daA" Target="_9ZTi8v5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Leases" timeCreated="2011-12-09T15:11:01.119+0100" lastModified="2017-01-17T19:34:38.395+0100" uniqueID="12da6f84-92c0-429d-850c-f21bad723c7a">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.765+0100" uniqueID="2c9b11ce-88f7-49c7-a0f1-fba983671e5b" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Reservation" timeCreated="2011-12-07T15:48:05.366+0100" lastModified="2017-01-17T19:34:38.029+0100" uniqueID="1dee8b92-1d48-4b90-9e2d-bec997194233">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.901+0100" uniqueID="34755619-2fb2-49cc-8a30-4c942768e2d8" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="RES1" name="RES1" elementKind="" description="Users may reserve media from the catalog. Reserved media may only be lent by the holder of the reservation." timeCreated="2011-12-09T10:35:29.031+0100" lastModified="2017-01-17T19:34:37.912+0100" uniqueID="1d83f68a-7303-4079-b499-d1cc9105c606" workPackage="" abstractionLevel="" id="RES1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.912+0100" uniqueID="5ac5a487-240e-43bc-ba18-7ffc0c2261a8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:36:00.620+0100" uniqueID="8d7b025f-02b2-4465-a477-e4005deec726">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:36:00.621+0100" uniqueID="48069e58-fb70-482e-a4a9-8e9d4dc7aebd">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES2" name="RES2" elementKind="" description="A reader may not hold more than one reservation for the same medium at any time." timeCreated="2011-12-09T10:36:19.984+0100" lastModified="2017-01-17T19:34:37.923+0100" uniqueID="18643c4c-d88c-479e-91d1-a9d96d8a0871" workPackage="" abstractionLevel="" id="RES2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.922+0100" uniqueID="3d6f277d-65c0-491d-87aa-ca7e30b6ce89" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:36:24.601+0100" uniqueID="0bab19ef-f90f-4d57-b8ef-e72e4113f0ee">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:36:24.602+0100" uniqueID="1960d8b9-9f5d-49f9-8082-475c5870ffb6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES3" name="RES3" elementKind="" description="Reserving a medium grants the reserver the right of lending that medium as soon as the previous reserver returns it." timeCreated="2011-12-09T10:36:43.518+0100" lastModified="2017-01-17T19:34:37.933+0100" uniqueID="929d7565-d38b-4b12-8b6d-178ce6b9764c" workPackage="" abstractionLevel="" id="RES3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.933+0100" uniqueID="bee6ec7f-73de-44ab-9ac7-17339b6ca9f7" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:37:11.644+0100" uniqueID="6642c7bd-2a71-45c7-a9b1-2af8060cd479">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:37:11.645+0100" uniqueID="dfd25a80-f517-4ab0-a4e0-36536cd3012f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES4" name="RES4" elementKind="" description="All reservations for a medium are satisfied in a first-come-first serve-strategy." timeCreated="2011-12-09T10:37:23.492+0100" lastModified="2017-01-17T19:34:37.944+0100" uniqueID="630a9627-0dca-487d-9efc-68748fa9a0f3" workPackage="" abstractionLevel="" id="RES4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.943+0100" uniqueID="63e16c31-643e-45eb-b5b0-010a25a5f141" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:37:40.043+0100" uniqueID="b1dfee25-6b55-40fb-b7a3-664511d1fa4f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:37:40.043+0100" uniqueID="9f9be4c9-f704-4dad-a401-30d2deca9b4e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES5" name="RES5" elementKind="" description="If a reader tries to lend a reserved medium without holding the first reservation for this medium, the librarian will be notified at once." timeCreated="2011-12-09T10:38:19.589+0100" lastModified="2017-01-17T19:34:37.954+0100" uniqueID="6464d4a6-b358-43ff-83ca-543d33f0765c" workPackage="" abstractionLevel="" id="RES5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.954+0100" uniqueID="ff81c6a1-d551-4bf5-b3e4-bea3059fcc97" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:38:37.311+0100" uniqueID="ae01f126-534d-4bbd-b233-707662e564ed">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:38:37.312+0100" uniqueID="468226c6-d1e0-4f5d-b20f-651472697841">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES6" name="RES6" elementKind="" description="When a reserved medium becomes available, the reader with the earliest reservation is notified." timeCreated="2011-12-09T10:38:58.662+0100" lastModified="2017-01-17T19:34:37.965+0100" uniqueID="1bc93a57-1552-4f06-9e38-c8a431a825d2" workPackage="" abstractionLevel="" id="RES6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.964+0100" uniqueID="d40df589-c864-43cd-97fb-4d2588f85c33" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:39:19.386+0100" uniqueID="92a7e7f1-7c7d-4405-b5dd-e32e378bf983">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:39:19.389+0100" uniqueID="af73fdfe-7dbd-4819-b0db-8e0a4f8a5ef6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES7" name="RES7" elementKind="" description="If a reserved medium is not leased by the reader holding the earliest reservation for that medium within a customizable period after the medium becomes available, the reservation defaults." timeCreated="2011-12-09T10:39:31.325+0100" lastModified="2017-01-17T19:34:37.975+0100" uniqueID="425c11b0-8a9f-43cf-8eb2-541d315245cc" workPackage="" abstractionLevel="" id="RES7">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.975+0100" uniqueID="e1f43174-5c68-4de3-a36e-a3ffd48fa22f" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:39:42.505+0100" uniqueID="67820fa1-bcc3-40c5-92c0-158f14a39b4c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:39:42.508+0100" uniqueID="de3e5cb9-36b1-4517-b82e-c632807a10ee">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES8" name="RES8" elementKind="" description="If a reservation defaults, the reader holding the defaulted reservation may be charged a customizable amount of fees." timeCreated="2011-12-09T10:39:59.211+0100" lastModified="2017-01-17T19:34:37.986+0100" uniqueID="e18dd2e8-9444-463f-950b-20b3027045a7" workPackage="" abstractionLevel="" id="RES8">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.985+0100" uniqueID="85fae6b3-1b82-4b22-8260-6b52541340fa" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:40:12.138+0100" uniqueID="57ba688e-6cb5-4052-854e-c34d4cb1bd23">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:40:12.143+0100" uniqueID="69373fae-9f3a-45ec-baf0-2b8b990838a0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES9" name="RES9" elementKind="" description="If a reservation defaults, it is removed from the list of reservations for the medium it refers to and the medium enter the state available if no other reservations are pending." timeCreated="2011-12-09T10:40:28.677+0100" lastModified="2017-01-17T19:34:37.997+0100" uniqueID="38ea26e6-a9bf-42c2-a6ae-1aac4b08a8e7" workPackage="" abstractionLevel="" id="RES9">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:37.996+0100" uniqueID="8fb856a7-b5ad-4998-819d-4707f8c97498" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:40:40.165+0100" uniqueID="f6b2efb1-9d40-4760-8143-2b07a7710709">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:40:40.170+0100" uniqueID="6cd3b5e4-59f1-42cc-a5d9-fc65d0abe32a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES10" name="RES10" elementKind="" description="A reader may release his reservation before it defaults." timeCreated="2011-12-09T10:40:56.284+0100" lastModified="2017-01-17T19:34:38.007+0100" uniqueID="93cc4f21-5073-404a-a225-37eb9ee8af47" workPackage="" abstractionLevel="" id="RES10">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.007+0100" uniqueID="e887f559-1cd9-4444-b59f-163ce616e3fa" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:41:11.554+0100" uniqueID="7e5e73a0-0a1b-478d-b299-b7072b9c8829">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:41:11.556+0100" uniqueID="0c83358e-61fc-4ad8-b88c-ff5ca6186386">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RES11" name="RES11" elementKind="" description="If a reserved medium is leased by the reader holding the reservation, the reservation is removed." timeCreated="2011-12-09T10:41:28.498+0100" lastModified="2017-01-17T19:34:38.018+0100" uniqueID="e5e0e2fb-d760-48d4-9e80-786ed8a2a9f0" workPackage="" abstractionLevel="" id="RES11">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.017+0100" uniqueID="797d8333-fcf5-4ac2-9238-4a4a922b348f" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:41:38.495+0100" uniqueID="28710ec2-41a4-4d7b-b33e-dbe8e2e2b961">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:41:38.497+0100" uniqueID="1ba4509a-8af9-4637-abb7-a801d0da53d8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="RES9sm" name="RES9sm" elementKind="unspecified" description="" timeCreated="2015-04-19T17:13:53.058+0200" lastModified="2017-01-17T19:34:38.029+0100" uniqueID="aa089a05-a4e5-4b29-bcab-1c9db7751441" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.028+0100" uniqueID="c5c916fc-323a-4303-aeb5-dec574553a17" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T17:15:44.704+0200" uniqueID="f0f9fdd6-1b52-470c-8bfb-77f2ba1c39ba">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T17:15:44.705+0200" uniqueID="af5c810b-4546-4db2-be89-9b5ac39f0272">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram">
            <Elements xsi:type="statemachine:VisualInitialState" Location="128,19" Bounds="30,30" Parent="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Diagram="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Connections="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" visualID="_9ZXNUP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="98,86" Bounds="93,28" Parent="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Diagram="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Connections="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="defaulted" visualID="_9ZXNUf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="96,176" Bounds="100,25" Parent="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Diagram="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Connections="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="removed" visualID="_9ZXNUv5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="98,242" Bounds="100,21" Parent="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Diagram="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Connections="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="available" visualID="_9ZXNU_5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualFinalState" Location="135,294" Bounds="30,30" Parent="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Diagram="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram" Connections="//@contents.0/@contents.2/@contents.0/@contents.11/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" visualID="_9ZXNVP5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZXNUP5YEeSzkfJMf5_daA" Target="_9ZXNUf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="reservation defaults" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZXNUf5YEeSzkfJMf5_daA" Target="_9ZXNUv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="removes from list" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZXNUv5YEeSzkfJMf5_daA" Target="_9ZXNU_5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="[no pending reservation]" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZXNU_5YEeSzkfJMf5_daA" Target="_9ZXNVP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="StateTransition"/>
          </VisualDiagram>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Lease Creation &amp; Prolongation" timeCreated="2011-12-07T15:48:36.519+0100" lastModified="2017-01-17T19:34:38.254+0100" uniqueID="9d06cf51-35c4-4f5e-83e3-98f9899dc470">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.144+0100" uniqueID="31974e31-4f52-463a-8c3d-a49281380cc3" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="LCP1" name="LCP1" elementKind="" description="A user may lease a copy or prolong a lease only if the reader account has the capabilities for doing so." timeCreated="2011-12-09T10:42:41.299+0100" lastModified="2017-01-17T19:34:38.155+0100" uniqueID="f3468bec-b766-4f29-b0d4-f75124d8a62c" workPackage="" abstractionLevel="" id="LCP1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.155+0100" uniqueID="cd16eb9d-57c4-4bae-85e3-512f1be9c928" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:42:54.564+0100" uniqueID="96e35d64-0154-4453-912c-b71f302e8b4a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:42:54.567+0100" uniqueID="2ee3fb64-13a3-46d0-86da-1ddfd62a322d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LCP2" name="LCP2" elementKind="" description="A user may lease a copy unless the copy is reserved for another reader. " timeCreated="2011-12-09T10:43:08.283+0100" lastModified="2017-01-17T19:34:38.166+0100" uniqueID="2dacb8b6-42e5-4c64-95d4-3f93c4129ee1" workPackage="" abstractionLevel="" id="LCP2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.166+0100" uniqueID="03ac91ed-a4de-4207-9514-51a281bbc6ac" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:43:33.726+0100" uniqueID="ba9518a7-dc61-498c-8100-698ce1457909">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:43:33.727+0100" uniqueID="483b2f65-4841-4c62-8236-067664918b45">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LCP3" name="LCP3" elementKind="" description="A user may prolong a lease a copy unless the copy is reserved for another reader or the maximum number of prolongations is reached." timeCreated="2011-12-09T10:44:07.779+0100" lastModified="2017-01-17T19:34:38.177+0100" uniqueID="79540444-2edb-491d-a94c-fe3036855318" workPackage="" abstractionLevel="" id="LCP3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.176+0100" uniqueID="39dd9176-2aec-478f-b9b4-4df554e5dece" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:46:23.090+0100" uniqueID="0fb6eb53-6fb9-4760-afb3-c0307931badd">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:46:23.093+0100" uniqueID="ab962c41-ebbb-4465-865d-d0afc690da45">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LCP4" name="LCP4" elementKind="" description="Repeated prolonging of the loan period of a medium may be limited by a customizable threshold." timeCreated="2011-12-09T10:46:47.714+0100" lastModified="2017-01-17T19:34:38.187+0100" uniqueID="bc617e38-f4ef-40ec-9ac4-4015bada68cb" workPackage="" abstractionLevel="" id="LCP4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.187+0100" uniqueID="4f028394-26c4-47f5-b905-755e647529eb" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:47:10.854+0100" uniqueID="3d9478c9-dddb-404b-a108-b1c3a9aca4b7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:47:10.856+0100" uniqueID="420f2a8a-5a86-434f-93d8-53219a8f6a71">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LCP5" name="LCP5" elementKind="" description="Leases may only be prolonged for the reader holding the lease." timeCreated="2011-12-09T10:47:34.240+0100" lastModified="2017-01-17T19:34:38.198+0100" uniqueID="e7badb39-2c8d-4877-858c-3b8472aefb53" workPackage="" abstractionLevel="" id="LCP5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.197+0100" uniqueID="937b60cb-0148-40e2-a445-4d2cc09b1fc2" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:47:38.997+0100" uniqueID="a4e91a39-4f8a-4794-a945-15702961563d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:47:38.999+0100" uniqueID="ec1c0a10-0c34-4299-8c45-dc0bcb460d79">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LCP6" name="LCP6" elementKind="" description="If a copy is leased or prolonged, the state of the copy and the reader’s account are updated accordingly within 5s." timeCreated="2011-12-09T10:47:52.241+0100" lastModified="2017-01-17T19:34:38.208+0100" uniqueID="aa425d18-0432-4c3e-a192-cabd2278a65a" workPackage="" abstractionLevel="" id="LCP6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.208+0100" uniqueID="d167d8ca-44b6-4dac-ac98-d8b76ba07ce7" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:48:06.216+0100" uniqueID="e4ef1330-57a6-4e5a-af9e-315c9af0b4d2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:48:06.218+0100" uniqueID="ab45abd3-e515-4901-840b-f72167d464a1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LCP7" name="LCP7" elementKind="" description="LMS computes the lease period automatically based on the lender’s account type." timeCreated="2011-12-09T10:48:59.153+0100" lastModified="2017-01-17T19:34:38.219+0100" uniqueID="7ca69604-26a9-4637-8f70-a20d91f91475" workPackage="" abstractionLevel="" id="LCP7">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.219+0100" uniqueID="d9b4f3b0-024f-4296-8a0d-1123c9365174" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:49:03.008+0100" uniqueID="746ec08c-cdcf-4cbd-a399-0c0514517914">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:49:03.009+0100" uniqueID="e2856416-9379-4afc-8cfd-b42da2f01733">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LCP8" name="LCP8" elementKind="" description="Librarians may manually modify the state of a lease, prolongation, or reservation." timeCreated="2011-12-09T10:49:25.088+0100" lastModified="2017-01-17T19:34:38.230+0100" uniqueID="a2c6f466-9021-4d14-95eb-9993fb58f170" workPackage="" abstractionLevel="" id="LCP8">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.229+0100" uniqueID="acde2769-4b48-437c-a8e3-4769eb47e115" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:49:36.026+0100" uniqueID="183ba3e7-5206-4b28-8e5c-814eb3e03cae">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:49:36.027+0100" uniqueID="7bd4ef93-fe48-4947-8810-3852a2cae38f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LCP9" name="LCP9" elementKind="" description="Lending and returning of batches of media must be supported such that no more than 15 seconds are needed to deal with 10 media items." timeCreated="2011-12-09T10:49:57.074+0100" lastModified="2017-01-17T19:34:38.241+0100" uniqueID="0e61df59-ade3-4561-b9b4-ce145f2f04db" workPackage="" abstractionLevel="" id="LCP9">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.241+0100" uniqueID="44c0cf2a-05aa-448a-a3c3-3be68aefaa8f" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:50:18.698+0100" uniqueID="edaf6308-0a48-41e7-b8aa-4e16fdba3589">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:50:18.699+0100" uniqueID="7e2f01d2-358d-452c-9a75-f62e4641dba0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="LCP3sm" name="LCP3sm" elementKind="unspecified" description="" timeCreated="2015-04-19T17:16:27.654+0200" lastModified="2017-01-17T19:34:38.254+0100" uniqueID="3dea9768-9d3b-47d5-9ec0-c24177e4be5d" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.253+0100" uniqueID="7aedb430-1f04-40b7-a976-6cfffc3b7103" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T17:18:00.281+0200" uniqueID="f4cbc3db-99ec-4db0-ad22-749910dfed78">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T17:18:00.283+0200" uniqueID="ba72283d-a1be-454f-916b-a5969ab0d12e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram">
            <Elements xsi:type="statemachine:VisualState" Location="140,134" Bounds="100,25" Parent="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram" Connections="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="Unknown1" visualID="_9ZZpkP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="279,44" Bounds="100,25" Parent="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram" Connections="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="Unknown2" visualID="_9ZZpkf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="271,204" Bounds="100,22" Parent="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram" Diagram="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram" Connections="//@contents.0/@contents.2/@contents.1/@contents.9/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="Unknown3" visualID="_9ZZpkv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZZpkP5YEeSzkfJMf5_daA" Target="_9ZZpkf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="lease[#available copies>#reservations]" Type="StateTransition"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZZpkP5YEeSzkfJMf5_daA" Target="_9ZZpkv5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="prolog[#available copies> # reservations]" Type="StateTransition"/>
          </VisualDiagram>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Lease Termination" timeCreated="2011-12-07T15:48:58.773+0100" lastModified="2017-01-17T19:34:38.395+0100" uniqueID="130fa138-d2c8-44e5-963f-856d1b566a96">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.328+0100" uniqueID="64ffe173-8b0c-4702-8070-d6c15e40023d" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="LET1" name="LET1" elementKind="" description="If a leased copy is returned to the library, its current lease will be terminated." timeCreated="2011-12-09T10:53:00.208+0100" lastModified="2017-01-17T19:34:38.339+0100" uniqueID="a3a066a3-7106-48ba-ac3d-42d81e903df7" workPackage="" abstractionLevel="" id="LET1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.339+0100" uniqueID="cf170848-61cd-403d-ae1b-2be5c30aca82" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:53:29.715+0100" uniqueID="fedbf800-abe1-48fc-bef0-80e96b04ed6b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:53:29.716+0100" uniqueID="f6296d89-b499-477f-bd14-3e1a864c2b90">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LET2" name="LET2" elementKind="" description="If an overdue copy is returned to the library, the overdue days are indicated to the user within 2s. " timeCreated="2011-12-09T10:53:47.259+0100" lastModified="2017-01-17T19:34:38.350+0100" uniqueID="6a6a848e-0450-44ca-9cf1-6afc33971c92" workPackage="" abstractionLevel="" id="LET2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.349+0100" uniqueID="a0ee4874-3c1d-412c-8d29-460afcec61e4" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:54:32.944+0100" uniqueID="6c8020e5-83dc-40a0-8b7a-b863cfb03aa9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:54:32.945+0100" uniqueID="89146859-9214-4da1-af08-0fad211d0b43">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LET3" name="LET3" elementKind="" description="When a lease is being terminated, LMS calculates any overdue fees, updates the reader's account accordingly, and notifies the user." timeCreated="2011-12-09T10:54:43.198+0100" lastModified="2017-01-17T19:34:38.363+0100" uniqueID="2d128c9d-503a-4168-af03-926c56060ea5" workPackage="" abstractionLevel="" id="LET3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.362+0100" uniqueID="bb5bec80-cc75-46c9-ac10-3f8d185ef473" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:54:54.452+0100" uniqueID="65439953-c149-43e6-8d9a-8f090a6e3d9a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:54:54.453+0100" uniqueID="8ac1231f-916a-43ba-b495-059e46898a3a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LET4" name="LET4" elementKind="" description="Librarians may terminate a lease manually." timeCreated="2011-12-09T10:55:14.493+0100" lastModified="2017-01-17T19:34:38.374+0100" uniqueID="2253acf5-c2b2-455b-9f0c-dbddedc4056b" workPackage="" abstractionLevel="" id="LET4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.373+0100" uniqueID="e916b431-4564-4483-afbb-bf1d8c14b28b" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:55:27.214+0100" uniqueID="2a091d8d-20c0-47c3-b60e-719367e7671e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:55:27.214+0100" uniqueID="0c60f5b7-c8a3-4f69-99ad-67d2de2184a9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LET5" name="LET5" elementKind="" description="If a reserved medium is returned, it becomes available to other readers immediately after the end of the transaction." timeCreated="2011-12-09T10:55:42.721+0100" lastModified="2017-01-17T19:34:38.384+0100" uniqueID="0759341c-d91b-4ce4-ab7f-5cc599a9f97f" workPackage="" abstractionLevel="" id="LET5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.384+0100" uniqueID="c1da647a-d9ba-4595-a25c-b3e541a241fc" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:56:00.535+0100" uniqueID="929dd328-2644-4842-9cf1-12fc0ec6e2a3">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:56:00.536+0100" uniqueID="e4fa4415-7d97-4e0a-986d-55fcbdb4d348">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LET6" name="LET6" elementKind="" description="When a copy is returned at the front desk, the librarian present there is notified within 1s if there is currently a reservation for the medium of the copy." timeCreated="2011-12-09T10:56:19.454+0100" lastModified="2017-01-17T19:34:38.395+0100" uniqueID="91c4dbf0-9eae-4591-a15d-4ea8ec705d87" workPackage="" abstractionLevel="" id="LET6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.395+0100" uniqueID="79874661-fb5e-4055-8ea2-58e35bf68cf4" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:56:29.295+0100" uniqueID="3b36cd98-3480-4a46-8422-9b103fd66945">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:56:29.296+0100" uniqueID="8d494a76-818e-4146-9f46-a01947a5a5a4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Accounts and Capabilities" timeCreated="2011-12-09T15:11:42.025+0100" lastModified="2017-01-17T19:34:40.009+0100" uniqueID="97672f30-dd50-4d35-84e5-5e4c7d9d039d">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:38.933+0100" uniqueID="bcdd253c-c79f-4a44-8a08-87a2162a9bec" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Reader Account" timeCreated="2011-12-07T15:49:21.604+0100" lastModified="2017-01-17T19:34:39.221+0100" uniqueID="ec97b3b1-33d9-46cc-b9e8-85022e59d6a2">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.081+0100" uniqueID="b79de5e3-3364-4429-a86b-95576fa2d509" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="RAC1" name="RAC1" elementKind="" description="A Librarian can set up a new reader account." timeCreated="2011-12-09T10:56:53.289+0100" lastModified="2017-01-17T19:34:39.093+0100" uniqueID="cf9a24a6-3a63-45dc-a61b-8bfb81c350a7" workPackage="" abstractionLevel="" id="RAC1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.092+0100" uniqueID="3668685f-384d-4bb3-b4ec-c846254632c0" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:57:06.064+0100" uniqueID="ce6be4a8-1c21-465e-abf9-848699744d1b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:57:06.065+0100" uniqueID="5bdde281-564a-4996-a6fc-7782c2d0c553">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RAC2" name="RAC2" elementKind="" description="A reader accounts stores name, birthday, reader ID, and portrait of the reader, contact data (e.g., address, email, phone), account type, leases and reservations, suggestions and comments, dates of set-up and expiry, and a history of previous actions." timeCreated="2011-12-09T10:57:23.326+0100" lastModified="2017-01-17T19:34:39.103+0100" uniqueID="12259d1e-8978-49ab-afef-0fb2eef52c4a" workPackage="" abstractionLevel="" id="RAC2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.103+0100" uniqueID="8de71195-34ff-4f8f-920b-9411d3f52d64" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:57:38.495+0100" uniqueID="cb3ca5f7-783f-4e5b-a19f-426f0d41a3c0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:57:38.496+0100" uniqueID="f655f4ef-b6d3-4cc2-93d0-0029248c727b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RAC3" name="RAC3" elementKind="" description="If a reader account expires, or if certain policies or thresholds are violated, the account is deactivated automatically." timeCreated="2011-12-09T10:57:49.011+0100" lastModified="2017-01-17T19:34:39.114+0100" uniqueID="13fa5a92-35e2-46e5-a956-addc62dd0590" workPackage="" abstractionLevel="" id="RAC3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.113+0100" uniqueID="8919fe1a-a567-46fb-a388-c4f928c7e696" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:58:35.346+0100" uniqueID="38d1d2b2-1f78-4816-a0b9-aa8660f6e3c3">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:58:35.347+0100" uniqueID="77d5336c-e1a6-4872-b938-aafed53a5db2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="RAC3sm" name="RAC3sm" elementKind="unspecified" description="" timeCreated="2015-04-19T17:25:28.354+0200" lastModified="2017-01-17T19:34:39.125+0100" uniqueID="e63b4f95-7477-4e3b-9310-7893a8c2f9cb" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.124+0100" uniqueID="09045cd0-9b36-470e-8390-b5c44f514bb5" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T17:27:09.588+0200" uniqueID="9acf2804-ba2b-4b43-a78a-4432121675e5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T17:27:09.590+0200" uniqueID="dc105563-1e88-4f3e-a96f-612130c635c6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram">
            <Elements xsi:type="statemachine:VisualState" Location="45,142" Bounds="89,31" Parent="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="unknown" visualID="_9ZdT8P5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="statemachine:VisualState" Location="227,138" Bounds="92,32" Parent="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="deactivated" visualID="_9ZdT8f5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="visualmodel:VisualGenericElement" Location="28,48" Bounds="209,26" Parent="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.3/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="state before deactivation is unknown" visualID="_9ZdT8v5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZdT8P5YEeSzkfJMf5_daA" Target="_9ZdT8f5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="violation of policies" Type="StateTransition">
              <Bendpoints>178,203</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZdT8P5YEeSzkfJMf5_daA" Target="_9ZdT8f5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="expires" Type="StateTransition">
              <Bendpoints>178,105</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZdT8P5YEeSzkfJMf5_daA" Target="_9ZdT8v5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RAC4" name="RAC4" elementKind="" description="A librarian may deactivate a reader account manually." timeCreated="2011-12-09T10:58:47.252+0100" lastModified="2017-01-17T19:34:39.136+0100" uniqueID="c2130967-a3b3-4356-9f4c-611635f98189" workPackage="" abstractionLevel="" id="RAC4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.135+0100" uniqueID="c5a32bb3-f335-4445-8baa-4fe6e9986dda" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:59:06.232+0100" uniqueID="e169e9a8-6483-4450-9e05-f98aeca84e03">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:59:06.233+0100" uniqueID="a6ea7969-5361-445f-b5bb-cdf07470f344">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RAC5" name="RAC5" elementKind="" description="A deactivated reader account allows readers only payment of fees, reading and printing the account action trail, returning of copies, and termination of the account. " timeCreated="2011-12-09T10:59:25.760+0100" lastModified="2017-01-17T19:34:39.146+0100" uniqueID="a995072e-2307-411e-a5a9-8473d2de2cd3" workPackage="" abstractionLevel="" id="RAC5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.146+0100" uniqueID="3b421457-e32e-4f77-9730-b3fac5f025a0" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T10:59:47.941+0100" uniqueID="2dc4c40e-efbb-440e-a814-0cdc71fcf198">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:59:47.942+0100" uniqueID="ea39af99-dc47-4e10-9dbc-fa85295d367f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="RAC5uc" name="RAC5uc" elementKind="unspecified" description="" timeCreated="2015-04-19T17:29:50.364+0200" lastModified="2017-01-17T19:34:39.157+0100" uniqueID="cabb9d25-b5ac-4dec-b3d1-2b324f1f7736" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.156+0100" uniqueID="d7c3196f-0857-4241-82e4-c08bcbeca40d" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T17:31:17.036+0200" uniqueID="4a4a2a3f-23d9-49c3-96e9-7dd03e5de8bc">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T17:31:17.038+0200" uniqueID="cee08335-bdaf-4ebe-8031-d83ab24ca38d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram">
            <Elements xsi:type="usecase:VisualActorElement" Location="71,185" Bounds="80,150" Parent="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.2 //@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.3 //@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.4 //@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.5" IsSketchy="true" Name="Reader" visualID="_9ZfwMP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="213,115" Bounds="150,50" Parent="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="payment of fees" visualID="_9ZfwMf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="223,185" Bounds="150,50" Parent="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="read account action trail" visualID="_9ZfwMv5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="229,256" Bounds="150,50" Parent="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="print account action trail" visualID="_9ZfwM_5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="226,321" Bounds="150,50" Parent="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="return medium" visualID="_9ZgXQP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="usecase:VisualUseCaseElement" Location="227,391" Bounds="150,50" Parent="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.4" IsSketchy="true" Name="terminate account" visualID="_9ZgXQf5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="visualmodel:VisualGenericElement" Location="37,44" Bounds="309,50" Parent="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.6/@VisualDiagram/@DiagramConnections.5" IsSketchy="true" Name="reader's only functionalities with a deactivated account" visualID="_9ZgXQv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZfwMP5YEeSzkfJMf5_daA" Target="_9ZfwMf5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZfwMP5YEeSzkfJMf5_daA" Target="_9ZfwMv5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZfwMP5YEeSzkfJMf5_daA" Target="_9ZfwM_5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZfwMP5YEeSzkfJMf5_daA" Target="_9ZgXQP5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZfwMP5YEeSzkfJMf5_daA" Target="_9ZgXQf5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZgXQv5YEeSzkfJMf5_daA" Target="_9ZfwMP5YEeSzkfJMf5_daA"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="RAC5sm" name="RAC5sm" elementKind="unspecified" description="" timeCreated="2015-04-19T17:27:37.792+0200" lastModified="2017-01-17T19:34:39.168+0100" uniqueID="3c32eb0c-dfb1-4b93-83c6-4f3271993f80" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.167+0100" uniqueID="8f4b171b-8348-4f05-852b-89de522e2417" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T17:29:18.759+0200" uniqueID="a8d65620-338c-43f2-b66e-3b60d87e5db8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T17:29:18.761+0200" uniqueID="60d485f1-02c5-4d1a-a20d-91b160a3dd66">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.3/@contents.0/@contents.7/@VisualDiagram">
            <Elements xsi:type="statemachine:VisualState" Location="101,92" Bounds="205,33" Parent="//@contents.0/@contents.3/@contents.0/@contents.7/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.0/@contents.7/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.0/@contents.7/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.3/@contents.0/@contents.7/@VisualDiagram/@DiagramConnections.1 //@contents.0/@contents.3/@contents.0/@contents.7/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="deactivated" visualID="_9ZhlYP5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZhlYP5YEeSzkfJMf5_daA" Target="_9ZhlYP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="return copies" Type="StateTransition">
              <Bendpoints>149,185</Bendpoints>
              <Bendpoints>212,191</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZhlYP5YEeSzkfJMf5_daA" Target="_9ZhlYP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="pay fees" Type="StateTransition">
              <Bendpoints>246,65</Bendpoints>
              <Bendpoints>291,63</Bendpoints>
              <Bendpoints>285,82</Bendpoints>
            </DiagramConnections>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZhlYP5YEeSzkfJMf5_daA" Target="_9ZhlYP5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="read and print account trial" Type="StateTransition">
              <Bendpoints>97,67</Bendpoints>
              <Bendpoints>168,44</Bendpoints>
              <Bendpoints>213,44</Bendpoints>
            </DiagramConnections>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RAC6" name="RAC6" elementKind="" description="A librarian may trigger all kinds actions of on behalf of a reader with a deactivated account." timeCreated="2011-12-09T11:00:10.834+0100" lastModified="2017-01-17T19:34:39.178+0100" uniqueID="6e83b44c-a711-4032-bb4a-3fb004c01854" workPackage="" abstractionLevel="" id="RAC6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.178+0100" uniqueID="33b0e16a-d42d-48a1-b300-70c92e09e93d" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T11:00:21.610+0100" uniqueID="eb4d370e-3fa7-414d-9cc6-c05c3aba8f8a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T11:00:21.611+0100" uniqueID="7e991e73-4467-4ef0-9e87-b052bf532075">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RAC7" name="RAC7" elementKind="" description="A deactivated account can only be manually reactivated by a librarian." timeCreated="2011-12-09T11:01:17.182+0100" lastModified="2017-01-17T19:34:39.189+0100" uniqueID="2457b297-690c-409b-bf0d-99d003c1b7f9" workPackage="" abstractionLevel="" id="RAC7">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.189+0100" uniqueID="3f5cdbbc-a115-400f-8cdd-d4bda385a3d1" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T11:01:33.870+0100" uniqueID="324f6eca-4afb-4ac4-ac3a-0a70bf707014">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T11:01:33.872+0100" uniqueID="21f18be7-2b41-4e12-9b22-64a0e56eedf9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RAC8" name="RAC8" elementKind="" description="A librarian may perform actions on behalf of a reader." timeCreated="2011-12-09T11:01:53.016+0100" lastModified="2017-01-17T19:34:39.200+0100" uniqueID="92031f3a-3d51-4aae-b035-6e625ee8d6a6" workPackage="" abstractionLevel="" id="RAC8">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.199+0100" uniqueID="a6a754ea-f8f9-4c56-a178-1d98794614b6" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T11:02:07.609+0100" uniqueID="c2e5cb3e-bae0-4596-b99c-0f54e1377d12">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T11:02:07.610+0100" uniqueID="a832130f-5dcf-41ec-b282-040821193493">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RAC9" name="RAC9" elementKind="" description="Readers are notified about actions initiated on their behalf." timeCreated="2011-12-09T11:02:29.894+0100" lastModified="2017-01-17T19:34:39.211+0100" uniqueID="4f78753e-dac8-4cc1-90e6-f41ea403fcce" workPackage="" abstractionLevel="" id="RAC9">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.210+0100" uniqueID="2f3ab02d-44b9-495d-9bd9-30f06c834de8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T11:02:46.533+0100" uniqueID="ca2d51d6-e2bf-42e4-8af3-6bfc11533053">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T11:02:46.534+0100" uniqueID="3f725e86-6e4f-42ff-847f-5d57765abae7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="RAC10" name="RAC10" elementKind="" description="A reader account may be deactivated, active, or closed." timeCreated="2011-12-09T12:37:33.293+0100" lastModified="2017-01-17T19:34:39.221+0100" uniqueID="17e4fe4c-2a85-4fe3-b9ab-e423bd1d5233" workPackage="" abstractionLevel="" id="RAC10">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.221+0100" uniqueID="ba3ef013-7414-4301-92eb-49c3a57834ce" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T12:37:38.218+0100" uniqueID="1ec7aeaa-062b-45e7-adac-1f16dd05f7f7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:37:38.219+0100" uniqueID="3be7f606-1d38-4cff-b2c0-aa06129291b1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Fees &amp; Fines" timeCreated="2011-12-07T15:49:57.300+0100" lastModified="2017-01-17T19:34:39.517+0100" uniqueID="948bcdf7-d8d7-44c8-a689-8bc1080b7ea7">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.373+0100" uniqueID="42d21f8e-e1de-418f-af9c-ccd66b5f2691" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="FAF1" name="FAF1" elementKind="" description="All actions that change the damage state of a copy, terminate a lease or a reservation, or that change the state of a reader account trigger a calcu¬lat¬ion of due fees and update the fee balance of the respective reader." timeCreated="2011-12-09T12:38:07.958+0100" lastModified="2017-01-17T19:34:39.384+0100" uniqueID="ae9c0ab1-ca23-40c6-8458-66dc85a7e12e" workPackage="" abstractionLevel="" id="FAF1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.384+0100" uniqueID="262dace2-b76b-47a4-89b6-1256904ef30f" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T12:38:18.444+0100" uniqueID="bb8740d5-941d-47d4-a904-3c6b46e769a1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:38:18.445+0100" uniqueID="ce665c42-5213-4032-a951-be9a2c1b94fb">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF1a" name="FAF1a" elementKind="" description="If a fee calculation is triggered, all other pending actions on the account are completed first." timeCreated="2011-12-09T12:39:49.099+0100" lastModified="2017-01-17T19:34:39.395+0100" uniqueID="7684868e-6b2c-4027-bc56-0aa9a50a11f9" workPackage="" abstractionLevel="" id="FAF1a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.395+0100" uniqueID="87d78347-39ed-41b6-b21b-5319a501fdcc" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T12:40:01.187+0100" uniqueID="eef88244-ac6c-41e3-8741-6985df385d08">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:40:01.188+0100" uniqueID="f4ebc5c9-5290-48fd-bf59-b91149e1184b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF1b" name="FAF1b" elementKind="" description="If a fee calcu¬lat¬ion yields a non-zero result, the fee balance of the respective reader is updated and checked for violation of thresholds and policies." timeCreated="2011-12-09T12:40:28.575+0100" lastModified="2017-01-17T19:34:39.406+0100" uniqueID="be3829c2-bbc0-4cb7-a235-b3b01c6bb130" workPackage="" abstractionLevel="" id="FAF1b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.406+0100" uniqueID="40670647-66c6-4dd4-80b3-34e6f8e21ab3" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T12:40:42.399+0100" uniqueID="cb1d5deb-9e99-468a-b58f-70adf50333d7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:40:42.403+0100" uniqueID="df666ffa-4f2a-41da-8cc3-f19002422449">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF1c" name="FAF1c" elementKind="" description="If the balance of a reader account changes, changing the account state is considered immediately (i.e., deactivation or reactivation)." timeCreated="2011-12-09T12:41:07.399+0100" lastModified="2017-01-17T19:34:39.417+0100" uniqueID="0b3ed269-6494-4aed-8bd2-fefd408231b9" workPackage="" abstractionLevel="" id="FAF1c">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.416+0100" uniqueID="610dc347-db0e-429c-89ea-204eec33271b" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T12:41:13.882+0100" uniqueID="b4baf5b9-fd5d-461f-b690-f54874a4cb88">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:41:13.883+0100" uniqueID="5346abaa-5e52-4cd1-8223-24ef2d8d9d61">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF1d" name="FAF1d" elementKind="" description="If a fee calculation yields a non-zero result, the reader is notified of the computation, its result, and provided with an explanation." timeCreated="2011-12-09T12:42:11.830+0100" lastModified="2017-01-17T19:34:39.427+0100" uniqueID="2d3fa2bf-4a7b-4415-9844-c4ef156901cb" workPackage="" abstractionLevel="" id="FAF1d">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.427+0100" uniqueID="5865b7e5-98be-4eb1-817e-b52eeed9afab" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T12:42:53.479+0100" uniqueID="6eee30b3-8c86-40eb-9190-6936cbce9e91">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:42:53.480+0100" uniqueID="2bf52848-f825-4ec9-bd12-b9f71974c896">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF2" name="FAF2" elementKind="" description="The due fees for account A are calculated as  FL + FR + FC + FA over all leases, reservations, and damages, where: FL is the fee for overdue leases (see FAF.2a); FR is the fee for reservations; FC is the fine for damaging a copy; FA are account specific fees." timeCreated="2011-12-09T12:43:13.774+0100" lastModified="2017-01-17T19:34:39.438+0100" uniqueID="db00b487-a439-4af8-bfbe-69ba83fd29cd" workPackage="" abstractionLevel="" id="FAF2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.438+0100" uniqueID="38778827-7174-4496-8df7-b192ed2ef403" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T12:48:17.006+0100" uniqueID="cd1407cf-bc35-41bb-913a-b3406fd50969">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:48:17.008+0100" uniqueID="45bab70f-183c-45db-b787-f583ca5c549e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF2a" name="FAF2a" elementKind="" description="The fee FL for an individual overdue lease l is computed as dl * t(m) *  al(r), where: dl is the number of days today and the due date of l minus an offset; t(m) is a customizable factor depending on the type of the medium m; and al(r) is a customizable factor for the account type of the reader holding the lease." timeCreated="2011-12-09T12:48:37.730+0100" lastModified="2017-01-17T19:34:39.449+0100" uniqueID="9da28b52-7af0-4c11-bb11-7d624cfb93d5" workPackage="" abstractionLevel="" id="FAF2a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.449+0100" uniqueID="5948a191-c195-4bac-aa00-974c1b3e42d6" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T12:49:37.494+0100" uniqueID="e8c66069-47e1-4610-889d-c9b0bcf13b4c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:49:37.495+0100" uniqueID="6296ca18-7cfe-4031-bfa5-671840118d78">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF2b" name="FAF2b" elementKind="" description="The fee FR for an individual reservation is computed as (rr * tr(m) + rc * tc(m))* ar(r), where: &#xD;&#xA;&#x9;rr and rc are the required and cancelled/defaulted reservations, respectively;&#xD;&#xA;&#x9;tr(m) and tc(m) are customizable factors depending on the type of the medium m;&#xD;&#xA;&#x9;ar(r) is a customizable factor for the account type of the reader. &#xD;&#xA;" timeCreated="2011-12-09T12:55:32.523+0100" lastModified="2017-01-17T19:34:39.460+0100" uniqueID="43dbe2cc-6d62-4bc8-9e7e-a9fedfaa62ec" workPackage="" abstractionLevel="" id="FAF2b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.459+0100" uniqueID="4a58b17c-dd4e-4414-aed3-1d656223a1e5" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T12:57:56.797+0100" uniqueID="b520b96c-92e3-4d33-b5e6-60c594e5c48a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:57:56.798+0100" uniqueID="a142e6fd-0a7b-4d9d-a8fa-3bdbe5022c53">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF2c" name="FAF2c" elementKind="" description="The fee FC for damages to copies is assessed individually by a librarian." timeCreated="2011-12-09T12:59:10.648+0100" lastModified="2017-01-17T19:34:39.471+0100" uniqueID="8148cf66-058d-4025-b648-07980f30c3a0" workPackage="" abstractionLevel="" id="FAF2c">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.470+0100" uniqueID="a4d6aeee-5152-4600-acb5-c4135c097c0d" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:04:29.113+0100" uniqueID="552cc506-1689-460e-8abe-119d10ccf4b8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:04:29.117+0100" uniqueID="73c2c939-cdc1-4335-ba51-1638c97e3cc6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF2d" name="FAF2d" elementKind="" description="The fee FA for a reader account is computed as td + tu (x,y) + rc(y), where &#xD;&#xA;&#x9;td is a customizable administration fee applicable if an account is deactivated;&#xD;&#xA;&#x9;tu is the upgrade fee that is applicable when upgrading an account from status x to status y (x>=y; x=y is the yearly base rate for the account);&#xD;&#xA;&#x9;rc(y) ist the administrative fee for creating an account of status y. &#xD;&#xA;" timeCreated="2011-12-09T13:10:20.088+0100" lastModified="2017-01-17T19:34:39.481+0100" uniqueID="fa5ea99a-267b-492b-815c-62fa925c1717" workPackage="" abstractionLevel="" id="FAF2d">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.481+0100" uniqueID="0d5ee4ec-6218-4af5-bc95-447a30891847" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:10:32.001+0100" uniqueID="7fffd148-a3f2-42af-adc1-40715eb31fd3">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:10:32.004+0100" uniqueID="10ead9e0-d1d9-4835-a3e1-9edc1af992ad">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF3" name="FAF3" elementKind="" description="A reader can settle the balance of his account by paying the outstanding fees." timeCreated="2011-12-09T13:10:51.738+0100" lastModified="2017-01-17T19:34:39.493+0100" uniqueID="ae2ffed5-028b-42d6-9875-6da8528bb3c5" workPackage="" abstractionLevel="" id="FAF3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.493+0100" uniqueID="78c3fef7-d454-4507-9c34-9ad724ffec68" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:11:18.558+0100" uniqueID="2ff1d0c3-3ced-4918-8dda-c2cf1d8e8f35">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:11:18.561+0100" uniqueID="8b2c4e66-a919-4535-a71a-8b814a7b0b00">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF3a" name="FAF3a" elementKind="" description="If an account is deactivated, it can only be reactivated after complete settlement of all fees." timeCreated="2011-12-09T13:11:32.219+0100" lastModified="2017-01-17T19:34:39.504+0100" uniqueID="b4d8c544-5080-475a-87c5-121cbacccce5" workPackage="" abstractionLevel="" id="FAF3a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.503+0100" uniqueID="8c54bbfb-4fa1-4e10-bf8f-40f86c74acf9" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:11:42.761+0100" uniqueID="2cc4f60e-8455-49e3-a66e-399dd978b6a5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:11:42.765+0100" uniqueID="0e87da1b-f229-4f41-a31b-60a56af5cf0f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="FAF3b" name="FAF3b" elementKind="" description="If a reader pays fees, the payment is recorded and the reader receives a paper receipt about the payment." timeCreated="2011-12-09T13:11:54.232+0100" lastModified="2017-01-17T19:34:39.517+0100" uniqueID="5304df70-c922-428f-889d-242e17e15881" workPackage="" abstractionLevel="" id="FAF3b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.516+0100" uniqueID="c418c74b-61fa-4ffa-8b3d-28c2e89cfae8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:12:07.795+0100" uniqueID="c78b0d5e-ffd2-455d-b1e2-69528f2a9ef1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:12:07.798+0100" uniqueID="0ca1ebb2-ff34-481e-b4ab-594c115d29fa">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Records &amp; Inspection" timeCreated="2011-12-07T15:50:24.272+0100" lastModified="2017-01-17T19:34:39.812+0100" uniqueID="025561fe-3a84-4182-80b5-9117cbac89cb">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.655+0100" uniqueID="11e1ee2a-07ca-49aa-a8ac-ea7526ac0841" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="REIN1" name="REIN1" elementKind="" description="All relevant actions on a reader account are recorded electronically." timeCreated="2011-12-09T13:13:04.952+0100" lastModified="2017-01-17T19:34:39.666+0100" uniqueID="2bd5db16-fe65-48e4-a176-d22de1560150" workPackage="" abstractionLevel="" id="REIN1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.665+0100" uniqueID="f5028378-286a-4b0f-ac2a-3509290ae485" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:13:40.691+0100" uniqueID="ea94377b-798e-4140-82da-1dc57908106e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:13:40.694+0100" uniqueID="1cbd68cc-2e31-4f00-8b6f-4138bf1796ea">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="REIN1cl" name="REIN1cl" elementKind="unspecified" description="" timeCreated="2015-04-19T17:32:46.232+0200" lastModified="2017-01-17T19:34:39.697+0100" uniqueID="4f0c605e-6d87-47bc-ae58-602b07d24271" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.697+0100" uniqueID="d97f5fbe-e77c-499e-a608-6ec44bdc7aec" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T17:33:48.459+0200" uniqueID="b825fb5b-7650-4915-9d61-7d1d2a4f9dde">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T17:33:48.461+0200" uniqueID="0cfc832b-9298-4b20-b077-9bbfc20a6bd4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.3/@contents.2/@contents.1/@VisualDiagram" DiagramType="Class">
            <Elements xsi:type="class:VisualClassElement" Location="102,112" Bounds="118,150" Parent="//@contents.0/@contents.3/@contents.2/@contents.1/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.2/@contents.1/@VisualDiagram" IsSketchy="true" Name="Actions" visualID="_9ZlPwP5YEeSzkfJMf5_daA">
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZlPwP5YEeSzkfJMf5_daA" Name="actionType" visualID="_9Zl20P5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZlPwP5YEeSzkfJMf5_daA" Name="timestamp" visualID="_9Zl20f5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZlPwP5YEeSzkfJMf5_daA" Name="person" visualID="_9Zl20v5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZlPwP5YEeSzkfJMf5_daA" Name="balance status" visualID="_9Zl20_5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
              <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_9ZlPwP5YEeSzkfJMf5_daA" Name="account status" visualID="_9Zl21P5YEeSzkfJMf5_daA">
                <Type xsi:type="datatype:NoType"/>
                <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
              </Elements>
            </Elements>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="REIN1" name="REIN1" elementKind="" description="The account action trail is stored in a secure way preventing tampering, loss, and leaking." timeCreated="2011-12-09T13:14:44.694+0100" lastModified="2017-01-17T19:34:39.708+0100" uniqueID="908087b9-f96f-48a8-b01a-13d2f9209177" workPackage="" abstractionLevel="" id="REIN1a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.708+0100" uniqueID="c320534f-c9e0-41ce-8222-7748bf1b60ab" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:14:59.268+0100" uniqueID="3d33edb9-4a3b-4db8-91c8-b8caf3ca44de">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:14:59.272+0100" uniqueID="950555a3-6af7-43c8-bf54-384a897e5d8a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="REIN1" name="REIN1" elementKind="" description="Reading an account action trail requires adequate capabilities." timeCreated="2011-12-09T13:15:38.365+0100" lastModified="2017-01-17T19:34:39.721+0100" uniqueID="54c7a4dc-f4de-4f11-9eb2-48d382bd486e" workPackage="" abstractionLevel="" id="REIN1b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.720+0100" uniqueID="394e6e01-4622-46e4-aa6d-d37b5d9ac4df" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:16:09.792+0100" uniqueID="7d7d0e5a-2f72-4119-8e25-4f57e2ceb093">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:16:09.796+0100" uniqueID="2c1c53e1-2742-48b1-9b01-33e538e01c42">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="REIN2" name="REIN2" elementKind="" description="The account action trail is stored in a way compliant with privacy protection laws." timeCreated="2011-12-09T13:16:36.218+0100" lastModified="2017-01-17T19:34:39.732+0100" uniqueID="1594d84d-9239-439a-9391-2a48eb67bcda" workPackage="" abstractionLevel="" id="REIN2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.731+0100" uniqueID="0ad3576f-d08f-43ac-b532-1df99b670923" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:24:27.225+0100" uniqueID="3bf88f68-0b4b-422b-b61b-14532c03cbcc">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:24:27.228+0100" uniqueID="c50f9946-57d4-4740-a185-0089137a46a8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="REIN3" name="REIN3" elementKind="" description="Account action trails may not be written to manually." timeCreated="2011-12-09T13:24:59.163+0100" lastModified="2017-01-17T19:34:39.743+0100" uniqueID="39040be8-d9a7-4287-abdf-e56c277236f0" workPackage="" abstractionLevel="" id="REIN3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.742+0100" uniqueID="66756efe-53cc-463d-9be2-fd658122fb94" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:25:15.762+0100" uniqueID="780ab28c-0841-4e90-bc16-cfc5b01baca6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:25:15.765+0100" uniqueID="1a41a4e5-512d-4daa-93fc-82c65a039686">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="REIN4" name="REIN4" elementKind="" description="Elements of the action trail can only be deleted if they are at least two years old and must be deleted if they are at most 5 years old" timeCreated="2011-12-09T13:25:31.465+0100" lastModified="2017-01-17T19:34:39.756+0100" uniqueID="9b16ce05-786d-4893-a409-36179183ef3a" workPackage="" abstractionLevel="" id="REIN4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.755+0100" uniqueID="334c916b-33f9-4ca2-8ae0-dc9b73672a84" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:25:42.120+0100" uniqueID="bf25f709-b55c-4d87-989b-db7972818a8a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:25:42.123+0100" uniqueID="671a361b-9411-4d5e-ae08-8db69123d02d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="REIN5" name="REIN5" elementKind="" description="Readers are notified about reader actions initiated not by them but by a librarian instead." timeCreated="2011-12-09T13:26:04.903+0100" lastModified="2017-01-17T19:34:39.767+0100" uniqueID="088abcf3-cea6-472a-99a4-46eb974509c3" workPackage="" abstractionLevel="" id="REIN5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.766+0100" uniqueID="5f6ea60a-7b32-4164-8d27-37db7976b42f" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:26:23.584+0100" uniqueID="3424f6b9-6009-466b-950a-fa2910c7317d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:26:23.585+0100" uniqueID="c6ce2511-cd9f-4a86-aeeb-1d33c7e113b4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="visualmodel:Diagram" label="REIN5ac" name="REIN5ac" elementKind="unspecified" description="" timeCreated="2015-04-19T17:34:23.003+0200" lastModified="2017-01-17T19:34:39.778+0100" uniqueID="dd92bdda-44af-4e36-ab93-bd8d032de49e" workPackage="">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.777+0100" uniqueID="8a9f8cd9-e530-4f1a-a281-4a60f6c33e31" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2015-04-19T17:34:58.219+0200" uniqueID="7780888f-217d-4b44-a8e5-613683e6a881">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2015-04-19T17:34:58.220+0200" uniqueID="242cc394-0350-4396-915e-d6b7eff4f94b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.3/@contents.2/@contents.8/@VisualDiagram" DiagramType="Activity">
            <Elements xsi:type="activity:VisualActionNode" Location="31,26" Bounds="146,29" Parent="//@contents.0/@contents.3/@contents.2/@contents.8/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.2/@contents.8/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.2/@contents.8/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="initiate reader action" visualID="_9ZnsAP5YEeSzkfJMf5_daA"/>
            <Elements xsi:type="activity:VisualActionNode" Location="48,95" Bounds="133,30" Parent="//@contents.0/@contents.3/@contents.2/@contents.8/@VisualDiagram" Diagram="//@contents.0/@contents.3/@contents.2/@contents.8/@VisualDiagram" Connections="//@contents.0/@contents.3/@contents.2/@contents.8/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" Name="receives notification" visualID="_9ZnsAf5YEeSzkfJMf5_daA"/>
            <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_9ZnsAP5YEeSzkfJMf5_daA" Target="_9ZnsAf5YEeSzkfJMf5_daA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="" Type="ControlFlow"/>
          </VisualDiagram>
        </contents>
        <contents xsi:type="requirement:Requirement" label="REIN6" name="REIN6" elementKind="" description="Account action trails can be search, displayed, forwarded, and printed." timeCreated="2011-12-09T13:27:15.038+0100" lastModified="2017-01-17T19:34:39.789+0100" uniqueID="5103da25-6c4b-493c-b0f2-3ddcde5c0618" workPackage="" abstractionLevel="" id="REIN6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.788+0100" uniqueID="0abaa0f8-9f20-4774-904a-b7970b513a8f" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:27:53.732+0100" uniqueID="d34579bf-751d-4d93-9385-db9c938a49b4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:27:53.734+0100" uniqueID="18ab21cc-f358-4adb-b575-c68b95976148">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="REIN7" name="REIN7" elementKind="" description="The user may print a receipt for every (sequence of) reader actions he has been performing in the current session/on the current terminal." timeCreated="2011-12-09T13:28:48.200+0100" lastModified="2017-01-17T19:34:39.800+0100" uniqueID="c6ceb107-5094-437a-b4bb-cedaee2fdfd2" workPackage="" abstractionLevel="" id="REIN7">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.799+0100" uniqueID="179eeaf5-ca03-41c6-b1d3-6e16a53eacd0" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:29:30.022+0100" uniqueID="a293d76d-416f-421c-8f26-8174f75d56f5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:29:30.025+0100" uniqueID="601be050-a73c-4bd6-ba53-2a3a151f76bf">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="REIN8" name="REIN8" elementKind="" description="An account action trail is opened at exactly the same time as a user is granted access to the account." timeCreated="2011-12-09T13:29:55.827+0100" lastModified="2017-01-17T19:34:39.812+0100" uniqueID="b8b38bb6-9cb6-4ab1-b414-322b3aef8237" workPackage="" abstractionLevel="" id="REIN8">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.811+0100" uniqueID="8055b148-f680-4838-af90-7350ca30be6c" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:30:01.714+0100" uniqueID="2f360da6-6357-438f-be3a-aa4abe48526d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:30:01.716+0100" uniqueID="9b3f5525-f491-4839-ac18-4e0d8903442b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="User Capabilities" timeCreated="2011-12-07T15:51:19.962+0100" lastModified="2017-01-17T19:34:40.009+0100" uniqueID="fb23feed-1af7-4a35-812b-6c125fed0b52">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.908+0100" uniqueID="a8a19824-725f-402b-9514-32e7b41099dd" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="UCAP1" name="UCAP1" elementKind="" description="Different user groups have the following capabilities:&#xD;&#xA;Guest: use catalog, return media/pay fee at front desk, read accession suggestions&#xD;&#xA;Reader: as guest + create/comment accession suggestions, reserve/lease/prolong media, inspect own action trail&#xD;&#xA;Reader (with AMDS): as reader + BookStation usage, online lending services, e-media access&#xD;&#xA;Librarian: as reader + manual transactions, create reports and monitor LMS operation data, accession, update own librarian account&#xD;&#xA;Chief Librarian: create librarian accounts and assign capabilities, customize LMS, and so on.&#xD;&#xA;" timeCreated="2011-12-09T13:30:40.735+0100" lastModified="2017-01-17T19:34:39.920+0100" uniqueID="51c3e620-98a2-4f79-ae0d-fefe80b11fa3" workPackage="" abstractionLevel="" id="UCAP1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.919+0100" uniqueID="2f2788f4-d9d5-4d41-b1b1-ca1ce05ee8ca" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:31:51.310+0100" uniqueID="b8bbf059-41f1-47ce-b60c-ea491e2cc29e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:31:51.311+0100" uniqueID="6e8175e3-9f33-41c8-be18-d1d961ace1e6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UCAP2" name="UCAP2" elementKind="" description="The LMS is able to identify a reader either by reading the reader's ID card, or by a remote log-in of the reader, or by selecting a reader on a privileged terminal by scanning his card or by remote login, the reader is recorded as the user initiating the reader action(s)." timeCreated="2011-12-09T13:32:33.210+0100" lastModified="2017-01-17T19:34:39.932+0100" uniqueID="d4209a3e-ecb5-4841-9118-96a805e8af39" workPackage="" abstractionLevel="" id="UCAP2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.931+0100" uniqueID="d284513f-ff83-43cc-a4a4-8a58ae42cd4b" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:33:03.577+0100" uniqueID="f0edea08-65cd-4f0d-a99b-886b8ebd8956">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:33:03.578+0100" uniqueID="ddb6520a-6567-4df6-8c2c-330c4515f149">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="A user of an LMS account must be identified in order to get access to the account." timeCreated="2011-12-09T13:33:23.560+0100" lastModified="2017-01-17T19:34:39.943+0100" uniqueID="a1965241-c0f5-4cdc-bbc9-244ae602d118" workPackage="" abstractionLevel="" id="UCAP3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.942+0100" uniqueID="8e038486-16c5-4d55-8bc3-766e242c9a3e" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:33:43.644+0100" uniqueID="65f8685a-7873-4fec-b7cf-872142b7d047">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:33:43.647+0100" uniqueID="007d72c0-9076-4ce1-bc47-a2219e8c5a81">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="If LMS grants access to an account, a session is created that records all actions in the session." timeCreated="2011-12-09T13:36:05.150+0100" lastModified="2017-01-17T19:34:39.954+0100" uniqueID="7c9085ba-cba9-42dd-ab0b-8d89b7274977" workPackage="" abstractionLevel="" id="UCAP3a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.954+0100" uniqueID="0d7ba106-6352-48b9-86e0-4598e7350fb8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:36:10.885+0100" uniqueID="bc7553f3-b20f-473e-9f34-9d889d1a8f19">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:36:10.888+0100" uniqueID="e450ae09-49f7-475b-9482-0976b3064ade">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="A session is terminated when either of the following happen:&#xD;&#xA;&#x9;time out: no action is performed for a customizable period of time;&#xD;&#xA;&#x9;log out: the session is explicitly terminated by the user; or&#xD;&#xA;&#x9;log in: another reader logs in at the same terminal.&#xD;&#xA;" timeCreated="2011-12-09T13:36:59.878+0100" lastModified="2017-01-17T19:34:39.968+0100" uniqueID="105e0a43-62d7-4e27-9d6d-b103f6de8e2c" workPackage="" abstractionLevel="" id="UCAP3b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.967+0100" uniqueID="be1f7584-dad6-44e2-aa6f-51804a6a60ec" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:37:13.139+0100" uniqueID="4bc2d87a-127c-43b9-b47a-d171db1d3c9e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:37:13.139+0100" uniqueID="40ce26a1-4b3b-4315-84e1-6c3966724a06">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="A Librarian may inspect the list of current sessions and may tap into one of them, thus acquiring control of the session and equipping it with his own capabilities." timeCreated="2011-12-09T13:44:00.322+0100" lastModified="2017-01-17T19:34:39.980+0100" uniqueID="c670277a-360d-4e12-be3f-0f065e2f05bf" workPackage="" abstractionLevel="" id="UCAP3c">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.979+0100" uniqueID="6cfd6ace-5e3c-4db0-98d6-6b6b32abfbd2" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:44:20.204+0100" uniqueID="902d6de6-130b-4217-85ab-e668d801a37c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:44:20.207+0100" uniqueID="39d8115f-32a7-4cf2-b701-506f3f6657f2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="A Librarian may create a sub-session for any given reader by identifying the reader.  " timeCreated="2011-12-09T13:45:10.668+0100" lastModified="2017-01-17T19:34:39.993+0100" uniqueID="0e958a9f-b9fe-47ed-9598-283f55cf08c8" workPackage="" abstractionLevel="" id="UCAP3d">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:39.992+0100" uniqueID="138ead97-80ea-4b0e-969d-fb8c2ea320ce" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:46:09.990+0100" uniqueID="08c3340c-324d-49db-9cc6-940dcb65e00d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:46:09.993+0100" uniqueID="9d9f3443-b0c8-4a11-b149-e41bd7f45253">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UCAP4" name="UCAP4" elementKind="" description="Reader actions can only take place if &#xD;&#xA;&#x9;the user has the appropriate privileges&#xD;&#xA;&#x9;the medium is in the appropriate state&#xD;&#xA;&#x9;the user account is in the appropriate state" timeCreated="2011-12-09T13:46:36.405+0100" lastModified="2017-01-17T19:34:40.009+0100" uniqueID="31274865-2171-4a28-bfe8-493fe2b7a02e" workPackage="" abstractionLevel="" id="UCAP4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.008+0100" uniqueID="c77704e0-b1eb-45c8-8b96-f4869d73f085" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:51:18.908+0100" uniqueID="3fdbedf3-45c9-481a-a843-5b780d27b116">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:51:18.911+0100" uniqueID="a1c7233b-bfc7-46d3-b080-18c0960fbd8b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Administration" timeCreated="2011-12-09T15:12:29.603+0100" lastModified="2017-01-17T19:34:41.280+0100" uniqueID="94a5742f-d802-4cdb-b53f-73dddb26a3b7">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.493+0100" uniqueID="fed4cd57-35e3-4c99-8c9d-437cd08ff2da" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Setup Customization &amp; Maintenance" timeCreated="2011-12-07T15:51:56.881+0100" lastModified="2017-01-17T19:34:40.670+0100" uniqueID="233899c5-6bd4-42a6-9ee1-5ff8e9e3c48e">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.586+0100" uniqueID="1e556681-72af-4c8a-9e96-80adcacb43db" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="SCM1" name="SCM1" elementKind="" description="The LMS runs on &#xD;&#xA;&#x9;Windows NT/XP/7, Linux (Kernel since 2.2.1), and MacOS (since 9.5), &#xD;&#xA;&#x9;requires 1GB of disc space, plus ca.. 1MB of disc space per reader &amp; operating year, and&#xD;&#xA;&#x9;needs an uplink to the LMS maintenance site of 1GB/day per 1000 readers." timeCreated="2011-12-09T13:52:14.987+0100" lastModified="2017-01-17T19:34:40.599+0100" uniqueID="5db460bd-4b24-4069-82ce-07ee8db9d40a" workPackage="" abstractionLevel="" id="SCM1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.598+0100" uniqueID="be5343b2-7c21-43e1-bbeb-0eb237c10b8c" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:54:42.148+0100" uniqueID="4b3702ec-28e1-41fe-aa71-31e13eec9ff4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:54:42.150+0100" uniqueID="83516e8f-5acd-470f-b54d-cbf1f6f5fc58">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SCM2" name="SCM2" elementKind="" description="Librarians with basic computer skills shall be able to install the basic LMS within 2h." timeCreated="2011-12-09T13:54:15.274+0100" lastModified="2017-01-17T19:34:40.610+0100" uniqueID="3f818bb4-f6f4-446e-bf8e-82b9ccbb3dde" workPackage="" abstractionLevel="" id="SCM2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.610+0100" uniqueID="86204a3b-121a-42a6-a71c-9e8812789994" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:54:38.920+0100" uniqueID="f57b766a-69c1-4c2e-b5c3-7b33a1eeee83">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:54:38.923+0100" uniqueID="762f0ac4-cb5a-4480-8dd9-99326f8d25af">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SCM3" name="SCM3" elementKind="" description="The system must be localizable by editing text or configuration files only." timeCreated="2011-12-09T13:54:55.839+0100" lastModified="2017-01-17T19:34:40.621+0100" uniqueID="76a549e5-a479-42c6-af24-13e78dbde5b8" workPackage="" abstractionLevel="" id="SCM3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.621+0100" uniqueID="ccd4cd8d-5439-4ea3-8d7f-7aefe6e2f82a" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:55:15.640+0100" uniqueID="2149eec6-5949-48de-a448-e827e144e4aa">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:55:15.643+0100" uniqueID="3f5e8e3c-b3c7-49b8-8630-3a62f8ecb224">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SCM4" name="SCM4" elementKind="" description="LMS requires a 5h/week system maintenance interval." timeCreated="2011-12-09T13:55:29.651+0100" lastModified="2017-01-17T19:34:40.634+0100" uniqueID="0c27ca46-afc5-49a8-827f-c915d20fa29a" workPackage="" abstractionLevel="" id="SCM4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.633+0100" uniqueID="f4d01bde-883a-42e2-9418-0e5cb2c89b5c" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:55:42.258+0100" uniqueID="b96f4e54-7e8d-4b9d-987c-2e7c24e8cf97">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:55:42.259+0100" uniqueID="cc576016-5d79-4a12-a4f3-acb7428bb112">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SCM5" name="SCM5" elementKind="" description="There shall be an easy to use facility for inspecting and changing the current profile of settings concerning:&#xD;&#xA;&#x9;the types of reader account and their capabilities;&#xD;&#xA;&#x9;message texts, channels, and triggers;&#xD;&#xA;&#x9;the triggers of notifications; and&#xD;&#xA;&#x9;process limits, fees, durations, and thresholds.&#xD;&#xA;&#x9;assignment of special responsibilities to selected librarian accounts&#xD;&#xA;&#x9;the number of reservations admissible for one reader" timeCreated="2011-12-09T13:56:00.377+0100" lastModified="2017-01-17T19:34:40.646+0100" uniqueID="3f022a32-610f-49a4-b5a0-3eab96805fe9" workPackage="" abstractionLevel="" id="SCM5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.645+0100" uniqueID="9065ec24-72ef-4783-a21c-599e17d207e3" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:56:06.531+0100" uniqueID="44c75661-f040-497a-9fea-697961b62e95">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:56:06.534+0100" uniqueID="bcc933c7-ddc1-4f7f-a5b6-95f36d2cd64c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SCM6" name="SCM6" elementKind="" description="The process parameters valid at the time of instantiating a process instance are valid throughout the lifetime of the process instance." timeCreated="2011-12-09T13:56:39.338+0100" lastModified="2017-01-17T19:34:40.658+0100" uniqueID="a2bd6c70-0796-4a51-82a5-84eb8174d93b" workPackage="" abstractionLevel="" id="SCM6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.658+0100" uniqueID="b7628f0d-39e6-47db-ba67-4065629f8cc7" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:57:49.971+0100" uniqueID="963e30e6-78f6-494c-9b9f-a88a3b4976a2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:57:49.974+0100" uniqueID="bfb8bc3a-b376-47f2-a486-b482ffd4b96d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SCM7" name="SCM7" elementKind="" description="LMS is only distributed with maintenance contracts and updates itself from the designated update server automatically." timeCreated="2011-12-09T13:58:15.610+0100" lastModified="2017-01-17T19:34:40.670+0100" uniqueID="d2a97477-b9c4-49a8-aae5-78f6786c3da5" workPackage="" abstractionLevel="" id="SCM7">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.669+0100" uniqueID="375ea238-ce32-457c-8aa6-948e031cc7fb" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T13:58:40.422+0100" uniqueID="0b5a9995-57fe-458b-8758-bb5ac9427b1f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:58:40.425+0100" uniqueID="8066ac3f-82a9-46c1-acce-07da551e02e6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Librarian Accounts" timeCreated="2011-12-09T13:59:35.848+0100" lastModified="2017-01-17T19:34:40.834+0100" uniqueID="efde8b44-fe1b-4ba2-be65-64c42de5755e">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.758+0100" uniqueID="335f926a-9ad3-4ea3-9848-35ced982b1b9" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="LAC1" name="LAC1" elementKind="" description="Chief librarians can create, update, and delete librarian accounts." timeCreated="2011-12-09T14:00:00.757+0100" lastModified="2017-01-17T19:34:40.771+0100" uniqueID="a98ee222-9ada-4585-9f90-59807b907771" workPackage="" abstractionLevel="" id="LAC1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.770+0100" uniqueID="74e6b7b5-2e28-43f6-8dcc-b89069af35e8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:00:17.925+0100" uniqueID="391ce9c1-23f0-41aa-baf2-3999b61e56e7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:00:17.928+0100" uniqueID="e6231678-6d42-4d3a-a934-0606004252d2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LAC2" name="LAC2" elementKind="" description="The capabilities of a librarian account can only be edited by a chief librarian." timeCreated="2011-12-09T14:04:40.883+0100" lastModified="2017-01-17T19:34:40.784+0100" uniqueID="a3f4c4a2-5738-4094-8db4-f5e74448d647" workPackage="" abstractionLevel="" id="LAC2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.784+0100" uniqueID="c30e6639-95e1-43c3-9a93-970cf0b722cf" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:04:58.828+0100" uniqueID="529b04ce-6e33-47a5-9858-a2468d423e2c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:04:58.829+0100" uniqueID="2ed8c7d0-f700-4be3-b0b3-b7e25d051009">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LAC3" name="LAC3" elementKind="" description="Chief librarian accounts cannot be created by the LMS." timeCreated="2011-12-09T14:05:16.743+0100" lastModified="2017-01-17T19:34:40.797+0100" uniqueID="b02c5f31-1cd0-4156-9912-2e2bb910282a" workPackage="" abstractionLevel="" id="LAC3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.796+0100" uniqueID="eca52a98-da55-44a1-843c-1b3a428ca121" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:05:37.932+0100" uniqueID="11ce28c7-e9ca-4add-98a1-f62e8bb4a180">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:05:37.935+0100" uniqueID="696b32a3-8d16-4357-ba3c-3c547903087e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LAC4" name="LAC4" elementKind="" description="Activating a Chief Librarian account requires a restart of the LMS." timeCreated="2011-12-09T14:07:00.513+0100" lastModified="2017-01-17T19:34:40.811+0100" uniqueID="03ad5347-7f70-4a3a-a9ae-7d7a0796f51b" workPackage="" abstractionLevel="" id="LAC4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.811+0100" uniqueID="84c55758-78dc-4bad-9b56-e307e3c6bbac" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:07:14.753+0100" uniqueID="8a0964e7-295e-4c1e-83ee-97909be330aa">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:07:14.756+0100" uniqueID="33f17726-c92d-4f5b-a72c-5c097218fea0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LAC5" name="LAC5" elementKind="" description="On starting the LMS, all chief librarians are informed about all other activated chief librarian accounts." timeCreated="2011-12-09T14:07:32.919+0100" lastModified="2017-01-17T19:34:40.823+0100" uniqueID="6ba3d7c0-c3fc-43c2-b47a-ab709a4271ec" workPackage="" abstractionLevel="" id="LAC5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.822+0100" uniqueID="ad67d692-8ebb-4c85-b142-f3f90c63552f" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:07:47.026+0100" uniqueID="50e6404c-b712-4ce3-a44a-f82921be6903">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:07:47.029+0100" uniqueID="53103199-a47a-463f-b4bd-ddf15a48213c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="LAC6" name="LAC6" elementKind="" description="Chief librarians can edit the profile details of their own accounts." timeCreated="2011-12-09T14:08:04.114+0100" lastModified="2017-01-17T19:34:40.834+0100" uniqueID="15e307e1-65a0-4f70-9283-6b8222fab4f4" workPackage="" abstractionLevel="" id="LAC6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.834+0100" uniqueID="b2ea028d-0917-4ddc-8629-10a99b258513" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:08:09.894+0100" uniqueID="8c76c67d-8c1a-4c75-9d85-7157dc336ccb">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:08:09.896+0100" uniqueID="b0448ad3-a8c6-41b8-ae97-c31351a81055">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Software Operation" timeCreated="2011-12-09T14:08:54.602+0100" lastModified="2017-01-17T19:34:41.132+0100" uniqueID="94aebeba-73e7-4138-93c0-79a5998bc63b">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.976+0100" uniqueID="4b955c0f-111f-4a8d-801a-9933a1257b30" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="SOP1" name="SOP1" elementKind="" description="LMS requires a 2h/day system administration interval." timeCreated="2011-12-09T14:09:21.058+0100" lastModified="2017-01-17T19:34:40.989+0100" uniqueID="dc190a43-9286-47bf-b9c2-4cf094448312" workPackage="" abstractionLevel="" id="SOP1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:40.988+0100" uniqueID="ab302c0f-ba00-4d76-ab44-d24626664168" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:09:45.628+0100" uniqueID="678177b1-2ad5-43e0-8c71-f7e7c1782264">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:09:45.629+0100" uniqueID="097cfc9a-440c-443e-bb97-5fdbb7502506">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP2" name="SOP2" elementKind="" description="LMS can be started from any system state within 10 minutes." timeCreated="2011-12-09T14:09:58.747+0100" lastModified="2017-01-17T19:34:41.003+0100" uniqueID="0a704fb8-e41e-4bdb-9ef2-fef76b9f7db4" workPackage="" abstractionLevel="" id="SOP2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.003+0100" uniqueID="9750f8f2-2b46-44c9-a548-5e94111aa617" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:10:13.288+0100" uniqueID="67d12e3b-31f7-4e5e-b9d7-6f055a36958f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:10:13.291+0100" uniqueID="d9c5df41-5d63-42c7-8901-ff43542f95af">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP3" name="SOP3" elementKind="" description="LMS includes a failsafe backup of reader and account data." timeCreated="2011-12-09T14:10:32.420+0100" lastModified="2017-01-17T19:34:41.015+0100" uniqueID="c0c9d353-9d56-4b9a-9520-aec2d333558d" workPackage="" abstractionLevel="" id="SOP3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.015+0100" uniqueID="8cde197a-e35f-4184-8b66-1b802fe76001" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:10:45.195+0100" uniqueID="9f546589-f33a-4620-a6d0-488109b91329">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:10:45.198+0100" uniqueID="bae48bb2-49bd-4c34-8369-4bdb8ea41764">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP4" name="SOP4" elementKind="" description="The LMS provides an easy to use operation console to manage the operation of LMS." timeCreated="2011-12-09T14:11:02.013+0100" lastModified="2017-01-17T19:34:41.030+0100" uniqueID="7623867e-0034-43e2-843b-d54a1780ad1d" workPackage="" abstractionLevel="" id="SOP4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.030+0100" uniqueID="1162d9b4-7a7e-4916-8c3e-9a04f04213c6" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:11:14.040+0100" uniqueID="30c79ac2-876b-4ce3-bb0b-855ce0b6b620">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:11:14.043+0100" uniqueID="6cc5efff-8d23-4825-b562-3c331400507a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP4a" name="SOP4a" elementKind="" description="The operation console shall provide a smartphone-interface so that the system can easily be monitored outside the regular library hours." timeCreated="2011-12-09T14:11:29.113+0100" lastModified="2017-01-17T19:34:41.046+0100" uniqueID="d8fbbc0a-4fdf-45e2-8ebe-036084ec252d" workPackage="" abstractionLevel="" id="SOP4a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.045+0100" uniqueID="63dd795c-d55f-47c9-858d-3c6aa8033d23" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:11:52.979+0100" uniqueID="fa1a4d2a-7300-4a85-b34b-0fb259b71e85">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:11:52.983+0100" uniqueID="6c46cfc3-229e-4e7e-9fa7-ead882db80ea">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP4b" name="SOP4b" elementKind="" description="All chief librarians have access to the operation console; access rights may be extended to ordinary librarians." timeCreated="2011-12-09T14:12:13.786+0100" lastModified="2017-01-17T19:34:41.060+0100" uniqueID="9f4fc9fd-a29d-4543-9b85-169e403e06c2" workPackage="" abstractionLevel="" id="SOP4b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.059+0100" uniqueID="b2ae11d3-6f2b-437d-bbf6-7a0915e52e99" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:14:40.327+0100" uniqueID="74ac40f6-0e87-4739-a471-7ba5b16372be">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:14:40.329+0100" uniqueID="b46c0073-f272-4765-8c23-df86d44ebb39">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP4c" name="SOP4c" elementKind="" description="The operation console allows to monitor and set the operation parameters." timeCreated="2011-12-09T14:13:17.229+0100" lastModified="2017-01-17T19:34:41.073+0100" uniqueID="6262dd2b-6d24-4448-a90f-ef27684410dd" workPackage="" abstractionLevel="" id="SOP4c">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.072+0100" uniqueID="51f542db-7c92-4434-8162-45768586c9f0" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:13:31.699+0100" uniqueID="ec44cf56-e613-43a9-b851-3b4d4d101689">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:13:31.702+0100" uniqueID="29b91260-0234-4663-aca0-3eb26d9a50e5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP4d" name="SOP4d" elementKind="" description="The operation console allows to start, restart, shutdown, and update the LMS and its parts." timeCreated="2011-12-09T14:13:54.378+0100" lastModified="2017-01-17T19:34:41.089+0100" uniqueID="c28f72d2-7485-40f6-a2d4-18800bb43504" workPackage="" abstractionLevel="" id="SOP4d">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.088+0100" uniqueID="a0de458b-3298-451c-8e17-54a634a3e274" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:14:27.276+0100" uniqueID="00addfdd-0d0c-4934-bc48-97e05dd62c43">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:14:27.280+0100" uniqueID="ba5edebe-17de-4944-b0bf-16dc3a5f403f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP4e" name="SOP4e" elementKind="" description="The operation console allows to start, stop, and monitor the LMS backup process, the backup status, and the remote backup storage memory usage." timeCreated="2011-12-09T14:15:45.739+0100" lastModified="2017-01-17T19:34:41.108+0100" uniqueID="c02c8d77-7891-4811-9c15-42afa896737e" workPackage="" abstractionLevel="" id="SOP4e">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.107+0100" uniqueID="780b7e99-7ae4-4631-a299-c7d72157b39d" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:16:09.729+0100" uniqueID="7b755800-c18a-4092-a1e6-2cd2045bc50c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:16:09.730+0100" uniqueID="9d9857d6-f3b8-45d0-8ab9-66c1fd4f9431">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP4f" name="SOP4f" elementKind="" description="There shall be a facility to edit all process instances manually for error recovery." timeCreated="2011-12-09T14:16:23.681+0100" lastModified="2017-01-17T19:34:41.121+0100" uniqueID="0eb4d2dd-13aa-40df-a367-96e62838772a" workPackage="" abstractionLevel="" id="SOP4f">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.120+0100" uniqueID="55bb5a1c-435b-49c3-8cc2-94a4ef13a1c1" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:16:40.035+0100" uniqueID="2aa89987-b313-4e5c-830c-c43c18efc9e8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:16:40.038+0100" uniqueID="7668e786-3b99-40a3-b62c-3c17c54c0cb7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="SOP5" name="SOP5" elementKind="" description="There shall be a facility to inspect and change the current remote backup status, available opt-ions, and corresponding settings." timeCreated="2011-12-09T14:16:54.007+0100" lastModified="2017-01-17T19:34:41.132+0100" uniqueID="2478e3b9-c0b5-4246-a5ae-0517f0416f6b" workPackage="" abstractionLevel="" id="SOP5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.131+0100" uniqueID="2e4fd525-65d1-4015-a82c-32ecded3814a" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:17:02.581+0100" uniqueID="15ea253e-816d-4acf-a3a7-76d14aea6fae">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:17:02.582+0100" uniqueID="e618f1d0-5706-4fc8-93dd-7f2875b7a373">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Monitoring &amp; Reporting" timeCreated="2011-12-09T14:18:19.856+0100" lastModified="2017-01-17T19:34:41.280+0100" uniqueID="5853043c-0179-452c-ad95-1db847a77cb9">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.208+0100" uniqueID="fbe589ff-1d3f-4d69-bce4-bec87586b452" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="MOR1" name="MOR1" elementKind="" description="All messages concerning the operation of LMS are sent to the operation console." timeCreated="2011-12-09T14:18:34.022+0100" lastModified="2017-01-17T19:34:41.220+0100" uniqueID="0c91ae50-353c-4530-8e1e-8ca6ac0248d0" workPackage="" abstractionLevel="" id="MOR1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.220+0100" uniqueID="1a7ebb6b-32b7-4db5-b234-53e86a3fb23b" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:19:43.992+0100" uniqueID="01605b5e-91d7-45d3-9111-3828744cc0a8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:19:43.996+0100" uniqueID="5adb68e9-5211-43f9-98f7-fa3193fd510c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MOR2" name="MOR2" elementKind="" description="The LMS may be set up in such a way that all errors, warnings, and status reports triggered during operation are forwarded to the manufacturer." timeCreated="2011-12-09T14:20:03.172+0100" lastModified="2017-01-17T19:34:41.231+0100" uniqueID="716fcc4d-b7b0-4f4f-a3b5-59438b705716" workPackage="" abstractionLevel="" id="MOR2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.231+0100" uniqueID="f626d1b5-666f-49cd-bbcd-2fa352261ca6" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:20:38.024+0100" uniqueID="82fd2092-59c9-4f12-a175-a7bf1fde420d">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:20:38.025+0100" uniqueID="ccaeeb61-bc0d-4445-a6d2-5c8ee543ceda">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MOR3" name="MOR3" elementKind="" description="There shall be a query interface that allows ad-hoc queries of the operative data." timeCreated="2011-12-09T14:21:00.391+0100" lastModified="2017-01-17T19:34:41.243+0100" uniqueID="f78a58c5-8b4f-4e26-96e9-b67310400791" workPackage="" abstractionLevel="" id="MOR3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.242+0100" uniqueID="056c7de0-f43d-44fe-8d63-0a3900964a11" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:21:06.021+0100" uniqueID="a7b67b1e-db15-41f0-849c-eb4b964a3b81">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:21:06.022+0100" uniqueID="50a08552-bfa2-49c3-a35f-ec53e30b768b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MOR4" name="MOR4" elementKind="" description="LMS shall be able to generate basic reports on usage and financial status of the LMS, the status of the corpus and the reader base, and the usage of online services." timeCreated="2011-12-09T14:21:23.375+0100" lastModified="2017-01-17T19:34:41.254+0100" uniqueID="cc8c344e-6b72-483e-b9e6-91d315a8e40b" workPackage="" abstractionLevel="" id="MOR4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.254+0100" uniqueID="a7c2f516-59aa-4c25-b723-3caeebe83e46" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:21:45.497+0100" uniqueID="53a1d44f-5eb6-4d3d-b64f-717d489b4561">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:21:45.498+0100" uniqueID="9eb15cfb-e7ab-48b1-9daf-6c18686dee78">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MOR5" name="MOR5" elementKind="" description="LMS shall be able to generate advanced reports for the library management." timeCreated="2011-12-09T14:22:29.718+0100" lastModified="2017-01-17T19:34:41.268+0100" uniqueID="554b2e64-2cbb-435c-8da7-23cdec8b54fb" workPackage="" abstractionLevel="" id="MOR5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.268+0100" uniqueID="f6d4ef71-278e-4523-a698-e9a616ef140b" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:22:40.492+0100" uniqueID="ab05e3b8-aced-40f9-90ae-b0248afa5d0c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:22:40.496+0100" uniqueID="c751d2ca-254b-4100-8686-4750bc6cdcb9">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="MOR6" name="MOR6" elementKind="" description="All query and report outputs can be formatted suitable for screen and print output." timeCreated="2011-12-09T14:23:02.310+0100" lastModified="2017-01-17T19:34:41.280+0100" uniqueID="32e42f00-a1d7-4fe5-a3d1-b80a83d62f28" workPackage="" abstractionLevel="" id="MOR6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.279+0100" uniqueID="9ea8811e-9fb0-4aaa-9b79-be42ffac4aad" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:23:18.053+0100" uniqueID="973f3a80-ddd9-4692-b307-8376c4e3cd1a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:23:18.054+0100" uniqueID="87a83fe7-a79a-4566-ab9e-d6ca786944d2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Crosscutting Concerns" timeCreated="2011-12-09T15:07:28.523+0100" lastModified="2017-01-17T19:34:42.434+0100" uniqueID="7ffbc6df-2131-4ad7-bfa8-86fdb239e7e6">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.690+0100" uniqueID="8bf56e70-b71f-41ef-b301-4eaad2cbd9f2" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="User Interaction" timeCreated="2011-12-09T14:28:09.693+0100" lastModified="2017-01-17T19:34:41.999+0100" uniqueID="72938236-5e62-46d8-abdc-cf5244d9d698">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.847+0100" uniqueID="04b00a49-b282-47ab-b363-58274929ccad" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="UI1" name="UI1" elementKind="" description="The LMS shall provide a graphical user interface on the terminals available in the library." timeCreated="2011-12-09T14:28:35.293+0100" lastModified="2017-01-17T19:34:41.859+0100" uniqueID="c5fc9a0f-6010-4fa9-9f3e-ccfe9f11c63c" workPackage="" abstractionLevel="" id="UI1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.858+0100" uniqueID="8ed4adea-a70c-40aa-8489-33f9a21dc997" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:28:51.793+0100" uniqueID="a748f303-467e-4c0b-bfa2-430b1aeb8452">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:28:51.796+0100" uniqueID="cf1d2b38-a1e5-4970-9ed1-61fa041f4da6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI1a" name="UI1a" elementKind="" description="The LMS GUI shall be easy to learn such that 95% of new readers are able to perform reader actions, catalog actions, and message reading with no assistance at once." timeCreated="2011-12-09T14:29:12.810+0100" lastModified="2017-01-17T19:34:41.870+0100" uniqueID="0badf31e-1eef-4d75-a030-e9e6ad62b818" workPackage="" abstractionLevel="" id="UI1a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.870+0100" uniqueID="86645804-a753-41cf-80bf-477a413074ee" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:29:20.704+0100" uniqueID="96fde674-279e-4883-99bd-48915938f861">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:29:20.706+0100" uniqueID="893671b3-5266-41a0-b19d-d30fe9cc611c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI1b" name="UI1b" elementKind="" description="All functions of the LMS-GUI shall be accessible by keyboard." timeCreated="2011-12-09T14:29:30.625+0100" lastModified="2017-01-17T19:34:41.882+0100" uniqueID="87dc60e4-58b6-4054-a47d-45ad237a875c" workPackage="" abstractionLevel="" id="UI1b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.881+0100" uniqueID="5085370d-d1f2-4626-9567-a24cf78c9b60" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:29:38.555+0100" uniqueID="e4596b9a-cd02-427d-9997-66248543ac67">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:29:38.556+0100" uniqueID="0ef68fbc-e1fe-49dc-bdb0-6b2a2cc1148c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI2" name="UI2" elementKind="" description="Multiple channels shall be available for the services described in domains “Leases and Reservations”." timeCreated="2011-12-09T14:31:26.508+0100" lastModified="2017-01-17T19:34:41.893+0100" uniqueID="84adc66f-e26b-424a-ad6d-595a597aa81a" workPackage="" abstractionLevel="" id="UI2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.892+0100" uniqueID="61653f25-b55e-450a-b5d7-963efb8d6154" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:31:40.672+0100" uniqueID="720bcfc3-83ea-4b68-8a18-af313618b574">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:31:40.673+0100" uniqueID="2e919fee-29de-40f9-94e5-e834cfd8950e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI2a" name="UI2a" elementKind="" description="A reader may have lending actions performed for him by a librarian at the front desk." timeCreated="2011-12-09T14:32:03.225+0100" lastModified="2017-01-17T19:34:41.905+0100" uniqueID="ffc891ba-881f-41c4-9ba0-6b329bc45395" workPackage="" abstractionLevel="" id="UI2a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.905+0100" uniqueID="1c8e6440-057c-4d30-a1d9-7bfa79bcdd31" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:32:14.069+0100" uniqueID="d0f95de8-2619-4aba-bc52-81914e5f488b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:32:14.073+0100" uniqueID="56272a25-77b5-4e4d-adb8-20c5da68a7f4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI2b" name="UI2b" elementKind="" description="A reader may perform reader actions at self-service terminals in the library." timeCreated="2011-12-09T14:32:37.287+0100" lastModified="2017-01-17T19:34:41.917+0100" uniqueID="04beeff2-3e23-4e0a-b1bb-0957c1f83a15" workPackage="" abstractionLevel="" id="UI2b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.916+0100" uniqueID="9a633f76-118c-40cd-b150-49fc43690e4c" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:32:44.442+0100" uniqueID="9a78778c-3798-4663-93f0-0552960568de">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:32:44.445+0100" uniqueID="6364a4c8-ad3d-4b4e-9d39-73251c23a935">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI2c" name="UI2c" elementKind="" description="A reader may perform reader actions online." timeCreated="2011-12-09T14:33:29.617+0100" lastModified="2017-01-17T19:34:41.929+0100" uniqueID="9d7eebfe-bfdd-496a-9f71-ed259e0fc39c" workPackage="" abstractionLevel="" id="UI2c">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.928+0100" uniqueID="88bac4d3-548e-4934-a2f3-d3cb97121e85" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:33:37.321+0100" uniqueID="7941c59a-6d05-42d7-87a1-65b273e34147">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:33:37.322+0100" uniqueID="c0b62f98-f13a-4fd7-b517-00b43f48a7c5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI2d" name="UI2d" elementKind="" description="A reader may perform certain reader actions by interacting with the BookStationTM&#xD;&#xA;media dispenser robot.&#xD;&#xA;" timeCreated="2011-12-09T14:33:59.549+0100" lastModified="2017-01-17T19:34:41.940+0100" uniqueID="2ab34767-e3b3-4ec4-b853-754ac600cd4f" workPackage="" abstractionLevel="" id="UI2d">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.940+0100" uniqueID="a5975dfd-3998-4afd-8617-04af67bdc79c" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:34:10.764+0100" uniqueID="55419fe1-7bf4-4be7-8fc9-a516ca517cf0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:34:10.765+0100" uniqueID="b97411dd-4751-42dd-b648-31f53648d3d7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI3" name="UI3" elementKind="" description="95% of all readers shall be able to complete the terminal usability test suite in less than 3min." timeCreated="2011-12-09T14:34:32.947+0100" lastModified="2017-01-17T19:34:41.952+0100" uniqueID="6f89998d-fd50-4225-94ec-ec40d2d3cc5a" workPackage="" abstractionLevel="" id="UI3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.951+0100" uniqueID="8f9127ea-598b-4fbc-8f08-6e48aa9ebf1a" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:34:52.251+0100" uniqueID="a60556eb-d53a-4d6f-8a0f-2e14858c88ea">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:34:52.255+0100" uniqueID="17706904-7975-4bda-9950-e75971739f57">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI4" name="UI4" elementKind="" description="Terminals shall be equipped with a scanning device to identify readers by their card and copies by their id badge." timeCreated="2011-12-09T14:35:14.867+0100" lastModified="2017-01-17T19:34:41.963+0100" uniqueID="45e7a1ce-f66b-4290-b917-173c1a8aceda" workPackage="" abstractionLevel="" id="UI4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.962+0100" uniqueID="74b0c95d-8fbd-440d-bc77-0a49bb3c183b" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:35:32.554+0100" uniqueID="1c5d73b6-6b5a-4513-be46-ba14d1446955">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:35:32.558+0100" uniqueID="2fc47591-03a5-4a73-8f23-5396ae5aa057">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI4a" name="UI4a" elementKind="" description="The scanning device shall provide multi-modal feedback on the result of the scanning action." timeCreated="2011-12-09T14:36:20.036+0100" lastModified="2017-01-17T19:34:41.975+0100" uniqueID="07e3e4ea-6a04-4167-802a-33a368a278d3" workPackage="" abstractionLevel="" id="UI4a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.974+0100" uniqueID="c3326c95-c19b-4eda-9ac5-5c7ad71d992e" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:36:39.538+0100" uniqueID="ec9a7825-49b1-4f44-b50b-22e5010bdefa">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:36:39.541+0100" uniqueID="932b5ebe-285f-409b-96de-db933d53a7d4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI5" name="UI5" elementKind="" description="When a reader is identified by the LMS, relevant reader and account information will be displayed at the terminal." timeCreated="2011-12-09T14:37:19.215+0100" lastModified="2017-01-17T19:34:41.988+0100" uniqueID="612a209a-dba3-4091-aeb3-c74a2c3b55f7" workPackage="" abstractionLevel="" id="UI5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.988+0100" uniqueID="eb38c512-e887-44e7-bf0e-9681bcc0c8b9" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:37:35.617+0100" uniqueID="31b1fa73-e140-4a76-a18e-4c4eb44d13d8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:37:35.618+0100" uniqueID="4e90c494-af15-45ce-aa42-8e9500eb9e98">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="UI6" name="UI6" elementKind="" description="The reader shall never be offered options or actions that cannot take place." timeCreated="2011-12-09T14:38:40.984+0100" lastModified="2017-01-17T19:34:41.999+0100" uniqueID="b5cf76e5-bf84-4942-9136-db685e6d2d48" workPackage="" abstractionLevel="" id="UI6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:41.999+0100" uniqueID="a14f3d49-3f3c-43ed-a842-2568e6ebebe7" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:39:09.599+0100" uniqueID="b7f0a480-0c16-432d-aa22-d80746bf5d6c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:39:09.603+0100" uniqueID="4825a28b-bd33-41b1-ae59-9f77b403a979">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Web Access" timeCreated="2011-12-09T14:40:01.248+0100" lastModified="2017-01-17T19:34:42.214+0100" uniqueID="81361aa3-1d35-45ed-b842-8435b7afed7c">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.109+0100" uniqueID="a6d48187-c991-4585-af4e-58d66e6f2832" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="WEB1" name="WEB1" elementKind="" description="" timeCreated="2011-12-09T14:40:46.812+0100" lastModified="2017-01-17T19:34:42.120+0100" uniqueID="feffe9e5-756a-470b-9aad-887ca941e7e3" workPackage="" abstractionLevel="" id="WEB1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.120+0100" uniqueID="29e7fe0a-4d2d-485b-b927-eda68bb4e2db" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:40:57.068+0100" uniqueID="724cd4d0-bba7-4e34-9805-7ecc415792f6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:40:57.070+0100" uniqueID="8833fde8-fd0b-42af-b0fd-c0527c3daa92">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="WEB1a" name="WEB1a" elementKind="" description="Initial passwords can only be used to log to immediately change the password." timeCreated="2011-12-09T14:48:50.962+0100" lastModified="2017-01-17T19:34:42.132+0100" uniqueID="d46a05ba-7304-4268-8455-8814a9a8b29a" workPackage="" abstractionLevel="" id="WEB1a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.131+0100" uniqueID="b51b1ecb-8d16-47d0-befd-f4b420cfc358" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:49:22.719+0100" uniqueID="c87362a8-7b5d-46cf-b372-9af80895881f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:49:22.722+0100" uniqueID="3ccd796f-ca5a-4f9b-8bbb-2b09a649e557">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="WEB1b" name="WEB1b" elementKind="" description="All passwords must be strong." timeCreated="2011-12-09T14:49:08.836+0100" lastModified="2017-01-17T19:34:42.143+0100" uniqueID="490cf373-1c90-47cf-b26f-7bc094bd95ce" workPackage="" abstractionLevel="" id="WEB1b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.143+0100" uniqueID="a785e697-ac10-41e0-a404-57c5c02e55b1" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:49:20.245+0100" uniqueID="21858621-ef10-4534-8529-fe451a093ed0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:49:20.249+0100" uniqueID="141a17b2-4039-446a-b3fb-e37c49f02495">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="WEB1c" name="WEB1c" elementKind="" description="When a user enters the login/password in to the LMS via the web, a one-time-tag is sent to the user’s mobile phone within 10s and must be entered within 60s to complete the log in." timeCreated="2011-12-09T14:49:41.835+0100" lastModified="2017-01-17T19:34:42.155+0100" uniqueID="de840cb1-2f52-425c-90f9-b2c906407854" workPackage="" abstractionLevel="" id="WEB1c">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.154+0100" uniqueID="4efbcd54-5594-4fa4-a3ff-81818e479caf" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:49:50.678+0100" uniqueID="5b83c3fa-c739-4e23-9d8c-4e0720d304d4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:49:50.682+0100" uniqueID="3a61f352-4225-49c8-b0c5-5befab2f2c04">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="WEB2" name="WEB2" elementKind="" description="All connections via the internet shall be secure." timeCreated="2011-12-09T14:50:07.949+0100" lastModified="2017-01-17T19:34:42.166+0100" uniqueID="cce6d3fd-03e3-4a13-8ab1-0df7e8a8749e" workPackage="" abstractionLevel="" id="WEB2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.165+0100" uniqueID="a1910e27-03fa-48fd-8855-a4a603d5578d" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:50:25.353+0100" uniqueID="87d0accb-b68b-4c51-a9f4-de52c898deb7">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:50:25.354+0100" uniqueID="3190570a-54e7-4a20-bf99-573b5f941c25">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="WEB3" name="WEB3" elementKind="" description="The web services shall be easy to learn and easy to use." timeCreated="2011-12-09T14:50:34.963+0100" lastModified="2017-01-17T19:34:42.177+0100" uniqueID="3642ca1c-5762-40e7-bb11-642055a99403" workPackage="" abstractionLevel="" id="WEB3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.177+0100" uniqueID="acbb4836-b333-4a60-bf3e-4580015a4010" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:50:47.467+0100" uniqueID="4f549294-fad9-4c87-8a1f-94f791b62a52">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:50:47.470+0100" uniqueID="9df906cd-6a28-4b33-9fb5-f8e2e445f0b2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="WEB3a" name="WEB3a" elementKind="" description="The web service front end shall be accessible." timeCreated="2011-12-09T14:51:11.910+0100" lastModified="2017-01-17T19:34:42.189+0100" uniqueID="1de1e4f8-5fe3-4cc0-8e59-1b0bd112c124" workPackage="" abstractionLevel="" id="WEB3a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.188+0100" uniqueID="9b31d745-e201-406f-9185-54b2cec9b5dc" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:51:26.829+0100" uniqueID="2566f29a-0e25-42ab-af90-9e2f02adbd01">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:51:26.830+0100" uniqueID="75206a23-904a-45c6-9ed7-8d440b2f32b5">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="WEB3b" name="WEB3b" elementKind="" description="95% of all readers signed up to AMDS without prior experience using the LMS terminal services shall be able to complete the online usability test suite in less than 5min. when using a desktop/laptop with a DSL connection, and 10min. when using a mobile device (smart phone, tablet computer, netbook with GSM modem)." timeCreated="2011-12-09T14:51:43.098+0100" lastModified="2017-01-17T19:34:42.202+0100" uniqueID="2cdf4bd5-27f6-4d4e-a6c0-71b53c580851" workPackage="" abstractionLevel="" id="WEB3b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.202+0100" uniqueID="9818882a-0bae-4afc-b123-ba177125d9d1" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:51:55.059+0100" uniqueID="98658fe0-9446-4476-be3e-e255377fd073">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:51:55.062+0100" uniqueID="4ac0210a-b688-4466-aa51-53331f6eaba6">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="WEB4" name="WEB4" elementKind="" description="The web service front end shall run in those web browsers that cover 85% if the browser market." timeCreated="2011-12-09T14:52:17.315+0100" lastModified="2017-01-17T19:34:42.214+0100" uniqueID="165b6dcd-447f-48a0-bdf1-f75b434e4545" workPackage="" abstractionLevel="" id="WEB4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.213+0100" uniqueID="7f88f124-c2ba-40ba-a567-207560aef300" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:52:25.876+0100" uniqueID="7355decb-b2c8-4e09-9eeb-8334178dc513">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:52:25.880+0100" uniqueID="720da462-02d1-40a0-a593-8855b62a5ab8">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Notifications" timeCreated="2011-12-09T14:52:53.363+0100" lastModified="2017-01-17T19:34:42.434+0100" uniqueID="62534c39-9581-4037-a99c-099773edc1ed">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.325+0100" uniqueID="944860b8-18f8-447c-be1d-08be6e1dfb06" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="NAT1" name="NAT1" elementKind="" description="There are different classes of messages with different characteristics and purposes:&#xD;&#xA;G – General: purely informative information (e.g. changes in opening hours or accession suggestions, newsletters, events)&#xD;&#xA;L –  Leases: shortly expiring or recently expired reservations and leases, availability of reserved media, due fees etc.&#xD;&#xA;A –  Account: re-/reactivation of an account, major changes in account status and capabilities, payment receipts;&#xD;&#xA;S –  Safety: login data/initial password, reader card, etc.&#xD;&#xA;T – Tags: one-time tags for sensitive online services" timeCreated="2011-12-09T14:53:06.677+0100" lastModified="2017-01-17T19:34:42.337+0100" uniqueID="7a77fe02-b1e5-4967-8dc0-e3e63f5c077a" workPackage="" abstractionLevel="" id="NAT1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.336+0100" uniqueID="5cb1e69b-b9e0-4be5-9e2a-b7821d8321ec" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:53:18.605+0100" uniqueID="97b925fe-85c4-4ca7-a347-dd9d6b222846">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:53:18.608+0100" uniqueID="26789a86-9a9d-4876-92ac-f3911040afe0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NAT2" name="NAT2" elementKind="" description="Users may chose among text to mobile, email, or paper mail to be used for receiving messages from LMS according to the following table." timeCreated="2011-12-09T14:54:02.356+0100" lastModified="2017-01-17T19:34:42.348+0100" uniqueID="61f453ca-5827-4c00-9f91-2c745be224dc" workPackage="" abstractionLevel="" id="NAT2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.348+0100" uniqueID="f4969f2e-b56e-4a16-8212-f8da1c9efc56" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:54:19.867+0100" uniqueID="47c180cc-fd51-457b-a482-2f4c707183db">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:54:19.868+0100" uniqueID="cb73d6ed-dda8-4b99-8b47-663e4bec224f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NAT3" name="NAT3" elementKind="" description="Readers shall only be notified with meaningful and compact messages." timeCreated="2011-12-09T14:54:37.808+0100" lastModified="2017-01-17T19:34:42.360+0100" uniqueID="da88acb9-63a0-4872-af8e-965d2a0afdf1" workPackage="" abstractionLevel="" id="NAT3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.359+0100" uniqueID="f733e9e5-3b37-4cd5-aa55-c11c0c886515" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:54:55.112+0100" uniqueID="7fd193f3-8c7b-49bc-9321-227271f34c27">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:54:55.115+0100" uniqueID="d33e87ad-4646-472a-903e-6d583c63276a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NAT3a" name="NAT3a" elementKind="" description="G-type messages are only sent outside of the LMS when the user explicitly signed up for them." timeCreated="2011-12-09T14:55:06.500+0100" lastModified="2017-01-17T19:34:42.371+0100" uniqueID="3a96b24a-0b9d-4fde-b667-5beadc380902" workPackage="" abstractionLevel="" id="NAT3a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.371+0100" uniqueID="f32ee69e-1292-41ee-a424-d212446dbe11" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:55:15.046+0100" uniqueID="0cb77939-576c-4873-a1ae-dffb8f4b995b">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:55:15.047+0100" uniqueID="dd7f2571-8c6f-4ef1-8b52-a10d7639b0d2">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NAT3b" name="NAT3b" elementKind="" description="A message is sent at most once per channel." timeCreated="2011-12-09T14:55:37.689+0100" lastModified="2017-01-17T19:34:42.383+0100" uniqueID="3a2748d8-956c-47cc-a5a4-42e2c0648065" workPackage="" abstractionLevel="" id="NAT3b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.383+0100" uniqueID="409f17e6-6dee-4393-a406-7a27c22b1493" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:55:43.140+0100" uniqueID="2ca8d458-570f-4d03-b025-11596060bd3c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:55:43.143+0100" uniqueID="cc9c1476-7c7c-4a34-9cf6-58e23a788330">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NAT3c" name="NAT3c" elementKind="" description="Sets of messages with the same type of event shall be bundled into a single message." timeCreated="2011-12-09T14:56:00.848+0100" lastModified="2017-01-17T19:34:42.395+0100" uniqueID="f6cd88ed-d088-4abb-98f1-661f0f34b5fa" workPackage="" abstractionLevel="" id="NAT3c">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.394+0100" uniqueID="5323f5b3-2b73-4b45-ad4c-020baa532c38" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:56:20.923+0100" uniqueID="291d778f-d938-4be9-a8fd-f0a4bd1033ac">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:56:20.927+0100" uniqueID="10bc3e94-be6a-4539-b18a-7c84fb589cec">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NAT4" name="NAT4" elementKind="" description="Messages other than of type T are sent at most once a day per account." timeCreated="2011-12-09T14:56:32.058+0100" lastModified="2017-01-17T19:34:42.408+0100" uniqueID="557a597e-14d2-4cbc-8163-57d3d424b1db" workPackage="" abstractionLevel="" id="NAT4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.408+0100" uniqueID="da144c9c-dd45-4b3c-98fd-b43e24e4385c" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:56:48.184+0100" uniqueID="a2be021e-d019-4288-ac60-a3073843e117">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:56:48.188+0100" uniqueID="f59b4156-323b-4b27-ac98-099056ac1487">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NAT5" name="NAT5" elementKind="" description="Messages shall be sent to their respective recipient within 12h of triggering them" timeCreated="2011-12-09T14:57:08.453+0100" lastModified="2017-01-17T19:34:42.423+0100" uniqueID="73a6e281-6727-4a25-b837-0e879470fe67" workPackage="" abstractionLevel="" id="NAT5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.422+0100" uniqueID="0fed9fff-e551-4717-b374-f38140af08d8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:57:20.402+0100" uniqueID="f9eb4e9a-cdc3-485c-aeba-855230ea3561">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:57:20.405+0100" uniqueID="a3771aef-85d5-4766-961c-26311a40569c">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="NAT6" name="NAT6" elementKind="" description="Chief librarians are notified about all starts of the system and significant events during startup." timeCreated="2011-12-09T14:57:30.948+0100" lastModified="2017-01-17T19:34:42.434+0100" uniqueID="77300c0d-cbb6-46e3-8cce-e604d2c0380f" workPackage="" abstractionLevel="" id="NAT6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.434+0100" uniqueID="1d994461-08c2-4745-b032-84781b2e9fe6" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:57:43.223+0100" uniqueID="ba762469-1f6e-4a43-a785-db63ff21660a">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:57:43.227+0100" uniqueID="6fc63452-c847-43b8-97f3-382c65c7871f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Third Party Systems" timeCreated="2011-12-09T15:06:22.516+0100" lastModified="2017-01-17T19:34:42.872+0100" uniqueID="f7daa6a9-2422-4ade-b64b-fed2056cdc6e">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.591+0100" uniqueID="260b283a-b34a-4d1c-aa02-d0c44e2e6ec8" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="BookStation" timeCreated="2011-12-09T15:01:48.462+0100" lastModified="2017-01-17T19:34:42.742+0100" uniqueID="ba10a192-4b73-4174-8b3b-63ef2578a68e">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.672+0100" uniqueID="c84bd3cc-b21d-4ca2-924f-468c18cb1735" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="BS1" name="BS1" elementKind="" description="The BookStationTM provides an administration interface for portable devices such as web pads." timeCreated="2011-12-09T15:02:04.152+0100" lastModified="2017-01-17T19:34:42.684+0100" uniqueID="4f4123a8-3c11-42b8-9967-27cc11008ffd" workPackage="" abstractionLevel="" id="BS1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.683+0100" uniqueID="8648e5b6-c48f-4907-9669-0c68bb3114ad" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T15:02:21.193+0100" uniqueID="6bc4e3cf-c4cc-487a-93f0-942ca5900096">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:02:21.195+0100" uniqueID="3f70e6c6-d1c2-4106-8222-af22d53d69b4">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="BS2" name="BS2" elementKind="" description="A load/unload record comprises the compartment id, a reader id, and a set of copy ids." timeCreated="2011-12-09T15:02:39.156+0100" lastModified="2017-01-17T19:34:42.695+0100" uniqueID="ebef8953-ab23-4a00-a576-e9422f5fa481" workPackage="" abstractionLevel="" id="BS2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.695+0100" uniqueID="a6abee97-1b16-48fc-ae06-e15e6ea069d1" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T15:03:08.715+0100" uniqueID="9425c007-1e33-42dc-8f20-a3676586a854">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:03:08.717+0100" uniqueID="9cdab1a7-021e-4593-8dd5-4a18280d2669">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="BS3" name="BS3" elementKind="" description="The BookStationTM records the status of each compartment (full/empty, deliver/return), and to which reader its contents belong (if any)." timeCreated="2011-12-09T15:03:20.465+0100" lastModified="2017-01-17T19:34:42.706+0100" uniqueID="1fd41505-e25e-4cd8-9950-c93f159bf263" workPackage="" abstractionLevel="" id="BS3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.706+0100" uniqueID="3b9ee4be-c8e7-4fbe-9991-90f97ee0bbe6" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T15:03:39.681+0100" uniqueID="f422bbdf-0174-482a-a8de-a2543aba9472">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:03:39.685+0100" uniqueID="75b623dc-0e5a-40e8-9e53-af66ac696240">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="BS4" name="BS4" elementKind="" description="If a load data set is forwarded to the LMS, the LMS interface triggers the reader notification about the copies contained in the load data set being available." timeCreated="2011-12-09T15:03:51.988+0100" lastModified="2017-01-17T19:34:42.718+0100" uniqueID="702b4f12-3e5d-4723-83a8-c57ebbc74c8e" workPackage="" abstractionLevel="" id="BS4">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.718+0100" uniqueID="e69fdfc9-acc8-4d2c-93e5-fedd615237d6" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T15:04:01.105+0100" uniqueID="fff7314a-0354-44f9-9deb-c1768d4e29a1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:04:01.109+0100" uniqueID="378be913-6cf9-4646-ba05-5af1ae159e10">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="BS5" name="BS5" elementKind="" description="If an unload data set is forwarded to the LMS, the LMS interface initiates lease termination or lease cancellation actions as appropriate for all the copies included in the unload data set." timeCreated="2011-12-09T15:04:19.518+0100" lastModified="2017-01-17T19:34:42.730+0100" uniqueID="4378eab9-9385-47a7-b7b4-05d492b34306" workPackage="" abstractionLevel="" id="BS5">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.729+0100" uniqueID="65af3d9e-4940-40bf-8e11-609783412024" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T15:04:34.379+0100" uniqueID="bbbcb354-7a2e-413c-8bf2-ae85305c312e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:04:34.382+0100" uniqueID="38330690-d623-4140-92b2-7f4ce65c7f9f">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="BS6" name="BS6" elementKind="" description="On demand, BSControl sends a list of those compartments to the BSS administration interface, that currently contain copies that are to be returned to the library." timeCreated="2011-12-09T15:04:54.053+0100" lastModified="2017-01-17T19:34:42.742+0100" uniqueID="79e0a4b5-fdb3-4dc6-a801-b370453c7f37" workPackage="" abstractionLevel="" id="BS6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.741+0100" uniqueID="5e7a1809-d4c5-43e9-808d-17d656f7b2e9" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T15:05:03.356+0100" uniqueID="494a5bc3-ed5e-4c10-81c2-e884884d74fa">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:05:03.358+0100" uniqueID="8651e66e-2741-4bc2-9576-821c9bba1537">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="BookTip" timeCreated="2011-12-09T14:58:11.335+0100" lastModified="2017-01-17T19:34:42.872+0100" uniqueID="7077963f-a8ba-4b7e-89ac-bcd658ccec55">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.810+0100" uniqueID="e9a59aff-fd40-485b-8790-6b3b80d4f95b" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" label="BT1" name="BT1" elementKind="" description="BookTipTM is fed by vectors of media leased by a reader (“personal preference vectors”)." timeCreated="2011-12-09T14:58:26.504+0100" lastModified="2017-01-17T19:34:42.822+0100" uniqueID="2019ca0e-7e22-4d7a-9580-714507e8d4ac" workPackage="" abstractionLevel="" id="BT1">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.821+0100" uniqueID="7bf8f8b1-10d2-49fd-b704-7384ce9c90d3" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:58:45.295+0100" uniqueID="a413e826-f84a-43f5-8f48-616fd62b70f1">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:58:45.298+0100" uniqueID="62c28fe4-48e4-4c88-8f7d-c92a446e2145">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="BT1a" name="BT1a" elementKind="" description="BookTipTM can extract personal preference vectors from a lease data base." timeCreated="2011-12-09T14:59:04.629+0100" lastModified="2017-01-17T19:34:42.834+0100" uniqueID="f6b569e5-8460-4eda-9fda-8cde4634edce" workPackage="" abstractionLevel="" id="BT1a">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.833+0100" uniqueID="b7d3e86c-f615-4d47-81cc-cecaa30402fe" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:59:15.125+0100" uniqueID="9a199bc0-d6e8-4d8c-bc2d-523329d9c455">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:59:15.129+0100" uniqueID="69cad797-e39e-4b3a-883c-164f431145c0">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="BT1b" name="BT1b" elementKind="" description="BookTipTM can be fed by online lease transactions." timeCreated="2011-12-09T14:59:35.109+0100" lastModified="2017-01-17T19:34:42.848+0100" uniqueID="fdebb5be-ac69-47cc-bb35-26eb93c27b0d" workPackage="" abstractionLevel="" id="BT1b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.847+0100" uniqueID="07dff6b9-26b8-4989-bfea-07a00b8b58a0" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T14:59:53.594+0100" uniqueID="a8c04de8-80d7-45e8-8c10-3ef9b7d29888">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:59:53.597+0100" uniqueID="b0420b55-565e-4780-a759-6064bb91f859">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="BT2" name="BT2" elementKind="" description="A suggestion is obtained by presenting BookTipTM with a non-empty set of media identifiers." timeCreated="2011-12-09T15:00:07.047+0100" lastModified="2017-01-17T19:34:42.860+0100" uniqueID="a37b5adf-2c47-46be-9e71-0b863eea5112" workPackage="" abstractionLevel="" id="BT2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.859+0100" uniqueID="c30241d8-bbf8-4d32-80a3-804c49dde10b" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T15:00:17.148+0100" uniqueID="f6edc4ea-41d2-48f3-970c-b99422031e56">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:00:17.151+0100" uniqueID="0b07ba3c-82f3-4f64-a63a-505eaea1ca24">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" label="BT3" name="BT3" elementKind="" description="A suggestion request may be parameterized by media type, age restriction, and medium availability." timeCreated="2011-12-09T15:00:33.554+0100" lastModified="2017-01-17T19:34:42.872+0100" uniqueID="e617dc9f-c4bd-41d3-9101-2fcaea5bf8b4" workPackage="" abstractionLevel="" id="BT3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.871+0100" uniqueID="6036a6c0-6f52-4087-aca9-057de1eb98a8" severity="Mistake">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
              </text>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-12-09T15:00:44.179+0100" uniqueID="672cd4b4-3ba4-4267-9f2e-b402aee3b42e">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:00:44.180+0100" uniqueID="2b3933e7-3616-4a41-85fd-5bba6ce42709">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
  </contents>
  <longDescription>
    <fragments xsi:type="text:FormattedText" text=""/>
  </longDescription>
</file:File>
