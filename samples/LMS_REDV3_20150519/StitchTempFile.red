<?xml version="1.0" encoding="ASCII"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:class="dk.dtu.imm.red.visualmodeling.visualmodel.class" xmlns:comment="dk.dtu.imm.red.core.comment" xmlns:datatype="dk.dtu.imm.red.specificationelements.modelelement.datatype" xmlns:file="dk.dtu.imm.red.core.file" xmlns:multiplicity="dk.dtu.imm.red.specificationelements.modelelement.multiplicity" xmlns:text="dk.dtu.imm.red.core.text" xmlns:visualmodel="dk.dtu.imm.red.visualmodeling" name="StitchTempFile.red" timeCreated="2016-09-21T16:31:57.053+0200" lastModified="2017-01-17T19:34:42.893+0100" uniqueID="c36118bd-66b3-422b-abc3-115bbc091274" license="Apache 2.0 (c) Harald Stoerrle">
  <commentlist>
    <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.888+0100" uniqueID="0b791771-b4c1-4bbf-a1db-93492cae14bf" severity="Warning">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <text>
        <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
      </text>
    </comments>
  </commentlist>
  <creator name="" timeCreated="2016-09-21T16:31:57.053+0200" uniqueID="73ee5c7a-1361-45ad-b0b1-7b0dcfc9ada2" id="" email="" initials="">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </creator>
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="visualmodel:Diagram" label="(AMDS1cl+AMDS4cl)" name="Stitch" description="Auto merged from AMDS4cl and AMDS1cl" timeCreated="2017-01-04T13:41:07.702+0100" lastModified="2017-01-17T19:34:42.891+0100" uniqueID="37d2be57-f4f7-4600-b9fe-0daa848fcac8" mergeLog="Flattening phase - total number of MergeElements :7&#xD;&#xA;.........  NEW FIND-MATCH PHASE .........&#xD;&#xA;FIND-MATCH TERMINATE!&#xD;&#xA;---------  FINAL MATCH KNOWLEDGE BASE ----------&#xD;&#xA;Final list of Matching sets:&#xD;&#xA;&#xD;&#xA;Number of output elements: 7&#xD;&#xA;Total execution time: 7 ms&#xD;&#xA;">
    <commentlist>
      <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.891+0100" uniqueID="97778aa7-a449-4a4a-a042-89019ff221a1" severity="Mistake">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <text>
          <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
        </text>
      </comments>
    </commentlist>
    <creator name="" timeCreated="2017-01-04T13:41:07.710+0100" uniqueID="26f31d5f-eeb9-472a-bf9e-b6932616889f" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <VisualDiagram Location="0,0" Diagram="//@contents.0/@VisualDiagram" DiagramType="Class" isStitchOutput="true">
      <Elements xsi:type="class:VisualClassElement" Location="437,123" Bounds="200,150" Parent="//@contents.0/@VisualDiagram" Diagram="//@contents.0/@VisualDiagram" IsSketchy="true" Name="CopyOrder" visualID="_EEM4cdJ7EeaQt6nZ63Rv5A" stitchTo="//@contents.1/@VisualDiagram/@Elements.1/@stitchFrom.0">
        <stitchFrom>
          <stitchOrigin xsi:type="class:VisualClassElement" href="REQ.red#_9Yu7MP5YEeSzkfJMf5_daA"/>
        </stitchFrom>
      </Elements>
      <Elements xsi:type="class:VisualEnumerationElement" Location="74,81" Bounds="163,83" Parent="//@contents.0/@VisualDiagram" Diagram="//@contents.0/@VisualDiagram" IsSketchy="true" Name="MediaKind" visualID="_EENfg9J7EeaQt6nZ63Rv5A" stitchTo="//@contents.1/@VisualDiagram/@Elements.2/@stitchFrom.0">
        <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_EENfg9J7EeaQt6nZ63Rv5A" Diagram="//@contents.0/@VisualDiagram" Name="media type = physical" visualID="_EEOGkdJ7EeaQt6nZ63Rv5A" stitchTo="//@contents.1/@VisualDiagram/@Elements.2/@Elements.0/@stitchFrom.0">
          <stitchFrom>
            <stitchOrigin xsi:type="class:VisualEnumerationLiteral" href="REQ.red#_9Ypbof5YEeSzkfJMf5_daA"/>
          </stitchFrom>
        </Elements>
        <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_EENfg9J7EeaQt6nZ63Rv5A" Diagram="//@contents.0/@VisualDiagram" Name="delivery = online" visualID="_EEOtodJ7EeaQt6nZ63Rv5A" stitchTo="//@contents.1/@VisualDiagram/@Elements.2/@Elements.1/@stitchFrom.0">
          <stitchFrom>
            <stitchOrigin xsi:type="class:VisualEnumerationLiteral" href="REQ.red#_9YqCsP5YEeSzkfJMf5_daA"/>
          </stitchFrom>
        </Elements>
        <stitchFrom>
          <stitchOrigin xsi:type="class:VisualEnumerationElement" href="REQ.red#_9YpboP5YEeSzkfJMf5_daA"/>
        </stitchFrom>
      </Elements>
      <Elements xsi:type="class:VisualEnumerationElement" Location="309,79" Bounds="125,100" Parent="//@contents.0/@VisualDiagram" Diagram="//@contents.0/@VisualDiagram" IsSketchy="true" Name="DeliveryKind" visualID="_EEPUs9J7EeaQt6nZ63Rv5A" stitchTo="//@contents.1/@VisualDiagram/@Elements.3/@stitchFrom.0">
        <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_EEPUs9J7EeaQt6nZ63Rv5A" Diagram="//@contents.0/@VisualDiagram" Name="media type = physical" visualID="_EEP7wdJ7EeaQt6nZ63Rv5A" stitchTo="//@contents.1/@VisualDiagram/@Elements.3/@Elements.0/@stitchFrom.0">
          <stitchFrom>
            <stitchOrigin xsi:type="class:VisualEnumerationLiteral" href="REQ.red#_9YqCsv5YEeSzkfJMf5_daA"/>
          </stitchFrom>
        </Elements>
        <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_EEPUs9J7EeaQt6nZ63Rv5A" Diagram="//@contents.0/@VisualDiagram" Name="delivery = online" visualID="_EEQi0dJ7EeaQt6nZ63Rv5A" stitchTo="//@contents.1/@VisualDiagram/@Elements.3/@Elements.1/@stitchFrom.0">
          <stitchFrom>
            <stitchOrigin xsi:type="class:VisualEnumerationLiteral" href="REQ.red#_9YqCs_5YEeSzkfJMf5_daA"/>
          </stitchFrom>
        </Elements>
        <stitchFrom>
          <stitchOrigin xsi:type="class:VisualEnumerationElement" href="REQ.red#_9YqCsf5YEeSzkfJMf5_daA"/>
        </stitchFrom>
      </Elements>
    </VisualDiagram>
  </contents>
  <contents xsi:type="visualmodel:Diagram" label="(AMDS6cl+(AMDS1cl+AMDS4cl))" name="Stitch" description="Auto merged from (AMDS1cl+AMDS4cl) and AMDS6cl" timeCreated="2017-01-04T13:41:08.662+0100" lastModified="2017-01-17T19:34:42.893+0100" uniqueID="a8e26da6-0f5c-47f6-bf37-345afdd72425" mergeLog="Flattening phase - total number of MergeElements :12&#xD;&#xA;.........  NEW FIND-MATCH PHASE .........&#xD;&#xA;Comparison does not match between VisualClassElement 0(LeaseCopy) and 5(CopyOrder) due to matchValue=FALSE&#xD;&#xA;FIND-MATCH TERMINATE!&#xD;&#xA;---------  FINAL MATCH KNOWLEDGE BASE ----------&#xD;&#xA;Final list of Matching sets:&#xD;&#xA;&#xD;&#xA;Number of output elements: 12&#xD;&#xA;Total execution time: 12 ms&#xD;&#xA;">
    <commentlist>
      <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:42.893+0100" uniqueID="48a50629-3718-447f-abac-d21fd86698f1" severity="Mistake">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <text>
          <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
        </text>
      </comments>
    </commentlist>
    <creator name="" timeCreated="2017-01-04T13:41:08.675+0100" uniqueID="263e1a98-de15-47fc-b1f6-06d04b6a5d12" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <VisualDiagram Location="0,0" Diagram="//@contents.1/@VisualDiagram" DiagramType="Class" isStitchOutput="true">
      <Elements xsi:type="class:VisualClassElement" Location="117,131" Bounds="200,150" Parent="//@contents.1/@VisualDiagram" Diagram="//@contents.1/@VisualDiagram" IsSketchy="true" Name="LeaseCopy" visualID="_ENW0ddJ7EeaQt6nZ63Rv5A">
        <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_ENW0ddJ7EeaQt6nZ63Rv5A" Diagram="//@contents.1/@VisualDiagram" Name="mediumID" visualID="_ENXbgdJ7EeaQt6nZ63Rv5A">
          <stitchFrom>
            <stitchOrigin xsi:type="class:VisualClassAttribute" href="REQ.red#_9YwwYf5YEeSzkfJMf5_daA"/>
          </stitchFrom>
          <Type xsi:type="datatype:NoType"/>
          <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
        </Elements>
        <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_ENW0ddJ7EeaQt6nZ63Rv5A" Diagram="//@contents.1/@VisualDiagram" Name="issueDate" visualID="_ENXbg9J7EeaQt6nZ63Rv5A">
          <stitchFrom>
            <stitchOrigin xsi:type="class:VisualClassAttribute" href="REQ.red#_9YwwYv5YEeSzkfJMf5_daA"/>
          </stitchFrom>
          <Type xsi:type="datatype:NoType"/>
          <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
        </Elements>
        <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_ENW0ddJ7EeaQt6nZ63Rv5A" Diagram="//@contents.1/@VisualDiagram" Name="deliverDate" visualID="_ENYCkdJ7EeaQt6nZ63Rv5A">
          <stitchFrom>
            <stitchOrigin xsi:type="class:VisualClassAttribute" href="REQ.red#_9YwwY_5YEeSzkfJMf5_daA"/>
          </stitchFrom>
          <Type xsi:type="datatype:NoType"/>
          <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
        </Elements>
        <Elements xsi:type="class:VisualClassAttribute" Location="0,0" Bounds="0,0" Parent="_ENW0ddJ7EeaQt6nZ63Rv5A" Diagram="//@contents.1/@VisualDiagram" Name="reader" visualID="_ENYpodJ7EeaQt6nZ63Rv5A">
          <stitchFrom>
            <stitchOrigin xsi:type="class:VisualClassAttribute" href="REQ.red#_9YwwZP5YEeSzkfJMf5_daA"/>
          </stitchFrom>
          <Type xsi:type="datatype:NoType"/>
          <Multiplicity xsi:type="multiplicity:NoMultiplicity"/>
        </Elements>
        <stitchFrom>
          <stitchOrigin xsi:type="class:VisualClassElement" href="REQ.red#_9YwwYP5YEeSzkfJMf5_daA"/>
        </stitchFrom>
      </Elements>
      <Elements xsi:type="class:VisualClassElement" Location="677,109" Bounds="200,150" Parent="//@contents.1/@VisualDiagram" Diagram="//@contents.1/@VisualDiagram" IsSketchy="true" Name="CopyOrder" visualID="_ENYpo9J7EeaQt6nZ63Rv5A">
        <stitchFrom stitchOrigin="_EEM4cdJ7EeaQt6nZ63Rv5A"/>
      </Elements>
      <Elements xsi:type="class:VisualEnumerationElement" Location="317,81" Bounds="163,83" Parent="//@contents.1/@VisualDiagram" Diagram="//@contents.1/@VisualDiagram" IsSketchy="true" Name="MediaKind" visualID="_ENZQs9J7EeaQt6nZ63Rv5A">
        <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_ENZQs9J7EeaQt6nZ63Rv5A" Diagram="//@contents.1/@VisualDiagram" Name="media type = physical" visualID="_ENZ3wdJ7EeaQt6nZ63Rv5A">
          <stitchFrom stitchOrigin="_EEOGkdJ7EeaQt6nZ63Rv5A"/>
        </Elements>
        <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_ENZQs9J7EeaQt6nZ63Rv5A" Diagram="//@contents.1/@VisualDiagram" Name="delivery = online" visualID="_ENae0dJ7EeaQt6nZ63Rv5A">
          <stitchFrom stitchOrigin="_EEOtodJ7EeaQt6nZ63Rv5A"/>
        </Elements>
        <stitchFrom stitchOrigin="_EENfg9J7EeaQt6nZ63Rv5A"/>
      </Elements>
      <Elements xsi:type="class:VisualEnumerationElement" Location="552,79" Bounds="125,100" Parent="//@contents.1/@VisualDiagram" Diagram="//@contents.1/@VisualDiagram" IsSketchy="true" Name="DeliveryKind" visualID="_ENbF49J7EeaQt6nZ63Rv5A">
        <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_ENbF49J7EeaQt6nZ63Rv5A" Diagram="//@contents.1/@VisualDiagram" Name="media type = physical" visualID="_ENcUANJ7EeaQt6nZ63Rv5A">
          <stitchFrom stitchOrigin="_EEP7wdJ7EeaQt6nZ63Rv5A"/>
        </Elements>
        <Elements xsi:type="class:VisualEnumerationLiteral" Location="0,0" Bounds="0,0" Parent="_ENbF49J7EeaQt6nZ63Rv5A" Diagram="//@contents.1/@VisualDiagram" Name="delivery = online" visualID="_ENc7EdJ7EeaQt6nZ63Rv5A">
          <stitchFrom stitchOrigin="_EEQi0dJ7EeaQt6nZ63Rv5A"/>
        </Elements>
        <stitchFrom stitchOrigin="_EEPUs9J7EeaQt6nZ63Rv5A"/>
      </Elements>
    </VisualDiagram>
  </contents>
  <longDescription>
    <fragments xsi:type="text:FormattedText" text=""/>
  </longDescription>
</file:File>
