<?xml version="1.0" encoding="UTF-8"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:comment="dk.dtu.imm.red.core.comment" xmlns:file="dk.dtu.imm.red.core.file" xmlns:folder="dk.dtu.imm.red.core.folder" xmlns:goal="dk.dtu.imm.red.specificationelements.goal" xmlns:persona="dk.dtu.imm.red.specificationelements.persona" xmlns:relationship="dk.dtu.imm.red.core.element.relationship" xmlns:requirement="dk.dtu.imm.red.specificationelements.requirement" xmlns:stakeholder="dk.dtu.imm.red.specificationelements.stakeholder" xmlns:text="dk.dtu.imm.red.core.text" xmlns:vision="dk.dtu.imm.red.specificationelements.vision" name="Library Management System.red" timeCreated="2011-11-16T11:20:19.039+0100" lastModified="2017-01-17T19:34:29.803+0100" uniqueID="6b73d005-1c99-406e-92b9-d0a1924ac213" license="Apache 2.0 (c) Harald Stoerrle">
  <commentlist>
    <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:03.708+0100" uniqueID="4151244d-1912-4398-b071-34c3eea8bacd" severity="Warning">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <text>
        <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
      </text>
    </comments>
  </commentlist>
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="folder:Folder" name="Library Management System" timeCreated="2011-11-16T11:20:19.039+0100" lastModified="2017-01-17T19:34:29.803+0100" uniqueID="4780e8dc-56c6-4456-972e-f75f20634430">
    <commentlist>
      <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.423+0100" uniqueID="2795589a-6aa9-432e-b408-510fe85efc66" severity="Warning">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <text>
          <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
        </text>
      </comments>
    </commentlist>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="vision:Vision" label="" name="LMS Vision" timeCreated="2011-11-16T13:23:27.656+0100" lastModified="2017-01-17T19:34:07.439+0100" uniqueID="2f12b6ae-95a5-4d0c-b559-6b1d3e39fafd" workPackage="">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.438+0100" uniqueID="30837ff2-d6e2-414b-a20d-1b18416eda21" severity="Mistake">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
          </text>
        </comments>
      </commentlist>
      <creator name="Anders" timeCreated="2011-11-16T13:23:45.801+0100" uniqueID="00b7f480-6f8c-4eb1-b5ac-64ef67d7959b">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T13:23:45.802+0100" uniqueID="47f7d44d-8d64-44f4-89e8-588c3ee12828">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <vision>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN lang=EN-US>&lt;FONT size=3>&lt;FONT face=Calibri>&lt;FONT size=2 face=Arial>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;FONT size=3>&lt;FONT face=Calibri>&lt;FONT size=2>&lt;FONT face=Arial>&lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot; lang=EN-US>For &lt;/SPAN>&lt;SPAN lang=EN-US>small to medium sized libraries &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>that&lt;/SPAN> struggle with innovating their service offering &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>the&lt;/SPAN> &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>Library Management System&lt;/A> (&lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A>) &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>is&lt;/SPAN> a new, &lt;/SPAN>&lt;SPAN style=&quot;mso-bidi-font-family: Calibri&quot; lang=EN-US>state of the art &lt;/SPAN>&lt;/FONT>&lt;/FONT>&lt;SPAN lang=EN-US>&lt;FONT size=2 face=Arial>information system &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>that&lt;/SPAN> handles all major library business processes from corpus management and reader handling to lending traffic automation. &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> allows to integrate traditional customer service with self-service kiosks and on-line access by readers. This helps reducing repetitive tasks of librarians dramatically, and allows to improve existing services in terms of convenience, speed, and reliability, and also adds new services for readers that help address new target groups currently not well reached by classical libraries. We expect a market penetration of 2% of all computer-supported public libraries in Europe within 5 years. &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>In comparison with &lt;/SPAN>manual library management currently in place at many libraries, &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>our product &lt;/SPAN>will reduce administrative costs, free staff capacity for strategic initiatives, and attract more users.&lt;/FONT> &lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/FONT>&lt;/FONT>&lt;/P>&lt;/FONT>&lt;/FONT>&lt;/FONT>&lt;/SPAN>"/>
      </vision>
    </contents>
    <contents xsi:type="folder:Folder" name="Stakeholders" timeCreated="2011-11-16T14:39:35.744+0100" lastModified="2017-01-17T19:34:07.867+0100" uniqueID="e92cbece-93e3-4137-98e2-8c6a8e6e1029">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.628+0100" uniqueID="56fa5770-b1ca-432a-87a0-8d270fcc93eb" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="stakeholder:Stakeholder" label="" name="Taarbæk Commune" elementKind="unspecified" description="" timeCreated="2011-11-16T14:41:16.704+0100" lastModified="2017-01-17T19:34:07.650+0100" uniqueID="3c70881e-c050-42d6-b12d-62ea48f9f11f" workPackage="" exposure="2" power="3" urgency="3" importance="3">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.649+0100" uniqueID="bd54e2ab-e93b-4d99-90e2-01303a0968c4" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element contains wrong relationship type: illustrates"/>
            </text>
          </comments>
        </commentlist>
        <creator name="Anders" timeCreated="2011-11-16T14:42:47.085+0100" uniqueID="5609af61-1621-4b1e-9ff5-aeaf06fb7b8b">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:27:12.255+0100" uniqueID="e1509e8b-94ab-4be4-b874-2248a33181d6" toElement="bc51e4b1-e551-45a5-8df9-c9e640e92434" relevance="" relationshipKind="illustrates">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.319+0100" uniqueID="9d7b7043-f84b-4620-abe0-b43bde8e7ccc" toElement="48d0e4c0-dffe-4608-bf99-92caeb1a0443" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T14:42:47.090+0100" uniqueID="29a22b0a-8f48-4015-bd45-b9c9754a38a8">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P class=MsoNormal style=&quot;PAGE-BREAK-AFTER: auto; TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot;>&lt;SPAN lang=EN-US>Tårbæk Commune has mainly two stakes in this project: (1) adding to the town’s attractiveness as a place to live, in particular to those of its inhabitants (particularly those between 6 and 35 years of age, which are also more likely to move away) so as to increase the town’s population; and (2) help improve the local economy by closer networking and cross-fertilization.&lt;?xml:namespace prefix = &quot;o&quot; ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P class=MsoNormal style=&quot;PAGE-BREAK-AFTER: auto; TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot;>&lt;SPAN lang=EN-US>We suggest that this stakeholder should be receiving regular official written reports to provide evidence of the project’s progress. A suitable interval and time point may be shortly before the monthly town council meetings. The client will have full access to all project internal documents and will be informed about all economically relevant plans and planning activities.&lt;?xml:namespace prefix = &quot;o&quot; ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P class=MsoNormal style=&quot;PAGE-BREAK-AFTER: auto; TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot;>&lt;SPAN lang=EN-US>Additionally, regular personal meetings between the project leader on the on hand side and the major, the Commune council, and the culture committee on the other will provide ample opportunity to demonstrate project progress, raise issues and questions, and communicate any problems early. &lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P class=MsoNormal style=&quot;PAGE-BREAK-AFTER: auto; TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot;>&lt;SPAN lang=EN-US>In particular, it will be important to include one or more people close to the major in the project staff as a client-supplier &lt;/SPAN>&lt;SPAN lang=EN-US style=&quot;mso-fareast-language: DE&quot;>liaison officer to form &lt;/SPAN>&lt;SPAN lang=EN-US>another, informal channel from the project to the client. This person may become a project sponsor in the Commune administration, and may act as a channel to signal potential problems very early to evaluate possible reactions. This person or persons may be part of the project controlling board and or the project steering board.&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P class=MsoNormal style=&quot;PAGE-BREAK-AFTER: auto; TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot;>&lt;SPAN lang=EN-US>Tårbæk Commune is the client and so naturally it is the single most important stakeholder which amounts to the highest possible scores in power and urgency. According to the scenario, albeit big, this project is only one of several activities launched by the Tårbæk Commune; whatever its outcome will be, the effect on Tårbæk may be large, but not unlimited. Therefore, the exposure is not assessed with the top score.&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="Executives / Owner" elementKind="" timeCreated="2011-11-16T14:58:10.746+0100" lastModified="2017-01-17T19:34:07.665+0100" uniqueID="7a91f6e9-f0d3-459c-9e3a-3b381191d0a8" workPackage="" exposure="2" power="3" urgency="3" importance="3">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.664+0100" uniqueID="4ab3f038-b552-440c-8bbd-715fb497b54f" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
            </text>
          </comments>
        </commentlist>
        <creator name="Anders" timeCreated="2011-11-16T15:21:50.873+0100" uniqueID="8a9eb9b8-381b-4c0e-aae6-7d5fe2a54243">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList>
          <comments timeCreated="2011-11-16T15:27:05.312+0100" uniqueID="9a69bc38-dc25-46cf-8888-9647d817feab">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <author timeCreated="2011-11-16T15:27:05.312+0100" uniqueID="33522305-4dd0-4d6e-8973-0ae17317bd8c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </author>
          </comments>
        </changeList>
        <responsibleUser name="Jakob" timeCreated="2011-11-16T15:21:50.877+0100" uniqueID="b19a5b02-4641-4a4b-b700-a8d78443858e">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>The stakes of the owners and the chief executives of the supplier will typically be almost identical, so we will refer to them as the supplier collectively. Primarily, the supplier &lt;/SPAN>&lt;SPAN lang=EN-US>wants to earn money, win market shares, expand its customer base, and want the company to prosper and grow. Its reputation among potential customers is a valuable asset, but the company is more important than this one project. So, should serious problems arise, the costs and benefits will be weighed, and if necessary they will consider terminating the contract with a loss.&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>The &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> project consists of three parts: an initial inception project billed on effort with a profitable hourly rate, a requirements analysis project which is also billed on effort but uses an hourly rate which only covers cost and is also capped, and a competitively calculated fixed price for the development as such. With this construction, the inception is self-sufficient, the development must be outsourced, and the analysis must either be an investment to enter the market and learn about the domain as a company, or to make profit from selling the product to other customers after the initial deployment. For this to happen, requirements beyond those found in Tårbæk must be considered. However, the initial customer is needed as a show-case and proof of concept for the product, so the overall requirements may contain more than those from Tårbæk, but not less.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>No specific activities necessary beyond the usual project controlling.&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>Just like the clients, the company owners may terminate the project at any time, so they have high power and high urgency. If the project turns out successful, it will generate profits, possibly help in acquiring new customers, and generally secure the company’s long-term well-being. If the project should fail, there may be the opposite developments. Since this is not the only current project, the repercussions are limited making for a medium exposure. &lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="Project Manager" elementKind="" timeCreated="2011-11-16T15:32:53.675+0100" lastModified="2017-01-17T19:34:07.685+0100" uniqueID="27218136-b608-4ff7-bab3-983d7fb8deba" workPackage="" exposure="2" power="3" urgency="3" importance="3">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.685+0100" uniqueID="13c0dc82-e103-4d00-81ea-c184937ff801" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
            </text>
          </comments>
        </commentlist>
        <creator name="" timeCreated="2011-11-16T15:34:26.489+0100" uniqueID="925ee7f6-8997-44e5-a0bb-2abfd77919f6">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T15:34:26.490+0100" uniqueID="64ed482f-c589-45e2-a0b1-430b4db9eb01">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The stakes of the supplier project lead are mainly of personal or professional nature, i.e. to secure and promote his career, to grow professionally and earn more experience, and to enjoy the personal satisfaction of a successful project. Potentially, the quality of his job may make itself seen in terms of remuneration, too (e.g. a bonus).&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Providing personal financial incentives may be a good way to prevent a skilled project manager from leaving a project for motivational reasons, but providing him with the necessary means and leeway to achieve the best possible outcome is typically more important, so no extra engagement measure need to be provided.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The project manager is responsible for the projects planning and proper execution. Practical experience and empirical evidence shows that the qualities of this person are among the biggest success factors of a project. A project manager who does not do his or her job properly or completely is a great risk to the project and so this person has great and immediate power towards the project, both positive and negative. Reasons for affecting the project negatively are manifold, either inadvertently (e.g. through sickness or accident), because of interpersonal problems (e.g. poor relationships with other key players), or simply poor performance (e.g. lack of qualification). On the hand, the effects of a failing project on the project manager are not quite as big in many cases in the given local socio-economic situation.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="Chief Librarians" elementKind="" timeCreated="2011-11-16T15:45:58.728+0100" lastModified="2017-01-17T19:34:07.714+0100" uniqueID="20f5974f-cc93-4ac6-b1ba-95438deeb53f" workPackage="" exposure="3" power="3" urgency="2" importance="3">
        <commentlist/>
        <creator name="" timeCreated="2011-11-16T15:56:03.608+0100" uniqueID="99e83531-279c-4283-8866-6a2d11052905">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.341+0100" uniqueID="47f0e220-6450-4209-bf25-55f96a202b8b" toElement="dd13c195-85ce-4025-a378-855e767dec07" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.359+0100" uniqueID="98ee3f6f-4fe6-4d32-98df-532952678aa1" toElement="2c9f2b17-8a5c-4c62-984f-e38e8c81f5bd" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.380+0100" uniqueID="98a3a2ab-0874-4dd4-aeb3-ca9312d694e4" toElement="dbd8acb9-23a3-4219-bcd5-a99fa9f1b11e" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.398+0100" uniqueID="701a2957-72b3-4486-b284-9a8563f91f89" toElement="0e0e51aa-cd72-49af-8997-11a8be18c50c" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.416+0100" uniqueID="366c8ff0-ea06-4ff7-90ef-c067185074da" toElement="28a2841e-e1b9-4cc5-9a98-3d2c9bd896f5" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.436+0100" uniqueID="8433f59f-4888-4745-ad9b-e47702c6fe7d" toElement="eaa2fee8-e318-4753-96f3-01b3fcec95cc" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T15:56:03.612+0100" uniqueID="852b3418-4864-4efc-ae5a-fc8ef3f6f912">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The new system will heavily influence the work of the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>, although it is unlikely to threaten their jobs (which involve many activities that cannot and will not be automated). However, some of the jobs of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> they supervise may be in danger which could both decrease their importance and may threaten people that they feel responsible for.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>While it is extremely important to win the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A> over for the project, it is important not to overload them. While the project is unfolding, they still have their regular work to do, and they might be more interested in getting that done first. So, while clearly being the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> with the best overview, it may be better to involve them only in the decision making and ask them to nominate some of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> to assist with requirements elicitation instead of the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>Due to their great job experience, the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A> are important sources of requirements as well as respected figures in the library (i.e. for the other &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A>), for their customers (i.e. the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A>), and for the &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>Commune&lt;/A> as such. So their input and opinion leadership can provide great impact both positively and negatively on the project. &lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>Most of all, however, they are the most knowledgeable employees of the client, Tårbæk Commune, and if domain expertise is required by the client, it is very likely coming from the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>. This makes them extremely powerful stakeholders whose influence is less immediate than that of the client proper, but still quite direct. In contrast to the client, however, the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A> are much more affected by the project and its outcome. Active involvement of the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>, therefore, is crucial to the project’s success.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" label="" name="Readers" elementKind="unspecified" description="" timeCreated="2011-11-16T17:16:43.192+0100" lastModified="2017-01-17T19:34:07.746+0100" uniqueID="b5798927-0042-4dab-bf5b-e8fddfdf844b" workPackage="" exposure="3" power="3" importance="2">
        <commentlist/>
        <creator name="" timeCreated="2011-11-16T17:17:01.219+0100" uniqueID="718524ca-e5f8-4daf-9db0-31afb619e818">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.321+0100" uniqueID="5df53374-c890-42c5-b091-deb23a5a391a" toElement="48d0e4c0-dffe-4608-bf99-92caeb1a0443" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.454+0100" uniqueID="27f717f3-b4d2-4bc9-88f4-73b95b12dd06" toElement="da439811-9b76-4dff-9edd-218d9f7d0977" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.168+0100" uniqueID="cfc2f79e-a2d4-404a-a37f-6ba79f86e6f8" toElement="1a423134-72a7-48fd-b5d8-26ad25900512" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.280+0100" uniqueID="4ce7515a-b714-44be-abae-62ffeaa17e1f" toElement="294cd77a-950d-4b00-b94c-dfd39e034f3d" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.301+0100" uniqueID="e28af85a-bb48-41af-a0a9-34c5b6f965ca" toElement="a027a898-0f20-473e-a0aa-25ad6bb7665f" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.330+0100" uniqueID="feb2bdb2-0825-42b8-8379-dde245598cb5" toElement="1cb60663-ed14-4d87-a38d-d12bb968f391" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T17:17:01.224+0100" uniqueID="84424ddf-7c71-4952-9401-698527e21c61">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P class=MsoNormal style=&quot;PAGE-BREAK-AFTER: auto; TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot;>&lt;SPAN lang=EN-US>The &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> are interested in getting the typical library services in a friendly, competent, and fast way, at an affordable price. Innovative services like online access that the Tårbæk library may provide as a consequence of the &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> development project may be welcome but will probably not be asked for specifically by the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A>.&lt;?xml:namespace prefix = &quot;o&quot; ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P class=MsoNormal style=&quot;PAGE-BREAK-AFTER: auto; TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot;>&lt;SPAN lang=EN-US>Being the constituents of Tårbæk Commune, the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> have great interest in the project success, although they may not even be aware of it.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text=""/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P class=MsoNormal style=&quot;PAGE-BREAK-AFTER: auto; TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot;>&lt;SPAN lang=EN-US>As the clients of the Library, it is up to the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> to accept or reject the new services offered by made possible through the system to be built. If the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> do not accept the changes resulting from the project, the project will be considered a failure by the client and may threaten the reelection of the mayor. Therefore, the power of the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> is high. Since this power is applied only indirectly, however, the urgency of this stakeholder is low, because it takes a long time to organize and express this power. The exposure of &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> to the project is also not very high because &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers &lt;/A>will only realize that there is a new system after it has been deployed, and possibly not even then.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = &quot;o&quot; ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="Developers" elementKind="" timeCreated="2011-11-16T17:20:17.472+0100" lastModified="2017-01-17T19:34:07.768+0100" uniqueID="2fd80d5c-f8bf-40a6-97d7-6e751f81dfe9" workPackage="" urgency="3" importance="2">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.767+0100" uniqueID="7ba89b8a-362d-4149-8ed1-ee40ad188d77" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
            </text>
          </comments>
        </commentlist>
        <creator name="" timeCreated="2011-11-16T17:21:36.770+0100" uniqueID="e046db3e-1f21-4cf8-b5a4-58a1c693e983">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T17:21:36.774+0100" uniqueID="f2bba3af-5dfc-475f-8d46-050ff3e035a8">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Each developer has professional stakes in the project keeping him busy for a prolonged period, i.e. job satisfaction, job security, professional experience, and career advancement. Possibly, pecuniary incentives like a bonus also depend on the project.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The normal methods of motivating the project staff apply, e.g. regular stand up meetings, a project blog, or posters on the project progress and main artifacts on the walls of the corridors at the development premises.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Collectively, the developers could act for or against the project at a medium level of urgency and with a medium level of power. They are not strongly affected by the project outcome.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="Librarians" elementKind="" timeCreated="2011-11-16T17:22:04.064+0100" lastModified="2017-01-17T19:34:07.786+0100" uniqueID="c80841d0-615c-49d0-99fe-38567f61c54e" workPackage="" exposure="3" power="2" urgency="2" importance="2">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.785+0100" uniqueID="fd9e51f3-917a-42b3-bcfc-26df988e685b" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
            </text>
          </comments>
        </commentlist>
        <creator name="" timeCreated="2011-11-16T17:23:21.317+0100" uniqueID="f1ffa4a1-2faf-4b0a-b6c1-e69aa0ea8619">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T17:23:21.321+0100" uniqueID="a4a24d1f-68eb-4a2d-b055-eae2d74d0b17">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>&lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>Librarians&lt;/A> are interested in improving or at least maintaining their work environment. Depending on the individuals, they may or may not appreciate change as such, but they would almost certainly appreciate if they can offer better services to their customers and have more time available to help them and make their job easier. The &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> have a great interest in securing their jobs.&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> are the most intensive users of the library system and so they also know a great deal about its usage, in particular the bread-and-butter functions. They may, however, lack the overview and experience expected from, e.g., the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>. We therefore chose not to directly involve &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> in the project, unless they are delegated for a particular function by the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>. On the one hand, the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> have a high workload and we believe their input would be not of very high quality. On the other hand, the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A> understand the needs of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> very well because they have all served as &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> for a long time, and are still taking over this role if needed. So, the position of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> is already well represented in the project. In order to secure support from the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A>, it will be vital to convince them that the system will actually increase their job security and long term job satisfaction, even if there are problems in the transitional period and they have to learn new skills.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> are the group of people most strongly affected by the project outcome, as it may change the way they work completely. It may require new skills, it may make their work much more dense or difficult, and it may even threaten their jobs. Conversely, if the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> are not able to work with the new system in the pre-production test phase due to problems of the new system, the client may decide to stop the development before the system goes into production, if the problems can be blamed on the system. So we assign medium power and urgency.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="Future LMS users" elementKind="" timeCreated="2011-11-16T17:25:36.788+0100" lastModified="2017-01-17T19:34:07.804+0100" uniqueID="72878282-6fdb-44bb-b9a5-7accc031caae" workPackage="" exposure="3" power="2" importance="2">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.803+0100" uniqueID="7d2e7c62-8ac0-4ea0-b6d1-b4b09fdef368" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
            </text>
          </comments>
        </commentlist>
        <creator name="" timeCreated="2011-11-16T17:32:35.270+0100" uniqueID="7f78c356-de67-42da-a321-df840b0e3f69">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T17:32:35.274+0100" uniqueID="6836fa07-cd12-4b9b-a989-0a1bb5323890">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P>&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>The stakes of future &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> users are naturally unknown, but presumably similar to those of the current clients and the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>We believe that the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A> of &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A> provide us with a good account of the domain requirements that are likely to be very similar for other libraries of the same profile (size, audience). One way to ensure adequate consideration of such requirements is to hire a &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarian&lt;/A> with a lot of job experience in different types of libraries to serve as a consultant to the requirements team. Another way is to visit trade fairs relevant to librarians and library owners to collect other requirements from the market early on. This may also act both as a first step to marketing the product.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>The &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> development is economically viable only under the assumption that it will be sold to new customers other than the Tårbæk Commune after the initial installation will have been successful. dd&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="Maintenance &amp; Operations" elementKind="" timeCreated="2011-11-16T17:34:09.312+0100" lastModified="2017-01-17T19:34:07.824+0100" uniqueID="11773630-0ea9-4f78-83dc-50d48eb60aea" workPackage="" exposure="3" power="2" importance="2">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.823+0100" uniqueID="854dd347-22e2-4074-a752-21bf21f8383b" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
            </text>
          </comments>
        </commentlist>
        <creator name="" timeCreated="2011-11-16T17:34:27.944+0100" uniqueID="8b72ff36-95f3-4112-9310-80bbc377631f">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T17:34:27.947+0100" uniqueID="f2c84621-0afe-4084-96b9-92b7226326ee">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>Given that the system is rather small and required to be easy to maintain by definition, the operations personnel will have no trouble with &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A>. The maintainers are interested in the technical quality of the product that will make their job easier. Since the maintenance staff of the supplier is identical to (part of) the development team, technological and learning issues should be no problem, either.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>The &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> will require very little maintenance and none on behalf of the client. On behalf of the supplier, maintenance will be crucial, however, in order to be able to cover future requirements from new customers and changing market demands. &lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P>&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: DE; mso-bidi-language: AR-SA&quot; lang=EN-US>&lt;FONT size=2 face=Arial>The maintenance and operations personnel can exert only little power (similar to developers) and only with some latency. Their exposure is also small, since there is not much difference from their point of view which system they are dealing with and it is only a small part of their job.&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="Local Media Retailers" elementKind="" timeCreated="2011-11-16T17:57:39.702+0100" lastModified="2017-01-17T19:34:07.839+0100" uniqueID="d84e6313-d0af-401d-9e5f-8fbb76bebd4e" workPackage="">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.838+0100" uniqueID="a6d5e234-ff5c-40b8-8023-f189718807d3" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
            </text>
          </comments>
        </commentlist>
        <creator name="" timeCreated="2011-11-16T18:00:20.426+0100" uniqueID="db95dedc-6ded-4d7b-9e95-0c14f5a54feb">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T18:00:20.430+0100" uniqueID="1460d275-9d54-4f27-93d5-2f62cba39c9c">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Retailers are interested in profits, non-profit cultural institutions are interested in widening their customer base so as to justify being subsidized in the future (and more profits that reduce the needed subsidies are also welcome). Common marketing and co-operations to provide better services to customers will contribute to these goals. &lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Take as an example some price-winning French novel that has recently been turned into a film. When the fil is shown in the cinema, the book may also be displayed prominently in the library and in the book shop. The literature café may organize a reading session with the author, and Folkeskolen may advertise its French classes to be able to read the book in the original. Even other retailers may jump on the band wagon and have French wine, cheese, bread etc. on offer.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The best way to ensure the support of this group of stakeholders is to inform them on the basis of a kind of round table or advisory board which also includes members of the general public.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Local book shops, newspaper retailers, DVD lenders, and culture-related institutions like the local cinema, the literature café or the Folkeskolen have the same target group as the&lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;> Tårbæk Commune Library&lt;/A>, so there will be a large potential of synergies, making for a medium sized exposure. Their power and urgency are low, however.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="General Public" elementKind="" timeCreated="2011-11-16T18:00:45.109+0100" lastModified="2017-01-17T19:34:07.853+0100" uniqueID="c0775ecb-76c3-4ff1-9f47-8d058889455a" workPackage="">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.852+0100" uniqueID="efcc9be0-c3ed-4151-a0f6-8794f0e4dcc2" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
            </text>
          </comments>
        </commentlist>
        <creator name="" timeCreated="2011-11-16T18:01:26.962+0100" uniqueID="5b147d08-dc49-412e-82d4-889d754ce929">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T18:01:26.965+0100" uniqueID="31648ef8-a287-4c16-85e7-62c9ab3fef7e">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The general public is interested in wise spending of its tax money, and may benefit from the plan as drafted by the client.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Other than by the political representation and the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A>, no further engagement with the general public is planned.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>The general public has little exposure to the project, and little urgency. It may potentially influence the project since the client is a democratically elected body.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
      <contents xsi:type="stakeholder:Stakeholder" name="Cooperating Libraries" elementKind="" timeCreated="2011-11-16T18:01:49.436+0100" lastModified="2017-01-17T19:34:07.867+0100" uniqueID="6c14faf4-3e74-4c37-8c9a-8e055d0bea02" workPackage="">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.867+0100" uniqueID="5ff3a4f1-9b6f-4f8b-b881-5d6ccf73fc7b" severity="Mistake">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
            </text>
          </comments>
        </commentlist>
        <creator name="" timeCreated="2011-11-16T18:02:06.679+0100" uniqueID="e496d945-e45c-43bc-9932-e8e16aa95fda">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T18:02:06.682+0100" uniqueID="3892ebdd-e968-445a-84ad-d24b5a9f74d6">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <stake>
          <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Cooperating libraries want to continue the &lt;A href=&quot;7824c81f-9a75-448f-a574-5f06d8316941&quot;>media&lt;/A> and catalog data exchange programs as before. &lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>&lt;/SPAN>"/>
        </stake>
        <engagement>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>None planned&lt;/SPAN>&lt;SPAN style=&quot;mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri; mso-bidi-font-family: Calibri&quot; lang=EN-US>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>"/>
        </engagement>
        <longDescription>
          <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>The only relationship to other libraries is via the nationwide book and catalog exchange which is defined by cooperation contracts that will not be touched by the new system. Cooperating libraries have little exposure to the &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> project, little power in favor or against it, and also low urgency.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
        </longDescription>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Personas" timeCreated="2011-11-16T16:23:01.940+0100" lastModified="2017-01-17T19:34:08.056+0100" uniqueID="22b00f21-48ad-4ad0-966a-bd863f9281fd">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:07.937+0100" uniqueID="178fb150-93b6-42a1-b264-cffe86c49c74" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="persona:Persona" label="" name="Anders Nielsen" elementKind="" description="" timeCreated="2011-11-16T16:24:08.102+0100" lastModified="2017-01-17T19:34:07.972+0100" uniqueID="5c73d5a0-cbe9-4c30-88d6-9f99eb0150f9" workPackage="" age="51" job="Librarian" pictureData="89504E470D0A1A0A0000000D494844520000009A000000E8080200000098776A8A00007F7149444154789CB49DBF4B1CFBF7FFFD13DEC5A748718B1B481121458414594893851459B0C8428A2C58C862218B852C1632D8C86021C3163258C860218C853016C2580863238C85B016814D11D814B7D8E21616B7B07817F93ECF39AF9F33B326F7F3E11B0ECBB86E56DDC73CCFAFD779CD2C6427A167C7617AC47618260761320AE3FD20DE63DB0D22D8CE30DA318FF40C9E8FF74258C28F64BBA17E315908DB0EC2AD2010DB1C0EB505FAD13C196CA8477912FF31DAC5AF11A787497E9C16E7C5F8AA9CDC8EA77793D9D7E9ECDBF4E1FB0CF6F8A3620FF4F817ECE1E78CEC118F7FC31E618F787CF837F68F63EEF37F6B931FF19735F5CCDFF4731FF52F2007B517F32F8C3FE1DBEC41ECAB3C4EC9F0077EA36FD10BBEF3DFF557CD9C3F7CA186336AC6B91BB8848CC51A67A2CD10755FFC04CEC0C119189CC2720BFF17EF16A507497A94E6A75979514CAE4B62794F381F18A7CF4FFF6DCC924C3E41CDF2E7FF826523D1BF3D9C8FB3EAA76C10BAF6B3F632F5DB7E3744F98FFA3A0F6713D11ACEC81A700A518533109CD11EB3548486469DF1CE6FE3DC169616675023EAB20C344B48333B4EF3136239BE26692A964697E60FAB49C47E8E7596FFFC1ECBBA55A5F9A85458D38DD562C588C1833A051D960AA70352CCE2AC13FDE1E27C3038C35FE01496BB43976523CEC4FA5B100D5D75865A9D151FCB070AB372B35BE4668525B9D993AC38CBC9CDDE28374B7FA7389FDAE92F0AF869BEAC809C47681E5DFBB29F0D38678CB389653354D7C1FE70207D73707E7344E9DA77C7E5FE6890A6A833FA17384DE04448DB76FC6D1DE75E0DA7F8DB4D2BD0C6473910374B2C0F928CDC6C2ED29C3AD254BA64608FEC4B15541DB11E5D07EBE1FCA9EC916D9EFE1AF15702A72BB8A70DBFA7ABA70690C6A6558A2E4B83F387AF4EFED682E7697F0B27DBF65070CE55A72F505F9D4125640E1D672B4A2569EE429ACCF298A4C938599AF73A64FEE58BEF697BF0713ED6EC9F9F0D7A9DEF691F8DA7D52C7FFE92A889F1DFAB389583FD3AAD2AB202D2F5B71ED187DFC41936E0DC3666B221CD72FF17FEB68ED3B740A2662851F320269C889A48682FC9D33AD2F47D693DDBD4F6E849ED6733CB46A2BF19381B139CB9441F6A2CA786251BA37D02E70F07672D82FE0267DCA8CE6D04B6FF1F380393D086DB9CD05271C251F3D2785AFE537FD4A4391F6795E513381B1DEF2F9CED6FE3FCD1E8662B2CAD461B92A0792C1DA20D38B956893C75EEDBB45640868A6853ECDC6F70B6B5F0F90B9CD136DE472541E5392741529FC899FBDD46CDE6C2A3B126F91D9C56A3BF8AA922FD5943D1D9F84137C5CB39205566DB9804CD61A97FD042134B5D771E72EC1C39850AAB93592AAEBA93E0C5CEB9D9D0AF9DAD2D37937D9C52DAD3124E93D34E19E74CE16C4C47FF4F38E7E4BD8D3D8479D56485652DF171E3E5C3BD43F45B2D88D6A5E9A6CDFE8F6BC2A98932CED0AB3BC152E1C487FEDB389D6CE877709A8A53E3CC154E385BA775D0EC6CEB2ED705F9EFD4E95726737A08BF76B63FAA2C2DC57B9FE513389FD66503CE636B0ECEC0748594B3DD51D2649CA6CF17FE56AD22C9EDD32C39B90D7533881A7B143BD9D97295623B41F5F0F9778DE5DF3569FE0B81D6945A77B67FCF57A7493BEB2CEF7D6B089FF3DDAC9BD3CE75B6C73E4E26EAE2B4A990899D5B1C3E9D5A654E6F88716E3F8973A3D6DE432AA40B95827B7B2AB3BDF38ACEDF2D547E93E83F4F26B7731C4043D159CF7ABECFD1E5DDA491A865FF04CEA66C6881F819A28DC9AD8733881C676B041AD704FABFC96C372A9D776EEF197F7B59D86CC8E0FCCBE96B4B62A23B354F127D54F07E1FA7CFF2D1CB869C00F6E3B75902A4D83C99363783664FB0AC395B83D364B692DC3AEA74599AF059C13937B375D5B9F194CBD5D950ACFCEDA96AF24D6FAAFE5652DC46B380ABE58A5380FEF3AF59FEA2F95EA92C9B58928F81DDD26395A8853A6B68BEFF989B1309E35AA1F20B7536E364A2A1211A571AF1BB157556B5D888534A4FE9F31151D6E8E4CAE9F37DD37FDBBCD4803E6B47A9F370FE536369729F277266E3692B445D9CDF9A42A662399EDDF0A3866A1FEF1BBCEE2F7A43BA16FA0D9C8E3AC31A4E53AE1881C655750655756E3D990AE91A54563A89286B5479DD334DF4AEB640F6D71CAEE48AE713FDA726CDC6CAA79ED0FEEDAD623E5D96B8358966E91813B566353A6B5E54A94075BE7C0267641B430AA7AE5204E46693BFDDADE3745221BF0B3FD7EB6E587FCB25500CA21C4793FC249532145E5725BAAE4CBF3784168EAF5AA30FBEF2AA329D8FB39E5ECD9EC439AFBE04CE5B76B3A4CB31FD1582D31811AD795DE3789F10EB1338DDF0E9385B8B33109C9BB5FC7637B45005A7BFE439371BDAA81DB394D592E73E6B5488CA22B626AA9223BD64A69623AC23320309BA6FFEE043ADA8F377406A96EE72CACF8AB39D5362DAA8C944E94FB8F6896A8D36E54733A3D7E6C5966FBFE16C9D54C8C169835C530475ACA1C9676B9539458B3172B9216B344A186A7A10EB4A948852AEAB652A4A9DB11FD69DA3070A3F7AED851E0D0C6FBD6C4ECAD3C4527CAC6D06D58B4E479D155D6A4F6B718AB39D1A97EB13B5326DEC36D4D65E1E7E0767ECE2E4C0A982DFC620D00275D63E9B704A57A89E0DCD2F40DD440938F9FD43B054ABD9A79416A5A388EAD1AB12599240C5DF09C6883D22D0F171264BC13F7FCC20027C5278FE67BD35F8CBF4F577DC6CCDD956E2E583ABCB9B71D57C7FFBE010F5A0BA1D417F5CE141E38C9962ACCDC6CEC4E9C25B9C4A9A03C22944B7EA44839A3AEB7DBEA7A04A880DB7ADA3A6203A8AF8310ED606E97ED4FFD0911A26D9092737252CDE1C0ABFF16916AF0D069FBAF8EEF824430E35A3CF77A2AA97871ACE4A59D9D85DAAB9D9866650BD38A938582D476BD7CD445D9C6C7CDC58C9F838234DD457E761AD50D9F1A5A9700E2A0255B37D9AA88E9D61E44E0CD9F0F994BF55BADC0E928318FF117284C1EBF63F75F164EBD56208F0EB8360A51FE117D81C26FB61B4D2EFBD6FF7BF743B1FDAF8DD862BBDFE4A0FEF19E30D57FB78710E773D8A675F273F4D53D039787CBABBD434C5E2750F1C9CF2A1BB2CA7B7DAAF3A2CA77567EBE444D6EE7D96C2EFBBEA1C49D280E7599DA7B14754F2A023D546D038D55A8A274D3215414DF8ACB8DCA8297CFE2282EACC16FE192915FEFB70AD1F6E0C7A9FBB11322CE8F20B115A7CF1BCFD66A9FFB9DB7DDFEA7E68F7BE74DBAF17975E2DB6DE2C753F768073B83E68BF6B853B616FB9D3FBD4C163F7732FD90D1FEEE9C39A5E14D3EBF2E7DF0F8FDFA63324565705B5F57F27F171672D1B5B7A7E12243855E25693E65C9CB71397E54315E7F4A1F2E3F827B23A4FE37969AD8B93F220ED69871B037C5830DFDFAA0596C875B63B73B2A1268192764D9F6F93142F4932BE1CACF6C00644891FE02D77965E2F7620447CF9A98B63A06DBD5A1A6CF441AEFD7AA9F3AE05A2F82F38808EFBAB7DFC97789B7FBD95FE90BF1C7CE9E1E4282FF2F105E55632B0A24BD56AB233D7CDFEA836D99B0A4D4DF4C60AD424414FA8D352ACB700BF2B756A4F4B8C5D67ABA0A6359CB43AB6A3716E11420FE7C6A092DF6AAF2B206DF80CB71BCB955A1C755221E0C43B439DDD8FED16E3EC2E77DA6F5BD0E2D2EBA5D6DB25F85588B583EFBE5EEC7EE9822528B6DEB63A10E872A785D7BC5E6ABF6F0D567AF88F5D926C1FF807AB7DBC60B0D6CFE0BDB783F2348364F101513DA3BC6E35D931381BE69EE7F70D8CB3AD48735683AA836B03CE59BD62D13ED6991933384FBDD899BACE76E4E3DC569E565892094EA70075706AA2866B4DA0A1E36FF102792B2886922004C23DF55F86AB3D606BBF25CDC19DF2710B600415B9D677ADFE6A8FE4F81E2C97E06FC9EBBE5E02B0C1C6A00BC6EFDB78194E02A879805F5B4B1F3F02CC9025D1C787F2865B65F231FDE4DEFAAF0647AA83B20DD2D4CEB692073567B6359C4D4B2E3A9B35715A13ADC54EBF256471EE5A9C43479D4337BFDD761222A5CE20AEE0148D1A7FBBA566BDA2BD289435B2F501C0404C708674EAE03DD97B034CE73DDCE6730814CE131417FFA463BC1E8A04333C0233F40715F6567B8B2F9F43AFE466577AE08A57E2BF93BB8646973B803ADC1AF63E41CDDD7827288ED2D9FD58E0FDD49D077F94D25F96FAE1772AE64D8DDC37A442558D9A8CB7E66CE7149D7E27C11DEA54A9D089970A35E334CED6099C0ECE41A55CB1CBDA558186261B12A86A8D6563089F49BEF4631B8FBD951E74862CBAF7911C26C8416DE0013638186C0E165F427C8B08997824D57E2067DBDF1874DE52C88416416EF1CF674B2F8928BDE07D1BFF114FE265E28DBB1F3A148971EAACD1D903F74B1D440EA2D26C1A9FE6003377FDB2D62FAD4E73B96D2017AAF43D2AFC1A63E7BCC5ED6F0D258A7C6B81A4791A55711EFF1ECE351D416DC32FA808D4CF6F3D9CAC4E593C09489412DED83122AFE1F9DB81844C7CFA4B6F280A4282C38D3E2428EE14B4286ABE5A0230A0C509F1FC8F67C87825BF7DFEE733189EC733A0887382D87372841F015DE227B2A7A15F1BF50F12E982175667B7636A225E96D263AAB649E7B74C09BF3BD0250D84395C7F6177739220BFE274593AEA3C9D53A8CC73B6EB03783F85F317FED6ED27589CA24B4A65F90DF16E40D5FDD8254814F616F1648FF50A21D293CCAFBBDC7EFEC77F1038710C4830E084918FC549F07609CF0021C831C225523304CACF8B52819971AA241967CC80DC35A18540B3515C9C65936B6A4AF0FAD4B4BE8DE4A93EB8271A9F81E9F355D0CE03ECD69AB5B4B6D20C7271469E3A9B06339BD50996205A2957B6BDFD0E06E71C678BF74465D2EF7F26BF87520486EF222EC21FA2B4A02496B259D22B8C48BC2712E007726D4E8B28BBF9D29530D97AAD1883DCD24B3AC06B06EB7D295AF01F8531BF1282668D7EECC00D20CB85B22527504BB6DB617E90C031CE65F9C4CA865308365823D7069CBF16E843EDA7CC6F23543A7C3B0DEA1CD47136F9DB86D8B9AD71E20DD7FAFD4F5D72A780C171AEB7DA0F7703F2B470BC1F29B521BA5FE08A919D12154971056197A509D2642C3E0AA86F488EE290DBF20CF8BD41D4C4FFA22F45D0488BFA5CDB50D2FB99D222FC2D783E1945C55142ED85AFD3F23CB7FEF69B3FA23ED7E55AA28F75A84E82A3BB774D442B389B27C4FC387DDF940AFD429D5BAA5071710E37DCF0E9FBDB9D4A36A45952F941EE9ADE67956BFF37A83ADAF1614C3D20D49A5B432A2B57C8DF42823061D961EDE243075162FF99A449FF175F42BE6FD9A82CA1984A61F8ADEA247062CCF9D4E7AE7A3D24FE86D2283A63F8F93EFE221436945453BD34E3B6FEEC7E8250EAE09C36E39C374F3BC7EB5663AAC372EA756E95D76D56AA02C91B5EEF26920AD532DBC65468DBAB3B15CEA66C28F470061575EAA93E7EAB4D4A68DBF8403FD147191DC4FDE5CE706B20B504D2CEEE323DDF673F0CAFDBE3BC9715094844AEC72438B96D51560CE4FC64975B4224BB4F5CBD7C680B3C46DE52ADA58FEDFE27CE6F11A43F75E42FEAAF0DA8CFB0D2EB7FEC24FBD1E43CAFFBD8A7F690346954B5E5BECE5C250980A770567AF1F79E580D63F71D1A9C6D7DCB51631B41FEF8067F2BCE5688F2C256A3B3C597C412951F1782D431C7EBF7C23EE544F4F9A2E20442FE717DAA25D6A89340CD9D8FD46127129F3B83952E78509301C7ABDDE15A6FB8D61DAEF077B94FDBD72119AF24D78A9F8553817BBC7412BC5914B977DFB5E84FD81A8AAB40D8C62F4367D8976EBA174D2E8B075E4CAD7AD1CAD6E82782A8D2A8D3C87D32764EB535ADB14CEAE7C1547710E7A8F3B79DAD116873F8F47322A5CE1D92E670BDDFC7470C59C08B2EB703641F0711BC22DE90FA41889D1B3851FA54D1E21CE27469C09C881961EEC7DB485B86F1EE30D91D46F8728B9FC1E3562FDCE8053803BE74C38D7EB43548F1865B837887975CE063F0ABAEF7F924A0DE02C44AA25F6EE3F5F4A7E12FDAA4C7FE4A1FBF5E769864FB71719C2A9C4E23C6CAF4D738FDEAA59EDC5670129B893214C175A80E42EAE36BFB3FB411D6A456F170068EBF0DEB099156278001126B8E625567B9439E7C73D85BA5F4323988C2AD01F146A2BB3580F559917DEEFB082162B3176487613A0AD23D101D243BC3743F48E96000C0E13AE1C4CBE2ED6176108268368AD890B806783E84EEC9B5F421597A6736C2B9DA97CE54FF738F126F9C5E3B549542A3759C4FB19C8FF3893274EA74EA354BB1897E14DE930A489ACDB82A17B4347F0F67C5D9AE8A404D2F7E5815A8CD721D69EE04D0193E47E80CD2044BD412C9518C3C28DC0BF1534891BB01B48B03940DE1661FEAC463BC17405EE928CC8E2210CA0F61113D1EB0C9013F93ED0D53105DEDE199742F288EE3E230E283243F8A8BA3188FD921D195C1E0902932D76EC80245B206970BA29CE8C1EDF7E1751FEE262EA4E6F682FF4C95E5D75AD4F4E3A56D045E3B506FAADD4117E1F40A076265559D697D8CAFA6CE8AB355E1D31F4EB03991C5C98F7BEC48E145499D7DC4C8C1063566E1F106F8BF1BA448F83DB874C0862E79C368007E38B1F2E3383F81DF038CA83889CBD3A8C4E349541C47E529BE4C7050E05B87211E01325AEFE3C96C3F2C4E121C807479965A3BA7477C2B3F4EB2C3381D45F10EF5FA11628528FF9E941649BE86DF363F4C90EB7AD82A8F2EE039D35F55697A4DDD4905D83CA346C755CD2ECB6AECFC05CE66753AED21671ECCCAD4E0DC0D21BB88141F42915C81F468C58AD63A7A035E881ED2DBF610DB689F283CE4710C9604120A3B4DCA33B673B2F1793A3E53CF8CF1E505F03051868A6F25DB433C492CF1CA7345717C918D2FB309590EC397F402703D4BB3A3243D88A3AD619FBBFF700C28930032DC0EC3BD0859187EB7D9D789AD55EABA9CD37B6BF6B18E4DBDA86998D5ADB4202F1BCCE0749653CC0EB2DF51E7AA82AA053AF489F270E57640604E92F42419DF9448209383188A1C8A53E5852DC94A786680929D98E262949F2440884FBFBCC880136C9401CF65CA48E811CFE040E8D277F1C88CF351085AC4F222E3FF98D16BF062FA32E3FF9811E0737E0DD05EE6C5495A9CA62894713E05ABBDF430064EAA9DD6E04242FC8DF0CF0FDFB9F9F764934F5F1EC84B7F9ED065D5CD1A7E57E346158E9B5832CE93DAEA98D93EF63BB1D3E0F4132207AACA8980333B4D93E3241945488570E2C7A308BAECB31CA946E4A60FEA8D7837480F4991057DCAD9E4269F5CE725AB8A85986A8AC9E4428E05520A973BD1BCC10C6E598448F0AE60391FE464FCFCF43A574F8A58717C851F94979739343A842EA151FC92EFA9ECA1D8B11BA288CA0FE24799F79C9BFBFC9E281BD31F1D14A786A543CB501C5F28D35F1613FE725EEC0CABCE76B79AD936E25402D5D706324433C4A793343B4DC8CD6E0C68CEE380DC175E438B941FB915F06689A2E67A2F3D22BF5A801C7DBED9F4AE9CDE150295A930C22B91A642ABA09E27F649A29BD0B7AE3288955FAF595EE553F556162D3D79534C6FE0CDF0B3CAE222074B72B02B3D0451AE6B915107F0B7F17EF470AF47799FC45983E70FD39AFAD20F965A9196E5583F5ABB283C3B67BB28161AD2DA469C7660BA19A7D7F07335CA5E97085DE6E971ACB6FA02EA5A5F4608A8ADF3A1D5A28A7E09192F121F8E8E690924D7F9F4B698DD974CB4C431184CAFB519B424CD44E164996AC699C279CE6A3E4B154B21770D63AE902941A527677763F959200A574F38D729E9C51989B82E9754C9E91264A9F5B7DF9C24B6D278D314BDA4D42B3CF43CD89C1CC74728144BCBAF66F370AA21DBB8A1EE1C36E07C82282F9B6467697A403E36398E6952729D7A05D49643BC94C6FA073A08B707487C58971969F126C7276B71927A5C812A8A158D32E04CC9D1E0D4A29CCAE3B5C15958C9C26EF1E3C6B0295BCCDE5556D7BBF0287B21FEB4E2340375CE86AC5907EB1721CE0442E9E0349ED6C3D918232DC80ABCB3BCD43626F371A64DB133AEAC77CE53A72940B5CB1DBA63EC7B617199E550E75144BD9E8D013C6D8FFBAEB4F8C56DD8FE17EAC9A1BAA70A043C18E4F45E9BE0BC03CE6272AD715E1A21A6FA19A1984E956BCD1C1F9B5990224D79B48C0BE65DCC4094D2D7C9F4EBB838CFF027E3AF838345723EF8D28BA84E8D9C4BAD4D6DD6532D28C70D03255A91BF6679A59D6A4D82E5996679CA26441D754682D35629FF4775BA1ADD1822032A2E329EAFA4B5E26814053C994739ED3B19C5535D85F4202C90CEDCB08FFD3A86898F2590B7244DF5A803A7F2B7269A5E8907E6836B1520F5818D9AEA791341E16CD9547E442E97897E9FCEBE4D20D0213719A801B23944AE8BB49C660CBE6A51CED1A5A5E550743CADE3844D1172E515212ECED2D162A9591627196D7BD550C1D8DBA3E2E0ACAAD3DD6C34AC172AABA6A5E0E11CB240F3F354D25AE441F0B1D252872E0700B9D2E5A50C2A3AC32DF2B4253ED03B666974795B28034871B62615BA48A78DBE97D31C8D531115A8E6D82AF24AC2672641549E279CDF26C0892C37DE8B5031032492A3EC3845F8C047FFF8BD5E8D38CD71C3F2D655A4324FB2A62CA9822C8D16D91C396A139C96E8E9BFC239479D7DC7D97A38756301791092209CD4D1362ACE083E9614C9AB1C6AB963B50BB4288AA8CABCCA01D24AF3CE82D462D2C08CA7F56DAA346AD2D7CC952339582E518C9B15752A99E2994B7A9E048A20FA7532BE2AD2E30446FE767B88FC3C3D491162A1DDA6854C07674D970D2C6F6A19901F2C2B726CC0795AC559AB52E6A5425E66DB7F4A9D6B0ECE2D1ABE452A447D96FD907B40ED609BDBB69B724E74D92DF7B3E318C52549F3DB58D439BBD3206F540A4A092AA5AF9CC1E2C0E85249162C2570A6D6EB2ACBADF8D481FAD2D6A0D0FA253F4F2E379F715A848408792C28A25CA6FCFC3C8B47316F3E9CE855CC4ABBC0044E4BD1F3B43ECB8A8FA5CBD55D1696A52177AAC8CDB392A1FE5F9CEDC0ED0AD197F2E43A99A7D1CD215F7E1C15673FDC0BA5E844A484E3E5A52E3A3992C3283F4BC71087C229BA7444C9C6CD1D6AFA103CE14AE46C423475046ACE80A909AB979AEEA5658C3498D4495ACF1469F1C6379C16DD97E5450E8AF959962309B8CC0B2A69D8D97EAF4D1A5412A25BFFC055675365620ACA52EBB281DCB17E5496AA03FEEE425597A661FB9B38F1E878DA1A4E2A5492C3383B4F911BC3AFA62709E225AD936C0DF0B6486891EBC2032357024EE4B41C38F121162A030212A4B2D79975B317BAD0AC3BDB8BBACBD52156F44A0796ABCB92BE3C4BD5F3FCA5220A9CE759719695D745769C8C6F4B3CF22ED2C9A35C89BE81686D85F9AE56991869D670965A9796DF5C4B8B2336FDCC42B5BDD788535FE422A8E234EB2A1EC881970DD14435DE21D8A281AEFC3C431284E3601B2FA0A809A5E27CCF4F1372B6E7C9F40E5E0E0665883429979148290D5B85D3740CAE1C90FABBD30B55862A66BAAB60D25745511F6BA8F265AA711251E04438CF8F12D228F7FF90D301866AC49B84A8BA0A36F1CCCD8F6E7D37AB718EAF4A71B3BF89333F4A8D19A80BE92FD4A93677868D5DA13547A38D6E7643B5FD1035832D9A2BA026DF0A4D81E00D396AF6115629F5950E2D3EC41BB66B6BA2B0B1E915589CC9C4C45183F3DC429527A7F6BFA41A18DB39DB85AE4F2E489D53F53E89A89922E85D39B9E5B6DF79066982684E384BB7B7F7D00855AE96C38F866815E775B5C4B46ED6C55985AA411E6AD344173C90EED5F8FCAB9F4A664B93253C9C2E12B40EB691A54394764D730F81A0EE0C21D010EADCA4556BE417906CC6EB5F53F858EE124CC5AE392E2A66CABBEA5591D4E32AA1F4D205A97576A935772EC2555F8A1CA782F68CBF7BAEDCB23CCEAEA506952EE3787C5D8C6F60447422AB9EE6A24D2E4E4FA63A63327AF5574E0427E9F2A2A8A6B28D898FE064721ECE430578419A0692FED8EB9E1E34B5842ACE761E4EB321C93CA2FA9461CCDD2185E1ED01BFDB002CE38310D24419535EE5920771CEC21421116219F382895B505A97E837F952EB3F5D1DCBB146A5DD6FA691BB59AE6E2988B3BDCCC8E77F958A654CEB0137457955E0E443BA6BA7E3F5418346DD394AA78B6B882A9C9745250332A9AC691128B1BA9EF6B0C11612E1E79AD9A5EBB3345BAF9B71AEF9EADC704DEDC3A52DDCBB403B88F0CEFB019F437181CFF1DB647C4B092DB7F1A42D40EE8E585A6926AA2031156725153AF77CACF9AE55E785CD924C27882B1329406DA234D599D4EC4617C1DFE03327F0B7889D0565E045F5463CDF9D4DED73AE216DE268A5B7E7785A2BCDE62AF3640E4E1D4417D485F7C802E560E52A99555D0ED8D3D6583A1567AD25649BB74294FA7C5B7DBC737A18A552EF9E25D96982B37E72574EBF8EA90D840C88A5393E0DC7673107C8180763E35475B26ACACDA931D7096BEACC29B36B67DA037B76A50FAE738BF39A333276B60FDF260F3FA6F90979115E85CD9B71D61BB90D25E964E6844FC7D3E616A7E91BD4BB7AB53CC8C651E08CE57AD2FB9E09488FE56685254F18AF2A63A84D2ED7E0E485CF7814F5573A83B52EDE1F85A674300A48E77E3C9126FB3DB7F4103E4997B1809C80E569343E8D155D950159092ACFEC66B69A13285A75B282A792F1BA056885A8F916D59DC452040A9C1065B21F42A0E3AB2675EA3B6835A545569AB38AA73519D0793345D5EA936E5FDDE51E656C5A9D115F495A5D4F9A6C68AF08455D3D2D4A7F6BCA60D5B2F488AE6BA29AA5B46D79A7D830D98F7A9F7876E48006C0E06971EAA88A131E0F9FDA376A064D24641A9C3416A429E298A0AA0C481DDB039169363509EDB98E97720668873C755ECCCF70A162BEAB1BF7ACCE52883E7C23A2E1E6A0BC46244BEBB1B341A0754F5B67C9BAAC37D9C71AA4BB9052D168DD16448515532CB7AB2C87355DF6571CA24D38ED752B76433CCAF433E1DC6337B0CF3341843323697E2581F20A25E33C6596173A6515844C57A9F64C319E98834AAD72A6FB06FC7FA7B6623149AC53C6D8FC565ABEAA5071051AEF06E5259294F4C1EC1574713EE56CE748D3AC999C5744D98CB3988F13325D50E48C51CA3350A224967DA6D21FACB3B9BA0448BA6A8F25EAE1745654CC0C18F5F9563AA852F086D4F61B85940DC1E51EC763EA01C9E2492E951FE194F4E75ADAE8D249109688AC497912494C9559AF31B3179B32FBE9A5537B086FBCE68CD88F4F62DB8860966A608CDE9685AE5B7DB21840EA04CEEF940D6548DF4E922AC8C6D8F9AD8AD3169D576AC647D3127E143EF5B19A17F1893E8593D419187216E140CD77F1B602DB6D37A2E46B2FF5BE10CB9EE05C991B41DD01309A90DBA20DD584739F5826234A8BB2A388C60F68F18B6A4D1DE762AA589019717B48A0CAA01E8DD79ED2842D1E09271E4FB451A0653B8E04AAA27B262CF19A08DF2A0E433A1BF8BFE3DD8AC3A838C09334239F1F84C5114D0D4ACB575602144EC482EF53E0C487EE0C07D5103625B7959CD68C6C79123462BDA89959F53C7DAA55B430645F6A2EFB245AAC82248A3D329E3BA5FD568EF599AB1668DFC5692FF8A4A76D914A0C68E7420F3E20DA190067B433CCF1415F7386722325A33465D8EEF4AAF56D816FD1477F4CD3D2C551581C87251DF3602D8D4A87E561084278728C6F1D06D38B44DCF594BC3183C4197018E67B8374AB0F4B367B091E916C6FF4A2F55EB23DA0FD11DB034025C02731D52A70B6EC6F09E7D7F1E35F33E044A5E8CCB93BF0E6942826AD159C9306753621AC4C762975CEC549CE5679D175C56F689CAA5DCEEC891C15C8CF35FB625DAE2D5AEADBCA76E85A6CE928EAF16E2FDA6DB2DD0F367AF1EE702CCDBCEB943D6A424D3E2E3C68810CDFBA95D5319A8AA63D0BA320DB1F92D1CE8501C158EFC5EB5DB0C9760630D0024B18A7C4ECB1C9BB12E3E220C8F11F77FAC946375AEB845FDA64AB9D60A513E060AD9B32CB1C388F69AC9E347AC3FEF6AE542EF7C714FE967665BBC1721E3FD3E1F32B4ECBF2E2F759AA32C6CB6C19A18FB3322342D6EB1B5BF1B5F89937CFF24665328D9389F63CA2EB7DCFD96E07C928A62BD3EE51ABAFBF42DDDA80AC078102E744BAEDD2B095752E72BC948CC8182DFC2A5498C139EF0CE2CD1E18846BC460F8A985C7E0338149D6BB4005CB77FBF9FE80CA9B339D4F31CEF26098EDD20BB2ED1E88C6EB1D5884B75AA1C778A39B6CF7B3DD217E0ACE1BD93D21FE96070AC752804E6137B56984FB396D776769457BDAF25FE07422EB2FD35A72B64A7915FB628DF93914F9A269C6E419ED727B1597AB71F24E15BE12787A9480626FB98DCC96D658B6FA089C13C6A67BEEBA0F7E99F0F3348727DE35234FD8870500B0DA192CB7FACBADC1A7D6E0E3D270B915AD10CE74AB0703AD7CAF5F1E05E39370223930581E05C5FE20279CF4826C0B5AEC66303CB33BC847C3FC2040042596143E6967047042A3321131A3A1089A7A799CCDC617D9632DCDF116C2EEFC4B82BBAD833ACB0AD1F31A45671AA1DABFADE0ECB9E49404BB7DC3CF18935314971D139C9FBD20AA5A450ECE505D5D8FEE1287BA939A09AB70B603D42A486B27B77A218C22A8EAFE507D724D7D0088323F24070BD74ABA84635C69B3225BB06815143BC96627DDEA66C68073A7571C0CC79CFD1A75E21960C677D3CD6EBAD149D63AF19756BCD24E37BAECA587C568884409BF009F4024503C9634759DDB20FA639A1DC60DD37B956183FA044263C8F4E1B9296E4393A83E9370EC7DB9A04360B7915F4F4BB0DBC0B2E3107582A8295AD6F426FB2D75194DB95C34EDD70412AE82A04EAA386FB955AB96C378FAF92A958D44F9312259906EF7C9C7AE75E112A3F52E454AA8100ADBED91D4C00608E5785B8CA016A341798CE42892C21444113BF14AE00FE1A23FB6A2CF24E8040E7F83A3EF7A4FED2DDC518E9A7E01DA6118C9403D79DD9B02F96D795D78CBD44D57F46A18EE920EADAF3F85AA42AED1A8919BD92993D30A4E08375F680659C7B95C074917FC90673C97EBF85B95DC6E0DCD6D17B2A324DE19A2FA44D41CAE75437C70D75C8A50DDC9432492DC5E67E5195523B4597317996737E6E425DE90E8088FDA85AA6204BCD56E4C51B34374415D9C2DBF80C3A72D46715C8C02F08B3EB7C2E556FCB91D2364AE74FA1F5BA642EB7D440CEE140761BAD6017EB80A720FB4733480CFA7D1DCDB1CF9EDECC714292EB3ACEACF2E6456EC4A2F9ECC1FB41460E519DB69B3CD572795A4EC6C3F2BF3C2A4EF637BCB648E3A3BDAF8D80DA22B1E4EE56FE9D27A61A2AEFE1DF53FB741543A501319D4134FCB465B8B4497C85C46485E06D9DE2043D8E3C897ACB4A32F9D7487335B9416B04D24B70409CE13090E45506434DB7D72B65C6B4A0F41704670F21F5B70B6D1A7760875EE0D8375DA8582DF27DC19C6BB417E92849F719674D2D5F6F838C0FFA2987A184DAF65CA9782E8C36C3A8540BF4DE4726CAAD17353DB765919EBAA0FE79D29FFD9203B879FA7CB8A340DCE5356A7CD7A2CD166992AAB685404FA490751DFDF7A9B3EB7D56D1CA1CEDE72ABF7A98D54283B4926327D29AD1FC963AFF0CBC54098EE0D4034E7EA022C91C844CBAD74770860C1A7F660B93DF8D8EEBD5F0295947D269C27E915503721CD21B2276A2C9034536E2F24284963AE46A24F1D308B5650DEF4E3AD215F126110AC0FE27564C554A8C0C172D9D3278D72D98A02574D54DF150FDF2778E7D969A8772B943ECE824DCBD10EE7E5FE12982BB59487B8B49DA48A5945970D45676A719E1A9C9FFFB7382B3951257C7A7B78E516C8D44900CEFE974E825CE3925BB5E466399BBDA6ED0C630A9C31129F14F5C601A8401C011E932F2D0975C1A716F57EF99A14780CD67A888509C754E4B788AFD02849EA28542B6BD2D445F83C8AA8B24486BCD1275B474E84547990EC223CC30D04F95E581E2763DAF4995266BBD32FF6FBE508556C486D7A5EED8151F83C8E67474376B3849358DE947ABA8071BA20AB2C6B490D53CCD91A713A2C9D9709CBE346757E69C2F9A906D5C5F9B1D38493883ACED6ECF50CE2114D794191283010AB52C179A7074AB893270D774A6110B4E0E8503FECF75314882B087810225599B4419A2EA14717A5A11B406C22E31DF4DF2DE105D02558227C521BEF38D2385348935ABE2731D21C6A096D5257882AD4BDA174F5A86F70429B47CD720A379862498278D134995D67D4F0E3356DCA8CCEE2A9A1C897F1D361B27044993B20351B25477ED4BACC2B9CE43E5DBE28F3636F8D534D65BA99ADD2255FB3594A140FEAA706F3C2E7C78E97E27ED195AB53AB0CF5C6239CD432039DEE87486B938390A7AA72335140F9E4792CEBD2D4C6DB2371649B8864142FC1324561037DEF0C69773474B9DE0F21B21DD43094FD5294DDA56C088E1A6A96A6AE5A8A91A512AA582882224F864C29CAAEB6F3ED5EB93F2C21E56372CE136E24515F0905B1ECF4E60EF0E43492F532907E804721BAB95A1BF1539EF1950679A1413669D1988554539E9D1832BC8FEC8B2B43B6DC466090DABABA6FD0A451A3CE4F8E3A3F3A1AFDD4757B7EAE40419416B3F6FBE3433EB8295077429D63C1A9C647F05152CD47692D1715DC5CED25C85789D3803AEFBCFB5A36C1D3EA073F224F81C852AE37124A6B2909A2EE3C37D97950C82E9E8CA5777FA42E8A016966BB433A0320689C04A30069EDF82419EB4922D09D492905BB488928AD9A15D3F364A6B6F756D257ADCB732547473D3E3F9790A6989F38FEF3A4E10C30AFAF3A5BA54E133835C8B92ED7295DFCE4560B54E374F25B099FB4883D85F2A0839D1E39D2AB8CDA7BF8E0CC72D84532E51E02CD94107292295E59CAEAE609F574CA93509583670A15B5D4417DA72FD92C2AD4E238E44B5DB0A7652DBAAB60B2A2A9D65E8ED9979EB01D516B9E1AFAF0BAFB01A0225E8E0F036ADFCB900AA0929B6596B78CF32C99DE42A9E5D4CB668BB18FD350CC8F7D7E4789A278D41C32ABBB187C415B9627739D6DCDD3FEA6B375D5F9A93B172728EEF4CBBD01DC1A7C29DC66B4332869CD0BA94744830754EF7387088C4FB930B822A7579C1018AA4151AEC0A3226BA5A60F0E7A94D0AE1990B4BA52CA4569F0480D04993092E56B667996CA1826AD6E9E2484131EE2847A40A44862CC5DA40B35E7478EE438541D2BC85188D26077313D8D2909F2703A2C2F64395A9008B6243F643B4A327EB4023DD2AE552A4EDB52F0322015682B08DDD285325BAD4B03B2FFB4B3F5D4D9ED30CE4ECDDFF6247CAEEAF0B9C1C169B75FEE07840D2ABC4C83B56EAE5628690D920AC44B1A42280F903116E3A3401634C694A0AA09047500077BC40B9387A15C7986AF2B142B9C2C5C72B66789D9B4AB6747C092A6A5E54B5EB2D6623D25C0F4288BDBB20D867771E317989C84049252A44CA9937E3766E9142736FD5151933F65C58C286A906C15459EE9C69ED7E1CB6A1EDB65591D2C5A6876B04FAAB3B7ECC5CE8E1B4175C3AFEF085436ABA0421F23A41DF0C94E3B67D378775808246EC5C1A3CAF1F426830BA58CF7923A73329F30E5104B8C65D55A0D0FE8C56A31992A923D65BCD666F760CB782D6543749112B5D9082FA6B18454F46A864BE858E6AA4F13332146692D37E20527A3B5A9ECA45A9C689CEC6C459AAE280D45D506127ED0B439157CA2AE07AE4FE196FA54686AF23DED693FF94D3EC6D93102C577FDFEADE4B70316E8E4905247596FA260795F96B48A191348EA9547920AD1320812E00BC2C3A308D9EC069F2CC742BD858112A2733B37A4280A54921A454491B2198FD6C92DF0147481A873BAF805159427700C6A8C687CA62789CED5D89F4C8ED1443C52D95B099C8CF3B674D459548956708AB315459EA894D5523CCFA930BDF4CB9B8B7A17DE6BFE59A27575FE2B69F6FCD8E9E0EC38AB6615810E01758AC8049CB7243B1421B3FB0291B2A4A1CB4816B0280302D48B78CA6B296ABB350FC21B8DF2C62E6743999E9E25D58E86C5612C4D0065D099D9494F692D79D4C905EFBB2672D9F8342D760794F5884CCFF55C27CFF6CDAEF442BA48137990A4B5B7F9C39D5CBAC22B3795CBBD7252A1532F15C23183D414655D8C11DAFF6BE7E2EBBE372F2A0D3F79B7D327707EF21F1B3CAD6EEF39EAB40528BDAC22503566AD26AFA09BD130FBDC0692D9D771C9B19332178892C32AD959C45D85949EB99281043DB6F3951F6544412E47031522A1DDED538D71AEAFEE759E8DF5D5BDD44E7AC9864E93C9692A17B98092104A8BBD61B1DD433A3D51FDFA9459EA4DA2570CF58A88E2C4E21E42219B799DF52F9D13B96D840B07E789534A9EF13DB2CCEAA6BE1169ED12343A9FAAE1AC44502D56DE7D3D57947358DAAED0C7AAB3ED689CD50254132D8EB84847B6B23FCCD63A0FDFC7B3EFE3925972FEC299ED19FB5BAE3E45A6706E5369C7DCD2474966766573C289540509738E8C7714C8F5BE648398B9FE0C395B35C08717C71C11792F036F392AF687D4CCDBE9E124C3A936A5D9225A42174F2B02558FECF6C9A950282DA677E359654553B95FEB6F3D9C90263B58B9E5D9E4B2B09D7A9E52987ABD88B22AD033EDBD556193A9BE92D3B25FF8B5776D6ED876AB2C05A78DA0B25ED6772368B41752D27118153BFD7CAB4F8371DFC60570B2B19B75E631CF6219069BB28F55FB0BEE2419D1D72F615DE2E440E982A2A5D845FE1C03186A4A6567A96449134E9D1026F1D3C18CF0F0F034C851A1B937CCB77AACD100671BD5943CFF272067578CF32C26977B9DD2E9855FE0AA708648C6D33B2553A799E0B4844C03D6B0D420A7D7656571D412BD2CBC087A9ADBB2C76BF0D67136B667E796280A67A76ECB0E4E12A89EC2E50E51B035A40D788751B9DD47C1476EF36B599C689C322A7DC68F5729F95EB52D9007886E8D28F5D64F242CC00369EE0F0083EAD1AD41390AC9E075E92018A3ACA4CA920B4AD8E190AA8EA388B2D6634E8268D52C04510814FA1EEF0FE8F953D6286F7A5138AF092707511A488343E67DF6DE5890D6A8F5B7A55BAEE8DCC73AD8AB1A4B7FA1ADBA0E73EA2456F57E3D13ADE17C5A9A9FE67BDA0F0E4E7AA59E0AB3437EE46FA9B58D7A71B38F9A92F74D1650A71015757236C4DB1918A7D2A8BA6A9B9ED694DD9CA73C0174006D0D7259D3DEEC21AFA106EC888CA2E9CE00BE148697511BF62018736F96C63337BAF82EC09738BD4054040A977B0097EB0A3499B146F19B4CA50B8FB07DCE039BD5592FE5782539F2B5A5D21F2D4DE7C6DD6612C50A54874FE36C9D1D9FD5D6E05105E7AF28DA64F5935AA976FB417581D63A4476A8BABF3AC0A94D3837286D91E0472B27C0C9392D042A1AE53D4694E28AFB9D5EEA3D81D211A4F93C4A86499A0701CDEDED0D08EA6E3FA3D5EC5ECA4340D1E776F4A995AC50939DB8D26B0833644A3869D90B07DCE1C3AF34A2DE5EBED5451826977B1A4D991F9D3DF805589AD44C6057F1C0837DBCB35A36E4FA443D7979D9A92B4D17A73B6734B9197BA990DB21721B84A6847504FA6B9C3D0F67738942D2FC60FD6DA72921927D10B39B02CE96CABBE358A66ACBF318C92D4550E044AD298FC701A155933EF4A4DE4746A3CF931335188DCF9D3CED1EAD6C334E5ACBCCF786D455DFE8D10AE85A9756B35761ED6CBD9B43BEFBDC67A7FE3B5979008B09E73EF56949CADB2CD0E350E4C813F494A0E1B7A5516CF613C86FCDB6EAEADE4D87A85B448E2FFC0CE8DA0F9C95CB26B8A7C29959E82696D25ACA0E4D5F4237257829FBB7712E5716AE7D757EF0FC6D470BB4EBFB5B185D6AE094D722681191EA109AF9207F1B92BFBD6075D2FAD45070960703D5303AE5052C5264A0EC6058F2884201BFBA475009A4D82ECD4FA73C3A9440AC1BB460425F82E81E4D291407A1E034DB19449D2450C4F5FD017ED0E428984A38BFA0840B6869B94DAEAD725754F62F18753AD343CE3482579638096D93BF359B3EDDB86B591EC46C6ECBD086D2F938976D65D233087F479D1FDDFC561658782B0BE39CDC95483895CF8422212FB044D58863F0BB909DB97129AB95783C1C96C7011D18A309F721E1C4C1884263CE02156992ED2BA2B44C2DC360F42886E301F5EB19647E40A60E46219146968B600C817237915C2EE762B2614D35E2EFF3877B75E5126772DAC1E965B97EBBE7921A05EE9A9A9B0D794950A5832FA2A46D0464D94841CD45A0667FE7EF64405E7D324F9D0E516780A8B2EB61901EC57C89B462AC4699439ED863759E2B69C2B57233161F344F964085807A34A4414BEAFA0ED9CDD2018D41134EDAA7408ADC0F687D9B6FC591C2DFE29187E261388879470A313BD4D23CB202CD46612ADF658D8E47039A0F4225CA55132DFB9C10DA31ADABE4BE34C9EBCEBE3A29AE9B13D1D097653971F22097E8C4B92C9FBABA10770CCC6A9A65B91F291B598D663A9A3EA94EDFBCD1BD2771B2CBF570BAFE96BB74B9F45FB8E99AEAD8C9EA64814AAB2897A18243260A96874C94D56959EED1CAB60CE212423C2AA3456F1A51D8A4FC28DF9551205A9D2E256472798A633C42AF744EE0C53B0399192BD8DFAA9A55E18CA5B98FCCB6B6536C3273D469046A71728EEA34F3ACB3750FECA585D45E14DBBB272D8E34CE1170C69AA8824A2F7B429DD5A5E9CA7A7523CB27716AA274D5287DD93CD9791953B1012DB2BF2D2F622943E16C33541A3CBA8EE848680F284C124BC2C90E96268920C47EB2439B1D284C92106990939C30D222805CEF175B836297F3D87D668963D81ED998AB1424BA3471B249A50EA9993CF680C7BD385E5EF115AAB86482D5706A96F763BB1DE5CE0F9F172621729A4157F5C69E0E99BC7901D2CCF4B29A71B3569D8273E485D239385589D96976B3CB0D2CDB15A2956CC8099FB0687B481FCD892AED81A4644522B915238A4721A2604953F0708F10569FB69D70420B9C5493F0DE929440F66012261141C9A033A43FEB64F95A2F5BC523A00E8B8D41B1C98FC0BC3D28406EA59B2CB76039654CBC0D6DBB8F1F978D688116A79DA43F6A259C2B609AB3759320385B5B7D8E05EAF46EA2CB153B9539B6850AF7D9DD6B2A7A8BDEB9A9494C1E6BD499ECD3B62D9892A98452217AF0A4B3F55C6B459A35A284F37DBBD61EEAE9F53217E740DA7ED136DD7B8A6E85B336480F391B42FA738E478EA920CAF39864343C4D1340BC89935C2EFB52C609965B34BD97F1BC4FFAA59D7EEEA42B6C34FADC457D02B4A04550B77AE54990ED7627372915B59709CE9578A3BDF8C77F7AAF9E87E0BAD1A173E26098E3A4C1A9769196340B9F9606E75536933CC8C4CBCA96B13BAFFA3463607A0945AF855DF819AFB910CDB966E9AC78334EBAC78B8B53137504DA80B3CAAFC9CD1A961F3C75B63FB4BDFCD65367CF552788F25E6EBA8E0DF7FFE8869DC509274462C78C93460E5436443879E096F4BA3F485582439B8768CBD19776F86131F8B0142FB7B2ED6E793A444D529E0539747CD09F5C45D3DB34DBED4D6E01239B5C25B36F3CDC7B4377F248F706ED17CF87EF5A34FCBED6C97810303F209CE64A54845686022F53E5606B08958F750B95EB868511BD9C92CBCCA6DD60E45C6D2657D24CEB38815011DD77A5A9882ECC29489AEC63A7B9B747D666966DEB6F97AB38BD8B62ACBAB7D419C86650D5ED239C116540AC4E33360DC72B6285B3CD476A9B266D04DBA2E1F7E1DBC5E0F352B0BC142D2F65FBBDF105C5DDF16534BE8C8B9360728B6211E4423CCEBEE693DB9C2E49C50BE3C5691C6FF5BBAF16935584DE7EB446EAA40D140781E852F6B2699C94E53A654935F7F12E5D7BE33786BC6B58E4FEFC6D5327CF65E985CF4888D6A549386B5AB4FA6B343B1CE4664086653515EA5633DBCAA552651697E789241B8240791A8FED28C88F94BFA5080A5D22093AA14DD4BA02A18594E05DABF76A31A40D499DFC7080D78CAF904C8563DA219AE2717A9FC368C5ED369BDCE131A7FD493B70BC61F4A5137FEAE6DB34398D3A275EEBF26827D22E072712EF83707CAA56D14D122B5AAC20D47764B0AB22D5A182F39AA9896ABB96A9D47944C96A95A84971FD2448A5424F9053A63A3E6DBE45B1506C772D42C7DCB104772EFE4BCF2AD2F073AF0FC6B7D9E5EBFAF3850E808D40AAD8495A39229966502A378FC8098F86E46991CDCA78ED5A2FF8D0EABF5F8A392D82108B633A154A1AA58FCA0BBE87D55148A5CBCE2059E9815FB91B173B486B915727B491E124D133B788CA43E27796E054A0BB98A13C659CB49D81B6D453CA53DBBE59A9239D96D0B99E0BB1E63C539B2EB07D76225AC1694C3BD8C3D86DFB2D3C01CFB1267806F04701D9D60309764399D992EDDE4870285746E1CBFB916DC98E41BAFA62CEA2CC296422ECD12693821914EC783371BC7289838380769049DF675B0A50C44B42157FA19D27F14A275EED25AB486E07F9E630DF0CF2EDA0800A7751AEA0DC4CCBA3740C3BA1F9129A22A39B42324ED439BBBC5D8970923A0B8EA3547722ADFD2A05890FF289E1E9669CD5A91F7DE1BDAC2650A7F46C206A41CA2CD20263683721647BEF58234EA1684176D48A1BDD8581AEAEA041F679EF915CEB66186CAB6B362ADB19F2FD0303880F2C334E2C7393D612D480D486E78FE835D274CDB8E9435B887687E48A0FB8E33A42A28422322AF6237ADCE5C71D68318410714C721CC5E343C81114F5D54F0DCE7DDAEFC0D746084BB9411D92DBA348F689F2ED4026956D9D2EC871BDA577A1A64C1A41DAFD7E46B296A8D30FAAE33CAC82544DBE0EB16C42C8076D6D8D5089E5725B5114FBCCE3D72B5DB9F9B80792593AFCB4F12ECF78972E2407C7981F07C252BCABD40CF988FDED71901DB20BE561945C6D35E12B1BECF3950D700650032F2C8FD46A09B1DC0BCBFD884C164F46118D2B9C24CAD4602DE33C8A3278887DBE72092A2214277C552AB5D70539ADD33170574E1A14E94E03998591265D1696A5970DE9F93F3D02E843CDA5B1A7DA7B759C9AA8E6D75207EF5ACAE899964BB46B702A909DFEE78EB064903D06D9F7402A7E24447D09C02016DB0F9251585EA4E469B98ACFB54605AD4893022A2B986CA46E8D0C9CDC88E7762BFBE7925734A9D773403309E57EC07D1F369C34D467E76FB10BD57B1CF8DA167499A8485DEAE2342EE9F69F7A02FB3A7FE0CBA02A7F6B5CAEDB6A37637C16A7B95C852741CFAA3BB19DC9771B44153F97AB9DC0E6914F5A51E92C2B966D25C79647D13545B4D5F9D012170D96BD4F6D8024965F3A16A4C33234204985169E3675C9553C26F81079D335EDB81E0DC5E54A729B71A142380FF998222BEF16E29B5DA7ACCE829B47A44E9629B9DF7D6AD2AAE54FDE32966EABD098F31940D327B22D89AFDDC6174688E40A17F99172B67CF112EA21E81BA7B8AD03D1A89FF85C157E57CFAE59DA8DF2F5CCC8497D0B3B16E4EF6F39727A7E0D9E36619C1F5B8CB3C5D264669A5FEBAD3297A88BB347389528F91E375D06D963907C9D70BAB41783742812B951E85C47972C3D24CBD8BF2195251B51E354A9F348C7D423859388B28E79F184052A0B96E272298ECA4ACB408C58EE2075E235D11D224D97083B523B00C7BCEB4856CAE86A25728D28BE876FC12CA737B9C2F96DE236DC953AE53691579590E979DAC2C3E98AD5A95E1A36669BAD2976F6BA4AD491A9A893597EB02C0DC83A51C1D9159CC272C561B9D1239074491266297151403A14051E1BDD46C05CC03CA3BE5A82DC9571B2691F2B3895B146E5EA34F4B83B94EB8091E6382752C32207345D90EFCAA5C0042DF7E569C4845E431B66D8CD0ACB026EF6D0B2445A5BF0E536C69799D373D738BFDA083A695CDA14695E38BB741B70563CADBF7FA18EF338F1047A10571225C6B94C2C3B5A9A2D8573A9F506D6A2C7B7646D980854702EB7C5C712CB35B0EC06CC32DCEACBF5F658944396A3F2A50AA1034F1EC5527E940BD18059BA3F20DB1B88D71584CA091F8846714C0B9CB4C649DB7821C740B6F3151C47795844BE0C7272B0435A201B856A8DEC30E27D80B44056C806EC43BA69BDDCF75CEEC72C374C275D7E77573735CE3B1FE7B53F05FFBB9EB6393FAAE29438EAAE97713F21334D3E7E72A1FB09B94C5B22685BFCAD27D0256BEF968CB36569B6599A1DD1659D656C58FA200D3F7D7725757BDF9C1FC77C57556842588A6587EC66099E089470A6F872344C47789E8228AA8B8C0B154E7914516149B4E47924B7235AE9D4435F64B4760DCCC7497104C7402C0BDEAE4BBF062F8A35E3BCF797A9AF0D513B053FB7E29C83B3BA61C1E0B439915D5DB1BD21A76D5BC1D932E1B386B34538DFB9389534A14BC44BC2C92C436239105DC6CC32A98952B38C5D961627970724D011E3DCEDABA8C9D7A54975A2948AD71D8985E92894CB97E69CB5E623455196AC154E59B83EA40294EC8059EE87E96E206E567B5A92A6ACD9D1DC9E8BD3C64ED36DE7D8E911F5761DFD5BA2FA3A339E2E0DCEBC8AD3EDDC02E767C2D9B5F96D05A772B6BEA76D494E4B385735CB4DC3D2F1B1C2F2C8D1A5E177DA6C63BE84093E47BAAE107B57AB51C1C94E5888AAB4083914F821D745A5C1192FD71BA04B2039B925A39A728F9B0C0791583EA29912E0CC386A968E9BA5B6FB090572BA1282C539513865F0E0D6C1E9EC3A1A5770CEF3B44D4FBA20734DD163791857A74CF623E5754784B3D388D317A893D9BA8193AFD34F388965DFF1B1C3AA2E8F2DCB7920F3B3243FE35BEBB2BF45DCCA0F95229113914605E4018D1F10544E9458AC4186440950F94AC7995C1588C50A279C73F5C992E5DEC2516CE7BEE8026DA849623A76A2A6B4DD11356512B8E26CBD3E9FBA0A8D8FB322CD2702E7A9CF550FEDD536DF27AE2E09E15EE48D2568639C3ED18EAE3BEB99AD1B387B9F95A785342964B28F552C459A8DBAF4F8198AD68A0B4A29CB73D534178172D10299F6C984EB2E1D733465A5924039881E2AAE502A7D693CB020D4E35E7C108B83259CC4926CAC6EA404AE91B9336F83A735D2BC7176795E1B69E60D384F7DAEA74D987D69DA1681CE7DD4EA18AD5DD33D9B53B38EAD6DA1472C1D9C2A216ABBD5A7ED2148C5B9AC0327795A85D34D7FAC34AD2E6BD2642D56580A4EF940D5F510A83EE105CE83A1A2B8A371B2490D4A49104E9D11E7BA236449E47E957164D557058FD821B3297546102570D27550F81C92D903B99C9C5C8B8CD21F66E9CC042937EB4A73EC5D1BC1E6418593E3D4AFCBE612F53DAD5362BAA2249061B2ABCD3DDE0D179825DB72ADD5576F09194F2B388DA7159C9665D0E86621CDAA3A953938CFF9BAA7723F40B9FA29CF245065C95E574D06EDF432122B5F194C172DC47E44D92F3516465CC01C4486ABF093CBD5E64AB21C620F38E81E47725147756308BEB691DC24D073B3A6F92EBBAF758932B63BE9BD669006996AF3D7C2F819AF237F5289945A97FBA24507DE0E9BFE32DE09602ECE4EAD795BC7D9F602E7AA099C956C561727159C95C0D9E46C73BAEA10EF943E53EDEF825D2E0DED8D742AB4D38FB7BA0255F44A4E58A7489C371151BAB03B5F3A9AE51B2A90BCB8A62E74CA5FE26519A551A1DAC5A6EFD2AB660DAF73AFF36E40BA9B735D969766D2C05FCE14D929A2A9DB1F709E371BC42AF398E2632DB606DB265B302C19A75DECD4026D1B9616A734DCA97BC06D20A935BD84D6099CD21FA8074E0149071ECBF490F75D5F66AAF23BE664D510DD27A8C00696F16637DEE8241B744145BAEE375F502AD961C9EEF413EECE27DCD1558D406E35A819797335F93DBE5438FBEA522E522513FA57EAD6ACB4B5F4DEAD321D07EB5D0F81415E56735A77BB6E7E9CD6CB0FB393D77D992E31B52E2B2C995CB43534166FA9671664794BE3ACFB5BAD51BD96A24A148BB36F70AA859103BF3E01CE9339386D04B51A1DAEF7C77CA3315A683CA32B0429751E70337D4FF572851988461B345312AF75E86AB79B24D9788B0C5C237D20D70D63A26A9C1AA489F11E78F7A533CC43DB89DAF82D779EBB56775CA6C4E7BEA126A93B58A7EDEEC4C2632D386A53385F8A1D25F6D83C73540999E25D0524F805D1E610166E0CC83607F425735DA84A53AFAEF8EA54BE57E3ECF4546FAFA6CED193384F6A81D357EAF826EF7DA65BD49B211D755F0D1A349150C71D3E01C30951B2451A85456B6D58B8DA8ED6E92AC6E17A27DA00EC2E88D205AA957C65AE9A4673092A2753B4507AC2AB9B677AF3CC4562EE874E13627715964E71E96E10D3335DEE05A28A633F47F5CDF6D00FCDE2979E17D1BAD4B90FE99298D12DA30821815C1F18A2620B9DE50ACEAAB36D51ECD48BD81FD462B5A4B503EAEDF9EA6CC2D99CD95AA824500877FA759C1CC65DBA7540476EAF2ACE5626340B3D71228307D4A3DFD3F9ED4E4F881AA8D12AB8B6427A64837637882E5DB59A836EB2A3E22ECE0FBACAC699B0D49EF6C2DEDE9E36EED3552D9A5856E428662B489DA0AA1C3536969B011F33EF7350B351A4D21F9DE928961B0A245D6472BD6F89B22DF89B6D2B8BD86EEC3438A5F3DE957B1E2B9C3B2ECE50371074E03CF6FB794D69EDF4C7747C57F6577AC089D248AEE1642E87A8880A4EAA4C382D92FE9F80E1E169033591834D360EAE09DF97810C38E93E009D8C736319C5563735D381936F6FAFEF1348D747E14B29DEC8EA74AEC3645E5658AAC427B599CEB1291C7D548773103A8D754A7F744DC2F1927CA9885240AA0B86AE3B1A5D67755674D9A44EC6F9C1C5D9E9ABF6DE93386B996DB5F494C6DE5D39FD3E292EB31EDFD5A3F3A183AC582E9048B74FE1E9029EFE527969C60991DA3846E3423D6DDD64C320ECD0978A22DD04806A1B02CFF701D8E9E5FB74AF955CDDCD41DF84F052DDBCCCDCC5932EC57A96F11AB56E11E8F4B5A40BDD5896B61A312C8F7D967EBBFC0996593D95159C1B559C01C9942CE4635F9D1235BDD8693DAD75B6A24E8D93963605E7BE8FF3C8E0F458EA8448B19C7C1D83251EA39D40467381B3F5A6C53823DADDC057DA9308CA0341A1142D92E56652A8084E96696A84C880E90E0E647CAF9C3D20ECAB2F499A83E230B0577FD337D3569E962E48944110C95E049CC2B2AA4847970AA4F1B1469787B6A1A3B70AA1FC358D56DB715520473A64EE9A3C76683DAD66395CED1151DF16DA7584D6D3B6449AAD77AEB3E5C920C2D9FB15CED0DC21BD527A0A51BAD4F4F7C9ECC774723FCECF5292E6E72E798B0F1DFCACDE725B8ACE521BCF8E043C953994ABC353A2BBA73638D05CBCDC75C3A8508E77D54D8F687E6C6F20D794A7FD68B25B94717A77B83FD7B1932F2DB4F47231DE09F95241395D47B742F12CB52CB52E1D968965299E732F4C5DD3749D66BA1CF08B459494B8925969324BDFFA620B4FB0544BD9C6D91A9C4A9D3DDD12FA853A53DFD95A963FA6C00983B34D0E238ADC5C2CE197E19FDBEE7D6CF165310375397871B6A340B5027897204FA240A6834CED29D368F99E2A0A2453E41C8AE7370F640B7040388F4285F352DFF9512E4D4D179A4AC2B53E0279B21F419AA51B2319A4D3EB312CBD7AC3D5A5719BD01C6737FAA04A571DAB549671EA4CC7EA72B0423664738F179C3D438D50FDD8693B7C15750EBD05CE4AA152099F5A97936F1371B3E54D118F42D9D9623AC67A2567896911455A8BA69D8183DCC44EE6C40D81813472355445D7E054432734DEC72BDB3C5254EA9B0ACA253245A034AAC9EB2AF9288434F19B045B430D92B19D39FC1C9695DA51253B0E4B55F26F0F4D2B4743B55C59C18176B378BD4973943B252D821F3E7FD73EAB034E854C03C1B4E0EB5C3F189C557506BF85D37A5ABAD9F5B731DDEF1A51F31BE3BCCAA39DA1C46FFA417C0EC974CB12D962BC49777ACBF70399A765BA43B96B980AA2FB3C27A61E69128CAF7CD13720099E9AB726751A072E7B89387CA6AAB3C893B7E549DCFBD0C64FC7AFD45FE9A70731B15420695C21D746204F4CF96FEA10770124D4C18F4BFEAD8182BAEDA13574F978E8A6B2122F873C22C95A247E7D196996834FCA6CCF56715D36158BAFD70ACE15C7D9CAAAB5BF965273B6BAEE3C4F8110DE9570F2DDF60427D4A97ECA7BF1B4B492432C5F03670B8F409B6C09D4216743E46F65BC5699E855E6FF78877DC69733510E967712CAB651955E1DE95B39C8CD79C9DFAADBD7A134829B5D7CB988DF81B6DC7CEE260794B219845685E6F8A8527E484474CB8C81D3C4619316DDB683D631ED66FB5EB0D4BAF42976E4A0B7DC59E87DA1168F5EF5E47E8203D54456DE51443328B26D4175F8D6FB1AE73052E37A2ECE7ACF3691483946DECFD2249CF7744BAFF43826D7CA4950CB8E9FD12C04117D258F8B83CF1DDAC5401A0D1545BA698E18FB5E81BA3F5437FD1BF1FD7488E89029AA8E845CE25ADD6E992F2F2DFE9606850EA27873809F85D388B27A4A17F0C70E663F668F8F8F7033393CCD11592E6BA5FC48CFA030138A23CD722FF46B8C3E9BEAE9981CC7A2758DA5AC0A92B51EC7CBAE65490895F596AD2DF456894D0F66B87E72B87EF4FCB0DABFF0A56B1BB69B03B7C3D78053D72A0036FB8B7A058894A4CEAF50273BDBFBB2BCA69BABE013943DBF8AE51B47A01A27ACF57A31DEA0161D8DEE1DCA8C35AF77EEEBF1CC916A32E47B2A832D18B30A9F27EA62F1DE9D95E55AC6F0A54771BC356CBFA61F47A79724801F3BBD955E7E963DCC663F7FFECC4F539A792084F228BA64960716A417FF5867E13A9B405DD76805B382CAA6F258292B75EECA2CFB9AA58B90ECA3B285FE5A976C958DA0F21D3A3F3B1E58EDD7A4B453ED28FA422F6EC4A926690FAB3D5B3C52D6C342044E099C1441BF8E09F0550EE1E25C6169B6B52E99A5E06483F7237B45D6FBD0A27B1BEFD0458278FD8B23AB40DD1BCA8632497D731A7BEFF34D358625DFF2BA742E3F2EF74097F4A73C4E92AD61E75D0B670F4F48C9D23DE3FCD20BB683E2227FFCE70144F157D0CC03E18C5203F280C6CF128765E4F8D840E1E97B3D9D8DBE95AC56AD3CA358AE299C224DF6B15A911FABD605CEC17A4FACBFD613AE3D0BB56B62AA5A78B1387B7A3945E3DCAF3B5B1B3B855F795BC0CDAAC0A9F320C209759EA5D16E20F589F2B13CE56B71BE62962F9F3F7FF17C11C668C9F7F2DA88AC7929F7CBD78892E517294F79729AB25CDEDAA76F624EB7DCB0CEB63C8E51E781E5223900C6C9619BC2E707382DDA3D8E083ABE19FFFCEF4FD894EEE2C9089922D9BEB0E47486706A5DB2283D90C654DFD585AA40066B9AA54CA58B34B52E7957AE6C13929D9CEA6061B849AB22B081D81AEDE283077688764DBAC4DB51F406311FA774F81A701E45AAB8BC1F0B4B85F39ED5F96D3C26675BE4E7597A144BF6E145CDD74AA08B0A271105CEE76CF2657F996E1868B9EECAA5DC064A9D3C1ACF32A5A2A53CB438D52D59E84EE81174D97ED3525EFDF5A2FDD16F961002A857B5DC0DB602B8DCE9F7E9CFFF3EFEE47F88208AA5E8723F20DB53D20CB79C9E9C07B2C7E6EB5543552C57BB642BB61AE9CBD62E148AB241E883B3F74B6FFD639C6C834DC6B92E44A1D11E888A40F912078E34CDD622C1B9A3D5E92CA790B1B385179DCD6653C656C509812A752A9C9CC12A9C154F5B21AA700AD117CFF117265B6A1553D6A56D796AF69771FDAA70F2E5FFD9D9521CCDF683E1E72EDE8AC2B361A970B6281B629C83D57E769C4EEE278FFF289C0F7F3F80A2214A4D0016A8C5B9396810E55ACF15A891AFE363BBAE2ED9CD9AAC47B37CDF600BC1563FD8A636FA10B649DBBE94E35DED0951D1286FC2A524A8F7A5599D91F4DFCD5E22B5A282F467C6D22C25A11DDF14E4722515627F2B388B8B0CD9D070AD2F1FA24D825EEB54E895A4422ED44545D4811A03EA165DFE4BB95F8EA67CC92FBE17326D6708D5BD56E4860EBCA7131F9362C93FC23D878CB345F8C4713C8A8BAB62F67DFAF0F70C1AC53F498E1C7F2BEAE44A830BC760C3C5492ED43C5A7FAB8328BD58BBD9A759AAA19FB7DAF87821D8E92BDB2603D1018CAEF5E310754DB304F5E1665D9D5A9A28242E52A482CCCCA63C206762A74D8580F312D9508AEA73F1852F8ED7BEBD71E3A8E37BFF7CF6FCCFE772BCF47271F8B92350911F89EFCD4D6B89770CF2326AC4F1728818891F2A38175DA214B9299613CE65BAC8C37FFEE73FC1C610FE767C37C6690AFBE9FC1BDF4F9010D93C68A7016795652521523A264FAB59F2A62E859377D31A96761F91B5857077A06C6710C0B607C3ADC1B04274A5AB1F1D69BA2D21D3B01D85F141989D250FF8F7F76C469D3C78549620447943A990F1B46206A7F85B8407FA4CEB082BA63EF7452356E024A22AA6925E079FDAA9BD8C66C0B92E0F7D719D0AD8E15A8F40124B3A0958FA6406A7D42A9CD293B37DF6C7B3DEA76E729894D72515CD3FE078A6C6F19A7F78323D4A489DDB038B73E3A9C019DABC57A4E9E892CD48936663D5C0AC0BB2A571EE0DD886F4B83B0C7686429434BA4ED78D013CB987634FDDC9D1C7E9B684F6038079947FFF3C3E50C8E44601E468E2A561A9890230E1BC629CC749BC1F1215A15545B8547786CAFD0AD41756AF4C9420D11D21B706727365D9B78B0A275CED2D99B247173FDE3BBF3103C6C0492C51AEE095C81EA3DDB038CF2710E80FF81EFC81D387D9C3CFA67F1071B81D04EC42C38D5A04D569AD5FC068698A3A1D69F654FA63E69FABD2249CD1FE502C8401AA4B746360893A26173D18ACF346F96DA54E541A3F114890C4E391703E289CBC04263589A268D25AFE52A91338297CA6C9618CDF5504B7E44275626715AA4BF4A5E6AA032A0C7F273EB578830C8A0CBE745B5CC5EA3769E95662CB80B475E77BCE833E524D8CEFB6DFB740283BCDCAAB022C0192887E9F2ADFFBDF06A8F053F1415C49629B4D45CDAE2D4E449A9FDCA8A971BAD2D4DBC216A2D1904C110D8093886E0FD9E50EC8E50AD135872805CEBEDE62DD4F4F122EC51EC5C8F9002768FE4514C1D2E2647893BBD2149D0AE78D5227CE09F85B9C1F1C08351BD7AFFA66A1FA9275A02E1AA177DFB791BEF6F18920C1F9F3396C49DCC01B0D5575FC6D4F4AD429385113E335380F80333D4EF38B7C7C03973B158DE21126A772A352A9F9703BA6ABB688E35DD71ED8149AAAD6ECBAEAA4A90F91A6F6B4569A6F8D8FB57B4F34CE5140069C7B201AB040879E401DE33C88D40925516AA741AA3F86A549AE96708E85E5D4B064694E75F37D72EF395BE0447E9B1C444B2F1D9C2F6B205D3FF952737DE5148B6E3EACE58B17745FB2BD607BB9D87BB9D49624C86D28BE512D21B3CADB6567DB7A4BFD0D683DD81CE20FCFCF91D6215727970BA8A4D4BF66F8A3711EE3847E9C0F15A77E7E5990635B73B89A2C49CACD55ABCE9E9B0479FBFBDCCDD4FCCC3BC219B84638F78280890EB7862C50222ADE552E1204A2C14E202A748D286AA33C8871520F41706A4F3BFDAA9E24965C8C5A9CA78CF33046C5028172AD292496B41C17AB7E5582E54B2F045A87FCCAAAB3F552E304C817C4B287835720FA5CDA174BBAADD8D249907C4C724DBAF6BB36BDE0D5227022281417390C445186C2CD2A972BC9D1DF0F2AEE34F9DE4A321C4236E26395B9383D4FDB911DF28D573BD03B1516A283804CE10C49A0FB210B34186ED34D8E19A718B9DC7037E42041FE54921E8FE5031B07CED90F6236E39690C871AA57515CBD1A9C52AB0027AA554988445B8BBEA755901A71D6B31BF9EF7C5AC0BBB22219E42BB2FEAB56EF550B8FE47BCD6656EDBB644EAAF586B7CE7DE848F751D4991DA7CC12BF3672754ADDADCB854681F3BF4F0AB4C10F97F0C3D43A5873A4A970B2343F6A693612D53B155C9CA132E0542E376081D2CDFDA2FDA8B82EC98D3C18097A72B4E6E2FC0E1744B1534175AD09A70814E113292EFE3CE5485F2F559DED2B9FA2EEF655B86A81EA8680C5B96870F65F9301F0734354B394537EC9462652270A5CBA7CE07182EAB32496F4CB13D16F5332263AA526C3838D3EFFE65F769E930B5CE9C0FAD4C3E974977D9C6667DFBB2ACBCE8736E144A5088B0E1C9C22D06DC219EE85B3BF67EC3C67F2EF81BF3442ACEA92BF45380192594A722B505DB18AEFA54FE4A630A52770AA8A8522E8A2699C36643A166115A41F74D5FF6ABB385F2C0ACEC16B36BCE6D933E7D3698BA7E504925705589A7813500F77C2EC3891DF9317884AE444D458F83E95BA851A61C4954BD2FFAA78F9AFFEE1838C46915CB0490267C7AAB36D66B85C372BCBD23E4E221A41884008A70A75124BA1286CD804278829840F0E4E98C10978224D79FCE1C9D4D42D0AA75627CE7ACA6F8F2921EA7DEE36F6FC6CE2FAF2B923CD1AC81ACEDE2BD62513EDBF169C6DC1D9FEE31929F85DCB2C739A753A8DB98D7703CE682F4A8F12844FFCB6E272C1B2E42C577222931CE17360968FA665FF6F954A1D56DE14A4369BBCB7FED6E264C6325FB0202CD9A2486C14419DD1413CFD3155A2C4E9069119249CC2B986E02F20956AFF512DA107FF3C30504D664BA9104B53A9F3222373883EC7A7CCA9666D69E5B95A2C6BD6652539A263A43C84D3F5B4EC6C09E7CBA50E779458881429DBEF449D5A971C53A5F71423EE5C16283D49A0674454D4A95CEED7A909A59C254D95D775A1FEF7E7EF244AF26FFAD72C3DCBD43C97B3515309F45DDBB0649C87617C18911D28833A399ECF5C0C7A88D210659BA9630FAD496B81F36FFD3C5EA3DF6DFA43F9DB09AFAEB034C9801399053E1DEA279C513F0138C39DE019EB46B9DCB72D09967A39A582F379B59231385F3C279C10E54B803446388710E82BC2F98CFDAD191367907A399D7322C149EB2A089C37889A25B91368F4925DEE2D11158AF8A048A0F71360A61A86CF728BD39403FF46B5D31FB3C1E650974FDAC7BE6B9B214B205F502C35D1F434451867EF3AD314A7EAD3D792126FE90AEE8189D2C14C795AC9802C4E27944A34F53CED750184C8EEB837C4384FD3040911E744A4C23F752DE12E9335172D55972B993054DEFAE359B782F3F512C7CEF6106EF68F67FFF99FFF289C1FF45C049BCC9E2D114E6AD6A3CE4B0E139C7C8007A2C4F28A525C2A5A58A3134688CF4D700A661CD0E7C69E4995A7006C82EBBFF9975DE654079B9D7D9A65F793C1791065E799A439C2D2CC346BC7C8A61BADD2BD33DF9DD5FC3012572B5957CA06E737A725441F0D0E4A95DF324E32F6BAD14EA8164C5E3CF7D7559C92F4658DAB5BDBBC46A2F36C497002E19B964354E184368193C3A71D47956C969EE1C04945F04B3A33A2DD9032DBEB02C058A6C4528228AFE91AC70B9C1385F3B6547653F2FADAC3A3D83FACDABAFBFD15E6D9DF0F83ADA0A53CAD1A17219CF8E018E4834A798C6B6545BABD56FA45EF9CF5907B072D43C5D920B52620559CB055E75F533356220D5B4E256C04257FCB3911C5516E152DF26A89982426869329632AF58CE5CDCBD1CF80EA8F671D044E522451149C9C07C1DFB6F002E0241FA045D9D6B5A61A42939E06E31C6E0CA89370290B441339172DB05B05D5FADBFBB1BC409F0463F1640F82F39F474DD4F1BD159CF3E9C647A91AE3FAD45DC03BB2F9A2542015CBF1BDFAE8CD38813157AF949A3F3C4805425F1A9C7FFB44FFD25DDC3B71B625DC2C795A537D5E38C6C129DA0B5DA242CB9B24725836ACA6314EE8AF45C92DABF3758BBB7DCF5BEC848119E2044EB881B619E0A3C69EAA3805A7FC14846D5926A33EDF8DE1A7CE756129050C6B60C2BCE9F99456D68A311736E27225D5F8C915BCCE981C78BF9D2EE15F7E5D0CB68305495C9D48E92852409A625F9F7D5269B93D58D586955496F50737223D4C1B3EFF16A233133B25701AA2C4521222D72E542F37DA0988E50B26FAC733134A1B0BD35AF37611A84074F18F67D4D5FBF339172D2D496E07AFDAA0FB1FC649D990BAF68EDAD3C103096DE7A4597ACE0BECF128524910B9592E3D6F39768ABFBD71F47A37C6DF958CE2FC2CD7F1B5148D2A9C8F5EC7DBE6C0EAD1D8AFF3A6054E3539DF71143916D77A6FCE38F5CB914B6400FCABABDF6C7267573169E852731584A618555CFFA202460DC27FA5793062C96F2B0905E7B7B9CA722F75314A443359CE544409EA73559256889A63FD0CFE8BC2F9EC59F7C5E2E05D7BF0B60D8DB2E1A01DBCEDB45E904061BC0AD696196ED62585CF45DBCA277FFB8CC2678020C53899E88D22279F8975BF776351647181742F9716120FA896F87BC59F718BF4D1A9675C813A61B5025509D763BCA012572DCAB18991DAC196FA97B39FBB0355A135AB987765B83518ACF6A2BDA0BCC9C5B53E72DDA2D4C9E70DEDCFFD3611D7AD42E655A1522143D4D065F70B9CC1D6F039770F0CD1675AA67A54CC2C72A1A459D2EB24AD679CE6C03A940AB51448228A2488ED6D7BF8BE2BEAC4DBCA247E5B559CAD457F4A4170069B83F488F25B5127732ADDB35F33E6D1E2AB42F42A4F8A00282BBEC8F5228C4E88AC461D96C25B7CF2A35A82B40B59FFD5ADC4FFFE5C9868512ABFEA28527DDC377596121B1CA84E1CC5AF88BF33DE0F838DC1E04B77B8D12FAE688F91B4849C09DB89FEB30BFE500A5EF514CBF5636E704AFF4F2F4ADB69127CB2921F99E6EA9269E570520AB48AD3330A93123BAD3A5177022A04FABEFB8C5E46C68DA18E7A4FCA69971C75AA1FDDFFD2A365B20B5926D3386F354E4628DF1ADFE82FB95F6FA32C3B617C5C3F2B6B53FF7DACB85F6A8F4B8AA3EB40D35BAD2C6A2DA80AC484491D294DB0542CAF55E6A9FAAB3CC4A502BBC179A787F36E557D4DD305A328DC1A0E788601190DA2A0E399354E26C7EA649CD72E541128792AE00C3687CEC881D5E833DDA253F35A4E33FD39F707A8A6FC8343E6CB25ED663D9CC3B79D457E255E4B099169F2BDF5D4A970BE788E2A9EC361C6D80A27588E4591E287E5981E6F5485AA42ECAD72CB72BE0A51EB72BD65477A1496E2DE54ADCF8988022CDFFDFB614195F32677BD3307265EEA16B9924EA1CE351E21A80B5462B03855F85EB390495DF5BD90EEAECBBC3977372747614E1785F35A87D2ABC2D528DEC41B5478B1A8D35D22FA9CBA742D816A06EA991049B3CDED3DE36685A583B3DD7FD326987F3C5311541A08AAB3E8C54ECA865E52B70F21405C8B3AAD15D4311D6839CA592B2F90E4CE341C58C7A5C4D48A408D47E5952BC22959A42A0AEEC713A7623405FD82AE439C52C4E0B9757CEC75935DE93F43FF17B7C9607AEE780D5590A7B44E8208CA15A71EFA6AD0BD03D5FA5E1D414FD2D6AB250FA7CD8C88E8E2EB45D593E3FE1CC4243817FF78DE79C94D83B72A0FEA53B9427527C54E263A78D7A197F2FB9003E737312CF5A3A8937C43B41B66A7DC85BFD2EB2A4E42AB71AABF71ACB541AFBF54658C8E3505542E39918A82BE3AF50A15CF5E7D55716AAC7F226FC79BC862CE828D97DAC1564DABD3E62C3E547D5616B6BDF075A2887E77B8522F70268D3D39C57406A85972542ECC0FBAF6D579651322B7FB637172AE4B24A46BCFDE92D8F0BFF68BC5EEABA5FE1B07E76BDD7F77FC2D902B9C7FA0BAA514D771B3D6D94A3614EE04BCAE4241C1B0749B44E28775AAA1DA0BE2756998E1927BF7B763F952ED507329CAEA322F6FD01524BE72CBF046655EF4BF74AAA82377E9E374A2A68FB30AD59591EAA1CB36BFBB5A17D0212A392DB5002597D64D3EED034A45B1E2788DD7BD249CE9093589165F2E2E39FDDB8A4649A6DC52506A437D22D27C634A144554E36C094E7CD7A853322C17A769113FA7AAE61972025AC73ECF557E6B742938AF0A2BD06B4DF46E2CAFE47D1C7C1E2021B215768E335EC7513DA62395FAB7897A4FD33BE3FF628CBE75592CD878A9E0E9DCD503E97B45DFDFAA13E4A6A8A4B8668E44DA40F2A74ADB883B0953D385B75ED7FC949BBA46C9287C9EA4FDCF3D6F45B326D3671AAAB267CF7ABEA71DBC6E99D8C9FE960CD52788B65FD8096CB362EA18FF2C52E7F3C10A25B71C3E0B931029995EDB6C96BB0DB93C692BD1DBB1E9DA93CE2439E0E5B6C7999D08E0539F7BBFE69C30277A8D286C415523468BD71A67D51C97785DD82C5497FCA5091B7EAB88DBF113FEA5736A25CA7082E9F37D353D6B3F4EDFF802355C59A0F8105542E4CDD6AA7D480D5081F31582A5CE80449DAFCD2882E06C079C10E15B48ACF404B637A3B4A85321AE559E75973B84F354E11CEB94C79EFD92395E16D949929FA54AC177635580EAAEBDF29CE27279495CA9F36FF2B1BAF2F14F7717A7817A9E2D8CDD6AA4016451896DE61D6D2D214AD7AA72FBBAB2474512604E8254C66B864EA6BC4A636B5C27D2B8A791257AC97BCD4ED2F69B255EF274E6A45DA27E0D837F1C290567DB63A9A4D996848889769638C3D227CA921969F0FC2D8A19D42A07AC4EF509947E04D5FE969FC76F9E1EC6E5956AF38A432E8C2B56D144AD4048B748D633CA1BEBB7BD60078F58C5992F3467ADE649D7FB39D2AC7A5AFDC3C68EBEC172CC858A2C207819EF5F6A5D458D67EABCBAEADB2BA7145BAE3627454AA02F9A045A210A9CB478E2E0743CAD4D6E4D8AFBB6FDDC7ADA450DD29BB5A741C3978BF835A8D577911706E7ADE36FAFBC6C080E96071852F337CAFF52A9CD9569731693DBB19CF194FDBA2CE5C048FF4277B6CFC970A22FD440726E796333D8CA81AD3ECDFCC095F7323ECE79F1762CF9911DCCD42BA314DEFFD24B3746A0A615657BFD35C77BA5057A9C2CD517AB9B704A72D479B958C5F9C6C7A9599246DF75CC066F4BB19A0DD13BC7FB610A474A1FABEADC96371ECEC251954E6B7314DFA52A4375F3CFA853FF171E48A654D6E23444B5964AADE6422F3D2DD448D80FCE7A7F3F1FF14EA54A6F4147D3316DE21C736E5D4E9D31065935E3359CE9EC9BDDEC201A9DDC8DDDDAD7D168611C2F09D4ED10D5D6AE3D9C2FB87AF99396C606CDCEB66D71B205DC916F538C5C5AF29220C7F8A740F4D16E904AADA27BF1B69FE07C7A35D2EA19E5D8546B4C4750F39156248E13C2AD7F8C4075E04426B5A0F05CDA06A9ADDCED81B5B2CE55FD77835F92B4D46E3052EBA6CE488AABCE7B07A7DAC4E28CE0D6B2EB423683D2F687D44B88E611FD831E97FE78A61C29B1D444B51CEB38FB6FDB8B7A4F9223502F91766A956A6F4861AB043C932B693D983516E5171D96FAFCA8454D391B2E8D780AAB4EC2A9FA675975A1D120AC1E7BEDB78A46E574330B5BCE4686094F2C7A537D16E7570BD56C17F4646A56B975162002ED7FE9CD9B12F2FDED7394AA436E17B8025580E5C071B6C88640B456A22CB94409274E91B53E0D694AAB4F87C9B1AB42D7373A7D1C5377C91CBD296C3C757A20CBB2E24495902C4E4A85CC38A4827AE1E074B9BACF54C117D5038D13FF656C574365E39C3300F64D5A534ABB761649ADB58DC775DFCB2EB73033D646A04FE0E456D12239DB9605E6EAB2F228E113F9ED4B3F5E3A75A7C1D9FBDC51A5E745EE26E47639CC8B7C36E9B579EC55A17A0BEAC9DCC379ED54135A2D15D57AB1530D70689C791DA70BF5A2F6A40BD55BAA547B4EA40C5511D4A4B55A9AC634D13A4E67B5CE75B99C10E1735C72335BE36F6BC9EDE21F82B323027571AA5AC5A14BFEF64DA7E5815CAC66B68CB3FBB14DCB648C53473B9DAC9ADCD5D599133B6599A8865339D24AD025BB74DEE7CABEC66B23E849D1DC95E913260A76A8FBB96E2DA6E69799E4446A80E8874D68A5A1EC107542A9B3B1D740F522E8952444994D88E645D03F5523D7EDB63748D361298F5DBB70DDAC4EE89EAE1478A0D6559CAAA3ACFCAA8D0255B1D3385BDB9971C3A7138675366BB95E56F35B85538D9F1B96174D2C2F346FD727FB504BDD1A2ED52A66018196B7FE48D1574F976610502F0B18B1562708CD9A8F1528136D48889A7AB9283DEB4EB5E14B8D1322766B153B5DA6DE79512EAE41A5E7995D57A93786DCB35CE7328AA8C279671A78B6B691DC4A25B446D3978ECBD582514415CEB34C1115977BA1B059601756B8959755BDAE5E70AE94376AC9FAEBD84C154DF5956C354E1B478DE39D7E9DB81AB5DD06DD0ED4024DBBCB5DAF21D71C419FF55E2ED6B10D5EFB5C1D81C2337BE7476DCBBEBC2D4A4FD9EE69C46771BA95A27DD4EA649C855D44B34B25EAC526B995E571A77563E4AE724FE36C7166E5625AA38E0AAD222B383DA2366A3AE9AEA1ABEB0A2F27B263661333653F35CED6E29C78015526989C9294889EE7C9513C2F2132027DC663250DA950CDD9BA4E78C951A7E9D6BAEA7CF6EC3F5C7A26B23C624A4F9B8E9A47579D5AB8D27FE04515C7513B6ED95B4E76BB7D5A94B662F19CEDB98FEAAA8962CD1A92DBCBA2EE844BBEA897CA72B91DEF6640B2F5C5562C8ECBB52FBBF7F3A35BFB9115B2A6FDA655CD863C674B8DA1EEAB451AF1AA3BD8D70D20E5CBB68BF095EEDFBE5C72703E0B77EDAAA717382DB9522A9052BF605C55E7D82AD2ACC6B8A5EAAD9E70B8F644E9FA5EF9C017AC345D9C6622B28EF38CBA8EF393A6A2680CA857748953FF0A6E1391A0385BD36D18EB8123C7F76AE17E75E2A876502CD02CDA0D6DD3DC94868E349FF18E86866CA88ED679A6FFBAE5E3AC4665E00C4C27A1D600324B9B6E1F4D7B4E277602E78593CA3A8FA6A76F47CC4D7BD5444DED7B09674678AACED64D7CF4986B2AA670565E7CE9C5CE3A4EEECC65E9716C5CEE582D749BD2D3AB52F486FB4A6634B15ED775B9BC6A465DD657B5CC564F123DA7E192C57AB7BD99AEA3D7E72FBCDDF91E4EE924AC5327C1E9F3154E2AE425444A9DA67B6771AA7E50AD31EB8F5FDF3AEFC6B98F97E232D70541A5883A7567D5D3F2EE1FDE06A4713AF9B057AD9A8533B73565AEEB058DDE96A6A5E7E074BB425A915FDDBC572EC9A8F76C9BC109FDCEC3F58127A34AE7F6CFE7ED178B2AB97DEDA8F075959FF75D93DCCE71E3C0D9FFDC950B99D8B6AD9901BBB17AF21A3A6E66AB9AB726A1ADE21CEBA9141D62D49FAC861074ADA2D5799A5AE59D4B395C55A7F03638B31ACE668DBA05A8CE92E81DCE336FD2DAA63C5E634845568DD342D51154FB1F1D41CF74C5F2C2AAE7B90439FEE89768BB6E2D1B9AC7553FDAF029077EA1426B351F7427C151A771A76ECAAA4F6BB3C8A5715ECB78A6534A5E59EAA62F589AC7EBB2BCB4AB636E345D302C154E5D75988DD06C4ABE7A9F5EDA9C10CDC3698972ABE82429DD8BF3DD575CEEA4FE8CE44A9EE3BD777A0B2AC5CD3AEFDB06A72B295523D290ED62034229573C9C1DF32D0A9FAF96166BE153CE12E06CBF6DE91193DC00F09CAD9BDDB8ED1E9E82378992E90799577A0D7D1EFAB29DCE2B2B502BE8CB42AB9339B96CD4963C95FB64959735A64E7665FCD275B6EEF482829A1EC5B619AB7B468AE59D576B5A757EAB6A54AFC08C8D4093A3C49922B0830A5EAD324F88F37BB98B6626E165A556A1F75C7AB568715ED5D469A5593AA7B5C6A9A3A9969AD3AD35EDDF5B6731D85FAB311FB87A3DA9F3A401A70A99DAC77A38CFAAEAAC24509512C58D1926CBCD69B34EAE27E879DF9264B3775676AE461BFA47F75E47572544DC217AFEA216E45EA85A85D4398FE5BC56D1EBB6F5B495BA537CF8CBE7B60BEF3566DDEAB3ACE1546D5B15382FBDE6808FD38F9D957ED3A58AA0FFAFB2EB776D63DBD6FE135CA4709122861431B88821C531DCE60852449022821411A4B8885B0491E2205C049126081746A430C285185C084685615418268561D2049CE2804F71E1BDE2C16BEFBF71F75EBFD7DE5B76CE61303A8A24CBF3CDF7ADB5BEB5F61EC478277E95552D2A8A6A09325B6BC89484F62A83F37A4B10BD49335B2BBFD12ABAACECD6F042B53BBF244DADDD7F6741D40F02C6ED27AE9AC1AB7E6C87E53E1F8D671E3875BDBF62796EE02CA442F4C941C3A36D2B2326BABAA84BE13429AE39393448CDE5A3F5F0C446B8639ADED9E456FD20784B88A93BF5F96201DF06C758C809BAA2FFB5EC14384987AF5220D3B68C27A83172C9760841345A48BC8E2E6B773B76725A447B35F0C43D0DA6500D1AF4F67CD17B71BC6F46C2ECC410949E857C2735133CC6BD679C04D96C4807F0F77062A8953CD359425DAABA96A3C04281B3CB2C59CF518EA0897FCB4137C2D96D9A700A42FD60731C01CFA5B5969A08B3758E12135F25B7F581D31CD79016FDEC64E195CED1FFE508CA09917104FFD7953728443121BA6A16677389A0C9485FFFE9FEB80458D918E207C3C3CC4CF0B54A9CE75B65706A7D729B027CC34A0B54EE36ADA1A6CB6FD518E2A52F24BC4939CB2F8EAE10203AB7C98E90950F91DCA6B51EAF6FC5147AE0A943D4A6887E8BAB8878DD120551115EED94D9FCF6FF8A3E11D6A0B4724D06B7D41882D8D9334D6CA4E9B8D829F3488757C2A866EAF28B3104D3B6E4F33937C0E641268DB8158D453839415538DD87F82C179714FEC80D0A6027A2557D9D87D31A83E8AA0C27E257089C56691D4D35D165ED35EEAE358F609F125A3A6383E89F5C7D329CFFC936533188129CB87C67FA71BC9F0439984938123801A4F1B6F427D3E1DEB3341572709E339C58082A356F1DB4C6F4E9B8D96962A764370E513BBB7B6798AAE1F986073F7FDC82C9B76E5AD8C727481FC2D95EF9FAD20651443775854C4EE49F27384BED6EA94429D1FD29DB0B785F572D4025E5FFF3861ABCA13CED0E421ABEAEE3C457D6C43E78BC1767DE899DC7133B28748FEDF722EAEDFE535FD18A93F08863E746D4C8874F8FE22DB7A6793AD7DB7589D5A7BEBCAC75519FC87AC2819A919DF17BAC23F9E65F6640D0CAB0B3E1ACC7E4B47943CDD6369B14DA8EF720495BA486A32DA645BC1EDD64B6CE30125C2D3BB52D2A770980AE594C719FB2D8FA99045935E6866CB795A1ACB7099C92D906763A38AF5BA793368E9AB49647732987B0461D653742EEEF6666E507C329C9914EAB40EC24E4D64D7DB100821AB165486C1E54B0ECD911142F4926911253B74DE1747D9810816E4572DD0D1D8CC9C079AFB033F589D024320B7B1D9C340E9F201A003B1E3FCF72229F25B9351489D87E65B1A510D83A8695704DE0B4765DAAB7DABE2EC0292E208B2DC119419A9F4E29825A765272ABDB70A9DE9AACC72267B7045205A6C73CB5EDC98A48D7978B5BE907895B649A9DCE0B948905E972CB8D02E0038F9EA5FB62C4D50DCF8ECCCCF4F1588034A39A4547173C7D25A85E257BBB58E9D1B4AD715035473558A226517FD40C12F8E6A57DA3FA850AAA6646082788EDCFDB1D0E96311F599CCEE24E78ACB7828166BC1E4B3B9360D999B648131D6638DBEB94AFB1E5B286BD94FEE4F2236BA89924C8CE75D2BF52C5122812D2F5AF0B38E9A6EBB987C6D0B19F822FDB7EE3E78EA3B40E895723A562BB524F2D1DCDF2322BA340F820EE35257A7B938D75DDB8D92263EB77AE7AE16347D39F80D665BCCF904650AC2C2D9C84A885A72D11742BA20E484B501E6469A0157CFBBDD3C2F9AF3BC97BB3968B224AC22B9E1F7CF2C11384F3C0C2497BD8DA254779019AC5D190DC2A3BED55B2C77527F6A3BE955154388985D40B8B9B657CEBEC45907E82CCEB5A506FD81CB6A3270C67251174F6795A2FABDC559074570267AA99C5A92257D2D06E98C54B41AF092882E3DF4FDB61E8A61B4E7EFF6D4D0666ED5FA6C702A963FFF75EC2CEB84D5402676EEC954CA23EC3293F959D54A8A8562540DA7AD4CDB9E31AEC2C763A38BFCB14991BEA54829A0E1AC44E0C96E0C5CF4E26A4B72BDA1FBBBD12203D3B4BF325E5BA253F785D6A4253DA3F6855872FA004FD8923996E298BD0541C7986937BDA58B15CD6D2BE063877FB4FF727299CAABA05B20A9C4F8FD4DBB321F94138BF39503B5ECB20622B099413EA9299A0B69F2728E54A379CD90AA8F1E659673325E85A7C3E1F3E7D8962806C0BE065B3480AAA4594A693E86664F163E192543DB1058C44535CA504EC94454BB2B97C070991D808E1BF3E2E2523E47AF6289BB71C4747CFC90ADE7FB67FC0028EA990BA421B4C6AD452CF03A7C0494C6576DE725FC5BD31772472BD855408B3E51DA21D820788C2F6640B27B9DCBB6E6D4E9B88E443637F0F1C0A2D3D03924B063D32555A2E14478DF69223CF86DF2D4D27C52FB6389DDBCC167D3EBB925ED02D142A8CE8984A4F8EC44E6C05CE26914D0149241463A7C0D9C292693E8D1C358B04BDA10B54068884AF04E7F76EB15CB0D8AED8E15B45BD0D0951F88AA16CD0A879E5D2DAB2C66EB6237AAFEA5A383BC36F0AA2BCD01525F7F6076F7D67EC409E9167B1BDD17A2EBC1DB024388F1338416C2762F895A8694BCF7D155BC3CE6565C5F6F69BCE62512064A8044EAA3EA3616EF4D9BEC5143C88A2F2D534E0EE780FA3F09AC9C964474495C47655D517D5ECCB249CCA782760F957716BAF7500C5568DF761F60B475C0B6EB7B1452FE2AA0981BCBB49FF004294E5375D992470F2D79B9D4C05CE2335867A26274ACB957192E8C241A5A7A95252B165385D20646FDD341E68EE19C6ED48A2E53A485D88E26366A7C4CEEAB21ABD1F72A1E2419D7C18855A0A080A39D1950F9929967F1FC87476D7990C5A98425A143BC3C8D13FEFDCA0A20C2DF088C99D6C56C658B67CE1EF3FA25D4C0C9C6E313DE9EDBDFEED91535A61E75E884D692AA4F3C6CACE4EA32329302D3DD30CBFB36FCF52DC4EDA9CB7D0DDC49FFF03C168FA793AFEE748C4568E2A6483B33FE2CD8FAB8B396EAEDF98BAA563574F62E7C38AFA2BAA4B7A6B5C7BEEC684AB4A9ADEFA57FDE892D59F3A50F393A76F4CDA3CFD30A1EDA3F67029596A0CB984684B2BBB67A366819D4D8AA580A4D191AF307C7263B1346F31CFA8EA8AA12FAB3C05CE9B2E5073F2719CC3893730594CFE182DCE208262A7055E6607A60BA5C8FD705EFFDACBE435D77AB9C45B48ACAA284766BD80E5A876747FF0CA3AB9D4E0672428EC00B6BF5760A745F49E49B041DC6A7ADFE6410538AF2D35D994175063F6CE70F292690555B0BCE6854449968B05A8EDC1DDC46D4EC2DF3B7CDD9FFC6BC4850A169A2170AEA23714101DBD1B44829EA3E4468EC663C571D438BA85235928F1B714F89AC753364AD0F07C1D4793690B02CD028C13CD63A89DC069791F3E0722E85E50DDF1A16E72A1DB5E1C8AE46E194EA036D97EBA16388333C9F94D5929ECA441B576E3097AADF0DFF24C97935C99128A0FE03530B41C1019BF1B4E3E8C770448E46583702EABE987F1EC13A4B881198A2825472E45E2D4B7F5FDED74E0AF182C73BD3511D4FAF52D24BAADC07943044D066A24477051003E333C79F0389E7DDB54611B81774B1027A154AEC4D2F349BAD74DF8C09A6F49F6A0783A136D8DE7C150538037D174ABB1101FB4D8F99F7E9985C019329E9D787734888E1423E141486EEBF3C5F85F438CA01444974A53BD0890D957CEA0377DEF128ABF4E5C04521CC4A0BA5FE728C2B0AEA3B50BB56CDB0F8ACEC4D3808F5A37BB8F76475A6E1A9AF2CF49BEF90513741C9D84129CE8A05D35052C13539A5F0375BC9E8D2E07F5DA321B198963D60824E5078178E1548CE29D2247D38FC84E14520BE7321EA3F783F997E9E2DCC2C9FF6A11E544C9C1691ADD5B699AE39A51CAD5A3212D8A70C640D8C996E3DF6D8397A706A576D2DF42416B7777576F0C28D424D535D56702277B4389C34799AD18A245BD5546D2768B1DDEA9D4C279EDE1BC960D2CF8B1C079A333092D4F1B0DDE0E26702BF21D3448154803D8E8ED00F4760E045D1882B2F07A9ADAA105C984DD30CA838896EDFB56A8D6405396822B2C64C02C5781A4DE535BFC15E15FF776496C4742CD17BE00CD4787949D3D27B6BC4C5FD8D9DAE43645A885ED392834425ED9140EB53CDDD0253D608D453308B66D8C4F621E34FD036D849567E7928E50AE44BD3D9B5680687D61F496A17504358683EB8C6E9AF67E50B729B0154C59FFB48A1BF2B5BC6B77772DCB9E09CE56D4ECCA7C386567EDF0659F3643754A7B2CDB476D9BE753389F3838433C36ED61B56D9597A6C120FF5B2F439042BD6DC549906F8B5B782544A7FAE48626A463A2109F699B4D337ACDEC54468AE4223BE1067DC37783D9E749AC58CE0DA2268EEAA560263A9D8BB4C92EC0BF0BAA3FE0EEF5F137A6A32A8945452F6EEC6E01E1AC85AB61105706F23D710AB58AD94E3C43F440E0945B2E3DDDAF217B48A3A08593D142FD74A7E2AAFCE74301C39F668CC0CEEC2D7D0757F0E262317A037042EC14F740A3663842E919E17CD30F980B9C6C2CC061A2A93AF876B4339BFF736B43EDB7BFCA7EDE032D20547D95790EB6CA10C884947C31357CFA8264F59F1CE0CEE1BA7F38DDC8336BAD6446FCD193837D8FE8F1E1814C74B47952638EC8457CB0928505856B9D2A519922D355F81DDF93B0A58604E480B32FB3D1DB0C4E173801CEC5D97CFC3EDE2E3D2444516F6D4E64E2A8E6B76BD79949843789A91C566B73C66B876542681D5C8A770988A330673359C4D824D73843E8D8C97A3B7D3B441447FE703B2E96F496E7A74D1EF464AFF7E208CB7125D61638310F80B962F33DB7BFFE5666C064FB639ED7BD93E5F537D1DE8B96D036B155767E9DCF4F2623AFB7E5A265ED79593A5A5FA172EA2BEB99FCA07D4ED9EC41F875312DDAA08DDC59516D3C9C0DAF35C60E46B81A62F83C3C1E99BB99BBE184DC1B62388FD1AA357950FF1FC7CA4E087B5BB0D428C0546EA9EEC4B760EEBDE1847643E50AAF9B6F05484C6BF1D61DE199C9C974F46E18A809A95011CE8B0A6E6E1B093A78DD9F7D1A2F4EA76A29D8A2E5921D7CF7330BA56BC34EBBB69047069B1451BB184619D618C0A29B7111D74B19E2DA7135BB58CA3015F459B1A45B787A44B7B353768A971EEAE055DF886DDB093C099C6B52D139EE74E23543F61726B24AF8BCD6C4CA35384D6B6CFC611C4434C0393B113897F658442C2F223B430E3978D99B9E8C62018A04CDCAD0D452105CB773B49806DB706B8E82722AF9D6352DC74842B279017E885C37A8788BCF33B9F775A6B766BEC43215E0C4892145F4F15E60867EB80D7ED7CE8C95EC269CF408675C79C05F756D548781849F042D812A1DB138F01EAD5A64E7F05DBC8354094E7A10471108CEB3F9E8753F942B416F63044DE15CA4668248EE368EDA7EF896E06A116D0912A3990ED4503447C9A5B3790D0D3587A582DACA9413A44E43CC6F9F9B1B9A935B948D509B7ECBE0D991B3841EEF85F3E8AE15D15B0FAA048E78E7AE35C38940663911C3C9BBBD9B550F22B67277ACE19B01C2392538973941B15059047686FA74F8B63F3B19CF3182A2A58007C089558A09A26A02BBB4A8C8D7027E6E69A970D4E24138F187C4F9A6A060F096CE6C03D05850F54A22411BBF1C500455144B1BC4BBBAF3185D78D926976E8E63D969C0102C1B8E912D9C28EA8F5EA528BAFC08AE06B123C446D01178EE238568387E1F3D84D9C97447E8E810850019E13C9DCF3E4D07AF7BD38FA380E8E2748611D4542CD869A9F207D09FB10A5CC6D206D4D631C962C98C5C2794A50B255C61E18BE1936835B486CAE6F39543CDB21A013C5669ADBD00701EDBC03986B59E9A0A019CB840A5E1A53EED55631989A8346B622D7EB1EA6C4EF0AF3D9624B3F87660B97A08DCD0E6D6D81DDFAC21046F8A9D3115A2AAC3C54E03E76CFE793A78D58B797020E82944502C5A96C61EA20BC2E6475E81D70F73D405D14496CB058FCE7387DF38FB3411B0F35AC5C1C9049DBC1996E1D4EE8AB3845238CDE2CE849D05DF00D0AA2FE3CB705746173E7DADAC706EC821A227055D4A88E28E348397FDD17BC96C2D1E4B8607B41433DBA0F5FDDF8F43F539FD30C28A454D227831552F970B1D5D405EDA9CC8CEAF148F84A9F7A45109E4F038FCBA705AF18EB70D06D1B5A7265F4C16CEE6B20E7A3B84E456EACED1F32C7C2A9CC704A764B68FF6C4114B4AAC8E731FAA4936241B514BCE7376C22A04937CB46B630F71AFD4CCF3D10E0C210908EC34850A735180A4D0884E026443A1B41ABDEB0744630485149724972B51F0FF547E6D10AD2D4DEF3F129CB649F49AA969AF031CA208DF969691F3B94B5EE6CF7B38470384B3143BA100558E2A9C8F2D9CD4EC6C2CC36C824AD5512B58C69523416CB12FC90D665C1381825C7451C4389496E71D0CC587D3EBE144FC960E51AA55CE239611CEDF8E423634FEE720465048711DA27CD476B6C81CFCCC43926B2FCF07ABD8FC7356788BD0A9E168EB116DE42D72EAE71F2703B8AFA7915C3337E40367E4EEE1F18165E7DE1E7DE655269B1BC2063DA00EEEFDA2DF7965FED8355193F89A88B0745237527D323BBFDF4E3E4D87AF19CE138093D949191029ED05C5CE40D020B6C3D7BDD15B47508AA001F2802B3A0CA612F51CAD9B84A3F68F29A285534B7915F40B88E2B76AD0755B37C5B7D8D4A3FF14E07492ABA39A63B957C7739A853F7AAC1E42EC8EF112CA2404761BA51A968F8D6D54587F5B5660DA83FA2AF2986C3FD9481CF6D96C076F07A15019898D50099C177A540227B033E01FB2A121C039FD83F5168FAF749874B7B20951ED085A3D20A1027F7E1D2491785BA05DF16D7C59031D41CD81D5673846BFF5862CB9C6B975BB6058761E3D39C08D33E298E7B3035DFD514A6A24700A84B0E6C01D02A105B5DB1876AE6BB187E2524E40345429E15F7BFF381EBE1D8067EB52A1CAC64E07E7E92CE4C18397BD50AE44383F8243743AB38856E7B374486CE94A1717447346BA41DF4A9FF1232FF7F94DFE32C74417D3B1D6686C01D1A081E715E9EDA1EF83FA1E998393EF3DD8FBEDB8B6AB9B95F4940A61508C4ABBB459A16AAC00993C6825A75537985A9ED8E9BC8B81B3EEFFDE4376524725899A55C6CE90D9869746BD7DD50B09510C9F9F42F88C83B8D1C8854A9473DD3818E6EAD1CB0CC54B9A1CAB2D303C809F31D23F6365EA7ED5156F81E7048AAF942C63207A6B383AE11BE8289C8711CE1EC209FF8534A4E651A9AC8264B71D4B94654529CFBD4A6BC96AD9498AFD8DEF8A04AA3B3999049A45384D665BB97879A1550AA44211AAF0D240EAA8B76FFA5880C6A11382736AC456864EF801AC2DACD94FA8136A26C0B00ED72929ABDA1F96DCB52F846A7389C4E8BE9CD3645AC1C420771E0B504E71BDE4BE50C985C01964F9B8FF94EEA71DFE0BBA5560A77920299286580BA705D56B6F2388AE5DF8A45D682015EA05C92438A1DF792270DAB456D919E19C7F9905D843AD12091AE18CE113098A70CEB9D9522D17D5923BDBD6304A535C45C842A885CD9AFBAF4EA22B7E596DDECE49D3A57B7BCD9F39FB34E575709C5826620BA729F0462A163FA5D01BDB6C08FE29BC52E00C177A3976B287D58AB1279B4ED88B98F12BC4514BD335F9971DEF4E8D1CEDFD4E70425B1AFB9D4B132C5963E9F81A03673842CA14B0C4FC76FC7E80865F90B2782041CF4C669B4D9F88AB607EBA1C2F83CD692C2BB6BE32B914B074B39FC081369EC7A909A2F5CA236A16C46136E413A27401E828C6CE380B8F58EEC6DB34CC949DCE186238919A17199CFACDABC653D3E3DA0853311EDFDE886D7B1702673FC0F9368A2DCDD96658CE1D9C103BA36DFBB2877A1BE00C6F0B7062014A8806BD65DBAF72D37E260F5A0B0B995E6B13470BA9ACFB3B1548D767ADDD93166C91DC980170A2BB35216AE71F26B65C19655812BA87E0F341ECDC7DB40BAB2D48BAF3DA5F9576A9764151695B1F442DB4368E4AAD82EDEBFEAB1E44C01C4E813087F30BC0F9AAD7FBED28837342709E319C7C90A3EBCF6F2D705E329C2B93163177FD1B1D232D9CE527FD7BF1774D3E8EE0F922411BCD6F0F53C3CFEDA2C0C9ADC2B9BB0BD91FF7ECD619A22CB99007612A54A504F569B95A7D4672515A3A6E99DDF13442A0A68133EAED7F014A7A6935098E98960000000049454E44AE426082">
        <commentlist/>
        <creator name="" timeCreated="2011-11-16T19:20:27.711+0100" uniqueID="8511aa2b-91a3-4aa4-bb44-961b7e8cfcac">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.343+0100" uniqueID="de51d35d-e3d7-41e3-bef7-71578e12359b" toElement="dd13c195-85ce-4025-a378-855e767dec07" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.361+0100" uniqueID="84c0a036-25c4-47fb-8190-6f5cf237481e" toElement="2c9f2b17-8a5c-4c62-984f-e38e8c81f5bd" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.382+0100" uniqueID="d6fd9afc-0efe-4582-b4d9-1d5d37321804" toElement="dbd8acb9-23a3-4219-bcd5-a99fa9f1b11e" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.401+0100" uniqueID="c90c941e-568e-46b1-a7fb-10771d48da1a" toElement="0e0e51aa-cd72-49af-8997-11a8be18c50c" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.418+0100" uniqueID="a9e0d365-0fe5-4e8b-afb9-231a048d0cbb" toElement="28a2841e-e1b9-4cc5-9a98-3d2c9bd896f5" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.438+0100" uniqueID="3ceb6f07-d640-4dcc-827e-bc984d9b9d08" toElement="eaa2fee8-e318-4753-96f3-01b3fcec95cc" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:15.704+0100" uniqueID="5778d643-c6a0-4db9-b66e-ac6ddbfddfd2" toElement="bc51e4b1-e551-45a5-8df9-c9e640e92434" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.174+0100" uniqueID="d398d23b-9ab8-4938-95a1-af9d988f2536" toElement="1a423134-72a7-48fd-b5d8-26ad25900512" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.203+0100" uniqueID="3039de15-27b3-4542-81bf-736169a9cb4c" toElement="6b7a24d7-6ef6-4bf8-9c9c-b3ef3c4410d6" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.302+0100" uniqueID="45338a52-a34a-4222-8d13-a91f80426612" toElement="a027a898-0f20-473e-a0aa-25ad6bb7665f" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T19:20:27.715+0100" uniqueID="5235a289-a772-42a3-a1f7-5c35c7fe066e">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <storyboards timeCreated="2015-01-28T20:44:29.306+0100" uniqueID="ae452f00-dd1c-4fca-89d6-4ffc1ec84c6b">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <introduction>
            <fragments xsi:type="text:FormattedText" text=""/>
          </introduction>
          <panels timeCreated="2015-01-28T20:44:29.306+0100" uniqueID="4e9178a0-1238-4373-842f-d7677067ae41" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.306+0100" uniqueID="e3abfa10-31a3-4624-9278-cc7101eb9e8d" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.306+0100" uniqueID="4f642f04-0212-45fe-941d-423ef3693486" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.306+0100" uniqueID="e127e0dc-ed16-4094-90dc-3ccc042e36c1" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.306+0100" uniqueID="8dc02edf-3915-47bc-ae97-3ce88ba02686" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.306+0100" uniqueID="16acbea2-6a02-4b3d-83df-5f2afb5cd0eb" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <conclusion>
            <fragments xsi:type="text:FormattedText" text=""/>
          </conclusion>
        </storyboards>
        <capabilities>
          <fragments xsi:type="text:FormattedText" text=""/>
        </capabilities>
        <constraints>
          <fragments xsi:type="text:FormattedText" text=""/>
        </constraints>
      </contents>
      <contents xsi:type="persona:Persona" label="" name="Thuri Andersson" elementKind="unspecified" description="" timeCreated="2011-11-16T19:25:57.104+0100" lastModified="2017-01-17T19:34:08.002+0100" uniqueID="feedbc44-2caf-41d8-a2e7-9e2810518ee0" workPackage="" age="27" job="Librarian" pictureData="89504E470D0A1A0A0000000D494844520000009A000000E8080200000098776A8A00008D7D49444154789CA49DBF6B1B4BDBFEF327BC658AA73881533C861411A488204D042E224861810B0B5218E1C208176671611637667161161746B8308B0B835C18D685615D18E4C6201706A50828C501A538858AA770F1142EDE22DFEBBAEF99D9D91F4A72DEEF61D8B3967FC4D667AFFBD7DC33F322398A380EA3C121AFC9619C1CC5C3E3787832189E0E523BF8A18E630E7C0DC6E0301E1CC4F17E24238EF7227B1FE52FCA4D7284EF4DB2F3747C954D6EC7B387C9FCCBECE9DBFCE9AFF9F3F7A7E7F9D38FFF3CFF787AFEF1DFE71FCF3F38FED78EE7BAF1BFC5F1C35E6BFFFBDF055FFFBF75FFCA7F7FF077C0F5E979E1F88F8C397FEDE7BF65E04FF83E7FE6DF2257BDD117FF365FF60343BF656EFFDEDF1CF3E7670EEF7B75FCEDFEF5FCDF7DA12C15E7E0A088F3E437701EC6F181628B2B2C2D4EFE58E24CCF8723C139BD07CEA9C539AFC1B968FCEF029C3FFFAF4A74114E47F49FE2FC6B5E1806E793C3F9FCDB389F4B1FD6E1748F481D4E55E7413D4ED168E2E34C4A383D1516EF1D637ED9F03849CF86A3CB6C7C3302CED9C314029D7F9DE5380DD19FE25CA4CEFF1F96BF89B3FC2ECBDBEADED032CBB961E90F2B2CE0F97D753A963F4A8FC5DF35FFFA0B5F9A394E121DA8BD5D88F3D01ADB83F8A7EA8C0527BF2B3D1D6617E9F87A6404FA38158D82A8FC9DFFB16F9C9ABB45A396E83F32B6FF1467BD015CC0B224D0BFFF99346B59FE98979F8C1FFE87FACF7DE37831A8E23CF4705A0FBA509D8AF3A0CA32CEC701BF4CDDA70A7472339EDED183CE40F4EB8C26F7EFA2469FFE39D112C2DF6759E33B17B39C1B675623CD6FDED5976941A0B0A5FFC471FEC74AD3435860F93D67F9F475469C8EE560DFB7B71EC29FE02C08B460604B382150DA5B08F43C15A22310B54E74669CE8DF56A6FFA933778B88FEA823FA7F60F94B9CF38A5CBE17C4918F4544F9BDF24C1425587DB184B36CB74DFCE5FEF519580ACE230F67ADBD2D0DC55915A8897AE202486F0C0EAD402D515A5DF1A324FAB548B46A91AA5C7F22D0DF5167AD577638FF5B6699C723BE3EF40DFD56376AADAEE0949FF36C3C681EE0E4814F21DAAAC5F9BD6815449A4F5F549D4E9A469D91B5B77544739C8301C6E1607050F2A0712D541568726C89C2EA5E5899AA1FC52F5494E9F34FB8FEA6C9FD4D8A39CB7A9CB090F5EFECF722C2AF462515A28580288F4EF3E0A8300A2F2ED265D1CC5A9CC2D2F9BCA2BD5D8CF3C80C55678E3377A571C9DE1AA24762722DD1EC5C5CE9ADF5A35F0CD4E7BF3C1B65FEBCDF23FA4B1BEB273C1CCF39CBAA3A9F729C3F4A2149C5CC3E5996F538AB81EEDF0B629C0ADAF203F457AECB276B66C1923899352ACEBD30DE0FFD80C878D0E3324E9F680DCE05EA74B07D8DA624CA5857FD6811EA3C875A0A084B386B03DD7F2ACD9FFACEF2DBEA279A4590F5445D1AEA46557375982B2C0B36B6F02F1A9CFB96A50C556721205A84B396E841E1BEDE89DAC848BF1D62956A91642F77C588F79B87F33F4F351EF41FB1FC25CE4571D0A23CCF1747612C8E894A507F8F6EE19F5E84F3ABC149511670EEFF14E7491D4E4BD41F0B71EE17BF127EDAD5FF6E0C54A9026A4A6A709603A245D1D0EFDAD8C5382B1967C1877936B656946575FA443916E0FC4DA2DFEB1EA66F522ED544A584D309F467384F4AF67621D1C1A298A838F04352898C94E8EFE1FC27EAFC59E0531ABF5700B26F9F93E3CF707E2BE22C8DDFC769593EB95FC081F470468A339251C2F93B447FC2B21E6785A8E2A440111649CD6856CA474BC6F69715A2DF31B0FE0FF96F519A855CF3A91E67D1BA3AEFF52B9CF39C621DCB5F05B46459C26902B15C9DCA72974302A2DFC29958933BF08956B816EB7FE25CAB023D8CD3D3244F465D0950A3DCBFE7C512E06FB3AC35B0856CA418CA56A2D9458165459A3F8D860A507FA1CB1FF34A085DF1D94F7F3945CAFB93AB735E87734FE3DBC5C16D11672ED03A99AA1C5D9D68E070F21A29CBDCD8822543DCF1ECD1D416F807B85251C96BFE0ECB7AEB5A0AA99EEB8BB4D53CC1160D2CCEDF769FDF16FBCE0AC88544BD1445881A69FAF6D6E2DC2DE22C0874F00BA27E3DA102359F542980B466F6D0FC7CE2E4DC994CB63C3A4B3BABA9FCFDB7487411D79FD707168F67CFCC9663DABFF31AD04F41CE1DCE3CCAADC5590266FFC59FE174E6A1E4387375EE7B3817D9DBAAEFE4484C4074E8054445ABEB8C6D4E71DFB194EF3D36A579C4417910E4D5FC2AA5F9E73CEB7F2E1AD59F473D3F9F24A9164B6D25A8B6A45727D0B97793532F087471346B4A25F9B550763771D07735D739C2E71A9C9E3A7FCF7D2632BC74A58AB3C6775AAE96E55075CF7A021CA787F3AB8B69E785024235A6FD4715BB7F88B34622C5EA4125BE5DE032EDFB5EC059573AF851ACEFFF28FDD3DF7D69D613ADE0DCCB7172EEAC5E9DBFC25936B685C9EDDCC6CACFB115F974AC290A2CAD46407EE5B6AACE5FC6ABD51E918AF80A73907ED5C9377A8B7016A0CE9E4A425C5491FF19CBA7FAD9CDEF2E0C2E95F7EC53E28D22CE9D823A07569DC9AF7CE76FE08C6D41D8CE7DC25FAA2E5DC699379D9808C84AB3E2387D726584BFD2E2DCCD5916BC94EFB4FCFAB8AAE447E9CDCDEB3B75B363FF1C6729142A2BB238435E92A68F7601CEBDBADAD0E272FC607131212E85B5073A3D6E6D6CA97AF0682DED5FCE6B16713E3D57B4B838C0F16613CDC4D3BCD87A5332680BA638EAA292A26EEA24EB9BD9DF31B6B946EB0CAC3F3DAE6EB2A04EFB4FD7E054A285E0B60667D1D81ED5ABD3EF5290F2A10B658DBF742C559AB9E39438E8878FB3DA77F2FCD31EADE28473354FAF815496A663BFA0C9C30F6A16E334EF7B29B2ADC35913CA9626354BAEFAB7705673950538CB532BD5382857A74B31A9F5AA2EFDE914936EFA38CB69E2C222CE73D1AE3EFB5D2075A9FA8F3A9C76E4AD24CF7E2F5DCD9BFE1B384BF6761154F7BA63F9B334B72C500FE78EAF4E636C7DF759855A5388AFC11979D1AC6B4830A16C796ACC3683998CD3F9CE7297499D5FB4B1CCF3DC77903538178DBC6E506AE799E73FB386E8F7729052F267FE4DA1C857855A8D7A5CF5C7AF2316AA898567C5E69D241A443B81C3598D86CA386BE7556AB294A8909C486662A4799D17DCC1D2CC747A387FF8EEF3694103511E8BBA88B460246B71FE285174338B759D76C5E7A3345956D3E7F12B9CF3729DAF605A8B33A9DF8A73700B703A8D16CA08E14EA53664E7B11DD1E427EAACD46CEB124DDB597299AACB9CD90E4D330ADD089E407DC53C7957BF5BA0D05FA3002CDD5A2D5A03FB3C2FC73E35DD59D5894F87A194B77CCD6D6C7594E326A7C592229D69157873194FF6EA119DBBAA4539EF0CA1CEDC7D46A5624241A68B12953AC739707D5FBE343533B933E5D979EEDE6DD2F93D276A7156A2DCC51500CF112EEC56ADC5691F8BBCB5CE8B8A7F8A33D7A53F0799976C3478A905EC57761CC5B947911FAA33B2A32050CBB5A4CEC0779FB9BDCD890EF2126EB504BF3808D27A9E6DCC1C8EA5E38BD5F63B6D879F3A904FFEDCFDDF739D4EC963D12AD7621CF4ECCD507A11CD8250B626FC298EB207ADFB21B575A2129BA261F43EEB46D996CE737EE6E6C95ECD28CEC7E9288442C0294483C2D48AA02A68F43770961CA7EB2391B669F19AA69564A245DADCC63A33CB37CBB3B7F35A90BFDB82FC5CAABE168DAD5F3DF8E19E1BBF7CEA4A36D540B45A24FA5969BEC84FC6FC4B418B8E5F793CD891D32DBBD21CA7B00C7554059A433D8C5D1B9F57432856820E74524C70EE397526A60B5E9313C5A9B966A92B33C759348316676D23F9F3827BBF9E5E9B6B7A06B6A2CB9229AE492A16D4FCEAB015221A8FE5BCA0C505202B38E7BE46ED78A17D5F8C6C559DDB81C9583CF7E926263D99167B33AB96F6A08A339F39C9734D3B1D56888004E70FCFB1FDA8AAB336D0F563A56AB8544152AED9566DEC2F58D6A79B795E510BD27787662CC6F6B060D451B7AD5FCED86E13A7B3B791146F75455F81E85181684997C5DECC489E09B3A221C7A941502ECDA98980DCA4981FA428429780D6A62BBF9AC25CD8F7ECA9D388BE16674DE9A05847ADE688158A4FBE39D530BE6A48FF0FE33107ECE3342C8D40BDF8B6AAD18135B985925E4D87ADE084404BEABCAD76D51627387D5DFAE14FAD1C5D61C15D4BFD22B6A1CB37B98544B35A3AF07156E71D2DBF5255B690267EA98436BE108B20671CF286B87B5D336907D339F7A17F5F1C75EA24D4C2DC675E772D122D97678BF327663DB60E5343C8719659627CAF0B7CE6CE59D6E12CC0F32AF2CFB6A25B2CD03F7B822BF9C51F45A8F9F4593527F1C9157292821CCB014E9D839C3996EA77FCF150C4F930993BCC75632E9FAA44B6CEDE9AE9CFE8D744EB3BF6A29CA515A85BB12B38CD725D4F97F3E782F75A20CAFFCA7BFDB4408E4A519B136A7BBA7C8A3596A0F205F58582C53D2539C5695E1EF1EB245AFFF255E8C69D370C24F795D3C297956EEC78612B7C1AD31A631B7AF55B37A1ED118D73A20B1B300DC5C88E581B304F8953AA073536B6A0CB45BEF0BF42FA59A13E15EAB72A4ADE3F69AF097F60B9ADCBC2FB4FEE9ECB86B730E95889746A59FAAF7F99F9F00CCB4703B286A203091F8407FDAE865359C125F0F6DEF4D9FA796705A727321FE741D1D2D6E18C7C9CB2E185E29C7838FD62DEB3AFCB9AF2EC538E1330BE23749AFD50A80EA76817EF97AE254290558C892A8D55CE3B16A6C6EAD2CA45DD24B9BDADE0AC049F82B3CE5ADE4D90824F05A721BAC0A8D6E03463AC387D63EB8DDD8538E3834A5A52B77821F2D429DFCEF8D6C799F777E511AC2D0E94264FE4C52704C07FCDA6B7D9F41181317EC868FA653CBA4EF1E1E87430BB4E9FA52917E3E9EF390C00BE65F6387E760F81256A629CBF0B422C14138CB3AC36735445E9F9CE12CEBAF43117D99D41E8783896B9408B86B7C6BA56A0DABCF31FE12C12AD898024082A4853D295843813676C8DA5ADEDEF2A8EA7AF93F1E9203988C697C3D1051E88E1F4CB24BB40E6839F190D8FA2F4384A4FE2EC3C99DC65B8E2CBA6F7486D47D959F2F40D429930917DB2BA2F4E62D788D2557F4CEC335B0C72568889F2CAC0B4904DDA7046315088B2B8CAD4526E8BE306D7490D54DFB45670EAF77AC676B7C0F217386D4BBBB7A033F259C62596524C3091EDF568FAE06D449317D98BC16ACE720A2AE16657972C869BBDDE6A2758EBE226DAECC5BBFD70A3DBFFDCE158EBE0C5C15E30D80F92FD70781C8FAED2F1DD083A864CA7B7A3D9FDE859CC3B7FB2562AFE63EB4D55AFF9B7EB9C2BD02AB2AC21EDE32C241E7716E46D1D427F98D727F65AA15BC4E97E8EC5B92708ABD2149CD1229CE544D36D169513356584035D2626598A9566EE35AB2CB5094FA0CE6EB3F4182A0C010912D47D8EF8E1A13E88C892FBFDF56E6FAD83116C740360DEEE73ADEA5E08EDA62703E81536797C354CCF06D0345E498F62EDB53759D03CE75A08686D8A99AFD5AAB5B10533AB2C67166751911EC2C98D0CDE8CB832E7466E70C5E3EE43B5F7B5E14FE9B128E2548D16A5B918676587923254D78629F362176659AE69BD2CF4EA1503D76759140D67F96D3ABB1B811C2418EF07F16E80AB5D1AA51FE23EE86F74A1D7DE6ABBFBA985D1FBD40AB77AFDCFDDC13EA76CA1D778BB0F830CB1826572100E0E4270C53301ED8E2E53CA547CB3CCDECC8D3ADD0C5D49A0DF4A66B6E435A7A5440538A7C530C7503408EDB82E8F5ABD960CEFB41E2781E520C322C832CEBD8ABDDD2F5A5AEFDE4C73CA7222B2BC752BFDEC665FF3922E7F68E0CAA8072C1FC7708AB4B10730B3C1E0304C8E635EAD2EC3ED3EE4D85B69F3BAD6E92C37DB1F9A9D65810A832C328DB6FAE0975DC2300C09F5284A28DC009E98FB259D2570B196652551F1046A64BA08ED57E6D0C5A281E0BCCF71164056C79537AE9D5E392CCEC52C2D758D6C7F8D33AA57A7BF835B6511E721A7D252598A3BBAC8B7B4F05BA2EB581227120CC43B90E000EFFE299E890814A39D1E3C68B04E37892B6805EB34AD1022886274573ADD4FED0E58AE75BAABF8B22E141C6DF5B273DA5898DCF424A2A6F9621FA11F7FBD8BE1E47E3CFF36FD613DA8B1FF45F7E98816FB5D8B385DAE994B5370DE1B333BB9B5386FEA298E2F331D3951333CDB2B09496EBA6F8A38236FF9D8229C0B4221539D2FAE59300ED56D3B93E9C66DC6CCDA4DA1549A25906266F1D6201005008044E00A31C1658262FF73BBBF4ED7C89047890227C6460F1150F7232C2D07EE6978D7BA5D5E1919011EAD2B3D2E1D70443BDC09B7FA7C56F623B800109D3F8E9F24853533AC763EC77CE8F72E7B46F8C977AB0575BA20485916BCE6A420D3B1126517DC656194C55A1B25DD94C78B9CE5822C8543C845352CE36AC6E2F5B9276E999F5D7C223D5D7F17F75474B55664839C609924C75172C2C4835E733F90E8A64B73BADA1607D9EE519DDDFE9AB84CE147517E6C419D1DDEB4A14B7C16577C59B4139858F788C151CC1BD986F08063783A801DA629BE49A1D1E7EF331B0DCDCB1EF4BBF7E1DFBE5B9D2F886927569A45C378E7795087F3D24973E44681E89547B476A8EF348ADCA90C4374A1A5ADA9C5E7CBC774073E097FF21D4FC5CC3A1B9B2F4F30D1ECEC7E0C9749259D534960402DAE77C39D7EB8059C9DAEF84828B2F3A1D5DFE80960F8CB3685C89B1658065B7D0C2D3B079BFD68CFEDC5825028C283829F6FC22844BF244D7F9C1CC5A3EBECF92F98CD29A5E95CE9DF95B0C8B6BCE4698C2F50274DBFFAE3D429DD170557EAD9DBA23A0B1F4E14EDB5097D2BF94C6E755F98C21EFEF86D0E37A39213F5045ACB32EF4C70AD7EDAE2A57B06E9FE986A66BFFBBA14907609DFE42A9D5C677090604993B84D5AC84098566E09D4AD1E6E30341BA169852857687541B70D45AED3892238822155EAFC4380764BEC2DA3DC8031D41EC5CA38993D694C6481333D6717787A964C6E321692BECF6C34342FDBDBEFC5579C13FD05CEBC0064F4EAA25C4BB46A6F7DA85E7C34AAC779EBE3DC36388305387D75163B13FC66B044E7A86D63B44A538B06A63C9B873FFE4ADBA7E7D1D5106FEE90518F18580438C0B9DB27571A4C442E040363ABA12CA31E063B1AFBD0E40227F313B1BDC00FF9E24F8067A5FBA44621CA3ECD389D716886EEC87C849C3806CBEC6238BE869B4FE75F27CF3E4B2D1BF9BEF397388B757645E8D2FF3C382AE01C154539AAC729394CC195DA7BBCF88208B78AC3CE91599CA579EC024BDB482D144FE92C65D30A6E746A22A07BAF3CEB5B5AB3D216FFFB31B94E412E39800D243620C1878C80B67A01B4B5CFE8942CD718E9D03B22E45957A8F4944C48B67A484E606FDBCBBC7645ACB0280295E929F7DDDD65C60203008152F7E64AAE628A6597335ADD149971B9FDEC7B45A39522C3FCAB573A583C3199FBD47B967B72A2CEEA3AB4C6029BFB42B87B5350AAFB212FD4D9F437FB7DDE7844B5132C276A71FA36D6B13CA51C15A4ECB4673ADCD97AE96F29A33D1C4FCED87220388A363BF10E9074A9C54D1A4CE071C616F9096E9099001E14A92E53635740C5B7F43EB5DBEF5BEDF7CDD67BE69DED0F8889DAE25C2971FE34494EA045A6A13B7D46460761AE5171A5095D2CEB829029FCE89C26775ED068D5EA7E2F4AD3D9D89FE034FD065EA9C8D768396F298445E36A625AC95C5F1024C646DFDC6C92AEC569DB12F642676FFD95436EEDB451A4B2840F30ED23B625DA2BB5FFF0635AC1F9FC751A6E020CA2981E0417ED212D617E0939D2C0F2BE073C3DDA4FA62B12F8747BC829F7FA7DA873851E14089BEF0427D4892F16DBABA12FBE5D5D2F68E1876BB68A8700DF4E8A608C34741BC6200C998C06C3B3042F8EE144BF8CB54EB410A75755D04E03DB14B20067A977448922E5B8A910BD2E87B5B9407F3A5E106471042A53F5A3AE1C6F053A7039A5D3A5B3AECAD26C6B9A47B379A25964F98CEB7C9EEC077D78C44DC63B7496BB41B8A12CA9D480C5589A5300EE2C37A139108A4062BB8798161F763FB65AEF696341145753B99594941A858857DAA1A6A79F3BE0678A0F88953E7700156680DE14DE64476CEF011F5F46C2FBA1EE815370961AE596B2144954F8D4BAC92F2BD0B90CEFDE9B5AC905EA9296BC6890E3AC702D18E48A945FF4D67B187D5E2D510874B35F36B98A533B4B8EFCA5F0DCB05F57C38B8D1D31F6C9F7D52BECDC554837F1D1ED28DEECC8DB8D88A625FC7AB0BA5489A893761529FF66B727D964C0577A70B1C19AE80FF1ED47526CBD6B764CC6D9826A459AF0AC92927E6A295A568EF01C88E165FD6FBDAB03AF0476128681EE01735304BAC9C9607C95D6384BD798FFB7B7DEAF84533B141FA6661D95CE74161ABDBCA0F7D64B40AF6D425252EAF5B8C6141746A681F18BDEE79E1996AB212A1A2DC644B9EF4C8A665677A6350B6F756320FCC65F04A7BF998CE73BF162E743A3BDDC004B383CB0041EBCA7D4289CE84E4F934EBCDDDDD5362041B8105972D0EF2EB7C0159F82B30442B0D4DA5EF35D035F260699590A7032CAC5B3B2416C8C8157F94A24990C22642D32E0F9A028A5022C1E94EE7320936B485D00A95C0F62286B96D0B82D11CC02129FD343A103C1F507955A84A6F5388B25C06BF9ACF91AF795E332D4CB1C67D7879A6BD438516F19AF5BA150C099EA122259AAA0FDD0BED7CC27C2F29EAEA7E7F428A2BF5C67744A54DB307A21E320578C95371AEF3B3407AB281323086E3BEA505B6F1BF846106D7F6C016DEB03BD66206E922521D68CC89221152B441D7DBDA78657EFA5C614886127489ADC00190B03DDE328659977387D9CE8EA441D79F1D60F82BED4E2344B1C67059C65B7CAF7AA60664715A896A53F7CAEF538D7EC7044ADC90DB7737B5B83D3E524EA358B38F385B7AE815658E24F02CEEEA766976F68072C872751F0B90D271A5A9678912502B094A413920DE0EAA4AA8090A723CE928A2436EA9BC5BFB5AE5C3B8192034BF85D7CB8D60ED739DDAD5969288EB3CF2F639C1C6EF4D40C8025AF266389A0CEC9FD68FE255FDF5F9E1DF3FB484A56F47EE25A67E7BE13AD3607558BF28E622DCB1254DFF6D27756711AA2BDBE87335FB5A26BCADC1EE0CED25EF38094895B79525DAAE01ADBF1E17C1E6F8A3ED6DAB0ABD065B8C177B9BFC9C8454315088B4658841B4894040D81505BC3D78FEDF672532B066D91664FEAB44C498525BE1D0E7800D3BADE196C76A3CF6D883BDEEE53DF78623ED3ADCAE003A4EA07D7A114FC54AC2927BDB3D9833E9A13D7AA2F2926FD886B1FC1238BFBFA80B6366FF18AF293DB92E6463F4358265A103488BE104B2B14CB022DE02CBBCFA35C9D2ED12CE034EBC2FCBDD84C10C117BF4E7A2BCDDE7A27DA052A269D52AB6BB23B64BBA7290A740639AA46593D3F6031A1F5AED17CB3048ACC49DE37E9FF36BA709F502A90EB8C0AB21768314666022BBAD91DEE07C94E2FDEE80C77FB03AA9C1AC5676987E14175BA5B9E9E505E4C0E39B90DA8D9D5907D65F723E03444CD5FA4B360046CBB81166426D5863C69B99BBA519860B1387D60B7C5A998D21CF8759968459D257B5B87D39FFFA2EF2CE0B4C6366F72F772151B1C26327319EFE1DA614976B5D55B69218AA1E993621EDD9B869D6B6D0885159C6DEAB2F596E14F17D2FCC8DA106C326B083265ADD36460896F812E1338E3DD3E58A666F4877BFC70804764BD0BAE832D20EF324886F785CDE753D58FA570C15EB25356FEE03E27B7198952A36364A202D58E470ED7075469D11BBBE1F32BDC175A4C2C21376B9D370AC9F0666626B5442DCE1A7B9BE7A0D5E0D6D4F964DAC4E17419A7F59D459CC6E44ADE320B3FC3CC22C081BD8532E0FC5A9DF70D490419E620CEA48793F21EDE68483392AE0328B22911102C2D73122911743E4807023E5C69D3F6AEB440089CC0120F4D76140EF7FBC03942747340AE781D5637825DDD0351A0A5D5350F01FE45C1895C0838D3534E7A8F056741A35F2744FBE872CAB1AFBFBC4BAF76787364F5386F7E89735283D3951116E15481E6D9A7B7E2339FE3444C84ECF3CC5568F3DDBF3DF76957568BC9C587B8061B12D37E84F343384A06ED8FCD40320AF1619426A25C3ABF7D966C58AA5D66E0A39D0630ADE0C759E8CD5EFB5D0338696941F46333C433B1D10548F8CBD171981D86D941383E1F8CCFE24C3F3C0C41345CEB50B80722D6358660F81D30600306C4190E0FA3EC4CE6586EC5E4A8401FC6468E74250292D21CEBC821D5CD75147459759C1569E638EF8AE3D6EF5329E7A3D677AE95EC6DD7E02C45435E679E29DE32CAF54FBAC9CCDF6FF7BED4A8C1F5EDC1F1806B7FB5196EB1B00751C2D2B6DE2CB159046E6FB533380811EE86DB7C4F29D03DA9D67E92BA8F6902A275053C8D4E1919BD6BF4D754642DF8C8C1265C639772DC0FC6A7F1E43C995E0D2797C9F83C1E9D44A3C3109F1AEE05709F29B40B8DD2FC76142795BD6BECEDF09844E13E616F69721F8081BDBB2A5311256DAF33A1F87B4D2BDE4D5E22F7FBBBA6154758D337647E82DF75302940AD55A7170A8916D77AC568C87AD0BC3CA4EED35367A916AFE739DAB38B6A6AB6062765CA69CB4FCDF6DBA50E7295E546E3CD124B3F482710F8D8A61068146F2B58B2D9E02353CCCE32131B5A5AC18940B4CB74859616372C00AD1267B2DD4D77FB30B3A038BD496777E9EC369DDE0C275796E849940AC578A39B1E47C95E1F115360354AA210F71E9F2736199D0F4697C3C94D46900FFCA3A4215B8383095FBC1FE9138CBF1AAF18424C1C333B2C511BCB141AF8B4A09337918C8AEA1C9771FE54A335EAECE6F656DCE7665016A8390EA53014AA9E0198EF7E50DAD2FDAFB9D689A2ED4EFF33D4D680D56DBD7ED5DF802ED95689F0B5FDBE81682861EDB42F2D421D7196B4AEED0F0D7CC82C4563E08D2E27AEDF35D88F89A4E563335A6B0DB73A40052D3A90F3BB8C37B8DEA6934B5A5D18615C87BB087DBB6272C344E8322CFAC40859BBB113B6F54629BEFE723882BE6F520D8B448E2336618B40255C00038BED3A732537535AF189D680345F890FBD2FFB95C9BDAB68B4EA3BBB1627C7A2F876CFEB122A7796D835B96A7B757F5ABBA1A912658AF6751AC3D2AEC16C22A46C35FE7C09B1C28FB2B34BEA064C04B728CDBED4669525673197499151CFA7362361EDC48443FD4096B0B1D9419FD6F56230B32C89F33E9BDF8F70C52BD3EB6472118F4FA331340A5D6E77A1CB0C66F9880E7540DBD0A603968E4EB8EDE12163229A5C3602B28AAB6B25408886F75635673E5472F9FCF35511E77591A5EB40509057A5AFF935CB224E531B7A514E3A2DCE9A7465DBC3991F59E4AF0C1CB85E93A16ED376ED7596686145FC68B42938917AAE34DBCB4D04B79DE506DC64FB6D43274F62E943D0923AD5098BFA1E5FDCEAAEB080009C400B5F8B34B483186AA505458210504DAF92E9F5D003E90D00BE19CEAE92890AF484C151B8D6627C7414695884304ACB0B0CB391ED1C8414289C28A08AE11D5F72190CA0D2FC32FCB148688D0C481A278753888E4B44ABF5BC426F6DA1B6B7C0D8162DADADF339DF5960D9B5216EBF1010994D4DFCA564D5E182DEA1AC2E2AF40A8934E70F63096B1BBD4F0D384E68147E1457206CC2894AB6805C9EB32530A19F59E88105A604D5EA2E3366A17166E341335C6D66FB6409FB09960466418AA595FB3BC1798B4F0D67D7C91426F73422D1D3189A86C91D9D4619E3A33EE2235AF8352915C96F82407778183A8DC2EAE2CAE0E84610CA021882BC32AF14CDECA8DECC2E1ABE99BDB6EAAC91E6A49AA8944AF0659C4581068580C875825571BA9EDB833839B6DBCEF851EE97E9F43A83E3044EC4B79D774B48403B708A8C445A94DAE7CE405243063834AA1D55274503754AD4C3986515316D235A6FA77B3DC0989C5B5DDE0C0B088D2EB3F9036E5223D0EB210CF2F8381A1F851068F4B99D1D061A25C189469220992211D3D08061D17E9F81AEA8734C3F9AD1958A8DC5BD89E76F321D6365736B73B63C942D422DA1BD71819267697FEE326D10E497E02B2C57EBEDAD1710957AFB4ADB839B17C5E4CAE9C8DE6C36C6E83C093E33A0EDAF341907AD210842D2D946A0CB9868AD3D90360384AC4C3125F061EC4384B20AE523C3DA3E8BAECD74A707069333659918963A1E2CCB077B95D7C50E0FE944C10F448FA378BD3DDCEB0127F49D4A56CAD9569909D0C996488B7F7BCC5EA051810A3F82C8284534A48A24C85BE34DF1C71A90A62D48E261BFB0775391EC6DA19E97E7A9AEABE8BE12D07AF5A09FE23444ED9499F69D6C05A58CC5ADA9CEC79EED88379BCFC4F9E9C86E97D387C9E862187C6E89A55D6ABD7D05C709B4C1E7B6669FD10E67B09981BCE76C494B221D064A9245C06542CA1D116882D887663644BC0AC1C18AC29C9299B1B1E99322BC4F7DA23335B937C3A938D1316B0BC160A30D898F91939C46887523A6497CB03849F089FF34FC347E2B1285A385D515A26A75A79A67DFDA88F74ED2532D73DA5143D4E636A5EEF8B2224B38AB1D9D2EAA82CFBEC8CA898A61B99ABBCFCA7C59E8132D2C4512C9BA46CE81DD0CC109948F2D63BF61B401FBB9D4FDD068BF5B0248A89369A86493EC466064C4322C9C257182EB4A5BD34A17CA3284D9EF8F8E82F15934BD02CE84E326012A42BD4FCDB8B3EEF33E7B127BAB2C89934E34811305D498015140C09783743F80B5D71A05035D96F565AC704E0644D3931050C760799D822B89CAFA51DDF97CF6E86DB02357D70F36752D99C5C2506E3FCBF523F32DD56E7A55A737773D1A5F147192DFAA8773B5626F4D49C1DBE8ADB22CA9B81982EC0EA54B01AF7443C511CDD4551AAC811659B6DF2FB1FEBE2A9AFB48A31A6F331221B6E5264C2BA7367D9C1F0CCE18D23C14692ACE2B8373AE385594F799C309964FD67D1AAB6B053A8122777AF0C153321E223E02AD480222AA53FE69350CE17A87C9E8317122D6A540596190ECE57E2C91812BD04FFDE1E6C88C4CEF7D997ABEB0C4B224D09A39B57CEE1A2C47E76901A70F32CF3E696F3D0F5A68AA2E22DCF5AE9E4019E25E9836A2EC94660A41104259E0EC4A55285C87A78469E5DB176D89343949D234E5757C4A585A9C4B786799999C86D4D67934BD1475DE08C81B8BF3CE252A62757D9C2E6FB915A2A7D1F46230586F8B1D66FD6804BDEEB0F2C799D7CFAC0677352EFBD41AEE11A7408D28CD9B4C16EE4B15DECCD8331333B3A136FACB357A5F90E9A4EC47170AB4BC6EE9262F101A9C48F4CB387DA2066A5E1E321E743B08F2752C02AF66483EB31F794B3CE16918326467097276F0EBAF110CF24EC644AB4DC89465DB8D8EA245C602B1B6DE2E91DFDBA560B51DAE8AE35C16B17E6C8E8E424AF33C169CF1F4229ADD2686A5F1A0438350AE4F8F238C3C32B2112F1E0266ABC0B9D14128C4F4E61EC6331B1E84F002D2D5C06E074EC3BD6D0C3E772767035680AF92E101894E4CFD6FECCFEFAA4CF3596EDB51ED4F650B24C159C955EA4BF6A5C2FDAD1FD622D37538D72A616D0D4E06443D8F68716592411816961446614E342F2C64674398D38855BD26DD27D409F189E145BC43CC2B946393201BC4B9CC6406BAEC7F327AC51703AD9A59E6278001758AFB9C33E9F4700A3C0549960F7690ABB967A9087810DAC0C6EE74E55BF83A5C63BC65BA8AB42525F8D89A9CE21F1A22BD991C05C96E0FEA4434044B3BBE18CAE682DEE9695FBD1D7A1DCEC7A2D5BDABABC2FBC5FAEA2C696955A1C509C729BE53D5B95631B6BEC975F1AD2F50EB414D236E75015AD1BF9A40F778C004EE300CD91664712E2F75DE23C45D627C0B9F2AF7888000B8F9FA152DED8706906B4CCBAFF9D88C37DA884825D71C509A2E1432B98A84420FE2321F05DEE398E3612CA8E4E6716CD0DEAB4099B3C69F5BAC3CE0EBBFC0BD65C95E6037D1E0845DB2D59F5FB2A8344692BAD385E71E221683EB45027A93D9346CA2BA7C32F3485E3F4A5D4C941BDB5AA2B59B62F8F6D6E154DF7961D569E2A0EA7038D77393AB0151B0F36BA291B7DA50425F7A9D640F76AC156D7640AEFBB181D12642920BD65A78B1F9E6554BA2245C3BA2DD8EDCB0CEF7B109BAE96E4F2D2D74C9A1A1D08D8D6CEF86B9837C10B521EC142B3ABBC5951F7273228C6B4833C5151491A50C77595DC2973D49D70194176953E73AFB7BB3E3587C73323E0846FBC1E8B0CF2955B84FE448C83E6F3231A763335DFFB56661B6D7803236387396952A9217F7FE0CA7669C178A337B5193A260707D5651A082B3E705440593BB489D7B91771F0EF67ABA300FC61689B9A893A53EA8B0F5E615ABEA2B74A8709C10285CA6D4F684E5FB868EFE3295CA74F334549086A88986ACBD35A110334ED6E2E10EA12104D517C9F83C199D0D265743CEB128DA9B9459C765323A8E908002FFD3D7F11367A747E951A433EAC84491D50027DCF004590A04BADF1F6C0367929DC4524C485DECE3A6EB3974C9C3D73C14F273156A8B20E56A6ABFE5BA600D482F141A5B75DA3242ADA595851F1D43B46771164B0A5B2172D082D5AD55A773B17BBABE27E4E417571775E10E59EA139CE23BA54E046912A4B1B4408857BA22507C4DFF630319272DED992DB88325647A1613E7A5B1B733E3442576BD01C5C188DD0831AED3DBF4E9EB84E3CBD88CAF6375A2485A1010C1EAF2C56FD3F9B709A25636DF1E84233063F97EF8749F4D4E63A81344C34F4DCEC62020B2B3A136F6C91B39754ED058DAFBDC6B6AFD282FD3E7F5FA1AC3EBC45A696028CCCC5471761CCBEE4AB793672C0575F637831A0FBA18A7AEE21EEC71F59D6CD215221A62D9654526551CCE652A1556B7F5C6C6411F44A36F96BA1F96606359055C6E20603131ED45ACE5BD99AA13376A75C5F04E6F92C9C5203B967E83AD6EBCD1C96048EF3275A506E42359CA205D88383BE80FB73A8273F2FC170048667531E0BFA2A1F223339CD141383E0ABAEF96581D14D14B85249DBB50C86D366493166D43C98B7CAE587F59B8999488566B810EA4E774DDBA86AA3A45973AEAE2DBBC22AF25051DB538B57B21AFFC71A5407248F7C9E9CCED1E59AE32146AFCFB65F7A34446CB4AB1C180E80DD5493BFCF655FF23035ABCD8FFB03482A1D39A7B4E914435B8D517273267825025DEEC600C767AD425847B67AB451AF4C2AE7E1909571A5890C617442B4D3C077315E897316BEEA085F4F46A3039EACFAF07B0B76328F224C2E3880705761BD61BD2C457CEA4B72F3F1046FA6F6DA9286799CF5A231DD7A55A3AE77FF953B156232657B3CDE73B7F07E7E7853873811683A3C856EAB540CF4D25E4F415B98980136FB1945A68639BFF7EA5619184B5E4A71421479A5CA4A71F696C7B1F440DB0B4709C17360EBA244E248E7CE54A24CBB991303DEC27ECE7EB6647C1F89CD5064D52A1ECD1B1AD25C1D7B2C8904273CC5099CC647814A28F0D0650B4C39054862889BD29A7D178AF373D0E9E207DF8E0D3B8FDE6D560ABC3FA2DB297CBA1A9273C8C25F699D8EAC144A569021F17885E1A9039CE8B3A9C57B94F1DBBF2ECE229B617DD72C669591267C7E2EC1683DB024E505490BEEDCD2BBA9C5A89396B78AC677672BB2DC5D963D28930E715425980843AC18CBEF3CD2B0066091019CB6B4A93D1D0BBA570B5393E0EE8355DC6792566F026515D92D99900E308464701C08F4FE0E7FA31CC007F3887445EAC18C380279F914D06262ABE93F93588EF348A579AE256271428C25768F424A2CB04D1B388B9CD79D27DDF88D73BE951089C8889A6B7D6832ACBC7A9E9E5FC3A2D3B4B33C5ED40E2DB87B940AB239F432D4A33EF3433E6F745D7779C2B9E349D409DB1CDCB43819B60C9EDAD6778A3DCCC9A5375060701F792E0C180201A13E76E8F010E0BF14B5427B9B27A27BE53DCE77BE07CA54506C27EF78A455AF68584086B590CC2B88867B6C207638B0F094F104ECE791D1DF592AD8E547D5B1DFCB415D6ED904D661743D8860EA0BE6BC4F8F0A03F3A0AD99582D855E2617C18AFB6E865BF4D184C9D0F6087A1C8F15E7F7A1C21C88240E1D4A3F5F6F02060EA79958E91F670466544EBAA4B8EA47393B14F9E26BAFE2083D32C733E979D4045ACFAA9324E17F796664CF3988845F91700D973665686658950C8E12CABD3C3199458163A720527AB07EC73E456873A5F8838089151573889EF3438F1BEB755A0AF45AFEF977ACB74A81DBACF06705276309E2C0989C93D159B293D069417DC1B0BB9C1F402320D4787BD6C9F152838E6C161D87DF74A26469A0856B3B324F8D4C2608824CDB77081D3DB7CEE053F3301AADD3E2CF044FA3AA9C8B378B4D323CE9B218C3C7E5B84D950A76CBB6206713EEAF2B1C2E489CD2B1C1BC5C9325976E688AAC975324D8B382B08FD10576E5E588A2EA0CD356AB24FDFD86A6BBCABC5BBF5BCA5DD88DC5A332DD8726B9081AC1E4C64B3D934DA6827C0F9D1AAF3CD92E2A4757DF34AD3D0B61858D528521AE820DEEA24DB9D74BF2F732901899E23FB8C75A693575A4B185EE6A3D0E5E8A0976CB4190C1F85A3EB2127A5575988E02ACFCF9DF4301A9F4B8C23A86666E265A4732F3A570A904C450E035CF195E3B3C168BB373D1FCCAF87D3AB61EF4333586B69F649F7799532A3A5C995894FDB4B9D3721A8E334312D9969D513CF16F74673387381967A8E5C335111A737216371AE749CB1CD47A92D61BD34AFE2AD5DF14F42B7A2CC4FB84272C269B26474C95F9DD3D76B4DE0C47BA1A5BEE6BF5F3227792771900441C6D2BEE78B5DA9B9C3D106ABADE8737B40A23D21DA9F9CD1A84294F31B3729863184D5852ED39D4EF4A909F7C65EEAAD5EBCC5C5A3D146375AEFA64791F85DE6AC3AA3224F43FA84719F97EC4D568A881798EF99984E98E03201C5B5F38EBFD86087CDBAEC38B9CDC055735076CD6BE94767AAB50DD3EFD9149CA3F33A75FA387D335B6A26AAB01475E6F02AC3F44F77BD0565F92E18169E3F146494EF8571C2CA3B6707696613710F9CC40F3FB7C4772E21D124CED7AFA406B4D4B6A190B69E0067B0DACC8EFAC3BD6E768C74BE079064B9DFD50109D2B442A0B7895668A9AA5B58C27874D4C757C650CF4637D9EC265B3DE42D483D596545F47B8A28C97418D1505F25F31B017997F1FAC81FF5FC75FC8CECF3DB44EB0C4869A647A1692ABB1ACEAF861DE4C19F80B3971E042346BF59C67A536A5AAB6D476E45976E08513F14BA705ED333B9E5B64DABCBDB71E16A71B67D9666973BB3AEA1DB37207B02B26FCFE4082C4877009252E410451A965CB5739EA4C731F7BD3B671B1C2C126EA24DAE51E97D940CE43D7D675BAAB2CA920E5546B0D2442083F89392FDF74B444C2D2DCA2F370630D75BAD74A70DA2B0BACCF11FA53C7B675A8118319D0419D8EF743146FB3D8CF1A18CA3FEF828501F4C5D828DB2A4049D2E1D4E2350309E5D26785D714E4E63A813CF5CBC41833196796C019398FE12ED9AD78056701AA857058DE6F16D35AC75D2F4B6FCF26746CB8391AD9166DB82ECF43E777ABA1E5DB642E3061300B9D30F773D9007616CF9718F52635723EE6F77C2A574C353AED749CFE92CD9A1AA7BB8C31521143A1BB06D7AA5A138DBEF5E35FEFD9235F777C2F21DA489A7BE013F478AEF1AE166B7FDEF97ED372F3B9FD8ED116C700B1A18DEF06363B0DE4CF73A14E88D4C87E98CE66D2A9324D2F08E9015D23904BC509BDF75B09C74916809627E0B458EC46B8E58277AC89E1DCBBF449DDFB484349A82FA5F53F605DEA410BDCEBC221A4A900D5F0CA5C92FCD4E6265A951AE99D4BCA92BE969536E9E742EC83B17756EFABAB4555C5567BB2B0BA1B9E78705D9D7ED45B77AB2CD523FDA03C84041E6142D3F8390140952580EC8F2C2523CA7E344280FB452E4EB48C8BAD4FDC80C0438DB829313DA2CB22FC51BADD6BF1913453BDDE420E87E7805F6EC4678F70A296CB8D204CB080E78B335DCED4285F4677766AAD2089469683CBD66BF88F6F9F12A43A6D2E8FC381D0642042905BFC7F133AFC2D2E1FCCBDA5B99EE9E5CC7B3AFA3A79BB4CD54AA09631BAEB5075B9DEC88DD43F803D3C370FA30362C6F8D4627CEE496059A16EC6A11A7E98EBFAAB2AC59FB60F34EC1A9D294AD62BB66C35182EC41946590BA7B68AE42E1776646EAC6B9897D6860AF53F64A5DE3974E124489E78368B3A39D7C321A4CED11CA728E8CAF449F9B7DA9FC719A6599CDB4ECDCF9C4F57E884823BC7DEB9DE14E6F08D25BEDE14E8736D314F0485475468D5E69517EE8E069932D29CADA1533A0CE9CE5F8F98B0CF19ACFDFA7C0A9755D662CE7D1E47630FF36420C25850E51A74467C95E177F207122C8BA1D3107BDB78D7DE24727C6EA6605A215847EEFFCC45F105158EF50C539F270AEB62DCB0E41CA9E3EA1EC2B11E9E6920EA4EC34EB531472E5C1AD9D312EC5595E73AB58364AF11A69F371BCD9D61678C003453AC5B72C0FA9D7ECCB8BB822D704CEFE4715412BD9EE0E77110AF5D39DEE90F7B876E01DC7679176FD40A0622DD56CC2118E8C100152AF5C7B444502E15CB24CE3321F0C4EC3D20641CF7F4D9FBF1B75D26BE2697800278405010B1DEF589D80838F373B098C041E5FFCEDB0E4B799AE465263EB5CA94D57A459FEBA6E3AE5B770FE6C589CCA72C38892DBE0ED721342DD81BD0CD2A368C809BCD260FFFF15598EB9F62AE5628FA35036F11E44EBADDE0A1BBA34146A8A1025D17CD5FBB0147C5C8AD75BF08BB0A52922D8C31EC2D4D171809121C4DDE964BBBD6C0FF16D2FD5D995B3686A25F8643B48CC8489D85EDBE845A832159AB9C150565DA6B27C14965F8D8DE5F56F127DFA26136A0FD9546669BA1F9A8AB3CF9E34D88C16D215B69B20E243DA73A70B92EC1AC23B9B7DBA04F456ACEECF89E63817AC41AB0ED66CD56B02E74627D8323BE191E57E3F3E5096DC67C7828C8D04F15761E4E4149E41988F6B8EB10C58DA2142922B6EC80EA93145794F7502615358AAFB0C9697E2CF00D986F286DBED6CB723FCCC351596084AB9B21A51EB415F6BEBD3920995256322D0CCF60DA5730D97E82F87797E796F66549E31449A4F7AE31CA7D8DB67C13939EC47F4DF08DF106C2FE9CA35E06431610BF6B6CF564DA9C86BAEC2C2906DC3743DF2260DBD2EA5A15A27AA98DF9246EB10E6932A0EA76CADCECD7F8D2E6960E56404B054519E1996A47851A4785D3FC637E6CA714D8D6627664156B0D66401E1BD84423A1786A413C6F6EDAB68B5018AC476D0A32E0FFAC4265A943E69AE136274CA6A9FB6C0CB0A40F18B6245C590DA352ACC28CE222427D3B370AED506E9107BBA4D9FA4E6F064EC6DF664715A633B16968253C75F13295A4925D2DEB07769A5C97D15D65BC3833E67E28053FAC1584FB04DF1F3C7892B0F1532D1AB2C4F34EDEC8A57124A6DD2E9995C9F6BCE3273F39D8253BCA69A596E25499681EA92384FE35475B99865015ECD48C7B7F85D075C317912E28936AD5FD2F7D57E6F8A79C0C9DC032CF7BAA3C33ED38C632618CC340E2553E4F24D2E2F612D5E67573853968834332EEBBC11D77863943A932EBD111289BD2E88CEAE62E2BC4E5C3B2E654A7B2BC989649C224D9FE8D440FD6BC25F52A6D6DBD2FBC272D5326B55DC860AAE7D975BA1A412E2CA12B3545C66DE27ADD2B4388BC1AD9B2C3B2F57852639D18A528B2C2DCECF6D585A23CDBDBEF84B6E842F3656583A03ABD6D5035943EE3635A3F0A1DC5C0D93FDFE60B7D75F6B6A3D4859B659187A099C709CE96E67C464BFAFCB0DA03CF0D0C149CD2B76DD4D748EFA4C674EA2A936FEB069D6B0141FC90FF929786E08FA38181FF7279029B29A8B48EA82809A3CC914CAD3DDD01075E35B45A07F4D808D20DF3114671F1327629B50271E4D06445B1D04F083EDAEAE77901D156CF6E91BDB1B1BE2DA42BC99F2D43A1F87516AB9D4E7612BD86A2F4E7E01963DDDCB6C9B38E13207071ECB33A3CBCC3A4B8A32079916F8FD6AC07DA6C761B4C9AE4CD6F096597F67F5E0EDABC69F8C86A295C6E8B8CFB79E13D483A9CB2E24C1D078075A9424923847FB5DC447321322ADB377CED28E6464E42AC9E5847B5EC47C0214EAA940BD8C952B71DE09D7470B5553CFBFC6C0F9032CFF26CEC15EC0FD1CDE4A1B29FBB95F49E0DDE08E0AABCD648793B8DCC4E630C083CB75F60C7F3283336F10299614CA2CBD99B21251AF845B70BA5ED5F045CFE6279A99D8C38442D165347436D619D8DF6039B933C3DD1B9CF8F64B269DDC7E4F70EA7C35524C6699AF5F22021A9F8AE02ED80B6282D25BDB9097A34DA67099C77D04476C5DBF433833D102C2DC0644C83D4854BE9D135E6C124BA8729DFDC61373D0C320D7F328876A9B139EBF42A0238D867E7C578D4E8647A19859185B669CCDD7AF7A9244E16F014E04B7C841870721646AD6802A4E7599B2045F57DE7BF3D86A602DC83319F6C31C67C9A15E7AAF5BEA5A5A7AC12366363C9C07C1C097A6EF2F4B36B60AD2A3E8EE39DC175C33FA8DB63A21887291028D2DCC6C83F5589664111F8ECF42D3F6E1FA2BF3A599928D40B5E07D1A8EF63AB2A2213181CF8D2C5041AE729B99BA1D1CE183480D38D9F19C4AE79FE944999C86848A78EAC08875EEA09A3A5FC9E44EB2D38184DF0D9D4B81C9ED7DC00DEF616F06DB5C9131D8EE0D0F02047D1A0AE9025E5D3A67D66CABA575D55AA1A873646E8CF20996A11D6949B2A3BC99C116F1CF8716275214759C8A535C66419A57BF81F3B620475FA3CE95429D3142869D9E364C6BD2A9A3F1EF57BD4FCD096CE04DA22BBCF2392F959D0063D701E22098D9FDAE3666CE4014E362C018E72A79021213C132EA99222BBD48A44933156F9A203E22547A5F9ADFC949C0EAFC41777A1EC2FCCE6F06244AAB9B79446975B3B38109826415941ADEBEACCBC0D3099C303CD17A274322B0DF1FABFB34EBB133B3545BFDE555CE203B4D6AC659E2CBD4275A18F60BDC73A0384D8A8298D604B40EE74511E7F5B0606C1780ACB01CE6EABC4CB891FFAE691482E31490946643C6F42A169CB699FDCE5B502DFD06EC3460D0DB85AAF443488DC2BAC440E03AE0C00F398F67A7E14CA627E75E4AEA55FB386B3D65A8157182E540885E84B3EB787E3B787A4C6558A26275D3939808A581BBF7917BE0085AC6E4C409AB8310F7730BBE132697E5A11BB32A5B119AC0C76F423813782766A4277218E5C9C0E3EA113D2F8FECDCFB39325EC8B489EF3883C4E24C15A789668759C1711671DE95B9BA6B2ECD5B7E6FC6B5765DD9878F38616F2DCE97E2415F8D904E284BED7BA6C2446A77BC423D93D360B4D735F35C279CE462187CC0CC521C211416812B3F346B050B1B99CCA4AA60FA51AEA51750DA8EC667FCB15C8986A70138EF87B913FDCB98DC642F6067A1E49DFD15EE9E62E92EF5A5D407638B8C85D294181EB9CA58FA4E84A5C6323A655DD4221122481C1406A09E58999ED5B034A2F47E4E7A5A83937150621C676CCA78459CA31B8E5ADF592F56759FE23887C761BCC70EDB60CD2C36D2EE4BE05CFAF3256E1025CDEE90476A337BA2E98486A06C8F4668AA20F50AFF274B3C710FDB8BE088C6936D446C179A5D0D0C4ED33B62AB7D1A4C5DE9E21655676F7CD287339E5E12E78CF636F1622283135926DB9ADEA98D6DB9267DAEB6204E48B31D00E76E2F836D3B0865050B11CE1EB985C0586C66660DAC0A9143F8B105E7A8445454EB642A23731193A76F36EE88B86B71BA38282EC741571ECE9FA62845810E9DA51D1E22EFE4A6505DE912EA684389F407355EB3DAB7F4C7FF10E78D4E6369333441CA10691E8834A1C8A33EC319A41FF4829136F0C15A8AF98D4C2711B31DB7CCC1EC73C12817DF8568E538CCB6BBC95A53C269E430B0DE30F550278C81E24CEDDC2771D223FC499CA6DF65A5A501912E598CD605E72AA70AA4FB2266939FF4839928F42A55008EA2209471581C47718A5107B5327C9C835FE3F48CEDB0A0CE4538EF8C4C9D34CD57E27A952407FD60BDDD59E6CE33E23B294D35B30D99E07CF5AF97E3CBD8E014F5509A18207462B28BB1B4759101704AFFB42C7010845C7B1B53B567A6539E9130C6693C3D8EC6075C2A84117F6A36FF78D9F9F355B2DED5D5BEF0D98233224BB6AAD0D8221A92367946B9D0379263FC92D22BDAECB35ADBEE7E948D5564F10C8DED67121D6C75C11263B0DB978E6A8D7D8652A017D3EA401E46C38358CF2A4CF665E8E97739D45CAC39D4EAB0B65A70EA2CCA5E9EA5245E589B5EFC04E72F6A083561ED16F7B0E5965F1A0ABDA5281B86288DEDAB3F5E465BDDE9B55AC25859D2BD01D5312D2A711EB3EFCB36F3B1CE47818A10DD8232DAD2D3787284B8299AEC0718E3DD3E9E1228727A93869F9AC9C756BAD59BDDA7CC716FB42E4171E7D254632B8D264F5F47D14647D4F952FAF135D7E4B6D6DC33E78DF61AB61007C17DC61B6D5D6A9F9E44660B85F344FB4E46E7469A02CCA3288BB1CCD837875126879E768F8C434DD5D1DA91C7503FC159CE52EA71A63FC379577E058E333B8BE2ED2E6DD42AD78E29CEA53FAD34594F20CECE72933841F1521BA319D7A8348DA53D119CA7A6377362D62C4476355934390AA7C7E1E4400659F62727D1E42078FA928D8FC3D1599CACB6473B815407697BE9AAB9D35064706A1C0475D2D832070551FA788773B9197C6201811B9CAC709307CEC87E6EC9B5CD8DCA0F83D145C23D8E8F435DEF40F37B3AB02ED3B1F428EECA509C8668C102A765CF5AA48B578E062FCC1C27BB0E7C9CD51A423EE135CACBB3BF8D53E22058DA688BCBE865B7A78654DEF9062DFD5B1CE7EB25DEFFF1F2E5CBFF99DE406D117142318C6E02234D5084A5C50DAE1887A254F851248E7BB806BC62ECF4C7BBBDF1365C6C0090D3B3F8E9FB04A4E78FE9F8301CEF8693A358969265669BA15B597476850728CCC35A0E185B16E821DFC61F2FE138739C2B3430DC0897C740B0B902BE33F8D400D488B5A13E8C2D049AEC7089190354C1098DAAA5358AD4C3D1ECD5E0E470B6170EB8E05387EC5B1E9483266189175FF4371DCEA05E9D55637BFD1B380B298A9166B8D1665BC94A8BC5F7655907F81E0811FEBC54C7892B71FEEB657A12CA9B1B1124068488C8F34074792CBEF3902D7A03C493309B6BCD74A39D616C754618DB1D112E7C6D38FF3A7AFA3E85217DFA6BC2E5BA07C84986D3AB4C3BF37425FDFC21D535DBA251249D89E0D444855061C34727A14AB385DFF9CDABDE07606B37FF7CC9632076FBE12A77AA09654939129550EAB7C47991704E9B5D0A89EEB909AEF4882703A3CB5D1E96A5C771C78AD311DD0F9D4693C34AAC64E85AB4F6E64550C55957ADCD2AD361BF4F141E9429CA51C0F2DE7A5BB6D66BD0C0BE96CE92370C7F729C22D0D6BB253AB34BE48E32245E35EA3C660D61B8D1EEBF7ED5C577FDEB659BCB577A6388189ABE1A80E5EC4B36FF3A9E205BFD3AE6AA5B240CC70439BB408A92DA25BA766737D93B4A6A4F203AA0E3549C52BFC58B321F479C4B324FC0CAFBFBA570ADCD3DAE565A3C1F60B515B36CC90E986843E63EF77AEC9D872A8EB90311FD2823550EAEA13B8C7296DBE69C5F12E535F4A1269EE1B5C3BC5254ED405D6C9D3AAB15BE8AEFFC87D329F01CF160AFC70DC3D7B8FDA592D3989657649CD2FD45A5FEF912745FBD447C1B4DAE10E68434B94807E56A8220103D66D2996E75934FADC1C756BCDA8280B8AE168F1702A82FA3D9E368A43D988869CF919924B333E98DFE32328D3FB2FCCFED0F4672181207D1775A96660BDCB308BA5C1275B2ABFBDD2B9003452895CE72AB1B7D6E21E30C57D84F1AC9267FB0B7F805180D9DC8B6F260292E13579E60A7F076B8D37DB4D5C755B89A17CDD82DC4471C7B76EC473EE9A185FD42BB2F4D97D74138B0F3D5C3CA1C67C9D8FE32F5F41DA7064121B7B7E0825CC5899BA53F4D9622950453E743340481B6295089862E65218AB9474212B86562E373EE143542C27E800F071337CE92E9C5502822BE1DCC4EE2E951C4A5F08F9278C0CC3E7A7B688A346589674A5D9AC176053A549D8C3B8F5ACC895F9A267DA87315D6B58D7B08140E3290FEE940263ED93AB4D519EE738D3042A1147EF4842CF580335CD559429472205A4F365BEDE743D1EE54D0CA8865380527F0B2EA68E5E6857445DB8692C322CE8B12CEA40EE7227BEBBD788DB82BC0C3CB8CF383B6612E35DFF2AAFCF03635A592B0247E94EAC4F8E3657A144C44941C92E0B34A8E04D1C4B4110CEC58121526943B3D46B0472C0E4C0F11D94A7C7B28E32CD67621690E1281AAA535EA94550C5F34EAC9599A054CB782F3226A0BCEE66B96973B6F5F21B28DD65ACC38971B09ACCE27520CB9355933940A11706612D9728DD4A944B372B2A40C11A565C9E16E36FB1805BABE76B7F18D6658350BDD1D35DDE18BC0F660B2867060BB49CE8A38AF6C91AFC6DEFA26B78E2B1B85D8F4C5394ED9BA96B3F9B22B89B5BA24D77AD7D08CC5B114E1BE9A4889870205CE6BAE45995E6B8864324EC11971A5FBA5CC79E10940347B104CC1F22CD2F565B282455A499C5DF5F760A43499590A4E0D68B96EC9CD04206BC23FD4812DF983C6961DB66F5F21738DA43F06D1EC60B30D1BCB4AC2064BF0B8C7CDF0A03754F709AEC21223816664F36539538BC764857A18D08639354B47B091DFCBA772C6B80936FADE2B39608C22CEC305382F7F8AB32AD0628B0953947DD669BB1F9B6D31B6ECAAFDD8E00954AF97AC3A19E23A9C4B82B3298B76271A0A51A011044A03C876AFC8649C17B28408B929525593ADE6430AB0DED68B26E41137E936ABB94BB5EE439C8F394B790874C50B71C2C0F2F7941E523C857D761CB63835069C1B14654F044A27AA38F7E93ED3232EEB87071D721FF2586D6CC2937C8D1CCD49A240B8EE866E66D475AFF4F34FF120521D81EC76846B08BA1C04FFC22D5B28E13415BE92B12D56128AC5040BD50F77853AE2A0788B532894268CEDBB2564995D66DF8D86B1B12F65338425636CE93B5F29CE161B735EE9C23FE54AA2B742F45274C90D8662D99446B7FF1A50BED702D2957CAFDD2E6F894FD1044152C923CEC7024E0AFADE464397716FB9C122DF9FEC69C295EDF950A76CA612AFB380D0FBD8A0EF5C653D01AF24BBDDE1619FAD6E073D9A5CC8F428948DB4A94ED562B0E1D8E4C3005BCBAF66EF275910A66B8A7A7A62F4AA7CC15AD7317E61C2DADDC02BD8FA15BEFA50A8501B2A652C370596C47911233F911DBD1AAA4E081121AE3CE64B0D1BE542BB4B36B23550FFCD25470D097DF1ED63CD41CF03C8D4985C99ECA4462FED866E17B1B9CAEBF98E1857C9CC6D2DEEF6D3CCD771EAC80CCB5B31B30FA9DD6F9CEE13310EA336C5F9C74BE92195CDCFDF2F45B2F75CFF13F7486228845CE5339B13B2D3903801F58856973B9E1D725B253D25C69D13ABFBC4516D9F3D96324A14BB9FF4C02E0EDCCB709FE27811D885280B7026B9B1BD2C12BD29C5B742D47E98074AD7C3F4340AA448AB8E1344C12F58E74ED212E54A4CF427F7A259FAC38294AB54FE98C0D0ADCA1626929F045CA87B11CA3E2512EB5E5A7B2B1E5459E6FB9AB8352AC4694E56713BDC9A58D767A92ED3B9D83B13DF0E363B0D5B4920D4D78869B90559E7DD2B585D9ADF4F6C0363DBED0AEBB783ED767A4296E911B721949393429E15B01FC871A43D835356EDF575B15711A761295B7676F534E8E5B639E5FB430BF7E64319FAA1C3E94DA754EBEF4E9D952A7C3107F57A33BD564D981AE0EC0A4E0D829AB2053C95CA7BC938656B212D231875FE4BA2A1D7A696DBA0ED7DD55F698E4F41B43F3AE953A392BA98FD677435B5412B339D6E52EC46D74BDB9355EEF26DE2739C2C120D359A35D2B41193C45F49B2D3559CA69EF0FA257072EEF3CD2B9856B6817DE28A1AF6697E6AC083C65BEDEC24444094C976C6723E004F09005744B69A9C041207A9A72CEB72B5A4CB8E1C592A677D7F30E7B63BB4E6C30FAD17A1ACDD3449670DCE05BEB386A8C5E964AACB196E868808B84338B7E46FCA6E984B7A3491D2D57405794B5B3E7491AD5E1BE253350096FE4D4E74EBA26B10A5FB941C865322D732A3A2FD9B6E82F36668770B4FCDAA07BB419459F5209D9886E56D62CCEC3D71BA005836A849B2C3407DA7ADF6C17D9A566FC1F99233B8EFB96255358AD433E53EA91D719C913418F78953CF105DA78D054861D656723D399DBBAFCBA51DCB4FC6C63A96ADF74D0EBDE7873AF8E28BC0C75929D87A1364B630549D262BB74D1B9CA68BF32A19ECF318CEDE2A0F59D0E8460E2E6294CB745336FA51A878C55485FE95679F12FD0AD737EC4451E17245836A5453D25BEEDAA6FB810B54BB70ECC6F6721A33EB6D0DFF3872F696D8A40AEFCCAC6E1C654A4582737C1A3505272BCC30B65C59BCD43615A2A6EE07A0FB1F71A6E553032F46EB6D06444721649A1C20020AF4D4E07093A2D4557B3D5D5FBB228E702537ADB9A77416151234D82C5137DE359BEF046758C279F2DBEA2CB8CFA2B1E5020736E5320E3A8B88F3B3E0FCD4A2D474D36169D4D0CD315B6F45A6B2A45E4BB8B940FF25F3A0AAE0D71206FF618866EA4791805EEA763406A4391CE73AF53A1052EB3EBDE50C0F9921F7904AFE33505DBA99CE9CB454E71158B5DFC8FC0FBDC04B085173D02EF9359A7FCA427FEE54F60A32C52BBD0F4B201A6FB647C8118E4231B33CDA4CCFF0D5E399FB72609E3CDCC0D632C77349A4D3FED8F66DA951A19566F33DF9352DC5A6E5FAC22CAED6459C47E50A5FEA2D132BBB4F6FA6ACAC54994211639B0C8F839827222BCEA6384BC6ABDD8F2DB8464643EF6866B9FD9EF84810A540FFC8896A1AD3B41BD4389CDC32E4B09713BD1EE8AE086695F5A50965CD2ED4D7669BF83CBE154F499692C99852AD61293865187572FDE800B26BFC692235D0A2EDFDE3655FAC6B93CBFD75A7B257DDF7DC05095011E2CADA959EA8939B1DC7721474B02E07A9AFF3E4431E9557C2661D21AED6A23639DE7B2CFDF12EBF7F21590A719A759C659CC9CF7056561AE580696921D04172D8D76AAD1C7CD0720616EA6CCA4A0FE0A4F95DE60DCB2EE2295F4964EB70B2C02B38F17EA9B9C3C0FB951D075C6576D21F6B882BBDF3D6D89AAD0FF28DA8AFBDCDE2751763C4B1CAD24E59CF1F729006A75127EDED70AFDF123B81DF0AFFBA5A5D25C7A5FFB41F2F3BBA4A75599DE852B2DF8B3659BFA595DAEB4B0188D16C24677AF7D68C21CD1DE18766E55ABA697A7473ABEBE3F496E52E32B6979549ECDAC932DFD85E33E3C4DF13EFF02C45EE94BFC6B38BA45AFB0A8697CB3ADF4B6DE8ED9246495A182A0544AC3668EB0927A74C1549DF4AE01C9D304FCF101C9D8792ABC4A63624E9CA4C33CE22D49974D9CB6E52B176D51A5D7AA2940A518E53DD271E97D61B44E32DD85B30E3C3F76FCE7D76DE492FBFCEB7BCB6F6560E5903CB64AF87B43BE5D2B94829EA6995EC965A69753E092413DD78018E9569AB326A5EB4B08D3ACB383D751673959A059D969F5BCA69584A572D1E49E0EC22C108373AC0A9D190E254724D89757B2B2D2D0FD102BFC9E75570ED7D6AABD7D49D8B9B82B3211113D20058DAD16998C97E6D9A959A0630DD7151CF71D00DC605B3D689A40E1CB11789139CD6C67EC9296AF6325763AB098CD85BFC093C4E5DCE0DC52FD091556F6DD9758E41AF44BC8C8F3E70179DFE0A7779E366657B3D18DEEC244A8EC26043EB061D3DCB12CF37F1BC77B21330165BFBE3EF0EFD9617E16E5E43F885EFFC953AADEDD595498916108630B6B239260FA1DFE8F8D19098D025495124877963221D5D4DE62CAD496FDE9A6D34CDFCA858BC01028DB3109696508FFBD9411763248BEF7512CDB4DD9A9622D3B329156076BB9B1ECC7BEB2F4DDF5E9E899A8D51EF86CEDE8E4EC2D6EB251EEA7C3868BDE38C8A6C42F74A0B46B4B75C3BC598484F9DD09A1FF7C238E8EB7E8C83FD005E534F13EE280625AA3815A481D466E90063C55EEB46FB930CF9962ACE7CA1F5C250A86EC5758E538320F990F9D6014FE2442884A75202A2B696E011FEB8C816387557627C2802B538A5342F382508427A204BB5F52D034EC42690E6F8221E5F726A45A1222BCD0EBBA303EE09C60CD5B4FD993139EDB3F02BD36D66A6E5DE4AD34D93B93AD16396679F9A085D0E6290F8D8EAADF5BA2B3DE9B9B54F98A6A46281B9CDD587068306F6833538FD891C74BF8F58171A05D1AE1C8FC7774328B6AD289D221D3F9E0F8D2F8661E33E6C6674E4C57C58A8059C527F2F6C80500D8516D9DB5C9A626911DC723DE86934D8ED717B0FB2EC48BAD26ECBDE02183A5FAD7522AE3782BD559CCC35AD83E4767DEA9C64FB21F1BB4D898C001B1F66278156F8D856721199495029048E8F811362C5E88C8FBAE3931E5B2FCF75AE6DE0CA05E6CCB9470BD295709DEF7CB0EBD7A4788B04947DB62B9D686FD879F35237DD95C91663365AAF758324CEB1C0DEF67582E5A8CF930D76D9B7909E843C25749D6759B67D67A92C3F199644AEFCB87117AFFE8D198EAEB0F7701EE5387F12D9960BF1A5A2FC75BE8E0571101E49AE2E5A6D29CB404A215CB7BCCCD3A84CBBC6BB865412967A744B26D2D1BABC493A5F1B69CAEE8B4B26DC78A75BBF35B899B4ABF05DCBAC673E7DC61640F1A65ABB0F8DBFBCB1D51F1D2ED13438B302D1078FA81E5B763EE0B1CA2BEDE159DA5FEDB62419D526BF966D941123DCE0B6906B6C3AE1625E80DC66940B571A23CE9727BBBB6A8B76C659B69D2EBBBAFDDABA19DCB8CB8E9EBDF6D6310A502D4E5BE1FBBFA9B34A549BA487077D2EE5DC90DF9EA78433A86BCB193792B12C99684838C9A9640D2DDB9A2E937F797D277FBED475DA76B75B889BB24658EBE14CCC3CB612E510A817A1F61CCDAE7406749017F3CC013AA9769C5833EB195B6D43C9B34FE24CF682682BE09EBDBB914E6B3B8BA2FD9BFA08CA7EBC3C1C467662ECF07C8A2D83134FB99481E814616CD5891A67697529383D849BE551214A9C4115E7F0AC1A0A0D559AB5EAF4931313D95E0EA04E3D9121B40FA3E0E451B9DC0F81A52953D8937A2C5B35F129EDD3D450B661DF26B1602F757192AA537A8E3801C95DBF986EB287CFCC604B066266436D638A8E9CA5CD3B8DBD354388FA89CA17AF9820C55B99FB1C64C751B8DE8D7607C1E7DE60BD936C76BAEF1BC6DEFE619BD90CD1869CB2C5B5D9D1367142A0888C06B2F29E7EF1234B77B9CB5C313E326709725B3D0C6D03F2075F37508DEDFD27EABCACC379E3E3CCC35AEE1F7414E20F808DE59129AB3CEA38C0AFC845D72DE25C9644F3B5E27CA53EB2BB6C2655D8FFC8FD504D0BAEDD20DE6006D71E2364D66292ED0E8FF004426DD32A11757D463AA72DC53C636C73A2D67D6A55CF252A8FBE3A4D6391A62BA3D32858690F0E87D116CF3A4A79187A981E047A32AF7454B76423F4256DE4D4FE8414B68AE70181A5E0DCA5FBE4118810A8F19A2A4DCB52401A78C8DDB9CBA51D3B7D7C68A07A32F5709AADBD0AB36365635B69B8757150214BC1B8881104E1579773F6DA7D1E3AC60D5B7A2BE6EFD403009BAF4D402BFB8773FA509BFC74DBE99EB6E3EA3CC66BD3E7678C2D7E0817B52F0127825B234D33AF02A5CAAC994F542A0CC6D8DE167DA73DB3CC4C7FFAC39E3168480B519E08791A876B9DEC6A34D88D1224EE9FDBF1E74EB2DD4DF7FAD921A2EB285AEFE83102DA5E44A22B4DF06351053837DBF1563BDEE9202367C460025AB1B46A665597C232F429EE1156C8E1B87A4437BA9EB12DE154A297495A0D85EA7D6732D21A82EC6781048B27536D73B3522E30D26546F2D8EA91AA1AC7B6DE7089A4AC687825675CEB5EF04BBACAAEB7D294E8F7A5898FFE341B9CF09C858FADA6E4038233E004A78793F3D857B173A25A3430AD43D7469D337F3ACC4DA454881A5D3EDAC1F3AF38839B5DA6D32FB3E9E36C7C82272699B2DC9F99FD70EECD353B8DF1BC6A3B27FC68CC9D187B704030B6C4B901BBC5AD674D91D6585A6E24D3732C851977B9DC0FB4AE8EA1F71884BA6764AADEF44594FB4E87D36E9628EAFC6D9C56A3702D273CC72812C7A9F6564036E5EC614E93311AFA2438DF4AC6221576F6F6898D35EB1DDE4AC782B1BABA48DB78263996B7A1FB690E77BB23E42A9771D5D8AA40D5EA4E7D8D5E0F0AEA34D24CED8C584ED1DC18965A241A4DEFB2743F189E24E3FBC9E46132BD49A7DCBC31D353EBF26325F564C92FE3F1758AF0B5F9B6D15BEB729B43E0DCEB45EB30BF2DBC39BA873005EA025A4418B0D85697059687DE00D703235312153FBA589D17259CC3EA4A1531B0A9676F134EA11C0530B3DCC343E6C542096B0331B67D3952155C41115AD4427CCBCE77EA6ED3BAA37F4366A0749B9AF65BD33CA7F12DCDF27BE8BB09F6C1A70657831C072CC15F893A6F06665CC7395A19C66BBA2353BC44C5C4B7CEAE3EA47923AE2F4DF9707A97A6C7D1E000EFC674AAFB51DD1B5DE614F588C9C7F1EC81C762CFBF4CB2F3C1D2BFFE875037FADC957D1DCA0B93C390C1ADE8D2E054AFA95B42DB992EE507403AE2C328272A1A8567ED4B64F4C26C575BC039705D990563EB42A15A81EA64F505172F70E5CD469BFD6A9CCF6B21B24580A0A7CB4B8CE0A4B9C407538A44B49C52E7C367B5554E7A15E826653B8225577C27CEE5267B9F3E2C21251FEE509D633D5E4EDAD5B3835E6E756F06FE4D81E8AD95A61B36C475CBC72CCE91B93E8E78D8E06DCAA33DF786C3E3618D220D51202C10E589BDFB7DD6B3DE36FA5B41F7738F9B676FF607C7038CE478C0D34A0E2239B3240411B7133B5880083710E666D0D1C00D703DC2B788D5DDED07B538753A65E8B5227838933C577144FDAD69204DF69170F966B80993D2871C253AEFCA92B9A6EBFE8276E154BA5C8FDED68D11D8C9B72C1B2F2DDB550FD233C7D5937E62C724BDD15BEDB4DF36B45F79B8D71B1DCBB154BA44570E93337ED40C05C91773C77993E4898A8FD3A5A1F799AB1649AC6B2C2DCFB0E2B181788B7852911CE83A32D75C94B93A89538842D3A3730450AD16DCFFFB6677B5DB59E9B497DB3CC8643B0CB60239AB4F0E4FD892D34DB6B8ED3E000FCFA0936CFA6532FB3AC578FA3E9F3E8EC105C21B1C51A322D040ED6DAD3A8B386B0B433951B7D22191921E7312CA719B93620C857619CB31519153CBF550E4AE390050CE9E37C7343026D27A8ACE51B7E58440DD3FA1F3D60C290C01670F38A3B556B2D3490F7A54A7EBCD745A3412345A34F6D6464085C29047F4E9A10055F697CFADEEEC3E9DDCA4E959929C66E3C779D5BACA39AE7255757EB1EABCE78E8BA373D6557A1F9660726D6B017B0F7A9F7B30BF102B0123BED5ABA9BCB3810F9FED6FF4E2FD085C31260FE3D997E9FCFB6CFED7147448F420500F5AC4791227A79550E8C233B67E312117A81085593E0AD459C6421108E39DDE40B6E1A7103F36D53B227D6EBE316773CAB1460D4D3AB99B1637A3345386ECAA9202B76E418D7701C2E50EF21F5BE15EDC5FEB71DD1D4F8CE3D14763D93A33AF24DC2439CEDB1CAADC6B87FBD09EC35B269A9714EE736F4A96DC482E1B5F0C067BE1E86E925E11923B433B87FA2507292C459AC079CB435FD36304FC9CF455966C0D59966E1241C81057AEFAD9B6D77709CF2AF96897A7469D52AF93FBD1E47E4CC97E9B02592431D10B0D9C72759E3ADF99B850282D18DB9240134D4E3853BDD7EBAF34A1C8445AF7E02607BB9C4EE9734B8826676BA5CE17ACB6F5D85C4D40F55E77A26CCB963DBA538F6C2F2A9B872F73A334E46DE1276961DD09C60F93E47418AEB6521E631580E558FB6C2FFDC02757E1029C89D6EDCA440D4B5F94D4250F3DBF15859D8EB2AB09CF467A18D9C398F338B680F341A5C96058F618A2018377503C20D77C2FFC58AA95EBB269CB6BBAFEAE424597E55C1E32B6DE435C9D9E435DE9E48E50618AC7F72398E5324EAF15A10E67D5DE6AB5FD72909E840324C52B4DE87278122ACE7887EE93D6F513D727031B000B3939A4538AEFDA1ADF7E670E2063C54728EA6172DA89137C6C86AB1C9C3EDCE983E5E0240979624E40699ADD2E72633BBBA980F42220035582DB7A9C5E9DC8B014694EB8C372945E00E79889A622D75028F79D7A14A835B316271E8509CC1877A7E971CA5A9ABB6CC66914D97CEFA9F6BDEBE9B213DAA25D4816B637D8EC0327649A5EA4E39B11CDEFD729A0D6E274916D524E3DCBC6D69859765F72C2BD83148559CA06134D8044AEC2F0671992ED418BEC7AFADC121FF98AF584E5A6524474232756D1AD9AB337A4784D903C50A515AD71F55DB4DA8C654D4F7C388C0F9278BDAD6656A5697196CCECD09EE4E84B33B15B22D4195B8FA5C1295E53796417A3F1FD7C7CA14767FBBED3AA33F79AA3B96369700ED9D577D887EF747D7842CE202CF471698BDE3BAFFB52DAF814BF9E278630180205D7D195F8D4BB7111A7F59DC3A2EF2CA8F3D22EF4B4B5595ADAF3184110A2D9588B06B25700CDEC0ABBF7F021825BDCC31AB7449AA6E8CA8240435391B6747F756981D5C62E69F33117337F26C518387902553BD96A87DB78ECB278B39BA9D7948620719C49BE75C5ADC92C6B7CA4269D0BF8E547DACB213BCC4CEEF518FBE1F8763CBE9F8DEF583A30298AFB4A7B0673CED2A953581A75CA2AF40670BE6D36E5AA907CBAFC146F1AF9EBCAF55DD1F6C2E3AE32D5814691EA802B882EF69D8B70CA62953CD7BC883945B0D7E351055262EEAFB5746A33A13459D86397F05A3B3B0EE92345978C6F81F3CD92B461B22C8040899F5A96ED61294A7193AB14254F6DD868EB48B63B087F06BB417A3D4A2F326E047C297329887D6E875341A85776E939CD992563A625D33B32D9B4EEE5B5824A96499CEA352F93290CDAB7F9E4669CD780EE334F9D16E41797AEA8B11569EAF62D67319BA4DE0064A3F17A49A0368D34DF16719A6B237FDD8DF7D62C7F6018851049CEBC65F25A8A6C8B38CFEB8CED259BA1413195CE6EEEC9BBDE02B9582680A0B0688BCDC15CDB4651F61001B14B58F6830837BAB25657CE115B6DEB06F98D7FBFEACB26033D7596CB4DB3247D4544F95915C9A31E873BDD748F47CA0D8FC2C997E9E471C2D376E038AFB4642ABE107ECE0E4F73365EB5A75E5703D7DC603EDAD04606B575339C5CCBC98F5793114F554D6D0DC846B685EF05D49115A8A74ED9F46C741691D01BB22454CBAF65C835205C816D3E34379E5976ADD2EA53D507B3C62487FC55F24E8B735893A8C886D312F5B07D14D9C84E37C07B7D10A82E65CD774F8E3C6D7199F1568F072C2257D90D92BDAE766D2146054E1A646EC7D3D5F0471BA8F4101C6360570912DE51CE3102C52ECF063CE823F6D1919C0CB3EB49B2079CF144A4C9EDA8256631527BF0524627BB9A1AACDCE8CE175FCCCDEC3EF3B5C5C3732F93E428C1BF383AE7A9AD0569DA635B5C28E43F13469D3CED8A87940D0EFA6469A4D95013AA2A6C589C865F8ED3DE3BEDBE2D1A6195E9271ED059CE3B99749A68766887AA93A6353D8DD2E3906D86DB3CB5969D06EB5C3783F89BA59FCF8C5D39A9B98214229ADC65D9191735060218E14F881BA90D419A5C5BC39A5FBB2755851E77F86D72BF1E063E96A598D64C4EEBE4B1383C263982B3C433CE2D402E46C9D11038A7B7569A423447A89D9502C96C55F238AA042FA3B22EBF78D2944DCB816178321C5E4CD2637868F957EE3C5D96D4F925273A7B3057986BE2BC18489B31F4B7D47CD3F0AD2B14692CF01B23505EDF1ACC86B1C59CBBDEF756B23200D5CEA8E82617A706A746B0A3EB7474938EB8BA2F43B0333C0E86877D0831DEEA685F812C1E6E0F4FE2F175C2A64B7600199CF8B3B9B3C36E2F6139BEA57399CF5F339E23B6C176A160B533BE4E2150D97D12A26CC15FF63E2C453C733537B00A92FB9400E4B959C7C9D3FF2EE2C1D130BB9964676269EFAC81F58DA7D59C699A2DDDC83B2EC14B1D5AC5792FA72D7C7B9A7E9DE3E9199F270BBC6635E9344AB5EA64589B9E442487F15A71369CDA7C96CD9AD174D74641A9E699301DF1C4C9504866C7DCD4D845E25802E4F82E63A7C869081B9B9D85DCCA618BE12B525A4EF7ECF5A1C2F1F980A9C827DAD8842F06087C7844D5099BDECCCEA02BED1F4F4F9C07DDEA35DF2E8DCE9360B38BAFEF2325D553C66CF8437FB9D9014E784A767621157199A559F5C7919D27932FB3F1FD343BE57642B3BCE8EA99566B42F30E204351AE16A163ECECAD7A4D486A743F9D7E9DCDFE627222016D9A970E6AE4EE55F87C637B4B897757DA86D99BA25F742A7CE388E636D6AAB3D928F17ED72C7E31C70B7F59A76E46ACC7FA01A7D9B3EB76981CF4C65783A7AF69BA6BEBE988A676B9C61F389FBE8DC6D0E24148A3BADA8228E7DFA688C8A777C3E17E1F1635D9E37C7D761A3F7DE5065C6CC27BBD84F885E71DACC8DCD9B29C46B2DC08574C289B6CB6F16F65722C0EA5A9BB99BA0AFBCD50B6831AA6E7697A358EF7121E74749715439BC242F9B937CCEB8F96A835C8B9E5B40CB2CB517A39C9AE330CCE519B2DAB3D908F7526378F6FB95B152C36EB0F170327C1A2E62CC537CEC616DC67C37E8D2AD2B9D8E24F3346FB054F4D390C8708DB4EE3EC7C30BA34ED5BBA37F8E42E4D8FFAE959F4633E9E5EC55C9EB8D9697F627F65B4DB6793FB49FCFC259D7D19B324F4B189D875FA309E7FE511A693EB847BF5EEF6E418F8467A319C7F9B6447B11E953894459F5DC6B1EC16D7FE284A53D2121E59BE27678DB97AEC85AB1248479DF84B7C2ADA4F6072E14419A1B8D8C7AEACCE2743AC04E7F94E08B9223DCF6A4A07308FD979CAD8E766328237C1C374931AAFF9E0212C4D563F8E8B655BE264C1F67A28738240E2F2938633B33EC8C6DBA66F6F1B1EB086AFE05CE24EAF24FA2291CDDF87DC682C1E5D1027584E109A23B6A6E718C65BEDA7BF6748CC877B3D0441E22F19AFB26800A267F1F3B711DE5C1ACFF5363CF1F3DF33467ADF803F19E00B0EE9F9BA1F59AD7DFE3E1D6CC994CBE7362329290462F48D9995B44422A07457C29F13AE3F312EF3C29859532890F4116F3AE28B680F71F868F2306549FABE10D9DA89ADDC953E7D2533C7EFC945B9D659D25FDE8D47B763FC4C8CD149C47E839BD453BF2FCAACA0D4A22BD532C2544242BEFB250C120DE5B1CF9B82B16D94EC306E8AE2363F4A3FB4B9CD8BE16180342E353819D400214439BD4FA70F59761C509A4FB3213CE26E3F94F390824D5976CAEDF95B50F0F377D9B1F0CBB82F1503EE7807A2F712D6EE86B0BA9C29DBEAC000C02C4772EE539FADA7DC6D1BDFD27ACB9DB735A0054B4893990972129EF0181AC7C95A41A2D2CC534C0D7FEE18DC66571912FCE95F4F3CCEED7A38CFABA9858E02034F22DEA75C9459419750F9DFF3D1ED343DCBB2C3486BB3B6A4E7C133237F140A50BFF879CEB0F3B1E57036DF356CECD3F4F8593056B88D9C68FE599792E6EC5F73A81FC5D710677AC4635CF0BEB3C272934CEE86CA92384F4238543CD1F07C886B604BBB1FDB30AD1DD85571A293FBEC793EFDF1FC0CE5E14338E0A76F93E7FFCCF0C621534C7603FCC1C84C907142B50010AF3300664B1F43A40E37C867942BBB879A1A9E44400792999C4582533693F62CEDECD6960B54A39C39CF265FA6E387E9F034CB2EE43886BB6295A0141C15425983D3FDA8F9DFF3EC7A3CBA4C27E70379684A2D07598DB1ADCB58D4D2C2DAF14D2F4540621B1B797CEBFBD4469EBD78698965D92C48D9CF70729CA7C4C916F25BB264BE259B4EE2530877F9475E0C063B3D3C65B3AF9318E9C73E7733C517A7E7F18FA7E71FF8EFF9196613A49FFF3307603CCEDC12FD2482B192624230BDE56461761C69751EEE9393A39F5B720426CD2F124D7617EC769DD7F4664BAAEA4CFD02D0949B5125D16E9C5EC24E4EA0ADC9FD642AD5F3BC06F4582DFD982F3026F122CBAEC6D9D50491607A9A8C4FE55850C1592E21F920EF47B5A45D8A1232922F86339E81ADC926DF35F3C0D533D10DCFC616C66B5363224E6E7C031DC83A2C042F0847115090A5C4089CF6E2114C11BE0C5128D79CEC70E94CBC894895336252C99C80250C2CB4F8E3BF4F90E98FA739FEA4C14E9FEC918F7E683CFD3D053F563E4F190A419DB8D2838A58B967F30AD34D7ACD03EB35B568A05D239A9CDC78BED32B03A9C9C567C72761BCD91BEC04C9499A5E8F870864EE67565B5A524F0BA655AC2B529DC9E36C709A0D8ED3E460901CC4A3A390AEFAC678683FBCD29B99DD2F63EE12D0D2ECCA83A92EE1FD29F83CA3487184166A9E7D7AE5D9861A589B5CFA2ACCA18AA595FB25BD21CEECB83F3A0BB5B7916F8D7A17BAC3D1609FEB1191412632F3C5DAEC36ABEDB0B483ED2E04CDADEB6FB3F9F709DEA9EC28025784B53FFE338732903BE226F8C4392FC47B9D0F4DFCC068B3DB7CBD04AF8907AACB9DD33B7A1A78E7BD6C03AB5ED306B4A353D76390E4ABE4FD025061268B5B5A4CB8A57B9CEEF507FBF1E06498DD8E11F78E6EA6086AA03CE4A9A3BBD9E46E343C1B8DEF18EC2467A3F47A34BAA3698DD77BA3C3607216CB6619E9CCDB1C2CC779EF4DB91494EA5E7415252A5E5B32FDD1F0EB765E046B60978B0995AF2C5A573B2CCEECA89F9DF4B9121DEFDA1D8FEA169694260C457FA387B89785BDFD3ED2479E0ABCD7E5C9DDFB3D385A480D4F1F1E43985F9E6472164296F3EFB3E7A7797A044107C8A9914A22088268000C8FBC56DEE38DF6121BF25AD212C698A8FDAEC94AEF76970D063C8255AA0767459C3AF9ACA2D1BA414DFF87AC3B8077388D601E061B9DC1C1607892209F81F8808D9D0C27C3C1D17088EBE120DE8D87FB6172108D0E43496D93D98D572CBC2F8D74E6007BBE59555E628957A29DBE6F3C8D34DF351B2E9C71250213A3362DEC661E1F1543E2B299F56F5E13A7DD2AE27A3073388528F25104B19C36D9EA30AF806FDBEBB7C53622F718B14E144110F2A90EC4CD13E21F46F0FC4FDF273C5B96EBB91259CE18F5965BC847E14DE51C126EB2DD7C83F8965D4530B6907E4BAAD203185BE0941F6E8220DDC2CBC641B9E32CA9D35F49F2209FBD91E54108D71119C819F6C97A6B78186517C321E3E74EFAFF2ABB7ED736BAA6EB3FE12D5DA478042EBCE0C20B2AB4E0260B2E227061410A2FB808C2451029827011841B2352049122081741B878C029024A11508A80DC049C22E01401A708F829DEC2C55BB84CF97E7366E6DE3BF7EE2ADFF785C5AC6579ADECD9333FCECC9DFBBCA2D7972F0757AF876EF76C4F7AE3710329E777B6C6196CAC6F98665CE910334B11D0B6635EC7C9723BA51571721313057D673BA26351779629A219FB4E68FA6B58AEFC76E82D2DC7F1626C978425E530C37DC42CC4CEE151AFB70BA160768C836EF1DDCFAB8BD7E3EA51412694CCE3023B9C90F7EA11717921C7F8026D7C44B82146759D4DA66C69E90DE8BEE4559E10253A05853FF451B2CD8CFE10C339566A7A20655A9037B3C6D8EA674E5A9CB528B69037DF30B43C2D8177057C3FBDF159EC27EB89652B575F0D0D287A5CC333149B5C1BFA4AC5BBBB5B9A00275431F3E8DBC4FC2A718399ADF1B2880C6CA64190FB0A383189F203A8894F2696F607A4290C033CC4E818CCC479D2E3FD4F802BDA3B9EF7B1B2E7D30C11E9319ABBE8619C1D95576F86C37D6C58708579F613CA55E8DBF9CBE1FC6C4A39491F7BB0E514C1D2096658EC7709DAEA1136ED222CE5F36184FA5B6766B9CD2078CD24E3AC195BB392247E3DEEA1BD75B56B97B9CA86730D6E52DC27FFAD4041052F80EA36AB73E9291D438A669D4AAE4D0581912E1A32A592547CF749AAB1AECD61ADC598E05CBCE92F094EB2B4F298FF80A04307C624BF1A60D6F911E459026CC03D221401512844F794D7FB0D0607648187D317038A80E6C730C2C83A0E4A0A862F4E2AF285C3FD829257E427A88EC14D52348BB11F7FAD53D03B46236735785402CE4DC049045DBC811274F53E0D82421CE493CEAF4915254E34D3631ED0F55D0A02D865EDC9F0586ADEA9A0865F67F7E98DF3ED5787E897C5EC0DD7A83B5668F5881A89C0AB3F5E5EF72A4F3B45ABD88EDE9046B6523DA550481A1B0D9CC0F2FED7353172C8DB620F901DE22B6A6190C5914890FD2457DA7B54D0DBA6BC5508862B1FF7E86AA30394307936D260F66A8CB8E9457FCCAB8E2808C220AC43CA52B2C1133C19183F88A69382B0CC184EFA4C94EC42D0D02E4BE5A5C64197217988D8693AD6FF00EABDE7D3576B45DDCE819FD3A6216161787ABE98BF1548ECB88BE52B0BCA596D9D24492205576F51736B5713B9E77F0D7F9CE39437B0792338B9130E2186585AC61259E329868EF50F7A7E1E20C5698410C514B71F27035EA989DAF541975588090FF719509A21DD088428F957F451F23EA50418BD99684DA69B50C472948705BD99A84C690905BDB0FE8EA0D57E499FCA5233F29ACE4E86E6E66F8BE8885BD7D3BC22C9346CF7970B686FBF36FCFAED97F8E148826AF69793D3A105B274E24ED44860AB9BDB4EEB69B2A5758D37F29D3EE3D4AFB87B6BA2EDA1400138AF00E73FD757EF67B397234211137FF6BBB0B4871559D4E5DF53E21F26743EEE522043C891E3BCFEB29C62992A584B89A32C3CAE76721EBA8CCD46C8A852EA397E8A0175E363ACFB44F7D70E767942F9EC451F0BB0B7327FD02726D28BE3940035F59A97A6B499346E25C797A617EB2637896EBE9968F6EBC29AE2DB9A4DD6C8F672313F9F969D22E265C7DB521F07D530736FB32E300E799AF89A7054552182F3DD1470B218A470FEBA9EBF9D609A11E02C094E026FF40CEB4C9714F7BF190E9E605C15B19300A6FB0E11FFE510539A5E0F66CF0115FA457679CB9F4374934CF07E6C3E8BCEE95D6EDAE3B50CF8E9014A2B3C213CF39FACDCC676901A6C7B0DC8373AD75B29BFC5A09AC876A55BFD1623EA6C660DECE0298DEF8C88EBCCECBC3AE8D9CA1722204F3E17D326425D617CAA3CC75EAEB332AFC53E4E343397A5C8DBB2352222E4507AEABFB112F4EBFAFE9F1B807732A4448560C3148DFD12D675AF2098272F8632BE54D24D04BACFD194353DEA521C3B798A78874256F4E4610F44F2A318BDDC63B2A2690FBDF00584A1A73D996542912D8FE58B722C78D0B351139C76A9D7A26666E7294D3DCC96CAD6F9D5EC70F4B8444A5084BDC358D909C5C0251EA13F36EEA9B4D48C443EFF6D483C6AA46C378448B14FC5EFAE5D7D4075136A35C309C7F97DD97F820D5AE1050F7858CA43D085D3C46A80B15D4819A71CEE8ED020024B4B1E71F2B8983CEB8D08FE87D87C9670E5C1E81063C9F68E1E0345EC0CD4C6F45372994450FADD6E476F012C95710FF4FFBFFE60561FD8E6D8BA7A50235FAD7BD68542815E0D66D3AC6B30048D7FEAF05EF804667E3E895AEEDACE1DBAE60165950729280906458F65CD65267D0B7AB215D24D2137C3F9912BD55C421138AF3F5E60FADF5E57188936766C0257F4D0B555C9F6C0DC14C22DD147156189BD0E0FCBD17E317B810D9A460710D6B1C788B48CF05E6B032C082CD0188D2D4C5142A1C8991E82EE0E777C339618CB6B444ECA6D6E784942B4BCAB31DD8C11BD77AD081183DD6FE96099D5714DDA201F2736B7CE088B17BFFA30CB6D37AC2D9E1818AAC73D47B2A0E1E596945B11294DC94C599E4755CFE038ED15D6D0447AC9708AB1FD79454110FD6D99900B338BC98F105409D12177C61235291945ECFA6A44C9C99837F81913608F726C32F17208718027AFF5799109865FA1131A6DEF32C78D0C2FD6F5611418DA8EFC0E2FC3A783FE011CB62EB8E91464F36F3EC7FE524E12A8BED570F5AF8B665977A2DF62F0EAD05A085D6E1A94073EAE3FCE8A7449426E1A094C2D6CCB94C0AC56607919DBD8A8F699A8F009F0EE226BDC78C058521C84A4F39AEE2CD68A3E2AC500CA28685E05262882941863F5BC9A3EC306A4DA1AF204EB5230F8F300EA0FA15E75F25EBBA57B51ECB1F9DDC72332E6BD1131B79EBCEC416F723A1EBD18F60F2BAC3D7E3AC03CF483CA4F42A7CF0A65386840B199FDF6FF3A960D66B91E4FD968B6764465F3CB39FA667DC34080D3BCD28EAC6B6E93CBAD58A5DBCA13821A331B2C73E441B7F87C330BECBCE18449E1FC896E25BAB358CEB22B0DDA323E58976322A005062506931CFA0DC530EB87B104EA4396112A5E0586D5F08FA0DB89FBCC365AB29A13C6F64997CC2C213A7A4E31D7001BDF9D8CB1C1D6933E8F47C7CA377EAABAF4545D899E6C82CC06301A03DD24DCAD99E5F4579A2C6DA0A66327AF8799D3C74B431E079EB7AB7940314E379B9CA58F7A72273814F1931129125B2EE934AE97E1FC1AD8B97C3783B97B548AC5E30F9ACB0461EC56CD1EB46282420AE8F0E8111E438273AC94CEFB58379FCB2E1404332A2AAF466C780B59CD59F27AA32E3F2818F5444E1A2BC547F4951889C4F47828DB4E30963A189D3274B1B112E3DCD7F16B82F03EA1669ACCD442A19AFBBCBDACC1C9BC0496EDB06C2FD4A50D8A498AA2301832F9C25626B83AA9C85D2A15EBF589090CCEF2388C5A939E205FB29E92E33CE8C9F4624C41ECA895A7FB0EAB8BE114D84D40A61CF0743DF4CAD2472C78A51F394B2C9CDECD39EAA120A87BF16AC4ED23981C2E1BC8C9DE1B3C731A73F5BABC9A1C1B46304D67AF26BC79C450FC68B587791018207050CDD1A9CBAD788D244B4C6BFAFA32185BED0F3227F520E8329C68BDE533EFFB002C295434BCDC095806E93568404D8CACC73EED404A80BAE31E88744D52C8651BAFB98656926F02270ECE3565553757033AC1D996BC4CBAE21107087A311111C30D08E0927716EB61BD6D8E8528BCECA4C76FC68EC24F11E9909B942999D83A8E774A16BDB1CB510F368239AC2AD908E6716FFC62343E66BE3EEEC98F064FFAF4ED35370EDA3443F4DB789E533D2C6A7A02D217A308C88FA9B1818FAC39C4F01C0FE44E30B6C647069245297F625AB7E296CCAD6063C30311F332C8465BD165B34D0FE777811376ECF6EB5C2CADEED7A1CE80EF3B2537621E09244CDD2D79F6A58CAB40EAC27B84F1585E769C10DC79596E5789C82332B7757B40DD2B6E5B667820E9A4EC8580A4F7F07622D014899DE23B7B8F7B022AD197DE438901DD65DD6ED38D296D8E8C1AB81B7AA3571E8999E5B85A56A801CBC32A56068ADC312674FAEC944976AF8A34DDF74695202C5B0805ED48C10FCDB45E0C8A048408CEBBEF84258EEB4FB34A3CD643F7C99C8D068D1ECA5A19D8D56ABF10250FC3641EEA2E71B2C78F2CFC236831528607D5F22C3DDE45974929C388E98215BAE92B0CD9E1390EE42CE901EA63240BC1D997093BF479889483A3419F42A4838ADC6A6FAFB77C3FFD7D77C3BB582F7FFFBCBA37A2C17D9D9A5F639AD679597FB38793FB54C8A38BADE28A34668DA88697D69F93C288B7A8B16E67730C9F845867E90BA5F4577C28649523FFA06C1A6AF22B6BC0D2C179F57E8A8928BC85832EE6568D261363CB692F13919755EB00120E686187DD80442225861BF090747E4F596C86C7D33F6B80EAB04F100E38D71C3D1B1035254519931F7D060F8A9DD50E01305CEC7E0F94DDEDD28F7EFFFBFA37B05CFEFE75C53BA92E1B82A3C6B8B731F5B4848EA979F17A449F4A9BEA3AA562E696CD869825D61014C5C0CB347CF5BA7CC2C8201DF8BCD34B07A18F240A82B24DC3CEBB1F0B1C8CE8C59BB1DCB8E8D1B346631B6E8F91CE45B9E523E71150C8354B99AE87E106054FD26E8904059EED755565DED6B209B943CA4F100711908FBAE39311BD67C411D0F805665E81A9D8A8B2275FFBFB3DC2B27A5C751551101488FEBCA2AF612CB49567CDA1E9CD37D3759784420E4EDFEDB0FC7B222B32A1559920B6A94DA4D6CD5C4F2571625D6C54693102A1D110A2EE2F07DB261B6DC155B00C70FE74707E9F4F4F47BDFDAEECD29268E2B67A552859A130F0A68D444D1E06DE46A2C9E3BE310A13A67593372B77B502B157F2398894339E45D7E3B5E06266A7A7633A085D7293140D49064C078147EFBC389B126B47BCC933BD6DF67AB2389F5CBC9DB2C995CDABE33DCABF46ADD5B7B666F207EE6AF104B68ABC40C9939C0AB7905D86FE381A157E91501E91D29CD4D2CAC4A8E6EDD01E6DC18BE06C6B349A89C362088B98A306CE9F0A2785A021E3EC587930D3C35F51D60FA3410DA1298104F271D4CAE3F45A1A18EFC48559A35023FC39EA8F999D9C568E47C7A3C587B98C6CC098C1E32126AE1C406E9CBE1A1396176F671845783CA437635CC7ABC9F435653543829F70FAFD431155F29916824899732D77CD392BCBB902E7E418C6BFB0E5919D9888A141A461A141F09412E96C07E09D06EB396AEE8C11DCE3AE3E042ED8439AB0DC70707A5E3A7BBB76FF8BE1FC0138FB4FC0869AA5CD7C40A4967A03703ABE6732FE1A693512CA420DB2D39705575B9EE5FCA790D847CC29B110810F8F3E22CE6170E4C9089B597272D23FEC839A6F671767185E46584E4EC604E4F4E51873245F4F27CF06632C4FBBBEFFE14CAE40F8D9E6181ECED05E74EB0A6AF7BC82035C7F33BEF982DD0EE76FC7DDDDD29A567F5B3C932211C7421842562FB53BC8839E6ECCB58570DB864586A31D8673A3257B48E79BC1D266C6DEAEDDFD5ADCB3BDA52C05CABB4CA0B29581E8E36672514D1CB7D4EA1296948D69F4E4731B86CD7A022F6662AF5F9E1CA8BBF8EC76FB7B9081E8F53E6F404A49E7F0488585F9F905C13C3FC7D8B28B375340CEE8829D2F274259F2C1D5C3C2185B1D1D33277878A60F8E4B83A8D71398A9D313F2D308C130653FDE60314A4B82B574377D275AA0E9602EACC217DA85625FEB63E0242A4EDE1012D9ADBCF517C1D9228EFAC2B58D83144E61E7CDE58514529CC337598EFCB23FDF64FE69C603A6F2AC38A419C1E9763482C0E439A36B709506A3028997C3277D3AA7E09660C34CD7331C646601CFE998109DBF9D2DDE61F21C103D077105D7D92BA2D194BE2544E9ABF8E009FCEEE8EE3BFAB6E922603685C4BB941F57CB77330F6AF0A36272BF2F474715A5CE64B10707BD9E1B7258E8FC88483855C2F9AE00EF38439F80F5A601BCDCC64DA6E3CB5AAC989716F5606C5B9E9D26A6B507C1B914DF79F571C6EC2C959D3EE9DC0A8E33B84F4F50BE0A8F15EBE9A8C74E50324B3E7836201E144431C7C319B30A90903BFCFB020C038A63C16CF961BEFCB0B8FAB4C45886CF980B89A99FEFE9C539414B6F008467C092CDEF94706568A77CD931196AFA29319BCE474FFB2C13827364CC510AE441DF58EE08382129CCCF30B709E55B72343AE1D00FF1C18C97A49461AA9591F86E8B62914E0BCC021153E72AD29800B9550F858CDBE65048764D64E45A99C2D9CA363DB42D0A85144E324D1861FDA80C0C6B1B380DBB138ED355100A3D4616D17572924C64A5AC91F20A68024F07C436B83D8A489988C0E6EF0BFA0AF03E2FE958127E9F16140D2D3FE3E4FACBD5F5D72B7A9D4EE85B80FA4141A58360E351A08CE51B60A9047D39D181CDC723D8EA27FDD141D587DE541253AF79D403ACEED7C5CDB7C5EC6C52603A6E0FFF7113BE163CACC7768424794510F62C99DA9C98760CDE4921C5C544A187264D4F233D5DC8EA070F117EEB0227A77F89D71474039CF3B7936A5F934ED7E7D11C0DBB0B050B4E9FB272552DDD74409C228FDCEDB10C4B418D803AE55BAF70029E39E6C961440A263F8297974B820D407E5E026F9EDD8AF77C58608509CFFE14DB2B14E7D9911CF7BE9E500C457E9410955467C4A0D21F45CECA9F67723AAC0E414442572ABB3C88546D09873C81A052EB8F84824EC02C42D4FF74C73D0426644D96DC1A809D12BB15549E10AF7A9A0A9C9B02674BE04C8C6DEEE0D44485A2C420EF3596CB134437229AF7DCFD125E261BA8B3A6C311ECD30138C488C25ABE9D11300BA69D50F0FA7229D082B29F1C23797E201BE1059D2F08BFB3297D4BBF8E9DA35E8EE150CFBC07A59C6728400E9F0D29A91DF8E3B0EFF67AEFE99EB6667EACC8AD72E2F29048379037D8E068D5911B36471366428AE9DE66AFDF2027659EA3E4382D9C6A6919058F28B31332C262FA72248ED3FD8186CA8E31B0C16A8B8B7674EC6A5F48DBD05A92138E3270739F42BD83D53D9B4A360984DE817C8AE5671E36F10953B201279B59BCE7FD9CA839033B67728E07E27C4606569CA54C4E2750398D99D0A33362D961C47D0E3C63BD5FB97DDF75A2EC4E0D09AFA6AE00A9D64D627EDA490D72F4ADC32CFADDB6330C1D639CB76D593413B78A38E881EC69DA72F7BF95A5A19013F9282C5438A34A69D608A7D713A44480360336B0DD47524CB0CF4E4BFAED0B9657A40446B75862164933D890F25CD68F0B39276AE2DB4F0B99A94CAF6BBA42BCE48C8533D4A97C15D941B29A91DFEFE0742C5536797430299F987AD8E7AE36978FED38CFE25BEE34C1887959432EC5B85358B5A116D3E6C12BC74BACA3E8C9443D929658958000265E7A76062C1D3B8DB16538E98EB8A735B779675DF015CD902ED772F12DBEB6D157504AB6CB9BFBD13F6F903D4191A53CAE58CA816F43884BE9C73B858A4EC877828E1AFB2C34DD3C9F2DDC7BC8D2123CC4668C44FF309F4A844C3425505F4EC8A29221255CE944C44282135AC413A44383C3BE3422918FF783644B577FD6EEAC3FA3D8E6FA9740B5BD2A15098755DB1B921077C140D39D329465CC0DA7EB10968073C319D88D3FB2737C3C1077126BC1A1F81264046BB85921E2175BA2C7BA5DC85B3A4857A7D497B20701B94F19D509A1CE195BA0E83C257111E75FBC995D084785B884EE8287664F4F10214F5F8C252612C16FC636164C7D3AA87882E490811C70A7208A33BC1F0DA7D7CE29744C11A306A4FA51C1A36E635731D8C2D98E881BC1F987E7267034B013FC10766EC604DD0CBE0F7072057B8EF6F6BDAE940EACFE6BE08C93D68D907DCAB5E8EE38152AC3E8FA5D3E1E753D96D840421CE7CBB1E4FEC23C208AA4652EE1AB6C2F21CE12589EC3EA2E3E025D7094BCE63902A839A2DCA9B013977A8B680841D62BA08B10FA740C11913F03026F6C65D1533D122CCCEB8EB3E838AE34FEA8D3848D3FB79D09ED049B94E279DDAD7AA67622BEBA0CB50667F0A0869DBE1B61723294C856F3CEB4D61367290CA427BE444370D77FAD8394B2B9B660E90E85F3A82F707A4435B865F567E1D24AD8D23309912E7CD423FE55453E4E57E038E938A368082AAEE04AD4077119541F0D8938CC558132B2FF3B4D50D5EFB2130AD258B4FE8B8DF46DD75EB44C0DB950E9436B3ECAC2A4B650F89CEFD43BBF1148A5ECF470A2D01F457ACE6B6EFBAE8828E90C17E2B8991E9C92D7F5755DBAA2C7AE4E44C6FC5CEEFA19724628E20E1BDBE905E09CBBC0E742C6D52F043CA6A0965324C53C43E0CA39C944440932B0F0C1671013261204913D3F190D39A0A58C45D250F4921D562271887A25F26C77D7F746252ECD8AAEA9850C5EB61DC5B48AF7760C58A22259BEBA40B78C9F8C20689873BAC9D95FEECE6F34585D69FDC23266A92147826D7B0535BDA5DD08D11002E8AD4C37E97188967E5797476AF1A45364CC993EEFBA059F875CE5DD9CB50556F2381F8562708E0087B88BF483E2584E2E01EDB954C7A0BCD3D3200A913C222C0C8D05545112E890A6402ECEA0854CFA0265271AD12619549350EEC4E77F00A3F62B791DC5E4A8F957F758D8414251BD53EA576CFC5A2D0BA78D893658E4BBF936BFF9CA703E1F48E9D1173B8334B59547703A19A1E510CD5D294E65204ED21D298DEF7CEC08CA8D2350704E45AB9B09605062CF90BA1029E975C24F93CBB31987398A93043E72826219BD7E329257E8221425E145B1B4CFA0128B20851097C32231BC3D87A5762E9A9514910965831C81DDE402EBE9473AE4A23114F2EF31E18FB4AC72BA297110FB35CE52C477A6889A1097FB6C59925638A5FEDE314294544EB65C8173CB58DA98F52025451C5EED132C9DD52DB9D987BB2F2B295C4B52A89482ED9D79B548445D3AA7F0475CACB8439108AC8C47C88D39E95474E9F567381FF34F071C48CB3B257B019C87908760FCF9C9EBEDF55C2F6A6D0F0CB575EEFCA18B53AC99B541AC2B6AC61D2469BD3AF5A96DF7DC48AF97AAF0AECCECA4987FAD6BA202449BEE3F1DB2A8017012634407901E03E73B53CD36DBCAA20BFD15085A3196C16BDA80C8A1ABEC24AF76C4BDD11CA748D8822D0EC5054A5153CA617FCF40E213E6A550F3F54412D6312327BD26646FE95C4AA4B82CB6DDE39367AAF3C98BD2DD294E543672C3E3251D49FBAE22E43736A9A513654DE10BB5B0E8DB5A1F493B26622D5C2ABDEF8C608E16A5ACAFFFABE5D8A987F7771C16B5029C97CCCE3D1FD9C6794F946E3A3DC85C0BF46738A5222DA6551283100D790FBADF538A1CB94AE7DB99F06CCA19A494A6B546CDD924FD08A550967B46ACEFB04A309467A27FA052305A1A8EB0CA05A927B35F88086A1E41E113CD56FA3DA53630E09A2B11B4F4315148D59A42D31886BC1D8E223ACFF3E4DB6DE380FDD5DC8B657862C29F83F1778F02C32909FD0A7BCB1CD525479E9DA115C136E9D66A293E4B9182AA3C29E21D7B7BCA4B78D03DF19D7A9BB03C816F22527BDCDFC1E099AE5F189F8CC9410AF950C17E3DD5BA26251ED89E94E3D5D3F180C51D11609D0C3BE0D605AC6FC1759E63B43D703A1A686D1CBC44D72EFD51F8EC67DAF259F127110F2AFB2D4A2D2810943B0DC29E354D696234A8DDAE4A68A7AB0F9AE32913D3C2D4B3C32E9D854FBA7CD6D7BDEF5CE541D577E2184A64FB30D43B57269D9B8697164E6E57EFA943D2BD27652B3C11D5A20494771F1DB01984E7E3C2C8E41557B8382BE556208E5AD5AE6AAF89F49DD02FE2E4298CAAE4B28419D49FE74C5C1BCAB2EC3E506D68200D82D20E818F71D817F144C240F7D879EF98AC365124F44643ED0BCB4B82E34C067DB75D0F433DD075195162C94B73F8FAA8833310B4B5913A518A6CAF6E78C310AC4E094DB6A1EFCBFB4EA7F3B63C9C7A4555F5D67B8AA552D0A79B614F4A0971F754ED13BA0C257239D1B088A11D4D38F1208E4AA63138D228060B40E9171F57E20BBBDC7F3B608B2AC8D18FB8A797D741E0F54A5AC8107F713147B294CA19123C73BB6575103EB048925602CC9BA8196073A534DB09E6D919BA4FDA7A357D326A363CEAE2D06DE44A6F69F320C11B386B88AEDD7EBFBEC55CE42B36B6DC02AF8F89417433F4CFDBBCB3C5A190D2FFC13AA1D597873DF59ACEDE864D2821A8CA5A2278BBA36036BD7724F33B10BC9F0E584E12557D0885FDD95012479F41F6B8CB097DB962D2F7BAC469BF8C096D5DBC0A585A7619482E931D54624EE4A38A072DB9FB4BE9E29B2E7D4350A21E98368388948E91A1A5CFDBDEB621BD41546A3B0ECE32B0D375F02A9C0F5A8DEC9493B5BB5F37773F8028B1934B0D265E8FD65AD832A7BB84DA716567812D9D7B5E170D59CAAEE80981A3F2538F28738B1DDB73DE029AA9C6F7BA622FCB6D0CE0A8F469A267ACC7775F3C9F3498D1FB9120CA33F4B0E49DA54B7190D2ACABFD128F7C07614F4479F95F6BF79A0B6EBDB1B56BFC4238E3FB0DAC396DECB635889AC4C694E15CFB887399A57EDD898D2DDFFC06386327BA76CF5B2813A28327957891084B6E51497969E17CE0C3ADF56C3BF7BD961A0AED864D47437D51F6CC12558137004619F2715519FDA1E275293EDF17E6B10DD7FD83FD36D10577D68391BBA19D85E273BA02728F5DFD5BD2FEA9EAC17E4F143EE0B7AB655A95115C3F66C3B28EED28D8495F4C1A6EED72CF649142E241D35CC88442DE6DF305633883BD55C7A770DEDDDEFFFBE6FED78D87D3446E7C150F674DB08DD8C919AEE4701A04B90D64CB5D7BB3C2E6A2E2BD2A074CCF69BCE1DCE94A129D8931A48FA4ABD8B77354CBB772DE504DDB3FB9511B110AAFB9E0930E6C06FD54AEE30BD75D474425F46E4051F780F28B173ACE89AE3A6A4BC33CC0F669F00F47E1B502076A198CADDF5A37A4BFB981B3C17D3A9A024E9EC47F7BFFCF8DACC42BADC8A7A150508552C156357E8593FE52755059A9A557CFCDA37D63839CCB1CD208D32F2D2D190F9163F26811395EF44D0E02957FF264008414D283C3E3ABC9DDA43F97F32219794517CD09C03BAEE2EB2DEAB693AFEB9D9516CEA6E5283651C9939527A1E5A341ACF0479026F87FE752CF560267E6C2D2B5DFF7F784E8FDBF6F25A1F69D7C7922F2D52A9DDE700B9CEBDC7EA059B94F407D1B9804170F535505459836EF74C7B75B5B5B3B4ECA1020CDDA9D4C16D7F9F5530142D120C36716B045B9B6F797CCB23E0492566A369207F5809B31F43D4E1337D508330688AF9F990F1395FA571C91BDED34185B4514024059F896946D2D795AEF96985CB0137B2BDCDF91C945F4C191AD8C490E6E23ED016CC570AE07381FAC53A822E98AF4047979CF8CE875570E295D6D2C873F89454BFF60F957C405245177F895B8D129D7E958B98359E92B06397C86B6D641D5D8462B3B14BCF819B227768D7B33650B17245BF7E97A428D07F51C357DA02EB88DE4218BE8DA7F7FDF13A2E43BFBDC65835B6F5D45DCB797C641319CF406A86802E77EA8FE27B1A2C2D6D6391A42A98443FEA6649E10DBB5C28E597B9CBC12CEEDEDE60B966E4320F44E08A8C90237CFA14E1189E05EE9E48F945885CCBC2760591BBD9F18E1B4A9C5113744B92E63F188AE37B073DDC2F99BE0BCFB79E38CADF683279A6D43A5D35FCB394EC2ACEF32B99E93D0ECFADFDC10D12C886C788AFDDDB7744CD06A84F34F875977EEC10E30B8FB5E448EC6850EB6EB7F2BB3BF1EC1E93F73C2D7C6115026ED518EBAAF5615F2812E3F5E05EB362D036794B4289C375FAF241350C3D88E19639A8BACEC9EC0593C2C246D175E4A12E2921F25A890208A2CF45EE4E661F7A4CC5761636FF1AA1FAD42B4E1D7B7A21F298F778AE471894C7DF4A84596234F98BA5DA369141315E1AB0480F295BED50D957399D8237312B28D56CCCE482152637B75B9AC58347172A583732B89835A319C51588BA2CA81875395BC8A57C07B41B26E51FD736DDD92843CC95DF645DA069B516B826AFE51ED171B4CB7639818A7C647C7BE9E47CF5F0DCB242C721C35EDF05E0ACE95A0822556B0D0AFB7F236668BB8B5D95914B2D4D43E8442742CDE5D549CC8AB9FF3AD5FB5424A94A5C4EC647B5B48E7A3BACFFDAE63BCF7E7A6EBD3D8A51514CC6BF7DD57DEEB7D6CAD4690C28B9BBA6E393C1C1BDA305CC7DEFDC75BF1E76930032953EBECB471EF9601B2C6D1487997AE58F9FF3E58FF7D773F7B3EBEFB74CD9D094DBE538DED3D22DB8BB753318C21EFF491A4BFDD691CE4AFA55872AEF22FB4BA8B98B0DF5545548A883BBEECA7582674AC53E77F3BDCBD0E5D8ACDCDC4E1A78DAF484BDC66E35355BB5ABC7A2B806A1F1A6D56353329FCD0916880BBE2AAD399AD181404297DAAFECBDBF6D1BFD1C92806328A865846F8CFDDE4742CB7DE85A006CEE42E6C04C79B18DB7563727BBEB515D31AC3C2153B3B23332BD9561B4C8B93FB157F1EFDA8B1DBDFB0D9B6BE6D34D2DABC73237967D353E2616BB2E1798DBE69F6690322972C396A165E3992DFFDFD9F7B81F3F7FD7DCB0A4371C62222DFEDF8C548EEBEB4B5152E646F78549D42180908819D1A13494C6BBBAAEA35D4F03FE70730F736D08C6F68A48B7B5BBA4EC3D0B415BDC1A3B2D14AFE2FA675AAFE34D806E5567271CD7A9B1F8B2CF92FD410D56D4FEC38AF10DF7AB9D8A07EFFCFDD7FDDBF5C5BFAEA1CE58A0A259D28D64B28E4E24FA744A4526D2B61E78306445B9B2D910CFD78BFB8D69306870D87F75BE10E5A435AC3CCA295F4B74576384134FCBF6AA83461B9D1F4D71BCD7B8DA609A2451C0DF9015FA5CD414D0DE0F63BF6BC95637636ADF94E07E7ED8F2B3A86BCEE5CC702B86BA5719029ADADB2B40AE7837599A82785A7EEC36E803342B1C6953AB1A2FB185BC25A2D3E7AFF460C7378254BDF9F3035F9F5A62398074BEE46BB6D6682D4B217B320D7F94E5BCAB62D1077BF6EEFE9DF7FEEE8B8FD79D34A857807A70C401DC8429C680A4D91B6BD6F66B1E34C15BE96417484E1797EBE706947983409877550135C9B8C67722B1B904B4CE86AD874355CEDD7FF006A337723479B3894A67037B3216ED984A5688D04E1ED2F3EF884B29738BE559AAE5D7FBAB8FA70C1A3462B3FF734049F8D8E337A22226AB6D6F55FDFD1DDCD1C30701A76E6F6894EDD5853FC52F7768D37DDBABA3F00B059437DD5D536577F8C55189BFF5A132F33D7499BF948C583EA8D99BCDE7B585E7D5D5E7D592C2FF9F8BC189F8E4C93A631B6CB7793C5F914ADFEBC3A53E1D4854BC62A5A13175DA5666C999DDDBDEEE8D9D0F5C6997AF8965F89E617F85BDAD57C52032D6AC04796D338DAE89968B4C34DBFBEC2C0AE2477DD4E7833EB3E8C03D2A3184E122C83FBF46EB5530C9FF467E7D3D9F904C75B3D32ABB37A38E767A38BB309279D3DBFB873558A92356529C1CCAE9B746533C3F8593FA828592A1A25E9B149AC13E20F466FA525CCA293C486D72D64236C7FF6A90D8FC20ABF601175A0FA969D08CB24D6E5A3BFD79BBC1C8D4F86D1714AF7B6DB606C79B372DD69C3C7C7F9EAA433DB48A969ED6DF0A37FB5B00CEFB0325DC81ED1ACC6CB3F1E8DB0D5EF75C37BB2F4470D26DDB8DED45AAE66E1AA6F63579DBB718835AF199DEB9D71A3D2C2682106757A3A22F046C783D18B01BEF2C9F8C590308E053F61E79B11FD02045B4AF6DBC12AC62D42AD66381FA4916D1DCEA83ED3D84556F75B8D58FE992B2B78132D3D4F09FA7F606A7CB4369A3E49F288247645705D01A7B3BA064E4D6082119EBC1A298AE61038B348F0032EFF03166FB4B5F83B419C0000000049454E44AE426082">
        <commentlist/>
        <creator name="" timeCreated="2011-11-16T19:33:42.308+0100" uniqueID="1440456d-cca3-41b4-8d34-78f071030550">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.028+0100" uniqueID="dce9e211-5fdd-46f2-aed0-428187e4a541" toElement="1fa8ae9b-9e0c-469f-8258-ad8d55b13eda" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.030+0100" uniqueID="92b82e8c-423a-4625-a761-dd77edd78030" toElement="1fa8ae9b-9e0c-469f-8258-ad8d55b13eda" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.055+0100" uniqueID="83a32876-d3a5-4de5-9d8b-33a552b72f81" toElement="a9ba9130-b3f0-49f9-bc6f-7275069345a9" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.420+0100" uniqueID="71e21b1d-e056-4fc1-96ee-c77df4dbcdf4" toElement="28a2841e-e1b9-4cc5-9a98-3d2c9bd896f5" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.144+0100" uniqueID="78360a5c-ff62-457f-948e-29f18b3c5b69" toElement="bc51e4b1-e551-45a5-8df9-c9e640e92434" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.178+0100" uniqueID="cb88728c-d606-492d-b072-e99a093bdd7a" toElement="1a423134-72a7-48fd-b5d8-26ad25900512" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.305+0100" uniqueID="4877763a-0075-418e-b2e7-e31c207518a3" toElement="a027a898-0f20-473e-a0aa-25ad6bb7665f" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.332+0100" uniqueID="85feb82d-39a8-487b-8975-43e0b7fa5f45" toElement="1cb60663-ed14-4d87-a38d-d12bb968f391" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T19:33:42.309+0100" uniqueID="d5fdf561-4018-4852-8eb5-e8f56168d1b7">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <storyboards timeCreated="2015-01-28T20:44:29.309+0100" uniqueID="6032238e-00d0-482d-8996-490b5462603b">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <introduction>
            <fragments xsi:type="text:FormattedText" text=""/>
          </introduction>
          <panels timeCreated="2015-01-28T20:44:29.309+0100" uniqueID="bb91da7f-77b4-4b84-9528-e6ceddcd1150" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.309+0100" uniqueID="4a9cf302-e6e9-4cd3-ab87-2a112dfa899c" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.310+0100" uniqueID="623e098d-4548-43fc-9226-7c09e499f6d3" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.310+0100" uniqueID="0c4b6edd-737e-4ae9-86f9-7f9ae22da2e8" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.310+0100" uniqueID="2a0333cf-004d-4f55-b03b-4a7c4c1950f1" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.310+0100" uniqueID="e3fe614f-7402-48a9-a990-c0dc3fd82c48" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <conclusion>
            <fragments xsi:type="text:FormattedText" text=""/>
          </conclusion>
        </storyboards>
        <capabilities>
          <fragments xsi:type="text:FormattedText" text=""/>
        </capabilities>
        <constraints>
          <fragments xsi:type="text:FormattedText" text=""/>
        </constraints>
      </contents>
      <contents xsi:type="persona:Persona" name="Pia Sondergaard" timeCreated="2011-11-16T19:36:09.114+0100" lastModified="2017-01-17T19:34:08.030+0100" uniqueID="1fa8ae9b-9e0c-469f-8258-ad8d55b13eda" relatedBy="dce9e211-5fdd-46f2-aed0-428187e4a541 92b82e8c-423a-4625-a761-dd77edd78030" workPackage="" age="57" job="Language Teacher" pictureData="89504E470D0A1A0A0000000D494844520000009A000000E8080200000098776A8A00008EFD49444154789CACFDBF6B1BDDD6FE8FE74F7817DF22C5539CC0296EC35DC49022823411A48820450429227061840B235C98C18519DC98C185195C98C185185C18C685615C18C685416E0C7261181701A50828450A1577E1E2142E9E229FEB5A6BEF3D7B467292F3F00D0B319665479ED75CEBD75EB3F56CB0360860EBD63682502C100B378368338C77A2742FCE0E927C981627F9F8BC28AFC6D3DB72763F79F8367DFC3E7B9C3DFC84FDF3487B78DAEC771FFF7958F45DFD0D0FC6E4779ADF3C678FF691F683F6D33BE65BF2ECE1DBECE1EB94F6A5B2D997893C5ABB9FD06E27F8BBA637E5E47A0C2B2F47E5C5680C3B1F8DCF0A63A76A391FE599D1292C1F9DE4C571560CB3E228CB0F61A9333CA3561D0F8D8DD48E6179C3DC6F73AF979739CBF97802CBC727FCDF71FCCC67195A96C63683702B8CB6C264374A0F920C6F053F8F3FE362843F757A57E22CE04CE9A9342C7F8DF3D7B688E5429C8FDEB76A8F3F9CD5703E3A9C5F7D9CB4079F257192A5C17965719E17C61A44CF6ACF08CEDC70527847696E412EC679E441AD40CAF130F37F5585D37F543BA9EC5913E4A628522846DBD425A5B91F678709DE1C2F84F35125CDAF469A8F7F22CDDFE37C70D60069645767A94FFE6C82F4D4F94DEC97386BD2BC33D2549CE5E578314BDFCE45B8E756A327A2AA6193564565A1F932C5F1F1A29F1A7A2087759C0EEA3170D2A90E16801496C95E9C1E084BB859FC8AB3027FE4E406D29C18697E9FFD749EF68FF4F747EA7C5CE44E6BE6F9D8DFE0FC56C7B990ABE09CDE799EF68A2CCBF32771E25BCE0CD433C1E9881A2AB9B5DF13FD73E48E685187FA4C8225717A20A9483A588997193C86F8D8D19975B34E9A7AEDE36CFEF33B9CFF98C7C73F76B68FB37A38F45DE88F59152FED6333702A4582FC154B4354A4399B97E659B1002752076382F3C211B5415423DFB06E753FFC9449AC9563B8E5C3DFE1746A76385597D11671C6D6BB562CA14B132FA14BB29CE00FA6348153CFAC950850FD89BF9D7F41E3A71CD45FE0B4FFEFE3029C9E2E1782AC584E16E3BC2E2B691A7823CF04E4C5A866AAD1B31123286C2EA9A9D35D44F1C82550B534AAF0D2A879A20AD2B76786A5F3AEBB74B0F4B1FB89F1B1F01EE76489906958DECB59F8664EEBCF3F8C9DBFCE959A44C57E91E0A8409FFAAE73B05FFEC86A81D3E234D9ACD55F13619DA571CB367C4A5EFA34CEE182E898FF16A79F0F1F1B4F5B54074C549FF9BA5496487C0CCB234965E9638B4A97F71235BF5638AB9CE5F7A1F10FB25FFB9AC7A640EBC9AA13684391CE7C21DE5B6BE4B1CEEE14E7C404CE4B5F9D4F83BC18D5226823760E9F62B908ADE23CF2CA9B038FE89C462B73D26472933EAB3258C7527C6CA62C91CA9ED5DDECBDF82887B326D087C5CCAA271F3C7B82AB7B65337C7ACAAB715D90F8546ED6B0B4CC7C7E7596A6E2546902A725F42B5D2EC4795C25B71E4B9F810FD57BA6D2683A6F8668EDF7A4C68C3415E7B6C199EC453ECB5CC8CF45CD724E9DB6EE9C5511F4D127BA00A447747136F4584B6E174AB081D613E56323D9F180D1EE1699EB1E5C89342F2BFF59E53BBF00795E959E4F46CD63CD867E695542B4A8115179E6B4625941B538E1699D9B95F427A59BC50FE3CDE15DE2AD5F8D509C34703E5A9C3F6BFE764EA37F8EF39F5AF55993E6F7396C4DD7AAE63BD8C943CD919AAC95763B6726A12D8D9BADB21B4D6B47B56CD67CD7A4B855D2FB044EAFCBE3D9029C8BA2A915ABA1E8DB3C518373C7C329213357095738C7469DB78AD3F8DB1ACE05CCFE4B9C0D8D56D2ACB3FC3A4FB1EE5D1DCBBB5AF9F16B9B78096DE9B7F49A7D038BB0D924CAB5CFE7C2E7AFCD717D324B5A5CC9FC826802AB62A7F5B469EE3CAD044EA3CE6BE91EDCD6C2A715A8758C8B990995A7BEB5387C7AEA5469368538AD3955BF02A985C34A79088A62E3C95376656ACDCA79FACDBCAAAB97DB9E6D652365E9E36C40AD9EB4CDA39339BA0DA84FB094C6A13968B024CE70CBC54EEB69870667E1706AEC940682E921D8935825998DFC76D624BA18E713D2F4DB78F5D0683DEAA26E80C97A5C3874CAB32D75476E72650FDC977EADD9ECB61B606CBB9FC9A3E5E71FFF114B83D359155FABA469516B3037B5698DA813683E4CD49E99DE1EC2E75E64FA064715CE91C559EADF2C029DF902F5D4E95B3D94DA70B890EBD3389BEAFCFA542939A9D2D7FBBA2E7D451A6C23C7AFB2CB3ACB79E7F97FB086835590A70D9C62358136A55915A34736333AF22DC97D9C66216C2B6808D4A642B21C762902AD4750496E4DF8F497357E3EC1F2495BD8E7ABE17C58D016B8776D9D5ACA3343B206AB7CAC95A01EF0BAB457A7FC51BA6662FA794F8134AB1C732DEF05086B4B5D454DA01ECBD306DA7A40FD7DDD92183BF2CCE21C04762DCCF6DC6B021DFB02F5ABCF0A675D94B5F4E7B106ECA99CA81E35FDA5B14A9DBE34EF4DB253076959DEA934C7B04A7F6EFDB24E51443907F27801C2E2B8D61D6DE294D7376B7CB7EC753227D079A2273E51FE6C3EB76EAA2CB9F07CD0201AD386B1C1196C5AA2DB4DA28C104EA0D7361BAA25B77E1B612EAD7DAAC95EBDCC4B885C4BC8C7E942A6655947383172944735C5691DECC8C5480BB2A856BECE74215A4260754E4D77B4D111AD2F2ED644592C66595F94B61A1D2FC059F857466D0DFCA0B28C96183B34E6585638DD62755825BA8D66421DE7A2D8D904B3B8087922BFADD73355D1F97D41C7AEA9489F6295CD2AC2D1E46A649DAD1565AD58B44EF2C4AAB0EAB9D41A69E6352EF139FD2F59BA9C6841625C13A8E2346E16FC00C23787F320A61DC6D93C4EA8D30AD44C204490E9AE916971928D6519613EB9AD65B6BF2825FD9593C5381F16795ACBD26FF1DC37413ED922B8A3D9DCC746CD0BB7DAEC473B0F49B3A4D39ACD2A5213DA3393E2FABFA1E16CABF98F3ACB05EA3C6D66B92E6A66BFC529448D408FE2673A1F1478A308E166A8166D19990AD17C649BB726B9AD979EB6B8FCDD0259A3175F77BFCDC6DE772F036A84C9DB799632EF725FCEBE947A30BDD3099872A24A356E56A5992BA1A202696B00EDC2781D511B2CABCAC4748BCC8850334BF242EC029C0B587A380BE7697D9C7B628E25B9C63E4EA7D167033BF15545D04D87D6123D70AB2B42D476FB6499ACD64CA8A9ADD6BA9BEBB02F6CCABB6912D70CAA32A066D6D310E5C3D7C9F476AC380DCB5BADAC00752CAF1F97D796A893D729CE6045D1E68D2A502B32215151D4F65E157D7303B59906CFE5BDA782F3AC18F96EB6910A0DE770EE79386B1A8D6B2EF7701E677D8CCF65BCEA7565F4CBCC0A558BD85F6A2B657632A19EE6F8D81634E59F58B2F687F07C9CD22E773829D0FBF2F1C7548488B744AEB3AFA5D844943A56BD52A3E27245585ED81323C5036BC2B24A5F8D7715782E1F76158E974F35FB7C16A14E29D4A6C56A81B3D11EB2692DA539E76CE759DA089A11E7DA60E04D65CE1B886ABA9B48726426F9AE16E744B5093CAF69F0D850E1229CCD05CE9AA7B5ED9E7B5D1531DE559CED181EE2E19B5C5BDF2738D0C729B8EAF16CFAF0634AB4F7E5E47A24E566DDD90EB5C7AD1AD509B76C7C5154208D1C0B43B151EDF82DDCD362DCA8594F2B2D36DBBF953A733305E8EA4E4D6B2B9C0BA1C636C5ADFCADE074447D90EB6272AC2ED764463200668686EA0BDAFE144F63F0AE3634BB28B97D6C2CA1D8E530C1E92F4C9A44CC044B78D72F8437FB3679FC674A783FC4F4E07B39FB46E397621071795510E779313ACB0B87939729DB2EC529923E92D6706B7AEEE29FABD6180A9EAAE65930B9D9EC4258152EC6E9D727BEB3559C86A8C5B9B7282762B912CB85E8E1AC11357A3544B5C310EF9AEAC556A2B64364BB0ACD85E51FCD3E51A353FFE83FD616AB679AD33E2CC2292683771ECEC7870783935AF4587EB792FD4696F82E9C3063E705711AA206275776F579ADB389D34D06B9BED862A2DE32CBF9D338E747737D9C270D9CB6CADCF7B2A15A0475EAACFA09CF06ABFD26D1A6198146E272CDF0B4B770A625416D4AC15F61AE242BAB69F589E7C600B401E9A4096BB6F124A12560AD46C6F0A28F0FB3C71951C11E15E7F7D208549FB7EA04DDC7EF8CACD0A88307180893E3CB02FE165F4E90319D17506715235D47ECBADEB2B773D5B57598799C272E7C7AE5E6229CA35A3F287D3276FE02671F38EB44E78D73D59BA1F68C54A0EC469E15EEEF9CBAA5D0FB6A3CCE740117A09DF91ED81BA69DD59736DDB249A5CEA93ADB2F3C20DDAF64F628147F3ECCE418CF94C409697E192B540A97E193F991BE46F35E785D1528325EF1AE23B859D565E92F3CB8BFD10DC8BB35B5ABB16B163AD9D52ADAA7A1BA5B219EC4B9B08D50C769BA7DA6D59712677F7560702AD1B5055679DD6D5B893A8D5EDAC5165BC51BF7EBD0EA525A55D2FCD2DC6D08DFA6B59CF65E02A71494F22DA1F8CF6CF615A950C9E31F2E70E2CB1A57DAF7D288F507C50AC0D37BC0288010C90EDE3C332339E07CD00533A689A7C8E65A77FDAE87AAF1AB38BD9CB946B45A19751567BDC7E4B7845CED5475F84CC8CC5DF559E1340B2CCFFA2B7DDAEA628AF350598CAAD73D9016A06D948C74E8CD9E85A917566BEBA3CDC12D0FE1F7BAA7B58B9A7646ABA4D7FD2A3EF34B29A9137DECE42A870AA7B7708FF9EC7E048A3F1F80197919C017B3BB7C7657E0F9876F6393227D376845B293C9CD08DE75725B52A99A1368FF4B4439F5413627517C9CB5A02893D3B63BE113755AAC2D975AB13A8136264B0E7CABAAA95A23FEB089132E773070501B07FEB7D60636D78DD3BDC4CD1619BDCA19310DDE1A570DAE661DC62CC52CB41F9E3AE53EAF99F1B7C4F9F87DAAA9CDE3FF3E02276437BE48A7773883F1F8349E5C6580F7F075FC13A8C4D9E25B240DBBCE704CF72BCE96DA0560E81832BD1BB3183DCD6D98F4DEFC75AD53E19A50667DC679DA7A377FE46E25F3BB7D0B5A0AF9C8E2F4D4E9DFB194D5FB1BB5814DC3D52D7FCAC1B39EC55911F58D2CFBF34407EB835A0163878CF8EEF55DCE4375446D587506484DD556B7F04906F445ECAB80FC6706A23F1F1E180EBF401F282AF2FC20C80E06C53002D412806FA1CB627A8788989757282253B5F20AEEB4D0980AE7ACFE19BF70FA858193E98F6D4D37E2A5E09C4C7D9C2ACD5AD1E97C666D0E76B4C0EBBA3106CFEBD6C32771D6B83E39B3592D651F0ACE9EC5D917667DB1817D7CD2D4F76E0486A80ECEBB76A0B4C4CAE69C516D92C1E64A166A3D6932B0CDF2B898A433A0F8F33F33D621486DBE4055312866FB8364AB07CBF604EA79028A6AA3F3B4B293787C4EA8E575A1A5EAA3265030FECEA979B76E4DEDBAAC13F56F6219D556D9CE1AAB2B559BB018CE39DE4A9485DF36F2C36773BEA49AAE9E6379584D0F3DEB7DEE1B817A50EDE362907D4FA97AAF52A2EBDEDE68A73FA35B65BF6E58D9F3BA8E6B9509FB329DCD7EFEE78125A3B8594D62257C8EA7F7503FF217FC7761B2DD0F573AD15A27DEECC1D2BD607406392A4BA8331B5FE5E3EBA2BC198D2F11E65196E41368F76E84DF0375FE7C7C64E58AA0FB9F992CC2D8C0D9F0B73773D234C3604E94A956B1CEAA85B6DAD2B7174A170D3F145EE89D47DB50A4FFDF01670F44FB4DA28EEB409D707F75B1BF356D7A5D4D138DFA5E776C6BF06A2AE5CEC3F9B5EE75BFD6050A51FEF380134D4365A9E1F05B098A7098E5753EBECAF28330DD1924B05D5AB4D60DD7BAF1763FDEEA87EBDDEC202C4E6421F0382E4E93E23C05D4F216B90F0D74C757787B19525C4245A98378FC30C3E3C36C26EA2C273ED16B0D99658DA52121E7FDA8D6C4F770A6BF72BCF3E60D30D8CEDFDC24D842676B71F69EC0595905D2768B54977AF720FDED4E2C41D44E5DFB69D1958FD3CB89BEFA6E764E97C85D0112FFFEF300EFAAD90D5832CEDDA1E40FA3F55E7A106647700C70F88370BD07034B7C89E7E3FD3058EB25BB01586687517E02B71166FB4171825310953705A1A2FAFC5A4E50B15C1780AA058FFC770F80FAC4F0A66D1A98FAD20AE8C88C7DA839A24DAFDBC8759BA37E0B6C0EA7B79077D4846AD459F9DB26D481DF671090667AC1E641BC75D05966A752B48CB3AD4E6FDDDB1B4CA9854CEF4E6966463FE000714E01F3F1E7FFB281C7DA1F0E569255508936FB20549C24E97E0066E15A2F58E9461425880EF05D204C8FE270A30FD2E96148F0B25E9FEE877C3C88F0B330949E54E757B6030158AAD809AF1E10FDF973FA6552559F97BA62EAAF7E5B51D65936884A60B3306AE9EED3323DAD29B5BEB49E15BEA7AD0B7411CE85D2647B2870BA0CEB692D44A996B9D5257BAF92F5B4B5D8E9A6AE6B38BD0602E3D9FF3E122704FA1F385E099650E7D771711C518287617E9288F2F03606834F9DFE87B61205CE648FFC927D1ABE8465C30820216558BC1340C1F9317E3C199DA6D24C28D8A9FF212B6BD2B227D147D1A8373F56A53F662704A74BC1B95FC7A955C46195AA14474DC7FBA4344FE74687FC49097F30B3AA3B93CAD936B32177BC429681D9DDC463C955B3B0BAABB75E7D567990EAF266EC778B665E4BA12A409D34BF496BF73F82F3612A79104EF19851F3CB08B150E538BA48110EA13304CEC1E76EB8D987E060D9719C1E46A008D8C569CA5540BC663F4C8F22E23F493320BCCC21DCE4208AB6067CD9493ABECCE06FA14EF68F6653F6EEBF720D55BCEEC38CEB30CECD566D3CC59937D48964D0581DAA9B759E1FF25B548FD612DD45CEB6A8E1B43D5B83D30974D523AAD2AC1644ABED303C6926D981BD00DDB0B5BCADB25A44ABDA43E269275E9552EF07FD104FFBCF14CAE0E3030B12F5B1502793D8A3088EB490AA83680FC2FEC776F76D2BDD1D94F723B0A1280FC2DE8716DC2F8E8976378877034813B0A957E87288CC2803DAF4208EB61177E17E13A4BBF871943174B9C88CEC421B0A5CCAF4E7233B47E7551BCF1524359C2EEE28D1AA7153F7BA55A2FB84A7F59B44B539A6ACE66C7D9C62CF7A9F549DBD859E56A366B5BB89DD0E83D294CD8652DD3CC1DDA2E42DDCEB7D67EE2EA599DF9AFFFA4463084F4213ECD2A1289C30F7A18F1D8FA52041A8832F85CE90A02270C2D3824DF7CD72EBE58BD6CB25A4B5C85AC59DC6F0BDC1462FDAE8E3581086E26025821E27F8292E109EA6A38BBC38CFA3DD102136D909E14EE07E8BE3647C46A85A17E17AFA399BAAE3854CB96072E29FE22A8C011EE30ECEC96E04AEC039DF842BDC45BF30709EE6B546EEC9C21A342DFCFA447EBF5BF27C2669AD9A93A30369587A2085A54B82F6EDA62627A66FE026725DFA63729FFAEE048F4FF4F9A46F50AA212B99208FFD062F974237A823237AD4102CE166514AA22601BCFEE776E7CD72E7F552EFED32248894B538CFE2DDB0F7BE3540258A9C489C6DB835E0A5A00EF928663445BA3B8C519572C9FA7A941CE2C94000670CABC378723362D70235AE5C553FA586997D9BE0AF73E737AF9CAD9B808D9565E6B3AC22A81F3817E541B591CF86341728D2828CF5F1990369F96912DBB7F172E0ED85E1B650B0515359EA0DF796A5BDF76131CB050589274D32964839FD8AD2B0C0491C5F22FE210B8D132438FBCC4E8173748EBF245454780C563AFDF7ADDE9B25A4B5709B808A9A32DA1EF4DEB7E18AF9B20329667651220FA4B681B29374CF64492C4C4FD2E22C1B5D14E990C9D1E83C872720EC3356A56E958D09DACF9FB3EF532750C5D90863CDCC762E7036C7FBE6419EE4950FB03817B3F487F988D3815CA7491D5219F7A821C520DA21457B27A8BAD918B1B738E658C6F85C57F08B893AD89BC682F6D30B9FDEA2984A73F66534BB2FCA9B9C4D9F6BFC0D21B21E15136C74813F2F86EF65FABA3B800B6589B9DD0F3EB6C2CF6D32C32BE14E912B5DE6F86EB0DA85378EA1CB5D5C8E7DB85F5024CE61924BA2A4352B9E2C3427BA2AC637A3D16541BA102EBC02A229646AC68E2646A3DFA7360AA6358D36B34DAF3D749436A3E6AF2BCE7969366E50115D0ACE484E8EE054908E62B8696D4B2D906421E0CE096A72CF7D76E0B9594E637879ECEDD8DEA43D59DCB75BB8C6F943D4F91D25E0787A8F5A7034BE804A58568228DB78BB24575E6592D0464C73F602A4B5C569CCBAF3C3327046D2124AF706101F0EE0A2F1E683CF9DE0533B66DB88AF2751498BE86C8F62B8D66427C8E4B7B154DDE79325BB4EEC1921371E213F62E44EA777942933DE7FD83F9ADC8DF12753A07E9E39DF50AD4A947A1BE1B846EE499CBE9BF5EE51F14632238B93F62CD8E83B8AD6A3D68CA2DCE5ED82C93EFFDAD48C014A662CD2B4C33555EEA333925C9BFCBA28777D02270765EF91558E2650E7553ABE003676E9880D30F611D810D510ED20BE01D40683ECF223FE1931129F9576B4DA9626D10017010E105C0B263EA168B7C36ED11E2F8B786B00D349467AD48B0C7F1AE94AC8C4DF88EF022413A2BB31BE3BC27F2AD11AE5A94C358C1F911FFDEF63793DB23DF77A2DD8ECD9CEDD1F317FBBD2823EDFBC347D67AEB590B0F4713A2D8A47A58916D5042459862689501FAD77131EAB343323CDCB42D798AAD1A1AF935A06FBBB2184197BA723149788976396957006034AF320A0E60E9179420A1247E13CE12437FBF17A3765E1C1A42659EFC46B9D74A7CF8C77A7AF828E3EB701867DA2CFEDF0533BD966B728937E027E3CDD955EC4112E113A5EFC2AE0971429A1EE8FB51D38622BFF8C75110ECACB9C230DDFA8D187D94C268CB26A64BE7953B4BFB94823097250E76E356CDC645153678513975D4675856C5E1A9CE133A40C0AD220DC23BC9AC13519EF24B3B90499989D4FAC34EDEE2682D38E80B824E8A999929FB39937B787AB1EE94F8E78C9F4E7104AEA831303E436F317896D1053C46F117314AF76A2D50E08A53B033E0F116FF5013513967CF24854FBB90D5AD17A172F868249749F7F7F2EA7001916F0F32FC2857296A63B01F0E307C118573DB816B88C2E73502C91828946E17B95E8CFFFCC2050BADCEA2EA5E69605F97051C3D6DDE164D7D416DE79E892E7399CF11C4E63CF9C16959C9A6A91F1490F0E8DB1ED79CCDE0AFE66C6154F9A12386BEA74FE765EA0045947CB41F5FB6272479CC571084BE951E1157B20911FE35C277C3C8ED2AD5E7644271CAFB4E3D576B2D9C5CB32D5317CF24617363A06DD41C635ED30D9EAC52B1DF8CC68A593ACB5C38F2DBC00EC0915723C493239236C21ED07AC689170E1AF930214A2E7B7B8809A8DB9E2968F4FB9EE36B9296428A2443DCAE131D51F4FFDDC0E14479E3AE7C829D4C20339AAB3AC49F3C06B3C3D89D329D2077958A398AA968F0CCE42708E2A9CB90D9CBFC039FD239CB7F9F83ACBF60730C43F652938513330948219611C0438C509A4B9DE4981137204CE43100AD2ED5EBAD1CD77FBC5309427C374BB8F1FC9F7E99C933508B4137E6AE149C2DE0F648D456E913C64533E3FA016D93662BC4CF52E3B2E891F45D4E529DB4654EA6536BDD33E0327B6E546170FE7D11C4ECFE52E74ADCD1DD9FCD54D972D1FF851933885681DA7A5183A7EDAA7D6F68A22D4CEA71A5D93B443C1D2E0244B0D9C234D6B6B38BF79C9AD8FB03E552BCBD105588E4E2232DBE9C1A9C69BBD086ADB65FA8A270567C09183DD3EC909CB6C9B43080C990073CC9FA52E377B14E86E9F8CF1D7A214D942498A78D9C3458010CB38BADAC905275E0FEAB92E9F210C83DC59C2F5D161AC51135E97BFFF2080BFC593E545A6032BD3DB914EF64EEF4B97102DD85F64AEFFBEF08EEEBA8FF596C19D2EF71D48C372114E0BB2C6AF6E125DCC3AB0C3A99599E0CCEB38C70B70D6D5396F089C92CDA6E9FE80CC767A4867A2F54EB4D1816BCDD5C066B7978225001C0429BEB5DD2DF0FABD3EE225CE388E0B71C8F86EBED51D9FC5D94E0FC2421C354491F782E81A72601245F40578CEA6C82F842209155A3C890BB8DCF3D4101507CB147A9F440198E32997488B32A46F3A7D3FBE1AE92687B5D2B3BAE5BDB9D788072FAD0F1ED4B22A37E265589A9E7044539622458917B8708367951C1BF01AC6E588CA78F19EA9B3CD142757247EA1CE799C66FC47D539A19BBDCA38BE754C690AD17EBCD9456864E2AAD2DC87B76C73CA0B2F8043DEEC167BFDD1FE607414E4D4EB80240E03E812840ABC6CB33B3AC14F913128428B39B8D221D3EBC674BC6D786618F320A43F2250FAE19D3EB21E66D12863D8973744479225A16EE12CC4455A72962CD3F125B85CBDEF05E658667E7FFCC8DCD8B478C32EB7D0EDEBBBAE4B0372373238F72445A505B403DA33A7488B6D8E9C6FA7F6F154FDAD4EE2543827B723ABCE1AD105A9D0AC228AFAA4BC419C40568977DC871CF1986C7561B8E2081205E8112926EB6D0834DF117FBBD51D1D0C4687B06044D8C0D9074EDA302870052082E2FAC0AF3D9258AB8361704AA2EF6CAB07C6C8A7403DA370437C8BD7C13167C9E803988BF5472731A73ECF1835193EB992938C8E13991FA34627D719174ABF4F90E522D507C2F2BCA0A46A2D78D7BC4D9A1B03397E875E3BE9D0064BDE9D22A2DC2548EDE4A0BEA2EDB1F541DB0F1CD1673545FA084FFFC088337502F570CA6DB38EA86BB8FBD2B4027DF83E9DDCE1BAC6FF0E1506EA6C5338D5ED5EB4DA2E94E530C80F06F4B4EBED7CB7071F4B5490E651001B1F06E3930856ECF7E1782947010C96F9760FC8F9257EF98E50DCA5F8328985B96A112CF10BB77B50A70451BC3896B4288088019520CF538AF2827F2CBE45DF7B1849F8CC91134DEF0A102D65A01ED1273F4ACA8BA25AF8AC5AB87356EB25D9916897F554A2244ECED7D14263EC0770E9B782AA389B2CFF04245912A7B8DC747421448D40C75E042D3D814E9BCE56640A6735BEC40504598474B60878078374A717A382DCED4B48436CEBA7DB5D3E225EEE74B3CD0EB04194E323B161089625881E47102BA11EF48193207171ECF4D4214364382663F861644F9B724D40ACDB7D1E40F15B7C6624EA1CC9BC2724CEBA08BE97CF24F8FD902328B2BD7099EB8C2067B5653996FEF60259710A3181A8B78E5D5FFB5C88D61F6FF77FCAEA9256C319D076C5F606291B61B46799CFF237F09CA5F50381EA6BD4E6447A6B7BAD5CB1CE9626F7014EEFF21C85A6E0CC999A32D5043CA813B94F4E3F090D21896D4BCA03C7D80154E2519CC3006759D53916A2A0CB9C084E580FF645A3FBFDF149C8B00A5D6EF704394943E27400A088EB06AE185179B78F828421538842A9A2F880E92E2E17FC7E289599760CA572A6907F3522E818FE16CE162110812DD3558A5D3B42B5E7A0C6D5E849DDB283B92BC005CB8634B78358CC421D248B71FEB18FADD3F588BA38EA05D105D5E76CA6F7EDA2121F9DA3A6440299B09A3C519CFD18C525CB8CC1E834C263BA8524968AA4A7DDEA1A37AB3885627916D34EC54834021E46566814BF045077993495E7097E8A08F1CB37CDEFC1EF04A47C0F0A0EF94A5C34C07F18B26F80D7237632D10D0B828C33CA974ACDF90CBB83A57415D858E0B26839BE2E4688A0DAB8114999035DD3DE7350C5F6BD1BE22DC26AA4C117E576A8165B965CDDB313A94EA082F3E497EA3C5BEC63E7717A1ACD279AE81A8136D52938670F5FCBC90D5E8FB40257741F79109B41FB22CD1D8982902653D33EA4095142370872CC808EAC9B05C8F36422569E25247AA244C331D880908DAFC52E858827F11A2AF8485E70202FD8276FB85311EB80D781BAE8BD7E799E4E2E334D852062BD13061E98F88F63F6ADF6E0C375E23EE744E0B7C9E46E2CBBFCB0D992EE201827D0534A13A27650E149F3413A5DEA9EC35B96E59625BA53114D85E8B3DF04CEB3BA28174BB38133D3BB06D4E5D671D6325B162717C9F812B9808C72419A43169721B24DF1968CA6FBC85F10E7DA4C8270BCD531E9CF3044B024398B73029CA7F1E42CD6038652BC6C7FA00E99E4E06F0FF8E5E43C1E9F08788DB8407B10F011C08E98218BEE4311710FFA46068400892A762C382971C457A64B42741F7463DED574C3BB623844713B62E3F790AED2A8CAE62F0AB552AAEF87EB898F875344B9E549736B4EA0361B7AD6284E6A14CF1AA26C78DAB41E539DBFAD82A8A7CE49439D0F5FC6647985223A0451F8DB827E3588D63AA83591C4420DC4B9879A04A5613B5B67EC1C0D25F1390EAD2ED309CEF5A53C2A541025DA78225EB75405E3807A0DF0580EC584FA1465C685F9595C0178255D31D449E1F23F6262B5C7149AFFA3BC00087131812E357ACA919778A3CF2CF73CD5916EAED7E24FBBCA4797797618F3D4AB596F69C46A6915C7A9EF60AB78B963E3A5EA921678BF6D0EE7FE2F702EF6B1738A9C4B91E6714E5D36544F6E65F10465384509AEA37326B7C866D90C5A617189629171741F8965275D6B217632100E598148B04C4A652938A772802789D368349E407F20274A255D1E0B5AF992D42FCC6FC06B98BB1E825FC884683FE031881ED321D3098B4CF95F9F317C02EDE82C1949158EAA9D5DFB93040235B75D7C199737A3E2088E87A3653CFB9B6490C02AA2915ACED4C9EBF5A8796ED6E234BFA44E741EE7C9EF62E75359EE02C6C4399A57E7FD1CCEEF13444DF01B41A0624C6BF707D1560F4910D4C9421369ED3E0A897E8E6C76AF07DFCBAC477C1DBD9F0AEB0A8F8E0A450917AAD2148BCA6164BE3C53A8F2E539A549F1A1F4DC658337DDE872AD74A34B69822B4A14257AC84B87EE1DBE1A5C71911D06DA1686AB60E7F61C594F88BF1ACFC89DA623DD2B05B1A6E4744B981D25A9B859C3C022514EECBE8A107DEFDA6059FDA0FC6CB4398837B9FCAE38B55C4917AAF3F7DD8379C99E3EA54E66435EB9E2F9DBEFBC2F931990DE7C094F0B9C478370BD8342255A471ED433853F70EEF7F2BD1E45793420C2EB1C0819BA0EA4FB031E1093E42F6315A2EA92D884EE30A453056C95EC49343D274E8446D49DC93A5757A24FAD78B5CDFAE43854C94EAF32355E3A67B1D63F9A4B23E965762D2B71C40FBD5E66AC67CE78B7E1E48A5D05DE447C558CCE3807C3118E83081838E400001B837883546493AE04B24BB6C2467C15F352D92DA56889D67006BFC2F99B42E56C0E67A399709E692A645A0AB65CF1AA4FB3270C3C2DD4595ED3E8694FA250E642709AC2CF6DB675E06959380EF27D16FE0C9357BC2D17294CC1AE29574B125991E688D06759F85C4731D363803C71128C99F89CC55389A60039BD4C264C7F4C41225D24D6A0E363FA55B91A80933195769DCF6EF3E975A6DE5813255438F85F60288BC38FAD54167390F79A3B47A5912B09518944373F4EB9EEB1C795FFF420661F0732DD0CA20DD24D7623198188C40F878E5F65025B2946FCA9014C58BACCD6E2DCF771FE57756753A096A527CDF1555E6BE1DEFBEDA109A7816EB2C95DCE21922BD676D17A275869175CC0EA46D20C62A03A0E28CA833E5DEB359B2F7A673522A5158DB4F1F6FAC906D7A5D38D8EB60590BF2046AA5F1D1F228FA53A1D51F1BAC8860222C4EF3FECE3BFD02C8982665A449B5EA6B3EB8C769343A6700C63C65DFE8F29071ED82A1A7C5C364BA7528CCA6A68C2A5D07B2E856A6634FD5EA216273C60DB0D1129C79A22399D3522A28BAF469716A7B084A78D3C9CA68DB0E7ABD3C4CE3F0E9F67732CCDBDCD1A353365A977DC792C4BDDCF89DBABDD23A1E74AF5E43A456611ADB6214D14C25C14639FB69349B1383E8B984F8A2EA5A6C4010324DDA9E054D70AA8E32348AD0B2AF0CCA84658901C0DCA9380D824E5214E1A0E902B85CC726150FF5E2FF9B81C7F6A27A0B2DA81659FDBC5560797024439BD4AA73796E84DCEFF5A34AA8B30C0197C6AF5DF2F0F3E2CA73B32E07218509DBCB93FD3FC7672371A9DA450AA4E5765F0B79BAAB040C144F2A5955D15207D90F146F51A25AAA36BCED31A75724525B45DA1FF43CFB68633ABA2E6B5DF151AD7DD2C37CFE3F61377797993E1EF4F363BE11A0727B97EB2CD3C88B307A8FC4E437282C26EB84B85643D52905C6A51C12A05B48AED6EB6D64E3E2C47EF9750C98CF6E93901494ACCBECB92E839AF040F02F671285DC07EB2D28EE02D573AD946872BE1D0DC66973D84619C6FF6C67B3DBEFE2A9D5D11278CDEFE3C451045D00D3FA16A425C68F5DF2D036A80DFC396A48CA120546B53FE7B59C8C28B4E6EC05BC2D3866B7D31240AFD707D20D68FD60734C7D5A1D567CCCBEC6B362D511F276F69E542FD229C7FDAEA4B6A21F3821D3E2BCD911735CD1615BAAE224185CE56E60ABAF0AE70B3C1E716340A37CB5BE137BA391CEC25334F244A9C38114F4B8D421F175424B0B187BEDD43F84C346AAE756244D09556BED5C9B7BBD2D8EB970896D70C7EF2283E53F21A541D19F8AD891CD7DA83F7ADCEAB17AD57ADDEBBE5FEC70E2262BEC36E222E08E0A4B82950D1E825FC449CC385AC304EE30D439DBDB74BC0197C5C4E377B5CA1432E0DFC5F79CB1BCE0614C38EE0718CBA3358ED015EB06A2C5C255A73BC660D74D70606B098FF2DDA46BFD2E8B6DC796E97530CCE6C2E1BFAA38AC597A6B29484B6ACB5F7AC34B9B3C154F7AAE0E8E579CC25B09D5EF069395C6B476B9DC1A7E5780BCEB61F6F7547AC46886D7ACBAD8224196186529E47F090C9A765044BC8A2FDF245EBEFA5EEBB56EF03AE06B0698310D0262B2D088E111491D2B1240C66344883F982AD5E8E8BE0732B59E3E4585790F0B6964F9D6CAB0F30F855D96A6B7216310C83A86A1479F5458A741AC957CAE5525E8BFD0F241AAEB42156B6F291069FC51C32FD5E4EBF96A3CB5C46E9B3686B3058E9D13EF782959AE19981FB72B5C69B07FEB71C7240DDECDBBAB3EE6CB3F97983D33FF0BAA63279320332F1D20E243C6889F2153519BB7A394E8A2812B12782443E2C73F660874305630D90D004A2ACE49612052388124E75F06E19167EEEB45F2F411CD4C7BB164257F4B913BD5F2686CD2E975CA469A0A2A4C901925EE044AD095D262B5268EE71D533DF0F4D93FD04FF35FC33DD7BB1C3304C969714E8C36DAE6911F41DAF708210A52AFC0A34DD7BBBDC7FBB842F9160234B621E7E9DC3D94EEEC7DC53E34B8947F846A8B3FFB1DBFF646CC0C71E8F3F9A2FC9F5B3A1ABE0079F7A03FDD6277B1DACF68235AADC08D4E1DC77B1F3C4E18C2B90FF3DCE6A2DE5D63483FC72D33C7E9DA0E2CC650913420C8528C30F7C26B2FF5D4E53E202875FE5DE405F70598C9082526A6751B1094D7461050A06549C07212742F60319F00950E470E5F220D0EFA22A65A6CA6E83E044F80424760F2264A75C29C38FEF076C171C094530C0A967B9CC3049CF7CC52F597840E23794B80894A439C3B0D38722A1CBC1474650DEF0F4998C518FF2BFBEC91EC4DFA238E1F8EE5E8813C2BBF0D6C9A9F7BED3FBA0D6E5A3FD925C1DEF8F95E16506B912254EDEEEC85BCD1567B57C3DAFCE3F75B67338AB7121BFB73771D284010FDBB327C4C93F7EAB8793828C7FC011F59EAA133C8893F964C6E4E50C8566888204EA4158E2CE4F3AA7C38A85730280C13E0E64C7E9AF40573C5023B2DEA09E721E5C11E754BAF3A828D882D81396D27C67027CC97288AF870A5138DE15A245B3E3149FC1F338BE297065C8EA770FD7621F143F31F40E247C86C89357DB48F158BCA25CF9314118629572C07B9BA2B53E8AD10122E846BF279CBAEFDA9DB7C6704C7B6FE9CA81F952ED6347887607AB5D12DDE889BF15A2721FC71338B55CF96D106DE0F4A2A60B9C8D66D083E23CE7F44D8ADC729B9509424E20C6F94A4E4A1227D351D4A6974C6891252A3CE64497092EFC29D2DD4B337905A2843A8C381D22A340E3E398CD55A424FC2505057A6ED4393D93851768F48CAB22ECE7C94A197B0870EF1789083123B6DB42CB4D009E29511E187F8B6B2B65D6D6EBBD7A117C580E44A6016F7B6A8128E7514E632471A83E113BE115655632068F68B537BA2AFA02C6807CD36EBF6935A18AD92F3BFA28F215A22BC0D9A540377B82B33F87F3246E0E7DFD89462B9CF9229C135BA2584F8BE2FA8EB7A014C390CB267BC8E23A089CF45AEF96F886F6068840D096D9420F79104EEE97916CB0C7EC1F9E50564882E93595C7EAE5CCF66B0E65D1E39462E514C8492CA2943CC8FADB09E222894A74846A11EF81F684E9AE54B19116B5DAE7132D825FA1742150A6B882797418B01FB4DD47E40642FC15DD372F06EF97231C23ACAEB5B900779E3CC8DE1CF10EEF7F82D70DD6FB9066FF7DA7BC1EF1E053B7FDBA056BC923A0EA976DF7E59BEA80B001F5836AB4638842A05B3D8BD32E5F7B38E305D9D0A2D2C55B4AF370DED49AB4AE3E79B49E96EDBD9B1CA131DDE966FB7D9C08864F4813B113A9BFCCB8223F82B6B84802755EA58C409CC4193D7C2D4197C6D32ADD3E5998A4C9EDEF9C6686524F53E432E5299CAA6003CE9B42B22164B6F914802F32EDFBE8E2DA540324159CAACB2DA5650FF6B3AB5C7B0806E78D6CBE792BD910ABCF5EBAD5EFBF5B1ABC5F82227145765FBDE07D166B2C9C72997CA0A3464274378E37FB0090EC47FDCF5D50E97FE814E779B01990E5AB65B5F62BE1FAAAFE8C774CA824DA06CEFE4A4770768973BBEFE64B0CCEC502B51A5D90E8BAFE9187D35F119BD9FAC4B0948514E02CAF521D1FC9767BA83B116FA255A4124BC88052099CF17A07BC27DA91F9523C4094B2B3291DDD9D24BAC023B507BD284D0E2440D2A8BFD4AC9D5D5AB7799D4B1B565E2338A767664D6D6A7225C2969E43A606F06C2068FAA376573C484C3538B908C3D81900277CEC2A92F3D6E0CD52B4D24A64A59D8D8ECB945B75DC8F112CFB2BBD707B8054B6F3A685C0D97EB51C7CEA763F759741EBA5D8ABC5B66CBF05F69DB72D38DE1E887EEE208286EB5DF5B7C96EBF86D31B8F7E9AA8C7B2F09ED149BEFA7A75E9874C8393D32413AE850D03B9A3089EB69D6C75A4C9D7814C53199CC4A50DD7473D995825F5FB756694C1B6F8682644D5735273EA4515E1B95D9166D8C38F8CA692BCC8CB72E534D5475FA967F283E702F24273603216815A9CB7C0291B715E1227529EF0C33270227CD2CD48F5DC7FF322C0A5B9D3CBA54FC9F9E14B3669CBDB916CCE3000C8EEC74EEBB5B079D746D5D17EDB56666A2DA1BB3C677C1E3F4581B67E8F73BE37B418AA13655DAC14A8556795017D6F0E63C261CA2817EABC014B8EED6ECC4E10DBEE890CBC8331020F63E7959407B742F43AB3D16BFC80F29C4A2D70A2C163729A549E936BD74939E40235A95C4B0B42C1B3A59E9B8448FC2AAF8353AEB438AE53912CB84EF132B0E48B9FC67914C62B2DE8B2FDD7F3DECB17DDBF9F832EA226CA95C1DB17A9AEED9C300360BA70CB3B09B93FC37E844CB5FDDAB0812EE97821D085F05E19C9FA4F1A9C1F3D9C5B4FE0CC6A027D9AABCB7B4F7C9CA9162AD570D037D709AA6E5E40523A3A0921D0845A44D464C8E9BF7D11AE31E430EAC81A246B0C71B6A6037EAB3B468F1F1044EF284D6199199C274C56397880F40739D15E8FAB289722E27BEE42CD04F542286A728BCBEE8C6A2E0F03AE86C21300ED59AA3639559C7C9C894C1DD107E054877F912260475C285D0651A81338A154BC7FA6B8AF11415B1C0B3D61B945677B5770B795F36CB0D68B760282B121B3FDB6852FDBEFDACBEA57EBF0FC2F6B38A9CE76439DB554684EA00B70168D2CA9960D71E0BFF416386B77A790251FC79732787014509A9B1DDE51C4FEFB32E846E2AF4874A3631638D99F4BA78A5392204A53531280394DA600791CC34A184479188C39D4134DAFAD8FD5B52DB7E67561D4399348891F0178FCD4E4984B693E5467208AFFCB0854D5795BB0110F75AEB6E157E3CFCBD147B8DC179D97CF9104A0D6627BE8738B2B01523DCBFC6DC9BFE57E1CED860C7E9FBA2D0AB4A57870ACEED7980B99CE1CCED7C4EF39DBCE1FE03CF105FA2B8D16CDE4B60A9FF58594A9EB0AE98E071957F37BE1272441D25579BF9C6E77229C979516CE089B3E9247187FEB5C2E7310783FF048A722479AE2C4893B083862B9D7371D5A3BFD658710CC849F59263B4F6617F4ABE3432E9395C36022D34342D4874A574C9C9213294BC19970FEEF240C0564FCA915BE5F8646712DB20BBDD20E3F2E711960186AD1CC5E3C0789B36C980CD846EF418E1A085B5A84BCE797AD37303E235C5B8A76D93383F3BD75B66BBE3A5DA1329CF3B4359CBF245A4F6E6B0D5BBF8D2050F165799D227672D072BB136F74829556883C70B78FFC1E5E8B383F2F673BBCC78FDDF34B8B53971E2F659D8B312F9A1C87139CAC210E226139802E47BBFD62B32BF0C272BF977E5C9645E9805FEA94D02907FBA257CF476BEDC93040EC1CEB480A7F15894E4F84A838703CCECE15A7A4B876990C17969900BD4893F52E34DA7FFD3C7ABFD479F9229125B664BB17BC7BC101D213B626D8ECFD5E6647319B597723DDB909FC040C1FFD03630A955C5BBE5E1567F77DBB163B7D751E0CCC2D474D8156209FC459854F6FFE5D074A26B7B5C69042950FBA487887DE11D257240EAD78BD3D903C30FCB084638EED889BE29417CEFE25971BA7EA722FC5202980390E40053801928F40B2DB1BC95D09A37D1E8F77BA39AE8CD72F7AFFFA7FC556B73C1A94C2B53C1C84C859DE2E152B2DF01EEF0EC6FB21A76DE1A88F06BC3E8EC3A9889E9EFC54954A1D1B9C57BA949D31E13A632CC0551871A9951A45D1C9F009EFB2DD1DBC7E9E6EB4C7C3C02C0A7DE39C26CBE8FB116267F77D0B8F9D0F6C0BA8C1D942AFED7774A46D35E16A956A4D0227EA4EE244DDE952A11DAB4EC1192D76B67FA051E23CAB77129C405DE756EDEB64828CE03CCE0F399E03785CE9FCB49C71B1896BBF81D4E3B8D8396135E4702C5B6E327EA0CBCE1C24A08F0D27600342C4C9C8374694DAE54D453225C4992ECEB06F74699BBD7CB38B17C0F0239C99DE1D8CB6B93436DA1E94C792469D7296934114448F253382D73D15A2E762176698C1E0BCCE64569B032E31A2D7FBE504415470C2DFE21A4D373A83372F88F32460ECFC52207C726BB96104AF9BEE47ECD5AD740354A29429A5D9FBDC255DD87B63A4EB885A996AE0A43A3FB5E7DB084D675B351316E35C0CD58F9DD54D643E516B1C0E3A8F8BE100EE345E6B2115E29DD2FB7D069B772FC2F792E26F76C6C716A7346C276721FBB4171AFC22C639E20CCC0109094B701D4632172F7737C8DD5E725F033C30E075473BBD92ED59A43FA1143302526A1B76F5701D0039709E22D1B544CF042714A644AF0C51FC085C45BEDB2F396BC87BFA83B74BDD57CF99D3AED1C7C0E5A4BCF362506A13F8BE185DE638570FDFC729C2E706FC6DAF279D1D845250045D08B4FBB1ADE6B82AD496F5BD06E707E21C28CEF59E2B54163BDBA71322C7325E84D3BBCBF309A29CC3AB70B6E19498E26E728C0AC9612438E1BEB8AEC921F7D0E03C0D88D38E6C21C8C1678AB31596077D70925B1542E9D346CA522789382FBF3FA01FDEE3ED0C06E75124B733D82EE099A9591983194143C466A34EE29486DFB954A897A64F24BD8BB4906513AE7D7E6C0DDE2E75FE3638D991FFB094724EA50F9C0C197745719EE6C7C4999FA6F15E0839022428F65701B2CDC74F1D631ED1864617E1ECCEE38C9AD950B34934CFD51BCD3D3577D5EB4042E966852C51813A925B3804279CED7E2FDEE05D2899DC440D1F9BACB5B85F01F22028095EF144D429D33D0EA70CB087C292B9281F116577BA484DED2D9EB14E889566204FD6C2789B8A2C810D95A2DE9324C8A5BBEBB52044A0C3017C80E1E79CAD3CCEAE526DE1EA04AF6667209A6DF5E069DB7F3FEFF372E4C82E7D0FCBADBE59FFB9E5A742846BBD87AF23EE8C7C10293FE0844CC3CD411F62FDDC7126503B9668CBCF92F08CE2AC9CEDA6C6CE863A8706E75CC3EFB736E76FAF8AEADE7A25CAC78223B5675171D4CF76D97FCF0F79373528E2A2665AFB616974C4093C38DBF234948F2CA27FA3B3BDE0D0ACDC5E226ED6E22CB63A7A0B91DEDFA9F7047226569AA5A69C38322B9AAC4954977E23C9DDE2729EEA4D4BA3DD2E52ADE9B9128D0DCE4B3703E6709AE7F9238701D25A187046C806B6BBE956071768B1DF9B5C25ACAFE06FBF8EE36DE9F9DD14C94114EE04C006A824BADE87FBE5B1DA4AB7B7D255A25DA75185FA8E5F42BB1667A7990AED0735752E10E81F10F5F35BBDA3C14D2698E169B9FDD1E084B3DDEE70BC7D18480BFB05D4D97BF302D2A4FE381C1B11EA6964714613995BA7231D064697C301C3210730894AE64862F3F946BAC0093B1515AA64E52E4109965C6C61E7FD3A9FB85B5CB417AF4419447BD3F3707A111BA2173228A433B7D24F908E8464DDE203F01E3AAF5E745FBF40C8E462276FF4EF26AB4CD1A14E4A93E173041F3BBB677B0838B39384E4D67A50271E63E875ADABC70A15442953DFEB6A0E6C710E7E950ACD0BF43709D19C07AE2F7FBA49303740C491B02B8B73AF971D70130AFCFDB8A215678E540538CF2299B60AC72716E785C169FC2D8922050DF20DE6C0E676EB638E4332DB3C9152722F18EDF4475B7D1CB01481280F42793218B13841F40DA5B1904EEC5890CE7BAA8BE644E7596859CAFCF48DAB5204E7656A0777F97A84C9DE1BFE09F84398A5737A9B3847877DF8151D2A06CEF488454B795764C749719E059B7DE25C37862F071BE658A1F656C4F1AAD7D5AAE6BDC589A2D3C75957E75CDDF97F72B95E1CAD266F8D582FECE7479D72B61DB113D52792A08114273068345A6973D619382589A5DBD406D065ACF7124936046F19B01B701C14C88086E29687729FD776AF58EF169B3DDA7AAFD822CE620335099E4172DB4765021BEFF071B4C303DA2E027040D5D26DC612351394404694EA661935B3A98F53A5C9563EFD79EF2D2ECA25FE2DEF586811E76E2FE5D536C0DF827839BDE3DEBC48823819733FE2B8C961046CD02BB3DC8D3E58A2120DE458A11AA2F5386A02A7E0ECFBA9D02F9C6D73F9F337C5E822A2A7E63607E77E65BD456E2D024EDE76D2C7012E64FCFDC1470E4D41A39D97CF8BBD3E7357C63FD122BD1C4B1486282750BE800AD69B1138F38E7C6AAD13A3FEFBD0CA56DAD9E7361E53DD5E66BD8B6F156BDD7CBD9BAFB50BDE2DD4CFD63BB07CB50BFCA3F5DE08A4377BA3ADDE788FE9314022DB325D0BB9AF81383D4F4B9CA716F66934E0181F4146ABCC69910A645B4C82D2CD369280C965CCDD96EF587A8EE59399A65FC61C871FC6E1F6801477033C065BFD706B80471E5744ADCB55A2C2D2F4101ACBD73B0B9DEDF113E58AAFD13FF4BDEA7EDDDDDAA7C9483FA3EF4C71F6508A849F99D0E3B1275107755BFBAFE77474A70A4CC62AE96CE53EA17371B9175AB1B0C3C05B1280F388778C0CDE2C2FFFEB79E7D5927E0C59BACD3BBCB8BD268A515C4F47BC7B84F36027DC7F8D1B1A700BD501C7097075BF5D6EFFCFFF0B5E2DE13A280F03E21C86268995473CD3C089B2673264828DABB0FF76B9FF6E99C9F95657A4D986A72D90E56D09CE8B989F21FA4537E91D733BF4AFE39CB784C604C9DD9FA94BA0C54128CF08D17981D2DF3A9C953A1B996DA5CEF9D2F3B89EEB9ED4E9FA5C1B8CE7942D2B6842F4CC385B0834FCB4ACC624E2FD121E5B7F3D07155CE3803A3EECE1EC4CAF2D4E26442E88462A53F0369EF628CAB706D93AEA96B0D80EF2ED20DB1C441F3BD107AE4172C74CB98D4916993BC1FBE568A51BBE6DA79FBAD94A2FDF40610A8F1D4B1742AEA4936876ADF7041A754A5AAB7720E50FB73907AC11F23F70F7796189EB92A3C2DC2090657477340C8A3DA4026C8090E5571A6F5941B5C61A34431C8DF7232E68EF91A5D946783F108D0E248EB2D560125D2B50E2D4B4B681B39E0A450B89FECAF136B8CE771EEACF14A7DC229AF78B9DA10209F383BEAEA870BDF32371E232EF094EDE5CF0D773188A99DEBB656DEF11A14AF642047A21442F788FCA0845A7DC583F3E8EC727E9F828190F93F2183951C6E3C364BC1F17DB61BAD6CB36FBE96AAFD80D473BD1783F298FD27288A4948F255B7DBAEB49A4AE1E384DEEA3208D58A550B9CD07729B114C3220C68BE013FBCF5C29E20E2B5D5CB2C541AFC49BC4EF11960FFCB0027EA421BC2ECE27772C3F8CF8811F2A4D10DD05CE508F29501055810ACE9EE294CE11DB499F17CC0AC9CE25BE3A15E7B042B818EDC24AE6F809C616273776873A595086283DD39D2ED74F5696193B5F9365FFDD122A719C9D161EDF2DA198E9BE79C1653245885303A2E79186553D6085CAAE5E549ECB281ED25A403D4E68077179108F0F6342DDC7234997C3546D729296F8F25840B28C89CCA60A67AC6ED91FB6FC706C7529629505D4F64BBEDBE093407DBF24D7652B5E6DE57BB21DD91E133DD4D025FCCA6D8654E84188F24344B897F6787495A350014E187002A49828756740DB96386A055A45D04FE4AA3807AB8DD8D99F53E713D2FC35CEC5BDA406CEB3446F1A1CE3BC9F09CEED4EBCC6D54D49855883735003C5F8AB179DD71C4E40258E03CE1FB361EB14292C2F0D51AD53C5627B8FB424BA43199D4541721096FBE1F800858AD8BED891AC8F4AB77DA237F6EA30A62C9112A79123EB1383F64A7172A9AE18865DB9F8E48E863647A50527F77EDCE9CAEA1E3383F1F100EF1338650671249BA2539D93BB11722264B9FCC82CDD49580E2A33A174A0021D88409D46699F459DAB0D675BC3B9381B7A0AE7EF1136EC94F74AD0DF2ACEC33E68C51BDC89DFE0E4F031D5D97DBB045126D25841AE1B6F764DFFDDE174761EC958ACB6DDE5FEF853DDC7CDDCBBC92D68F6B90ECA94754F6A1239E04C896E6172129AAD12CEEC662772173ED269732BA0E0B47990B174B7CF915A0996BC9551EED88DD8D8634E4B9C07DC5E6C8C82EA3AE107A94B1E2411949FF283F039BEE6A049B4178225E02587B16319EFF1D1E10CEA11B4C2B9E2D59D6EF4CBE03C8A7E4B345F74FC5FE014874C9C17717919B3CFB7DD4936B9E4499C5C8E788183E57F3F6FBF7C8E2F75F399AEC8D453A7E44490A697E89632BD2E0DF748839F3E52A043EE23C51D8298030FC687BAAF505F7ABC83D2AC69CB8882B2348FB13A58B360EEC54E5D7CC515C6A5CD4FBC2F25DEE0FD52089C892C10718D48B6711873E13A244E93D6163C509C50E7554E75CAE7513203F2050AC056A0CC893624823A9CD6E671C64E9D6985F33709D1535EF7F77425ACBA84683464DB964B9E5C4E5A461EA4EE0BCE1686A491F797EDF7F024CE5D7961C2E7F442DCECA5E92768F3CFC44E71B606AA3E7322FD7A7914B4A16E2DA4BBD07040DE6E3F64589E3996526EEAB2B9E444FAA5E2E41E0EEB6DBCD548E62874BD9D81937B1EB17500C7C37BF481F33C9ADEA4D3FB7C0A90D468C1DB20E5B37EE4339B8B684F3EE86E881437AC395B3144D0C0FA5B5BB1747D9C70B6036F812C76937CE9BC3AFF4BA28B7B498D22A7CA89E015492BC1E5BCDDC179013F285292FE25C4CBC1C7654E2CECF5E084C3F5CEF4262BB53EA9DC6C950D696BDE84BD336D04C6BA9190F01399CA766F5C51E14661767AC84C6526F4B7B2FA66364F50455ECB82B995E9CC2A3596DDAF39EABDD2927B4C97346AE279E832596F8DB865067B55CC83EEE4EE257E6853AEB1B3BC2D4A3A5B2E7F42949AD0463B4D96EA6F9FC2A969ADA642C17AA54EBD4D45709AADDEA33FC4993FC1755EA97E4702BF5F044AA228B4D39D0E02240C2705C6EAF3C3126FDC5969B1B57B3840400D373AC8264A8BCAB0BC884D196A3D7069914C9C46CFE28AE521A714646C33D27D69AA6DA26C06A45B60B8C513DA793C13A2E64BB90D1B256C20034D0896E97657D4B9AC6E26D968B3132451B33CA7A79DDE719304AB4E8313B1533E99304B8F63F85B16273BB6F4ACB2A1B972C5E6B7F338C3CD863A8771DA70B67F1C479F92693E4FD43619B839E66582AB18FE56051A387FCB7A6E29DA68C353A13645F50229E01AE79E17DC20AAEAC54F784BBDF7A5C2BE34FBB8E9F616BAEA69963FF536A333BB5ECD79DAC82E679AFDA5EC1C49E2A449755EDA853059A9CE8F42444A4E3921F7DEA58341D41C70B1BA13AF2CA3D6D44A0CE91E3C2D7767E18D5370B0F9E43687B3D53C08554A764CC39967EC948FE956D37E42D8889D759CBDDFE03C8A52B317FCA284E877D1345F74B050BE99F98478EE758A3F38DFE7E474B2D549241AB111CFBBB196719A46A7D1E824D0716A241193AB042CC7A791D1A867A5942B7AAB3DFB47325B64EE68909B4F885057A7D54ECD2D0C32F92E83488EE5A5D9DA04992DBDAB1E6B4F8A85AFC529C192A314C72114C9F0FFB9250BB7F0B403B0E45B82E7B8E236A042340551E2BCE76227AAB5544E0580C1DFF2D3B9559DBB429175675035139A38BBF3387D679B7838E35A7BA85189FE71345D7C1DC86FD6BE04889A5DDB64062C935A0D4527AE7142C5958ED202723C0B59A1AFB4783FFD5DA62E74EC02A4E641E766414D5A3989E6BAD58DD617D5A48186CCF23836D3B916A7EC376447812E92A9D5A239D0DF291B2340FA63D9CB4A93A0941F3E30803A91D9727F4FA94FE0724A79E7C8D74A1A47112690E96D3E82BE91045D65C5599A9D265CC1DE361D3E7E6E9F7BB456B5FAD6ED0AE8E7A63A07569DD1229C91C5B9E083372A3C7F92FAEA2FA97EC47A72F9964988E06F4F82E2B007A7CA9DA4372850A418C8835074727B928B186821057556B23FA697BEEACAA870758BDEAA21B719AAEECFE772A272285F5644EDF888A63F978943A80BD7C601D8A515190E0AB98783CC03EB640CC204DE33049ACB968123D93907D722DE3CAF48D451207AC7DBC58BF3B4BC01547E1E332055A588D697F2618C81C3B9650367ADEEF470CA7AE760CED9D6D579341F413D8DFE79ABA182E7B154812A5108F42AE51F7C1A16C37EBEDB4512813234108146EBEDF1392FEDE023D75B0C4EDDCB44F8F19431E390ED2F4FDDA3E90954254A053294B1AED00ECE473A1D6F6747AC2EC5262AD6735BB4989C96550A77FA973B18637E164828D294F5EA9D2E171550CE72C35BBE3DE58A3F81C65DEA529C0DEAF238867755544DDB1AD4CC7ADA1ACE7A57A8A6CE9D3A4E53AED45CEE13A1F4170175B19A6B44D5B8E7F6653C3EC7256CA687A235A4FE74651028BB0D5709EF6A46968B0CF34B41376B709A7E1E4711D80092AAE044B7F617B4EC0F84660A57678B8696A86C664B9CC7DADE8B38B46703E4F432A91AC23AB4ADAD03E9EDA1EE0C573BE10AAC951DF286C678B343965BBC871C3E46F739E7BD8E62448B5F759BF3235C905ADFA03261D32758EF477B611F9C74B17A9356E72ABA5C10386B3D5BF1B7267656CE76CFC339E775E75CEE423B9EE7F72B90CE15F3F623E03C8FA42CE94B0DCA5B7759956F769041F056A48D760DA7837A6E8872EC8F4D5A79D4FAFD24744362B6E20C4BB9B5884455A6A77EB736AACA59E7BA653ED6B4DDCD1E51D9E0539B7B936CF31379101102B9C786FDC89D1E3223E0CC0F74DBF31E7D89EC883E822E8721FE5EA43FC0D393D13DE081FEDC5889E1BAD1AF1CAC8D9ADEA28A1D06FBE8F5F956AD3AB7AC3A1B38ADCBFD639CC773073596BFC46976B22551E484D95E176578BCCA1A00670AA723149CD0013F9BCC4A9315082DD61DFB0AFD589C214DD73E8D294EE907E97898DC5D64709A2D3535E26A09AB77B088D0D5CDCEECBD3106E76DDE79C3754DDE1FBEC11B6C42E950E2807BCFADB5B37D2E13E1A2845279F1C9F485383C7EDA0922A22A4C7186DB415F06BD1A443DEB594FDBEBFD9F71A6D547574575247F68519DE562638A7B14E59210F113548E831147C2A455447FBB0CAE7A2B59B6C75C69F625E70992BDA34A399828D19348B6BC94CFD8A8AC2F36D09EADB611C6EE13577413791134EF5AD16D189D49692B9B42A56609453DADDCC2B6F4174A29DE341FADF11663A469FDF79ABEB522B98F11EF16FE567C6CC63DB80E437857B6E236FB3AC44512327729637C66926F5EA6952ED79A2C17E3DCF6711EC64F0BF4BFC5F91B908DFCB9BCC998135D704337EE36CDC9DB2E5BF36BDC5E20E6ED929D113CE7A5D9505F29CACE7CB2A7DB995911938D833DA247958D8F2CD761A07E9807C76645C53C9E48267566596A63AFE16C6FF3FC247FF1FC79B4CABD5AE38D2EEF80FBC4488FF40D6911EA9642768AE7276E1D45DC5F8437CAEBE8BA998F258CCF1D8353AD41D4821C78139A663AFE0307C07446DE2E793A75F69E56E791FF09737F4E34FA2FA05AB4C55982526C741EE35CC41B6D3AAB1D0EC8E3ECC8ADBB6C6D6B3120B1D3B85942353589EC7E2A09916E472D1F4616D0F72ACB61E0586A70952E7CA5456B86E5A46AF265B3ABBA3AEFF2F276921C66CBA2D12EB78DE306577887DC5CF06080A25E4F3180F17ECD57CB9D0F1D7B2782CCE17DB038A92D4754705AA2FD3996FE64097EA19D2FB138D77E87B306F58F703E1D2017AAD3FBB5DACBE518D1594451AE730B13AE237E5E0E3EB30685431BBC5F6EFFFDC2385B1985D51B3F6C96CB54D61055A8BAE9B0F9F423B52AA08E8FAD533D336DDED21E686B496F44B17D5A7377A9B9ADF36E3ABD9FF5568217FF7AB1F4F772FBF572EF631B8965B8A1BB57B4CC8E07B5CD293C752ACE4F16A7256A707AD677C5891DB5B538DB759C1DAA73C3C4CE78F7099CD97F47F40F1036701ED5BE2573F4F0B47D88126EB6FBE685E98B6E76FA1F5BCB7FBD78FE3FCFB5122DCFCD49B753CE895626322EE4D9A97E8095D6A66CA296E65B810B90422E99A85D5436AD563ACD0482D94E88FB294FA65F66932FB317FF5A421C6DBF69735B83B738D16EAF8AA56A2B03BD5FDAECF0D4AAE19408FA2B9CEEEE866A4AA89AE4AB70AE389C3D0FE7E102751AA8CD1CF577D26C06DD2710FACF28D1E388440F7857368822C5E0F0EA87E5D6CBA517FFF3BCFDBE53AA83BDB49FC8203D712350EB33F5636BE4EE0669219DDA710533B7109A9E835426E69ED1ABAADB3EF19753744EFAC6DD714D9CE57539FBF600EBAF852FFEBDB4CC5D9D3A9D77DDD61B68A84F847F2F35771F913BE6791FAE71B66D8BD3F7B74DA20B7176DF3B89B71998ABF91251E7F6D3EAAC12A2A3D86B2C3492DE45EAAC0BD1EB4E545FA6F338E5A0388D58861F07FAD1BA28F2707696FEF5E2C5DFCB932F53B3A1A2252A5ED754FD6608C1055797289D57632822C7C84DA84CABFBBA334F8EBA6B94DE7F92998D8DEC3E7C205A5E8D67DF1F1E668FD9D9983829CD6EFB5D178FCB2F5BDD4FFDF6DBB60874C909D4E0147F6BEE47F0FDAD61F947387510DECCD9CA6CA61D48E0468BAE93F0CC7C58F9EF2368035EBC10E13C3CDF813770FA0D290A9413F4310EF09720EA2CFD1B89E48BE9F787F2AE24CB4B7BB7D7656A762BB60B2C66233DFDD6957BF47D696C9A3EBC20AC0AA53960D2D72BBBFDC285EDEA5D579B21E85E99E3F302D27CF8E7E7F866B6F4D732C32758BEEBE171F9557BE96FA8B00799D209FF6D71BE72385BE22A3BDD399C569D1ED73ACE9EBD95CCF7D8A63154C33980CD3BDBD813A8B37A19EABE9C63B9F0B25888B0A954D8311F3BEF9119C283B5A080E4B400CEF1D5887B65DA2DC41D4EDB70B70BD11716E4957EB283B24F2657EA4E53CE74395DCAA645533BD0E5EDA6E170E653E76905EAF82C9F7E9D01E7F4DBE38B7F1327DC6CE7439FEA7CDD86E3854679F00629CCA012E81B3F1B7238BB361B32A96C65EB7DDD0E83DF620F019708ADFBA9D79583FE6A9F86D76C0C82CD20DA0EA25DAE95260711CCE29C17E830AEA00EE78A5175C24771D3B53E81F3A96F3941E318EFA6FBB1DFFE8044B187530337FBF3E7CFE9D7E9E82C9395A6CC42757BFCC713DB5B3001F5523EF7488019B4D72E1C7A56C7695CEBA5DD1C45594A7DA2BB9FCACEBA15CE87879F4B2FDB10A8C10975BE46664B834CF5B1FB79D0F9D4C375D9F9A824FABDD57E6F6DD087AD07838D0024685B61B01D869545303C13E0710B160E36C536C2BE987E293F15853B51B41B457B71BC9F2487497294A4478975B60BFC6D43A6B584C8644947B5477DB18C37C47A41E8CF562CF9BCB146B3106F0EE1A7F3A98FBF1CA700A963763E7AF8E77172571627F299C4FAE9178AD36643F6A31E2D4EE78DC94CCD2A4F7740B9F236B7B1BBFD9925D24A9736A1BD354990ECED578C8E53C4CEC7879F8FFFF9D9FD14509DAFDBCED912E45B38151A9EC131480FD6C3DE6AD05F171824110D0089C00023A6EDD222D81E2DDC4BF419DA4E1CA86DD3060A78DB3CC917EC25D17E121F243159A6E990F64C27046B36348F4FB76A93CAFC2F87DE97C3A4F6B2A7EC24E598C551DC5D1900645771BEEF02E7F407F28E87F2665C1C27DCC9F626D3FDA7BDE868CA15F378E1E1E45EC6224D07AF613A9C77E358EAA2B7C32FA46FCDD6A78AB3388C1DCEDE6A58E17CDF23CEBF99E842AFC009B1AAC1D9404CFD8DA82F2007DBCAD2F250246AFBDE23A092ABB16027091CDA1DFBBCBE9838D3E430B538B367E9710ACB60274FD869F6FF5FCBEBC7C9414C8A90E6C77E4FA0B6DEB45FFC259EF6DBB4BC19A5BB81E0E4F99DE8E79968803C7720CD5890FDD03F539B36B4A8DED53C5E570B99958FBD7471D4EED76977FA02D17C3F422A04968F8F3FBB9FA1CE16284A724B9C4B2F5B7CE6655B944A7FABC7000C759222A8ECD204461AED8B1D3C61F2DD504DE9EEA53479467F303ECCE2235A32CCD2E33C3DA13DCB4E325A758A736BD5A9CFCD9779BED0CEDC7126563D932D36FD85785916EF4770AD9D0F3D3E5A678BC0D9FED0FBF9489CE38B3C5AEBF106E65BB98D59FCAD098D55966B0EF413C48C5E2F93898994731A35E5A67C468A0B9955CF5DB7592D2A137F9BED85B36F33E0C41BEB7C18005E8BB4E857255E4A2A446BDB2F99EE42B590295E8FF847901692C106248B2C3A5033AF317694C3E2A1D8719E1C17B0F4A4484F8B0C76061B3D6B9CEBDC3B3060FE0FB6F007AB5F58E4E739AEA1DEEA0014DBC29202FDD887CB055A9C14BCE3C7FF3C4EEF27F0B4BDF76DF96024D99B583E8BCE64B90EA7464DFD403FF7D9C9175AA8F096C2C9A597045D6766CA4BF668331B1D70736BEE8CE9E3B49BFAEB568D79B6133EFC78843481B3FD9ED79C8ACFE2147882533323E2A46491E2B6943ADC0F944A692A3081A40AA379C706F3B08227467EF1499188A5A723804CCF463080CCCEC7F9F9F819CFEF39CFAF58F19415177356FFEE933F7BA6A614D572263E9F49AE2DB91FC20F8EF10C70B6DF76700A66FF3C3E3E10677610A35E26484B74A20991FF495657F60376E5131CD4DF6AB3DE1771D53130437B16A45BDD749ED62C8AF17FD4BDCACBF334DD09909A29CE161CECDF46889A016936B4A4027DD5517F0BC0AC50FF5AE62BDF6864EDE08FEDAF85F1E11C4EDF86109FE8CFC24B4E47BE09C5310C146917E35CEC597159FC898DAE6023B53FFC119A636F8F11B4E176E05429CA0F5D79149C1FFB1A415DE0441E049CF2C9CF7DDD2DC20954B7033111D4D04AAB8F69BDB44A950FB9AAFA097EEBE7B23AA83AB455A0B59FCE20CEB6BCE0278666BB21AA14E084B5DEF68C10193E7B46A0D0A23E29020551AA53701A8D6A25238246D0C5E52BF0C4851AE397C9D082A4FE3CB3088D094582BC2CF3CBB1DAB3D1F5E8D7369EB3DFFE4865F60AC031F22E2D9ED80F7BDFA57DE8773E68E0D4A83980BF5D7ED55A7ED3214EA952926D7E1487099CCE5C8A5BB9DC4A9AA5B7C34C594B97AA04D8E641AEE2B407D7A6229ADAC039B9CA47C749BE1F8C4E32A6B58AF34DD7385B692090E81BC96695E52B3D56812E5BA2AD4AC1AF8D58D9485A650113A96B95D0E8F9524771E410AA8922CBEC92469C57F278593E1BDF8E7E6DE5DD18567BE6D63C33FFAD45069F9E439170A40C969FFAD21BEB1A8AD6A444197424E9EF6D843FFF9738A75F26D1C620DBE933ADF5C2A77EE69049712FB59D6BAB14D3363255A929496DAE5439D82BCFB55ED936D095F934A3A9FD740671B3597E10A45B780FE583C5B9FCDAE03491D2F95B3FB385BD167F5B278A2B40754CFBF752FBC3A0F339E8AE84BDB528DC4D811311319997E3B9C71220D57C9C62CFCABB11ED7E640E8C8DCBFBDFDB64EEA061E39B116A6494D2AC295784E57B24058C1FED0F36A1FD2820C5D34A49DECA2E46C0C9D8F9659A6C05C0096002B2B0662368E56C655F218333932F1353BA38BB48DC07D81B21CE854CF7F973CA523E0336C97607F15A97554A85B36370BE325A5447EABE54992E19F7DBB2FEB67AE6C5BF97B86EFA578B8E9767432EEBCF83DE7AD45F8F902E19519E3689562C2F2C4BA74EE204C82F633EFA24BE8C277F685F1B5F9678C4D5802416F53240B2FCF8608A1086C97774B32609B20235D2FC28385FB6A6B307AA73F680C2203F8CD3ED1E7781BACA5CB9A21F5D35B9B21FCAA0F02E4C7EEB70BA2643C5D5F7B4DE07419AA457D579E33A509466711C275BFD68A5CDC0E9ABF36F87B3ED7A7B2E6A0ACE9648B365B4089CAF780090CFFFE7051EC9D88A7B591C2F73884F03BADFAD047A1D6CC589CD5D4DB0D47879593ACB1BEA240F22192DE0F4951B344C9C7DA98EA7DFC4BEBA477E1A5E260841A5F5AE0BEB49D541BFAA5D0241A846722E6ADAFA04CFB498D6B6103871CAE06C67DF67C5499A6EF6B887CC79AA9D049B0A893AAD9B95ACC7E641FA09659E8FAD7A0B17B6D0D41869FA7FB9FBD038F7A897457110E58751BCDE4D36FA4C6BA5E8C423709A58F8D25A85D64B6B559AE695D0A882348E575E2038DF6877105C7B840AA57E0AFA9B71773584580115550DA5E9D21F65D9F0B7921001E7C820F95A2134B41AF6D59A7BE67B0921C64729CA2982010F59CE65D521FC90A676F0E6A40ED1906993A09EA93845B8DA12A21F6642D1064E71B614E8E8344B367AFCD4DA537E6A72E569653335B3C6A28A74811369EDA5AE89A6551C356E36369B5D36DCACD3A5FBC09D2B162792D006D14A273F4A2A9C50A7A642468BC6AF1A37FB5ABFAC486B7EFBFC7F9E3F7FFE62E9DF0EA44D83E50A30ED085A4F65DAFD1C40A6DD1584551EF437902EE5D6DFD2E5D6D32263CFA6DF04271F473578DF3DAB739DC993781CDD8CC2DDA4B716505E1FC152EB2AEAD260D394C7D3A582542DCA31580EB46EC1DF0075B6DE778953448033383ECB93CD5E71A89B3C894095A549853C8A97262D724EB8AC7A7EA9DF0EF470CA473988A94005A4F9F0A4F1595A0C817310AD74CBCB912B3AA94E83B365DCAC36805ED511AAF85E71EDE5B9FE531FFB77CB11AD02B06DF3A2A2D568DAF938607E2444912B0127C49A0C8B2A8E5619EF48AACF5176069CDF47957D73C7E3D90F027336F51EF12D54907002121A07EA3999C5E99B7BD35E16A8558CFCD8AB474D7DD2E6B492071967FBA6D3DF8C7C9CC53005CE5C3EC691111427FADA064E257A6D0138A5D20967B69FE0F5E5AD5E35709A9F326ED65B4F95E7F9A983A7093C2D0267BA177169CC064E53A854792C88762B96B6BDA7392D10FEBFFFDFFF539AC87D9801495AA4C991AD5E2C4BD5E83B7DC459EAABE345F60BEBAFC783CD184E5893DEE46464EA19D7183A1B3D9BFD1819FBEE997BF2C7D8D8777330BA02C8C830F86822223D8C891C2D3858A2FD6B894AFD407ED4DCBBAE7A57D56BDB244703D69A9F07FCF2BD946EAFDAD151A6B193E1F3E171725B261BDD6C6F20959F4450FDC4318D9D38F5AE49745537DFDF9E27136F056D7A555F0CE7877CD8C5D44BCBF24C3CED7E18AC742777534AD3E164ECEC98969E04458BA4EDDA08F82E539E7F71A042DCAC4853716A12A40DA3BF5BB5D04B7576353992A537512A587E1CA85E51CCC04034D84900525A0D8668265081B3A8ECBBD88F791B3DFC184DEE8B105507173D060A4375267F0692B46561D9512A305C869CA99192593B79EA6CCDA312FDA06E96D50B7FF6753BBF2E0DCEFF307C4EEF26F1460FB50A3F9B533EF7BDE4765389256AB9364CD5E93A7C975ECBDE7C72EE624D1B96889A27497114657B61EB6557A2F84F5D4BD13726A1513BB42DCFCD9A680AA8A028F096358F15A82F1C5157869AD4D713A8A979D44C8A64C40A03516A602504D1FE466C1A0ED285D79EC3B3871FC5C3AC78F0E03D389B199B7D03C804EE1B67BFFB69E0A4A965A229AAD8BE6A8B0A0949931A3CAFD7233CB0A648469DEF48B4E51E251326D4D7EDC9F719F220A6423871FFB06D1BAD76D32DD93551FC2D58F2B31D2FD455FA44E5F1C613AE6369C26156FBB28153599E892E4FF961ADF0B4E96E106EC670B3BEA77D004E2D455EB5975D65F9D2C874C9D6276A2F9804BD804C015270AA3AEB449DBE2BABA2A9F5C3EA8A11B60694E94AC8C8BA1E45FB99DFDD7D26C072438E14734791F64F51DEE44C9AC94FB2D0CF0193970FEA1BA5E5212CA94261A9198DBA5FC169AED0177F11AA7EB76DA8AB2EBB3A43C59F7AD566BA21FF14E7C38F87C1C76EBCD63309D1712C3891F2207DB51F555D0D2A7802BDAA29B24A9A1C5A3F52CAA764A3CA24CE5391E6903893EDC1E46EA24BD6260F6208F86962E42B4FA39E585D27087F72C515E7E15F95BFAD795D7335B44DEAF4D22B785E77AA0309AED273A0FBD55E12222B94264469359C469D15CE517A9CF63742161BE21575D18395E5DB1A4B693F76556D323BC320AA2FD09E88A400CC0208F575DB7A69690FBDEFF923718FA24B8773F6FD211FE6ED57ADF0732743F83C66C5623E7C553FBCF3CA94FC1E4B8BD63572FD92A62ECAF2D2EAF2221BAB344F204D78DA383F0891073D3C546ED6C3E9D5275EC7C04AD3263B4685951CABF2545FCF13B25CA54E361F5EAA07D49A3726D13E894AA204A2D0031E63D4A6569D35672BFE369FDCE548795050EA1917547D69D73164C2AFCA3B5E5AE2621EB5289A332CB509A27D90653DD69C5E4652E17996652C43DDAC4ED6A846F176673F1E60C6DFC2CB7D7F987D992EFDBBDD79D3C9F6F829F24888C6F279D7E4A41F8C7BE5216CF84F8353DB815EFA6311BA0383F394C5094D70226C57D2FC5F133875F4CB054BFE697F7BA274C77211DB4B79D97C59F56F97DD6991E5B3AE3B69D22D6A2DD55A1335BDDA44A94FFB38109C015224E4BD889DF90C0E961473E4411A328BAB0C91B2B716EAE0A16955485391C3766FBBEE1A34B98F785A59273255338568DB5A5C456268649E26D757479FE1CFBEB3A9B9C6CE379DF27E3ABE9954E76EF638FB365B7ED96DBF823A437EF838F717960EED85C150854F69205438EDE7B2BAB4A8748D0579524997726CA59916C789E2C4D5136F059C40D0A8F9BF2EDFFEB9F4B739C5AE21E0C74B03F52F5FA3D6C1DA6303D5B5E69D40FF7238AD4CD50D549DFD4ED5487AD75F520FF7AE2744C367AA45B2FCC13DA9061C1A8BBB6BF81EDBAD2D7FF5C3765671D28DF85E7231CBA9531A22AD177FD9F75DADD77764815AC2EA1B13564D2A6B5A211D9310BDE9C4C36C7C553EBA6CE89102ED7F1CF43FF0C3B18BC388F92D059A48966B15E6D5277E315ACA7A9991B2EBFF5DFA8C05E7A97CAC8FF1B460C9DEDEF862A49EF6A7741C9DCB9DFDF3B3D9F4F97B1EA72B2EAB25CF0AAA64431259975DC06A0016F3BA4BAFDA5E69DB569C70BC4B3272669AF89F06CF6686659E9F67208CD3CA95ACB5500264D7F4FB756D528B930F32E9246FC52D03693562739F25F52A2E3F32EEDA8237AF7FDB3538DF094EE5FAA683CBA8BC29B3E3DCE084BFFDC1756C080572C189868940630DA2E3F3B496B53A9C9759E9F89DA7A55B0DBD704D5D7790E5BBFDF159A61910DDEC6154E292924A098FD3EFB3F26E32BE2E1F1F1EA5B9E1E3F428BE74CED673A7B6BE746EF68517508DFBC5B9AA3BE7CA0F4B665483FAD2055439E17FB5B45A55C1189CE50D59B2C0909C132AD4B3DCB65544479CADB6E234BB31E5F36B59C0D3C757ADEAEAD31449AE069FBA59F37B6B262D04A47FCCD78F2E47E9618633A82794BD7804D4AFB37467407592684CA2C74AD4385EBFB82CBDA5ECCA7413B0738FAE464D5699A9A63F9AD04EEEA6C6D5FF8374EC272EAFF27A3CBEA499D8F9B739AD8BA4E985403F9456F951CDFD9A2CC9E23439A3F402ABDEA1D5A56B1C6A1CB5454E477C64D7E084B38DF6621DDE597EDB214EC95D5D7683478313A75B46D48DA7B5904C59F2D2C7D9523C55F34F976D711D58CF6C103AA8728C77569C16D951569C15C6D9FE43812288A63B412ECE96021D92A5D568A2384BEB4B4B171DE5A3FC1C3FDFF75ACBFCCA84BFFF38B529CFE3A35C49E56D49BB1EE32DE16D9854A892CE5C4EDB50E7029C2EDD5D5A322E57829457C654382BAFDE76E590026656CC0AA22313115402714EBFE4A6D90625B183D377798AE62F3ACBA351135EB79A71B269ADB6F7F4B2525A8CAFFA1AD3FA6A89976F3B7FDB527F6B63A7C96FE5A7E2DD241BE63877D3AF5327509CC7E99769B617E687D6DFAA404FA4B1709696361CBA479D323138CFFD202A4993453E926C36DB8FB283281FA674F2FFD1FFF161FA6D86FF14A5A708B4843AE139AA54484FF1C224C8C5CB97ADCADFFED5C0B9ECAB5317B4AB46A0EB19551DA8B64D91F4B8CD97D904456F677B36FB9A47BB917606B463E722999EE2B6C5C97B6BA40FE097CF3664B2CAD43E80116EF51FB75C6DEA6664DC412D1B928B00E576BC97E62745713E82BFC5093538E5E690E9FD343F804063CD6F79203859899E576EB6740796A5BC40D6446D31E3125AA4B2D1E6A0F5BAD55F8DB86C2ED17AF67D4603CEAFB3C9FDB4BC9DC0103E719101AD3DB9268D6F12F571FADFAA245B832ADF32389F573897DDEAF7B257DDDA73DBD696930BA5EC30BCED3ECB8609C721DF99946449C67CDB6F6D15A111F443DF357A9CFFB4EB271D4BB1C5CEAD2DA7965CB9F95AFF33DBA97A6D13A237EAF16BE113CFE32F519CA3CB31CE230CEE4E27019428CE2659B2624998888A6999618B480FA7C44BFDAEA4C15519534ACD8A9FCDF7C3D6CB56713E36759180A47B970A78F6FD81386F2608A8B8BC8AB3517E9CFBE7F7E9A859C7F9F209AFEB775AB411D854A7AD6E5D4524072FACFA15A75167B019E8A2A33A5869222BA4AEE9E84B0381CB5E122DE87EBDE1251711EB7FC3B2791FAF4D12D4F2E1698FDE8F9DFA5D75D17F2D875B717E5A8CAFC613B9799D1E4FC6CF350DC1E3E82CE7679C9DA5F16ED05FE9E6C88C4E13EB487331119FF4D365A98B09B0A96A4CC39D2CF9A139C749B21D180720F0181DADCD7E3C4E45A09026DE0970E222034E93A1FCBDA8E2ACCA128B733EB8562F30C074B9E2F9D338AB76BFFD1FF1627FC5B42D8DB9679CAE93CE9C0E125445A1AD1C744CCB607BD56AE2B4ADBE7A2EA7578D1D5D747E55B055D9937A5A2B56A58B9F1DAC87C4795D4EBE4C712AA90F397095031EB383B8B712B4DFF65BAF3ADDB79D91542C2EC5D516A0CB74B49EE193B6C7CB0F16274E44CD84C1F21F663D1AA11FEA0774B60272F2F5617C3DC101E2BAE7815A0BF4D7C0395F8FD67B43CB7F1B9CB536BD4A426CC90B5EEEA279E19A472F797B0571BEED3CEBADB80AA4EACF99B3FFCEE0A483B533A2A615E07EBBF3AB7575FA658CAF4237F1E633F60D6FB7BF1A20708E2188FB29C40189E084AA6E1CD1C975A943E89DF783D6CBDE487DE9B90C85881725AD4BF948D1733ECFC74B65291F40C94F9F64D4D456A233F5E77CB407D36F0F1A3EF11EC63793F27E9A9D14A6B3FA54F760BE50793997FABA8691496B9749F15FDE225A153BAD57F77E8991AF15AE39C9AFDBCFB4ABFEFF7576FDAE6DA45BDB7F428A5BB8D8C2821412B8D0800B0FB8B1C045042956906205291691621129169162316982D8C298148B4811448A05A50828454029169C26A02D024AB1A02D024AB1850A1753B8982285BEF3FB3DEF3BE3BD97EFA22B1CD996BCF3CC39E739CFF9F122AD6543F17DBD927AF6A5C59B7E9F1AB47A06277365BA25D93F64998555A730A868109404AEEAB9C0291A21FC3A981DF834641F0CE7D7826D540C541FBDB30179BC4EF75EAFBC29716DE33B72B3002741CB70F6EEF7BA675D7CD1B0C445077320B49B2F1BD17AEC6D3DB4E48111CE2FE8845768A39BC5BBE5FCF59570CBDB324ECF6FDBCEB62251D7DB68A0B5FBDFED53C3A6AB863A4F6BD66F709AE5E474C1F7B01C4D70B2CDB1870CD6C3CAF851C7688E5927C767DEE1E1085EE6396D6E70EABB4973D4B10BA8E6D8F97E6AE7F0C75CFDB142383FAD818C6CBE160C2A22FAB5B0CA3624F8D941ABD7CEC63FF416BF8E2063211F4B07367FC4DE6EC8737ADF8FBAF7479DB351F774B8DD6E97EFF1983E7CBC9B6DBF6C3827B14266842BC569700CF8B9F0BC2D579F208417B35748B9338B61356A6D6C9DDE279B6C94BC88F0641C38B175C15A8AF4C7347C760296A634F11E06B59FBDFE231C0FEEFF30E44E2D468BEB24F9B1632EF2D72B9C4772C761C1EB2861E1FA171C392C552E8862E771C7D56635B8B63BC0BCC0AD31A20C273E7F119EB935446F768B57F3D9B3F3C58B4B089058AD7C27517349F4E7EACD243F820C64D039E9B79ADDE9C5D8222898A669C2F50F7A7F40511EC089FE465CE76FAE66AF17591ACCEA686D338DAF1135F5A0129C0D0FA73AE14076F47DB87E2C66731880E48C650FB17C3402FF865E97A6F2726C84C90C868CCBD45ABE212A14FA48A3BFDEE463FD0C19483EED7A4F1BFC6D14353D11EBB15B43EBDC1694A820C3DC12DBE44C546CA8DC0197E124044BD954015D71F1F2F504E8EEE4C9286BE6F018FF3424CD8154246CD95D06377B3B9CC8865865C4CF2DD65F0A88025348548E2AD6590D8DCA21525B6C47666AA666D649CEB615EE80907A129C072D1F385B217DE8AA753EC2117EF0BA7D1ED23BEDB59A99B72AFA0B5267DBD2767D65D559C3B917A1B55E2B601EE4E08C23ABFE00FE7DDDF5E72DC0098416AF2390A06287364A6C73F3A50870DEA0B0B07C4334874AD92BA34260A09892627944EA24D2CC80AD46B528EE6238194BF1BA08277E34F06D82B393A5A6597151CD2CC24FAF98F939FE49C08F2F5A304D08A25E9A685B044589665F236B986D7235D13D0C9C0FC93AB9098886464459252A94391ED4A2653A9CB4643ABF881FA0ECD97E920523A7FB74AB46E9D3152BB4B1100A3472F17E89ED90E8E85046404FBB055C319871F30E6AAA74DD054EB0CE3F1652FB244B2597CB22C3EC8A84594D5AE60984B58E5788347B5D0CA2E58AE4A1F1C534C059AF24E401D488FF573C2D3689ED931177A8436CBF8E0AE53C4B9AD9D5D6EFE6A181520C740F50EC139CDC6940DDEB0356005846D04B2C77537E16DA30D922E1E333AF4C5AD279EC8B9A22C7079DCFC48453B35D315C781348F2184E42B16477B725F99B7D6F69ED5837BBD51F57AE88BDA0077D0D88BEE1BC531A47286399AE3FAD1848EE79D8DD0A6770E99BAF08E7F2CF0D4481D12F97524B49F5F70AA8490351144759EFCD04CEA32A9C21EFB450DDE0B10893938E43D52CA77A19262AB82C853AD9D146597F3FEA340039126C2D7B21F05A2229383188374826379D2D7AF04E5BD545B54E8BAF511AD30DD6495456327AEC8E2438299A2AF92CB926BA92B47241363A13503F2ED85257789CFA5C1398A9F7ABBBC4C746CE3686F31FB04E9414806D58A3504DEC749CB3C634BD0BA5FE4D74B64D81F38E7657470261A577D7AB83467AC5D9E2B29BC7F8E8A3698A4204DF80DFEC688780F15878173359530F62514A7075B5B3908D78FD360A9F9AA5C8B78E3ACB4F9BAB0F2BD6D8589D292980017EEC7E2D4D641BDD7C5E4BF814D3242CFF6001889393C58A717D3B33075BB54B8396DB0F0A4D40915763AE027970C1AB4A2238DB314E16447DD259ED5B68E777FEB31FE03C6848B3BC74CA47C073F6B97F60FD7F9ADF5BBD858A5A7BE7CFA6F0C0E1BD47E7BD1F06BCA680A5F08EF4095848436397BE4B9E2B267B955BA6EDE0545DD84CB3A3FB20B2C08694C7FABACAB10C474E5F5F41AE4204645B08903B31D3ADA0EB13440060F56E411A9ED9253E6FE8D8AFB5C1F96EB6F96B5DFE4B8AF24DDBF5AE83816E917F95DBEB1DC0097758D0C8EADDAC8FA02E3FA9F0DBC6A1CCAE10E7C00B6EB30F551AC51E5B72186D6CF6A497B9E7DEE4C51C1EDC158D3327F70760A67059AD728952FD49CF880F9BACFC93ADF3001D7AD68EB89C5AA73952868A26585CC53B14CBD43AF15E69772E9FCF57384A8084964926C326822A7D11825C21CADFEADD3C34DF7E5049EF83BE4270DEC666D94C77B198C0CB10780FDF76BB034F0B9FAB032A2E57B915D43CB2609775209C6A8E9296E86452D4261F4CB0A370BA144888922A09ED7C8F162BCC67BF639FFBF0A73113221A32C95AC71D6D9CECB2BE8370DEC34CC6ACD32A3B215A1C3A385D9622AFF034B97E1D88EE592865C3AF4F7E9BD3B5DB098F753983C0E9DAD2194B2AA295220CFDE108D187B9A8B594C34853D937C783CCF7EAB764A585DA3DFAFCAFA81BF3E09134E92415ABC4D98604A32E51696B77FC77C47D3454DDD9BF13E04CC3A7044E6B2F729250D087F7649D0D2FB2B998219CC086480A6FF08E4FAEB71CCB9082D5D1F82DA46F8F53257F03265EF4244A54CCD33AD948E0CC30CDED4C5E2C367F4B7E6246C99889A7F5706AE18C1C72C136CA6202C7518E9AF00A7A5AE75477650CA77E2B82F39BF02020DBB88DF5F3563B776EA73951CC4BF313095BE4D24437D0A682144EEF72496708232E0E4EEB0844EB3CFF758A0B337E9BE18E305C3136A54DBB3D162CA82B537B4D084E6E88D5BF529C2DFE719CAED86DA885940EF7EA9D0403B5E5923EEFEC98664BAA05C2F9452A8E85163AD8AF6A96123A2545C22D03115DD174A6E8B7822579DA9BA863F6360DC17D965827046FACEAFC5300A83AF557D1F9AA3A918F7CE10B8153AE9B2AB4F0B6779244C52436F3B407143BBD815A3F03BD8350215CE5F7CB04B7FF3DB91CD268516670DEC3693D4654E03CEB0738B555093ECCFF37887C7F2F34B92B890DE5EB20D85A7F09F68E4AEC843C6F4BE9A6F7AE82AB153E7DB473D0620104AD73CEF3A02B2E68BBC0B98B9FBD99DAFBF38752BE4B5B713E63A9EEEAC3DA868D0C9B5B43A6C7D2528E23E90B21F535D372582EA9E777FBB1094A2A2219E75DD79D4B3D9E0D2E7C1E8A6EB307F88DD92E9F4E004814FC1EF27E8A01FC904E84C914114E6D72AFA5D271F2B44A6E8367408DB063DD46F74212921FA7127C52EF64667BF9DB02E1DC56E0B4891132448B7689D68A068ADA827ADDF78CEB95FF81DB1E854662FEC2E48B352EB32DE76F972A79FBB2574DD44C1271EF7B69BB712E48A84A8A98A1B1C653BDD65C4293BFA114DAD4867AC31288CE29C0F97482CB197F9DC2179C8302A23D9ADD4455E928E79177B14E8093DC6FA61B73C23CD44154D6C9751547472D3B73E1533396AEBD1E85D8A3CEF8D7D9F61FA4B5183EB7A58F8EBE86B573CDF21148F42D2E59CBE3ED74FB751B21572BC1FB7496BE05771557B3D714CBE7EF57A187CF71855A142D85D01765A08583A5A4A78EA99293DB678F6A70EAFB67BAEF244C9F35B4A4CA6A0CC279F97C067082A71D3EC1596D1CD726381133FA60E92422728BD2FC298E69E6C7A1F6D6F0FEB61DCAA5B2DB82A7E72D341AAD0D8DF03E4BE110DB4538295CB1BBF3100A007CB9BF39465AB1336CA87CA7A5ECD7D3E8D7CB5DFA4F530F2C6AD2FB239C5AC78657A6AF16A120555F26AB50A1003057883142C93FDDCF674277BD75E6CE3A737FA965FD0963A9A56574B6CC6C3164B23C0470D28E64ECAA3D06F36F70C33405368293AD53D793359A153835A19449BFFB349D425A92D783427272D28D9ED93A2FE610B18046B27A702B9C4E7A3505C063C39D252B9A3FA987B3887F65AB70EAED225CAC900C18F23AB92395A3D6E42A2EB98CBF106587E04C7A74A57E99AE503013979674371EEAF13E22388F3B7B70BB4D80D95ECCD8CD0E990A71233CC1491B8246BCBB00F34E8247FE4BB4C6E9D990373E1E9C505FAD4A42DA56225F68E7748FA950A1D6C9973885D3662EFFD54057EF1724BEA3169F20E78BA6EE0ED0D2980AB608E78DB6255C979317336B9B4A7395766A9AC977F9B7E2668320E57357BBC11951A1236CEE329A22D6E9EE9B9CA56F8013658497730015BC2E0009660AA0E2DA931F4698AEDCC58D15164DE1037826303B8E1A1D42F8D4465E8647B0E4F079EA296ED450122AD82A3800152AB6A8947A38BD12E4E12C1D9C260E0403A5A2CAE6F3BAB03EA09B7A4F6B92212AC39CA2100FC2AA2A555B577F6D2F9F4F23C136117D2A2806D3D47D11FB315A7EEE9A9D6DD809A68340DC42E5EDB211721885935BBF20E99CF2BA8457735A538E6A1F6D271A721DBB75D2397F7249E1133797F57E4468B3BAD8C98FEC48C4201E0C25D3EC4B5BBDA9B2D6DE17F5D95A7CEDA27592C267C99FC059E9EE49ACD3FF931F384EF4F252986A0267EA9C557E52998249105502B02A77F5C71AF83F57647500E8562C4385DF153E590AF0C9225BA74A8699AD1FC22B696563749362948C65A319F961F6B408276F298607203AFB7D31793EC3D849C5B2EE59BF4553B780314453C837E0F3B0DCFD60C83968154B70EE2DAD62CA9C2F0D93F2ACB5741D309CC7DD286A9E46E68BD6C9704A96528A27AC3AC99B8062124AF90770B1DF0B5D57919866C5D392BE2F364A5B3B396AA2C8075F2C3FAEB9D81966800E1D658D95BCF49FEC69054ED780D2564D94A910FC4FB3918C57E0726E6379AA6BE251C136AC62DDC34DE26FAEA62F719F38D9E862FC740AEE14D53E4A3DE1F7C103633FD18F23F8CD1EB862F8C28D72460F1DAF771B0F683CF4ACAFF519E5B7BE8DC8F7F399B3A5EBB8BD2E85DC2AED4C138CC2D9622CBD9A7AB77CB7149C6AFDAD712B1387F5FDA5FAF6A5E4087AF5610597223B0E5B11EA7DEC6D8FC3CCC5452BBC6401FEC34CF6D5B0698A30AE5DB891A7CDEC572238014B304D5912FF3BD1A2DF66E05206183BF1D6D8BFDB9A5CCCFA1432E177FA40807F3A67C1AF2163501A41F5930095EEBD7E6273B9AE4108C516CD44C540A9139FBB142E5F2CA42FD2E0F4B1D3B0FC16B24FDEE4973CD846BDD9D53F4AD30EC39B9BB2C89A2DBC0ED629B56B9DA20D8CA65A0B8B1CAF4E4BBA82A5C1D9D2231E50C943CD56CD579BA7F755114CF2544B6384AFA0B32538A704E7ECF515CAF117B3F1AF53A03F70F539DD19FD3A193C1CF116218CAC103BDB9AA8B89D2AA27140003FD1A1253F8966C570DF367DEC48D019770CF5D83AB7D72296864AE77570B0A1A27293DA65CDA314388BDB03277F50F896A628AC24B07B0038F1BFFD483742DDC269AB590A3F420692144C22E9C71215F1B75C7B89AA667127911A283EF6A6BFD30EDC570B5EEA86D6F97C86C5B29F7180B74525EB2E86D261EF3E325B88A3C06E4267894F8328D0E2EB7ECA45ECD28CD5B750BB086ABBE808CEF1F3F9926AC56697CC4A383A4666741357BB8C10ED221B2DB6BB08CEAAB667DB0FF40750AA2D245761AD71F5E7DA4E4D314F5B23C44B384C32197598CDC82E63E9270F9AAD1AB188F54D679AFE8688667E3314F9F8C8154C405F2E50F063451ED8ECC3114A07E0C7CF7A609D39AD441822CB7532C2DDCC440A77DFE54C8272AD96B0B1D2E96B1426FD8820D3E0C09550150267BB78BB6456595ABF6B219132B1B0C4347DDE6285309161AF6B7EB7B4FB23FE5A3811A79E540C587DDE64DAE254936ED6711F43B46153F271E0F41194E1F4534799D9F45DBFA2483DA2BE9B455F5485C6CF90B8F67E1C8D7EE13391A6BC51A8470E167FF930C37F3E18C05B00F0407A857F8B75660DDD9E63BC8BB1C1B9C333D16C731D39E2CE84D077E26D5777965DBE5C5CBD5F05C1F63A68B63C519B44BE7FF7B4EA6FD50455EF2DF5BB1E6623C0D259C2453AD46CB1EF2B9C6094B0A15AF5A0AD9D53CC69FDFE9948110CCD22614F6A53165E9012D40892501A7D35A96538C1D342C844BB846C04E8CFF77DDE354C139FB2D8026E103C48EBE108DE05ACD9C7CE16C379372C431238D9E02C7C6A42E9D9AC2DFB4A4BDC47DDF3E7F3AB3F56CC86D813461016F1A5AF83337AB1949B20BC0FD3A858BE17500B1386E84E7215157878C1B695B8D958B08DC2A77A5AD74890D661F8E743A2A2FE53AAD6F8081ED89542837C88F54EC09249103C438AC2CA5C4E1BBF39D4711B58FE7D1F8C12DE022CB847CD442E76723E146D6A55BBEC5B877B500FCCC14A4ADA0B3F7322FB55CE2F104E368EB2DC252CA670707A2EEA218C9C70B9D38D078A59B9B3D545969904381DAD45D3240F81B3C35F3C9C6E114D62A015792110C62A9C11A9C99DC8D792668F8364736A2D9CEA6C17EFF1741C4C515ECE810AC13F478FC6990FE65CA36EB67A3F9EC38B63485AD009F75BCA632DBD8D16EFB673D90BA55E37EADBD3D9B4D022C4430D7CBC1E58E7C5ECEAC33A649CAE493A0050A98AEC6E5715C4DF5EA799A5A52846AF8A04CEAD30EAD5A70D5A673B6429A17CEDC5745F14D312B79BA9AE01D26902119CD9A1F46139A9165EF7BF15A2AFC039A765E2600A68A02F177C7EDDF993CB9C153E598D48CD5D4779E7B80B9115BC714EFBA66AB0E448403781493CC1D33A85B6A39B8D82119F9A8112B3FD88AB2AC138C44F16CE37168EC5F874C5119FD2D75B761E4E1741AD8A92C80BAEA862A3BB2B9AD84DACB325BB62E2A2D861EC879B99CF3D9C117B683373B65E82F7F3D8A128D60C3138B36679899D98715ECDDE825DAE161F56F3375763223BDC0E2F1C4783F0E0219E8886821F290C2DC77D1CA21AA27972DB82E87154D794CAF6BDBEED5ACC942B81754E5E5D817526CE76E7CDD16B434A91A2D24A952895157F6B296CAD952B05E3073A5B8D9D4EB34D958400AAC229C1AF19F19724E85A0874CD2558756968C6D9087B8892B435F38F3DB048483431D7BCC093EBC046015AF0BDE74F26583939D41D7B34EADD7B848B36014ED26C2DF574BAB08B0DF09FCD1BA9B313DD38C55F8B06A48D0A222FF8F0D999BE59329C8532DB9277E3D509B69175FA657EEA937D99656BF250195961F516E19BC93C306E48F8BC3509C6BA84B20448ABA2683E23E61536F0A524C879CB8E586753E1746E56DE21BAC859C28CA46D1A7B129E4DE18BE1CF7814ECF839E42A7876E8E0C7F34CB832F9DB93EEE4B7791F77A8BAF3D27CF6E9E13C092B5179752D07513A4EB86F066AE29F9556E02A80752E3F6E18C8A0D72403088EBEA670D23357D65279C8679F4954AE2A7F5BD116B6D725982678AF285139BC4DB69545672DD674EA1686A79C560D346AFDC20B2B594A30CD04CB5068C3376167BB001F0BCF4314F006BC0A1AD710FF20ABF7D9FB6373D73150A1F9E8E74B8E9D2DEF692D82EA67887A772CCB6D745D6657053F5D5D7FA6AD273C1678AC70FEB96137BB557D2E0808A518ABF52498B3F5F427CD552C5D89FBC7AA3A910457D770BBFE52009C7089EC3423F5B771DF90D316141E5F42C9EBAC33525F89343538215115A9658D5E2D33D066ABE2BDF1CFD89BBF5F010F025A0B4191AAD643D9F0CEF366DA5ECBD6999DF520B28E9E5C22B375961E956FB4EE93C9F2F7BE14EDB43ACD7BC2737F1881ECC20D6365972F16AB4F5B2E765A335FA400D8B49006C57AD9B6026D11F31DAFEAD99B9BE0676E794325CFCB17737F10B219A2E2D749E154D1AEA21BA46E36B24ECB520E82FAE362672B324A0552E0E41405455A6C45A0B9065A9D991D7ADDA7C5F13CBBDF831FEE4326A3CB221B91BF8DCC1FE0ECD11A3FF3BA8E19C5951657B8E644658C430D5AECB41AE4B583332993FDAB30144CB6BC1DCE9BE0780D459951A109B2CD972DDCF1DE346BFAF9CCD9B2C4136D2209456CADA2D43A5B9774C68BE31B6EAFB17523B4ACA242EF80B1935A32B145A8FFD339F691F03E5BDABD273C5BD7A480750257829F64671B8478C789EC63723C7909A9106ABF525A8981AC16AE65201055A1F5176945F0032AC1CD9A3C14379AFC17A9EF9B6699371109AAD528EC13B758BEC65D1BE0BD2C5151D3F4671AC552DF616EC5CBFA7433A5B54E0BBC9BF2A0965B429ED865F875889DBC2304491035BFDB4953A298B75D6FF4410B2EF7F84299ADB34E7B8EE0A49E313E8BC197C9725D6C6128CA8BAABFC3F51A3D9BE250833A5BBBB83B25B111B3754EF8BF3E82FC7B1D97CCCAF83DCB2038D0D2146CFD827BDDB2942A090AEBB9C8B135DCCAC41A38DB91755A290D329396AED1F48D5E217768BA0259E51DF620C2831B81E7D5C7CDFCF5D5F89749F7AC673E8167E283759E76272F160CA712F4A0D3262D497872F723391940E402AD693BDD20116CE55098D12F132E1A1736FBA703409187F4ADF1FF239C558B2C63446F8445F30FECBE49DF17FC317D6AC3F0CD25A9693A36C4A99D5BE155C1C0FA371D1EE899A5373A0BFD41D653929029AD7786BC13C8F7F2D39AA7DD567F6D977FAEA7CF675D1A3912F5F528FC65F98301E4A9E08A1D9CB985CCC815B43B7414E588373476A90F5B44DA535993ABC29EEBE4D3D8397A3AA50E9D224D244A376C54C914FF1F70266F1EC87304278EA9209C8F46B68B2088090E425F380B44A6194BB5ED609A494B11FD56C356DDD23BC4EA69DAC6C0ACD3CB08AF214B592EFE4049E88A9EE7EF968B77CBD9ABC5F9CF979D939E583D11EEDEA373CC67E0F5539BD8D57319EE1A7596BF6FF098C65DBE1FF05A14A9979D86D5F01D55F54227DF4904E7DAB6B679DAA957B974FD252157F9F708EA1A01ADC52BBE33E27B853E880B6460A038F96A6B4222CD36905B7E04FE7250D5710275CAD44C4D7455EBCCA2BBC117AE1301214E43F7B04FFA099ECE307C3A193C199F5F4CCF9FD3AC27360D49DF90F6EC367A8FF1CC4174A161623737371BE0A4071831B6EC524F7DE74C8BD8DA75E08BA041DED32EF8C1E3CBCD3F299C56EC0C3968A9DAC237A54865A862D6A2EBDFADAA1BA42ABF6A435B6264D8866154C8DA8562F3128F75D07278546A9CDE345D8348CBE07403464EE44B345B97861A9C70B9073F9EF3D6AF2E35F00D1F2300D26A0B9CE81EF671F18DD6F961001843EAD9BD3708701EBAC0E9ACB30F6FFB888E9FA4CEE91E9F4D7616F293D0D2A77B85AC94D67B788E23475F8AC43AAB661ADCE675C97A506959666D1DB48EC1A6ACEA5B50FC094B69BE952517517349454368A7EDC72DF396357A50E8B01567ABF087A621767B35D9A7FDD3C1C97DD23CDCD9A368C71E120063E9DCFE3E206904278D3FFC240D60BE641D1A61E8BF6A4807EBC89D41F70AA6B3D6B4E7124D15FFC24C60F787D196BAE02D28862B5EDC02A71386CC6A6B7350CF9E3C9C92C6D80F289C16BFF3533D75C234F7B67A4B978C2658D689E69172C45ED7C3D9E053DB34690981CCBF5B1A4A49E443601E8E70DEE8676C28C1BC93D72DF21A625A3024CC1638F44967FA0A6327589E6CEB6ACB9CA2ABD0CAADA7BBE146FC2C736467BAC83158A44E06BA1D35C09E10CEAF6548346303DA69994C5EF916155542D6C1AFECEAE08C13D6B20CB7C8CEE43D83937E120F063C724D4071AE99E96A785F344CA5B8587937721B8E2493C0D9F0CE3682334D70FDF159B857E872F8130E8EA1244407E682B186C57BF8799677361AED6CFAFB150EDC7F3FE411793E98B4E19DADFA9C019D79C56BA878E5421859B9E7FAE2553DC8DDD195F9D940D69654ADB34CAD3350A1B85E9DC651575789E074D6197972D3786FE461B4566CD1670BED7022A2775741C189158634E3544D86B94F38ECD31FC591D02215535B2E6DDD1B3FC7AE5A1A229B92D99163A4A3C76255887721EF03519AFC366736644BFD1D9F0E4217CF16E291DC8F84E2F249837C1F480BB5AC134F1FE0D608CED25B4FE98A91BB6FB1751AB3DD055B2C2B9E76A7A4C9D4BB1A6815574B6C0A9DAA37C156B49828DD1437DB6AA670DE86A54A7D9DC840E957AA5846143775E0C1E5F2F43556C746B65A08772D8E06E41EB9AEA99C1BE1ECFD8C47804AE2A181C44750AE0D81F10D1EEB2CB76C57C59562F8055BB65FF315CE9395D62F3C2AF05AFABE8CD7985FADC9FD931A9953DECB327DBD0642DFEC6966EAE0647E1BD653B65D6FA63B81C39B667CD16B1055A3EC84D8E9462B23389BB561B8E681890AB2216AC30433025A3B7C2C0B2F28E54FE06CB44E71D29DA68EF8D0959EE52A2DE76C69311C58278564B24E3D815E1856E678AC3B1E580A64AD7617986D2D9CBB2A9C898CB00B361AD4A2646B5B256A7ABD29BCE8521A6CB2F5709ACB3566EBD4B188EA7B4D2E2EB9787FEBC3A7D0DA036BC98C9BBE6A101573DF431BA22A0A8F5ED3F398BD2E5D681DE3B6C5F3ED6CFE6689E77A9EF565F6F6D01F82270E04EF9227B43E035DEE986911377B5AA349E7D4BAC2A2C0C95B16375F6338BFC557F95B8D79D59058EFA5B5B422C2E14D28677ABAEB935A9FD85CFDB9D6B9EB8E1D831B0E643CCC13A3E4627ED4455521415538B92DCBCE5776A967256AC6708A664B16393EC744059BBE46A42AF0C99DAAFB50B1544F6F868F99BF5D02FC0C092F38B1D4AAC107C81E77794B11243F839F654C9FE1EC291522B556D583D0452D430DC021D75FCA103B2BDBD6FC80987F0E3CD64F2059D03538AD67A570E5B0B8CD93B12C6E840DCDDE5EB985257622988A09DE1013113BAA81D4545184978A62DAE28B5C07679DC09448F038404FE688B3BABF48990C45F6D370DEAB49F0FC31F3772B84533BB8E49EE23E5BF8153A5DF2921816B871B84BC8E55E92CB1D06427412E603739B3D3238DB1DDAB25526B615586E323A5F1D258BE1F49B834ADBD9C85363D715EF5D07A76E45F002421E9A86BC6AEDF2C214CE5812BA15CE7FB14E16D2DB7537C751BEC7DB4F45497872C93BBE8C04B592553364A093D7570354E175D5AD8B16EC69E1D72F89008F717A698658529F0386CFEF0D4B71B361D6ECA4679202B0A1D55F299CC1106F82B57966F46F707A6B763B1B4DB60D994F9C11D90F68752CA75352ADDCA8096825C7BF45374F8A2111A81638F70FD244A5D2849049AC6D1BBA4C857EA27114BAD698A2D0E1AB684067BDF087DE8D0C74F0F4923BA711CEC398D151130280C74BFE8003636DFC09864F11861E84C65A099936B2E297271C75979FB62623944E73F7B433CA61ACBFC4C359493A39C349E1AC936D23388B5DB53A169A4B9A41EC8CA0ADD7CA232528B38E02B34E5DE516E08CC43C613DAE3466827EB6275799567BF5B8BD96CE03E489F9CC4448A542B8EBF47E0F0CAE2BCB2FF4C0A5A664B590849C3F9BE241E9AF16BA2C7782C1F8019DA5745F1A126C354D889A67DAFA45DD62F3F76B13F9BC92E0E1F4F9659920572BC49B32B0759DD3263355E07485EEB49CD2B2D64BD761E3ABBF756A5CB8F4F5E56BB34EDBF19FD455A2EA987CE1ED1B77C1F383C96D9F6A20C08C5896A343381C9C4A6E012476CB96749A86D07B38C43A0C6D123B7F3A253598572B501DDBCFA528B3E56D7CB2904F9281EEF4F53248F03781CAFAF423D16CCB04BFCA5083A910F11470B4ECB8748A6E8093F5F7E3D0611B70F5A9C86105D143FFDD3C81D3F721646A36119C77B53921AA9B56C44283534CF3C1802A5974A60A1D7C2D5BA5DB123EF7154B6E7EB9BC98F1E1D8B2F9D6659CFD9FC6A3A7D33119E8F9C58C84C3733E89B97B5F1799288D32F15DCE90380E708EA95D2830946F9A45A8F5F82CC2D295442E484D567F0B532019280B43F6D6F960B347859E2A805B89593669C7F9495BABF70667D305CE3829F46C28A5429674F2CEE2EF2A9AEDDDB02833AB1308F98B3D9E71972233395E3C1C8788AB6D9B519DAFA12B581B972FF0405EFE2FF4A64995E70930202C973E9FDB0EB1DE0F23E1410FE4D06539DD2C5A0516F21678DBE19389B7CE5485779CE836388375D68D924531B20CE78E05386F1C9CC52E3BEE9A12645FBB7E6845ABE963A703B21DBE0E281E054948CED5B09DC509B3B5E15CBD5142EC746FB547535D1DB51BDC6EC9554FDB4ED46A46B4163EE9CEFE3E002672DD51F03670B7023CE087810181B34506F4187BEA69CBFCD0A272F7011F5FA7FED606058F5527A273BABB0FCF71ABA11CEDA61326450586320A783B957822EA14A72BD1FB7C4B1D6C3D9CD7A55B321D0A9C319CB1A7BD45B8A9E3B4CA83744D54089C0789665BD70B6FE11312153FCFC51D021DD9BA97CB11904CBB83B3456730C6D18611D744C3DD87A7C18D86986802F1190E1E8D797323D7E0985E91D437E4BBDBD626D0E043741A071ECF7DD6C7163AA3A06570AD96EF1BD8A1A5A83625752F56E79652215ED398D2CE20C363764B2668629AE46F9DFC1679DA8A5A2B36D46A3A63F20D956AA078B01C07CE44B00DED077CC7B8E6A37684E85E34A147C21B586A9F0F0CF4A76B04668B70F649E84117FAFD803330B02D72D723C60C9526E2B4281CD29A851ECBB65C51713BA22C6931794F55B42EEFEB96D319EAA6AFFDC30B3A69BE51B1E3229211285E1A2D7270CA713C74941DFD6D3648E43CED610C6792A854B294DAD8D91238254B493A4B52CE9CD8BD7B652FB385ECBC1CFAFE80A538910824238E9C2DFC2FBBD71B3DBE1C3E46D90193935374D1B41D8314FCA7C869B16FE1C9846BDD22D8E27A463E60B013DAF86CF4DA9FBF841F8DC2D0FA6F1E6D28EDD2BB6BADD0160EE3AA07BEA918EB4DA407197D0D13BB74604B699B15B7E5F2D3868D521BF8F2F85AE7E9F54D14F3E86734581E7552380F029CFB2EE9F479A06355557FCE70D294351B4A57D389FCB8139607E9AA0C0BD4770E1A93DFE6607C0C03FC0AAF3C8130C9C21E5824AECC20D394257FD4118FFC96DA12780567E8E7837B9F33F4E38E128D0E1F12552428563B2B7D577B8225A1929E63E45521B75E3A786F05988F9BC425D3EF96C2D7DC8143B705CE86553AD3D4333748422FE0A173B67A1447B5A252334F1849F9EA6C85AA1DE5B9AEFB9620AA5B5B5BD63872D032EB0436347DB980A0C8C922A038C615AAA8DD3396584E41417FC265325A488E92130BB68CABC1C9AA9EDCF896ABB43B571F37CB3F65A4DE764E87335574CEFE36272C794891F65A9645E25A43721936DEE8CE922DC139FD7DC1A505494EB471F2363823C53C35508533B5CE700C9983D33797D44C883A6D88ADD3682EED45B4E74C0F3C09AD69765604C0F99F3B97380771C9FD22889C32A03EAA86636B271BA9C2C7B4960F46E78CB3A3ED99999DDAE0BE0068177FAC971FD78C5CD817A527605B8E2867E5380B56D8E095425EF7A21D9FE7211B66E40DF9F841FE613EB58E8E4F29F894423C15D01DAD18D5209BFFED9146D0B42E665FAB80B0EF04DB8410C987A632A1B2E53D9124EC3AB6E5206588A33D4A10B350F26C78EBECFC381CB17874BF3FA0F374B846CD9D4118321F8D7921F9800E38A3EE4071E6A22410FFC2ABD36C69DF57A76527E6B4B1CD4CD7D1C8317285ED430D6B344B6F8EDE10051E3350479AB676FE95D30A4AE3C964A376E036207DFE8CC30AD68BACCF2DF0A05A55CFB8689DA4606290779B7C6D2B70BA895DAF0DB5FDB9D41DE76C19644DFED8DFB2A4C097DBC6540C4E9CF96E67971733483D0109EE06E2D6DC1E1DF400862BDBC3A8D5413AF918C533596CCBFD0CD2DDA9B9B997B9C1E8D13AAF054E9B0F144EC427CC9993BC2EA3ADF134FE4040D2B3DB64C1CBC402BD6267FE4F61A6C95FE3A0C7173C931EBE4B8B32E5C46F758089FF8C104D55DC28E6A979C55A7C8B970AC50A9FDF16DE4AB24F55889455D109819946E65C77707539D98FC86DCB69B6720781814E5E2E78361BF043AD80043CECDEC34ECF4BB0DDF1F3D9E0F125642FF2861C26A3FC12CC512A094A344CD5EC80592F3FACF098643AC5534656B6A5F7AE21942AD269F8B463CB743D8960A61072F91363249BE95785F3F366F3F7069FBF16DA9BD1552691E83E491525AF73B30EF86A96426F623CA8629DB56282D313DAF29E9AA8B873DD3AE1101CFE43832AD470D67967FFCE90D616C3CF00A843DA5D23D51870B6C06CA97CCD5D4888281DC39C6B81933F577B67AC2ADB710C1E8796004E60435B1AAF5CFFB5315AC43B4D83496DE383203D392AC44CE921ED07123BDD9D6123FBFCFE00217C1C3F666F164CDC5A929F9017BD1BC31952C03C75B0BE7BAFAE1BC1BE9B5A67B599AF629D8930B46751337430D3A10C3AB3E012156F9DF4BF2E9D9B9DE30CC2105718F30900A798BCA269FE3A251B9D7204D546A11E2FBF8807013A7CD3441528489F4E7A387EFA1E970B73182BE804233C9C5E8CAC3461A8AC3DA2C3945801559EB51B415E17064B88D2FB6FF9EE617F3BBE98F08DDE3A745133B1CE8ABF4D2D3226A575706621E9FC2EE6417E00DB14A224C1F5324278B64D5FB699D16BB607C26CE5A4C2E31C5211AE71B2F4C306DA132A44332AD487CD551AD903C67531DF57C102187D563201B2FC8070B2B96CE89C21B29E82C1DB7CDD962EF2A175DAC227CB20FDC3A8D03FA58B97E11DD0B1E391BE38F0BAFEB45E7F5EC32BA3276396846C11739A7B24C9653305AF5A208B5AAED5A02B9E367857EBA68F7A60AB328236BC18D172F9933B53C7A9F0C13A315D793EE7C1871E115A6E36E892C867522D279D3A5C6D6EC00B40EEF826E76CE1BF13982DC08957795BD2CECA0D4B6EB29494E9AE7A5A3DE55C4B98D54780B308BE5A63279DFE5A089C7F2109828F8357868FCFA9D93F8BE03CD49A6054FCAAC6CBDA4735D3F0705ADE59D72D9D4298A7707A14A35988A83A161AE10D4DF8FFF0199D5C7017E7ECD140B96CF20021C4E4848A621D3EC54C6737E50B4D90F8D30D4E42341C763A7F7305702EF9E064829361C053775DE2688CB41E45AF0216629D1671D5C1167400207A57F4B49F37AB4F2B3050F868E4076D59DFD26AFA14258BB17476E9FD70DAFC9E3A5BDB2AE2A810D9E8774E126AB6FE87BB04121553846DFC56B96E10109C8146D6B97FA771940F1E9EB32BA73D44231607504F1007DB67B6CC65135E7113FE26E3414DFBBB3373B6F03C79355FBCBD82075EE8BFE012AFD9250AAE6449A6A75BEE789B5D7AEBE423D1E519A9163900B048F8A0AFC5EACF353CE053166F17B9365DA4D29D378E8A3614F3A0A01E78E6E2116DE861C94E4C88A4BE789750D63A8C422F4F58EC85DB2769D00E17BAA6A262CE161E9397578C7A76DAA3A52648E8257521D9BDF7C0A53D4D2BB7460CC2AB1ECC8958C21D5F4C17EF960027F05B309A1565817CF519003A3A87B9E8769B2C4BACB54EDB24761D4E20C4C04CD6B921EBDC0A9C2B8073FE6611A722957AF5E12D402688C67ED255ADE57D82606B881EC4CA6DCD52A1CC72564954E23B28B3AC28C2D275C1A770DEB933BC98F27876EBA47BFE74426765A356A0BD0DC30E3320CB62F94F69EB3DAB7FA2C589861CD185D679FE6CB2788FC3FDF0C0AB0C7052D202CFEBBF054E95CB01E65BE0BC899DED75382B208879143B7947045AE7C7D5F2E36AF5793D7F3D8F759F88D666DED7558521FB8FADC4BCCC37C273965285334E575AC1405BD1FB3838FF0FA357CA337147B32E0000000049454E44AE426082">
        <commentlist/>
        <creator name="" timeCreated="2011-11-16T19:36:31.105+0100" uniqueID="788c885c-c8ea-4eb2-b175-75dd33239828">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:08.456+0100" uniqueID="c37b8ac9-b37e-4a31-94bc-ab398b2baf4e" toElement="da439811-9b76-4dff-9edd-218d9f7d0977" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.209+0100" uniqueID="42f34ea0-a4b4-44bf-8a38-8bdebaf62d7d" toElement="6b7a24d7-6ef6-4bf8-9c9c-b3ef3c4410d6" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.282+0100" uniqueID="e86992cf-18a3-4c4b-87d6-2f87ddaa436d" toElement="294cd77a-950d-4b00-b94c-dfd39e034f3d" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.309+0100" uniqueID="32c8fd76-0b1a-4f64-b31c-8cb319392744" toElement="a027a898-0f20-473e-a0aa-25ad6bb7665f" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-16T19:36:31.107+0100" uniqueID="5ca77e26-9b1b-43d8-aaa6-25d9e355d6f0">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <storyboards timeCreated="2015-01-28T20:44:29.313+0100" uniqueID="e2e50d9e-ffbc-4538-9ef3-0b140d2edc20">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <panels timeCreated="2015-01-28T20:44:29.313+0100" uniqueID="a0df892e-dbf9-470e-ae35-18e2d00901e5" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.313+0100" uniqueID="7239978c-4d6c-4518-8240-05ad96eeb6d0" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.313+0100" uniqueID="1871e534-44b5-44e7-8ef5-eef57bc0b3f5" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.313+0100" uniqueID="fbc657b3-5ec6-4212-8a2c-cbc2d1bdb278" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.313+0100" uniqueID="c6f32fb5-5d2f-4121-9174-08f3ca65aac3" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.313+0100" uniqueID="32ce7f5f-ad69-41e2-9614-9023e67966d8" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
        </storyboards>
      </contents>
      <contents xsi:type="persona:Persona" name="Carl Svensson" timeCreated="2011-11-18T09:36:09.322+0100" lastModified="2017-01-17T19:34:08.056+0100" uniqueID="a9ba9130-b3f0-49f9-bc6f-7275069345a9" relatedBy="83a32876-d3a5-4de5-9d8b-33a552b72f81" workPackage="" age="38" job="Electrician" pictureData="89504E470D0A1A0A0000000D494844520000009A000000E8080200000098776A8A00008E0A49444154789CA49DBF6B1BDBF6F6FD67DCE22DDEC02D8E21450429624813418A085258E0C2021746B830830B235C98C18D195C98C185195C98C185615C18C685412E0CE3C6201701A508284540294EA1E2142A6EE1E25BE47D9EB5F6DEB3672439B9DFF7B0101359D649F4D1B37EEDB5F72CE59749769164E7497616A7A771721225C7517C18450761B41F467BBD10B68B475CCB3307617CC017C447517214D38EC54E1263C79E1DC571CD0EC50ED422F37818FB26EF99A427497A9AE6E75971DD1FDE15C387C1E87138F93A9A7C1B4FBF8FA73F26CF3F27BF26D3E7C9F4D73FCFBFA662FF81FDA23D2F367D817BA5DAD4BEC33F2F1AFE5F6A7F4F9F9DFDE4DFE4197F9FEFC6A6DF6063DA57B16F33665E33E185187FDDD894F6D3B3BFABF6B36AF617F57D9680D3109D8F330C856854C11996388F4B9CA9B592EB4B382383B3CA3236EF89F74933E0BC109CF703E01C3F01E7983885E5F3DF93E739389F5FC259E3E7835C80F3F99FE92CCEFAE7AB1FE88F124F4974BECDE29CFE11CE9FB338A7759C64E9E33C12F109CE70AF62C4B96FD5795857E71C9C73892A5415A542F5A529BF8537C9449AC479439C23E0FCA2D2947FB6FC3BCDE75BE2FC5557E77FAA1467E53817E4E459BE28FA68EC977F51FF887D7955B93A21D6EC7BC54A960ED20BD2FC312DA9FF913A8F3C75EE861567FB07380D548B33995567C5DF46B36ED6B1EC5FE6831BE3690D4E27CDBFE5F3FDC7E2ACF9CF598F3AE3579FE77BD4E73AB3C9B4CE75CEA73C293FE5EA473CDF2CD7E79AB3FDF907D2FC319DFD7F3DCFC5995A9CF1A1A8D308542268E96F954115E7719CD6043A579D25CEC873B991C56958666774B3FDAB1C9E76705B8C1E86E26947366AE29FE7054EDFD93E3B90D67E2744F368CC62B31FE2AF3F24FABBA85601E911ADE37C81E88C83F541CA1B8EABCEF6AC54273E621B3B45A09EB3753863C55946BB85EA9C23D0036B9E3453BCC9694296E7254B7ADA478BF3DB98FF001B352B38FFE30BF445844682355FBA98E50CCEDACB1642FD5175C27305BAC8DF2EB2EABBF92C0DCEB9A990C1B93F07A773B671459D0B702E12A8CB6C7D9C27CA32858F45C824CB3B464DC1393269AD3A5BAB92993C689E28E7B2AC62986539E7471E7B0DAE4EA615BD2E92513D57AA10AD24B73F6712A217DCB897452F8C9DC479E09CADC5E99CADE23CACE04CFE1738791D555966FD0BE8321FDC22032A4C12F424EA34B1732CB173C6D34EE7B15C181D9FE73BCCBFAB458847D483FAECD90BF9D18B38BFCF4F8BAA15CB64D666DE811427D616E33C0CA3831975566367357CCEE03C79D1D91ECEE852DD2C52D9ABDCF858D1A57ADAF117E36C113E2BEA9C0D8D7FC4D2E25CE433FF9E1320AB32F589CEFB72549DED0BD9D04B19D34B99D4B864F9C55815E7595D9DF5D8B9C8DF1E9902235950AB9444E7B2D46CD6BAD9C10DFB06D6C75AFB3232383D758A40A71581CE235AD68E65CA6385B5409AF34BCC7A10ADE0D417FFAAFB5BC3F2F9059C8B0B984AF6E4D7B24E915F9C8D608B71FAEADC5B8CB312410DD17A87E8455DBA6C162C2BD2D4CAE4CB682C56EB0739AD08CE19AFFB8240273379D062EFFA3B9CB6369D9F104D5C3DFA42663BA7B7F06D4682DF486E0EC2AF1596333859A8C42FE07444E3D2DFC60B71CEC6CE392CBD6C56AA4C6DE995B90FFF01CA52A539311F53D912FA1DD12AD4675F9A1ECE5F0B70D6A12E4871EB11D763391FA7DF082C71D62ECAA058310BCFD853694B99C379FE22CE85E133F6882615A27F82132C8F4DD3802CAFFCCAC453A4B5E712A7293D95E5F322A2FF541F67DB75B33DD8C55C6B89CFFC64F8E7EF58D64529CA9B6904CE20AC0B713ECE7C0ECED8E10CABA99057AE447E7EEBD8CC3503720ECEC4E0F40BCD3B4D824CA159E2FCE1F7830CCEE75AADF2E769919F102D52D862A2BF7CA2FE2FBA78E9B7FD5C6B7E4EBC5CA0BF17B4F8A73825767AEAF4EACE2AD112A74FD4F7B77317556603A7B0344990B4DAB5D09C83531AA19E2E4B7118692E06F93C5D20504FA9732A8D3F88A3BF16F9D89F15900BA43979D19D3A45CE90ABF2838D7FA74E8BB3E66CEBFEB652B1FC7FE234BD59CBB2EE6CFD90F9B79707397B91E87C814EEB44FFC4DFCECF8F9C8FAD76E12DCB71A5DE5FE451671032077CF21EBFCCA138567B34172FAAD36F23ECF6E60A34F2705622E851F24271A2CD208B333538ABD2F4B2D9AA2E7DBF57E6410B34FADFF8E14571F4258A35903584957CB52C30A673E55815E2F8A96E9399674A96F8F68B49CFF6D22E5FFB380F3C75EEF60CCEDD2ACEFD4A03BDAC41A5FC781167641AEE9207C10C4EDB3AF02B13B71C5656788AD0395BE8F5E7989951BD06953FFAAB28DA3092EF81FCAE2CAA4CAB1A9DD73DF8AD28674B0BBF462C97B27F97E0CCE16751556DE4D970FC60AC8E33A9C5CED2D3F62A02B5BD7823508FA893E9221FABED3DFC96C96C4F4B755658EAA7A09ED60B964282FC265F07E347FC4A31F93A641923DD2269328C8DEFFDC756324685536539C58BFF9E8CBF0E860FC5F3C4BCD20FA2F3FB41B320FD350D1FDEBC7058AF1A671459D59CF8A7C72ACB87B95C87759C7EEC4C4E2B38CBCCB626D0FDAABFF597A017AD885996E597C0B4F7803333EA342307A2CB326A96051F040796E32FC518487E9A8C173821CDF1D340353AFD3EE23336D00AD489853AC54F25259EF07BF06DE472E3E7DF25447314F9C373AD2FB2F44CBEA933D9E9F88B07D2E3C41EE78398F6AE2DB65990FAB2199C7353A15DCFCA6C282AAD26D07965895A0453A2FC2D134125B315751A69FADD9F49591E80E5DFE427BE51C54A3090E9E81171B73FF9865F1F4C7F8CC6D02B2E84AEC233FEF93F1262F12BDF2968FDA9E91ABA3C6BE20AA1B92C677B3D1596159CB6A62C597A5A9CA34BE5E79B61A9387951127DA8B39CC179B60867AF8633DC5781CEC3F98219371B1B59EFABCB4D5CEC6412541939505DDA68F7F7D8C436E8F2DB60FA7D48B7491B3EE3F1C768F484E8DB2FAE33289574BF0EA55A1D4F27AA51FD7538C62195FD936E1962557DE37BC0D7D363FB745FCA7A2A2C3D9C0B5A01F54CA70AD2F8D5D143D51464C5CA9F8EEBAF1CDAE5EBD954A89ED9FEFFE28C8CA7F5D4697A113127F62E4CEC549C9AFEFCFADB043F00183F1592BE4E284184CCBB7C709D0DAEF13D48FA17717E1E674761BA1FE4A7517E866712E08464C115D818597F121BB5F873240A1EE371F4D01FDEE723889BDF837CF80093DF12B4A633BCA0E353D7E5B7F164DE94D75C1F5B75AD3320EBFC685CF7BDF388FA74D51E2CCE8A3A4F5F74B633BD781F6774B8906864DCAC95E67E15E749029CC54D15A72620FFF013C4E70E19C149E2132F2E12323B8DFAE7497A142607417218A6C751B8D98E778370BB1BED04781E3F1D7D8107CE09EF27DE134A1D4D7E30AC4E105CBF0FF1536443B0C16D869791EB23599231FE7897E342A3B2C539A975092A4BC70B86F6669B3BE39948F93245DF0CD13BCF7CA2F75E669B96CE36B638A3B94D3E5BAB44D11F0AF4C06440B14D82DCAFF049C9865CEC2C0782ECEC087316080B2CEFF21C2CF1CD3B0E29C7931017B0F4B087EB702708D6DBC15A3BDCEE845B9DEC24C28FA05DAAED6B01FD811CF0E8051E010F08077779719D1457C9E026E51F6FC975709315D7297E3420E6FEDCC06989BE307DB9206A7A2CFD4C67A876B7C06EAD599015C0B338BD42259ED315AAB5FA7C8579F9ED5C815A373B0FA74B85103BEFF0A10F2B8193735953A6AC0878DF49A2B84AF3B31890C00F1493831E1FF703DA412FDE0BC2DD20DA0D0015BE37DAE95273E047727D281B1E1B66883E16C22F03F8FC34A6E24F42786FF5BA038AB518DCF70567D9F49F6DFD980CFCB738BF2EC6F9324B071299FFED02BDD6715ED4D5191DEAE73EA34B46CDA816382B05E8814558562625D1C8162AAEEDA02CE9691142F06FFE6E57A7A516944F6488908964959FEF4D0600F8E8C120D9EFC5BBDDF8A007670B84B8A0EDF7403181133E0CF3ABB480E66E534AED36831C8BCB98DF0944DC2B3E8377EB5FA670D4F845811AA5D0FD19A26FCC9FC2093FF44114CED924C93F67FC6D7DCCC02E18D45BB2B365C9D0E1AC8324B6C2C0538A379EDDCE28D573CEBEB34DEA75E781577A5688D67559DA0275BAC0191F18D23A50C22448A6BCF04F1A3B696A02229522B3981F634844B315F17E79761619511E8A3AE16F4FC4FDE2EB781882657A12853B5D287808F11DD31503617E1EF111B4CE45DF70D4A791CA3D39E29700E28621EEE205F9A522A7B345DC6555E3D2DD5A9F5DA156D0967AADA8F36B85A52B2B3D8AFEA3B1C14D6996A8FFCA4A405D3253990EE7491CBF80D348D363B95F93E6E26CC817A56DD5EA1614BA5929514C7BF6EF7264049FCB04EE51D31929278021C1878E4706CE081F3AA800240CB92EAA1404D7F408F245361423E94DF6BA74A4808D97B1B68EA048E0CC2FF07F4FE0A22350DCA7B2E1A8F15588F67BF875FCA8B8C9F05B0309A20275C416C402A2F3A6E06BB58A5F99782D8279294F49D4B1BCAE12AD702D03EA923F646BDA08475567BB3FC7D9962CF7AB2CCDA0A56911CCB5E4D81B8C3643EE5EB96916A54D8B151F5971993017FD3142E1886BB001331454F83BD319DE6650185C650A4F0B1E84014F0BED765324BD7BDDF42C1AF20B8190D947B24A4F7B49D921C31A3E144C856EF3FE0DBF04800D96F0DB103770E28B82473C5FD02723576202CCCAE7C7D8A4BB3ED47A8A54E6BD33388765BBE061309BD396386F2C4EB0C4977E166ADD2D03E7B929510CCB139B07B9BAB3AE4E0FE7C16CE07CB1FA747B16EC022742A671B35FAD344B96DC5E82CF0E2428CDEFAC34A895FB1CAE75887CF51BD2871C2C2156D427E156B7B7D1EE7C6E753EB57AEB6D804C0E83FC226642FB9DB21E49E2037E902C7E0B41B1B8E1F827B015D770E009048A380ABD42A3F858FA9719BE3194A9881EBF22AFE45781D99942AD4CAFCF9431DF4D3D5AE2B4CED6E09C6D112CC469CDE29C07B5584A7D5D1E9BC05991E6BC6C683ECEDF7585CA5112D9B060A6A21F3D37EBE9F2D7338922A195BC14E56331FD9BB5FF50EA07B6057E32A6463B9D60BDD5FDDCEC7C5CE97C6A4AFC83C2BAC16A333BEA2152F27BF073243A962CF7CB60F05880597E12B1B8FC361C4A069B490B25373353F0DEBCC84ED9A0A052E115CE63F504781FFC118047546A15A447D4EF335470DA4512A34EBF0FF0E06546966849D15EF88C4BA272B1C4256BBF556B58867E4BA807DBA555FCED229CFE3299B7886D46494ECC58D0C0EC0B93656A970195BAE46E132860702B49E95341857DA3C280163205663C1F6E767A9BC0D946E20381F6363A9DD55677AD156EB6C22D0A94A5E7593462AFA74FA277AC2CA1573A52E6B4215D285CEEAD2459142EA3297FEB22E95FA7D0281225DA29C27042DF7BCE92A6602B0A8CD3A9367EFF10E7579B07953DA041A5A5F758235A86C95AD4748EB7F6E4122731DD30A663E98952411A9C7EDD39EB69675856B739A45C0B33D3977D6D1A8C64CBA64AD3844CB7A36FFA8CBC141F9C140C087539C2276BC7AFC5E4DB00553F7C2CE25CB84B66C865E0669B6F1B1068EB5D034AEDAD357BEB2D108D773B4882F885F83652570983B6FA9A379D27434A93D913949DEC062C81C4EBE282491312AB631645A6157591F0D7AFE0B4FBF0C6F8E3D425BD0B9CADC1E93CADAF4B0F646509650667E9543DBAE6D173C24BCC635D2AEBC6BD6CE75D28F63C5B80D36D70B0439A158492F82848B32F4CA5F9E46740769382DD0536F9324499883827294C3E64E188E40555E060709DC091D22B1EB12B0481B63F369B6F1ADDCF2BDDCD6EF35DA3BBDAEC7C5AE9AD37E172A3ED0EB3DCAB14FAC6AF5355749BB994B0D21542047D2AF06EBD2D68BDAD150BF329143C47A17688C035399032E63094C43845163D7A1C30C45EA4F544F7A726B7332C6B39AD95E39C15B1FB6125BFADDBC0D7ABEF8A9774682FAE81DCB31477AA261B3D672368B9BBC18EB44B4D99589042D182AC4C455B696ADFE0D939DB7FA6C5458492910C6E5857D019DE6670C2780463249F8870C545126E77BAD0E5BB46F3C30AA0AEBC590642027EDFC005032A94BAD1564FCBDF653A9369F033E243CD0A777AC2668278EF365E1F4B9B09DAC5FF8899B3045164CEC009C678CC4E6292BEC992A3A81E3EEB7950B549EB137D2C755936E21F6722A88EDDDCCE31BF2AA53A354C0ACB5E684F427020836D5A0F8F16E7DC086A707A9B760D489FA56E0AD3530EAC2E5D4BEF57756B1FC25876148CBEE26B88E815494FAE2F4D73F674E006010341143E10552354D55D4542DB048695B70D385BE0843A01B2FD7EA5FD6125DEE940CD22C44C63615F6B9B233675B56C45F4450C46F40DD65AF8F5789FD50EDC2C704A7E4BE3FF6B833D6128383D60DFBFB8E497E3D9AD9856664D3C69962C4BA28B6C813AE7E3AC115D32D397FBE12CCBDEB6C169A01AA22FE14CAB387507B5196FAFED56D035EAEF3A716947028428329D02FA3BEC2195CD4F7BF8C4874888AE12A633B714045C1C3252088B79EC7ED0D324E813F3DBEE56A7FD191748889AED778DF6077AE0E07313417448B799B119848CF73CC69B24E239A58F1F457BBD60539264818A2C1719722605A8B1B308113AD8A076E990993145E27B13E86F8E3ABFD5700E2B381743D502C6E0BC5984B3A811D5C725BBB3DA07890F2808D4B6AC39A2F2B2CA0C981D133138AD2E0DCB4BCBD21D71205BFBCAE959379C273306D36F1422B3D9873C3B61A5515CC508A2D2A84BF2537CF4BD82157D9FBEEE36834490D40046846C68AD8D6C08726CBE598658D5D3763F369A6F97C100DF0F504C8F7AE961901E047201FD85DA2FC43703EAC49BC06353A69B1D14B2CC72E966F1ED8CA9E693089E80EB36C8A5373B1A47D98DBAC9FD4E826DCDCB54F497329B559613DFEBCEA05D980ABD88B36C2394698E0FD252EC8A59A241E972F77A7E4E14578F3570890F402ACB999039F2C647ECECC8CFF1E8CB60FC8D2B91F979941E74212396FC97086CA4989FF452C9477269E921171D4BD588FAAFB8CE637C9990829E25F80B773E3701151E183951EBED72EBDD322EE02A89535A45B202C3751840E512DB51C86EFE5E4097BBD1A6AD4904DD236F9628F0CFE74C71F1CA2E71767A0215EF09D2103D9BBAB5A6BC95E6E4C9C7590DA233322DD7CBACA7357D03BF23EF23BC112B7102A1982F4752DCF4CCE1DC0ECA9CC8CD8095CED66C1E32D2145D967BE2658F9F1DA0ADEF05832E91674E7E0C99FBDCE770B6D2560536D406A813BA506AB2D789F644315C4E09A0247CD0E3AF43EDC62137819E10D2F2B3043C507D0662F0B78CA66BADEE275C332D0A565BA86190F1C6DA76DF255D8DC17826DCE5054A200A74BB9BCADA38DBF4226EBC1E9E3CD8602EDD5D43C6D443D1525C49B9529D1E72B37AE570E5D3B08E73D6CD3EBE9CD6CE8B9DBE3AEB4E155682EC7637BA7C943F965EB78673DFC7E9D5249766DB4929CD99AD0A26D84CA6284226B2A2C9919FAF03E441FD7382C4230A47B847B2DC6ACBC841178AC107AD5D1B16FEFB5C1733691162DB7A0B3F55FD45A83DD65B81C8949911FCF0DB65CD9BBA9F9A81D4A652998834F16280DCEEE2DDD894901E05B2597A5A591045D204752254A3C68540F1A589E42F832F13FE5DFEAAA726B47E93D6235A0739331F34BBD259AE8855963C679DADEF570132508A6A1BCE6A1AAD142D559C66D7AD4B7F28CD194F5BDF79F2F764F265307C2AA033785AB66121478449F85870BD8CD3FD4EBCDD869E4005B98F0E91685D984997156262AF1552031BA80DA1F1A097EE334666B80666305E6B0321725DF61924B23275FADC0C91AC6E3159C5DB46D25D8AB6BAF0BA6009B4FC1F51A0612A4BE5F0CCC8333AABEDAEE0C46B58C9DCE4DC1CFE6DA6A5E70F3D2F9892ADB7876697B27D968BD6B7EF4A9C5D9A4870966547CC40D5D76C952ED70AD4CC88B8F67A36837378571E402225CAA84C6B398EC5A839BCEF2316729AF9B1801C1135C112BA89B620056A084820AC788FB323C010EDEA0A65170819FCE018C14F10E2A3CFF0D183E56190E119BC12CAA3BF054EF68CBA3651423DC3CA04B03744FAF4BD0CCFB8A63A45AC5C0F17F5EB1705BAC75F00CE96125F6D33633A0C657070545F3CF158BA2AB3AEC58741690B4684E6DBBD67F6C92580A4519A0416381FBB51216AA05A97DBDBF671DAE4D6E13CF3709A0348AA38FD9D7E3F275CEEF822E3740FFD311735515C26E97E37C4A7CC0E7B139F63B8019FC90BB85C000009BA5397D4B03FD735140FD95EEF43D9873D3CE6C73D10C575B449A260C02CE9331B460AB5FB71859D07FAE4A6C654F08B44AF4C71D74C94657CC5F3DB1DA6C18774B9ED8F0CA2780D322604570AB49CBB9C997B365DBD3A39C3EFB7F325F358567EDDE0AC444D255AF3B49E4CF1BC87B3E7E18C0EAD40BDB5CCFE229CEEA083EF9C9A1C40975F07E3EF1C94C5755F8A4E880F09AA4C03C1B52264C2B989343798E080043FF72D06545CB0E4D80B04247BEEFDD3B07F1AE547C449A2485FC50FE3F560262057A467C40B53CF08E09E1005C250EACB9EA4B82A5F26501B2DFCAD204DFCAD103BA5DD2F393089068811656559822CE1190C35612DC2795BF1A2737559767DEF4D17C9E1ECFA446BB1738E406771D6260DCE393532930A0DFC732B26DF5193B1D2801CA14B4EC07E1DFC7A7ED64E4D2A690E22684C4174E1FD701D6DB749E273B3B7D68413C687CE6263BF9BEE75C12C3F26C502A5EA79CCC7336A14682150FD29148C7A319090C954E85353E91AC7FBB1813717AEADEEE715A0E5F7E673D3E64DBC56B7CFE932C97B036D5FC0E56ED3E1A3641A3D795AD4F9D812927CAD5D9A7A67BEE525A1EA34C2A0BA9659B2BC73D2AC2650F735750ACEC009745104DDAA66434E9D876EBF9168946D8485E1732C3392D0E2E0860B873264950FF0F8587000FA0805808C775C700D121542719D2228F64458400B90111ED79BD1660B90800AC08A530109BB4C50EA18A8E75101DF7B1C322782AFDE6C0327406A1ED46331435A70B99D0FB480FC563AEF59DE50C752C2AA41CDECD1EF06322D467F8BE08A273BD27940D0454E04905536456920E4667F6A9C2AD9ECCC88908FF3CEC3F9F05B9C5B8A33A838DB759A47D4B6141CCEFD529DC64AAF9BFB9DF7A10C468BBF1D22714531D79776A82C37669C999B8CB9D6719301DEE03C9A70396C08306006903D7EF42B3D126D812583EB7A2B3F105D9E8478D9E022A15D2683AB948FE08A27852BA329622D5CF4262322250EFDF13D794DC9C2E5021B287E56C936A4EB4BAE286F5A6F1B8DD7CB64BC2E750E6B1554B7ED8E963DAB506D909F72CEA1C64F1B751664DFD86D7F5091A9653F77DCCB9B09F270CED9FEB054015911A89713AD5743E91C7F3B83D3C551F613CA4E82081444875C5D3A9306F7690887C91EDE5532422A84507AC7B101CE469F8483CB18AE32943C8500D625EC01E746ABB78A04B501572C2C23BAD98B58110EAE04E7753AC4230153A97801125D08942E170172D5E214AED4BDA4459D0F0D48931AC5C58706712AD13764B9F2A601AFDB7CCFE67E47FDF067E6DBEC10A1143EE66C3EB2DC3A9892D00B3879A14FEAEB871511CFE2AC3BDB599CCED30695F0B9DE715E57FD6D59AEB8EA73768BA73B35EAD49C64610AD08701D29FF1D340B797B0CD76C201D708D9C48D8CE7C8B495CE4BC2A9326EAD4B1AB2C9C0D9E5A78CC006AFB8126DB4005B1C2CDDAC5024C8E18DB0BC8165FCE3752A4423858A409BEC76F0BBE11A2986B03558AB27A20C3EDB74F7331C2C89B6DE2E2B4B3CEA051C6FEB3DADFD41F2E435B670B5C1146E7748F4B6D07F2C47B6AE2DC21BFBE4ADE574E747D06A64B5DF864A4BC865B9F53D2A830538AD3A6BB153055A0651AF9FA0E54A257C9AB184F284043BEE25BDF83B0EECD0C7B2AD13E5A78C945CE5979173952683DF4DD697A65A2A3E1917EAFA0216FEA2D1CF2B39CA53C8FA2C52510E05E4D082541BDD667C0698853A704A0D13245BED188110A9CD1A8952A06B60C9D849B422569D6D60E07C4FA87C54901F9AAD0FC212B5CAA6F66F9922B1797491CA3F0484C8B228595AA225C5811E3AC8C7AAB39D413B33FCEE403EBC8C73D37515BAB3E1D344D0CD197FBB1BBA4D0A7337ED12AD95A9243E0508A1FC4059D9DB6AEBDE04880FE5234705AE324E505EA52839B2A39EA63F2E863185F900A82B486835FD19F05784E2B587504DAF6F9428354A03574045AE7B18209A4A406DA7A876F8D84E773BCC9850A5480BB02B8BA64876A8C8B7FC0B48D9DA92FE3E57E2DA9FD8BC45668B2F5C26DD5DCE2ADCC015F1A0487E7D7DA23AED763FA8740FC44A81D66D6E6D538F9A7EA1D2ADE641F36A95F5D2BCF02905E86E75DC64D1366C1B4D87C8E61FB80ACDF9E6938863E9174C44CD543B7C2F205DEA0A73C87C07FEF6A3096378EC7D5E89375B6060A4293E5614497E235A3EBA13BBB7173043B78CA66A7C87D3B0D0F2F450EA196467F2089F1C338586525BD27010631F5FD0BE6FB43FF2119E5FCA180A54892A48F378ED89D29DE75A4E7CCD12F58ACBD954763664CEE0ECCE24417567DBF558BAF0695BB86E44A13A113FBB89CCCE660EEEFA1C2A904D5BB0D1637FFCD4677F67AF0B45726F17A47999727EE0C6BC0621969B38CFB82B08C1928D827396958C9417B1064B0AF14E70E2D150CC0CD47B0FAA3AE1AB544CF22697404962CC6A870E3932FD878320DAEEB0ABB0DE0E6CEE0345AAD765371F4F8AB186D9668741C7B2078EA5F39FDE19A06273343A2AF5576D2F2C62599DA377382B81B3EE6CD73B9D3944BDFEEDEE9C79B0FA8E06DD877404D96583BBACB89521E65BD2621F001F1CDCE66D36E53677AE608FBF1493A7FEF38FE1F407DB7E40CEA9CC5B6ED1358F363A8A47F514E99B3C33BEEF5B9C243ABAA1C133F3E25ABD74AADE58190FB5C8B9B0E5CD21BBF9F8B68156B0DAD2815E6D2FC8D4A0E0D4E47687EB773ABFA2E96B5956BA4359BC75150BB52E53E38DEFCBA6816B1ED5CBCD05EAB49DDBAD1969CE385B43B45C6341043553B89571EAC3C8DB6C54324E8FA9B668B7C3156916949CB69265A678F27DF8FCF7E8D77F2628374171CA3FF2195EF39C92BEE2A153F55C68E9514B907D1FEAF8BEE6782D548BD6D8B5C16C15ACE995F1CC5C423FEA45D2FD27C8B70D66B61F9BBA3EC332748DE977C83E4397939BD779A9AADA61AEDA48F1C7C01E0796EBB074BC33FEB6D6AAB50726942F5EAA04CE7969AD65392350B708EA17A07BD1A22D0F25D1630E1C6B0381CB5EBB5D7851EEF6BACF9F27E35FFFD09E7F0E9F7F8EE47AF46B327AE656F8E14489DE57FCE7D038D87CE473BDAB0BD4C739BEF7B82A548DB86243735DC2164F9E72451D45CE7E809A843E1620A556E15ADB27E9197D6A22AD8B65565BC6770B2335A748376AEB9BDBE050EDF17AB9EEDC8A73A6DF5BAEA87815E7DCC0D935383B3E545D042D17B477CA336A5E22BA2F38B93520E564E575CAEEF921C73EC64F85C1399D00211E9FF1F80FED994487CFDF0724FA90D31CA1DBAC04A9487CA27733D2745682CC0C57EF4DC67734138CC5F0BD4190CE0EC378879B614054A5A920A14EAE8FCA281A3220CF737AC72B9BF392ECF1BCDE18512D940EEF6CABCF7B2CA1DE2FB4A55A7DF2A23A2B44BB2F087456A3D54D67BAEB831BF6AE508F72D73B07A3EF7361398683D58B5F70BCB421E9E29909548B4F0131B5C023945A415563A62ED7FF912FCA5BEB7215A4453B762FF0A56CD32B4EFC9E46F12E7122526AD494A9414E3B705A537489BC5D9CA73750624156CC9BA5F6046A2B96DB5A3B695E8377C60F2FB9C5CE452C67D46989CE6BF8698AAB38E75A7A96229A661769719D24324EC79103D9F68CF487208193D2149C939105C9479E24F3F7E8F99FC9F374A2DB54265F28D65AFA337EE8D73DED8CBFADC4513F7C3A4F2BEA14B70CC699FA6704D4FE590C7FCB2C578A4E0D999C4B9281B15826FF54672EE571A294B9E291BF7B778E3A4D57A1DA11B435AB6D41CC2C8256706E2EF6B41B332CD7EA38BB6E6CD3C75923BA671EA55689B3F3343D0A9283AE19933C0E139425B719B5F88FE0FC67EC02A7BDA63AF9F81F9EF6C423A3FEE1516E3CEEC0F1AB67B67D637736399A8BF3CE6546B908B40CC6E3FBCC10952F8DE08CD2832094F931F5B1D27F972A652FD0F1B032648A34AB38C7B338BDBD9E06E7407B84D79EF9386B4725DC2FC639AF81D0A9E3B444CD1811D3A89E2FD0DE5EC5EB96806D6D2A7B1F7AD1763BD9EFF4D69A488890E52238A12C316E7662B94E6D34D5C7A9B07C9EFEFA9F671AAEFF994840B58ED720F4AE4BC6FD8A704DD2DB7799918997C6EB664E973409D8788DC529E5CA47E9EE72595BA429739D70B93CCDE6D153A7C7D24C85598D7A075EF87305332CBD5E44D9EFBDAD368CFCE192CA10C29C12A5CA92E60AD0A014E88EA958AA44AB8F32710F75CA96846EB489A8C3D52E5C03E7F4DBC0C85189AACBA52227621624FE539C134AD67ADDBEB0A129D1F2E2C13CC98BC7C23C3ECA1FD594E52DCDE53E86A5665E8FE2C3EFE86C5180A21A911DA5D23D9021504E15ED75E975D75BCC694DE0B4070AEB18D137CF8C3A75FAAB04A92CB9482C36B862D7B7F089DA3EFEDC35EDA56E0DE7FC8AD32A72ADA24E21EA86FC7AAE00EDD58F958AFCDDBE209D9EA74882E06FC3AD56FB7D0338A1CEC1558272F317325893D02A4B96A1DCEEA914F5BFFFE16189AC5EBE0DA6DFC872222ED79853E79D20BC33CF588ADE2BF519622EECB7C168D45367069693877CF2C8F744B9C23ED196C64E6D12D1D37233E1612803BADD8A34DDE88573B695513F0999F77EFA232C2FAB5669FC2ED0A8AAB33BCB726E5AEBE16C1BA2B502D4E2DCF1B6840ACBDE9EB950AEF0B772545C2FDAE970FE71AB2D1B313BC5658C7CF5F9C7C0444D9526ECD907293B79FF994C1F72B0D42CB782C711BDB3A2BCCD9D4025BBB122B60D23B22463FB685E60AA1455E7C45647039D49DB68E93A1AE71024158234D323AEF1E567497548BA52A25446FD74AFE7830529AB69C2527668B97D5A8668BF4EF4C62F604C1C5DAA539CB796E2EBB2ED1E5D42E4E134FED673B9CE1C4E264447DC989EF0B4AEA8C7C1AA2670F6CF23103238A796281CAC2F4AEECA9E22BFA522BF0E4CB96223E2F83EF75C6BB538713FB54E786C43A97C030A2B5C7B21221E556327C3E76D961D06E1BA59166D6BECDCEA98B9DF13F6DFED16866AC5696BCDDA09097EB0545D962C7DA29520EA395ED710D639DBDFB29CC5D936382BEAACF8DB1ACE5D82EC7927C1A51CAD8E75FF33A0061B2DA445C039F9D27FFE392CB359B8D95FCF8E250B1576FE06D4E557D9587F639736BDB61FCD75181CCEB214A9569F773663BA15E72C8F60C960FC343020EF73071580F39390332E9F9B6D19D30554C44BA4428AB37F9956711A7E8AD31DC8E76D2D2A389940967D15625F899E3B9C76C455E7E8EA5CCBB59A79EADCA8E1AC27411ECE6A3FC166439584A8245A4977B3331EF012EF07E929F79F709675A79D9F86889D9E3AC74E96EC0AFD3D9AFE4498646B9E07659E459CCF3B0CFA6E4A48973CAF1200AE9592A6BD7ECD7EACCE1315E7497126B3611731B21BAE9FE0F194565C24EC0A19A285223450E511FF6B59E836EAD4F0C99C762FC8F09E50A7DF67F70EE61FFB7D8387A1EBFE00496171F69D3ACF33DEC2EBBCAA519722799991DF30FAFFC659D9F2A0FEB61A3E77ABB9AE9D2A42CAC085919B34DEE920B9659FEF8C835EA6E89C4E8C9B852801F22B425A569C71CA12A10B854DBCD58A3668892C38233DE99FF48AF34856BB6212558AD7B8309865F1CB0E97EC74E2EDD238AFBBD9E67AB84E049EC75CA5B9E78133D2F7CFAC1BA72165EBADAD743FEB6C1F87E85177722BC47EA01B1CFC54C8DBD359EE1AF3BB3F5A62D67182251C985845A39A165DE585578CCEC3E956316BD3D2F5FAC4C3B936D34C28B781CEC1598E141D441933DB94A5E76E5736AF37E3BD6EFF9CA990AD4CB4B2FC05AE6049245709877DCEC2E204A2ECA67B1D09601C03C36382DC78BF0B99EAEA8715683ABA36E32660ACAB2200A991AFC73992151D24C39723B3B30D2A5F7E0FEECD918C26C4D228537C0B4DDDC9A1DC156E66FA2C9D84AD4E7214E597A92E8F54B619F91BC47C96372E6AAA199CDCE453C75966B99E40EBEDDCA5EE46A762749EEE7AA1346792DB6ED91BDA99E927787BCD80333D4B868F1CACCDCFE298070A87D1568B9B52900AFD181A5DDA46014229CF15BAD285AA68701A16A73D10EDC3D31E04F97E27DB6BC79B1CF649B65AC0CCD7409DD7493934A4D2BCE4A010B218083AF8D4E87EE4540327C70EBAFDE3A0C0F7E09C432AFC1F5D1B170D8A6C0E3FB9DAA66C2640CAB2D145362DAD36BBB2FFBE876C481A5EB62B349859A62ED7352B199087B322D073BD9BE93C4F5B9766E1E15CAF98229C65594B6EBDCCB6D6B9ED79AEB5B4C8DE7105FF606E4A791A0CE4B044392CA49D1C06C54D4A4FAB25A6E29C8EA75FFA9426209D8764791414C71627F404DBEB643B6D588AEF04F2A953BADCA18E9BE8F490785D60EE1F05D95E072FE3EB773BF82A90E54988E8AB1308203ABC2051681ACAA61C1F6B0D07A6C178C364AF6BF62DC9EA5828671B45FB3DA8333B4F860FC5A8B61C3DB3AF4817BF5C334F9D6745A30AF5322BBC3CA89204CD2B3D973A8B28D61B7BED0AD4F52ACEAD7290BADC9B6D0E18B7B74472B76F3DE17C89E0EC23BF0D11C680F32018DC655C20FB1F8BF33F529088340767D0650F20FB071D21D74EB6DAD16A2BFAB8127D6A241BCD74B3054E102898C1210F3C9C6C9D5FA7600C78E0ADECB35D79E41F3BFD836E71C489217C5D86207A1E8D2E13E26406A4AD0683732217C4B91FC859374DDDD4665321C1799614B7FD4514ABCB9316E78D69FD5899E6658ABB30A79DDF4958AA439AD1A267ED0AE38D8E3F736B87E27B568B7654D30C6CF2E82239DA24E61ED00B9EE5C5F992FB3EE7BB000318AE53A94C5C89399D7E6106343C13511E76E3B59568BDC5398F9D8ECC3A73DA365E6F46C844103ED79B1C09DBEF2027224E1908D25284EA3C0B8110BCD38D16DE27FCD0883EAFC4ABCD68AD156FB6B99970A79BE0CDD79BC5613038EE112AF3A954B2D94CCBD3C9237D2FAE111A4C0B7E954D3E6E0FE5C6DE2E0F97BA48D9E4F3E0B91E5E75C357E1AB7350C15936F97CAB4D04DA767CB52B6495D7EEAC5A5B9B6756BB3242ED407665562890695BC3528FB5E1216047A5D9833084258FBB9323D22E78DA134F898640F73AC3879CA27455E677C92A91019D04E946139F3E54C841E78D36CF23F9D4806FE86DB0E08B6160B9DEE2E3561B31750079E954A6F48670DDC7FF68B3C5D76C3493B5265FB9CBB32D3823B8DAD6859170BD9DEE74F032FC8FE0098626A54ACD7A99C8549B50DC2DB3DE920C88DB9E74C7206F2CC033C1FA666EB6DC3256CE4657F7EFB981778F28C97938AFD5AA0D845B6F9CACD6826FAFB66B36CBB5BBE6288AD901869EB0F441CA895316E1B1912315A92620656F368FC4CB398F9A265C0D6E46BB9DC9B7015359FD8F2B9A43C4B0FE21595246B26B0C9F5A761CB2F1BDDAC06F753F2EF3F8D9835EB8D6066FC0004ED4214862477236BF69065DA71CB4DCA5B38588C38F48653B2C7C0F7A0172A2752447CB01D2A2535420ED1089EE5A33C11F773B8CA69792E54A66842C772244F12D0CD6388CC9C92024B47B5D564A87BD914C4B0FAB12AC8CBAD7B7FF95FEB68C9DD6E5CEACA8942CAB1B22CA6FCF52FB735BAC652E7CAE0E27BEC5029227EFC008B22B200301D993D018961479CE5FAC87371A3B4BD2331EE5A32079DCD655D6BFC95199C0AD217CA6873D3F703EFF1C3DFF180ECE65A7E6496487EA2229F3D3FC9487FEC47BDC3F9B6C77E3F5B648B395A000DDEF227D254EB72806677B95CA5E41DE992386775DA5714E7A37E86D7460E176901DA004822788B3BD5EFF88F911F01452F950A31247D5D9E2EF9C9F46E61086AD0EDE13D1BD8F327AAF337E3259EB6896E87DC5E556717A444BAFEBE53E0B33A039385B2DB2AC1A028315A8B2B4203BC1B6C792207B02328C0DC5C8F28B955F69720EB21ECAADED3D044E209C1EA1E2E4B133E52AA669E98D787722E9D0722AF32E23D4D3A87F1C416ADC50BDDBCD2056680EFAD868417CB2F53A007546CD07D3B61DDFF58797A9EEDECD0EC374B39DAEB7F88B3B9D6CBB4BDB417A1CF4F77BFD43108D0667311E8BB358F7A08DEC58BD24BAC0C946F1E0269321CD26A48977C64F07C0B9D3993C56B29E0AD1CAF8DD0CD199F5EA052DBD7E75C7676D58BE586A7D6AF946969F2C519526D7660D4B01D90D77BBE11ECF8101CB5858AA224B8A0AD2F05333DE35D7437E799A5D3E7C1A70FCE7329673A172B667FF676A0A154D85B87B77FAEBE77088B413EADC0FFAF069FBDDFE5EA7BF471E60936CB45320D90DD2FD5E7E1C69D581E4D3AC7CE9EA18C0704B76CC93D77603083AC56F6DB4B2CD363C2ADEB33842DACCCC76003902E4197B0E668CEF2A350B9F8FA60C65DFFFA9E0CCFE2EE3E5E02261B6751C0DB7DA80AD0B5EB31BE5AB1B196C27A116446FFBBE4C07BECBBDF903A2B7C0F9B1559A236A718A34DB55963C82072CE343B24C8E4301199572548A1796E2954128A2B4D2E4815DBCBD10872EBFF4B904FDB5300B265AA5E8BA187479DC1B1C476CD39CC7F8C44DA3955DD6B84F78112445BD1EB1E9CA86CE598C6847C788D2F64667F2984F0DD9EBC10B92EC3002D1FCA0677ED75A711CC106A7122CF1BFBBCAF856173143EF0D714E1E1C4E0874505C258914BBC3CB183F1ADFE606E78307AF3E133B67DABD1662FD506A8D5DBD72A6DEDFEE32634BCD8F4D672DD8275A1D674D9707C2F2A8C25241965A74FCE4E4D8BE3D3F96E754DE28CE6CFC6DF80B31F2EFF1F3D762AA69ADEA723AA1B3FDD29FF2FCF789F8B7E11829C679323897DDB820774AA8E6F15C37E7B2CFAE0D1DDE4FEEDB50D6A2F141F7390F8D9F9E83742A5DF844BF0AEA5AF19E7C5E0D6F7512C370CDDFBACEF8E530CD8442D7C9B9C82A37BCEAEF77982E9DF4906AE1F9E156A78EF3611ECEDA34A5BFC9BE52BA1061BD5CB99E23565FD633383F1A9CDC54BCD65269F6B6B9CE0C96D4E54160590A48B9FD81E7572B20B9A94FE1093FF3686DF4A5F825B3D1CF77E9F43E337DDA67E9F041B5D3A95FB74863E1199F173E685912118A97669D840B2697C2525259665293C9F4CB803891E2020C5E70A1B203F58CE2D34515B181F9A2C027E335609F71CAE446BE0D5CD0E67B3A5D2ACEC9D7011C7871D01D1CF54617F114A1FA281C3F0EA4E8549B97FB2C1065B9A1D3C359189C59712976E5643A0FAAE26C2D50278FE55877389D2E1DCBD067695CAB2FC79B3ABC428ED5378F729EFBE0A1CF26ED3F93E79B647293D856ADD8C475E1F5507889A9CF7203CE6FC3D195F0BB914D0D9625CA89B15DBBE619C7FF912E04D5998BC852219A9A173B03575E64CA18EF36E6DB32E84E1E0A638F85CD690776CA57165C6FE09013C199E00563E65FCAAF30154BA54A29EC6ECE7935CC6D357CFAD2BCB438CBE5141F6AEEA7BE7370B615E75AABF4B490E61E17808465AF6479161B515E2506A4AF453DCB59C9CD357CFADFD8737FBE4927D789CC044D4CE07471D4B7FF70D68BEB6570A46EEE595DA2ED1868DF9C2FFE07EA2C8CC26EB2A1BA62657F2102BD30CF30C4C2AE458E0079AF0807B427672609D21108E2FC36A48305D1A37078164F9F0632ADA25AB4D29C0BD24ED2D6F65A570A504F977D6B2AD0AAE3357D8681BD36A9D0BCC0499CC1669B9E76972C559A724B21C73256518A2E6714592537D7E86FFF337DBE4E9E7931910C487DEC73D95290F920FA5EB947B294A41C2E3144CDEEBE4471E2234630E6EBF11A992099DC177CE595EE2812842A50F5CF16A7E6C06479079C82F07160BCEB93F3B1267C8E952BEF3BD967A03D8E80135F0215A5C569419644E505F5B34CFA65427BE3F9D8CB6C66CAC4E79A553A47F67AC924B4C4D9129CAD12A7E76905A748F324B4B98F93A6F8D81B1F274559FC8EA5C1898F1E3891E59AE94B11A863F9EB97FA58B244D284E4E8DB702A36D69111B3852FD1B6EA54E42EABA483890E01597F6BB6FF21713546998E54A092008FEFF0E202F89D34A75F8653BD51AF1F35BF08CE2FC2EFB65F1C46C3E398711A25CA9D0B9CC5EF71FA958931F1A58E25A711D2FE85B3126A9D6889B356772A4EE7690D4E2B4D444DE03CF3A4E9E12C59DEFE114B3D488838BF145304BCA97ADA8959B8565DCA60F42F05F97538450C93AA602C6E73C4A202C54988E4682C375F35BF359D3ED32167A6F444AD22FBFD101A99469DF490C5B0258BE252B88E6FCC90AD09998F44357D34BAE4B7C7F8D8422FC6E26C65F7603E3849F0EB134E7C0D1CB6129EE3773FA8E0A48345056929DED860E944E956B0B9EA99F6CFCCA3055C73BF4A3793AE50AD8D603D6D57CBCD9D3209329E76569AA5A7CD5DD6F3A73839D82E9D2075B3FF9978C1722A9B0347CFDF87D487CE37C337AAC3048FF308754271140C39A153F07DF4B790313D998C5438F5D5A33264E22338E148C3C0AE8871394C2A9CB1D1A82641DAD2538D0E0CCEAFE5C5E4EB70701249813B84571F9DA706672DF7F1FE58A17BEBA9D3263E7D2B4A3B8D90183BB58FBC9011050BB5F03DF065669A7C0667B58110589CDCE726B5A64982C87291A7CD356AFEA13A878F7DFDF46D3562B321DD88F2CF988529A3545F5822EDA4C3A4A4CE63AE661F9365BEDFE587FE6DE48637B977850E56474C24C1918A056242CAC38A93DD89DE8050B9C049995E41A39919C40551813AB54D5A97D31A757ED5021474797BEDD15932BECCA4615B98B9D959AB46CD32646A22E339D8929C58E6AE4FBCC7B3C428F53CF5E3EB92F680AA386D6F4F2BCEDD999CB64C82AA38673DED6F713ED96610FEFB8FDDBC30950B78DDBF0D4B7ED0B732FB831879250E160A3B0C64E53940456FA2ACEE2FFB39421588E8A891123E767CEB06DB21D33EAA8B42BE0AFDC3EEE038189E951AA57FA6E5101C052A4BD6534D8234B9FD3A506325FAC8508DAF0EBE2213DE6A7B200744F5AB20F519D9585FFEA85FC1E9E2A58A12084F9CC5C68EC5F4E2C44255D55A870C332B2A2DD56805A769EC3170EEDB72F3A486533DAD75B61ECEDFAA9387F03D88B3B56DF7E7EF03E36C1527DCEC7761799B99698FEBD4B03C15966CDE12271213DD3A38BE4618C3CB92916C821F2AFB730648A2958E1D13A8AB9467D19C08D17DBE03A2EFF022E277455637C7B72AD3DCF6F66C7B0F4EA2C449114F355DE2D1A7C3E18309870EE1F0D662F36C584965ABD25496C419A7C07614A7B48876288FDCAA253F72744F1C549A87D3A85396C6D62D4E6DD2EEDBAE9EF6D9BDC039A34EE3698B3F099C8C9DB9DB7CC2ED638AD3EED59D7E014B8E048C40E83A91E425D289A1FE5E27DFEB90E8418040483607DDFE0E874B38FA7511D1B85A194B888D3960701692F435594A9B30E2F933F0D57B1D36771047253302547E2774B8E45EB6A93C680924CBD726701A9C70E3D3A7E1E4963754AE68AE96B2AA4775FC7CD3468175B346914AF1304AD40EC41C5135C7B594B2C1D9F2D4B900E751687146D94B38F30ACE3F899DBFBC24567B0813D9B4FB03B98F0E5726868A65C971AF5D1A87F90E64725A2CC7333B34E6BA6792E9085475CB647F18689396CD5EC589DFDAEF663BED3E89F6F41B30B2D35F7662A8DCD4603B09039337DDF42752DB4CBF8E0CCBEB1960DA43D7C2FFAAD6B7CBBC9069DC2C399514436B0EA7330335F36CC6D9DA856BC1D93569EDBE5D08ABE39C8D9DFF5DA1C2D8E9FF2738B9077B327CFE8EAA0E5A8987AA364962F1A1A73BED0C38F7BA00C933BB643E568E011293B96760E378D1113587EBFE1127FFFA1CA38D8B3319813F4FECAF843C7073BF9BEEB6F3BD3688EA3C9FFA67332EC44DBBD948866CB581C0811506DA3E4B94EB3ED4895CCC951C33E560055ECDE6B03CB220F769BCDF92C549A28755DF6B65AAD70E67BB123BD7AB380F3C7556AB14D70F2AEBCEBB3F0D9F65A1F20F5B04BF640B8AEAF2F92752479416D065C4D15948ED041F7A27DD6E91A58C1C9873C2EDA9C4FD33B9BBFC514FCE76EAF2653CD75F609331A71AC8D2E0942D0CF62C289EE6C65FE18257711C0CCE65A4EF2AA693BFE528C2E8DE34F7351BE20135A7D1989DFD6C2C6D5EA3CB5A97B5C6EF42CC5496657F20772153591E925F2C203974B16FB8FA32B55CADE355C0879187D38F9D6B73709AE5B03938D3DFE07C9128EFB37A974F6EB3C94D3AB94D89F3FB60FA5D26DFAF1567C8A9CCA320D96A72A0129FFB2171728F0A2DCC795379B9C5A61C9FA80700E9C9E1F25303DB1874099C1789D994222789F1D436619F6C37E1788B9300DF21E2BC4974B24470D2588C7E1DF02668FBDDE109BBB54CBBB439E7BAACAE7DE31E2BFCEC1FA5CCC8FDF447E3E58111A50CD0A819BA46B23694CEDA9273B365ADB2489DC039D3DE9BDFE1D37B64CE34DC17BADCDB8CA50242DD59C85169CEF07127E5F016D929429D4EBE07F4870790263C2D279EC9F25451F11C4CBDBF662E38156AA1A84E2383F33456906479AE26804FE16FA966EE95C09BEF8A4005E7E04A06EA55A077BA6B85454B72C4BB360C785BBB7EA94BADE82DA78A795AACFD48AB4CC3D2BA59DE5A7DAFC73B71EDF63CA8C2D5A29D6BB338DBFF1DCEEB79B1F36E469DBF0DA2B73C656BA8EAFCA13863183FD34BBB35859EB34BA2E26F797DDAE36C188FE88BFB7256ADC149CCA11E77AA87C2178293AB9B166771694E1747589560DCC11BD239EF771CCE21FF0EB23FE2CE6874FCC42D6C1C26DDEB4AEE932FEAE9186067F3E8FA2C15E7B1C579E8E1DC0DC47ACE6A62150557AC8AF3F3EFD4E99CEDE56FD4F9BF208A383A9415D0E98FC1E8211B1024F531901D0D1068FF38E89FF6F8A1EF736F42BCDE4A773B7D1E4F1CF2E477D93B26A68BDBB1391953CF0FB78728CAF6236E59E1E23340EE76B8BF65A3C90D0EFCAEC896152444C0792DFFF78A3A05E75311F228C540AA11B7309999AD42E7A695939D562A4235CB18AEDE35F0B439A0C1CFB85361D98B760263BBF662A7E7D3ADDB4ECFCCD92E28546662E78BCEB6EF67B6FF8DB3F51A0BBC4927D4890F6EC01B0346942668A9714320A166B2BD24F8D068FEF5AAFDF6550C277CD825D71344D9883B20745BE0955D3CE119B6A6993710A117A7418F77845C5EF9EB55B82AC7A9A2963D9636131BF4A16C5D9213398D3A3315A8AA536FA053EF9B6BE1289ED3D6F862A7B1BDED9507D8F5F094E5916159BAD99D20DC16DBB1B6FD1BEB6D05E5D87459A8AC094E9DA7DDADE02C97C6E6A9B3D218AAF513FE84EB0389A212455A4B9CD7105C44B32CF1113BA22CFF0F7BE1E766F3F5AB1420912E5D4705DD32B70B72A7CAA10CE721821EF506AC2F01092F8BF032FDA234DF355EFD9F7FC5EB6DC97BA148BE86DF096529FF6BC64EDDE02D87B8311BE271AD456FA30506B2D0A8B94F6A9B00321DEE4A43703AAEA29DB51996EA66851F6760431A5075E7987782905E2F019E316F5A5A46DD65A4F68F706665ADE24F932C1A4578C1D93EC278A7F2F183E0BC44500C0D51FD88CF91E670676E5F3791C1616C72F21D417478970CEED2C12DBE04C1F03EA3DDA5F9490F17A327B8F11C7FA4DDF3A4DCE69B46B48EAC27E86B3FC19C50CCF7E4CE40FD1A5DF0D6A1469DB7A9C129E3BBE15687F7D5BD32896B6E7599BA4A434CEEAC4C5A642696D9C752C1C79A0185128F6DBCDC09E620DC34C6395963DD72EB9F6C4E59B27BC4DA8E6B47F72FE88434E7DC679CED4592599679BD673B87E84281DECDE014A2232EF7F70712BA0AF7C9427F52B4084E8A492CCA79839430D9EA0E2EC378BB3DB84D0637F10835EB633EC29BDCA6E3AFC5F8DBA0B84AD39D4EF4A9C9F9F7CD6EBEC771CEE254A735A509ACF7463AA9E2BCD4A3ACF5346B6EE0054EA43F19BE0157A6F6C89D2E8F5D672EF4BA3902D5435BD56E4D9A8EA5F2EB88598A0E24EF66E72896FB8BDA9F5BDCDFD9D9F076FDE9D630F1B406E77E10D9D1BD0A4EDFD95EFBB1730ECB3F92E9432EEA149C4F7DC7526EFB88F435A42FBD90082AFBE615A7DC960A8F49FF30CA768264B51D7F6E259B66D360F87125FAB8127F6AA6EBDDFE7E581CC78393A438454E24839C125965C376429CB450F3A9C29AB9FB83EEFCBDE33990F0B77D48936589243E86650DA4DC828EC9272F2A680D4E734B74A34BCB9213937297C21E4FBCEC9442F44459D98CBB6640B6A53059EA6EE91E7A0BD56C2A3279504F772E00E7B1E76C17E1AC08B492DF2E74BC7755995A75F28681B7A9385B7EC406A77025CE33B93B038946F6761A3109A95D64E5C5596AEC5CC6F52EF3E155AE137B7AB6F440661274ABBDDC1829D2745A52E5C8C3C963CCC17228072614724B7BDE03D6B2F41519EFF74C14748F24AA66946A1C7295A5EAD249D308D19A4318D47489ECD58C073597204125DAD9285976559AA6611B189C6E42DA1B2B598C339BC5F9F2549F8F73FC75886827FC04E7598F766EB812AA9E057FE6F25829427444EFDA02BB9471BDEB4C87106420C19E45A37343B20BC56CB5473DC3335923A3CB73396E5EEB1F5127EFBD74CB5468F458888F4DF47EB072877BC5D98B0F7A25CB9A19A882F0D0C9B727F7ACB42C77EA2C155B697EA4340E56591267F30370EE04C10E77F779E797389C41E8F62F389CE773D5594B6EE77BDD971CAF256A710E26DF4705DE56F8B15D70C67B5F33AD55759E474A949BE02F8C868CE7E4B4BBD427B2E4E966BDCC70823DC3D61EFE2EE2D3C375E55C1AC392FF17C129D9502138872C540AC199D8FB9BC7A9CD650C480F27A7386A389568A94BD92162CA4A65C99B8606B23BBFEB36DD7E965B63797B35CDC6130B52591227DC69B02B4455A67A5AB1B03479D0BEEC117B01A7D785AFE0BC53AB0934F3895AC05945A08AF31BEF83CDC029FC1427B80ADA50D523614F04EABCA2255AD85BA6D87B6CE8BDC9CC89268E250F44B9505DC6BC891D352A8D5CFA00ABD4ABB4D0F079CB261FBE6D7273737BBBFA63D734F75906467696A875B62E8E9A175774B9DDE96DBA0029BE5477807DE2FD77CC609E36EFEC2CAD9991FE6071867B3CCA2030B732EF1ADBF1701E54B716D5705E57FA7C33E173569DFA47475AE9668EA824B78C9D827384A2C5FA5B2353C5999F1967283DA3489B06832BD30F923455EC586FFE189BBD4AE7BAF9C41E28A5BF72C12D2E726F0F5E284E0D9C042C8CF153E2BCE38019D4693220D97A8CFA0438AB6ED66A6ED7C3B9EF13752FB6E98F6EB3DC3238C972D5B0F4C567ECA3F758B525643AC0498DBA9BD3136750C3E93A7CB338FDF9923F74B6BE589DB9DE90E21C0BCEE9CF313F4DF2A365272688E6E7516E70C6E6F0197BDA8CDEE4D1E1EC2BCB53DD5D64CE0CB23853BD6719BBB8A251835375A98EF74AFCB090E651710F123B6DB7BDB8E917B7FD84CE761E4EA3D15A3654E29497754B691A37DBE9DA7CB53517E7625B0A0F425849544C59F6F6A44A310DDB683ECE6AE9391FA70FF576D6FD5A67EBAB53B2210874FA630C538172FD44A032A0CA5D72FA55A2347B7B4751A46BBE47AA4E135675785AA072EB1911A67DB8595E88B395BBF008CE44FE2F9A28A5836BDE897278DB4F4FF0CFCFF2B3AC7FC59BCD163779CDD92ACBC86359379701399C469A6DDFCDFE066475AF182E9610174398D128A13A0BF7CCFEEAFF0AE7DCCD46B3325DD4A32F713A8182E8CFF188C74A198DF635E3BD88F5B1B874CDF7589A00E257EDEDAF792BAA0333AEA0E1536E4996294810627F474D62A4BC6DAC6FCBFBDE9DF2B662BA0E5348C88428E10FE37D2832CECFF35C8E8BE10D5AF73DCDF981730E4E03DBB0DC319ED6CB805A4E9A8B40CA2614B371C1D9527414C1A851256AA1F2D00373EE8119FAF2D7AEFDAED07C75FEBE49940D8C28E5A64735672B3895E5F8CB903BA76EE5B44A8DA0E7D25BB8301F7AA10B2FE26C4D1EA4DB05E10C658EA4B848B9D9EFCA501CBA7BDFC81F0BD9DF8F47F852BD07A563C965F053BBFAC65B1A4A957918423DCD772BC17A373BCDF28B2CE38D6713AF3EA9E64195D225F07429D9EC1C6972FBDE0BD2B43B502AA3D16A4B901D891E46E17ED8DB0F0DD13D3DEA690ECE743ECE6C7E72FB8269C8741B057D75822574C97B7C0ED3A338DCECB6DF36DA1F9BBF2623940D7D657909E36D79D53DB2CCBF7637EFCC74F76E6917F2789591E84D4E2348C3D2E8F29A2C73D1655FD2E6F4B0971D85BCE39FB23C650F0F0CF0B923CF5C7983BF523BDA8FE06F63C8F422A32ECD7A88CB806A384BCCEA667D9C906657711A69CE0B99A5287D8A658376293E89A363C179A0E7747938F717AAB3F4B457F554E8BF203ADBAF97BA13A21C3D0E62FC6536BA5DFC93DE345AAF971B7FBDFA35194EBE167D6DFBB9CC533A00B60F87C0968B59776A882606E775A62C89FC3AB39E9651931D3BBD9FE8396F5A2837EB416914C91D465990203A4648123F4B91F076057FABEE6A97376D3FCB68A7A9E0ACE4B436159AA1B8D75596919F04596992906CE56B7D9C495F2DCBD6670FE43A57C0D488333E8E8D402DD1D20E3C9CF3163B4B75DEFCF7383D814A36944197D3EF6386A5FD3058C5A7D668BD69ACFCF50A385BFF7E357AEA4FBF7365DB385B4D3B212669E84892A2B7C2E993E84D85E85023E595F0169003EED1712133D5B6006F42781AF25E20FB5D0E369C462C2ED98F0D511D06AB6D7CA62B6F7923CFEE6A07866BB8DCC1DD00713485308459B86B6ED960A0BA47F364D7854C9166C749D3B8D9CF86651D674597B258224B99653B6FA3B3149F2614287C9AE23C7087225A9C872FE2F46B158FE542A2770BFF38947343F3B334D90BF119F55078BD6BF4DE2C27AF5FE57FBD1ABC7E35380E7980E653BFA09BA58C4094D14E1ED987E3AA64AE77611D1896A6ACD424687065CE295684855CF4E59C23B8590811C1323D08123DD8962CA54B70C0F180DE3AE3E50AECF572840FF4FD4A749874777AADF7AD6033480EF1574A350F2A51D1823908691D80B42CDB224DC3B22D3BA667711A37ABBA5C3372B44D59634B09709E8A4081B314A8211AF9381738DBFFBD3ABD56C3E09E3E7670DD4F0FE3D1453A3C0C87DBDDF16AB3FF6679F476B94F9CCBFD0F2B089FD3EFC5F02E73A52723A808D454FACAD282347D3B0DB1F238909243452920130999497A1C66C7BC474FBCDB01D4EC98F75E525DC6122F5BEFC9B2F1BA91AFB6A277727BC0B5369EE9AC228B09B2F32CDC099110B18956C1D675BA2C41AA199CDC13ED49B3F4B42D5B84986AC48D5A5A512A48D7F9812D256709898ABF159C3E51779C57A9CED49D50F232CE3F270A3D3D16C38722BFC8A78F83E7C7E2F92A7DBE4CA607C17407445BC3778DE2F52B10EDFFFB5F3C02E3C770749F1592B058A209882A5446532D5DF46EF43A7C7B2A850D1B0BD2ECD56079611C354226FBAE224ADE47EC5058D2D8BD43CC43F1D0121F8BF4277DDFE8BC5E864C11CB97FFFD0ACF743E435B0172223CC6FBD06806A2A1AFC55DE363EBBA1496C4E927B48AD32B259B359C7A1A9BB75212C8C2979AC529FE56041A962EF7209CC599CDC7F9BF17E8441A05831BC4BCE2F9367B7EEC3FDFA4D3D3687AD89BEE76C79F57FA6F5E0DDE2EC3DFA67FBD9A7C1F3CFF3DD4B14DCD72F33395A9249FE7BA02A3F0749E2F62A7D74E6BF675995A3A035A50D29D8A838528796FDCFD2EB259C5990ACB70B3B3F2AE01968DB72B2112EC370D4EA5FCDF7F91E5EB06A0B63F34E383A8F58150C3AD9059EE795AF5ABF37CACC1E9795AE26C9AC0F9A98E533DADECA4B63E5675A92CF7B4E7D303CE1444E39324AA09F44071F6E6A8F3F2F738FF88E57D7FF03848CF53CEBF3F15B4AF0312BDC9C6C7D170B7976E76C24FAD0E12A2D7C889967B7BE1E8CBE0F99FD1E42B929D9444D9C121D1EC94C681E913FA4C99850F70A1FE9307CD1FF57424253F0E003893916B89945D116527DAE259F099DC791B02E5EAD50EF0749BF0B16F5680B329EA6CBC5E06CBC65FCBB886B5DE35838D6EF35DABBBD605D170ABD7DBEAA5470935CA8448FDAA340A2CC82A4EF5B42D23CDD5BAA7F559B6DC516CEB76BEC0B20CF703CD756A3845A0FF2DCEABFF569D265E4EFEE6BD18E2A3987B72BF0D9E796BE4D1F3653ABE48F3C328DEEAC61BA8F09AC0D9847F7BBD3CBCE70DD03957FDAD183DE4839B443BABAC11C1E6049F63403BD02D0F5D5CA4783C94678E02023EB43FE5CBE85D61D4E5569B38F97C987148A097A04EDB62EF943EF66DA32319D0CA1BB07CA5BA24CED7CB486E5B1F5A00898BF627141BBCB77B7C08E79FA7A7894D8B4CB0EC5990A173B3BE3459A234E7E2AC785A1B354D6B7D3770492B6C293907CE14F96D89F3D0C7391B3BB58D90FED6D9FE56A3C32F83C103B725A366787EE823704EEF0A8E3AF2B09E68B0D5CD3F37D3B7CBC5BB46F4FA55BADB43D6DADB68FFFACF04D9D0E44B5FA6FDE872B5690EC1299E0482DBEBA462B88877DB784C7669B8A6EDB413FB64B4DDE61D7EB7F00C8F564D0F7A624C7F02C974C03260BDC4C2F7D5FFF9D7F25FAF549710AB28B2B5F2B6D9F9D481409BEF5BDD75C0A3BFEDAC07F95996F376363924E1B4E8E265C972A3653DADA4B59FBD8AF363D998F5704A5962477FD4C72A4B8BF37C1E4E3F761ED70B95ACEA6C671B43FD79C77CCD9AACEF0FC6CC69B92FE0F981FB559EAF92E97E30DDEA8C3E37871F56FA60B9138CBF0EE39D2E4A178EC9FFE4CD71265F0B12955BD7F3CE2A17283302F2DBE1E1F2FAA81724BAAB683B7ACD0BA1186DB562DEA850DDAC19A723CB8D8EE8120E1620971BFF7EA53E565CEE72EB7DB3BBDA6D7F04EF268836FE6A20BF0D367BDDF5A0F9010EB30DA8EDD52EB826BCD1778E92B4B7DDAE80A4B502C7D2AF383FD984F6430567B91B6CB3C4C9CE9D78501420782C719A6CE8A8E66C17A742972F3ADB174ACF9A5DB352E43409D2DAEB1499EDE4222D768364BDD343E04449F0D7ABE1D3102F08575B50C9F4276F0A29F7373767C2708407F52B57B2A28C71911E35D9138A7BA2420AB1AD4A25DDED166FC3B24DA352F799CDAA2E93BD0021B3BB6A7C2C83E55FCBCBFFF755355EAE743EB5C5C712247C2CB8429D08B19DCF1D606EBC59C13324FA193E168120E95FF5F1B90725488353A5D91596651EB4A0816070EAF1D0EA69B50B4B966247E1527AE1709A6C289A75B6C72F162AB5A314679DED8B5C791CFC39972906777D9483F11E32D5343D887AAB9D2E3C0C02E7FB95C9B7617195051F1BCD37AF8AFB5CEFB542A2DF07DC04F193CDBFE1BD1015C78B64273D80B36D273B2D58BCDD5493EB9651E42E731F4450844C9E660A23CB6EE733FBB14697AF5F49128B8B8663C9C1006109EF8A47FC14176DFA5B69187D68036A43D1AE05D11E2AA85CD7D142E49F3569FA9E76B5EA69677B7B9AD67A9ED6F6790C4BC59949F8AC66433ED17938B34538FF77CD84BB2C3B8DC39D80EB8B1729F7851FC6F16EAFB7D66EBF5FC19771F275D83F4B3AEF9913E1AFE1EEEBC9FB9BFF3DE231D5D3C9E4DB0032657EC4CE1F7B759029B70E1E705F18D1EE8AED98B0AA19AF24B18625AA437C941065E32D3358569692F8345E2F6BE2A3A92C10427C30306E95449BED8F9DE5BF1A7C126215C0BC99D37E1CEEA27E4DB233FC4B8BE4185EB72B51932C03B9F7A72D51CAF69EA1F8BEEA6C7D4F6B9320D59BB28C8F238BF334FD039CF1229C7F9A0DCD056C9F8CF7C3DE56B72F078387DBA816A26833801A10C5E16951F52B4EFC6378588D9C88F12C275CF05673720BBA31A2E9A338DE6B6911A02485EF9584369336AC80D48C177FA45951B226D1BECF8AB0043C802C9358EA95CF2376B63FC0D3B6011249100B18B85954A57FC18BF01910C52318A362412865AEBB857F0EF2A324394AF30B59F1BE86434AE3C328D8EC94396D4D9AEF9B2BEF57FC551487B392D3AA3A85E55C9C918FB31A3B17E0F4973C7F2BD0179F49E063B7906146BDCD2EE846DB416FAD939C26C00967DBF90089BC6A7E58B13827FE8DB29FE5BED908A523B9AF94594A93AE02A1726C2C94EE9D14A30796E57EA0B76C68BE5BA1BD5F112D2E2383454DE2B15CD1D849F169D47CB302A2D0254226882EE3A7A4DBE4F31F20BB009E16FEB6B3DA95EA258CF76318644AA8E7F8C21510EBE0AE902E52D85D6FB7EAD25CA92D8A695A5B06CE4538193ECF2AEAD4F0199538A359673BBF569927D0DF4335DBB6B3E143811A1C59257C204214C715B703B92BD2B0B8C9BB1F51FCBDC2076D0E38D1C36AE86C47F0BA1389A350AAF1BA1CEA341D2280CCA5C96016498EA5517020BADCEBB63FC982D77BE2A48F55374B1FDBD06C48BA3FCBD2D223543AD8F72DFC14C1B2C11F35F8FC9B15865861C95B647E6619DAF9DC054E68140205D16837422815A249262B6B9C648058C975D0BF4670453EDC168A2B2B54A7F8DB6A95621B080EA7E76C8FAC3AEBA9505D9DD14CDD5977B6F9826CE8376B2C379EDD122712DDEC38416A00F5F436BAE166373989658ABAE8ADB756FEC2A7FC4AD4A9F7A7E7B91824FAF70838491435CC8FE1885DC054879E05A1C19989E95AA6BA59B214514AF7A7B1CCF767AF805AACB17CCD5A9314DFE357C4A952974D0A549E6FBC66FA43901FDB089F70C86D512764AA4451C9002A0CD114192FD0B292394AD2E3343BCF2CD7025C7B3BBDE6C79611E8C7D6229CBDBDD954E8DCE23C26CE50591E86BFC399567166BF55E75C9CEEA7BC8D158748FA7D392D0281135F4C7CDCE17600AF3B7AE23DCBB2C310BA6902A79E934A9C13A958889065E8537FF27D08753282E23DAFF4488B7293BD108DA41F8B78D6E53648CD38DE294BC8ABF1EADF64D990468176F5962D4E76FB449452C6C0AFF299E5D70D105D96D29329D227D327D27449D5D9954778DD605DD03AAE3B11D0225D4264E59CCA796ED2E06BA245D260F3A0A65D48E9C82DC23C9CFB559C655708EA3CB2386D1E14FAB1B3325C92D69C6D3D1B5AC0D5F3BDB290024E5F86E36FE6E6C1191B63E1F0BEE85FE690261293741F094567704BD8F959CC125E4B4FBD51BD2957C89237E778C8910D11276FBCCBBA85C723C86917F9192912A763A923E4A8374094F2D2DCC7D624AF4DBCA43489D3149DFABC7671891F2C91CD223FFAD496BA4574E9D4899254BC2E1DEF6A4742698774D784AE264A9B4C94A8D44323533AE10BE17A892F77BFBDDE91C0D9AAE0D49B4BED79FE9644A37A93AFD6820F173BDB199CD9A2EAB3D627C2C5E01E4A1AF2E6C16240853F0EEF07A8B87B9B415FB634C7BB213E3EE4442804799336DE1C216B7F5869BF5D063371B6CC86F4FEF4932F92D3DEE710E89835682EF3D0B2CC72129990497546DAFAE9AC755A0EE7FBA666ADCB3640EA5AD88AC84E5A428A931E95BE573DB0E0545D2A3C4D91B47469A94CE5B16389EAB5B85F9AE2A4582153128D982B1DA780CA06E1799E9DE7EC289D6590A9DD46EDD6C5647ED675F8549D87E18B38F7BDCC968399F18B38BDA1A18511148A1C408B936F484179FFE7E183D8FD00D284A745D40C36F877E5C9111759B815E0B3CB8F630086B345F84C7603E04C517AAA3AC5D99A6C564E16E1348266B69771DF3A58214A7582689565B3E558429AEA63DFD9C513A12B991189D2AFAA1C254B5A56D54A7D62403A9CEF3DAEB648F5892A4E63E27EBD7A264E0E44A6A719946A347AC9F1CFCE66D7C359F1B72E825A9C9505326F20618EB34DE6C44E099FB9778F8D7A7BE8BECF31E8EF6318E52847E097F76BBA83B68648D951A874D73AA8B71041F1C7F4306E7F68E1DFC07B235D67209A9FC4C019AC35CD0D95A77A343CBE0D7DB9F572A6FBBCF4E6D8B21C660F1E02CE132E4777379C9BAD44CD65F1AB5247B28884EC34216A0842352EA7B0E14753B47C87F72D63EF9ADA886FD96734888A7645AF56AC26A0AE49DEEBC48AC82A311532457EC4CEC3A9385E91A9E6C0F1715C2E5C5771AA466B384D666B8D37DDC43332331D5B7526736267E96FAB2C458EDC6AF29D7224C8A72128163795FB560EE50623FDF32CDE0B59D2FD7B393D49B849FD34055D7CEE2C604E13F85B3C8972050528FDAD2D37192CEF3283F39AED78DDDAE7AB13D204CB603368EB81581E4BA6B222CDE6DB26D31C292E97C5C10ACE86864F5294E04A13BA6C17105B534D15D9AC5ACB72659FE823FB7FEDCF46A02C63489474F9CCBA4980911FD1EB82E8B11913742912A042B2AECF374B54D73B895326343934A4467EA7096F05C77BA4A4CA2F23B9CCDA2C4E53AE80EBE0A1003CCAF11BE5C8FB773FD1AFAA22C597D2A4A0EE439A781E0123DA0D3B9F5AF8F8DAAB9DE2864E26DAEEB53F346566A787108B9C28DAE90227AAC65FD24040D1C93B26DCC98890E2942448BA072EA1E59EBD602B602AAB3BE824FD11376BA4495DBE979CC8E63E0D9316AD68D4A464E5C58DBF94E58A878DDF03FD36A840F9F84EFEF8DEF3BA9FAC3A259A0A516129863FF6367A368EC6AC508F5256A89A199DD3EBE6921CA19229B71209CEDEEC7AA7128D4F14A4B03C3720094F7DA91F0BAFCD9386AE5C17526F00A1898E4FE5CDBB4788917785CA516EEC9C2951A025CE9B025A24BC8FA6030EC0C099EC47DDD576B41764FB01CFFF7D844053E00CD79B70B0469D889D32C067A4A97B133828C48D0C1C773E8E7ADB0143E6472B26BF083121B3296E56BD2B9939720D1B26AB2C9B0658C51CC8126753DABC82B364E9AE8D52D93CEA99447707050C7B0E6C2449CF41A7794D1CBDA6C1BD21149A254FBBEAD9DB0FCC02192786C452B573C35256C1DC6D6EF2F2CC20982B482C48E6389E1CFD3B8708CB81E892B749CBCF526E0438650A872747A25AFC11EA94590D96E7247DDDE73D3537781FE274AFAB09112A548ED6ADAD50A0AACE6F0366403776DB89EE1C9231BEC16D0E57DFDB65FAD3763ED6B0349C1AD21F58D120FA6F4D7F4CB05C79ADF22D139F860E9A284B0D9C6F9B8E9C84DEA62D4C57AC1F76D26463414A97B6E1AA44D7A4D5B01168F854A88172DD95C21468A58C61D28B8F051FE33585919EA746A04A742F58C253B4B3544172B3BFB7A2E9DF01AEBE399E44734447FAD56F634D73507B305F955B6B0DFCDBBFE2F1B6D0DB52E632338E2496023D179C8F43923B49A1C570BBD75DED04EB5D9E83C67E420C75C6FB61BADB8D8F62CEE23E0DFA57596FAD196EB4506E6A2A3492AE9E3F068D0B245FF886E12F19EEF6F0091A1FFBCE6F0E58376BDB7564F99765A93E562BCBBFCC33DA40B0894FD3C2A3E9857BC6B95F3A5B899A5A7A2A5A234DEB6699106DD87A54A16EF534DD856A295970B5C50CA19EE7EA75D942B23839C96770AA1CC5F40E54DC85732D83C59A2B56B77A29D1E1D3C0F8D5AFA6F628F355813730776626571B2F3339EF9A4489F312A50571E247780638E16F517AA2E8CC4E52FE0A02EA36270480B3BDD661C1FA65883234829F596BA687813A5B9307E90E054EC4E7931F6319F6CCB411CA9A04F6CE2C4A6B89499680A4AB286FAC2F7DE3C54B65F95A44298D59234AE74E3D6C44F8B642570DFC4CC3C8985C3B96AB9210AD99EE6E09580AD3DE566832DECD9E6649F4C3D2F815A899A648889D02B54B9C19CD82BC48CC4E47192A5778C399239D460439621F4003A4C448BDFDB26309BF6A6E340AC3F788AE3F5745A66C5426BCF3FA458E9F22B272CDE83849915A6FF7D8D8DB0BF1473E799A026DB8D34BF77BAF5E2FC3D3220D06D4FC3CEDADB6C2F5D658EEA60967AB208B8B74F4086F3142F18364B8B723A9AC96253A60F0DAAB32B56307B1FE7BD991ABB1D424B6212CAB389D536DDA95329DC55D29A3A6A4BB1229DB3AB4A0CDBFD9C682B48DBA9AE2EA4883FBA3F605B5E7A08B33602CDD7CB38C0AC32789EC95EA64B2736158EA48B81CEB9072C7C8BD94E4E6302E311EFD534CBE99C243D31C7CBEC307EF36CEA2CEFE65AEB771D667B42D00907264799A724921E1765738DB5BDE3246036A7210C7F027BB61B4C3F1C682BC51BD4408A8C95E0FA502C2A70A14292E0B8FCFCDE4B0A7B1538FAB1DDEF79114343F362314CDF0B1C2526B92A60E8B68575657A425C23544A04AD7B0D460F96F5B710A48C769A594E08AC5B9623B7F4D3EBE2B23ABFAD5B62D3D4DE7EF53D955308E775562AA57C9945C8D70BBA641A8C58C34F1CDA29BE44AD4C9757FC9DD3DD5636940CAA12BFDD1931CF4F36477446B1FE069E85A01F67E7792E95CF755732671155F0AA27220AF6440C7099DEA41CC61D4333EA96F8257F2472729E08125D4991E26AA6648965B5650667C58090F420A14F9ED352AB034DCEAF6D65BF0257024725E418CD8BFF2AE21E27B6582A5AC7C99E511D73170AD57C969976B35C96B0FA775B32B8690099C8EA5E87BC5FE71C56643A687A06ED63613DA2DE77215AAB92E7B81C2DB3CD9B22FD32F04D17E66D2A48992AEB8B9BE201F4FB32ACEAB447DAC6129877DAA371BE99D9A34717DB2A21421BAFB886AA6A345080DB9CC65AE8CF5192EF2D112C4C812A744560A5772DD90919202A582798710C129475DF5363AEDF50E70E6975967BDD3FADC4A8EA2FC2C41B233E209143CA92D3B4D1A6F3878A73B0E34F7A12E897359DDAC2C6AEA82092B93A6AC42DBF4A751E2FCABC4B95273B3156936574ABA16A7785A4D657574C136FF3C84ECD4B7FC3FDA9535AF2FF8B1FC06A85E519B06268E7A6DDE23E2842D69BC3421F33A19DCA67A4E999EF439963BFAE8293F25CB279BBBDE983A52598A2ED9CA01334D5FC5E5E6FA23050C9672BA2E8F55D66C487360819D30A61EF105F829286AAEC41DBBDBBD60AD136E771B6F1B4870C08623AFFFF715FEC8F3C19E249A5EA4D141981CC73634BE7AF5EF7F7115D3B06C942C7561C42C647A4EF5B5C9604D4DF2BA96CDDA88E847CAD2CDAE94199092FBD8D23CB6655B8095D6AE9AE02CBB80D5C53577AD92D50A55135DE024CB03E644B1B40329D0C364C94B7FE49CB2BB6CF420EB12F68621884C72208CF5B1224D4D70942517292F7953D05C1586D0782CA15171AA34CF4CD44CD862C6778A9689C7976E5FA1B7D6024B864FE4B7BB24CA6CE8324F0EF92B7846E713E2C3485A6EFF8215D73934DAFCB002D38322A50EF1260AFE7A252173D965B34E825A625A8FDA7044B57A31E58AC4D75A4DD27C5B71AD0EA72F5F20EC7040BE6D12A2F75E6BFE63CBC6D196F5C32DE35DDFB72C456746B22E13D6455398CE36C407C4095DCA110D7D87935B5F21CD914A53597EB32CB5E32A8690596A51D4636F029B1B1E50DE612CC90E2786D998957EBA6256F1217D051E444700D3DE42AEE92E7E2AA4F91AFADB18724F6DC68B8FAC23F0A0AA7FFDEB5FC839834DED3918600D3E2EF39165257102397F2AAB2242D72C5B8A52EDD441294DD3FA0100C3F24D85A521FA4627C42AA294670CC8A6F5962DBF3CB5BD40F66FCB3E91F5A59FCC12DB8A7D81ED0EB62BE5A9AC8497DD06995931AB313B74BF4B36098A0B3D25DB49932061C3E9F7B1263EDAAB635E236ACBDDA34055EF8AAA1F469C824A7B78BA3C02B12A2D9E80BDDD4BC4DFE219C71BCCF02B7CF240925B08743F926498F52800439D0D0987F0B4FFFA3F5467A0DB2E65C701E7B5FE6DE728351B129CB590E956BE4CDD69DB43B6936797505CF7C7215994C7D6D4F9DE149AD21C6E7BBD5C4BC8D7E88752A9BAF2EA4BD924C09AEBAE7A0D5E6D2DE93BD810D0911D324B26A1D57378C5D3226ACABD7CE4FE863F4C734071326711516A829A6A67E722D70A4450854864C4A3C6E6A75A6B5EF057C0183F0DB9B0C783CB013E93EA93F5A5A448F8A3BE097ED4DB843F097369042A5A3CA3F37660007582285224EEBC7C631AADBA72A99E569DED2B5237D71643C3C54BCD6C2DCEB28D50D3A5AB2C576C655931F37CD3395BEDE7A9C86C55B3D2324B2E4DB3C6F2D176FE8465ADCD645856D2602214CC32B369E3BA71C8D2D0877097CCB92BC07993E8C1CCC6D37EE7114D93AFB6D183F4D54A93221361D1A31E275A60E03A62BD1886E24863C95D7983C9AB5C7AB32C51A05AFC2894BE8F385B4A597D75C66C3BD13029D567186EF135FAA531EE7727C43F46A6781AC88380B3B3DA524F6B169949D1B959B57FA90A6D8DDFD0A52EAD3B1B9E34DDBAE66C202C3B068AD629F29DCB896C0DA351533220BF79AB2FF317B7BD64C7943DEA00CCCB6CC3A1E5122515A256C96FECCB4C15EB2657BA4BB99C0CE0EA1326B472BCE1F4C704E98F8994D2DF014E4A0D55E3218B7DC956625598F1A2BB2138C9AABA148EC7A614214E7A5AC971D8F4E9F5E8EE03D5311DF2255F236115E13DD4C487D5A72444F802D17B0BECEE6A07EA5C1618C0D928E56828FA4669FE9F57DD8DAE7E163A4E20626D942C15A76917181F5BEBCACEC963DF9490DCB289B0E459883A3724332BE6F5C685CA5FC38F9486BAFD8A782C4DE0341ED8ACF62009589127DBA6A16F554B0F2C95EB922E8914D73C38C42441CA52D21FE9B54A13E02A477D69A4B917120615136943400183252585D0B8132A6C75C58AD37C09E447200A97ABAFC17B12A724BD7A9496E64AEA90F54DB46EC13BE0BB824FC40C62E904E51BE31E975D44146B7E6886BBBDF42C8D8E63B7F8A5A9AC99FA31D26C2C577559D624954E5E3D952DC197C94E53ABC396E933341B8E3A9DADAD4CAC8355B599B9244FC156B806AA9DFA34532CAE0035ED055034032BECE00367AE6B26C33BCD6907660444935869D769BE53DCF473E992879B41B0D1A547559CC7CC66559A5218D11C89BEA03229927C03E890017E5BC517B39E91602C37628AF5AD2406132A5E038933B315A5E28F9DCF6D95233E05E08C8EA2F43CC123A7998E5153C7111702B9A8DB92FB15F00B2E9F8891E69B12A48B9A1ECE3204EAA3D977AD09ADF91EACCC050F3CAE9FD774AF7F53E1A4183458FA5A6FA882ABF36326C1FECBC448339B627A84EDB62D6FFC2683E2E47DED4CF7E0CBD0B0942910E9CC4967E09455073E7470022DE91F76B9F4C14670ACF96A0F20D7BBDC50BE49EDCAD92C360912470A18DA8F9563042862E0643D73991BDEDAB3DD3301983E6037941C2A56C7CB5D0F709E14E82B1D748E0FE33E4A5BEDF9ED87EDB54EEB733BD80A327C0B6FFAB8685A376B666581F68D59C8146936DC2ACA6C4D52AAD34F7CB449F4B6FA324D683FF353D65F69BC29E31CF3A38FEDB27FFBBEA571D715BB2E6ABAEF877E75744CD007D6B67B11FD44893824E95DEADF70D972700B9CECD0CA82D768FC0469CA1AA4B605A4BAA70A0F623000331E92B4D6B5F12FD2A08888D89553594CE28AACF53C33958638529A1065ECDC08340DD6F2D4549C7B9175B364D91387AC0997C6663AEAAD00D9ACCE9EB73E361B487475547CB5DDDEE85097BBBD608F53E41DE9BFBBF447C7D8CB2AD309D44CCFD657314D71F2AE0C81A5437E5769C72B4EFDDFB5BC8EAE163C569A2D61D972A29792C9E214711B97A0BFFB9761A965896D3B783382DA3214A542B25D4D8538D45AE22CDCC8E4E87E200DB6C4746AF6247A1DC4F834E51C00143A3CD9813865CD593C6D47579E559DDA04603D7A9C1AE5095176ECD6E99029C47DD9CD7A91E90B0C307937E0D448AC197266326716ACF4B75299E84983BA01812A7CB762CA8C772B72487A5B677F349575DD9C864FD412B2DD38DB0A78EF2D9EBC51415BC1BD9DD1B195A68B764E9D8ACAF6D94D63C135FA75E04178B74D11A2AA1596BA2CEA468D5CB65C66C85E34B5858A1CC76A703EF16043334E20890F1D9D4435862E71AAF8A0F5EFCD2DE35A93C81A48A02B38546D479DB06230FD20098A5AA8103C44BC1128CE4C1260FBA521304994025579A82A97456F3A802D9EAF85FF858E13F094A6F52E125D40954D1D4DC5893C08AF3183070667C3A8CDFB284D2A5BE6FDF27999A68C1738ADA7350016E0346EF0BDF5B46FCAA922F9C4A5E460CBD7FB3B48166622A573EC12E6573859DFD15F295FA0B3838EA80DA86E7C7749268078AB170E346B3F569AA8F4B4B200A9728964A297D5FD562041B825DBDEBACC5477F829CBFC525B599A1E9E7052752612FC1838AD4FE62BD5974A8855671B6A19B369594A2AC4B51753C6F06F02FC78C43F4C8B451436747D1F5BDDF54E93FBF4480852C687BBE2D21FE7CD9CCF74AB983290E79A322A11D39CF3CB92329575E952CB654C920479E2730E405256338D50361656CA14EC2FF302F7B5C0FFE81507421B1A655D5FB0559B34B35D7ECD9EDC32F812581227D2DA2F9CF0E004ECB5E094054895A3E9E348E4830ADD3F83D9D096E01137ABCBB0AE02510C2421CD204D857A5B96E546105AEAEA06A46114A8AFA6A79508EA70F205A26CAA7C3FC2BF6159048AB2B2814FE4731B893744196C0678B740FA47155DCA875EB6816C5EE3724EFB1AE3C49A6E158CDB8CEA8566CB53B0FE969E48230EDF01A3E14D340362C3EF435975D8A9B36527B296FC74598E62505DFA09B6BA68B798D32C97545BFE1AB8E2CCB913EFAACF48769A0CEC12F4FFABEBFA5DE34A936DFF190E1CACC0811A14748382BED0892E28D00505BAA0C01714888B03D338308D83A5713234130C6282416CF0302F58F00B06BC993758F0260BDE60C19B798307B3E1041B4C38E17B5FD539F5E3BBDD03C2D8B2D4DDF7D65755A74E9DAA2BE581B23CAA2DDB20B16921CF3790F69B7A923CD6E35AE2898CF059042E1EF9DE9194F141C55AE5BDC5FC8A8165CA5A1B9F6E2DBCDAEE05BDF351A331C235B88BBD62DDF23AA83D5ACD97F2B1FFFA797C21D310E5D8512EAB3AAE9679B1ADE08CD524B930A852D49ABE924903FF15B7624B1AC8583D2B49E790AA6818D70EB6B10A2ED95573BAE7817C2FBF7222B6E4AB21D4B74618E58F11B9D3140E5AC01473FEF9E3E7BF7C94719F6F1ECAA1DE953A8FCDE7F78E71D454C2F096903B5CF57E5ECABB0ACABD652524CE8A0AC44A52F4ABA5C0D04AA6FC976A857BE44E453A42C6023F3F286A456655F981524BC6230A89F88AF568F9F9F203283996AADA7AFCE1B1D42A25416C05F7CA2C2DD81F9CB9F28EA6796FDC3599F02E736F39864CA26712164DEEEBB869CD5E983241A400BD046A74E29341D81803EF8D030489CB2A4E465E106FD6D50A6EC82E877D7C736D2D176F8EBA77165B7E16D754BD79314F09562A0D11CD95123D1B19EA905F9000584C5BA28A071905DF23E71AC5B4E54B6EBA2A0AD826B3A2732F2DE8515171AF4AB581D9516B15C84A044F5DF7166F099BF122412A95F4F9FD638148E54D25DE9E2D1F7E78BF79BDFDF0C70F9253578A8694642F9F937551B98ADBD1B934E44B970154B3415A95B3479D18F6C41590DC310CACB1D4236DAA3110E4C5D217BDC3E3A5F5CCBDD684E795AB2829B398DF1879F2B772EC164DED9A4D24546B80E3D088777EFEEBC7CF7FFB546E50B966A98D6E07A899CBFD050A457854736EC59C72FD9136F45C94BAA7D7C591E3CEC839D424126FD1DE12CFDB6EC595C741AFDFF14E36677159C263599F557010CD99182509D708DD255A34EBEEE3DFBFFCFAEBFFBD2BEFF2ED838647CA7CCA65A3B42DBF32A2E033518F80B8E763524AA6A10394AAABB6F64BB2B87E07152EF13B000D99A185FF953F71DF3C68477BFCD99CD04C7F0B03BFE50892617039207075A6A5B476D2205FCDA9F9B5CCF0C8CC476D48E1C204912A9294C429E151C3C5A52CE1D5BB60280B54A4A2DCBE5C15129EA643A4CF3D1C1405A5BE14302D1C1A3F8C5C08F5C2836E2506AA2A56876B82E547D70CB6D1824723F98F1FBEFC4BB6FA7DF8F1D3FEF55EA6210025B4FFBC936269A723EC928C614E70A7BB973B9C9EF22E30A7F96517530FEBF0CE803F6BD0EBA46F50D79793D75D329B3A3B01FFA3992D6E2BB18C99D1250B212808153429D91B3716A9C1361CD5402C678708331221665FFE5E72E7A772E53AAB2577045D46B1C14BE5F334D58923DE0E2564313A211AA081A077411C4E793B49B700A56E4E2574762AEB1EAF69AD8D4CD86C4C212D074898A0BB0D6297046D4BC048E148BD6CDDBCD9BDFFE3879FFEF7E7F77FFAB87BAB61FCB9C452B1A5863810FD380122ABBF1EBC562B2F22C1409BAF1FBE7F574E154A028659C790865C12E46959591206D3A1816903E6A8AB95C839B0323166C35D531224C926EC3B113FD608EFD87BAA5F4991B6BBA81267EEB5A977FE43B6E983BAEBF47C6DEE4711DB2573CAC197827D408CC2942B15FB16A998F0B4F3B57FE94D1565D5B578DD2A0EDAE8D854393AE595116FA559ADD895F4A17A92B8AF92F8F04B8BE16C6297FFFAE53FBF7CFDF7CF3FFDFBE77285E8F6695DD892AB02B9A138CEC329780F716BC556707DA5FE4B64DACA6D2A17A2E60421AEDED3066272458F17A95A50A2BB0940EB02DDF2A7F8B16AB29D569C9F72D457F877FDE1968BC23A2523A3FD12DCC559583438C57537816F0EE8665FFF217DB1BD1694C54EA5122AF90313A3E5762839006CD6B2B8BCEA733DDB99F245C9770DB65663987BEDD90A15CD92FABAC8F5EDEBCECCF99D453F48D66E062A34F50B811A65AB5A65F7F37F7EFDE9DFBF3CFCE1BDEE1DE127296765AFE35728AE34CCCA3B222295D704B7251EAF30ADA45BF554C92CE52DBC13D25A5633563D4929932DE535752ACD1D686ED2C06ED5B2344AFCB0E448B8A6416B64C472C3136B013DF06F46DAA011F4D386544C29DCD957D994F5590A4A045B5DA653FE0E39A41494A88174E9397EA6372C40B662CD8A65AB6506BCC7CB15B43025906A05321A112855CD733A28B06BF9199C1E7892F0790A7F50ADA22BBED55AE8E38F1F8B2D7FF9CFAFAD22BA0D8675F494808250404E5BF6A6652D1F8999F8EDDE14BFEF50D4A249200B4D5102DABD1B6F46D98A7365ED62CA03E816406D844ED68363A42DE19D247B481D94EE99C3C66ECB411381DB4F1FE9D1D0A239CC263E21949E07F555F14E352748D7AB1E09A07C50C958AF646B8897B4E5108D4971E494B19E1178E7965F4AAE22DE3AED0EF248878DD59CDA1995EAB30455F515815D5A71A3C7EBDE8936EAF6E5467D742CB5EF977F7E2D61F6E79F7F69A4F61D20F8F7C25764A856BC6AC54928AF8EBB650F4E64A1AAC71735C53B34C67516915E829B555EAD2475C1DB5ABCF6AE1350ABE372E09D19B59E3C132A80DF7C96CD7902E60F794A646C6B1252E6971E66933973B16456F429E0FEC23F92C0DD99ACECD167D0A0206935620CA05E5F6D11FDE0D7C3EDC0DB9D5860BCBDE0DE3B763AB7100CDE93488239715CC6045CC5FCF8617D6A85F66AC819E122CBDF3DFFED2C19EB41D9165B96C4F9F1CF9F95A70684918FC47797D19C115DBCE192DD82D1AE8891165FDF51A8FD4E6383580BC1D63A948FBF7FC0FF4A0E4EC19691F646090AF32D465AA5155590D62CD31437988DC61AAB20A7503258B2E426A3C4122FA79136542C6AC2F221F37E8DE29D5F540D24DCFA9DB64AD6ADEAFC5B50E4B4B14EB9AA63450AE92F82A70E28A4042F5A956E0336B72DD8C241817A941734732A8FE1B0A5981C091899786780A8D8F8EBBFBE7EFDD74F9BD73BEF57E00B6424B2A6F9BABCE07015940555866A248881B546DAEB21D8B0E4D0FB55FEE92465F92AAF1CA043E3C7A8659BB67122D216B083A0E58C01901145DB663909182064B2BEA44A9C0E7FC2A24D9AE0CF3196AEB56A67B28240E43C8FEC7C29B89DEB76822DA10436B52E25D882CFD38B711614E6D4BA938DADED8BD412310E419C0FC94C7D052157CA95E29D528D48E1AB1C0AE3090826DAF2258E88BAFBDB87CF7FFBFCE51F5F3AEFCBABF8180FC040CA04FD84FF1529CD151EA340730A35AD626076EEBE214A688367E9D435F7182B766D9BD7030041B81B6D2807E89D784A877D87427BE85A5A6BB87A0A6BC2236D07D55965D1AA8DE35C71D292794346BC1382E6F28959E36BFA9C4B3297076733DB6BFDD429FD060E2814317A64CAF5C33569C8172942BE61EE9464A677599CE6BAB78137A1820B3601CFA09D07E2C6F1861D34777A48141EBE7DF8F4974FE50BC7C819CB126051FFA8D36B18BC68C1290E376CE0005B8197780770AB75ED83B6163AC3AB8F9A20408DB9680DD5B3B82331ED98CF74F64E1AF8D4DA73A7C99CABB6E05B439760A9B25FDAF08C69F0AD8B826AB8E51E8D54A5646424B9D3474A1E742F81E4212C2F137C3B82BD03E1D2BB39AF2BEE8AC156B5263BCB9AEA0D1B2F579017D978E19607F6D0A15021C367AF8C0A010EBA45F4E671518DEE9BDDEECDD66939089700ACC0446F6E4757CB2196A02E62E5A32DB647F54BE10BB5616ECDE70E31191D75E828A842FDFD1E5727BEAE40DA68F7C67484EA8BEA9DCBC5329613B11F3047454BE5A6C39FB3DA8A2E90384C9C8937F63F73435B822D71810847F6501474BADD73AEF00CB6D4782BE68CFC3709B60A85D0A4748BD2A5DEEC9DE1D3D380451D744D366A5EEDBC75D33A4171D1D1D1EF18BAE1FD924D5F496387115B11960EE28C2E6222D1A3A0113A660FB60FAF939E54598EF2EEC30D6923908BC58A9253951A83C941454196B6D7B1100B5AEDC435A5B2B46F62AD94B45D9F9E2C2D3B862730AE86772E6DF8A9E28AC166242B7624EE8FA8D466F8883283F7CD83704057D4A2612136D0398CD76B3B932D14CF19EE9D77561A9A27C15DF6AA40808D5108FA2B0CC6E781C31B8D61C10705B8051CF5E8EDB6DFDDF3BDF03330E7D68CEA3A74707260FC91BFBD3780EA088510A21FC014497FFB0246A324439D1BA52A8AD42851DC9C223124795BAC88DC598C8A9F5416B7A3E725BFF4B9D2E53471AAD96AF96764D043732A57AE84DCB70F408300DF82CACEE6AC522EF4BE945A50E96C507DD1425AA9774299706F23FC77E69DAF77AE7C0FB1AF7A3C0223EA1946634D603E2C00C6CEE510AE11746698E7E6561DD40C2991F69AA4576B5B4336D65DF1D63A32687991FE1A4D3181EE4251B921BFA1144D623BDE5407B53E6903B17CDFA549DEC2C4EE7828D0CA3F9F3CE578C592EB6EC8FA1EDA328448CEFBDB544C6CA232D4D3F9F69B75F5A740A19D5E218FED9B3DC0273A4D271A377A6B32085353EEDDFD88CCE49D778542648518695F503FBD337C1B9156FDB2BFA66B6217DDEE1508FA71B04E6F6BDDB7116A1548352D7D721246D32A7581F635A2DCBC0E360EB005BD771E08C5DB62D7D79208746DA55C111A6AE8C7A9B67B6B45F38EB67CC98D0D427A68BA8D1666F64E530361EF1B226DF9270A87489967017F8E85D989C4DE07D98EC4D840B61E1E21F001378488A7CBE7E6D8F921665BB71E337B4F1B51A8545008651CC1AD464BD6F5D734271BE85A77C27D472387BDCF2C4E7CD37BE2F490EB8706ED581D5F65E2C40930ED721B20F9AE6ADE411D015A51FA2DA0150564C598062A66C8DB50C802EE7EFAF123FEB753D9832B456C9E626EE65C721923880513B6E7B86A1E199A429F95A0EE774DC2FDB74CD8AC280656D0DBCEA452BC23D1AABD8591C51F5465BF3B71FD6A791B177FF4503119FA2AC116B40BB96FA314BCFAD4CE8C1208C59617E451C92768D35B1D7AA86082ECCE6FA1C3866BA254A55550C86A7A861E7F730FCE7D08359329BB20EBF5DA0995C9FEE50E6103CC8004E4FBCD3B83BBEEFD681E68B696FA5897A97C00C15B972827C041989F599A88509F7325C6268BEB1CDE199E077108680FFC7295226AC2444DC888C265670641E52B54D197EC684ADC372243FAFB7A48811B33A3A8E61CF13A7014372783A435AE636CF18A24D9D6F222A588E7150F82608B33870A84B6D45FD9DC325F52C600B47CA509F81C7DE0161C7D2E5E43BDADDF4758C641B19927D9C684FA045C3128CCF21D59BAABADEFE176A039BD29F6CC72277657C39C18F0563ECFE8400FB0F3E5A4CA3C4FAE99036C666BB38DD36A40927CDB3B20C3910C8EF4B147306DADEA91F0F99029377AC0DD3B1B9B52E6ED7819250A5853C793888A2669A12D116F91B1B68AC23A1394FA4101B98F08EF03303BD6B5DB5C9F30981BEAEE6C698C3EB86F442EC0F182B5C8EC3FDF80B14334862DD1FAF6F08E4353BEF3E187771857153EE1DA48F61AD6CE6D91318A4E05B773CA1272A63409BCFED3C2AC891F3207440474DE76EB6C481ED6892BCF585120158983EE4D47D3817844218C043E6A512151D14488F4CE3572A76535BBCBCECBBB866FD0FAC412A742212B2ED9A1CB123A33270B4A446606DB8D5113A353C14EECE5234C4BB3D9B2715C23E6B49206E4140EF4A3F9EE4EB56AF4FEFBCDC3ABDD47ECD7C21CB84B496C3B062D8AF1420DB005D92AED4ED7C4840CC3ECA2A9226DBD898AEE787C2967B628426E74CDE89DEE520F3AEFA7C8D0844969896477DDEBCAD9BE373945B04229C012A1588863974339261F66237DA34D6FE810381999C5CA6C200FDE218149F082AEC08699491F5E862D41582BF367E5130A27284E0C9AE9A2B4D155A5902C01A83383E8F47149A85C1CF856460142CD75CABE18E95937A7CE87177326F5499A734ADE99B52366C8CE9CB239E297EC7CC09C3EFB0D73DEC74D97B30FE9DB2DD327E289EDCA9E17702B68F0C626523D3E4807664418B4EE71349383E1BB195C980359027E0B3FEFB53FE91253540C8A39E9F1E8925A818B4828F35396985B574F21A35CF6E48CEEA87C401AC63F1D6C3343EB0C0581F48B7421EAB8600F08FEAFFB368D2E9939B91C8595A89A13DDC3D08BB815F3D451C51844C3646AC243731A61D49A62741677CADC143BF0ACFAA43969D1C51242F81EB57098535AF33B8542D57D0F4F2533AE7E69DB400DA9E2670491BA39D370A4B72FA465F682A901B9D323AD4BB7BBD4B66B8DB703F501B0EAD386E8FDE9323C9371A3ACF2425931B32B603E281D2F2CD2CBAD51016C50A7DC89D62661D193274FD0D1A4F39DD5DCEC691040ED394A8E284BA6E0B6FEA76DDFF01F23053673BFF46251481081AFBC35CB05A96424861DBD3374C090BD64088A94E6377DCB510576C770132100733667F4D6CD7953576018D9E96112A4069E000900FAB25783EFD742EAE581B526F368E510BA02081B9835C00F0887A093527209F7325F45A0FB464806B453A8C487A86A1DD2BDF924D89E459522D326D7AE0E2493D77072ADE2F368CEB478A81A47318FB405AB0680DD354D1F3263D4BA1B4793E2813789627CC1D91D7C11821E9AF376CC20D68F08F0E7D6DA29BD75285D9340D7049C5E8777B21D0FB64F1DC8DD5D31D7C603F8E88F8AB225FAAE0EA16AD0842F3E3C4AA4866DEC1AF6E1AF62AD7B0F00FC6CCA1FED309DA1756A3DA319E6ACF616154C8B8BC51E6B62DA846CA7DCEC2A5260E4FE5C77869CBADE869C36C44D73271DB45CD52D447B0D170BD86A9741EF0EA0739873D5423CE0A17B624EDAD2D4C92E1702B2DDDE8DDE0ACEF31870531F1C0053E1DE39189041C3D90FB2EF00C25DE0789372405B62A211A91AA74A63837DD45BFB0B8A1F4E8973085C3633DC56E2A064CE704D3CFAA198137A83A53B659A2B0D67F04A631DF3C2115AA9D39C6C8EF35E565574D29C0AF9789D3003A0A63E6ECD1A40CF78E2BA2B696D22BDFB27433DE084C0C6D087E74E6F9DB250B9EE596348DD298101F45E6729D99BB74C9FE6D03974F3356D70C0472D0C1FF2C082E5F70A157FF774ABA17874AD281803CAD25E28DFA42D39EC2883A57B5B6B9370D0892D1C5BFAD11F75E007C55E86B2955F2634EB14C1A4D0CAC8CE2B19D8B2898BE5F6B09933A2AE7B66ABCBBA8FA193C0868155532267156D54788EBA93AFA0778711EC2E51429714A68442F3C6519289D0CF83B3658F50C5D9F8AD489CDA9A1D4C84DEF9C81F9F9F81455B3A74800F469A3786FEBDF727B1FA6AC864054E0C0AD907DA323428A6556FA297E2B096AE290DCE52A49617C977296B0C528CB5E673C4D86AD36A6B4DB16691E4D4B4653C3A40BFFA99D5091B4F63CEE678C1E47531F0AD62813A77828D4BC1367B27B025D08A77C73CD862A81FB52CBC2A2A5AF3CE2E31E9D14D3395210780262B4614C423B06F8DB8E7D97A3E9A1C82C225D717BA9E21A09C0D68803DA05C3D19A982B5A7DCE5583ECFBBEF1EA15BAED9F66065DDD5BAEA3949154B90FE5E8D364483CC66147B15E3CDA4E185B897DA4C8079A469AC5AF227FD6CEE2A7362B544B9354C4E0231861CD98859D0AFB615D9D00CE026BAA22C5B65992212CE2FC45DBAF166E3BFDE271AC8094FDF84D76BDDC98EBAD6A9A3A1681C054EC041A162A40488059452A86D309731DCA671947A22CCCCB944D773A38A7BCAD00FB14F9A53E02C58BD5C638A833231724C09D6D9ECD10C61C7EF2CF9118D4B72EB397DE11A4305B7771BEEC6B3C450DE60D42B7760355ADDC93AE436BC6120ACA5770E46DE761715AC8D8E8DEF72B9827A883D4E6A5CF25C7BECF1265B8D9A958483C60C06F62B8AD9B1848264024AA96BA321AF588F4261B48194C97B0FD60573019F537D25D2CA40F1FD26C6F7EBB224D32FAD13EBF94FBF221F59A970500D1192EC794625EA35AF2A333534E7794E129AE46F55AA996388CEFDA472700481E735A8DF44A4ABDEA68569CECBBEB74D1341F2D5970D1117A7505E2473A69463FC75D098D257BF4D31DF54BE94B620D2E608E17BD8C10EEAA1C476325432ECDF79C319FA2EBD2DA8CBE1A3D0E5CA713F751A68D29AB6EBB2C190803C89890D30916C396518D6B852F5CE9EB2D2CEEA87600020036BF27606E51330C453075B597BB1B78541EAE26C6681877376D44A0E8E16F9B4AF2B3DC341CFA3C9003B89FCF36EB4A953BE668C639E57410CFC03FBEAF74C99238BD400502AAA66A41D74F73A57D18A40578E02A61025D2EA54649B3759247332D8AA836E14E4830824D56E1DDC093D1B68D6E2ADE9F39ACEB14FFE5A34DC839C087AE64EF59359175B71DBC09CE64FD518D4292138746F79E1B6045BEB44468C4D722C9422EE97E3351B205E8CD6F9CF6884B308B6AD35A2395076EF7D59FBC5553BB96B2E4F7116130E4D21FF9A83C683A552336A4C2E30D8AA12C5BBE21EAE3C778677EA71C7AA1CCE3B9C4F3DD2D1806399EAEF8E89E219109317692BCEC1F797E8B99CE96B3578EC17A66E23F81836816B5A9E3881B0CABF896757A077863068C8D6BA2BB72C31093A92530E57F68C9138986D7B106C0101D02C53A98AD5B24EA1D5FB9CFCEC730E098A350D09B4E8254302212E42C5850D57DB6227B439A5EBF09CA36DCEEDB94569CBDFD1A262CEEF1F8B77A67EF5C1D7B9AD44B541BCE9C30627DC5EB5B53365567FF2CE25EFE4CC0B55BC07D10ACA445BDFE0850A601BEBD1943BCB2B3891E65D0882DB3B8E194909980D499DAD0D26F826243BC216C9B379A41CF28536908B76566546FDB0B09D1F36EBB2B151277FBBC1F87A4E0EDD9243F64D13B2F9E9AA477B157B1832525BA642E5C410A25974FEF076AFED8476E2944C4CBEB73686118C3DC87EE93D9389733B554B39BC9B536EE92CA65EC877B78E8CBAD423F3DC39D760DB63BF96311D30A78394E083EE828E49C992E53FC36F5A1BE8C7D069B038CED65545C372B425280E822A0065D5B7571DC89AA3F75C2F2D6B1A3734D800A17F1EFC802D761DAA1186586D2CE674899748E0D72D9E73081E34C77FEC5E6DD21338BC6AEC26CF0108409442D4C27699AC2AD7642CC17AE2E82829A416D1EC9A16CD6CD6DCB587BAC7A84BBD78DD44D9821ACC9D707353C6BAD1BB631AEE5C37D4C578623B81B5B9EED62D15C23DB9641EF9AF4D629465AA565BDBFD3C5A1800FCF109B5FE8268D052000ADF6A9321B991D45F7268635363B5399F89F0F1F1ED7E08356172CD68F911CD76EB6AC7BF5F69B53FCE1686F3A5628E2CAF2BD775F037E30CF39E1646F07E1675A3A352A99BE4DA12146AB4A3E2E68409278048B778223FB1C444EA8A18AB6B3CAA486BE674599464E8DB81C9CCB40753CEC1EE084A4F9A33A0B53D69910AB48EB8EC9A626EB6012E58C619F337A0179BFC2C5442DE6B82EEAB5C913C4296E321696DDC199F9485C3E4139938521574B072D3DF2B7349E6BE516BF25022D8F636C550CDE6DB5A00DF94354F34028E7626AE5A2B54388B7207B6368C3A3A8EF5587745DFAA52C87944334F84CBA44C188DA601318457F058148956F11473A78DAAB9F0CCA060ECFC4D025DCAC61A36E6061731B51769DDC1B1600BD15E79052C0028178BD698FEF0DC85F090C9B170BAB6398EAAEE6CA6B9C3237602B4A141B14D51624E5BF5D82607A5457B2B54BC0704699A3C0A3C075BE961C978D0DEF60EA3F4744EDF9C23022CA4B65144E75491414D78E7D2BC73C4849743A123579ED123968B5C1955624354DC9FA3EBBE07EBA877A65FE443E3945402178FBE7AD46C673ECBB0745B62E4AFBCAC4C82DE6078A17A7E1278B4F2FDADCD7D88B6012B36260B4B1675D6B4BF67DE20F2AEA579295406DF4E1E049B39A89665CD796A90C19CBAB025179D62CE171C8EA02EEB2E75A39EDBCCA50758FF5AA7E5B1AB2640F9C2D6F11849D698EC56D78FEFB0C03C1CFA9048F20E76AA6E03795D1873E2A138BC561F35A41F0932A5C1C65E79B672A7D3832D457B3220B67BB5D35BDAD28F6DDB85274E76C57580626ACEF3E48B4ED61F34385B2B6F9CADED694E8F3F5307D5F7B6E517CEF0D19C17B5392F3AEFF5BB24D315435E273885C684914594261C3D74500702980A72B202AF96AF3FAD9C6D6012969E005FBE6008EC41A2359C7A441A73082398E0791ACB5D85022172A73FB3E5A9EC271F74E78ACBAC74A31723AD311BB2286C77BF79AFA3FCC595737956F9E594176C556891A80694C8F6F4E5F2E7CC4668D3C2479FEDD6B5CD9DD2B3AE037EF2F489BC4AE26CB5C0EF31DB85D904B6F26DB5D7C6A647862B2C5BEA3ABBD75EF566F17B735EF50823FE686C843A0BF9ECB02FB65C980AD0BDD353B55A11C3620E7141DB9AE3DA09B3532558FA2E44E464543225E4C1F62983EDA053B0318B921E8405BEA5BCE6E676F8F8077960D0C35BD9526AC7D1449AF95A72F8AD08FA18BDEEC1C6E89F33F496FBB429D9031DECAA4B3862484A1E46231B545AA7A15B5D1F129362DA3EA4BAC7906DA6E07387F28872225CCD6E9C075B79A41C69041F7EA69A647118A3A2AD467ABDDEA58E8D379D3D390A923EF7CED6062B71147ADD5DBDAC0595844234A73C8267294B0FE41D9D7B7173C296ADD681A2C0FEF1E3E3778FDB97DBF6A2CB93F4F9F8863973EF13B0D6BA28B6CB83014F0602B9B3848B655ABF958DC5521DAF4F1DEC9221B239559A15B220633EC3A236A4E7792B107926B16A9364E6C5252C30270A95A805F3AFD41C37F2057AE39EB07BDB88D8A50728E0A6F8FFDA6456E387EF30052C53D012EF3473F618145CD491D66AADF2493EEB33BDD910B54098A351C20D47EA13EC67CD6DCEFED2CDA9DD76A7B3A7AC072DBA6CE2D39FCCCB87CE674A17826D755E00DCEC68351C684F06DBEB94A45393D64D988BEEB86B9918D273E37BC0863AA21C41B6E724A2E988EA6A1EA3028E612D98D12BB074FE545D4C701C88B8C0D93E85773E2996938585171169ABC4A99F67FF625B5C73AB32FCAA94A85FD64E80BBA3A5CC739B34CA6B782D4DC850833AA88D757A03685105169E20DB9223F02F05DB72977736A399D023BD76A305968759DF7E30EDFEE8B5258AE72077AAE6C117670CD8189A5B636731ECD158F2EB8D0332FBB1C63568DD3ACAAF107E0AFE30F07291D4EBF14CE525FAD5306723739C6D08E4D3CAAF862540FBF94FE29A58AC32F54B7BE5544327A75C4D25614E77F3385E7633A05059F760F56CE59D1337B5F19A5E25339EDECBEF827F777302748DF65895D13636D59C624A9653BB32729A843A0A154CE76FB8BBB3AB8E4545A058FABC0C22AD75BEDB586F625D7FE20CC8367F3543558E33270E04733ED1C42920D112077313C5B7F6E4F4956CBF7EF7FDA33C7BA9EE301E38655D9678299C24421E63ECC64AAC9A69E748B68D42A3C64F93625D9E175F1A2612839939B518EF398249E693C7BFE77EA68D55299E96D8700F1493F08B4E5A1DA1B848F269753B1A7271675A7ABACDA24E6F5FC48A68FED96BF5DCD5866C8D0FB2306894E9410A88D253BC531E260A733675038A15E79936F856EDC7FFF9E0654C04D8D3709EE9AD587553D1496A8E76EE97F6AC8719343BE0CDBD2376780CC3A2D809E62C09E840D949B1413E731A16814E9FC8B00187E0F7AB59D9D3F3CEA658BC391097641CA18B6276D8B8D13B0B911CBA3A1C0BF661ACA7A8F59535771972ED2BF776C23517A4F410C92791B632274A385799E35E3D4B3868D594E2E4E19B87823396C73365FD74173FF75E974FEC6AA474B431C49C3721FB27563CAF51656240E6FEF40F3367BBE2CC1EB5AFDE83B4868628FCF2DE83DCD1CD42C54999916D79B6F42B44836C6BE7A34D046184E8FA652353D6FD5467A360CB6122259C04F04989F21BB9B3D50999685758482BDFDCDF6D1EBF155B722BAAFBE5692DF5731A6895EC5733DBB187CF80BA77A566A189557CE16D20DB9DBC4C6F6C3DB2D3B943069873D4F1B10DC67871FCCD717B3367744E181B8F28DB782A53A73387207034BE12B522B3F8C31175738866FC0F06CA1462B841FA455F9F28023D731F0F574BABC539CA8910BD3C5BFA2436B9BD45F3E1BF5445BD4A54D1993D37D40A42E4CB908FD86A2483B56D3E8261D17469B3500EE8746665CE337B1E62654E74F5BA7615370B82E30D9B7CB14B01651F7267775983A0036B797531FD81B33027696B33A74F89DB034C8E572C1E2D1A5BF2DEFA9E88D44C4E71251648FB4E91C3C43937BC43733E3D61703EAB06CACA8B6C6FC6C7EF1EBAEBBE4177E5AC3267748DE89A9635F3CABD943BFD43B24449BBC467A64C1C506C78D335967A329F87394F748B20CCC916B1117B11D052A34DC278B520D2F0C231C9416BA31735C0E67BA1BA85C801A56D422E61BF3A76B1F5DDD6A11EC7CE66F05AF7751DBFA92BE0C5B1ACE985F829B94F3C5332969EA6C4F9F1BFE5192F0D1EE273160F200B5BD68C415516675B26B51F39040310AC3BB394065A376F533037D0A2D93B4F6C037B985367F3FA785C04781C9DFF7273E69AB2F6BC7AE9F921AE59D02454EE98C8366608354C796D33517446684D7D288FBA7A0429E5CD5D814908C943F09EF63295CD09B2B460083EB77FB1750414B4DF14CA9A845FFDB2CB03D8A6099A065B08AC2E322C372834F8D6601D5800BEB06769CDE3EB193FBDD0B656C601EF8490D3CD893246DB7B7D7A085714159392D964A5EDF4C7F8CFF222D87021151106C768AA63447C8A04A0EBFCC53DE6EBDF91FE4D747375644EE608304C507F6ACEB4C61667FADDF7A2886FE3292BF3E51173FA97DA0FE63958DBE14645EE1CFC114DA0A035D862B2C0F6C31474EAC3DF09732773728D15504FB70E8B2271BA21B1064922A4212CAF6D2A917F6C8D4CE63CC6DB89AACA261AD0B74290C10A21143FCBB3740812409D4C07B4F99BA9AC0A7A367D2D73D8AF3904DF72FA44D741E500365710F4F866BF7BBDC3437E6398A07E787AC53327F6EED8D37E2D14C7A3151267E9DEC9325FBB019E0299BD4E2716D506C2B339CA701C7092FA9807F533A8E64428EE0F226DCE46F9928CFF6BDD6BF3543D76557087989DCD282D92A34F1CD483AD7D73D97A4E551C60D928D684E45A65526B1A340D15FC13256CF3723E59607BDEBEFFE15D7FD3FB0D8962AFC6F3F109ED196739ECA3779909049AF0D2B0923D8163B6B145935462E2D9A1D36717467A775902C8C996B50ACBF096483DB84A3453AD23510D1E67D41AB9CDD16F0AB90E92F9C074D336A0D63EF0A4234AB02A83824CF0D95E8000EDEC5A8740670732B03A68A4241D799833A7A4F2630FBACC8F61D6CBD0A0DB5210B232814FFCD3277C38F609EFBC20859BAA94AAA9303339797EE4399E0611D07C7E5A591462A764CEB6A3324FCD7966E65420CEA898C9A6D4C86C2687D40686AC91EEB480FC64797D3E8F853AECDE8A9FC3AC9C8A160FADD9E78CFF832D730685A7C6A75A2875650FBE762BA22877B12A72675E16550EC7C3DB072B34E7396536699DA15D3579835E9F774D73AE278933D72A515975269E66A13298CACD6B52955F54F3D593DC8992D9B1BE03FD263196B880CD73DF963AF54BC3415656FAE4469281398E47DF062A21CF0EBEB3A559D527605185D9C6F6EF4CC26F63AC856122831B75EEAC8ACE84F6E7C93B6382CC0A01F409F293714296707EFCE341510034D3BB77A6DBE22BA3887D0C8DC66E843EC492A1E767E7EFFCA83903DCC29C7E02BA830988F24F304DFCDC93D26D11706092182269055DD2405902A5E0680F0DB58C18E3064D3667B65C6D2407FD11E457E1A91EF341D12561AD558D1924D29C73BFF0E29AB27EE8AA233ECA9176313D528DB11994CEBCDE4196EC75949BD0FF99336B464333DFC1E50DAC1068E726CE695AA0635BA32AEF54641487915C603B5EA76234715A166C97D5FD8D637890FF6C5E5F68AC5BAE08D309F2BE723B874255219BE796A356997C350E74CF1B0F270EDF26AE1999D20509BF3BF100B6D5991644DAE4B2F3AA32898A53F8BCE297B216F9CD1E4F90546B4D269062BC3728E875F2CE753BF3A1116F2954558EBAD43CCA290D2CA65D230E32876E934066A956D79098B8BD9A245B9A2DDBDA571ACB5EAC101429B42681F7F95F28427C7B47333901392BAF2A9236EB6EE2BF92E36AF56C4F5DACBD33E894837025CB31D5539BD3A56E066B33D12DDD15873F8612FCEFC518F2A4A5372A38BD1D7C317155A5C44067942813AF9DF51C1CD735B068DE66BAC447532D846673368B947256C99CB69B65733B54FDEA339B72AD312DEEA9B31E557960B7A0D50D415036B11367E13D26AD7ED39CC972AB26DF9D66953C55A30B30012ECD53FBF26C9277F2964C37E713B866A727AFB5070C19E197B04F1892C1637333423D2483B0D769FCE8BCBA33881C81C00F3A7DFF0F6C4B1CDACB650FEA0000000049454E44AE426082">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T09:40:12.195+0100" uniqueID="511bc2ec-fecd-4d18-9a95-8414b0a96d8f">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.235+0100" uniqueID="a356af98-1291-4e36-9647-47cf16e74cfb" toElement="684dfca6-88f2-47ca-af25-c127ef7d58c1" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.257+0100" uniqueID="ba0f467e-21a3-47dc-a6b9-5f8d4f036755" toElement="95dd8545-134b-47a5-b9b1-11cc8bf60146" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.284+0100" uniqueID="4778c123-e2eb-4005-b417-da8e2eeb0d32" toElement="294cd77a-950d-4b00-b94c-dfd39e034f3d" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.311+0100" uniqueID="9b1686bb-3945-40f6-b778-4aecc683dc73" toElement="a027a898-0f20-473e-a0aa-25ad6bb7665f" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.333+0100" uniqueID="0ccbf406-1385-44f2-be67-06981dba7241" toElement="1cb60663-ed14-4d87-a38d-d12bb968f391" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T09:40:12.196+0100" uniqueID="f3b8ca1f-2177-45d2-80fe-076969acb258">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <storyboards timeCreated="2015-01-28T20:44:29.315+0100" uniqueID="12c07dfa-16f4-4349-8c8e-f73a4ab6c13c">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <panels timeCreated="2015-01-28T20:44:29.316+0100" uniqueID="caf0682c-e2af-4bf2-83a6-6f1075a83c29" caption="The overall machine." imageData="89504E470D0A1A0A0000000D4948445200000103000000B00802000000849A9CF30000C2EF49444154789CEC7D3168E34AD7B68A5BA8B8858A5BA8D862055BAC20450429224813418A08524490C2821441A430224510298C48134C8A205C049122181706B908284540290C4A13508A805304BCC582B7D8C2450A175BB8B845FEE7CC48B6EC38BB7BEFFB7EDFFF177FF661763C9665593ACF39CF99198D84D7FFFFF73FFE374E1F9BD9533AFE7B8C17C361336C5BFEA91A5D7BEEB1EA1CAAE6B624FE29087F0822FE0982F827959228C892A0C882B1229BEB8AB12AD9DB9AFA513456656345D156046D53746AD889EF9FA983AFA3E68D1DDF59414B760EA4E4410D5A6AD2D5E35BD53F51C29691DC6BCD1BD93D94A3AE9E7435B72A5915317BD0B327DD3B53EB67D1E07B1AB474F744491E3CFF4CF18E55EF4C744F15B322E268FB5FEABD6FA67D205A7BB273AC9A5541FA20CCFCFDC1FF13F535C5AD69E185115E98D195E51E68DE81563F568763DD3D56FC5339BA554723B379A9052DC33E12BD53D9AA0AF816E758AE9F6BD88577AAE91BA2FA59D1B78466073F50A95FAA765532F645E744760E25A7A628CB82B9279A7B3295BBA25DC54E44BD22FAD8B226EB3B82B92B58FB12F6E99D9ADAA6601F28F85488135EB347A3A1F25934F7A5E69563619F87B25111B50D5198B9647F97CAFF67FFDE1EDEFFE307FC3A4EBA61F698F117A31F697C6737AF4DBFA1D6CFAD663BB0F774E92FC604919B9320FE21487F0AB24C4CD096C8FAF515495F91D54F222386E69DA8F6A1816B997DB107DF06D9633FBA536037EEB1943E18D9A319DFA9C185D2FBA2C38652BCBC579BD7AA7F2AFAE74AF645CB9E0C7B4FAC9FCAEEB1ACAE098361EA1E69FE99651F68F69194DC0561CBF12F14A322383532B2C1D7AC7EA1D887C458D88D51918C6D49F9242CFEFBA35C11B56571FCB7ABAF08CE819CDC19FD6F46FAA0BA079259C14F10F40DC9DC03646357F42F8803F8697E4336B714F99360ED4AD957158764EC09CABAA0026B92B22AC89F05983B88641FC8F8813830EB400273CC5DC93952B09975A0D847B2770EDA68565552570432FD1BB779E5052D0B078613E59F58C69612DFD471C2B53549F8F935FCDDBFBF5FC73FC6FF9D5DFDFA0F5F348E6FE2D168F4BFF58DFFF66F4AD1F1A4DEFF16C75D27EC7870AEFD6F697CAF6B2B222C0C6410451E138806CA0749FD2C699F731A589B2A98A02F530531C439529B5770D569F3CA1C8DC6D9737DF0DDED7DD3A35BFDF5EF007E37B8D0FD86923E687157AB9F6ACD2B356C6BCEA102B3706B6AFAACD51B6674EBC0629C2311C1A47E0EFF2AD835C9DC87F54B565536F745B2D17D111F819D9955D9A9593035B84FF744B60EE08CE1CBE59CBBF99FF88616EC8FFDBAF451B32B92047F4CF62A19BB92B5477B86A7C73ED50D415B1761CD369CF4B6A4AE82180AC2085CBEB625C8ABA2BA2A2138983B3836495916CD039402F8696C8BC68E28AF0AFA160E4CB276551CB3B641C786CF7A67B25D13F075E6AE5CBFA8FB172A08D6FB1A65774DB48C7E0CAC1DADFFDC8B3B75A17EEA4557CDEC21496EA3FED71E2278F6980EBEF5CB1754429C7E9FFAB87EF2475C2DE37F8D0CE3F128BA0E86DF07FF7A0FA397FF6B2C4A1F82ECD989BAA6051F79DB4CEF035809A07C243E800B22D345440659A050B02C82000C2493A091DCAA81601276C08151F2182332842D2B7B6E46372E2EE27094243761FF39081A6A74A33855A97EA6655FF5E0D288BB767C5BF76AAAB9A51BDB867F1AD4CF8DA0A540F6780DC5395660346443FBF0FA901F307A01F66AEC501D42C2A4BA087F4C54D9279320675C858DBE114BEFFCC1EBD72F65FAC6AA40565E1161D3FAB61877747D530ADA0A241F6CD7A909A4C40E24635340640BAFD4E01A020976262020E8BB24A51013401B1CBF778ACD4820697010C7368EDC3F971100EB1752D856B535A2965995BC86292F11D9C0F9B06DFAA7B67B64C3F8B3A730B97307DF6261F8351B3CA5C3E76CFCBD3FFCDA1B7DEB0D9E93FE6332FADEEBDD2778373CF78D7595C76EF259A8E03A49CC87E1CAFD45DE4B5992958FCAE86588484DAEFAEFFF71C532061B7E0CFFF5C79B97CDFFE2C1FCA3BFE1B04731ADEB05E71EB48D7F62AB4BA2F2499464B27E3AA57F5092A07E1095BF18139644F5A3A07EC279168C55C5DA509B6D7B3CEEF7BE367978347714BFD1F4CE74FE12671EB122BA962193C63F9C66474EEEC92C9A1D1B2698743DB322F9E76AFA10674F96770C012DD75BB25B839C80E3149D23D9425838241A98553253E80A6D0B19022922F1A36057E1AA617314378C6D3259EC012A05D9057E82F2595057B0B102F39017D1C3A96AF523435BA660C248055648FE254293AC92992A10EE611BD064481AA407DB62BDADD4CF25EF4446CE80DC0394D0C0874DFC0ABDDE425A025D24C30B84E775635F496EE2E85A0D2F158A303B80A0AC882C9DA0E33776906F0861CB876A0ADA56EF29863E44BA957413216E07DE811D779AD1453D6E87D94D33B9A232BAACC75721DE0DCF7CFFC4CD8380984703E92F71C207F98328CB925F7546C3FEF04B6FF02503A306082F2F83FE633A1E0DC1A8C1732F38AD0FBF0D92AB68F0A517B79BBDFB34EB26112A8F597056CFEE526C80581475A2DE632FBBCB46C3211A075F0720D8F0DB9002CEDF4480894925B7F1F8EF775C7B898A83EFC3B7B4F44F82FF8251FF7BB68309817D60C29AAD8A068B81E6CE9D8B483460EA4894FF12CC75E4C7B2BE4401A1489D95E0428BBAEA70D47B65B66F1FDADA3A7CBF179E86F5A3286CC449A7D743FEF0DD486ED5B0A5C65D35BA55EA676ED0D29041EAEB867D88162834056E18A2083E1E3440098B81F6404C800DC12E911240A2A011C28331840CCBDA57DC23081BCA5FD51505C141FA48EE9932D4234A2DC01F0827B00522CA3D2309B44035FD4184C71FBC00FC37A45170052B379C1AA4BF1275152436411B0A0DBE5C01C7207BE868CFA1706430D93D95BC53D1AC280847482D942570589324C8249C07C7A1C3937024C6B6A2EF095083F681E49D29FA26C51C6B9FC498550155048438AF61E134F6C084A453F70FADDE7D945E05493B0007D2AB66AF1B81092958D10E82133738F3263F816775B86C246725CAEAA40FA2B9A6F5EFE3B8554F3A61EF2ECABA51724DD44AAFC1AB66761767B7F1E039EBDDC584FB18EFE22DB4A4B711BDDB8D11854021306744A1291B0F87A0D3EB78040D3746B6FF6DF0FA63D47BC8A24B502803555066F7D1E05B6F34EAF79FB3F88AF8435CFA3A885ACDB49B342FC2B813352FC3FA693DBD4DE3EB387BC810B2FACF83FE97817BE84120015CD19509F6BF430CF7D8A85FB861CB851F519764F92F11D9B0C47265999F58090141B636148402D0C0DAD2B44FA2BDA3EBAB32120084144882F0B21EB5D2F82A8E3A290452F2D81CBF8E92078F1D4C1F89E960688497AA7FA6C2C260C161DB86428041041DA4AD708A7087482BA18E24B47867AA5141022DC25CE415113A44FC242008C84BD44905439797056149103E0A6147D561BB2D28168176881C7A17B92C7408F620A202A115601BD0A32AD62F896C65399D1B524110EF5485C3F61B801A9C6BE1A506F71FDFE91039EEA114B460FD92772C85174409A40AC6A684BC199A2D7ED47D8A691ACB6D44EF58754FD43EB9CE91BEAAA4F7997368B9BB0E05B70352566127C04940F6EC9FBAF0C5DAA6641D2271D7E24EE01C1B02CC3D3873A34B6FFCAD179DFB64C73751DA26236E9EFB4927F0AA26CC772E379832E18380AB888FF41F6204106C99E2E3D7CDE197ACD78DC7C341FF214DAF237C0B59FC1D3810B297113800DA8033D818EF821E1963457A8BCD1043C08A04EDFD8704C8EED11E35CFEBBD871411A6FF94620FAC92F41E127C04F1A7F7980EBFF53328BA6FFDFE53360118858DC73F46D943FAFAF778F8B53F7E19220A0DBEF61173D08240843A2887C0DAFFD2079140BCA88D234CB041D48A606AD97D9676894EBDA75E7A97A2EC3FF7116D480DFE005547C3EF14B5087F8F796F292FDF63487CEB245D1FA1004E11A100C9B12C53C22C51CFA98812EA08B981494CA0E0606DA9F68EE6ECEAD6B60C750E5F8E04B4FFCD82578E1F127CC570E4F6BEFABD6FC883356484BD6F3E7283E84A6976142869480875430C2E5DF93384B80649907D51B07DD8D1486DB734D2480D8B77C2184852B78909D26728183271B4205530285D266E40994071212ED54F0DF754A68418126E9D725F7D1B4905F7B5B27B42BE1CDC40B4718EDF49A63925FE148C2DB179431D5C387EFCB4DE9396DCA9BD2773F86222DB499FD4A0A33955455BA6A0C102940CDF1FDDAA5041F17DE8D7ECF052EF7DCDE2EB109703FE2EBAF15D8A270EAE29B264A4D738EDDE215282410AABEB46DABAA46D0AFABA506F784ED510E0EFCD4D0DE7DA58516094A3AF59745E8F2F3C700086086ED48F1DD828757897439B54640B1F047B430F4F3CD86570E290BF6F232C24712BC0DEF041682DEC273CF3106AA0BEFA8F19B6C4BB88153D224633EE84A4CA282CA4A00D9548D9BFF4D0C2CD9DC4D5978CC51698268514583F3ED87BA09D8333D8DB00067A832093B29694530BDB1020F6EE12BF061D1978476E7203CB0E714682333FBEC206A4D6508E8683E605B2F03EBE94B7E000C02E22F003B1ABF744241C7D1FE05D6C4CB18BF52B0CBEF48960DF07E018F1EAB9474AFD89A40B8884ED41BCAC9BBEFE18835D086BD8BED9AA6363B8221040FD2491AD33EB878FC485503FA245D03E8BD042C81320878C5559FB2C50BABCA37947708A72F3C60C3B3AF5C69E5BFDAFA6B12E275D7DF4120F5E8CD1C81A7CA75102B3228306CE81043B30B6247B5FA60B87C07288645183CC48BA76F6458573F54E891BC983AB6D48241ED64505D6BF0321AE21013077246505E9818916F7D882D5C2AA608BA80497F820F5D8C0A34390D88722549675209338A14E1E010289328A03EAEE84A01217F5BC949D2C845678A3C60F3A84FEE09B3E7E758643D3AFA9CEAE4A5E63159931F20AEAEF42DC00F170249061D973145E84E3BF5F931B0B5ECC3BD5BD734442B379E5DA8788753A760E69277F12CD0D15FADCAD6A5047CD73CFABE949B7191CDBEAA6C83AAD91902D4B0008A7AD8894A5ADC906242948B30C6FA4989B0AAED90C13C45CD72249B2B775F866EFC0187E4DE38ED3BCF49B0DCF5A57FA8F91B76F232C30BB4C21C59846EAC1AFE368D26B586ACCB385E1973EFC7A9FD28324E944C3AF8366231C7D1F36CF0234C66D18650240ED44AD10405481E706379A0DCA6DC01F840B502238F1100410CA1034F02E0F23C36F3D38008A30B71162223CBDBBEF203F819482B9839930EBE83218BDF4C10D6435A4D01061586C019DC6A301E8F4FA6308FE601B10039B60FF20093E88B750E2B0B13158849C07E105C0CFA148351AD2E13D655CF8619BB41BA304377012B287A6C8520299797D599EA6072A759E4A4812101668286D8DD2035C45630D125C863CD037A90331BE47C20047680C5FEA09DC565785BC4E1EB4F84E7D7DB5FACF66FAA8D42F74D80A640FBCB273A8081269FADE37BDD9569A375A7463046D237BB2D108450E99018F6BD724F7C868DEE8D6AEE8D2E0945CBFB4C28EA7AFEBD9430F09313800878A4C14D9AD7520B08E261245945AEC520420297FA2407B50AFFFA18C1862EE80EBC42EB01D5BC2E2178607FE1FD26E0830E4CA90F2C873E0179C7D29BC52210BF535E4C752B32B538FF03E58ADE02B9003389046A760BE1B5EDAD09CF42BAAD846C70F473E494375EB82B284936625370E54B777A0C737BEB625F4BF64129D13923762DE73F7815D920FD447016EC81FE9A5C4FAF5C8E2A5120DFE2C98805F274BCA47C61F1CE23AF5767B5523E920CE28EE81EE1D1A7ECDF28FCCFA31950008E372540D842456375162B3FA89553FB3833307885A7EF3D20BCF5D56AF476DDF3FB2E04421E4A07698BD66F0CA64972F30C11ECC14D686972C67A02090319B23823DE3DD26545342DC8B9A8D00BE1F1F8FDA08A91E364346048AA2D2BC447B1AB742EC1E4A2FCF5B861425504130E93DD26EF917E1DB4130A24137CAB77C21862022711691AE7B424A93C2E8C1467C238209B5DCA78C63D05A169CE44467524524264C47D396693C81B02CD230C23A125C09B201E4EC7FF7EB17A67B6220BB85A18757DAE07B2F6283CACD8E1ADF92761A0C71B1E11A0DE92F1196A7ADCB94E541876CCBC3E1C06FC841CBF41B7ADC357ACF517F683B07303507860B714F19EAB56EEE926543DEE8DB70EA567059770E4DEB00BA5F5256C9B6907AD25B47225258FF82FAEFFD86C6BA41A9CF87F284031AF6A23E2804A59A8C168A158734600CF2200381AD973D2C33B33C8180B28795C358C32BC83C3BBDD7C058FCA2FA858AF0025DD4FF1A7AA70A5C3E0E9834D8911A34DCB41BD894D3CBFE85155E19D69ED1FB92D5CF154E57FF1CA985895F8A9386E8177468041A078943824E4540A0BE0B58FF0C3E113760E5FCE54C8FD81FA58BF7170B299FC88D29C04711B99D5BD591C8532E88E88F0BB924410D6B2B1201C1674926E7F7117519F5BCA48A828AF2495697417D455B25E8BC5C03546CA0C13E585D5F07141D5E735D31B734B458DB9AB94DA5BDABDBBB9AB3AF3B8C6CCEBE09D611F1F60CF7D0F4C1C9531BA57B60C170C10A2421A4D91E531083DCF94D82B43B6E35216C82131FD116E69B5E235749291CDDC6F491BB2464E22A3CAF23F8840D64AEA4FD58053B4C28A5794A193F917E3449E33DE47DD383AF485DFAC36133681838AB93FE3749CAC795F9940AD64D0432B061B56DF817DDDC90FADFC3E6A53A7809930703DA06BA25EA9AC4848E327E8DC24BBFFFDD0213EC8A943D1AE9B306790D9724311F47F283E6719056198E86D02D30594ABEEF4C888DF17834861681633A3182960E790332F05E23735F82FA776A8A8B947A8FBA954C460F3E16665674F70461075901920464DB945438C7343A6650DFAB4029F8061439F53BD1061B92BA4A5345B02B041F6E50DA1AED6A1A28FE104BA355E4CB91A09B3B44ADE84AA703AB52C6923D051069F8BAE0D243CE9DDE85CEB61C5D7B3836D04FDD12918921C94604D337599E5351FC9A811FA257E8A7D168C60A1D3042198D87304B15F455945401312049D5CF8C21A82FB1FA67A2447E647FE4EA2827834C860E11A5AFD01EA8B36F5D8EDB75F64131DFC9B2C840EA0B9BE9AB920EE14BC498D2635A5992F34A196859223D4DEF2ED1BC03C628A21997DA02F5C7C9AC05324FA6122F976596D3E3E34A0E4639C63764A892BD6778077608BD786451743A44697A9C2D0CF563DBAB99F5133B68387837BCF0903545ED7AFDD489AF82A0E1253761D40E104C1031A24E98DEC5F563BF791EC457C8616234821BA00DC8030A419B212881635080BD673F38B578D42569F4179F65242A1FC462108DC840FDA74B942A20D2866D2B7D8C7BCFE96064265D63F8C31E7E4F838605834E915F76A188FAF0C4C3171DB966F34AF50E25EA3F6DEB7CD60393E122F25D182E6C25BA7568E3B10F21E19FCBC19542D9CE4B0F5615B4286DB0D9841CEF14B1C2C657C09840217D474228307669AC0DA64C36BA4DE36BF54B05F1A17EAEF10002278D8421BA5391A96BD4D543DE1D7B00076828AD429E9EC6980FC91C7178CA122B5769446F915C62B53F04EC30EB9A50836685F48C5D1521A2AC5DC93E50ED2319FB8C6F1D8D0522B017EDC61E1C254DE8D0897EA2B62334DB1662207E38C9B94D01820A128BB2888A043325989BF03D82B602839E83C881B831CD6C8A51363E320AF10A518B525F232B37D7E466CB03B50A20EBC0BB12B28E02F22CF8BB54675BF24A1912238F646C28BCC2919387C0C893B3EB2D910ADA2C33AA30B68009006208220C98606EA9C606723211A549B145B37674ABA2B3F0A28330F69E696C6ACE9E8908C360A1111B98DBBAB5AD9B5BBAB563A042253E957FD0E01FC7DE48071E1AE4710F4D172AF1C4AE376C7C3522278C53623DD3D29F221F53A33CE113D19B131EFEC5C6F16CAA908EE3D761F3CA1F8CEDC1D0ED7F0D062F66BDA121F403D9B353A7DE55B5FFCD913F4B3EE8718F1F22621B8801A473B86A2AC2CB81AA6F2A4E4DF74EACA41B0E8651F3CA896EBDA41B3B07105459FF6B868C3FBCA8E3B7345B61F3BA9E3E86489D49F0900231B8F84104000168FADA8E1CB615E710C9B464EE22CD90D8043831E848F99CA25D093E1E9687684006BA4F1DB5D4F7BA257A0D29B8D0B081C8669AB021300A2CBDAF9A5BA3B139FEC7D487C86821227A34AF150F39FA994A63797FB280C3F27B6B47716B707F12270EC285BD4F23E2C616CE32CD59322A527415E0B334204893F664B005D1C96F98FEA14DD100166F6DC3C88492F9CE0056AEAD8A652608796A01864826F2A10D566E92A1DB5B6A74E553CB466EFAA019606DFD0226950A87C9D2F4023287B3A7E5F50DD9D8C8CB32A9CC0D65AE85C71FB08BBA0156395B1818379C7D0D1CC8EE237C8B5581ACD2AC8A6AEF726836D5A9C5DA51F312D8E6C786AF568C75F0079C31CD2DDBAFD5C3F366D48EE3EB24B94DD3FB1E924BA0F7D4CB1E51F67B348E31EC7F190CBE0E07DFA9D775F8321A0E47A39731817A605FA14FC66C98988F0CF2FAF80736180DBF0DB3FB8CE6C23C44D19597DE47BD2F6EFAA8C32942DB20816E5E59C6BE907DD77ACF7A78A185E71ACDB4BBD787230DBF14970C4A12611C5ED9D851EB9776B3AB537279652677F5DE73BF0F59F8D0EC7F8D5E5FF3814867CF437DF07D001D12B6DDE6956B56E47A4B613D420AAC9C26FF5441062834BD796146373A48826042DFB5C988B187EC56844642A202970C1F0C874DBA6817E62ED300F036F1C1D811D9B0345EB2210BB8834F385AD1AE4A418B460F0A419E4B261A84BE96D3470B42B1F7C54042EB5F2AD04E341382F50091DAC41ED6C8C1504ECBFA39F182469DD7991C4272BF22286B94E78096D61E4D5E12C8A3AF89F60EAEABF8136017B9749B3081757A908ADD92C894B7794572B6D5A8E37362E40CA1770BEC304C5E6E2B3F4569832D05F9F71C73CC3297C846593B67CB5641A10D4627325C460F1E679804474EEF54759246559DC33BD059A2AFF3B7DC7D83C33BB4EB275EF3324C6E92ECAED77B1CF49E06BDC73E99F8633F0360F7F7253C94F03821431F4C207C1D0CBE111F08541982120CACF242188DC60CF44764188DF3F18A1F340C38FE31622479E5FCA1018D9761FFB11F21696979F5860A0DE01DAB414BF60E550865649FC68E428602F3ADA85E038EDC198D06F19D3B44A2FF35E55357FA5F7AD821FF533FEB7DEA101BA6F70330A1F7A5179C079052F50B3BBCF4AC7DC13FB5686638ECF2468AEF119A28EBF04EE4ECAB81D0049F4DFCBCD121A806DF6D32CD1D2286BEC1B25B44C86311898DD7D0402A9A887AAE42AF53AFE82175E3C6779AB129060D3DBA51834B48209939FA7CB22E92D8F489A697D72F70C5115854A72A7105E59DD0DC6F1C0F76CB271A99BB32EC5E60933B404EE12F8186113604EA20AEC8DA9A82D2D8540466B2A2BD0B33223BCE2D7853B24A15C0D8288D8C889361359044B6C181EDC2BE7764675B6BB63D70002FE9E38C06609A5D619854CA2FCBEFEE4C2B560EBE73C53F36F2FA3C9D263463DB6FCF94262749C18A291F56240FB1FEC8844AA99F5A407086D20E1B2EA47F7213C3E2733B7E62B6FEC4EC9E19F4A4259BD0E0913666A120CBF138C70186AF7DA2C177C6849C03FC8F06BCE9EF078F0F631E1F68089C070A36DB97DEA5D47654BC3B2A88412165C4DB472CBC7022BDE02B7AC159D3DE758D2D9A3CD6BC769A9D7AD4D5062F99DFD092BB387DCC86C33E2F7BCF49F2E8062D376807D207A9D98ED28734BA8EC77FBFFA0DB4DBCE296489EFD454A811BB467DACEE91341EEBE93D4DA7434C08AF284CF5BFD9A08485BC769D7A51DD739A9864EC0806753A9127A6A9DD7B943A430B9187DEE2BD5BE4FE91D136AF7575490C5B4427BFA1B24E529A4C5A38E252F6205127A704A1BF69802D344BF790260BBA6752F2A8D3A0C71F4C17ED8AD88C642864D8074A4888184B02B20BB2D56D49DB021340806D094AC0E2064DE514F4EE0ED10EAAA38809220F0B7C8A15DC2D5844C88D5BC6AE1013F029D43937D0EEEC71A80EDF788F8097930AD5E9AD8938518A962960AC36572F939230F9EAD996092B4A986AAD0D8405B81C07B9AF5BB593EB38EDA619CDE320CB869221974FC86DBD5CA1779F790B4C3F8B6F92A81305E734ADC3AB79EEA1EB1E38CEBE63EFD9CE9ECDCB85986CB00816AF38FBB65375B043EFC8F54F7C78E5A843734968E6C8D73E2201F1E06F36E5FBEF31FD3116F178327C61188E29CE7C1FE5CEFEEF571A647D6C0EBEF7DD63257968863465DFB30F8CECB9691FAA5615F9343CABAB2CC93452CEB4192ACD8E1F5CE9F8C9D877D0F0E1BF8723FC5E19CA24BE51AC3DC93BA51EFDF05A03CDFC738984F81E4D38853F8640D7B7F2C9DE2C2D9695159AC48A9266C8ADB3291B24A88CE0A20E13835C41CA0A3BF68E159A21774483CA5059DE89569AB241D68C43B50F6918207DD2E2AE6EB199DEDEA902DD05CAD178398CF050F48E6DE903EFEC11F98D50145896298947B0327715EF4C13C85E7748829311932796273037C41C9BC8DEA67982C8C0D3657B5B7176D5020A19FAB612B5DCBC4EC4905171F754C23E954E61FA4E5E2F50DA097BA938331C53EAC7E68415D880EF9CC3A2722EDABC11605B8A41C10D15CD3FF4E0F0929B34B9CD48CC14CE9B552616CFF044CE1EE61E5E86B045B7EA90F9EE5AF62E2BB9B14EEC7B7FD6DCF7CB70F2CADC5B7B3FF9C89BCF72546D1C06085CDA8985468F8DA3B339233D8825365D9753625480B7E4D20BF4E87F1D2246851761FF254CEEBDF1DF23F7D0E273FB40696C8C5FDD6C07D853FA1086340FD4C47EEC7D455F5783B639F8EED9FB305F05D936ECD5A739D23A2286774223DC488EAD7D246050E1B063C5AE6A6645913F491275E9227FD08C6D555BA3113AEFCCA56E89AA022386D6D0D664A72ADB7B2A2286472368D4FB24FF2522454E9F0C737BDA7903B54637E8507F28E41F0D24F33B1CB03D0C5DA33C449A19C52BCD7D12D90C09367E40998C4046034BDD071394E64510B5A21262A48094055EC1EDA1823241468897F1551A5FA709C74DCA4D2AEDD2E41CCA17EF7AE92DABA0E58EB5A3E56E526619A147B87F4F5EF7E750887266B20CD92C7A73989A35A5AD5CD8E0C2E3F8F15BE857DC24098EB69BD2F13C646C03D236F155543FABC30D33732FB037A9701A588C0F84B76E7EDEB2DFBEDC5FC484852FDF2546816A411254AAF60CF6E920695E492388DA4D683362C0DFE35C65B1098848D669CADA0F620529B4E12B28C49316A2CA7054E42AC5F6234A6658969F7F1C0A8DB593641BD11EB82AA30D984ECBA739E609CF88273C79CB78F4CA371BA1425F8697F86E46DD6F3C951A0E5837030E12C78F289DDDF7796F447A0703C345A44B1971FBECC4CD76DC6CC5612B0A2F0941039732AC374294EEBECD09E09F4286206A69EEA14AA31CDB9277ACFB87BAC09DAE7B404CE83DF4C8E0C8E64A19214B0A8B1C713079994B64666A64AC8F85053F4EEC78CEB2F332E5B86778C8FE19EE7BEF61864E0BD9F5D8C339E562869F4146DA8C9F5634FA271E0CC8AE58845D3BF7FA33B0E7B1579425A9E3CC96F31C784B895FC784390EFC749BEA1B147BAE9FFAE0399F354811230F0B03182E310156F8C2721666E2390D0A8B1FBD8C8AB49E936148764FDB8F27F59C27C311CFE087D3C651DEC8D3A10946FC8B867C033A86EF030079D41034F83A64332307FD676690E4E0681A32BC15AE1A68000F1BDF82063415B7091A80009C03178C06174D302168344103FF34F06B3682407C534FEE35651929BBD4EC283459A322056D2BBCB4042E51BC2A63C2FD809929D971CEBC02DCB8D31CBCE5ADF3EEA5B3DD26E59725ABCD184A7C98AB4FB729B6BC5FC881AC7478D3434D67183803A81D04019C3BD020BC08DC4372FCD68EC94CFF7D10376CA762CFF32117F4D644D64FE0ECBE090EBF89F7EC7BCF59F4AE33C3903DDB2D879ABC6EBDDD1B8F72C83DE2EB886245915AF07C3D47D1325E84F7DA7F1F33DFC2FF183D78E7F2F0FB90772A50346098E466CC963819D2F8269DC40422433B9A060430E1BC490181C504BFE1841D23BA729078583B62BDA146D776FD42A13B317604755B14489AEFD3603E44797A0BC1406C63486741728244C50C62C6C808473045AB49B80C438E0B8E0040C2476804C1590EE890199C023EE1640A9FE318F06650F3BCF770E44E40292C83C7725973DB30B7A0350D102047C5B2781C5800DBAEBC8D0C3351C2791B251661414A909BBE358B9FC701E7FDB06015A2689603F391C79A0064600ACAE2EEE05DE05C6D9B3976F20A9DC6595893FA5609732F81CDBC34A6D0A7D858847582BEAECD602D87B6CAB0C2A1AA1CCB25AC28CA921234741A26AF4A71DBD73785E8DA746B6AEFB91E779CFA991A5C6A02B25884059A185745B6CAA7EE4C7F1B0D9A2E40E93455662B9519D853583FF7BBEFF8487ECD7EC38FBED738AF1C1C3610461723BFAE33BFE52D138AE35FC481A2820D0CBBA2D3B0F4B68E74DCDAD2736CEA26419B6283E16DCB5C63010BD852ED6DC5DE56ED1D768B424573F60C06B3C41C8782C68280508A09251AE469CF1E3FF33919FE31DE751F39ACBC2C99C40CC1CA30ACB75CCACF1E618627EB9A314306552732A8849502CB0A032AAABD67660F71D87090C71B9BE278D4F46B7ADCF19C231A29F71B48F769BC5C70AB1A40234AAC4253B7B7E98EEFDC7196FDC15B2C74216F98C0EAA573B4BBD0B0DEE3C66F30E11F02075390C1F82919CC193EEC183044B248604BB1B7647B5B76B6693885EA5BACDC94AD4DC5DA54AD37063D5779CFF4673F85FDA8D867BEF3FC5B24A700BDDCE6600753518927BB6CAEE1CC792B1160C2813DCE61FAD5FFC0BE176DC04F5DE95A17F505E1A5CC81C2FAB7DEC68D120136666305A7C15A099C062B6FB0AC2A9FE466CB1F0E910A22A9709B57AA4F335368611896374BD1B561EDA26ED07C7566FD6C6CB5CAC8708097D42B6F6E697974A3A935F91117B1D2C87F09FDBC72A078133166B951D8963973667F89BDA9F0C8B352DE7D39D5E856B93E87B211387B5CCC949940304B47CB2C5E6716AF700B9B07334A87C12E4A0EA2C106DD4830EFFE377E13EACC4B62027885DDCA6526D8A56F2C5A66C04822163C5110AF9C8AE1E40EC82EFAC14AE1EE1D1367FC37678CBEE4DDE8AD12CA8E7FA67D67CEFA0B13DA9AC17C10D8D0CD597534CF81554065A00AB97F2280563041C136F155105FFBC36FB484827BA07AFBA65D15AD7DB18984A1ED226D08AF34A4CEE60E4DA9128A5906C6940F077CAE01BE586164D0391FA661ABDC52307B3E7ABCA5C4BC649A7533FCDAFC3A5C98537FF67B419C25AF850028BA7AE808B74C8B93615BB7B7989B9F3A78F98DB5BD6D2C61934703024DEEE064F8A5DDAF032A2B7F428C820939A49F10A00038203A9B93D02172D89B227D9CF640F7BED104C13DB338E7ECBAEC2EC09CDCFD05A697BB9C78307793A7166609A53483E70F9B3C7F280701ADA0014F15F47234D00A51A4AFEADCFA19481A05E77E7C1526D7F1EBDFAFA3E1A0F7140FBFA710FFF0FDF596DAFB128D46597869D19D7A77CDF1CB38BB770A261CE64CE06460D36F4832E9EB92C9E75AB232AFF028C1EAB328E20323865DC6CEAF50F917D07F0B3B0C7945CB8174884D529AB521E6E3B9CDBDEF74697A1537A9025641036BA3CC0475D6E8E72DDE28D380EA341F768E09D6461116B6F2695D3FE500C326AF08795860C4C841475BA2C4FBE0BF31FFC6B9773726BF7A161BA5CA86CC2059EBFCA564AE8B266C69AD84F502450B4D495ECD4BB6E69F584C6166139637CA7399F9047E9A563C9973A9AF8AEA27D9DA356958E4753C1AD25442F750612BDF8CD3BBC83FB1F5753647B5AAF7BF64A36F63F758F24ECCE1D7CCAF29F34CC8F9504C4743ACA0699B9FF11D6CA62ACDBBA63BD4349A432FE61393B6F8D40CD9DE9108DB74BA9D4D71E28D18248737E65722BF18CE5619B3DBFF5BD8EFB733975F1834BF4EB317C3A489ABEC7A702CCB0CE58AC4EF19E0D04A75FD33206BB4701DBBCB6249A109E1EC8E089D6686F3BB914A9549F91E3EB3CF2E89062DFE3529CB6B8189FC904A100DBECD12AF0BC68A9037AE700845453456456B835D8B29557E0DAB80BD41B0DE00466F016BB3258359C66A81950904931F1E7EE012035E82429B256C88C6069B12BA2CA89FE8FE3E9A13B4492DB22CB80776FF6B8FCD2A19234BA69B72BFD3CCC2D1CB38BCF08373B77EECE8389E5D29A67B0F7BF10DDD170901A96F20638610DA9F8D097C3E26BBF71210045ADB4B5F95E50F6CC5A5359A8ECB2EB690CFCFE33397B6D91C8D6DE6BD367EEB9CFE269CB25BDA9A96CE8C0B9C758AE5ED673693E776E5EDA91E9F09C22ADEBEE6ED31EC6B797D7F52D7BDFD5FC3DDD3DD5D94E6AF60FDC6368462CFD323F1AB84720B3516ED047AA92E06FF997B2A19E886343DCF851D93AD974CDCDE7863F1EBEF4160462F14A62FF092D93D2B578519AC14E50AA32B55187597083AC7329B2BBD51A2C1BAC0402401136499667DD3F45051082E3CFB500D3AD6F0A5F9FA778F2F7533FA11F3A9B5836F03874CDAD4370598F1E0658050E1D75C9A52D5B2BD1383C5847D1D128A57A8BEA771F06C81A667FC45F7169A5B8AF807DD80866FB5B775F58380239BA101C50499B8B1CE4E5F7E8A0586E909B5DF60E6A4FF163D7E1ADC377E1DFDED3C881313A60674A0CFE390E16DFB14C61CBC2AC381E9554D2A09D66FC32CCAE2E355B3381203A833F80BA0D77F03C56FA11F5BF6E2BF67EBC282C6B53204B27B183795DCEEDF587F61FA13EBCFA316AF70300E689F05ED1323C32AD93DC5810D56613711D03D64CB3413091A92EEBE1004BBAAD152317B744F69BDA52313180EFBE3718FAD38C862C4CBA877DF0B4E3DBA01751D4973B3F7406B366737A973A4D2AC557B87E6BD991B129860EFF0E9A26C66684561B3E55499ADCEC9A72E593B0A4AA435EE9EA14882B5454C2529B9C367C2913AA2F8488E819DB88DF74FE27B985C8C4591B700B7635CBFDCA00B793A79EB1FC0C1AFAEA8CECE6F603B87FD1E9079B3AE4F1A5598EB365DF4720136CA7D4D946CB03C4199A6229B72A1C267EB5C976FCAEFFED2F5E2E4400DAEFEEA2ACC99F87A6EE8539DB35AC69CB98B6FED7E81F52F4F01B3D6974AF89C3381C8B03261020504E873BACB92850B913CB226FE29D8FB3A2DDF5DE1CB3F8AC63EDDBB93DCD9D983877CA058726A3CFC364AAE42D8BDB5A37A47567213D31218FBB27BA4D12C575AE9681546ACF935BACFBD7E8CB2A8B016BC449D6E09677FD63662859BDD35E53FF319DD4406DEAB5D613358D7498CB280285865AC4E1C0681FB8FDC9130E1383DF5137DB93EDBF8936BF6FB4C2B7F703D4F15AC35661F6B79658249563793519453BD7576C3719E66C8946610147DE5BF01B61F5AEEA548632C8672DE69955056E1D6FB201A70535EB471FE2E7F5918BAB5F6BE8F7FE3EF739D33B1FB9579BBCFAD7F69113E331A300ED06AB01FC8E873264011ADB19B8D57880FEA679A589ADD34CD5D956E7CA3E5B5055A5DA6A6042DDD3E94C28E16B6F5E14BD2FFC66EC4A319E6A3C1738A0AEB14555ED9FD1BCEAEEC1C2BCE819233012CE104A0B266E538B626757623299BCBFA2704953DFCD693FEA0F5DB8C0D4606B8E76D89AB23643CF8498B9DC44AA10E17437C0B6B925DADCD63C1352E950B52B4775A58E36CB7C6AAF4EB9649FBEAA4225392BDC217A460A6BC4CD096A99EF7F12D17256F5C7E0FB29E6F40CBA1F2F49D7DC59B5334EF9EFF67B0327BEDF29713732F72F1E505FEFE5DBBFF3C03AD1407721A14CBACF0540101812D1C417A495D664B818842D68DCD5DBA1BC1D8119DAAE4D514F7508683A6E7309C69C1253DA8C53BD5A3AEC325D22B5B9D50FD44B72ED01AA02F63BAB3674F363755F64D60C2F6840965D839198E2DB1B83D41FA5374F69DF18F216282424B4B0866D1B148916187AD0CF089F7002C3E29D3F395FB8FE9999D09A6BF60CB62E6FCCF03462972024CBB985672B045DE69650AFD5D1367589AAD2F4D16DD28DEE22D3925A452CFCF22B0EE1763A5DC093379C9CFA4382B54CADB2C6C178C39A7BEBC48D07355C33027728CB746BFF4C6E8674D7FCA81221AA8B4D0285B3A7F8DC2025F81057C80C9D1AA4D5BB41042EF3EB6F73573872D23B047B744D372C287B25B93AD8A88767585D62DAE9F69F5062D9A149CEBBD67DF584210A0E7F444B736228C77A6C45D8B98C057D4998909B9469A7263727F0398E0EEBBE093F4A7C05791A0D5CC91225758AE8C635A66F26E55661D610B9C01750E94D2A399970B0853BE247346301B7F7F622BBF01F39F6DCFFB28677A30F39E4DD6B50A5356393E17E562C8EFB62FF177A95B96759BCEC098807B9CB796BAF2C6E94CCFAA9877B02E2F366BDE69332D7F13252B2F9B7B59F02CC047028F00DCFA794561CF1322267C60416089141168C017153736457585DEEDDD27C68ECC33048D2D2BC69EAB40EBE459BB1A5B61896E65B62A8A7DA0D1B3BC6A4A78E9B8072A3D93EA50B25900B1F6A4B41B1305351A19504AD63F0B5A9DCE9EDEE9230AEE9E434C90705822F26C6D4546566DAC4B905CD6B64CEB23D13E7109457D8EF7A55333C5CFCFEFCC1512671AE7DE9D6262310BDE32DE5815ED76EEE5046FB72FDACB9B69BCFCCCD68C620BB4A89F1485204FF1519E7939699CE0ED4BD682BDF195F0748E25B174D2668FF6F7ADF6F7CC7A817AF9A93BFF09D48F254C2C7E522F4C1F5079A558038AAD8DC912E515BE3423759B9A6CD925F983D47B48F44D7ADA95BE49EB1D997B32DDA75611AD2348265AB60C248110F24EE91953E69EE09EA8C1A59DDD0E49D5EF49082CF0E6418BFA52E9D154A470D665983B326394A598502C80556202DD32B7E34062C932AD78E7566985397D9DD6D8A235322A32F1F8AFFC19610AFFC1F2F407E73FBE3829DA1462FE92F90975E1D99C6B797B49F8DA899FDE5E36B1C09BCFE6EFCEBE2CEFAD8C4F8BA17E64600B39D23DE6B2A44E62C204CB6F5ADEB6CF6F43CB01F2659895B2F5D077B1F263A125F293434BCFB39762D192BF35838FEF412C4A71C66A3FCE5B6D194A716C13537E0B99E3AFA232A94FCCBD0CF6702DBE36263DAEE94F5A3F4E5B6516F5993F3381160AA065663E0960020C58A785F768E5227A04DB8AA06E129435B66C6B959EBE8350E09FD184D3F8A63EF84EFDAAF59A6757E981853450FD49F061E17655A5653656155A8EEAD8A99F38C48113ABA0449E30946E0515EC8AF5FAF718E70B81DB3D34D98A8B86B1A9D21DA8104B7F91594B1F247AD60E5B600B8E0DF4CDF1112F8B76C2024FA9BE79F93E949FE1B3A2019FE41C9F996D15E023B8E5D1DCBC52823E0B2D573E6F91F304E6424BD94D56DD2BE5C7797D1EEAB4BE5AC2B45155658A33F960F672092C27C993935994539729CA59CD2A5B7878AEBEBA6057FACA9BAF589EA443A50AC3CC095992261E449D5472AE327C12D95AA3225F2A9CC09EB295831629A2A70AA98C39480C68E2CF8E44EB1AB19E225AD67755E93FA670FCB07B8A0064FD080892BA29EAB4001EA5C2B434E589422B9FEE81094A7069A50FF438A5FE7D1FEE9B1EC5F0815693AF575D7A448A4DCBD243E26B34176D97E6F338FBBACB8687DC03C33BA40512DF30E1953D2055E26BFDB255472193241CB4B526194BF8855C348BF9D400B21521D71B2CC9234C2A13ACE59505C3F26F3B49583FB7B92E98E5814CDE7BBBFE1BD8580C7B82F5527D767C700178B7EC0A57BDE2D4CA5795E93CE155757E1A7DBE8DCA08C0DF55F229966B040DE567C15AA5FDF32F728A6F7436F37201B6FE632CDCEDE6F44B677F3B3F57EC2019DE8CC7B1134E3DE9A2B54AFDE9D66C37549E314EC4188B6F24BFFFA4F3492BB27CA0B553AD5D114092003E2074C0118309F4F4135CB83D5AFC42DB14B4626D56FEC44462C80E89227A16568516E0A85F585034C3C72139EE2522DBF8F5356E87ECB940BB92BD8B0B46E6CB9E74447147FE08E90FB74AD33926CB038B6C35637BC77EFDF1AAD26AF4B4222A17152AFB88BEA6787BAAB98C58E198EB74CADC2DC1D920E004391BBC4574B709DEF6B43EC5D61BBCD7BE08CEA4BEC930A93038389832360BCCBC7CDFB6A8FE761BF692DBC41A2E27B970B68071196CFA6471BF9551C6C62C36A7A021B62DDD5CC66E453A879BF9D92B9BECFCD99B606702A954FF27786FCF05FE117F9C8D1C13C7811F95033C2FE861B12E2CAAC081C2023FB13E6EF2F73C3D60A890BE87F921226577B17BAC91BFDF57942541DD9064B6A80C5B9E953F309756383668157BC3D855BD33DD6FE8A31FFDB413D30A8E149A04B766F59F23813F135B96E797C11005D270F38DB4AC398B09E357A42F2A17D67C21612E4CA1B20E357B43F28E5C6B43F48A734AA7896820BCB94E33F0FED12529BF4B362130CC32E727342B5DD1192EF1460E7E81B70567D232FBD66457DC0E1037988793C8823775768F873E059BC65B7EC927F09AE57B032BC6ECBD7E267C2A0E807E6985E071EC4A0C22614FF281FD0255A94E90EB07CA0CA00D0E5594B3ED32B6C44708933DEC491E435111F372B704761893A3722BB3D76BD1B55BC8993CBE11318409F86036F5E62D513CD137E8D158C60E2D8A6A6C092C2648F553535B16FB0F99B12DC8AB82C29E9040CB77B30589516A8C09141F2A54D7B7256543701B3431BBF7A59EDE383463EF881E93D56C390E5FD80E9E1EA9B434FBBC136A5C5326C3081326504CD8329127143925D1C0DE51789791B94DB74DC02F22B570B6651F570B21A9323D29DEF45ABE4165E62CFF33ECBC79B910A5B7E618B810BF45D4D225A7DB00D628B8ABC540819AAF865F02EB1B9DBCE48984BA54942B248D26DA099104C10A46EFEFCB64DF30DC02012CFB0850C39A1A1E6BCD139D706A10CE4C206A00D60CCE675EF2CD9A6746FEA9133D3CD1B0AB1C358DF6CC10004700E844DFCBCAC99170E211DE304A64A08AC731A15065E6A44DE3C6841B1B1425B4BFA87FD2DC51F54DB688EA260C8C1E4D6BC0E36CD3A2B7FD87B47E6EFA2706F593EE6BF212ADA8476BC16FC80A3DD944D068DD55F6CCAB5DE40C74FF7ED0D16841B113D9A5258455AF661B9FD8A2603CF747A091FECC7DBFCC16B793593788587E4ADC1FF90360AC2DE3753CA61EE8559606140B6BE71AE9038536FC5476E508FE9EC02841A7809F177F5FE427ABFE0E8A93F8CFC089E4FF36BCA2FCF7249CF591A00AE21E9C99BD673AECBEA27C59AEBDFC867D9716EDB2DD03C73DB02120BDA31C7ECDF58FBDFA4981533F3803EA41A30E7F3C31F7B06CEEA746746672FB8E2F009B70E9242D2769BB5374006F06BCBD55942D079F22F03DD0AE1867A6E0A402F08DA0194A1C0300FA311CE3D82650C25A8EE04826106D24801B43BDCAAF7EE99215A78E079349DCE04C90F8CABE5BECD1241B9424D87BB42811AD9DBEAE8009F22701E7999EC009BB5F93F41D1901415EA275F544B8A44DD1BFD4FC53C33DD27496480457BA772A393599164BDE12BD335AE69D9E9F40C9384DEF96F267267C62533B96C4FCB1099F4A4982983FEA0271FFF5C798721D2443B40CB04853F1287345B22BD46B9AB32ED4F7A4F0480C6B7248E7223F05F57D913B8FE0E0DFA25A8638A9D3CEABD27F8A9C87E20413C6E6176F21F64A155CD76D8AEC64E8C57243DCEE3DD83DC7115BDAB1C6705C80AC9F085007011AF5F01C1CF0C3F300153860667C060C313E37E3DCEEADF8D24E5A36B37B3B693B840EB77B37BDF2D26B9F80CAA43E8547E06F5DB9297DC4C9F740BB62BB9D5418E24B2B077DB549A02361DC207AE80534801842509B271C0AD1E358CE8DE1885811102B4476D5C43A3FBD7B25AFC42206A4A6B52CB8C87A3768696B2822A823EA30DD82B297DD43A45560426254947AC3F34E0D64081AE509ECC1A1EBD4A3AA6D81188AB94B0F01A20ED3AD5C3E212098104EEB62FD4CD1D769D210AD404C230E159A3C67ACD18040BE76FC1A0D71B325E385F27AF1EC41609C09AFE62ABF535664D38D44BB22393B34ED82CEC5A1D43C91A253A9792C356B52740256E4BF1F95F09095FF2D1CFE2EE8DB9973E24792973F45BD28797C9BD6ABD30DF2F68942D8453A28B987E4F239CA1C2808E0C1FA4B1180404100D67F510FCE890940932D8D03E70A7FCCAD3F6959533365044889004ECAAC3FBB06FC1C3725A07DEE2503A3844B6420602705DA5496BE8583BE3D6EB1F2D26430E20B40CF710E68510350A3330E8570AA46A74A133891610F618DE168E65AE4677592A5304A9070DA103CB8A463D3DED7AD0ABF3D860D366F88265B14BD7F9F891F400C4790690081751C89EC993A323492BE2D871D7A5405ECDEAEA251C3BBF54BD339504027759D3DE9674D720E54E740CA6FF961AB88F289A5B42A30BF3182CF0847659A27B007C0981B3A3266CAE468E95299C3DD23A0129DEBD1A91C9FC971438ECE880F780932801284633A1DFF1EB5FF14FF09A3F895E3AC9EFBECE4A2C2BDD99BE244F64C944F290278FE94003C0870D47934082F83668B9ED34395CB1006041F9C5C028C096D3BEDD864AFB05D58F03581D97A197E76CB905BFFECBBD8FE86E19A83EF6A161D7B066D8B23699B498BC3482E017DCA840B0D4C88CFD5B8418826E07C204AC8448693DC00A69428BC645044669E5D783B741BA35B11D2BBB8D909A05CE801392B94D3C226C10A6B4BED3D24F4D89A2DC96F18103F8803E6AEA42C0BD69E8AD2D895ADAA1274F47A43F72F757D8BD2062415DA06AD0DCC1497045D8446C413812F2BCD57E7A59521F305DC69D96092FEF434CE69A620FD459DAA8C0963C604D9AB2A531C28EEBE4227A52127177272A924E7C407C219C8407C2056709E9CCD547E1B52097C9F6F21CDA2D478CC31C746F95FD02C2C9593165C54682A1BA7FBD82D11C09BA820B27E4E83333F4F061ABCCC39C0AC9F312147189F2964733041D862C722BBBC021C58706ECA64D6CCC46FBDDE147E8169634100675AA1BA935738AE6CC23595EC8BF8977298291D8691B6713C46D2D209978006C4C0853AC5F9044ADC0013E41CFC3A9E906A985262C287C36980456E89B08028E1D534639B1E628B806021505465FD3372037AEE4CFF31533E0B0ED20678ED0DBA05C7A087236AC68E8280A0AD6BC48A7DF6B0DA139D9E1BB281649AE206A490FC5944222140FF43FBECA802DD9EB6CB96B0DED7087C096BC60A7383EE792F3F8959CCF3040D4C80E773F714FF50CD71A4D68F54EF408DCE35D020BD94D316C8C028C1905382010C2192CCA1F11FE3EC2790E249809A6114ABCF93479A65CE2C4E8A72164D16F7E0D89C6D89BC3E53FF33797061FD45429CC70192431775BE4C203DD0891034DB21014C682870C0304142C7E2363A31E2DE8D3BC50C13BC5EB7C0E42555B0A5D3BB75A82C81EDCACE6E18077258842B0E9300265CE1180C425B2FA0252D0E957059C205A0101338263EF14C9E9CF99C0F93705DE85862C21E63C21E33710A08A2BD2B51AF1192E9CD0913521A465817B828724F14BB46D38DE8E96F4772726779A72A3648EE43A4005655E5ABC9EBDBECF156DBB28A1C7A4B36F668CE92E0EEB305BFF6F365BFA84294203ED04DFA9599BE55F6F410C40462022297B7AF203F2EA002E04374AE8203695BC93A4ADAA60AA135A50491A48449FBEF82B168C1CBF3F7D1C8F1FBE429D8C22ABF8A5AF1E9D4D5E12ABADB1219FD596EFD4101220017427942CC0950272174C142014583306A875187CA2695CD04E7938C0F266892515E333230ABCD6D9AA3EBCEC2EBE7707B77843E6BA7926F4C65E9E3C40DBB80559456CE876B93407C3038D28E4E686BF368E1EAABC904D005C00503F1A17CCEA5C9498B66F9C08303A201F26628A57A43332B727CDD74F6653868EF58B3B68809F6B6D2BB4BB2479C4F2D6C1B267BBE9B7B2AD75B1A3D52FA4C728F2957760F283EF8271A8C1EAC50D853DE0489F52CADCA4645939645794D10264BDFF11BF9A72FF7357A08C8AE3A5D729E0D30D31CEC0D75FC63EC80AF076AFD580B384E08080B088819A341EF4AC9AEA8C241AC687162C88BD1FA3D5CE60167E6E522249733416931A34A54F9CFC311AE2B5C9A5B110B975F78FDC2F7E7D97011010897130E4C68D024F04A3B4C2F153081D91FCCD12ACC94E1D62E6890A34F70199C3EB3FE056577BA3D03DF0F8795E3660293A32083915DE34874424703D28E9AB6D5BCCCA150499450089C0CC407260DCAF13F3F695CC14E534122030B0BD4ED5E95C20B2D38D7C7E37E78A9FA67B25BA33563DC1A19677613C75D9FD6FDADC9B40C4C851EDF8610611DD0636A6941979A12755C7ACAED96A4AD49D2920051247DA67E204826F99310B6037E7BA7507EC458BEB04535E703BBAD592B3D0934EF3B02135E7F8CDC1D09761F9CE84078CA4194806404077AD74AEF86951C392B6462C555091DDE28E76F4DD0967F032552B594DF22558924FF3E28BDCF2B5C5D5C456F57225B6FE45D40DCF19740D6CF84D0341980108ADA251A705C35E3AB2645D76BB2BFDE8D91DB6561AFFD5BBBDF2D706733EB7F0B77BEA56BBF8135E5400EB3284DF6BD39B26BBD77ADA3CCAEB4026AD621A4391442BB849692EBE4CBD97355CE21DF0407CAB820902A62FD400601E25B2BBAD29B6D356C6B2194583B6EB64DBBA2F6BA51F2584FBACD7A43F56A063D4EB34A4FFBA4A1347A861A9B7454D3A22B5FF84C76AF2E4BE2077AE09AF297C86F0312D82345E8CF3BA407EC4DD674E17CE060CB686B338F2161B32D481D8D46E06BFD482B38A087670462424B85E9F76F94FE2D23C34D891257F2941B65A09D306548EF0D0A92C8BFC0CF09F373AABC1F5EA611A6C08277D9656ED6646F4F224F5FB8FC89E39FBAFF4208710E34B91C22EBCF9910330E44D7114A8AAE373A83D1BB35FAB766BFCB614D71679731B877CA987BB7809563BA9F7CCF390172D097321AE805B4DE3521BB56890613101F4A1260860F2C695CC887697090F284ED38EF7E4058201B438CADD24213EEA11C5C28614BC91EC2F426F54F246757CBBA9177AADB50E90DC3ADD1C8B17D444CB00E4463971E03C71E0E8D771D6B47D577159001671E2565BC1F69B88C1E5278E4A6F789901300386298BC7C9F09C6BA3A7E197ABB6250D3380168C4BE6134CF8DF08494628FD160D055FA5DAA704C59F11E1692E47A012B6618B210E5F0F21B54E1957F2FDB8A6D70999BC732042E99F865D89CEB082A24104729084C4301E7407C050E448C0951EF4A25FBBBD5FB3334283031E829EC0191618AB2E90FCA1CC89930B74F23077D1DE71E23C36D41030E22035C9E3ACB87F9F39F76E40913A69161960F658599772B11196840969284035AE6485F13E8B6CC23396C810FA657538286EC5434E4094E4D41C408DBBA732439C7B2B54F8368F4C4E85310836628D1E3A776156D9D9E68A8ACD3EC6E81A5BB24933E09504AFE993D1A0F8589F5FB35D37FC30428A5F2D423F10F765FE9AAFC3A1A82AF8800448086CE4AC684533D69E5A63FB82BC830E1C3CD9418657026F40BFC9A1B3FC11B92FC8C30D32B272F16697352EDDDCDA69C89880932EFF6E1657AA9A72D2D6BE9C9954A9D2D2D23A55E518D259D46D6617D323C1B6E5B59C7068809D760020452C4DC8ADEEFEA531BBD87058303E6E08E0C77006B460B8C1E867E3F815D2A19EE664B02DBC304651A748978BD3B8331D0A4CA8D96DDCAFDAED66704E8DD42B329D98DDAEF14A7FD9A9DEDBC54F3F3C6CE6DE12C148ACF459A979C4BC98544E54429212CF0C8702CD19874950DF01F8AB4B82FE97664C0627AAF47575AFD44F14F686DF0DE7DD2FF8EB364356F343081468E376922AA7FA15AF0D4171A7FE61ADDBAB0429D45C6BAA62E914C022BD46594B27BA2C8CB827DA4099350E01F31261C9598B0AFBA078658EA3DA23CE12398208D5FFA8809E1B10EEB07A21C349D8B33011C2026DCE57C9852622116D1638A9B77D17B4B9EDFA7D03B24F9356DDE0F44B8DE48FEEA55B949C96E9398D06966B00060574E96A4645906B29A1CEF887899359478594C77A5645DCAF689603D18564749AEE398809810D30FECEA058CFE1DA00F1E0C1AEEBD72A29601C497E6E0DE18DC9B2526BC0793703707BEDB029C0C54D1472F597A60677BD6F021ED5D06FD2367341AA53537D93547DFFA59C3CB6ADEE87B2FEB26D6969EDD2530356D59EE3D6670A0C36F7D6D59553FCB6937094EFDE0B4AE7C90705AAC1D13E264F87DC096B36EF69E328EEC31CBF02D77697697A6777140F36445FF406AB661961AC26974ED67F7702E4ED870C20B074CC037FA0D25B9B7A32B0F194278A5DA873417D5DE973D28A503C9DC071F4C634B3276446408E68EA62C89F4806A962D481F69BA9E7DA01ADB8600EB0707F2985033CB1A893D57A1C404DE77F49186DBC6C31E624278A247172631E1C2E4604C90390D86F7EAE07E9E0F330C2937CEB1E5E7DCF88784F9EF449B5FF08AF9BF36C5F77A5541B20BA9C3BB41794F1ADE25733F95935531ABA8F147315E92E22531DD94B37D05AC205A120D28952226DC34E31B448698B9156DD0D50777131883AE317A36864FFAF8D91C7F010C6AE4563E878702D34663067713E4FBEF77F31211603CEA674756FFA23EFC9EF6CFBDB462F41A3EE831FA3EECDDC6FD3377F405869B40370CBEB1E73A77A3FE731A9ED77B8F69EF3149E941A0195226BC0C1AD8C9C0DCA475AD9D3D1B75C8C5FE975ECE84C79C09D93D6890A47759C8E7261D4AE185EE5495E0C2F38EEDB06DC7B74ED476FC13079A85EEDEA4F52CF4A0A5F3A76EDA143AE889CE6E4D34B604D8BD5B53FD334DA37B3E256D55A6A789FF252A9F451A884023AD0320D3C39E0BD3373913382B3819D8CAC1A624CFE409C8B8B565693CECC3F385C75ACE814B8026A5402321F0712B1F3EA8C3FB377CF825BABFC63F8E2ABF47955FE7303FDF802981F8946E0C604C88D8433EA3645BCEB6A56C97CC3D0556A4744F4C56C4FE8592AC4A590DA59C6D2AD9BADA3B507B552D3B32929B88C8704B4C187026DCE933B8D74183E133608CBE18E3AF2663428E218B0FC387098C79EB9F07ED70CA040670AFD7C5FEA36127187DE965553B3BB1B35D3D5D5387E3E1E06590548CEC9802C2683418BE8CA017462F7D1A757D1D09B4429C01BF6EAC68A39781FA49F68F9CC1B75E7A4B2704DE3DBB4FEAC7DEE06BAFF79CF59FB31932D033F51813F2E9AB627C4DD228387720E8A36B97D0729B6D8FA9A394EE33DBD7F84DC9CD5B03E9845D15DD23256C9B4E95C48F5DA5E946A24CA36FFAB2E6EE1B2003341202825931BC13DBDC8588328B980069746C3181C4EA9C0F4409733217351F599369DD8AF1CB8026B49D969960C6978C096D99070462C203AB101F9421A7077F6B8AFCADE14F28F10F79B29836FF34B6DC96CA5FE23627031C3FF46E702843D5B0AC3744A507B9D886EC914952B7E4DE8D8C961EE5EE1255AED85BB40DD5A96C69F16D0C32D003426F22620299A9C609909BECBD3E7AD247CFAC7C32006A87C53F18C37730787863FAF7333B2C98802FD2FAC43DFADEDEA5474F92FD3E1CDEC6E3D7D774431B5C45E98E3EB80A934D6D34ECF7CE3C7549B1363538786C29CB92B36BD1D28BDFFB70EDCA4769F463880D929B1832C9AE980892F484D9AF7D488CE10B55FAC017A0D77F66CF0B7E640F05BECB82E37C725E7A6FD41B4AB3E3C7B79E7F6626F7755022BEF1DD8AD607A3CE6C7D4B8A6E4C7347F12F14735B4EEEB5F449EE3DF7E2EBD03E10EDAA247FA47B7D48321D3BFA8A40AC589520767AF48D19B493776A0AF40C9103BA0B19EE7F725332035B30F8C0401C293341F920682B60C230A88A05130C4E06C484084CE8288B9990E3EDCBBCA51C3A8677EFD0E39FB3E267F9C93F22C93BE97E39E9CFAE28ED83C0CD3B7F582F109B174D282681BA59DB4D69D2049BF9D371B32B279FEA73ED50FB8D0B02C0741262028B09B0CE7BAD30DC72256F1F3EE8BC6428AC9F0507C6013D47FEC1374CB89F67028B426A1F94B871864FFEE0DE1F3C79FD477F70ED0C1E83EC1C97CCEF5F51BD7FED242D1B0A79F014F41FEA38F8A4EDF41FEBB459D74B3A4EFFBE9E76DCC143503FD4FA0F01CE43EF2E00A24BA7D7AD53BD5BCF6E81807013A437F5E41A1FA98760C2A1101C49832FC9E865D87B8E9B2D376ADBCD8E05F112DD58EE2EF51D597B32688074B9D9D6CC1D991E0EF2087544D1C03954BC33D5A92167B0D47539BDA775B36924E1A308FF0EB113DDD41903FB5E8D9E2442764F6BA6324A949880A49954D38298B022BF8E46011294339DC7847C9A2E5347F08B64D9A0C1236302E7C34361FA939765DCCF638615F70B88F10B92FCC300F26BB670C2FC9C3937E4D4F398701DF3FE9F0462802811C51D1A1C6049306F67C2E98697D4C8EA313DE6947160CA041CE1BD362CEC7EF830454E8329077E0B8B6830619A9633E11E345059706095AE3AA0C45DED811E5DEA38EADF6AE41AF0F29A7590DCA88875383914F16E6436A0A47249995D238322F4AE8B6EB7628CB534F4A64CC7641A0A0D2F9CD2B4C8E040C4C9EC3F37D3FB30BD8BB22750284ABA3E3890DC7A2EF2047A24AC1BDF19F5132B6EDBEEA112DF6A61CBC89EBCFA8544830CC8193A8EB9A3DB47506BCD5EB747934A974596EE4A3DB64C6A3EC64CCB00D72C675767EB9F52DD3B2AC33436B572DF1163823406130E1813CE4DC224633ED5B30EF3F10FCAE8511D3D52396FF46F99F03E25E671575259BF1337FE4B547917642E6A4EA75BBAF0C9B91C1EB194F78A59F6751C5E046CA080D40EB401D5998947575CFFC4393DE811C04972CD1E017C931013A0AA6F63B2C28719022C02B3F2C702FF8015DA7B00F79846427C50FBF7F90F1CDC6A4CADA9BD6E1127393DCAEEE07A327E2A17E0D2519E0EEF94C71C2EF30EE8629041E273579BC77278282363CEEEC3E43648BB61D685DAF48263D33F508353034CC870AEEEDD98BA56ADE84AF34F95EC311C7EEBA7B7617CE3F4BF05C1A5A6AF0BF69EE61CA961DBF50F3CEA38621D3FC1B93F7EA167ED44D734CE23F8C7B4C091BDABE52B1DF125B26BB677647B280F2DAF365D0A9246E6D8DA18C48423393C779A174EF39C109EDBE199D53CB3B22B8D4923256BE793B492969E16607564D5469297E6149765E8D119DD083269E1B74AD11CFD29EC32A6B720D2CD5C667E4F23DD69C56F6B64B87038A2735639A74A8E86D36CD8BC6C36CCF0C40A4F01D49D1C67CE4CFD94E12C4778CA8093B96F38153377F0CCD093DBB8ECE67F86DB32122ABB311DED453DBAF00AF8BC125F023EA105B09754F1933643C78BE85DB4BBF46E8E6263BE3D7DC44BF2976E81FCAD045B5EB8099D5594C55B6CFB1C977985DEBA7092E966FCDCDAECF63A5C1A27B9742667BE049B5D089C739B6EA73E25842746786C4247D50FB4E6199B807424F5BF24FDE714E87D497ACF49FF4B169C3AD9436457E809E2F19D1A758DE1B0974286DDFB6937AA37F4F8C13076040484FAB992DE07DA1A2D6081F3A9AF4B225B564CFE24644F59FE6885F16B9433A166DB95820935BE587CC10494B3CB7ED1EA7CC484617824479776D4B211952264089704FC2462C2833A7A2A4703758A7B65F430819AE3512D02088F242A79FA5BF87B7578B720A3288456795733B1055EA7A4CDCA286FF6B63DFF48D290D8C7DFDB833A1FD9787F00DCE40DCDE4F3F7D59C03ACE48F6A0F2FC2A81391D7BFFD89F54F3840886F23BCEC75708AB4E214E1C46AC3024897879431EBBC5EC6F0C94070E85D91CE19B2CD28563CBDC1A3F10679548158424C20AD8BE84121427F9B9F4CD514CFB329B520F9C4D184C447637986411E1C8AD14C3E5E3999E7C2879CCFA5E84C0C8F69661E31A15630E14B4A952FE9E06BE65494FED704657A1D0F7ED8C95D7DF432C81E82D18F2CBA31284B3E90903D078833F7B06DC96FA8E6B6626C8B30DDB0A3DAFB925DA5D1CFF178D87F4E1010B40D316702F284494C6004B0BC636B414C10697D32F5339830080F255244EC8EBE0226BC69764D1603FB1E3FEAE32766DCB89013A39FDAFD3CB8A09A1AE51D51659105BF23A84A2D590B590A251BE5C4635185F5F0DECF692D8854681ED6177CFF13A8332FEF2885801280CCF5F6B4A49B92C261469FDF7840932F42F001E5CF099070994425A50DC4EABB451CE6AE617A8ADE482698F23525BEF31BDC4FF18E3452294F6080C9E6F6FD16B9C5E7E9536F02D8FD2DE5125089BD5B993A126ED88034F284BC94F3E48129A5B9592DC9A5125FC8CD333962D3198363CE8464F0251B30260CBF656E45815E72F7D4F4BA993DD5C3B635F896D64F4DA74A04A8B7144A978F64983E2283B224780D397E54EC23D1AA886C82AA6CECD1AD9B61C74EBA4DFF44AB5F6A9C0996B9A570262008A0B42A9ABA2CB8A001F036267C962926509EA02220C4FC8EEF9619D4C4E699318D094FEAF859E1C16192332C82528E063C56D0F5BE53A6F5B2114C33EF526FEC03337D6CC04AE461C3A7FC2383F7C0AE37A1BC1FA6EB90B10DEE6788F44BE4534B6E943C264CCD3A6683CD941E30022479F97374F3CFA244163EBC7B2F82BDCB84013B2770C094C62CDA6CC013EE8712262704E421674F1973EF2ACF85FAA03AC3805716742AC80C8C152C02346B32B182E7D037ACC2D2064E033EEDB2980C56E4CD2D25B994902DF0DB42B087909810F79F59340013BE2226A4EEAE32F8869820A7D749F6ECF9679A7FAE59BBB27F6C5AFBB2B94FF731FBE7BAB14BCF1FF1CF4DB87C7D5B6AB6BDF04AF52FA4A02D9BBB5270E1D71B967BA4B1F5F3046202BCBEB12E736944D1A04661C1DC51F2E0508A097947EA2771FCD24736135D5801A2D885119EA8E1A9129EC9219870CD740BD1007C60782708CCD240998B0FC4848539F7BB981A0ACEECE851FD453099FFECF425FCFA6F7F308F4283397554D831503FA59B1350D26D37132B7F8F0FECDDB43B090B49AFC338396FF7BF038D33E1DDA0915382A832A541890F407655EE635027F577C830D399D684B0B95D344099F72995277D95A6ACB2BC393A43DECCEEAA3D91A396D344A6D17109D75E72ED7B7B6A7861B87B5A761DBB351D3EBED9B1F44DC1A9E9B454DE1E4D3DF24E3475458043D7B768D0CD3950DD135A35DEA808F5868C2811B53DE7C88CAF9ACD4B2F3CF518138E2C7D55AA4F988038706423DD96E8B19E26F830E5C11F6C12DE277134644C60A1203CD382131951AC7E2486271A5347BFCF84F90C61211316C9A75F31E169A1EFFC2DCB7E87096F3F3EE920CEC7D129269CCB7E55A5CED082096C602162534D230636C8C01286B4FB86065DA241CE846ECE84371D6EBF4B0662C2FDC2B77EC904E5D74C584886DF6642296198DCDE48C899D0A069799C09691779701D65D6AD03E96D1DBEA6D936491DE1C4DE7876553577901B28347480B0C0A61B19DB82554145A89F99EA0A2D936AEC30861CABE9176DFCB735F86EF79ED5EC49D3B7681D55812D09AC69CB222D0CBC6700D4B2AB03DAAA645634679F1E448B7DB11522F3C5BB47DFFB943153FF2923C389020435D93F9017C484DF22C36CCEC0543E683024A8056658F1131431E19FD9CD842DF0EBEFD0EC9DC64220810948FB880937B94DE70EBE287925BD2BB594ACBF84342D3361E6ABFFC16F799F0985B62CD51733A1B3B803FAB79870FC8609EF86053E9F771A16A2C6FF21EEEB411AD9DEFFA7D8628A2DA6D8E2145BEC010B0F583860E1808D03160E583860E1400A192CC290220C29C26013068B3058846021C14288859014422C84D808B1106221648B05B7D822C52D2C6E61710BFFCF73CEBC269398DD7BBFBFFFE561EE389964F3723EE7793ECFAB122141193DB5868F9DE1731B6CA4E1530724A859C016AC7D36E8F6824BAD79C680229B87C00114739F68DBB27384391778A54CC15E324B44DDC6DA1DA7AC61ABBB152938238367BD0954E442EFDDEBCD0B0D3778BA842102CADBF0C351F4C00385C05614E59B247F4108C089285400C60CF7804E001BCE174DCEAA4AEB9805DCE1E515951809AF6924CC02C3CC87706D4DEB8AF9C655AC340412A67D4A0BC923CB43C242069240825F62BD6EAF139B40D7A910817028A183B5972F6915C1FFCCEA8469407E8084F11C243CCC42421CD99C898445C0D03E2219C7512E18324848020B1DAE13E0159A8884CEF0B1C779738793E61EBA89AF2D1B1973BB771F38874670C1302B7B0F3B82A95B589060607B0BD11C52E25DC0B0010C6FB84DD44D6CFBD53C377A8FEAE8C7F0EDEFF7D1D348123BBDB64E78E3C74C1BD4B86DB0FC09A0204EB84E009E307E019D10F787F3CB8A8F55D8C43D90FB8884180634BBD6532BFB69AEA023F5B735492CC23A423865D7EB7CB7557C0248987EEE7C188448B841240425B50348C0C85A8F2757E39AE6E789C44B3FFC934B78E755A7276EB801CDD0E73A215F222E3453B9719DB0A81AC9C02004C39F22E13642C2ED4748C8B08518090A86991BBC014F4D193E77464F1C09409D9F000CFD5ED76F9E630796FE65C72CA9C07A073F34B7467A7796B145ED2258378CAE627F48739F1A055408C098D9A64C96708BA72B7270666097476CB78A569355E19D4F41B475AA7C9126FE63CB4A061A72988B4ABE8175F482658ABC59223684AB12D10D6E26129E1646C2E3BF47028D91F0914AC98504E54860F3903369A4E149888433E21F526EFCF4FB77984CC64D1D2E775C6EB37217CAE07E2004F5C0D368F03084A7F7AF7BC3AB3F4602A642FC8641753F0586B9489807062E3391703D1F09E83E0A75428D23E17B8C84BED009E39FFDD6B9E51CAABD8BF668DC72AAB4756905808D126D9E1AFD27EA14897742B41DD92CD0502D6C627317749E6ECAF2B2645515B46E567020955B63D66134FC279C62226314598926FF8886F292D0159F4230D0651C76F4F6D7B8053A21EA468A27A5B93A61DA04FA9F22E13264CC739030CE084D511164BDFC0A7D9D2159F6C212245C73241469C40710097ECDF38F7D21D8F4172548095EF1EB7E7C8F537246CF2321F0229348987CE7136F237592E884C5F010677CA56226C27395B2976680210F0FB08E47617881CBA2482021124E43248C1224F0C0C28F5E50379CA2E21C2012064F9E5D62EEB16295802EABCD33C3AB32FF58852BD82F7E5BC602FF3DAC65C36EDB7CF81AA66763C33C9EAABD23DBF8444CB10E1B03E3342B3EFD3C6E154C0473584EE904052B7D087A5131C68C4651093180A651157BD48096104878FBFF8904FCFD6224A4D7CA8240EA37C87C7D927A291ADB5109120E310B20D6095ED5752B7932E3BAB9670206303FF96938B8E348B84F94C06BF69F9E87F3077C3F1F61204B3926C070C7309E70BF18186620219F2A5C4F538524CA864868E420E1E547BF77E339651916BA73888D1B7B1741FFD1F68E4DA70AD6BFA4EF10AF46D475D93890B170679F58456A1E2832519465C93EA258C3B925B35549DFC5C647B0A78B8ED98022091BA16E2BD8FF744BC1F9CF7C18BAB1855DAFB50D595B93E118271DF12322E96D3C6E8A86E0554548B386E76E412081BE3DE72DF1F0F79B8B84E8E6975B1AA3E24F90F03CB15616B7940009E1C63F1303A977156FA82F1863C6D8505024A8136EB8CDF33074CBAE5B7260A71747A768C3D12E46E742B09FB66D176DFBD032B68D10098F80843ED7092C63112D2A741432E63978C823DF493A701E12A6F090A3166E1640C275562DA4E3CDE724D2094A88041E5983E3E0B1D9BB7507776DBFACD9FB5AE72C787D1DF9473A8325BE27F9A7BA57C70645620E15EEE9EB925D6538BD7C8BCF5F3BC0F1B574490ACE55B09ADA37AA51C0F8837F4A01370CFB1AED33EC11FF2D998E08C442412C09DE2C8B604208892FD22B4702378A50820A96600747049170358D049A8384E9559E05C3CBDD8C85BB0036E0F79B42C2E2122261C1E7863008B32DB0D1154742BFCFF92ED8FDB0CD8B852E163D4E5438B4510E2C3E5DC1E66209B1F64D7DCB000C084124742788C15C03691E12E86C0CFCD74898D20973D442AE81245AEBF2A6C211129EE39C8B3E5A4707322CDACE294742D9C03E79058C1CC38A5737D080276BB8EB8315649771E494BC2423635E9370D6CEAE4C3724AFC1B045E4216D5E99CD734DC2721C1415EC22410CB099C527A4C5C60E35B653B59B311814E9F517B78EB85D244882701F39FF06098F8B2161119D1023E1E9F75FE469512424D6792AEF08AC2340421F236B1C090F036357E7D33E295DA5EAA6AA6E803006B28E0305D54DB8488D3DD5D8831F46B50A2A5BA5A142B81F224FE84E2DD969A292509DF4A3C23A8ADFE787604821E16106127EC74682759CE8845CB6309D93179166B08E30AA308184EFE845EDDF348363BD77E13AA0131009436B5B73CA04B4010695B1472A4E12B10F75B085706EF90E67B99FC1B087AD1D874DB975125C209306A308EEB7CB04F0C3FB1D9550D43559E6A9DB7C62080619D8B292E90619FFF7592021C240343BC7E7EDDF7E0309B3619041C2EF1B48F0FBE11B7864FF7324A483090912288F31F77BB73DD009C6AEA1AE11FF40F50BAC59D1BD03DD2B997E994BC50C603F2BEAA19474B3801338796DFB009030B8ED4F20E1755185904602C7C6C79A21A31338F3F90D244CA760A49190C1C3C74820934878EAC56A61F4DCF1AAC68067E075CE9AE3BF865EC900FAAAEF49C626B38A846C003396CC5DECEF02EBBE7DEDA0A7671558017A51718E38D83B4B129F3C82836BE189C0A445E72FC32D19C0330006F1506875052BDCAC7D351F0963810425A884FE53A116DCFD8575C2471CFA25CEB648D38CC550F17F85049A87049A20011B960C8C3D435B53FC821A1CAAADAA616D327B4B757634107B5B75F7556797397BCC3DD01CD4099ABAAEA23BF581D7B6873AE14F79C23412164957499040FF8F90D0CDA65D6037249AA71342E9DF05A3C79E73A0824E18FF3572F6757D5FC291842BB2794014E40C380C04E7D2A260AB2F6D3B6CEEC24763CA1496F73A0E58C099543B12306CC9E3856900066D4311BE5421F0A2A0A9B52D328503611D61B6050E4D2B85825ED40A36C7ED5F0B248429F56F20F396EF8C0507BFC72DBC823AB9FA9F68288F74023969195DF134D83F2109206AFF9445FF340D4DAC50B2DEAD099D70838996A0D9FD125A477DCCA440D26C8275B4CADA173ECA256F837781C25BDF056167ECCB66E7AAD9BAF08D5D9C38885188FB3ECA6D3FB4F5B9C469EA99E4F6B84863329D91F3840845B9851C61D2D194F0405B8C84A988DB7D961E8479DA7442DA75324C676BDF9261CA4C1A5C934478B676FF8AF4A775C2B1024A60F89CD209DF31E7028E60D277CE5AA013FC9AE95415A742D475057DA34BB8E2B555AC52D0B6702C95F415B76FBA823D5229419711984FE847DA41372B5995407B48BC6813FB598448F822E3F1332041718ABABE4D437AF03961CC8084F18F51502662E669782CA217D5D913BE23F2F623B2D1710191997BF953769D251B3F7DB90D4F32F7444F7C4B5094B37F8FBAEC1591A0E23FFD073AE13485B71C07543A871C176258A97383EB06ADA3B2F01D7124DC7324ACD1A4DEADE1C0B1DD70DAA7919CA188EBFA0E20411D0818F0CE3FA3ABA90CBC4C22563EDAC74F78C3F086651845A216E6F066816D2682D3F374422E754EE984CE71C69B34CC250C619167AA7BDAA5D0092112DAD348889403EA046E1D0DBFBBFD07C3ABEA6014611C6D4396BFF178C0160E1661EB9C3A6F2A981EF14DD2B789B18BC3D7B46D054802506ADE0046E148C0FE2EA6BE99D2099FB1DE194C26D015394890A5D79FA813EC3DD406AE18B907602811D409BC52E7FD89AF12B1761F587A434A6F4B99B595AEE6B9C712F270E77B989541144646E3BA82584697E4ED49CD94C5E5485EF51CBFDE3F5542BD94C9A7C854CC25EB20AAE8C56C7B747A503FB48E44480191A0AED166494529ABAD8AD6AA6AEDA3486A3A1C5B4778B15951F52DA0D11AEF7E85E108E4DC972C5B4B947A1BC997308D16BE8E7333F0EE69F6187DC0D487E530C0462F60B8CFAC559AF61D456078E1753C9D1332273F2F6326C548B8C2D6C2BD0B64CCFD34129EB248780E91D03E4524040DAB75A99A3B3C8650C2096B382607F4C38A847916FB183BB3F754EC85FA55F21B8653D69C8A0EDA00A7ECAC23970008853A019020401382812341184E39D6D127E9F5C71018825DA05E49730ACCD96770B4F7A9B503D611AECBB7EF0C273136C2218DE1C0C6532CFFE763EAD212B7C6E065CD175CB0618C1A0EB43BD34311E3BDF86D7CC08C9E080EB9D0E23917BD73FE2C1C8367C6FF74F24E1A29091F4DBD9F33BD75ACC23F115EC77F3AAEAEE6C3C5E2416342C47BE323F7703C6B4DF58AAA400282E1AE6FEC80754401031112745CFD91B48E74C400C200C1A06FD234127A773D78FD70C84D38EE890F59C3D967B6905E2CE77CAAACA81B3EE755C2276244A79D8C9A9D90D3B87CD98E4FDA5839EDB41B2E161937CCF6A928F5B6DAE9B26F51822CAA90B9B44FECA80A1CC4C4DAEE239DBF2CD691B74FAD70A6ED89D9AEA3E03D622634962F1BC11197AA1E5434AFA4BBF01D9E8B99542112A6A403D61147C22868E8CD33233845BA8C4A6053011EEC1454BA2E3965D52E2B5E8D983B98634A8902440200C30100A4023D486419830CC813100C601DAD2B089A2F92B0910412EC432D87317F92C6DFB97554909BD5D438D112B78EAE50230312D206EB14D79C94699E80D6D1A39A6FC07C24C34B41D917A60A592A02AA7932403E83B287BBEC5D9874C4AD23CAF38E44F526A805F4A2D2156201452E18CEBEE11D5A5EC9740F0DB7A07B45C32BA213098E5EC9B2B655B641C13A12E94958027AD30FB3B233B59A912AC8B84D2744C498C907CD44A662CC61B1DE03AFD4B9CF5A47131479BE75043C01C8EEED046926BC9E332BDD686040C898A9E00978E4A3A8F275C233F004ADDDF0C7AF23EF483376C8CB2FDB6F30B7AABCFC7087DF6DD89A198F279805C52EC9D63685E54D79A778F380A938BD136D27C083B6AB906549F28F2C5EA363EA5B24D1090AEA04BF6AF9B54C998E40850C3CE13BE804EAED73EBE8500E497331E53BFACEB2A6F63C184C2301AEBCDC452BEFF745645B24EC76118917FA13CFC08B69494E983CFB591EC2FA15F8A551B99FE3E47A9E53DD179166A113AC356AAD526395B825CB3D0424C0D1F2CA960BA82899880D0043D9A26B54DFD4010961A5CE4D9C8B3A65DC4F259260C16A36BCF09B48989499BEA3D951B697A8C213A473ACBC24D8486A3B537E2432E54B257CF0C2624838002404BDFBC03D62FEA9DABB35AC82DCEE5AEF7F37DFDE9A7E45D3F73813D855F45DA40D0883AF5C5631CAA66ECAA2433059918D228978C21147C257A110422470C3C99CD208F8DFF879E497091FA140A2A9E072988BCAB303704B7E8828EFE3EFD356916D31D501E06D864C3CCABDA8339C4B693D90BBC49F18E61D3DC5DA2C9B3F3B8184874C4A3650851412C2B46A4002F284A2DA2C6ACD43B48E9A15B088C02E32A2A3D1AC86477583E91B9A40429FEB84D155486133DA605E6F8424DB0AB65B7C7B13EAE2432FAA482814BEA32ECDD62DE4266F4F72060112B0F2B133409A21DCCEF2A826F5CDC27DC4918005E5ED9308095360708B5AFB34689EC36E62395565F4C30630B4BBFADBDFDEEBAB3FBC75AD122C77E0C492770C040CC7C63258FADF64A3C07084C2AA44D7D18E12531424C0006CFC88846DAA90C83A52B076C7C7762F2659C2C082B629A79B66A3755461CE2EF52BBA7BA0BA87AA7BC0BC430A2274C2EB332C6515674F74752E5A389CEB4A1B2E22D768F4C3130797C91327E5321431FD6E9092FE29F623817F7474AD8DBA890CE3F36B2E37915CAB28372A9F54A0F64EF0885302D26FA99B95F0BA3E14EFE482777302B65057FD43B5773DE03A2144025B256DA001158DF304215A24F19F7854D788B1A9C7A59BBD9B41BFA1F6CF41F09F0825FA98FC0DA8897423111FE71AE7A08DAE53D773848572CD9273FC1356272C629C0B83DDEC2644DC739599A933CCCED7811BDA4762D41A8B044DC79C2BF148C2702A21D26550058887141244B5DA288244EBDCE43AC1F78E2D6B9FB6BB385701F8807BA4BCBD05AF7FB7470F9E5385452F7B75EAD735631F6B9A512DAC4A0ABA5015E98B24117484C209D851601D2112B0133009CB94D191AAC8F093686B2400EB4816916A294D18C6C09831079BFB4FA3C89A5BC42BBC1B24E804DE3B1E9DD3F42593FBFE416EB0D8EAE0FE11EE678C372389AE4FCB0CA37F7849C661E9C94443E28F3293F96BF6EACA8B6881F891609B931B368ABC1FA8D0B17A93C6056880047D4BA74B2416F24D418113F8733911B6224E384FB815841BB13438E71F24FC17B163E928127EAE257283A377A2E13708D7FE0543D0DE24328A1F8D2656A586B8C5A28963FF92016D4D3F7D38F9F4E4DF42894E06387B4A059D2046B3893F07576AFF3292643C6166766DE794B61BB475C29A35FE65F2BA7E51B3866078149080FDA5E955997DA0B74F82E0D40526E01D33500BCD0BD5ADC92F7FB983276BFCB305FA2138614143752B4CDB908D7D85E2F465495EE618207C61AF4AED1BAD7DA74A5E19EC54C33ED0AC02D62B8B8E46A019B475EAC0665FD49D8A199284D47F2FA0134AD43BE0794725D92B0355C0E170EEAE0C5B02EA84EF71DA26365C99B0AD23B6904B6743838433E6DF098DA54C1DCC3B7ACA0B474C84BD9FF25835F0847AF436A2278EC5A7C8843E52F9D8B17504783823F0CD885ACDB0CF29B267B07686FD3B2EF7A3C1C36870CF253EB91FC143A001DA573DEF2848F184C100AB37D59926CDBC183341F0DCA73391A6139066300411597BC00D7E469727369349F3B2CFD11D691FE3A3692F6A3AD01607191247AAA00A175872886D21394F8890102A04A1197A3758DA6F17F4D67100CBD5AD1AED2B73F068BDFC34BD132568E0EAF76A46EFD61E7D77FA8FA6B9A7004FF0EB386D8DAEA12D041691BC84C1042C80DE57D45511632E732FEA3AFCADB025EC13062720E62EB30B9A5BCEA10A2FDF87B0EE9D82E48960C20152050F236B52A8139ED011F1C6579B88F2A4B2C452F23425D1453037C57333367AFEB29E34FAB152E73B3F9F4E0ECF93710A4EE367026A1A9E389EFF94E8DDF2D64974C47F720C94224FA01DB08EBA915AC02A85E1E011640432C4E370F83402E95CF73AFC1E7E44CCC0897FEC0B272C5EBC1E0C2F933114220820FA354D398E923FC3EDE6096DF4977B92C2409A6CCCE50961B4816BD794469DA20AB3F0800E0FD8D1333D98737B8C4F2281479A2FD04C82A398BF367C6C0F2224A0A5F4D8EB63636DDF3E0424F8C68EEA3734BF61BEFCF28663D6BB57F51DD9A9AAFD1B8FB7BE93E0681670E09A7940B535F49FFA0D8AC917042C2505F08035FA9F25E003545DA184C8D6BE2E4269CEA16EEDA98001638792AF6026C99338F824BD3C0F82127A51C3B0DA61185C737664D1F90B7D473CB2F6F634AFEFDDACAD1D08F7CB0D5FEE0F73563F9D25E8450DEF51E7ACE6372E59C711B78E1A18597B9B7E4ADE1B0E23B211631E844810F5FB1112EE051286C3B43C8FDC8A1317278439DB07B6B96346D6112281EB84A8575FB264455C92C707E33793B21BF93D04F38EEEE7C416F3F09084EDC2DE1649E46EAA55F3B42448B807D54AC39661A14E60795136B42D47F1A8E2A8C90598496924089D105385FE6D80356B450390A06D916697BCFCA5394764F04B076E6015885D56868F4D58FDAD4BDD39928D5D3E6BE79874AECDE6A50EA26EF374A37D4CC49009E6A8F2ACECA20E2B5EDFA0F0D2D601665818DB0CA3CE22D5620A08A8131E812750B700ACC5748A96736882B845CBDE077310362175FC93F52FBDCE69D06EC0D1E77D6D136935405C7ECC5C6F9F7109BBD8C2B9DB9E786E2375273EE40367C28BE2A51AE10DF074D812B01BEE05881FCA797444F122C13F7BD1A370D287F333AB77E1F4CEBCB015EE4528FD0B0F3E5428575EEF92CB4574DBB9074FE9346CBF647560778F1A5884C56B776199B228648313500EB0F4CD3D1D65DFB0F60D7357377634E015BC99645F706E8C949D63AFDF5ED8B8D70D2368422EA22346D644708D9F5CC047B0FB97F8101ED3226E10E1B60989226EBCE3B2D93B3779605174F3E5FD9533513911838B9B2E5B899CF230DC89195F8F226E56AB0E6272495DC1236FC95C43E121366029822728A3C7DEE0A1337CE047200CF0E73DE2C1296ACD9ACF96B113B0E8F26215A5C14F2D00EA7C82EDABD9BA121C6B3EE8845DAAEFC99D6BCD3A24D825725F91410F1059DD54403F780DD27960BCB757D9B00A184B063EA71059C40DF44D6A1D68DA06D5B7D8B44E183DF4D13ADA8D74023FFAA81324BE8B085520A20A6C722FCFF163E6583B400761477F7BC618D9072276F7D49551579CA878FC3E43265F8786F25D1D9C6306E1FC27A2DDF54C43EB8B6B0651BD0936AE5F629D6E3FD5B565B2781FD328F83138F15A887CC061004780B75F73AC822902737CB24E5F709E7164F0448A8ECB73EAE439B982FD833128A98D30DE1F69C5D850146F9EDF3C7E4A8BB0F778EF61383EE2BE8E5A650E1579489FAB51BC4F153D363B75595CCCEFB11749A834E2692C5D74BAA01715732E94CEB982D6D1437BF4DC1E72C23078688D9EC140F26107E748C01A65FF9CF1CA04C9824558957B0F3ADCD03C375BE746706CE21CDB35DECA6809F5807B4C9425D93FA5C69EECD619B06DA0DA9255D01405B77DB6828D5EF4756260884D5C5144C5E6F47FC3BB8E7F484428CD03E65E9051C05E2A69384CFB1C31807AF929D4DA99884F5A3BCF0AB4C17777C342EB68A62935B3E21EB46D12634E27D2A5535927C97472052C9C14999E91F71A19247102739895CDE309D8283B6AE5D26C34D3C2EB16C2107250F7FCAAE3551DA76439650B8EA057AD8215872378642DD5247CDAC49F0A5046A16234D86061C1713298903E9999811776FF0EABA8D3D956F761C7D87979DA7C7D774EF849B691F0848194441552B9D958CA7C8A5ED4003D3140853B8327D40943D4096DAFAABA153538A660CC07471EE8844ED7772BF4E567CB2969A31FADF7F757EC05FF3A164DE18323D3D801DE2B690095352009BA5FD3DFDF5FFAD74DBBC4468F3E5647AD605323DEEF680D7B83993BC01914638B9ABB2A5B55C03A9A8584D1432F28D1560D9B590801B51080EAD995F41519F64511597B13C6EB648C79BAEA2AF32757178423414D567CEC0B8AA9ED63AA954B76990E452EEA47C9DB3344E5F5096A1E294F3728089D4E62750A2709FE9CE714BE997453A3D679AB7DD1C61922176D380FCBD9B80475DFABD8DE91E3D46C94AAE5144DAE1344881AE034C018F35DE8869EF4F94E39215E635A8CADD33014902CFDFCC8DA2C6FB2F850A85D7391901772660949B8153166223214F307174D54752649789C319F92DE056D5588B622B52F3DA407DC340286D0BBB6BD0A75F6254442D5D556C9CBAFC1FB3F6FEFD9FFC63FC5788471E7DC742AA475435BE79659C46616EA061A3E9882BA2B034FD07788FC157BDA61C08C7C51B009F6129EB365A2AE2A3CA0A68463C9A7A8025A4758AA060692E4828A81B785EC59B1B6A5CE29111A9977EE0F75EE74EA7C4E654276C7E55E5484C41BD2EE5C0949EDDB948CBA2434789E3EB2AC264C231475704A3E7E62C2B6A948CCC61FBB4BF94E463AE1FC0444427012045C1BC03138698629497CCB1748F08F1C00031C413F709D60262DF1BAFDD145AA7BFEF41735DB1B867947B7DCA31D5F09D192FC16F36C1E8E84A84E681149F721C763E7988AAE9EF303D2133D9144F15AEF54E99F63E7AF6695000CC03AE2BC19C5AD2ABDAEEDEC2960D8FB15C7D8A0FD7B7FF0D07C7F1BF46F9BE3F1F0FD1F44C00B4E8E7A797BEDB72F751B166A958069D4EC52BB4AED0A23CB925DA26445728ACC2EABAD33232AD7E495FB607289385A9C6214FE1723E153D8FF6B78D7F3CBD4DE51ED7D8D0BFA9AE0C4DA566029E0FEF183B6EB16E63372CED43A7370FACE291C792E7EEA98CED14F4D7F719B40A41A0E12EBD3443887E6C733778285A7A573211E05321D64B9B82FCCF1098926D370EA8CEC198866D03903EA1CCC97B610CEDD5B277EF3D86B1D3BDE21DFD471A632F284D6799B8F5DEBB42F3B384C248D84133F38765B0D1FA479E2358F5DB7C4ADA30806D852F21CB844B37D1A44D24CE42C2578A5154BE71C2E06BDAB56E7A20924844B332DF0283F363BB1646EC04FD7BBC4E17FBDCB260ABCCEC5C42B0849BFB1A0DD68C277DE0239F15BF8B5875F51EB945F14D753D204A9733946096A20AE5F01B17A97E88705AA00ABBF7FDF1EA07406F76023F5F44DD93FD29DA2E1971C7B57EDDDE1349DD7F1C03FD6CD7DB97FEB0E1E5BA0435EC6ADF1CFA607A6D419B10EE9E03BCE4278FF673CB873C67F3BFEB1E65659FBCAD00BB20F3C01D7FDE7EC8A97A3453F4594C38B9FA5E13DF7A21EC84E414E0CA44309540418CAC28B1AA6D0E578EE9270F2644A7DE211A72FD7C23422937AE3C3E01A56EAF0A2B98FBCA81953276568F5CF144E28695657E4F970237B23B28E9027E0EC4DB08EAE391250278036085033A034E3345538C17500A41980046B05F1E07965DB8E90C09FDE1F5E844D5A933A8A199A21E2F169AF43E8BD98F208674BA372BE49FED1EED168C94994CCFE8E79E108BC882DC7D3D3B8EF595A218CC31175624A1D0B47624733DE3B681DA1E3084E10030F2D7E443C0475B37DE1B9C0513193D7760E34A7ACBDFD3DE6AA0019C2F8FBE8F5AFF1CB03984CA819EC4362ECCBCD2B2CED6F5EB0F7B7F1CBD87DF9D9E9741D51BDA0EF93DEA38BFD30145EDB8600F8826918A22530E6632811486284708D0106168ECB2D534002184558C329BAA396309E80533CB0071E7C5A352609D93AA9B4999BE785E08E88D1752A2134C392D9B4653591CD0ADF69F833677EF8E9B8447E38026CFD1998C9BC421CB215D30646372CCC452DD34EAACF29ACFEE6A9109C31358104308A402DE04678044AD20724C48C99C7DD06381F280EABE52496CE76ECC012BC8938C307B9777961664EBB459DD0EF89C851BD276220CB87931612C6CC79024EAA3E0F1933EF7AA4A0BFE8B1DD3CC5D96A409ABDAA0E5AC23920A013BC92EDEC6B4641F2EB5AABABF51E8CC177A7776BB56FCCF6B9DDB9F35EFFEAF835D56F500103754319DC75DEFF41C0BCFC6A990559DFA5C6B6641DCABC612A50826F186943F9CABBDCADF0E37278CE3357F184AD4BEA3A9E8CEEFAA813E22266DEE2C5AFF21E7817F88361682C2CC9D7846D3A2BE7719CC48632EB1BD85EE4C099E5269AB9A5092FEAC73A24AB49E2E5DE6F90B7DCD28829DFD1382FB28615FD60D8747BA14E68C4304049F5C146EBC83F023BD0F3AB8004D7AB38AE4082D009FCB621F084C9C81ACDF33A4C6E25BCA21FC79F65932C52AF300F09C201A58AACEC6C8C7942E2C923D9D2857BC691C0E63444CA3084B4EFE892741AFC887D82E9F0A1DDB90ADA1756AFEB79156D88FA014C3BD73ED0DD82E59775A3205B15B979A9BB356AC13A3C57FD73E63754AB48BC23BD7941B57539B8606C45C2893975D53FA3EE31B12BB27B024725B8A07455C286DA6C83577F62B927AE75268E206B92BA21A99B580AAD6ECAE236FC731D790220C1AD285E0501202438221E2281F00C3CFE0DCEE90639779DBD3D6BB8AF3F4648987E91B9D60ED631FFB6CB2892679195BD182B4D90C007155F31D1DBA27395E8043E5BAD85728E479152119169DFAF392041DDF5AA4EF3D40B9120E209A17514457027D5422A67317449A7CC4BAC55C07596F2B4CE719E4EAAEBB058E79E08DF51CEBA4F675ECC28DCC1FA84E8D1D12C3C4CE88468346D184FA82BBD33023C187842E7CAC316D9277AFF1A4F7A57B67D8848009DE0373450028367BBF76076EECCCEADD1BED1FB8F965DD27AB74EFB9682D887D42A604669BBABB5AEF556D708CE358007A0A2D5C526AAD8F245E53D536189A36CF3F36D4C4EC24693BB92BE23197B3208EFAE8AD7E9A634BCE90445153BC45471FC9453311CDE20C33D64C23A02C6DCAA59AD3AB2C0660DA7F734C5395A021E08904B14F1D0B19791BAD73A11516411840633DAE7FC353911E769E1AC370A245FB8FD6E1C4E0E83C7BD38D81C492F7D3DBE0D23C74EBF2B66B966E52275F1C29D8A31BB9D53A7756221DBBB1D0018845A403CDC24B9A5134800008036F0E15873BD23AE13F6CD8827F431B8766646C599A942CD48782567467A97E2BAD917C72B2CFB4C2A3FB1F833111E8A8EE41C458CEBE5A5AA76FF8AD7AC9E5B29B14575682F89495B51583A115E32EAB41B462FBCC18AA2D4512567FA4FACFC3440C24A4E1CFE6B348FB43EE0E184CF5C3B7745FCB179EA34CFECF6850F7FB64E1D4082B36F3ABB9A5BA7568568BB927F4EEC23C539227002AB1C68AD7F4ADC3A19FC34BC06F1EB266CFFDE293EE49D50B7468233D6BCD25A571A184EB8B27189EFCA1124240103711197FE6E82019CC3B387770EAE3B4189FA35250805A74B0575D40CC277F4F6A40A1B8973D6D0EF2978D85B4AD2D64EA6F8E6998E6F850F942621E469CFE6534E8019FE21CE9817084EC7F23D7D54071744FC89A4390A2DBFE6BF60941C758F2D8D5FAED1031894C184B571AE783C362161C0E913EE3BAA214900F0035B0030B8A59475741D59478F290D30C7608B63CF3C480CC770CEDA1C3E36F150AC13F8D429DEE99AE4B481B9CFEB0D33115B00EBE89824453F731A02A47BA70AC67C49DBC7B09094761DB3215BA76650378213D86A61C9615FF8664383A355402478870658385645314BB25956AC23190C24E350768FA95E90EDAAA21FA2ED641E3038374B7027A0855A55C5AAC20922C7AE10A7A660B7309C487580156EDA9E242A3B791F255CF4780EC7025E01028EC2BB100312FC12F38F622428CD63D23C4E21E19937BB7E0A03C9AFD376EA5456F0043346D7D354546E41812F74D21ECBF117CD347BC2F10BD386D9144F083F572A2B1BE30945D249AFFB2E0F93E581A17912000C860FBD66DD01FE377AEA7B556C99CA5DA8E1730109E9C8EE4412DEBC5488C7B857F62C5B682E4F882642E459470B30E6A9C85A7E882D3FC64CDB752564CC67CCAB29765171B14536D8FD8A5F273EAC3430F40F757BCFF44A8673446141C3E206D3DF0630948971A8B847CC2C12A32883809984504184E0A30015C000200100631C1278C8A951C48058E27A56780FE1D4B1909C035A065D4402AA82639010064D788B15A57F192241342C4AC53E7F1309B76967D1BF46C247D422BDDC23DFD16C24A4DD5631126E2791104FD399354B4AE884C15DA775EA05756770DB71793A6A8C96CE7582847CDFD17C24DCFC6B245C4FF3E38F6090F08485901069039262CC58B719E6A29E120716F7BEE2D7B0233C20C13B92C118815D18C702EE595ED18005093BBD894B9C5825021B3F9C3B250647E300963EECF25C63E0751450114691EB07D01245B81FD112EDF47C959B077CEF8F173D17F310AFC778803FE1FE41B7ED1729C2A08E461182A1CE915015356B020991FF74D277C172919005C3FF06091F02234402C95FFD79BC5F7C9634129A613C21348DA26C8B50269000240174C2CB733FA8392FCF03A71846D662B32A8384E9259B9FB7421324E43A5EFF1C090BB850FF3D122E0412B8EFE88C804230776474CF5409CA110A80C101BB68D7740F74A3000000AA8013438C022E71B3A858070C767DDE0B0C377EB32C871654094FD8A604FA01F000184078941449AC750B335AE1F9FC9C27B882E2400C14E528DF554129A19858B6DFF62224E000437E6C9D0037E7B33705126E9278FEEB2412F27F8CFF1A09B3B339661948FCCE3F41C27D343F412021DAD1614DFB351FE5188F6EC58D6305222549841A5A67E85F8293A01E3865375629C23A1ACF43C21C9DC0FE0D12C4CAFEFF868413E13BC27802E8041BECFEB2C231A0A081748C1CD529EAD68E61EF010120986E5D453BC73C84658D4611F6BA2B60630B58E5DABE240000AA03173C67146020E1BCDA037EF15091AC329A56A154F068C1738A7CE997907CA0515516482000359BFF63BDCB568404D26C905683361B0A4782821D0843244406466E8CF93EF363643BD2F10E2529E32A13E08C9760741C4F5D442FEAB41E5844273C0924D039389978B538C62C90006C9BC7983BE848E5CBDD2961A2B5B58FA9EFC68E96F290F6DA575179E7EDA07F3B408FD375DFAF075C2784030B672061619E301309736090EA95DDCDA4DFCDAAD789EBD4524245642D8A224FE121AE5C8B1BBD441DB3C13A6A9F10114FE89D12B7AAA09463D308CD24A00A76D1B01109D46B50A7465A97B6C519B38E960B36C0736B0A5B97ADAA8CEB16ACA9AA226C271C545EC3BE770AEF18C9B624B091245CFD151C648B3D569147A313CAE6E403FE044E2D1E12F76030E21809874042AB0130500009ED73D23AA3ED33540E836B51B7A9623BC7471A2F9A0FB7A24CF4ED89678F3D4EE65AE676B97ACD9E6056F6753AED6F0135F25F21E13A8304711C3D8D5E7EBC808C7F8D47CF2F022162CB87D5DFBF1F46B59D617967E2718A9090671D7D8484870FADA3459010F50E9B938B3AB31118EBD448E6CA6D5E702D294BA0599DA00C2E14CC3BE248C0A61515396850AF26B7BBAC794E82138C315B9BBA7BC806CF46EB52ED5C6BFEB10A27BD7BB575A5AB9BB25365DE09B3CA54DBC591CC7A012793A3EA28E30AF71AB0B3636B30B8EE3634C96F10FF94FAA74AF39239555CE8700530E01E51FF8406A73438A3F0104E6F06A45EB35697064068CE5BDE21ED5CD1FE1DEDC10A78527B37A473C99AF001782CE6F5873ABC4EDA6F453CE18364C6F8D785678D6E72D7FDA44298D60F202FD7F1929DDD456FD6124F236101998904B1944554E1AADDB96AB52E5A1C03A9B9B4F114DAD49FED4BE17EED2C848459609887844584A71263DED144EDF28C6647597888081AD631DFD3A48DF69C1205611D5D71E1FD8EDA274AFF324202AC46A0C875B48B0000A00DC0128113B76C9A1BBA734061A7F64F011EB03855EF182B6F60DDA2136857C60AB53D19270E62F1BE8AC4BAA48042C0157E4C5C00D8B10244026883D4871FFE591DFE50473FF5CE0DAC786DF8DDECDD6AAD2B75F86C8C7E98AF7F3BAF6F16C8F0873EFECB78FBC718FD327A172DEF80BEBEDA70F1F5D5EB3FE8FD7B63F0A8BF8CCD7E173FFFDB137C36FDE541E3FD48F4741B92D10D36E409E5263AA2B04854D8D4475D2C5108EB5CAF738AC15F72E5565829EAC7DA60B60769219D308D84B0668D23E1AA1362800F66C6BA9CD895145EEFF07B788EEA55666633B0EA442D5C75067F661DFD5B24842924C39BB943DA67543687D6D1893259C193610BBC2B1EEF8437121DB3AF8481C4AD2360CC976202270200D6AB077B741DB9B28B8E54EA1F8748B00B180DC09DBE2483E9EF1C31137D3F0A5B03CB9FE8BB4C0410C0FEE1960E15E345E07EE788BAC7146C7ED018383FE1FDDD7E7F77DEFF69BEBF7BEFEFE2E8BEBFB7DFDEFCF7F7FEFB7BE7FD1FFFFD9F165EFC67F8FE1EC00D2F63BB77D1760FE9FB1B3CDA7BFBABF5F6DA7CFDE5BF8E9BAFE3568F7B515F9FD5FE39995CBED79312F590A293D265BD53F4EB0F44579CCB05E4424DE43CE6B8BF4FB89FD8F0E27775024B2181362B1C09895A488D28EFF69C03DB5A55DD3514638DD16FD43974221874429DD04DD032409D30DD0D9BCDD606C2D40C19F34BCE64F28F61F0F210A69176CEACC98C7791C47E3699DC1E5EE719F5BD0BCC726F1EDBFD2BB717167CBB895C24D2013977DA615B62D158DF6A1E5B41D9E88759D9D43B06FB1CB80185B52B040C16B702CCC132370CBB48B83314D82CA612D947B27540DD2AD576147D0FED1FA384E3D59475896D02EF25186438C074236C15BC87A13793BB8224BEF461D1B7F8B1072B9BA3A2C931D0E1D73B08897FDA000FF1E778DCEE9C05C013DEFEEABDFED5797F6B8B3BC76378A9410F7B310012A848261D47F59353766AEEAF12FF9074749DD74D7E4EDBAFD443B0D98CFF6072C29F2381E6234160E0AA93B6FB9D435BFE14365F13FF01A54EDD908B84DC8CA33C9D90FD32239DF07B30101F074FEED83CDFD19CC93A8227605F54362BF12E651AF1F909681DF1A4A30B021FB97D1CFA527B60BA3794E00C8C7605C308C76817053CB8E6946D634DB30E44801943C5182D06A88009B487AE24C25B1BB975A26E011F50BC1322C99259217CE92311001BC938E4482813896B000FD5C27BEBED1F1731F08F58FD020F2D04064AFFEDADF5FE0690188CFFC28E12DE21E36819E0FD6FBDF737BCF3EDAD2374022F1CA311579E40C202F218F184EC8FBDE0EA44B7D5E36F76974949D860F87774421266BE8C9010AA854EA21F3824EC437BA2EEC32DBB69C0844808B1D4FE00090FD1C0A8BCD86516090B1A45C9B008EE45CDF43BC23AD244D26D5D22BBE856141EC45E549271194D59B3A1A5105A479C245C6270B65D17F135ACD4695D015925CD7360AD4AC051D1822FF98C78550790E094895B074541BC3A3A781C6C0889B342C010B2781A05E6986E4964530654E020DA5D2CDD84237A9C8E089852CA8AA4AC4AD2FB3FC1FB3FDEDB9B09E6FEEB9BF3FAB7F5F666730C808A80950D86537FFCCB8725FEFE3E7E7F1FC2C9E847D06E04C098E189EF7FF7DFFE0630BC8069F4FEF710A0125A474FE1A61E7A7532B51D1FC220CAA30C9F1B4F4F9A588233852381CE1939F37F8484944E68470B1D6C214596E9A7A830F693E455BC882E771643C2E4AE31E39B9CB08E7E0F0961E3899B8F2A986768865027704742CC10729B7F8D26DA65A35AC0CA4FAE19B05570BB4B61E9A39C93E629F7DA9FD3D619710109AB1A584DAD1BA375CDC0226A5EA85659C6C65E9B386C1C983459912C2C62569D63193BA2AEE2A04160057443360AC43F614E8DF2383491DEDE1D58FAE3BFCCFEA3DABFD7E13878525F7E9AE35FCEF05107EBFFFD6FC4C0EB5FFDB7D7DEF8176CFCAFAFAF9DF669003CE16DDC7AFF1BACA9E1DBDFA04606EF7FB75FC7600A0B9EC05B32A657CC42D6114D5947ECCFD6F16B3CD57C617368E2CFDF42C2ABA0A709120847421B99F155670A12681D695F1410FA594CB9965D818409EB0898349EB7F3E20953D6510E1E580A098B01208304914232D73AFA080C184F9864C98BD42770DFD131C613DAC7C813DA57B47D4D3B37B477CB5A1708069F4772BDAAA503E3026D70C2FC53125C820545DB5DD53BD2B41DB975C330F3B4CEB47DAAEF033C98BE471106874AABAB915505D4825940D76A70067C5A91463FACD1776BF06486B0BB6270ECDCAA70D2B9D5878FD6E8D97AF9896DB8DFFE0645D17B197B8367AB73DA744BF4751C004EC63FED97EFCEF82FFBFDAD19F384B767BE9EE621611E2A5248F81330FC1E1266E984C5D5C22248B80ABD466EC5059D6EAC6BC686AEAEAA6C59F5AA5E5A692012844F8923E163DF51BE66F84D24E469860F9070F73F4702F7A27255704683061C09A002FEF479828F5771F46515CC7DA3800DBC0200C905FA4FED020B1A6CF8430FCE55A340D55DE40F5E9D0FA5DD65FD2763F4D30E1A5AFFDE30F701270A0E525896A5C1A3D97F30DA579ADF20C12958602C807FF89202C89A17A473A3F5EFB4C1BDF9F2D31AFD809770064F46EF5EE74860C307FBF597074818FFB2DF5EAD77A4196DB08EB05A2D42422E93FB58593FE6C613FE0009FF73EBE8779010D6F577B083CB005B02DF0D5AE7ED66A39966D57F8284195FF2FF2F248C662021B7D74B3E126A24AA59234143F64F309E80AAE0544192803692023C415F518373D8EC15C040EB1AD7EDE059D7D648EFDE002458878ABA8551E4FE93C9D625F92BF6745137095992DB5DA37F6F39474C05E6B02D49DF242C6540CD72060C9D62940D1909F6C70BE0A5AF540046F302D1D6EE826252019AAD4B156E009E8093B06EC0A052473FB4F1D87CF9658F7F9AAF7FF9FDD07794D1097948981D59E39420ECD533450F1642C2DD9F2B84FFC43A82153C85040403A657E040CE41FF5E34871CC251A45D248CE2A21DC7194224DCCD8DACE52884F09E3F41C2E2D6D14C86300F093959D9339030B80C278974AE198674394B6E5F828DA476BAB47349DCB2A52D31FF8C18FBB8869B97EAF017D8F69A18F7D17FD682330D6C21B6290DBF1BCD33D739D22559D276C8E8973D7CB2FCBAE19419DB5060E9361BBAE49D50EF04D837FA98BC061E7DB852076D22E3C5063F3FC68B410305140592F7BAEF1619C06070A78E9E8DD1776370670C1E38D30819F31C247C2CFF1209E37F83843FD309BCAE5F8C5A8B91D08EB9B2089FA509833096F83DEDCCF5CE34125EE62321472184B21012A6B389628AFC3F4042E2429D46C2D53412D03A020D00D60AC080DB489870D102327D4EDD8AAD2DABFE29A5CB122C4B20CD01DFA9FDBAF6F297D9BED2BD63661E28831F262001E7D26E494E0540A50F1E9DE04477AAA058B45657073CA057DB3D0E03781E3F02FF706B3C0FAF18E61D619D5B1D2BA07D8E99E01C8FAD134082D6B9D2DB975AA76BB42F413F58ED0B1D4F422F2A195E1A2FF7F6E8D61ADED8A31B13E55688216488473D255A5A70A6CB1DC69E4761A499CB4D22E12C8FF8CF586EC57CB1B07D88F8016676759E9833C2F302615FC7E4BF29706657FFE47A0A13B32FB05955E7325CCD9DCBCEA466E8F662CE10E321752585844BD00FD85A53E89CEC9B891B1DCCC22746D9110977A93E5F533A79BAF54E941FC99B9CE29C3592454BFABB9AE156E28E23B8A17742E22E2F6190E156FC46DC7F2A06298890423785840BF49FB66A3C2BFB18EB13026E11A1B5720224013543E75AC4132C40827B4271E4F8910C6BD56FA8FA367286C177D6BBB7FC33A67C91FA3FB4C18361D558F34CD5B6E4E6854ABE49DE290D4ED5E042050B8AAC29385D0A43D0559E851A26E111074F88C84EB5303B55C6F8F6096803C6CD270006691E7B5E45EDDCB0F625E95DAB8307A0132A265C3C183DDE0D017BA59C09ADCDC7DB3C64369224DA929DBD9596FE39093BA2A1B07CB94C240E36C3F51E9FD4C2E3CD5A3CC765C6A02A31B836B9D2EFEA9D33AD87536ECD4C99EFC58CF25FAC0036C322E053AB5537BC92DEC4C6586DDEFEB19D4EB16E0B2570C92DA5CBE8241201863607036883F645A775DEE21DAAE3BEDF59B904F1417A42AE524751C6CD5B85B7B1FD70221DD1FE2C96B8103C2AFB6E8727010680F11582B8C778B6767CB2815A1BCB8B79CFB53317CB8E4FEC54F3737EE4EDBE27A47DEA761A7C826DDD6A1D5B01C891E91DF03AE663A503767B9DFA27CCAD319C13750A1B3FF34F54CCB6A8D8EA32F34E89F24DE269A358A603C68F59A2F611A59B32DD90E8B6AC2CC9641594860666D2E0D972AA1467262C49FD67A375C1AC7D42D6B14905CFDBE699D8221F1B4B424B3C139BE3C1411581BC1B9B6B036539E3475055350F78C2CB4F58FADAE8D9E454C17DFDE5BCFDED709E405E9F315828F6AD71CE5CF17CBB36B53F895E3D94376DCEC9C39B91979A5847D8A665DAB24A67104D07AAA3A6C5830BF555F45D9DDC68D3BEAC744F453A0E9B7F91E195D2AC52AFEAD987B655308D1D63F82846E97041968C443992F83CBAE70E99837D60D945CB293BFE910FA6DACB6D94109A6346CE8E5ADEC38E0E9F68F63C9EF966D243A451273AB8CC0E2024AD516FD12E6A1F91972C25989D7E2762CC5C2E68C898C104AA61A50E46D6CE5133F4EFB5D639E9DC502003C051DDAAA37EC3AC3BE5B304DBBFB6233B15CA56A55697A99B927940643E4A50DDC1B266FB0898B7DABB5329688015495EE18306D724B062E4659CAF8359D95684010E03D9D89370EC4215B5815395C11602BD238C28540BC8B01920C12B32A0072F3FADD75720CAE6EBAB3DFEE5BCFC32FA57BCD391E89F85AB844CD912B3E8724A6B8B09AA1117CC6BA83ADB3B84E37C48B2F4D377264898CD0404C37916137DE2B9524CFC19BF42F8506AF8B1B089110915CA270EE2EEEE1DF949A5CE711080D4937E785CA23FEBFCD1631F4EC2E0344A1FBFC6B8CBCBF4F29DE8EA95A002DB1CCDA84F58842DF0162FB7E17CAAFCA53F872A60348DB56B0ABC483AC09C4502CB14F2A77351CFB917955B47ED06305AAB77C786CF7AEF9681E931FECBE8DD6ABD2E43247CA5E35713DB933E3B569169DB8ABE2977EE54D8FEC91A6FD9A8888832E6DE99870A706B400259052D21F33F6584C1373E738D17E5A042408B08F44085D82522CA74CC4340020200741390F7DE9DD67FD086CFC6F03BE82F1774C2F04947307CB75E7EDAC347BD7F0BCC01EC13611DF1BA78B1FE2635C084A76886EFA81B69893F60CCF7BF110DC841C28558EE8B92F590B0F2782AD8722DE009D77DEE05EAC0B2B60FB0485F8CCC594C9C8451F06E90D1E8C45C24E46B57F17D46FD7DFF0009282341B41663C959C61CE984BBD99184899616699E20E20958D70F4820AF6FC6DB3FEEEBDFFAF0BBF6F6E675E0CE47B060C15AB1D957F2FEEEE85BEACB0F73FC663A478CADF11E45BBB268E808861359C5140CC4C63AE666036F360E086043DBC3C8265A50F89024091348D8426817715488E23580017A8D1A4A0768EB0F73F0006F0553AF7B0FD8AD1B90C0490205980EEEB5C1BD3A7A56FB77ACDF0D7D470209F196F95B48183F465D9A3356D0CCF48A89A5F9729F6926F0BBF2BB48788D9170C79150114840E70F18FA6ED9C5B151875C0E2CEB002B953302B61008BFC129D96EC58D53B5791D738484C5332622FB937F877F0883178184BBD94898018CD11412F261908B844B924582D23EA1EFEFF6DB3F36581FC3EFFAEB6BF0FAB7FDF66E8D9F4D40022580139BAE28ED2B6D786F0E9FCDF6A50EDA40DD95F43D05F67BBA214B5FE5E05283254D37150648D821EA365179DB2E1C3AB88B99178804B388553C821E6057BDA2C8E9C352069FF7480ACE29186A5CD0638594BAAA346BE83BC23A355061F019EED9E081F5419F3E6BFD2BF29FE884E1D5BFD00977FF2EC6FC6748E075FD2924F4A2224CE1126D2772394F3A11A54E907037E93B9AAD0DA690F02FBCA87FA01346492E6A8884515A27DCFC1E123A5C278CFFD247BFB4D1777DF0043B3218E468938FEE0DEFD861DF14B08BCC823A1ABB5E59EBDF59E35777F068742E75317B1CC43BD13002D655B56DDCFE81498F9E3BD6816A1488BE8B2DEDD8A68C0612E709B228E111A4592F48BC9F8514320751CB5F94DD6395D7FE6345BF60CCC13169D6B17413F0D0834F72A777BA6A8E75F4FB3A412061DED0E5FF35121EFF0809C23AAA280209717F97745676BEA47DAC69B9EE0DE620611E18F0A1D187489881813FB68E46599D309AD009B390703D4327A0EF4801BB032CF397EFF6E887DE47C2A001557879D2DC23872D296E45052BE7ED1FCFDC65C6BA3C78B4F56D62ED637D82BA49A4259CA726AD4A60C8605FD43D6A1D32638FB2753E9F9C1FB185D7AE240117E9DDABED2E26BEBA3520C784EFFA880A5EFBC38905EFEF82D53DDB92C99110543DB7A43545B3A313828D2DCE68EB943439D1E13E6C8104F25197E6A91873D8D693E154A5F0F74E972C273559AF59D3689C1AAE036F60CC87768E1FE24717B594C64F18D3897C4759C9242365EAA7458F75AE1348B38A48C026C151878B49E92E26D877BE0F4818DF4E57EA445FC5AC2FF61127BACFE877944542124348C5073800783862AAC1D1873C41D42BDF638D0140226B11F13AB53CDF5158CE1F45D6802B0FCE453C012C0E1D6C728CDBC25ABDA17D30409EC016D2BC9A438902969224C9EF6F76FFD1D13694D75777F4DDB30AAA244BA367EBFDBD4557256D9FC015B74A9D3245BB681D3583BA2D53A0CE1B684169FB80846BAD73AD6164EE04C36A5649C2961625D1F002D933F06BB3802C1B35C3A110023CC12B69FE11098E48DC1E1854845F95FB1758B3F70AAAF92235D12C536193ACA1716631658603F01075DA5F94ACC2B7F4DA9DACBDE4CD711F4453C4E946BF79036D1F33D7C74F48DA5EBFD3D87194F61D25926129A12B1337C52BDAAE007D9A0D83C505A7770E30B276475EEEA7A8D4CC6D85BF130CE3E00A4B776E1437BCA486DF4C0695EF691CF94195C29DA1F3B3504779C2F1C0DA47F9C42012023204E1953A51630B106C32DD3A567AA019802734C800B3A419680634C2EF3411BC42241C03128031BBF22769FCECC2A26F5E18EFEFFEE0C17C7FF7FC63FDEDD5D7B6782BF855094C267D8BB855DDDCD7B46D741F211E9615C0895562EAA62CC1CA36F6780F60D8F8F7255CF4788295A0BCDD5DD8084CDBC113B3086C43D27764BF62834EF08EA857655E85BA65E296F9B1447A973CAEF94C9B475A8B474CDA27663284F4C46AD6ADA6386FF06A3D1C9C93119CC4D3709AC776F3D8C321B3A911B4AD532138AFA5759291F6493485169B0AE3D09D563D689DB8F8281F5AC327DF0429C12B9D533138C74B0B4E6EEDBABD733F537378E9F663B9F2B2E27081EB6EFBD40C4A5AE7BADFCB2EEB74C3AF0591D0BBE9B7AF3BBD33BDDFB50657264A17C4E04733BC3243FA977034069718DA1FC6726D0CAE8D61D71412BE0EBEA081375F19FDCB58F4FE95014F495D31A3A3199E5F58B922228C836BBBDDD0FB61E36113FB1C2762C5C29B049BED86C91789984E6B366B7A50D678BD0E2041E12E191DFB459CB1CEB5DABB35F1E49C79758728CADBD8279F24BFCA460FF6F087F3FE16BC604AA83BBCF30000EABA8C33D43EA1AB94AC2838118AC06AC7710ADA06FA97E46FB0D133638BE0FAC6E27F9E91A7EF2AB0E2C10A320A92E8F005C0C04EA93B326F97AD00C2E00A80C42FD9DEA1E6577963E01AE804194F40335414611D8D4527B9B0B5ED44C82637EB4E28E85047BF3EA65359E9783286803B74CE50F1D8D9FFC06F98DB15F83525130F0DAFC26EC1AF1332BB4592D0722F7C084D2BD409FF4E2D08EBA8DB1F9C71A7FE5D3471304C0951C31984F76A2C2F1372C7FAF0DC3B3594E4A179336143C13E0C986A81692C938D8C583853301416CB284E7E411388B58EE497BB590A213B7130AEDE442F2A3204B0307BE78884CE29367E84DD16ED8E9AD23EA56D60D25DD6BB52BDBAABC8D2F8BBA3C232DEA6DE81F1FA57EBED57F3ED4730E8DA6C15A77F4844A22B326080AD636D9AF419DD440C4D23500B383A87ACC858470B68E148C09E90E621811358E5821B98400C0E147D5B32705621F68ED736B167B0E81EE9156DAFA4FB15BEFAC13A122760299595BED009A257CA633846364DE33EE00991111CE6DB447FE6C94C468BF1848FEE99F95C64381321B9FCDB260B88632F6A5999D6097F28373DF81E5E921D648EEF282758C9FD6FD3C460165DCECE0AB90BD3961697C43AE2756AEDDA7CEB6866FA1D5A4755A5CF91D03B035B03B8A80ABB2DECBC6090374F68B30EE613F16A2E51A4565DD7BE29ED73D758A5BD1B7F78ED0627AA5D50E425896D00784CA7ACE25AFF22B1752A7FC1B23579850F8EFA2CF9A70A804459C67833ECF1DC10DA93CD7DC5DC97F9099EDB0704872FECE33C126D0D95031A4E00154E1BDC030B9150C5DEC01C0321180009BD9031638C792A98B0A8473C46C2EB542973BAEFE7CC08C37DFAE6DF2E838E9130278231F1B2A1EFE87F8484BBB80873A16F2FFEAA739030DB7194C3011646420E55B8FF1748388F9050439DD0BE605E55C6D14D35EA55887320DB7BF0A7ECD75C5992AC4DC2BECAEE01A10AAC4C6A6E137D8BB945C32DABFD73C7DE663EB6D456C580280100B7A0828D14D42CFA5512BD15D08BAA6FE1AE8F00D853AC3D455F978C4DD9DC056DA080210547738F98FB44DF040524C39DDA2A8E770624B8871AB2E46AA8169A0056F8B39241C25430E137740268F6F19F768D4F6565FFC9D317D10969CD10B971E278C2FF894EF8AF9130393C6A1612A6DC479330B81521B98F91104F9E9D89049E81D73AE385CBA70C30E01CCAC131F32BB8D2FCBAA7C8B2B3A7D12509078F6F0024246D553637A9B343411BC05AF52A7AB366118251642CDB275CBE4A32379C441C1A4E904BE8B0DCD765D8FB0D00D38662EC102E0AFC69EC233130F688308DAC02B50EA8BE09775267CF00C60C6FC82BCA01BE2D80047A0FFD920CE6DDBF47022CACFE2919FF69738AFF0409791978B36DA4B4752490D0FD537A900A26C448F85FEB845C57E9223A2176164D34BAFB8F902063BFA3B21CD429C0C005B55025C019EC7DAE22EA9EFC49B2B6A82CA38348DB92B54DC5003C1099F225EEECA95491B465455124B6CD91A0845333956F32861A30450F670862061E3C937C013CA0065057156D4D61CBB2BEC52D225005A019B614B6827A40475D41CD2D0083E21630F738403D40050CE0BC7DA20665A43BF099C7A175149928533FD28C1F2F22060FACDF98E88831354A2C59EB937C3A2C4F997DC35CA1D12491854CA3099D201873271A1DF2BBFEA21C9D703689840487D92F2D07091FE61DCDD1093C1CF17B76D16D16091F7851E7584798BBD5E37978C0138C7509CC21679F58BB320E9F3D54DC223052B08E1C58D59460FA90BEC6D465D8E4A566C9B0776950D683B269ACE315C6AFB35DBEF1C7F396BFC9988EBA82E9A8C013903D5BFBAAB5A7597B0C767A4082BE413056B7239B3B4CDB94312CB74D95CF92BA228396B00BAAB5C3F44DE2164C0F236BF04F2AB0FAE108A6115005CE13F033739D40F2928EE649BAA10BD709E23C319932EB6F4A636478C29F22619C42C26F68A1C7B080137ED4B6C8C0FBAFACA31809F9D1E23C8D9A41C2227651562DDC45056B6924DCA78EB948B8FD4F91500933520109D696626E4AF62E31371012EE81E295D037033C817C56CC6D1517FA37C5DC2184C8DE2E437828B2BA243B3B1AA6A3F21622EAAE44BE62B33585E0117802DB94C99A0C48008460A50EECF46063A15DB4454021C0AECFE90136C0B0F699B94D8D5D5417706E0036B608D8484E45B3F70CAF6CF8253E86199180D2AAB1A08CADCBB046EC093F92D829E7EA8119BA422021AD4CE2BE49F1FA7B985A8BF10E7DCF72A3CB53B1BCB8C020C3C807510FBCDFD309221735A513FE1BEB682124CCB28EE676357DC86060D246FA5DDFD144BDF234126699467948684648C0993A7B8A5724D696046AC12DE0895F66EE81E4D530A6E61434E0C19428DE9EDA6B5840A0B515A575E6368F2C6313870E224B4638C8B142204B8AB18BE56CEA065196B869B4823A819ABBC4D8429E804B1FEC1F1060C97B0CD402680978485F25EAB2646EC19FB28E4614A82AC3AB0055009E8063C9FD220A3F079EA0606B890786356B0F4912CB2C498D4D48ADEC27CE98D315051381E17C89E2C42924CCEFEF9BAF13CEE9E2A6514A11C53CE17FA31366AEF8593A81E5F3843985CB79D6113EFA9B485844277C8084B21223C12F325405055863D43B24CD9AE6ECE2E46FFF18E309DA8A4CBF2A764153BFC2CA24BD0B3B0003699FB5CA96B9C98024A08AF882550AF4AB0CD60D5B558061C39FC61E68098E0C85830196385002EE2302A050B60C57647545B10E1966321598B547413380EA01A34803606C287062EFA24E086A6A70A436EB7A5055838AEA95985FA2BD732ADA92B6AA2A06172F8CCEA5D139E77266B61B06061443B14447582CDBAB635FD8446A26C698EB1616F509A97339818BB1386969819C80B818A2C6A7C015BFD5F0448C5944949321B6E7C908DADE45C067CB62D163FF8A4BD743B90AFA5D3F96DE959FFAD31BA4E51AA5DFC5F073E7DCF6CB1AACE03F5FFD699DD0EDF52EF4C1B5394431506E8C54CC38BAC883C73C5AAC7331065770832EE2C7C33C1141658C2B0BC198318889C773A3C78FF0227C0AAD99956822ED99951D506B75CE4DF889B958FD2B8C31C33DF02B0B999A3F6B7371F0D870782E02FEC4CD13137E77BF04CF553812A8B3CF9C82EA1535D8FEBD430D082A9C001EFC1A3266DCF53F49C07561A127DB7EDC69B3A82BFC06494A3F881AC2393014C22F7F12ED690F75B0FE61A1AB2B9255509DB26617501BB055C9DCA7003858FDFA36967B9A3B54DF00030CD8B30C48F0CBA660CC602309F751EB9889183332E607D63BE196FD3D8E989FB17BCD4BA5EC3594E9CCBC6C2ADE74E652960F60603849284A46D93E6522D078CFB31A093E6BD8256FD3A1E5F0CEE835D38F3E47F1877B3AB8A63C032F5ACAFF1609FDDE391BDDA5A67F4F0F84BD4F8D854DE2C42A2A2851DB301D214E8793A7FFBC134FC71CCAD16D7865749B232FD317C32D1F4F3A7565565C39DB0A329D7744448CD92F804EC05C54E009E6866C6EC97E89593B68177945C0868496C8B19B2CED4F92B689844151E2C51EFEA76D3085B36AC21982BA8A76110E4C10DA20A6D17611F77E758D587BAAB143DDAAE1564DFB4005FE00CC01B7FF43CD2A6048C1DA654E51B5412AC09B75D009AD13D009B459A3AD066BD5595005458643D5450A64FF64BA32614A3B3F4CB6967815F248FBA7BCFAF99E64074FCDE89B3DC1191E4498994E7189E962E86981DF03177736CD6E7E942D2A65C666100B5847F3F24F27EE1CF479065E58BD29ACBE7806D71CEBFF019EA28ABABFEC173EC90DC67169682C7722DE4C46DD9C4EC05973287F7294E0099D3A4FB34B1B45B30CA454E9E6E012CC0AC50733FB0CAB993B0D4482B5A3387BB2B5055BB06C6DCBF60E184B32F0841809E62EC2807C95D932B18B38C84BE2EA425D67C6BE6A1D68FA368D01024880FFC3CDE49B68C8C9BDAB6C49D1B780185058F4B0F7C342B78A1463089B8A5BD6F535C003184EA013548081B9CFD09BB4A3D8BB3AC6988F2800A009AAA046D18F7444DC83309EC07502C3DCE0D8D9976AA3B2088106C69CCDD44824CF7F9A5DB5DCA139E631814C4AE9A4DEC8F72971C69C76D4E6D189A74924BC3CA00B6578C5B0CB0BAEE33FA50AE9ACEC2ECFCABE9B5DBD999364917CBDC8981FE9445396E94E2D19BA7C27B8014B18F3DC64ECB8477C76861A3E1DB3B293AE3C193D909DA1912D62BE44B3C22FC242E23AE1941A6B127A8D7610036E91BA25EA81290EA4F4C845EB7F490618C8DC47A4EF1158BA602409AB8980E5BF43D99AC2361434A22235A16D137D173BC86B6B542462489F25495FA7A00D802DC0A2D7D6657D93B9479A55244041903AEFE0D2E7D765750D3003DC1AF3B4AD2DDDAF1A986151A37E050180691715F4F53A077AAB866BA857A7AF93BEA385301022E12CC782CAF799A6BC43F1DECF67D3671CAF0BFB8204639E74DA7E200F61646D28EA986FFE5427A4184258A993936D31231C3905125861C9FD79B538939ED309DFD1F59F788D122FEAF1BC2E3E792ED470C81ACEB43E14D611E2C100BB7D4B0E91704082630DA70F166544026CE52B44FA26E172DF96D40DBEEDF3C5AD6E13B6499CA285905887DBD266931CBA533FC9F23799ADC9A825D42549C5A08102CBDD3A20DA06BC9C8CAED2B206E4C13E600009A0232A0FAEA91B989BC4D6E19DA97ED5449F6E49F14AE835021804154485B1A68281380E9130B194F36AAF7E0709693C2C848487DF7701CDF31D4DF79549E556DD8BDE163CC6FC2F919082047C0F2112EE535FE38248B8CA47C26C7F51266EB0101272E76AC648F8300B750612FC4334B3DD3DC52BDBE616B17664E78038FB0AF7A52A3CAA20FBC7C89871A7FF842B9B6DCA744561DC594408F2017B8F299F2551C6C96F83658C6E52B4A3BEC9E83F5D0E0BFFF13F6B07F83155D7659EB1AD689BC03078EAF59E6AED311E789631DAB04D011EC62EB10E9873C4AC6D44021283322201D88257929B758281F12302263EFC00BD6392D2095924CCD4F50B212194B9CB34A513164DA48B5FA77F462224E4F659FA48277C8884C5CD244442522E33F56D2C8684D96ED3E91842C6F2F91009B9189846C29C985AD6851AE7A2824EC0F9B3BBE8F774CBC04B65F7104C0FC480B52D8138FB7270E272422CD16FB88FD365C9AB99321AFCA022B03213D585C471F24932F755750DEE493C489897BA1C0698C98A04AF22D36FD82F1290C09664635B010E8DDC600B1354ED323180A31431BEA66F29FAAE821540DF003F9A57B54021F865B9DDD0B178AD4244A0AD7D823C097E80CEB1F81952BFD6C48F3117093C17F5A31FFB7FA3135248F84DEBE87FA71312B5B098F38DDF3C8D8497590A21377E7C3DB75C73863688AC23E4BB7374C27C24F81C09415169D58853C0AC3BA00A2E18ED7BB2B92D79650A04DAAF63644D0E1D4484ADCAEAA6A26E29644D62BC750501837F1B836B9868F445A22B184C20444A8301B38FBEF21205A74CFD13DD2C601E1EF6985F928C75AAAE4AE6360178F01E78AABA092C990012BC238D2DCB38D570D7F48F6D8C2E834584361271F7251F6CA412864246D7E8D14B21814CFF481112D894F92B96388657C68F5361E6F89EFCFACF90078FC3C6F16A8496A4B740EC681A27F39B33FC7BFC1806F5D28C599864E38768D14F6A24BC2218F3E88A36CBC8137AD376FFF5D49FF3E1C1EB980109A3748C79720761890B6E6ABB096BC12791C0C67793DC405C9C18338EBF63BAB35D46D8641A76EC57057E2C2AFA11096CBA7E3F3C094B3769D2F313E932BA507B67E845ED3568B32AB7EAC4023D5090BCAA621F60BAB4B5035A82814E402470970FECF7B0831B050A000072AC6EC9EA2E6F79F459527715E12C82BD9FAE72CC6C82E1A3C07A36F609DC034FC119B5BB12680D82A6D13626A57A75EA56741B132BD07FAAAE4B70023A05B5C136CE3C340E70680FD84E5C27984159F58FEC66DD0D6A78F4CA5650733BE7DA908F39EB354C11B7EA9CFAD9F98D6E2C51734C07A4776EA35CA0F47913D241D7E95FDA3CE2C3E5325B5E78A9A56570A586D2558757EAE81A5B0B0FBBD8410CF7B6EBA8376D5460150E0B0BC787A53C2A808473E5F5497D49FBBBEED86BECF5124B2A5D0DCCC1F9C2BB410E2E1920A1379D8BBA4878218B04DE130091F0324B8B3E4E1B4BB1608B5F585BD10E92460ECBE02725715481FB8EE03B24210CEE13BD94A33D6EA32AFEBBD02E128D9280EF8EEE321186C47184B38623FF69977086400443E89F1260C9DE810C7800ED0A26867340ED82E21C4A782C8281A4B89C33040D8F135FD8A6A975A881C1A26EE32435200C6C0BB7799920370093075487BA29039326C08A77B05FAA0674775B221BB25E20FA3ECED791B42D8488B94BBD9AE19DA84143778ACCAD0124C05E92EC22714A9A5DC278825120388D07B356157B47F78E4C1B933234635BB3F6347D536DC3EF77D3B70F2D7B1FA380705DDB36946F947C53C9371609255FF184822CA9892CAB6C45880647755557D754B6AAB3159DAD6A6C2D232AC83A1C0D754D8F455B37F44D10138EDA96A56DE91A9EE8DA86B818C996696C5B283B783477B9ECD9D69E6D0AD9E5FDB9F66DABE0C2456BDFB10B0E1C432938F6A10BE2143D71744A9EB8621F3ACEA10BDAD2D8D27B372217B51F49FABCCF2789442759C922017442A77DE60435BF596F364F9AE963EBA4D56AB45AA75CCEE0BC897FC6728A7FB6CF9AEDB3006E6872E177B6C3A74C4BA30D4F699E70A9378323F857FCA0E6618FCA5AE01FC1399E047022A4165E0FA5CAA5E2C1D12B795EC9F56B0EBE8EE881295E01EF0CC23BE136908AE796E13B74ED03F86EE1DB869F007E1AD3D860ED3A05631B28A87D6898FBBAB10B0B832ADF247503E9ABB525070D1F78B079880395952F329CB30D9E7DCD9329C0FA87158FEED14F12905B99FB94D806CFC126BCF5DD160AE34D90003F58000A86102370B70688310F70B99B07B0DC256D4D71AA14EB9D4133AC2ADCA94A2D8C2D30CCB6A8D95E05D69061C1BBDCD6C49616349ADE916BEFAB360F6D8875AF10AAC01100208443822E09410CE0095C5C56C9129765440E5D61F890382E222B91ACE291E19131841317716542D24F8984AD32C01E5D0661C90D2BC9FD24F9B7F80DCB2CFCD711D24CFE42A4CF8A6069C97F9FA223FA3AF87F9F15D8A3503E8782FFC1F10BFCA88484474ABFC16B52BA4C937F45EC17B3BE84D4CE4297B4E81C9ECB08FFC2F108AF8992DD89F89B4FE41BA562B7FA16DE2F7E38E52BC5DF54C8D75808083C844222F9C2F89F2412719184A210990B7E57B04261157FE222C961745896F44DAAADE1C2D3F70C6D5735F6606B63E61EFC40947C91CC4D39A87B681DADF0C201D003C886657545F22A0015FC09C8AA0C568CBE47B56D59851D7C9BA8DBB25160A03D000FDA9E0246918A0692C44027D847B0F465A7A80306AC128E22344B8A79287B65C3DC64600569ABB25D5261715BBB6A70ECB6CF9BEDABA6B56B047507DE0A6828AFE6F53841F44F824EB7EFD77D6BDF109F0ABF3BF1157C8D24560E4B89D0A508030206CB314216C3C00412224165B292C0203ED2156DFAE649997EC10FFF4544325F6DB04A96F822E0FF85ABFC0B17C2253C89574974E54B8404F1DF370100C66200AC86A2AEEBD1B996BCDBBC2F61E69BCFFD389963B407719C88CD2BFDC3257FC6BF29E284A3E56BEA174F0B5F0CB05980842708068260F8940643B871E8EB54DFD2C837A26EAADA96AAC36ADCA0C68E6A15747D8D04273E3A4C398E70EFFFCC5D465FD10304EB5EFE8AC4004732AFA2CF142C7CA40D3BC80D904CAFCA4097C134023309C0006615A6DF191B988F8DE3CB4115ECA0BFC82A2AFE89E15634EB10DB89D905E696CCF6991F9C782DD0B61781B5AB07C0986BA0D46C61E37AC77EE7061B93580533725DC9B847C2078EF709FE7588EF2B47F0FB0D577F7CF25F20410B9751787D1E0CE2FB7F6301A5245C25E1678936CE6F2949EFBB4BC96D7429735B72CFC407844FC445DD00BB11DEA7C62696FB1C0CA4CF735F3F5FC9C4AA83653E5DEE2F98B281A70130295F622470B5F02902432A6908B389D699BAA1B23546D798B6A58166503728E0415B556035622EEAAA626CA2F1C356B17F51EC4DC2F0C2BAA46E030720C61EC302CE151CAB8393D7D6F05FA00095652CD601B410B0A3AC02F38E0C037B7B294E41579708C6923715630D132EAC03C5ADA9602CF2117A20ADF6650B8E3620A1EE8036000C003D708FBCF615C2C03E34F1C30888739D1022417C1DA18D340309295DF11B7BD80C18844848CB5C25907974756A1989853E0B7E396F8FC53649B28966DE339B7E85C99BF3777A30DB540183CC2E3EF13DCCFAA292253E73AF49BF8D48D48F96FE2412C80412A6F0203443160C72366334FC4F5D67DAA68686EE9AAA6E69FA9E4E57147549F68575F455215FC0CA44D8D065D93C50C18431F671E0AC8A4D8940A3CAE4AB20CD040C24BA8A3E252002F22734D0E0A56482FA01EBF471D72F32750DAB0EDC02D6AF99D83D06236BF69EE656CDF679A0AE32B08B3A1740B98000B9F69EEED7EC161FFDE2945DA116DCB2C34D6130826561F9095D196903848132BDEE27ED25755124CCC7C0E2664FEEA3B316502E1A57B2CB285AE51316E007F8E117E3E74E7FBAC89DA0C5FA2DADF1E67DA8E90F18013B415D68A386DF7F4AB3A57EA3BC2D8CCED50FF3D5824CC8A481948704F4FF2C53A11980248066D0B6356D8DFAC7D8E5058C767D338C110024D8B242D7B0E32F59479D207D41458129461B6AAC6DD8966CEC9290BCF11A68EE5E052ABCA53A25B5D9B0CC2DAAAD487EC9D4D789B58D1691BE42DA67ADE6898F6E84632FA8B95EC56E367CD40935BB79DEF2EB0148FBAA3378E863E61380FB33D7716924CC3387B23A778227CCD9D5A21F35DA1DB570235FCD9843781EF2602D63F6246B6BEE324AEFA0D36F6601538408269A7E858F16E8E4C78C14540CEFF80D6778C29C3736BDEBA7702BDE5E96B04D2121BC12D1802C12124BEF03249044397CCD2A0725723348594F430A0C800410F24D410309340322C1533E631D33528568992312C0E059C63E5FE827059309272A60633B39CC3CC23C0B6D3DFC8794382BDBDAD5CC6D867D2B301D55C1ECEB43C52F595ED986BDDF299ACD13172061AC53A760B8073A5850A016AC7DDD3FB25A75B09A5AADF376EFAEA77C018423FFE39E10F48EA05E5BA618C3061DF44D612B842D63A5A90E96DF0AA14B0A5B22E4ABC243E50CCD4180FB860AC256A8BEA181C089B10D47C6969139C13D70455D01339AA8E8B7A1EA1A7CDD04BD3D6BFC21385961EA3A85FB55A095AB0CEF5C57D1F7C26F4673E52BBCA5C864174B4158324B343C86FB1C1AEB62178413F85CE8DD12FE13820E31B11786AC66729B5413C61FDB155F59F40A2A3F57D1505CE26E1CBC87FB67C413BF4D18F15AF86AA935FDFFDABA7A1637922DAAE005136CA06003050E3CE0600736B0C081079C8CC081050E56E0C0820D8C98C008074638188493A17130340E866682A19940D00A04AD60A02718D0240B72B0300E16FC82857DC10BDEDF78E7DC8FAA6AD9503425A9BBBABAEA9C7BCFBD55920E7E0D95C396DAF95EF33C0A0ECA50EEBDEAC754DE037F33BCB303EB07E637B4CE11B08F70497CFC7D4D973F0857594C282326A587BAB67090A49E2466E812391E27FC980C874724036673F40A3EA1373F99F00B3A8FF9837607CFB8646611C2636EAA8360DA3FE2D794B9E4FC5C12444FF6B883EE319720FACFF6862FE5BB9DB2979B978D9EED8F8EF6472FA085F6FBBFF0275011184CDF0C8BB39C5FF5BACCB39369F6713A7E3D1CBF220736779BFA763B7A3928CE66F808EA6873DBF41EF4D03FE00F48922320BE878026FF34D546A0AF508157292FF2E2F31C1EA65E435995F6F27C5E5C64FAEBA5F61BA6D0606733C4E8E5A5FF61DEA2A89639B3E397B87CB6B9AE1A299B9B9A95B5C430D06FCBB25917D5226FD68867F425CFD9DCF2345CBE91939B15A31DB429E714FC1F3E3FD60B564AE9732DA7E985728B82FFC6B7426770268FCD8AF762C125F29D38ADF3E5558647906E20B82A34BEAA97F6B2C439A8F0F2C2A22FB42F4F21BFFA9AB1030BF6BF5E4B07968575581A64B7E525C68DA3773ECF4EA7B30F135E78957130F9089AE1C0CB797199E920A3929FCF71E1FC749A9FB387F9D91C77877D954B7269902D78E13B50E4857489979C7352C6BF0F214EE61FA768013201A0D497681615A9CFE6A7AC64A7AC4FDF8D5100A1D9BB31824994F9C974F2768C4B50E7A7EF27D3B7F868AC7F4ED7EDDA37074468506F03D9DD9F60DA0F1136C0100F9E74E71F27FB0FF6FA47B0839DC16FFB078F7B3DF909D45ECFE8B4FF94CB671D7E799F2B06FA8566FD8EDB21828237FB874FBB88B3E116400CC409880A0EA66F0EA7C730F3D3E9EB43780300024FEB435FF11F20979C8FCDDDB6B961193E1FCE4E308898D1029E80E8171AEC3FA4D51FFF362865C9590800E0CE1DCD2C982DB67F91E37644A420AF763C19B02EF14EA185E72C0DD3A813CD3715017D83223410A09312021A83AF20981CB831CE608237B715AFC5F1B6DCDC5869AE0BB990F0C211444201D0E5A6B952251286DD23DCC98AA5D38095225696FA51017402B8827201F48DF659FAE0F5FA5ACADA8A3C4B834BF8D1CA68503BED95F03C4D5E4AA5E620E0AABB263B911CB79903293A0E2B61FED2786895250C07FBA685E91039560B1A14CC0E9384560A6D9665516AEE04154865D8ACC22D173F254830E08D9460A1801FE284343BCF66EF26B9F08A77BF4AFAC01014F72D989A3F9B833CC31703D0009480DC902289E69F3A836703C60C0F3A38A74BD57438793B013347BF0DFB8F816FF264723C10E5D3E5DF0A3EE45ADBFEC34EF8DEC240F659800C07BFF09DC1F32E2EECC80ADADEEC78307ED93FFCB507AAD11B5CE6DB5BB3A328A5184B2E543D1E1C3C191C3C455746874783C1D108DE00AAE6E011FD0099F0A08B01AA887E788319EDD0277EB7383B99F0FBC462350BFEE4358F041C6D9BD9C5C64C1D31A7A634CEBDD705A96570051BC57A000ABA7A91F1126B81CDD66A591785E08FC4D80282EB727B579312D7E5568E8A4EA9B02E30320E90BAAB42DD08DB5C1BD308387B531126D4D5BAD01B26407A55131FB78D94BA556EAC6268BE6D9A9B667BB701AA78D5BAD20230F9E37B651D2B3214E2F1C2FFCCAECACDB59161A3A3745D1937D64987C95E6211BEBA71A744AC730069048D09F45179AC5FBA8B86473A93DF2D3FCF820B6D4D938EA10D9D9C7F4E77076F5010EE33B82C7153B9558457F959467F72024D3E19BF1EA95B80370025F6B8918EFA7E8088F9597F723C81E5EDBF1CF68F0EE71FE7C317C30EF7D14D67EF27D9E97CF60142060A791FD7C247F49FEF71F345877B2EBA5DAE30D07588F3001F0E1E753A30CCD039CC111D8FF45131A39C9B7509AF77FF4783688133BAAE6CCDF5512BF8834657394E29FFAC5F7CD6A1A15316532175F99D77FE412FFC32D54E2678153CADE30CF19DA5D060E570572AD28A8BC95FF9ECDED4DB3F1A42D9ED2BEA420F9E090E6FEFE4D33B079CA064736D279B67C055B73BEF784F5C23D10FA84A21B61214AE23454DC3048B6BACE0A3615ED56CD38E8ABD975205021819DA2F718E3281CCC1837CD96CFFD8C813B5CA26546EF8BCB81637455C270ACDBC9C9321F887221D6D4ABBCB5C0B846EF415E2818975FA813CAA532D57CA8D4CC5270C9F4EBADCB41232982B73BABABDA079AA5C2733F884CF24FCCEE7401AB5B4483594E9F1583515303D7D3721FA3BBE422F6400B4872F477D58E4A703EEBE793A403402013F93BF6CA45A7BCFE3ECBD88AE77E3C3C7FBFC12F3BF1842F41EF2D7B30747FB3D8F95F929C8317B3BCE3F4EC54867E2DA14640D8E32B8C53D66E20B6C55C528D382CB90F03E90E8B67FF0B09BBD9F00E20A56880A1DE8C6F486585928E073E380B9CE1DA76F83552624092708E6AEA8CB8148A07C2B88572B2E465D2DBAE2BBDEDC84D88096D56E8A76DCE788E82A947841FC5845459A8428CA84DA916126DF414FABA91089DAAF5053AA5AA2E4B572B23F88E2DE64920B398D1302D46A099F6AFEE15A45994147514705E88DA42E85E5AEE193CA088BDD491CD4DAC6965D52A7210FAB4FA4D0A4E5BACA23CFAFDBC5C8A38F60D321FA8AE0A64CA2C7C86AEFBFB1C8B9646EE42A67A028D616E80719E627631454C0017809468C5A8194389B671E84800FF00FBD9FE913A4306CD0289CD9A7871673F39FA6DE8EA8AC4E59A0DEE9168E0F26EF7BD3F7637A9857C38307DD7D08AD7F7526AFFAFA2D9FBDF09D4E3C3FE8088735FF003E702CD0034EC67976FFE7F6DBBFEFEFC52661ACF775CBCAA3434B1A68EEE241173498BF1B4105A9C9A14B511D4CE4951609001FE7E213043A3E645E0C6185FC1DBCBB78B5B52BB3673A7F2206C496DF9A2137BB7EAB52A7524F22B325505B9695C2D1C58C3B1F2DD1CEF1D6E9CBA5CA9B2CCA9E458B33A98DB4A3DAD7857140435E531104686EEF2B495C3A6B4525B85F4BAD5C2FD0F94A6D53D03F91063BA6C40B3A66165A3B7355B4CCF962B7F3943DAE7C30EFD6729B0922AECC87B45C8ADD91634BE97F361306E636143E89617CF42E8504F1B22F909400D0090C951297CAE15C2BA50B693A8A53FA87FEAFFCBA190408F8A029A9BD9F7B3DCFD2EE3FDC9B1E8F46CFFB42AAACF894C1B16488375E76C7C7FDF1F1E1F8CD00AE6FBFB7D77FC41FBB80238098EFFDB43778BADFFFB5D751DB237FF9480F4E0B740D6FBB0101C00456FEDCDEFF790FD3CE1CE8C32EE38187BD835F2D4D343C3AA07C3ACF52CD60E258EDD0A5665A0A915889FE518F4160E5E910DB095AB1F72B6D8134B8761514548D59A9E8169A95592F852FBAE75411CBEA7924EBADD2CF39ACE63FC81B6A86366315AC1A7844B317DE0F537E4143ABE92C3B4770D91251891FA8168109A553B1B4F02005A5FB84EF6CB63C8B0C3E37C6A726A65D37329837288C332E78982C09DE786D49B618935CB7A4AC7989B504C492FF5063AFE9BE50674581EE91404121C48F726696A6904985E8251C8D063F28D220ECFD872957D07A7BDD07BD6EB28B0794987D986467797E91C3F60F8EFA3355476FB8337AF8A2CF9F6F7C3D40997D18A38C5FF56727C3E151AFFFA483E07BF0B4DB6184B434F34345BB28C50334A22B001DF9B7AF752D510EDCD944BB55D93C692C586B6629C47632C7B9B8BC99BA3CEA131103494223DA9890EDA9D3F7A54134AE93171594CC8DF3ED3BC99EC82AD431D09E568AF9D6A84C94064B21E4625710ABF70FF520903C8BD27E7F5906A869C1D4061195B69996288ABC12F6B3F8DD8D931CD844B7246450135E5B1249B00B43585EE609B79DE1CB9D778CBA929BCE54E071A656D1A38689B05BA7C4F068DEC2EB7335F0F35233229AC9352F618356B6EAEE1B05E5B0DF24037D8B2654CCBD289124D5EEC9E5CB1CCEE1E0C981EF50DCE742D6CF5DC4CAF939040E4B71512068F6C4B4FDF96FB92825298C13E86740AAC1D31E7F02F8B7EEF0C55EA756B8AF0CF492F32AD54B40A4627C450BF2538DE19A5B57AE42158C025ED299DC351AE1F12853A279436091A1828C1D85FE3A4A9456FDDA53318E7B1D71069D0685988F6F47173E1F69A2D08DFA8E354D6CDB77E6CD2F71A754C1C3469D73A9623A4FB328965D39B7D35C10DB4C17EA139601D345CA8A5D3E2C5BCE21DC3DCA18D75D2102B17BC9F921D129F2239B31662BD2A14869E06A30D175579614D24467719E85F64BE5C94528996793981111D39EEB1210A3858B6CF26668C095A2C3A5E2300E9DF453AE2D6441C906968BB9675C7F50BF61437A658F996475AD29300795FC13173D10AC02D9B37788BF89751CC98AB34C5361A5D814001B1C201FA03F219FC896D9F079BFFF0BD3A99DE646B37BCCDF69D9DC6DAC7EDDE01ABCB42406C3E84D2C5F3612426CEFBF6CEFBFCA913A6ACBD0EDA65680E2215DE3D6C14E7BAECD134721DFB2562096C1D260B044CC341AC4332FF4471383C524708C1915CFC0E04CD36C214E0D1E5F93A4810FA15709DF48C2CFC4BAAE6C780090AE7B14014661C222E8974C4D9AB19764F96E7E6927B52F6786970C43E5F2F8E9E207C8AE427CD58A5B0A59AEC9AA34A59B74D5637A0B6C42905D2A5E61DA4F884863DDB26C415050E8CC2F525C866635E08C8C720284E8283A844B5BCB2B9D6C19C06D9E84B7763545ACDB55D19196AACA0A3A019E0F4F285356127EEE27E5FF4B2D7707E35ECA55ACB035B8880265FCFB78F46AD4D97ED9520E312B07706F1DF79BE2BC3014DE35DFBE3274FEF69744CF123F00FADFFEDA7ED323A2EAAFF2CED77BC9F76D04F7163AA78E7B170A2115E3404C570F10C793005FB46F9ACB6AE5E323EE852AEA8E021FC0A2E037D24CBC43D0A39145A2BB962D1B1C3A49AFE80B083BF6355C620A813833B1347B3769870151EAB488A13CD125AD45199412E6557C481C9F6AD1CA5CED70C91FCA9E62FA76ECD1671BF1979E5C8AEB035910F1EEF764B5E7DA824659E12E53A6D10529E556657955B448228F03A7D4CA19586E20D7545BE93229BA08F948EB4CA7C2215CE9AA7C95AEFD995C5FE92A642DBF8C56D966852B5A7A748C8B099F20A5723FA18EE7EBC84B9B617D50E9013E7492A51963923C67A9BE42107F0FC4FFF3EFFB7FFEBEDF0A13F8F2EFFBFFFDE7DBFFFEFB0D151472E32FA7C14D1DADD1B20CC870EDE10A671D7119CFD7CE8833D1A66C41EAC612858A75CFBB33439F66D69D0F7408866F7F6637F989D5F7CE84D94A132C5C1E4E547E0B6AAB765986003A0F0E01059ED77CBA0B83684A2F8205F513AE5A67522A20580F704F0555FBEEC2ABD0AB2AB085F24C6C2DD81E886D1D58307B111E39CC516B1CAE727029ACB55561A09218A9B47C575CA8D637F5EE93E391E63914E292A7B2045A2A14033DBC41E30C778E688E4196A8FD1695A2346C4EB1CAAA34ACCB5C8348BC5C6DBF80D9495539182472101AA86A4231751468806843C047FFA06A07965E6CBFD280B6FF9FBFBF4526FCE71B1D024491050F50F63928D1DCA40246E44D5CA650343721419E269DF01157B8BF0449B691CBE3EA52F0542822CF1AFB48027DBD0A300A23DE0AB2D74938D15E080B335D3B7515649109EB96210FDC68597DDFECA01164DAAC9B7C0B0C98E03B9D05AEC6A0398095CB3B5580E00F6455582858B9A494CD0E8115E3D7438B67AE8A560BABC4247DE79AA435C316FC52428F3216977CEA16A2977032C8424A819821ECDD12D4C613627F04CD7C526F363CA09892DC10EC2817DD5EAB81D76898476E48A9DDF0D7C4F7B262E87C464707C59553655930CD70590B3FCDB3339C8032EF58E0EB1B4E42C0E0A84A020307254302D154F7AC5BA26923B1016E40ABECE1B54A172D1666DC3925928552B6A04BB0B22ECB48E3BB1D0A9659BF0DE69F218AF72AF084472E4BAD6C3D2B86D76902247D2744DE897F8050DE4974B6C4BA6C2BAA13C190C05DD6C5640543006DE616DDAB3D4FAD8FA96986907E09FECA40E68C0A0E6D97818115DFA5D4FC9C4A7706EC44E7AD744212B905464922B1D45C08C54FC8012C2C6CA8522F2A8981CA53C066BCCD1372117AF26624B710BBEE9408206E5CDEA46F1AC716B64C517A5640D54E6382871563887B0315E164827C6D46893DFF30CB3F4F671FB9D54F08C06D5408A6C101542C8F24A54321715D891B556562AC303965E3A52ABF32AD26D329D3463F4BCB71C9510001149111FD719B80BF9F04D096625A993CC3A7AA5653ED2419278E85CA6BED9E99409948B328327C312AB0914D3280EB5D02D44942B0057A4EC0AC65A47D5548EDDF4E36306C4DB304CED297D23CDB138CBA6702A28F8E562DECC00B9B7F4240BC72EF9F12406913ACB8A6329D3C2120D1746AB28E512544AAD2718E6D4611CF39A5485BFA3AF12291438B18B96ABD74ED1736F01512B58F5F0F3484FD4159ED0E8544B7A6644A671A401CC5D8D252A272897A0074A05285535E95EA10B4C81A653E7D3F1CBCEC4EDF8DD09FF929FC8372A028840956CEF30E426F2EA5899E89965802E8CD4EB2081EE0EB56435843FC5DA3C38AA8CB23DA18B66E2581234DD1C0E8F96AD7A32B5067A2C2ECD33CC231C168735D051CC4905A073798198F31245076B25D471AB7C8102A9E5A8D305A0A0D96450297C466B70DB05ABB240A2F3D47213FAFB2088E22EA0A634204A537BBB4F7DD22B203B30FD3C89688F82A69A4D4B51D4B4FB53D1B2AD3E351DB9B715F6A54472176F2FEC74DA9C95A0AF7325D498647C29BE22277335186CC9276587C82840AB611DD5C075A70DE469F60A3776DB91DDDC79AAAFFC837514ABAF446B573CA752D7E732649F80AF168E00B5FB62F6DB59E449ABE1F8F5E1D4EDF8DB951FC6C0E2DA46B0E967295D2D1602044052C5E675A2914C92C59F2547247FCA2822C23003A968072268464914AA9EDEDA6B105873AC40F6ED46B751474B2CBE888C2F4376D1312E29980A1C6B73433E9F639935BFB469D9B648FA7BBA026F1724AFE38F7E2793209555DAE5446D4DDB58BCAE6D5ED7180B5E278FA665C7BF2543980414F9321317433715CC9EA728C2550C6BF8F7EA8225C4C5B7411CC79541757A6DADB02A3BDC0E7948EFEAA1544C5EDD982FE4C133B29819D094548B744359F7658DE41CC60E938CF6F7AB01B123B9A793709C416ECB44A93515C6AB0B8A54C63683DC18E9A5A0DE95A79595CE48118F3D3395715CE18245014894CD2D2D96A242070B7A8E02E06069E63B5005ADCC2FDFD5F4C166930A0719B9EE3791E0D12B843AE916CEE2658E5963831ADA515DBB6B90E4ABAAE13D4D6ED63E4896FD317D751CF3FCEC4D2B798B07126346D2698415A3BAC5D49736D385ACD8409A9C0582652BBB5DA5A69DA3B3B99D99E9C657A6155BB9D6B99F940AD658C0AF072F266DC7ED8F6B3AF038B9C0989520AFA67FC6A50D856EAD6F690F00875E24F128D6E265999208920C8893CF67F197391BA0B211878DF72EB56468EAA945A2660B1C3842078AA3A7A756B56BA9D8D5F0D5B3E169D316B62472965A44AAADCA4A2E8575F919DCEE943CE6D19EEFFE530A86656B0E5050000000049454E44AE426082">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.318+0100" uniqueID="3c1d4fe0-e83f-4bac-b287-b4e4f4ed717a" caption="An opened media closet." imageData="89504E470D0A1A0A0000000D4948445200000103000000C20802000000C27030390000A49C49444154789CA47D3F4823DFBBF714BF628A2DA6D822C5161BD862035B6CC0C2808D010B0316062C0C5848B0906021C142828D040B0916122C24582CC462215B2C8C853036422C846CF1036F71616F718B2DDE628B5B58BC85EFF3EF9CF39C331377BFF79587617232334E92CFE7F97FCE44E36FE3C9F7F1E4866562B693F456644292DEE525CDEED314E48EB60F6E3FBBE7B726B8F3502CD3870CDE85EDF43E9B3E667C4A063B0F7829DC61C1837167CA02FB0FB2C323198F58B9B722B7C13225C9E89EE51EF826F983E87BE353EEE814BAC9EC4E8D3F78979597C1A0F77F3373E5AC48645C7F162DEE9BC13BF1DFE5E3EDA01C03F79CCA59FAE0E03092D4DD7F36FF0EB3342F8F28FA007D18FF70A9F917DE59F7E6B683AB3D64133A7252F8EFEE33C25526620FBE4B278437DEC17DDA8E6F262CA3AFA3E1D5A073D8E99E74079783E197E1F00B6DAF5106B43FB81A0CAF8670588434F84EE8BF4502F0BEA501DE411107103D96094C8307D99F077D4F1E0D0768CB50965FF711DFCD7E6420D31FC4811F0E2B534509D93E38564C1F7CC1EB13322C13EE7DC43302FC7B36E7A6E65FF858F70F731812D01BF86A7E5A5617603D0D3E1ABEFC3165D197F208507CA94041A47A30B377A22FE2F390EE738A32E73F0600A5B7A6C16169FE0E1FA7C21C79776A78A8CEB2F2A0F6FD8F6969C04C98582698B72C1FC6B71396D13760C21098D03BED01E24740004303CB04E0006D87913502ACFB7927BDF56C02AB7FA7E6EF4358B0882EB73F8C11F393330708DC0F3934CB2F9132FA33C301947F4FA7B24F107974BFAB478C3C5B72078815CA914771232D20953347A9BA2071EC2177B052F09A09F2111E3D0B40EF4EBDFBB41F50F8E03E356DA7C6424EED8999BD7EC0283E86CFF5BE648554FFC60AF8E68E9C1AE141D9E77F21E3B0E37E3E7DA43517D3ECC7545D1F5F2A0218CEFC98669618CECEC08953E1D58FA9654EF6983973A14D043261CC4CE89FF586C484D197211A8AAF43142606BCA49D486870E371C0990556FFD6E1B9D3A63F0B9970EF98C03E4F9E09C57A5DBB3D8FC60E985FC8EAC8D9238A1D9C99F1D70890779F1C8E43AB923D840CF15F1AE8E7AF564806DA99E508993D841FDCF0813F88812F7DAE99F701D5FE8FA926833931F8EC392668374C7152BE52856F0D770374632EE400A2A565C20F77961834735826EF5AAA4CB3F01F998BEBC3FC7BD0E2598FFBCCDA84CC6782B609FD8B3E607DFC75846420268C4088002366C2B56142CAD0BF33EED0ED2463FFE796E4CE882583753A43CB1078ED0E7099055F4086FB22045B7568D424CB4C4670671670208FE322B330F3209B063BD9836F347217442A5A7CEB770B795838EE53C51286793E7B9CBA137FB8CF3EFBC16F59922894FF8112FA5DD9179BF398D723826F8F094400FF9FD2F8838C6456D95BEBF12384AFC3BA7A4B71666A430B47A13FD2E051F969140B895930D102A01CC283CE4107B6A3EBD1F87A8436E19A0499306219E2C848E284D492C19802C03DF2019930F1C87097860E9284989369CE1A6841CFA458D766B3027DE6838FD160C960F5A5010DEEFB97759052C89BD911EB26E5691330E13EB41EB3904E8ACF792AFEC97A2007F89A9666CAD6213788F3F4D9798755408E150AA64695F8EF7AB7A14D5016EA14A39279676AF860FF4566B911223E7388F7112C476A26989DA90B4EC4FF09CC05E1DE270CBD95EA60C39201FD2224033061F8653438EFB7F7DB4360C297A113360E96095F947794FAB1414A34006E0807AC892860821F7472C2E4C144C30113F4B80F8B900C0A3AFCD6EC31E769043624B86C111B6DE8ECBDCCDB01EDD8E43E88C70475C3B3222B317B0C0FA39753754BF22FEC079C29280B131E3DA331FB9187BEEF5C7923F3ECE4546DBDC1C0CF09AE99A9AD3508F6AD9009FCAE490084F62170BA84092E8629340B3A2E3766C4D140047C24B009C404B009640AD0268CD9205C1B267CB1323436E1362483A01FB71371937C1A4852F28E7226F72645F3607066F2420570F435A573093482E7FE7E06F7FC9676A3F32E87AF7A837B086DD47D1AFE9787E2CB7A087E9CF32FF234F06EC6DDB3D8044575E7F829376C66F5BAB51256EBFF08DCA4C008A01BC3A7CF429EE4D8C2CADEE383BEA078444ED3FBE6A2D81D3258B76E55E65FC13B3D7490F2218A7ACBD884D464ED388F2F36E1FB048284FE69BF7BD8E55819E20414A1C11089F165642D43247E91310B99B50606FD34280764F7BE65B8CBECBED3AF4EE37A4CC802AF2984A6FFC3E4D1FF8A2928846F9E7E249949B74F752EA8E07E8A9D1907FA7B8F2A336D13D4079C159929227FEEDBC89DEE0E73EF1A831038485A793F78C09D59041B1CCB75F417CE4CFBA19C2B67227262FC1CEB2615D8843CDC6D6AF8478E060FFEE936D3EA104F12D8105DF1B0B923CAE3031326CC844B62C2B162C2F548F8C094F8628207B00908F1EF93800FC2041165228C4C49307D64C8E01C0F6B13EE1D19C2483AD0949A007920CE53D5AFB0C55D3FD598E3A4A149A5A7DEF1F31069A1EF70ACD4BFD5D98609DA32B0BE2FBAB757096F4DCA8FF07E668F81FACFBC98A110B8C281CC31C1D807235AB31802141A199F09DAD10F1CA7D0263C140615E6F4794C78F09910B8648228B20CB600772FE1B2D8848B41FFA4874C20AC8FC54112268C0A6CC2AD2380B77F935A0E4C953B24FE12D984E99D1474ED01CE3268D7885491E3B1F5CECD31D983EF50E5C181FE40A0ABF2FB73157926DB34409BD6EBF2DFF96A5E3CE31BAB3F89A6CA6BB1CD83FFDF0BDF32E271EF87727EAC22773641DFF0D4BFB7790E9207F4DC6D8434D0B09EFE9831C7F2F9A5BC04B92381F8436130A0A367877E67883C9B90668A09E92D9A85F1B7F1E062D03BEA8159E03861E47247230C9ABF5A1A589BE01904B9567AAB52A841EE4868A04D84C787D038300AEF5DCD9F46BCDC7C66DC86101C79403C7A3FB9977C7C0C5DB29CA45E59A09809B9B31EB330925158F14C84F2F867F970B9E8CAE1351F8A8F042DE03EA62E26844142663C28C561A38990095E21C21A8DCC28E6A2DB78CCE1DB30C1B85B05070431800CE699A0E2E03FF0415B093EDEE48B840F365CA65061FC1599D03DEA0ECE0756F17B4CB8D6824C18E7C820947004C879472674A65E89FBCCB94CF77E6BC3BD1080D069CCC2BDB209AF6AD6F96A32A7F30A219547D5AB76C3C7E874162854F52F5C22CB8C7BD0CF9B3575BA76488AD3C7FE91C5562550D5E63BB1D9645597982A15E3D904A196F19ACC5B2E3D2D450CC713CF8D29D6FA9627DA4438C5EF7BFF85B1754003E31AF1C5D3A0F9CA366ED8DE24A2C1E466024CE89F61B83C38EB33079C77648266B20C4396888A09E3D03B3235354AA44A2E1580EEC70F9E7150DE916E80B3343001038F3CF89A3BAFC503B0DE1B02B877A7E6379E07655F917B66618EDEB5A0099D0A4FD95B321421B8E04467701EBC036C4AD4E3C3834FB320BFEC3EBB6682672725ADE480E82CA7C07D5EAEC92875577B76977DCDA162C84E83E85971F535C5FF602A09856F293298CB2AE3F0A0FCAB07575323324CC03BEA9DF63B8768132438C6F8786843645B72665660DF916382E5033B489606B726442EB60CB686203430A95563313CD017151C1C5B94DF52EC2ACC81DABDAE4FCDC9E20B0D7235351FAF85D4D2F52FEF0EED3F0ADA1F1E3C747A312B570FC05179F05CBBE2DB7E5425F69C65334ADDE3F0CC7A47369CB0DBD08C3892F8F1836D642A827EEE528E097AB0A07A9085AEBF6F19D28009816550079365704CA0BE071527DC914D00269CF4980942039421C9484ACED6383826DC08076C6D41050913EBFC6802B8003A00F7BD4676A69CA8D47F2BF5EAB2A13A5768A08B3B53AE2C834DD70429F9E000EFFFFAF430C5ACD0A70A4EB4A509BC877BFF9E6D02C74124A8705B779CD29A8E0F53476CABB0FDD28186AC757E3C63628FF701ED90EDE5463D8B24FFDD57FC5E99425983D90FEF30654666A2B37F687C077D7B7E73513E25FA0A13F8E212947BFE12F321BD935C930E12B0FDEEEBA87BDCED1C74865743B206266B743D543E922491A8DB820D8222839496416EAC8324FE8F67134C1FBFEDE6B7AE91ECD037CE51018E3CD89AAEEF4F3F844643E36C96930245AE9C0A538BF5601A765E68B21528DA3067AA6F35B8D4CC476AB137FFE0602AA258A1F57A3E32513E86350E535780F3409F8B1C1EB2DCE034C4FD1CFF277B0C995070BA41E72B1952863E13D259033F9E769AFE2147868722F11D27554CA0C6EC5B891300DC60103A87C4041721A87D631938A91AA59609DF5DE8EC7AEF6EBD38C1D1E05605093679AA6A0899273E13EED3005EAFC509F79A0F39AC6826589EE420EE147F913B54E026057E51E0B414F127E7CAEBC3A4C2A5B0AE4C84B1001E131E75C550033DB306C71E1CF8368E1805B7973F2C44F654E135180F4FF4EB00A65BA9980F53BFA0569C1DCA8F28024C0326281AD8DEBB89B5093793A130A1EBC5CA5F7C26D87AC2D7918A1374A5190DC2846D8261C524BB09BB8FC40EB828D9969C334C3D394AA822B44AB6CE1E3C3F2AE4C3A3DF9DF6E0ED7B880F83811C9D7C1D5F98F7B48779DDA0CA839A0BF762CCF99D425A7E7846C0B7150AD946A3FB891A3A20DF93E7B92B02E2D9A3F68B72C6C12F45CF351DAF8844C9FC1F67F30830CD4705AE6496B9C6BB871C19FCC1A94F0C718DCCD6258EEE4C81F90B32410ACCD746FDFB42ED77DA3B2226647EFAC83848A6FBC830C184CEA6E34846A8C466BD23953F7534209BE09C9C3FB627F9DE91739CF21ADD77F143F4E768101E19344AE471AFE587FF52A77AF2E306AF533FBA153E08B1A75ABB634CE298A05D0817946B9B60DC331FF1BADC16ECCCB30942091375F0F16148E06689B8F4EB0F93E59CEF20398F6B8E95F8B347541824986DF6E0F56383D8393ADD939E6382EDB670C5E6319281AA6C613D213364305DD9A631DBE68E6C93B6CE9FDEA9F040D7109CBE0F9A40ED60E68DCCF752021C5BB8B8A2B54261712785B6273EEEF36DA4B31CE23D0E98D31D943D3BE0D7D79492CEAB7FE70E05018305AB9FF474FF2B007758D6CD3C6228021470206048510BAABB931FB34213A4FF51E80559A74BEDCC8D2BD401A9B6090FCA86D8A09C27EBDCB1A89909DFC683CB41FBA0D3E362424083AF3671242DD9C36B61C2D8A3C18DD884349F33F5E7EE4C834AB3F38E529D29F25CA33CE80302F85DCA16FD3A202EF6820223F0C36C0304BB53543D5B8AB87F32089627A2950D131E74B1CCBB8EB5094E9BE6031EFF1369C2789A5E71C68766511F9EF9A75EE2E8FF5FD4F56DC1CBBE55E0E1589B40EE9367130A08E0351AA524AED4E02A0FD3A0AC66C36599B0768BDE519FFAB107E7FD575C23491C7DE5BE23463F85CBD602CC65C26D51E785AB2EBB38C13848A952FF5E485DACF8F393017C26847A34C7046B0D4C87BDDBC93B3605747A08415FE817E58CC05443DFF5576B56D8F040E58BF853B8B4A906BA7B699860D1FCA06C42BE29B598093252543DF833E8E78906FDB4C8B9D74C2882FE5C6BE0DB0DCD045D49286A4435C584419E095F5DAC2C4CF836564CF0BBEE386D2AF6C1CE5578D514148C9802F3D48B95538F120F3684F027DAFB7C280A70430DAADD15E7C698A53152B76C0CAD9A516C1FB2D7E658BF6A22E626311F2DCA330D620F8B0FF99B713E4C5146485DFFC1AF22FF9873987D578519B3E26BFE99184110AC2961F6BDD0B930929E163B48B92E8CA0E3480C05CDCEB957D33579910BB5B68530E1ACDF3BEA0E2F4C59EDDAA3C1F8DA15D7C61E136EA4A4807DA666764E68197C2BF17AC9D9CDE6090860CB0EF3ED80F5916CAC3CB34EF983F59DA60E40DA15094C049B851F192F2D83C26BCCE4CE25E35E3059C7F55A5B7FDD4DA1CE234681DE396053CB0A9D4B2D046291C55354F7CC60780F39B74AA9F640C1EB62C2BCA8608E11F04E214DAF663E08FAAD2F34D5D0B7C291A422950BA01FB46B14F8428609F7F6A537A39FBD23EEC70626F44FFBBDE3DEF072E071E04BCE4D320E5264CA6A265A706E923111BA806082E6A9F188BC746ADE563864EB96246B108A7D24268F73878244AA09373D3B604D876EEEC077533111FF6613917A6E12F12A53CB6A0417B47669BE5998FAD0CF8A18A2B11B0400C566244C1FE16D380417CACCBFB8DB1671C0BB4E3EE42DAC1EE44A63BAB3C8BB3843DF24FE2D1FC270D99FDE20BD18BAF942C8900B1EEE150DEE7311B329AB758FB11FDB2FAB19267C1DDBD03937672D98A2C00E922BB13907493AEDFC052F0CCAFDFE0B9342D526228C9B5F0F1B7CBB31B7D9CE330BD35C3C9A49BB514EDFBBED8F70C4DA1C3FBB5F08740D650D0BEDF76745873973E4FF23D74A34FD0B1FC6A6653DB390AB339849A1E6DD80219A0FF9E4A9BF63F950C801778066453E7ED053DE4C5F963E4CAD5BE132A764044C90600C024D1723327081F93B31E1A8DB3FEB0FBF14060963DEEAE52DA282393A922A55095357727695E6697E96822DAEB98CAAB1037A6DB9B00169BE8FE48B5F0D50EAD6E3836EAD29664B816BF49792D7823995EC1B81E0BF18866832E8193606FA5ECEFE151AFC701037E53639EB95C8387C2B98EB93C3BD3D328CD13D91803878372BBAFF60A681E3925F484E1FA71E13EE5542C964EACD5A914C838C5B2DA4AC76D0E99F0F7845A3D157BF8C704D64B8760566E31DE9BE23CD04DB80A466ED4CFDE29A8E988B1A518DADD093D4F2F2270EBC66075E1F09B8E45594A7FF9809BE769F13D17ABE50EEF49C4DB07DA38A0905787D8509399B10D2C9D7DC61B8FC3A131EBC73432BA199A062036D1CF2FB0599A52226643E13D27BE735196ED83E0B918999ADC64C1850B8ACE7E5E89A9A6490BE5A26E49778B164B83165B53C25D452172E79EAFC25B456C63E842BE64E4DE09BEFA82BE8B1CB39EE05C9A2790ABED0AA98602354DB413CE05FD0654B1F9CCB545C089BA3861586C27820705466018E8BBC767796D5DC6A27B866DE1772EB88FDB514F38783635B58C0975AF7BB1D3FCDAAE7EB64210764D9E0A95944759A497391E9323223CC01CA176528F7C8045AF08B0ACC079DE1E55031C1AD6D61BCA39009B9C90937BE834451B2373FA1B0E276977A9372EC4436DB7FA12366F5AE17583F18CCCDD1E89A00F904EB5CBBA1006DFF45FE001D5DD86314019CD7310D2841F8F6D3591EEC3CFCE5747F0871F37FE53AE61F15F84EBEBE0F460A82811CF73C87473A2C72AB86F107CF172EFC9EA2692E37AAEFCD3629151710541CCF8E13050926687E3436C152E5AEA0AC66BB5041DF03073AFB9DD19531055FC3396B2E716467EA38D74897116E74A84036E146118324BD9B64B96451183338E8FB6143A177F4AACBE44D9411C0A556CDBB23F5B979A31170A9A8E388B0E205245E1AC74BECE80A5A30FBCCA5F0039407C664F6C31B7F450226846AFB211C774CF0EDC3EB54E1E3C36CE9EBC2A640A5445DD6289FB9CA85CE41BBABAA1BB023A44A69F72E5676D397A5E3080D82CCE5E719CC87BCA4855743F0B2A866B91765134C2FAA877B973B3269533B73CD3620D9D0596554BDA6A3DCE217663C17347B84C92D8E32A7E97AE65A1E7271C2BC98C161778EE9702F9DEE774CB0FB4536E16FBD0E756EFEFA9EA3FF431D3FE7E29A1BDEFE1CA07B4E57CE310B07F32C0ADE0D32A7F9B8E2611A041BBE351063AB9E0E314D75AB856DE5B46ED2832AABDD9B94112EE598B2E0825F3483D92E6951D07B673BF0BEB82C6A8E095EC4AC9248C087DB34B00C7662E7D47A3BDA2FBA0B211E3221D0E5B6E9E82104F42CF48ED2025B5168010A9DA5BC4DC8CD77F370AF41AC21E59295D26CE7E3AF20D7A49D2BCFE869E3E3CF3A7036E4870371109FE8A843674B19AF76CDBCD91C86387F29E0C08FDC9DCF67C2D43161E6D14095DE8433DA2F328BC414334193C19FCEAF8A09D635C2142A84CBFD73B609C62F2AE8421DC96A90B69E90E5F34544806940061904CBE0F83055A9A4A9AA2D181AA4A1B059F096C01092B8FAB12B38B8F841BAB865C41B0F83EC4253E02BE3A92DCF293F6A36CF261840CF736604B8B27AA98F480F6D599E0981E713DE61E04DE5FDA8BC467F3040D7FDD58F1E5E43ACFB83333D5E14661497EA2C0DFCA9AA5C7DB3EE8D3DCCD6DD7C7185646FFC5EA54DEF1C25D82FE2C9306C1668F27DCA8B4062819997432DA2816582AD2A4454541E0BE26F72A26CC2D413A48AB84CDA6BD2B5363B91CDD516E68963828FEC746AC71F7369254586022727EF26050CB90F92AA85147244FA831F1F64F48DA22D0C12FC68C16BD40B7A5A3DAA587AE44282C044782CF5C3DF82CC52916735373CC8D507B287E0DD99D88460DC6788B51E4EE56B6E68A371EF5B8607671FDCD34378792E300E37345BED72D83BE9F5CFE674A1DAF0C0D69B299BE433E1D6E7806682D5EB9A0F39ADAF52ABD60EE438F050C88A5C9CE0A1362D9CC4ACE6FDBC0AFA10D9EA9A5E96763A27F7CA1009F5743119FCE8F62F5854CC044312CD19FF449F0FF9FF55E0DA15DE6A8E12FF4CE6215E8FCC6182D769171A872C1CD401832DA899093A04BC0CA3856FB8082418045CE6886DC2179F03BA35156930663E4482FBEFAF5883BCB87AB3A347CE26A82452A15FA427EEA466565A3182BD499B8A002113E670209CBC9F373EF95AC4634003A34A4D42732E1972EF867A3A3C37344AE63AAE7E121240CD810E989067A6F373749C105880DCCB578C4031197CC46785E84724D89759A66B08F73E0DEE3DC5EFDAF2D4E47DAF195B96B2C62081BA503148185C0C64F153AFE3C804D0BAB600024C98DA904041DCC5C44161C1CE462099DDFB3180EE387273FC4D81D9A1DF6CCDA09BD05CC4047CF7CE4CFB7C2CA041F19A1736BAD00B1CE9A93FBED6576AB8008296511AF18534F094EE0F7DEE1FCC829F939D7FCA43D1FFB56D17C1CDFC30E1F2BF55417A4E87D25F19847941821A092BCD0F734508200DAA2A49AA1709A64A425A4803356933354C009B00414267BF63BA50FD355DB4BFF4D530E4EBD8D884227D2F3DD879D164B85736C1003DCCA8E2F1934C3B450F8A1281F33387091AEB01190A5C29CD04A5F583F96EB87FEFAEA90D48E0A32B5DAE0C829B8366DE529999B96C994783029BF0D762BAC403E28571BF5FDF28BCC8CC86D7DAAD7AC52CF8A5007D7C16BC8536415D1C450EE3368AF4DEB70C8F36AF3A75B8B74ED14368136CD00C64C095E2B1AC36744CF83A9A7C95C52CB811D544D2E3916582793CC2583AB1ED0AF2B248B01080E3631733DCE7C870E73F5896FC36BD2E2A9FA512449EBC0E6B87541BE63EF85EFE7CC7E9CF7568ABB30375EB2F46E4EAD06135AD803685507E9A03E5A9CF4F750378FFE6ACCC8ECC8B58745BD154D98AA97693CCBF9B6758FEE02CE589F143CD1F78851B5EC2546593AC65304C9806D1C24316941132B74CBC7908B2D8047C3A14E0BE7FDCEB1E74CC7277C34961D01C840A6813748BD18DD3FA6EB69AEE45557DD7B97039D3C28DB2AAC6ECE6FB3313E447E59D7B14614880513DEB5FD4BC0D2AD2BC712896C71CA30A4D81628EC2B78B34667A06B3433C1DF0C383D43FA81C2BE479007DC831B3288BAAC759D1E699E0B96A22798BE7A1DCA347D08E816FCD668F0AF17E92545515A63457441D4670F7D2470F26E3248184D19EAAE2649E80ECEC805B1858B09ADA151C01DFBDA36E67BFEDD1A0708E8E6D4A05327C8338C12D5A21FD45C60E8C8DA87AC21D4F67934AC2CC4F28CDEE32C58A5CD6E821F38D0086192419871CB3208C36EE8DC27A3A17EBF746C2B7240F1BCEFBF9A3A8952938509E060CF961D476014CB3990F268F1BFEDA75E1E901010AA3857CA850C8403F41E4A2F9C2E66A5D70B004502F7DF3353723944B25E5CCAF1BB7DE29BB49FE2AF0F7AAB2497D6BF659E053DBD069DD78EA94231A0C213CE8ECB6BAFB6DF28E8A6C824F8911D160FC7D1C55172B20B5A56A7DA5D658AD35576BADF57A77B3DEDBAAF7B64976EAFDDDC660BF09323C6C0D8FDAC3E3F6E8A4333EED8CCFBAE37390DEE4A237B9ECA75F06E9F53003F9360299DE8C456EC7B6FE60113FB3626362A687737E52873C7EF9E09D12728048A54C4DA1F82AFFEFF96039F99897A93FEE33C1BD0C4D4A5834B0D7979DA9B6480546A3E04E147BF5672C3044AA7A9873448B32B92A7DC773BE1FF249B68C7F56F9AD6F783BCEBE8FB2AFA3F4EB70F26530B9EA4F2EFAE3B3DEE8B43B3AEE20960EDB83FDD660AFD9DF69F4B66ADDCD5A77A3DA5907A974D62A9D5590727B19A4D45E2AC14E07F697CACDC57263A154FBC452AE2F556A80E1C5320BBC6CEF34AD77A4D6BDCB37E18D71EA0230E1661235371B8DF57AE57319A4FAA95CFD903416CAADA5726BB1D45E2CB51692D622C98211F5B2F93969C276A1D45C34B2546A2CA23497CA8DA50A487319A4DA5CA93657ABCD35A4596BA3DEDA6C74B69A9DED6677A7D5DD6DF5F6DABD834EFFB0D33FEE0E4E7A83D3DEF0BC3F82AF0CC2FFAB01D17A987E1BA5DFC7D9CD1876D83A49690F76EE258BE5B8A49960838A872C64851D57307A0A501502D4B0C2BCFBE43BF1FEE9D3A700B23F5C35CD8BCE75BC31977216D68E7E4F3F50F4CDA3FC987A37FCE87D049B42B07515C9FBA9BC08CFE39D7CC340130540032AF67230BA00E9C30F3438E9F68F3BBDA34EEFA0DDDD6D767650DA5B8DF646BDBD560369AD545B2B95D6320A80A1B52CA0425C2D219ADB043096CE12017D31415942E9B02CC69D45DCE92EB294BA4B783060AFF129A97D8CAB1F93EAC7121260C1D00048B252EDEC3647801CB209A33C13D4A47E61C2F70956D60076B5A54A159950AA7D48EA1F13BCAD85B8FD396A7F866DDC5940E1FD1609EE7FC29DE627270D23F58F71FD034AED3DC987B8FADE93CABB50CA259277BEC091EF93321D6F072B38C86FB124B2FD9054593E96503EE1D63B80E54391F0297C16087C15466A9FE95B46A980D41741AAB06D2C57496A66A7DA00A3BA526BAEA05DD5D25AAB3741709F77EAEA5DDC6F58A12BC8355748962B224B155075F585B216BC2BBA43FCE1685BB51FE44389BE10D8D2F7801FB3445FA613EF3BD15B120219097DABB58F880D94F749FD034AE323C211B71F93A6954F22ADCF4A3E19CC802C089C4261D02F32FA51BA5616447ABCBF045A1860161113E2EA42097F9425FA965645E1F60EDBAC43C920D03313AE8736501EF9DBC9B7F104983005EA5F0E4067E337FBA954FF94801041E1A623240071A0F3B9800948868F244C868F71E38348FD3D918168207C000EBC8B04FAA5B9527E1BA194580C553479DEFBA2C6AB960C56E0A77D473420B2B9E38BD8688FCCFF97B21C9090D8E3F1655543EABD3980F683B7AAFE20DF9E816C81B8C30897B58F74BC481C7C4C81ACFF4FF1227233569524AC71FC4F8D5BD150E623D395715B73227A0D351D2B3BDA697C140DD82424584820423E468413DC760C13840C9F7D1A2C38C40704100E2CE216A4BF8C84818B804B02586D2C96C9E3008D536B6FD6D12E6D3706C75DC304823B3D542ABF0C1E6F27942F354C58AD01B1C8EB129B4037E79840925826A07C928FDDB4F281BE14E283D0C072A01455E1BB161A4482FB77027D84A91020604214A2B624F6C15892C8F281AC8D024ADEFE9472B8CF8F1470C3FB8F1658AF9DEE6E4968636F8C9953F5B9E199ACF79A06EE9A70648D75B31C107B34B0C4B0D764D5403B74356B6613D9513F016FAB4C80F7F63F8AD47C75262F3F88D9AFDB1FDDC0C03241C8C0C2CAD4ECB0748DC8C882E2C3A2A01F69404CE82C8135887B4B717F15ED0632017C24F0CFC10327AF1B68D0D96A74B61BE0A781F386DE917D72C297A206249348E5801BB3A8E00882E30E76194D2D9805CB84CF51C7D257990567103E45C2076B1334133C6B102B26588950DEA294E788E60CD2C6D80A2B168E8236DE7FA768C0FF421D29FFFA5D812DF2464A39B8E8FBCFEFDB2B972CE123EFAD3FB2AED05899FBA9AACFE5E45D4EDE9BEF59D4BC5134F67B7BA73E5A29F838919C65F9A024A044DD88750444217E725AD2C28339309F0651681000F70B91B50328CB717F291E2CC7A3F5527F258197E0B634414DAF54DA9B8D3672006315085ABA7BADE1695745CC366B34E474AACD264DA8E782E6360013EE261018B5B69AE0A70213EA9F8509784FD67E01250C6B35195A1F23360EFCF93154F8183119EAEFA3800935F8364B280ED36F23C68D837E4242FB25DAC9A34DFD9651B95484B0928302FFBA16E2FC1F9DB529797CB3FF2E649AEC689E44DECB7745A87AE71FE68E91FF5E56DCF6AEE013A0ACC63D26BC53DBF7854C88EC5631C11A5E75716B6C4BBEFB9AE7D87BFC1D85091F4226B05970CA91F73FE79910692674350D165478B018210196C8230253B01C010D862BF178A3345841627438B65EAB761C0D5A4083EE3E30A14706C127C397A2C2020409F460C128BBC7027377AF8D3601438504A4BD5266C3E49C3965DDACC96B695348DE5193BF91F7C404FA9AF08B7B17191A442C15052C0662408652601372AADABA4F82A4791AB7A4FC2E16F7D2FCC792EF9859D194302321E84B1643F3E52D49C9C2D1FC471A2F8583EA5F077795FBA455FA5615EB8AD868DC48C581A2CF98FFAADDE9400F706E23E72CB1BC8BE8578E8406B813353FA2102AD85F8888065107E204235D4D03767E8C7048D05B520420192C45839568B09A8CD7E3743B19AD2783B5A40B660198B059073BD0DB6BA1ECB77B07EDFE6107138FB6B2A6F9E03D664A6ACCFCE4E568FA807D41FDC36E73AD8E79A8CF182AB4564A649E1C0D388FD4B1AE11DB844F9E0260313681BF29C70426835293FE6F9FF8068125710738CE948A50155CB6648E74BFBA8FC8BCE07562FEA7A5C4DC8096648EBCF9BB112BF38EA4FDC48C24F665D12906B8CA01B34C786BBD416B00BD8FCCD7297444E5B26F49E42B8D1C13489803B5F7516D9E77244CF04205A181EF177934580C850880EE109A82E568B81A8D80065BF1742F996CC5A34D0C18BA00D4ED8670601F38404C38324CB8F2EB098536E1EB98274047D34762C251B7B9DEA82F96EB0BA5FAE7A4B51C3021F2CCC227C707C7840F2A77F4918304E320B14D4026485450612F45B9259A0C0E8B01130A41AC34BA7136EC367698D6977D1B158C270E8279998B667B8011FB32F1DFD2C7B82BBCF9DB7FEA8D30948D3AA878E2CCAC6182583FFEBCF3E231F73D3BE3A0C956C004E31DB14D88840968167211F3E76226F402262C447D20C0A2B50386062B28A355F08BE26C279EED2313C69BF1602DEEAE957A3B0D3105C481DE611BC3E5CB4151652D4F03C5045C31F73E1D9CF62054908CF5E75273114D4F7731CC73D9B899B7AD4FB90C92CDA2AADC917C89943E0A2254C781B78A0316A66FB43A9FFBE395127511FAED2DCA0B11E6499CDB297C99C37792F8E26B74CD2BFDA1B4562E94448E848F1027DE1742C47E1BBBCB6A4550E8DDE5BEA5FC3DE4CD82CD5E54DE3A82D16F1719BF28727102E78E5490E0E58E3E523DEA1305999F9C1D701C60DC2BF4F79789004B6C078800C40190091984D95EFCF328C976E3C9763C584F7AEBE5FE7E134C0170800598303CEB8DAE062454567B9D0FE21D65C284E179BFBDD36AAC561B8B156242A9B7EACC4277D130C1485B25C8422670EEE87D64BF324386D89AF20277487E98380F0EEFE77CEBF9301E1912771D0DD024806F7E2499FFF28D07744BAD92BEBE7F7C29777022FE4C9ECF6EA4A43EB8BB6CF055281A3882CD53ED4ADD84C630FFF5FA2CAABCF54C77F5BD09F0B44D60B34096216402E1C1C2A3F3D90B0F9803FD45D9B20C721680241AADC5639075DC021380003F0FE39F27C9743F4E7789099B95FE410B083038EE82C00E268E8009C62678647029A3F1C44CDAC46202AD0910E1E2B80F199CD9C1A0B9DA20B3404C4824685E506ED2821F40BB24925758A85B2628AB8A6535152B071E5139F70B154BEEE7CC9F3B9706854C98273E223D8390CC911C1FBC91B94C88F3FC4FB47D08220A3AD8B2D17E279E81CDA98FC22FF30F3E12D1A96ADC5A6142893910D8048C953D26E8F0407944DA1430138403428368684C81B843448309CB064408F1AFE3E4D729302142266C24BDAD2A18840131A17F4C3661BF3D3AA73E1DEAB360D14CD04B807175D93081FA4FE0B8DE610799B0484C5828F5C1035B4E7ABADAB71892210C9D6DD0ACBD235B492839E38B4C48C29FCDFBBDBD1F4CB9FB5AC3051061B006B88C9D9F53D2EA1C77E2BF2749680DE671A688090A763E1954C2A098F30AB2F6335AE6243ADEC82518E632E14D78CD5799605D23B4EA9CF630E1B2D884205CCE87C82E325E740681C380FE62C434D0E86719AF8A1D00024C3651C0357A3A8C7F9F25BFCE0C13362148A80F8000C7C6261C1A264859CDA701775EE8D582C138DC4CB8D33BE2D96493EB11C4194D620248F333316125B1813CD53892AEAA8CB475B98DD5C0E7C43181B2A8354B060E124C6A223008C1EFE490141779DE4A5932A03D57C4E232FE5BEFFF6FA43447E587F7A6D1EF8716CC0431890E67715947F93E3D1467727964204310C7079CF1DD4577C3EA9E8B3820BF8E18849297FB363940E680E83BC70123D6290A1244FDC5C40B887534AC39B0266269001C00C9B6A25FC7F1CB45F2FB3C014A64C884727F179900D0B54C80B8991A372971E452465EB4E0EACDDF3413E8C102E9D7D1E0ACE73161BDD45F4DFCC456A2DCA4C4F3910C135AD886E533E19D6282A4F9C832787182710F82B8B328F31868FD6226C44A024C5B74C66132278F7ECF14F81771A80A723B897FD9371E2E73853917959AE4AFB1999ED31FA94C83E4D00AFE57E0F9282D137C288F09396FAA900935EB1A91705F19678D1C130406C509A2302A5851345851345875EE9058836D94E976F4FB3479B90426C4C48468B85DEEEF3506278A0907C4848B81C99F8ED476686D825D0409E73DDFF2C4B72CC2A95E3FA6D9CD7874D1C7EEC8E56A1398B058EEAF5706601696A29EEA00B122DED1C7A8E3A7535D49C192E19DEEB6284A9BFABE81E7AB3857210E0FC8B93436D952E8E7085E636D4C7CEDAE0ECB13C0213EF608A0757F581CD07AFA8DA2816AF5092BCABA472338A664320D6F8D63692C43D93A4E49919462675DF3147DE31277418450090D82A90BD932C27BED17452D955EEF580E98CA800A8B93612E2F149A02A241BA0976C0C87634DD899ECF93972F86097BF160A73238680E4F2942E02E71CCA5762657C3892D265C1B07E9DAF5A58ECDBA77BC8624AF1C434C78344C58AB35A99B1C99B05119021316232E7A074CE8E69A913C267CF099E0CC82ADFE14D47A94841E7C014F921C19FE140D978ABC23CF6D883D26940A99F026C78400FD854503491FB98FEF6CA355C3B90E25BF67A4605F5861FE4531135889BC99C384A4C026F84C886BBC7D67FA2CDE459A09CD0FAE9AC69DA7121EA862999882651312D076B41C32616269B01EA7C8844831219EED46CF17A5976B64C2EC2042EF681799800681A44F9325C04112EFE8CB70AC23E66BE720D91520C76413683D7A6002CF42BA9DC011ADF57A935AE1810CBDF5CA70A3DC5B76A5EFDE42D832CEE9544D06D79BFD3156DD16DA41922FBA9009F3123EC50C79F3EA596FBC88626E89B7C8DB76D099E357CC738104F1495C0E2EEE1CA4582BE0401F87F9B4B773B67F292A0F119AC724D61F968F17739D68324455CB07D56E24A6E04341B2A86D6900EED05252502673D620B191F138E0803508DB28D31D94A7FDE8E5AAF4F2959880362119EE558747D86C27A1023181FAB187223650560D17936BB7C805DA04F48E7015198C9871DA28F7E16DD61B38C50CC9D05D2B0F2122593634585464D066414FE2E1F4D127D770E1D247258F09C1AF5ED2316E20B1B834D6BECF3302854161DE9BD7902D467632FF18831B0D201D06684D5C0ED01FB88579073DAF9EF3416D4EDF8B042FBD8B87EAA3300FA1FF97EB11B63535E31AD503267C3053118C53D4599078B2AF72444200E600120085E28104C55803C381C4DA8129CB0E1884F8E761F4724D4C388B8009E96E32D8AB0D8FDA1C240C0D1386673DE9B3B8927A02761F29710D79C884949860734734F57EF27DD4DA6A2013568009E5CE2A316125EE5BB3B0240174DE41CAFB483C59470C82A816E31DF93F6DE81725053F5BB1EBEFF3A738769C8FECA2FF5E840F03F7BFD4C1850168519A32763BEA662412309792D609E5B7D873A53B283032EACA26068839B6F903131C0DBC38BEAA9C228910BC16234383CFCA1A30136C70ACEB65AB620A46CE0E249E29D870B1C1D45803A001959623A00133617A10A760130EEAC3E336D804A0C1F0D830E1BC2F9336752F2A86078A092E7194D2EA496C136419A2144285CE6EB3C9F3DF962A9DE5D268BB02017E1F82E625D3216893AA86095D9D4452DD78D298ADBD232A33575D9C106B2C0AB28DEE8F0DBE63F9D962768DE065AC4B044924B9231BFFE5AA54D649F01C867C8859A8740B947D01E22B6AC4546755275C98A4377D7225E72B1688BD886E087701C6BC5322FD327490624F89D8E8251724381A485DF99DEEC196229ACB999A94BA9D5F067ED140558E0B53A54200E6006DB34D9F06101B181A3CEDC5BF4EE2976FCC84189830D92D8D0E1BC3E30E32816830A06E0B640277A17E1547C831E17AE42DFA628A09264EB893250932ECCD6EE144B81564427BA9043661B08A36A1BFCCDE5ED20F3C256B161654E785B2090571820184517EB1218374DAB03B146B3E284311FF2B82BF38561E7CEC9B823756B9AA54A302B4CB96A8247A5E9787F00DC0ED6017F626F8E8CC8F9856EAB7E478F8C0B557B0DEB91C668D2A9D527DAB8EF7B578D5BF0EE664AD67E83B9F616C13F6DE45960341788006A168FE4D57B7120113282CC6EDFC5A81B3039BF369B08B3478DA8B80002FDF8109F1EFD338DB4FC6BBA5E16173442954768D504EBAE3A0F72E2FDECC849496A19F4AEE88D6CCC2C539C0B8A04108991031135CE84CB585DE428206C17A479437B07DA97399E0B45D6CD31ADA8F07ACA3C44E840C2A6CF0ACBCC9F920A0BD9C60ACC9C088AF58975A1DE0F121F44322A77D030E28956FBD6A4B8CAAA7EF3D3E08CA0D13CCBE3BACFA2E2C69551537DCCBB7E6F4526C1B1CFDB3226EFEE5AF28F8EAC28859678DDE5A83E04D4FB339D38269682680EC29A748C5061E0DC66BBE3BE43361BA1D1884E8691FC2E5F8F98298704D4C3800269487872D60027B47C284E3394CB856B9233B9D5F33016C427A331126DCA7FDE36E6BB5864C584626F4D7CBA38D64B0626CC272D2B37C9079158988F02169FBB9D4BAAAACF1D6252804B2C626F8391FC704430CFB133A078907FF656890B0F840CF65692ACE81719EB1E7256BC7E3AD95C853EAEF62E53F28D1482D7E4B206B1544AD64BBD6BDAB7190AA0256493CE87D93E617E7B3F62EF04523697C34C6D0A689755ED8C6F7F8F11361425545087553516EE493456AF2313B45523C56CDA443360841E598B243E9A6E1C0663CDDF2E263630D223408C08483F8F93211269CC4E97E3202261CB5475260EE708199A6AA0D26D77E64ECE78E944DA08E23728DD03B9A7C1B739C30E5DE6CC384167CAAB5D268A3240E926142DF64543D262C48CB615B79476216F24C78ABC06A2BC406D99E41F8D77C26F89925933F5164B00ADE013D367E3C42CD29FED799A07479C564C09CDA56F18FD2E545D38B6D7D4A95DE399DE052F576DCED2B7CBF57DBFC29FA00D53E6D67E86BC3AB130C5677C837632C4F305FD9A38159E181AC416227E00342880909D1207199A2351B22ABF8782B49B529D0D66057FB45F1CF03144CA17E8B5FBE1013F6900923CD8493AE248E2E07139B42B5956619B1FDD8B416AA650247CCE9775C079257CB024A61718DBDA3C5527725996C9781D3386D9498400981C496CDA5B6F0D9060C498E0991CDA5569517EB72326FB496723171FCC6D7FD2654E011E0866DA630BF628116B728AFA8F9BBFEC4CEDC4C45C501F6E5D80FB193B06B9A0325A5EC6DFB3EEB72A3E6A5C44EC0A265824C0BE7FB5018CDE1A085239DD2F89878077C98B34F2FBDD9F71F24EA280735C4372E40AAB8CFE52624784E913F43DFAD46B1280915A92573B24838908CD6C2FAB15731D01E11678A763044161AEC130D0E2971749D00139EAF22089D27584CA80C8EDBA3B31E1904889571C2DAE8A23F5265B5A0A0E656802432A437A95E7D1E99C04F2778BACF804FCDF51A860ACBD5D662A9B39CA4BB95D17A6CE2E68472029816E8E75A302883A498E005CD62105C3CA76365C3813C0D3C2F28564C30E3706E18710679153395995F7A59A652ACDC0362E3BFCC6134A8BC949CE415B3AF41AD7A76D0A415B24435A8E8F33509D614FAD3C16EC73FB12E73A770B92EAB86F271823508DE6751CD45B48C97EA2C62A7404FB67409D364B4424C582726986C69BA91A4D62FCA674B772436605320342026FCC2146A8999F0F328C67079BF3A3AE7EA324408346F73BF854C304BDFF1D289AEEDC2CC63B64F194426DCA53E1368A5D1A7870CCC4A6BA3D65C1326B4979009F031245420265883607BF264150C9548D54C30265E223F1B31BB14AAEDDE899D23A49910FB490F1737074C281435A99FC3656E012AA13111C4E35A5DCC8A7F01FA719D18509C15624208EEF79E07E2E9E30FDE1CAE5073AB05D1E6E21825F146708539D9718BCC05A7A8E586DC4578DC9E2E574626D4DE27CE323826C4C6AE46BAB1226482F18BB8FF522F4CD4531DA6A6BF9A3990F84E51E23245DB89B60696069C33E5409999F00B98701C2313BE42B4804C18ED94867B55B003BDFD36DA84A30E30A1BBD71A5D12134C8705A54DBDD079F4C53D6590A7AAE1A31B48703548B3706F06046A6DD6A9FBA8DA82A079B994EE55C69B1034477D4D06E283C704355D819788D4EB7F7913150C468D8324A1820577EC8B6713E2793430B343759AC5CD8B2026BC9170907F7256F9D27189AB24257C6388800F0973809D19B7AA9F5BE52A7168E37D356E4A4E885A005FF38302B1BC6B8F89D5A079F949BD740B262424ECA2E80312D9BA353913F79615E98CC08519D9BFAA11DB5D4354428EA2491985DDA6FEDA2D3A4A461A2C51F4B8CCE101C40689F84514258B41D84828445634D8745181CE14596B8034D827BF080D0230816CC275FC7C113D1D26A39DF270BF5EFD886B9C95E186174AAD8D7A0FBCA3CB7E384F2D08970D13D03BB24C7860267CC3C70DF272D3E9D73130A1B58E8B7BB6962ACD4564C2641B3E5BE43248A6B550C5CD3E13C23E3C9D417229799BDDD71D6F892E9FBDF1661A94FC442A47C91559DEC79F6FAE034726C35B9579B4EEBE736F5859A2035327555A17AC27943B4FD84B36CBBC99D53FED96A1F9C906948C5A1CB7FBF492DEFAEC0DAA1139B1CD2F3F5B059C982C4D227DEF7240D25E30277E365B3BB35C4F31E7772DA3E80A7033F0C12B41E306EDD8D66B315F1FC340D9258B90039244116BB09A48CE748D7A8AD6715196F19C84A91060DB58839D026B6099F0FB24C2B21A84CBE7D1EC301942F87AD8E04019AC01512296B550AFCC43A50A2B09C2049CAD96DE8A6B34B9478900FDF8E803B209C08A2E97999731686EAD5426BBE5142C113161B08239014786252936F7749FB6628224526D36B024A0D4355A2F7164CDC2BF7C8F28510430790F3B7985D6AC8DDDACC2C22C8AF2E91B9F4AA8173F58A130F4435EC1C7C192B75A0D0BD02D2E2D193EC40EF708D652DB41DFC33ACDE5802FAD0480264CD3629B0B0983DE88DEA7B7F8E0CFEA145985576883077C76446226E88BB4E42225F82055308F6F22EB31DA582B683142E6186BE0228445A2C162422923B2092B2A67BAAAE61FE76830359922C781203CD877B1F2AFC3088284DFA758607EBE22261CC4C31D64C2F0A4C313D6AA9F71F163D8F15676D153D5AE541EC91498D92650E208F910812F2536E1219BDE4C20ECC05526576B2D5CF619985049F7CACC842133C1D59BED7791740B1B2EF42217B6B846BEBB5645BA42ECF51AE53B2855BAC3E6F575786757A42A72E26979E70FE281B06FE3C0FD413B275AD7162978841440BCD4124C97049A9F69317DDA32525B069A6D3E65A1D459446E746850F6591604F7E482CF173ED2881D77237435FBB26D2E4EDB585F0A271892B9AB183ED8F63B6B5DEBDA3532AD65B47ABB8E92132F59E47A4B13573AD890D28170602B670D80063667AA03658A957F1F47CFE7D1CB770C127E9F46666642A3B3556F6FD4BB7BCDCA07F491B89830FEA22A6BAA092F2C2938266413320B111C0D4133ADAC3F05E300B6068266AC2A2CE35C85D17639DBAF00D18786098E0C8B5E1B92B60941710D67F72BEFC86BE379E397D5FC4C91DBF1E797A1DEB2C9193361DA4B50DACC492EBE4474D2732182B4A071823D0D8A4A1D0E2610E34AFF0BBCD83F3ECFA2BB52E9AE567BEBD5DE1A6C6BFDAD3AC860BB01FBBDCD5A7F9346366B83ADFA68AF093206D96F4EF69BA39D063E0700AE63C0ADCA32F64101395107E0D2EA0B66BB60F617D5C862118B68D05E875F025BC86E9B8263C95331AE92A027A3C9327509068D4C030246504B462698D820F54D810B0C7672A5837DC7845FD6209C445460C632F3AF539AA3B3571B9DB49B2B55AC96C0EDAD56C197199DF7314EE01AF3F550998291670DBEB8252DB287A9D80464C2E5109C2261C25D0AF1787B0317E0C6E7002C57875BA51498B0E698D067322CE90C92C704EB2079895494C4CD632EAA27CC9B6F1930417C599D680F38A01770FEE4E755382FBE800FB37051A0752D7C019DDDDFA80DB71B0382F870B789B2D318ECC03E6E3DD96A00F47B1BB50E3004B8B151E3FDFE667DBCDF4239684D8CF470B1CD127280218EC450889F4706F76E293C8BA4B3E0EDBFCE04147E2AC7277C224C55F78F6826B059A02F4A5246CE20241221D879986B49D04F313134C8B68A98E0D340A2E4FDC81904C384974B64C2EF0BECC34BF7620897F1793C10241CB4C12034962BBD3D4CA14E2873EA31C1ABAC99415E29FE36754C00EF687431C4508199709F0DCF7A6074A80F0F4B6CFD750C9A81E8180C613CC466814A89F45DB0A768D783E918C7C0AE02662366132728D7C825B64DFA68DE2C05D38E0AC75380AB7FA744697D93D5B101EE2713F321DCF1487660E8512ECA8B6047027F69746010A64083B56A1FF57D15D1BFDD181FB401D69DE50AE0BBB55806B803A3C0DCB5E97947ED251CAC53CD18EEA1035A0AC2AD0F099888C9617B72D89A1CB55390C35676D41EEF36BA0BA5DE1242B9B758C2EF70A9D4B3DE26EF2FDA7D92459225B55D94B7BA561679EB1E3FE3C6F1132119ECCBDE5262C63156C1B0C17604D25AA855F139A38659D64EDB044703F48B7C83B0EECDC04CFD10393008D629E2363B10630DD83542EFE8F9347AF9922013CEB0C48665B5C3E6903A2C863463138284FA62797C094C20EFC83E43C4AB343B1381E132F5637B4C189E0F4CC30596994767BDEE4EB385A102B6E2F5D64AE9410D3EDB708D1264C6411A9850A14F91939DE92F8F8AC0EC4494EFC3AB96D494C544D513AC41F8979B90E082699963459D146FA2DCD26B8997327F17E9840FE749C2D59B296AECE0CAF825CFBB58103075081F600A50910370610BD1D8411B143FD000DC21F08E58E5B717710718025B718DD66A7DDA0741A708CE3D6C21078EDB9991C95EA3B7CC804E840F4BA5FE32BF943C3533A1BF64A5D427DCF797CAC0A2FE4AB90FF7B0542632947BCB6586B513B82C0CE2B6449F14E310D8C797CC071375F0C707BD0088AF9A56146E21A1299A112DF018C997E657946D8430F42204A641E2D2A67E118D69F0B46BC4F945116C7F596B601CA4E733293023138EB1C03C0055B24573F98F3BDDDD16384860162842184C0AB346365AC833C1765B0C4EFB1034DB67018E2FFA60689809209DE5D264AF3AD964DE131356624386447B477A41243B7FAD61DC15B109B69BCD35637B3DC3858D316C0DD02F7A834E914DEC7851AF9783377943F1F575D82A31A5D18B25402460C2A867D1C1EC848C76EAD93180B895E296F43AFA36E0E8D747BB8DE116384E35D80E00F71B603C2BBDD5727FAD029E4F1F9CA21544670FB7045F1CC4913E8A801EB6831582326F69C4133C0BEF106F12C02A8A1993488D12CE1B6E224C710B78C56FFB1DE215B1CB21D0FB882790743FA3C864924FAE5B8C3F7E4FB240F8D426B878CDF6816B9B00622A09C6261818ACD8C60AC3840DBFC7CE778AA63BBE47B49763C201D1004522660C97AFB11FFBD769F474148DF792EE460DCB3E1FE3DA277910561BBC502C260C4657737A510D1F4657C0840930419E17FEC04F47CFA2FE510F5822F375EED3C9D5007CAFD63AA58F80094B385F0797265EA324319221B664B026D23948A859DD1291799BA02B6B7A3E8DEB98B76D48B1698F311D0140A106D686F0B142C6F991D292CD94B74CA0D2B659459B66214DDF213589829E43D2259CF5568CD0CBFE2A8158E0CB0794490D977260255893C3DD5BABA067053A1B426AA0D9326E9178CB658938174B32C96B0153A8AD0F11DC03E01541C9345829033706AB4A56E85FE0833312F7EF96D9B70158475CE0A7D6AF84EF64B8863C84738945099D920C564B03201E6C5744FAABC2CFE16A79B806C7CB6787DB03BB5AE3464322836EC6660749E6E2D8DE8A554A9E72DAD4AECEC24C286C2B0A9860A3E47D67070C13304E78BEA0620230E1249A1D02134AA3E3F6F0A8D3DB6B0201305CDEC3A551A598703508A3026B13CC1C1D7A9C94C7049C9FD03BEC024BF8F1C9E02041CCD13FECB4D76B6C16C0260CB72AB8623D31C1AE5969A2059F09289491B04BE2F1B2906AD578E720F97388BDE923B863B2ABE600CA7323079A14E1D9C7DAD91D95F031597993616402B0740D13306CA597A8CB01FA2CACB90D1358E0181C21A3C1081BAC0A28015230D801571014F062095535A864F0D0DE462DD6D66F23F8A74D52ABF804550AA2BA6407406BE0F5E19A084D4424437FE89800E3E0789444D6CB0265022E89398CF7574AA3F58A5C010F43B8C3560EA0F1E1AA63C260B52C47E25B4C392403DC76DD3C30C16742D4A68780B077A498603AEDF422456EC5AEF94CD837C92262C2AF039F0947860997345BED3AFE294C288F8E3B2369C6C63E0B5A21B8EDA550AF945CFB4CB80E994071C234EAEE778697C3CC3021FD368640A4BD5E4726ACD5201C1C6C48716D48C573720A35194C956DC9050C3697EA35E1959C83144C6276EDF289972D75DD726FD05ED3F31E4D0AC854BB6C9ADF393FC6F7E54C252A6950CF0B94CE5F605350EE2C484CD9E694C867D4D04D02316E4BA4A7C971024137E3538C8A16D42D40670D8D83D1D9287D32236C40FAE4C988C7057A9AA106385E2B0394476B658B6CDC5F91ED7085474AA35591E11ABEC532E4D357CD4BDC5618D6709827ABE1CBD17A192F05FF9A64BC51196F5527DBD574A79AEDD5B2FDFAF4B001323B6DCE4E9AD9417D446781526BD0834264B9BBF73C793F6AF1E37054CB1DA78C3C26B035D84E523D311F68B0AB92452891A601C86F76870E38508EC52C1C43B81CBFDC24CF5F8009F1F4201AED56C6A79DD1696F78D291E9CBF8109D2EE78E4697A14D18E938816C42FA1D9789E7A96AC08109559AA3CE5E7B7036C86E8509D9F7C9E8BCDF6226AC56DB98352F67C0C2355CB8989248D89A3AB4D1823CFD2AE92DB9742A32E1A37847DE7C9DB71C93E9C9C131F74DCC99772F89A6CA1B4CC8344DF91630EDEA5C1F2501CAFE4F7749BC6A7809E006916AEE62896ABA18288BB900CFE4734CBA9C7C24B20C7DD6B82BCE9118AE0A8E0704419175B50FA0C46DC9BCC41DC41F218F50CBB07632464A94C6240277462A1F60504B23805D94C956050510BC5DCD766BD96E7D7A509F1D369E8E1B3F4F9A3F4F5B3FCF5ABF2EDA3FCF5B2C4FA74D786B7658CFF66BE96E75B2531D6F55461BC0DE84CB41D834093AFE23C618181B2451FD6DD4FE140137E052706E7F3569C853738409980531D10297962571B41A7B34D872D6C0AED152E817FDB46953E717B9E4A96C8109D7385BEDF92A7E3AC219CC232C2600136822FF09A58FF65B386FF3CA648DF42A2F5E65CD54976F98093C833983D0797297469DDD3604CDD94D3ABDC3D6A3E9CD647C311026AC31134AD9BEC70429B42DBB87FFD8F9CD368924AB679B32B3628274C9EBBE23CD046B28CA6F797D699968D65E2CB352E74CBFF5763A3655C234B0C22E3E793B980E32F5D7CE2712B20994724958D98B8E47C74380CEDAD7A8DEB2D1D006AC04FA911B6740E7A0CC9A789D004D3BCC0496C9064B29DD2CA7DB950C64A73ADDAB4EF76B006244F919421CC14D5BC03A421CF4F731C96133DB6FA43BB5C95615FD228E2B96C9382FC6E0B3B58DB05268C9A39F483829F401B1CED93CB0DBF07B81B796ED567F9E34E0BFC0EFD8306B0037E5C951266EF66C023161C3C5068135D0B5645744DBD3AE91890A0EB5776498F0959870113D1DC71932A1EE986012A9B4B84B182B87D5E52B1AF199804102F1216A6FB7FAC73D99A5F08865663031ED0DB60920950ED88483CA683DE20F3C5C4B06AB3A5A703E924922253A77649810D9F451B5E4D62609A64DE5569A889906ADCF0C74291B41688B5BF14924781D881F4223D6D55E13708B27C3AA7DA332409D5D19227A4AA38D0A0B8CA322877D80E966051D09F3D6685D74B396C92608AAEA9404400C18021083008811C7A7CD5FA0AACFDB80602B3FCF5173038EC12701053CD9AD4F766AF8DFC91661569A5BB93E45006541F327876956E4FC56E79312FB4C4BE990A314D967E1BC2D69FB4506FC32318EFF8C45CF3A0503F0ED8D374BE38D24DBA94C8F9AED0F9149D645B6246F3AF0D02C3013C6EBAEAB220C94B70B4B07CE1A508410173081857BEFBE27BF81094771BA1F8F0E1A400366C2886C02CED139A3799BC6208CAE6CA688F6BD396B63F080B23B470364C25D16B5369BBDA32E3141CACC93EB9130811D2460C2A16182F58E565DF3053A48526F963AA85D3358D98448563D32DDBFDE22C17ADA71B0B0CA1B748D48F127ACED38B0EB5356442743861CFF41E4B79458BFDC6E477A7FB3023204FDBDE9437C13FD07DCA24F5241946F8A4F226EC98E68EBD95183857C920EE0FBF76587E5D725C01D9D13705D66070DF064C033419D4DF1003F37009F36F929F2B0FE919EC9F731962DAEAC88D2F9ECD64C10A02F4A838664484D51D974861AB1F50A690C93CC9208A7C8389FBB829D23A06E1A9FD06984087BB253C9762BB3FD2A70B52D0F539592029341AA0A4BD278E798B0A966E1E46AC9F398A0FCA2794C88B198701ECD8EB0C03C3E6C220D1413205400977EF2451247A32BDF1AE8E8999890DE66346FD3A65099091BCDDE4137FD36A18ED40C9800417367BBD932E9A3F60A30A13ADE8C47EBC48435E720A974AA6454CDFCB5C43E4EA161D34725B71643C5A25CAD2851B1F38CC90EF0D405600E9C9802A4408E9AB3F3F6ECAC8DDBF3F613ED3F5D7466A7AD27183C6DCD4E5AA0C6267BF50CA2C02390266D1BB36394A793260A7A17707C8B5FA2D7019A9BA1FCA5F37CDD45CD7D8E834F70225C07C2CADD1AD30399C65514D2D902E50F2480E60F4A677F32E0368F93313B614B85056B5044F372B54B96F0E5015521484A5EBE75D5042DAB1C6433F35DF48C5E19119EFDB7E18671D8B66AA3AD7A2D899A98F02DC138C415E96EE5E9A80E7AA1F52E32331FDC1AC026688EFA4B910B12FC358B8249C94FF9B4295A83A820734A75658A1370F0F7698413F9B1AC864BDF4D76E3F1516B48DED14816FCC2C5BFE49909B6DBC2550F421A703101C8C04E11B56463D01C3556EB9DBD0E9B8C299599B3EFE3EE4EB3BD566B8B4DA84C0EAA93CD885C232EAA471227AD4AE159B721717DADADB2A8DC8157353641D5D7EC2242FED2296F1D132A093201B42F5800089A4BFF421301EFC220A6BDDFE216023B2C2D717CCC993E596180DC06DF8BB06E8600D76AE50FC699C671B7C427EE7C7408E8B8EED1D8D3CAC609E9AA160983632AA2B1AC78227924BBB35A1E06810707D31B2ECCC83B69ECC5A11103DBC5813526BED188A53B9423DA25D9C364113B6F186D1FD49F0EAA20D3835A766898B0948CE0DC5DB409BFCE1BA0F5DB7A116CBBA60BD63168ED1F4AA1F23C048E958379C9DA2FFAE92472D6E0205281B28A10702791B21A2DF805C661BA174F76CAA3A3D6E8B4C389233208C0047CB09A574FD0A182DF928D4CE02E540E97B9E18299D0DE6D636AE98698F090F112606DE71D5526FB960989F848C20ADB82E115163413C43B72ABA39AE25A6257D7897D3EA859FF6FF130803B6874B0F8C0A52492121B36E19584094C06CC7E121F9A141A1A4CFBF249C491E4A372B2659D6749C5FA2897AD6A04A21689A57CADADCCFE9B53D526076AD3A03A5F64A2E78A05B42FE8AD597C8397959AB084B0AEDFAA6464BE18F4530B7A2333CC35815F875BD0F728873560C2ECB0066613BFC04F984F43269077F4FBB2095E5CDB98820EF5D198758D6805F52566822BA8E94079A6F24505213247C92E42D0A565C504EEC706EF88CA6AC084F17679749C63023E58AD6F1247B9CADA950E1E46E67152860924933B62426BBB35FA32C4858F9809B7130841B0B806345803A98C7641DFC4D47D248902899E553A5507CDD2806417C3D311B359E5A5F25696739315ACFCFD8A591514F6E147CA0EEAEDCF68104A319DE89A25E90147DC64C1335416647A00579425D164C4C48B0AD0D66F5E32AEB3C42125F6558C1352124CAF0AA66D16359729AA90163750DE34F1868AAD05C7ACB00311FD5D9B8A50D69F64EA09C17DDF8DC059887544BC917D9427FB12707F680840F2F3A8FEF3A8F674549B1DC3D78B617167199CA50AC6097B95E72F2D74FFD8C0A2441DB3583A4708FD259AB5B2866E3332C12E6D1D345D874E91ADA3D9521A9A85DF5EA810D962C2F3253181CA6A19B84660134EDAA35397451D98252DE4298397A6F5485B033751C13041382006016D4273A3D1DC6C8E2E8738A199FAF0A677132CAE6D9844EA5A75B0554E77304D86A10217D53792B1293953B42066C1E6525D6FB6CAA2D6DC144A25761D382D66E27FFD2DFC00C978AB0A8A1F9880FE52C9348751EB11161916B081CC959097A4AAD0CBAB6A2E1AD884D29A9255AF4A30F221CEBA99D53368E294FC9074CBBCDC9178DA0334ED4C77AB0C5C90190862B7CA1805744EF7AB1E760F64FC494159C3DA1BC98D23D0410E08E5C74AF12BA171E1C0CF9306664B810CC7F5C17A193C4CB4099B860957ADDEE7C8CC538F7A6C0464824A441D47C0848869C04C48B7F179C96E595FA2C12CA08197298A7EA3C45A74DF11F6635F45B4A445FC7484CF561BED540C133AB828EA09258ECE71223FD5D40661887CE9DB045CD2628211B3B106860953CC1D01198617DC919A09134E84096413AAFD4DF01D131B34A341B04C20B3206BA72E7B5585A245B3D5646241BCD906643005693008BD15CCF65478B9BB44261636CCBC4AA4C192E9265A92262256EDACCBC505277C730A95F2A7ACBC4D9294504E39D3322BF2F1A6833ECA669955B81574BB774510E882781406A8ECEFD59EF6951C28281F92107067E8B2D742D90F1932530ABE803F02FD7AC8842323307E52473926396DA2D0083845A06BDACC846DF68E5A3DF4825CCB5D7FD934DEAD70FE30B20661CCAED18E30C1D492A3D95E34B333F40F340D0226F07EFCFB4831E128C670593161B29B807B42C504940133E100672618BF68400B628F26011F3C26A43677A48C03D5139AEB75EC48FD4A4CB897DE6CF08EDA8609DDB572B65F9A6C72B3218647F8F9A91F9DC9C04924CF41B2DE91DF87171A84796248D27C17A5FB7550E4BCB227F84B3C91D2B64E60ED8C343DEA75AAECB200A639253AD17EF6B6A444292B5A11586F3B4CA39FADBD917DBB25578460372D82E3D381F3439E0CBEC911F7E4E721CAD37CE1037E1EE5DEF2FF85B00804FD7E4AEC12137E1E379800ACEF49E5D741E56B61E8FF3AA164DA71034808A669B25785280B4C2BE694E09BD9ADFE3C6D0013B84DD5AEE7053232AB3B5A0E70AF11D260975A2AF6AC29E0254D611BB9A8E0408281DF4720087D24036F85122E6EFE7D4605E6AFF1EF4B2C268C8109FB58601E1A19B04DB8340641BB4397393248817992A70132A1BBD76EAED5FBC73DECCD16266493CB4167A38E4C203E60C3C54119CC1FD9C164C44C58B36B209B566DB5E605A75682D582EB255EFC504B9CDBA7E717BD9315E7E197981D3741DF9768FD5370B4D805426F07D14F0A9E95FA16C9761565A736214951AA29E31BB752F99A3A27BB6A51EEE2CB032716FA127732EC44C8F9B69A58F4B1F649E4E54F2B8766C7E8EC9F476E470E3874834FF6FAF66013EFCE04E20DBE19BE49CB017B0C7E0A345F159674A78CB25DCEB6CBE92E05C73BE50C07F16B041546F5846AB68FA7F4E9C162640722B3688599AA8FE88FB0D96E8BBB8CC81AEC691AB029A054A9840411678A04FA4791126642649940C740B88C34C0B55069223F3061B85FC708013BF03AF4AC417C18F3F8721036A2167947AED5827AEF2C1F2624D8810736A17FD485E31C13AE869DCDBA310BB5CE6A253B84AF0F73C68E09AB62169009CB8E097DFB944E1D343313CCF2213E193C26986778457C1668F7E9510342055E09B8F149FAA8D908300D1C13B62D13AA96095ACD874CD8F7D0AFFD8DA9BFC59D43CA3C1ED63534250F731C8E3BEC1A256DB12E2F2D137C4A38AC071724AD3F53EC4564B387B68380A6E8A5828561F36E0AD1DD16825E0BE31E054F476E4CF7480E308394EED5BA9FC109A94E0F6B93CDD26029722D152B91E9BB8EB8DB74B215A5141BA41C2B5B26ECBB058B9435401A986020F2696044394B7CCCCB8530E1173161B45B1A1D044CC0B29A478022BFC8258E341394594026F40EBBAD8D0676A45E0CA9CC4C0B1F5D8F3A5B0D0815900CEBC2846C3711266C80C80343475255E09273C273D9B483E4D6B9A0462EFD1C5F2D7A5D72D3E5823BD94103943ABCE46EBCCE0AC6BBFDF532130039B0551D9129C0522EC82E480D7ED1D43A39FB39651F24168D4E15B8B3E87D417CA340F7B373221E8BBB4E469005A4A63BAC772B268616C8A65B6A879B3558B6712B6FA1483F12A39CC04D2FE99A448072C6D7B742FA5EF0CD913A278EC82FFA75DAF875DEFC7DD1FC7DD57AFE82F2F2B5CD028101784454772BCD8E6A639CBC4ED921F185A2F1863C1396E301F085D81D0202580EB0E8C0C0C606BF9D35889FC9263C1F47BF8FE3DFBC859724D65CFC122660EFDDAFB368BA1F8FF6CAF8F410ACA9519C00AED109CE66469B7039B48F61E6A70C8665359EBECCCB1CDD3A1A4CAC4D006BD0DA6876F73AC3F341464C98E21260A3EE76B36D1C2462420599B0C1CB39211326EB3C612F31F31692A1CCEA8CB968EA1CA48F4203E1837E26C507719F1AFAA9A6C4042E474C8F9AA0EC216E2E53E4D0C7D961140F1853609930DEB164504CD8F7C94018CD8C2F846F1DD459C87DE2F25395DEAA0A94297E50D064B439E4D9F1C9163890650CB5374A20FC92FAED4AD863C70D76F6F46D8172A6C70B65B722713987E67BCA88897BC622164602E2D30620FBF715E2FBF94BFBE5BA83F2D5C8B7EEF3D70E8C031F7E9E41C0807C4EB74AC034B873F841A707D5C906FCA01147C6E8026C44FC90F06C3BCA76A26CC77000C3624D0379DCC1D3815AA242E2019467A2C13343FF98769009BEA764997029CB1CFD3AC562C268AF323A6A723FF6E8ACEB987061380001C3D530101ACF31814A0AD86A71270E12CE59EBECB6B137FBB42F8B66636FF6B8BBDB6AAF8B83D4C1E21A7CF5E5097C1D1B09A650372477C613F646A63B95DD247EC60275C8D8293B917E409D74B07C740F30B52376C941AE7FA5474D088821ECAEBDC5B2FF700B092053554CA7F4C07644AF976CA06C769C40C42CFB5B9435DFAE8C2586B6EA99DD09F5D2EE9352A7F09A015A3530AD726A1F1CEBA9156B5E8E449C2B7588199B5FD899078AB90502706479B9F6F1FAADEBB620DFBBB80F0856F2FBBAFDFB4BFB17E8F28B260A60FAB4C1FF1117B4DD2E8FB6E09B497A9FA39EE97402E7A7B784E5E1C11A06C7A3CD32AA0CF0FA4E9BCFD79DE7EB365C64BA8FC1C368353273F3E1478FB2AD684A0F459E09F4292F24160063E2A70392C35876C02F7213F329FC25793E01F4A335783EF1442CC3B18B1F64660230E1828A097BF170AF363AC6560B79DE2695D546E7BD7C1B76011FF871525860CEA4FD8EB6526503260C4E7AE01AB5B79A40095A2D98CCC2EDA4B7DFD6DED1187E726002044934591B99B06198609209430C9D5164AEC2927AEAD447F75C3A92C87FA92AF9EA192D701150F91D591411E7DC708268B0816D3364194C8EC8F6876EA3269E18F832825DB94A9C224CB9802244671DE24B72187E5D809012BD42780126504071827CEBBCDC745FBEC3B6F372DB4550C2FE3792EF8451161A9153BE767E93C6956EBCB3E6EC043BA0C01C91FF5605368E36E9F114F48DA1215D88A433EFA3B433B5DF87D2B2A2EAE82D533BC7D9642BA5C17A85E328D00BE39DCACF8B1636055E02D93AF0D17EC29DE0A405309E155C027D29629EF43F4710150C97A2D172345E8D18FDE926440291940824251AB1BE47313B141347BC7223ADF04E5B1A943411439CD43F3281E4E534F6C8C06F5913714C8BBBD0F46508978109584C8020018D0031016BCC9DC1610B98C0353550FC231312C8886F256499235E0E15D06FAA0AD47A9446FDE35EEFA0831DA9875DDB7031E53233840A8609A31D88A27C2690C2109B60A7B3F1EF6A9E5D8B25366E70F01B2AE78A99DED0F91CF114C1D146A58733CD639C20BF44F304B64836710B78825F1424DD35F99F4314508DA0DB7E9E377F9E13BE2FC149683F5F81CE63754BBAF6A6EB407CC3D20379FE8EEAF6E5BBD5BBD89907DB5FD7ED5F57ADA7F3E60CF4EE4923A349303C0306C44EADE44594F1E62DAC3F9836A7F7B8954EA70F22BA1DD5B55B4B0F48D82ED5F9E41A46EC174BA2BE61EA5A0795017CC39B3CACE31C9DF53276F2E2C32EE822F47FE1F6980640C2FE42440922B4034880CD28DB440B601F7D491CE07CA8D1FA248600112F60FAF32892196747D44F21D64068F05B703F9F0927729830E12B4D5F06261C13130EEB5C56E3752007C76D64827966829BAD566C1386182E7FA77E6C6682899B8106C804B009FDE36E731D8366B01DF8A0722CAEA583936E0743855A0798B0561D6C56A687E03044B4C665C253B627D62C50ED999745B26B09F7A83595BB7A4CEBA5142CB166691E505720746E9F5A1DC1708FB731BB97C20EC01DB4F8397A0200CA9F97A8ED7E82C2BBEAFCBCEC3C9DB76627CDC95E6DBC531B6E5638B3D4E739BBCB25DB9C2CDDB21A4C662E8BA0F3BDC2E8071F9AAA5B899C0D339B1E211875E873751749168C7C2687846BB48BB463F749FA4B223DB3D317273392ED12C972918016B70F785DA15C27A179402DA2F012AFB6180DACE0F18278D81901EEB16B281A1B0B002E90AD03F0147BBB4CEF4F378FCC6F9C3EB282CD11BF10C18C667CF80D8E1CF1208E10D6A367E6C069F48262F910AB2D71E6CC4CE4A7252D263BF1E8A8414CA0260B728DB8AC56E80EA920611030C1C60912345B26800013205A48151386673DECC3031A6CD4BAEBB5FE069614306B46133278CAB6ACE76102869169D8C62982C2042ACDF0DC4EF5702A3DD94D0BAF2B285B7A86E9084CFC56A5BF9A6427CDF4B03EDC2C01E000BE32E798847B51599A1F9CCFD0327AD4CC6531B26050ABB18BC2C5104AA22346D58D09343504094F0841DA59E59711B8D72C6396B548A06665C3496A655304B028B24D7EF9B6F5CE63DCEA75E3F619B2CA47270DED14F67E02F213E400051BDA50C0114F7E0B6AF1B9040C59EBBD387FDD775404DF47911BB46F798721C445CE6204FDA9B9CE694CF88EDD01047A2403EF9F1A92F0BE62024EE4DF4E46C74DE9BDE3B2DA611BC830A6A787F04AF116F4639538C2C12F265CBE493D1A48E2281DDF2A9BD0DE6971991999709F02D57AFB86091BC884F4004B0A1973A09009666E273DC5192D03F5EE26F280B6E5B89747BF2944C876C514F32D196861A9FE2AB8BC35D8B28BCC34E0FED316C7DC1867472DA5E919F4A89209E5D4366334F192D3C4FD40DDAE189549E076B05E3392C3B403B1E038A6E0B260CDC3C2757EDCB30224D392C852E928C9AFA384834E802F656012A9CB1EE5C06AF17A2288E403303F7392B00BEE44E3D5C7A5715444316B11552D1A3DD6B80F3940382689612B3ED2B17F8CDBCF3181C7B91F5B96B488C73B25D785AA99602301CB046D0D2E4DA80036E13BCF4C48257114F4A20E4EC12674214E686D35B1B876C70FA5C5274D61A800DED166ADBB59EF6F5426FBE57427E185BF65D676408675310BD27C218B895305DA427C3976CB87AD38CB6E6BF866050D296A7266161DDC4F281D890EED4C45836C8363BB70F9D85F7EC74E29F4A6536DDBF5D89227B3EE08A7FF9ED82BF0166D8E0D280D108F5414E84066C4BE746162983041771994A29653B5A3016A5F9E46DAB5F02532DBE8E52C7E019DAAE57C9EC428677C8A7796A0F99C442E125EF6D93F38F85FCF740A5D84B6E623BC9CF99FE854B1C2328196397AFE123F1D47532AAB0D8F5B439F09B01D5F15674EC75F0C13648E8E2CF8E50CC26D36B94D5D3D6170864C686F37C12C8CA8B866173E1A1C75C020A04DD8ACF7D621682E65BB094F4A724CD894659E9C8364C980DE6A62A16957D6778D2B6BAE80CF4B088E6D2BCBA6CAD2F2DC20728247D4F584076C25E956824F28DA0E9B1F9FF6E7B57C29045359E799572477193DEBC8FA783D0D90A70067C581231628B89724E7B1DBF7E47598FE49F2E75E18B97C5D6292A2B72EFCFD0BB563E53C37A2DF3ACF1D796E2871AABFCF3C19623111A7F44FE971524F875456DBC57EEC21B7A01E4BE20899C02A3FA824782F07AA0B35D5AE916DB540260C91093D708D8009C3F3C1F45698905E0F87C7DDEE569D6D426FA33202D8ED950226A4CA4D92B89999E056C2916EAD913FB761BCEE0427FE6D268E575B0874B77A145E9F0BFB491A2C1C52B0E0B86974310D8FA674CF75CD82749E36D30AEBC59AAF00CAAC53AD7EBD887D68C61E32CEF388A1532E95CC03DF4514BE2B808E1CA6AFFE5ABE90E897767F1E79E6DE55D1F865FE23CB77F5B74CB832BD7787584CC0B2DA699BA62F5B2660F43CBA28B009234B0FDC2962C22DDB04CB8434EA9FF6FA58526837D6B02395D77AC1E2DAB7D1E8BC2734C0F5D02B83CDD2743FC112E33695DCB742E340A1216BFAC8CEF2D68877D0DFF09742F05D176E6291691FB8134D8B561817F4D354409AF4248867A7999C93481771ACCB11EAEF33A55F03157B11BB5FD11335F28A72D572453FED55EC8DFC23ECFE25B8AD5CFBFB7F29FA0A57FE7E214F5E61CE658E2ACCE473F18E5C4461B8E1C286532AABC9E3A430853ADEAF3113B019FBB83DA4052147673D7A6C8289130A28C16FD10C660E126E3327142440B88C113332E1149980BDD927FDE96DCADD47D9F7D1E8A20736A1C7B2094C284F0F4A546C374CD81626380789A38595881F32572C9B1E011C072C0D94D6B7BABF68194D9EF5C72DBEAE8C2F7E0E6B9793393ECC2BFE46B1D1B7CA38566AD8EC5CE580520CD6D8C87C28CF83F53F150DEEAF7F21AF53E24B1125F29FFA755FCB1A0A0E21FECC84889810CF0E0C134ED826748509C71DA241E808054C185E1213BE3213321D27A07744E1F2986D02840ABDC34E73A381C53559EE05E770422CD2DB69000DD02C30130E85091C745A265832889BB4C19E4C622D46B655A0F8A71AF1BBBEBBBF6B6678EC79A9959F7E265BD2232618C57CDC59CC793A85F8D84779EC213B6FDF8BFDE91C7CFF312863FE6949E8E575EEB07950CE4B1EBB8523AFC83712BDFFCD1FFC7B6EFC237A28E730C835794C38334C38837039C2252D0EEBC39336F59FA28334A0278978C504E308D196E452D984AF636710749C702B42B9A3B37EEF1883E6EE7E074B0ADC87870B1F0D7BBBCDEE76BDB75DEF6F57075B95E96199218B1578A3BFB3D02C444803E5E164FE0CD7FCF280DE343FD3D7EE75B71F4AE1C626AD5DDACE3AF17FD6EBB9709095FA9FD573FC1AF403AC1442F32B2DE4F635164AE8030A601ABBE3FF88E6C297DF8AE4FB5F8CE8716D2E3437F224F97B7A787C8825E3E45B062CC03113AE71AADAAF9328DB8FC654564326807784B3D5DAFD43590B35ECB0F86299301426189B20C5845B4D8334F599802585CE6E8B8B6BD9AD2C01967E1D5926F4B6AA7DC504128919C4326C923B845976E040822DBB960C3B9A009ED31F4EEC90594E6C01B89BC54CE4E302D0899FBA5699BEB9398DB95EFB1CF4FF0DE25FD1C77FF043E21CC47323B4D095D96174C6C5907D45BEAB9DFF9D7C7BF583CC8B310A899127830DA035194EB94CA1987049BD77FB3132E1B831040E70C711A550A5E34855D0C62E5A1848B84C4CD0CB1C0913F4563AF0CE304E1850A8D0DE6EE1B314DC6AC1E3DE3E33A1063601989009132201F76E2C01349A0549FE60A695BC1DEA628FA6BBBC2F3B0AFDD193CA78DA9E16EEE5B22ED06F5D91A14A4DE8E8BF9E312C807BFC679417FCD2B182AC8F5D075C0D447C2E9887607949C2EF7EB728D783EA5DBB7F63C47BA9507BF317723B679F5FE6DFFD3EDF6858272ACF963FDA0ACF38C45ED870864FCDB1BF35268E2EE29FC711AE7BB7938CB8987046AD16DC6771DE1F5F7A93D48C291898F99CD2923DF93A366535152EDF65D6205057F609D200044285D666737C3592D58271E1A309FCBFDE6EA3BFC336A19AEE0B1358B5DB0057D247EB0077A4C18CDAD6ED1C0E915D3DA9CF287EABFE0F6D0CC0EDEC52377D16D7DF14806CEAC6B9EFF930945EBA6F3F7668769E89F140F28A3CFCC9E33F8B00B750627F5F033D3F68B1CEA08C5F9557417F3B67F06FE4CE27C32B46639EA3F53A370A3C25B4EABF4F71ED476CCAE002F3D7E4D75934E38EA3ED1275A18241908E235ADCC59490AD17E4420535CEE1F2F7C9E4D6B709B7122748EE6870D21B9EF561DBC3293B8DF1E528FB2E4C98DE6047AA30016D4275024CD82B6002860AD477004C300488900C7B6A42D39E2E7B45664213F77879BD5C26DF4F413025EC9FCF726E8FD3FA45D998BF0F3DFFC804ABF8D90E14D320767E48E890CC670203D423401EE2AF92E1360E15B906F4FF8E09778A0956FE914F55C887AF394361F9C04CB8C0F9FBBF4E910F96093F71054862C20E97D514138EDAAA68207371AC359071A0CA053E4E8A6726D8A8C06E5D650D9970DA6726F48F7BADF5C6E87C00A1C28C5790BF4D7121E2BD2630A1BF830ED278AF3CDD4F48DF473C73CFD8046C64071ACC1CEEB1977DBACF3BB1BFD447E462005E04F3D8B63192D8648225C05F3AFA573E01E6D12088FF0A932A0596210A7D9B626726CAF9337FA3E073CAFE36FE3BF927CA3E0FF7FCE05DD1BBF3DCAA57D812188ABC89D094B8249B701E81116000508139F9C91D475BC404ECB3704CE81FB58717FDE1A5CA1159B370198C8CC64083EFA92DA5B988F9861A2EC4269CF651A8231598303C1DA4D7E399491FE17A32C8841A31A136DE2D59264812899920D6207113F9CC84A6D9BE9BD5611C2111B403C401170A9B1E4617095CFCD1EFFF87A1EDEB7998B96E718075ADEF19F1FEBB7F50E473C66F35131419EE8C148E1422781ED0F388CFCBFD1CAA04C478851537267AF95EC4872267096D0291E1373BC317C884A763C384DD0A4F5C1ED04374FACC84CBC1F0D2E4882E862E59A4B34697F288412E264C7C324C5C9919EB097D0C15CEFA60139AEB8DC1510F99703FE5452EC6E7BDFE3EDA84C12EDA84E17679769490E76372A954034626ECBB09DDB3FD08E7B31EC4565418E09E95F25B1B01DBC215C4C17F93BC7F2599F3C76CFADF265BF201ABBF3F0FD9B73E7CF3FB77F3E53ED8B7488D05AC7CC0BDC26E806339FD55D0E7659E65281CB49FA8901B79C7C966CC286C7BFE1A3D5FA378B9B26B5A04F22A918EA32D5CE60895F2113D58ED08D7381A9C764797B2F4DDF01C55F9D8E64F03B3F06534B9112698268B94849990020D8809C001B00967C8078898FB075D081578057964C2657F70D8EAEF5A2654A6C0847D6E978F393E061ACCF693D9413C13DCDB8E792F14FE79685A73A55B5D7941E771410A4873E01F39FA7FE3ED14C2BD38D9325FAFFF8DF7F20AD035CAADE447FE81BC8AEF8739E39ED0751E6277F05D8E1BF3E8A1DF2A8A4F9E59E09847921F20F1CBBF63DCFE485E1E9397FBD2CB2D3E819CDB8D9E2FA8BABC170F76CAC34379FA32D2E0A8D33BA4F5B171498BC1584708923F55FDD83839816C02CFDBA45ED48923433ABE31DE11DA0472908009EDED566FBF3BBE185A264CAE06E893ED2113063B8E0953CB0432084C83999B23E226B69AB97CAE433D288AE1EA4E5E22C8D0406BFD7909EC3F24EFFF9A0637454CB031AB537BFF6BF4479E8EF794BD82601ED90FEAE543FCDACBFBD8C32E43DFCAFD9FF63D2644CF20F3ACC4EBB62560085F9F71FFEFE8E53FE397FF22F9EFE8E5BF637CF99F860C8F31D2E00623B1E72FB8ACCBEF8B787A18A5FB717FB73C38ACB36B44060198D01900132E061C168F09F4C30B749686170317305CE49870AB7AEF980CB6036F70369050E1B4D7DD6D7777DAC333EC48652660472A58A55D8C98073BB5E176757A549A1EC65225D8A640799FACC1214864A6B4468E00C751180D73B7BA8B014CD2F3CA0F76B535F85FC0FD2F2D40A12970AEF6FF87A6F7803E0FEB46013F4482EC401E8D067D20AC3C060744AF8A7F7040A14775E463D1E9854CB847A5CE247966C288768F5EFE2346F9EFE4E557FCF22B79F99DBCFC8F95D2CB6F92FF036F454203E0C3BF891E8F8642E43B3D5FC5BF2F921940E82CE19ADA60A7323C6818BF0869D03FEA0ECFFAAEAC66C3832B15245C905CCA447E7090D02048135EEA6C02BA46D998BAB207201831835938ECB6B75A83E35EF63DE5858FB29BF1084285BD0684CBC0840116D78009922ACD76A2A74370E6929F47C9D3A1CA051D999ED0C002E820F86A8EFF6383AA42B7BE10F7F994C5DFD49BF269C7E280728E33A35DF6D794FA1C5CCE9564EE48C084472D11BDC5B0CE7366CE6DDCFB8321CDFC7FF4EFE4E53F9397FF4A10EBFFA78402F87E26F9BF6525A59797126E9FE980FF214A20016244FF7F5877885C23B68DB7C9F30D3E49EDF93A79FE52FA795E999D616E66B8190F3793C15A32D8AA0DF75BE41475FA68165086F8101D41FCF86AC4BDD92E8076E1B294D5AC7734217728953881BC2361C2393181D247CC84FE113061C20B1FA5DF8809BB8DC10E33A19AEE2313B066CC09D38384C8C06B7B70771095C66C4D40B7459CCF89830BB5BE25C6EB6AFEDBDFE15EC773B7391ABC62EEAD6FE305AC9A007A27A77AE72AFB3CE219F4E4313F5A682605A239C097D2570B9990D7FAEA2D4386673EE61161FAFCEFE8F93FC893F96FC2FD2FC07DF9E57F2A28CFD597FF5B7D7961A9BCBC94512C0D9ECB440032024015B00C20A0FBFF3342F90F321DDACADD959E6F4ABFBF557E7FADFCBCAAFCFC527BBAA8A527F5D17E7D74D8181DD5877B95FE661993F87BCD1E308138D03BEC021F86DC85CA9504F68E0C0D8617286216C826E00398AD35B871531438681E93448393BE9414C0261CF5E4B16B76E1A3BB14A7710213769909B5C94119CD82188458E408D7E1402FE8C435089968B808FDFFC8C57FBD945388F8BFC9A6FF398BF26A78FA973AFE3190C44931CAAD584027CA2C98F11F06B8012B0CAC4970841D18148038A21C1473F20CF25F80D43242FC77E5E577F5E57F6A2FCFF597E7C6CBFF6DBCBCD4496A06F10CFA8A077751F9E4F3FC0F5CA44C86A28C46E33F8DEE970020797E283DDF27BFEF4ABF6FAB3FBF566697E5E97979720C52191F564687D5D1616D7850ED1FD5FA87B5FE41A3B7DFE81D34BB7B8D2EB8E587ADCE5EABBDD3EAFEBFC6BE1EB491AC6977C3379C705287861B5CC34D1C7E822F58C1062BD8600D132C6282414C608403239C18E1C0340E4CE3C0340E0CED40D00E04AD40D04A04AD402007039E60612698C0C1060EDEC0C106BEF573AA4E9DD32DEF42D1B45A6DC9337E9EAAA77ECEE92364C2889970364AAF92B8A7A633D88606AEADC6BA88AC2032F8A2EA8CBB0A3877944058E054010BA9BF75079FFBB8DDCB92D4113D943639EC3926FCB19F7F462614BFFF547DC2C544180A1C07B8558E9DC267CE04AE7E7AB90EA738DF563B13DBC36A457CDB88C17C0B016C35E38D728D57F36DC9685364071CD8EED7E3B74202D89325D9420CCFE55B4266BE2CFFC3C6047B5933B209D65F59BABC7FFDC1F8DE7DFD0BC0DD456370B3BD82F5C422C4EF8A31D6C99CBE678543063E9E253E26BBEF5EBFBE7FF9B2F3F200B6FBB206DB7B5EEE7D9BEE3DDEEFD5377BD5E55E79BE579CEDE5A7FB6CD969072C3DD9074BC04EBB64BDF109D8C1F8FC20B93848D1FAC9451FAE8C8E7BC34F5D40FFE0B00F7C181E0E800F60C9C5D8974AAF7C432DA52B9C3403AAB599E039C034D0C6C24C4FB8B3761132E1639F36B928C12A7A286D7228EAE88F0E08B8F2E87DF10117593FD24E4FDF4E1D079E8406CF97C881971B9AC5E7691F9EF0F9579D2C195C6B67C27FFE8106FFA8F2FF6599B20971BDFE0F01E15D4C06F7537405718F573CAC6DCB6CF19F974503EE68EF5E1EC0DEBF7ED979FDBAF3FA27B8DEDDD71FBBAF4FBB087AF4E87BAF2FFB687FEF0BBE01E80706F70C7D32640511006E06A9F3F72E1ABBF997774ED93FBF73DA8605D27762DA9FEFC97618FDCFEBDDE7D5EED382A03FD9ADAF77AACB9DF262B700F42301F6F3B36E7EDECB4E7BE909D9692F81E399D8790F417FD94FAF0660D9CD30B91CE0CB8B03A4C1797F7C86A3D7C3CFBDFEA7DEF86C084C003E3019800969D034C03291AB1D49F9A885096AC204090B2531013E1462C2856B29F47EEBF1442AC5041C402A27597A7C901E76D3CF9DE46327FBB8531CBE7F3CDB79BA78FF7CF9EEE5FAFDEBCDFBD7DB7704F777B82D073D47FA75FE5EEC1D19FEA55F9C355B3066B2D2C0FD45EC9F65CF5BD5BDD6827D6BC5DD2400A6B0F8B22283132A9B04B66A1ADFFF0E6D8DF68A208623E058A08C68DE4503403FEDA1810B6789C298668CBEEE0BA6AD3B3F88C10DB0667B35069FF0B22BAE7DC7546FDEA3EE7F7A6F50FE1E0D20CEBF18FC865F765FBEEC3EAF779E163BDF663B8FF7BB08F49BDDF262A738DFC9CFF7F233F0EE00F44E7EDAC5DDB84E05EE70C47D4B0F32B0F383ECE220BBECE7D783FC7658DC818D8A5B6FF90D5A763D84778106D9F520BB1A826140B81C8C2F06636CA5F541260D3E75FB7F74FA1F7BB881EF215A723916C427A28EDCF85DDA6CABC93CB6678228223962DE2CD316C204C8987122D53101478FAAFB3C3D22267CEA80E59F76F38FEFBF9DEF7C3B7F074C78BD7DFF3AD979BD7FFF3A23C463C4A73FF957F15ECEE00AD83B321691EFD01E0299CB656C41987AC7778442F194783142A4C07441E7FC396A720FA35C6EF3454072BA7A22F6F09F485E7BFB6AEC4F67E241499FFCD879F9B1E31C36FB6C8772D6E21DA7C8D17AAF7F8B359DB717302A63C8FEDE13DBF5F662540D8BF8BF8CA47912C47F172A7E05D7BE0BAAE619240DD8AAF36DD679BCDFDFDCED5737FBE5F57E71B987A03FDFCF00F7E79DF4AC9390A567DDECAC0BC7F4140D409F92659783FC7A8828BF1B979371391D57F764D3A4BC87974931192319980F709C8CF2DB11700099E0C8004C003E0CD20B7C44C8F8B43F3A3E181DF60640830F9DD151FFE043D731C1B70EA49F70E5D4913021E16A126FEE524C4BB43814549E1894278C8161CC04A00448A360BB172CA416B8B1CC712F3D82B0D04DFFD81BFFF20E172A1CBF7B3A8798F00E03C23D058105FB3CF8DBEFD35FB4FFFA3A7C7D1D918DC912677F27EECADFA3D717B0E1EBCBE0F5A58FF6DF03B29E39E939C9FBDF0E7E32E0898FCEF6027B51DB0D8D6F303FFBB2EF15059BFB417DCB40F6EFAE60B7E37DB0F3C45D2348BA2DC0F51626A02CCAFF0EED454A909A953A5FCE807EEF8D5382EFE468D097EFBE7ED97BFDB2FFB2467B5EED7F5B741EE79DCDFD7E7DB757DF01B8F7CAABBDE202A54B8ED2A5C3AA3D3B434BCF09DC04742F6018E517FDF472003045284F937A956FD6C5E343F9EDCFEAE97BF5FCD706ECE989EC07D8E3B7EF9B6F7FD6DFBE5670DB6695D78BAC9AA760E50C29417C104A880119F29B018585017E1DAEDCEF279039001320267CEC1019203274F1F8F1C0C7842B491568EE8805924F216E6821FFD4554E8380E04EBC64FA49030233617884FBE1C139AED7A1253B902A24A783D1E7EEE8135AFF7F77073FEF8C7FDB49FFD829207B3ED9A94F771E2F769E6E769E6E779EEF775FA6BBAFCBBDD7877DB4AF9DD73FC9BE775F7FF45E9FC8FEEABD3E1F20E8FF167B1DBCFE4DC6274821BAE86D283608AD2F76105AD3BFF6DA4573EC80F71B19E46E582E7C1F9A9453FE6B8C734DAEA6FF57AAEF8AE0BFA8620320FE610C6412781040F3D7FD9707B00E38E9E725DAD3A2F36DBECFDEBABEDDAB6E10D0E5E51EFAEC0BE7B69D5C41647733522CE989F3D96804EE04A5F9410220BB18B091031E92571E01C4CB455A2DB3CD43FEF8A5041C03CA9F7ED4CE9EEAE767423CE2FEF1E9E99171FFED7BFDF4BD862340FF11A0FF50A201558000CBAC5EA4684883A49A39C3F8301939BB2303ED743D00032985820A9800F43B3D484E0FC6473D001ED060F0A173F06BA7F76BA7FB6B67F0B91FB4935D8FD9AC4F90B44198503832689230B5B5A32D4C181D0F215548CE8809141690096783E1C72E64F1F00B75FEEFFB83FFD91DFCB233FA0DF9907C007B9F7D7A5F1CBD2F8FDF57673BF5C5CEE67AF7F176EFDB0473A9A7D9DED37CFF7949B6DA7F7EA0BFF4D7CEF39F6C5DB097EF603D3065CBCB5FCE5EFF3A080C28F4DC477311438386091D2F727CA130E2ADFBFADC41FB6F9B3D471186CAE71EDF522B44133FFD2C572264839FFE8119EDCBF79D17CE6EBFEFBEFCB9FBF275F719F4F797BDE787BDA7F5DED312EDDB6C17EC718A5A7C73B7BBB9D9ABAFB0EA525DEC95675878C1DACB19D75E3AE4C811EB696022D0D168D38733B60376AEEAD4519A833F9E24C57D024EDA79F72FE5E3D7F21BC09D800E10477C3F79378F2F7FF015BAE707B9FC3F2B3476FC18228A473CC9D15620AD990309D040F94096545361C2DD10394034C8AFFA60905A70144A8E7BC001A4C121D752C12032808174C75252C804EA275C9936B3C60AD9DCC573804FA65E141926280DA882E45A0AC7435EB956AF7067EDF472DCFF08D4ECF67EDD075E1EFCD6E9FF8E3B820159992118310EC5DC796F7CD84D0EBBE3C30ED9BEB3A33DB0E4682F3D04DB453B42CB8E77B213B4FC14AD38DF652B2FC9AE76AA1B4CDAEADB1DB2DDFA8E7003E8B92718A9CDD0BECD435B90CDE5A836DBF54064BB17635C82DDA2E1570346AFF7AAEBDDEA0ACDFD62F04B9EA1816FCECEF6B253B21338EE67271D36A7A703044B3905ED008D71EC4EA8CC722AE2C41900055C26384E975922B209DC2436589A03CEC0EFA21421979C6F96804B466A8988FF5281E7261CA32F273103F87E4437FFE3F1E9FB860D6FF8513F929B47FB523A7B20C47F41C43F0AFA1FE17C05963DAEC91EB26F0F3959F6ED4BFEED4B01E78FCB0CAC82DF7002D08774A29F5D81E84201969C76C7C79DF1516774D8197EEA20960039C707B810E70AFE455935CBAB695E4E7350FCC0019046A3D361EA3ACA66AA4299E02E26D85C73ABD5481A719E30AB5CCEC0D2685EE5942ED3041E8D644BDE9C8CCFC690A1E32617D45CE3B000E18632F75EEF977DCC5A3EF686870738197B3A482F47D9D528BF1E6597C3FC6A985FA265147C13B6F3C1F89C8EA7582176767CC0A40FECB047FCC12409FD0119CE841FC6B791B770C4F3EFF2B9BBD275762C47B49E9CA025CE7AEEE4A487E747743C81BF442FB8ED846E3841802614B88DA11BE6F300BBEA8F2F08C1940BE65798566239853245F488779262A22560D53D5A0936A5239983F8943546EA8CF47705AA669121F4C1C01FAF73B0CD17453F83D89D90EC111A3C79C398F003E450F5FC54A1EF7FC29778FF9FF0236C05C21A3F36677340FF42B8C763067CD8C02F03FA07FE51F06F04C173C19E1EB88DFF87FCE7707FBB63EC21805ACB6EC739FCC367904E14F5A280236E560706AAE61E2D9F80E5F96D86CF04FCBD276D35CA0A74DEEE4A92E62BEC8FA1592690D7CF9903538909DC6066267875A44C38C547EC0C3EE3762FA08BB88294DFA4B4F3C541F7E73DC85A8012A32300741FF08D34B819939173BA7596936537E30C4822370061D8D0A55D0E21643BCD4A27F09F921067F0D1A26784AD33B1187C62270844343839263BD9628D9FC5286C6EA0F25F9F8B800EC468788E7F4E6B97E89891F6CEE0AF3E44BB01F355422782590D3BA08F2B853B221E409322DCC92A6F88F252E13E13F4CF53CF01B27A9EA12D733221039C83EC7928360F85A3C1D7D28919B2E71FF5F313DB06ED2F317C89D7294380DBAAA73F8BA7EFC40136C70442FC2ADD2CD1EA99533BE8ECAF58E86309155DC689A2BF43864E0AFE58E8346FC8DF03EEC19645B928CA8800EE88A8252614C00448628109005476FFA9EB217826386E2843EE725EADE645916742E599308FABA809C7841108A40F0785670236D7204D0195D6FDDFBDFE87CEE023101A008A3D11529F64B75814CBEF46D9ED28BB1BE764541670063C617A2055F0DD91B7097805AC2AF027E477EE1EA556CEE5673D32DAF806033EB9A845897178054FDC9F8D614A3794B784D43B32812CF96984ACBB0EC677EA4BB26AE2ACB476BFC504EB25C37DEA815E4E7D4E1938FEB93352DB99B7391E37604B3EB244612BD0D6E2B91D88F36F00EB3F4B40F6F30FB0EAF97BD34AB0A7AFC513FCE03A639457F728E845CDA301D029E839F146C1B3C7CE1EF5FD31C5CCB33EF8BB7202FF84BC064F4F5DDA7A5182BF87FCB3C24938C6BDB67E69EB217E69F17AAF5640FA0B20042690BF9798C01D652F90129B406324B91746C9279722902044A834F2AB3753A241C084494EDBBD94C4847CF819F780E9FECF1E640B20E650C99D0D240FA362309381909D11B83D226F3D1FBC11DA9803F9C491813E8158844C483C79EEBCF157D82B1685FC8D7A52082E8B09D7EFD43DABCF166F4DC56F55236813396E4179A5A66F45F74CC64EE44CC70EDF1A01BC9BA7734C2E937A666C9E549A6E120D36CB8C8F7C0209289CA04027156E9800BA9C84FB2A05230D937F5B67AC619ED400F12CE257E4E3E788FB1AD25948646F06C5753FBF04730E9E04BDCB5E9C6E049143860A87B6B086D848D087A0546C5665BDC2010503FD52A12F882FC2BEAFD12DEAC2491A1564A076FA9F1C138298E0991096926E8409D3226097F996800929D180C6511397349F8F8109F05998342F713C1B58C14F2544267CC02C990A14030C73D7A8738AEB11FA663500F4B548851BC704D74C112BAC2952D192FCCE858836C3BA071E9D11191870F77A8C8C913DA66FC1AF431ADCA3555CD1831340005EE14E1076851CD6E9BCB23613B317F10713798927351DB9AFC4B08E811E7DCE3DA210AE6FC0F0CE31199E6FE8FE8D3B1FE3713E465B90CDC78F8B04AC9E8E629B800DABBB61758B559A922A95C5553FBF000388F7B273341E7CA02C88D221CE8B8EBA9C14694B01FEA0E82FA6908B1735011DB10EB674562EE811667322004E7AF2333B82C500EE22F6B67826B42C7D28288400853B973C8199905C001306FD3FFA3A6B2491C105044EA34D7B21431671C154A72D20264C2B9B2A6864F8299168A0064CE8FD863BC88362DBACEBCDAA86138809982EFFB20734187D4626D0C6C52A9A313DE224C98551AA6CA0747679E150D4CB881AEF7C45DE0AFB2CC564145DB15E599CB73542F364446F8DC8C6ED86B01BC93D72FF84CEA77265E2DE2DF59E69F30747085CB07BB651AD57D8667A1CB993D938B861AA9F4090BD1F6DA668F5FDD07D1A5F875F06D0AC80BE19B0C32E09D3C5E54171D92B2E7A39613A3BEBF9F214B6143AE971373D36E0A6D45FC6227A588CBAECB3F30226D7332A37AD21BDAE3660EBAA5EBBE22119219E9FD2B7288D55F12642B195727443D185E092B5908912FC6EE1C820C6790220131CF4E070102C3F00C45FBAF394ED4AA6B29909F73E02802E228B63423675798270C09D8C4120757FEE40EA0CA288BA0A65352B2051C624E1B77DA0C1F8086B7F994F2E5D0C85604AD6676270FB9D4B2548094E2B9DA01FAA6138A6F60ADA9DCB357D5BDEA97316E892835ABBA34F98E0B1BA23584F0C64F5A5BF3E446378B97B8616E2FEADE83A3BDAFB369B3834B3114FC4A2DB00F1EEFE21DA1DD9EDB0BA611B8001CA9D39ACA3E17F2C201E1D390E3BE0BC835469D18ED1D2A34E72B49F1CC2910CCB6254A5A50E434EA11BFE1B6BF0EBB30C118F563D3ED460ECF27879960E5FA231FA17A531618277F0F1C672210DC43850CC4227AD7A696E64D2AC85096360C21F07C3E3616A37B0B8C2493BE640B68D09566EF912AA1B37CAA605596B4C381BEFFFCFFEDEFFD9DDFF7FBBBD5FB07BC036F803D2FF83E4884A34AE9A29C54A2A0E245AA33CB2F5478ABCE09CCEA852EE8B8C6834A7E53A8B549641C33E8B1C0B3000C4D580DB9060EC17CBDBD82AB2F2A65FDDF62BBD88F418547243496FE19586D90F71F7DCF42BF3B1EE8BF8DBE125BC8BA8ED7BBB422BD8AEE937BF441D8250BEF0969FF5D0706AAD4B836BDC0FEEA00B3FE9E0825D364633C21AFDBAF61F78B80D150E866270E76354E753481BF20D0974F0E860DFBED48F6BB08A575C09C4C5C0D3378D7DFF423BAA62EE7A25E227D8755D201EFBFEC07C0490279DCD031A14AA8EF4A8D2085285BB3CBBCDC7E7C884D1E928BD11F77F2D2B72645902A50DC2845B573FD53A2993218F9284ADEAE812D51124C760E363F0B5396A246C2C94F5BCD82CC8E6453DCBEB690E7F80F23E2D21B5BD19C35F853A0968F4B85C9E1BE9B167A2927F772C7D37ACFA7FDE1F7FEEE0F1B0833B061C8A1D511B8E8FF4169C24CC343277722C4792B68495D04821247ABDF59EC6FD2DC63F2800B5277C1E21987F2A91CFE446442A55DA8C1B64905F528A85F915D506B0A20AE264468086FFE165F1B8129F6DCCC15ACED179F3B352D904E5D145C6B13B615BB9A7113B027826701C60D08B2D628B1ED054CEB7C487796552647FAE1EBA0C277F0273B523664201B01E9D8D0F3EF671394DB864D9524299400D665742CDC59ADFA2E93232618C137889334A12C00694A48F0E875841C266B3D80C688094C0CD2F70FF8B72B340A32BC810A60ABFACE7199647802AAE028315C9425A0AD4861B102CFA6E8EF78CFBAC3DA9C7A1B47531E7D05AC7C422ED6177F8AD44AEC849D7BE850B2D8EECC7BA8FF2177DC7CDF5DD5231C1744FA61B7A1CE234A6A92CE4914C2E4C519A213E1B9BBE05E27BC5ADAE8AFD3759BD59BBA3C01DB50A3FDF6813817B5583015EE11E478015F972E5C32AA084073AFD141ED7B57FB970A180D91244037D963D397BFF32A0415302854C98966EF1E434C807AC5EF7D1C084851CCCB5D58AEC0E99D0FFD44F2EA5816084507A959A5E9B30819B09E2F89B34730161EA3412ED7724CD044D188687D8C2187E1ED0C39C0B9E4172719342249ECCD9B046866C993B73A985AB211816F17F34EF3329A681DBFED5C148BF56914724474818721AB714B15B38837757EE1CA70CD0A4BEEEAD74EFAE0BFD1C34FAD8CD03AB0B7AF960904A30FDF6C092A3A65FA60ECCFDE6756CE61F15631A715CC72EBCE54AF02EC1BA6693FF43FC22FCC1A535FF9F5CB519BD552BEE3D0196754B04101A60BADC70FC958D0C4EF45B32549606264FA8A269D07C46E86702DCE309E2F51E6990DE6483A3E1F06808F8840420DCECD10704FF12E7B1734F80D62AEAD40B271313BC3A4A8109A3137A56F9C77E7E8BCF69E32C8A1A6D9245CD05EB72520749954BADD4ACA7D1796F27BA16AE56CB47BA5EB206B03CD9060E7D0BEF0703B7FA20E76BA72536462EA3AFB5D7C9B67EF8BA666B5CE41FACAD2116ED395948835A7EDB5A5DFB260234E15889E41DBC533E21D657FE8B44E4D4A27FEA7A51D37F75AD7F023CC73F84DCEC0AA084EF65890F1DA367D697E1DFCEC8A1521306FF74A6B9203B544426FD0DF2019F199BCC55D059F066754ECD130D3020DCE6E975D6FF3C189D8EFD52FDAB900CBA905F07902826E453336411C584FB800C8609E75A454D202FE9FED28554013E919FB213A3DCFAFEB9F3FA0D26383ED48DB0EBDD15B7B163FCB1CB7440DFAC0C58D75E1E38E4D151B165811BE095B12527F823F4530170D7CE3CB805DF758CE040A8E06F65D06F4DB81D0404FF1BAE7C84DC84EE5C428AA3879E0466BFCBB97F77BD722EBF5626A0B3C7F35A99C0D0172654C284BA74D5A1402039F42FAB58022DAA200E483EE03AC7C207D54521134A3312E799902B4C3920DCE6C97576F0A93F3E1BA721015CF9282CA1EA427E9F1C4FC3B923211B58466C4175C459729C349F8D8109D4CF4B8BBB42858D2BA8CD4B3D5145C4DA4979A224A94841B1AC72E24A5496AF63B0C6F592A9363E32A878D41EF7A13540BCE1CAE0DAFA6363EBE0A78CFFAE9B3286E9D40AF4F817F0573C8D3D67D6F5164514D23B4E7F6BFB9F13FC6F78915347EFAA04D214D94AA352920495FBED09B18A9FB9BF5E99B7788FD140084958F031616E9CB12D9536150BEDCFE5307ACF566493029830BE4C7B7FF4C717892FA1861973E268900813529F2ECF42B3177D3EDDC604403F24CDC084DE6FBDF422296EF36AEE98509B521ADACC91A19AFB14C2E8A56AAB795D24053EF9BB6E42261891F00F0470689698204C5050BACFE1B0A0370B3A2B8BDD401469C88AB12E9F107C4E4006AFE003946F89308D1F775FBD6E5CB1246932A4991E447552170A6A876C5B246D67426DB3E46A11E609CD869A84081D28921D253C25F0DDFB060DA67E32C20194120650470932E1C0A5CBE1D3354D40480C13B099904B1555F63FAD5C53591365914FC4844B5EBD491C104BCEC6BDDF7BC004080EF4BCA9A00CE7F5E25CD55155B9056E12372C61A273C7071E6ADA5AFB736A67A568B6688899109C47C1A121E5ED0D61CAD1122802208612FF4DABDA6E6BFA7E81B217393E55083CFD4AD35CB959E11EFE605BA25CDB80E05B047ADDC684799C377B1AE803382C07240E1433CB87D2A510731316EC181C99D52AE1E01D154F65241B608DCD840F07094F9E42C64CA6EE3F6E305F6758429D9812EACC1752552359B281FD94F0C6163CD27D9992212B70DEEE97EEE878840348735762737E45322AE7E0E71C0D74E644C34200FD2A8A094BB7345467BFDB058002741BDA1A043050AE6A0D11EBD87387576C25670B70B73264CBC596CF89B490F9FCA6F4772F2D491CBE03FDC375A4D61A9166CF2E43D08269C8075547725BA34E5A36AE187534F34FE5F04FB39CB5C481A0783A351942830F8A4E6602207B04AEF9838B09DA4E0EF860EAAA480329A14A9250855A4888C1DF45D5AA9FC690155CBAF4C02D71A0B030F8D4EFFED2191E0DE1A31DACBDFF90D290E740E5AB4941ED4838B36C6608656DBCD4361A04A2855911EA1991E00A7A4564D5C2842DE2CA8078BB7CB73974149A9A94D81A0A14C1E18F474C581A2618E85B391431E1DF9A07BD54452515F64C089307DF3FA66DA5DB878B16B26744BC9F4A6CA5DD51A2ADC4E90AA9524BCD26902EA7A3D311B6D52EA5848A1C68A4CE9E09596E5626A8466A61C24C9840FD84842D6A368F8E87DD5FBB83CF0338071E7B47A26AC7E8229B1E8401C140BF992AC81FC312606369B0AC22D08B17AF626BC68787368164316DC960EA39DEEC27FB3CC108A420FE60A59FEFAC625DD4E08CBBDF5BE8F8B548EAFF2B1CE8B93AB47215215547DBC9507B335E3F08115A3B6A368F2591D81610A245F141D954DF921BFCF49B01A869275BD11232E12A1D1E0F018A614A60A491F6D778A91A2DE477ED8830026070086B47FC5DD93D31C167CC972967CC70055808A942FF531F0213E43191E2B76522877BCB07BD27AA1ACD632164BA3CB6768410AC9A705C9B2B51B2EB144E24962A2B9636912222B634644F43CAAF23C457E1CD018E2BA26E15E13EA05C10375AEF746A473F3088247565AAA8ECDAF9A2B14610F02775A09416262C106DA4841AE2DE3E98CC3CC938488835139837EA455E175585D5E83A0BD4D6F9D2067376970344B1AD763C6494B776D6FCB811240937AC8EC2E538B6A26AA4514625D4B4BD8ACA5D85535CAFC3334FB47E2D68900913FC3CBA3F09BA6961C1544B4C8B320CD68E0F5A36D572C776266C8F062113B665CC5B9810FAFB162887C59C750CE25A261ADA64521BE81B7CF0AD31AFA36C58D02E411DE2BB6A3021BC1E2603FADFEE1B6ACB06F4E7E6A5D936CB3D9646AA31669B5165821652AB60478970954CC40176CFFE7C820101E72CCEC7D8563B19B9DCE0DA332168A82913384F887DBF4D91A95F21E37719D94FDA43E098009F9548D20CF108C202A40A387DA47A66EECBA9F4B21113940FBEC9E01B734A8C203A477982D240201EBC0CC860DC733B191AC9C0037D60143D02DD52351C7F5B89B32DEB5026B4823EEA85556D02A9326F45DC08F2DA2002D45BAC2D1404E74200DB590B4341504132F940E9D16F2D8A06E13C45D4EB6DE927F8EE2F9A4E1C9D8EFB9F06D8565326281FCC73433426E4A6BBDCE81B3881E463C27D9992FDE4B778314C609934A4BD8F0687831C172A100758BF6A0EB0B4A1201CB258B6564E03F3651013161A15D526A4AAAAEDDD4A7DB975E14D7B400326E0145AC484655B05C9172E2D25CCB78BE8F74C0825507B34587AB857E14BCF84B581B5240978B208158EC07A8B400AE9E17ACCB50C563438601A6A01077C725C33BEC3C9A2AAF44CA03AD2CC55968A48053569705F06BD857BA91D4DDCA8C5F06474F0073598AF990099260C991C7DBACC49C2C430210808E1F97D49EAA8A4F50997BE6AE4384034007136E2ADB33FF5E1D36D28F05A28682094C1C95CF48FCF2BBCEFB7A5D566D25CF9ACB165C02670F0567333B2A33B2D82D7EE1EB407B228203447182C1F6C4F237C0BFF09C284A6DAA95BAF3498D08C128603CD2050B55D6CDC192922CB843820D43113C89A4D34C78145C001CF040B745F20AA3C19E486D2D280E58A3F7734C8EF8AF4268724414BA8CA04427F160D59B8664214139A95D369A09A329DB6B083D94E1D5D2119462763377D749DD1B4A901AB1240DBC9CB70A2DDE60FF3B8DFACBC42E43986D4F6C37DF1A4A140EA651568A16D7DAE2D01C111E6C1C9A4985DDB98B0CDDA7CBFCD625BDFB5D71D649BE86F2586417334831447839561D1C24783320E08B59143B5597F63474D6BF7D622504751438DFB03655B97C00C45575A26C23BEFC3C682D9C9A2E0CD5D8009571948A3830F7D5EC89F3A26D0D112C0A7CBD45D966642D1563CF51C98B8A12362C255E2D72748586026403CEAFCDC81BC195B0AB3D227BEAA76963619287D6F61615778F852D2362670E9D0040DD32BB5F9E2D208124A88DF105171D0103204A248DEADD68132F9171C90928ECC484711A09503ADF450C8DA1B8C5E8AFAC12101162D4C2865ACA834047064582A13A44CB48D092DA2C864CC210DECA6D3B1583255D43C4C06CA165DC48BD44A5CC22F8523DCD2E223318141EF38806490053A9957478609CDF1BBED26555457489598C0C61A89A78F92F3B11B31D2D2E72206BA944D83F29CA926056D694F8C4017B54C9285ED330BC42D5AE50DE71D7120B44A6EFB7794A8243DF0DFAEEB609AE86F9E9B3AA97CD77AA329C75BB6F4B0B6100F8593234F69DF3531810BA685CD0D78B8684EDC98B7F0212E13E993BD67B2CBA24D91C3B0907BB114A60792B66ABD48E72C582081341A5FA490241C7CEC4B2FD9C484A63AE2C2D17DF08DD178851F3D9A1432E78703483FC58B13E025D280655272F07BAFF76B777C3A0206EB9EC19E0C9615F3F0380F08109121CA0DFCF922E2406D5DAFF79AFF9E002B0BF78DC998253EAC1A7973ECB0AB567C6B33C147806693B8F99B98DCC07F45583232894134315105B06E3D97AC5AFC7D18139CCE69340DA28269C4017DEEC63C080B2E336E3C8B20F829C3902826E85A7BEB9BFD201049173785FAE1A0EFDB6A4C00C99B750C9BDFBDA124C1122F2E1CF9DA5461E78E2645B88EF992E343CAF181771380B0303E19E1E690DA696E6542930F3113B6585B73AD410681C8DABFF401A13D56B4D0A08AB2856D6249824350E5DC9E4244959F6D2AC86A211B8282B7226BA187FFF0B62C595D7EA38FA659F242338406251A7973290F9E294326C46D044D8EA761F134CA104C47D98482D225B8F786093C8F7D930108810990345B26B88923532F72EAE8866342E171FFB62EBAD72F2D7F12E8A73E43201B1313069F07DD9F3BF834CEFB02C34293098B06137CD9C18CACDA726A8345F8476D54902C13643556A300DFEC88B5C4848DD69AE274F9A1ADE720EAC540B052205ABFEE7CFF6AA36A27426A4C8365F0CB0710B7D703ACDB618AED84699698EC4BAD879A112349156A278D9A8377E67A90212C0C3D167561B285C8192B01FCB043386967915ACC149A52E9277544B377FDD1E93821979FDED8126A2890689BF8CCCFDE5596065A2DCDB8AF6CCAA97885D491A741E29A09CA0D9A3EFA05A78FF2DB3C66423320349DFDA2F427CB380E4812DCD2598B905DA9875E6F42EFBB5D23F962EBC691017E9603C283E3832FAA6A6059B72335146681937609ABB91EE0353C6987F26A2B13EA56D0BFCD813026948BD0ACD78F98A07C88C68A4C58283531985B5D142AA82020544D62440C714D5F6DA8712115D3E50240383A1943C63C3E1B27E4F25351417E06C9B4175262822627412155F7F90A38E08E1976D65A9890EA5617D852A005CDC0BF8809F59B64A86D34504AD85D438C288A03828EDF398D5E59ACFC03139A8ADF948F6C27A1324C8852E75893345450C48437948C42BFB64C68FAEFF0DF558549F616FDD3C6814515DF261CA8AC1CF28523E540DD644294259B04A0F120574D9723B164116F95120410AFD4CB8809D25DC61D208727234812C6E7494000238D52BF7A13A5513611D165028E2ED1CC6C70984A678D9980B5232DA15EA501312E53E062FF631F17495CA66EFAA89500CB2A6830F3FE50F396242168472CE310D14233BFDEAA8D0C5B32DD481A495ABCA91F364E1A856181EEA97C4058077120A681E6B27CBE68D0E06D0B3EC73875CBAE30E0B44706FDF645CBBB0EE8714CD08B5500FDB69A69D94683E06250273536259397B93382A6ADE1481A1DD340EB39B4DBD7F078343C1A211A8900494B40C8940CBA4D7C9425B7B6D8F298094C80AB741B13068703DCB4FE22019A8AD7D99E27B455579B4CA8EDE758B3D23C62827B6B1333619B34325421B88334E2DAD1469950054C089BD041E409C5529BEFFFB7E86F65C216A9D3D638DB2A87B6312114484D26D4ED4C68AD9CBA2E418309561D21D60362044CA05010CDFF7826D82C9698303A4F068743080B892C464B038DC4E73113A225CBC1006C4BC66C9830363D353F8447FD0B24E5114E1F41E8C8EF729D5677608D4A464DDB769DE5905FD45F474C90DCA07278753728284514F92B920F6C49182A7F8FA10119116353D90F21DA58C8FA98D0047D2BCA5BC1BA88D1AC8DDE18DFADF2894E7CBFCC7EAC894B81F89193A65852A1EFDBC6C8076926CCF107B52A1A0EDB954104889284A98F09B95B0C204757E6AFB88EE99930354B8A672E57E612AA99BD23C41307DCD4DD8D5BC88FC624C1126AE19726DB80634D492243A9D93DD78EC2EE7290305CE0117E9BEEAF5DDC6AE63A2DE78DF25168A5A95104734AFA53F32098840B157C4070F5A275CC8126138210B10CB851D992915C610E940F55F910A40A9572E00D58EB87AC5A301AD0B8D5D987770685CE56DA2C821F940D29B68A2297104783A5713260BB079E3691FEF12DE445334908891144002189604E526439BA098BCAF1C10E3EDC0B8819A0C484C1312E550377ECB0EE3A0966153FEF6AC124A1240161DD14423E6188B991D998905058181301B8A83ABE70B5542003EFF882D3E1578609EAC505D6ACFE4BD941B6F21B61844CD07CBAD14670FADBEA2207E2382DAECCD474C08448C934EEE10F2CC11E9C0572284A972304B709FA40CE6D8B0611B514B88609AAE9A39FB519B6AA9D6D7146906D86EA9675DC37B0706F4C13991508B59B398DDA08B3304F98B631C1E0CF10C613A049039FD73A43BC22130E8707B4B90B178E922857B64CB8364CB04942FC152D34A0A9ECE227209C49156CF9281D5F0A137EEEE2467C97A91FAC58F98D043DDC974164881ACC760735DB98F3C9F132C45313E25B0A3BEAA47D824B05D3CAE5BE425D4783AA5C558E0970BE8EBFAB15B531A6D1360D3DA3FF8AED3F1B12A905D96F487F56476F28AEA58D094283A8791CCE5797B40CCD217B61406F1B0561B5346F55473125E466D145EEE802825984E0F759F145D58C0A47BCEF5DEFC301FBE8446242B01D2AD18071CB4CC843AF6F6AB5512AE2862C781055FA09DA5CE31326C3B5B4D82E92EEAFBD0155B28A69A9EEBFE9E9DBEA4565E0E6DB8A450113BC7230BED6ECEBC6F82E1B6CA99A01816D514551C231615D154C83468ABC55E784508EC57D93A28139747AF7DF50F096425A212D5B804EDF1E0ED595A2A382B93A9F15B4CD11CD0C13D43433B627911C6AAD14A9BF3784F149C254674E2BDF3B23D4669AE03207D8B8844A53A8BD3FFA89747BD3880C2E7310D5741732415B134D024C9C4CCAEE9D2A4326A49C2593FB4F351A5C24FAF5CC04F89D20550816342F5AE3404883E0E12B414C28CD8FD756173520A599B1FAF5CAAAF9469D2792E3C17560C20A99502019F0DCF713ECD7D99F32A2C8EB190D085100095F2A402BB356D8E1D56FBC15C4877219FFF31B0AAAF2D05F6E7494C87FEC1BA2685105A9024B20E5C0A2F2997453173529D16A53BFC196F60D6CD994C342AC5BEE83134C9771F932EE7B475B5A50400075A47CB862A3EA0E0A24104E942E4F8AA82C5B44A939BE55295B022624E729582AF9716A6A476EE6E23285DF06E4DAF0D82F686E068472D1F0FD737EF656BCBED9FCACCF717D4C58C7A8D254586ED8385B91B5306163C114068A8A434AD9080511139A7EBA2945F08BFE8DB03171A3B44717DC3616C4558063D318B69430F3A4FEE6E64B53238AC74B758C6211EBA2B05F161EDFB6C69DB909113A7CDA2C9B6ACEAAA0CCE88109C8840F7D10483E1368332F9C640A358A00BEDFECAE542E1A4C9072A08ED2698179427A411CB8C02DB23D13AE640089C412CBB5C1D1105B0A51534C8B45912ED2F5CD6D85265CFFC1D0F74EB12D2008860226444A3D0E209B9A19A23F1BA87FF98A35A170ED3EF3ED6EEE7688BFC984B6542166C2528EFF8209C1C565E3AD2DAC287C926095926B23142E26D445D3EB3722437B6ED0C898733523D6C331EC36264C4326DC15347BD787A459111FE5092AE0932B6142DC9790CE9A565427410F01D3E529A6CB09C6848B5487F0FC22E6B0D106D10017347FA605CD619B2CC0B754EE0C19249F6E76D9227A08137CADD0E23B4218D1C0237EBD8938532FF9DDB0A86A553EFB635559CB8078CCA8E0EB5AB1BEA5BFDB4209817BF0EDAB9006AB8800661C88AFCFC9165B2C22CFBC8A6F3045A12868445DE468DCC8C07D4B2781713F27E342BE0D08539718040DE6A92FE4E7536DF416AE9C7A9BD3EC5D7240B3772A8482E61A959212C7077899037972DFA9A80C250AEDD6D92CC2936102866BD6A84614A5CB92243013A8A580A902FC1E7E41F3DC2F34ABF47972515B2DEEACD5514621CD6696E0C4255E8A10BB7FC5EB26806604718E09CB8D79E927E4AA28755671D206F7DA5C71085EC8F52607F04BDB3AC1A27CBCFE89D15FB5F8FB18D6D6A3BF6195FF595C85137040216B3B6B1C0DFCCB783932D303E9673ACAB11CCA6D175969602AFABEEBACAB764CB830D2C8D48E28268C4EC6C0044C5FAF224564F079A555D41CFDFDC44603931E342B484E201522937C8F997744E579EC94EB56F01B70A00026747EEEF20A3AB71F9EDBEAA2AD91EC3CBD73423AA42DB4891FD9C2F7BB5A87E0C64F77DA2AE75AF48CF5DC4A83F5A65CB6302448A3973E15292322D9D5364EADD5AEE0A39D5DEBE317E1AF4181C891848EB5E9527B0EAC2C195AABFEFEC4F06D53CEAA76DC3352E5DD625E93E0716470E73E3DA8DC8F9850D0324CE1D764C6597219C581696833BF22CC558AA60E9185D52D61E74B45912E9AE13C61783402E79BE0EC9DDDCF429ACD1C13AE536932646E1380495039CD0CEE99A55A38CA8503D263BE92690BB7FB9DA30171C33101E41330A1F7FB01109416AFF9D5CC9195731F100226C05F746EEE59C67CF07332C6C7BF3DED535B99DE0C1A919B7FE3A33432187DE5A9F22F449195E6FC3B7031A78A3201A9F9300D0AB136883BECF28757DE735BDDEF612D5520853E9DCC6BA908B5ACBC69C992A3F6D9B65E0122BE218DCC70917FD6E5D44F5B84F57B9A03F5AD343931FBDEE1E3A40E8783A3113D33412A4561095563025796FCCE2E613D2A53F52521C8A60A29878568EE4863822B571113901BE7C804104863DE19D26D7CD4F0EE266FE63D652D2B22B1546AFD94038573B4926944D85DB73480A3115157A30C27145A52D8980C1BEFE09B3F25DA3DA041F481CDDE70F85BC5BE5F99C09B2B2A431CE25B67A44D9E10B5C9342598BB8C829360CA95850C41E148D44EA39B66C850970AFA96B4D806845223436145915B3DDC98FC09C7A46D0FC1B6D5D2BB3CB9C9FA9F87C3D33197509348203113AEA5E30631E12EB7AD099F72D0DE5E21190A7DC934808C39F57B65EB96F151A7F9CA31016800862D85FBA26AD9EEA5D14F9837CEE7412B3ABC5F157915E06965E620D6010DBC9E897A086FA6AD6F458636B38EBC8509CDF2E5320C029609248A8A251B4503E967A983AF14D00B035C2FF76D2ADC60888F0C0C71514ACD122AD1A9BD72EAA451EDDDFFB4254BCEAD340A6452C484B2B099EB34A4C1CCD1408F0EBB3471448F931A8C78814E93095A3FE598A04C906F675664B630E57392C2C70709087EEEC83F4D87B7BAE00C412691E8A1D0FD0ECE5C8C8A4951CA8345CC6E90FC04D2D2433FD8FB48C28284116933F9A4C2E50F023575D2419985CFAD700AA59184884D2542255250A2BE362A878C9E096736A3CED72A3CAE42D0AF08DC2B630F75BEC23676BE2C7338AE4B7889B6245BE011B1B834648866E016E2E9E72A8D7CF5D3078459E599C03FE88BA73E26A8A62A66FE2BDA7B67DBA627E69AF5868A682A9553930ACB0092AF1D8595FE22F4D9E6E5C42DDA0404E2A2CDB3B1ACD80C02822FEA504F0D68C04CD0F599B9EC659437C8E01B6AF0452E63C6F3E0F909FE0BB4F12C2F41B40113069F87389B3DF54F453031A12CE7AEA96C364BD5B4B8B13F7350451590594A28E6A2F18A666ED07C2937978BAA3620D67A51B90A6EF33CB1CEDE963E45D248BEEBDC3CE11E8FD4B726B8AFAB6C5DA6EB72BC2CD0D6CEE04A86C410F9C76E821DB08816EFAD4523C99C5C6D6F082746EB404199FA29ABA368BC74EBDA833786295ABB665CA4975A50B0B1DCBD3FB7C5D360859A3E46CD4E58B001ACAF716502308127E2AC1CD273AF8E64E248AB4F9909327179CA1FF19E74E24DE68EC275FD1113202C0C8F86DD5F7A834F6E41B3D3FDA6C1AC55D4326A264469B110A35CB6A8232342AA2A80E0BF90376D4C88DCB9ADDEE80DA5FE886B2F60C468889C66F14704CF0A5D7BB62AD365912E8A7459A2D14BB8983F402828B32592842380D736E2DA234CAB820FE280A5CA960E806102DF503B99B4D8FA232D5DB3D60E5A288DECD1F40DCC9D124072D34388CE2D1FA88C235B5AD03367211A6009F552C78DB85ACA2FFD155745BDA566C2BD5B16E7075A8509B96182CE3B5183B9E0B65A32B113783292CDF522575495FE1A6E31F0DB41FF0F2CA416D8699645679A242C834E333F762E480CE67E22A3948CD94409C38145235BF073E06DEA3FA0501B1334446C6382BCEB93E0E06676FFA28216BEEC53B0D45900D09DFE71EAC82C12B09534EFE97D225B0758B783403A133A0F581125BB3A56FD761E1CA8A06D01C1E1B86EE183C98CF93C3A919420AC9C36CBF95341FF4467B08DC9D3972123C589239D7570D55261820CA2323D481D79E8475B0428C7E41B850C3C6A413410265C9AD98A56265CE16C76EFF703DA9ED230216820843317614C501718E409DB99503722C3B66810D75E2375B40C69B06E6142909DAF650A88A0AFBABF147C17F418639FEF6A2594AC50C7BF948DB796B5E64B9E0C763180CD86175E1D150B7FB36382360DCCE868BCC060DB1CD13F5A9412E845EBE94326A83598E05208FC849809856742B81AC18925A78E325EAD962AEE3526B0B16ABA72F470B377D14A689BA047DFA54CA0AA9130819F1B7291D88D8F52B5EB8CB51ABCDBFF3880B0800B9AEF0A9701FB1EBEE915CCED9C05CE9F96731F37A22AAAA4CB950A712640F03CED45D5825A1533EB4D4C83C8C76B1A10C504538F2A5752DED14C0068B026322C2BC9865D4C304521AE5A3ADD1FD4F54996B81BD804D995AFF604BE5F9281AA58F81290514D7147CCA1DF448960C49AC9637AC91EDCF3460CD1569A027DCB5445100DDCCB32220CB7D8F49139AD5554C78A49E11CF6A4501AC0710C79E9F1B07F384CBC1C4A7916D508244912B87034917AD4CC2C7FF321C2848589FB5E5744A25A6ACA1973621B6A121CB4E59CF0502A3E4E61C485546002D320A88D2ECC430775C3F1A854AA8DE7A064644D17C4A9168A56FAB76402B51B76A0398B05F57A65AC288E090A745AB946CE1EF95038DC577C5D6A414E1169C250D85139EC159A5E98BA6437D6D69886F0B5CECA13C6348603893FF3282F5523C5ED854AEF6F4AA6163914AE40D038E0DB6D7EA796DA437F1A7020D735CA71FDB4E2AA65248782AD8766BE9EE3BB6C661F2E97C5DEE5237E9CD4C9C8F58FFD3E2E66818E260C70C3845364EDA3D1CB89D667BD4C720498784B3120E460E11E78578DDA11C4049EBF00269C8C3ABFE232CEEC261326349B6BFE41B4560BC50D8495A619D2598B3CBA72601B130C1F64A9DAA6A279071EC1F04C087CBFE300D2C0419F56ED60C5B32EE4A25341564DB58D7CFA4772D836F0820823E7311F6CD57FA1EEDF5D093AC101132AEBDA4D6A1132611173A00C8346207B0C13E465E9DF7DDB74D26EAE9428E13C9B9AF5F2E689669E09A6C61A5551D57021FFD9B87F3818EABE77E1C265E58376972D13E87BF1A5DB6DDB64CF8E03D1D789C9B3375902B905096699CE255EE4850AF0FB757EE90E8F711927EDF4A8A5779B00940D5326943A66E7130CB3E2916A38B2E9B99659ED2E2FCD6C8197E13F280136950CCFC5B5D7955FBE5CB001011EB0E8993FD0C94385554E65080B21BB0A2C98181538865343923C449C09E7291A8C2A0C37C28535BE7B60F7B5368972ADCB0C58A705EAC81646A328B1A8F499F5B1521262E80D7A51E487565111FD6ED608CF83518BADA6D9823CB880C28281E65D3E3C1D1D7CEA4364704CB8CD6843977821BFD34EB7190796C23C5296BFA508E79A5A3830C939694E312648370D3970C516CCFA8D39320013CE91094056B819BE55600A2073193051C2AC50A3456A65086ED9DD3F78A04BE5748BB72AB62607C2C67334CBBD369900AF53E338A034F852E75FAAFC4B997D2DB33FE9E4012B9E9C1B942B3FACEA5B7BCD4952332E6568600A47CB06915A1EF65A4B46E193E022BE47B3F0980F2A7BCA656DFBD685FF70F7D78978924731245AC08937945816E33B1BEA0BDFC51BFCC336F98A67C57C2B3748C413FA6768701B9E4CA9AE7F9F27B7192409BD3F0EC6921063C9484EEC4B5750BACB52FA28C7013D914126C7B7D0D2699ED2233779990E4F5B2013807FA34B4B06890CD79E093887074CC005CD63C704C534A35FC92057AA65E3DD88180BC704DEC9E2DF31C16F7B51CA0F060A4A36B0100E903D1013BEA0FBC7088034A88806640F8563C2434B57DB15525B99105CB44CA8C37F6F83092656140BF32E7F0839DA8060C2041955F2D074D1C03221C2BDBDC201611EBCC42B64860C046EBD18A9AF45E5B18E702F0A06FDDC45097EA0A5E386A5C1DCA917BAA1C89803F38299C0AEBAC9842462C22D4F58B82330013F671E47187DC2B927007E7E4E3D84DCD9944F5C4C40D08F8803233121832BE5BA97C084DFBA071F0F2078C117601FCD57CD0D0DF0BCF4274B3D81FF4177D26625AD2AD6250AADC640AF8436B23B987263256BF6D7FC4C41E6005A414BF8736102E03EFB52A65F8AF1973CF952404C0092F0627F5F6B5AAA46AA2C138276C152FB6554695DFA4A6BB98CE283677EBB7777782DA37924A5C15694EBBBDAE2D0468799E9C0A6C7C2839B9C7D89B709E273FE6A7DA0F2C279FD7C611CBC25C0BCC8251AE0CD2A7EE84A2637A0939ED1513E24734C401AA464C884FBDC31E1263BF8DCEF7DEA27210792EB24B949F0C834B849C770BC4DD3498680A6CFC967C6E523E8732240EE8CCE01F4CC8104E2CF24835C19AFDCE52E4F8098D0CA043826FEE5B8F37B17C83A3C19F2C6DC0EE84B83780A97419E40D70BB196E0B0E2C0520AE02A87F595C91374FB238FFBAA8CAE683240EF5264707B58484A502A13D287327D288003A32F3990A1F85A160FA5638294AD14DC1AA90AED182C03AC131C4B392FDD8F9897A6D1560634F0C283F16499504601A15896EEDDD03D478E9F469B4A37DDE48DC04AC6BF9293F80BAF73F458CC8CE0693261669940A631C132C1415F148B32419DF7AC4C3D1308B8CC84EB149284DEC703EFF85D5B2DA1F8C064400B9980E0762A6BCA40C7EB48867B39320DE07EC7848C98903113FE3F03FCE69EA8E1275E0000000049454E44AE426082">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.318+0100" uniqueID="d8d51341-bb4b-41d4-a32c-a5495110354a" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.318+0100" uniqueID="7f10bb1e-5132-4f04-9e83-13a6b8b94e3b" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.318+0100" uniqueID="ef1af0f5-f1ab-4a65-9974-dbffd7dee952" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
          <panels timeCreated="2015-01-28T20:44:29.318+0100" uniqueID="cb62c0ce-1f33-483c-b0a9-9cf21ffdd164" caption="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </panels>
        </storyboards>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Goals" timeCreated="2011-11-16T16:23:17.524+0100" lastModified="2017-01-17T19:34:16.336+0100" uniqueID="706f46e2-ecfe-4ede-96cd-eaf045e90da2">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:08.303+0100" uniqueID="83ae3374-907a-48f8-b575-4ee92960314b" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="goal:Goal" label="0" name="0 - Revive TCL as a social life incubator" elementKind="unspecified" description="" timeCreated="2011-11-18T10:25:36.912+0100" lastModified="2017-01-17T19:34:08.321+0100" uniqueID="48d0e4c0-dffe-4608-bf99-92caeb1a0443" relatedBy="9d7b7043-f84b-4620-abe0-b43bde8e7ccc 5df53374-c890-42c5-b091-deb23a5a391a" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T10:25:51.404+0100" uniqueID="dd74c9c5-f8fc-4dcd-b658-932b3abfbdc9">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T10:25:51.407+0100" uniqueID="66892e50-81df-45ec-a167-3baf8d7b9707">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="1" name="1 - Increase librarian job satisfaction " elementKind="unspecified" description="" timeCreated="2011-11-18T10:44:08.317+0100" lastModified="2017-01-17T19:34:08.343+0100" uniqueID="dd13c195-85ce-4025-a378-855e767dec07" relatedBy="bd18fa30-2b7e-4c62-b9ea-c715d89c7a1b 898f620e-d1fe-45ba-b9b2-af75e3bc7a78 47f0e220-6450-4209-bf25-55f96a202b8b de51d35d-e3d7-41e3-bef7-71578e12359b" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T10:44:28.655+0100" uniqueID="1970aa3d-3f4c-488b-8044-1e6f62277776">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T10:44:28.656+0100" uniqueID="f48a71d8-61ed-4ff4-83af-a3a1c6d0a025">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="1.1" name="1.1 - Reduce amount of repetitive tasks " elementKind="unspecified" description="" timeCreated="2011-11-18T10:45:23.438+0100" lastModified="2017-01-17T19:34:08.361+0100" uniqueID="2c9f2b17-8a5c-4c62-984f-e38e8c81f5bd" relatedBy="98ee3f6f-4fe6-4d32-98df-532952678aa1 84c0a036-25c4-47fb-8190-6f5cf237481e" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T10:45:33.123+0100" uniqueID="f50dc21b-32f6-474b-9de2-02066250070e">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:23:06.857+0100" uniqueID="bd18fa30-2b7e-4c62-b9ea-c715d89c7a1b" toElement="dd13c195-85ce-4025-a378-855e767dec07" relevance="" relationshipKind="is kind of">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T10:45:33.126+0100" uniqueID="3af7e998-d4d7-4624-9b6a-b69bd2c49e85">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="1.2" name="1.2 - Allow individual variation in job " elementKind="unspecified" description="" timeCreated="2011-11-18T10:48:09.885+0100" lastModified="2017-01-17T19:34:08.382+0100" uniqueID="dbd8acb9-23a3-4219-bcd5-a99fa9f1b11e" relatedBy="98a3a2ab-0874-4dd4-aeb3-ca9312d694e4 d6fd9afc-0efe-4582-b4d9-1d5d37321804" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T10:48:55.448+0100" uniqueID="312acdfd-1e7e-4a17-866a-bf5df1d5a552">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:23:06.857+0100" uniqueID="898f620e-d1fe-45ba-b9b2-af75e3bc7a78" toElement="dd13c195-85ce-4025-a378-855e767dec07" relevance="" relationshipKind="is kind of">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T10:48:55.449+0100" uniqueID="9eea735e-fc60-4f55-baa3-35b986f1ef6c">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="2" name="2 - Reduce need for subsidies " elementKind="unspecified" description="" timeCreated="2011-11-18T10:54:23.137+0100" lastModified="2017-01-17T19:34:08.401+0100" uniqueID="0e0e51aa-cd72-49af-8997-11a8be18c50c" relatedBy="0961c19d-edf8-4a2e-9287-383a034178f2 b20dbf90-dcd6-42ef-a16b-1043bffd2220 701a2957-72b3-4486-b284-9a8563f91f89 c90c941e-568e-46b1-a7fb-10771d48da1a" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T10:55:25.218+0100" uniqueID="71a69554-a186-4000-b944-e3abb90bdff3">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T10:55:25.220+0100" uniqueID="197f8aa5-7889-4d83-93d9-012180ad5f30">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="2.1" name="2.1 - Improve TCL management " elementKind="unspecified" description="" timeCreated="2011-11-18T10:56:03.784+0100" lastModified="2017-01-17T19:34:08.420+0100" uniqueID="28a2841e-e1b9-4cc5-9a98-3d2c9bd896f5" relatedBy="366c8ff0-ea06-4ff7-90ef-c067185074da a9e0d365-0fe5-4e8b-afb9-231a048d0cbb 71e21b1d-e056-4fc1-96ee-c77df4dbcdf4" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T10:56:30.643+0100" uniqueID="6d87bdf3-ec23-4071-95f3-027e06848e1e">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:23:06.862+0100" uniqueID="0961c19d-edf8-4a2e-9287-383a034178f2" toElement="0e0e51aa-cd72-49af-8997-11a8be18c50c" relevance="" relationshipKind="is kind of">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T10:56:30.644+0100" uniqueID="57e6099c-b37d-4704-ad62-be146a0e2d43">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="2.2" name="2.2 - Reduce theft &amp; fraud in TCL " elementKind="unspecified" description="" timeCreated="2011-11-18T10:57:34.239+0100" lastModified="2017-01-17T19:34:08.439+0100" uniqueID="eaa2fee8-e318-4753-96f3-01b3fcec95cc" relatedBy="8433f59f-4888-4745-ad9b-e47702c6fe7d 3ceb6f07-d640-4dcc-827e-bc984d9b9d08" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-23T13:33:25.507+0100" uniqueID="e6152347-69b3-464e-9dcb-c4afc465a0ea">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:23:06.863+0100" uniqueID="b20dbf90-dcd6-42ef-a16b-1043bffd2220" toElement="0e0e51aa-cd72-49af-8997-11a8be18c50c" relevance="" relationshipKind="is kind of">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-23T13:33:25.509+0100" uniqueID="36e0976b-6de6-47a5-870b-3c68139d8140">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="3" name="3 - Host many cultural activities at TCL" elementKind="unspecified" description="" timeCreated="2011-11-18T11:17:54.943+0100" lastModified="2017-01-17T19:34:08.457+0100" uniqueID="da439811-9b76-4dff-9edd-218d9f7d0977" relatedBy="27f717f3-b4d2-4bc9-88f4-73b95b12dd06 c37b8ac9-b37e-4a31-94bc-ab398b2baf4e" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T11:18:24.575+0100" uniqueID="eeb0982a-775c-492f-8fd2-ba847ece9ba8">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T11:18:24.576+0100" uniqueID="3b5194d8-8d57-434f-9677-2f0d511184e8">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="4" name="4 - Increase TCL popularity " elementKind="unspecified" description="" timeCreated="2011-11-18T11:20:42.519+0100" lastModified="2017-01-17T19:34:16.144+0100" uniqueID="bc51e4b1-e551-45a5-8df9-c9e640e92434" relatedBy="e1509e8b-94ab-4be4-b874-2248a33181d6 5778d643-c6a0-4db9-b66e-ac6ddbfddfd2 78360a5c-ff62-457f-948e-29f18b3c5b69" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T11:21:29.870+0100" uniqueID="c0cc8fe4-eba0-41a6-ae77-a990cebc6a10">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.214+0100" uniqueID="173fb175-9f72-425e-a094-fb81b26c5344" toElement="6b7a24d7-6ef6-4bf8-9c9c-b3ef3c4410d6" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T11:21:29.874+0100" uniqueID="987ba3e1-e798-43f5-ae93-b939fc015abb">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="4.1" name="4.1 - Provide convenient access to services " elementKind="unspecified" description="" timeCreated="2011-11-18T11:22:55.352+0100" lastModified="2017-01-17T19:34:16.179+0100" uniqueID="1a423134-72a7-48fd-b5d8-26ad25900512" relatedBy="cfc2f79e-a2d4-404a-a37f-6ba79f86e6f8 d398d23b-9ab8-4938-95a1-af9d988f2536 cb88728c-d606-492d-b072-e99a093bdd7a" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T11:23:41.567+0100" uniqueID="897977b9-3f18-4d3a-9286-c45842a7cd4b">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T11:23:41.568+0100" uniqueID="9089e826-70bc-43d2-9f85-691905b9ba2c">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="4.2" name="4.2 - Involve reader in TCL operations " elementKind="unspecified" description="" timeCreated="2011-11-18T11:24:02.509+0100" lastModified="2017-01-17T19:34:16.214+0100" uniqueID="6b7a24d7-6ef6-4bf8-9c9c-b3ef3c4410d6" relatedBy="3039de15-27b3-4542-81bf-736169a9cb4c 42f34ea0-a4b4-44bf-8a38-8bdebaf62d7d 173fb175-9f72-425e-a094-fb81b26c5344" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T11:25:25.892+0100" uniqueID="e3b1cd64-268a-4efb-bf25-98e916f47833">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T11:25:25.893+0100" uniqueID="a7818ac9-93ca-4d91-99f4-a4d69bc96bfa">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="5" name="5 - Reduce environmental footprint " elementKind="unspecified" description="" timeCreated="2011-11-18T11:26:01.861+0100" lastModified="2017-01-17T19:34:16.235+0100" uniqueID="684dfca6-88f2-47ca-af25-c127ef7d58c1" relatedBy="a356af98-1291-4e36-9647-47cf16e74cfb" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T11:26:35.371+0100" uniqueID="730ab98d-7844-4f84-b9b0-6e5cdcdcf98c">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.259+0100" uniqueID="05f37518-f204-4f72-9cf3-cbc235ad3d19" toElement="95dd8545-134b-47a5-b9b1-11cc8bf60146" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T11:26:35.375+0100" uniqueID="2bd4e39d-210c-4da0-9ef6-1d3867929db1">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="5.1" name="5.1 - Reduce amount of paper mail " elementKind="unspecified" description="" timeCreated="2011-11-18T11:27:35.326+0100" lastModified="2017-01-17T19:34:16.259+0100" uniqueID="95dd8545-134b-47a5-b9b1-11cc8bf60146" relatedBy="ba0f467e-21a3-47dc-a6b9-5f8d4f036755 05f37518-f204-4f72-9cf3-cbc235ad3d19" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T11:27:40.452+0100" uniqueID="20df9f7e-5737-4bca-86f2-bcd7d536fad2">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T11:27:40.455+0100" uniqueID="65671cc9-3375-48fc-b665-1b9d9e58f39c">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="6" name="6 - Increase visibility &amp; transparency of operations " elementKind="unspecified" description="" timeCreated="2011-11-18T11:28:12.587+0100" lastModified="2017-01-17T19:34:16.284+0100" uniqueID="294cd77a-950d-4b00-b94c-dfd39e034f3d" relatedBy="4ce7515a-b714-44be-abae-62ffeaa17e1f e86992cf-18a3-4c4b-87d6-2f87ddaa436d 4778c123-e2eb-4005-b417-da8e2eeb0d32" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T11:28:51.907+0100" uniqueID="d5c10580-b56d-4e0b-821e-1be4a2c0e150">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.313+0100" uniqueID="f1d412d1-6f1f-4486-989e-5f921df1539b" toElement="a027a898-0f20-473e-a0aa-25ad6bb7665f" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <relatesTo xsi:type="relationship:ElementReference" timeCreated="2017-01-17T19:34:16.335+0100" uniqueID="be35e628-2607-46d2-b7a1-dedab510a64d" toElement="1cb60663-ed14-4d87-a38d-d12bb968f391" relevance="">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </relatesTo>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T11:28:51.910+0100" uniqueID="d9819d0b-0711-434a-b473-bb8e76676c41">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="6.1" name="6.1 - Provide information about TCL events " elementKind="unspecified" description="" timeCreated="2011-11-18T11:29:11.987+0100" lastModified="2017-01-17T19:34:16.313+0100" uniqueID="a027a898-0f20-473e-a0aa-25ad6bb7665f" relatedBy="e28af85a-bb48-41af-a0a9-34c5b6f965ca 45338a52-a34a-4222-8d13-a91f80426612 4877763a-0075-418e-b2e7-e31c207518a3 32c8fd76-0b1a-4f64-b31c-8cb319392744 9b1686bb-3945-40f6-b778-4aecc683dc73 f1d412d1-6f1f-4486-989e-5f921df1539b" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T11:30:04.416+0100" uniqueID="5d9da0dc-100c-43b6-958d-e34bded6dcbe">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T11:30:04.417+0100" uniqueID="445e1c3b-1cab-4393-982e-040500c5932f">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
      <contents xsi:type="goal:Goal" label="6.2" name="6.2 - Give fast &amp; accurate status information " elementKind="unspecified" description="" timeCreated="2011-11-18T11:30:28.587+0100" lastModified="2017-01-17T19:34:16.336+0100" uniqueID="1cb60663-ed14-4d87-a38d-d12bb968f391" relatedBy="feb2bdb2-0825-42b8-8379-dde245598cb5 85feb82d-39a8-487b-8975-43e0b7fa5f45 0ccbf406-1385-44f2-be67-06981dba7241 be35e628-2607-46d2-b7a1-dedab510a64d" workPackage="">
        <commentlist/>
        <creator name="" timeCreated="2011-11-18T11:30:33.935+0100" uniqueID="aecaaa8f-698d-454b-9025-0de031e91c59">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2011-11-18T11:30:33.939+0100" uniqueID="8801b0f1-9a5d-4fa2-a62e-58393c51c83c">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Requirements" timeCreated="2011-11-18T10:07:23.270+0100" lastModified="2017-01-17T19:34:29.803+0100" uniqueID="3bcd6f72-901a-462c-a81b-7529569c3271">
      <commentlist>
        <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:19.612+0100" uniqueID="1b9197de-f32c-40d5-afca-04773964af2e" severity="Warning">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <text>
            <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
          </text>
        </comments>
      </commentlist>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Corpus" timeCreated="2011-12-09T15:09:23.409+0100" lastModified="2017-01-17T19:34:20.593+0100" uniqueID="5ddef5ac-6045-47b0-958f-ef7fa895ddcd">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:19.960+0100" uniqueID="23e5d36d-0294-4a3d-8231-1be4c4b9a462" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="folder:Folder" name="Medium Life Cycle" timeCreated="2011-12-07T15:29:54.893+0100" lastModified="2017-01-17T19:34:20.267+0100" uniqueID="01a489a4-c58a-4b57-aa27-9f16534fd98b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.119+0100" uniqueID="c3ae254c-31ba-426b-a392-a830d75d5682" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="MLC1" name="MLC1" elementKind="" description="Media follow a defined lifecycle from suggested, via acquired, incorporated, to removed." timeCreated="2011-11-25T15:32:46.357+0100" lastModified="2017-01-17T19:34:20.134+0100" uniqueID="d9c4ff89-f5ad-472c-8006-78bc1a090776" workPackage="" abstractionLevel="" id="MLC1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.133+0100" uniqueID="6589dcfb-fccb-4765-8740-97d3917ed297" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-11-25T15:43:01.465+0100" uniqueID="7d3559f9-bce0-4df9-94e5-d698c2326392">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-11-25T15:43:01.469+0100" uniqueID="5bf9d520-c1cf-40bd-879d-ebea4723fbac">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MLC2" name="MLC2" elementKind="Feature" description="Librarians may add, update, and delete corpus items manually." timeCreated="2011-11-25T15:47:54.636+0100" lastModified="2017-01-17T19:34:20.148+0100" uniqueID="ce4a3259-f77d-4d12-93ed-5319436eff22" workPackage="" abstractionLevel="Activity" id="MLC2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.148+0100" uniqueID="c1a53439-1854-4678-947f-745bdbcff569" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-11-25T15:48:46.130+0100" uniqueID="07c330a5-6045-425b-844b-d886db1381ea">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-11-25T15:48:46.151+0100" uniqueID="345b21e8-1b21-4375-bdf6-542ff72a5666">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MLC3" name="MLC3" elementKind="" description="On creating a new catalog item, the LMS creates a new globally unique identifier for the item." timeCreated="2011-12-07T14:08:12.923+0100" lastModified="2017-01-17T19:34:20.162+0100" uniqueID="6bb0c7ff-3583-44df-92ea-5bc909682c56" workPackage="" abstractionLevel="" id="MLC3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.162+0100" uniqueID="adefd80d-aed2-4220-b293-7e5e76ff8730" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-07T14:09:42.289+0100" uniqueID="44a1236b-7a64-463f-a4bb-42844f1992aa">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-07T14:09:42.291+0100" uniqueID="45eb942e-b19c-4bf2-90a9-2f8599911f5f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MLC4" name="MLC4" elementKind="" description="Librarians and Readers may post and inspect media they think should be acquired by the library to a public “wish list” indicating the status of the wish and the originator. " timeCreated="2011-12-07T14:40:09.009+0100" lastModified="2017-01-17T19:34:20.177+0100" uniqueID="aee159a9-3461-4739-a52f-5e7ef25bcb4c" workPackage="" abstractionLevel="" id="MLC4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.176+0100" uniqueID="f67bbdca-251c-45f0-bec3-656058a1be2b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-07T14:42:10.846+0100" uniqueID="c98012cd-c341-49e4-8fe9-a153222f93a8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-07T14:42:10.847+0100" uniqueID="a21f83cf-0973-4edc-a92f-17539490055c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MLC5" name="MLC5" elementKind="" description="Postings to the wishlist may be publicly commented by other users" timeCreated="2011-12-07T15:26:25.726+0100" lastModified="2017-01-17T19:34:20.191+0100" uniqueID="244af1d4-32b7-4964-9514-74b278b06777" workPackage="" abstractionLevel="" id="m">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.191+0100" uniqueID="599b57f3-3801-4065-b14a-b5ed4986802c" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-07T15:28:56.415+0100" uniqueID="ce673a9a-40c2-4aa7-83a1-0ec0c0a9cd84">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-07T15:28:56.417+0100" uniqueID="08c52fb2-2500-4b0c-a10a-baebedb7e7c2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MLC6" name="MLC6" elementKind="" description="Librarians may remove or deactivate entries to the wishlist." timeCreated="2011-12-07T15:30:34.012+0100" lastModified="2017-01-17T19:34:20.206+0100" uniqueID="767854c6-e241-467f-9032-b33b7ec589db" workPackage="" abstractionLevel="" id="">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.206+0100" uniqueID="82bc1e95-c2fb-4fcd-b4f0-ca2e90168e41" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-07T15:31:47.414+0100" uniqueID="c8df4845-5164-4ae2-8289-f456f32e7f4b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-07T15:31:47.415+0100" uniqueID="81b5812a-551a-44ca-9052-b8c853c497a8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MLC7" name="MLC7" elementKind="" description="If a suggestion changes its state (see previous requirement), the user who originally posted it is notified electronically." timeCreated="2011-12-07T15:33:47.454+0100" lastModified="2017-01-17T19:34:20.224+0100" uniqueID="9ca79e2b-b450-48a4-a184-257dc57205a7" workPackage="" abstractionLevel="" id="MLC7">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.223+0100" uniqueID="eef72f0f-5d12-4bdf-9264-584cd42c0a7b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-07T15:34:56.496+0100" uniqueID="50999170-30bb-42f9-870e-7aed28382107">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-07T15:34:56.497+0100" uniqueID="2f5f0aef-e7a8-47dd-a8fa-c01638035732">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MLC8" name="MLC8" elementKind="" description="Guests have the capabilities to use the catalog except the features requiring personalization." timeCreated="2011-12-07T15:37:15.353+0100" lastModified="2017-01-17T19:34:20.238+0100" uniqueID="3ccddefb-2f1c-4d0c-a2ed-b305789a4947" workPackage="" abstractionLevel="" id="MLC8">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.238+0100" uniqueID="2101e6cc-076f-423c-978b-4dce33c18a7b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:23:34.669+0100" uniqueID="d6814062-c12c-4be8-89db-d03c5caaca98">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:23:34.672+0100" uniqueID="7a2575df-070e-4a9d-80d4-4131d596da9c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MLC9" name="MLC9" elementKind="" description="Guest readers may inspect suggestions." timeCreated="2011-12-09T09:24:02.147+0100" lastModified="2017-01-17T19:34:20.253+0100" uniqueID="fc938ff1-a668-4c7a-bd63-5632cb32c859" workPackage="" abstractionLevel="" id="MLC9">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.252+0100" uniqueID="45e751d5-8f98-4cd4-8059-e5a12aa478ce" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:24:31.211+0100" uniqueID="aa71aafd-c452-4511-a76c-27bbc49136b5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:24:31.218+0100" uniqueID="c9a1ba3a-2686-4d16-bf1c-c7e5ba3c1acf">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MLC10" name="MLC10" elementKind="" description="A librarian can do all a reader can do; a reader can do all a guest reader can do." timeCreated="2011-12-09T09:24:46.293+0100" lastModified="2017-01-17T19:34:20.267+0100" uniqueID="a0c43a61-f5df-43f7-bdaf-e75f2e8eaf64" workPackage="" abstractionLevel="" id="MLC10">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.266+0100" uniqueID="417b5bd3-232d-4040-921f-28f03ace7cf9" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:26:31.981+0100" uniqueID="86745771-f300-489f-82ec-e276a308c4e2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:26:31.986+0100" uniqueID="7dcc03ae-d530-4ffa-964a-0115115f18b9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Advanced Medium Delivery Service" timeCreated="2011-12-07T15:38:10.730+0100" lastModified="2017-01-17T19:34:20.460+0100" uniqueID="7298855b-607f-4624-9a76-5a389f0a7cc3">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.369+0100" uniqueID="94ce0a0f-d37d-40a2-a9c7-e8459997f917" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="AMDS1" name="AMDS1" elementKind="" description="The AMDS delivers both electronic and physical media across both physical and online channels." timeCreated="2011-12-09T09:32:05.986+0100" lastModified="2017-01-17T19:34:20.385+0100" uniqueID="2ee06d2f-380f-4623-a0e5-1647923f7629" workPackage="" abstractionLevel="" id="AMDS1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.384+0100" uniqueID="7b5c188d-ccb1-440a-a1d4-c8850bb16bd0" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:32:41.948+0100" uniqueID="f0a7312e-1db3-48c0-a6b1-1e7d77442576">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:32:41.949+0100" uniqueID="7f4feaa0-3287-4133-aa35-b93009c1b483">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="AMDS2" name="AMDS2" elementKind="" description="Readers must sign up to the AMDS in order to use it." timeCreated="2011-12-09T09:33:52.604+0100" lastModified="2017-01-17T19:34:20.399+0100" uniqueID="7d77b7d8-c459-45b8-b5f3-c341429e74a8" workPackage="" abstractionLevel="" id="AMDS2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.399+0100" uniqueID="68cd039d-bcdf-4fd3-a2f6-3200b0c0b9be" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:36:05.611+0100" uniqueID="4b844b08-65d4-4cba-b60a-3a4ce6bf3939">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:36:05.612+0100" uniqueID="44f142d6-bda4-49f2-ab34-dc0d8d53106d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="AMDS3" name="AMDS3" elementKind="" description="Participants of the AMDS must document consent with its specific usage regulations." timeCreated="2011-12-09T09:37:30.620+0100" lastModified="2017-01-17T19:34:20.414+0100" uniqueID="91aca4be-bb60-4f24-a6b2-49abd810d51e" workPackage="" abstractionLevel="" id="AMDS3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.413+0100" uniqueID="ab569d41-3e54-4d81-b3ad-b72ffcf34324" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:38:07.585+0100" uniqueID="9162ed7c-a74a-4b01-ba04-b5be0a01f929">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:38:07.592+0100" uniqueID="aaab6b3c-1b14-477f-934d-3bdfdaf2b00c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="AMDS4" name="AMDS4" elementKind="" description="Electronic media may be delivered to readers online." timeCreated="2011-12-09T09:39:33.597+0100" lastModified="2017-01-17T19:34:20.428+0100" uniqueID="8d934b6f-d5e6-4c99-ae22-05373427e6fe" workPackage="" abstractionLevel="" id="AMDS4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.428+0100" uniqueID="be75397e-9922-4407-8fc7-566a9a936a06" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:43:33.166+0100" uniqueID="3e6b349e-53a2-4d51-ae05-31cfe53e1208">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:43:33.168+0100" uniqueID="9371cc72-7f74-48e6-b517-4a85d2755b13">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="AMDS5" name="AMDS5" elementKind="" description="Physical media may be delivered using the book station." timeCreated="2011-12-09T09:44:15.936+0100" lastModified="2017-01-17T19:34:20.445+0100" uniqueID="829ea81b-69d5-4e7f-a133-ee9dc80f323e" workPackage="" abstractionLevel="" id="AMDS5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.445+0100" uniqueID="8954bb8e-de30-4b20-b348-a1b62fabcc8a" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:45:41.834+0100" uniqueID="f526656e-e6d6-4d82-8287-c2cc9ab884b7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:45:41.836+0100" uniqueID="062391a6-dc1f-4f71-8af6-f75aac27cd19">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="AMDS6" name="AMDS6" elementKind="" description="If a leased copy is taken from or returned to the book station, the delivery date to/from the reader is relevant for the beginning/end of the lease." timeCreated="2011-12-09T09:52:41.626+0100" lastModified="2017-01-17T19:34:20.460+0100" uniqueID="d37b04ef-3dee-43bd-91ee-3f560e57c99d" workPackage="" abstractionLevel="" id="AMDS6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.460+0100" uniqueID="2bb59beb-4add-4efc-a9a4-56121ca57547" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:54:54.937+0100" uniqueID="aaada215-ed44-48a9-a384-137593a67101">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:54:54.938+0100" uniqueID="6d86fbee-d1a3-4a86-8d78-cb9b92a6fa41">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Identification Tags" timeCreated="2011-12-07T15:39:02.504+0100" lastModified="2017-01-17T19:34:20.593+0100" uniqueID="2f107ae2-2b58-44c6-80b4-8835c1c872de">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.531+0100" uniqueID="ff6d93e2-f236-467e-86b9-a0f306269129" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="IDTAG" name="IDTAG" elementKind="" description="Every copy can be identified by the scanner attached to every terminal." timeCreated="2011-12-09T09:58:36.183+0100" lastModified="2017-01-17T19:34:20.546+0100" uniqueID="7324ba2a-e538-4627-a0e4-5194447c736b" workPackage="" abstractionLevel="" id="IDTAG1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.546+0100" uniqueID="43db4ce1-4b51-4ee5-bea5-a5ef0e6fbdf6" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T09:58:45.382+0100" uniqueID="9f5254d7-fee0-4a02-9569-9f67642d6e93">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T09:58:45.383+0100" uniqueID="c7d049b5-c0d7-4cce-9939-19b35fcc5947">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="IDTAG" name="IDTAG" elementKind="" description="Scanning the id badge of a medium displays a picture of the cover at the terminal." timeCreated="2011-12-09T10:01:13.652+0100" lastModified="2017-01-17T19:34:20.561+0100" uniqueID="006282e6-7444-4633-8e47-4f9517adadc8" workPackage="" abstractionLevel="" id="IDTAG2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.561+0100" uniqueID="7806835a-a9d4-474b-b00e-0096433d0689" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:01:16.200+0100" uniqueID="41e8e78a-4147-4c5f-bcf7-ea2a16332229">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:01:16.203+0100" uniqueID="45173dca-89f3-42d4-98c6-40c436e60863">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="IDTAG" name="IDTAG" elementKind="" description="Scanning the id of a reader card displays and identifies the reader." timeCreated="2011-12-09T10:02:48.052+0100" lastModified="2017-01-17T19:34:20.576+0100" uniqueID="050e25ee-7926-4270-9ed9-b2e8f6790dec" workPackage="" abstractionLevel="" id="IDTAG3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.575+0100" uniqueID="16e3b4bf-cedd-4c27-9169-f3d8710dba5c" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:03:13.000+0100" uniqueID="158fc73c-4146-4d71-b045-459d270bb5f9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:03:13.001+0100" uniqueID="14ed9a14-46ba-44fd-8c83-8ffd6e32edd1">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="IDTAG" name="IDTAG" elementKind="" description="Online access to a reader account is only granted to persons who can identify themselves as the owner of the account or legitimate proxies." timeCreated="2011-12-09T10:03:36.187+0100" lastModified="2017-01-17T19:34:20.593+0100" uniqueID="a33b1879-09ec-438c-b4e2-cbdd29b0b810" workPackage="" abstractionLevel="" id="IDTAG4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:20.593+0100" uniqueID="d642a636-f1c0-4c85-84f0-cacad764e88e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:06:14.065+0100" uniqueID="ad699e51-9a04-47dc-94c4-9099f749f041">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:06:14.066+0100" uniqueID="1764408b-f775-4fb4-bab2-8976e6cfa0e3">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Catalog" timeCreated="2011-12-09T15:10:16.357+0100" lastModified="2017-01-17T19:34:22.066+0100" uniqueID="b8e2fc9c-f730-4727-8f85-aea58589c89d">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.100+0100" uniqueID="d76a054e-f33b-4b3b-b571-a0a9cc428baa" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="folder:Folder" name="Catalog Data" timeCreated="2011-12-07T15:43:50.580+0100" lastModified="2017-01-17T19:34:21.327+0100" uniqueID="fcbd04f1-9c7f-4026-9e26-e4b8f4b1eda6">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.215+0100" uniqueID="1ab8da11-47b1-487f-802f-61168d2161c7" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="CDAT1" name="CDAT1" elementKind="" description="A catalog provides information about media and copies in a corpus." timeCreated="2011-12-09T10:06:53.259+0100" lastModified="2017-01-17T19:34:21.231+0100" uniqueID="4b3e61e6-3f0c-46c0-9d41-25ce3f9e49f5" workPackage="" abstractionLevel="" id="CDAT1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.230+0100" uniqueID="1c30e97d-2ba7-49b4-852a-83af1db614a1" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:07:43.429+0100" uniqueID="708d2f66-742a-43fb-a2c0-dbda35055a75">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:07:43.430+0100" uniqueID="a030d818-9294-4326-916d-a466815f5bf5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CDAT2" name="CDAT2" elementKind="" description="Multiple catalogs shall be made available to users in a unified and seamless way." timeCreated="2011-12-09T10:08:16.306+0100" lastModified="2017-01-17T19:34:21.245+0100" uniqueID="6d61d2e4-9009-4ef2-9fc8-775317017d80" workPackage="" abstractionLevel="" id="CDAT2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.245+0100" uniqueID="99fa5909-f308-4a71-8823-a2d6aa57555c" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:08:44.571+0100" uniqueID="f32879fb-8dd9-4583-bfbe-783e4d203893">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:08:44.573+0100" uniqueID="786ade98-6f58-4b86-9163-216eb20410a1">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CDAT3" name="CDAT3" elementKind="" description="Each medium is stored with the full bibliographic data according to DanMarc2 and a list of the copies of this medium in the corpus." timeCreated="2011-12-09T10:09:00.514+0100" lastModified="2017-01-17T19:34:21.261+0100" uniqueID="e7ac3734-1fe7-470c-9f07-e04b07c94dc1" workPackage="" abstractionLevel="" id="CDAT3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.261+0100" uniqueID="db01f02c-2eb1-49a8-b229-4be2bcf4cd4a" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:10:24.172+0100" uniqueID="0bd24f62-e984-41e2-b8d1-64e7a6b58867">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:10:24.175+0100" uniqueID="71509d44-8f7c-4d51-88d0-101ba8d407ab">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CDAT4" name="CDAT4" elementKind="" description="Each copy is stored in the catalog with a picture of the cover, and a state indicating its degree of damage." timeCreated="2011-12-09T10:10:38.137+0100" lastModified="2017-01-17T19:34:21.278+0100" uniqueID="1a8eb053-febd-4448-a202-84435855ed0a" workPackage="" abstractionLevel="" id="CDAT4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.276+0100" uniqueID="bbe05e57-0178-464e-98a3-65242ab428c3" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:11:13.247+0100" uniqueID="9689be43-a094-4689-85c6-afa9c049776f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:11:13.253+0100" uniqueID="b1466596-3a6a-4a3f-bf07-20b075c3c648">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CDAT5" name="CDAT5" elementKind="" description="The catalog may be updated semi-automatically by a librarian using electronic sources." timeCreated="2011-12-09T10:11:48.024+0100" lastModified="2017-01-17T19:34:21.293+0100" uniqueID="9a3ec2bc-ef72-47ad-a08f-9be32c94738a" workPackage="" abstractionLevel="" id="CDAT5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.292+0100" uniqueID="78b26fe5-e0f3-48b0-9081-f7d7c6b2ea32" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:12:10.183+0100" uniqueID="3756f2a5-2cf6-40f9-8155-8f542a716091">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:12:10.185+0100" uniqueID="1306d4f6-3a82-4a26-abfd-17efa2f7e85f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CDAT6" name="CDAT6" elementKind="" description="The catalog may be updated by a librarian manually." timeCreated="2011-12-09T10:12:31.664+0100" lastModified="2017-01-17T19:34:21.311+0100" uniqueID="c1056659-8b01-410c-9a53-3682714c1fe8" workPackage="" abstractionLevel="" id="CDAT6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.311+0100" uniqueID="7fa59365-0f5f-466c-b783-ddd3e6f00142" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:12:45.669+0100" uniqueID="3d411480-919c-4316-8f19-4039cdcc35f9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:12:45.672+0100" uniqueID="87752de3-7978-4015-970c-e83720e6c632">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CDAT7" name="CDAT7" elementKind="" description="The LMS shall be able to batch update the local cache of all remote catalogs it offers to the reader." timeCreated="2011-12-09T10:13:10.464+0100" lastModified="2017-01-17T19:34:21.327+0100" uniqueID="47c4422f-67b4-4b47-bbb7-f454111bcb53" workPackage="" abstractionLevel="" id="CDAT7">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.326+0100" uniqueID="7acaab01-3e3c-4353-a7f9-36d1a1d21321" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:13:35.218+0100" uniqueID="872ad5b9-a859-4cb7-a08e-19aecfd91137">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:13:35.219+0100" uniqueID="cb1c536f-a6ca-4b30-87d3-eced409f0090">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Catalog Search (General)" timeCreated="2011-12-07T15:44:19.774+0100" lastModified="2017-01-17T19:34:21.708+0100" uniqueID="6fbb5a9b-f661-414d-baae-05b8a70b06c9">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.519+0100" uniqueID="9df177cb-aaf7-479e-9719-0e22728a8d40" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="CSG1" name="CSG1" elementKind="" description="Users may search the catalog from local or remote terminals " timeCreated="2011-12-09T10:14:47.390+0100" lastModified="2017-01-17T19:34:21.538+0100" uniqueID="ff9f69a3-7315-4c79-9e61-c85388e2650e" workPackage="" abstractionLevel="" id="CSG1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.537+0100" uniqueID="1703fb25-dcfa-40fd-84ad-3a4072d7f685" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:15:05.035+0100" uniqueID="98e3bbb9-1990-4c7e-a0bc-0761b0a526f6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:15:05.036+0100" uniqueID="8bfb1a25-e390-47bd-9433-a1297156ad15">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG2" name="CSG2" elementKind="" description="Users may search the catalog using two modes for experts and novices, respectively, called “Expert Search” and “Simple Search”." timeCreated="2011-12-09T10:15:20.390+0100" lastModified="2017-01-17T19:34:21.554+0100" uniqueID="b3170a9b-a38f-4219-9c6a-4f28e6b82653" workPackage="" abstractionLevel="" id="CSG2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.553+0100" uniqueID="ed323453-e213-424b-aec7-3916d3584d75" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:16:34.730+0100" uniqueID="f2bfa5eb-9a1c-463f-8f8d-5ce38aa0b185">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:16:34.733+0100" uniqueID="8ecf59c6-1ecb-4bce-b7df-c9c1e50cb1f0">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG3" name="CSG3" elementKind="" description="A search yields a list of overview results that may each be expanded to detail results." timeCreated="2011-12-09T10:20:27.561+0100" lastModified="2017-01-17T19:34:21.569+0100" uniqueID="6d4e7081-a879-4f3f-a162-15044923df64" workPackage="" abstractionLevel="" id="CSG3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.569+0100" uniqueID="72b1c104-ca0c-4eb6-9d6d-576e871d8012" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:20:46.190+0100" uniqueID="450f06fc-60ac-4bba-8845-d11836500952">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:20:46.194+0100" uniqueID="9ab4f7c0-a4b8-4d83-b3e1-277bf7ac506f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG4" name="CSG4" elementKind="" description="An overview search result consists of the medium category, author, title, and publication year." timeCreated="2011-12-09T10:21:05.518+0100" lastModified="2017-01-17T19:34:21.584+0100" uniqueID="698441bb-7043-4cea-910b-f4aad0dfc6fb" workPackage="" abstractionLevel="" id="CSG4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.584+0100" uniqueID="6f041c0d-1da5-4504-b6c2-c6055da6ade2" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:21:18.004+0100" uniqueID="5e0f55db-a32f-4a64-b276-2f1716ed666d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:21:18.006+0100" uniqueID="4f961d98-36bf-43cc-9d12-e6a36ddd3868">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG5" name="CSG5" elementKind="" description="A detailed search result consists of the full bibliographic details, a list of all copies and their availability, and all comments on the medium." timeCreated="2011-12-09T10:21:39.733+0100" lastModified="2017-01-17T19:34:21.600+0100" uniqueID="bad992de-56f7-47b6-96fc-32602ecc0e5a" workPackage="" abstractionLevel="" id="CSG5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.599+0100" uniqueID="c74f94e6-c4a0-4b57-85d1-1b18de76c678" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:21:45.492+0100" uniqueID="aea739f3-833c-48bd-80e5-77d8805630bd">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:21:45.493+0100" uniqueID="d29dd120-07f4-42ef-af77-ef8f7720d8a9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG6" name="CSG6" elementKind="" description="There is a special function to quickly identify copies and return their overview data and the cover picture." timeCreated="2011-12-09T10:22:08.156+0100" lastModified="2017-01-17T19:34:21.616+0100" uniqueID="4d16d1c7-90d3-4256-b801-48433d50ef11" workPackage="" abstractionLevel="" id="CSG6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.615+0100" uniqueID="6767a348-9d73-4590-8d6b-9d9913b2781c" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:22:19.492+0100" uniqueID="4ea0462f-3357-48f1-a13a-02585e54170a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:22:19.493+0100" uniqueID="b2bd41e5-64fe-49e4-922c-43fdf0f86e3b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG7" name="CSG7" elementKind="" description="Identifying a copy takes less than 0.5s in 99% of all cases." timeCreated="2011-12-09T10:23:18.441+0100" lastModified="2017-01-17T19:34:21.631+0100" uniqueID="4d181500-03bf-47e8-9092-c2f4c5ce5310" workPackage="" abstractionLevel="" id="CSG7">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.630+0100" uniqueID="98c2ac4b-a1f3-4889-a635-1ce912a74bf1" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:23:33.869+0100" uniqueID="46e5b4b9-9022-4957-b745-2944b09ee2df">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:23:33.870+0100" uniqueID="3f521dc2-3663-4c24-815b-93fd5d05773b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG8" name="CSG8" elementKind="" description="If a copy cannot be identified, an adequate error notification is issued within 1s." timeCreated="2011-12-09T10:23:55.979+0100" lastModified="2017-01-17T19:34:21.646+0100" uniqueID="0e33e5f4-7372-444e-8016-a265b8ed3cff" workPackage="" abstractionLevel="" id="CSG8">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.645+0100" uniqueID="589730bf-fd57-4261-aa34-c7b0a3f2c898" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:24:06.940+0100" uniqueID="5f3b90d2-aa06-4abf-a5fd-5d5943c45e9e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:24:06.943+0100" uniqueID="3e256a3d-5784-4295-8d16-88cfd49df2a5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG9" name="CSG9" elementKind="" description="All search queries, their results, and data on which of the results were chosen by the user are archived in line with the applicable privacy reg¬ul¬ations." timeCreated="2011-12-09T10:24:32.603+0100" lastModified="2017-01-17T19:34:21.661+0100" uniqueID="8de0b7fc-6b81-4498-ac57-9c6abc969e2a" workPackage="" abstractionLevel="" id="CSG9">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.660+0100" uniqueID="0c162fba-ef89-4fb9-955d-32473fea9700" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:24:57.527+0100" uniqueID="d94771da-89b2-47b9-a852-46729ec71e01">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:24:57.530+0100" uniqueID="b4a016aa-9785-475e-a466-79d1a81dced2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG10" name="CSG10" elementKind="" description="If a search query involves only the local catalog and yields at least one answer, all LMS will provide the first/next 10 answers to the search query within 5 seconds." timeCreated="2011-12-09T10:25:29.977+0100" lastModified="2017-01-17T19:34:21.676+0100" uniqueID="c2d677ff-894f-4c0b-88c1-90fc6706e152" workPackage="" abstractionLevel="" id="CSG10">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.675+0100" uniqueID="348f6a0e-0f34-43eb-bbaf-c4fd8eec28ab" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:25:41.579+0100" uniqueID="180780dd-f1d1-4143-b9c1-af3167c03711">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:25:41.582+0100" uniqueID="2b8eb90d-2fd9-4d18-b5c9-cfec5f1516a5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG11" name="CSG11" elementKind="" description="Retrieving the detail results of 10 media takes less than 5s." timeCreated="2011-12-09T10:26:10.739+0100" lastModified="2017-01-17T19:34:21.691+0100" uniqueID="8395caba-8170-452a-9b59-47b8c9f7e59e" workPackage="" abstractionLevel="" id="CSG11">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.690+0100" uniqueID="f853b731-755a-4492-89c6-5da893997df4" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:27:12.082+0100" uniqueID="4347ee47-115b-406a-8bb6-84163122aaa6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:27:12.083+0100" uniqueID="c50158a5-c27e-4992-900a-c1769a14e287">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="CSG12" name="CSG12" elementKind="" description="When a medium is scanned, the respective catalog entry is displayed within .3s." timeCreated="2011-12-09T10:27:26.683+0100" lastModified="2017-01-17T19:34:21.708+0100" uniqueID="569cb9d0-f6c1-4e12-91d0-023b88524da0" workPackage="" abstractionLevel="" id="CSG12">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.707+0100" uniqueID="62a94dfa-a058-4417-9889-c66ceca4f135" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:27:55.640+0100" uniqueID="2f932e21-e8bb-44ec-9de4-d48b6afc2acd">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:27:55.641+0100" uniqueID="d35b3dff-78c4-4cdb-a637-8ecc332cde3b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Expert Search Mode" timeCreated="2011-12-07T15:44:42.205+0100" lastModified="2017-01-17T19:34:21.892+0100" uniqueID="f9344502-a2e3-4dde-9501-b41f99bab383">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.798+0100" uniqueID="d7fee12b-9a82-4e43-89fa-df7f156a00a5" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="ESM1" name="ESM1" elementKind="" description="In the expert search mode, users may specify any set of bibliographic details or catalog information, and the catalog to be used." timeCreated="2011-12-09T10:28:10.762+0100" lastModified="2017-01-17T19:34:21.827+0100" uniqueID="bb0ef371-1bea-4691-9e9d-ea193ba67243" workPackage="" abstractionLevel="" id="ESM1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.826+0100" uniqueID="c8a04d8a-19cb-4990-b6b0-546e4cac762e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:28:17.065+0100" uniqueID="64e63741-7cf3-4171-a4ea-530a2ef546f7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:28:17.068+0100" uniqueID="32387c9d-6bdf-4249-98a9-17b3d089b289">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="ESM2" name="ESM2" elementKind="" description="The expert search provides connectors to combine any available information item into complex searches. " timeCreated="2011-12-09T10:29:08.954+0100" lastModified="2017-01-17T19:34:21.848+0100" uniqueID="8c8b549b-7362-4b89-8076-bd2eb1337378" workPackage="" abstractionLevel="" id="ESM2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.847+0100" uniqueID="e503229c-73dd-4c14-8064-0c9173f48f0a" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:29:26.060+0100" uniqueID="fb84265b-ff6b-425f-bcc0-8acebbb3f109">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:29:26.064+0100" uniqueID="dae02d19-08b9-4cb2-a28d-328403f162d4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="ESM3" name="ESM3" elementKind="" description="An expert search allows to adjust the length of the result set to 5/10/20/50/all." timeCreated="2011-12-09T10:29:39.658+0100" lastModified="2017-01-17T19:34:21.863+0100" uniqueID="9f49df16-4fa8-400a-af6b-bfb9e930267c" workPackage="" abstractionLevel="" id="ESM3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.862+0100" uniqueID="7cdc9d03-1085-4772-9238-9b65f4f7bbe3" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:29:57.806+0100" uniqueID="8571bbfb-217b-4e02-99da-047955b632e4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:29:57.810+0100" uniqueID="07a33c07-1121-4ba8-89cd-1011e6fa98b1">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="ESM4" name="ESM4" elementKind="" description="Expert search queries and (parts of their) results may be saved, loaded, and forwarded to an email address." timeCreated="2011-12-09T10:30:19.593+0100" lastModified="2017-01-17T19:34:21.877+0100" uniqueID="4c20bd3b-d4e2-40a8-a4e6-192cd738f687" workPackage="" abstractionLevel="" id="ESM4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.877+0100" uniqueID="040c3a9d-a824-4157-aea2-7b6cbc995e2d" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:30:38.468+0100" uniqueID="48d8cda7-224e-4b72-8427-c954371207d9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:30:38.468+0100" uniqueID="579f7071-4a50-4d50-b522-6be92f5ebc04">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="ESM5" name="ESM5" elementKind="" description="Saved expert search queries may be loaded and executed automatically at preset times and with preset actions for the result." timeCreated="2011-12-09T10:30:51.744+0100" lastModified="2017-01-17T19:34:21.892+0100" uniqueID="f7497150-3b46-4724-8067-02a389b5cac4" workPackage="" abstractionLevel="" id="ESM5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.892+0100" uniqueID="9553fa94-fc73-457c-b300-7386baecd428" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:31:11.446+0100" uniqueID="18a1a19f-0746-4e9c-b95c-3a888c1bf86c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:31:11.449+0100" uniqueID="cb1b14da-f515-4a7d-be0a-f1eed3eeb453">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Novice Search Mode" timeCreated="2011-12-07T15:47:34.444+0100" lastModified="2017-01-17T19:34:22.066+0100" uniqueID="74fbf5c2-c0ef-4634-8414-d826a4a281ac">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:21.987+0100" uniqueID="03fc658a-a6ac-4763-bb1b-bab648b4682b" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="NSM1" name="NSM1" elementKind="" description="The novice search mode offers only have a single text field for entering search terms." timeCreated="2011-12-09T10:31:35.318+0100" lastModified="2017-01-17T19:34:22.003+0100" uniqueID="cdee9ea8-6423-4ba5-8c42-dcd89f830cee" workPackage="" abstractionLevel="" id="NSM1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.002+0100" uniqueID="794f00a2-4579-4dd5-ba38-87a34b8a94d2" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:31:57.072+0100" uniqueID="9954a3f5-f84f-436c-bcbf-a928b5953a64">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:31:57.075+0100" uniqueID="d0598c71-e669-4de2-ba97-9571df2e1693">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NSM2" name="NSM2" elementKind="" description="Query sessions initiated from a terminal in the library and running in the simplified search mode offer spelling help and query-completion-as-you-type." timeCreated="2011-12-09T10:32:27.238+0100" lastModified="2017-01-17T19:34:22.018+0100" uniqueID="281ecb72-4233-4e95-9611-d7a8d3b50f10" workPackage="" abstractionLevel="" id="NSM2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.018+0100" uniqueID="65fe1fd2-8179-43cd-8182-d36f3684303e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:32:45.434+0100" uniqueID="152f758d-69bb-4271-a5a9-b18f5e3cc1c5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:32:45.437+0100" uniqueID="9b3c078d-e681-4565-870e-bcbae2ee5a42">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NSM3" name="NSM3" elementKind="" description="Users using the basic search mode will be presented with reading suggestions that may be interesting to them based on the heuristics implemented in the BookTip™-system." timeCreated="2011-12-09T10:32:56.054+0100" lastModified="2017-01-17T19:34:22.036+0100" uniqueID="d996b788-1a2c-4e70-9da1-82e7155e1305" workPackage="" abstractionLevel="" id="NSM3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.035+0100" uniqueID="01769459-4ac7-4b5e-af6e-e49777561e08" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:33:22.953+0100" uniqueID="cd380bee-cdd7-4edc-84c4-73e4a4798db8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:33:22.955+0100" uniqueID="2b5630f8-e830-4b91-aad7-f050a3b799b9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NSM4" name="NSM4" elementKind="" description="LMS will provide between 3 and 5 reading suggestions within 5s after issuing the query." timeCreated="2011-12-09T10:33:39.232+0100" lastModified="2017-01-17T19:34:22.051+0100" uniqueID="56dbf1fa-ab3a-4ce7-b845-1c5c6c72f9d4" workPackage="" abstractionLevel="" id="NSM4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.050+0100" uniqueID="c72c00b5-76e5-49f1-9235-04c5c2a6a3c4" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:34:08.419+0100" uniqueID="d2c09029-154e-4363-a8f9-8a6d150a0e77">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:34:08.423+0100" uniqueID="f1c8207e-8f97-46ba-b5b9-4dbafc0d8bf3">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NSM5" name="NSM5" elementKind="" description="Librarians may define preferred media manually or by automatically created queries to appear as suggestions to readers when doing their search. " timeCreated="2011-12-09T10:34:25.214+0100" lastModified="2017-01-17T19:34:22.066+0100" uniqueID="e9b02eba-eba5-4d60-82b1-f8885c39fe64" workPackage="" abstractionLevel="" id="NSM5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.065+0100" uniqueID="4b2e02d7-b50a-464c-8326-6c982ba89ad8" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:34:36.341+0100" uniqueID="40015501-b4ce-4c80-bf49-a9294377138e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:34:36.342+0100" uniqueID="321ab487-f318-4dcd-8910-62b49f29ac50">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Leases" timeCreated="2011-12-09T15:11:01.119+0100" lastModified="2017-01-17T19:34:23.683+0100" uniqueID="12da6f84-92c0-429d-850c-f21bad723c7a">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.531+0100" uniqueID="ce1e6dc7-2be9-48dd-a4ca-d920ce3f9fa4" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="folder:Folder" name="Reservation" timeCreated="2011-12-07T15:48:05.366+0100" lastModified="2017-01-17T19:34:23.004+0100" uniqueID="1dee8b92-1d48-4b90-9e2d-bec997194233">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.767+0100" uniqueID="d5ac9d7e-e274-469c-a6f7-ef991e04060e" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="RES1" name="RES1" elementKind="" description="Users may reserve media from the catalog. Reserved media may only be lent by the holder of the reservation." timeCreated="2011-12-09T10:35:29.031+0100" lastModified="2017-01-17T19:34:22.790+0100" uniqueID="1d83f68a-7303-4079-b499-d1cc9105c606" workPackage="" abstractionLevel="" id="RES1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.790+0100" uniqueID="fef629d0-6480-44a3-acc2-df3e3dd1b0bc" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:36:00.620+0100" uniqueID="8d7b025f-02b2-4465-a477-e4005deec726">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:36:00.621+0100" uniqueID="48069e58-fb70-482e-a4a9-8e9d4dc7aebd">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES2" name="RES2" elementKind="" description="A reader may not hold more than one reservation for the same medium at any time." timeCreated="2011-12-09T10:36:19.984+0100" lastModified="2017-01-17T19:34:22.818+0100" uniqueID="18643c4c-d88c-479e-91d1-a9d96d8a0871" workPackage="" abstractionLevel="" id="RES2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.817+0100" uniqueID="1f3c01e0-f59a-43fe-9d09-ec192f824187" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:36:24.601+0100" uniqueID="0bab19ef-f90f-4d57-b8ef-e72e4113f0ee">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:36:24.602+0100" uniqueID="1960d8b9-9f5d-49f9-8082-475c5870ffb6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES3" name="RES3" elementKind="" description="Reserving a medium grants the reserver the right of lending that medium as soon as the previous reserver returns it." timeCreated="2011-12-09T10:36:43.518+0100" lastModified="2017-01-17T19:34:22.840+0100" uniqueID="929d7565-d38b-4b12-8b6d-178ce6b9764c" workPackage="" abstractionLevel="" id="RES3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.839+0100" uniqueID="39012d4f-3290-42f9-9f86-e75c88bea39e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:37:11.644+0100" uniqueID="6642c7bd-2a71-45c7-a9b1-2af8060cd479">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:37:11.645+0100" uniqueID="dfd25a80-f517-4ab0-a4e0-36536cd3012f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES4" name="RES4" elementKind="" description="All reservations for a medium are satisfied in a first-come-first serve-strategy." timeCreated="2011-12-09T10:37:23.492+0100" lastModified="2017-01-17T19:34:22.862+0100" uniqueID="630a9627-0dca-487d-9efc-68748fa9a0f3" workPackage="" abstractionLevel="" id="RES4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.860+0100" uniqueID="29054457-a945-4c4e-aedd-155ac956fc7e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:37:40.043+0100" uniqueID="b1dfee25-6b55-40fb-b7a3-664511d1fa4f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:37:40.043+0100" uniqueID="9f9be4c9-f704-4dad-a401-30d2deca9b4e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES5" name="RES5" elementKind="" description="If a reader tries to lend a reserved medium without holding the first reservation for this medium, the librarian will be notified at once." timeCreated="2011-12-09T10:38:19.589+0100" lastModified="2017-01-17T19:34:22.887+0100" uniqueID="6464d4a6-b358-43ff-83ca-543d33f0765c" workPackage="" abstractionLevel="" id="RES5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.886+0100" uniqueID="baa343c5-c057-46b5-83ae-87d96ede22fe" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:38:37.311+0100" uniqueID="ae01f126-534d-4bbd-b233-707662e564ed">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:38:37.312+0100" uniqueID="468226c6-d1e0-4f5d-b20f-651472697841">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES6" name="RES6" elementKind="" description="When a reserved medium becomes available, the reader with the earliest reservation is notified." timeCreated="2011-12-09T10:38:58.662+0100" lastModified="2017-01-17T19:34:22.905+0100" uniqueID="1bc93a57-1552-4f06-9e38-c8a431a825d2" workPackage="" abstractionLevel="" id="RES6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.905+0100" uniqueID="79573a27-ff92-43e1-a386-8f91676c4674" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:39:19.386+0100" uniqueID="92a7e7f1-7c7d-4405-b5dd-e32e378bf983">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:39:19.389+0100" uniqueID="af73fdfe-7dbd-4819-b0db-8e0a4f8a5ef6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES7" name="RES7" elementKind="" description="If a reserved medium is not leased by the reader holding the earliest reservation for that medium within a customizable period after the medium becomes available, the reservation defaults." timeCreated="2011-12-09T10:39:31.325+0100" lastModified="2017-01-17T19:34:22.924+0100" uniqueID="425c11b0-8a9f-43cf-8eb2-541d315245cc" workPackage="" abstractionLevel="" id="RES7">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.923+0100" uniqueID="280d8fa6-bffc-4814-a0bd-66c029890858" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:39:42.505+0100" uniqueID="67820fa1-bcc3-40c5-92c0-158f14a39b4c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:39:42.508+0100" uniqueID="de3e5cb9-36b1-4517-b82e-c632807a10ee">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES8" name="RES8" elementKind="" description="If a reservation defaults, the reader holding the defaulted reservation may be charged a customizable amount of fees." timeCreated="2011-12-09T10:39:59.211+0100" lastModified="2017-01-17T19:34:22.943+0100" uniqueID="e18dd2e8-9444-463f-950b-20b3027045a7" workPackage="" abstractionLevel="" id="RES8">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.942+0100" uniqueID="866e678c-527a-46f7-9f3b-9636a4c89f5e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:40:12.138+0100" uniqueID="57ba688e-6cb5-4052-854e-c34d4cb1bd23">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:40:12.143+0100" uniqueID="69373fae-9f3a-45ec-baf0-2b8b990838a0">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES9" name="RES9" elementKind="" description="If a reservation defaults, it is removed from the list of reservations for the medium it refers to and the medium enter the state available if no other reservations are pending." timeCreated="2011-12-09T10:40:28.677+0100" lastModified="2017-01-17T19:34:22.966+0100" uniqueID="38ea26e6-a9bf-42c2-a6ae-1aac4b08a8e7" workPackage="" abstractionLevel="" id="RES9">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.965+0100" uniqueID="124bbe9d-c86d-4950-97f4-a279fa3c0ad9" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:40:40.165+0100" uniqueID="f6b2efb1-9d40-4760-8143-2b07a7710709">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:40:40.170+0100" uniqueID="6cd3b5e4-59f1-42cc-a5d9-fc65d0abe32a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES10" name="RES10" elementKind="" description="A reader may release his reservation before it defaults." timeCreated="2011-12-09T10:40:56.284+0100" lastModified="2017-01-17T19:34:22.986+0100" uniqueID="93cc4f21-5073-404a-a225-37eb9ee8af47" workPackage="" abstractionLevel="" id="RES10">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:22.985+0100" uniqueID="63f83550-d67f-4b45-8ba3-44104a8b1be8" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:41:11.554+0100" uniqueID="7e5e73a0-0a1b-478d-b299-b7072b9c8829">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:41:11.556+0100" uniqueID="0c83358e-61fc-4ad8-b88c-ff5ca6186386">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RES11" name="RES11" elementKind="" description="If a reserved medium is leased by the reader holding the reservation, the reservation is removed." timeCreated="2011-12-09T10:41:28.498+0100" lastModified="2017-01-17T19:34:23.004+0100" uniqueID="e5e0e2fb-d760-48d4-9e80-786ed8a2a9f0" workPackage="" abstractionLevel="" id="RES11">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.004+0100" uniqueID="a48f4bdd-bd76-4b43-bdbb-95e36ee5bbf5" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:41:38.495+0100" uniqueID="28710ec2-41a4-4d7b-b33e-dbe8e2e2b961">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:41:38.497+0100" uniqueID="1ba4509a-8af9-4637-abb7-a801d0da53d8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Lease Creation &amp; Prolongation" timeCreated="2011-12-07T15:48:36.519+0100" lastModified="2017-01-17T19:34:23.458+0100" uniqueID="9d06cf51-35c4-4f5e-83e3-98f9899dc470">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.206+0100" uniqueID="45cf9b6b-a631-4255-a096-bfa13fcf3fe2" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="LCP1" name="LCP1" elementKind="" description="A user may lease a copy or prolong a lease only if the reader account has the capabilities for doing so." timeCreated="2011-12-09T10:42:41.299+0100" lastModified="2017-01-17T19:34:23.235+0100" uniqueID="f3468bec-b766-4f29-b0d4-f75124d8a62c" workPackage="" abstractionLevel="" id="LCP1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.234+0100" uniqueID="316e19dd-a8af-467c-a346-cad815e822e9" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:42:54.564+0100" uniqueID="96e35d64-0154-4453-912c-b71f302e8b4a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:42:54.567+0100" uniqueID="2ee3fb64-13a3-46d0-86da-1ddfd62a322d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LCP2" name="LCP2" elementKind="" description="A user may lease a copy unless the copy is reserved for another reader. " timeCreated="2011-12-09T10:43:08.283+0100" lastModified="2017-01-17T19:34:23.273+0100" uniqueID="2dacb8b6-42e5-4c64-95d4-3f93c4129ee1" workPackage="" abstractionLevel="" id="LCP2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.272+0100" uniqueID="96b357d8-29d1-49d7-ab22-e8484998a337" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:43:33.726+0100" uniqueID="ba9518a7-dc61-498c-8100-698ce1457909">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:43:33.727+0100" uniqueID="483b2f65-4841-4c62-8236-067664918b45">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LCP3" name="LCP3" elementKind="" description="A user may prolong a lease a copy unless the copy is reserved for another reader or the maximum number of prolongations is reached." timeCreated="2011-12-09T10:44:07.779+0100" lastModified="2017-01-17T19:34:23.304+0100" uniqueID="79540444-2edb-491d-a94c-fe3036855318" workPackage="" abstractionLevel="" id="LCP3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.302+0100" uniqueID="5575f89a-2ddb-43cc-805a-8037beecefa1" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:46:23.090+0100" uniqueID="0fb6eb53-6fb9-4760-afb3-c0307931badd">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:46:23.093+0100" uniqueID="ab962c41-ebbb-4465-865d-d0afc690da45">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LCP4" name="LCP4" elementKind="" description="Repeated prolonging of the loan period of a medium may be limited by a customizable threshold." timeCreated="2011-12-09T10:46:47.714+0100" lastModified="2017-01-17T19:34:23.332+0100" uniqueID="bc617e38-f4ef-40ec-9ac4-4015bada68cb" workPackage="" abstractionLevel="" id="LCP4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.330+0100" uniqueID="90e0f93c-d848-4745-8c6c-70aaaa72fa4d" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:47:10.854+0100" uniqueID="3d9478c9-dddb-404b-a108-b1c3a9aca4b7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:47:10.856+0100" uniqueID="420f2a8a-5a86-434f-93d8-53219a8f6a71">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LCP5" name="LCP5" elementKind="" description="Leases may only be prolonged for the reader holding the lease." timeCreated="2011-12-09T10:47:34.240+0100" lastModified="2017-01-17T19:34:23.358+0100" uniqueID="e7badb39-2c8d-4877-858c-3b8472aefb53" workPackage="" abstractionLevel="" id="LCP5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.358+0100" uniqueID="c94eebff-1f9c-4d41-ba4f-a0da9a6947e9" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:47:38.997+0100" uniqueID="a4e91a39-4f8a-4794-a945-15702961563d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:47:38.999+0100" uniqueID="ec1c0a10-0c34-4299-8c45-dc0bcb460d79">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LCP6" name="LCP6" elementKind="" description="If a copy is leased or prolonged, the state of the copy and the reader’s account are updated accordingly within 5s." timeCreated="2011-12-09T10:47:52.241+0100" lastModified="2017-01-17T19:34:23.389+0100" uniqueID="aa425d18-0432-4c3e-a192-cabd2278a65a" workPackage="" abstractionLevel="" id="LCP6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.388+0100" uniqueID="25a500ce-19ec-48e7-aec6-670bd8170b42" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:48:06.216+0100" uniqueID="e4ef1330-57a6-4e5a-af9e-315c9af0b4d2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:48:06.218+0100" uniqueID="ab45abd3-e515-4901-840b-f72167d464a1">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LCP7" name="LCP7" elementKind="" description="LMS computes the lease period automatically based on the lender’s account type." timeCreated="2011-12-09T10:48:59.153+0100" lastModified="2017-01-17T19:34:23.418+0100" uniqueID="7ca69604-26a9-4637-8f70-a20d91f91475" workPackage="" abstractionLevel="" id="LCP7">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.416+0100" uniqueID="8ee564f2-fbe5-4486-9fc7-1988d021e13a" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:49:03.008+0100" uniqueID="746ec08c-cdcf-4cbd-a399-0c0514517914">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:49:03.009+0100" uniqueID="e2856416-9379-4afc-8cfd-b42da2f01733">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LCP8" name="LCP8" elementKind="" description="Librarians may manually modify the state of a lease, prolongation, or reservation." timeCreated="2011-12-09T10:49:25.088+0100" lastModified="2017-01-17T19:34:23.441+0100" uniqueID="a2c6f466-9021-4d14-95eb-9993fb58f170" workPackage="" abstractionLevel="" id="LCP8">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.440+0100" uniqueID="363e9e8c-1ae4-48bd-bb3b-86d5603c5b5b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:49:36.026+0100" uniqueID="183ba3e7-5206-4b28-8e5c-814eb3e03cae">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:49:36.027+0100" uniqueID="7bd4ef93-fe48-4947-8810-3852a2cae38f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LCP9" name="LCP9" elementKind="" description="Lending and returning of batches of media must be supported such that no more than 15 seconds are needed to deal with 10 media items." timeCreated="2011-12-09T10:49:57.074+0100" lastModified="2017-01-17T19:34:23.458+0100" uniqueID="0e61df59-ade3-4561-b9b4-ce145f2f04db" workPackage="" abstractionLevel="" id="LCP9">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.458+0100" uniqueID="5cfcae6a-b116-44b3-9a1a-75152898e795" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:50:18.698+0100" uniqueID="edaf6308-0a48-41e7-b8aa-4e16fdba3589">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:50:18.699+0100" uniqueID="7e2f01d2-358d-452c-9a75-f62e4641dba0">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Lease Termination" timeCreated="2011-12-07T15:48:58.773+0100" lastModified="2017-01-17T19:34:23.683+0100" uniqueID="130fa138-d2c8-44e5-963f-856d1b566a96">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.578+0100" uniqueID="f75d0fa4-cd8b-4c53-b0b5-c7bc3f0b1f7b" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="LET1" name="LET1" elementKind="" description="If a leased copy is returned to the library, its current lease will be terminated." timeCreated="2011-12-09T10:53:00.208+0100" lastModified="2017-01-17T19:34:23.596+0100" uniqueID="a3a066a3-7106-48ba-ac3d-42d81e903df7" workPackage="" abstractionLevel="" id="LET1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.596+0100" uniqueID="b4b5d9d0-9061-4a26-b5ad-38f9d550114d" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:53:29.715+0100" uniqueID="fedbf800-abe1-48fc-bef0-80e96b04ed6b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:53:29.716+0100" uniqueID="f6296d89-b499-477f-bd14-3e1a864c2b90">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LET2" name="LET2" elementKind="" description="If an overdue copy is returned to the library, the overdue days are indicated to the user within 2s. " timeCreated="2011-12-09T10:53:47.259+0100" lastModified="2017-01-17T19:34:23.614+0100" uniqueID="6a6a848e-0450-44ca-9cf1-6afc33971c92" workPackage="" abstractionLevel="" id="LET2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.614+0100" uniqueID="9bbbfae6-c933-4010-b4a1-c22311ab1965" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:54:32.944+0100" uniqueID="6c8020e5-83dc-40a0-8b7a-b863cfb03aa9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:54:32.945+0100" uniqueID="89146859-9214-4da1-af08-0fad211d0b43">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LET3" name="LET3" elementKind="" description="When a lease is being terminated, LMS calculates any overdue fees, updates the reader's account accordingly, and notifies the user." timeCreated="2011-12-09T10:54:43.198+0100" lastModified="2017-01-17T19:34:23.631+0100" uniqueID="2d128c9d-503a-4168-af03-926c56060ea5" workPackage="" abstractionLevel="" id="LET3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.630+0100" uniqueID="5ee51c10-4358-439b-b539-2721f44ea572" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:54:54.452+0100" uniqueID="65439953-c149-43e6-8d9a-8f090a6e3d9a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:54:54.453+0100" uniqueID="8ac1231f-916a-43ba-b495-059e46898a3a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LET4" name="LET4" elementKind="" description="Librarians may terminate a lease manually." timeCreated="2011-12-09T10:55:14.493+0100" lastModified="2017-01-17T19:34:23.648+0100" uniqueID="2253acf5-c2b2-455b-9f0c-dbddedc4056b" workPackage="" abstractionLevel="" id="LET4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.647+0100" uniqueID="1142aa69-e3b7-4b37-9381-ba8468772399" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:55:27.214+0100" uniqueID="2a091d8d-20c0-47c3-b60e-719367e7671e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:55:27.214+0100" uniqueID="0c60f5b7-c8a3-4f69-99ad-67d2de2184a9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LET5" name="LET5" elementKind="" description="If a reserved medium is returned, it becomes available to other readers immediately after the end of the transaction." timeCreated="2011-12-09T10:55:42.721+0100" lastModified="2017-01-17T19:34:23.665+0100" uniqueID="0759341c-d91b-4ce4-ab7f-5cc599a9f97f" workPackage="" abstractionLevel="" id="LET5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.665+0100" uniqueID="3dc13c9f-8a9a-4d09-aa95-cc815a2639e1" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:56:00.535+0100" uniqueID="929dd328-2644-4842-9cf1-12fc0ec6e2a3">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:56:00.536+0100" uniqueID="e4fa4415-7d97-4e0a-986d-55fcbdb4d348">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LET6" name="LET6" elementKind="" description="When a copy is returned at the front desk, the librarian present there is notified within 1s if there is currently a reservation for the medium of the copy." timeCreated="2011-12-09T10:56:19.454+0100" lastModified="2017-01-17T19:34:23.683+0100" uniqueID="91c4dbf0-9eae-4591-a15d-4ea8ec705d87" workPackage="" abstractionLevel="" id="LET6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:23.682+0100" uniqueID="b2b68bec-c671-4874-a55d-a321d4430c99" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:56:29.295+0100" uniqueID="3b36cd98-3480-4a46-8422-9b103fd66945">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:56:29.296+0100" uniqueID="8d494a76-818e-4146-9f46-a01947a5a5a4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Accounts and Capabilities" timeCreated="2011-12-09T15:11:42.025+0100" lastModified="2017-01-17T19:34:25.903+0100" uniqueID="97672f30-dd50-4d35-84e5-5e4c7d9d039d">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.445+0100" uniqueID="7191fea0-a28a-405f-8a6b-664bca0c75bb" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="folder:Folder" name="Reader Account" timeCreated="2011-12-07T15:49:21.604+0100" lastModified="2017-01-17T19:34:24.782+0100" uniqueID="ec97b3b1-33d9-46cc-b9e8-85022e59d6a2">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.617+0100" uniqueID="57986ad7-97d5-4117-b7b3-5d8503c42a80" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="RAC1" name="RAC1" elementKind="" description="A Librarian can set up a new reader account." timeCreated="2011-12-09T10:56:53.289+0100" lastModified="2017-01-17T19:34:24.632+0100" uniqueID="cf9a24a6-3a63-45dc-a61b-8bfb81c350a7" workPackage="" abstractionLevel="" id="RAC1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.632+0100" uniqueID="c8ded25a-461e-4836-bd89-74af95ece958" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:57:06.064+0100" uniqueID="ce6be4a8-1c21-465e-abf9-848699744d1b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:57:06.065+0100" uniqueID="5bdde281-564a-4996-a6fc-7782c2d0c553">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RAC2" name="RAC2" elementKind="" description="A reader accounts stores name, birthday, reader ID, and portrait of the reader, contact data (e.g., address, email, phone), account type, leases and reservations, suggestions and comments, dates of set-up and expiry, and a history of previous actions." timeCreated="2011-12-09T10:57:23.326+0100" lastModified="2017-01-17T19:34:24.648+0100" uniqueID="12259d1e-8978-49ab-afef-0fb2eef52c4a" workPackage="" abstractionLevel="" id="RAC2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.647+0100" uniqueID="2218d04b-237d-4e59-b814-c430da524420" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:57:38.495+0100" uniqueID="cb3ca5f7-783f-4e5b-a19f-426f0d41a3c0">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:57:38.496+0100" uniqueID="f655f4ef-b6d3-4cc2-93d0-0029248c727b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RAC3" name="RAC3" elementKind="" description="If a reader account expires, or if certain policies or thresholds are violated, the account is deactivated automatically." timeCreated="2011-12-09T10:57:49.011+0100" lastModified="2017-01-17T19:34:24.663+0100" uniqueID="13fa5a92-35e2-46e5-a956-addc62dd0590" workPackage="" abstractionLevel="" id="RAC3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.663+0100" uniqueID="c603d30c-e100-46ea-8f23-b8b2d280765b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:58:35.346+0100" uniqueID="38d1d2b2-1f78-4816-a0b9-aa8660f6e3c3">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:58:35.347+0100" uniqueID="77d5336c-e1a6-4872-b938-aafed53a5db2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RAC4" name="RAC4" elementKind="" description="A librarian may deactivate a reader account manually." timeCreated="2011-12-09T10:58:47.252+0100" lastModified="2017-01-17T19:34:24.683+0100" uniqueID="c2130967-a3b3-4356-9f4c-611635f98189" workPackage="" abstractionLevel="" id="RAC4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.683+0100" uniqueID="01bcf1c0-aafd-4c07-887b-6c2187c77500" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:59:06.232+0100" uniqueID="e169e9a8-6483-4450-9e05-f98aeca84e03">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:59:06.233+0100" uniqueID="a6ea7969-5361-445f-b5bb-cdf07470f344">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RAC5" name="RAC5" elementKind="" description="A deactivated reader account allows readers only payment of fees, reading and printing the account action trail, returning of copies, and termination of the account. " timeCreated="2011-12-09T10:59:25.760+0100" lastModified="2017-01-17T19:34:24.703+0100" uniqueID="a995072e-2307-411e-a5a9-8473d2de2cd3" workPackage="" abstractionLevel="" id="RAC5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.702+0100" uniqueID="05c48388-6195-4fe2-864a-2b2cae9ce4db" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T10:59:47.941+0100" uniqueID="2dc4c40e-efbb-440e-a814-0cdc71fcf198">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T10:59:47.942+0100" uniqueID="ea39af99-dc47-4e10-9dbc-fa85295d367f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RAC6" name="RAC6" elementKind="" description="A librarian may trigger all kinds actions of on behalf of a reader with a deactivated account." timeCreated="2011-12-09T11:00:10.834+0100" lastModified="2017-01-17T19:34:24.719+0100" uniqueID="6e83b44c-a711-4032-bb4a-3fb004c01854" workPackage="" abstractionLevel="" id="RAC6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.718+0100" uniqueID="c455dcf2-e530-4c0d-b1a0-78b3e7e4c605" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T11:00:21.610+0100" uniqueID="eb4d370e-3fa7-414d-9cc6-c05c3aba8f8a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T11:00:21.611+0100" uniqueID="7e991e73-4467-4ef0-9e87-b052bf532075">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RAC7" name="RAC7" elementKind="" description="A deactivated account can only be manually reactivated by a librarian." timeCreated="2011-12-09T11:01:17.182+0100" lastModified="2017-01-17T19:34:24.734+0100" uniqueID="2457b297-690c-409b-bf0d-99d003c1b7f9" workPackage="" abstractionLevel="" id="RAC7">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.734+0100" uniqueID="25bf45b6-2ede-4ae9-8a51-1bf07a09a508" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T11:01:33.870+0100" uniqueID="324f6eca-4afb-4ac4-ac3a-0a70bf707014">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T11:01:33.872+0100" uniqueID="21f18be7-2b41-4e12-9b22-64a0e56eedf9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RAC8" name="RAC8" elementKind="" description="A librarian may perform actions on behalf of a reader." timeCreated="2011-12-09T11:01:53.016+0100" lastModified="2017-01-17T19:34:24.750+0100" uniqueID="92031f3a-3d51-4aae-b035-6e625ee8d6a6" workPackage="" abstractionLevel="" id="RAC8">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.749+0100" uniqueID="5a5f0766-26f8-49a5-b649-713ae16c800b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T11:02:07.609+0100" uniqueID="c2e5cb3e-bae0-4596-b99c-0f54e1377d12">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T11:02:07.610+0100" uniqueID="a832130f-5dcf-41ec-b282-040821193493">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RAC9" name="RAC9" elementKind="" description="Readers are notified about actions initiated on their behalf." timeCreated="2011-12-09T11:02:29.894+0100" lastModified="2017-01-17T19:34:24.766+0100" uniqueID="4f78753e-dac8-4cc1-90e6-f41ea403fcce" workPackage="" abstractionLevel="" id="RAC9">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.765+0100" uniqueID="1b60cdfc-bde9-4f73-89dd-0572e01d6916" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T11:02:46.533+0100" uniqueID="ca2d51d6-e2bf-42e4-8af3-6bfc11533053">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T11:02:46.534+0100" uniqueID="3f725e86-6e4f-42ff-847f-5d57765abae7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="RAC10" name="RAC10" elementKind="" description="A reader account may be deactivated, active, or closed." timeCreated="2011-12-09T12:37:33.293+0100" lastModified="2017-01-17T19:34:24.782+0100" uniqueID="17e4fe4c-2a85-4fe3-b9ab-e423bd1d5233" workPackage="" abstractionLevel="" id="RAC10">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:24.781+0100" uniqueID="7913f89b-c3f5-4b50-b4c4-619165d74cb1" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T12:37:38.218+0100" uniqueID="1ec7aeaa-062b-45e7-adac-1f16dd05f7f7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T12:37:38.219+0100" uniqueID="3be7f606-1d38-4cff-b2c0-aa06129291b1">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Fees &amp; Fines" timeCreated="2011-12-07T15:49:57.300+0100" lastModified="2017-01-17T19:34:25.243+0100" uniqueID="948bcdf7-d8d7-44c8-a689-8bc1080b7ea7">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.007+0100" uniqueID="f4fa0af5-5a0d-4563-9bc9-f2526b4f4654" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="FAF1" name="FAF1" elementKind="" description="All actions that change the damage state of a copy, terminate a lease or a reservation, or that change the state of a reader account trigger a calcu¬lat¬ion of due fees and update the fee balance of the respective reader." timeCreated="2011-12-09T12:38:07.958+0100" lastModified="2017-01-17T19:34:25.024+0100" uniqueID="ae9c0ab1-ca23-40c6-8458-66dc85a7e12e" workPackage="" abstractionLevel="" id="FAF1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.023+0100" uniqueID="5d48f88b-ada3-465f-9f1a-1d4b6f1676f9" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T12:38:18.444+0100" uniqueID="bb8740d5-941d-47d4-a904-3c6b46e769a1">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T12:38:18.445+0100" uniqueID="ce665c42-5213-4032-a951-be9a2c1b94fb">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF1a" name="FAF1a" elementKind="" description="If a fee calculation is triggered, all other pending actions on the account are completed first." timeCreated="2011-12-09T12:39:49.099+0100" lastModified="2017-01-17T19:34:25.041+0100" uniqueID="7684868e-6b2c-4027-bc56-0aa9a50a11f9" workPackage="" abstractionLevel="" id="FAF1a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.040+0100" uniqueID="1305d5da-8667-4e55-8f0d-8b3e93a893dc" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T12:40:01.187+0100" uniqueID="eef88244-ac6c-41e3-8741-6985df385d08">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T12:40:01.188+0100" uniqueID="f4ebc5c9-5290-48fd-bf59-b91149e1184b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF1b" name="FAF1b" elementKind="" description="If a fee calcu¬lat¬ion yields a non-zero result, the fee balance of the respective reader is updated and checked for violation of thresholds and policies." timeCreated="2011-12-09T12:40:28.575+0100" lastModified="2017-01-17T19:34:25.058+0100" uniqueID="be3829c2-bbc0-4cb7-a235-b3b01c6bb130" workPackage="" abstractionLevel="" id="FAF1b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.058+0100" uniqueID="00e9f0ba-7e12-45dd-86b5-f5f364a93f01" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T12:40:42.399+0100" uniqueID="cb1d5deb-9e99-468a-b58f-70adf50333d7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T12:40:42.403+0100" uniqueID="df666ffa-4f2a-41da-8cc3-f19002422449">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF1c" name="FAF1c" elementKind="" description="If the balance of a reader account changes, changing the account state is considered immediately (i.e., deactivation or reactivation)." timeCreated="2011-12-09T12:41:07.399+0100" lastModified="2017-01-17T19:34:25.075+0100" uniqueID="0b3ed269-6494-4aed-8bd2-fefd408231b9" workPackage="" abstractionLevel="" id="FAF1c">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.074+0100" uniqueID="8ca3ab97-0674-4eba-81f9-3ed561eeb518" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T12:41:13.882+0100" uniqueID="b4baf5b9-fd5d-461f-b690-f54874a4cb88">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T12:41:13.883+0100" uniqueID="5346abaa-5e52-4cd1-8223-24ef2d8d9d61">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF1d" name="FAF1d" elementKind="" description="If a fee calculation yields a non-zero result, the reader is notified of the computation, its result, and provided with an explanation." timeCreated="2011-12-09T12:42:11.830+0100" lastModified="2017-01-17T19:34:25.091+0100" uniqueID="2d3fa2bf-4a7b-4415-9844-c4ef156901cb" workPackage="" abstractionLevel="" id="FAF1d">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.090+0100" uniqueID="a539b8cf-ef97-4480-a0a7-6e170b850808" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T12:42:53.479+0100" uniqueID="6eee30b3-8c86-40eb-9190-6936cbce9e91">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T12:42:53.480+0100" uniqueID="2bf52848-f825-4ec9-bd12-b9f71974c896">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF2" name="FAF2" elementKind="" description="The due fees for account A are calculated as  FL + FR + FC + FA over all leases, reservations, and damages, where: FL is the fee for overdue leases (see FAF.2a); FR is the fee for reservations; FC is the fine for damaging a copy; FA are account specific fees." timeCreated="2011-12-09T12:43:13.774+0100" lastModified="2017-01-17T19:34:25.107+0100" uniqueID="db00b487-a439-4af8-bfbe-69ba83fd29cd" workPackage="" abstractionLevel="" id="FAF2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.106+0100" uniqueID="bcf18320-8992-4fb1-82f5-88ecb9aac349" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T12:48:17.006+0100" uniqueID="cd1407cf-bc35-41bb-913a-b3406fd50969">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T12:48:17.008+0100" uniqueID="45bab70f-183c-45db-b787-f583ca5c549e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF2a" name="FAF2a" elementKind="" description="The fee FL for an individual overdue lease l is computed as dl * t(m) *  al(r), where: dl is the number of days today and the due date of l minus an offset; t(m) is a customizable factor depending on the type of the medium m; and al(r) is a customizable factor for the account type of the reader holding the lease." timeCreated="2011-12-09T12:48:37.730+0100" lastModified="2017-01-17T19:34:25.131+0100" uniqueID="9da28b52-7af0-4c11-bb11-7d624cfb93d5" workPackage="" abstractionLevel="" id="FAF2a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.130+0100" uniqueID="bd298e6e-2d78-479b-a573-532e0757d91a" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T12:49:37.494+0100" uniqueID="e8c66069-47e1-4610-889d-c9b0bcf13b4c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T12:49:37.495+0100" uniqueID="6296ca18-7cfe-4031-bfa5-671840118d78">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF2b" name="FAF2b" elementKind="" description="The fee FR for an individual reservation is computed as (rr * tr(m) + rc * tc(m))* ar(r), where: &#xD;&#xA;&#x9;rr and rc are the required and cancelled/defaulted reservations, respectively;&#xD;&#xA;&#x9;tr(m) and tc(m) are customizable factors depending on the type of the medium m;&#xD;&#xA;&#x9;ar(r) is a customizable factor for the account type of the reader. &#xD;&#xA;" timeCreated="2011-12-09T12:55:32.523+0100" lastModified="2017-01-17T19:34:25.152+0100" uniqueID="43dbe2cc-6d62-4bc8-9e7e-a9fedfaa62ec" workPackage="" abstractionLevel="" id="FAF2b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.151+0100" uniqueID="7e02c482-e40f-4061-bde0-121632ac700b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T12:57:56.797+0100" uniqueID="b520b96c-92e3-4d33-b5e6-60c594e5c48a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T12:57:56.798+0100" uniqueID="a142e6fd-0a7b-4d9d-a8fa-3bdbe5022c53">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF2c" name="FAF2c" elementKind="" description="The fee FC for damages to copies is assessed individually by a librarian." timeCreated="2011-12-09T12:59:10.648+0100" lastModified="2017-01-17T19:34:25.178+0100" uniqueID="8148cf66-058d-4025-b648-07980f30c3a0" workPackage="" abstractionLevel="" id="FAF2c">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.177+0100" uniqueID="1e7b91a8-e257-4ce6-b691-e463559642af" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:04:29.113+0100" uniqueID="552cc506-1689-460e-8abe-119d10ccf4b8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:04:29.117+0100" uniqueID="73c2c939-cdc1-4335-ba51-1638c97e3cc6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF2d" name="FAF2d" elementKind="" description="The fee FA for a reader account is computed as td + tu (x,y) + rc(y), where &#xD;&#xA;&#x9;td is a customizable administration fee applicable if an account is deactivated;&#xD;&#xA;&#x9;tu is the upgrade fee that is applicable when upgrading an account from status x to status y (x>=y; x=y is the yearly base rate for the account);&#xD;&#xA;&#x9;rc(y) ist the administrative fee for creating an account of status y. &#xD;&#xA;" timeCreated="2011-12-09T13:10:20.088+0100" lastModified="2017-01-17T19:34:25.193+0100" uniqueID="fa5ea99a-267b-492b-815c-62fa925c1717" workPackage="" abstractionLevel="" id="FAF2d">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.193+0100" uniqueID="02bec43b-b3fb-4de7-b3b8-c4d2c78013bc" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:10:32.001+0100" uniqueID="7fffd148-a3f2-42af-adc1-40715eb31fd3">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:10:32.004+0100" uniqueID="10ead9e0-d1d9-4835-a3e1-9edc1af992ad">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF3" name="FAF3" elementKind="" description="A reader can settle the balance of his account by paying the outstanding fees." timeCreated="2011-12-09T13:10:51.738+0100" lastModified="2017-01-17T19:34:25.210+0100" uniqueID="ae2ffed5-028b-42d6-9875-6da8528bb3c5" workPackage="" abstractionLevel="" id="FAF3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.210+0100" uniqueID="f6cb2fc0-2cff-4941-95ba-b97fae152bf2" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:11:18.558+0100" uniqueID="2ff1d0c3-3ced-4918-8dda-c2cf1d8e8f35">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:11:18.561+0100" uniqueID="8b2c4e66-a919-4535-a71a-8b814a7b0b00">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF3a" name="FAF3a" elementKind="" description="If an account is deactivated, it can only be reactivated after complete settlement of all fees." timeCreated="2011-12-09T13:11:32.219+0100" lastModified="2017-01-17T19:34:25.225+0100" uniqueID="b4d8c544-5080-475a-87c5-121cbacccce5" workPackage="" abstractionLevel="" id="FAF3a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.225+0100" uniqueID="31f75dbf-7413-4a4b-a78d-c9bf6f814365" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:11:42.761+0100" uniqueID="2cc4f60e-8455-49e3-a66e-399dd978b6a5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:11:42.765+0100" uniqueID="0e87da1b-f229-4f41-a31b-60a56af5cf0f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="FAF3b" name="FAF3b" elementKind="" description="If a reader pays fees, the payment is recorded and the reader receives a paper receipt about the payment." timeCreated="2011-12-09T13:11:54.232+0100" lastModified="2017-01-17T19:34:25.243+0100" uniqueID="5304df70-c922-428f-889d-242e17e15881" workPackage="" abstractionLevel="" id="FAF3b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.242+0100" uniqueID="f8bab6cb-1744-4209-85a1-f125da8ccf22" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:12:07.795+0100" uniqueID="c78b0d5e-ffd2-455d-b1e2-69528f2a9ef1">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:12:07.798+0100" uniqueID="0ca1ebb2-ff34-481e-b4ab-594c115d29fa">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Records &amp; Inspection" timeCreated="2011-12-07T15:50:24.272+0100" lastModified="2017-01-17T19:34:25.580+0100" uniqueID="025561fe-3a84-4182-80b5-9117cbac89cb">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.420+0100" uniqueID="4fec6853-2b44-49ea-9463-ce0f3c96b281" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="REIN1" name="REIN1" elementKind="" description="All relevant actions on a reader account are recorded electronically." timeCreated="2011-12-09T13:13:04.952+0100" lastModified="2017-01-17T19:34:25.437+0100" uniqueID="2bd5db16-fe65-48e4-a176-d22de1560150" workPackage="" abstractionLevel="" id="REIN1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.436+0100" uniqueID="5e4921a7-e729-49b5-94cb-4d7fd1ec3f53" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:13:40.691+0100" uniqueID="ea94377b-798e-4140-82da-1dc57908106e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:13:40.694+0100" uniqueID="1cbd68cc-2e31-4f00-8b6f-4138bf1796ea">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="REIN1" name="REIN1" elementKind="" description="The account action trail is stored in a secure way preventing tampering, loss, and leaking." timeCreated="2011-12-09T13:14:44.694+0100" lastModified="2017-01-17T19:34:25.453+0100" uniqueID="908087b9-f96f-48a8-b01a-13d2f9209177" workPackage="" abstractionLevel="" id="REIN1a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.452+0100" uniqueID="78d0fc75-6a6c-493c-9623-c53d42f8ffbb" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:14:59.268+0100" uniqueID="3d33edb9-4a3b-4db8-91c8-b8caf3ca44de">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:14:59.272+0100" uniqueID="950555a3-6af7-43c8-bf54-384a897e5d8a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="REIN1" name="REIN1" elementKind="" description="Reading an account action trail requires adequate capabilities." timeCreated="2011-12-09T13:15:38.365+0100" lastModified="2017-01-17T19:34:25.469+0100" uniqueID="54c7a4dc-f4de-4f11-9eb2-48d382bd486e" workPackage="" abstractionLevel="" id="REIN1b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.468+0100" uniqueID="d85b2b3b-5834-43f0-8694-803f088fcbda" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:16:09.792+0100" uniqueID="7d7d0e5a-2f72-4119-8e25-4f57e2ceb093">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:16:09.796+0100" uniqueID="2c1c53e1-2742-48b1-9b01-33e538e01c42">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="REIN2" name="REIN2" elementKind="" description="The account action trail is stored in a way compliant with privacy protection laws." timeCreated="2011-12-09T13:16:36.218+0100" lastModified="2017-01-17T19:34:25.485+0100" uniqueID="1594d84d-9239-439a-9391-2a48eb67bcda" workPackage="" abstractionLevel="" id="REIN2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.484+0100" uniqueID="9a730b83-d2cf-4892-a15d-f417ffb01f8f" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:24:27.225+0100" uniqueID="3bf88f68-0b4b-422b-b61b-14532c03cbcc">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:24:27.228+0100" uniqueID="c50f9946-57d4-4740-a185-0089137a46a8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="REIN3" name="REIN3" elementKind="" description="Account action trails may not be written to manually." timeCreated="2011-12-09T13:24:59.163+0100" lastModified="2017-01-17T19:34:25.501+0100" uniqueID="39040be8-d9a7-4287-abdf-e56c277236f0" workPackage="" abstractionLevel="" id="REIN3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.501+0100" uniqueID="be9ee05d-7959-4705-9aea-056e0bdc3eb7" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:25:15.762+0100" uniqueID="780ab28c-0841-4e90-bc16-cfc5b01baca6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:25:15.765+0100" uniqueID="1a41a4e5-512d-4daa-93fc-82c65a039686">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="REIN4" name="REIN4" elementKind="" description="Elements of the action trail can only be deleted if they are at least two years old and must be deleted if they are at most 5 years old" timeCreated="2011-12-09T13:25:31.465+0100" lastModified="2017-01-17T19:34:25.517+0100" uniqueID="9b16ce05-786d-4893-a409-36179183ef3a" workPackage="" abstractionLevel="" id="REIN4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.516+0100" uniqueID="17e95def-5e36-4ae5-b522-3e687cc3e18b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:25:42.120+0100" uniqueID="bf25f709-b55c-4d87-989b-db7972818a8a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:25:42.123+0100" uniqueID="671a361b-9411-4d5e-ae08-8db69123d02d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="REIN5" name="REIN5" elementKind="" description="Readers are notified about reader actions initiated not by them but by a librarian instead." timeCreated="2011-12-09T13:26:04.903+0100" lastModified="2017-01-17T19:34:25.532+0100" uniqueID="088abcf3-cea6-472a-99a4-46eb974509c3" workPackage="" abstractionLevel="" id="REIN5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.532+0100" uniqueID="eb0ec1b8-3310-4b59-9394-634965e8c2e0" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:26:23.584+0100" uniqueID="3424f6b9-6009-466b-950a-fa2910c7317d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:26:23.585+0100" uniqueID="c6ce2511-cd9f-4a86-aeeb-1d33c7e113b4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="REIN6" name="REIN6" elementKind="" description="Account action trails can be search, displayed, forwarded, and printed." timeCreated="2011-12-09T13:27:15.038+0100" lastModified="2017-01-17T19:34:25.548+0100" uniqueID="5103da25-6c4b-493c-b0f2-3ddcde5c0618" workPackage="" abstractionLevel="" id="REIN6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.548+0100" uniqueID="f0f0e8a4-bb70-4bd6-9eb8-5058fcfc96d4" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:27:53.732+0100" uniqueID="d34579bf-751d-4d93-9385-db9c938a49b4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:27:53.734+0100" uniqueID="18ab21cc-f358-4adb-b575-c68b95976148">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="REIN7" name="REIN7" elementKind="" description="The user may print a receipt for every (sequence of) reader actions he has been performing in the current session/on the current terminal." timeCreated="2011-12-09T13:28:48.200+0100" lastModified="2017-01-17T19:34:25.564+0100" uniqueID="c6ceb107-5094-437a-b4bb-cedaee2fdfd2" workPackage="" abstractionLevel="" id="REIN7">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.564+0100" uniqueID="7a228e37-dba1-4175-9ff4-e9c94aefca37" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:29:30.022+0100" uniqueID="a293d76d-416f-421c-8f26-8174f75d56f5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:29:30.025+0100" uniqueID="601be050-a73c-4bd6-ba53-2a3a151f76bf">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="REIN8" name="REIN8" elementKind="" description="An account action trail is opened at exactly the same time as a user is granted access to the account." timeCreated="2011-12-09T13:29:55.827+0100" lastModified="2017-01-17T19:34:25.580+0100" uniqueID="b8b38bb6-9cb6-4ab1-b414-322b3aef8237" workPackage="" abstractionLevel="" id="REIN8">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.579+0100" uniqueID="926b442e-2a68-4c76-8888-219e9ebb7be6" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:30:01.714+0100" uniqueID="2f360da6-6357-438f-be3a-aa4abe48526d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:30:01.716+0100" uniqueID="9b3f5525-f491-4839-ac18-4e0d8903442b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="User Capabilities" timeCreated="2011-12-07T15:51:19.962+0100" lastModified="2017-01-17T19:34:25.903+0100" uniqueID="fb23feed-1af7-4a35-812b-6c125fed0b52">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.729+0100" uniqueID="3fa55283-de71-4f09-9cb2-c8257108b8b1" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="UCAP1" name="UCAP1" elementKind="" description="Different user groups have the following capabilities:&#xD;&#xA;Guest: use catalog, return media/pay fee at front desk, read accession suggestions&#xD;&#xA;Reader: as guest + create/comment accession suggestions, reserve/lease/prolong media, inspect own action trail&#xD;&#xA;Reader (with AMDS): as reader + BookStation usage, online lending services, e-media access&#xD;&#xA;Librarian: as reader + manual transactions, create reports and monitor LMS operation data, accession, update own librarian account&#xD;&#xA;Chief Librarian: create librarian accounts and assign capabilities, customize LMS, and so on.&#xD;&#xA;" timeCreated="2011-12-09T13:30:40.735+0100" lastModified="2017-01-17T19:34:25.752+0100" uniqueID="51c3e620-98a2-4f79-ae0d-fefe80b11fa3" workPackage="" abstractionLevel="" id="UCAP1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.751+0100" uniqueID="3b38eb12-c0ed-4dd3-9f55-020029a3640d" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:31:51.310+0100" uniqueID="b8bbf059-41f1-47ce-b60c-ea491e2cc29e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:31:51.311+0100" uniqueID="6e8175e3-9f33-41c8-be18-d1d961ace1e6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UCAP2" name="UCAP2" elementKind="" description="The LMS is able to identify a reader either by reading the reader's ID card, or by a remote log-in of the reader, or by selecting a reader on a privileged terminal by scanning his card or by remote login, the reader is recorded as the user initiating the reader action(s)." timeCreated="2011-12-09T13:32:33.210+0100" lastModified="2017-01-17T19:34:25.772+0100" uniqueID="d4209a3e-ecb5-4841-9118-96a805e8af39" workPackage="" abstractionLevel="" id="UCAP2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.772+0100" uniqueID="a1ef2166-0134-4252-9413-40b937903908" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:33:03.577+0100" uniqueID="f0edea08-65cd-4f0d-a99b-886b8ebd8956">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:33:03.578+0100" uniqueID="ddb6520a-6567-4df6-8c2c-330c4515f149">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="A user of an LMS account must be identified in order to get access to the account." timeCreated="2011-12-09T13:33:23.560+0100" lastModified="2017-01-17T19:34:25.791+0100" uniqueID="a1965241-c0f5-4cdc-bbc9-244ae602d118" workPackage="" abstractionLevel="" id="UCAP3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.791+0100" uniqueID="1ad15600-ef2a-4de6-8ebc-e448bfce294e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:33:43.644+0100" uniqueID="65f8685a-7873-4fec-b7cf-872142b7d047">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:33:43.647+0100" uniqueID="007d72c0-9076-4ce1-bc47-a2219e8c5a81">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="If LMS grants access to an account, a session is created that records all actions in the session." timeCreated="2011-12-09T13:36:05.150+0100" lastModified="2017-01-17T19:34:25.811+0100" uniqueID="7c9085ba-cba9-42dd-ab0b-8d89b7274977" workPackage="" abstractionLevel="" id="UCAP3a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.810+0100" uniqueID="898a08fe-cf33-4297-91d0-0ad54a75aac0" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:36:10.885+0100" uniqueID="bc7553f3-b20f-473e-9f34-9d889d1a8f19">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:36:10.888+0100" uniqueID="e450ae09-49f7-475b-9482-0976b3064ade">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="A session is terminated when either of the following happen:&#xD;&#xA;&#x9;time out: no action is performed for a customizable period of time;&#xD;&#xA;&#x9;log out: the session is explicitly terminated by the user; or&#xD;&#xA;&#x9;log in: another reader logs in at the same terminal.&#xD;&#xA;" timeCreated="2011-12-09T13:36:59.878+0100" lastModified="2017-01-17T19:34:25.836+0100" uniqueID="105e0a43-62d7-4e27-9d6d-b103f6de8e2c" workPackage="" abstractionLevel="" id="UCAP3b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.836+0100" uniqueID="8fe3be35-e575-4daf-8559-a254a0243600" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:37:13.139+0100" uniqueID="4bc2d87a-127c-43b9-b47a-d171db1d3c9e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:37:13.139+0100" uniqueID="40ce26a1-4b3b-4315-84e1-6c3966724a06">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="A Librarian may inspect the list of current sessions and may tap into one of them, thus acquiring control of the session and equipping it with his own capabilities." timeCreated="2011-12-09T13:44:00.322+0100" lastModified="2017-01-17T19:34:25.856+0100" uniqueID="c670277a-360d-4e12-be3f-0f065e2f05bf" workPackage="" abstractionLevel="" id="UCAP3c">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.856+0100" uniqueID="08381fa1-0bef-4211-a9b0-1563685f3db3" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:44:20.204+0100" uniqueID="902d6de6-130b-4217-85ab-e668d801a37c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:44:20.207+0100" uniqueID="39d8115f-32a7-4cf2-b701-506f3f6657f2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UCAP3" name="UCAP3" elementKind="" description="A Librarian may create a sub-session for any given reader by identifying the reader.  " timeCreated="2011-12-09T13:45:10.668+0100" lastModified="2017-01-17T19:34:25.876+0100" uniqueID="0e958a9f-b9fe-47ed-9598-283f55cf08c8" workPackage="" abstractionLevel="" id="UCAP3d">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.875+0100" uniqueID="ed2237de-f637-42ed-bf6b-adbda5cac37b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:46:09.990+0100" uniqueID="08c3340c-324d-49db-9cc6-940dcb65e00d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:46:09.993+0100" uniqueID="9d9f3443-b0c8-4a11-b149-e41bd7f45253">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UCAP4" name="UCAP4" elementKind="" description="Reader actions can only take place if &#xD;&#xA;&#x9;the user has the appropriate privileges&#xD;&#xA;&#x9;the medium is in the appropriate state&#xD;&#xA;&#x9;the user account is in the appropriate state" timeCreated="2011-12-09T13:46:36.405+0100" lastModified="2017-01-17T19:34:25.903+0100" uniqueID="31274865-2171-4a28-bfe8-493fe2b7a02e" workPackage="" abstractionLevel="" id="UCAP4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:25.903+0100" uniqueID="37904bde-49cd-4d9c-afc6-3e085fa501fb" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:51:18.908+0100" uniqueID="3fdbedf3-45c9-481a-a843-5b780d27b116">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:51:18.911+0100" uniqueID="a1c7233b-bfc7-46d3-b080-18c0960fbd8b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Administration" timeCreated="2011-12-09T15:12:29.603+0100" lastModified="2017-01-17T19:34:27.479+0100" uniqueID="94a5742f-d802-4cdb-b53f-73dddb26a3b7">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.456+0100" uniqueID="fa035a1e-290d-49ed-9eb0-aa07fc36c8ee" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="folder:Folder" name="Setup Customization &amp; Maintenance" timeCreated="2011-12-07T15:51:56.881+0100" lastModified="2017-01-17T19:34:26.695+0100" uniqueID="233899c5-6bd4-42a6-9ee1-5ff8e9e3c48e">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.583+0100" uniqueID="301d5de0-8c22-4173-98e0-baaf6ad6451e" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="SCM1" name="SCM1" elementKind="" description="The LMS runs on &#xD;&#xA;&#x9;Windows NT/XP/7, Linux (Kernel since 2.2.1), and MacOS (since 9.5), &#xD;&#xA;&#x9;requires 1GB of disc space, plus ca.. 1MB of disc space per reader &amp; operating year, and&#xD;&#xA;&#x9;needs an uplink to the LMS maintenance site of 1GB/day per 1000 readers." timeCreated="2011-12-09T13:52:14.987+0100" lastModified="2017-01-17T19:34:26.599+0100" uniqueID="5db460bd-4b24-4069-82ce-07ee8db9d40a" workPackage="" abstractionLevel="" id="SCM1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.599+0100" uniqueID="8f4cbec8-6fcf-42be-a16d-4a481481948f" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:54:42.148+0100" uniqueID="4b3702ec-28e1-41fe-aa71-31e13eec9ff4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:54:42.150+0100" uniqueID="83516e8f-5acd-470f-b54d-cbf1f6f5fc58">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SCM2" name="SCM2" elementKind="" description="Librarians with basic computer skills shall be able to install the basic LMS within 2h." timeCreated="2011-12-09T13:54:15.274+0100" lastModified="2017-01-17T19:34:26.615+0100" uniqueID="3f818bb4-f6f4-446e-bf8e-82b9ccbb3dde" workPackage="" abstractionLevel="" id="SCM2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.614+0100" uniqueID="060cdbe3-456c-4084-8a0d-1c2998961a8c" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:54:38.920+0100" uniqueID="f57b766a-69c1-4c2e-b5c3-7b33a1eeee83">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:54:38.923+0100" uniqueID="762f0ac4-cb5a-4480-8dd9-99326f8d25af">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SCM3" name="SCM3" elementKind="" description="The system must be localizable by editing text or configuration files only." timeCreated="2011-12-09T13:54:55.839+0100" lastModified="2017-01-17T19:34:26.630+0100" uniqueID="76a549e5-a479-42c6-af24-13e78dbde5b8" workPackage="" abstractionLevel="" id="SCM3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.630+0100" uniqueID="4a67d713-e5dc-419f-9a35-07145d0aaf56" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:55:15.640+0100" uniqueID="2149eec6-5949-48de-a448-e827e144e4aa">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:55:15.643+0100" uniqueID="3f5e8e3c-b3c7-49b8-8630-3a62f8ecb224">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SCM4" name="SCM4" elementKind="" description="LMS requires a 5h/week system maintenance interval." timeCreated="2011-12-09T13:55:29.651+0100" lastModified="2017-01-17T19:34:26.646+0100" uniqueID="0c27ca46-afc5-49a8-827f-c915d20fa29a" workPackage="" abstractionLevel="" id="SCM4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.646+0100" uniqueID="7883db71-43ab-4325-a75e-2764d265d137" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:55:42.258+0100" uniqueID="b96f4e54-7e8d-4b9d-987c-2e7c24e8cf97">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:55:42.259+0100" uniqueID="cc576016-5d79-4a12-a4f3-acb7428bb112">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SCM5" name="SCM5" elementKind="" description="There shall be an easy to use facility for inspecting and changing the current profile of settings concerning:&#xD;&#xA;&#x9;the types of reader account and their capabilities;&#xD;&#xA;&#x9;message texts, channels, and triggers;&#xD;&#xA;&#x9;the triggers of notifications; and&#xD;&#xA;&#x9;process limits, fees, durations, and thresholds.&#xD;&#xA;&#x9;assignment of special responsibilities to selected librarian accounts&#xD;&#xA;&#x9;the number of reservations admissible for one reader" timeCreated="2011-12-09T13:56:00.377+0100" lastModified="2017-01-17T19:34:26.662+0100" uniqueID="3f022a32-610f-49a4-b5a0-3eab96805fe9" workPackage="" abstractionLevel="" id="SCM5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.662+0100" uniqueID="8f3877d6-c2a0-46ae-8ee4-76971d3a94a9" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:56:06.531+0100" uniqueID="44c75661-f040-497a-9fea-697961b62e95">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:56:06.534+0100" uniqueID="bcc933c7-ddc1-4f7f-a5b6-95f36d2cd64c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SCM6" name="SCM6" elementKind="" description="The process parameters valid at the time of instantiating a process instance are valid throughout the lifetime of the process instance." timeCreated="2011-12-09T13:56:39.338+0100" lastModified="2017-01-17T19:34:26.678+0100" uniqueID="a2bd6c70-0796-4a51-82a5-84eb8174d93b" workPackage="" abstractionLevel="" id="SCM6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.678+0100" uniqueID="f8127c8d-ac71-457a-9e60-c4d485db7ef8" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:57:49.971+0100" uniqueID="963e30e6-78f6-494c-9b9f-a88a3b4976a2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:57:49.974+0100" uniqueID="bfb8bc3a-b376-47f2-a486-b482ffd4b96d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SCM7" name="SCM7" elementKind="" description="LMS is only distributed with maintenance contracts and updates itself from the designated update server automatically." timeCreated="2011-12-09T13:58:15.610+0100" lastModified="2017-01-17T19:34:26.695+0100" uniqueID="d2a97477-b9c4-49a8-aae5-78f6786c3da5" workPackage="" abstractionLevel="" id="SCM7">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.694+0100" uniqueID="9aeb744e-51b2-4c5a-93d3-083332c309c5" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T13:58:40.422+0100" uniqueID="0b5a9995-57fe-458b-8758-bb5ac9427b1f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T13:58:40.425+0100" uniqueID="8066ac3f-82a9-46c1-acce-07da551e02e6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Librarian Accounts" timeCreated="2011-12-09T13:59:35.848+0100" lastModified="2017-01-17T19:34:26.899+0100" uniqueID="efde8b44-fe1b-4ba2-be65-64c42de5755e">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.805+0100" uniqueID="633dc23d-f137-4c53-b52b-2fd731d90def" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="LAC1" name="LAC1" elementKind="" description="Chief librarians can create, update, and delete librarian accounts." timeCreated="2011-12-09T14:00:00.757+0100" lastModified="2017-01-17T19:34:26.821+0100" uniqueID="a98ee222-9ada-4585-9f90-59807b907771" workPackage="" abstractionLevel="" id="LAC1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.821+0100" uniqueID="c781236e-c3a3-4d48-aa58-c7953b5d028f" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:00:17.925+0100" uniqueID="391ce9c1-23f0-41aa-baf2-3999b61e56e7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:00:17.928+0100" uniqueID="e6231678-6d42-4d3a-a934-0606004252d2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LAC2" name="LAC2" elementKind="" description="The capabilities of a librarian account can only be edited by a chief librarian." timeCreated="2011-12-09T14:04:40.883+0100" lastModified="2017-01-17T19:34:26.837+0100" uniqueID="a3f4c4a2-5738-4094-8db4-f5e74448d647" workPackage="" abstractionLevel="" id="LAC2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.836+0100" uniqueID="593a1b20-b228-4907-8626-34f1c90775ff" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:04:58.828+0100" uniqueID="529b04ce-6e33-47a5-9858-a2468d423e2c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:04:58.829+0100" uniqueID="2ed8c7d0-f700-4be3-b0b3-b7e25d051009">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LAC3" name="LAC3" elementKind="" description="Chief librarian accounts cannot be created by the LMS." timeCreated="2011-12-09T14:05:16.743+0100" lastModified="2017-01-17T19:34:26.852+0100" uniqueID="b02c5f31-1cd0-4156-9912-2e2bb910282a" workPackage="" abstractionLevel="" id="LAC3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.852+0100" uniqueID="0ec942bc-c99c-4cd3-a545-d4fb5a9c8821" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:05:37.932+0100" uniqueID="11ce28c7-e9ca-4add-98a1-f62e8bb4a180">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:05:37.935+0100" uniqueID="696b32a3-8d16-4357-ba3c-3c547903087e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LAC4" name="LAC4" elementKind="" description="Activating a Chief Librarian account requires a restart of the LMS." timeCreated="2011-12-09T14:07:00.513+0100" lastModified="2017-01-17T19:34:26.868+0100" uniqueID="03ad5347-7f70-4a3a-a9ae-7d7a0796f51b" workPackage="" abstractionLevel="" id="LAC4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.867+0100" uniqueID="fe26a04a-a542-494c-8891-72dc06cefbd2" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:07:14.753+0100" uniqueID="8a0964e7-295e-4c1e-83ee-97909be330aa">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:07:14.756+0100" uniqueID="33f17726-c92d-4f5b-a72c-5c097218fea0">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LAC5" name="LAC5" elementKind="" description="On starting the LMS, all chief librarians are informed about all other activated chief librarian accounts." timeCreated="2011-12-09T14:07:32.919+0100" lastModified="2017-01-17T19:34:26.884+0100" uniqueID="6ba3d7c0-c3fc-43c2-b47a-ab709a4271ec" workPackage="" abstractionLevel="" id="LAC5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.883+0100" uniqueID="9f3424b2-5eb0-4438-a812-c62ca53a4e02" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:07:47.026+0100" uniqueID="50e6404c-b712-4ce3-a44a-f82921be6903">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:07:47.029+0100" uniqueID="53103199-a47a-463f-b4bd-ddf15a48213c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="LAC6" name="LAC6" elementKind="" description="Chief librarians can edit the profile details of their own accounts." timeCreated="2011-12-09T14:08:04.114+0100" lastModified="2017-01-17T19:34:26.899+0100" uniqueID="15e307e1-65a0-4f70-9283-6b8222fab4f4" workPackage="" abstractionLevel="" id="LAC6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:26.899+0100" uniqueID="91cc1f40-99c9-4b86-be6e-d050bec9e379" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:08:09.894+0100" uniqueID="8c76c67d-8c1a-4c75-9d85-7157dc336ccb">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:08:09.896+0100" uniqueID="b0448ad3-a8c6-41b8-ae97-c31351a81055">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Software Operation" timeCreated="2011-12-09T14:08:54.602+0100" lastModified="2017-01-17T19:34:27.265+0100" uniqueID="94aebeba-73e7-4138-93c0-79a5998bc63b">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.087+0100" uniqueID="1d402ba7-7ad8-4b87-90aa-b5ba6fe58c96" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="SOP1" name="SOP1" elementKind="" description="LMS requires a 2h/day system administration interval." timeCreated="2011-12-09T14:09:21.058+0100" lastModified="2017-01-17T19:34:27.103+0100" uniqueID="dc190a43-9286-47bf-b9c2-4cf094448312" workPackage="" abstractionLevel="" id="SOP1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.102+0100" uniqueID="38c03b8c-fc3e-4796-a191-73d47a32155b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:09:45.628+0100" uniqueID="678177b1-2ad5-43e0-8c71-f7e7c1782264">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:09:45.629+0100" uniqueID="097cfc9a-440c-443e-bb97-5fdbb7502506">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP2" name="SOP2" elementKind="" description="LMS can be started from any system state within 10 minutes." timeCreated="2011-12-09T14:09:58.747+0100" lastModified="2017-01-17T19:34:27.119+0100" uniqueID="0a704fb8-e41e-4bdb-9ef2-fef76b9f7db4" workPackage="" abstractionLevel="" id="SOP2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.118+0100" uniqueID="714dc504-5e22-4846-b736-8a0007fbce8d" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:10:13.288+0100" uniqueID="67d12e3b-31f7-4e5e-b9d7-6f055a36958f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:10:13.291+0100" uniqueID="d9c5df41-5d63-42c7-8901-ff43542f95af">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP3" name="SOP3" elementKind="" description="LMS includes a failsafe backup of reader and account data." timeCreated="2011-12-09T14:10:32.420+0100" lastModified="2017-01-17T19:34:27.135+0100" uniqueID="c0c9d353-9d56-4b9a-9520-aec2d333558d" workPackage="" abstractionLevel="" id="SOP3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.134+0100" uniqueID="8e86884e-ee2f-4b51-b8b2-71c415d63938" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:10:45.195+0100" uniqueID="9f546589-f33a-4620-a6d0-488109b91329">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:10:45.198+0100" uniqueID="bae48bb2-49bd-4c34-8369-4bdb8ea41764">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP4" name="SOP4" elementKind="" description="The LMS provides an easy to use operation console to manage the operation of LMS." timeCreated="2011-12-09T14:11:02.013+0100" lastModified="2017-01-17T19:34:27.151+0100" uniqueID="7623867e-0034-43e2-843b-d54a1780ad1d" workPackage="" abstractionLevel="" id="SOP4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.150+0100" uniqueID="8a5becbd-b382-427c-8e1f-06235443cdb4" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:11:14.040+0100" uniqueID="30c79ac2-876b-4ce3-bb0b-855ce0b6b620">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:11:14.043+0100" uniqueID="6cc5efff-8d23-4825-b562-3c331400507a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP4a" name="SOP4a" elementKind="" description="The operation console shall provide a smartphone-interface so that the system can easily be monitored outside the regular library hours." timeCreated="2011-12-09T14:11:29.113+0100" lastModified="2017-01-17T19:34:27.167+0100" uniqueID="d8fbbc0a-4fdf-45e2-8ebe-036084ec252d" workPackage="" abstractionLevel="" id="SOP4a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.166+0100" uniqueID="6960d8de-f993-4669-bd44-4869ea736b53" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:11:52.979+0100" uniqueID="fa1a4d2a-7300-4a85-b34b-0fb259b71e85">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:11:52.983+0100" uniqueID="6c46cfc3-229e-4e7e-9fa7-ead882db80ea">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP4b" name="SOP4b" elementKind="" description="All chief librarians have access to the operation console; access rights may be extended to ordinary librarians." timeCreated="2011-12-09T14:12:13.786+0100" lastModified="2017-01-17T19:34:27.185+0100" uniqueID="9f4fc9fd-a29d-4543-9b85-169e403e06c2" workPackage="" abstractionLevel="" id="SOP4b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.184+0100" uniqueID="96cc53ca-5618-45dc-86e7-ce05df2639b4" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:14:40.327+0100" uniqueID="74ac40f6-0e87-4739-a471-7ba5b16372be">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:14:40.329+0100" uniqueID="b46c0073-f272-4765-8c23-df86d44ebb39">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP4c" name="SOP4c" elementKind="" description="The operation console allows to monitor and set the operation parameters." timeCreated="2011-12-09T14:13:17.229+0100" lastModified="2017-01-17T19:34:27.201+0100" uniqueID="6262dd2b-6d24-4448-a90f-ef27684410dd" workPackage="" abstractionLevel="" id="SOP4c">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.200+0100" uniqueID="f32be493-0511-422f-b9c2-2a6c1f9b731c" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:13:31.699+0100" uniqueID="ec44cf56-e613-43a9-b851-3b4d4d101689">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:13:31.702+0100" uniqueID="29b91260-0234-4663-aca0-3eb26d9a50e5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP4d" name="SOP4d" elementKind="" description="The operation console allows to start, restart, shutdown, and update the LMS and its parts." timeCreated="2011-12-09T14:13:54.378+0100" lastModified="2017-01-17T19:34:27.217+0100" uniqueID="c28f72d2-7485-40f6-a2d4-18800bb43504" workPackage="" abstractionLevel="" id="SOP4d">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.216+0100" uniqueID="d518e259-b014-4e4f-9c90-7e39e0fc5872" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:14:27.276+0100" uniqueID="00addfdd-0d0c-4934-bc48-97e05dd62c43">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:14:27.280+0100" uniqueID="ba5edebe-17de-4944-b0bf-16dc3a5f403f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP4e" name="SOP4e" elementKind="" description="The operation console allows to start, stop, and monitor the LMS backup process, the backup status, and the remote backup storage memory usage." timeCreated="2011-12-09T14:15:45.739+0100" lastModified="2017-01-17T19:34:27.233+0100" uniqueID="c02c8d77-7891-4811-9c15-42afa896737e" workPackage="" abstractionLevel="" id="SOP4e">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.232+0100" uniqueID="48ab5909-7556-417d-8ba2-66a4318f5dc3" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:16:09.729+0100" uniqueID="7b755800-c18a-4092-a1e6-2cd2045bc50c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:16:09.730+0100" uniqueID="9d9857d6-f3b8-45d0-8ab9-66c1fd4f9431">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP4f" name="SOP4f" elementKind="" description="There shall be a facility to edit all process instances manually for error recovery." timeCreated="2011-12-09T14:16:23.681+0100" lastModified="2017-01-17T19:34:27.248+0100" uniqueID="0eb4d2dd-13aa-40df-a367-96e62838772a" workPackage="" abstractionLevel="" id="SOP4f">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.248+0100" uniqueID="8c6f477b-a966-40ae-b4f0-8a7c2e16d46b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:16:40.035+0100" uniqueID="2aa89987-b313-4e5c-830c-c43c18efc9e8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:16:40.038+0100" uniqueID="7668e786-3b99-40a3-b62c-3c17c54c0cb7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="SOP5" name="SOP5" elementKind="" description="There shall be a facility to inspect and change the current remote backup status, available opt-ions, and corresponding settings." timeCreated="2011-12-09T14:16:54.007+0100" lastModified="2017-01-17T19:34:27.265+0100" uniqueID="2478e3b9-c0b5-4246-a5ae-0517f0416f6b" workPackage="" abstractionLevel="" id="SOP5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.264+0100" uniqueID="958b30a0-b15d-4f62-8c63-3eea6c8825df" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:17:02.581+0100" uniqueID="15ea253e-816d-4acf-a3a7-76d14aea6fae">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:17:02.582+0100" uniqueID="e618f1d0-5706-4fc8-93dd-7f2875b7a373">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Monitoring &amp; Reporting" timeCreated="2011-12-09T14:18:19.856+0100" lastModified="2017-01-17T19:34:27.479+0100" uniqueID="5853043c-0179-452c-ad95-1db847a77cb9">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.376+0100" uniqueID="5b09cf1d-d749-46d1-91d8-edb60db95153" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="MOR1" name="MOR1" elementKind="" description="All messages concerning the operation of LMS are sent to the operation console." timeCreated="2011-12-09T14:18:34.022+0100" lastModified="2017-01-17T19:34:27.393+0100" uniqueID="0c91ae50-353c-4530-8e1e-8ca6ac0248d0" workPackage="" abstractionLevel="" id="MOR1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.392+0100" uniqueID="11e1ab9b-6b80-4cbf-894a-f942e3ca51f3" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:19:43.992+0100" uniqueID="01605b5e-91d7-45d3-9111-3828744cc0a8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:19:43.996+0100" uniqueID="5adb68e9-5211-43f9-98f7-fa3193fd510c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MOR2" name="MOR2" elementKind="" description="The LMS may be set up in such a way that all errors, warnings, and status reports triggered during operation are forwarded to the manufacturer." timeCreated="2011-12-09T14:20:03.172+0100" lastModified="2017-01-17T19:34:27.415+0100" uniqueID="716fcc4d-b7b0-4f4f-a3b5-59438b705716" workPackage="" abstractionLevel="" id="MOR2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.415+0100" uniqueID="4a1cf3e0-b823-4baf-bbec-b12343cd6d01" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:20:38.024+0100" uniqueID="82fd2092-59c9-4f12-a175-a7bf1fde420d">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:20:38.025+0100" uniqueID="ccaeeb61-bc0d-4445-a6d2-5c8ee543ceda">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MOR3" name="MOR3" elementKind="" description="There shall be a query interface that allows ad-hoc queries of the operative data." timeCreated="2011-12-09T14:21:00.391+0100" lastModified="2017-01-17T19:34:27.432+0100" uniqueID="f78a58c5-8b4f-4e26-96e9-b67310400791" workPackage="" abstractionLevel="" id="MOR3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.431+0100" uniqueID="29863aa0-bce4-48ea-b2af-20c4ab3850eb" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:21:06.021+0100" uniqueID="a7b67b1e-db15-41f0-849c-eb4b964a3b81">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:21:06.022+0100" uniqueID="50a08552-bfa2-49c3-a35f-ec53e30b768b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MOR4" name="MOR4" elementKind="" description="LMS shall be able to generate basic reports on usage and financial status of the LMS, the status of the corpus and the reader base, and the usage of online services." timeCreated="2011-12-09T14:21:23.375+0100" lastModified="2017-01-17T19:34:27.447+0100" uniqueID="cc8c344e-6b72-483e-b9e6-91d315a8e40b" workPackage="" abstractionLevel="" id="MOR4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.447+0100" uniqueID="02963a45-8dbe-41c3-8a6d-748d8e8444cb" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:21:45.497+0100" uniqueID="53a1d44f-5eb6-4d3d-b64f-717d489b4561">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:21:45.498+0100" uniqueID="9eb15cfb-e7ab-48b1-9daf-6c18686dee78">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MOR5" name="MOR5" elementKind="" description="LMS shall be able to generate advanced reports for the library management." timeCreated="2011-12-09T14:22:29.718+0100" lastModified="2017-01-17T19:34:27.463+0100" uniqueID="554b2e64-2cbb-435c-8da7-23cdec8b54fb" workPackage="" abstractionLevel="" id="MOR5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.463+0100" uniqueID="fe62a836-817e-4b4f-a757-19e4478ffe4c" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:22:40.492+0100" uniqueID="ab05e3b8-aced-40f9-90ae-b0248afa5d0c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:22:40.496+0100" uniqueID="c751d2ca-254b-4100-8686-4750bc6cdcb9">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="MOR6" name="MOR6" elementKind="" description="All query and report outputs can be formatted suitable for screen and print output." timeCreated="2011-12-09T14:23:02.310+0100" lastModified="2017-01-17T19:34:27.479+0100" uniqueID="32e42f00-a1d7-4fe5-a3d1-b80a83d62f28" workPackage="" abstractionLevel="" id="MOR6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:27.479+0100" uniqueID="9c11cd20-eede-40b1-9287-580ba27986ef" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:23:18.053+0100" uniqueID="973f3a80-ddd9-4692-b307-8376c4e3cd1a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:23:18.054+0100" uniqueID="87a83fe7-a79a-4566-ab9e-d6ca786944d2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Crosscutting Concerns" timeCreated="2011-12-09T15:07:28.523+0100" lastModified="2017-01-17T19:34:29.165+0100" uniqueID="7ffbc6df-2131-4ad7-bfa8-86fdb239e7e6">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.031+0100" uniqueID="ba0031f2-6f95-4599-9dba-ed2470381ab7" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="folder:Folder" name="User Interaction" timeCreated="2011-12-09T14:28:09.693+0100" lastModified="2017-01-17T19:34:28.465+0100" uniqueID="72938236-5e62-46d8-abdc-cf5244d9d698">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.255+0100" uniqueID="684b3f15-aa22-491c-b978-b31b73434be6" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="UI1" name="UI1" elementKind="" description="The LMS shall provide a graphical user interface on the terminals available in the library." timeCreated="2011-12-09T14:28:35.293+0100" lastModified="2017-01-17T19:34:28.272+0100" uniqueID="c5fc9a0f-6010-4fa9-9f3e-ccfe9f11c63c" workPackage="" abstractionLevel="" id="UI1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.271+0100" uniqueID="ea9e9a57-84d4-488e-a6b3-58354c7786f3" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:28:51.793+0100" uniqueID="a748f303-467e-4c0b-bfa2-430b1aeb8452">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:28:51.796+0100" uniqueID="cf1d2b38-a1e5-4970-9ed1-61fa041f4da6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI1a" name="UI1a" elementKind="" description="The LMS GUI shall be easy to learn such that 95% of new readers are able to perform reader actions, catalog actions, and message reading with no assistance at once." timeCreated="2011-12-09T14:29:12.810+0100" lastModified="2017-01-17T19:34:28.288+0100" uniqueID="0badf31e-1eef-4d75-a030-e9e6ad62b818" workPackage="" abstractionLevel="" id="UI1a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.288+0100" uniqueID="c2494d3e-9ca1-41f6-b9b5-133bbbc018cf" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:29:20.704+0100" uniqueID="96fde674-279e-4883-99bd-48915938f861">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:29:20.706+0100" uniqueID="893671b3-5266-41a0-b19d-d30fe9cc611c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI1b" name="UI1b" elementKind="" description="All functions of the LMS-GUI shall be accessible by keyboard." timeCreated="2011-12-09T14:29:30.625+0100" lastModified="2017-01-17T19:34:28.305+0100" uniqueID="87dc60e4-58b6-4054-a47d-45ad237a875c" workPackage="" abstractionLevel="" id="UI1b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.304+0100" uniqueID="2e24f213-094e-4c21-8eb4-c7c17350b9fb" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:29:38.555+0100" uniqueID="e4596b9a-cd02-427d-9997-66248543ac67">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:29:38.556+0100" uniqueID="0ef68fbc-e1fe-49dc-bdb0-6b2a2cc1148c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI2" name="UI2" elementKind="" description="Multiple channels shall be available for the services described in domains “Leases and Reservations”." timeCreated="2011-12-09T14:31:26.508+0100" lastModified="2017-01-17T19:34:28.321+0100" uniqueID="84adc66f-e26b-424a-ad6d-595a597aa81a" workPackage="" abstractionLevel="" id="UI2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.320+0100" uniqueID="ca025fbe-d5f1-4faf-a3cc-9bc1a337ad94" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:31:40.672+0100" uniqueID="720bcfc3-83ea-4b68-8a18-af313618b574">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:31:40.673+0100" uniqueID="2e919fee-29de-40f9-94e5-e834cfd8950e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI2a" name="UI2a" elementKind="" description="A reader may have lending actions performed for him by a librarian at the front desk." timeCreated="2011-12-09T14:32:03.225+0100" lastModified="2017-01-17T19:34:28.336+0100" uniqueID="ffc891ba-881f-41c4-9ba0-6b329bc45395" workPackage="" abstractionLevel="" id="UI2a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.336+0100" uniqueID="4f2bc748-0a85-46c5-96a6-5e7281716e2a" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:32:14.069+0100" uniqueID="d0f95de8-2619-4aba-bc52-81914e5f488b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:32:14.073+0100" uniqueID="56272a25-77b5-4e4d-adb8-20c5da68a7f4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI2b" name="UI2b" elementKind="" description="A reader may perform reader actions at self-service terminals in the library." timeCreated="2011-12-09T14:32:37.287+0100" lastModified="2017-01-17T19:34:28.354+0100" uniqueID="04beeff2-3e23-4e0a-b1bb-0957c1f83a15" workPackage="" abstractionLevel="" id="UI2b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.354+0100" uniqueID="ae58093d-ef19-4d51-aba2-7e5bfb265a1a" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:32:44.442+0100" uniqueID="9a78778c-3798-4663-93f0-0552960568de">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:32:44.445+0100" uniqueID="6364a4c8-ad3d-4b4e-9d39-73251c23a935">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI2c" name="UI2c" elementKind="" description="A reader may perform reader actions online." timeCreated="2011-12-09T14:33:29.617+0100" lastModified="2017-01-17T19:34:28.370+0100" uniqueID="9d7eebfe-bfdd-496a-9f71-ed259e0fc39c" workPackage="" abstractionLevel="" id="UI2c">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.370+0100" uniqueID="ec56dcbf-454f-4eed-b2a9-510682aca97c" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:33:37.321+0100" uniqueID="7941c59a-6d05-42d7-87a1-65b273e34147">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:33:37.322+0100" uniqueID="c0b62f98-f13a-4fd7-b517-00b43f48a7c5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI2d" name="UI2d" elementKind="" description="A reader may perform certain reader actions by interacting with the BookStationTM&#xD;&#xA;media dispenser robot.&#xD;&#xA;" timeCreated="2011-12-09T14:33:59.549+0100" lastModified="2017-01-17T19:34:28.386+0100" uniqueID="2ab34767-e3b3-4ec4-b853-754ac600cd4f" workPackage="" abstractionLevel="" id="UI2d">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.386+0100" uniqueID="0843708d-3ceb-4701-a27a-82bdfdc551c8" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:34:10.764+0100" uniqueID="55419fe1-7bf4-4be7-8fc9-a516ca517cf0">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:34:10.765+0100" uniqueID="b97411dd-4751-42dd-b648-31f53648d3d7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI3" name="UI3" elementKind="" description="95% of all readers shall be able to complete the terminal usability test suite in less than 3min." timeCreated="2011-12-09T14:34:32.947+0100" lastModified="2017-01-17T19:34:28.402+0100" uniqueID="6f89998d-fd50-4225-94ec-ec40d2d3cc5a" workPackage="" abstractionLevel="" id="UI3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.401+0100" uniqueID="c7ddcfff-eff9-4f13-9d4e-b1c046d6971f" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:34:52.251+0100" uniqueID="a60556eb-d53a-4d6f-8a0f-2e14858c88ea">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:34:52.255+0100" uniqueID="17706904-7975-4bda-9950-e75971739f57">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI4" name="UI4" elementKind="" description="Terminals shall be equipped with a scanning device to identify readers by their card and copies by their id badge." timeCreated="2011-12-09T14:35:14.867+0100" lastModified="2017-01-17T19:34:28.417+0100" uniqueID="45e7a1ce-f66b-4290-b917-173c1a8aceda" workPackage="" abstractionLevel="" id="UI4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.417+0100" uniqueID="4958dadd-946f-4fa3-8695-7368d948c766" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:35:32.554+0100" uniqueID="1c5d73b6-6b5a-4513-be46-ba14d1446955">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:35:32.558+0100" uniqueID="2fc47591-03a5-4a73-8f23-5396ae5aa057">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI4a" name="UI4a" elementKind="" description="The scanning device shall provide multi-modal feedback on the result of the scanning action." timeCreated="2011-12-09T14:36:20.036+0100" lastModified="2017-01-17T19:34:28.433+0100" uniqueID="07e3e4ea-6a04-4167-802a-33a368a278d3" workPackage="" abstractionLevel="" id="UI4a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.433+0100" uniqueID="90c155d3-9eb2-44f9-b5fe-6e31e885fc19" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:36:39.538+0100" uniqueID="ec9a7825-49b1-4f44-b50b-22e5010bdefa">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:36:39.541+0100" uniqueID="932b5ebe-285f-409b-96de-db933d53a7d4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI5" name="UI5" elementKind="" description="When a reader is identified by the LMS, relevant reader and account information will be displayed at the terminal." timeCreated="2011-12-09T14:37:19.215+0100" lastModified="2017-01-17T19:34:28.449+0100" uniqueID="612a209a-dba3-4091-aeb3-c74a2c3b55f7" workPackage="" abstractionLevel="" id="UI5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.448+0100" uniqueID="87634273-04c3-4f3c-99d1-478643e5f9e3" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:37:35.617+0100" uniqueID="31b1fa73-e140-4a76-a18e-4c4eb44d13d8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:37:35.618+0100" uniqueID="4e90c494-af15-45ce-aa42-8e9500eb9e98">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="UI6" name="UI6" elementKind="" description="The reader shall never be offered options or actions that cannot take place." timeCreated="2011-12-09T14:38:40.984+0100" lastModified="2017-01-17T19:34:28.465+0100" uniqueID="b5cf76e5-bf84-4942-9136-db685e6d2d48" workPackage="" abstractionLevel="" id="UI6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.465+0100" uniqueID="52771fa8-e391-4050-ace7-ec099996f66d" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:39:09.599+0100" uniqueID="b7f0a480-0c16-432d-aa22-d80746bf5d6c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:39:09.603+0100" uniqueID="4825a28b-bd33-41b1-ae59-9f77b403a979">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Web Access" timeCreated="2011-12-09T14:40:01.248+0100" lastModified="2017-01-17T19:34:28.856+0100" uniqueID="81361aa3-1d35-45ed-b842-8435b7afed7c">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.626+0100" uniqueID="6255c880-ebec-41ae-9c68-7c3be25c142f" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="WEB1" name="WEB1" elementKind="" description="" timeCreated="2011-12-09T14:40:46.812+0100" lastModified="2017-01-17T19:34:28.642+0100" uniqueID="feffe9e5-756a-470b-9aad-887ca941e7e3" workPackage="" abstractionLevel="" id="WEB1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.642+0100" uniqueID="bb620479-204c-46ea-b2d7-c8bdfa114c4e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:40:57.068+0100" uniqueID="724cd4d0-bba7-4e34-9805-7ecc415792f6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:40:57.070+0100" uniqueID="8833fde8-fd0b-42af-b0fd-c0527c3daa92">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="WEB1a" name="WEB1a" elementKind="" description="Initial passwords can only be used to log to immediately change the password." timeCreated="2011-12-09T14:48:50.962+0100" lastModified="2017-01-17T19:34:28.658+0100" uniqueID="d46a05ba-7304-4268-8455-8814a9a8b29a" workPackage="" abstractionLevel="" id="WEB1a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.658+0100" uniqueID="11b66bb6-2b74-4295-ac50-91830a061fc0" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:49:22.719+0100" uniqueID="c87362a8-7b5d-46cf-b372-9af80895881f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:49:22.722+0100" uniqueID="3ccd796f-ca5a-4f9b-8bbb-2b09a649e557">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="WEB1b" name="WEB1b" elementKind="" description="All passwords must be strong." timeCreated="2011-12-09T14:49:08.836+0100" lastModified="2017-01-17T19:34:28.675+0100" uniqueID="490cf373-1c90-47cf-b26f-7bc094bd95ce" workPackage="" abstractionLevel="" id="WEB1b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.674+0100" uniqueID="af2a66af-618b-4e3d-84df-20e704e3e01e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:49:20.245+0100" uniqueID="21858621-ef10-4534-8529-fe451a093ed0">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:49:20.249+0100" uniqueID="141a17b2-4039-446a-b3fb-e37c49f02495">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="WEB1c" name="WEB1c" elementKind="" description="When a user enters the login/password in to the LMS via the web, a one-time-tag is sent to the user’s mobile phone within 10s and must be entered within 60s to complete the log in." timeCreated="2011-12-09T14:49:41.835+0100" lastModified="2017-01-17T19:34:28.691+0100" uniqueID="de840cb1-2f52-425c-90f9-b2c906407854" workPackage="" abstractionLevel="" id="WEB1c">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.690+0100" uniqueID="abeacd03-2740-44a6-b0d9-11374f6878de" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:49:50.678+0100" uniqueID="5b83c3fa-c739-4e23-9d8c-4e0720d304d4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:49:50.682+0100" uniqueID="3a61f352-4225-49c8-b0c5-5befab2f2c04">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="WEB2" name="WEB2" elementKind="" description="All connections via the internet shall be secure." timeCreated="2011-12-09T14:50:07.949+0100" lastModified="2017-01-17T19:34:28.707+0100" uniqueID="cce6d3fd-03e3-4a13-8ab1-0df7e8a8749e" workPackage="" abstractionLevel="" id="WEB2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.707+0100" uniqueID="242986c7-7768-42b5-af1a-3aba2772143a" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:50:25.353+0100" uniqueID="87d0accb-b68b-4c51-a9f4-de52c898deb7">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:50:25.354+0100" uniqueID="3190570a-54e7-4a20-bf99-573b5f941c25">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="WEB3" name="WEB3" elementKind="" description="The web services shall be easy to learn and easy to use." timeCreated="2011-12-09T14:50:34.963+0100" lastModified="2017-01-17T19:34:28.724+0100" uniqueID="3642ca1c-5762-40e7-bb11-642055a99403" workPackage="" abstractionLevel="" id="WEB3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.723+0100" uniqueID="a1fb0545-6674-4eb5-b84a-ce2e7db32ebc" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:50:47.467+0100" uniqueID="4f549294-fad9-4c87-8a1f-94f791b62a52">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:50:47.470+0100" uniqueID="9df906cd-6a28-4b33-9fb5-f8e2e445f0b2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="WEB3a" name="WEB3a" elementKind="" description="The web service front end shall be accessible." timeCreated="2011-12-09T14:51:11.910+0100" lastModified="2017-01-17T19:34:28.740+0100" uniqueID="1de1e4f8-5fe3-4cc0-8e59-1b0bd112c124" workPackage="" abstractionLevel="" id="WEB3a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.739+0100" uniqueID="f840c1cc-7df1-4f34-93d5-bef881f0ab37" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:51:26.829+0100" uniqueID="2566f29a-0e25-42ab-af90-9e2f02adbd01">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:51:26.830+0100" uniqueID="75206a23-904a-45c6-9ed7-8d440b2f32b5">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="WEB3b" name="WEB3b" elementKind="" description="95% of all readers signed up to AMDS without prior experience using the LMS terminal services shall be able to complete the online usability test suite in less than 5min. when using a desktop/laptop with a DSL connection, and 10min. when using a mobile device (smart phone, tablet computer, netbook with GSM modem)." timeCreated="2011-12-09T14:51:43.098+0100" lastModified="2017-01-17T19:34:28.756+0100" uniqueID="2cdf4bd5-27f6-4d4e-a6c0-71b53c580851" workPackage="" abstractionLevel="" id="WEB3b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.755+0100" uniqueID="4c454c3a-5f1d-474c-b0bd-75daf5a02de8" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:51:55.059+0100" uniqueID="98658fe0-9446-4476-be3e-e255377fd073">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:51:55.062+0100" uniqueID="4ac0210a-b688-4466-aa51-53331f6eaba6">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="WEB4" name="WEB4" elementKind="" description="The web service front end shall run in those web browsers that cover 85% if the browser market." timeCreated="2011-12-09T14:52:17.315+0100" lastModified="2017-01-17T19:34:28.856+0100" uniqueID="165b6dcd-447f-48a0-bdf1-f75b434e4545" workPackage="" abstractionLevel="" id="WEB4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:28.856+0100" uniqueID="8ce08468-52d1-404c-8a41-634949b54e99" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:52:25.876+0100" uniqueID="7355decb-b2c8-4e09-9eeb-8334178dc513">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:52:25.880+0100" uniqueID="720da462-02d1-40a0-a593-8855b62a5ab8">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="Notifications" timeCreated="2011-12-09T14:52:53.363+0100" lastModified="2017-01-17T19:34:29.165+0100" uniqueID="62534c39-9581-4037-a99c-099773edc1ed">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.014+0100" uniqueID="09e08675-e15f-44ca-9ab2-73b28b7b6d5f" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="NAT1" name="NAT1" elementKind="" description="There are different classes of messages with different characteristics and purposes:&#xD;&#xA;G – General: purely informative information (e.g. changes in opening hours or accession suggestions, newsletters, events)&#xD;&#xA;L –  Leases: shortly expiring or recently expired reservations and leases, availability of reserved media, due fees etc.&#xD;&#xA;A –  Account: re-/reactivation of an account, major changes in account status and capabilities, payment receipts;&#xD;&#xA;S –  Safety: login data/initial password, reader card, etc.&#xD;&#xA;T – Tags: one-time tags for sensitive online services" timeCreated="2011-12-09T14:53:06.677+0100" lastModified="2017-01-17T19:34:29.031+0100" uniqueID="7a77fe02-b1e5-4967-8dc0-e3e63f5c077a" workPackage="" abstractionLevel="" id="NAT1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.031+0100" uniqueID="38ee4d5f-0ffd-4eaa-8dd9-0f46688de43b" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:53:18.605+0100" uniqueID="97b925fe-85c4-4ca7-a347-dd9d6b222846">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:53:18.608+0100" uniqueID="26789a86-9a9d-4876-92ac-f3911040afe0">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NAT2" name="NAT2" elementKind="" description="Users may chose among text to mobile, email, or paper mail to be used for receiving messages from LMS according to the following table." timeCreated="2011-12-09T14:54:02.356+0100" lastModified="2017-01-17T19:34:29.048+0100" uniqueID="61f453ca-5827-4c00-9f91-2c745be224dc" workPackage="" abstractionLevel="" id="NAT2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.047+0100" uniqueID="09395da0-2b6c-470d-a112-83600368341d" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:54:19.867+0100" uniqueID="47c180cc-fd51-457b-a482-2f4c707183db">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:54:19.868+0100" uniqueID="cb73d6ed-dda8-4b99-8b47-663e4bec224f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NAT3" name="NAT3" elementKind="" description="Readers shall only be notified with meaningful and compact messages." timeCreated="2011-12-09T14:54:37.808+0100" lastModified="2017-01-17T19:34:29.065+0100" uniqueID="da88acb9-63a0-4872-af8e-965d2a0afdf1" workPackage="" abstractionLevel="" id="NAT3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.064+0100" uniqueID="0685f417-2717-4bae-adf8-9bb012df7637" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:54:55.112+0100" uniqueID="7fd193f3-8c7b-49bc-9321-227271f34c27">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:54:55.115+0100" uniqueID="d33e87ad-4646-472a-903e-6d583c63276a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NAT3a" name="NAT3a" elementKind="" description="G-type messages are only sent outside of the LMS when the user explicitly signed up for them." timeCreated="2011-12-09T14:55:06.500+0100" lastModified="2017-01-17T19:34:29.081+0100" uniqueID="3a96b24a-0b9d-4fde-b667-5beadc380902" workPackage="" abstractionLevel="" id="NAT3a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.080+0100" uniqueID="8e511922-0184-4916-9034-78e657155b30" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:55:15.046+0100" uniqueID="0cb77939-576c-4873-a1ae-dffb8f4b995b">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:55:15.047+0100" uniqueID="dd7f2571-8c6f-4ef1-8b52-a10d7639b0d2">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NAT3b" name="NAT3b" elementKind="" description="A message is sent at most once per channel." timeCreated="2011-12-09T14:55:37.689+0100" lastModified="2017-01-17T19:34:29.097+0100" uniqueID="3a2748d8-956c-47cc-a5a4-42e2c0648065" workPackage="" abstractionLevel="" id="NAT3b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.097+0100" uniqueID="b70b25a0-f258-4d2f-b365-6926c26811f6" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:55:43.140+0100" uniqueID="2ca8d458-570f-4d03-b025-11596060bd3c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:55:43.143+0100" uniqueID="cc9c1476-7c7c-4a34-9cf6-58e23a788330">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NAT3c" name="NAT3c" elementKind="" description="Sets of messages with the same type of event shall be bundled into a single message." timeCreated="2011-12-09T14:56:00.848+0100" lastModified="2017-01-17T19:34:29.113+0100" uniqueID="f6cd88ed-d088-4abb-98f1-661f0f34b5fa" workPackage="" abstractionLevel="" id="NAT3c">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.113+0100" uniqueID="d5eb9928-accb-4f40-8160-4c0aa111a5ab" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:56:20.923+0100" uniqueID="291d778f-d938-4be9-a8fd-f0a4bd1033ac">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:56:20.927+0100" uniqueID="10bc3e94-be6a-4539-b18a-7c84fb589cec">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NAT4" name="NAT4" elementKind="" description="Messages other than of type T are sent at most once a day per account." timeCreated="2011-12-09T14:56:32.058+0100" lastModified="2017-01-17T19:34:29.130+0100" uniqueID="557a597e-14d2-4cbc-8163-57d3d424b1db" workPackage="" abstractionLevel="" id="NAT4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.129+0100" uniqueID="5bf2e1e1-2948-44c1-9935-8565679a378a" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:56:48.184+0100" uniqueID="a2be021e-d019-4288-ac60-a3073843e117">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:56:48.188+0100" uniqueID="f59b4156-323b-4b27-ac98-099056ac1487">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NAT5" name="NAT5" elementKind="" description="Messages shall be sent to their respective recipient within 12h of triggering them" timeCreated="2011-12-09T14:57:08.453+0100" lastModified="2017-01-17T19:34:29.149+0100" uniqueID="73a6e281-6727-4a25-b837-0e879470fe67" workPackage="" abstractionLevel="" id="NAT5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.148+0100" uniqueID="525049dd-19b6-4f5b-b576-2594b6f2075e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:57:20.402+0100" uniqueID="f9eb4e9a-cdc3-485c-aeba-855230ea3561">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:57:20.405+0100" uniqueID="a3771aef-85d5-4766-961c-26311a40569c">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="NAT6" name="NAT6" elementKind="" description="Chief librarians are notified about all starts of the system and significant events during startup." timeCreated="2011-12-09T14:57:30.948+0100" lastModified="2017-01-17T19:34:29.165+0100" uniqueID="77300c0d-cbb6-46e3-8cce-e604d2c0380f" workPackage="" abstractionLevel="" id="NAT6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.165+0100" uniqueID="4ab6de06-9e3d-43a4-9767-1ca7b3a6ebc2" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:57:43.223+0100" uniqueID="ba762469-1f6e-4a43-a785-db63ff21660a">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:57:43.227+0100" uniqueID="6fc63452-c847-43b8-97f3-382c65c7871f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Third Party Systems" timeCreated="2011-12-09T15:06:22.516+0100" lastModified="2017-01-17T19:34:29.803+0100" uniqueID="f7daa6a9-2422-4ade-b64b-fed2056cdc6e">
        <commentlist>
          <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.405+0100" uniqueID="dec32880-e2b8-48ae-b5fc-5bc2e5876c84" severity="Warning">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <text>
              <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
            </text>
          </comments>
        </commentlist>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="folder:Folder" name="BookStation" timeCreated="2011-12-09T15:01:48.462+0100" lastModified="2017-01-17T19:34:29.616+0100" uniqueID="ba10a192-4b73-4174-8b3b-63ef2578a68e">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.518+0100" uniqueID="32c55155-4032-40ac-9856-1bded6872832" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="BS1" name="BS1" elementKind="" description="The BookStationTM provides an administration interface for portable devices such as web pads." timeCreated="2011-12-09T15:02:04.152+0100" lastModified="2017-01-17T19:34:29.535+0100" uniqueID="4f4123a8-3c11-42b8-9967-27cc11008ffd" workPackage="" abstractionLevel="" id="BS1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.534+0100" uniqueID="43a83b6b-c27b-4601-875c-98d4afa24ca6" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T15:02:21.193+0100" uniqueID="6bc4e3cf-c4cc-487a-93f0-942ca5900096">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T15:02:21.195+0100" uniqueID="3f70e6c6-d1c2-4106-8222-af22d53d69b4">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="BS2" name="BS2" elementKind="" description="A load/unload record comprises the compartment id, a reader id, and a set of copy ids." timeCreated="2011-12-09T15:02:39.156+0100" lastModified="2017-01-17T19:34:29.551+0100" uniqueID="ebef8953-ab23-4a00-a576-e9422f5fa481" workPackage="" abstractionLevel="" id="BS2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.551+0100" uniqueID="ca22c0b9-b8cf-42de-9001-7bc5fc6300e9" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T15:03:08.715+0100" uniqueID="9425c007-1e33-42dc-8f20-a3676586a854">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T15:03:08.717+0100" uniqueID="9cdab1a7-021e-4593-8dd5-4a18280d2669">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="BS3" name="BS3" elementKind="" description="The BookStationTM records the status of each compartment (full/empty, deliver/return), and to which reader its contents belong (if any)." timeCreated="2011-12-09T15:03:20.465+0100" lastModified="2017-01-17T19:34:29.568+0100" uniqueID="1fd41505-e25e-4cd8-9950-c93f159bf263" workPackage="" abstractionLevel="" id="BS3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.567+0100" uniqueID="cd6714ef-34f1-489b-a64d-836167af13a2" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T15:03:39.681+0100" uniqueID="f422bbdf-0174-482a-a8de-a2543aba9472">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T15:03:39.685+0100" uniqueID="75b623dc-0e5a-40e8-9e53-af66ac696240">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="BS4" name="BS4" elementKind="" description="If a load data set is forwarded to the LMS, the LMS interface triggers the reader notification about the copies contained in the load data set being available." timeCreated="2011-12-09T15:03:51.988+0100" lastModified="2017-01-17T19:34:29.584+0100" uniqueID="702b4f12-3e5d-4723-83a8-c57ebbc74c8e" workPackage="" abstractionLevel="" id="BS4">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.583+0100" uniqueID="2b035e0f-4e8f-49d1-8474-e05144a88288" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T15:04:01.105+0100" uniqueID="fff7314a-0354-44f9-9deb-c1768d4e29a1">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T15:04:01.109+0100" uniqueID="378be913-6cf9-4646-ba05-5af1ae159e10">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="BS5" name="BS5" elementKind="" description="If an unload data set is forwarded to the LMS, the LMS interface initiates lease termination or lease cancellation actions as appropriate for all the copies included in the unload data set." timeCreated="2011-12-09T15:04:19.518+0100" lastModified="2017-01-17T19:34:29.600+0100" uniqueID="4378eab9-9385-47a7-b7b4-05d492b34306" workPackage="" abstractionLevel="" id="BS5">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.599+0100" uniqueID="b9f46391-4aaa-411e-8140-af0495d850d2" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T15:04:34.379+0100" uniqueID="bbbcb354-7a2e-413c-8bf2-ae85305c312e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T15:04:34.382+0100" uniqueID="38330690-d623-4140-92b2-7f4ce65c7f9f">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="BS6" name="BS6" elementKind="" description="On demand, BSControl sends a list of those compartments to the BSS administration interface, that currently contain copies that are to be returned to the library." timeCreated="2011-12-09T15:04:54.053+0100" lastModified="2017-01-17T19:34:29.616+0100" uniqueID="79e0a4b5-fdb3-4dc6-a801-b370453c7f37" workPackage="" abstractionLevel="" id="BS6">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.616+0100" uniqueID="4a61cb45-17c2-45b4-b17b-0cc8b03f3579" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T15:05:03.356+0100" uniqueID="494a5bc3-ed5e-4c10-81c2-e884884d74fa">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T15:05:03.358+0100" uniqueID="8651e66e-2741-4bc2-9576-821c9bba1537">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
        <contents xsi:type="folder:Folder" name="BookTip" timeCreated="2011-12-09T14:58:11.335+0100" lastModified="2017-01-17T19:34:29.803+0100" uniqueID="7077963f-a8ba-4b7e-89ac-bcd658ccec55">
          <commentlist>
            <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.717+0100" uniqueID="6910ef4b-1107-4915-975a-bb20a5a80914" severity="Warning">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text>
                <fragments xsi:type="text:FormattedText" text="'Label' is empty"/>
              </text>
            </comments>
          </commentlist>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <contents xsi:type="requirement:Requirement" label="BT1" name="BT1" elementKind="" description="BookTipTM is fed by vectors of media leased by a reader (“personal preference vectors”)." timeCreated="2011-12-09T14:58:26.504+0100" lastModified="2017-01-17T19:34:29.734+0100" uniqueID="2019ca0e-7e22-4d7a-9580-714507e8d4ac" workPackage="" abstractionLevel="" id="BT1">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.733+0100" uniqueID="fb19a38e-c271-48be-895f-0601e3d5fb9e" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:58:45.295+0100" uniqueID="a413e826-f84a-43f5-8f48-616fd62b70f1">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:58:45.298+0100" uniqueID="62c28fe4-48e4-4c88-8f7d-c92a446e2145">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="BT1a" name="BT1a" elementKind="" description="BookTipTM can extract personal preference vectors from a lease data base." timeCreated="2011-12-09T14:59:04.629+0100" lastModified="2017-01-17T19:34:29.750+0100" uniqueID="f6b569e5-8460-4eda-9fda-8cde4634edce" workPackage="" abstractionLevel="" id="BT1a">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.749+0100" uniqueID="1d01d97f-362f-450f-b0ca-cfff08c99188" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:59:15.125+0100" uniqueID="9a199bc0-d6e8-4d8c-bc2d-523329d9c455">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:59:15.129+0100" uniqueID="69cad797-e39e-4b3a-883c-164f431145c0">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="BT1b" name="BT1b" elementKind="" description="BookTipTM can be fed by online lease transactions." timeCreated="2011-12-09T14:59:35.109+0100" lastModified="2017-01-17T19:34:29.769+0100" uniqueID="fdebb5be-ac69-47cc-bb35-26eb93c27b0d" workPackage="" abstractionLevel="" id="BT1b">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.768+0100" uniqueID="5c77f7b0-01b4-40d2-bd6a-3300349399ce" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T14:59:53.594+0100" uniqueID="a8c04de8-80d7-45e8-8c10-3ef9b7d29888">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T14:59:53.597+0100" uniqueID="b0420b55-565e-4780-a759-6064bb91f859">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="BT2" name="BT2" elementKind="" description="A suggestion is obtained by presenting BookTipTM with a non-empty set of media identifiers." timeCreated="2011-12-09T15:00:07.047+0100" lastModified="2017-01-17T19:34:29.785+0100" uniqueID="a37b5adf-2c47-46be-9e71-0b863eea5112" workPackage="" abstractionLevel="" id="BT2">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.785+0100" uniqueID="9d6ebf43-5d23-4dc4-8409-eaf7c8386842" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T15:00:17.148+0100" uniqueID="f6edc4ea-41d2-48f3-970c-b99422031e56">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T15:00:17.151+0100" uniqueID="0b07ba3c-82f3-4f64-a63a-505eaea1ca24">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
          <contents xsi:type="requirement:Requirement" label="BT3" name="BT3" elementKind="" description="A suggestion request may be parameterized by media type, age restriction, and medium availability." timeCreated="2011-12-09T15:00:33.554+0100" lastModified="2017-01-17T19:34:29.803+0100" uniqueID="e617dc9f-c4bd-41d3-9101-2fcaea5bf8b4" workPackage="" abstractionLevel="" id="BT3">
            <commentlist>
              <comments xsi:type="comment:IssueComment" timeCreated="2017-01-17T19:34:29.803+0100" uniqueID="f070fc39-6bd5-400c-bbfb-67442129a5d2" severity="Mistake">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <text>
                  <fragments xsi:type="text:FormattedText" text="Element has no relationships"/>
                </text>
              </comments>
            </commentlist>
            <creator name="" timeCreated="2011-12-09T15:00:44.179+0100" uniqueID="672cd4b4-3ba4-4267-9f2e-b402aee3b42e">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </creator>
            <changeList/>
            <responsibleUser name="" timeCreated="2011-12-09T15:00:44.180+0100" uniqueID="2b3933e7-3616-4a41-85fd-5bba6ce42709">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </responsibleUser>
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <details>
              <fragments xsi:type="text:FormattedText" text=""/>
            </details>
            <remarks>
              <fragments xsi:type="text:FormattedText" text=""/>
            </remarks>
          </contents>
        </contents>
      </contents>
    </contents>
  </contents>
</file:File>
