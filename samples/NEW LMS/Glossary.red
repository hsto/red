<?xml version="1.0" encoding="ASCII"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:file="dk.dtu.imm.red.core.file" xmlns:glossary="dk.dtu.imm.red.glossary" xmlns:relationship="dk.dtu.imm.red.core.element.relationship" name="Glossary.red" timeCreated="2014-09-21T22:08:29.980+0200" lastModified="2015-11-16T14:24:37.239+0100" uniqueID="1fd9b9d3-d737-439f-a3af-4fa20fc7dc56">
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="glossary:Glossary" name="Glossary" timeCreated="2011-11-16T13:34:00.058+0100" lastModified="2014-09-21T22:09:18.432+0200" uniqueID="69f8489b-7539-4060-9011-06ba5ae58b83">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-16T13:55:09.132+0100" lastModified="2011-11-16T14:23:56.018+0100" uniqueID="25d7258f-b33d-41dc-a026-06edb2cad3cb">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.1"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.1"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.2"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-16T14:17:39.771+0100" lastModified="2011-11-18T15:40:20.963+0100" uniqueID="a0eb6dab-02d1-4b6e-ae02-12cd694b9437">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.0"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.0"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.0"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#53f5e933-0ca8-47a7-8e37-070af5e7baad"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.1/@contents.3/@relatesTo.0"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T12:47:21.642+0100" lastModified="2011-11-18T15:42:13.211+0100" uniqueID="6410b974-6288-4fcc-98f2-74662f652f54">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>electronic delivery</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T12:49:08.124+0100" lastModified="2011-11-18T15:43:15.765+0100" uniqueID="e3e14f75-4aad-42c3-97f4-4cf83454a911">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.9"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T12:55:29.888+0100" lastModified="2011-11-18T15:43:47.202+0100" uniqueID="2d6396e4-b59c-43a6-9e8f-7a7408855b22">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T12:57:20.062+0100" lastModified="2011-11-18T15:44:55.807+0100" uniqueID="590ebb09-0726-4045-b6ad-a6dc19811c0a">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.3/@relatesTo.2"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:02:29.468+0100" lastModified="2011-11-18T15:45:47.443+0100" uniqueID="e4f5dd0e-14eb-45df-915e-ec5b48be1486">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:06:11.065+0100" lastModified="2011-11-18T15:46:48.030+0100" uniqueID="4931b8e9-ea9a-4299-a8b3-66aec67d9ab6">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:07:53.864+0100" lastModified="2011-11-18T15:47:52.708+0100" uniqueID="94170a14-3810-48ac-8c93-9952b577fd86">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:08:55.489+0100" lastModified="2011-11-18T15:48:44.590+0100" uniqueID="56df337a-c4a6-4441-808a-3bb21ccc8a54">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.2"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:10:05.265+0100" lastModified="2011-11-18T15:49:25.374+0100" uniqueID="14e37ade-81d3-4353-ae0d-d92ee927318c">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:12:09.960+0100" lastModified="2011-11-18T15:49:53.559+0100" uniqueID="fa3cb4cb-9e75-47d4-aeb8-d23a21d6f8b4">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.6"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>Repertory</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:12:57.415+0100" lastModified="2011-11-18T13:25:58.711+0100" uniqueID="76089203-875f-4320-b2e0-1a36d5f9390d">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:26:22.080+0100" lastModified="2011-11-18T15:51:16.638+0100" uniqueID="4f59c7cf-60fb-41b0-a8d5-2abc421b7aaf">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:28:10.318+0100" lastModified="2011-11-18T13:28:32.000+0100" uniqueID="b8085485-efce-417e-8626-d64a5ae4498b">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>remove</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:30:30.678+0100" lastModified="2011-11-18T13:31:00.329+0100" uniqueID="c787633d-ca64-44e6-aa0f-dc7403228dcc">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:31:20.463+0100" lastModified="2011-11-18T13:32:01.556+0100" uniqueID="bc544897-7813-4c37-a6e5-3deec2e0ebed">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:32:41.685+0100" lastModified="2011-11-18T13:32:41.751+0100" uniqueID="66e912bf-be38-428c-8558-f58b2a80fc12">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.9"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T12:46:42.934+0100" lastModified="2011-11-18T15:51:52.479+0100" uniqueID="1fdd4e8a-59bc-4107-b5c6-aeb969439379">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:35:19.330+0100" lastModified="2011-11-18T15:59:05.892+0100" uniqueID="4f65cd21-a6ff-47ac-bd39-1b0b22cb4080">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:36:05.108+0100" lastModified="2011-11-18T15:59:49.059+0100" uniqueID="216fe2cb-e921-4314-bfba-0dbdc3b45499">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:38:49.956+0100" lastModified="2011-11-18T16:00:17.401+0100" uniqueID="d1945a00-2a73-4a61-92c4-2e8d7092c915">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:39:31.378+0100" lastModified="2011-11-18T16:01:06.088+0100" uniqueID="af028a31-4b06-46e4-b78a-729ad13784f0">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:40:21.314+0100" lastModified="2011-11-18T16:01:28.435+0100" uniqueID="c2059a7f-a225-4a7f-90ae-288129083ccc">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T13:40:56.634+0100" lastModified="2011-11-18T16:02:12.084+0100" uniqueID="0fdbb6f3-e971-4925-97c1-4631fb842ee3">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:11:06.220+0100" lastModified="2011-11-18T16:03:00.458+0100" uniqueID="afa995e1-23fe-483e-807d-6592653f827a">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.7"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.4"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:11:57.757+0100" lastModified="2011-11-18T16:03:41.786+0100" uniqueID="fe189edc-d440-4fe4-9a7a-8e215ba148f3">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:12:46.900+0100" lastModified="2011-11-19T21:23:03.966+0100" uniqueID="825ffb50-dce0-4b6e-9517-d14ecc5d6a7e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>bibliographic detail (full)</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:14:52.057+0100" lastModified="2011-11-18T16:04:37.168+0100" uniqueID="5c150c6f-2246-4156-9822-fe8e27171590">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:16:21.340+0100" lastModified="2011-11-18T16:05:12.463+0100" uniqueID="016def47-90f2-4eb0-b7a5-2296cbeac890">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:17:00.240+0100" lastModified="2011-11-23T09:56:29.735+0100" uniqueID="dcbcee44-d8a0-4f88-b777-87f5c0d5033b">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:17:53.552+0100" lastModified="2011-11-18T14:18:28.274+0100" uniqueID="b1f4f856-f03e-42ea-b839-6d15a960ede7">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:18:37.602+0100" lastModified="2011-11-23T09:58:03.486+0100" uniqueID="ed720c01-ab4f-4133-95ba-b58395320c96">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.3"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.8"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.8"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.3/@relatesTo.4"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:19:14.762+0100" lastModified="2011-11-23T09:58:34.925+0100" uniqueID="85b1ff38-7fb1-4881-9796-ff58fd2fb3e2">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:20:10.784+0100" lastModified="2011-11-23T09:59:36.184+0100" uniqueID="76da67a0-cdef-49de-afa0-57e0adf11724">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:21:53.454+0100" lastModified="2011-11-23T10:00:13.369+0100" uniqueID="63b7cff4-a61d-45e2-9206-61f148f2c146">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:22:20.511+0100" lastModified="2011-11-18T14:22:20.557+0100" uniqueID="0c50960c-7a23-4d27-9e6d-d0a802f4badd">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:23:17.070+0100" lastModified="2011-11-23T10:01:58.538+0100" uniqueID="a4ed6b0e-2188-418e-ab80-9c98737e0091">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.5"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.4"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.3"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:24:06.055+0100" lastModified="2011-11-18T14:24:20.941+0100" uniqueID="aad1d261-d269-4698-8aa3-90488cdd25b7">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:26:42.829+0100" lastModified="2011-11-23T10:11:37.313+0100" uniqueID="141b3fdf-96eb-4144-8b36-3cf878218498">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:27:09.772+0100" lastModified="2011-11-23T10:13:33.240+0100" uniqueID="301c03ca-ce0f-4f83-a9c8-8cb61de7e3c2">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:29:41.669+0100" lastModified="2011-11-23T10:14:15.942+0100" uniqueID="58906022-8092-41a5-9f02-954b84e5d062">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>catalog</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:33:25.906+0100" lastModified="2011-11-23T10:15:37.397+0100" uniqueID="d0c6dd29-d7f7-48e4-90f9-660762d021c4">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:33:46.081+0100" lastModified="2011-11-23T10:15:53.748+0100" uniqueID="24bb6e67-ca35-4f34-bf75-eaa2b8518e65">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>remote access</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:34:25.073+0100" lastModified="2011-11-23T10:16:21.015+0100" uniqueID="78454bb0-f8ff-49d0-b0bf-be15c987837d">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>lending duration</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:35:05.346+0100" lastModified="2011-11-23T10:16:43.684+0100" uniqueID="ae237e5e-f2ce-4806-b82e-0a75aa09a26a">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:37:25.281+0100" lastModified="2011-11-23T10:17:34.798+0100" uniqueID="01df2eaf-acf2-4fc9-898d-f524417f238e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:37:46.626+0100" lastModified="2011-11-23T10:17:54.308+0100" uniqueID="fe30c141-54b0-458b-9718-5ae2991a11ae">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.8"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>Catalog</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:38:30.784+0100" lastModified="2011-11-23T10:18:30.725+0100" uniqueID="c791e94b-ca7d-45fc-b4ba-2e48d9319dcc">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.12"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:39:01.217+0100" lastModified="2011-11-23T10:18:50.737+0100" uniqueID="4315d953-4cd1-46bf-9625-a35bced63015">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.10"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:42:07.024+0100" lastModified="2011-11-23T10:19:12.668+0100" uniqueID="99f9a4cc-8c89-4231-bbfc-3e9cb4a3f225">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.11"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>ID</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:42:49.087+0100" lastModified="2011-11-18T14:42:49.155+0100" uniqueID="7824c81f-9a75-448f-a574-5f06d8316941">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.7"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.5"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:43:13.223+0100" lastModified="2011-11-23T10:20:57.720+0100" uniqueID="92b16762-ef0f-45f9-8b4f-0214a8f146a0">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:43:32.190+0100" lastModified="2011-11-18T14:43:32.244+0100" uniqueID="14645462-454e-4d6b-b63f-ee0c5ff8c292">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:43:56.463+0100" lastModified="2011-11-23T10:22:54.418+0100" uniqueID="ed3a58d8-f539-422c-a07e-0b2afc5839e8">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:44:15.775+0100" lastModified="2011-11-23T10:24:08.526+0100" uniqueID="8c580a32-d566-4ded-b549-6bc2fc0945de">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:44:33.966+0100" lastModified="2011-11-23T10:24:47.057+0100" uniqueID="128e5f69-0edc-4767-96d3-e8d7affe2a4b">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T14:45:08.582+0100" lastModified="2011-11-23T10:25:14.833+0100" uniqueID="abee48c7-5fa7-40ef-b193-c4eda2a7af30">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:02:06.688+0100" lastModified="2011-11-23T10:26:08.893+0100" uniqueID="26d54ad5-1e50-4216-8b4f-69680428bc41">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:02:34.783+0100" lastModified="2011-11-18T15:02:34.851+0100" uniqueID="b7d74612-37a0-4b2f-bfb0-3f3f09d27d29">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:02:56.893+0100" lastModified="2011-11-23T10:28:16.770+0100" uniqueID="ced6ab35-7264-4522-b012-a36be44b919e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:03:17.951+0100" lastModified="2011-11-23T10:30:37.902+0100" uniqueID="656e9293-ee00-4c2b-b49c-51fe188d5866">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:03:46.063+0100" lastModified="2011-11-23T10:31:13.771+0100" uniqueID="a72653ea-707f-42ed-a98e-d4498036545e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:04:04.527+0100" lastModified="2011-11-18T15:04:04.588+0100" uniqueID="1fece6e5-bf7d-4558-8bdf-fee612a9a8c7">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:04:25.830+0100" lastModified="2011-11-23T10:32:59.317+0100" uniqueID="8da985c5-bc4e-482f-b6d8-b7ba3748ef7d">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:04:54.984+0100" lastModified="2011-11-18T15:04:55.049+0100" uniqueID="77870acb-c5f7-4eaf-89e3-6122794a242f">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:05:14.045+0100" lastModified="2011-11-23T10:38:22.147+0100" uniqueID="82d443f8-6c1b-4d6a-afed-ef28d06ed46e">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.4"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.6"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.6"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>End user</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:05:51.374+0100" lastModified="2011-11-23T10:39:17.644+0100" uniqueID="3297cc6a-f854-4e6b-b19a-19fd2ebc65b8">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.11"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>Account</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:06:29.099+0100" lastModified="2011-11-23T10:45:47.735+0100" uniqueID="27a992e5-7e53-46d9-8388-8a63874318ea">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:08:18.213+0100" lastModified="2011-11-18T15:08:40.540+0100" uniqueID="2ec98fef-f530-4f6b-bd8e-3e063cfadac9">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.12"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.7"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.3/@relatesTo.3"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>Card</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:08:55.324+0100" lastModified="2011-11-23T10:46:41.883+0100" uniqueID="e6fc295f-f39b-4879-bf8c-d74eb56515f9">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:09:15.188+0100" lastModified="2011-11-23T10:48:45.003+0100" uniqueID="5b472d8c-a701-4531-8ae2-a4254fb410c9">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:09:37.002+0100" lastModified="2011-11-23T10:50:03.315+0100" uniqueID="6ac9bd1a-6088-45ba-b44f-d0688c9899d9">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:09:59.396+0100" lastModified="2011-11-23T10:51:13.269+0100" uniqueID="9cde7c04-69ad-43b7-9224-4a2858c1e5c4">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:10:45.828+0100" lastModified="2011-11-23T10:52:01.404+0100" uniqueID="871fa5f9-13f8-48eb-afd6-ab9be5d84f11">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:11:06.332+0100" lastModified="2011-11-23T10:52:34.860+0100" uniqueID="ff630953-6b2a-4545-b5bd-19fb497032b8">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:11:22.515+0100" lastModified="2011-11-23T10:55:42.111+0100" uniqueID="661e0be1-f6a6-4ed7-b1aa-e78e36337621">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:11:58.139+0100" lastModified="2011-11-23T10:56:29.968+0100" uniqueID="152c0b4b-1e00-4a27-9ede-8ef435cb9639">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.10"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.5"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.3/@relatesTo.1"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:12:24.602+0100" lastModified="2011-11-23T10:57:34.239+0100" uniqueID="b338fd10-9a6e-49ac-ad8f-30d97def130c">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.9"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:12:50.561+0100" lastModified="2011-11-23T10:57:32.904+0100" uniqueID="5d75837b-03d7-400b-88a2-9aece76a92d0">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>bibliographic detail (restricted)</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:13:33.882+0100" lastModified="2011-11-23T10:57:30.130+0100" uniqueID="594e23cb-adc8-438b-bfd3-f117bfeb1f6f">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:13:53.410+0100" lastModified="2011-11-18T15:13:53.483+0100" uniqueID="6c1dd08a-516a-4dfb-b35a-41efabc2eece">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:14:17.874+0100" lastModified="2011-11-23T11:01:07.820+0100" uniqueID="21e2c0fe-4012-4280-9117-a7c6b5762646">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:14:46.569+0100" lastModified="2011-11-23T10:59:19.811+0100" uniqueID="de8ea429-8210-45a3-b2fa-7236b8afb360">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:14:59.969+0100" lastModified="2011-11-18T15:15:15.445+0100" uniqueID="549f14c1-a5c5-4ea9-920c-dd369721da8e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:15:39.050+0100" lastModified="2011-11-23T11:00:11.381+0100" uniqueID="029319bb-c289-49f8-bebb-a54d4c415d05">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:15:58.139+0100" lastModified="2011-11-18T15:15:58.206+0100" uniqueID="5f77e9ca-1c3f-469f-bff3-c6ca40059847">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" timeCreated="2011-11-18T15:16:17.849+0100" lastModified="2011-11-23T11:01:02.789+0100" uniqueID="58ac6d41-faec-4f0c-b965-58394edea65a">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="foo" name="bar" description="" timeCreated="2015-11-16T13:56:19.292+0100" uniqueID="1603b8a0-380b-4c7f-a59e-378a6efdf19e">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="foo" name="bar" description="" timeCreated="2015-11-16T13:56:40.559+0100" uniqueID="bdc89fb3-1057-415e-b8fb-0b3d6d5f30c3">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
  </contents>
</file:File>
