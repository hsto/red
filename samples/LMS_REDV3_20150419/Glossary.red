<?xml version="1.0" encoding="UTF-8"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:file="dk.dtu.imm.red.core.file" xmlns:glossary="dk.dtu.imm.red.glossary" xmlns:relationship="dk.dtu.imm.red.core.element.relationship" name="Glossary.red" timeCreated="2014-09-21T22:08:29.980+0200" lastModified="2015-04-23T11:19:10.378+0200" uniqueID="1fd9b9d3-d737-439f-a3af-4fa20fc7dc56">
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="glossary:Glossary" name="GlossaryLMS" timeCreated="2011-11-16T13:34:00.058+0100" lastModified="2015-01-28T20:44:18.989+0100" uniqueID="69f8489b-7539-4060-9011-06ba5ae58b83">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="glossary:GlossaryEntry" label="LMS" name="Library Management System" description="The electronic system to be developed for TCL." timeCreated="2011-11-16T13:55:09.132+0100" lastModified="2015-01-28T20:44:17.877+0100" uniqueID="25d7258f-b33d-41dc-a026-06edb2cad3cb">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.1"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#83a32876-d3a5-4de5-9d8b-33a552b72f81"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.2"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Taarbæk Commune Library" description="The library for which the LMS is developed initially. Includes the premises, organization, clients, and media in the corpus." timeCreated="2011-11-16T14:17:39.771+0100" lastModified="2015-01-28T20:44:17.932+0100" uniqueID="a0eb6dab-02d1-4b6e-ae02-12cd694b9437">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.0"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#92b82e8c-423a-4625-a761-dd77edd78030"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#dce9e211-5fdd-46f2-aed0-428187e4a541"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.3/@relatesTo.0"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.1/@contents.3/@relatesTo.0"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="AMDS" name="Advanced Media Delivery Service" description="A bundle of services to deliver media to a reader online or via the book station." timeCreated="2011-11-18T12:47:21.642+0100" lastModified="2015-01-28T20:44:17.966+0100" uniqueID="6410b974-6288-4fcc-98f2-74662f652f54">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>electronic delivery</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Bibliographic detail (full)" description="Like --> bibliographic detail (restricted) plus &#xD;&#xA;&#xD;&#xA;Publisher &#xD;&#xA;Place &#xD;&#xA;Year &#xD;&#xA;Edition type (e.g. 1st, 2nd, …) &#xD;&#xA;ISBN/ISNN &#xD;&#xA;Classification (e.g. DK5)" timeCreated="2011-11-18T12:49:08.124+0100" lastModified="2015-01-28T20:44:17.996+0100" uniqueID="e3e14f75-4aad-42c3-97f4-4cf83454a911">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.9"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Bibliographic detail (restricted)" description="The reduced bilbiographic details include &#xD;&#xA;&#xD;&#xA;Author &#xD;&#xA;Title &#xD;&#xA;Age recommendation (0-1, 1-3, 3-6, 6-10, 8-12, 10-14, 12-16, 14-18, +16, +18, +21) &#xD;&#xA;Category (Literature, Science, Lifeystyle, Politics, Art, ...) &#xD;&#xA;Keywords, and  &#xD;&#xA;Media type (book, periodical, CD, DVD, Board game, …)" timeCreated="2011-11-18T12:55:29.888+0100" lastModified="2015-01-28T20:44:18.026+0100" uniqueID="2d6396e4-b59c-43a6-9e8f-7a7408855b22">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="BS" name="BookStation TM " description="A stationary robot suited for dispensing and accepting deliveries of media." timeCreated="2011-11-18T12:57:20.062+0100" lastModified="2015-01-28T20:44:18.052+0100" uniqueID="590ebb09-0726-4045-b6ad-a6dc19811c0a">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.3/@relatesTo.2"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="BT" name="BookTip TM" description="A third-party supplied subsystem to suggest media to readers based on other reader’s previous choices." timeCreated="2011-11-18T13:02:29.468+0100" lastModified="2015-01-28T20:44:18.075+0100" uniqueID="e4f5dd0e-14eb-45df-915e-ec5b48be1486">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Catalog action" description="Catalog actions may be initiated by anonymous users. Catalog actions (both for local and remote interfaces) are:&#xD;&#xA;&#xD;&#xA;searching the catalog &#xD;&#xA;proposing a medium to be purchased (&quot;wishlist&quot;) &#xD;&#xA;tracking the state of a wish (identified users only) &#xD;&#xA;reading information about new media in the library" timeCreated="2011-11-18T13:06:11.065+0100" lastModified="2015-01-28T20:44:18.097+0100" uniqueID="4931b8e9-ea9a-4299-a8b3-66aec67d9ab6">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Catalog item identifiers" description="An unique series of digits that identifies a medium inside the system." timeCreated="2011-11-18T13:07:53.864+0100" lastModified="2015-01-28T20:44:18.114+0100" uniqueID="94170a14-3810-48ac-8c93-9952b577fd86">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="CL" name="Chief Librarian" description="The librarian who is in charge in the library.He/she has higher rights than any other librarians." timeCreated="2011-11-18T13:08:55.489+0100" lastModified="2015-01-28T20:44:18.129+0100" uniqueID="56df337a-c4a6-4441-808a-3bb21ccc8a54">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.2"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Client" description="Any person or legal body asking for services by TCL. Includes Readers as well as other people." timeCreated="2011-11-18T13:10:05.265+0100" lastModified="2015-01-28T20:44:18.143+0100" uniqueID="14e37ade-81d3-4353-ae0d-d92ee927318c">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Corpus" description="The complete set of all media owned by TCL." timeCreated="2011-11-18T13:12:09.960+0100" lastModified="2015-01-28T20:44:18.157+0100" uniqueID="fa3cb4cb-9e75-47d4-aeb8-d23a21d6f8b4">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.6"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>Repertory</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="DM2" name="DanMARC2" description="A special format to catalog (code) bibliographic data for library systems (see http://www.kat-format.dk/)" timeCreated="2011-11-18T13:12:57.415+0100" lastModified="2015-01-28T20:44:18.169+0100" uniqueID="76089203-875f-4320-b2e0-1a36d5f9390d">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Database" description="Database containing data respectively of media, readers and librarians." timeCreated="2011-11-18T13:26:22.080+0100" lastModified="2015-01-28T20:44:18.182+0100" uniqueID="4f59c7cf-60fb-41b0-a8d5-2abc421b7aaf">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Delete" description="When an entry of the catalog is deleted, all its data are deleted and the entry is no more stored." timeCreated="2011-11-18T13:28:10.318+0100" lastModified="2015-01-28T20:44:18.195+0100" uniqueID="b8085485-efce-417e-8626-d64a5ae4498b">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>remove</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="DDC" name="Dewey Decimal Classification" description="A numeric schema for classifying books." timeCreated="2011-11-18T13:30:30.678+0100" lastModified="2015-01-28T20:44:18.207+0100" uniqueID="c787633d-ca64-44e6-aa0f-dc7403228dcc">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="DK5 " description="Version 5 of the Danish universal decimal classification system, a hierarchical classification schema for media developed and used by the Danish National Biblio­gra­phy (see www.kb.dk/en/klassifikation)." timeCreated="2011-11-18T13:31:20.463+0100" lastModified="2015-01-28T20:44:18.219+0100" uniqueID="bc544897-7813-4c37-a6e5-3deec2e0ebed">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Due date " description="Max. limit date until when the item has to be returned." timeCreated="2011-11-18T13:32:41.685+0100" lastModified="2015-01-28T20:44:18.231+0100" uniqueID="66e912bf-be38-428c-8558-f58b2a80fc12">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.9"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Balance" description="Amount of fees accumulated by a single Reader." timeCreated="2011-11-18T12:46:42.934+0100" lastModified="2015-01-28T20:44:18.241+0100" uniqueID="1fdd4e8a-59bc-4107-b5c6-aeb969439379">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="End media limitation " description="Maximum number of items that can be lent out to a user at a time." timeCreated="2011-11-18T13:35:19.330+0100" lastModified="2015-01-28T20:44:18.252+0100" uniqueID="4f65cd21-a6ff-47ac-bd39-1b0b22cb4080">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="EDA" name="Expected Date of Availability" description="Date in which the medium is expected to be back in an “available” state." timeCreated="2011-11-18T13:36:05.108+0100" lastModified="2015-01-28T20:44:18.263+0100" uniqueID="216fe2cb-e921-4314-bfba-0dbdc3b45499">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Expert Search Mode" description="Expert search mode which allows to use more detailed search criteria compared to the basic search mode. This search mode allows to use full bibliographic details." timeCreated="2011-11-18T13:38:49.956+0100" lastModified="2015-01-28T20:44:18.274+0100" uniqueID="d1945a00-2a73-4a61-92c4-2e8d7092c915">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Expiration" description="A lease period expires when the day on which the leased medium was due has passed without the medium being registered as &quot;returned&quot;." timeCreated="2011-11-18T13:39:31.378+0100" lastModified="2015-01-28T20:44:18.286+0100" uniqueID="af028a31-4b06-46e4-b78a-729ad13784f0">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="First Come First Served" description="Policy whereby the requests by users are satisfied according to the order in which they arrived." timeCreated="2011-11-18T13:40:21.314+0100" lastModified="2015-01-28T20:44:18.297+0100" uniqueID="c2059a7f-a225-4a7f-90ae-288129083ccc">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Foreign reader" description="(1) A natural person registered as reader at an associated library(2) A legal person such as another librarian" timeCreated="2011-11-18T13:40:56.634+0100" lastModified="2015-01-28T20:44:18.305+0100" uniqueID="0fdbb6f3-e971-4925-97c1-4631fb842ee3">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="FD" name="Front Desk" description="Point-of-service for readers.&#xD;&#xA;Place in the library where the main activities in the library take place. Here the lending and returning processes are carried on." timeCreated="2011-11-18T14:11:06.220+0100" lastModified="2015-01-28T20:44:18.314+0100" uniqueID="afa995e1-23fe-483e-807d-6592653f827a">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.7"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.4"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="FDL" name="Front Desk Librarian" description="A librarian whose duty is at the FD to deal with the customers of the library." timeCreated="2011-11-18T14:11:57.757+0100" lastModified="2015-01-28T20:44:18.323+0100" uniqueID="fe189edc-d440-4fe4-9a7a-8e215ba148f3">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Full bibliographic detail" description="bibliographic detail (full)" timeCreated="2011-11-18T14:12:46.900+0100" lastModified="2015-01-28T20:44:18.335+0100" uniqueID="825ffb50-dce0-4b6e-9517-d14ecc5d6a7e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>bibliographic detail (full)</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="ID badge" description="the ID number printed on the sticker and attached to the physical media that makes it possible to scan the media ID." timeCreated="2011-11-18T14:14:52.057+0100" lastModified="2015-01-28T20:44:18.348+0100" uniqueID="5c150c6f-2246-4156-9822-fe8e27171590">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="id" name="Identifying Number" description="Number that uniquely identifies a Reader or a medium to the LMS." timeCreated="2011-11-18T14:16:21.340+0100" lastModified="2015-01-28T20:44:18.361+0100" uniqueID="016def47-90f2-4eb0-b7a5-2296cbeac890">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="ILX" name="InterLibrary Xchange " description="Agreement between libraries in Denmark for mutual media sharing. Defines an interface for remote access to catalogs, for requests and reservations." timeCreated="2011-11-18T14:17:00.240+0100" lastModified="2015-01-28T20:44:18.374+0100" uniqueID="dcbcee44-d8a0-4f88-b777-87f5c0d5033b">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="ISBN" name="International Standard Book Number" description="Unique numeric commercial book identifier based upon the 9-digit Standard Book Numbering (SBN) code created by Gordon Foster [see http://en.wikipedia.org/wiki/International_Standard_Book_Number]" timeCreated="2011-11-18T14:17:53.552+0100" lastModified="2015-01-28T20:44:18.384+0100" uniqueID="b1f4f856-f03e-42ea-b839-6d15a960ede7">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Late fees" description="A late fee, is a charge levied against a reader by the library for not returning a medium by its due date" timeCreated="2011-11-18T14:18:37.602+0100" lastModified="2015-01-28T20:44:18.395+0100" uniqueID="ed720c01-ab4f-4133-95ba-b58395320c96">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.3"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.8"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.8"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.3/@relatesTo.4"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Lender" description="A reader who has lent a medium and has not yet returned it." timeCreated="2011-11-18T14:19:14.762+0100" lastModified="2015-01-28T20:44:18.407+0100" uniqueID="85b1ff38-7fb1-4881-9796-ff58fd2fb3e2">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Lender (of the medium)" description="The reader holding the current lease of the medium." timeCreated="2011-11-18T14:20:10.784+0100" lastModified="2015-01-28T20:44:18.418+0100" uniqueID="76da67a0-cdef-49de-afa0-57e0adf11724">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Lending Dialog" description="Part of the Librarians System which deals with the lending related activities." timeCreated="2011-11-18T14:21:53.454+0100" lastModified="2015-01-28T20:44:18.430+0100" uniqueID="63b7cff4-a61d-45e2-9206-61f148f2c146">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Lending Status" description="The lending status is one of the three parallel states that are combined in the Medium state.Examples of lending status are available, lent, restricted…." timeCreated="2011-11-18T14:22:20.511+0100" lastModified="2015-01-28T20:44:18.442+0100" uniqueID="0c50960c-7a23-4d27-9e6d-d0a802f4badd">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Librarian" description="A user working at the library the person who works in the library and uses the Library Management System with specific rights and responsibilities. There are different types of librarians: chief librarian, vice chief librarian, front desk librarian, and auxiliary librarian. chief, vice chief, front desk, auxiliary" timeCreated="2011-11-18T14:23:17.070+0100" lastModified="2015-01-28T20:44:18.453+0100" uniqueID="a4ed6b0e-2188-418e-ab80-9c98737e0091">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.5"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.4"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.3"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Librarian Database" description="Database containing all the librarian accounts." timeCreated="2011-11-18T14:24:06.055+0100" lastModified="2015-01-28T20:44:18.466+0100" uniqueID="aad1d261-d269-4698-8aa3-90488cdd25b7">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Librarian Dialog" description="Part of the Librarians System accessible only by the Chief Librarian. This interface deals with librarian accounts." timeCreated="2011-11-18T14:26:42.829+0100" lastModified="2015-01-28T20:44:18.476+0100" uniqueID="141b3fdf-96eb-4144-8b36-3cf878218498">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Librarians System" description="Part of the system accessible only by Librarians. It includes the Lending, Returning and Reporting Dialogs and the Librarian Dialog." timeCreated="2011-11-18T14:27:09.772+0100" lastModified="2015-01-28T20:44:18.485+0100" uniqueID="301c03ca-ce0f-4f83-a9c8-8cb61de7e3c2">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Library Catalog" description="The complete list of all the Medium files." timeCreated="2011-11-18T14:29:41.669+0100" lastModified="2015-01-28T20:44:18.496+0100" uniqueID="58906022-8092-41a5-9f02-954b84e5d062">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>catalog</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Lifetime" description="The period of time after which data (notification, medium,…) in a particular state (expired, unavailable,….)  are removed from the system." timeCreated="2011-11-18T14:33:25.906+0100" lastModified="2015-01-28T20:44:18.506+0100" uniqueID="d0c6dd29-d7f7-48e4-90f9-660762d021c4">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="LMS webpage" description="Part of the LMS accessible from outside the library through Internet" timeCreated="2011-11-18T14:33:46.081+0100" lastModified="2015-01-28T20:44:18.518+0100" uniqueID="24bb6e67-ca35-4f34-bf75-eaa2b8518e65">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>remote access</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Loan Period" description="Period of time exceeded which the medium must be returned to the library." timeCreated="2011-11-18T14:34:25.073+0100" lastModified="2015-01-28T20:44:18.528+0100" uniqueID="78454bb0-f8ff-49d0-b0bf-be15c987837d">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>lending duration</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Log in" description="Access the LMS webpage and identify with username and password." timeCreated="2011-11-18T14:35:05.346+0100" lastModified="2015-01-28T20:44:18.540+0100" uniqueID="ae237e5e-f2ce-4806-b82e-0a75aa09a26a">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="MARC" description="Acronym for MAchine-Readable Cataloging, see DanMARC" timeCreated="2011-11-18T14:37:25.281+0100" lastModified="2015-01-28T20:44:18.551+0100" uniqueID="01df2eaf-acf2-4fc9-898d-f524417f238e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Media Catalog" description="A catalog containing classification information about media." timeCreated="2011-11-18T14:37:46.626+0100" lastModified="2015-01-28T20:44:18.562+0100" uniqueID="fe30c141-54b0-458b-9718-5ae2991a11ae">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.8"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>Catalog</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Medium" description="An item owned by the library for access by readers." timeCreated="2011-11-18T14:38:30.784+0100" lastModified="2015-01-28T20:44:18.574+0100" uniqueID="c791e94b-ca7d-45fc-b4ba-2e48d9319dcc">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.12"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Medium file" description="Set of data containing all the information about a particular medium such as author, title, …" timeCreated="2011-11-18T14:39:01.217+0100" lastModified="2015-01-28T20:44:18.585+0100" uniqueID="4315d953-4cd1-46bf-9625-a35bced63015">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.10"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="MID" name="Medium ID" description="Unique number which shall identify a medium." timeCreated="2011-11-18T14:42:07.024+0100" lastModified="2015-01-28T20:44:18.596+0100" uniqueID="99f9a4cc-8c89-4231-bbfc-3e9cb4a3f225">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.11"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>ID</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Medium, Media" description="Any physical or virtual media item such as books, perio­dicals, papers, CDs/DVDs, PDF files, Games, EBooks and so on. Every medium appears in the catalog." timeCreated="2011-11-18T14:42:49.087+0100" lastModified="2015-01-28T20:44:18.608+0100" uniqueID="7824c81f-9a75-448f-a574-5f06d8316941">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.7"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.5"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Message Expired" description="Notification which has already been read by the Reader or that has become obsolete due to expiring of its lifetime. Expired messages are archived and may still be read." timeCreated="2011-11-18T14:43:13.223+0100" lastModified="2015-01-28T20:44:18.619+0100" uniqueID="92b16762-ef0f-45f9-8b4f-0214a8f146a0">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Naive Search Mode" description="Basic search mode which allows to search according to the common criteria as title, author..." timeCreated="2011-11-18T14:43:32.190+0100" lastModified="2015-01-28T20:44:18.629+0100" uniqueID="14645462-454e-4d6b-b63f-ee0c5ff8c292">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Network of Libraries" description="Set of libraries which share the owned media to increase the quality of the service offered to their readers." timeCreated="2011-11-18T14:43:56.463+0100" lastModified="2015-01-28T20:44:18.640+0100" uniqueID="ed3a58d8-f539-422c-a07e-0b2afc5839e8">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Next Reserver" description="Reserver just below a particular reader in the reservation list. If the particular reader is the last one in the Reservation List the next reserver does not exist." timeCreated="2011-11-18T14:44:15.775+0100" lastModified="2015-01-28T20:44:18.651+0100" uniqueID="8c580a32-d566-4ded-b549-6bc2fc0945de">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Not loanable" description="Media that cannot leave the premises of the library but it can be requested and viewed inside." timeCreated="2011-11-18T14:44:33.966+0100" lastModified="2015-01-28T20:44:18.662+0100" uniqueID="128e5f69-0edc-4767-96d3-e8d7affe2a4b">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Notification" description="Text sent to a Reader or Librarian by the system to inform about something." timeCreated="2011-11-18T14:45:08.582+0100" lastModified="2015-01-28T20:44:18.674+0100" uniqueID="abee48c7-5fa7-40ef-b193-c4eda2a7af30">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Overdue" description="A medium is overdue if its lease is expired." timeCreated="2011-11-18T15:02:06.688+0100" lastModified="2015-01-28T20:44:18.685+0100" uniqueID="26d54ad5-1e50-4216-8b4f-69680428bc41">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Payment slip " description="Paper proof indicating that the amount in due has already been paid" timeCreated="2011-11-18T15:02:34.783+0100" lastModified="2015-01-28T20:44:18.695+0100" uniqueID="b7d74612-37a0-4b2f-bfb0-3f3f09d27d29">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Pick up" description="The action a reserver performs coming to the library to lend the reserved medium." timeCreated="2011-11-18T15:02:56.893+0100" lastModified="2015-01-28T20:44:18.704+0100" uniqueID="ced6ab35-7264-4522-b012-a36be44b919e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Pick up time" description="Time granted to a reader to come to the library to pick up the medium he/she reserved before the reservation is removed" timeCreated="2011-11-18T15:03:17.951+0100" lastModified="2015-01-28T20:44:18.716+0100" uniqueID="656e9293-ee00-4c2b-b49c-51fe188d5866">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Policy" description="a set of settings of policy parameters that form a coherent whole" timeCreated="2011-11-18T15:03:46.063+0100" lastModified="2015-01-28T20:44:18.727+0100" uniqueID="a72653ea-707f-42ed-a98e-d4498036545e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Policy Parameter" description="A library policy parameter that is customizable by the library staff and does not require help by IT support staff." timeCreated="2011-11-18T15:04:04.527+0100" lastModified="2015-01-28T20:44:18.738+0100" uniqueID="1fece6e5-bf7d-4558-8bdf-fee612a9a8c7">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Previous Reserver" description="Reserver just above a particular reader in the reservation list. If the particular reader is in the first position in the list, the previous reserver is the present lender." timeCreated="2011-11-18T15:04:25.830+0100" lastModified="2015-01-28T20:44:18.750+0100" uniqueID="8da985c5-bc4e-482f-b6d8-b7ba3748ef7d">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Prolong item " description="Extend the lease period of a item already leased." timeCreated="2011-11-18T15:04:54.984+0100" lastModified="2015-01-28T20:44:18.780+0100" uniqueID="77870acb-c5f7-4eaf-89e3-6122794a242f">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Reader" description="Any person not employed by TCL, and registered for using the facilities of TCL." timeCreated="2011-11-18T15:05:14.045+0100" lastModified="2015-01-28T20:44:18.791+0100" uniqueID="82d443f8-6c1b-4d6a-afed-ef28d06ed46e">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.0/@relatesTo.4"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.6"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.6"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>End user</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Reader Account" description="The reader’s account contains personal information of the reader (generated id, name, birthday, address, CPR number, photo), and information about the reader’s card number, login id." timeCreated="2011-11-18T15:05:51.374+0100" lastModified="2015-01-28T20:44:18.800+0100" uniqueID="3297cc6a-f854-4e6b-b19a-19fd2ebc65b8">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.11"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>Account</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="RA" name="Reader action" description="RAs involve a user, a reader account, and a medium. Reader actions (both for local and remote interfaces) are:&#xD;&#xA;&#xD;&#xA;reserving a medium &#xD;&#xA;lending a medium &#xD;&#xA;prolonging a medium &#xD;&#xA;returning a medium &#xD;&#xA;paying fees.&#xD;&#xA;Catalog searches are not reader actions." timeCreated="2011-11-18T15:06:29.099+0100" lastModified="2015-01-28T20:44:18.810+0100" uniqueID="27a992e5-7e53-46d9-8388-8a63874318ea">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Reader Card " description="A device used by user to prove they have an account at library." timeCreated="2011-11-18T15:08:18.213+0100" lastModified="2015-01-28T20:44:18.818+0100" uniqueID="2ec98fef-f530-4f6b-bd8e-3e063cfadac9">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.12"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.7"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.3/@relatesTo.3"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>Card</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Reader Database" description="Section of the database containing all the reader accounts." timeCreated="2011-11-18T15:08:55.324+0100" lastModified="2015-01-28T20:44:18.826+0100" uniqueID="e6fc295f-f39b-4879-bf8c-d74eb56515f9">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Reader Dialog" description="Interface of the system which deals with the readers accounts." timeCreated="2011-11-18T15:09:15.188+0100" lastModified="2015-01-28T20:44:18.834+0100" uniqueID="5b472d8c-a701-4531-8ae2-a4254fb410c9">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Remote catalog" description="Catalog of items not directly owned by the Library but that may still be lent by readers. Examples of remote catalogs are Catalog of other libraries, catalog of Newspapers…." timeCreated="2011-11-18T15:09:37.002+0100" lastModified="2015-01-28T20:44:18.841+0100" uniqueID="6ac9bd1a-6088-45ba-b44f-d0688c9899d9">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Remote terminal" description="Each device with which the LMS webpage may be accessed." timeCreated="2011-11-18T15:09:59.396+0100" lastModified="2015-01-28T20:44:18.850+0100" uniqueID="9cde7c04-69ad-43b7-9224-4a2858c1e5c4">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Report Dialog" description="Part of the Librarians System which deals with the generation of reports." timeCreated="2011-11-18T15:10:45.828+0100" lastModified="2015-01-28T20:44:18.857+0100" uniqueID="871fa5f9-13f8-48eb-afd6-ab9be5d84f11">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Reservation Capability" description="The maximum number of reservations that a Reader may request." timeCreated="2011-11-18T15:11:06.332+0100" lastModified="2015-01-28T20:44:18.866+0100" uniqueID="ff630953-6b2a-4545-b5bd-19fb497032b8">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Reservation List" description="List of all reservers for a specific medium. Reservers are ordered according to the time of their reservation, from the earliest to the latest." timeCreated="2011-11-18T15:11:22.515+0100" lastModified="2015-01-28T20:44:18.878+0100" uniqueID="661e0be1-f6a6-4ed7-b1aa-e78e36337621">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Reserved" description="A medium is reserved, if at least one reader has a reservation for this medium." timeCreated="2011-11-18T15:11:58.139+0100" lastModified="2015-01-28T20:44:18.888+0100" uniqueID="152c0b4b-1e00-4a27-9ede-8ef435cb9639">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.10"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.2/@relatesTo.5"/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.3/@relatesTo.1"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Reserver" description="Reader with a reservation pending on a specific medium." timeCreated="2011-11-18T15:12:24.602+0100" lastModified="2015-01-28T20:44:18.895+0100" uniqueID="b338fd10-9a6e-49ac-ad8f-30d97def130c">
      <commentlist/>
      <relatedBy xsi:type="relationship:ElementReference" href="Library%20Management%20System.red#//@contents.0/@contents.2/@contents.1/@relatesTo.9"/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Restricted bibliographic detail" description="bibliographic detail (restricted)" timeCreated="2011-11-18T15:12:50.561+0100" lastModified="2015-01-28T20:44:18.905+0100" uniqueID="5d75837b-03d7-400b-88a2-9aece76a92d0">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <synonyms>bibliographic detail (restricted)</synonyms>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Returning Dialog" description="Part of the Librarians System which deals with the returning related activities." timeCreated="2011-11-18T15:13:33.882+0100" lastModified="2015-01-28T20:44:18.916+0100" uniqueID="594e23cb-adc8-438b-bfd3-f117bfeb1f6f">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Search Dialog" description="Interface of the system which deals with the searching related activities" timeCreated="2011-11-18T15:13:53.410+0100" lastModified="2015-01-28T20:44:18.927+0100" uniqueID="6c1dd08a-516a-4dfb-b35a-41efabc2eece">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Shelving" description="Putting the media in the appropriate shelf according to a defined policy." timeCreated="2011-11-18T15:14:17.874+0100" lastModified="2015-01-28T20:44:18.938+0100" uniqueID="21e2c0fe-4012-4280-9117-a7c6b5762646">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="State of a Medium" description="Each medium has a set of three attributes that defines its possible use in the system.The first attribute may be &quot;reserved&quot; and &quot;not reserved&quot; and states whenever there are reservation pending for such a medium or not.The second attribute may be &quot;not damaged&quot;, &quot;slightly damaged&quot; and &quot;damaged beyond repair&quot; and states the seriousness of damage the medium has been subject to.The third attribute may be &quot;available&quot;, &quot;lent&quot;, &quot;restricted&quot; and &quot;unavailable&quot; and deals with the lending status of such a medium." timeCreated="2011-11-18T15:14:46.569+0100" lastModified="2015-01-28T20:44:18.948+0100" uniqueID="de8ea429-8210-45a3-b2fa-7236b8afb360">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="sys admin" name="System Administrator" description="A natural person liable for the correct functioning of the system. He/she is not a user of the system." timeCreated="2011-11-18T15:14:59.969+0100" lastModified="2015-01-28T20:44:18.959+0100" uniqueID="549f14c1-a5c5-4ea9-920c-dd369721da8e">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Unidentified User" description="A person lacking in a reader account. He/she may not be identified by the LMS." timeCreated="2011-11-18T15:15:39.050+0100" lastModified="2015-01-28T20:44:18.969+0100" uniqueID="029319bb-c289-49f8-bebb-a54d4c415d05">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="Universal Decimal Classification" description="The Universal Decimal Classification is the world's foremost multilingual classification scheme for all fields of knowledge, a sophisticated indexing and retrieval tool. [ see www.udc.org/about.html]" timeCreated="2011-11-18T15:15:58.139+0100" lastModified="2015-01-28T20:44:18.980+0100" uniqueID="5f77e9ca-1c3f-469f-bff3-c6ca40059847">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="glossary:GlossaryEntry" label="" name="User " description="Any person employed by TCL using the LMS (e.g librarians, interns, temps) or a reader or a guest." timeCreated="2011-11-18T15:16:17.849+0100" lastModified="2015-01-28T20:44:18.989+0100" uniqueID="58ac6d41-faec-4f0c-b965-58394edea65a">
      <commentlist/>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
  </contents>
</file:File>
