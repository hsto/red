<?xml version="1.0" encoding="ASCII"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:configuration="dk.dtu.imm.red.specificationelements.configuration" xmlns:context="dk.dtu.imm.red.visualmodeling.visualmodel.context" xmlns:file="dk.dtu.imm.red.core.file" xmlns:folder="dk.dtu.imm.red.core.folder" xmlns:goal="dk.dtu.imm.red.visualmodeling.visualmodel.goal" xmlns:goal_1="dk.dtu.imm.red.specificationelements.goal" xmlns:persona="dk.dtu.imm.red.specificationelements.persona" xmlns:stakeholder="dk.dtu.imm.red.specificationelements.stakeholder" xmlns:text="dk.dtu.imm.red.core.text" xmlns:ucScenario="dk.dtu.imm.red.specificationelements.ucscenario" xmlns:usecase="dk.dtu.imm.red.visualmodeling.visualmodel.usecase" xmlns:vision="dk.dtu.imm.red.specificationelements.vision" xmlns:visualmodel="dk.dtu.imm.red.visualmodeling" name="main.red" timeCreated="2015-09-10T08:08:09.281+0200" lastModified="2015-10-26T08:19:45.976+0100" uniqueID="1b31a005-1257-4753-a5d7-23af8f741c06">
  <creator name="" timeCreated="2015-09-10T08:08:09.281+0200" uniqueID="d5eb7472-1ccc-4144-be1a-7700aefd4836" id="" email="" initials="">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </creator>
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="folder:Folder" name="Goals" timeCreated="2015-09-10T09:53:40.521+0200" lastModified="2015-10-26T08:19:45.976+0100" uniqueID="a7c6bf4e-31c2-4f14-99e9-0da9f728a33e">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="visualmodel:Diagram" label="GOV" name="Goal Overview Diagram" elementKind="unspecified" description="" timeCreated="2015-09-10T08:26:48.566+0200" lastModified="2015-09-10T09:53:47.729+0200" uniqueID="6358e669-9bb9-40d4-a7a8-c0910d44b762" workPackage="">
      <creator name="" timeCreated="2015-09-10T08:27:00.432+0200" uniqueID="3583060e-11b4-4e0e-9416-56dffa50f302">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T08:27:00.433+0200" uniqueID="1f0479eb-8c6b-45a0-b3dc-52589db0ea9b">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.0/@contents.0/@VisualDiagram" DiagramType="Goal">
        <Elements xsi:type="goal:VisualGoalLayer" Location="73,54" Bounds="500,220" Parent="//@contents.0/@contents.0/@VisualDiagram" Diagram="//@contents.0/@contents.0/@VisualDiagram" Name="Strategic" visualID="_8vBq0FeEEeWFXJq3apS_nA">
          <Elements xsi:type="goal:VisualGoalElement" Location="137,85" Bounds="288,50" Parent="_8vBq0FeEEeWFXJq3apS_nA" Diagram="//@contents.0/@contents.0/@VisualDiagram" Connections="//@contents.0/@contents.0/@VisualDiagram/@DiagramConnections.0" Name="Demonstrate benefits of EU integration to all citizens" visualID="_7uurcFeEEeWFXJq3apS_nA" SpecificationElement="f77f8674-6591-4b66-8206-dd2357886768" IsLinkedToElement="true" SpecificationElementType="dk.dtu.imm.red.specificationelements.goal.Goal"/>
        </Elements>
        <Elements xsi:type="goal:VisualGoalLayer" Location="73,264" Bounds="500,220" Parent="//@contents.0/@contents.0/@VisualDiagram" Diagram="//@contents.0/@contents.0/@VisualDiagram" Name="Business" visualID="_9Z5MwFeEEeWFXJq3apS_nA">
          <Elements xsi:type="goal:VisualGoalElement" Location="135,82" Bounds="288,50" Parent="_9Z5MwFeEEeWFXJq3apS_nA" Diagram="//@contents.0/@contents.0/@VisualDiagram" Connections="//@contents.0/@contents.0/@VisualDiagram/@DiagramConnections.0 //@contents.0/@contents.0/@VisualDiagram/@DiagramConnections.1" Name="high-quality healthcare for all citizens" visualID="_8LPoUFeEEeWFXJq3apS_nA" SpecificationElement="85582111-834c-4a12-be31-0c7256245a7c" IsLinkedToElement="true" SpecificationElementType="dk.dtu.imm.red.specificationelements.goal.Goal"/>
        </Elements>
        <Elements xsi:type="goal:VisualGoalLayer" Location="73,480" Bounds="500,220" Parent="//@contents.0/@contents.0/@VisualDiagram" Diagram="//@contents.0/@contents.0/@VisualDiagram" Name="Solution" visualID="__Kx7kFeEEeWFXJq3apS_nA">
          <Elements xsi:type="goal:VisualGoalElement" Location="135,65" Bounds="288,50" Parent="__Kx7kFeEEeWFXJq3apS_nA" Diagram="//@contents.0/@contents.0/@VisualDiagram" Connections="//@contents.0/@contents.0/@VisualDiagram/@DiagramConnections.1" Name="Convenient handling of appointments" visualID="_8eFEwFeEEeWFXJq3apS_nA" SpecificationElement="9df83440-b1b7-4187-8c95-b91e60e27a26" IsLinkedToElement="true" SpecificationElementType="dk.dtu.imm.red.specificationelements.goal.Goal"/>
        </Elements>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_8LPoUFeEEeWFXJq3apS_nA" Target="_7uurcFeEEeWFXJq3apS_nA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="Supporting" Type="Supporting"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_8eFEwFeEEeWFXJq3apS_nA" Target="_8LPoUFeEEeWFXJq3apS_nA" Direction="SourceTarget" TargetDecoration="ArrowHead" Name="Supporting" Type="Supporting"/>
      </VisualDiagram>
    </contents>
    <contents xsi:type="goal_1:Goal" label="G3" name="Convenient handling of appointments" elementKind="Solution" description="" timeCreated="2015-09-10T08:25:38.835+0200" lastModified="2015-09-10T09:53:47.698+0200" uniqueID="9df83440-b1b7-4187-8c95-b91e60e27a26" workPackage="">
      <creator name="" timeCreated="2015-09-10T08:25:38.835+0200" uniqueID="52c196cd-59d8-4d98-a121-20e2ae2c52c4" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T08:28:15.569+0200" uniqueID="67a8799e-646b-4651-b130-a19bd951dc0b">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text=""/>
      </explanation>
    </contents>
    <contents xsi:type="goal_1:Goal" label="G2" name="high-quality healthcare for all citizens" elementKind="Business" description="" timeCreated="2015-09-10T08:24:45.721+0200" lastModified="2015-09-10T09:53:47.667+0200" uniqueID="85582111-834c-4a12-be31-0c7256245a7c" workPackage="">
      <creator name="" timeCreated="2015-09-10T08:24:45.721+0200" uniqueID="a13992c0-4317-418a-95a0-17973e1c498d" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T08:28:37.961+0200" uniqueID="f37d893d-07ed-4946-b575-b1010cf91c07">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text=""/>
      </explanation>
    </contents>
    <contents xsi:type="goal_1:Goal" label="G1" name="Demonstrate benefits of EU integration to all citizens" elementKind="Strategic" description="" timeCreated="2015-09-10T08:24:22.082+0200" lastModified="2015-09-10T09:53:47.634+0200" uniqueID="f77f8674-6591-4b66-8206-dd2357886768" workPackage="">
      <creator name="" timeCreated="2015-09-10T08:24:22.082+0200" uniqueID="6f5a69d0-fc03-4d09-b6e3-6b4a4b247bb6" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T08:26:17.347+0200" uniqueID="e8a7ee7f-4425-4ccc-adf1-90ab6d1b2ae1">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text=""/>
      </explanation>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" label="GP" name="General Practitioner" description="" timeCreated="2015-09-11T12:41:11.452+0200" lastModified="2015-09-11T12:41:11.478+0200" uniqueID="9f399fab-db0c-4703-86a9-7af35b98b446">
      <creator name="" timeCreated="2015-09-11T12:41:11.452+0200" uniqueID="ec3239c3-b1cb-42e7-8bcc-da2ced728d32" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="persona:Persona" label="sdd" name="sdsd" description="" timeCreated="2015-10-26T08:19:45.961+0100" lastModified="2015-10-26T08:19:45.976+0100" uniqueID="a80175dc-2ea3-40c5-9e9f-cc7e515303c2">
      <creator name="" timeCreated="2015-10-26T08:19:45.961+0100" uniqueID="6d135bae-e9bd-4e0b-ad8d-f824a0e12b88" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
  </contents>
  <contents xsi:type="folder:Folder" name="Context" timeCreated="2015-09-10T09:55:05.953+0200" lastModified="2015-09-11T12:41:34.704+0200" uniqueID="f74396e2-68c1-4425-b53f-2535d54ec23b">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="configuration:Actor" label="PAT" name="Patient" elementKind="unspecified" description="One of the GP's registered patients." timeCreated="2015-09-10T08:09:03.292+0200" lastModified="2015-09-11T09:55:33.997+0200" uniqueID="a694c110-13e7-4fb1-b269-9c429ba30bb6" workPackage="" code="">
      <creator name="" timeCreated="2015-09-10T08:09:03.293+0200" uniqueID="9fa77769-1d95-455e-90de-66f325eba558" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-11T09:55:33.993+0200" uniqueID="0bbc7340-a1a4-49ca-8643-a6a1c356ee06">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="configuration:Actor" iconURI="icons/system.png" label="APM" name="Appointment Manager" elementKind="system" description="A system to handle appointments of patients with the GP's practice." timeCreated="2015-09-10T08:10:00.690+0200" lastModified="2015-09-10T11:14:53.152+0200" uniqueID="ee01cdb0-6d33-4488-bc78-4fa7c4693562" workPackage="" code="">
      <creator name="" timeCreated="2015-09-10T08:10:00.690+0200" uniqueID="d04c5d88-3478-449c-af4e-5c4b4b03ad40" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T08:16:55.691+0200" uniqueID="4fe805ff-0178-48d4-a215-2444b0c55746">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="visualmodel:Diagram" label="CTXT" name="Context Diagram" elementKind="unspecified" description="" timeCreated="2015-09-10T08:15:47.486+0200" lastModified="2015-09-10T11:14:53.116+0200" uniqueID="af9edbe9-e4e3-4c77-bfcb-7b4d8f494d73" workPackage="">
      <creator name="" timeCreated="2015-09-10T08:16:59.924+0200" uniqueID="7a2a2941-72f9-420e-b35d-45f12a21b61e">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T08:16:59.925+0200" uniqueID="4b4bced6-9586-42ad-8bc9-5ff24b0747ac">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.1/@contents.2/@VisualDiagram" DiagramType="Context">
        <Elements xsi:type="context:VisualContextActor" Location="436,308" Bounds="60,70" Parent="//@contents.1/@contents.2/@VisualDiagram" Diagram="//@contents.1/@contents.2/@VisualDiagram" Connections="//@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.0 //@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.1 //@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.3 //@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.6" Name="Patient" visualID="_Y2jBEFeDEeWFXJq3apS_nA" SpecificationElement="a694c110-13e7-4fb1-b269-9c429ba30bb6" IsLinkedToElement="true" SpecificationElementType="dk.dtu.imm.red.specificationelements.configuration.Actor"/>
        <Elements xsi:type="context:VisualSystem" Location="343,36" Bounds="404,200" Parent="//@contents.1/@contents.2/@VisualDiagram" Diagram="//@contents.1/@contents.2/@VisualDiagram" Name="GP-Appointments" visualID="_aoI6gFeDEeWFXJq3apS_nA">
          <Elements xsi:type="context:VisualPort" Location="90,170" Bounds="69,30" Parent="_aoI6gFeDEeWFXJq3apS_nA" Diagram="//@contents.1/@contents.2/@VisualDiagram" Connections="//@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.0" Name="PAT-APM" visualID="_bYdkMFeDEeWFXJq3apS_nA"/>
          <Elements xsi:type="context:VisualPort" Location="318,170" Bounds="69,30" Parent="_aoI6gFeDEeWFXJq3apS_nA" Diagram="//@contents.1/@contents.2/@VisualDiagram" Connections="//@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.2" IsSketchy="true" Name="GP-ADM" visualID="_3h0HoFebEeWFXJq3apS_nA"/>
        </Elements>
        <Elements xsi:type="context:VisualSystem" Location="364,491" Bounds="200,200" Parent="//@contents.1/@contents.2/@VisualDiagram" Diagram="//@contents.1/@contents.2/@VisualDiagram" Name="Electronic Patient Journal" visualID="_JH0xgFebEeWFXJq3apS_nA">
          <Elements xsi:type="context:VisualPort" Location="170,83" Bounds="30,30" Parent="_JH0xgFebEeWFXJq3apS_nA" Diagram="//@contents.1/@contents.2/@VisualDiagram" Connections="//@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.4" IsSketchy="true" Name="EPJ-PHY" visualID="_585C8FebEeWFXJq3apS_nA"/>
          <Elements xsi:type="context:VisualPort" Location="0,49" Bounds="30,30" Parent="_JH0xgFebEeWFXJq3apS_nA" Diagram="//@contents.1/@contents.2/@VisualDiagram" Connections="//@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.5" IsSketchy="true" Name="EPJ-HI" visualID="_6iH28FebEeWFXJq3apS_nA"/>
          <Elements xsi:type="context:VisualPort" Location="170,37" Bounds="30,30" Parent="_JH0xgFebEeWFXJq3apS_nA" Diagram="//@contents.1/@contents.2/@VisualDiagram" Connections="//@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="PAT-EPJ" visualID="_DyJQ0FecEeWFXJq3apS_nA"/>
        </Elements>
        <Elements xsi:type="context:VisualContextActor" Location="232,305" Bounds="60,70" Parent="//@contents.1/@contents.2/@VisualDiagram" Diagram="//@contents.1/@contents.2/@VisualDiagram" Connections="//@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.1 //@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.5" IsSketchy="true" Name="Health Insurance" visualID="_cGroMFebEeWFXJq3apS_nA" SpecificationElementType="dk.dtu.imm.red.specificationelements.configuration.Actor" actorKind="Organisation"/>
        <Elements xsi:type="context:VisualContextActor" Location="664,308" Bounds="60,70" Parent="//@contents.1/@contents.2/@VisualDiagram" Diagram="//@contents.1/@contents.2/@VisualDiagram" Connections="//@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.2 //@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.4 //@contents.1/@contents.2/@VisualDiagram/@DiagramConnections.6" IsSketchy="true" Name="Physician" visualID="_2NaggFebEeWFXJq3apS_nA" SpecificationElementType="dk.dtu.imm.red.specificationelements.configuration.Actor"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_bYdkMFeDEeWFXJq3apS_nA" Target="_Y2jBEFeDEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_cGroMFebEeWFXJq3apS_nA" Target="_Y2jBEFeDEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_3h0HoFebEeWFXJq3apS_nA" Target="_2NaggFebEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_Y2jBEFeDEeWFXJq3apS_nA" Target="_DyJQ0FecEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association">
          <Bendpoints>595,443</Bendpoints>
          <Bendpoints>596,541</Bendpoints>
        </DiagramConnections>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_2NaggFebEeWFXJq3apS_nA" Target="_585C8FebEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association">
          <Bendpoints>697,588</Bendpoints>
        </DiagramConnections>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_cGroMFebEeWFXJq3apS_nA" Target="_6iH28FebEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association">
          <Bendpoints>262,554</Bendpoints>
        </DiagramConnections>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_Y2jBEFeDEeWFXJq3apS_nA" Target="_2NaggFebEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
      </VisualDiagram>
    </contents>
    <contents xsi:type="configuration:Actor" iconURI="icons/port.png" label="PAT-APM" name="Call-in interface" elementKind="port" description="" timeCreated="2015-09-10T08:17:25.901+0200" lastModified="2015-09-10T11:14:53.073+0200" uniqueID="4f23dd54-814c-43b5-9c87-c04f51080b31" workPackage="" code="">
      <creator name="" timeCreated="2015-09-10T08:17:25.901+0200" uniqueID="8f1f8a05-c9ad-407d-afd9-bc84b53270a7" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T08:17:32.395+0200" uniqueID="a8127189-d758-4171-8363-10f604124315">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="configuration:Configuration" label="CFG" name="Healthcare for all" elementKind="unspecified" partOf="9df83440-b1b7-4187-8c95-b91e60e27a26" description="" timeCreated="2015-09-10T11:17:05.515+0200" lastModified="2015-09-10T11:27:29.019+0200" uniqueID="73e2413e-da49-4238-bfc6-23b950994baf" workPackage="" parts="ee01cdb0-6d33-4488-bc78-4fa7c4693562" ports="4f23dd54-814c-43b5-9c87-c04f51080b31">
      <creator name="" timeCreated="2015-09-10T11:17:05.515+0200" uniqueID="54660759-f0f7-41cb-ad49-7bd4d624739a" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T11:27:29.011+0200" uniqueID="5c692c80-21a4-44a0-9140-cf073427f3a2">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="configuration:Configuration" label="SL1" name="APM System" elementKind="unspecified" partOf="ee01cdb0-6d33-4488-bc78-4fa7c4693562" description="" timeCreated="2015-09-10T11:26:28.611+0200" lastModified="2015-09-10T11:27:28.982+0200" uniqueID="a48e521b-ba69-4915-b090-8f5d56f4a9aa" workPackage="">
      <creator name="" timeCreated="2015-09-10T11:26:28.611+0200" uniqueID="9a779726-8e76-44af-a5a5-2e5598f462f3" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T11:27:28.974+0200" uniqueID="341e1acf-dd88-424d-bf81-03d6b2f9e9bc">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </contents>
    <contents xsi:type="ucScenario:Scenario" label="S12" name="Get Appointment" elementKind="unspecified" description="" timeCreated="2015-09-11T10:43:33.744+0200" lastModified="2015-09-11T10:55:37.281+0200" uniqueID="799d478c-0583-4f88-9e2d-6906b7e32473" workPackage="">
      <creator name="" timeCreated="2015-09-11T10:43:33.744+0200" uniqueID="42e7c212-a19c-4d7f-8391-3ceca7f6d216" id="" email="" initials="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-11T10:45:37.191+0200" uniqueID="54cec705-05a3-4ecb-933a-c4d1a7374891">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <startConnector xsi:type="ucScenario:Connector" name="Start connector">
        <connections xsi:type="ucScenario:Action" parent="//@contents.1/@contents.6/@startConnector" name="Start" type="Start"/>
        <connections xsi:type="ucScenario:Action" parent="//@contents.1/@contents.6/@startConnector" name="Call GP" type="Send"/>
        <connections xsi:type="ucScenario:Action" description="In Denmark, people identify to their GP by their CPR-number" parent="//@contents.1/@contents.6/@startConnector" name="Identify"/>
        <connections xsi:type="ucScenario:Action" parent="//@contents.1/@contents.6/@startConnector" name="Describe problem"/>
        <connections xsi:type="ucScenario:Action" parent="//@contents.1/@contents.6/@startConnector" name="Ask for appointment &amp; propose a time"/>
        <connections xsi:type="ucScenario:Connector" parent="//@contents.1/@contents.6/@startConnector" name="Check availability" type="Alternate" guard="practice declines">
          <connections xsi:type="ucScenario:Action" parent="//@contents.1/@contents.6/@startConnector/@connections.5" name="Offer altrnative"/>
        </connections>
        <connections xsi:type="ucScenario:Connector" parent="//@contents.1/@contents.6/@startConnector" name="check availability" type="Alternate" guard="practice accpets">
          <connections xsi:type="ucScenario:Action" parent="//@contents.1/@contents.6/@startConnector/@connections.6" name="Receive electronic appointment " type="Receive"/>
        </connections>
        <connections xsi:type="ucScenario:Action" parent="//@contents.1/@contents.6/@startConnector" name="Stop" type="Stop"/>
      </startConnector>
    </contents>
  </contents>
  <contents xsi:type="folder:Folder" name="Processes" timeCreated="2015-09-10T11:15:06.881+0200" lastModified="2015-09-10T11:15:14.396+0200" uniqueID="a385a97f-b536-42de-9d5f-50274932e8b9">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="visualmodel:Diagram" label="UC1" name="create appointment" elementKind="unspecified" description="" timeCreated="2015-09-10T09:02:17.553+0200" lastModified="2015-09-10T11:15:14.396+0200" uniqueID="20777d09-b0e2-46db-b11d-b30ed7d29a66" workPackage="">
      <creator name="" timeCreated="2015-09-10T09:07:23.053+0200" uniqueID="b1f64a50-11a9-495c-a297-733af62adde6">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2015-09-10T09:07:23.054+0200" uniqueID="d67b5f67-935c-4187-822a-48b0775314ad">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.2/@contents.0/@VisualDiagram" DiagramType="UseCase">
        <Elements xsi:type="usecase:VisualActorElement" Location="99,223" Bounds="80,150" Parent="//@contents.2/@contents.0/@VisualDiagram" Diagram="//@contents.2/@contents.0/@VisualDiagram" Connections="//@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.0 //@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.1 //@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.2" Name="Patient" visualID="_4i2MAFeJEeWFXJq3apS_nA" SpecificationElement="a694c110-13e7-4fb1-b269-9c429ba30bb6" IsLinkedToElement="true" SpecificationElementType="dk.dtu.imm.red.specificationelements.configuration.Actor"/>
        <Elements xsi:type="usecase:VisualSystemBoundaryElement" Location="364,154" Bounds="212,396" Parent="//@contents.2/@contents.0/@VisualDiagram" Diagram="//@contents.2/@contents.0/@VisualDiagram" Name="APM" visualID="_6BfUQFeJEeWFXJq3apS_nA">
          <Elements xsi:type="usecase:VisualUseCaseElement" Location="25,62" Bounds="150,50" Parent="_6BfUQFeJEeWFXJq3apS_nA" Diagram="//@contents.2/@contents.0/@VisualDiagram" Connections="//@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.0 //@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.3" IsSketchy="true" Name="create appointment" visualID="_6y_psFeJEeWFXJq3apS_nA" SpecificationElementType="dk.dtu.imm.red.specificationelements.usecase.Usecase"/>
          <Elements xsi:type="usecase:VisualUseCaseElement" Location="31,136" Bounds="150,50" Parent="_6BfUQFeJEeWFXJq3apS_nA" Diagram="//@contents.2/@contents.0/@VisualDiagram" Connections="//@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.1 //@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.4" IsSketchy="true" Name="cancel appointment" visualID="_XajgAFeKEeWFXJq3apS_nA" SpecificationElementType="dk.dtu.imm.red.specificationelements.usecase.Usecase"/>
          <Elements xsi:type="usecase:VisualUseCaseElement" Location="27,202" Bounds="150,50" Parent="_6BfUQFeJEeWFXJq3apS_nA" Diagram="//@contents.2/@contents.0/@VisualDiagram" Connections="//@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.2 //@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.5" IsSketchy="true" Name="move appointment" visualID="_Y_YIwFeKEeWFXJq3apS_nA" SpecificationElementType="dk.dtu.imm.red.specificationelements.usecase.Usecase"/>
          <Elements xsi:type="usecase:VisualUseCaseElement" Location="32,272" Bounds="150,50" Parent="_6BfUQFeJEeWFXJq3apS_nA" Diagram="//@contents.2/@contents.0/@VisualDiagram" Connections="//@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.6" IsSketchy="true" Name="make series of appointments" visualID="_aeZEcFeKEeWFXJq3apS_nA" SpecificationElementType="dk.dtu.imm.red.specificationelements.usecase.Usecase"/>
        </Elements>
        <Elements xsi:type="usecase:VisualActorElement" Location="686,271" Bounds="80,150" Parent="//@contents.2/@contents.0/@VisualDiagram" Diagram="//@contents.2/@contents.0/@VisualDiagram" Connections="//@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.3 //@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.4 //@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.5 //@contents.2/@contents.0/@VisualDiagram/@DiagramConnections.6" IsSketchy="true" Name="Practice Assistant" visualID="_dfOYMFeKEeWFXJq3apS_nA" SpecificationElementType="dk.dtu.imm.red.specificationelements.configuration.Actor"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_4i2MAFeJEeWFXJq3apS_nA" Target="_6y_psFeJEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_4i2MAFeJEeWFXJq3apS_nA" Target="_XajgAFeKEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_4i2MAFeJEeWFXJq3apS_nA" Target="_Y_YIwFeKEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_dfOYMFeKEeWFXJq3apS_nA" Target="_6y_psFeJEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_dfOYMFeKEeWFXJq3apS_nA" Target="_XajgAFeKEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_dfOYMFeKEeWFXJq3apS_nA" Target="_Y_YIwFeKEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
        <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_dfOYMFeKEeWFXJq3apS_nA" Target="_aeZEcFeKEeWFXJq3apS_nA" Direction="Bidirectional" Name="" Type="Association"/>
      </VisualDiagram>
    </contents>
  </contents>
  <contents xsi:type="vision:Vision" label="VISION" name="Healthcare for all citizens" description="" timeCreated="2015-09-11T12:42:32.308+0200" lastModified="2015-09-11T12:42:32.326+0200" uniqueID="27ca93ff-823b-4f8d-8349-a825338057fb">
    <creator name="" timeCreated="2015-09-11T12:42:32.309+0200" uniqueID="11687fac-1476-4696-8dfa-0a12880c4657" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </contents>
  <longDescription>
    <fragments xsi:type="text:FormattedText" text=""/>
  </longDescription>
</file:File>
