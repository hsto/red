<?xml version="1.0" encoding="ASCII"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:assumption="dk.dtu.imm.red.specificationelements.assumption" xmlns:configuration="dk.dtu.imm.red.specificationelements.configuration" xmlns:document="dk.dtu.imm.red.core.element.document" xmlns:file="dk.dtu.imm.red.core.file" xmlns:goal="dk.dtu.imm.red.specificationelements.goal" xmlns:persona="dk.dtu.imm.red.specificationelements.persona" xmlns:ucScenario="dk.dtu.imm.red.specificationelements.ucscenario" xmlns:usecase="dk.dtu.imm.red.specificationelements.usecase" xmlns:usecase_1="dk.dtu.imm.red.visualmodeling.visualmodel.usecase" xmlns:visualmodel="dk.dtu.imm.red.visualmodeling" name="f1.red" timeCreated="2014-09-15T15:48:20.026+0200" lastModified="2015-09-07T15:49:36.805+0200" uniqueID="87883a03-6fbc-4cae-987d-8d011d7a924f">
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="goal:Goal" label="goal" name="1" description="" timeCreated="2015-09-07T11:40:26.630+0200" lastModified="2015-09-07T11:40:26.630+0200" uniqueID="963b8f36-ff57-43d1-aaaa-328eedab505d">
    <creator name="" timeCreated="2015-09-07T11:40:26.630+0200" uniqueID="bf4a7193-1ce4-4d2f-958d-fa7596848f00" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </contents>
  <contents xsi:type="configuration:Actor" iconURI="icons/port.png" label="actor" name="1" elementKind="port" description="" timeCreated="2015-09-07T11:41:01.372+0200" lastModified="2015-09-07T11:41:16.254+0200" uniqueID="bc16feb7-4842-4cba-aa0c-f44ce41d68cf" workPackage="" code="">
    <creator name="" timeCreated="2015-09-07T11:41:01.372+0200" uniqueID="fa741979-0e79-4459-95fd-e57c30773bb6" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <changeList/>
    <responsibleUser name="" timeCreated="2015-09-07T11:41:16.254+0200" uniqueID="c8890c0c-2324-4c7a-9d42-ba2a60a8f3f1">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </responsibleUser>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </contents>
  <contents xsi:type="assumption:Assumption" label="ss" name="ss" description="" timeCreated="2015-09-07T11:42:02.927+0200" lastModified="2015-09-07T11:42:02.937+0200" uniqueID="174b3ee9-2ed1-4ebc-bf5c-8825fe4af17b">
    <creator name="" timeCreated="2015-09-07T11:42:02.927+0200" uniqueID="fed533de-769b-4805-ad97-81051785231b" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </contents>
  <contents xsi:type="persona:Persona" label="sdsd" name="sdsd" description="" timeCreated="2015-09-07T11:44:10.267+0200" lastModified="2015-09-07T11:44:10.267+0200" uniqueID="37d0c472-0dda-48e1-ae41-0de158a25022">
    <creator name="" timeCreated="2015-09-07T11:44:10.267+0200" uniqueID="b8977a80-c907-44c8-a6a7-76993764c815" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </contents>
  <contents xsi:type="ucScenario:Scenario" label="sdsd" name="sdsd" description="" timeCreated="2015-09-07T11:45:35.975+0200" lastModified="2015-09-07T11:45:35.975+0200" uniqueID="07a8678e-09e9-491e-bf23-8884ae6bb6f1">
    <creator name="" timeCreated="2015-09-07T11:45:35.975+0200" uniqueID="139024ea-a2a9-4544-b94b-b26296c48967" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <startConnector xsi:type="ucScenario:Connector" name="Start connector">
      <connections xsi:type="ucScenario:Action" parent="//@contents.4/@startConnector" name="Start" type="Start"/>
      <connections xsi:type="ucScenario:Action" parent="//@contents.4/@startConnector" name="Stop" type="Stop"/>
    </startConnector>
  </contents>
  <contents xsi:type="document:Document" label="dfdf" name="dfdfd" description="" timeCreated="2015-09-07T11:45:58.008+0200" lastModified="2015-09-07T11:45:58.008+0200" uniqueID="783f3ccb-9cf3-4e23-81c2-bbea6f3773a9">
    <creator name="" timeCreated="2015-09-07T11:45:58.008+0200" uniqueID="745950af-0efb-41bf-ac1e-5a4115f1230b" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </contents>
  <contents xsi:type="configuration:Actor" label="sdsds" name="sdsds" description="" timeCreated="2015-09-07T11:49:46.750+0200" lastModified="2015-09-07T11:49:46.750+0200" uniqueID="18f586a0-ba15-4ab8-a140-a7bb9cb5a427" code="">
    <creator name="" timeCreated="2015-09-07T11:49:46.750+0200" uniqueID="3a51e18c-577b-44db-96a0-e79e47d74182" id="" email="" initials="">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </contents>
  <contents xsi:type="usecase:Usecase" label="dfd" name="dfdfd" description="" timeCreated="2015-09-07T11:50:10.033+0200" lastModified="2015-09-07T11:50:10.033+0200" uniqueID="ba31726c-b822-439c-bb54-3a8b1259958c">
    <cost name="Cost" kind=""/>
    <benefit name="Benefit" kind=""/>
    <PrimaryReferences/>
    <SecondaryReferences/>
    <incidence name="Incidence"/>
    <duration name="Duration"/>
  </contents>
  <contents xsi:type="usecase:Usecase" label="fdf" name="dfdf" description="" timeCreated="2015-09-07T11:51:04.159+0200" lastModified="2015-09-07T11:51:04.159+0200" uniqueID="0926abc5-4b47-45c3-857b-ab7cfe7ffce3">
    <cost name="Cost" kind=""/>
    <benefit name="Benefit" kind=""/>
    <PrimaryReferences/>
    <SecondaryReferences/>
    <incidence name="Incidence"/>
    <duration name="Duration"/>
  </contents>
  <contents xsi:type="visualmodel:Diagram" label="dsgfdgf" name="fdgdg" elementKind="unspecified" description="" timeCreated="2015-09-07T14:04:45.771+0200" lastModified="2015-09-07T14:04:45.812+0200" uniqueID="3b0c57c0-d2c8-47e3-ba3c-2a4e81cd20c6" workPackage="">
    <creator name="" timeCreated="2015-09-07T15:49:36.805+0200" uniqueID="8ff93412-9221-4dbf-aaa0-fec6fa5bd7bd">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <changeList/>
    <responsibleUser name="" timeCreated="2015-09-07T15:49:36.805+0200" uniqueID="2ef8ec8c-a80c-42da-b66f-a7849d737a7e">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </responsibleUser>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <VisualDiagram Location="0,0" Bounds="0,0" Diagram="//@contents.9/@VisualDiagram" DiagramType="UseCase">
      <Elements xsi:type="usecase_1:VisualActorElement" Location="134,211" Bounds="80,150" Parent="//@contents.9/@VisualDiagram" Diagram="//@contents.9/@VisualDiagram" Connections="//@contents.9/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" visualID="_o_0H4FVYEeWAHdUc4r0Cwg"/>
      <Elements xsi:type="usecase_1:VisualActorElement" Location="716,238" Bounds="80,150" Parent="//@contents.9/@VisualDiagram" Diagram="//@contents.9/@VisualDiagram" Connections="//@contents.9/@VisualDiagram/@DiagramConnections.0" IsSketchy="true" visualID="_pb8qQFVYEeWAHdUc4r0Cwg"/>
      <Elements xsi:type="usecase_1:VisualUseCaseElement" Location="404,257" Bounds="150,50" Parent="//@contents.9/@VisualDiagram" Diagram="//@contents.9/@VisualDiagram" Connections="//@contents.9/@VisualDiagram/@DiagramConnections.0 //@contents.9/@VisualDiagram/@DiagramConnections.1" IsSketchy="true" Name="fgfgf" visualID="_pyErQFVYEeWAHdUc4r0Cwg"/>
      <Elements xsi:type="usecase_1:VisualUseCaseElement" Location="460,355" Bounds="150,50" Parent="//@contents.9/@VisualDiagram" Diagram="//@contents.9/@VisualDiagram" IsSketchy="true" Name="fgfg" visualID="_qLWG4FVYEeWAHdUc4r0Cwg"/>
      <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_pb8qQFVYEeWAHdUc4r0Cwg" Target="_pyErQFVYEeWAHdUc4r0Cwg" Direction="Bidirectional" Name="" Type="Association"/>
      <DiagramConnections xsi:type="visualmodel:VisualConnection" Source="_pyErQFVYEeWAHdUc4r0Cwg" Target="_o_0H4FVYEeWAHdUc4r0Cwg" Direction="Bidirectional" Name="" Type="Association"/>
    </VisualDiagram>
  </contents>
</file:File>
