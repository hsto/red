<?xml version="1.0" encoding="ASCII"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:file="dk.dtu.imm.red.core.file" xmlns:folder="dk.dtu.imm.red.core.folder" xmlns:glossary="dk.dtu.imm.red.glossary" xmlns:relationship="dk.dtu.imm.red.core.element.relationship" xmlns:stakeholder="dk.dtu.imm.red.specificationelements.stakeholder" xmlns:text="dk.dtu.imm.red.core.text" name="Stakeholders.red" timeCreated="2014-07-10T13:36:46.166+0200" lastModified="2014-11-15T11:37:10.533+0100" uniqueID="6b324177-e270-49c5-b403-cacf6ff10064" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="folder:Folder" name="Stakeholders" timeCreated="2011-11-16T14:39:35.744+0100" lastModified="2014-07-10T13:36:53.454+0200" uniqueID="e92cbece-93e3-4137-98e2-8c6a8e6e1029" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" specialType="Stakeholder">
    <creator name="" timeCreated="2014-08-09T11:57:19.715+0200" uniqueID="f6455807-3ca0-47f8-bb26-b1264db7a171" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <changeList/>
    <responsibleUser name="" timeCreated="2014-08-09T11:57:19.719+0200" uniqueID="b67b2617-902f-41e6-b8ee-327405ccc9fa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </responsibleUser>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="stakeholder:Stakeholder" name="T&#xe5;rb&#xe6;k Commune" lifecycleStatus="incomplete" timeCreated="2011-11-16T14:41:16.704+0100" lastModified="2012-02-01T15:21:05.405+0100" uniqueID="3c70881e-c050-42d6-b12d-62ea48f9f11f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" exposure="3" importance="2">
      <commentlist/>
      <creator name="Anders" timeCreated="2011-11-16T14:42:47.085+0100" uniqueID="5609af61-1621-4b1e-9ff5-aeaf06fb7b8b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T14:42:47.090+0100" uniqueID="29a22b0a-8f48-4015-bd45-b9c9754a38a8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>T&#xe5;rb&#xe6;k Commune has mainly two stakes in this project: (1) adding to the town&#x2019;s attractiveness as a place to live, in particular to those of its inhabitants (particularly those between 6 and 35 years of age, which are also more likely to move away) so as to increase the town&#x2019;s population; and (2) help improve the local economy by closer networking and cross-fertilization.&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>We suggest that this stakeholder should be receiving regular official written reports to provide evidence of the project&#x2019;s progress. A suitable interval and time point may be shortly before the monthly town council meetings. The client will have full access to all project internal documents and will be informed about all economically relevant plans and planning activities.&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Additionally, regular personal meetings between the project leader on the on hand side and the major, the Commune council, and the culture committee on the other will provide ample opportunity to demonstrate project progress, raise issues and questions, and communicate any problems early. &lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>In particular, it will be important to include one or more people close to the major in the project staff as a client-supplier &lt;/SPAN>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>liaison officer to form &lt;/SPAN>&lt;SPAN lang=EN-US>another, informal channel from the project to the client. This person may become a project sponsor in the Commune administration, and may act as a channel to signal potential problems very early to evaluate possible reactions. This person or persons may be part of the project controlling board and or the project steering board.&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Executives / Owner" lifecycleStatus="complete" timeCreated="2011-11-16T14:58:10.746+0100" lastModified="2011-12-27T17:20:02.118+0100" uniqueID="7a91f6e9-f0d3-459c-9e3a-3b381191d0a8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" exposure="2" power="3" urgency="3" importance="3">
      <commentlist/>
      <creator name="Anders" timeCreated="2011-11-16T15:21:50.873+0100" uniqueID="8a9eb9b8-381b-4c0e-aae6-7d5fe2a54243" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList>
        <comments timeCreated="2011-11-16T15:27:05.312+0100" uniqueID="a227b3a6-67cd-4415-94e1-302dc86a71e2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <author timeCreated="2011-11-16T15:27:05.312+0100" uniqueID="33522305-4dd0-4d6e-8973-0ae17317bd8c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </author>
        </comments>
      </changeList>
      <responsibleUser name="Jakob" timeCreated="2011-11-16T15:21:50.877+0100" uniqueID="b19a5b02-4641-4a4b-b700-a8d78443858e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>The stakes of the owners and the chief executives of the supplier will typically be almost identical, so we will refer to them as the supplier collectively. Primarily, the supplier &lt;/SPAN>&lt;SPAN lang=EN-US>wants to earn money, win market shares, expand its customer base, and want the company to prosper and grow. Its reputation among potential customers is a valuable asset, but the company is more important than this one project. So, should serious problems arise, the costs and benefits will be weighed, and if necessary they will consider terminating the contract with a loss.&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>The &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> project consists of three parts: an initial inception project billed on effort with a profitable hourly rate, a requirements analysis project which is also billed on effort but uses an hourly rate which only covers cost and is also capped, and a competitively calculated fixed price for the development as such. With this construction, the inception is self-sufficient, the development must be outsourced, and the analysis must either be an investment to enter the market and learn about the domain as a company, or to make profit from selling the product to other customers after the initial deployment. For this to happen, requirements beyond those found in T&#xe5;rb&#xe6;k must be considered. However, the initial customer is needed as a show-case and proof of concept for the product, so the overall requirements may contain more than those from T&#xe5;rb&#xe6;k, but not less.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>No specific activities necessary beyond the usual project controlling.&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Project Manager" timeCreated="2011-11-16T15:32:53.675+0100" lastModified="2011-11-16T15:35:01.876+0100" uniqueID="27218136-b608-4ff7-bab3-983d7fb8deba" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" exposure="2" power="3" urgency="3" importance="3">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T15:34:26.489+0100" uniqueID="925ee7f6-8997-44e5-a0bb-2abfd77919f6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T15:34:26.490+0100" uniqueID="64ed482f-c589-45e2-a0b1-430b4db9eb01" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The stakes of the supplier project lead are mainly of personal or professional nature, i.e. to secure and promote his career, to grow professionally and earn more experience, and to enjoy the personal satisfaction of a successful project. Potentially, the quality of his job may make itself seen in terms of remuneration, too (e.g. a bonus).&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Providing personal financial incentives may be a good way to prevent a skilled project manager from leaving a project for motivational reasons, but providing him with the necessary means and leeway to achieve the best possible outcome is typically more important, so no extra engagement measure need to be provided.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Chief Librarians" timeCreated="2011-11-16T15:45:58.728+0100" lastModified="2012-01-03T09:32:11.061+0100" uniqueID="20f5974f-cc93-4ac6-b1ba-95438deeb53f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" exposure="3" power="3" urgency="2" importance="3">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T15:56:03.608+0100" uniqueID="99e83531-279c-4283-8866-6a2d11052905" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <relatesTo xsi:type="relationship:ElementReference" timeCreated="2012-01-03T09:32:11.039+0100" uniqueID="53f5e933-0ca8-47a7-8e37-070af5e7baad" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" relevance="">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <toElement xsi:type="glossary:GlossaryEntry" href="Glossary.red#//@contents.0/@contents.1"/>
      </relatesTo>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T15:56:03.612+0100" uniqueID="852b3418-4864-4efc-ae5a-fc8ef3f6f912" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The new system will heavily influence the work of the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>, although it is unlikely to threaten their jobs (which involve many activities that cannot and will not be automated). However, some of the jobs of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> they supervise may be in danger which could both decrease their importance and may threaten people that they feel responsible for.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>While it is extremely important to win the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A> over for the project, it is important not to overload them. While the project is unfolding, they still have their regular work to do, and they might be more interested in getting that done first. So, while clearly being the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> with the best overview, it may be better to involve them only in the decision making and ask them to nominate some of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> to assist with requirements elicitation instead of the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Readers" timeCreated="2011-11-16T17:16:43.192+0100" lastModified="2011-12-27T17:10:54.546+0100" uniqueID="b5798927-0042-4dab-bf5b-e8fddfdf844b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" exposure="3" power="3" importance="2">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T17:17:01.219+0100" uniqueID="718524ca-e5f8-4daf-9db0-31afb619e818" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T17:17:01.224+0100" uniqueID="84424ddf-7c71-4952-9401-698527e21c61" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> are interested in getting the typical library services in a friendly, competent, and fast way, at an affordable price. Innovative services like online access that the T&#xe5;rb&#xe6;k library may provide as a consequence of the &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> development project may be welcome but will probably not be asked for specifically by the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A>.&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Being the constituents of T&#xe5;rb&#xe6;k Commune, the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> have great interest in the project success, although they may not even be aware of it.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text=""/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Developers" timeCreated="2011-11-16T17:20:17.472+0100" lastModified="2011-11-16T17:21:36.796+0100" uniqueID="2fd80d5c-f8bf-40a6-97d7-6e751f81dfe9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" urgency="3" importance="2">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T17:21:36.770+0100" uniqueID="e046db3e-1f21-4cf8-b5a4-58a1c693e983" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T17:21:36.774+0100" uniqueID="f2bba3af-5dfc-475f-8d46-050ff3e035a8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Each developer has professional stakes in the project keeping him busy for a prolonged period, i.e. job satisfaction, job security, professional experience, and career advancement. Possibly, pecuniary incentives like a bonus also depend on the project.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The normal methods of motivating the project staff apply, e.g. regular stand up meetings, a project blog, or posters on the project progress and main artifacts on the walls of the corridors at the development premises.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Librarians" timeCreated="2011-11-16T17:22:04.064+0100" lastModified="2011-11-23T09:43:45.408+0100" uniqueID="c80841d0-615c-49d0-99fe-38567f61c54e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" exposure="3" power="2" urgency="2" importance="2">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T17:23:21.317+0100" uniqueID="f1ffa4a1-2faf-4b0a-b6c1-e69aa0ea8619" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T17:23:21.321+0100" uniqueID="a4a24d1f-68eb-4a2d-b055-eae2d74d0b17" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>&lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>Librarians&lt;/A> are interested in improving or at least maintaining their work environment. Depending on the individuals, they may or may not appreciate change as such, but they would almost certainly appreciate if they can offer better services to their customers and have more time available to help them and make their job easier. The &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> have a great interest in securing their jobs.&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> are the most intensive users of the library system and so they also know a great deal about its usage, in particular the bread-and-butter functions. They may, however, lack the overview and experience expected from, e.g., the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>. We therefore chose not to directly involve &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> in the project, unless they are delegated for a particular function by the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>. On the one hand, the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> have a high workload and we believe their input would be not of very high quality. On the other hand, the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A> understand the needs of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> very well because they have all served as &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> for a long time, and are still taking over this role if needed. So, the position of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> is already well represented in the project. In order to secure support from the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A>, it will be vital to convince them that the system will actually increase their job security and long term job satisfaction, even if there are problems in the transitional period and they have to learn new skills.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Future LMS users" timeCreated="2011-11-16T17:25:36.788+0100" lastModified="2011-11-23T09:47:02.213+0100" uniqueID="72878282-6fdb-44bb-b9a5-7accc031caae" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" exposure="3" power="2" importance="2">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T17:32:35.270+0100" uniqueID="7f78c356-de67-42da-a321-df840b0e3f69" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T17:32:35.274+0100" uniqueID="6836fa07-cd12-4b9b-a989-0a1bb5323890" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P>&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>The stakes of future &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> users are naturally unknown, but presumably similar to those of the current clients and the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>We believe that the &lt;A href=&quot;56df337a-c4a6-4441-808a-3bb21ccc8a54&quot;>chief librarians&lt;/A> of &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A> provide us with a good account of the domain requirements that are likely to be very similar for other libraries of the same profile (size, audience). One way to ensure adequate consideration of such requirements is to hire a &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarian&lt;/A> with a lot of job experience in different types of libraries to serve as a consultant to the requirements team. Another way is to visit trade fairs relevant to librarians and library owners to collect other requirements from the market early on. This may also act both as a first step to marketing the product.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Maintenance &amp; Operations" timeCreated="2011-11-16T17:34:09.312+0100" lastModified="2011-11-16T17:56:39.285+0100" uniqueID="11773630-0ea9-4f78-83dc-50d48eb60aea" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="" exposure="3" power="2" importance="2">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T17:34:27.944+0100" uniqueID="8b72ff36-95f3-4112-9310-80bbc377631f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T17:34:27.947+0100" uniqueID="f2c84621-0afe-4084-96b9-92b7226326ee" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>Given that the system is rather small and required to be easy to maintain by definition, the operations personnel will have no trouble with &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A>. The maintainers are interested in the technical quality of the product that will make their job easier. Since the maintenance staff of the supplier is identical to (part of) the development team, technological and learning issues should be no problem, either.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN style=&quot;mso-fareast-language: DE&quot; lang=EN-US>The &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> will require very little maintenance and none on behalf of the client. On behalf of the supplier, maintenance will be crucial, however, in order to be able to cover future requirements from new customers and changing market demands. &lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Local Media Retailers" timeCreated="2011-11-16T17:57:39.702+0100" lastModified="2011-12-27T17:11:13.679+0100" uniqueID="d84e6313-d0af-401d-9e5f-8fbb76bebd4e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T18:00:20.426+0100" uniqueID="db95dedc-6ded-4d7b-9e95-0c14f5a54feb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T18:00:20.430+0100" uniqueID="1460d275-9d54-4f27-93d5-2f62cba39c9c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Retailers are interested in profits, non-profit cultural institutions are interested in widening their customer base so as to justify being subsidized in the future (and more profits that reduce the needed subsidies are also welcome). Common marketing and co-operations to provide better services to customers will contribute to these goals. &lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Take as an example some price-winning French novel that has recently been turned into a film. When the fil is shown in the cinema, the book may also be displayed prominently in the library and in the book shop. The literature caf&#xe9; may organize a reading session with the author, and Folkeskolen may advertise its French classes to be able to read the book in the original. Even other retailers may jump on the band wagon and have French wine, cheese, bread etc. on offer.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The best way to ensure the support of this group of stakeholders is to inform them on the basis of a kind of round table or advisory board which also includes members of the general public.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="General Public" timeCreated="2011-11-16T18:00:45.109+0100" lastModified="2011-12-27T17:11:20.464+0100" uniqueID="c0775ecb-76c3-4ff1-9f47-8d058889455a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T18:01:26.962+0100" uniqueID="5b147d08-dc49-412e-82d4-889d754ce929" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T18:01:26.965+0100" uniqueID="31648ef8-a287-4c16-85e7-62c9ab3fef7e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>The general public is interested in wise spending of its tax money, and may benefit from the plan as drafted by the client.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Other than by the political representation and the &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A>, no further engagement with the general public is planned.&lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
    <contents xsi:type="stakeholder:Stakeholder" name="Cooperating Libraries" timeCreated="2011-11-16T18:01:49.436+0100" lastModified="2011-12-27T17:11:25.109+0100" uniqueID="6c14faf4-3e74-4c37-8c9a-8e055d0bea02" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-16T18:02:06.679+0100" uniqueID="e496d945-e45c-43bc-9932-e8e16aa95fda" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-16T18:02:06.682+0100" uniqueID="3892ebdd-e968-445a-84ad-d24b5a9f74d6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Stakeholders.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <stake>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>&#xD;&#xA;&lt;P style=&quot;TEXT-ALIGN: justify; PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>Cooperating libraries want to continue the &lt;A href=&quot;7824c81f-9a75-448f-a574-5f06d8316941&quot;>media&lt;/A> and catalog data exchange programs as before. &lt;FONT face=Calibri>&lt;FONT size=3>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/FONT>&lt;/FONT>&lt;/SPAN>&lt;/P>&lt;/SPAN>"/>
      </stake>
      <engagement>
        <fragments xsi:type="text:FormattedText" text="&lt;P style=&quot;PAGE-BREAK-AFTER: auto; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot; class=MsoNormal>&lt;SPAN lang=EN-US>None planned&lt;/SPAN>&lt;SPAN style=&quot;mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri; mso-bidi-font-family: Calibri&quot; lang=EN-US>&lt;?xml:namespace prefix = o ns = &quot;urn:schemas-microsoft-com:office:office&quot; />&lt;o:p>&lt;/o:p>&lt;/SPAN>&lt;/P>"/>
      </engagement>
    </contents>
  </contents>
</file:File>
