<?xml version="1.0" encoding="ASCII"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:file="dk.dtu.imm.red.core.file" xmlns:folder="dk.dtu.imm.red.core.folder" xmlns:goal="dk.dtu.imm.red.specificationelements.goal" xmlns:text="dk.dtu.imm.red.core.text" name="Goals.red" timeCreated="2014-07-10T13:37:34.380+0200" lastModified="2014-11-15T11:37:10.613+0100" uniqueID="a289de38-0d3d-4b33-8f03-5420c74bf6d8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
  <creator name="" timeCreated="2014-07-10T14:21:31.638+0200" uniqueID="26a88ea4-7694-4f1c-a4f2-4b2746ba9d8b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </creator>
  <responsibleUser name="" timeCreated="2014-07-10T14:21:31.648+0200" uniqueID="36dbecc9-4825-489b-9c54-42b97a51f6a8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </responsibleUser>
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="folder:Folder" name="Goals" timeCreated="2011-11-16T16:23:17.524+0100" lastModified="2014-07-10T13:38:02.325+0200" uniqueID="706f46e2-ecfe-4ede-96cd-eaf045e90da2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="" specialType="Goal">
    <creator name="" timeCreated="2014-08-10T12:09:19.259+0200" uniqueID="8396a486-d048-4148-932e-191a621986d7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <changeList/>
    <responsibleUser name="" timeCreated="2014-08-10T12:09:19.263+0200" uniqueID="ad9733b0-92cf-4e44-acfd-6a1b1205c85d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </responsibleUser>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="goal:Goal" name="0 - Revive TCL as a social life incubator" timeCreated="2011-11-18T10:25:36.912+0100" lastModified="2011-12-27T16:08:41.122+0100" uniqueID="48d0e4c0-dffe-4608-bf99-92caeb1a0443" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T10:25:51.404+0100" uniqueID="dd74c9c5-f8fc-4dcd-b658-932b3abfbdc9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T10:25:51.407+0100" uniqueID="66892e50-81df-45ec-a167-3baf8d7b9707" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>The overall goal of the &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> project is to free resources (both budget-wise and motivational) to kick-start the lame social and cultural life in the commune. A special focus is on interesting more of the younger generation into the libraries work without losing the traditional customer base. An important aspect is the public image the library projects &#x2013; if the people in the commune consider the library as a commercial enterprise, there is a danger in the long term that the library will have to merge or even close due to financial reasons.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="1 - Increase librarian job satisfaction " timeCreated="2011-11-18T10:44:08.317+0100" lastModified="2011-12-27T16:08:46.733+0100" uniqueID="dd13c195-85ce-4025-a378-855e767dec07" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T10:44:28.655+0100" uniqueID="1970aa3d-3f4c-488b-8044-1e6f62277776" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T10:44:28.656+0100" uniqueID="f48a71d8-61ed-4ff4-83af-a3a1c6d0a025" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>Being on a tight budget, the project needs to efficiently utilize existing resources, and the biggest asset we have is the expertise and network of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A>. It is vital to make them active proponents of change. We critically rely on their enthusiasm and participation.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="1.1 - Reduce amount of repetitive tasks " timeCreated="2011-11-18T10:45:23.438+0100" lastModified="2011-12-27T16:08:50.066+0100" uniqueID="2c9f2b17-8a5c-4c62-984f-e38e8c81f5bd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T10:45:33.123+0100" uniqueID="f50dc21b-32f6-474b-9de2-02066250070e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T10:45:33.126+0100" uniqueID="3af7e998-d4d7-4624-9b6a-b69bd2c49e85" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;P>&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>Initial interviews have shown that currently, &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> have a large work-load but most of these tasks are of a rather simple nature and are not very popular among the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A>. Reducing them helps both in increasing their motivation and freeing time and energy for other, more strategic initiatives.&lt;/SPAN>&lt;/P>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="1.2 - Allow individual variation in job " timeCreated="2011-11-18T10:48:09.885+0100" lastModified="2011-12-27T16:08:53.722+0100" uniqueID="dbd8acb9-23a3-4219-bcd5-a99fa9f1b11e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T10:48:55.448+0100" uniqueID="312acdfd-1e7e-4a17-866a-bf5df1d5a552" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T10:48:55.449+0100" uniqueID="9eea735e-fc60-4f55-baa3-35b986f1ef6c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>Our initial study has also shown that each of the &lt;A href=&quot;a4ed6b0e-2188-418e-ab80-9c98737e0091&quot;>librarians&lt;/A> has their individual style of doing the more advanced tasks, e.g., the sequence of steps. Forcing them into a single path should only be done when necessary for quality management or technical reasons. &lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="2 - Reduce need for subsidies " timeCreated="2011-11-18T10:54:23.137+0100" lastModified="2011-12-27T16:08:56.634+0100" uniqueID="0e0e51aa-cd72-49af-8997-11a8be18c50c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T10:55:25.218+0100" uniqueID="71a69554-a186-4000-b944-e3abb90bdff3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T10:55:25.220+0100" uniqueID="197f8aa5-7889-4d83-93d9-012180ad5f30" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>We cannot possibly increase the annual budget for the &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A>. Outside the investment for the &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> which is subsidized by the National Library Foundation, the DTU, and the new governments education initiative, we must reduce the &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A> cost.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="2.1 - Improve TCL management " timeCreated="2011-11-18T10:56:03.784+0100" lastModified="2011-12-27T16:08:59.831+0100" uniqueID="28a2841e-e1b9-4cc5-9a98-3d2c9bd896f5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T10:56:30.643+0100" uniqueID="6d87bdf3-ec23-4071-95f3-027e06848e1e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T10:56:30.644+0100" uniqueID="57e6099c-b37d-4704-ad62-be146a0e2d43" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>In order to direct the available funds into the right channels, we need to improve the &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A> management, based on accurate and up-to-date data.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="2.2 - Reduce theft &amp; fraud in TCL " timeCreated="2011-11-18T10:57:34.239+0100" lastModified="2011-12-27T16:09:02.634+0100" uniqueID="eaa2fee8-e318-4753-96f3-01b3fcec95cc" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-23T13:33:25.507+0100" uniqueID="e6152347-69b3-464e-9dcb-c4afc465a0ea" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-23T13:33:25.509+0100" uniqueID="36e0976b-6de6-47a5-870b-3c68139d8140" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;P>&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>&lt;FONT size=2 face=Arial>A substantial amount of &lt;/FONT>&lt;A href=&quot;7824c81f-9a75-448f-a574-5f06d8316941&quot;>media&lt;/A>&lt;FONT size=2 face=Arial> goes missing every year. They either have to be replaced at substantial cost, or they are taken out of the &lt;A href=&quot;fe30c141-54b0-458b-9718-5ae2991a11ae&quot;>catalog&lt;/A>, reducing the offering of &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A> and thus the attractiveness of &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A>.&lt;/FONT>&lt;/SPAN>&lt;/P>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="3 - Host many cultural activities at TCL" timeCreated="2011-11-18T11:17:54.943+0100" lastModified="2011-12-27T16:10:50.210+0100" uniqueID="da439811-9b76-4dff-9edd-218d9f7d0977" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T11:18:24.575+0100" uniqueID="eeb0982a-775c-492f-8fd2-ba847ece9ba8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T11:18:24.576+0100" uniqueID="3b5194d8-8d57-434f-9677-2f0d511184e8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>The library offers a location that is well-suited to host readings, picture shows, and permanent exhibitions of paintings, sculptures etc. All of these could be backed up by special promotions on &lt;A href=&quot;7824c81f-9a75-448f-a574-5f06d8316941&quot;>media&lt;/A> the library can offer, such as other books by the same author reading from his/her new book, DVDs and books on a region or topic that is treated in a picture show and so on. All of these require no technical support by the &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A>.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="4 - Increase TCL popularity " timeCreated="2011-11-18T11:20:42.519+0100" lastModified="2011-12-27T16:10:59.004+0100" uniqueID="bc51e4b1-e551-45a5-8df9-c9e640e92434" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T11:21:29.870+0100" uniqueID="c0cc8fe4-eba0-41a6-ae77-a990cebc6a10" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T11:21:29.874+0100" uniqueID="987ba3e1-e798-43f5-ae93-b939fc015abb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>Besides offering interesting events, the &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A> needs to improve its popularity in terms of the acceptance by the people living in the commune. We see mainly two ways to do this.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="4.1 - Provide convenient access to services " timeCreated="2011-11-18T11:22:55.352+0100" lastModified="2011-12-27T16:11:07.494+0100" uniqueID="1a423134-72a7-48fd-b5d8-26ad25900512" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T11:23:41.567+0100" uniqueID="897977b9-3f18-4d3a-9286-c45842a7cd4b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T11:23:41.568+0100" uniqueID="9089e826-70bc-43d2-9f85-691905b9ba2c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>First, we should improve the convenience of the &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A> offerings: right now, there are many people living in the commune that pay taxes and fees subsidizing the &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A>, but who can hardly benefit from the services, e.g., because they have to work during the opening hours. Longer opening hours are too expensive to maintain at current staff levels, but we might reduce the number of people necessary to run the library if we can automate tasks or provide self-service facilities, and we can offer self-service terminals placed outside the library for many operations, including pick-up of &lt;A href=&quot;152c0b4b-1e00-4a27-9ede-8ef435cb9639&quot;>reserved&lt;/A> &lt;A href=&quot;7824c81f-9a75-448f-a574-5f06d8316941&quot;>media&lt;/A>, recommendation systems leading &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> to &lt;A href=&quot;7824c81f-9a75-448f-a574-5f06d8316941&quot;>media&lt;/A> they might enjoy, an ATM-like information terminal, and so on.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="4.2 - Involve reader in TCL operations " timeCreated="2011-11-18T11:24:02.509+0100" lastModified="2011-12-27T16:11:10.191+0100" uniqueID="6b7a24d7-6ef6-4bf8-9c9c-b3ef3c4410d6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T11:25:25.892+0100" uniqueID="e3b1cd64-268a-4efb-bf25-98e916f47833" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T11:25:25.893+0100" uniqueID="a7818ac9-93ca-4d91-99f4-a4d69bc96bfa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>On the other hand, there are many people that are ready to get involved and contribute their time and effort, for instance people after their work life. Their contribution could simultaneously give them a valuable calling, reduce some load from the library, provide better services to other library users, and increase their emotional binding to &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A>. Examples of processes where &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> might be involved include: selecting the &lt;A href=&quot;7824c81f-9a75-448f-a574-5f06d8316941&quot;>media&lt;/A> to be acquired, and suggesting/reviewing new &lt;A href=&quot;7824c81f-9a75-448f-a574-5f06d8316941&quot;>media&lt;/A>.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="5 - Reduce environmental footprint " timeCreated="2011-11-18T11:26:01.861+0100" lastModified="2011-12-27T16:11:13.095+0100" uniqueID="684dfca6-88f2-47ca-af25-c127ef7d58c1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T11:26:35.371+0100" uniqueID="730ab98d-7844-4f84-b9b0-6e5cdcdcf98c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T11:26:35.375+0100" uniqueID="2bd4e39d-210c-4da0-9ef6-1d3867929db1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>Part of the cost problem is due to wasteful processes. Reducing the environmental footprint helps cutting costs and improving the image of &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A>.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="5.1 - Reduce amount of paper mail " timeCreated="2011-11-18T11:27:35.326+0100" lastModified="2011-12-27T16:11:15.702+0100" uniqueID="95dd8545-134b-47a5-b9b1-11cc8bf60146" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T11:27:40.452+0100" uniqueID="20df9f7e-5737-4bca-86f2-bcd7d536fad2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T11:27:40.455+0100" uniqueID="65671cc9-3375-48fc-b665-1b9d9e58f39c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>The library sends out all its communication by paper mail which is very time consuming and costly (print, fold and stuff into envelope, add stamps, take to post office&#x2026;). An initial calculation has shown that almost 9% of the library cost (including work cost) goes into mailing.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="6 - Increase visibility &amp; transparency of operations " timeCreated="2011-11-18T11:28:12.587+0100" lastModified="2011-12-27T16:11:18.581+0100" uniqueID="294cd77a-950d-4b00-b94c-dfd39e034f3d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T11:28:51.907+0100" uniqueID="d5c10580-b56d-4e0b-821e-1be4a2c0e150" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T11:28:51.910+0100" uniqueID="d9819d0b-0711-434a-b473-bb8e76676c41" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>In order to improve the &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A> public image and attract volunteers contributing to the library, we need to make clear what we do, how we do it, and where people could contribute. Only if they see their efforts both appreciated and visible will potential volunteers be motivated.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="6.1 - Provide information about TCL events " timeCreated="2011-11-18T11:29:11.987+0100" lastModified="2011-12-27T16:11:21.108+0100" uniqueID="a027a898-0f20-473e-a0aa-25ad6bb7665f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T11:30:04.416+0100" uniqueID="5d9da0dc-100c-43b6-958d-e34bded6dcbe" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T11:30:04.417+0100" uniqueID="445e1c3b-1cab-4393-982e-040500c5932f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>On the one hand, we need to inform people about the events taking place in the &lt;A href=&quot;a0eb6dab-02d1-4b6e-ae02-12cd694b9437&quot;>TCL&lt;/A>. We must find individual forms of communication suitable for the different customer segments, e.g., Facbook/Twitter for the young, leaflets distributed in the church for churchgoers, posters at the Kindergarden or the public transport for parents.&lt;/SPAN>"/>
      </explanation>
    </contents>
    <contents xsi:type="goal:Goal" name="6.2 - Give fast &amp; accurate status information " timeCreated="2011-11-18T11:30:28.587+0100" lastModified="2011-12-27T16:11:23.875+0100" uniqueID="1cb60663-ed14-4d87-a38d-d12bb968f391" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red" workPackage="">
      <commentlist/>
      <creator name="" timeCreated="2011-11-18T11:30:33.935+0100" uniqueID="aecaaa8f-698d-454b-9025-0de031e91c59" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2011-11-18T11:30:33.939+0100" uniqueID="8801b0f1-9a5d-4fa2-a62e-58393c51c83c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Goals.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <explanation>
        <fragments xsi:type="text:FormattedText" text="&lt;SPAN style=&quot;FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; FONT-SIZE: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: major-fareast; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: major-bidi; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA&quot; lang=EN-US>Another important segment are the current &lt;A href=&quot;82d443f8-6c1b-4d6a-afed-ef28d06ed46e&quot;>readers&lt;/A> &#x2013; our focus group revealed that they expect a speedier update on their &lt;A href=&quot;3297cc6a-f854-4e6b-b19a-19fd2ebc65b8&quot;>readers account&lt;/A>, as they are used such services from their bank these days.&lt;/SPAN>"/>
      </explanation>
    </contents>
  </contents>
</file:File>
