<?xml version="1.0" encoding="ASCII"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:file="dk.dtu.imm.red.core.file" xmlns:text="dk.dtu.imm.red.core.text" xmlns:vision="dk.dtu.imm.red.specificationelements.vision" name="Vision.red" timeCreated="2014-07-10T13:39:54.990+0200" lastModified="2014-11-15T11:37:10.453+0100" uniqueID="97173faa-2176-4792-8b44-bc383e867888" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Vision.red" workPackage="">
  <creator name="" timeCreated="2014-07-10T14:18:26.816+0200" uniqueID="796f9afc-f220-4808-8837-3aedd944d95f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Vision.red">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </creator>
  <responsibleUser name="" timeCreated="2014-07-10T14:18:26.825+0200" uniqueID="85aa2b79-294e-45a2-b075-9d7aa0a9dc12" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Vision.red">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </responsibleUser>
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="vision:Vision" name="LMS Vision" timeCreated="2011-11-16T13:23:27.656+0100" lastModified="2014-08-11T10:27:04.698+0200" uniqueID="2f12b6ae-95a5-4d0c-b559-6b1d3e39fafd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Vision.red" workPackage="">
    <commentlist/>
    <creator name="Anders" timeCreated="2011-11-16T13:23:45.801+0100" uniqueID="00b7f480-6f8c-4eb1-b5ac-64ef67d7959b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Vision.red">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <changeList/>
    <responsibleUser name="" timeCreated="2011-11-16T13:23:45.802+0100" uniqueID="47f7d44d-8d64-44f4-89e8-588c3ee12828" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Vision.red">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </responsibleUser>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <vision>
      <fragments xsi:type="text:FormattedText" text="&lt;SPAN lang=EN-US>&lt;FONT size=3>&lt;FONT face=Calibri>&lt;FONT size=2 face=Arial>&#xD;&#xA;&lt;P class=MsoNormal style=&quot;PAGE-BREAK-AFTER: auto; TEXT-ALIGN: justify; MARGIN: 0cm 0cm 6pt; mso-pagination: none&quot;>&lt;FONT size=3>&lt;FONT face=Calibri>&lt;FONT size=2>&lt;FONT face=Arial>&lt;SPAN lang=EN-US style=&quot;mso-bidi-font-weight: bold&quot;>For &lt;/SPAN>&lt;SPAN lang=EN-US>small to medium sized libraries &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>that&lt;/SPAN> struggle with innovating their service offering &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>the&lt;/SPAN> &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>Library Management System&lt;/A> (&lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A>) &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>is&lt;/SPAN> a new, &lt;/SPAN>&lt;SPAN lang=EN-US style=&quot;mso-bidi-font-family: Calibri&quot;>state of the art &lt;/SPAN>&lt;/FONT>&lt;/FONT>&lt;SPAN lang=EN-US>&lt;FONT size=2 face=Arial>information system &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>that&lt;/SPAN> handles all major library business processes from corpus management and reader handling to lending traffic automation. &lt;A href=&quot;25d7258f-b33d-41dc-a026-06edb2cad3cb&quot;>LMS&lt;/A> allows to integrate traditional customer service with self-service kiosks and on-line access by readers. This helps reducing repetitive tasks of librarians dramatically, and allows to improve existing services in terms of convenience, speed, and reliability, and also adds new services for readers that help address new target groups currently not well reached by classical libraries. We expect a market penetration of 2% of all computer-supported public libraries in Europe within 5 years. &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>In comparison with &lt;/SPAN>manual library management currently in place at many libraries, &lt;SPAN style=&quot;mso-bidi-font-weight: bold&quot;>our product &lt;/SPAN>will reduce administrative costs, free staff capacity for strategic initiatives, and attract more users.&lt;/FONT> &lt;/SPAN>&lt;/FONT>&lt;/FONT>&lt;/P>&lt;/FONT>&lt;/FONT>&lt;/FONT>&lt;/SPAN>"/>
    </vision>
  </contents>
</file:File>
