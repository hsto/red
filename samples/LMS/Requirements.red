<?xml version="1.0" encoding="ASCII"?>
<file:File xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:file="dk.dtu.imm.red.core.file" xmlns:folder="dk.dtu.imm.red.core.folder" xmlns:modelelement="dk.dtu.imm.red.modelelement" xmlns:notation="http://www.eclipse.org/gmf/runtime/1.0.2/notation" xmlns:requirement="dk.dtu.imm.red.specificationelements.requirement" xmlns:text="dk.dtu.imm.red.core.text" name="Requirements.red" timeCreated="2014-07-10T13:37:54.005+0200" lastModified="2014-11-15T11:37:10.623+0100" uniqueID="8f2c3d8b-f5c2-4a05-b672-ea9ccbc53608" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="">
  <creator name="ff" timeCreated="2014-07-10T14:53:54.335+0200" uniqueID="f2012558-54af-471c-b550-42a8f6f36dab" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </creator>
  <responsibleUser name="" timeCreated="2014-07-10T14:53:54.345+0200" uniqueID="49487657-c419-4884-bd0b-1c3048cc1de8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
  </responsibleUser>
  <cost name="Cost" kind=""/>
  <benefit name="Cost" kind=""/>
  <contents xsi:type="folder:Folder" name="Requirements" timeCreated="2011-11-18T10:07:23.270+0100" lastModified="2014-10-14T12:00:31.182+0200" uniqueID="3bcd6f72-901a-462c-a81b-7529569c3271" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
    <creator name="" timeCreated="2014-08-10T12:09:23.581+0200" uniqueID="98ac9530-148f-4431-b9f3-1b3fd1763d80" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </creator>
    <changeList/>
    <responsibleUser name="" timeCreated="2014-08-10T12:09:23.582+0200" uniqueID="8c85358f-6d32-461b-a3b7-8a9021c53868" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
    </responsibleUser>
    <cost name="Cost" kind=""/>
    <benefit name="Cost" kind=""/>
    <contents xsi:type="folder:Folder" name="Corpus" timeCreated="2011-12-09T15:09:23.409+0100" lastModified="2014-10-14T12:00:31.182+0200" uniqueID="5ddef5ac-6045-47b0-958f-ef7fa895ddcd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
      <creator name="" timeCreated="2014-08-10T12:21:35.634+0200" uniqueID="5172834d-67fd-4bb0-908d-bb96c7424d1f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2014-08-10T12:21:35.637+0200" uniqueID="355547ed-17fa-4492-a6c9-102c9e28d3fa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Medium Life Cycle" timeCreated="2011-12-07T15:29:54.893+0100" lastModified="2014-10-14T12:00:31.182+0200" uniqueID="01a489a4-c58a-4b57-aa27-9f16534fd98b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:21:41.504+0200" uniqueID="f2076bae-8d4a-4d52-abca-1b4f77095bd2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:21:41.507+0200" uniqueID="2caa779f-75e7-417f-bfc9-17fa8ae69e0a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="MLC1" timeCreated="2011-11-25T15:32:46.357+0100" lastModified="2012-02-01T14:58:32.117+0100" uniqueID="d9c4ff89-f5ad-472c-8006-78bc1a090776" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="&#xD;&#xA;&#xD;&#xA;&#xD;&#xA;" id="MLC1">
          <commentlist>
            <comments timeCreated="2012-02-01T14:32:41.558+0100" uniqueID="6c86496d-4dbf-4e46-adbf-4665ad7fe72b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" category="unresolved">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <text/>
              <author timeCreated="2012-02-01T14:32:41.558+0100" uniqueID="10bcee60-4ade-4ad9-addb-a0945a2b81e6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
              </author>
            </comments>
          </commentlist>
          <creator name="" timeCreated="2011-11-25T15:43:01.465+0100" uniqueID="7d3559f9-bce0-4df9-94e5-d698c2326392" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-11-25T15:43:01.469+0100" uniqueID="5bf9d520-c1cf-40bd-879d-ebea4723fbac" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-11-25T15:36:30.425+0100" uniqueID="3ba7c25b-6647-4964-81a5-175cf5fdd819" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="1a" precondition="[test 2.a successful]" action="create a new medium in system, check the defined transitions are possible but no other transitions" postcondition="">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
          <modelFragment timeCreated="2012-02-01T11:46:50.703+0100" uniqueID="5edaa638-a4ff-4a40-b591-52ed5987d2e1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <fragment name="MLC1" timeCreated="2012-02-01T11:46:50.703+0100" uniqueID="afa14e3d-7c2c-4bcb-b9c9-dcf9f9db2caf" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
              <commentlist/>
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
              <contents xsi:type="modelelement:Class" name="foobar" timeCreated="2012-02-01T11:47:15.663+0100" uniqueID="89bc7179-2df7-44cd-803d-e8cd1cc15435" relatedBy="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.1/@relatesTo.0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
                <commentlist/>
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <contents xsi:type="modelelement:ClassAttribute" name="barfoo: int" timeCreated="2012-02-01T11:47:41.059+0100" uniqueID="cf14b363-d035-41f8-a01e-98cd44a232ae" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
                  <commentlist/>
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                </contents>
              </contents>
              <contents xsi:type="modelelement:Class" name="Person" timeCreated="2012-02-01T11:48:10.574+0100" uniqueID="32590e11-e26e-439c-8e4e-45ae67aece2b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
                <commentlist/>
                <relatesTo xsi:type="modelelement:Association" timeCreated="2012-02-01T11:49:51.567+0100" uniqueID="353bf4cc-9952-4633-8535-fed15d47c6be" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" toElement="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.0" sourceLabel="^hghg">
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                </relatesTo>
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
              </contents>
              <contents xsi:type="modelelement:State" timeCreated="2012-02-01T11:51:13.778+0100" uniqueID="603c520b-30df-4864-a7b7-0a1be26d7d2d" relatedBy="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@relatesTo.0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="Final">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
              </contents>
              <contents xsi:type="modelelement:State" timeCreated="2012-02-01T11:51:16.461+0100" uniqueID="36ee34b3-707f-4343-b052-51b4b2ec634a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
                <relatesTo xsi:type="modelelement:StateTransition" timeCreated="2012-02-01T14:44:38.291+0100" uniqueID="09aa7b47-32d7-4236-ab61-b4eb5e957ee2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" toElement="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.4">
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                </relatesTo>
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
              </contents>
              <contents xsi:type="modelelement:State" name="created" timeCreated="2012-02-01T11:51:25.790+0100" uniqueID="1908b5f4-2df1-4bcd-8591-ea0374d1a6d8" relatedBy="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.3/@relatesTo.0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="State">
                <relatesTo xsi:type="modelelement:StateTransition" timeCreated="2012-02-01T14:44:44.468+0100" uniqueID="36b8d013-c31b-47f8-a5cb-b746d4614b91" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" toElement="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6">
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                </relatesTo>
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
              </contents>
              <contents xsi:type="modelelement:State" name="prepared" timeCreated="2012-02-01T11:51:40.126+0100" uniqueID="9ef40f61-9ca2-4168-9ddb-d77e55b0f309" relatedBy="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6/@relatesTo.0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="State">
                <relatesTo xsi:type="modelelement:StateTransition" timeCreated="2012-02-01T14:46:10.673+0100" uniqueID="e7578658-3394-42fe-b516-6a266632dfd3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" toElement="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.2">
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                </relatesTo>
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
                <contents xsi:type="modelelement:State" name="initialiyed" timeCreated="2012-02-01T11:51:33.090+0100" uniqueID="eb09a64e-308b-48ec-b2e4-567e4cd6d933" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="State" entryEffect="">
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                  <contents xsi:type="modelelement:State" timeCreated="2012-02-01T11:51:20.002+0100" uniqueID="18497efc-a8da-488b-8a93-19aa97ce7d78" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="Deep History">
                    <cost name="Cost" kind=""/>
                    <benefit name="Cost" kind=""/>
                  </contents>
                </contents>
                <contents xsi:type="modelelement:State" timeCreated="2012-02-01T14:45:31.190+0100" uniqueID="23f79d53-961c-4618-81ea-65cc684e26c1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="Deep History">
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                </contents>
                <contents xsi:type="modelelement:State" timeCreated="2012-02-01T14:46:29.065+0100" uniqueID="da1b7c28-9f1b-4ee8-baa3-29ff3f8b725f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="Deep History">
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                </contents>
              </contents>
              <contents xsi:type="modelelement:State" name="dgfd" timeCreated="2012-02-01T14:45:19.474+0100" uniqueID="21b93a82-d53a-4702-9114-d3bdd1ca436a" relatedBy="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.4/@relatesTo.0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="State">
                <relatesTo xsi:type="modelelement:StateTransition" timeCreated="2012-02-01T14:46:22.419+0100" uniqueID="7f16f2c4-2bd3-4e61-b90f-2a7eedf1ff85" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" toElement="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5">
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                </relatesTo>
                <relatesTo xsi:type="modelelement:StateTransition" timeCreated="2012-02-01T14:48:08.623+0100" uniqueID="5fea10b9-9984-429f-aea6-8a5de4b70e7c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" toElement="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.7">
                  <cost name="Cost" kind=""/>
                  <benefit name="Cost" kind=""/>
                </relatesTo>
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
              </contents>
              <contents xsi:type="modelelement:ActivityNode" timeCreated="2012-02-01T14:47:50.199+0100" uniqueID="3931a062-5449-4b48-873e-5263b89c7c5a" relatedBy="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6/@relatesTo.1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="FlowFinal">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
              </contents>
              <contents xsi:type="modelelement:ActivityNode" timeCreated="2012-02-01T14:47:52.820+0100" uniqueID="aeb0cf35-3c96-41d4-90dc-25ab36b24d22" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" kind="Final">
                <cost name="Cost" kind=""/>
                <benefit name="Cost" kind=""/>
              </contents>
            </fragment>
            <fragmentDiagram type="Modelelement" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment" name="MLC1" measurementUnit="Pixel">
              <children type="2018" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.0">
                <children xsi:type="notation:DecorationNode" type="5021" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.0"/>
                <children xsi:type="notation:BasicCompartment" type="7001" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.0"/>
                <children xsi:type="notation:BasicCompartment" type="7003" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.0">
                  <children xsi:type="notation:Shape" type="3002" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.0/@contents.0" fontName="Segoe UI">
                    <children xsi:type="notation:DecorationNode" type="5036" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.0/@contents.0"/>
                    <layoutConstraint xsi:type="notation:Bounds"/>
                  </children>
                  <styles xsi:type="notation:SortingStyle"/>
                  <styles xsi:type="notation:FilteringStyle"/>
                </children>
                <children xsi:type="notation:BasicCompartment" type="7005" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.0">
                  <styles xsi:type="notation:SortingStyle"/>
                  <styles xsi:type="notation:FilteringStyle"/>
                </children>
                <styles xsi:type="notation:DescriptionStyle"/>
                <styles xsi:type="notation:LineStyle"/>
                <styles xsi:type="notation:FillStyle"/>
                <layoutConstraint xsi:type="notation:Bounds" x="50" y="160" width="178" height="123"/>
              </children>
              <children type="2018" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.1">
                <children xsi:type="notation:DecorationNode" type="5021" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.1"/>
                <children xsi:type="notation:BasicCompartment" type="7001" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.1"/>
                <children xsi:type="notation:BasicCompartment" type="7003" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.1">
                  <styles xsi:type="notation:SortingStyle"/>
                  <styles xsi:type="notation:FilteringStyle"/>
                </children>
                <children xsi:type="notation:BasicCompartment" type="7005" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.1">
                  <styles xsi:type="notation:SortingStyle"/>
                  <styles xsi:type="notation:FilteringStyle"/>
                </children>
                <styles xsi:type="notation:DescriptionStyle"/>
                <styles xsi:type="notation:LineStyle"/>
                <styles xsi:type="notation:FillStyle" fillColor="8905185"/>
                <layoutConstraint xsi:type="notation:Bounds" x="680" y="140" width="166" height="130"/>
              </children>
              <children type="2038" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.2">
                <styles xsi:type="notation:DescriptionStyle"/>
                <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                <styles xsi:type="notation:LineStyle"/>
                <layoutConstraint xsi:type="notation:Bounds" x="715" y="605"/>
              </children>
              <children type="2037" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.3">
                <styles xsi:type="notation:DescriptionStyle"/>
                <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                <styles xsi:type="notation:FillStyle"/>
                <layoutConstraint xsi:type="notation:Bounds" x="145" y="445" width="20" height="15"/>
              </children>
              <children xsi:type="notation:Shape" type="2023" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.4" fontName="Segoe UI">
                <children xsi:type="notation:DecorationNode" type="5026" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.4"/>
                <children xsi:type="notation:DecorationNode" type="5027" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.4"/>
                <children xsi:type="notation:DecorationNode" type="5028" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.4"/>
                <children xsi:type="notation:DecorationNode" type="5029" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.4"/>
                <layoutConstraint xsi:type="notation:Bounds" x="160" y="510" width="205" height="76"/>
              </children>
              <children xsi:type="notation:Shape" type="2023" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5" fontName="Segoe UI" fillColor="15053796">
                <children xsi:type="notation:DecorationNode" type="5026" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5"/>
                <children xsi:type="notation:DecorationNode" type="5027" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5"/>
                <children xsi:type="notation:DecorationNode" type="5028" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5"/>
                <children xsi:type="notation:DecorationNode" type="5029" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5"/>
                <children xsi:type="notation:Shape" type="2023" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@contents.0" fontName="Segoe UI">
                  <children xsi:type="notation:DecorationNode" type="5026" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@contents.0"/>
                  <children xsi:type="notation:DecorationNode" type="5027" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@contents.0"/>
                  <children xsi:type="notation:DecorationNode" type="5028" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@contents.0"/>
                  <children xsi:type="notation:DecorationNode" type="5029" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@contents.0"/>
                  <children type="2035" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@contents.0/@contents.0">
                    <styles xsi:type="notation:DescriptionStyle"/>
                    <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                    <styles xsi:type="notation:FillStyle"/>
                    <layoutConstraint xsi:type="notation:Bounds" x="485" y="412"/>
                  </children>
                  <layoutConstraint xsi:type="notation:Bounds" x="445" y="505" width="171" height="96"/>
                </children>
                <children type="2035" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@contents.1">
                  <styles xsi:type="notation:DescriptionStyle"/>
                  <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                  <styles xsi:type="notation:FillStyle"/>
                  <layoutConstraint xsi:type="notation:Bounds" x="574" y="542"/>
                </children>
                <children type="2035" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@contents.2">
                  <styles xsi:type="notation:DescriptionStyle"/>
                  <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                  <styles xsi:type="notation:FillStyle"/>
                  <layoutConstraint xsi:type="notation:Bounds" x="540" y="402"/>
                </children>
                <layoutConstraint xsi:type="notation:Bounds" x="660" y="410" width="161" height="111"/>
              </children>
              <children xsi:type="notation:Shape" type="2023" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6" fontName="Segoe UI">
                <children xsi:type="notation:DecorationNode" type="5026" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6"/>
                <children xsi:type="notation:DecorationNode" type="5027" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6"/>
                <children xsi:type="notation:DecorationNode" type="5028" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6"/>
                <children xsi:type="notation:DecorationNode" type="5029" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6"/>
                <layoutConstraint xsi:type="notation:Bounds" x="502" y="622" width="159" height="59"/>
              </children>
              <children type="2034" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.7">
                <styles xsi:type="notation:DescriptionStyle"/>
                <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                <styles xsi:type="notation:FillStyle"/>
                <layoutConstraint xsi:type="notation:Bounds" x="495" y="505"/>
              </children>
              <children type="2032" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.8">
                <styles xsi:type="notation:DescriptionStyle"/>
                <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                <styles xsi:type="notation:LineStyle"/>
                <layoutConstraint xsi:type="notation:Bounds" x="776" y="763"/>
              </children>
              <styles xsi:type="notation:DiagramStyle"/>
              <edges type="4010" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.1/@relatesTo.0" source="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.1" target="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.0">
                <children xsi:type="notation:DecorationNode" type="6009" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.1/@relatesTo.0">
                  <layoutConstraint xsi:type="notation:Location" x="107" y="-10"/>
                </children>
                <children xsi:type="notation:DecorationNode" type="6010" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.1/@relatesTo.0">
                  <layoutConstraint xsi:type="notation:Location" x="15" y="-42"/>
                </children>
                <styles xsi:type="notation:RoutingStyle" smoothness="Normal"/>
                <styles xsi:type="notation:FontStyle" fontName="SimSun"/>
                <bendpoints xsi:type="notation:RelativeBendpoints" points="[-54, 22, 426, -3]$[-234, 99, 246, 74]$[-391, 52, 89, 27]"/>
                <sourceAnchor xsi:type="notation:IdentityAnchor" id="(0.3253012048192771,0.2)"/>
              </edges>
              <edges type="4007" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.3/@relatesTo.0" source="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.3" target="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.4">
                <children xsi:type="notation:DecorationNode" type="6006" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.3/@relatesTo.0">
                  <layoutConstraint xsi:type="notation:Location" y="40"/>
                </children>
                <styles xsi:type="notation:RoutingStyle"/>
                <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                <bendpoints xsi:type="notation:RelativeBendpoints" points="[2, 20, -2, -82]$[10, 82, 6, -20]"/>
              </edges>
              <edges type="4007" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.4/@relatesTo.0" source="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.4" target="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.6">
                <children xsi:type="notation:DecorationNode" type="6006" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.4/@relatesTo.0">
                  <layoutConstraint xsi:type="notation:Location" y="40"/>
                </children>
                <styles xsi:type="notation:RoutingStyle"/>
                <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                <bendpoints xsi:type="notation:RelativeBendpoints" points="[20, 1, -213, -15]$[271, 10, 38, -6]"/>
              </edges>
              <edges type="4007" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@relatesTo.0" source="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.5" target="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.2">
                <children xsi:type="notation:DecorationNode" type="6006" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.5/@relatesTo.0">
                  <layoutConstraint xsi:type="notation:Location" y="40"/>
                </children>
                <styles xsi:type="notation:RoutingStyle"/>
                <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                <bendpoints xsi:type="notation:RelativeBendpoints" points="[-4, 56, -4, 56]$[-4, 56, -4, 56]"/>
              </edges>
              <edges type="4007" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6/@relatesTo.0" source="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.6" target="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.5">
                <children xsi:type="notation:DecorationNode" type="6006" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6/@relatesTo.0">
                  <layoutConstraint xsi:type="notation:Location" y="40"/>
                </children>
                <styles xsi:type="notation:RoutingStyle"/>
                <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                <bendpoints xsi:type="notation:RelativeBendpoints" points="[3, -20, 3, -20]$[3, -20, 3, -20]"/>
              </edges>
              <edges type="4007" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6/@relatesTo.1" source="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.6" target="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragmentDiagram/@children.7">
                <children xsi:type="notation:DecorationNode" type="6006" element="//@contents.0/@contents.0/@contents.0/@contents.0/@modelFragment/@fragment/@contents.6/@relatesTo.1">
                  <layoutConstraint xsi:type="notation:Location" y="40"/>
                </children>
                <styles xsi:type="notation:RoutingStyle"/>
                <styles xsi:type="notation:FontStyle" fontName="Segoe UI"/>
                <bendpoints xsi:type="notation:RelativeBendpoints" points="[-79, -7, -79, -7]$[-79, -7, -79, -7]"/>
              </edges>
            </fragmentDiagram>
          </modelFragment>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MLC2" timeCreated="2011-11-25T15:47:54.636+0100" lastModified="2011-12-07T15:30:00.184+0100" uniqueID="ce4a3259-f77d-4d12-93ed-5319436eff22" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="Activity" rationaleText="" id="MLC2">
          <commentlist/>
          <creator name="" timeCreated="2011-11-25T15:48:46.130+0100" uniqueID="07c330a5-6045-425b-844b-d886db1381ea" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-11-25T15:48:46.151+0100" uniqueID="345b21e8-1b21-4375-bdf6-542ff72a5666" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-11-25T15:48:44.738+0100" uniqueID="9769c431-9baf-4454-9642-3258a02edaf5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="2a" precondition="" action="creating a medium in the system" postcondition="medium can be found and updated.">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <acceptanceTests timeCreated="2011-11-25T15:49:54.554+0100" uniqueID="d995cd41-55a6-4b7c-8421-6b8d1cff1c48" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="2b" precondition="" action="delete medium from the system" postcondition="medium is not available any more">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <acceptanceTests timeCreated="2011-11-25T15:50:31.522+0100" uniqueID="be775863-54bf-4dda-a1c7-4f053327a5ad" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="2c" precondition="" action="delete a medium that does not exist" postcondition="create error message">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
          <modelFragment timeCreated="2014-07-10T13:31:57.855+0200" uniqueID="cfa4c40e-2990-4da4-a599-0f8b72320ec8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </modelFragment>
        </contents>
        <contents xsi:type="requirement:Requirement" label="foo.bar" name="MLC3" elementKind="unspecified" description="" timeCreated="2011-12-07T14:08:12.923+0100" lastModified="2014-10-14T12:00:31.182+0200" uniqueID="6bb0c7ff-3583-44df-92ea-5bc909682c56" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MLC3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-07T14:09:42.289+0100" uniqueID="44a1236b-7a64-463f-a4bb-42844f1992aa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T14:09:42.291+0100" uniqueID="45eb942e-b19c-4bf2-90a9-2f8599911f5f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <longDescription>
            <fragments xsi:type="text:FormattedText" text=""/>
          </longDescription>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
          <modelFragment timeCreated="2014-07-10T13:32:10.704+0200" uniqueID="4eeb28e0-d38f-4927-94cd-a371a9fa42f4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
            <fragment name="MLC3" timeCreated="2014-10-14T12:00:05.202+0200" uniqueID="f70c7c3c-81e2-4f68-b53a-a874d4bdd18e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
              <cost name="Cost" kind=""/>
              <benefit name="Cost" kind=""/>
            </fragment>
            <fragmentDiagram type="Modelelement" element="//@contents.0/@contents.0/@contents.0/@contents.2/@modelFragment/@fragment" name="MLC3" measurementUnit="Pixel">
              <styles xsi:type="notation:DiagramStyle"/>
            </fragmentDiagram>
          </modelFragment>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MLC4" timeCreated="2011-12-07T14:40:09.009+0100" lastModified="2011-12-07T15:30:00.387+0100" uniqueID="aee159a9-3461-4739-a52f-5e7ef25bcb4c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MLC4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-07T14:42:10.846+0100" uniqueID="c98012cd-c341-49e4-8fe9-a153222f93a8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T14:42:10.847+0100" uniqueID="a21f83cf-0973-4edc-a92f-17539490055c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-12-07T14:41:00.976+0100" uniqueID="5ddd80b2-8df1-4bf8-9c26-c7a1db7a30e5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="4.a" precondition="" action="Log in as librarian, open item from wishlist, edit, log off. Log in as reader, open same item from wishlist, log off " postcondition="On second log in, edits by librarian shall be found, contents shall still be editable">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <acceptanceTests timeCreated="2011-12-07T14:43:39.631+0100" uniqueID="88f4e782-5951-4994-966d-712666b4c29a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="4.b" precondition="" action="log in as librarian, lock item from wishlist for reader R, log off. Log in as reader R, open same item from wishlist, log off  " postcondition="item shall be found, but R shall not be able to edit any more">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MLC5" timeCreated="2011-12-07T15:26:25.726+0100" lastModified="2011-12-07T15:30:00.488+0100" uniqueID="244af1d4-32b7-4964-9514-74b278b06777" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="m">
          <commentlist/>
          <creator name="" timeCreated="2011-12-07T15:28:56.415+0100" uniqueID="ce673a9a-40c2-4aa7-83a1-0ec0c0a9cd84" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T15:28:56.417+0100" uniqueID="08c52fb2-2500-4b0c-a10a-baebedb7e7c2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-12-07T15:27:13.220+0100" uniqueID="f9479694-7c31-425b-aacf-19755d4fe1ab" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="5.a" action="log in as reader, open item from wishlist, comment item, log off. Log in as other user, open same wishlist item, log off " postcondition="on second log in, comments from first log in shall be found">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MLC6" timeCreated="2011-12-07T15:30:34.012+0100" lastModified="2011-12-07T15:33:20.839+0100" uniqueID="767854c6-e241-467f-9032-b33b7ec589db" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="">
          <commentlist/>
          <creator name="" timeCreated="2011-12-07T15:31:47.414+0100" uniqueID="c8df4845-5164-4ae2-8289-f456f32e7f4b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T15:31:47.415+0100" uniqueID="81b5812a-551a-44ca-9052-b8c853c497a8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-12-07T15:31:23.970+0100" uniqueID="ee42bdc4-8d34-4fb5-9b12-9df7686ca190" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="6.a" action="log in as librarian, open same item from wishlist, edit, log off. Log in as reader, open same item from wishlist, log off" postcondition="on second log in, edits by librarian shall be found, contents shall still be editable">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <acceptanceTests timeCreated="2011-12-07T15:32:11.270+0100" uniqueID="b3c44537-2e27-4003-98c4-276517bb25f9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="6.b" action="log in as reader, open item on wishlist, log off. log in as librarian, open item from wishlist, delete, log off. log in as reader, look for same item from wishlist, log off" postcondition="On second log in, the item shall not be found any more. Instead, a message shall appear indicating what happened.">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
          <modelFragment timeCreated="2014-07-10T13:33:02.440+0200" uniqueID="0ae9e3c5-de94-4d32-a632-aa020c12dc87" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </modelFragment>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MLC7" timeCreated="2011-12-07T15:33:47.454+0100" lastModified="2011-12-07T15:36:46.442+0100" uniqueID="9ca79e2b-b450-48a4-a184-257dc57205a7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MLC7">
          <commentlist/>
          <creator name="" timeCreated="2011-12-07T15:34:56.496+0100" uniqueID="50999170-30bb-42f9-870e-7aed28382107" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-07T15:34:56.497+0100" uniqueID="2f5f0aef-e7a8-47dd-a8fa-c01638035732" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-12-07T15:35:54.641+0100" uniqueID="ff2219b8-b45a-487c-bdd7-34d731b3b666" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" precondition="" action="log in as reader R, create new item on wishlist, log off. log in as reader R&#x2019;, comment item on wishlist, log off.  log in as librarian, delete item from wishlist, log off" postcondition="Reader R should get notification, but not reader R&#x2019;">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MLC8" timeCreated="2011-12-07T15:37:15.353+0100" lastModified="2011-12-09T09:23:34.704+0100" uniqueID="3ccddefb-2f1c-4d0c-a2ed-b305789a4947" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MLC8">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:23:34.669+0100" uniqueID="d6814062-c12c-4be8-89db-d03c5caaca98" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:23:34.672+0100" uniqueID="7a2575df-070e-4a9d-80d4-4131d596da9c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MLC9" timeCreated="2011-12-09T09:24:02.147+0100" lastModified="2011-12-09T09:24:31.388+0100" uniqueID="fc938ff1-a668-4c7a-bd63-5632cb32c859" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MLC9">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:24:31.211+0100" uniqueID="aa71aafd-c452-4511-a76c-27bbc49136b5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:24:31.218+0100" uniqueID="c9a1ba3a-2686-4d16-bf1c-c7e5ba3c1acf" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MLC10" timeCreated="2011-12-09T09:24:46.293+0100" lastModified="2011-12-09T09:26:32.008+0100" uniqueID="a0c43a61-f5df-43f7-bdaf-e75f2e8eaf64" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MLC10">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:26:31.981+0100" uniqueID="86745771-f300-489f-82ec-e276a308c4e2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:26:31.986+0100" uniqueID="7dcc03ae-d530-4ffa-964a-0115115f18b9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-12-09T09:25:16.709+0100" uniqueID="5789b1b3-e05a-4012-b9f4-e5f05dc00b3e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="10.a" action="log in as librarian, try all actions, log results. log in as reader, try all actions, log results. do not log in, try all actions, log results ">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Advanced Medium Delivery Service" timeCreated="2011-12-07T15:38:10.730+0100" lastModified="2011-12-09T15:09:46.352+0100" uniqueID="7298855b-607f-4624-9a76-5a389f0a7cc3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:21:45.084+0200" uniqueID="7c5f5224-c0bb-4c71-a577-0ae31fca4ae9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:21:45.088+0200" uniqueID="e62b8ebb-3e8c-40b8-8954-8c9f671d4da4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="AMDS1" timeCreated="2011-12-09T09:32:05.986+0100" lastModified="2011-12-09T09:32:41.965+0100" uniqueID="2ee06d2f-380f-4623-a0e5-1647923f7629" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="AMDS1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:32:41.948+0100" uniqueID="f0a7312e-1db3-48c0-a6b1-1e7d77442576" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:32:41.949+0100" uniqueID="7f4feaa0-3287-4133-aa35-b93009c1b483" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="AMDS2" timeCreated="2011-12-09T09:33:52.604+0100" lastModified="2011-12-09T09:36:41.761+0100" uniqueID="7d77b7d8-c459-45b8-b5f3-c341429e74a8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="AMDS2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:36:05.611+0100" uniqueID="4b844b08-65d4-4cba-b60a-3a4ce6bf3939" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:36:05.612+0100" uniqueID="44f142d6-bda4-49f2-ab34-dc0d8d53106d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-12-09T09:35:43.852+0100" uniqueID="4fb37ec4-87a5-4d47-9a6f-11bca4d8acf2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="2.a" action="Create new reader R, try to use AMDS" postcondition="shall fail">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <acceptanceTests timeCreated="2011-12-09T09:36:22.092+0100" uniqueID="e41827e6-8cf7-4d15-80e6-a017e57dc8a4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="2.b" precondition="" action="edit R from 2.a to allow AMDS, try to use AMDS " postcondition="shall succeed">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="AMDS3" timeCreated="2011-12-09T09:37:30.620+0100" lastModified="2011-12-09T09:46:13.188+0100" uniqueID="91aca4be-bb60-4f24-a6b2-49abd810d51e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="AMDS3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:38:07.585+0100" uniqueID="9162ed7c-a74a-4b01-ba04-b5be0a01f929" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:38:07.592+0100" uniqueID="aaab6b3c-1b14-477f-934d-3bdfdaf2b00c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="AMDS4" timeCreated="2011-12-09T09:39:33.597+0100" lastModified="2011-12-09T09:46:12.307+0100" uniqueID="8d934b6f-d5e6-4c99-ae22-05373427e6fe" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="AMDS4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:43:33.166+0100" uniqueID="3e6b349e-53a2-4d51-ae05-31cfe53e1208" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:43:33.168+0100" uniqueID="9371cc72-7f74-48e6-b517-4a85d2755b13" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="AMDS5" timeCreated="2011-12-09T09:44:15.936+0100" lastModified="2011-12-09T09:46:07.255+0100" uniqueID="829ea81b-69d5-4e7f-a133-ee9dc80f323e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="AMDS5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:45:41.834+0100" uniqueID="f526656e-e6d6-4d82-8287-c2cc9ab884b7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:45:41.836+0100" uniqueID="062391a6-dc1f-4f71-8af6-f75aac27cd19" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-12-09T09:44:49.293+0100" uniqueID="f754061e-fcd9-41cb-b13e-203084e86375" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="5.a" precondition="" action="log in as reader R, reserve available medium, select for BookStation delivery" postcondition="medium shall appear on &#x201c;delivery at BS&#x201d; list.">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <acceptanceTests timeCreated="2011-12-09T09:45:09.749+0100" uniqueID="8ace580b-4888-4947-a5ba-9476907d1c69" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="5.b" precondition="" action="log in as reader R, reserve available medium, select for Front desk delivery" postcondition="medium shall appear on &#x201c;delivery at Front Desk&#x201d; list.">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <acceptanceTests timeCreated="2011-12-09T09:45:49.653+0100" uniqueID="9b68f9ac-326b-4f4e-892e-15fd96e6186e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="5.c" precondition="" action="log in as reader R, reserve available medium, select for online delivery " postcondition="medium shall be leased, lease in readers account contains a link to a copy of the medium">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="AMDS6" timeCreated="2011-12-09T09:52:41.626+0100" lastModified="2011-12-09T09:54:54.952+0100" uniqueID="d37b04ef-3dee-43bd-91ee-3f560e57c99d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="AMDS6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:54:54.937+0100" uniqueID="aaada215-ed44-48a9-a384-137593a67101" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:54:54.938+0100" uniqueID="6d86fbee-d1a3-4a86-8d78-cb9b92a6fa41" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <acceptanceTests timeCreated="2011-12-09T09:54:27.218+0100" uniqueID="90986a22-84d7-4bab-b753-8d864dd347c8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" caseID="6.a" precondition="[after 5a, librarian has deposited medium in any compartment of BS]" action="reader authenticates to BS, BS indicates and unlocks compartment" postcondition="lease is issued with current date/time">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </acceptanceTests>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Identification Tags" timeCreated="2011-12-07T15:39:02.504+0100" lastModified="2011-12-09T15:09:46.454+0100" uniqueID="2f107ae2-2b58-44c6-80b4-8835c1c872de" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:21:51.374+0200" uniqueID="6ae7bb2f-dde8-4efe-bb1b-fac5269f66eb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:21:51.378+0200" uniqueID="b88d837b-0608-4ed1-a07d-f499fa06d722" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="IDTAG" timeCreated="2011-12-09T09:58:36.183+0100" lastModified="2011-12-09T10:00:29.924+0100" uniqueID="7324ba2a-e538-4627-a0e4-5194447c736b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="IDTAG1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T09:58:45.382+0100" uniqueID="9f5254d7-fee0-4a02-9569-9f67642d6e93" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T09:58:45.383+0100" uniqueID="c7d049b5-c0d7-4cce-9939-19b35fcc5947" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="IDTAG" timeCreated="2011-12-09T10:01:13.652+0100" lastModified="2011-12-09T10:02:23.471+0100" uniqueID="006282e6-7444-4633-8e47-4f9517adadc8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="IDTAG2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:01:16.200+0100" uniqueID="41e8e78a-4147-4c5f-bcf7-ea2a16332229" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:01:16.203+0100" uniqueID="45173dca-89f3-42d4-98c6-40c436e60863" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="IDTAG" timeCreated="2011-12-09T10:02:48.052+0100" lastModified="2011-12-09T10:03:13.082+0100" uniqueID="050e25ee-7926-4270-9ed9-b2e8f6790dec" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="IDTAG3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:03:13.000+0100" uniqueID="158fc73c-4146-4d71-b045-459d270bb5f9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:03:13.001+0100" uniqueID="14ed9a14-46ba-44fd-8c83-8ffd6e32edd1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="IDTAG" timeCreated="2011-12-09T10:03:36.187+0100" lastModified="2011-12-09T10:06:14.188+0100" uniqueID="a33b1879-09ec-438c-b4e2-cbdd29b0b810" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="IDTAG4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:06:14.065+0100" uniqueID="ad699e51-9a04-47dc-94c4-9099f749f041" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:06:14.066+0100" uniqueID="1764408b-f775-4fb4-bab2-8976e6cfa0e3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Catalog" timeCreated="2011-12-09T15:10:16.357+0100" lastModified="2011-12-09T15:10:34.622+0100" uniqueID="b8e2fc9c-f730-4727-8f85-aea58589c89d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
      <creator name="" timeCreated="2014-08-10T12:21:56.609+0200" uniqueID="91c3081a-e6a2-43c4-bb0d-ee16ac2a259d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2014-08-10T12:21:56.613+0200" uniqueID="35de6c74-acb7-4563-b7ec-a6d06a6a9804" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Catalog Data" timeCreated="2011-12-07T15:43:50.580+0100" lastModified="2011-12-09T15:10:34.280+0100" uniqueID="fcbd04f1-9c7f-4026-9e26-e4b8f4b1eda6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:00.710+0200" uniqueID="04b00fa2-82fb-4ca7-8a1f-8c59a8385c99" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:00.714+0200" uniqueID="358d5ca4-510d-40a7-bfa8-8da5e1947fd8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="CDAT1" timeCreated="2011-12-09T10:06:53.259+0100" lastModified="2011-12-09T12:37:42.300+0100" uniqueID="4b3e61e6-3f0c-46c0-9d41-25ce3f9e49f5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CDAT1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:07:43.429+0100" uniqueID="708d2f66-742a-43fb-a2c0-dbda35055a75" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:07:43.430+0100" uniqueID="a030d818-9294-4326-916d-a466815f5bf5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CDAT2" timeCreated="2011-12-09T10:08:16.306+0100" lastModified="2011-12-09T10:08:44.675+0100" uniqueID="6d61d2e4-9009-4ef2-9fc8-775317017d80" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CDAT2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:08:44.571+0100" uniqueID="f32879fb-8dd9-4583-bfbe-783e4d203893" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:08:44.573+0100" uniqueID="786ade98-6f58-4b86-9163-216eb20410a1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CDAT3" timeCreated="2011-12-09T10:09:00.514+0100" lastModified="2011-12-09T10:10:24.296+0100" uniqueID="e7ac3734-1fe7-470c-9f07-e04b07c94dc1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CDAT3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:10:24.172+0100" uniqueID="0bd24f62-e984-41e2-b8d1-64e7a6b58867" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:10:24.175+0100" uniqueID="71509d44-8f7c-4d51-88d0-101ba8d407ab" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CDAT4" timeCreated="2011-12-09T10:10:38.137+0100" lastModified="2011-12-09T10:20:13.353+0100" uniqueID="1a8eb053-febd-4448-a202-84435855ed0a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CDAT4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:11:13.247+0100" uniqueID="9689be43-a094-4689-85c6-afa9c049776f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:11:13.253+0100" uniqueID="b1466596-3a6a-4a3f-bf07-20b075c3c648" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CDAT5" timeCreated="2011-12-09T10:11:48.024+0100" lastModified="2011-12-09T10:20:09.975+0100" uniqueID="9a3ec2bc-ef72-47ad-a08f-9be32c94738a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CDAT5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:12:10.183+0100" uniqueID="3756f2a5-2cf6-40f9-8155-8f542a716091" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:12:10.185+0100" uniqueID="1306d4f6-3a82-4a26-abfd-17efa2f7e85f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CDAT6" timeCreated="2011-12-09T10:12:31.664+0100" lastModified="2011-12-09T10:20:08.031+0100" uniqueID="c1056659-8b01-410c-9a53-3682714c1fe8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CDAT6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:12:45.669+0100" uniqueID="3d411480-919c-4316-8f19-4039cdcc35f9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:12:45.672+0100" uniqueID="87752de3-7978-4015-970c-e83720e6c632" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CDAT7" timeCreated="2011-12-09T10:13:10.464+0100" lastModified="2011-12-09T10:13:35.232+0100" uniqueID="47c4422f-67b4-4b47-bbb7-f454111bcb53" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CDAT7">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:13:35.218+0100" uniqueID="872ad5b9-a859-4cb7-a08e-19aecfd91137" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:13:35.219+0100" uniqueID="cb1c536f-a6ca-4b30-87d3-eced409f0090" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Catalog Search (General)" timeCreated="2011-12-07T15:44:19.774+0100" lastModified="2011-12-09T15:10:34.411+0100" uniqueID="6fbb5a9b-f661-414d-baae-05b8a70b06c9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:04.131+0200" uniqueID="dd0762bf-c339-4bec-926f-45b8e3a77d3f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:04.135+0200" uniqueID="b0bb4ebd-c450-4a82-8421-fc0051936d73" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="CSG1" timeCreated="2011-12-09T10:14:47.390+0100" lastModified="2011-12-09T10:20:02.870+0100" uniqueID="ff9f69a3-7315-4c79-9e61-c85388e2650e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:15:05.035+0100" uniqueID="98e3bbb9-1990-4c7e-a0bc-0761b0a526f6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:15:05.036+0100" uniqueID="8bfb1a25-e390-47bd-9433-a1297156ad15" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG2" timeCreated="2011-12-09T10:15:20.390+0100" lastModified="2011-12-09T10:20:00.914+0100" uniqueID="b3170a9b-a38f-4219-9c6a-4f28e6b82653" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:16:34.730+0100" uniqueID="f2bfa5eb-9a1c-463f-8f8d-5ce38aa0b185" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:16:34.733+0100" uniqueID="8ecf59c6-1ecb-4bce-b7df-c9c1e50cb1f0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG3" timeCreated="2011-12-09T10:20:27.561+0100" lastModified="2011-12-09T10:20:46.309+0100" uniqueID="6d4e7081-a879-4f3f-a162-15044923df64" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:20:46.190+0100" uniqueID="450f06fc-60ac-4bba-8845-d11836500952" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:20:46.194+0100" uniqueID="9ab4f7c0-a4b8-4d83-b3e1-277bf7ac506f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG4" timeCreated="2011-12-09T10:21:05.518+0100" lastModified="2011-12-09T10:21:18.017+0100" uniqueID="698441bb-7043-4cea-910b-f4aad0dfc6fb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:21:18.004+0100" uniqueID="5e0f55db-a32f-4a64-b276-2f1716ed666d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:21:18.006+0100" uniqueID="4f961d98-36bf-43cc-9d12-e6a36ddd3868" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG5" timeCreated="2011-12-09T10:21:39.733+0100" lastModified="2011-12-09T10:21:45.504+0100" uniqueID="bad992de-56f7-47b6-96fc-32602ecc0e5a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:21:45.492+0100" uniqueID="aea739f3-833c-48bd-80e5-77d8805630bd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:21:45.493+0100" uniqueID="d29dd120-07f4-42ef-af77-ef8f7720d8a9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG6" timeCreated="2011-12-09T10:22:08.156+0100" lastModified="2011-12-09T10:22:56.111+0100" uniqueID="4d16d1c7-90d3-4256-b801-48433d50ef11" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:22:19.492+0100" uniqueID="4ea0462f-3357-48f1-a13a-02585e54170a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:22:19.493+0100" uniqueID="b2bd41e5-64fe-49e4-922c-43fdf0f86e3b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG7" timeCreated="2011-12-09T10:23:18.441+0100" lastModified="2011-12-09T10:23:33.985+0100" uniqueID="4d181500-03bf-47e8-9092-c2f4c5ce5310" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG7">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:23:33.869+0100" uniqueID="46e5b4b9-9022-4957-b745-2944b09ee2df" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:23:33.870+0100" uniqueID="3f521dc2-3663-4c24-815b-93fd5d05773b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG8" timeCreated="2011-12-09T10:23:55.979+0100" lastModified="2011-12-09T10:24:21.399+0100" uniqueID="0e33e5f4-7372-444e-8016-a265b8ed3cff" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG8">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:24:06.940+0100" uniqueID="5f3b90d2-aa06-4abf-a5fd-5d5943c45e9e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:24:06.943+0100" uniqueID="3e256a3d-5784-4295-8d16-88cfd49df2a5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG9" timeCreated="2011-12-09T10:24:32.603+0100" lastModified="2011-12-09T10:25:17.651+0100" uniqueID="8de0b7fc-6b81-4498-ac57-9c6abc969e2a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG9">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:24:57.527+0100" uniqueID="d94771da-89b2-47b9-a852-46729ec71e01" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:24:57.530+0100" uniqueID="b4a016aa-9785-475e-a466-79d1a81dced2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG10" timeCreated="2011-12-09T10:25:29.977+0100" lastModified="2011-12-09T10:25:41.704+0100" uniqueID="c2d677ff-894f-4c0b-88c1-90fc6706e152" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG10">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:25:41.579+0100" uniqueID="180780dd-f1d1-4143-b9c1-af3167c03711" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:25:41.582+0100" uniqueID="2b8eb90d-2fd9-4d18-b5c9-cfec5f1516a5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG11" timeCreated="2011-12-09T10:26:10.739+0100" lastModified="2011-12-09T10:27:12.215+0100" uniqueID="8395caba-8170-452a-9b59-47b8c9f7e59e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG11">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:27:12.082+0100" uniqueID="4347ee47-115b-406a-8bb6-84163122aaa6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:27:12.083+0100" uniqueID="c50158a5-c27e-4992-900a-c1769a14e287" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="CSG12" timeCreated="2011-12-09T10:27:26.683+0100" lastModified="2011-12-09T10:27:55.757+0100" uniqueID="569cb9d0-f6c1-4e12-91d0-023b88524da0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="CSG12">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:27:55.640+0100" uniqueID="2f932e21-e8bb-44ec-9de4-d48b6afc2acd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:27:55.641+0100" uniqueID="d35b3dff-78c4-4cdb-a637-8ecc332cde3b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Expert Search Mode" timeCreated="2011-12-07T15:44:42.205+0100" lastModified="2011-12-09T15:10:34.516+0100" uniqueID="f9344502-a2e3-4dde-9501-b41f99bab383" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:07.768+0200" uniqueID="bc58db5d-2dcd-4c76-b374-364b20c8c0c0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:07.773+0200" uniqueID="49f0be08-ce20-4d8a-80f5-877d59f0fd0e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="ESM1" timeCreated="2011-12-09T10:28:10.762+0100" lastModified="2011-12-09T10:28:55.636+0100" uniqueID="bb0ef371-1bea-4691-9e9d-ea193ba67243" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="ESM1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:28:17.065+0100" uniqueID="64e63741-7cf3-4171-a4ea-530a2ef546f7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:28:17.068+0100" uniqueID="32387c9d-6bdf-4249-98a9-17b3d089b289" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="ESM2" timeCreated="2011-12-09T10:29:08.954+0100" lastModified="2011-12-09T10:29:26.237+0100" uniqueID="8c8b549b-7362-4b89-8076-bd2eb1337378" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="ESM2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:29:26.060+0100" uniqueID="fb84265b-ff6b-425f-bcc0-8acebbb3f109" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:29:26.064+0100" uniqueID="dae02d19-08b9-4cb2-a28d-328403f162d4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="ESM3" timeCreated="2011-12-09T10:29:39.658+0100" lastModified="2011-12-09T10:30:04.001+0100" uniqueID="9f49df16-4fa8-400a-af6b-bfb9e930267c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="ESM3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:29:57.806+0100" uniqueID="8571bbfb-217b-4e02-99da-047955b632e4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:29:57.810+0100" uniqueID="07a33c07-1121-4ba8-89cd-1011e6fa98b1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="ESM4" timeCreated="2011-12-09T10:30:19.593+0100" lastModified="2011-12-09T10:30:38.584+0100" uniqueID="4c20bd3b-d4e2-40a8-a4e6-192cd738f687" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="ESM4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:30:38.468+0100" uniqueID="48d8cda7-224e-4b72-8427-c954371207d9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:30:38.468+0100" uniqueID="579f7071-4a50-4d50-b522-6be92f5ebc04" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="ESM5" timeCreated="2011-12-09T10:30:51.744+0100" lastModified="2011-12-09T10:31:11.581+0100" uniqueID="f7497150-3b46-4724-8067-02a389b5cac4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="ESM5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:31:11.446+0100" uniqueID="18a1a19f-0746-4e9c-b95c-3a888c1bf86c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:31:11.449+0100" uniqueID="cb1b14da-f515-4a7d-be0a-f1eed3eeb453" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Novice Search Mode" timeCreated="2011-12-07T15:47:34.444+0100" lastModified="2011-12-09T15:10:34.621+0100" uniqueID="74fbf5c2-c0ef-4634-8414-d826a4a281ac" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:11.222+0200" uniqueID="4e4937ce-8c69-468c-af44-f0a36e96e735" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:11.226+0200" uniqueID="606e4121-a87d-45e8-823b-37f14f41d9d4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="NSM1" timeCreated="2011-12-09T10:31:35.318+0100" lastModified="2011-12-09T10:31:57.209+0100" uniqueID="cdee9ea8-6423-4ba5-8c42-dcd89f830cee" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NSM1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:31:57.072+0100" uniqueID="9954a3f5-f84f-436c-bcbf-a928b5953a64" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:31:57.075+0100" uniqueID="d0598c71-e669-4de2-ba97-9571df2e1693" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NSM2" timeCreated="2011-12-09T10:32:27.238+0100" lastModified="2011-12-09T10:32:45.578+0100" uniqueID="281ecb72-4233-4e95-9611-d7a8d3b50f10" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NSM2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:32:45.434+0100" uniqueID="152f758d-69bb-4271-a5a9-b18f5e3cc1c5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:32:45.437+0100" uniqueID="9b3c078d-e681-4565-870e-bcbae2ee5a42" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NSM3" timeCreated="2011-12-09T10:32:56.054+0100" lastModified="2011-12-09T10:33:23.087+0100" uniqueID="d996b788-1a2c-4e70-9da1-82e7155e1305" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NSM3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:33:22.953+0100" uniqueID="cd380bee-cdd7-4edc-84c4-73e4a4798db8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:33:22.955+0100" uniqueID="2b5630f8-e830-4b91-aad7-f050a3b799b9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NSM4" timeCreated="2011-12-09T10:33:39.232+0100" lastModified="2011-12-09T10:34:39.819+0100" uniqueID="56dbf1fa-ab3a-4ce7-b845-1c5c6c72f9d4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NSM4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:34:08.419+0100" uniqueID="d2c09029-154e-4363-a8f9-8a6d150a0e77" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:34:08.423+0100" uniqueID="f1c8207e-8f97-46ba-b5b9-4dbafc0d8bf3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NSM5" timeCreated="2011-12-09T10:34:25.214+0100" lastModified="2011-12-09T10:34:36.355+0100" uniqueID="e9b02eba-eba5-4d60-82b1-f8885c39fe64" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NSM5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:34:36.341+0100" uniqueID="40015501-b4ce-4c80-bf49-a9294377138e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:34:36.342+0100" uniqueID="321ab487-f318-4dcd-8910-62b49f29ac50" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Leases" timeCreated="2011-12-09T15:11:01.119+0100" lastModified="2011-12-09T15:11:10.187+0100" uniqueID="12da6f84-92c0-429d-850c-f21bad723c7a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
      <creator name="" timeCreated="2014-08-10T12:22:16.673+0200" uniqueID="3ae5bf89-559a-4804-afeb-1c5f78b5c45a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2014-08-10T12:22:16.677+0200" uniqueID="11c8c8cc-02f1-4d1f-8a9e-38d2a3661f26" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Reservation" timeCreated="2011-12-07T15:48:05.366+0100" lastModified="2011-12-09T15:11:09.972+0100" uniqueID="1dee8b92-1d48-4b90-9e2d-bec997194233" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:20.493+0200" uniqueID="de612ab3-3be9-4e43-932f-8aa3aff7d856" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:20.498+0200" uniqueID="c34368d4-594e-4948-94c1-f2ce9b4581a2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="RES1" timeCreated="2011-12-09T10:35:29.031+0100" lastModified="2011-12-09T10:36:09.939+0100" uniqueID="1d83f68a-7303-4079-b499-d1cc9105c606" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:36:00.620+0100" uniqueID="8d7b025f-02b2-4465-a477-e4005deec726" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:36:00.621+0100" uniqueID="48069e58-fb70-482e-a4a9-8e9d4dc7aebd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
          <modelFragment timeCreated="2014-07-10T13:33:30.015+0200" uniqueID="4a8f6bfd-f6c8-477b-91cf-9177580a4d5a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </modelFragment>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES2" timeCreated="2011-12-09T10:36:19.984+0100" lastModified="2011-12-09T10:36:24.613+0100" uniqueID="18643c4c-d88c-479e-91d1-a9d96d8a0871" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:36:24.601+0100" uniqueID="0bab19ef-f90f-4d57-b8ef-e72e4113f0ee" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:36:24.602+0100" uniqueID="1960d8b9-9f5d-49f9-8082-475c5870ffb6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES3" timeCreated="2011-12-09T10:36:43.518+0100" lastModified="2011-12-09T10:37:11.777+0100" uniqueID="929d7565-d38b-4b12-8b6d-178ce6b9764c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:37:11.644+0100" uniqueID="6642c7bd-2a71-45c7-a9b1-2af8060cd479" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:37:11.645+0100" uniqueID="dfd25a80-f517-4ab0-a4e0-36536cd3012f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES4" timeCreated="2011-12-09T10:37:23.492+0100" lastModified="2011-12-09T10:38:40.811+0100" uniqueID="630a9627-0dca-487d-9efc-68748fa9a0f3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:37:40.043+0100" uniqueID="b1dfee25-6b55-40fb-b7a3-664511d1fa4f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:37:40.043+0100" uniqueID="9f9be4c9-f704-4dad-a401-30d2deca9b4e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES5" timeCreated="2011-12-09T10:38:19.589+0100" lastModified="2011-12-09T10:38:37.452+0100" uniqueID="6464d4a6-b358-43ff-83ca-543d33f0765c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:38:37.311+0100" uniqueID="ae01f126-534d-4bbd-b233-707662e564ed" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:38:37.312+0100" uniqueID="468226c6-d1e0-4f5d-b20f-651472697841" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES6" timeCreated="2011-12-09T10:38:58.662+0100" lastModified="2011-12-09T10:39:19.554+0100" uniqueID="1bc93a57-1552-4f06-9e38-c8a431a825d2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:39:19.386+0100" uniqueID="92a7e7f1-7c7d-4405-b5dd-e32e378bf983" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:39:19.389+0100" uniqueID="af73fdfe-7dbd-4819-b0db-8e0a4f8a5ef6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES7" timeCreated="2011-12-09T10:39:31.325+0100" lastModified="2011-12-09T10:39:42.659+0100" uniqueID="425c11b0-8a9f-43cf-8eb2-541d315245cc" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES7">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:39:42.505+0100" uniqueID="67820fa1-bcc3-40c5-92c0-158f14a39b4c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:39:42.508+0100" uniqueID="de3e5cb9-36b1-4517-b82e-c632807a10ee" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES8" timeCreated="2011-12-09T10:39:59.211+0100" lastModified="2011-12-09T10:40:12.296+0100" uniqueID="e18dd2e8-9444-463f-950b-20b3027045a7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES8">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:40:12.138+0100" uniqueID="57ba688e-6cb5-4052-854e-c34d4cb1bd23" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:40:12.143+0100" uniqueID="69373fae-9f3a-45ec-baf0-2b8b990838a0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES9" timeCreated="2011-12-09T10:40:28.677+0100" lastModified="2011-12-09T10:40:40.318+0100" uniqueID="38ea26e6-a9bf-42c2-a6ae-1aac4b08a8e7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES9">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:40:40.165+0100" uniqueID="f6b2efb1-9d40-4760-8143-2b07a7710709" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:40:40.170+0100" uniqueID="6cd3b5e4-59f1-42cc-a5d9-fc65d0abe32a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES10" timeCreated="2011-12-09T10:40:56.284+0100" lastModified="2011-12-09T10:41:11.717+0100" uniqueID="93cc4f21-5073-404a-a225-37eb9ee8af47" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES10">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:41:11.554+0100" uniqueID="7e5e73a0-0a1b-478d-b299-b7072b9c8829" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:41:11.556+0100" uniqueID="0c83358e-61fc-4ad8-b88c-ff5ca6186386" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RES11" timeCreated="2011-12-09T10:41:28.498+0100" lastModified="2011-12-09T10:41:38.644+0100" uniqueID="e5e0e2fb-d760-48d4-9e80-786ed8a2a9f0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RES11">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:41:38.495+0100" uniqueID="28710ec2-41a4-4d7b-b33e-dbe8e2e2b961" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:41:38.497+0100" uniqueID="1ba4509a-8af9-4637-abb7-a801d0da53d8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Lease Creation &amp; Prolongation" timeCreated="2011-12-07T15:48:36.519+0100" lastModified="2011-12-09T15:11:10.084+0100" uniqueID="9d06cf51-35c4-4f5e-83e3-98f9899dc470" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:23.483+0200" uniqueID="15e9651d-e3a0-4bb5-bb74-3ef7a99bd7b6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:23.488+0200" uniqueID="bdcc89fb-38d0-4240-a65f-ffc7779f9da7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="LCP1" timeCreated="2011-12-09T10:42:41.299+0100" lastModified="2011-12-09T10:42:54.751+0100" uniqueID="f3468bec-b766-4f29-b0d4-f75124d8a62c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LCP1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:42:54.564+0100" uniqueID="96e35d64-0154-4453-912c-b71f302e8b4a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:42:54.567+0100" uniqueID="2ee3fb64-13a3-46d0-86da-1ddfd62a322d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LCP2" timeCreated="2011-12-09T10:43:08.283+0100" lastModified="2011-12-09T10:43:33.865+0100" uniqueID="2dacb8b6-42e5-4c64-95d4-3f93c4129ee1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LCP2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:43:33.726+0100" uniqueID="ba9518a7-dc61-498c-8100-698ce1457909" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:43:33.727+0100" uniqueID="483b2f65-4841-4c62-8236-067664918b45" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LCP3" timeCreated="2011-12-09T10:44:07.779+0100" lastModified="2011-12-09T10:46:36.610+0100" uniqueID="79540444-2edb-491d-a94c-fe3036855318" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LCP3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:46:23.090+0100" uniqueID="0fb6eb53-6fb9-4760-afb3-c0307931badd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:46:23.093+0100" uniqueID="ab962c41-ebbb-4465-865d-d0afc690da45" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LCP4" timeCreated="2011-12-09T10:46:47.714+0100" lastModified="2011-12-09T10:47:19.540+0100" uniqueID="bc617e38-f4ef-40ec-9ac4-4015bada68cb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LCP4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:47:10.854+0100" uniqueID="3d9478c9-dddb-404b-a108-b1c3a9aca4b7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:47:10.856+0100" uniqueID="420f2a8a-5a86-434f-93d8-53219a8f6a71" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LCP5" timeCreated="2011-12-09T10:47:34.240+0100" lastModified="2011-12-09T10:47:39.015+0100" uniqueID="e7badb39-2c8d-4877-858c-3b8472aefb53" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LCP5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:47:38.997+0100" uniqueID="a4e91a39-4f8a-4794-a945-15702961563d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:47:38.999+0100" uniqueID="ec1c0a10-0c34-4299-8c45-dc0bcb460d79" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LCP6" timeCreated="2011-12-09T10:47:52.241+0100" lastModified="2011-12-09T10:48:29.293+0100" uniqueID="aa425d18-0432-4c3e-a192-cabd2278a65a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LCP6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:48:06.216+0100" uniqueID="e4ef1330-57a6-4e5a-af9e-315c9af0b4d2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:48:06.218+0100" uniqueID="ab45abd3-e515-4901-840b-f72167d464a1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LCP7" timeCreated="2011-12-09T10:48:59.153+0100" lastModified="2011-12-09T10:49:03.021+0100" uniqueID="7ca69604-26a9-4637-8f70-a20d91f91475" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LCP7">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:49:03.008+0100" uniqueID="746ec08c-cdcf-4cbd-a399-0c0514517914" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:49:03.009+0100" uniqueID="e2856416-9379-4afc-8cfd-b42da2f01733" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LCP8" timeCreated="2011-12-09T10:49:25.088+0100" lastModified="2011-12-09T10:50:21.947+0100" uniqueID="a2c6f466-9021-4d14-95eb-9993fb58f170" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LCP8">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:49:36.026+0100" uniqueID="183ba3e7-5206-4b28-8e5c-814eb3e03cae" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:49:36.027+0100" uniqueID="7bd4ef93-fe48-4947-8810-3852a2cae38f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LCP9" timeCreated="2011-12-09T10:49:57.074+0100" lastModified="2011-12-09T10:50:18.872+0100" uniqueID="0e61df59-ade3-4561-b9b4-ce145f2f04db" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LCP9">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:50:18.698+0100" uniqueID="edaf6308-0a48-41e7-b8aa-4e16fdba3589" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:50:18.699+0100" uniqueID="7e2f01d2-358d-452c-9a75-f62e4641dba0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Lease Termination" timeCreated="2011-12-07T15:48:58.773+0100" lastModified="2011-12-09T15:11:10.187+0100" uniqueID="130fa138-d2c8-44e5-963f-856d1b566a96" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:27.887+0200" uniqueID="0d31ac3b-ad0f-467b-9b52-7cac0f4e7bc6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:27.892+0200" uniqueID="81b8d02d-459a-4667-a149-ffb230b48e8d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="LET1" timeCreated="2011-12-09T10:53:00.208+0100" lastModified="2011-12-09T10:53:29.896+0100" uniqueID="a3a066a3-7106-48ba-ac3d-42d81e903df7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LET1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:53:29.715+0100" uniqueID="fedbf800-abe1-48fc-bef0-80e96b04ed6b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:53:29.716+0100" uniqueID="f6296d89-b499-477f-bd14-3e1a864c2b90" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LET2" timeCreated="2011-12-09T10:53:47.259+0100" lastModified="2011-12-09T10:54:33.158+0100" uniqueID="6a6a848e-0450-44ca-9cf1-6afc33971c92" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LET2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:54:32.944+0100" uniqueID="6c8020e5-83dc-40a0-8b7a-b863cfb03aa9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:54:32.945+0100" uniqueID="89146859-9214-4da1-af08-0fad211d0b43" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LET3" timeCreated="2011-12-09T10:54:43.198+0100" lastModified="2011-12-09T10:54:54.622+0100" uniqueID="2d128c9d-503a-4168-af03-926c56060ea5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LET3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:54:54.452+0100" uniqueID="65439953-c149-43e6-8d9a-8f090a6e3d9a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:54:54.453+0100" uniqueID="8ac1231f-916a-43ba-b495-059e46898a3a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LET4" timeCreated="2011-12-09T10:55:14.493+0100" lastModified="2011-12-09T10:55:27.390+0100" uniqueID="2253acf5-c2b2-455b-9f0c-dbddedc4056b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LET4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:55:27.214+0100" uniqueID="2a091d8d-20c0-47c3-b60e-719367e7671e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:55:27.214+0100" uniqueID="0c60f5b7-c8a3-4f69-99ad-67d2de2184a9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LET5" timeCreated="2011-12-09T10:55:42.721+0100" lastModified="2011-12-09T10:56:07.670+0100" uniqueID="0759341c-d91b-4ce4-ab7f-5cc599a9f97f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LET5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:56:00.535+0100" uniqueID="929dd328-2644-4842-9cf1-12fc0ec6e2a3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:56:00.536+0100" uniqueID="e4fa4415-7d97-4e0a-986d-55fcbdb4d348" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LET6" timeCreated="2011-12-09T10:56:19.454+0100" lastModified="2011-12-09T10:56:29.486+0100" uniqueID="91c4dbf0-9eae-4591-a15d-4ea8ec705d87" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LET6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:56:29.295+0100" uniqueID="3b36cd98-3480-4a46-8422-9b103fd66945" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:56:29.296+0100" uniqueID="8d494a76-818e-4146-9f46-a01947a5a5a4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Accounts and Capabilities" timeCreated="2011-12-09T15:11:42.025+0100" lastModified="2011-12-09T15:11:52.777+0100" uniqueID="97672f30-dd50-4d35-84e5-5e4c7d9d039d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
      <creator name="" timeCreated="2014-08-10T12:22:31.678+0200" uniqueID="cb37b4b4-2e02-48da-bbc1-2017608370d6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2014-08-10T12:22:31.683+0200" uniqueID="1ec13a93-c6f9-4c9f-a51d-b72d3bbef9be" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Reader Account" timeCreated="2011-12-07T15:49:21.604+0100" lastModified="2011-12-09T15:11:52.407+0100" uniqueID="ec97b3b1-33d9-46cc-b9e8-85022e59d6a2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:35.825+0200" uniqueID="032b23eb-8715-45af-9a12-2a23c2d5f072" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:35.831+0200" uniqueID="2ee24203-056e-4844-9714-da92c529ab56" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="RAC1" timeCreated="2011-12-09T10:56:53.289+0100" lastModified="2011-12-09T10:57:06.279+0100" uniqueID="cf9a24a6-3a63-45dc-a61b-8bfb81c350a7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:57:06.064+0100" uniqueID="ce6be4a8-1c21-465e-abf9-848699744d1b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:57:06.065+0100" uniqueID="5bdde281-564a-4996-a6fc-7782c2d0c553" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RAC2" timeCreated="2011-12-09T10:57:23.326+0100" lastModified="2011-12-09T10:57:38.713+0100" uniqueID="12259d1e-8978-49ab-afef-0fb2eef52c4a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:57:38.495+0100" uniqueID="cb3ca5f7-783f-4e5b-a19f-426f0d41a3c0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:57:38.496+0100" uniqueID="f655f4ef-b6d3-4cc2-93d0-0029248c727b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RAC3" timeCreated="2011-12-09T10:57:49.011+0100" lastModified="2011-12-09T10:58:35.578+0100" uniqueID="13fa5a92-35e2-46e5-a956-addc62dd0590" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:58:35.346+0100" uniqueID="38d1d2b2-1f78-4816-a0b9-aa8660f6e3c3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:58:35.347+0100" uniqueID="77d5336c-e1a6-4872-b938-aafed53a5db2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RAC4" timeCreated="2011-12-09T10:58:47.252+0100" lastModified="2011-12-09T10:59:06.466+0100" uniqueID="c2130967-a3b3-4356-9f4c-611635f98189" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:59:06.232+0100" uniqueID="e169e9a8-6483-4450-9e05-f98aeca84e03" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:59:06.233+0100" uniqueID="a6ea7969-5361-445f-b5bb-cdf07470f344" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RAC5" timeCreated="2011-12-09T10:59:25.760+0100" lastModified="2011-12-09T10:59:48.174+0100" uniqueID="a995072e-2307-411e-a5a9-8473d2de2cd3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T10:59:47.941+0100" uniqueID="2dc4c40e-efbb-440e-a814-0cdc71fcf198" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T10:59:47.942+0100" uniqueID="ea39af99-dc47-4e10-9dbc-fa85295d367f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RAC6" timeCreated="2011-12-09T11:00:10.834+0100" lastModified="2011-12-09T11:02:13.046+0100" uniqueID="6e83b44c-a711-4032-bb4a-3fb004c01854" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T11:00:21.610+0100" uniqueID="eb4d370e-3fa7-414d-9cc6-c05c3aba8f8a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T11:00:21.611+0100" uniqueID="7e991e73-4467-4ef0-9e87-b052bf532075" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RAC7" timeCreated="2011-12-09T11:01:17.182+0100" lastModified="2011-12-09T11:02:11.021+0100" uniqueID="2457b297-690c-409b-bf0d-99d003c1b7f9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC7">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T11:01:33.870+0100" uniqueID="324f6eca-4afb-4ac4-ac3a-0a70bf707014" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T11:01:33.872+0100" uniqueID="21f18be7-2b41-4e12-9b22-64a0e56eedf9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RAC8" timeCreated="2011-12-09T11:01:53.016+0100" lastModified="2011-12-09T11:02:07.869+0100" uniqueID="92031f3a-3d51-4aae-b035-6e625ee8d6a6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC8">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T11:02:07.609+0100" uniqueID="c2e5cb3e-bae0-4596-b99c-0f54e1377d12" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T11:02:07.610+0100" uniqueID="a832130f-5dcf-41ec-b282-040821193493" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RAC9" timeCreated="2011-12-09T11:02:29.894+0100" lastModified="2011-12-09T11:02:46.780+0100" uniqueID="4f78753e-dac8-4cc1-90e6-f41ea403fcce" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC9">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T11:02:46.533+0100" uniqueID="ca2d51d6-e2bf-42e4-8af3-6bfc11533053" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T11:02:46.534+0100" uniqueID="3f725e86-6e4f-42ff-847f-5d57765abae7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="RAC10" timeCreated="2011-12-09T12:37:33.293+0100" lastModified="2011-12-09T12:37:38.236+0100" uniqueID="17e4fe4c-2a85-4fe3-b9ab-e423bd1d5233" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="RAC10">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T12:37:38.218+0100" uniqueID="1ec7aeaa-062b-45e7-adac-1f16dd05f7f7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:37:38.219+0100" uniqueID="3be7f606-1d38-4cff-b2c0-aa06129291b1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Fees &amp; Fines" timeCreated="2011-12-07T15:49:57.300+0100" lastModified="2011-12-09T15:11:52.552+0100" uniqueID="948bcdf7-d8d7-44c8-a689-8bc1080b7ea7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:40.772+0200" uniqueID="a8239c6d-40df-4f74-b4b2-a85740cf89c0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:40.778+0200" uniqueID="eca694cd-0005-4938-8a4a-bc756143049c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="FAF1" timeCreated="2011-12-09T12:38:07.958+0100" lastModified="2011-12-09T12:38:18.702+0100" uniqueID="ae9c0ab1-ca23-40c6-8458-66dc85a7e12e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T12:38:18.444+0100" uniqueID="bb8740d5-941d-47d4-a904-3c6b46e769a1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:38:18.445+0100" uniqueID="ce665c42-5213-4032-a951-be9a2c1b94fb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF1a" timeCreated="2011-12-09T12:39:49.099+0100" lastModified="2011-12-09T12:40:45.936+0100" uniqueID="7684868e-6b2c-4027-bc56-0aa9a50a11f9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF1a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T12:40:01.187+0100" uniqueID="eef88244-ac6c-41e3-8741-6985df385d08" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:40:01.188+0100" uniqueID="f4ebc5c9-5290-48fd-bf59-b91149e1184b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF1b" timeCreated="2011-12-09T12:40:28.575+0100" lastModified="2011-12-09T12:40:42.615+0100" uniqueID="be3829c2-bbc0-4cb7-a235-b3b01c6bb130" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF1b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T12:40:42.399+0100" uniqueID="cb1d5deb-9e99-468a-b58f-70adf50333d7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:40:42.403+0100" uniqueID="df666ffa-4f2a-41da-8cc3-f19002422449" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF1c" timeCreated="2011-12-09T12:41:07.399+0100" lastModified="2011-12-09T12:41:13.902+0100" uniqueID="0b3ed269-6494-4aed-8bd2-fefd408231b9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF1c">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T12:41:13.882+0100" uniqueID="b4baf5b9-fd5d-461f-b690-f54874a4cb88" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:41:13.883+0100" uniqueID="5346abaa-5e52-4cd1-8223-24ef2d8d9d61" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF1d" timeCreated="2011-12-09T12:42:11.830+0100" lastModified="2011-12-09T12:42:53.747+0100" uniqueID="2d3fa2bf-4a7b-4415-9844-c4ef156901cb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF1d">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T12:42:53.479+0100" uniqueID="6eee30b3-8c86-40eb-9190-6936cbce9e91" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:42:53.480+0100" uniqueID="2bf52848-f825-4ec9-bd12-b9f71974c896" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF2" timeCreated="2011-12-09T12:43:13.774+0100" lastModified="2011-12-09T12:48:17.234+0100" uniqueID="db00b487-a439-4af8-bfbe-69ba83fd29cd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T12:48:17.006+0100" uniqueID="cd1407cf-bc35-41bb-913a-b3406fd50969" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:48:17.008+0100" uniqueID="45bab70f-183c-45db-b787-f583ca5c549e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF2a" timeCreated="2011-12-09T12:48:37.730+0100" lastModified="2011-12-09T12:58:00.552+0100" uniqueID="9da28b52-7af0-4c11-bb11-7d624cfb93d5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF2a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T12:49:37.494+0100" uniqueID="e8c66069-47e1-4610-889d-c9b0bcf13b4c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:49:37.495+0100" uniqueID="6296ca18-7cfe-4031-bfa5-671840118d78" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF2b" timeCreated="2011-12-09T12:55:32.523+0100" lastModified="2011-12-09T12:57:56.809+0100" uniqueID="43dbe2cc-6d62-4bc8-9e7e-a9fedfaa62ec" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF2b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T12:57:56.797+0100" uniqueID="b520b96c-92e3-4d33-b5e6-60c594e5c48a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T12:57:56.798+0100" uniqueID="a142e6fd-0a7b-4d9d-a8fa-3bdbe5022c53" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF2c" timeCreated="2011-12-09T12:59:10.648+0100" lastModified="2011-12-09T13:09:55.959+0100" uniqueID="8148cf66-058d-4025-b648-07980f30c3a0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF2c">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:04:29.113+0100" uniqueID="552cc506-1689-460e-8abe-119d10ccf4b8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:04:29.117+0100" uniqueID="73c2c939-cdc1-4335-ba51-1638c97e3cc6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF2d" timeCreated="2011-12-09T13:10:20.088+0100" lastModified="2011-12-09T13:11:21.117+0100" uniqueID="fa5ea99a-267b-492b-815c-62fa925c1717" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF2d">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:10:32.001+0100" uniqueID="7fffd148-a3f2-42af-adc1-40715eb31fd3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:10:32.004+0100" uniqueID="10ead9e0-d1d9-4835-a3e1-9edc1af992ad" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF3" timeCreated="2011-12-09T13:10:51.738+0100" lastModified="2011-12-09T13:11:18.759+0100" uniqueID="ae2ffed5-028b-42d6-9875-6da8528bb3c5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:11:18.558+0100" uniqueID="2ff1d0c3-3ced-4918-8dda-c2cf1d8e8f35" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:11:18.561+0100" uniqueID="8b2c4e66-a919-4535-a71a-8b814a7b0b00" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF3a" timeCreated="2011-12-09T13:11:32.219+0100" lastModified="2011-12-09T13:11:42.793+0100" uniqueID="b4d8c544-5080-475a-87c5-121cbacccce5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF3a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:11:42.761+0100" uniqueID="2cc4f60e-8455-49e3-a66e-399dd978b6a5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:11:42.765+0100" uniqueID="0e87da1b-f229-4f41-a31b-60a56af5cf0f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="FAF3b" timeCreated="2011-12-09T13:11:54.232+0100" lastModified="2011-12-09T13:12:08.020+0100" uniqueID="5304df70-c922-428f-889d-242e17e15881" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="FAF3b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:12:07.795+0100" uniqueID="c78b0d5e-ffd2-455d-b1e2-69528f2a9ef1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:12:07.798+0100" uniqueID="0ca1ebb2-ff34-481e-b4ab-594c115d29fa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Records &amp; Inspection" timeCreated="2011-12-07T15:50:24.272+0100" lastModified="2011-12-09T15:11:52.663+0100" uniqueID="025561fe-3a84-4182-80b5-9117cbac89cb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:44.608+0200" uniqueID="6f189de2-4e45-4064-aca1-300713dac258" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:44.613+0200" uniqueID="95d02074-140d-4756-abce-875b7d51b59c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="REIN1" timeCreated="2011-12-09T13:13:04.952+0100" lastModified="2011-12-09T13:24:31.456+0100" uniqueID="2bd5db16-fe65-48e4-a176-d22de1560150" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:13:40.691+0100" uniqueID="ea94377b-798e-4140-82da-1dc57908106e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:13:40.694+0100" uniqueID="1cbd68cc-2e31-4f00-8b6f-4138bf1796ea" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="REIN1" timeCreated="2011-12-09T13:14:44.694+0100" lastModified="2011-12-09T13:24:29.797+0100" uniqueID="908087b9-f96f-48a8-b01a-13d2f9209177" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN1a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:14:59.268+0100" uniqueID="3d33edb9-4a3b-4db8-91c8-b8caf3ca44de" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:14:59.272+0100" uniqueID="950555a3-6af7-43c8-bf54-384a897e5d8a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="REIN1" timeCreated="2011-12-09T13:15:38.365+0100" lastModified="2011-12-09T13:16:10.166+0100" uniqueID="54c7a4dc-f4de-4f11-9eb2-48d382bd486e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN1b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:16:09.792+0100" uniqueID="7d7d0e5a-2f72-4119-8e25-4f57e2ceb093" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:16:09.796+0100" uniqueID="2c1c53e1-2742-48b1-9b01-33e538e01c42" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="REIN2" timeCreated="2011-12-09T13:16:36.218+0100" lastModified="2011-12-09T13:24:27.254+0100" uniqueID="1594d84d-9239-439a-9391-2a48eb67bcda" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:24:27.225+0100" uniqueID="3bf88f68-0b4b-422b-b61b-14532c03cbcc" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:24:27.228+0100" uniqueID="c50f9946-57d4-4740-a185-0089137a46a8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="REIN3" timeCreated="2011-12-09T13:24:59.163+0100" lastModified="2011-12-09T13:25:44.831+0100" uniqueID="39040be8-d9a7-4287-abdf-e56c277236f0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:25:15.762+0100" uniqueID="780ab28c-0841-4e90-bc16-cfc5b01baca6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:25:15.765+0100" uniqueID="1a41a4e5-512d-4daa-93fc-82c65a039686" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="REIN4" timeCreated="2011-12-09T13:25:31.465+0100" lastModified="2011-12-09T13:25:42.348+0100" uniqueID="9b16ce05-786d-4893-a409-36179183ef3a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:25:42.120+0100" uniqueID="bf25f709-b55c-4d87-989b-db7972818a8a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:25:42.123+0100" uniqueID="671a361b-9411-4d5e-ae08-8db69123d02d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="REIN5" timeCreated="2011-12-09T13:26:04.903+0100" lastModified="2011-12-09T13:26:23.808+0100" uniqueID="088abcf3-cea6-472a-99a4-46eb974509c3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:26:23.584+0100" uniqueID="3424f6b9-6009-466b-950a-fa2910c7317d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:26:23.585+0100" uniqueID="c6ce2511-cd9f-4a86-aeeb-1d33c7e113b4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="REIN6" timeCreated="2011-12-09T13:27:15.038+0100" lastModified="2011-12-09T13:27:54.081+0100" uniqueID="5103da25-6c4b-493c-b0f2-3ddcde5c0618" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:27:53.732+0100" uniqueID="d34579bf-751d-4d93-9385-db9c938a49b4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:27:53.734+0100" uniqueID="18ab21cc-f358-4adb-b575-c68b95976148" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="REIN7" timeCreated="2011-12-09T13:28:48.200+0100" lastModified="2011-12-09T13:29:30.271+0100" uniqueID="c6ceb107-5094-437a-b4bb-cedaee2fdfd2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN7">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:29:30.022+0100" uniqueID="a293d76d-416f-421c-8f26-8174f75d56f5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:29:30.025+0100" uniqueID="601be050-a73c-4bd6-ba53-2a3a151f76bf" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="REIN8" timeCreated="2011-12-09T13:29:55.827+0100" lastModified="2011-12-09T13:30:01.738+0100" uniqueID="b8b38bb6-9cb6-4ab1-b414-322b3aef8237" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="REIN8">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:30:01.714+0100" uniqueID="2f360da6-6357-438f-be3a-aa4abe48526d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:30:01.716+0100" uniqueID="9b3f5525-f491-4839-ac18-4e0d8903442b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="User Capabilities" timeCreated="2011-12-07T15:51:19.962+0100" lastModified="2011-12-09T15:11:52.776+0100" uniqueID="fb23feed-1af7-4a35-812b-6c125fed0b52" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:22:48.382+0200" uniqueID="09eeae53-bbb3-4674-89e9-ee300ea876aa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:22:48.387+0200" uniqueID="647483ac-2ae8-4d15-8c70-b8bcf9c3be3f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="UCAP1" timeCreated="2011-12-09T13:30:40.735+0100" lastModified="2011-12-09T13:35:40.607+0100" uniqueID="51c3e620-98a2-4f79-ae0d-fefe80b11fa3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UCAP1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:31:51.310+0100" uniqueID="b8bbf059-41f1-47ce-b60c-ea491e2cc29e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:31:51.311+0100" uniqueID="6e8175e3-9f33-41c8-be18-d1d961ace1e6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UCAP2" timeCreated="2011-12-09T13:32:33.210+0100" lastModified="2011-12-09T13:33:49.070+0100" uniqueID="d4209a3e-ecb5-4841-9118-96a805e8af39" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UCAP2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:33:03.577+0100" uniqueID="f0edea08-65cd-4f0d-a99b-886b8ebd8956" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:33:03.578+0100" uniqueID="ddb6520a-6567-4df6-8c2c-330c4515f149" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UCAP3" timeCreated="2011-12-09T13:33:23.560+0100" lastModified="2011-12-09T13:33:43.911+0100" uniqueID="a1965241-c0f5-4cdc-bbc9-244ae602d118" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UCAP3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:33:43.644+0100" uniqueID="65f8685a-7873-4fec-b7cf-872142b7d047" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:33:43.647+0100" uniqueID="007d72c0-9076-4ce1-bc47-a2219e8c5a81" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UCAP3" timeCreated="2011-12-09T13:36:05.150+0100" lastModified="2011-12-09T13:36:10.924+0100" uniqueID="7c9085ba-cba9-42dd-ab0b-8d89b7274977" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UCAP3a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:36:10.885+0100" uniqueID="bc7553f3-b20f-473e-9f34-9d889d1a8f19" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:36:10.888+0100" uniqueID="e450ae09-49f7-475b-9482-0976b3064ade" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UCAP3" timeCreated="2011-12-09T13:36:59.878+0100" lastModified="2011-12-09T13:43:47.439+0100" uniqueID="105e0a43-62d7-4e27-9d6d-b103f6de8e2c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UCAP3b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:37:13.139+0100" uniqueID="4bc2d87a-127c-43b9-b47a-d171db1d3c9e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:37:13.139+0100" uniqueID="40ce26a1-4b3b-4315-84e1-6c3966724a06" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UCAP3" timeCreated="2011-12-09T13:44:00.322+0100" lastModified="2011-12-09T13:44:48.144+0100" uniqueID="c670277a-360d-4e12-be3f-0f065e2f05bf" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UCAP3c">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:44:20.204+0100" uniqueID="902d6de6-130b-4217-85ab-e668d801a37c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:44:20.207+0100" uniqueID="39d8115f-32a7-4cf2-b701-506f3f6657f2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UCAP3" timeCreated="2011-12-09T13:45:10.668+0100" lastModified="2011-12-09T13:46:10.237+0100" uniqueID="0e958a9f-b9fe-47ed-9598-283f55cf08c8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UCAP3d">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:46:09.990+0100" uniqueID="08c3340c-324d-49db-9cc6-940dcb65e00d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:46:09.993+0100" uniqueID="9d9f3443-b0c8-4a11-b149-e41bd7f45253" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UCAP4" timeCreated="2011-12-09T13:46:36.405+0100" lastModified="2011-12-09T13:51:38.893+0100" uniqueID="31274865-2171-4a28-bfe8-493fe2b7a02e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UCAP4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:51:18.908+0100" uniqueID="3fdbedf3-45c9-481a-a843-5b780d27b116" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:51:18.911+0100" uniqueID="a1c7233b-bfc7-46d3-b080-18c0960fbd8b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Administration" timeCreated="2011-12-09T15:12:29.603+0100" lastModified="2011-12-09T15:12:42.877+0100" uniqueID="94a5742f-d802-4cdb-b53f-73dddb26a3b7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
      <creator name="" timeCreated="2014-08-10T12:24:38.531+0200" uniqueID="816c2b8a-6e8f-497a-abb9-3a4c70cf95ad" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2014-08-10T12:24:38.536+0200" uniqueID="37e9e785-691e-4dd1-ab3b-f196b04ecf12" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="Setup Customization &amp; Maintenance" timeCreated="2011-12-07T15:51:56.881+0100" lastModified="2011-12-09T15:12:42.560+0100" uniqueID="233899c5-6bd4-42a6-9ee1-5ff8e9e3c48e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:24:43.366+0200" uniqueID="8267efea-8717-4333-bcdf-9b03b4466305" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:24:43.372+0200" uniqueID="63b6b1a5-8d93-4021-b033-fb2859060462" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="SCM1" timeCreated="2011-12-09T13:52:14.987+0100" lastModified="2011-12-09T13:54:42.183+0100" uniqueID="5db460bd-4b24-4069-82ce-07ee8db9d40a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SCM1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:54:42.148+0100" uniqueID="4b3702ec-28e1-41fe-aa71-31e13eec9ff4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:54:42.150+0100" uniqueID="83516e8f-5acd-470f-b54d-cbf1f6f5fc58" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SCM2" timeCreated="2011-12-09T13:54:15.274+0100" lastModified="2011-12-09T13:54:39.162+0100" uniqueID="3f818bb4-f6f4-446e-bf8e-82b9ccbb3dde" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SCM2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:54:38.920+0100" uniqueID="f57b766a-69c1-4c2e-b5c3-7b33a1eeee83" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:54:38.923+0100" uniqueID="762f0ac4-cb5a-4480-8dd9-99326f8d25af" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SCM3" timeCreated="2011-12-09T13:54:55.839+0100" lastModified="2011-12-09T13:55:15.900+0100" uniqueID="76a549e5-a479-42c6-af24-13e78dbde5b8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SCM3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:55:15.640+0100" uniqueID="2149eec6-5949-48de-a448-e827e144e4aa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:55:15.643+0100" uniqueID="3f5e8e3c-b3c7-49b8-8630-3a62f8ecb224" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SCM4" timeCreated="2011-12-09T13:55:29.651+0100" lastModified="2011-12-09T13:55:42.481+0100" uniqueID="0c27ca46-afc5-49a8-827f-c915d20fa29a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SCM4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:55:42.258+0100" uniqueID="b96f4e54-7e8d-4b9d-987c-2e7c24e8cf97" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:55:42.259+0100" uniqueID="cc576016-5d79-4a12-a4f3-acb7428bb112" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SCM5" timeCreated="2011-12-09T13:56:00.377+0100" lastModified="2011-12-09T13:56:15.763+0100" uniqueID="3f022a32-610f-49a4-b5a0-3eab96805fe9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SCM5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:56:06.531+0100" uniqueID="44c75661-f040-497a-9fea-697961b62e95" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:56:06.534+0100" uniqueID="bcc933c7-ddc1-4f7f-a5b6-95f36d2cd64c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SCM6" timeCreated="2011-12-09T13:56:39.338+0100" lastModified="2011-12-09T13:59:42.014+0100" uniqueID="a2bd6c70-0796-4a51-82a5-84eb8174d93b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SCM6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:57:49.971+0100" uniqueID="963e30e6-78f6-494c-9b9f-a88a3b4976a2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:57:49.974+0100" uniqueID="bfb8bc3a-b376-47f2-a486-b482ffd4b96d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SCM7" timeCreated="2011-12-09T13:58:15.610+0100" lastModified="2011-12-09T13:58:40.828+0100" uniqueID="d2a97477-b9c4-49a8-aae5-78f6786c3da5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SCM7">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T13:58:40.422+0100" uniqueID="0b5a9995-57fe-458b-8758-bb5ac9427b1f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T13:58:40.425+0100" uniqueID="8066ac3f-82a9-46c1-acce-07da551e02e6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Librarian Accounts" timeCreated="2011-12-09T13:59:35.848+0100" lastModified="2011-12-09T15:12:42.662+0100" uniqueID="efde8b44-fe1b-4ba2-be65-64c42de5755e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:24:47.195+0200" uniqueID="bc41dfb2-15f1-4e63-b00e-06c49f978f2a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:24:47.201+0200" uniqueID="5811a7fa-e7d1-47d8-9bb8-2f5ac197319c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="LAC1" timeCreated="2011-12-09T14:00:00.757+0100" lastModified="2011-12-09T14:00:18.203+0100" uniqueID="a98ee222-9ada-4585-9f90-59807b907771" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LAC1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:00:17.925+0100" uniqueID="391ce9c1-23f0-41aa-baf2-3999b61e56e7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:00:17.928+0100" uniqueID="e6231678-6d42-4d3a-a934-0606004252d2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LAC2" timeCreated="2011-12-09T14:04:40.883+0100" lastModified="2011-12-09T14:04:59.106+0100" uniqueID="a3f4c4a2-5738-4094-8db4-f5e74448d647" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LAC2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:04:58.828+0100" uniqueID="529b04ce-6e33-47a5-9858-a2468d423e2c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:04:58.829+0100" uniqueID="2ed8c7d0-f700-4be3-b0b3-b7e25d051009" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LAC3" timeCreated="2011-12-09T14:05:16.743+0100" lastModified="2011-12-09T14:05:38.192+0100" uniqueID="b02c5f31-1cd0-4156-9912-2e2bb910282a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LAC3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:05:37.932+0100" uniqueID="11ce28c7-e9ca-4add-98a1-f62e8bb4a180" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:05:37.935+0100" uniqueID="696b32a3-8d16-4357-ba3c-3c547903087e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LAC4" timeCreated="2011-12-09T14:07:00.513+0100" lastModified="2011-12-09T14:07:15.001+0100" uniqueID="03ad5347-7f70-4a3a-a9ae-7d7a0796f51b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LAC4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:07:14.753+0100" uniqueID="8a0964e7-295e-4c1e-83ee-97909be330aa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:07:14.756+0100" uniqueID="33f17726-c92d-4f5b-a72c-5c097218fea0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LAC5" timeCreated="2011-12-09T14:07:32.919+0100" lastModified="2011-12-09T14:07:47.296+0100" uniqueID="6ba3d7c0-c3fc-43c2-b47a-ab709a4271ec" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LAC5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:07:47.026+0100" uniqueID="50e6404c-b712-4ce3-a44a-f82921be6903" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:07:47.029+0100" uniqueID="53103199-a47a-463f-b4bd-ddf15a48213c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="LAC6" timeCreated="2011-12-09T14:08:04.114+0100" lastModified="2011-12-09T14:08:09.913+0100" uniqueID="15e307e1-65a0-4f70-9283-6b8222fab4f4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="LAC6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:08:09.894+0100" uniqueID="8c76c67d-8c1a-4c75-9d85-7157dc336ccb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:08:09.896+0100" uniqueID="b0448ad3-a8c6-41b8-ae97-c31351a81055" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Software Operation" timeCreated="2011-12-09T14:08:54.602+0100" lastModified="2011-12-09T15:12:42.776+0100" uniqueID="94aebeba-73e7-4138-93c0-79a5998bc63b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:24:51.128+0200" uniqueID="695ee850-2b9c-4cf2-b1e3-791609f98604" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:24:51.134+0200" uniqueID="b99a5e0b-705a-4040-a5c8-e3651691c45f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="SOP1" timeCreated="2011-12-09T14:09:21.058+0100" lastModified="2011-12-09T14:09:48.300+0100" uniqueID="dc190a43-9286-47bf-b9c2-4cf094448312" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:09:45.628+0100" uniqueID="678177b1-2ad5-43e0-8c71-f7e7c1782264" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:09:45.629+0100" uniqueID="097cfc9a-440c-443e-bb97-5fdbb7502506" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP2" timeCreated="2011-12-09T14:09:58.747+0100" lastModified="2011-12-09T14:14:54.435+0100" uniqueID="0a704fb8-e41e-4bdb-9ef2-fef76b9f7db4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:10:13.288+0100" uniqueID="67d12e3b-31f7-4e5e-b9d7-6f055a36958f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:10:13.291+0100" uniqueID="d9c5df41-5d63-42c7-8901-ff43542f95af" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP3" timeCreated="2011-12-09T14:10:32.420+0100" lastModified="2011-12-09T14:14:50.840+0100" uniqueID="c0c9d353-9d56-4b9a-9520-aec2d333558d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:10:45.195+0100" uniqueID="9f546589-f33a-4620-a6d0-488109b91329" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:10:45.198+0100" uniqueID="bae48bb2-49bd-4c34-8369-4bdb8ea41764" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP4" timeCreated="2011-12-09T14:11:02.013+0100" lastModified="2011-12-09T14:14:47.036+0100" uniqueID="7623867e-0034-43e2-843b-d54a1780ad1d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:11:14.040+0100" uniqueID="30c79ac2-876b-4ce3-bb0b-855ce0b6b620" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:11:14.043+0100" uniqueID="6cc5efff-8d23-4825-b562-3c331400507a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP4a" timeCreated="2011-12-09T14:11:29.113+0100" lastModified="2011-12-09T14:14:43.914+0100" uniqueID="d8fbbc0a-4fdf-45e2-8ebe-036084ec252d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP4a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:11:52.979+0100" uniqueID="fa1a4d2a-7300-4a85-b34b-0fb259b71e85" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:11:52.983+0100" uniqueID="6c46cfc3-229e-4e7e-9fa7-ead882db80ea" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP4b" timeCreated="2011-12-09T14:12:13.786+0100" lastModified="2011-12-09T14:14:40.356+0100" uniqueID="9f4fc9fd-a29d-4543-9b85-169e403e06c2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP4b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:14:40.327+0100" uniqueID="74ac40f6-0e87-4739-a471-7ba5b16372be" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:14:40.329+0100" uniqueID="b46c0073-f272-4765-8c23-df86d44ebb39" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP4c" timeCreated="2011-12-09T14:13:17.229+0100" lastModified="2011-12-09T14:14:30.741+0100" uniqueID="6262dd2b-6d24-4448-a90f-ef27684410dd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP4c">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:13:31.699+0100" uniqueID="ec44cf56-e613-43a9-b851-3b4d4d101689" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:13:31.702+0100" uniqueID="29b91260-0234-4663-aca0-3eb26d9a50e5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP4d" timeCreated="2011-12-09T14:13:54.378+0100" lastModified="2011-12-09T14:14:27.581+0100" uniqueID="c28f72d2-7485-40f6-a2d4-18800bb43504" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP4d">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:14:27.276+0100" uniqueID="00addfdd-0d0c-4934-bc48-97e05dd62c43" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:14:27.280+0100" uniqueID="ba5edebe-17de-4944-b0bf-16dc3a5f403f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP4e" timeCreated="2011-12-09T14:15:45.739+0100" lastModified="2011-12-09T14:16:09.744+0100" uniqueID="c02c8d77-7891-4811-9c15-42afa896737e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP4e">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:16:09.729+0100" uniqueID="7b755800-c18a-4092-a1e6-2cd2045bc50c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:16:09.730+0100" uniqueID="9d9857d6-f3b8-45d0-8ab9-66c1fd4f9431" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP4f" timeCreated="2011-12-09T14:16:23.681+0100" lastModified="2011-12-09T14:16:40.355+0100" uniqueID="0eb4d2dd-13aa-40df-a367-96e62838772a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP4f">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:16:40.035+0100" uniqueID="2aa89987-b313-4e5c-830c-c43c18efc9e8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:16:40.038+0100" uniqueID="7668e786-3b99-40a3-b62c-3c17c54c0cb7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="SOP5" timeCreated="2011-12-09T14:16:54.007+0100" lastModified="2011-12-09T14:17:02.597+0100" uniqueID="2478e3b9-c0b5-4246-a5ae-0517f0416f6b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="SOP5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:17:02.581+0100" uniqueID="15ea253e-816d-4acf-a3a7-76d14aea6fae" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:17:02.582+0100" uniqueID="e618f1d0-5706-4fc8-93dd-7f2875b7a373" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Monitoring &amp; Reporting" timeCreated="2011-12-09T14:18:19.856+0100" lastModified="2011-12-09T15:12:42.877+0100" uniqueID="5853043c-0179-452c-ad95-1db847a77cb9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:24:56.107+0200" uniqueID="1337a7b7-13ea-4fe1-b93c-31152c2ac4d7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:24:56.113+0200" uniqueID="24127704-2b38-4348-88d9-0b3a2d71b1ba" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="MOR1" timeCreated="2011-12-09T14:18:34.022+0100" lastModified="2011-12-09T14:20:40.759+0100" uniqueID="0c91ae50-353c-4530-8e1e-8ca6ac0248d0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MOR1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:19:43.992+0100" uniqueID="01605b5e-91d7-45d3-9111-3828744cc0a8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:19:43.996+0100" uniqueID="5adb68e9-5211-43f9-98f7-fa3193fd510c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MOR2" timeCreated="2011-12-09T14:20:03.172+0100" lastModified="2011-12-09T14:20:38.328+0100" uniqueID="716fcc4d-b7b0-4f4f-a3b5-59438b705716" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MOR2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:20:38.024+0100" uniqueID="82fd2092-59c9-4f12-a175-a7bf1fde420d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:20:38.025+0100" uniqueID="ccaeeb61-bc0d-4445-a6d2-5c8ee543ceda" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MOR3" timeCreated="2011-12-09T14:21:00.391+0100" lastModified="2011-12-09T14:21:06.034+0100" uniqueID="f78a58c5-8b4f-4e26-96e9-b67310400791" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MOR3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:21:06.021+0100" uniqueID="a7b67b1e-db15-41f0-849c-eb4b964a3b81" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:21:06.022+0100" uniqueID="50a08552-bfa2-49c3-a35f-ec53e30b768b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MOR4" timeCreated="2011-12-09T14:21:23.375+0100" lastModified="2011-12-09T14:22:07.361+0100" uniqueID="cc8c344e-6b72-483e-b9e6-91d315a8e40b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MOR4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:21:45.497+0100" uniqueID="53a1d44f-5eb6-4d3d-b64f-717d489b4561" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:21:45.498+0100" uniqueID="9eb15cfb-e7ab-48b1-9daf-6c18686dee78" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MOR5" timeCreated="2011-12-09T14:22:29.718+0100" lastModified="2011-12-09T14:28:16.724+0100" uniqueID="554b2e64-2cbb-435c-8da7-23cdec8b54fb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MOR5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:22:40.492+0100" uniqueID="ab05e3b8-aced-40f9-90ae-b0248afa5d0c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:22:40.496+0100" uniqueID="c751d2ca-254b-4100-8686-4750bc6cdcb9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="MOR6" timeCreated="2011-12-09T14:23:02.310+0100" lastModified="2011-12-09T14:23:18.332+0100" uniqueID="32e42f00-a1d7-4fe5-a3d1-b80a83d62f28" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="MOR6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:23:18.053+0100" uniqueID="973f3a80-ddd9-4692-b307-8376c4e3cd1a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:23:18.054+0100" uniqueID="87a83fe7-a79a-4566-ab9e-d6ca786944d2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Crosscutting Concerns" timeCreated="2011-12-09T15:07:28.523+0100" lastModified="2011-12-09T15:12:57.848+0100" uniqueID="7ffbc6df-2131-4ad7-bfa8-86fdb239e7e6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
      <creator name="" timeCreated="2014-08-10T12:25:28.459+0200" uniqueID="f60376f1-f519-420f-82c9-110d657b81a4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </creator>
      <changeList/>
      <responsibleUser name="" timeCreated="2014-08-10T12:25:28.465+0200" uniqueID="ac234da3-0bb3-4f07-bb19-0e961bbee049" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
      </responsibleUser>
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="User Interaction" timeCreated="2011-12-09T14:28:09.693+0100" lastModified="2011-12-09T15:07:43.503+0100" uniqueID="72938236-5e62-46d8-abdc-cf5244d9d698" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:25:58.110+0200" uniqueID="e9c4511e-1e03-46e3-9133-33f04b80e1fb" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:25:58.117+0200" uniqueID="b7b0a6c8-f5a0-4a74-8972-db312a083c0b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="UI1" timeCreated="2011-12-09T14:28:35.293+0100" lastModified="2011-12-09T14:29:00.435+0100" uniqueID="c5fc9a0f-6010-4fa9-9f3e-ccfe9f11c63c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:28:51.793+0100" uniqueID="a748f303-467e-4c0b-bfa2-430b1aeb8452" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:28:51.796+0100" uniqueID="cf1d2b38-a1e5-4970-9ed1-61fa041f4da6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI1a" timeCreated="2011-12-09T14:29:12.810+0100" lastModified="2011-12-09T14:29:20.723+0100" uniqueID="0badf31e-1eef-4d75-a030-e9e6ad62b818" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI1a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:29:20.704+0100" uniqueID="96fde674-279e-4883-99bd-48915938f861" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:29:20.706+0100" uniqueID="893671b3-5266-41a0-b19d-d30fe9cc611c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI1b" timeCreated="2011-12-09T14:29:30.625+0100" lastModified="2011-12-09T14:31:16.310+0100" uniqueID="87dc60e4-58b6-4054-a47d-45ad237a875c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI1b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:29:38.555+0100" uniqueID="e4596b9a-cd02-427d-9997-66248543ac67" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:29:38.556+0100" uniqueID="0ef68fbc-e1fe-49dc-bdb0-6b2a2cc1148c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI2" timeCreated="2011-12-09T14:31:26.508+0100" lastModified="2011-12-09T14:31:50.510+0100" uniqueID="84adc66f-e26b-424a-ad6d-595a597aa81a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:31:40.672+0100" uniqueID="720bcfc3-83ea-4b68-8a18-af313618b574" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:31:40.673+0100" uniqueID="2e919fee-29de-40f9-94e5-e834cfd8950e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI2a" timeCreated="2011-12-09T14:32:03.225+0100" lastModified="2011-12-09T14:32:14.379+0100" uniqueID="ffc891ba-881f-41c4-9ba0-6b329bc45395" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI2a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:32:14.069+0100" uniqueID="d0f95de8-2619-4aba-bc52-81914e5f488b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:32:14.073+0100" uniqueID="56272a25-77b5-4e4d-adb8-20c5da68a7f4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI2b" timeCreated="2011-12-09T14:32:37.287+0100" lastModified="2011-12-09T14:32:44.474+0100" uniqueID="04beeff2-3e23-4e0a-b1bb-0957c1f83a15" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI2b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:32:44.442+0100" uniqueID="9a78778c-3798-4663-93f0-0552960568de" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:32:44.445+0100" uniqueID="6364a4c8-ad3d-4b4e-9d39-73251c23a935" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI2c" timeCreated="2011-12-09T14:33:29.617+0100" lastModified="2011-12-09T14:33:37.338+0100" uniqueID="9d7eebfe-bfdd-496a-9f71-ed259e0fc39c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI2c">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:33:37.321+0100" uniqueID="7941c59a-6d05-42d7-87a1-65b273e34147" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:33:37.322+0100" uniqueID="c0b62f98-f13a-4fd7-b517-00b43f48a7c5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI2d" timeCreated="2011-12-09T14:33:59.549+0100" lastModified="2011-12-09T14:34:11.048+0100" uniqueID="2ab34767-e3b3-4ec4-b853-754ac600cd4f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI2d">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:34:10.764+0100" uniqueID="55419fe1-7bf4-4be7-8fc9-a516ca517cf0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:34:10.765+0100" uniqueID="b97411dd-4751-42dd-b648-31f53648d3d7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI3" timeCreated="2011-12-09T14:34:32.947+0100" lastModified="2011-12-09T14:34:52.544+0100" uniqueID="6f89998d-fd50-4225-94ec-ec40d2d3cc5a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:34:52.251+0100" uniqueID="a60556eb-d53a-4d6f-8a0f-2e14858c88ea" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:34:52.255+0100" uniqueID="17706904-7975-4bda-9950-e75971739f57" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI4" timeCreated="2011-12-09T14:35:14.867+0100" lastModified="2011-12-09T14:35:32.864+0100" uniqueID="45e7a1ce-f66b-4290-b917-173c1a8aceda" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:35:32.554+0100" uniqueID="1c5d73b6-6b5a-4513-be46-ba14d1446955" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:35:32.558+0100" uniqueID="2fc47591-03a5-4a73-8f23-5396ae5aa057" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI4a" timeCreated="2011-12-09T14:36:20.036+0100" lastModified="2011-12-09T14:36:55.785+0100" uniqueID="07e3e4ea-6a04-4167-802a-33a368a278d3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI4a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:36:39.538+0100" uniqueID="ec9a7825-49b1-4f44-b50b-22e5010bdefa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:36:39.541+0100" uniqueID="932b5ebe-285f-409b-96de-db933d53a7d4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI5" timeCreated="2011-12-09T14:37:19.215+0100" lastModified="2011-12-09T14:50:52.372+0100" uniqueID="612a209a-dba3-4091-aeb3-c74a2c3b55f7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:37:35.617+0100" uniqueID="31b1fa73-e140-4a76-a18e-4c4eb44d13d8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:37:35.618+0100" uniqueID="4e90c494-af15-45ce-aa42-8e9500eb9e98" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="UI6" timeCreated="2011-12-09T14:38:40.984+0100" lastModified="2011-12-09T14:39:09.917+0100" uniqueID="b5cf76e5-bf84-4942-9136-db685e6d2d48" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="UI6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:39:09.599+0100" uniqueID="b7f0a480-0c16-432d-aa22-d80746bf5d6c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:39:09.603+0100" uniqueID="4825a28b-bd33-41b1-ae59-9f77b403a979" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Web Access" timeCreated="2011-12-09T14:40:01.248+0100" lastModified="2011-12-09T15:07:43.634+0100" uniqueID="81361aa3-1d35-45ed-b842-8435b7afed7c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:25:58.913+0200" uniqueID="e26443db-988f-4260-8e0c-5b34da739f41" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:25:58.921+0200" uniqueID="73693c86-4c9c-4131-bf27-a71013afd484" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="WEB1" timeCreated="2011-12-09T14:40:46.812+0100" lastModified="2011-12-09T14:40:57.097+0100" uniqueID="feffe9e5-756a-470b-9aad-887ca941e7e3" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="WEB1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:40:57.068+0100" uniqueID="724cd4d0-bba7-4e34-9805-7ecc415792f6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:40:57.070+0100" uniqueID="8833fde8-fd0b-42af-b0fd-c0527c3daa92" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="WEB1a" timeCreated="2011-12-09T14:48:50.962+0100" lastModified="2011-12-09T14:49:22.755+0100" uniqueID="d46a05ba-7304-4268-8455-8814a9a8b29a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="WEB1a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:49:22.719+0100" uniqueID="c87362a8-7b5d-46cf-b372-9af80895881f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:49:22.722+0100" uniqueID="3ccd796f-ca5a-4f9b-8bbb-2b09a649e557" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="WEB1b" timeCreated="2011-12-09T14:49:08.836+0100" lastModified="2011-12-09T14:49:20.547+0100" uniqueID="490cf373-1c90-47cf-b26f-7bc094bd95ce" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="WEB1b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:49:20.245+0100" uniqueID="21858621-ef10-4534-8529-fe451a093ed0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:49:20.249+0100" uniqueID="141a17b2-4039-446a-b3fb-e37c49f02495" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="WEB1c" timeCreated="2011-12-09T14:49:41.835+0100" lastModified="2011-12-09T14:50:50.085+0100" uniqueID="de840cb1-2f52-425c-90f9-b2c906407854" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="WEB1c">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:49:50.678+0100" uniqueID="5b83c3fa-c739-4e23-9d8c-4e0720d304d4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:49:50.682+0100" uniqueID="3a61f352-4225-49c8-b0c5-5befab2f2c04" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="WEB2" timeCreated="2011-12-09T14:50:07.949+0100" lastModified="2011-12-09T14:50:25.658+0100" uniqueID="cce6d3fd-03e3-4a13-8ab1-0df7e8a8749e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="WEB2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:50:25.353+0100" uniqueID="87d0accb-b68b-4c51-a9f4-de52c898deb7" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:50:25.354+0100" uniqueID="3190570a-54e7-4a20-bf99-573b5f941c25" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="WEB3" timeCreated="2011-12-09T14:50:34.963+0100" lastModified="2011-12-09T14:50:47.780+0100" uniqueID="3642ca1c-5762-40e7-bb11-642055a99403" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="WEB3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:50:47.467+0100" uniqueID="4f549294-fad9-4c87-8a1f-94f791b62a52" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:50:47.470+0100" uniqueID="9df906cd-6a28-4b33-9fb5-f8e2e445f0b2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="WEB3a" timeCreated="2011-12-09T14:51:11.910+0100" lastModified="2011-12-09T14:51:27.123+0100" uniqueID="1de1e4f8-5fe3-4cc0-8e59-1b0bd112c124" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="WEB3a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:51:26.829+0100" uniqueID="2566f29a-0e25-42ab-af90-9e2f02adbd01" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:51:26.830+0100" uniqueID="75206a23-904a-45c6-9ed7-8d440b2f32b5" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="WEB3b" timeCreated="2011-12-09T14:51:43.098+0100" lastModified="2011-12-09T14:51:55.501+0100" uniqueID="2cdf4bd5-27f6-4d4e-a6c0-71b53c580851" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="WEB3b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:51:55.059+0100" uniqueID="98658fe0-9446-4476-be3e-e255377fd073" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:51:55.062+0100" uniqueID="4ac0210a-b688-4466-aa51-53331f6eaba6" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="WEB4" timeCreated="2011-12-09T14:52:17.315+0100" lastModified="2011-12-09T14:52:26.203+0100" uniqueID="165b6dcd-447f-48a0-bdf1-f75b434e4545" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="WEB4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:52:25.876+0100" uniqueID="7355decb-b2c8-4e09-9eeb-8334178dc513" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:52:25.880+0100" uniqueID="720da462-02d1-40a0-a593-8855b62a5ab8" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="Notifications" timeCreated="2011-12-09T14:52:53.363+0100" lastModified="2011-12-09T15:07:43.756+0100" uniqueID="62534c39-9581-4037-a99c-099773edc1ed" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:25:59.673+0200" uniqueID="62983242-88b4-40a2-a063-45c42fe7c643" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:25:59.680+0200" uniqueID="aecf3a87-bad7-4ca7-8740-3ce0bc3d58a0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="NAT1" timeCreated="2011-12-09T14:53:06.677+0100" lastModified="2011-12-09T14:53:53.835+0100" uniqueID="7a77fe02-b1e5-4967-8dc0-e3e63f5c077a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NAT1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:53:18.605+0100" uniqueID="97b925fe-85c4-4ca7-a347-dd9d6b222846" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:53:18.608+0100" uniqueID="26789a86-9a9d-4876-92ac-f3911040afe0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NAT2" timeCreated="2011-12-09T14:54:02.356+0100" lastModified="2011-12-09T14:54:26.994+0100" uniqueID="61f453ca-5827-4c00-9f91-2c745be224dc" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NAT2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:54:19.867+0100" uniqueID="47c180cc-fd51-457b-a482-2f4c707183db" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:54:19.868+0100" uniqueID="cb73d6ed-dda8-4b99-8b47-663e4bec224f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NAT3" timeCreated="2011-12-09T14:54:37.808+0100" lastModified="2011-12-09T14:54:55.438+0100" uniqueID="da88acb9-63a0-4872-af8e-965d2a0afdf1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NAT3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:54:55.112+0100" uniqueID="7fd193f3-8c7b-49bc-9321-227271f34c27" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:54:55.115+0100" uniqueID="d33e87ad-4646-472a-903e-6d583c63276a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NAT3a" timeCreated="2011-12-09T14:55:06.500+0100" lastModified="2011-12-09T14:55:25.120+0100" uniqueID="3a96b24a-0b9d-4fde-b667-5beadc380902" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NAT3a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:55:15.046+0100" uniqueID="0cb77939-576c-4873-a1ae-dffb8f4b995b" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:55:15.047+0100" uniqueID="dd7f2571-8c6f-4ef1-8b52-a10d7639b0d2" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NAT3b" timeCreated="2011-12-09T14:55:37.689+0100" lastModified="2011-12-09T14:55:43.160+0100" uniqueID="3a2748d8-956c-47cc-a5a4-42e2c0648065" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NAT3b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:55:43.140+0100" uniqueID="2ca8d458-570f-4d03-b025-11596060bd3c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:55:43.143+0100" uniqueID="cc9c1476-7c7c-4a34-9cf6-58e23a788330" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NAT3c" timeCreated="2011-12-09T14:56:00.848+0100" lastModified="2011-12-09T14:56:21.267+0100" uniqueID="f6cd88ed-d088-4abb-98f1-661f0f34b5fa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NAT3c">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:56:20.923+0100" uniqueID="291d778f-d938-4be9-a8fd-f0a4bd1033ac" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:56:20.927+0100" uniqueID="10bc3e94-be6a-4539-b18a-7c84fb589cec" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NAT4" timeCreated="2011-12-09T14:56:32.058+0100" lastModified="2011-12-09T14:56:48.517+0100" uniqueID="557a597e-14d2-4cbc-8163-57d3d424b1db" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NAT4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:56:48.184+0100" uniqueID="a2be021e-d019-4288-ac60-a3073843e117" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:56:48.188+0100" uniqueID="f59b4156-323b-4b27-ac98-099056ac1487" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NAT5" timeCreated="2011-12-09T14:57:08.453+0100" lastModified="2011-12-09T14:57:20.422+0100" uniqueID="73a6e281-6727-4a25-b837-0e879470fe67" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NAT5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:57:20.402+0100" uniqueID="f9eb4e9a-cdc3-485c-aeba-855230ea3561" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:57:20.405+0100" uniqueID="a3771aef-85d5-4766-961c-26311a40569c" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="NAT6" timeCreated="2011-12-09T14:57:30.948+0100" lastModified="2011-12-09T14:57:43.562+0100" uniqueID="77300c0d-cbb6-46e3-8cce-e604d2c0380f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="NAT6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:57:43.223+0100" uniqueID="ba762469-1f6e-4a43-a785-db63ff21660a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:57:43.227+0100" uniqueID="6fc63452-c847-43b8-97f3-382c65c7871f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
    <contents xsi:type="folder:Folder" name="Third Party Systems" timeCreated="2011-12-09T15:06:22.516+0100" lastModified="2011-12-09T15:13:04.630+0100" uniqueID="f7daa6a9-2422-4ade-b64b-fed2056cdc6e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
      <cost name="Cost" kind=""/>
      <benefit name="Cost" kind=""/>
      <contents xsi:type="folder:Folder" name="BookStation" timeCreated="2011-12-09T15:01:48.462+0100" lastModified="2011-12-09T15:06:42.362+0100" uniqueID="ba10a192-4b73-4174-8b3b-63ef2578a68e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:26:00.441+0200" uniqueID="91ed875e-7968-4081-a9da-9c8d60b547c9" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:26:00.447+0200" uniqueID="7c71daca-0033-483a-8eee-b110e468cb5a" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="BS1" timeCreated="2011-12-09T15:02:04.152+0100" lastModified="2011-12-09T15:02:21.542+0100" uniqueID="4f4123a8-3c11-42b8-9967-27cc11008ffd" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BS1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T15:02:21.193+0100" uniqueID="6bc4e3cf-c4cc-487a-93f0-942ca5900096" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:02:21.195+0100" uniqueID="3f70e6c6-d1c2-4106-8222-af22d53d69b4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="BS2" timeCreated="2011-12-09T15:02:39.156+0100" lastModified="2011-12-09T15:03:09.044+0100" uniqueID="ebef8953-ab23-4a00-a576-e9422f5fa481" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BS2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T15:03:08.715+0100" uniqueID="9425c007-1e33-42dc-8f20-a3676586a854" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:03:08.717+0100" uniqueID="9cdab1a7-021e-4593-8dd5-4a18280d2669" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="BS3" timeCreated="2011-12-09T15:03:20.465+0100" lastModified="2011-12-09T15:03:40.055+0100" uniqueID="1fd41505-e25e-4cd8-9950-c93f159bf263" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BS3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T15:03:39.681+0100" uniqueID="f422bbdf-0174-482a-a8de-a2543aba9472" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:03:39.685+0100" uniqueID="75b623dc-0e5a-40e8-9e53-af66ac696240" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="BS4" timeCreated="2011-12-09T15:03:51.988+0100" lastModified="2011-12-09T15:04:01.466+0100" uniqueID="702b4f12-3e5d-4723-83a8-c57ebbc74c8e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BS4">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T15:04:01.105+0100" uniqueID="fff7314a-0354-44f9-9deb-c1768d4e29a1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:04:01.109+0100" uniqueID="378be913-6cf9-4646-ba05-5af1ae159e10" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="BS5" timeCreated="2011-12-09T15:04:19.518+0100" lastModified="2011-12-09T15:05:06.547+0100" uniqueID="4378eab9-9385-47a7-b7b4-05d492b34306" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BS5">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T15:04:34.379+0100" uniqueID="bbbcb354-7a2e-413c-8bf2-ae85305c312e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:04:34.382+0100" uniqueID="38330690-d623-4140-92b2-7f4ce65c7f9f" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="BS6" timeCreated="2011-12-09T15:04:54.053+0100" lastModified="2011-12-09T15:05:03.695+0100" uniqueID="79e0a4b5-fdb3-4dc6-a801-b370453c7f37" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BS6">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T15:05:03.356+0100" uniqueID="494a5bc3-ed5e-4c10-81c2-e884884d74fa" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:05:03.358+0100" uniqueID="8651e66e-2741-4bc2-9576-821c9bba1537" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
      <contents xsi:type="folder:Folder" name="BookTip" timeCreated="2011-12-09T14:58:11.335+0100" lastModified="2011-12-09T15:06:43.638+0100" uniqueID="7077963f-a8ba-4b7e-89ac-bcd658ccec55" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" specialType="Requirement">
        <creator name="" timeCreated="2014-08-10T12:25:52.573+0200" uniqueID="c8e3cafb-d513-4f9d-959b-577586ca6c95" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </creator>
        <changeList/>
        <responsibleUser name="" timeCreated="2014-08-10T12:25:52.580+0200" uniqueID="87d33bc1-7980-4e79-86d8-877564395b70" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
        </responsibleUser>
        <cost name="Cost" kind=""/>
        <benefit name="Cost" kind=""/>
        <contents xsi:type="requirement:Requirement" name="BT1" timeCreated="2011-12-09T14:58:26.504+0100" lastModified="2011-12-09T14:58:45.650+0100" uniqueID="2019ca0e-7e22-4d7a-9580-714507e8d4ac" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BT1">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:58:45.295+0100" uniqueID="a413e826-f84a-43f5-8f48-616fd62b70f1" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:58:45.298+0100" uniqueID="62c28fe4-48e4-4c88-8f7d-c92a446e2145" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="BT1a" timeCreated="2011-12-09T14:59:04.629+0100" lastModified="2011-12-09T14:59:56.415+0100" uniqueID="f6b569e5-8460-4eda-9fda-8cde4634edce" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BT1a">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:59:15.125+0100" uniqueID="9a199bc0-d6e8-4d8c-bc2d-523329d9c455" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:59:15.129+0100" uniqueID="69cad797-e39e-4b3a-883c-164f431145c0" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="BT1b" timeCreated="2011-12-09T14:59:35.109+0100" lastModified="2011-12-09T14:59:53.949+0100" uniqueID="fdebb5be-ac69-47cc-bb35-26eb93c27b0d" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BT1b">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T14:59:53.594+0100" uniqueID="a8c04de8-80d7-45e8-8c10-3ef9b7d29888" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T14:59:53.597+0100" uniqueID="b0420b55-565e-4780-a759-6064bb91f859" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="BT2" timeCreated="2011-12-09T15:00:07.047+0100" lastModified="2011-12-09T15:00:17.472+0100" uniqueID="a37b5adf-2c47-46be-9e71-0b863eea5112" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BT2">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T15:00:17.148+0100" uniqueID="f6edc4ea-41d2-48f3-970c-b99422031e56" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:00:17.151+0100" uniqueID="0b07ba3c-82f3-4f64-a63a-505eaea1ca24" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
        <contents xsi:type="requirement:Requirement" name="BT3" timeCreated="2011-12-09T15:00:33.554+0100" lastModified="2011-12-09T15:00:56.803+0100" uniqueID="e617dc9f-c4bd-41d3-9101-2fcaea5bf8b4" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red" workPackage="" abstractionLevel="" rationaleText="" id="BT3">
          <commentlist/>
          <creator name="" timeCreated="2011-12-09T15:00:44.179+0100" uniqueID="672cd4b4-3ba4-4267-9f2e-b402aee3b42e" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </creator>
          <changeList/>
          <responsibleUser name="" timeCreated="2011-12-09T15:00:44.180+0100" uniqueID="2b3933e7-3616-4a41-85fd-5bba6ce42709" uri="C:\_ARBEIT\TOOLS\RED\samples\LMS\Requirements.red">
            <cost name="Cost" kind=""/>
            <benefit name="Cost" kind=""/>
          </responsibleUser>
          <cost name="Cost" kind=""/>
          <benefit name="Cost" kind=""/>
          <details>
            <fragments xsi:type="text:FormattedText" text=""/>
          </details>
          <remarks>
            <fragments xsi:type="text:FormattedText" text=""/>
          </remarks>
        </contents>
      </contents>
    </contents>
  </contents>
</file:File>
