package org.eclipse.epf.richtext;

import java.util.EventListener;

public interface RichTextCommandListener extends EventListener {
	
	public void commandExecuted(RichTextCommandEvent event);

}
