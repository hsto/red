package org.eclipse.epf.richtext.actions;

public interface ILinkActionExtension {

	void linkClicked(String target);

}
