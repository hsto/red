package org.eclipse.epf.richtext.actions;

import java.util.List;

import org.eclipse.epf.richtext.IRichText;

public interface IActionExtension {
	List<RichTextAction> getActions(IRichText richText);
}
