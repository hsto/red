package org.eclipse.epf.richtext;

import org.eclipse.swt.events.TypedEvent;
import org.eclipse.swt.widgets.Event;

public class RichTextCommandEvent extends TypedEvent {
	
	public static final int LINK_COMMAND = 1;
	
	public int commandType;
	
	public RichTextCommandEvent(Event e) {
		super(e);
	}

}
