/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

/**
 * This class is responsible to provide cell selection management features for
 * the agile grid.
 * 
 * @author fourbroad
 * 
 */
public class CellSelectionManager {
	/**
	 * The agile grid that this class works for.
	 */
	private AgileGrid agileGrid;

	/**
	 * All of the selections managed by this cell selection manager.
	 */
	private Set<Cell> selections;

	/**
	 * To Be Determined selections, this case will happen when user press
	 * navigate key or mouse left button on the cell and the shift key hold on.
	 */
	private Set<Cell> tbdSelection;

	/**
	 * current focused cell.
	 */
	private Cell focusCell;

	/**
	 * Shift cell is a temporary variant that used to record the cell selected
	 * by navigate key of mouse button with the shift key hold on.
	 */
	private Cell shiftCell;

	/**
	 * The cell navigation strategy which decide how the agile grid is navigated
	 * using keyboard.
	 */
	private ICellNavigationStrategy navigationStrategy;

	/**
	 * When cell selection is changed, the selection changed listeners will be
	 * notified.
	 */
	private ArrayList<ISelectionChangedListener> selectionChangedListeners;

	/**
	 * When focus cell is changed, the focus cell changed listeners will be
	 * notified.
	 */
	private ArrayList<IFocusCellChangedListener> focusCellChangedListeners;

	/**
	 * Creates a new cell selection manager for the given agile grid, the cell
	 * navigation strategy is initialized by the parameter navigationDelegate.
	 * 
	 * @param agileGrid
	 *            The agile grid that this class works for.
	 * @param navigationStrategy
	 *            The cell navigation strategy.
	 */
	public CellSelectionManager(AgileGrid agileGrid,
			CellNavigationStrategy navigationStrategy) {
		this.agileGrid = agileGrid;
		this.focusCell = Cell.NULLCELL;
		selections = new HashSet<Cell>();
		tbdSelection = new HashSet<Cell>();
		selectionChangedListeners = new ArrayList<ISelectionChangedListener>(7);
		focusCellChangedListeners = new ArrayList<IFocusCellChangedListener>(7);
		this.navigationStrategy = navigationStrategy;

		Listener listener = new Listener() {
			public void handleEvent(Event event) {
				processEvent(event);
			}
		};
		hookEventListener(agileGrid, listener);

		addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				onSelectionChanged(event);
			}
		});

		addFocusCellChangedListener(new IFocusCellChangedListener() {
			@Override
			public void focusChanged(FocusCellChangedEvent event) {
				onFocusChanged(event);
			}
		});
	}

	/**
	 * Returns the navigation strategy for agilegrid.
	 * 
	 * @return navigationStrategy The cell navigation strategy.
	 */
	ICellNavigationStrategy getNavigationStrategy() {
		return navigationStrategy;
	}

	/**
	 * Sets navigation strategy for agilegrid.
	 * 
	 * @param navigationStrategy
	 *            The cell navigation strategy.
	 */
	void setNavigationStrategy(ICellNavigationStrategy navigationStrategy) {
		this.navigationStrategy = navigationStrategy;
	}

	/**
	 * A common method for processing event.
	 * 
	 * @param event
	 *            The mouse or key down event.
	 */
	private void processEvent(Event event) {
		Cell selectedCell = null;
		if (navigationStrategy.isNavigationEvent(agileGrid, event)) {
			agileGrid.forceFocus();
			Cell currentCell = focusCell;
			if (!tbdSelection.isEmpty()) {
				currentCell = shiftCell;
			}
			selectedCell = navigationStrategy.findSelectedCell(agileGrid,
					currentCell, event);
			if (selectedCell != null && selectedCell != Cell.NULLCELL
					&& !selectedCell.equals(currentCell)) {
				int stateMask = event.stateMask;
				if (event.keyCode == SWT.TAB) {
					stateMask = stateMask & ~SWT.SHIFT;
				}
				focusCell(selectedCell, stateMask);
				if (!agileGrid.isCellFullyVisible(focusCell.row,
						focusCell.column)) {
					agileGrid.scrollToFocus(false);
				}
			}
		}

		if (navigationStrategy.shouldCancelEvent(agileGrid, event)) {
			event.doit = false;
		}
	}

	/**
	 * Hooks all of the listeners which listen the event on the agile grid.
	 * 
	 * @param agileGrid
	 *            The agile grid this class is working for.
	 * @param listener
	 *            The listener for event.
	 */
	protected void hookEventListener(AgileGrid agileGrid, Listener listener) {
		agileGrid.addListener(SWT.KeyDown, listener);
		agileGrid.addListener(SWT.FocusIn, listener);
		agileGrid.addListener(SWT.MouseDown, listener);

		agileGrid.addListener(SWT.MouseMove, listener);
		agileGrid.addListener(SWT.MouseDoubleClick, listener);
		agileGrid.addListener(SWT.MouseHover, listener);
		agileGrid.addListener(SWT.MouseExit, listener);
	}

	/**
	 * Redraws the old focus or selected cell when focus cell is changed.
	 * 
	 * @param event
	 *            The focus cell changed event.F
	 */
	protected void onFocusChanged(FocusCellChangedEvent event) {
		Cell cell = event.getOldFocusCell();
		if (cell != Cell.NULLCELL) {
			agileGrid.redrawCells(new Cell[] { cell });
		}
	}

	/**
	 * Redraws the removed and added selections when selection is changed.
	 * 
	 * @param event
	 *            The selection changed event.
	 */
	protected void onSelectionChanged(SelectionChangedEvent event) {
		Set<Cell> needRedraw = event.getRemovedSelections();
		needRedraw.addAll(event.getAddedSelections());

		agileGrid.redrawCells(needRedraw.toArray(new Cell[] {}));
	}

	/**
	 * Returns the focus cell.
	 * 
	 * @return the cell with the focus
	 * 
	 */
	Cell getFocusCell() {
		return focusCell;
	}

	/**
	 * Decides if the given cell is focused.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return true if the cell is focused, false otherwise.
	 */
	boolean isFocusCell(int row, int col) {
		if (focusCell == Cell.NULLCELL || row <= -1 || col <= -1)
			return false;

		return focusCell.row == row && focusCell.column == col;
	}

	/**
	 * Selects the given cell and scrolls the cell to the viewable area if the
	 * cell is not visible.
	 * 
	 * @param cell
	 *            The cell to be selected.
	 * @param stateMask
	 *            the state of the keyboard modifier keys at the time the event
	 *            was generated, as defined by the key code constants in class
	 *            <code>SWT</code>.
	 */
	void focusCell(Cell cell, int stateMask) {
		if (cell == null) {
			cell = Cell.NULLCELL;
		}

		Cell oldFocusCell = this.focusCell;

		doSelectCell(cell, stateMask);

		Cell newFocusCell = this.focusCell;
		if (!newFocusCell.equals(oldFocusCell)) {
			this.fireFocusCellChanged(newFocusCell, oldFocusCell);
		}
	}

	/**
	 * Selects the given Cell. Assumes that the given cell is in the viewable
	 * area. Does all neccessary redraws.
	 * 
	 * @param cell
	 *            The cell to be selected.
	 * @param stateMask
	 *            the state of the keyboard modifier keys at the time the event
	 *            was generated, as defined by the key code constants in class
	 *            <code>SWT</code>.
	 */
	private void doSelectCell(Cell cell, int stateMask) {
		// close cell editor if active
		agileGrid.applyEditorValue();

		if (cell == null || cell == Cell.NULLCELL) {
			focusCell = Cell.NULLCELL;
			return;
		}

		// assure it is a valid cell:
		Cell valid = agileGrid.getValidCell(cell.row, cell.column);
		if (valid == null || valid == Cell.NULLCELL) {
			return;
		}

		Set<Cell> oldSelections = null;
		Set<Cell> newSelections = null;
		int style = agileGrid.getStyle();
		if ((stateMask & SWT.CTRL) != 0) { // case: CTRL key pressed.
			if (!agileGrid.isMultiSelectMode()) {
				return;
			}

			selections.addAll(tbdSelection);
			tbdSelection.clear();
			shiftCell = null;
			oldSelections = new HashSet<Cell>(selections);
			toggleSelection(valid);
			newSelections = selections;
			focusCell = valid;

			// notify selection changed listeners.
			fireSelectionChanged(newSelections, oldSelections);

		} else if ((stateMask & SWT.SHIFT) != 0) {
			// Ignore when not a multi-selection agile grid.
			if (!agileGrid.isMultiSelectMode()) {
				return;
			}

			Set<Cell> oldTBDSelection = new HashSet<Cell>(tbdSelection);
			tbdSelection.clear();

			shiftCell = valid;

			int startRow = Math.min(valid.row, focusCell.row);
			int startCol = Math.min(valid.column, focusCell.column);
			int endRow = Math.max(valid.row, focusCell.row);
			int endCol = Math.max(valid.column, focusCell.column);

			if ((style & SWTX.ROW_SELECTION) != 0) {
				CellRow cellRow = null;
				for (int i = startRow; i <= endRow; i++) {
					cellRow = new CellRow(agileGrid, i);
					tbdSelection.add(cellRow);
				}
			} else if ((style & SWTX.COLUMN_SELECTION) != 0) {
				CellColumn cellColumn = null;
				for (int i = startCol; i <= endCol; i++) {
					cellColumn = new CellColumn(agileGrid, i);
					tbdSelection.add(cellColumn);
				}
			} else {
				for (int i = startRow; i <= endRow; i++) {
					for (int j = startCol; j <= endCol; j++) {
						tbdSelection.add(new Cell(agileGrid, i, j));
					}
				}
			}

			if (!tbdSelection.equals(oldTBDSelection)) {
				// fire selection changed event.
				oldSelections = new HashSet<Cell>(selections);
				oldSelections.addAll(oldTBDSelection);
				newSelections = new HashSet<Cell>(selections);
				newSelections.addAll(tbdSelection);

				// notify selection changed listeners.
				fireSelectionChanged(newSelections, oldSelections);
			}
		} else {
			// case: no modifier key
			oldSelections = new HashSet<Cell>(selections);
			oldSelections.addAll(tbdSelection);

			clearSelectionWithoutRedraw();
			addSelectionWithoutRedraw(valid);

			newSelections = selections;

			focusCell = valid;

			shiftCell = null;

			// notify selection changed listeners.
			fireSelectionChanged(newSelections, oldSelections);
		}
	}

	/**
	 * Clears selection without redraw.
	 */
	protected void clearSelectionWithoutRedraw() {
		selections.clear();
		tbdSelection.clear();
		focusCell = Cell.NULLCELL;
	}

	/**
	 * Clears the current selection (in all selection modes).
	 */
	void clearSelection() {
		clearSelectionWithoutRedraw();
		agileGrid.redraw();
	}

	/**
	 * Toggles selection of cell. Has no redraw functionality!
	 * <p>
	 * 
	 * @param cell
	 *            The cell of agile grid.
	 * @return true, if added to selection, false otherwise.
	 */
	private void toggleSelection(Cell cell) {
		int style = agileGrid.getStyle();
		if ((style & SWTX.ROW_SELECTION) != 0) {
			CellRow cellRow = new CellRow(agileGrid, cell.row);
			if (selections.contains(cellRow)) {
				selections.remove(cellRow);
			} else {
				selections.add(cellRow);
			}

			Cell[] overlappedCells = this.agileGrid.getOverlappedCell(cell);
			for (int i = 0; i < overlappedCells.length; i++) {
				if (overlappedCells[i].row != cell.row) {
					cellRow = new CellRow(agileGrid, overlappedCells[i].row);
					if (selections.contains(cellRow)) {
						selections.remove(cellRow);
					} else {
						selections.add(cellRow);
					}
				}
			}
		} else if ((style & SWTX.COLUMN_SELECTION) != 0) {
			CellColumn cellColumn = new CellColumn(agileGrid, cell.column);
			if (selections.contains(cellColumn)) {
				selections.remove(cellColumn);
			} else {
				selections.add(cellColumn);
			}

			Cell[] overlappedCells = this.agileGrid.getOverlappedCell(cell);
			for (int i = 0; i < overlappedCells.length; i++) {
				if (overlappedCells[i].column != cell.column) {
					cellColumn = new CellColumn(agileGrid,
							overlappedCells[i].column);
					if (selections.contains(cellColumn)) {
						selections.remove(cellColumn);
					} else {
						selections.add(cellColumn);
					}
				}
			}
		} else {
			if (selections.contains(cell)) {
				selections.remove(cell);
			} else {
				selections.add(cell);
			}
		}
	}

	/**
	 * Adds the cell to the set of selection without redrawing.
	 * 
	 * @param cell
	 *            The cell which will be added to the selection set.
	 */
	protected void addSelectionWithoutRedraw(Cell cell) {
		int style = agileGrid.getStyle();
		if ((style & SWTX.ROW_SELECTION) != 0) {
			selections.add(new CellRow(agileGrid, cell.row));

			Cell[] overCells = agileGrid.getOverlappedCell(cell);
			for (int i = 0; i < overCells.length; i++) {
				if (overCells[i].row != cell.row) {
					selections.add(new CellRow(agileGrid, overCells[i].row));
				}
			}
		} else if ((style & SWTX.COLUMN_SELECTION) != 0) {
			selections.add(new CellColumn(agileGrid, cell.column));

			Cell[] overCells = agileGrid.getOverlappedCell(cell);
			for (int i = 0; i < overCells.length; i++) {
				if (overCells[i].column != cell.column) {
					selections.add(new CellColumn(agileGrid,
							overCells[i].column));
				}
			}
		} else {
			selections.add(cell);
		}
	}

	/**
	 * Selects the given cells. If scroll is true, scrolls so that the first of
	 * the given cell indexes becomes fully visible.
	 * <p>
	 * 
	 * @param selections
	 *            The selected cells. If <code>null</code> or an empty array,
	 *            clears the selection.
	 */
	void selectCells(Cell[] selections) {
		if (selections == null || selections.length < 1) {
			clearSelection();
			return;
		} else {
			try {
				ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
				agileGrid.setRedraw(false);
				if (agileGrid.isMultiSelectMode()) {
					for (int i = 0; i < selections.length; i++) {
						if (selections[i].column < layoutAdvisor
								.getColumnCount()
								&& selections[i].column >= 0
								&& selections[i].row < layoutAdvisor
										.getRowCount()
								&& selections[i].row >= 0)
							addSelectionWithoutRedraw(selections[i]);
					}
				} else {
					addSelectionWithoutRedraw(selections[0]);
				}
			} finally {
				agileGrid.setRedraw(true);
			}
		}
	}

	/**
	 * Returns true, if the given cell is selected.
	 * 
	 * @param row
	 *            the row index of cell.
	 * @param col
	 *            the column index of cell.
	 * @return true if the given cell is selected, otherwise false.
	 */
	boolean isCellSelected(int row, int col) {
		Cell valid = agileGrid.getValidCell(row, col);
		int style = agileGrid.getStyle();
		if ((style & SWTX.ROW_SELECTION) != 0) {
			valid = new CellRow(agileGrid, valid.row);
		} else if ((style & SWTX.COLUMN_SELECTION) != 0) {
			valid = new CellColumn(agileGrid, valid.column);
		}

		if (tbdSelection.contains(valid))
			return true;

		if (selections.contains(valid)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns an array of the selected cells.
	 * 
	 * @return an array of cells that are selected.
	 * 
	 */
	Cell[] getCellSelection() {
		Set<Cell> cells = new HashSet<Cell>(selections);
		cells.addAll(tbdSelection);
		return (Cell[]) cells.toArray(new Cell[0]);
	}

	/**
	 * Decide whether click fall on the selected cells or not.
	 * 
	 * @param click
	 *            The mouse click.
	 * @return true if the click fall on the selected cells, otherwise false.
	 */
	protected boolean clickInSelectedCells(Point click) {
		Cell[] selection = getCellSelection();
		if (selection == null || selection.length < 0)
			return false;
		for (int i = 0; i < selection.length; i++) {
			if (agileGrid.getCellRect(selection[i].row, selection[i].column)
					.contains(click))
				return true;
		}
		return false;
	}

	/**
	 * Adds a listener that is notified when the cell selections are changed.
	 * This can happen either by a click on the cell or by arrow keys.
	 * 
	 * @param listener
	 *            The selection changed listener.
	 */
	void addSelectionChangedListener(ISelectionChangedListener listener) {
		if (!selectionChangedListeners.contains(listener)) {
			selectionChangedListeners.add(listener);
		}
	}

	/**
	 * Removes the listener if present.
	 * 
	 * @param listener
	 *            The selection changed listener.
	 * @return true if found and removed from the list of listeners. otherwise
	 *         false.
	 */
	boolean removeSelectionChangedListener(ISelectionChangedListener listener) {
		return selectionChangedListeners.remove(listener);
	}

	/**
	 * Notify the selection changed listener with the selection changed event.
	 * 
	 * @param oldSelections
	 *            The old selections.
	 * @param newSelections
	 *            The new selections.
	 */
	private void fireSelectionChanged(Set<Cell> newSelections,
			Set<Cell> oldSelections) {
		SelectionChangedEvent event = new SelectionChangedEvent(this,
				newSelections, oldSelections);
		this.fireSelectionChanged(event);
	}

	/**
	 * Notify the selection changed listener with the selection changed event.
	 * 
	 * @param event
	 *            The selection changed event.
	 */
	private void fireSelectionChanged(SelectionChangedEvent event) {
		for (int i = 0; i < selectionChangedListeners.size(); i++) {
			selectionChangedListeners.get(i).selectionChanged(event);
		}
	}

	/**
	 * Adds a listener that is notified when the focus cell are changed. This
	 * can happen either by a click on the cell or by arrow keys.
	 * 
	 * @param listener
	 *            The focus cell changed listener.
	 */
	void addFocusCellChangedListener(IFocusCellChangedListener listener) {
		if (!focusCellChangedListeners.contains(listener)) {
			focusCellChangedListeners.add(listener);
		}
	}

	/**
	 * Removes the listener if present.
	 * 
	 * @param listener
	 *            The focus cell changed listener.
	 * @return true if found and removed from the list of listeners. otherwise
	 *         false.
	 */
	boolean removeFocusCellChangedListener(IFocusCellChangedListener listener) {
		return focusCellChangedListeners.remove(listener);
	}

	/**
	 * Notify the focus cell changed listener with the focus cell changed event.
	 * 
	 * @param oldFocusCell
	 *            The old focus cell.
	 * @param newFocusCell
	 *            The new focus cell.
	 */
	private void fireFocusCellChanged(Cell newFocusCell, Cell oldFocusCell) {
		FocusCellChangedEvent event = new FocusCellChangedEvent(this,
				newFocusCell, oldFocusCell);
		this.fireFocusCellChanged(event);
	}

	/**
	 * Notify the focus cell changed listener with the focus cell changed event.
	 * 
	 * @param event
	 *            The focus cell changed event.
	 */
	private void fireFocusCellChanged(FocusCellChangedEvent event) {
		for (int i = 0; i < focusCellChangedListeners.size(); i++) {
			focusCellChangedListeners.get(i).focusChanged(event);
		}
	}

}
