/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import org.agilemore.agilegrid.renderers.ButtonCellRenderer;
import org.agilemore.agilegrid.renderers.HeaderCellRenderer;
import org.agilemore.agilegrid.renderers.TextCellRenderer;

/**
 * This class provides the default implementions for the methods described by
 * the <code>ICellRendererProvider</code> interface.
 * 
 * @author fourbroad
 * 
 */
public class DefaultCellRendererProvider implements ICellRendererProvider {

	/**
	 * The renderer for header cell.
	 */
	private ButtonCellRenderer headerCellRenderer;

	/**
	 * The renderer for text cell.
	 */
	private TextCellRenderer textCellRenderer;

	/**
	 * The agile grid this renderer provider is working for.
	 */
	protected AgileGrid agileGrid;

	/**
	 * Creates a default cell renderer provider.
	 * 
	 * @param agileGrid
	 *            The agile grid this render provider is working for.
	 */
	public DefaultCellRendererProvider(AgileGrid agileGrid) {
		this.agileGrid = agileGrid;
		headerCellRenderer = new HeaderCellRenderer(agileGrid,
				ICellRenderer.STYLE_FLAT
						| ICellRenderer.INDICATION_SELECTION_ROW);
		textCellRenderer = new TextCellRenderer(agileGrid,
				ICellRenderer.INDICATION_SELECTION
						| ICellRenderer.INDICATION_COMMENT);
	}

	/**
	 * Returns the cell renderer for the given cell, If this does not suite your
	 * needs, you can easily derive your own cell renderer provider from
	 * <code>ICellRendererProvider</code>. If it is some general, not too
	 * specific renderer, we would be happy to include it as a default renderer
	 * provider.
	 * 
	 * @see org.agilemore.agilegrid.ICellRendererProvider#getCellRenderer(int, int)
	 */
	@Override
	public ICellRenderer getCellRenderer(int row, int col) {
		return textCellRenderer;
	}

	/* (non-Javadoc)
	 * @see org.peertoo.agilegrid.ICellRendererProvider#getHeadColumnRenderer(int)
	 */
	@Override
	public ICellRenderer getLeftHeadRenderer(int row) {
		return headerCellRenderer;
	}

	/* (non-Javadoc)
	 * @see org.peertoo.agilegrid.ICellRendererProvider#getHeadRowRenderer(int)
	 */
	@Override
	public ICellRenderer getTopHeadRenderer(int col) {
		return headerCellRenderer;
	}
}
