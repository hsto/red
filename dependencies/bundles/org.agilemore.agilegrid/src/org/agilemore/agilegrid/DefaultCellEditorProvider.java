/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */
package org.agilemore.agilegrid;

import org.agilemore.agilegrid.editors.TextCellEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * Default implementation of ICellEditorProvider, which use TextCellEditor as a
 * in place editor. Customs can extend this class and override some method if
 * need.
 * 
 * @author fourbroad
 * 
 */
public class DefaultCellEditorProvider extends AbstractCellEditorProvider {

	/**
	 * Create a default cell editor provider which works for the given agile
	 * grid.
	 * 
	 * @param agileGrid
	 *            The agile grid this cell editor provider works for.
	 */
	public DefaultCellEditorProvider(AgileGrid agileGrid) {
		this(agileGrid, false);
	}

	/**
	 * Create a default cell editor provider which works for the given agile
	 * grid.
	 * 
	 * @param agileGrid
	 *            The agile grid this cell editor provider works for.
	 * @param enableSmartToolBar
	 *            Is the smart tool bar enable.
	 */
	public DefaultCellEditorProvider(AgileGrid agileGrid,
			boolean enableSmartToolBar) {
		super(agileGrid, enableSmartToolBar);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.AbstractCellEditorProvider#canEdit(int, int)
	 */
	@Override
	public boolean canEdit(int row, int col) {
		if (row >= 0 && row < agileGrid.getLayoutAdvisor().getRowCount()
				&& col >= 0
				&& col < agileGrid.getLayoutAdvisor().getColumnCount()) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.AbstractCellEditorProvider#getCellEditor(int,
	 * int)
	 */
	@Override
	public CellEditor getCellEditor(int row, int col) {
		return getCellEditor(row, col, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ICellEditorProvider#getCellEditor(int, int,
	 * java.lang.Object)
	 */
	@Override
	public CellEditor getCellEditor(int row, int col, Object hint) {
		return new TextCellEditor(agileGrid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.AbstractCellEditorProvider#getValue(int, int)
	 */
	@Override
	public Object getValue(int row, int col) {
		return agileGrid.getContentAt(row, col);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.AbstractCellEditorProvider#setValue(int, int,
	 * java.lang.Object)
	 */
	@Override
	public void setValue(int row, int col, Object value) {
		agileGrid.setContentAt(row, col, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractCellEditorProvider#createSmartToolBar
	 * (org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Composite createSmartToolBar(Composite parent) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractCellEditorProvider#showSmartToolBar(org
	 * .agilemore.agilegrid.Cell)
	 */
	@Override
	protected boolean showSmartToolBar(Cell cell) {
		return false;
	}
}
