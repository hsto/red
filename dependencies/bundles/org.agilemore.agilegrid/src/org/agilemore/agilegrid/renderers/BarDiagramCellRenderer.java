/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid.renderers;

import org.agilemore.agilegrid.AgileGrid;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

/**
 * The bar diagram cell renderer assume that the content provided by content
 * provider is a kind of Float, Double or IPercentage. This cell renderer will
 * draw bar diagram as the width of bar diagram is according to the value of
 * content.
 * 
 * @author fourbroad
 */
public class BarDiagramCellRenderer extends AbstractCellRenderer {

	/**
	 * Creates a bar diagram cell renderer which works for given agile grid.
	 * 
	 * @param style
	 *            The style bits to use. Currently supported are:
	 *            <ul>
	 *            <li>INDICATION_SELECTION</li>
	 *            <li>INDICATION_SELECTION_ROW</li>
	 *            <li>INDICATION_GRADIENT</li>
	 *            </ul>
	 */
	public BarDiagramCellRenderer(AgileGrid agileGrid, int style) {
		super(agileGrid, style);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.renderers.DefaultCellRenderer#getOptimalWidth(org.eclipse.swt.graphics.GC,
	 *      int, int)
	 */
	public int getOptimalWidth(GC gc, int row, int col) {
		return 20;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.renderers.DefaultCellRenderer#initialCellContentColor(int,
	 *      int)
	 */
	@Override
	protected void initialColor(int row, int col) {
		if (agileGrid.isCellSelected(row, col)) {
			if ((style & INDICATION_SELECTION) != 0) {
				background = COLOR_BGSELECTION;
			} else if ((style & INDICATION_SELECTION_ROW) != 0) {
				background = COLOR_BGROWSELECTION;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.renderers.DefaultCellRenderer#clearCellContentRect(org.eclipse.swt.graphics.GC,
	 *      org.eclipse.swt.graphics.Rectangle)
	 */
	@Override
	protected void clearCellContentRect(GC gc, Rectangle rect) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.renderers.DefaultCellRenderer#drawCellContent(org.eclipse.swt.graphics.GC,
	 *      org.eclipse.swt.graphics.Rectangle, java.lang.Object)
	 */
	@Override
	protected void doDrawCellContent(GC gc, Rectangle rect, int row, int col) {
		Object content = agileGrid.getContentAt(row, col);
		drawBar(gc, rect, content);
	}

	/**
	 * Draws the bar diagram according to the value of content, which value is a
	 * kind of Float, Double or IPercentage.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param rect
	 *            The area to be drewn.
	 * @param content
	 *            The value to calculate the width of bar diagram.
	 */
	protected void drawBar(GC gc, Rectangle rect, Object content) {
		float fraction;
		if (content instanceof Float)
			fraction = ((Float) content).floatValue();
		else if (content instanceof Double)
			fraction = ((Double) content).floatValue();
		else if (content instanceof IPercentage)
			fraction = ((IPercentage) content).getPercentage();
		else
			fraction = 1;

		if (fraction > 1)
			fraction = 1;
		if (fraction < 0)
			fraction = 0;

		if ((style & INDICATION_GRADIENT) != 0)
			drawGradientBar(gc, rect, fraction, background, foreground);
		else
			drawNormalBar(gc, rect, fraction, background, foreground);
	}

	/**
	 * Draws the gradient bar diagram according to the fraction.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param rect
	 *            The area to be drewn.
	 * @param fraction
	 *            The value to calculate the width of bar diagram.
	 * @param foreground
	 *            The foreground of bar diagram.
	 * @param background
	 *            The background of bar diagram.
	 */
	protected void drawGradientBar(GC gc, Rectangle rect, float fraction,
			Color background, Color foreground) {
		int barWidth = Math.round(rect.width * fraction);
		gc.setForeground(background);
		gc.setBackground(foreground);
		gc.fillGradientRectangle(rect.x, rect.y, barWidth, rect.height, false);
		gc.setBackground(COLOR_BACKGROUND);
		gc.fillRectangle(rect.x + barWidth, rect.y, rect.width - barWidth,
				rect.height);
	}

	/**
	 * Draws the normal bar diagram according to the fraction.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param rect
	 *            The area to be drewn.
	 * @param fraction
	 *            The value to calculate the width of bar diagram.
	 * @param foreground
	 *            The foreground of bar diagram.
	 * @param background
	 *            The background of bar diagram.
	 */
	protected void drawNormalBar(GC gc, Rectangle rect, float fraction,
			Color background, Color foreground) {
		int barWidth = Math.round(rect.width * fraction);
		gc.setBackground(foreground);
		gc.fillRectangle(rect.x, rect.y, barWidth, rect.height);
		gc.setBackground(background);
		gc.fillRectangle(rect.x + barWidth, rect.y, rect.width - barWidth,
				rect.height);
	}
}
