/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid.renderers;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ColumnSortComparator;
import org.agilemore.agilegrid.ICellRenderer;
import org.agilemore.agilegrid.ICompositorStrategy;
import org.agilemore.agilegrid.ILayoutAdvisor;
import org.agilemore.agilegrid.SWTResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;

/**
 * Class that provides an easy way of rendering a header cell.
 * <p>
 * Provided styles are at the moment:
 * 
 * <ul>
 * <li><b>STYLE_FLAT</b> for a flat look.</li>
 * <li><b>STYLE_PUSH</b> for a button-like look.</li>
 * <li><b>INDICATION_SORT</b> if a sort indicator should be painted. Can be
 * combined with <code>STYLE_FLAT</code> or <code>STYLE_PUSH</code> by or-ing.</li>
 * <li><b>INDICATION_SELECTION</b> when a focus bit should be colored
 * differently. Combine by or-ing.</li>
 * <li><b>INDICATION_CLICKED</b> shows a visible feedback when the user clicks
 * on the cell.<br>
 * Only applicable if STYLE_PUSH is used.</li>
 * <li><b>SWT.BOLD</b> Makes the renderer draw bold text.</li>
 * <li><b>SWT.ITALIC</b> Makes the renderer draw italic text</li>
 * </ul>
 * 
 * @author fourbroad
 */
public class HeaderCellRenderer extends ButtonCellRenderer {

	/**
	 * Small arrow pointing down. Can be used when displaying a sorting
	 * indicator.
	 */
	public static final Image IMAGE_ARROWDOWN = SWTResourceManager.getImage(
			HeaderCellRenderer.class, "/icons/arrow_down.gif");

	/** Small arrow pointing up. Can be used when displaying a sorting indicator */
	public static final Image IMAGE_ARROWUP = SWTResourceManager.getImage(
			HeaderCellRenderer.class, "/icons/arrow_up.gif");

	public static final Color COLOR_FIXEDBACKGROUND = SWTResourceManager
			.getColor(SWT.COLOR_WIDGET_BACKGROUND);

	/**
	 * A constructor that lets the caller specify the style.
	 * 
	 * @param style
	 *            The style that should be used to paint.
	 *            <p>
	 *            - Use SWT.FLAT for a flat look.<br>
	 *            - Use SWT.PUSH for a button-like look. (default)
	 *            <p>
	 *            The following additional indications can be activated: <br>
	 *            - INDICATION_SELECTION changes the background color if the
	 *            fixed cell has focus.<br>
	 *            - INDICATION_SELECTION_ROW changes the background color so
	 *            that it machtes with normal cells in rowselection mode.<br>
	 *            - INDICATION_SORT shows the sort direction.<br>
	 *            - INDICATION_CLICKED shows a click feedback, if STYLE_PUSH is
	 *            specified. For text styles, the styles SWT.BOLD and SWT.ITALIC
	 *            can be given.
	 */
	public HeaderCellRenderer(AgileGrid agileGrid, int style) {
		super(agileGrid, style);
		this.style |= STYLE_PUSH;
		this.setAlignment(ALIGN_HORIZONTAL_CENTER | ALIGN_VERTICAL_CENTER);
		this.setDefaultBackground(COLOR_FIXEDBACKGROUND);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.renderers.DefaultCellRenderer#initialCellContentColor
	 * (int, int)
	 */
	@Override
	protected void initialColor(int row, int col) {
		if (agileGrid.isHeaderHighlighted(row, col)) {
			if ((style & INDICATION_SELECTION) != 0) {
				background = COLOR_FIXEDHIGHLIGHT;
			}
			if ((style & INDICATION_SELECTION_ROW) != 0) {
				foreground = COLOR_FGROWSELECTION;
				background = COLOR_BGROWSELECTION;
			}
		}
	}

	/**
	 * Check for sort indicator and delegate content drawing to
	 * drawCellContent()
	 * 
	 * @see org.agilemore.agilegrid.renderers.AbstractCellRenderer#drawCellContent(org.eclipse.swt.graphics.GC,
	 *      org.eclipse.swt.graphics.Rectangle, int, int)
	 */
	@Override
	protected void doDrawCellContent(GC gc, Rectangle rect, int row, int col) {
		// draw text and image in the given area.
		String label = "";
		ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
		if (row == -1 && col >= 0 && col < layoutAdvisor.getColumnCount()) {
			label = layoutAdvisor.getTopHeaderLabel(col);
		} else if (col == -1 && row >= 0 && row < layoutAdvisor.getRowCount()) {
			label = layoutAdvisor.getLeftHeaderLabel(row);
		} else if (row == -1 && col == -1) {
			label = "";
		}

		if ((style & ICellRenderer.DRAW_VERTICAL) != 0) {
			drawVerticalTextImage(gc, rect, label, null, foreground, background);
		} else {
			int alignment = getAlignment();
			drawTextImage(gc, label, alignment, null, alignment, rect.x + 3,
					rect.y + 2, rect.width - 6, rect.height - 4);
		}

		drawSortIndicator(gc, rect, row, col, label);
	}

	/**
	 * Draws sort indicator in the given rectangle.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param rect
	 *            The rectangle area to draw sort indicator.
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @param content
	 *            The content to be drew in the given cell.
	 */
	protected void drawSortIndicator(GC gc, Rectangle rect, int row, int col,
			Object content) {
		ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
		ICompositorStrategy compositorStrategy = layoutAdvisor
				.getCompositorStrategy();
		if ((style & INDICATION_SORT) != 0 && compositorStrategy != null
				&& compositorStrategy.getSortColumn() == col && row == -1) {
			Image indicator = null;
			int sort = compositorStrategy.getSortState();
			if (sort == ColumnSortComparator.SORT_UP)
				indicator = IMAGE_ARROWUP;
			else if (sort == ColumnSortComparator.SORT_DOWN)
				indicator = IMAGE_ARROWDOWN;

			if (indicator != null) {
				int x = 0, y = 0;
				Rectangle indiRect = indicator.getBounds();
				x = rect.x + rect.width - 1 - indiRect.width;
				y = rect.y + rect.height - 1 - indiRect.height;
				if (x >= rect.x && y >= rect.y) {
					gc.drawImage(indicator, x, y);
				}
			}
		}
	}

	@Override
	public int getOptimalWidth(GC gc, int row, int col) {
		String str = null;
		ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
		if (row == -1 && col >= 0) {
			str = layoutAdvisor.getTopHeaderLabel(col);
		} else if (row >= 0 && col == -1) {
			str = layoutAdvisor.getLeftHeaderLabel(row);
		}
		if (str == null)
			return 0;

		applyFont(gc);
		int result = 0;
		if ((style & ICellRenderer.DRAW_VERTICAL) != 0) {
			String text = wrapText(gc, str, layoutAdvisor.getRowHeight(row) - 6);
			result = getCachedStringExtent(gc, text).y;
			result += 6;
		} else {
			result = getCachedStringExtent(gc, str).x;
			result += 8;
		}

		resetFont(gc);
		return result;
	}

}
