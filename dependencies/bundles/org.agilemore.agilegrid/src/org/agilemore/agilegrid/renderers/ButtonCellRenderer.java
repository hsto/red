/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */
package org.agilemore.agilegrid.renderers;

import org.agilemore.agilegrid.AgileGrid;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;

/**
 * Class that provides an easy way of rendering a button cell.
 * <p>
 * Provided styles are at the moment:
 * 
 * <ul>
 * <li><b>STYLE_FLAT</b> for a flat look.</li>
 * <li><b>STYLE_PUSH</b> for a button-like look.</li>
 * <li><b>INDICATION_SORT</b> if a sort indicator should be painted. Can be
 * combined with <code>STYLE_FLAT</code> or <code>STYLE_PUSH</code> by or-ing.</li>
 * <li><b>INDICATION_SELECTION</b> when a focus bit should be colored
 * differently. </li>
 * <li><b>INDICATION_CLICKED</b> shows a visible feedback when the user clicks
 * on the cell.<br>
 * Only applicable if STYLE_PUSH is used.</li>
 * <li><b>SWT.BOLD</b> Makes the renderer draw bold text.</li>
 * <li><b>SWT.ITALIC</b> Makes the renderer draw italic text</li>
 * </ul>
 * 
 * @author fourbroad
 */
public class ButtonCellRenderer extends AbstractCellRenderer {

	/**
	 * @param agileGrid
	 */
	public ButtonCellRenderer(AgileGrid agileGrid) {
		super(agileGrid);
	}

	/**
	 * @param agileGrid
	 * @param style
	 */
	public ButtonCellRenderer(AgileGrid agileGrid, int style) {
		super(agileGrid, style);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.renderers.DefaultCellRenderer#drawCellBorder(org
	 * .eclipse.swt.graphics.GC, org.eclipse.swt.graphics.Rectangle, int, int)
	 */
	@Override
	protected Rectangle drawCellBorder(GC gc, Rectangle rect, int row, int col) {
		Color bottomBorderColor = COLOR_LINE_DARKGRAY;
		Color rightBorderColor = COLOR_LINE_DARKGRAY;

		if (agileGrid.isHeaderHighlighted(row, col)) {
			if ((style & INDICATION_SELECTION) != 0) {
				bottomBorderColor = COLOR_TEXT;
				rightBorderColor = COLOR_TEXT;
			}
			if ((style & INDICATION_SELECTION_ROW) != 0) {
				bottomBorderColor = COLOR_BGROWSELECTION;
				rightBorderColor = COLOR_BGROWSELECTION;
			}
		}

		boolean isPressed = agileGrid.isCellPressed(row, col);
		if ((style & STYLE_FLAT) != 0) { // Style flat.
			if (isPressed) {
				Color pressedColor = display
						.getSystemColor(SWT.COLOR_DARK_GRAY);
				gc.setForeground(pressedColor);
				gc.drawRectangle(rect.x, rect.y, rect.width, 1);
				gc.drawRectangle(rect.x, rect.y, 1, rect.height);
				rect.x++;
				rect.y++;
				rect.width--;
				rect.height--;
				return rect;
			} else {
				rect = drawDefaultCellLine(gc, rect, bottomBorderColor,
						rightBorderColor);
			}
		} else { // Style push.
			drawCellButton(gc, rect, "", isPressed
					&& ((style & INDICATION_CLICKED) != 0));

			// push style border is drawn, exclude:
			rect.x += 2;
			rect.y += 2;
			rect.width -= 4;
			rect.height -= 4;
		}

		return rect;
	}

	/**
	 * Draws the cell as a button. It is visibly clickable and contains a button
	 * text. All line borders of the cell are overpainted - there will not be
	 * any border between buttons.
	 * 
	 * @param gc
	 *            The GC to use when painting.
	 * @param rect
	 *            The cell area as given by agile grid. (contains 1px bottom & right
	 *            offset)
	 * @param text
	 *            The text to paint on the button.
	 * @param pressed
	 *            Wether the button should be painted as clicked/pressed or not.
	 */
	protected void drawCellButton(GC gc, Rectangle rect, String text,
			boolean pressed) {
		rect.height += 1;
		rect.width += 1;
		gc.setForeground(display.getSystemColor(SWT.COLOR_LIST_FOREGROUND));
		if (pressed) {
			drawButtonDown(gc, text, getAlignment(), null, getAlignment(), rect);
		} else {
			drawButtonUp(gc, text, getAlignment(), null, getAlignment(), rect);
		}
	}

	/**
	 * Draws the button with the state of button is up.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of text.
	 * @param x
	 *            The coordinate x of area.
	 * @param y
	 *            The coordinate y of area.
	 * @param width
	 *            The width of area.
	 * @param height
	 *            The height of area.
	 * @param face
	 * 
	 */
	public void drawButtonUp(GC gc, String text, int textAlign, Image image,
			int imageAlign, int x, int y, int w, int h, Color face) {
		drawButtonUp(gc, text, textAlign, image, imageAlign, x, y, w, h, face,
				display.getSystemColor(SWT.COLOR_WIDGET_HIGHLIGHT_SHADOW),
				display.getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW), display
						.getSystemColor(SWT.COLOR_WIDGET_DARK_SHADOW), 2, 2);
	}

	/**
	 * Draws the button with the state of button is up.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of text.
	 * @param rect
	 *            The area to be drew.
	 * @param leftMargin
	 *            The left margin to be left.
	 * @param topMargin
	 *            The top margin to be left.
	 */
	public void drawButtonUp(GC gc, String text, int textAlign, Image image,
			int imageAlign, Rectangle rect, int leftMargin, int topMargin) {
		drawButtonUp(gc, text, textAlign, image, imageAlign, rect.x, rect.y,
				rect.width, rect.height, display
						.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND), display
						.getSystemColor(SWT.COLOR_WIDGET_HIGHLIGHT_SHADOW),
				display.getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW), display
						.getSystemColor(SWT.COLOR_WIDGET_DARK_SHADOW),
				leftMargin, topMargin);
	}

	/**
	 * Draws the button with the state of button is up.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of text.
	 * @param rect
	 *            The area to be drew.
	 */
	public void drawButtonUp(GC gc, String text, int textAlign, Image image,
			int imageAlign, Rectangle rect) {
		drawButtonUp(gc, text, textAlign, image, imageAlign, rect.x, rect.y,
				rect.width, rect.height, display
						.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND), display
						.getSystemColor(SWT.COLOR_WIDGET_HIGHLIGHT_SHADOW),
				display.getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW), display
						.getSystemColor(SWT.COLOR_WIDGET_DARK_SHADOW), 2, 2);
	}

	/**
	 * Draws the button with the state of button is up.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of text.
	 * @param x
	 *            The coordinate x of area.
	 * @param y
	 *            The coordinate y of area.
	 * @param width
	 *            The width of area.
	 * @param height
	 *            The height of area.
	 * @param face
	 * @param shadowHigh
	 * @param shadowNormal
	 * @param shadowDark
	 * @param leftMargin
	 *            The left margin to be left.
	 * @param topMargin
	 *            The top margin to be left.
	 */
	protected void drawButtonUp(GC gc, String text, int textAlign, Image image,
			int imageAlign, int x, int y, int width, int height, Color face,
			Color shadowHigh, Color shadowNormal, Color shadowDark,
			int leftMargin, int topMargin) {
		Color prevForeground = gc.getForeground();
		Color prevBackground = gc.getBackground();
		Rectangle clip = gc.getClipping();
		clip.height++;
		clip.width++;
		gc.setClipping(clip);

		try {
			gc.setBackground(face);
			gc.setForeground(shadowHigh);
			gc.drawLine(x, y, x, y + height - 1);
			gc.drawLine(x, y, x + width - 2, y);
			gc.setForeground(shadowDark);
			gc.drawLine(x + width - 1, y, x + width - 1, y + height - 1);
			gc.drawLine(x, y + height - 1, x + width - 1, y + height - 1);
			gc.setForeground(shadowNormal);
			gc.drawLine(x + width - 2, y + 1, x + width - 2, y + height - 2);
			gc.drawLine(x + 1, y + height - 2, x + width - 2, y + height - 2);

			gc.fillRectangle(x + 1, y + 1, width - 3, height - 3);
			gc.setForeground(prevForeground);
			drawTextImage(gc, text, textAlign, image, imageAlign, x + 1
					+ leftMargin, y + 1 + topMargin, width - 3 - leftMargin,
					height - 3 - topMargin);
		} finally {
			gc.setForeground(prevForeground);
			gc.setBackground(prevBackground);
		}
	}

	/**
	 * Draws the button with the state of button is down.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of image.
	 * @param rect
	 *            The area to be drewn.
	 */
	public void drawButtonDown(GC gc, String text, int textAlign, Image image,
			int imageAlign, Rectangle rect) {
		drawButtonDown(gc, text, textAlign, image, imageAlign, rect.x, rect.y,
				rect.width, rect.height, display
						.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND), display
						.getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW), 2, 2);
	}

	/**
	 * Draws button with the state of button is down.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of text.
	 * @param x
	 *            The coordinate x of area.
	 * @param y
	 *            The coordinate y of area.
	 * @param width
	 *            The width of area.
	 * @param height
	 *            The height of area.
	 * @param face
	 * @param shadowNormal
	 * @param leftMargin
	 *            The left margin to be left.
	 * @param topMargin
	 *            The top margin to be left.
	 */
	protected void drawButtonDown(GC gc, String text, int textAlign,
			Image image, int imageAlign, int x, int y, int width, int height,
			Color face, Color shadowNormal, int leftMargin, int topMargin) {
		Color prevForeground = gc.getForeground();
		Color prevBackground = gc.getBackground();
		try {
			gc.setBackground(face);
			gc.setForeground(shadowNormal);
			Rectangle clip = gc.getClipping();
			clip.height++;
			clip.width++;
			gc.setClipping(clip);
			gc.drawRectangle(x, y, width - 1, height - 1);
			gc.fillRectangle(x + 1, y + 1, width - 2, height - 2);
			gc.setForeground(prevForeground);
			drawTextImage(gc, text, textAlign, image, imageAlign, x + 2
					+ leftMargin, y + 2 + topMargin, width - 3 - leftMargin,
					height - 3 - topMargin);
		} finally {
			gc.setForeground(prevForeground);
			gc.setBackground(prevBackground);
		}
	}

	/**
	 * Draws the button with the state of button is deep down.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of image.
	 * @param rect
	 *            The area to be drewn.
	 */
	protected void drawButtonDeepDown(GC gc, String text, int textAlign,
			Image image, int imageAlign, Rectangle rect) {
		gc.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
		gc.drawLine(rect.x, rect.y, rect.x + rect.width - 2, rect.y);
		gc.drawLine(rect.x, rect.y, rect.x, rect.y + rect.height - 2);
		gc.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
		gc.drawLine(rect.x + rect.width - 1, rect.y, rect.x + rect.width - 1,
				rect.y + rect.height - 1);
		gc.drawLine(rect.x, rect.y + rect.height - 1, rect.x + rect.width - 1,
				rect.y + rect.height - 1);
		gc.setForeground(display.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		gc.drawLine(rect.x + 1, rect.y + rect.height - 2, rect.x + rect.width
				- 2, rect.y + rect.height - 2);
		gc.drawLine(rect.x + rect.width - 2, rect.y + rect.height - 2, rect.x
				+ rect.width - 2, rect.y + 1);

		gc.setForeground(display.getSystemColor(SWT.COLOR_WIDGET_FOREGROUND));
		gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		gc.fillRectangle(rect.x + 2, rect.y + 2, rect.width - 4, 1);
		gc.fillRectangle(rect.x + 1, rect.y + 2, 2, rect.height - 4);

		gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		drawTextImage(gc, text, textAlign, image, imageAlign, rect.x + 2 + 1,
				rect.y + 2 + 1, rect.width - 4, rect.height - 3 - 1);
	}

	/**
	 * Draws a flat button with the state of button is up.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of image.
	 * @param x
	 *            The coordinate x of area.
	 * @param y
	 *            The coordinate y of area.
	 * @param width
	 *            The width of area.
	 * @param height
	 *            The height of area.
	 * @param face
	 * @param shadowLight
	 * @param shadowNormal
	 * @param leftMargin
	 *            The left margin to be left.
	 * @param topMargin
	 *            the top margin to be left.
	 */
	protected void drawFlatButtonUp(GC gc, String text, int textAlign,
			Image image, int imageAlign, int x, int y, int width, int height,
			Color face, Color shadowLight, Color shadowNormal, int leftMargin,
			int topMargin) {
		Color prevForeground = gc.getForeground();
		Color prevBackground = gc.getBackground();
		try {
			gc.setForeground(shadowLight);
			gc.drawLine(x, y, x + width - 1, y);
			gc.drawLine(x, y, x, y + height);
			gc.setForeground(shadowNormal);
			gc.drawLine(x + width, y, x + width, y + height);
			gc.drawLine(x + 1, y + height, x + width, y + height);
			//
			gc.setBackground(face);
			gc.fillRectangle(x + 1, y + 1, leftMargin, height - 1);
			gc.fillRectangle(x + 1, y + 1, width - 1, topMargin);
			//
			gc.setBackground(face);
			gc.setForeground(prevForeground);
			drawTextImage(gc, text, textAlign, image, imageAlign, x + 1
					+ leftMargin, y + 1 + topMargin, width - 1 - leftMargin,
					height - 1 - topMargin);
		} finally {
			gc.setForeground(prevForeground);
			gc.setBackground(prevBackground);
		}
	}

}