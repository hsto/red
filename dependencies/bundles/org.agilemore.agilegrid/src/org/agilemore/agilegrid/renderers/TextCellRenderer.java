/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid.renderers;

import org.agilemore.agilegrid.ICellRenderer;
import org.agilemore.agilegrid.AgileGrid;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

/**
 * Simply draws a text to the cell.
 * <p>
 * Honored style bits are:
 * <ul>
 * <li><b>INDICATION_SELECTION</b> colors the cell in a slightly different way
 * and draws a selection border.</li>
 * <li><b>INDICATION_SELECTION_ROW</b> colors the cell that has focus dark and
 * its content white.</li>
 * <li><b>INDICATION_COMMENT</b> makes the renderer draw a small triangle in
 * the upper right corner of the cell.</li>
 * <li><b>SWT.BOLD</b> Makes the renderer draw bold text.</li>
 * <li><b>SWT.ITALIC</b> Makes the renderer draw italic text</li>
 * </ul>
 */
public class TextCellRenderer extends AbstractCellRenderer {

	/**
	 * @param agileGrid
	 */
	public TextCellRenderer(AgileGrid agileGrid) {
		super(agileGrid);
	}

	/**
	 * Creates a cellrenderer that prints text in the cell.
	 * <p>
	 * 
	 * <p>
	 * 
	 * @param style
	 *            Honored style bits are:<br> - INDICATION_SELECTION makes the
	 *            cell that has the focus have a different background color and
	 *            a selection border.<br> - INDICATION_SELECTION_ROW makes the
	 *            cell show a selection indicator as it is often seen in row
	 *            selection mode. A deep blue background and white content.<br> -
	 *            INDICATION_COMMENT lets the renderer paint a small triangle to
	 *            the right top corner of the cell.<br> - SWT.BOLD Makes the
	 *            renderer draw bold text.<br> - SWT.ITALIC Makes the renderer
	 *            draw italic text<br>
	 */
	public TextCellRenderer(AgileGrid agileGrid, int style) {
		super(agileGrid, style);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.renderers.DefaultCellRenderer#initialCellContentColor(int,
	 *      int)
	 */
	@Override
	protected void initialColor(int row, int col) {
		if (agileGrid.isCellSelected(row, col)) {
			background = COLOR_BGSELECTION;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.renderers.DefaultCellRenderer#drawCellContent(org.eclipse.swt.graphics.GC,
	 *      org.eclipse.swt.graphics.Rectangle, int, int)
	 */
	@Override
	protected void doDrawCellContent(GC gc, Rectangle rect, int row, int col) {
		// draw text and image in the given area.
		Object content = agileGrid.getContentAt(row, col);
		if(content == null)
			return;
		
		if ((style & ICellRenderer.DRAW_VERTICAL) != 0) {
			drawVerticalTextImage(gc, rect, content.toString(), null,
					foreground, background);
		} else {
			int alignment = getAlignment();
			drawTextImage(gc, content.toString(), alignment, null, alignment,
					rect.x + 3, rect.y + 2, rect.width - 6, rect.height - 4);
		}
	}
}
