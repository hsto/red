/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid.renderers;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ICellRenderer;
import org.agilemore.agilegrid.SWTResourceManager;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;

/**
 * Cell renderer that expects a Boolean as content of a cell. It then represents
 * the value by a checked or unchecked box.
 * <p>
 * It accepts the following style bits:
 * <ul>
 * <li><b>INDICATION_CLICK</b> shows a visible feedback when the user clicks the
 * cell. This feedback resprects the checked state.</li>
 * <li><b>INDICATION_SELECTION</b> causes the cell that has focus to be drawn
 * with another background color and a focus-style border.</li>
 * <li><b>INDICATION_SELECTION_ROW</b> causes the cell that has focus to be
 * drawn in a dark color and its content bright.</li>
 * <li><b>INDICATION_COMMENT</b> makes the renderer paint a little triangle in
 * the upper right corner of the cell as a sign that additional information is
 * available.</li>
 * <li><b>SIGN_IMAGE</b> forces the drawing of images that are defined in the
 * icon folder. This makes the rendering of a cell 2-3 times slower. Overwrites
 * all other SIGN_* style bits.</li>
 * <li><b>SIGN_X</b> makes the 'true' symbol be an X. This is only valid if
 * SIGN_IMAGE is not given, and it overwrites SIGN_CHECK</li>
 * <li><b>SIGN_CHECK</b> makes the 'true' symbol a check. THIS IS DEFAULT.</li>
 * </ul>
 * 
 * @see org.agilemore.agilegrid.editors.CheckboxCellEditor
 * 
 */
public class CheckboxCellRenderer extends AbstractCellRenderer {

	/**
	 * Style bit that forces that the renderer paints images instead of directly
	 * painting. The images used for painting can be found in the folder /icons/
	 * and are named checked.gif, unchecked.gif, checked_clicked.gif and
	 * unchecked_clicked.gif.
	 * <p>
	 * Note that when using images, drawing is 2-3 times slower than when using
	 * direct painting. It might be visible if many cells should be rendered
	 * that way. So this is not default.
	 */
	public static final int SIGN_IMAGE = 1 << 31;

	/**
	 * Makes the renderer draw an X as the symbol that signals the value is
	 * true. This has only an effect if the style SIGN_IMAGE is not active.
	 */
	public static final int SIGN_X = 1 << 30;
	/**
	 * Makes the renterer draw a check sign as the symbol that signals the value
	 * true. THIS IS DEFAULT.
	 */
	public static final int SIGN_CHECK = 1 << 29;

	/** Indicator for a checked entry / true boolean decision */
	public static final Image IMAGE_CHECKED = SWTResourceManager.getImage(
			CheckboxCellRenderer.class, "/icons/checked.gif");

	/** Indicator for an unchecked entry / false boolean decision */
	public static final Image IMAGE_UNCHECKED = SWTResourceManager.getImage(
			CheckboxCellRenderer.class, "/icons/unchecked.gif");

	/**
	 * Indicator for an checked entry / true boolean decision that is currently
	 * clicked.
	 */
	public static final Image IMAGE_CHECKED_CLICKED = SWTResourceManager
			.getImage(CheckboxCellRenderer.class, "/icons/checked_clicked.gif");

	/**
	 * Indicator for an unchecked entry / false boolean decision that is
	 * currently clicked.
	 */
	public static final Image IMAGE_UNCHECKED_CLICKED = SWTResourceManager
			.getImage(CheckboxCellRenderer.class,
					"/icons/unchecked_clicked.gif");

	public static final Color COLOR_FILL = SWTResourceManager.getColor(206,
			206, 206);
	public static final Color BORDER_DARK = SWTResourceManager.getColor(90, 90,
			57);
	public static final Color BORDER_LIGHT = SWTResourceManager.getColor(156,
			156, 123);

	/**
	 * Creates a checkable cell renderer that shows boolean values with the
	 * given style.
	 * <p>
	 * 
	 * @param style
	 *            Honored style bits are:
	 *            <ul>
	 *            <li>INDICATION_CLICKED</li> <li>INDICATION_SELECTION</li> <li>
	 *            INDICATION_SELECTION_ROW</li> <li>INDICATION_COMMENT</li>
	 *            </ul>
	 *            <p>
	 *            Styles that influence the sign painted when cell value is
	 *            true:
	 *            <ul>
	 *            <li>SIGN_IMAGE</li>
	 *            <li>SIGN_X</li>
	 *            <li>SIGN_CHECK (default)</li>
	 *            </ul>
	 */
	public CheckboxCellRenderer(AgileGrid agileGrid, int style) {
		super(agileGrid, style);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.renderers.DefaultCellRenderer#getOptimalWidth(org
	 * .eclipse.swt.graphics.GC, int, int)
	 */
	public int getOptimalWidth(GC gc, int row, int col) {
		return IMAGE_CHECKED.getBounds().x + 6;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.renderers.DefaultCellRenderer#initialCellContentColor
	 * (int, int)
	 */
	@Override
	protected void initialColor(int row, int col) {
		if (agileGrid.isCellSelected(row, col)) {
			if ((style & INDICATION_SELECTION) != 0) {
				background = COLOR_BGSELECTION;

			} else if ((style & INDICATION_SELECTION_ROW) != 0) {
				foreground = COLOR_FGROWSELECTION;
				background = COLOR_BGROWSELECTION;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.renderers.DefaultCellRenderer#doDrawCellContent
	 * (org.eclipse.swt.graphics.GC, org.eclipse.swt.graphics.Rectangle, int,
	 * int)
	 */
	protected void doDrawCellContent(GC gc, Rectangle rect, int row, int col) {
		/**
		 * Note that there currently exist two versions: One that uses the
		 * images defined in this renderer, and another one painting directly.
		 * <p>
		 * NOTE: Default is drawing directly (no images used) because this seems
		 * to be at least 2-3 times faster than painting an image!
		 */

		// draw content as image:
		Object content = agileGrid.getContentAt(row, col);
		if (!(content instanceof Boolean)) {
			if (content.toString().equalsIgnoreCase("true"))
				content = new Boolean(true);
			else if (content.toString().equalsIgnoreCase("false"))
				content = new Boolean(false);
		}

		if (!(content instanceof Boolean)) {
			// draw text and image in the given area.
			String text = "?";
			if ((style & ICellRenderer.DRAW_VERTICAL) != 0) {
				drawVerticalTextImage(gc, rect, text, null, foreground,
						background);
			} else {
				int alignment = getAlignment();
				drawTextImage(gc, text, alignment, null, alignment, rect.x + 3,
						rect.y + 2, rect.width - 6, rect.height - 4);
			}
		} else {
			boolean isClicked = agileGrid.isCellPressed(row, col);
			if ((style & SIGN_IMAGE) != 0) {
				boolean checked = ((Boolean) content).booleanValue();
				if (checked) {
					if (isClicked && (style & INDICATION_CLICKED) != 0)
						drawImage(gc, rect, IMAGE_CHECKED_CLICKED, background);
					else
						drawImage(gc, rect, IMAGE_CHECKED, background);
				} else {
					if (isClicked && (style & INDICATION_CLICKED) != 0)
						drawImage(gc, rect, IMAGE_UNCHECKED_CLICKED, background);
					else
						drawImage(gc, rect, IMAGE_UNCHECKED, background);
				}
			} else { // draw image directly:
				boolean checked = ((Boolean) content).booleanValue();
				if (isClicked && (style & INDICATION_CLICKED) != 0)
					drawCheckedSymbol(gc, rect, checked, background, COLOR_FILL);
				else
					drawCheckedSymbol(gc, rect, checked, background, background);
			}
		}
	}

	/**
	 * Draws the image in the given area.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param rect
	 *            The area to be drewn.
	 * @param image
	 *            The image to be drewn.
	 * @param background
	 *            The background used to draw the image.
	 */
	protected void drawImage(GC gc, Rectangle rect, Image image,
			Color background) {
		gc.setForeground(background);
		gc.setBackground(background);
		gc.fillRectangle(rect);

		drawTextImage(gc, "", getAlignment(), image, getAlignment(),
				rect.x + 3, rect.y, rect.width - 3, rect.height);
	}

	/**
	 * Manually paints the checked or unchecked symbol. This provides a fast
	 * replacement for the variant that paints the images defined in this class.
	 * <p>
	 * The reason for this is that painting manually is 2-3 times faster than
	 * painting the image - which is very notable if you have a completely
	 * filled agile grid! (see example!)
	 * 
	 * @param gc
	 *            The GC to use when drawing
	 * @param rect
	 *            The cell area where the symbol should be painted into.
	 * @param checked
	 *            Whether the symbol should be the checked or unchecked
	 * @param background
	 *            The background color of the cell.
	 * @param fillColor
	 *            The color of the box drawn (with of without checked mark).
	 *            Used when a click indication is desired.
	 */
	protected void drawCheckedSymbol(GC gc, Rectangle rect, boolean checked,
			Color background, Color fillColor) {
		// paint rectangle:
		Rectangle bound = getAlignedLocation(rect, IMAGE_CHECKED);

		gc.setForeground(BORDER_LIGHT);
		gc.drawLine(bound.x, bound.y, bound.x + bound.width, bound.y);
		gc.drawLine(bound.x, bound.y, bound.x, bound.y + bound.height);
		gc.setForeground(BORDER_DARK);
		gc.drawLine(bound.x + bound.width, bound.y + 1, bound.x + bound.width,
				bound.y + bound.height - 1);
		gc.drawLine(bound.x, bound.y + bound.height, bound.x + bound.width,
				bound.y + bound.height);

		if (!background.equals(fillColor)) {
			gc.setBackground(fillColor);
			gc.fillRectangle(bound.x + 1, bound.y + 1, bound.width - 1,
					bound.height - 1);
		}

		if (checked) // draw a check symbol:
			drawCheckSymbol(gc, bound);
	}

	/**
	 * Draws a X as a sign if the cell value is true.
	 * 
	 * @param gc
	 *            The gc to use when painting
	 * @param bound
	 *            The bound of check.
	 */
	private void drawCheckSymbol(GC gc, Rectangle bound) {
		if ((style & SIGN_X) != 0) { // Draw a X
			gc.setForeground(BORDER_LIGHT);

			gc.drawLine(bound.x + 3, bound.y + 2, bound.x - 2 + bound.width,
					bound.y - 3 + bound.height);
			gc.drawLine(bound.x + 2, bound.y + 3, bound.x - 3 + bound.width,
					bound.y - 2 + bound.height);

			gc.drawLine(bound.x + 3, bound.y - 2 + bound.height, bound.x - 2
					+ bound.width, bound.y + 3);
			gc.drawLine(bound.x + 2, bound.y - 3 + bound.height, bound.x - 3
					+ bound.width, bound.y + 2);

			gc.setForeground(COLOR_TEXT);

			gc.drawLine(bound.x + 2, bound.y + 2, bound.x - 2 + bound.width,
					bound.y - 2 + bound.height);
			gc.drawLine(bound.x + 2, bound.y - 2 + bound.height, bound.x - 2
					+ bound.width, bound.y + 2);
		} else { // Draw a check sign
			gc.setForeground(getForeground());

			gc.drawLine(bound.x + 2, bound.y + bound.height - 4, bound.x + 4,
					bound.y + bound.height - 2);
			gc.drawLine(bound.x + 2, bound.y + bound.height - 5, bound.x + 5,
					bound.y + bound.height - 3);
			gc.drawLine(bound.x + 2, bound.y + bound.height - 6, bound.x + 4,
					bound.y + bound.height - 4);

			for (int i = 1; i < 4; i++)
				gc.drawLine(bound.x + 2 + i, bound.y + bound.height - 3,
						bound.x + bound.width - 2, bound.y + 1 + i);
		}
	}

	/**
	 * Returns the location where the checked symbol should be painted.
	 * <p>
	 * Note that this is only a subarea of the area covered by an image, since
	 * the image contains a border area that is not needed here.
	 * 
	 * @param rect
	 *            The cell area
	 * @param img
	 *            The image to take the size of the checked symbol from.
	 * @return Returns the area that should be filled with a checked/unchecked
	 *         symbol.
	 */
	protected Rectangle getAlignedLocation(Rectangle rect, Image img) {
		Rectangle bounds = img.getBounds();
		bounds.x -= 2;
		bounds.y -= 2;
		bounds.height -= 4;
		bounds.width -= 4;

		if ((getAlignment() & ALIGN_HORIZONTAL_CENTER) != 0)
			bounds.x = rect.x + (rect.width - bounds.width) / 2;
		else if ((getAlignment() & ALIGN_HORIZONTAL_RIGHT) != 0)
			bounds.x = rect.x + rect.width - bounds.width - 2;
		else
			bounds.x = rect.x + 2;

		if ((getAlignment() & ALIGN_VERTICAL_CENTER) != 0)
			bounds.y = rect.y + (rect.height - bounds.height) / 2;
		else if ((getAlignment() & ALIGN_VERTICAL_BOTTOM) != 0)
			bounds.y = rect.y + rect.height - bounds.height - 2;
		else
			bounds.y = rect.y + 2;

		return bounds;
	}
}
