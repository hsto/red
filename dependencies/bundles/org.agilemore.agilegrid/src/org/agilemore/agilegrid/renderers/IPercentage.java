/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid.renderers;

/**
 * Interface that provides access to a percentage value. This is currently only
 * used for the BarDiagramCellRenderer to determine the length of the bar to
 * render.
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 * 
 */
public interface IPercentage {

	/**
	 * Returns a percentage value between 0 and 1.
	 * 
	 * @return A percentage value.
	 */
	public float getPercentage();

	/**
	 * Returns the absolute value that was responsible for the percentage value.
	 * 
	 * @return The absolute value that was responsible for the percentage value.
	 */
	public float getAbsoluteValue();
}
