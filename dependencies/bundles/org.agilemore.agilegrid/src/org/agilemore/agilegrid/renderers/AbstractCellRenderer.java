/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid.renderers;

import java.util.HashMap;
import java.util.Map;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ICellRenderer;
import org.agilemore.agilegrid.ILayoutAdvisor;
import org.agilemore.agilegrid.SWTResourceManager;
import org.agilemore.agilegrid.SWTX;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

/**
 * Class that provides additional facilities commonly used when writing custom
 * renderers.
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 * 
 */
public abstract class AbstractCellRenderer implements ICellRenderer {

	/**
	 * The display to use when painting.
	 */
	protected static final Display display = Display.getCurrent();

	/**
	 * Holds the style for this renderer.
	 */
	protected int style = 0;

	/**
	 * The alignment style of content in cell.
	 */
	protected int alignment = ALIGN_HORIZONTAL_LEFT | ALIGN_VERTICAL_CENTER;

	protected Color background;

	protected Color foreground;

	protected Color defaultForeground;

	protected Color defaultBackground;

	/**
	 * Holds the vary of fonts.
	 */
	protected Font gcFont = null, tmpFont = null;

	protected Font font;

	/**
	 * The agile grid this cell renderer is working for.
	 */
	protected AgileGrid agileGrid = null;

	/**
	 * Creates a new default cell renderer with a flat style.
	 */
	public AbstractCellRenderer(AgileGrid agileGrid) {
		this(agileGrid, ICellRenderer.STYLE_FLAT
				| ICellRenderer.INDICATION_SELECTION
				| ICellRenderer.INDICATION_COMMENT);
	}

	/**
	 * The constructor that sets the style bits given. The default cellrenderer
	 * ignores all style bits. See subclasses for their honored style bits.
	 */
	public AbstractCellRenderer(AgileGrid agileGrid, int style) {
		this.agileGrid = agileGrid;
		this.style |= style;
	}

	/**
	 * Overwrites the style bits with the given one.
	 * 
	 * @param style
	 *            The styles to AND with the current style bits.
	 */
	public void setStyle(int style) {
		this.style = style;
	}

	/**
	 * Returns the current style.
	 * 
	 * @return The current style.
	 */
	public int getStyle() {
		return style;
	}

	/**
	 * Sets the alignment of the cell content.
	 * 
	 * @param alignment
	 *            The OR-ed alignment constants for vertical and horizontal
	 *            alignment as defined in ICellRenderer.
	 * @see ALIGN_HORIZONTAL_CENTER
	 * @see ALIGN_HORIZONTAL_LEFT
	 * @see ALIGN_HORIZONTAL_RIGHT
	 * @see ALIGN_VERTICAL_CENTER
	 * @see ALIGN_VERTICAL_TOP
	 * @see ALIGN_VERTICAL_BOTTOM
	 */
	public void setAlignment(int alignment) {
		this.alignment = alignment;
	}

	/**
	 * @return Returns the alignment for the cell content. 2 or-ed constants,
	 *         one for horizontal, one for vertical alignment.
	 * @see ALIGN_HORIZONTAL_CENTER
	 * @see ALIGN_HORIZONTAL_LEFT
	 * @see ALIGN_HORIZONTAL_RIGHT
	 * @see ALIGN_VERTICAL_CENTER
	 * @see ALIGN_VERTICAL_TOP
	 * @see ALIGN_VERTICAL_BOTTOM
	 */
	public int getAlignment() {
		return alignment;
	}

	/**
	 * Set the foreground color used to paint text.
	 * 
	 * @param fgcolor
	 *            The color or <code>null</code> to reset to default (black).
	 *            Note that also the default color can be set using
	 *            <code>setDefaultForeground(Color)</code>
	 * @see #setDefaultForeground(Color)
	 */
	public void setForeground(Color fgcolor) {
		this.foreground = fgcolor;
	}

	/**
	 * Changes the default foreground color that will be used when no other
	 * foreground color is set. (for example when
	 * <code>setForeground(null)</code> is called)
	 * 
	 * @param fgcolor
	 *            The foreground color to use.
	 * @see #setForeground(Color)
	 */
	public void setDefaultForeground(Color fgcolor) {
		this.defaultForeground = fgcolor;
	}

	/**
	 * @return the defaultForeground
	 */
	public Color getDefaultForeground() {
		if (this.defaultForeground == null) {
			this.defaultForeground = COLOR_TEXT;
		}
		return this.defaultForeground;
	}

	/**
	 * Set the background color that should be used when painting the cell
	 * background.
	 * <p>
	 * If the <code>null</code> value is given, the default color will be used.
	 * The default color is settable using
	 * <code>setDefaultBacktround(Color)</code>
	 * 
	 * @param bgcolor
	 *            The color or <code>null</code> to reset to default.
	 * @see #setDefaultBackground(Color)
	 */
	public void setBackground(Color bgcolor) {
		background = bgcolor;
	}

	/**
	 * Changes the default background color that will be used when no background
	 * color is set via setBackground().
	 * 
	 * @param bgcolor
	 *            The color for the background.
	 * @see #setBackground(Color)
	 */
	public void setDefaultBackground(Color bgcolor) {
		this.defaultBackground = bgcolor;
	}

	/**
	 * @return the defaultBackground
	 */
	public Color getDefaultBackground() {
		if (this.defaultBackground == null)
			this.defaultBackground = COLOR_BACKGROUND;
		return this.defaultBackground;
	}

	/**
	 * @return returns the currently set foreground color. If none was set, the
	 *         default value is returned.
	 */
	public Color getForeground() {
		return foreground != null ? foreground : defaultForeground;
	}

	/**
	 * @return returns the currently set background color. If none was set, the
	 *         default value is returned.
	 */
	public Color getBackground() {
		return background != null ? background : defaultBackground;
	}

	/**
	 * Sets the font the renderer will use for drawing its content.
	 * 
	 * @param font
	 *            The font to use. Be aware that you must dispose fonts you have
	 *            created.
	 */
	public void setFont(Font font) {
		this.font = font;
	}

	/**
	 * @return Returns the font the renderer will use to draw the content.
	 */
	public Font getFont() {
		return this.font;
	}

	/**
	 * @param value
	 *            If true, the comment sign is painted. Else it is omitted.
	 */
	public void setCommentIndication(boolean value) {
		if (value)
			style = style | INDICATION_COMMENT;
		else
			style = style & ~INDICATION_COMMENT;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.ICellRenderer#getOptimalWidth(org.eclipse.swt.graphics
	 * .GC, int, int)
	 */
	public int getOptimalWidth(GC gc, int row, int col) {
		Object content = agileGrid.getContentAt(row, col);
		if (content == null)
			return 0;
		applyFont(gc);
		int result = 0;
		if ((style & ICellRenderer.DRAW_VERTICAL) != 0) {
			ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
			String text = wrapText(gc, content.toString(), layoutAdvisor
					.getRowHeight(row) - 6);
			result = getCachedStringExtent(gc, text).y;
			result += 6;

		} else {
			result = getCachedStringExtent(gc, content.toString()).x;
			result += 8;
		}

		resetFont(gc);
		return result;
	}

	/**
	 * A default implementation that paints cells in a way that is more or less
	 * Excel-like. Only the cell with focus looks very different.
	 * 
	 * @see org.agilemore.agilegrid.ICellRenderer#drawCell(org.eclipse.swt.graphics.GC,
	 *      org.eclipse.swt.graphics.Rectangle, int, int)
	 */
	public void drawCell(GC gc, Rectangle rect, int row, int col) {
		applyFont(gc);

		drawGridLines(gc, rect, row, col);

		Rectangle leftRect = drawCellBorder(gc, rect, row, col);

		// draw cell content:
		drawCellContent(gc, leftRect, row, col);

		if ((style & INDICATION_COMMENT) != 0
				&& agileGrid.getLayoutAdvisor().getTooltip(row, col) != null) {
			drawCommentSign(gc, rect);
		}

		if (agileGrid.isFocusCell(row, col) && !agileGrid.isCellEditorActive()) {
			gc.drawFocus(rect.x, rect.y, rect.width, rect.height);
		}

		resetFont(gc);
	}

	/**
	 * Draws the cell line in the given rectangle.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param rect
	 *            The cell area are to paint a border around.
	 */
	protected void drawGridLines(GC gc, Rectangle rect, int row, int col) {
		Color vBorderColor = COLOR_LINE_LIGHTGRAY;
		Color hBorderColor = COLOR_LINE_LIGHTGRAY;

		if ((style & INDICATION_SELECTION_ROW) != 0) {
			vBorderColor = COLOR_BGROWSELECTION;
			hBorderColor = COLOR_BGROWSELECTION;
		}

		if ((agileGrid.getStyle() & SWTX.NOT_SHOW_GRID_LINE) == SWTX.NOT_SHOW_GRID_LINE) {
			vBorderColor = COLOR_BACKGROUND;
			hBorderColor = COLOR_BACKGROUND;
		}

		drawDefaultCellLine(gc, rect, vBorderColor, hBorderColor);
	}

	/**
	 * Draws the cell line in the given rectangle.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param rect
	 *            The cell area are to paint a border around.
	 */
	protected Rectangle drawCellBorder(GC gc, Rectangle rect, int row, int col) {
		return rect;
	}

	/**
	 * Simply draws a border line with width 1 on the bottom (<b>h</b>orizontal)
	 * and on the right side (<b>v</b>ertical). The other two sides belong to
	 * the neightbor cells and are painted by them.
	 * <p>
	 * The result is a agile grid where a 1px line is between every two cells.
	 * This should be considered default behavior.
	 * 
	 * @param gc
	 *            The GC to use when painting.
	 * @param rect
	 *            The cell are to paint a border around.
	 * @param vBorderColor
	 *            The vertical line color for the line on the right.
	 * @param hBorderColor
	 *            The horizontal line color for the line on the bottom.
	 */
	protected Rectangle drawDefaultCellLine(GC gc, Rectangle rect,
			Color vBorderColor, Color hBorderColor) {
		int pxLine = agileGrid.getLinePixels();

		gc.setBackground(hBorderColor);
		gc.fillRectangle(rect.x, rect.y + rect.height, rect.width + pxLine,
				pxLine);

		gc.setBackground(vBorderColor);
		gc.fillRectangle(rect.x + rect.width, rect.y, pxLine, rect.height);

		return rect;
	}

	/**
	 * Applies the font style of the renderer to the gc that will draw the
	 * content.
	 * 
	 * @param gc
	 *            The gc that will draw the renderers content.
	 */
	protected void applyFont(GC gc) {
		gcFont = gc.getFont();
		if (font == null)
			font = display.getSystemFont();
		if ((this.style & SWT.BOLD) != 0 || (this.style & SWT.ITALIC) != 0) {
			FontData[] fd = font.getFontData();
			int s = SWT.NONE;
			if ((this.style & SWT.BOLD) != 0)
				s |= SWT.BOLD;
			if ((this.style & SWT.ITALIC) != 0)
				s |= SWT.ITALIC;

			for (int i = 0; i < fd.length; i++)
				fd[i].setStyle(s);
			tmpFont = new Font(display, fd);
			gc.setFont(tmpFont);
		} else {
			gc.setFont(font);
		}
	}

	/**
	 * Resets the given GC's font parameters to the original state.
	 * 
	 * @param gc
	 *            The gc to draw with.
	 */
	protected void resetFont(GC gc) {
		if (tmpFont != null) {
			tmpFont.dispose();
			tmpFont = null;
		}

		gc.setFont(gcFont);
	}

	/**
	 * Draws the cell content.
	 * 
	 * @param gc
	 *            The GC to used when painting.
	 * @param rect
	 *            The cell area.
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 */
	protected void drawCellContent(GC gc, Rectangle rect, int row, int col) {
		this.foreground = this.getDefaultForeground();
		this.background = this.getDefaultBackground();

		// initial color for current cell.
		initialColor(row, col);

		// Clear background.
		clearCellContentRect(gc, rect);

		// draw text and image in the given area.
		doDrawCellContent(gc, rect, row, col);
	}

	/**
	 * Draws the cell content actually.
	 * 
	 * @param gc
	 *            The graphic context to drawn on.
	 * @param rect
	 *            The area of cell content.
	 * @param row
	 *            The row index.
	 * @param col
	 *            The col index.
	 */
	protected void doDrawCellContent(GC gc, Rectangle rect, int row, int col) {
		Object content = agileGrid.getContentAt(row, col);
		if ((style & ICellRenderer.DRAW_VERTICAL) != 0) {
			drawVerticalTextImage(gc, rect, content.toString(), null,
					foreground, background);
		} else {
			int alignment = getAlignment();
			drawTextImage(gc, content.toString(), alignment, null, alignment,
					rect.x + 3, rect.y + 2, rect.width - 6, rect.height - 4);
		}
	}

	/**
	 * Sets up the background or foreground of cell content.
	 * 
	 * @param row
	 * @param col
	 */
	protected void initialColor(int row, int col) {
	}

	/**
	 * Clears the content area of cell.
	 * 
	 * @param gc
	 *            The graphic context to draw on.
	 * @param rect
	 *            The area to be cleared.
	 */
	protected void clearCellContentRect(GC gc, Rectangle rect) {
		// Clear background.
		gc.setForeground(foreground);
		gc.setBackground(background);
		gc.fillRectangle(rect);
	}

	/**
	 * Draws the text and image in the given area.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of text.
	 * @param x
	 *            The coordinate x of area.
	 * @param y
	 *            The coordinate y of area.
	 * @param width
	 *            The width of area.
	 * @param height
	 *            The height of area.
	 */
	protected void drawTextImage(GC gc, String text, int textAlign,
			Image image, int imageAlign, int x, int y, int width, int height) {

		Point textSize = getCachedStringExtent(gc, text);
		Point imageSize;
		if (image != null) {
			Rectangle bound = image.getBounds();
			imageSize = new Point(bound.width, bound.height);
		} else {
			imageSize = new Point(0, 0);
		}

		if ((image == null)
				&& ((textAlign & ALIGN_HORIZONTAL_MASK) == ALIGN_HORIZONTAL_CENTER)) {
			Point p = getCachedStringExtent(gc, text);
			int offset = (width - p.x) / 2;
			if (offset > 0) {
				drawTextVerticalAlign(gc, text, textAlign, x + offset, y,
						width, height);
			} else {
				drawTextVerticalAlign(gc, text, textAlign, x, y, width, height);
			}
			return;
		}

		if (((text == null) || (text.length() == 0))
				&& ((imageAlign & ALIGN_HORIZONTAL_MASK) == ALIGN_HORIZONTAL_CENTER)) {
			int offset = (width - imageSize.x) / 2;
			drawImageVerticalAlign(gc, image, imageAlign, x + offset, y, height);
			return;
		}

		// text align left
		if ((textAlign & ALIGN_HORIZONTAL_MASK) == ALIGN_HORIZONTAL_LEFT) {
			switch (imageAlign & ALIGN_HORIZONTAL_MASK) {
			case ALIGN_HORIZONTAL_NONE:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x, y,
						width, height);
				break;
			case ALIGN_HORIZONTAL_LEFT:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x
						+ imageSize.x, y, width - imageSize.x, height);
				drawImageVerticalAlign(gc, image, imageAlign, x, y, height);
				break;
			case ALIGN_HORIZONTAL_RIGHT:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x, y,
						width - imageSize.x, height);
				drawImageVerticalAlign(gc, image, imageAlign, x + width
						- imageSize.x, y, height);
				break;
			case ALIGN_HORIZONTAL_RIGHT_LEFT:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x, y,
						width - imageSize.x, height);
				drawImageVerticalAlign(gc, image, imageAlign, x + textSize.x,
						y, height);
				break;
			case ALIGN_HORIZONTAL_RIGHT_CENTER:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x, y,
						width - imageSize.x, height);
				int xOffset = (width - textSize.x - imageSize.x) / 2;
				drawImageVerticalAlign(gc, image, imageAlign, x + textSize.x
						+ xOffset, y, height);
				break;
			default:
				throw new SWTException("Unknown alignment for text: "
						+ (imageAlign & ALIGN_HORIZONTAL_MASK));
			}
			return;
		}

		// text align right
		if ((textAlign & ALIGN_HORIZONTAL_MASK) == ALIGN_HORIZONTAL_RIGHT) {
			switch (imageAlign & ALIGN_HORIZONTAL_MASK) {
			case ALIGN_HORIZONTAL_NONE:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x,
						-1000, width, height);
				drawTextVerticalAlign(gc, text, textAlign, x + width
						- textSize.x, y, width, height);
				break;
			case ALIGN_HORIZONTAL_LEFT:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x,
						-1000, width - imageSize.x, height);
				drawTextVerticalAlign(gc, text, textAlign, x + width
						- textSize.x, y, width - imageSize.x, height);
				drawImageVerticalAlign(gc, image, imageAlign, x, y, height);
				break;
			case ALIGN_HORIZONTAL_LEFT_RIGHT:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x,
						-1000, width - imageSize.x, height);
				drawTextVerticalAlign(gc, text, textAlign, x + width
						- textSize.x, y, width - imageSize.x, height);
				drawImageVerticalAlign(gc, image, imageAlign, x + width
						- (textSize.x + imageSize.x), y, height);
				break;
			case ALIGN_HORIZONTAL_LEFT_CENTER:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x,
						-1000, width - imageSize.x, height);
				drawTextVerticalAlign(gc, text, textAlign, x + width
						- textSize.x, y, width - imageSize.x, height);
				int xOffset = (width - textSize.x - imageSize.x) / 2;
				drawImageVerticalAlign(gc, image, imageAlign, x + xOffset, y,
						height);
				break;
			case ALIGN_HORIZONTAL_RIGHT:
				textSize.x = drawTextVerticalAlign(gc, text, textAlign, x,
						-1000, width - imageSize.x, height);
				drawTextVerticalAlign(gc, text, textAlign, x + width
						- (textSize.x + imageSize.x), y, width - imageSize.x,
						height);
				drawImageVerticalAlign(gc, image, imageAlign, x + width
						- imageSize.x, y, height);
				break;
			default:
				throw new SWTException("Unknown alignment for text: "
						+ (imageAlign & ALIGN_HORIZONTAL_MASK));
			}
		}
		throw new SWTException("Unknown alignment for text: "
				+ (textAlign & ALIGN_HORIZONTAL_MASK));
	}

	/**
	 * Draws the cell content (text & image) vertically.
	 * 
	 * @param gc
	 *            The graphic context to use when painting.
	 * @param rect
	 *            The cell area.
	 * @param text
	 *            The text to draw.
	 * @param textColor
	 *            The text color.
	 * @param backColor
	 *            The background color to use.
	 */
	protected void drawVerticalTextImage(GC gc, Rectangle rect, String text,
			Image img, Color textColor, Color backColor) {
		if (rect.height <= 0)
			rect.height = 1;
		if (rect.width <= 0)
			rect.width = 1;

		Image vImg = new Image(display, rect.height, rect.width);
		GC gcImg = new GC(vImg);
		applyFont(gcImg);

		// clear background and paint content:
		gcImg.setForeground(textColor);
		gcImg.setBackground(backColor);
		gcImg.fillRectangle(vImg.getBounds());

		int alignment = mirrorAlignment();
		drawTextImage(gcImg, text, alignment, img, alignment, 3, 3,
				rect.height - 6, rect.width - 6);
		gcImg.dispose();

		Image mirrorImg = mirrorImage(vImg);
		vImg.dispose();

		gc.setForeground(textColor);
		gc.setBackground(backColor);
		gc.drawImage(mirrorImg, rect.x, rect.y);
		vImg.dispose();
		mirrorImg.dispose();
	}

	private int mirrorAlignment() {
		int align = getAlignment();
		int result = 0;
		if ((align & ALIGN_HORIZONTAL_MASK) == ALIGN_HORIZONTAL_CENTER)
			result = ALIGN_VERTICAL_CENTER;
		else if ((align & ALIGN_HORIZONTAL_MASK) == ALIGN_HORIZONTAL_RIGHT)
			result = ALIGN_VERTICAL_TOP;
		else
			result = ALIGN_VERTICAL_BOTTOM;

		if ((align & ALIGN_VERTICAL_MASK) == ALIGN_VERTICAL_CENTER)
			result |= ALIGN_HORIZONTAL_CENTER;
		else if ((align & ALIGN_VERTICAL_MASK) == ALIGN_VERTICAL_TOP)
			result |= ALIGN_HORIZONTAL_RIGHT;
		else
			result |= ALIGN_HORIZONTAL_LEFT;

		result |= (WRAP_MASK & align);

		return result;
	}

	/**
	 * Mirrors the given image. Note that the returned image must be disposed
	 * after rendering!
	 * 
	 * @param source
	 *            The source image. Gets disposed in this method.
	 * @return Returns a new image with mirrored content. The caller is
	 *         responsible for disposing this image!
	 */
	protected Image mirrorImage(Image source) {
		Rectangle bounds = source.getBounds();

		ImageData sourceData = source.getImageData();
		ImageData resultData = new ImageData(sourceData.height,
				sourceData.width, sourceData.depth, sourceData.palette);
		for (int x = 0; x < bounds.width; x++) {
			for (int y = 0; y < bounds.height; y++) {
				resultData.setPixel(sourceData.height - y - 1, x, sourceData
						.getPixel(x, y));
			}
		}
		return new Image(display, resultData);
	}

	/**
	 * Paints a sign that a comment is present in the right upper corner!
	 * 
	 * @param gc
	 *            The GC to use when painting.
	 * @param rect
	 *            The cell area where content should be added.
	 */
	protected void drawCommentSign(GC gc, Rectangle rect) {
		gc.setBackground(COLOR_COMMENTSIGN);
		gc.fillPolygon(new int[] { rect.x + rect.width - 4, rect.y,
				rect.x + rect.width, rect.y, rect.x + rect.width, rect.y + 4 });
	}

	/**
	 * Draws the text with a vertical alignment style.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param textAlign
	 *            The alignment style of text.
	 * @param x
	 *            The coordinate x of area.
	 * @param y
	 *            The coordinate y of area.
	 * @param width
	 *            The width of area.
	 * @param height
	 *            The height of area.
	 * @return the width of text.
	 */
	public int drawTextVerticalAlign(GC gc, String text, int textAlign, int x,
			int y, int width, int height) {
		if (text == null)
			text = "";

		if ((textAlign & WRAP_MASK) == WRAP) {
			text = wrapText(gc, text, width);
			text = cropWrappedTextForHeight(gc, text, height);
		}

		Rectangle oldClip = gc.getClipping();
		Rectangle newClip = new Rectangle(x, y, width, height);
		newClip.intersect(oldClip);
		gc.setClipping(newClip);

		Point textSize = getCachedStringExtent(gc, text);

		if ((textAlign & ALIGN_VERTICAL_MASK) == ALIGN_VERTICAL_TOP) {
			gc.drawText(text, x, y);
		} else if ((textAlign & ALIGN_VERTICAL_MASK) == ALIGN_VERTICAL_BOTTOM) {
			gc.drawText(text, x, y + height - textSize.y);
		} else if ((textAlign & ALIGN_VERTICAL_MASK) == ALIGN_VERTICAL_CENTER) {
			int yOffset = (height - textSize.y) / 2;
			gc.drawText(text, x, y + yOffset);
		} else {
			gc.drawText(text, x, y);
		}

		gc.setClipping(oldClip);
		return textSize.x;
	}

	/**
	 * Wraps the text according to the width of area.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param width
	 *            The width of area.
	 * @return The wrapped text.
	 */
	protected String wrapText(GC gc, String text, int width) {
		Point textSize = getCachedStringExtent(gc, text);
		if (textSize.x > width) {
			StringBuffer wrappedText = new StringBuffer();
			String[] lines = text.split("\n");
			int cutoffLength = width
					/ gc.getFontMetrics().getAverageCharWidth();
			if (cutoffLength < 3)
				return text;
			for (int i = 0; i < lines.length; i++) {
				int breakOffset = 0;
				while (breakOffset < lines[i].length()) {
					String lPart = lines[i].substring(breakOffset, Math.min(
							breakOffset + cutoffLength, lines[i].length()));
					Point lineSize = getCachedStringExtent(gc, lPart);
					while ((lPart.length() > 0) && (lineSize.x >= width)) {
						lPart = lPart.substring(0, Math.max(lPart.length() - 1,
								0));
						lineSize = getCachedStringExtent(gc, lPart);
					}
					wrappedText.append(lPart);
					breakOffset += lPart.length();
					wrappedText.append('\n');
				}
			}
			return wrappedText.substring(0, Math.max(wrappedText.length() - 1,
					0));
		} else
			return text;

	}

	/**
	 * Crops the wrapped text according to the height of area.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param text
	 *            The text to be drewn.
	 * @param height
	 *            The height of area.
	 * @return The cropped string.
	 */
	protected String cropWrappedTextForHeight(GC gc, String text, int height) {
		String[] lines = text.split("\n");
		int linesToTake = height / gc.getFontMetrics().getHeight();
		if (linesToTake < 1)
			linesToTake = 1;
		if (lines.length <= linesToTake)
			return text;
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < linesToTake; i++) {
			buffer.append(lines[i]);
			buffer.append('\n');
		}
		return buffer.substring(0, Math.max(buffer.length() - 1, 0));
	}

	/**
	 * Draws image with vertical alignment style.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param image
	 *            The image to be drewn.
	 * @param imageAlign
	 *            The alignment style of image.
	 * @param x
	 *            The coordinate x of area.
	 * @param y
	 *            The coordinate y of area.
	 * @param height
	 *            The height of area.
	 */
	protected void drawImageVerticalAlign(GC gc, Image image, int imageAlign,
			int x, int y, int height) {
		if (image == null)
			return;
		Rectangle imageBounds = image.getBounds();
		Point imageSize = new Point(imageBounds.width, imageBounds.height);
		//
		if ((imageAlign & ALIGN_VERTICAL_MASK) == ALIGN_VERTICAL_TOP) {
			gc.drawImage(image, x, y);
			return;
		}
		if ((imageAlign & ALIGN_VERTICAL_MASK) == ALIGN_VERTICAL_BOTTOM) {
			gc.drawImage(image, x, y + height - imageSize.y);
			return;
		}
		if ((imageAlign & ALIGN_VERTICAL_MASK) == ALIGN_VERTICAL_CENTER) {
			int yOffset = (height - imageSize.y) / 2;
			gc.drawImage(image, x, y + yOffset);
			return;
		}
		throw new SWTException("Unknown alignment for image: "
				+ (imageAlign & ALIGN_VERTICAL_MASK));
	}

	/**
	 * Draws a shadow image.
	 * 
	 * @param gc
	 *            The GC to draw on.
	 * @param image
	 *            The image to be drewn.
	 * @param x
	 *            The coordinate x of area.
	 * @param y
	 *            The coordinate y of area.
	 * @param alpha
	 * 
	 */
	protected void drawShadowImage(GC gc, Image image, int x, int y, int alpha) {
		Rectangle imageBounds = image.getBounds();
		Point imageSize = new Point(imageBounds.width, imageBounds.height);
		//
		ImageData imgData = new ImageData(imageSize.x, imageSize.y, 24,
				new PaletteData(255, 255, 255));
		imgData.alpha = alpha;
		Image img = new Image(display, imgData);
		GC imgGC = new GC(img);
		imgGC.drawImage(image, 0, 0);
		gc.drawImage(img, x, y);
		imgGC.dispose();
		img.dispose();
	}

	// Calculates string extent.
	private static GC lastGCFromExtent;
	private static Map<String, Point> stringExtentCache = new HashMap<String, Point>();

	/**
	 * Calculates the extent of given string.
	 * 
	 * @param gc
	 *            The gc to operate on.
	 * @param text
	 *            The string to calculate the string extent.
	 * @return A point containing the extent of the string
	 */
	public static synchronized Point getCachedStringExtent(GC gc, String text) {
		if (lastGCFromExtent != gc) {
			stringExtentCache.clear();
			lastGCFromExtent = gc;
		}
		Point p = (Point) stringExtentCache.get(text);
		if (p == null) {
			if (text == null)
				return new Point(0, 0);
			p = gc.textExtent(text);
			stringExtentCache.put(text, p);
		}
		return new Point(p.x, p.y);
	}
}
