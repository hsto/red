/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import org.eclipse.swt.widgets.Event;

/**
 * This interface define the strategy how the agile grid is navigated using the
 * keyboard.
 * 
 * @author fourbroad
 * 
 */
public interface ICellNavigationStrategy {

	/**
	 * Is the given event an event which moves the selection to another cell
	 * 
	 * @param agileGrid
	 *            the agile grid this strategy are working for
	 * @param event
	 *            the key event
	 * @return <code>true</code> if a new cell is searched
	 */
	public abstract boolean isNavigationEvent(AgileGrid agileGrid, Event event);

	/**
	 * Finds the next selected cell according to the event.
	 * 
	 * @param agileGrid
	 *            the agile grid this strategy are working for.
	 * @param currentSelectedCell
	 *            the cell currently selected
	 * @param event
	 *            the key event
	 * @return the cell which is highlighted next or <code>null</code> if the
	 *         default implementation is taken. E.g. it's fairly impossible to
	 *         react on PAGE_DOWN requests
	 */
	public abstract Cell findSelectedCell(AgileGrid agileGrid,
			Cell currentSelectedCell, Event event);

	/**
	 * This method is consulted to decide whether an event has to be canceled or
	 * not. By default events who collapse/expand tree-nodes are canceled
	 * 
	 * @param agileGrid
	 *            the agile grid working for
	 * @param event
	 *            the event
	 * @return <code>true</code> if the event has to be canceled
	 */
	public abstract boolean shouldCancelEvent(AgileGrid agileGrid, Event event);

}