/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */
package org.agilemore.agilegrid;

/**
 * CellColumn represents a column of cells.
 * 
 * @author fourbroad
 * 
 */
public class CellColumn extends Cell {
	public CellColumn(AgileGrid agileGrid, int column) {
		super(agileGrid, Integer.MIN_VALUE, column);
	}
}
