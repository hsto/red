/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeListenerProxy;

/**
 * ILayoutAdvisor provide layout information for agile grid. Generally speaking,
 * all functions should return their results as quick as possible, since they
 * might be called a few times when laying out.
 * <p>
 * NOTE that there exists a default implementation in the class
 * <code>DefaultLayoutAdvisor</code> that handles the column width and row
 * height in the agile grid.
 * <p>
 * <b>Before implementing this interface, consider extending
 * DefaultLayoutAdvisor</b>
 * 
 * @author fourbroad
 * 
 */
public interface ILayoutAdvisor {

	public static final String RowCount = "rowCount";
	public static final String TopHeaderHeight = "topHeaderHeight";
	public static final String TopHeaderVisible = "topHeaderVisible";
	public static final String TopHeaderLabel = "topHeaderLabel";
	public static final String ColumnCount = "columnCount";
	public static final String LeftHeaderWidth = "leftHeaderWidth";
	public static final String LeftHeaderVisible = "leftHeaderVisible";
	public static final String LeftHeaderLabel = "leftHeaderLabel";
	public static final String ColumnWidth = "columnWidth";
	public static final String ColumnResizable = "columnResizable";
	public static final String RowHeight = "rowHeight";
	public static final String RowHeightMinimum = "rowHeightMinimum";
	public static final String RowResizable = "rowResizable";
	public static final String FixedRowCount = "fixedRowCount";
	public static final String FixedColumnCount = "fixedColumnCount";
	public static final String Tooltip = "tooltip";


	/**
	 * Allows cells to merge with other cells.
	 * <p>
	 * Return the column and row index of the cell the given cell should be
	 * merged with. Note that cells can only merge with cells that have a
	 * row/col index smaller or equal than their own index.
	 * <p>
	 * The content of a spanned, large cell is determined by the left upper
	 * cell, a 'supercell'. Such supercells as well as cells that do not span
	 * always return their own indices. So if no cell spanning is desired,
	 * simply return the given cell location:
	 * <code>return new Cell(agileGrid,row, col);</code>
	 * <p>
	 * To visualize the expected return value: <code>
	 * Normal agile grid:   Spanned agile grid:
	 *  ___________     ___________
	 * |__|__|__|__|   |     |__|__|
	 * |__|__|__|__|   |_____|__|__|
	 * </code> In this case, the left upper cell (0, 0) returns its own index
	 * and is responsible for the content of the whole spanned cell. The cells
	 * (0, 1), (1, 0) and (1, 1) are overlapped and thus not visible. So they
	 * return (0, 0) to signal that they belong to the cell (0, 0). Note that in
	 * this case, the value of the cell (1, 1) is never requested, since the
	 * large cell must always be a rectangle. But cells like <code>
	 *  ___________
	 * |   __|__|__| 
	 * |__|__|__|__|
	 * </code> are not possible.
	 * 
	 * @param row
	 *            the row index of cell.
	 * @param col
	 *            the column index of cell.
	 * @return The given cell, or a cell the given cell should be merged with.
	 */
	public Cell mergeInto(int row, int col);

	/**
	 * The row count that the agile grid has to display, which don't include the
	 * header row.
	 * 
	 * @return The number of rows in the agile grid, including the fixed rows.
	 */
	public int getRowCount();

	/**
	 * The row count that the agile grid has to display, which don't include the
	 * header row.
	 * 
	 * @param rowCount
	 *            The row count that the agile grid has to display.
	 */
	public void setRowCount(int rowCount);

	/**
	 * The height of top header area if the top header is visible.
	 * 
	 * @return The height of top header.
	 */
	public int getTopHeaderHeight();

	/**
	 * Sets the height of top header.
	 * 
	 * @param height
	 *            The height of top header.
	 */
	public void setTopHeaderHeight(int height);

	/**
	 * Is the top header area visible, if true the top header area will be
	 * shown, otherwise false.
	 * 
	 * @return true if the top header area is visible, false otherwise.
	 */
	public boolean isTopHeaderVisible();

	/**
	 * Sets the visible status of top header.
	 * 
	 * @param visible
	 *            Is the top header area visible.
	 */
	public void setTopHeaderVisible(boolean visible);

	/**
	 * The string returned will be shown in the given cell of top header
	 * indicated by column index..
	 * 
	 * @param col
	 *            The column index.
	 * @return The string that will be shown in the given cell of top header.
	 */
	public String getTopHeaderLabel(int col);

	/**
	 * Sets the indicated column label of top header.
	 * 
	 * @param col
	 *            The column index.
	 * @param label
	 *            The label which should be shown in the indicated column of top
	 *            header.
	 */
	public void setTopHeaderLabel(int col, String label);

	/**
	 * the column count that the agile grid has to display.
	 * 
	 * @return The number of columns in the agile grid, including all fixed
	 *         columns.
	 */
	public int getColumnCount();

	/**
	 * Sets the column count that the agile grid has to display.
	 * 
	 * @param columnCount
	 *            The number of columns in the agile grid, including all fixed
	 *            columns.
	 */
	public void setColumnCount(int columnCount);

	/**
	 * The width of left header area if the left header is visible.
	 * 
	 * @return The width of left header.
	 */
	public int getLeftHeaderWidth();

	/**
	 * Sets the width of left header area.
	 * 
	 * @param width
	 *            The width of left header.
	 */
	public void setLeftHeaderWidth(int width);

	/**
	 * Is the left header area visible, if true the left header area will be
	 * shown, otherwise false.
	 * 
	 * @return true if the left header area is visible, false otherwise.
	 */
	public boolean isLeftHeaderVisible();

	/**
	 * Sets the visible status of left header.
	 * 
	 * @param visible
	 *            Is the left header visible.
	 */
	public void setLeftHeaderVisible(boolean visible);

	/**
	 * The string returned will be shown in the given cell of top header
	 * indicated by row index.
	 * 
	 * @param row
	 *            The row index.
	 * @return The string that will be shown in the given cell of top header.
	 */
	public String getLeftHeaderLabel(int row);

	/**
	 * Sets the indicated row label of left header.
	 * 
	 * @param row
	 *            The row index.
	 * @param label
	 *            The label should be shown in the row of left header.
	 */
	public void setLeftHeaderLabel(int row, String label);

	/**
	 * Each column can have its individual width. The layout advisor has to
	 * manage these widths and return the values with this function.
	 * 
	 * @param col
	 *            The index of the column
	 * @return The width in pixels for this column.
	 */
	public int getColumnWidth(int col);

	/**
	 * Each column can have its individual width. The layout advisor has to
	 * manage these widths. If the user resizes a column, the layout advisor has
	 * to keep track of these changes. The layout advisor is informed about such
	 * a resize by this method. (view updates are managed by the agile grid)
	 * 
	 * @param col
	 *            the column index
	 * @param width
	 *            The width in pixels to set for the given column.
	 */
	public void setColumnWidth(int col, int width);

	/**
	 * Returns true if the user should be allowed to resize the given column,
	 * false otherwise.
	 * 
	 * @param col
	 *            The column index
	 * @return true if the column is resizable, false otherwise.
	 */
	public boolean isColumnResizable(int col);

	/**
	 * Set the resizable status of column indicated
	 * 
	 * @param col
	 *            The column index
	 * @param resizable
	 *            Is the column resizable.
	 */
	public void setColumnResizable(int col, boolean resizable);

	/**
	 * Each row can have its individual height. The layout advisor has to manage
	 * these heights and return the values with this function.
	 * 
	 * @param row
	 *            The index of row.
	 * @return The current height of row.
	 */
	public int getRowHeight(int row);

	/**
	 * If the user resizes a row, the layout advisor has to keep track of these
	 * changes. The layout advisor is informed about such a resize by this
	 * method. (view updates are managed by the agile grid)
	 * 
	 * @param row
	 *            The row index.
	 * @param value
	 *            The height of row.
	 */
	public void setRowHeight(int row, int value);

	/**
	 * Returns true if the user should be allowed to resize the rows, false
	 * otherwise.
	 * 
	 * @param row
	 *            The index of row.
	 * @return true if the column is resizable, false otherwise.
	 */
	public boolean isRowResizable(int row);

	/**
	 * Sets the resizable status of the row indicated.
	 * 
	 * @param row
	 *            The row index.
	 * @param resizable
	 *            Is the row resizable.
	 */
	public void setRowResizable(int row, boolean resizable);

	/**
	 * Returns the minimum height of the rows. It is only needed if the rows are
	 * resizable.
	 * 
	 * @return The minimum height for the rows.
	 */
	public int getRowHeightMinimum();

	/**
	 * Sets the minimum height of row indicated.
	 * 
	 * @param minimumHeight
	 *            The minimum height of row indicated.
	 */
	public void setRowHeightMinimum(int minimumHeight);

	/**
	 * Returns whether a given cell is fixed.
	 * 
	 * @param row
	 *            the row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return true if the cell is fixed, false otherwise.
	 */
	public boolean isFixedCell(int row, int col);

	/**
	 * Returns the total number of fixed rows in the agile grid.
	 * 
	 * @return the total number of fixed rows in the agile grid.
	 */
	public int getFixedRowCount();

	/**
	 * Sets the fixed row number of agile grid.
	 * 
	 * @param fixedRowCount
	 *            The fixed row count.
	 */
	public void setFixedRowCount(int fixedRowCount);

	/**
	 * Returns the total number of fixed columns in the agile grid. This is
	 * nothing else than the sum of header columns and selectable fixed columns.
	 * 
	 * @return The total number of fixed columns in the agile grid.
	 */
	public int getFixedColumnCount();

	/**
	 * Sets the fixed column count of agile grid.
	 * 
	 * @param fixedColumnCount
	 *            The fixed column count of agile grid.
	 */
	public void setFixedColumnCount(int fixedColumnCount);

	/**
	 * Returns the compositorStrategy used when sorting the agile grid.
	 * 
	 * @return the compositorStrategy.
	 */
	public ICompositorStrategy getCompositorStrategy();

	/**
	 * Sets the compositor strategy, agile grid will use it to sort the column
	 * of agile grid.
	 * 
	 * @param compositorStrategy
	 *            the compositor strategy.
	 */
	public void setCompositorStrategy(ICompositorStrategy compositorStrategy);

	/**
	 * It is shown when the mouse stops over a cell and typically gives advanced
	 * information or shows the complete content.
	 * <p>
	 * Return <code>null</code> or <code>""</code> if no tooltip should be
	 * displayed.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return The text that should be displayed when the tooltip for the cell
	 *         is shown to the user.
	 */
	public String getTooltip(int row, int col);

	/**
	 * Sets the tooltip of indicated cell. if setting null, no tooltip will be
	 * shown.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @param tooltip
	 *            The text that should be shown when the mouse hover the
	 *            indicated cell.
	 */
	public void setTooltip(int row, int col, String tooltip);

	/**
	 * Add a PropertyChangeListener to the listener list. The listener is
	 * registered for all properties. The same listener object may be added more
	 * than once, and will be called as many times as it is added. If
	 * <code>listener</code> is null, no exception is thrown and no action is
	 * taken.
	 * 
	 * @param listener
	 *            The PropertyChangeListener to be added
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener);

	/**
	 * Add a PropertyChangeListener for a specific property. The listener will
	 * be invoked only when a call on firePropertyChange names that specific
	 * property. The same listener object may be added more than once. For each
	 * property, the listener will be invoked the number of times it was added
	 * for that property. If <code>propertyName</code> or <code>listener</code>
	 * is null, no exception is thrown and no action is taken.
	 * 
	 * @param propertyName
	 *            The name of the property to listen on.
	 * @param listener
	 *            The PropertyChangeListener to be added
	 */
	public void addPropertyChangeListener(String propertyName,
			PropertyChangeListener listener);

	/**
	 * Returns an array of all the listeners that were added to the
	 * PropertyChangeSupport object with addPropertyChangeListener().
	 * <p>
	 * If some listeners have been added with a named property, then the
	 * returned array will be a mixture of PropertyChangeListeners and
	 * <code>PropertyChangeListenerProxy</code>s. If the calling method is
	 * interested in distinguishing the listeners then it must test each element
	 * to see if it's a <code>PropertyChangeListenerProxy</code>, perform the
	 * cast, and examine the parameter.
	 * 
	 * <pre>
	 * PropertyChangeListener[] listeners = bean.getPropertyChangeListeners();
	 * for (int i = 0; i &lt; listeners.length; i++) {
	 * 	if (listeners[i] instanceof PropertyChangeListenerProxy) {
	 * 		PropertyChangeListenerProxy proxy = (PropertyChangeListenerProxy) listeners[i];
	 * 		if (proxy.getPropertyName().equals(&quot;foo&quot;)) {
	 * 			// proxy is a PropertyChangeListener which was associated
	 * 			// with the property named &quot;foo&quot;
	 * 		}
	 * 	}
	 * }
	 *</pre>
	 * 
	 * @see PropertyChangeListenerProxy
	 * @return all of the <code>PropertyChangeListeners</code> added or an empty
	 *         array if no listeners have been added
	 */
	public PropertyChangeListener[] getPropertyChangeListeners();

	/**
	 * Returns an array of all the listeners which have been associated with the
	 * named property.
	 * 
	 * @param propertyName
	 *            The name of the property being listened to
	 * @return all of the <code>PropertyChangeListeners</code> associated with
	 *         the named property. If no such listeners have been added, or if
	 *         <code>propertyName</code> is null, an empty array is returned.
	 */
	public PropertyChangeListener[] getPropertyChangeListeners(
			String propertyName);

	/**
	 * Check if there are any listeners for a specific property, including those
	 * registered on all properties. If <code>propertyName</code> is null, only
	 * check for listeners registered on all properties.
	 * 
	 * @param propertyName
	 *            the property name.
	 * @return true if there are one or more listeners for the given property
	 */
	public boolean hasListeners(String propertyName);

	/**
	 * Remove a PropertyChangeListener from the listener list. This removes a
	 * PropertyChangeListener that was registered for all properties. If
	 * <code>listener</code> was added more than once to the same event source,
	 * it will be notified one less time after being removed. If
	 * <code>listener</code> is null, or was never added, no exception is thrown
	 * and no action is taken.
	 * 
	 * @param listener
	 *            The PropertyChangeListener to be removed
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener);

	/**
	 * Remove a PropertyChangeListener for a specific property. If
	 * <code>listener</code> was added more than once to the same event source
	 * for the specified property, it will be notified one less time after being
	 * removed. If <code>propertyName</code> is null, no exception is thrown and
	 * no action is taken. If <code>listener</code> is null, or was never added
	 * for the specified property, no exception is thrown and no action is
	 * taken.
	 * 
	 * @param propertyName
	 *            The name of the property that was listened on.
	 * @param listener
	 *            The PropertyChangeListener to be removed
	 */
	public void removePropertyChangeListener(String propertyName,
			PropertyChangeListener listener);
}