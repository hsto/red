/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.util.EventObject;

/**
 * This event is fired when an editor deactivated
 * 
 * @author fourbroad
 */
public class EditorDeactivationEvent extends EventObject {

	private static final long serialVersionUID = 1L;

	/**
	 * true if the event is triggered by applyEditorValue method of the
	 * AgileGridEditor, false by cancelEditorValue.
	 */
	public boolean isApplyValue;

	/**
	 * The cell editor for agile grid.
	 */
	public CellEditor cellEditor;

	/**
	 * Creates a new agile grid editor deactivation event.
	 * 
	 * @param source
	 *            The source of event.
	 */
	public EditorDeactivationEvent(Object source, CellEditor cellEditor,
			boolean isApplyValue) {
		super(source);
		this.cellEditor = cellEditor;
		this.isApplyValue = isApplyValue;
	}

}
