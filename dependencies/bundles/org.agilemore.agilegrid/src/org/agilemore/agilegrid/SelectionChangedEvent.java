/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */
package org.agilemore.agilegrid;

import java.util.EventObject;
import java.util.HashSet;
import java.util.Set;

/**
 * Event object describing a selection change. The source of these events is a
 * agile grid.
 * 
 * @author fourbroad
 */
public class SelectionChangedEvent extends EventObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2351169764483288361L;

	/**
	 * The selections before change.
	 */
	protected Set<Cell> oldSelections;

	/**
	 * The selections after change.
	 */
	protected Set<Cell> newSelections;

	/**
	 * Creates a new selection changed event.
	 * 
	 * @param source
	 *            The source of event.
	 * @param oldSelections
	 *            The old selections.
	 * @param newSelections
	 *            The new selections.
	 */
	public SelectionChangedEvent(Object source, Set<Cell> newSelections,
			Set<Cell> oldSelections) {
		super(source);
		this.oldSelections = oldSelections;
		this.newSelections = newSelections;
	}

	/**
	 * Returns the old selections.
	 * 
	 * @return the old selections.
	 */
	public Set<Cell> getOldSelections() {
		return this.oldSelections;
	}

	/**
	 * Returns the new selections.
	 * 
	 * @return the new selections.
	 */
	public Set<Cell> getNewSelections() {
		return this.newSelections;
	}

	/**
	 * Returns the new added selections.
	 * 
	 * @return The new added selections.
	 */
	public Set<Cell> getAddedSelections() {
		Set<Cell> addedSelections = new HashSet<Cell>(newSelections);
		addedSelections.removeAll(oldSelections);
		return addedSelections;
	}

	/**
	 * Returns the removed selections.
	 * 
	 * @return The removed selections.
	 */
	public Set<Cell> getRemovedSelections() {
		Set<Cell> removedSelections = new HashSet<Cell>(oldSelections);
		removedSelections.removeAll(newSelections);
		return removedSelections;
	}
}
