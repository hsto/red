/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.HTMLTransfer;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.actions.ActionFactory;

/**
 * This action handler is used to deal with the usual operation, include copy,
 * cut, paste and selection etc.
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 * 
 */
public class AgileGridActionHandler {

	static final char TAB = '\t';
	static final String PlatformLineDelimiter = System
			.getProperty("line.separator");

	public CopyAction copyAction;
	public CopyAllAction copyAllAction;
	public CutAction cutAction;
	public PasteAction pasteAction;
	public SelectAllAction selectAllAction;

	protected AgileGrid agileGrid;
	protected MenuManager contextMenuManager;

	/**
	 * 
	 */
	public AgileGridActionHandler(AgileGrid agileGrid) {
		this.agileGrid = agileGrid;
		createActions();
		registerActionUpdater();

		// add actions to context menu:
		contextMenuManager = new MenuManager("#PopupMenu");
		contextMenuManager.setRemoveAllWhenShown(true);
		contextMenuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				fillContextMenu(manager);
			}
		});
		Menu menu = contextMenuManager.createContextMenu(this.agileGrid);
		this.agileGrid.setMenu(menu);
	}

	/**
	 * @return Returns the menu manager used to build the context menu of the
	 *         agile grid.
	 *         <p>
	 *         The purpose for this is normally the registering of context menus
	 *         in the workbench.
	 * @see org.eclipse.ui.IWorkbenchPartSite#registerContextMenu(org.eclipse.jface.action.MenuManager,
	 *      org.eclipse.jface.viewers.ISelectionProvider)
	 */
	public MenuManager getMenuManager() {
		return contextMenuManager;
	}

	protected void createActions() {
		copyAction = new CopyAction();
		copyAllAction = new CopyAllAction();
		pasteAction = new PasteAction();
		cutAction = new CutAction();
		selectAllAction = new SelectAllAction();
	}

	protected void fillContextMenu(IMenuManager menumanager) {
		menumanager.add(copyAction);
		menumanager.add(cutAction);
		menumanager.add(pasteAction);
		menumanager.add(new Separator());
		menumanager.add(copyAllAction);
		menumanager.add(selectAllAction);
		menumanager.add(new Separator());
		// Other plug-ins can contribute their actions here
		menumanager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	/**
	 * Registers the cut, copy, paste and select_all actions for global use at
	 * the IActionBar given.
	 * <p>
	 * Currently does not set up the UNDO and REDO actions because they will be
	 * implemented in another way.
	 * 
	 * @param actionBar
	 *            The IActionBars that allows global action registration.
	 *            Normally you can get that with getViewerSite().getActionBars()
	 *            or getEditorSite().getActionBars().
	 */
	public void registerGlobalActions(IActionBars actionBar) {
		actionBar.setGlobalActionHandler(ActionFactory.CUT.getId(),
				this.cutAction);
		actionBar.setGlobalActionHandler(ActionFactory.COPY.getId(),
				this.copyAction);
		actionBar.setGlobalActionHandler(ActionFactory.PASTE.getId(),
				this.pasteAction);
		actionBar.setGlobalActionHandler(ActionFactory.SELECT_ALL.getId(),
				this.selectAllAction);
		actionBar.updateActionBars();
	}

	protected class CopyAction extends Action {
		protected CopyAction() {
			setId("AgileGridCopyActionHandler");//$NON-NLS-1$
			setEnabled(false);
			setText("Copy");
		}

		public void run() {
			if (agileGrid != null && !agileGrid.isDisposed()) {
				setClipboardContent(agileGrid.getCellSelection());
			}
		}

		public void updateEnabledState() {
			if (agileGrid != null && !agileGrid.isDisposed()) {
				Cell[] selection = agileGrid.getCellSelection();
				setEnabled(selection != null && selection.length > 0);
			} else
				setEnabled(false);
		}
	}

	protected class CopyAllAction extends Action {
		protected CopyAllAction() {
			setId("AgileGridCopyAllActionHandler");//$NON-NLS-1$
			setEnabled(false);
			setText("Copy All");
		}

		public void run() {
			if (agileGrid != null && !agileGrid.isDisposed()) {
				setClipboardContent(getAllAgileGridCells());
			}
		}

		public void updateEnabledState() {
			if (agileGrid != null && !agileGrid.isDisposed()) {
				setEnabled(true);
			} else
				setEnabled(false);
		}

		private Cell[] getAllAgileGridCells() {
			ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
			if (layoutAdvisor == null)
				return new Cell[] {};
			Vector<Cell> cells = new Vector<Cell>(layoutAdvisor.getColumnCount()
					* layoutAdvisor.getRowCount());
			for (int row = 0; row < layoutAdvisor.getRowCount(); row++) {
				for (int col = 0; col < layoutAdvisor.getColumnCount(); col++) {
					Cell valid = layoutAdvisor.mergeInto(col, row);
					if (valid.row == row && valid.column == col)
						cells.add(valid);
				}
			}
			return cells.toArray(new Cell[] {});
		}
	}

	protected class CutAction extends Action {
		protected CutAction() {
			setId("AgileGridCutActionHandler");//$NON-NLS-1$
			setEnabled(false);
			setText("Cut");
		}

		public void run() {
			if (agileGrid != null && !agileGrid.isDisposed()) {
				Cell[] selection = agileGrid.getCellSelection();
				setClipboardContent(selection);
				removeContentAt(selection);
			}
		}

		public void updateEnabledState() {
			if (agileGrid != null && !agileGrid.isDisposed()) {
				Cell[] selection = agileGrid.getCellSelection();
				setEnabled(selection != null && selection.length > 0);
			} else
				setEnabled(false);
		}

		protected void removeContentAt(Cell[] selection) {
			IContentProvider model = agileGrid.getContentProvider();
			if (model == null)
				return;
			boolean updateSeperateCells = selection.length > 4 ? false : true;
			try {
				if (!updateSeperateCells)
					agileGrid.setRedraw(false);
				for (int i = 0; i < selection.length; i++) {
					model.setContentAt(selection[i].row, selection[i].column,
							"");
				}
				if (updateSeperateCells)
					agileGrid.redrawCells(selection);

			} finally {
				if (!updateSeperateCells)
					agileGrid.setRedraw(true);
			}
		}
	}

	protected class SelectAllAction extends Action {
		protected SelectAllAction() {
			setId("AgileGridSelectAllActionHandler");//$NON-NLS-1$
			setEnabled(false);
			setText("Select All");
		}

		public void run() {
			if (agileGrid != null && !agileGrid.isDisposed()) {
				ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
				if (layoutAdvisor != null)
					selectAll(layoutAdvisor);
			}
		}

		public void updateEnabledState() {
			if (agileGrid != null && !agileGrid.isDisposed()
					&& agileGrid.isMultiSelectMode()) {
				setEnabled(true);
			} else
				setEnabled(false);
		}

		protected void selectAll(ILayoutAdvisor layoutAdvisor) {
			Vector<Cell> sel = new Vector<Cell>();
			for (int row = 0; row < layoutAdvisor.getRowCount(); row++)
				for (int col = 0; col < layoutAdvisor.getColumnCount(); col++) {
					Cell cell = layoutAdvisor.mergeInto(col, row);
					if (cell.column == col && cell.row == row)
						sel.add(cell);
				}
			try {
				agileGrid.setRedraw(false);
				agileGrid.clearSelection();
				agileGrid.selectCells((Cell[]) sel.toArray(new Cell[] {}));
			} finally {
				agileGrid.setRedraw(true);
			}
		}
	}

	protected class PasteAction extends Action {
		protected PasteAction() {
			setId("AgileGridPasteActionHandler");//$NON-NLS-1$
			setEnabled(false);
			setText("Paste");
		}

		public void run() {
			if (agileGrid != null && !agileGrid.isDisposed()) {
				pasteToSelection(getTextFromClipboard(), agileGrid
						.getCellSelection());
			}
		}

		protected String getTextFromClipboard() {
			Clipboard clipboard = new Clipboard(agileGrid.getDisplay());
			try {
				return clipboard.getContents(TextTransfer.getInstance())
						.toString();
			} catch (Exception ex) {
				return "";
			} finally {
				clipboard.dispose();
			}
		}

		protected void pasteToSelection(String text, Cell[] selection) {
			if (selection == null || selection.length == 0)
				return;
			IContentProvider model = agileGrid.getContentProvider();
			if (model == null)
				return;

			try {
				agileGrid.setRedraw(false);
				agileGrid.clearSelection();
				Vector<Cell> sel = new Vector<Cell>();

				String[][] cellTexts = parseCellTexts(text);
				for (int row = 0; row < cellTexts.length; row++)
					for (int col = 0; col < cellTexts[row].length; col++) {
						model.setContentAt(row + selection[0].row, col
								+ selection[0].column, cellTexts[row][col]);
						sel.add(new Cell(agileGrid, row + selection[0].row, col
								+ selection[0].column));
					}
				agileGrid.selectCells((Cell[]) sel.toArray(new Cell[] {}));
			} finally {
				agileGrid.setRedraw(true);
			}
		}

		protected String[][] parseCellTexts(String text) {
			if (!agileGrid.isMultiSelectMode()) {
				return new String[][] { { text } };
			} else {
				String[] lines = text.split(PlatformLineDelimiter);
				String[][] cellText = new String[lines.length][];
				for (int line = 0; line < lines.length; line++)
					cellText[line] = lines[line].split(TAB + "");
				return cellText;
			}
		}

		public void updateEnabledState() {
			if (agileGrid != null && !agileGrid.isDisposed()) {
				Cell[] selection = agileGrid.getCellSelection();
				if (selection == null)
					setEnabled(false);
				else if (selection.length > 1) // &&
					setEnabled(false);
				else
					setEnabled(true);
			} else
				setEnabled(false);
		}
	}

	/**
	 * Copies the specified text range to the clipboard. The agile grid will be
	 * placed in the clipboard in plain text format and RTF format.
	 * 
	 * @param selection
	 *            The list of cell indices thats content should be set to the
	 *            clipboard.
	 * 
	 * @exception SWTError,
	 *                see Clipboard.setContents
	 * @see org.eclipse.swt.dnd.Clipboard.setContents
	 */
	protected void setClipboardContent(Cell[] selection) throws SWTError {
		// RTFTransfer rtfTransfer = RTFTransfer.getInstance();
		TextTransfer plainTextTransfer = TextTransfer.getInstance();
		HTMLTransfer htmlTransfer = HTMLTransfer.getInstance();

		// String rtfText = getRTFForSelection(selection);
		String plainText = getTextForSelection(selection);
		String htmlText = getHTMLForSelection(selection);

		Clipboard clipboard = new Clipboard(agileGrid.getDisplay());
		try {
			clipboard.setContents(new String[] { plainText, htmlText }, // rtfText
					new Transfer[] { plainTextTransfer, htmlTransfer }); // rtfTransfer
		} catch (SWTError error) {
			// Copy to clipboard failed. This happens when another application
			// is accessing the clipboard while we copy. Ignore the error.
			// Rethrow all other errors.
			if (error.code != DND.ERROR_CANNOT_SET_CLIPBOARD) {
				throw error;
			}
		} finally {
			clipboard.dispose();
		}
	}

	private Cell[] findAgileGridDimensions(Cell[] selection) {
		Cell topLeft = new Cell(agileGrid, -1, -1);
		Cell bottomRight = new Cell(agileGrid, -1, -1);

		for (int i = 0; i < selection.length; i++) {
			Cell cell = selection[i];
			if (topLeft.column < 0)
				topLeft.column = cell.column;
			else if (topLeft.column > cell.column)
				topLeft.column = cell.column;
			if (bottomRight.column < 0)
				bottomRight.column = cell.column;
			else if (bottomRight.column < cell.column)
				bottomRight.column = cell.column;

			if (topLeft.row < 0)
				topLeft.row = cell.row;
			else if (topLeft.row > cell.row)
				topLeft.row = cell.row;
			if (bottomRight.row < 0)
				bottomRight.row = cell.row;
			else if (bottomRight.row < cell.row)
				bottomRight.row = cell.row;
		}
		return new Cell[] { topLeft, bottomRight };
	}

	private Cell findCellSpanning(int row, int col, ILayoutAdvisor layoutAdvisor) {
		Cell spanning = new Cell(agileGrid, 1, 1);
		Cell cell = new Cell(agileGrid, row, col);
		while (layoutAdvisor.mergeInto(col + spanning.column, row).equals(
				cell))
			spanning.column++;

		while (layoutAdvisor.mergeInto(col, row + spanning.row)
				.equals(cell))
			spanning.row++;

		return spanning;
	}

	protected String getHTMLForSelection(Cell[] selection) {
		StringBuffer html = new StringBuffer();
		sortSelectedCells(selection);

		Cell[] dimensions = findAgileGridDimensions(selection);
		Cell topLeft = dimensions[0];
		Cell bottomRight = dimensions[1];

		ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
		IContentProvider model = agileGrid.getContentProvider();
		if (layoutAdvisor == null)
			return "";
		// add header:
		html.append("Version:1.0\n");
		html.append("StartHTML:0000000000\n");
		html.append("EndHTML:0000000000\n");
		html.append("StartFragment:0000000000\n");
		html.append("EndFragment:0000000000\n");
		html.append("<html><body><table>");

		Cell nextValidCell = selection[0];
		int selCounter = 1;
		for (int row = topLeft.row; row <= bottomRight.row; row++) {
			html.append("<tr>");
			for (int col = topLeft.column; col <= bottomRight.column; col++) {
				// may skip the cell when it is spanned by another one.
				if (layoutAdvisor.mergeInto(col, row).equals(
						new Point(col, row))) {

					if (nextValidCell.column == col
							&& nextValidCell.column == row) {
						html.append("<td");
						Cell spanning = findCellSpanning(row, col,
								layoutAdvisor);
						if (spanning.row > 1)
							html.append(" rowspan=\"" + spanning.row + "\"");
						if (spanning.column > 1)
							html.append(" colspan=\"" + spanning.column + "\"");
						html.append(">");

						Object content = model.getContentAt(col, row);
						html.append(maskHtmlChars(content.toString()));
						if (selCounter < selection.length) {
							nextValidCell = selection[selCounter];
							selCounter++;
						}
					} else
						html.append("<td>");

					html.append("</td>");
				}
			}
			html.append("</tr>");
		}
		html.append("</table></body></html>");

		return html.toString();
	}

	private String maskHtmlChars(String text) {
		text = text.replaceAll("&", "&amp;");
		text = text.replaceAll("�", "&auml;");
		text = text.replaceAll("�", "&Auml;");
		text = text.replaceAll("�", "&ouml;");
		text = text.replaceAll("�", "&Ouml;");
		text = text.replaceAll("�", "&uuml;");
		text = text.replaceAll("�", "&Uuml;");
		text = text.replaceAll("�", "&szlig;");
		text = text.replaceAll("\"", "&quot;");
		text = text.replaceAll("<", "&lt");
		text = text.replaceAll(">", "&gt");
		text = text.replaceAll("�", "&euro;");
		return text;
	}

	protected String getTextForSelection(Cell[] selection) {
		StringBuffer text = new StringBuffer();
		Cell topLeft = sortSelectedCells(selection);
		IContentProvider model = agileGrid.getContentProvider();
		if (model == null)
			return "";

		int currentCol = topLeft.column;
		for (int i = 0; i < selection.length; i++) {
			for (; currentCol < selection[i].column; currentCol++)
				text.append(TAB);

			Object content = model.getContentAt(selection[i].row,
					selection[i].column);
			text.append(content.toString());

			if (i + 1 < selection.length) {
				for (int row = selection[i].row; row < selection[i + 1].row; row++)
					text.append(PlatformLineDelimiter);
				if (selection[i].row != selection[i + 1].row)
					currentCol = topLeft.column;
			}
		}
		return text.toString();
	}

	protected Cell sortSelectedCells(Cell[] selection) {
		Arrays.sort(selection, new Comparator<Cell>() {

			public int compare(Cell c1, Cell c2) {
				if (c1.row < c2.row)
					return -1;
				if (c1.row > c2.row)
					return +1;
				if (c1.column < c2.column)
					return -1;
				if (c1.column > c2.column)
					return +1;
				return 0;
			}

		});

		int minCol = selection[0].column;
		for (int i = 1; i < selection.length; i++)
			if (selection[i].column < minCol)
				minCol = selection[i].column;
		return new Cell(agileGrid, selection[0].row, minCol);
	}

	protected String getRTFForSelection(Cell[] selection) {
		return getTextForSelection(selection);
	}

	protected void registerActionUpdater() {
		agileGrid.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				updateActions();
			}
		});
	}

	protected void updateActions() {
		copyAction.updateEnabledState();
		copyAllAction.updateEnabledState();
		cutAction.updateEnabledState();
		pasteAction.updateEnabledState();
		selectAllAction.updateEnabledState();
	}
}
