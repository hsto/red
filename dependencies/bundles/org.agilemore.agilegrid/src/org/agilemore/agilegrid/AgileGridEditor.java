/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;

/**
 * This is a editor implementation of agile grid.
 * 
 * @author fourbroad
 * 
 */
public class AgileGridEditor {

	/**
	 * The cell editor as used for editing the value of cell.
	 */
	private CellEditor cellEditor;

	/**
	 * The current cell to be editing.
	 */
	private Cell cell;

	/**
	 * The activation event of agile grid editor.
	 */
	private EditorActivationEvent activationEvent;

	/**
	 * The provider of cell editor.
	 */
	private ICellEditorProvider cellEditorProvider;

	/**
	 * The cell editor listener which listen the activation event of agile grid
	 * editor.
	 */
	private ICellEditorListener cellEditorListener;

	/**
	 * The focus listener which listen the focus event of the cell editor
	 * control.
	 */
	private FocusListener focusListener;

	/**
	 * The mouse listener which listen the mouse event of the cell editor
	 * control.
	 */
	private MouseListener mouseListener;

	/**
	 * The agile grid that this agile grid editor is working for.
	 */
	private AgileGrid agileGrid;

	/**
	 * The traverse listener which listen the traverse event of the cell editor
	 * control.
	 */
	private TraverseListener traverseListener;

	/**
	 * The editor activation listeners which listen the editor activation event.
	 */
	private ArrayList<IEditorActivationListener> editorActivationListeners;

	/**
	 * The editor activation strategy which decide when the agile grid editor
	 * should be activated.
	 */
	private EditorActivationStrategy editorActivationStrategy;

	/**
	 * Tabbing from cell to cell is turned off
	 */
	public static final int DEFAULT = 1;

	/**
	 * Should if the end of the row is reach started from the start/end of the
	 * row below/above
	 */
	public static final int TABBING_MOVE_TO_ROW_NEIGHBOR = 1 << 1;

	/**
	 * Should if the end of the row is reach started from the beginning in the
	 * same row
	 */
	public static final int TABBING_CYCLE_IN_ROW = 1 << 2;

	/**
	 * Support tabbing to Cell above/below the current cell
	 */
	public static final int TABBING_VERTICAL = 1 << 3;

	/**
	 * Should tabbing from column to column with in one row be supported
	 */
	public static final int TABBING_HORIZONTAL = 1 << 4;

	/**
	 * Style mask used to enable keyboard activation
	 */
	public static final int KEYBOARD_ACTIVATION = 1 << 5;

	/**
	 * The bit mask controlling the editor.
	 */
	private int feature;

	/**
	 * Creates a new agile grid editor.
	 * 
	 * @param agile
	 *            grid The agile grid this editor is attached to
	 * @param editorActivationStrategy
	 *            The strategy used to decide about editor activation
	 * @param feature
	 *            bit mask controlling the editor
	 *            <ul>
	 *            <li>{@link AgileGridEditor#DEFAULT}</li>
	 *            <li>{@link AgileGridEditor#TABBING_CYCLE_IN_ROW}</li>
	 *            <li>{@link AgileGridEditor#TABBING_HORIZONTAL}</li>
	 *            <li>{@link AgileGridEditor#TABBING_MOVE_TO_ROW_NEIGHBOR}</li>
	 *            <li>{@link AgileGridEditor#TABBING_VERTICAL}</li>
	 *            </ul>
	 */
	public AgileGridEditor(AgileGrid agileGrid, int feature) {
		this.agileGrid = agileGrid;
		this.feature = feature;

		this.editorActivationStrategy = new EditorActivationStrategy(agileGrid);

		cellEditorListener = new ICellEditorListener() {
			public void editorValueChanged(boolean oldValidState,
					boolean newValidState) {
				// Ignore.
			}

			public void cancelEditor() {
				AgileGridEditor.this.cancelEditing();
			}

			public void applyEditorValue() {
				AgileGridEditor.this.applyEditorValue();
			}
		};

		hookListener(this.agileGrid);
	}

	/**
	 * Hooks listeners to the given agile grid.
	 * 
	 * @param agileGrid
	 *            The agile grid that listeners hook to.
	 */
	private void hookListener(AgileGrid agileGrid) {
		agileGrid.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				onMouseEvent(e,
						EditorActivationEvent.MOUSE_DOUBLE_CLICK_SELECTION);
			}

			@Override
			public void mouseDown(MouseEvent e) {
				onMouseEvent(e, EditorActivationEvent.MOUSE_CLICK_SELECTION);
			}
		});
	}

	/**
	 * Processes the mouse event, if needed, activates the cell editor.
	 * 
	 * @param event
	 *            The mouse event.
	 */
	protected void onMouseEvent(MouseEvent event, int eventType) {
		if (event.button == 1) {
			Cell cell = agileGrid.getCell(event.x, event.y);
			if (cell == Cell.NULLCELL) {
				return;
			}
			cell = agileGrid.getValidCell(cell.row, cell.column);
			handleEditorActivationEvent(new EditorActivationEvent(cell, null,
					event, eventType), null);
		}
	}

	/**
	 * Activates the cell editor.
	 */
	synchronized void activateCellEditor(Object hint) {
		if (cell == Cell.NULLCELL) {
			return;
		}

		if (canEdit(cell.row, cell.column)) {
			agileGrid.scrollToFocus(false);

			if (hint == null) {
				cellEditor = cellEditorProvider.getCellEditor(cell.row,
						cell.column);
			} else {
				cellEditor = cellEditorProvider.getCellEditor(cell.row,
						cell.column, hint);
			}

			if (cellEditor != null) {
				activationEvent.cellEditor = cellEditor;
				if (editorActivationListeners != null
						&& !editorActivationListeners.isEmpty()) {
					for (int i = 0; i < editorActivationListeners.size(); i++) {

						if (activationEvent.cancel) {
							// Avoid leaking
							this.cell = null;
							return;
						}

						editorActivationListeners.get(i).beforeEditorActivated(
								activationEvent);
					}
				}

				cellEditor.addCellEditorListener(cellEditorListener);
				cellEditorProvider.initializeCellEditorValue(cellEditor, cell);

				// Tricky flow of control here:
				// activate() can trigger callback to cellEditorListener which
				// will clear cellEditor
				// so must get control first, but must still call activate()
				// even if there is no control.
				final Control control = cellEditor.getControl();
				cellEditor.activate(activationEvent);
				if (control == null || cellEditor == null) {
					return;
				}

				if (cellEditor.dependsOnExternalFocusListener()) {
					if (focusListener == null) {
						focusListener = new FocusAdapter() {
							public void focusLost(FocusEvent e) {
								applyEditorValue();
							}
						};
					}
					control.addFocusListener(focusListener);
				}

				if (traverseListener == null) {
					traverseListener = new TraverseListener() {
						public void keyTraversed(TraverseEvent e) {
							if ((feature & DEFAULT) != DEFAULT) {
								processTraverseEvent(cell, e);
							}
						}
					};
				}

				control.addTraverseListener(traverseListener);

				if (editorActivationListeners != null
						&& !editorActivationListeners.isEmpty()) {
					for (int i = 0; i < editorActivationListeners.size(); i++) {
						editorActivationListeners.get(i).afterEditorActivated(
								activationEvent);
					}
				}
			}
		} else {
			// Avoid leaking
			this.cell = null;
		}
	}

	/**
	 * Applies the current value and deactivates the currently active cell
	 * editor.
	 */
	synchronized void applyEditorValue() {
		if (this.cellEditor == null || this.cell == Cell.NULLCELL)
			return;

		// Avoid indirect recursive call for applyEditorValue.
		CellEditor cellEditorTmp = this.cellEditor;
		this.cellEditor = null;

		// null out cell editor before calling save
		// in case save results in applyEditorValue being re-entered
		// see 1GAHI8Z: ITPUI:ALL - How to code event notification when
		// using cell editor ?
		EditorDeactivationEvent tmp = new EditorDeactivationEvent(cell,
				cellEditorTmp, true);
		if (editorActivationListeners != null) {
			for (int i = 0; i < editorActivationListeners.size(); i++) {
				editorActivationListeners.get(i).beforeEditorDeactivated(tmp);
			}
		}

		this.activationEvent = null;
		saveEditorValue(cellEditorTmp);

		cellEditorTmp.removeListener(cellEditorListener);

		Control control = cellEditorTmp.getControl();
		if (control != null) {
			if (mouseListener != null) {
				control.removeMouseListener(mouseListener);
				// Clear the instance not needed any more
				mouseListener = null;
			}
			if (focusListener != null) {
				control.removeFocusListener(focusListener);
			}

			if (traverseListener != null) {
				control.removeTraverseListener(traverseListener);
			}
		}

		cellEditorTmp.deactivate();
		if (editorActivationListeners != null) {
			for (int i = 0; i < editorActivationListeners.size(); i++) {
				editorActivationListeners.get(i).afterEditorDeactivated(tmp);
			}
		}
		cellEditorTmp.dispose();

		this.cell = null;
	}

	/**
	 * Cancel editing
	 */
	synchronized void cancelEditing() {
		if (this.cellEditor == null)
			return;

		// Avoid indirect recursive call for applyEditorValue.
		CellEditor cellEditorTmp = this.cellEditor;
		this.cellEditor = null;

		EditorDeactivationEvent tmp = new EditorDeactivationEvent(cell,
				cellEditorTmp, false);
		if (editorActivationListeners != null) {
			for (int i = 0; i < editorActivationListeners.size(); i++) {
				editorActivationListeners.get(i).beforeEditorDeactivated(tmp);
			}
		}

		// setEditor(null, null);
		cellEditorTmp.removeListener(cellEditorListener);

		Control control = cellEditorTmp.getControl();
		if (control != null) {
			if (mouseListener != null) {
				control.removeMouseListener(mouseListener);
				// Clear the instance not needed any more
				mouseListener = null;
			}
			if (focusListener != null) {
				control.removeFocusListener(focusListener);
			}

			if (traverseListener != null) {
				control.removeTraverseListener(traverseListener);
			}
		}

		cellEditorTmp.deactivate();
		if (editorActivationListeners != null) {
			for (int i = 0; i < editorActivationListeners.size(); i++) {
				editorActivationListeners.get(i).afterEditorDeactivated(tmp);
			}
		}
		cellEditorTmp.dispose();

		this.activationEvent = null;
		this.cell = null;
	}

	/**
	 * Decides if it should activate the agile grid editor.
	 * 
	 * @param event
	 *            The activation event of agile grid editor.
	 * @param hint
	 *            The hint used to select cell editor.
	 */
	void handleEditorActivationEvent(EditorActivationEvent event, Object hint) {
		if (editorActivationStrategy.isEditorActivationEvent(event)) {
			if (cellEditor != null) {
				applyEditorValue();
			}

			cell = (Cell) event.getSource();
			if (cell.row == -1 || cell.column == -1)
				return;
			cell = agileGrid.getValidCell(cell.row, cell.column);
			agileGrid.focusCell(cell);
			activationEvent = event;
			activateCellEditor(hint);
		}
	}

	/**
	 * Saves the value of given cell editor.
	 * 
	 * @param cellEditor
	 *            The cell editor.
	 */
	void saveEditorValue(CellEditor cellEditor) {
		if (cellEditorProvider != null) {
			cellEditorProvider.saveCellEditorValue(cellEditor, cell);
		}
	}

	/**
	 * Returns whether there is an active cell editor.
	 * 
	 * @return <code>true</code> if there is an active cell editor; otherwise
	 *         <code>false</code> is returned.
	 */
	boolean isCellEditorActive() {
		return cellEditor != null;
	}

	/**
	 * Adds the given listener, it is to be notified when the cell editor is
	 * activated or deactivated.
	 * 
	 * @param listener
	 *            the listener to add
	 */
	void addEditorActivationListener(IEditorActivationListener listener) {
		if (editorActivationListeners == null) {
			editorActivationListeners = new ArrayList<IEditorActivationListener>();
		}
		editorActivationListeners.add(listener);
	}

	/**
	 * Removes the given listener.
	 * 
	 * @param listener
	 *            the listener to remove
	 */
	void removeEditorActivationListener(IEditorActivationListener listener) {
		if (editorActivationListeners != null) {
			editorActivationListeners.remove(listener);
		}
	}

	/**
	 * Process the traverse event and opens the next available editor depending
	 * of the implemented strategy. The default implementation uses the style
	 * constants
	 * <ul>
	 * <li>{@link AgileGridEditor#TABBING_MOVE_TO_ROW_NEIGHBOR}</li>
	 * <li>{@link AgileGridEditor#TABBING_CYCLE_IN_ROW}</li>
	 * <li>{@link AgileGridEditor#TABBING_VERTICAL}</li>
	 * <li>{@link AgileGridEditor#TABBING_HORIZONTAL}</li>
	 * </ul>
	 * 
	 * <p>
	 * Subclasses may overwrite to implement their custom logic to edit the next
	 * cell
	 * </p>
	 * 
	 * @param columnIndex
	 *            the index of the current column
	 * @param row
	 *            the current row - may only be used for the duration of this
	 *            method call
	 * @param event
	 *            the traverse event
	 */
	protected void processTraverseEvent(Cell cell, TraverseEvent event) {

		Cell cell2edit = null;

		if (event.detail == SWT.TRAVERSE_TAB_PREVIOUS) {
			event.doit = false;

			if ((event.stateMask & SWT.CTRL) == SWT.CTRL
					&& (feature & TABBING_VERTICAL) == TABBING_VERTICAL) {
				cell2edit = searchCell(cell, AgileGrid.ABOVE);
			} else if ((feature & TABBING_HORIZONTAL) == TABBING_HORIZONTAL) {
				cell2edit = searchCell(cell, AgileGrid.LEFT);
			}
		} else if (event.detail == SWT.TRAVERSE_TAB_NEXT) {
			event.doit = false;

			if ((event.stateMask & SWT.CTRL) == SWT.CTRL
					&& (feature & TABBING_VERTICAL) == TABBING_VERTICAL) {
				cell2edit = searchCell(cell, AgileGrid.BELOW);
			} else if ((feature & TABBING_HORIZONTAL) == TABBING_HORIZONTAL) {
				cell2edit = searchCell(cell, AgileGrid.RIGHT);
			}
		}

		if (cell2edit != null) {

			agileGrid.setRedraw(false);
			agileGrid.focusCell(cell2edit);
			agileGrid.scrollToFocus(false);
			EditorActivationEvent acEvent = new EditorActivationEvent(
					cell2edit, null, event);
			agileGrid.triggerEditorActivationEvent(acEvent, null);
			agileGrid.setRedraw(true);
		}
	}

	/**
	 * Searches the cell according the indication of direction.
	 * 
	 * @param cell
	 *            The current cell as a original point.
	 * @param direction
	 *            The searching direction.
	 * @return The cell to be found.
	 */
	private Cell searchCell(Cell cell, int direction) {
		Cell searchedCell = null;
		Cell newCell = agileGrid.getNeighbor(cell, direction, true);
		if (newCell != null) {
			if (cellEditorProvider.canEdit(newCell.row, newCell.column)) {
				searchedCell = newCell;
			} else {
				searchedCell = searchCell(newCell, direction);
			}
		} else {
			if ((feature & TABBING_CYCLE_IN_ROW) == TABBING_CYCLE_IN_ROW) {
				if (direction == AgileGrid.RIGHT) {
					newCell = new Cell(agileGrid, cell.row, 0);
				} else if (direction == AgileGrid.LEFT) {
					newCell = new Cell(agileGrid, cell.row, agileGrid
							.getLayoutAdvisor().getColumnCount() - 1);
				}
			} else if ((feature & TABBING_MOVE_TO_ROW_NEIGHBOR) == TABBING_MOVE_TO_ROW_NEIGHBOR) {
				if (direction == AgileGrid.ABOVE) {
					newCell = new Cell(agileGrid, 0, cell.column);
				} else if (direction == AgileGrid.BELOW) {
					newCell = new Cell(agileGrid, agileGrid.getLayoutAdvisor()
							.getRowCount() - 1, cell.column);
				}
			}
			if (newCell != null
					&& !cellEditorProvider.canEdit(newCell.row, newCell.column)) {
				searchedCell = searchCell(newCell, direction);
			} else {
				searchedCell = newCell;
			}
		}

		return searchedCell;
	}

	/**
	 * Returns the agile grid that this agile grid editor is working for.
	 * 
	 * @return the agile grid that this agile grid editor is working for
	 */
	protected AgileGrid getAgileGrid() {
		return agileGrid;
	}

	/**
	 * Updates the cell editor bounds.
	 */
	void updateCellEditorBounds() {
		if (this.isCellEditorActive()) {
			if (!agileGrid.isCellVisible(cell.row, cell.column)) {
				Rectangle hide = new Rectangle(-101, -101, 100, 100);
				cellEditor.setBounds(hide);
			} else {
				cellEditor.updateBounds(cell);
			}
		}
	}

	/**
	 * Returns true if the given cell is active, false otherwise.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return true if the given cell is active, false otherwise.
	 */
	boolean isActiveCell(int row, int col) {
		if (this.isCellEditorActive()) {
			if (col == cell.column && row == cell.row)
				return true;
		}
		return false;
	}

	/**
	 * Sets the cell editor provider.
	 * 
	 * @param cellEditorProvider
	 *            The cell editor provider.
	 */
	void setCellEditorProvider(ICellEditorProvider cellEditorProvider) {
		this.cellEditorProvider = cellEditorProvider;
	}

	/**
	 * Returns the cell editor provider.
	 * 
	 * @return The cell editor provider.
	 */
	ICellEditorProvider getCellEditorProvider() {
		return this.cellEditorProvider;
	}

	/**
	 * Is the cell editable
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * 
	 * @return true if editable, false otherwise.
	 */
	boolean canEdit(int row, int col) {
		return !isCellEditorActive() && cellEditorProvider != null
				&& cellEditorProvider.canEdit(row, col);
	}

	/**
	 * Gets the activation strategy of editor.
	 * 
	 * @return the editorActivationStrategy
	 */
	EditorActivationStrategy getEditorActivationStrategy() {
		return this.editorActivationStrategy;
	}

	/**
	 * Sets the activation strategy of editor.
	 * 
	 * @param editorActivationStrategy
	 *            the editorActivationStrategy to set
	 */
	void setEditorActivationStrategy(
			EditorActivationStrategy editorActivationStrategy) {
		if (this.editorActivationStrategy != null) {
			this.editorActivationStrategy
					.setEnableEditorActivationWithKeyboard(false);
		}
		this.editorActivationStrategy = editorActivationStrategy;
	}

	/**
	 * Gets the feature of agile grid editor.
	 * 
	 * @return the feature
	 */
	int getFeature() {
		return this.feature;
	}

	/**
	 * Sets the feature of agile grid editor.
	 * 
	 * @param feature
	 *            the feature to set
	 */
	void setFeature(int feature) {
		this.feature = feature;
	}

}
