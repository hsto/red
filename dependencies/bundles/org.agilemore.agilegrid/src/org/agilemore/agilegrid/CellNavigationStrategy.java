/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;

/**
 * This class implement the strategy how the agile grid is navigated using the
 * keyboard.
 * 
 * <p>
 * <b>Subclasses can implement their custom navigation algorithms</b>
 * </p>
 */
public class CellNavigationStrategy implements ICellNavigationStrategy {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.ICellNavigationStrategy#isNavigationEvent(org
	 * .agilemore.agilegrid.AgileGrid, org.eclipse.swt.widgets.Event)
	 */
	public boolean isNavigationEvent(AgileGrid agileGrid, Event event) {
		switch (event.type) {
		case SWT.KeyDown:
			switch (event.keyCode) {
			case SWT.HOME:
			case SWT.END:
			case SWT.ARROW_UP:
			case SWT.ARROW_DOWN:
			case SWT.ARROW_LEFT:
			case SWT.ARROW_RIGHT:
			case SWT.PAGE_DOWN:
			case SWT.PAGE_UP:
			case SWT.TAB:
				return true;
			default:
				return false;
			}
		case SWT.MouseDown:
			Cell cell = agileGrid.getCell(event.x, event.y);
			if(cell.row >= 0 && cell.column >= 0)
				return true;
			else return false;
		case SWT.FocusIn:
			return true;
		default:
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.ICellNavigationStrategy#findSelectedCell(org.
	 * agilemore.agilegrid.AgileGrid, org.agilemore.agilegrid.Cell,
	 * org.eclipse.swt.widgets.Event)
	 */
	public Cell findSelectedCell(AgileGrid agileGrid, Cell currentCell,
			Event event) {

		Cell foundCell = Cell.NULLCELL;

		switch (event.type) {
		case SWT.KeyDown: {
			if (currentCell == Cell.NULLCELL) {
				currentCell = new Cell(agileGrid, 0, 0);
			}
			int newFocusRow = currentCell.row;
			int newFocusCol = currentCell.column;
			ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
			switch (event.keyCode) {
			case SWT.HOME:
				newFocusCol = 0;
				foundCell = new Cell(agileGrid, newFocusRow, newFocusCol);
				break;
			case SWT.END:
				newFocusCol = layoutAdvisor.getColumnCount() - 1;
				if (newFocusRow == -1)
					newFocusRow = 0;
				foundCell = new Cell(agileGrid, newFocusRow, newFocusCol);
				break;
			case SWT.ARROW_UP:
				foundCell = agileGrid.getNeighbor(currentCell, AgileGrid.ABOVE,
						true);
				break;
			case SWT.ARROW_DOWN:
				foundCell = agileGrid.getNeighbor(currentCell, AgileGrid.BELOW,
						true);
				break;
			case SWT.ARROW_LEFT:
				foundCell = agileGrid.getNeighbor(currentCell, AgileGrid.LEFT,
						true);
				break;
			case SWT.ARROW_RIGHT:
				foundCell = agileGrid.getNeighbor(currentCell, AgileGrid.RIGHT,
						true);
				break;
			case SWT.PAGE_DOWN:
				newFocusRow += agileGrid.getRowsFullyVisible() - 1;
				if (newFocusRow >= layoutAdvisor.getRowCount())
					newFocusRow = layoutAdvisor.getRowCount() - 1;
				if (newFocusCol == -1)
					newFocusCol = 0;
				foundCell = new Cell(agileGrid, newFocusRow, newFocusCol);
				break;
			case SWT.PAGE_UP:
				newFocusRow -= agileGrid.getRowsFullyVisible() - 1;
				if (currentCell.row == layoutAdvisor.getFixedRowCount()
						&& newFocusRow < 0)
					newFocusRow = 0;
				else if (newFocusRow < layoutAdvisor.getFixedRowCount()
						&& currentCell.row != 0)
					newFocusRow = layoutAdvisor.getFixedRowCount();
				if (newFocusCol == -1)
					newFocusCol = layoutAdvisor.getFixedColumnCount();
				foundCell = new Cell(agileGrid, newFocusRow, newFocusCol);
				break;
			case SWT.TAB:
				int direction = (event.stateMask & SWT.SHIFT) != 0 ? AgileGrid.LEFT
						: AgileGrid.RIGHT;
				foundCell = agileGrid.getNeighbor(currentCell, direction, true);
				break;
			}
		}
			break;
		case SWT.MouseDown:
			if (event.button == 1) {
				foundCell = agileGrid.getCell(event.x, event.y);
				if (foundCell.row == -1 || foundCell.column == -1) {
					foundCell = Cell.NULLCELL;
				}
			}
			break;
		case SWT.FocusIn:
			if (currentCell == Cell.NULLCELL) {
				// Gets the initial focus cell when agile grid is created.
				foundCell = new Cell(agileGrid, 0, 0);
			} else {
				foundCell = currentCell;
			}
		}
		return foundCell;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.ICellNavigationStrategy#shouldCancelEvent(org
	 * .agilemore.agilegrid.AgileGrid, org.eclipse.swt.widgets.Event)
	 */
	public boolean shouldCancelEvent(AgileGrid agileGrid, Event event) {
		switch (event.type) {
		case SWT.KeyDown:
			switch (event.keyCode) {
			case SWT.HOME:
			case SWT.END:
			case SWT.ARROW_UP:
			case SWT.ARROW_DOWN:
			case SWT.ARROW_LEFT:
			case SWT.ARROW_RIGHT:
			case SWT.PAGE_DOWN:
			case SWT.PAGE_UP:
			case SWT.TAB:
				return true;
			default:
				return false;
			}
		case SWT.MouseDown:
			return true;
		case SWT.FocusIn:
			return true;
		default:
			return false;
		}
	}
}
