/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import org.eclipse.swt.graphics.Rectangle;

/**
 * Provides a layout advisor which can adjust the column widths automatically
 * according the width of the agile grid client area.
 * 
 * @author fourbroad
 * 
 */
public class ScalableColumnLayoutAdvisor extends DefaultLayoutAdvisor {

	/**
	 * The factor used to calculate the values of the current control width that
	 * are stored.
	 * 
	 */
	private int FACTOR = 10000;

	/**
	 * Creates new layout advisor with no horizontal scroll function.
	 * 
	 * @param agileGrid
	 *            The agile grid this layout advisor works for.
	 */
	public ScalableColumnLayoutAdvisor(AgileGrid agileGrid) {
		super(agileGrid);
		initialize();
	}

	/**
	 * Initialize the column width.
	 */
	public void initialize() {
		int weightSum = 1;
		for (int i = 0; i < getColumnCount(); i++) {
			int initialWeight = getInitialColumnWidth(i);
			weightSum += initialWeight;
		}

		// initialize with % values.
		for (int i = 0; i < getColumnCount(); i++) {
			super
					.setColumnWidth(
							i,
							(int) ((getInitialColumnWidth(i) / (double) weightSum) * FACTOR) - 1);
		}

		int pts = 0;
		for (int i = 0; i < getColumnCount(); i++)
			pts += super.getColumnWidth(i);
		FACTOR = pts;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.AbstractLayoutAdvisor#getColumnWidth(int)
	 */
	public int getColumnWidth(int col) {
		ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
		Rectangle clientArea = agileGrid.getClientArea();
		int width = clientArea.width - agileGrid.getLinePixels()
				* (getColumnCount() + 1);
		if (layoutAdvisor.isLeftHeaderVisible()) {
			width -= layoutAdvisor.getLeftHeaderWidth();
			width -= agileGrid.getLinePixels();
		}

		double percent = super.getColumnWidth(col) / ((double) FACTOR);
		if (agileGrid != null && !agileGrid.isDisposed()) {
			return (int) (width * percent);
		} else
			return (int) Math.round(percent * 100);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.IContentProvider#setColumnWidth(int, int)
	 */
	public void setColumnWidth(int col, int value) {
		int width = agileGrid.getClientArea().width;
		double percent = (value + 1) / (double) width;
		if (col == getColumnCount() - 1) {
			int weightsum = super.getColumnWidth(col)
					+ super.getColumnWidth(col - 1);
			super.setColumnWidth(col, (int) (percent * FACTOR));
			super.setColumnWidth(col - 1, weightsum
					- ((int) (percent * FACTOR)));
		} else {
			int weightsum = super.getColumnWidth(col)
					+ super.getColumnWidth(col + 1);
			super.setColumnWidth(col, (int) (percent * FACTOR));
			super.setColumnWidth(col + 1, weightsum
					- ((int) (percent * FACTOR)));
		}
	}
}
