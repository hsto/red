/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */
package org.agilemore.agilegrid;

import java.util.Hashtable;

/**
 * This adapter class provides default implementations for the methods described
 * by the <code>AbstractLayoutAdvisor</code> class.
 * <p>
 * If client want to customize the layout of agile grid, who can extend this
 * class and override only the methods that they are interested in and use
 * <code>AgileGrid.setLayoutAdvisor</code> to change the default layout behavior
 * of agile grid.
 * </p>
 * 
 * @author fourbroad
 * 
 */
public class DefaultLayoutAdvisor extends AbstractLayoutAdvisor {

	/**
	 * The row heights used when painting the agile grid.
	 */
	private Hashtable<Integer, Integer> rowHeights = new Hashtable<Integer, Integer>();

	/**
	 * The column widths used when painting the agile grid.
	 */
	protected Hashtable<Integer, Integer> colWidths = new Hashtable<Integer, Integer>();

	protected int leftHeaderWidth;
	protected int topHeaderHeight;

	/**
	 * Creates a new default layout advisor.
	 * 
	 * @param agileGrid
	 *            The agile grid this layout advisor works for.
	 */
	public DefaultLayoutAdvisor(AgileGrid agileGrid) {
		super(agileGrid);
		leftHeaderWidth = 35;
		topHeaderHeight = 18;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.AbstractLayoutAdvisor#getColumnWidth(int)
	 */
	@Override
	public int getColumnWidth(int col) {
		Integer width = (Integer) colWidths.get(new Integer(col));
		if (width == null) {
			int initWidth = getInitialColumnWidth(col);
			if (initWidth < 0)
				return 0;
			return initWidth;
		}
		return width.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetColumnWidth(int,
	 * int)
	 */
	@Override
	protected void doSetColumnWidth(int col, int width) {
		if (width < 0)
			width = 0;
		colWidths.put(new Integer(col), new Integer(width));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetRowHeight(int,
	 * int)
	 */
	@Override
	protected void doSetRowHeight(int row, int height) {
		if (height < 0)
			height = 0;
		rowHeights.put(new Integer(row), new Integer(height));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.AbstractLayoutAdvisor#getRowHeight(int)
	 */
	@Override
	public int getRowHeight(int row) {
		Integer height = (Integer) rowHeights.get(new Integer(row));
		if (height == null) {
			if (row == 0) {
				int h = getInitialRowHeight(0);
				if (h > 2)
					return h;
			}
			int initialH = getInitialRowHeight(row);
			if (initialH < 2)
				return 2;
			return initialH;
		}
		if (height.intValue() < 2)
			return 2;
		return height.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.AbstractLayoutAdvisor#getInitialColumnWidth(int)
	 */
	@Override
	public int getInitialColumnWidth(int column) {
		return 80;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.AbstractLayoutAdvisor#getInitialRowHeight(int)
	 */
	@Override
	public int getInitialRowHeight(int row) {
		return 18;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#getFixedColumnCount()
	 */
	public int getFixedColumnCount() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#getFixedRowCount()
	 */
	public int getFixedRowCount() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#getRowHeightMinimum()
	 */
	@Override
	public int getRowHeightMinimum() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#isColumnResizable(int)
	 */
	@Override
	public boolean isColumnResizable(int col) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#isRowResizable(int)
	 */
	@Override
	public boolean isRowResizable(int row) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return 4;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return 6;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#getHeaderColumnWidth()
	 */
	@Override
	public int getLeftHeaderWidth() {
		return this.leftHeaderWidth;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#getHeaderRowHeight()
	 */
	@Override
	public int getTopHeaderHeight() {
		return this.topHeaderHeight;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#isHeaderColumnVisible()
	 */
	@Override
	public boolean isLeftHeaderVisible() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#isHeaderRowVisible()
	 */
	@Override
	public boolean isTopHeaderVisible() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#getHeaderColumnLabel(int)
	 */
	@Override
	public String getLeftHeaderLabel(int row) {
		return Integer.toString(row);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#getHeaderRowLabel(int)
	 */
	@Override
	public String getTopHeaderLabel(int col) {
		String result = "";
		for (; col >= 0; col = col / 26 - 1) {
			result = (char) ((char) (col % 26) + 'A') + result;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetColumnCount(int)
	 */
	@Override
	protected void doSetColumnCount(int columnCount) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetFixedColumnCount(int)
	 */
	@Override
	protected void doSetFixedColumnCount(int fixedColumnCount) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetFixedRowCount(int)
	 */
	@Override
	protected void doSetFixedRowCount(int fixedRowCount) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetLeftHeaderLabel(int,
	 * java.lang.String)
	 */
	@Override
	protected void doSetLeftHeaderLabel(int row, String label) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetLeftHeaderVisible(
	 * boolean)
	 */
	@Override
	protected void doSetLeftHeaderVisible(boolean visible) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetLeftHeaderWidth(int)
	 */
	@Override
	protected void doSetLeftHeaderWidth(int width) {
		this.leftHeaderWidth = width;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetRowCount(int)
	 */
	@Override
	protected void doSetRowCount(int rowCount) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetRowHeightMinimum(int)
	 */
	@Override
	protected void doSetRowHeightMinimum(int minimumHeight) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetRowResizable(int,
	 * boolean)
	 */
	@Override
	protected void doSetRowResizable(int row, boolean resizable) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetTooltipAt(int,
	 * int, java.lang.String)
	 */
	@Override
	protected void doSetTooltipAt(int row, int col, String tooltip) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetTopHeaderHeight(int)
	 */
	@Override
	protected void doSetTopHeaderHeight(int height) {
		this.topHeaderHeight = height;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetTopHeaderLabel(int,
	 * java.lang.String)
	 */
	@Override
	protected void doSetTopHeaderLabel(int col, String label) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.agilemore.agilegrid.AbstractLayoutAdvisor#doSetTopHeaderVisible(boolean
	 * )
	 */
	@Override
	protected void doSetTopHeaderVisible(boolean visible) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#getTooltip(int, int)
	 */
	@Override
	public String getTooltip(int row, int col) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setColumnResizable(int,
	 * boolean)
	 */
	@Override
	public void setColumnResizable(int col, boolean resizable) {

	}

}
