/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Shell;

/**
 * Agile Grid is a custom drawn widget for SWT GUIs.
 * <p>
 * The idea of agile grid is to have a flexible grid of cells to display data in
 * it. This class focuses on displaying data but not on collecting the data to
 * display. The latter is done by the <code>IContentProvider</code> which has to
 * be implemented for each specific case. Some default tasks are done by a base
 * implementation called <code>DefaultContentProvider</code>. The agile grid
 * asks the <code>ILayoutAdvisor<code> for the amount of columns and rows, the 
 * sizes of columns and rows which are currently drawn. Even if the agile grid has 
 * a million rows, it won't get slower because it only requests those cells 
 * it currently draws. Only a bad layout advisor can influence the drawing speed 
 * negatively.
 * <p>
 * When drawing a cell, the agile grid calls a <code>ICellRenderer</code> to do this work. The
 * <code>ICellRendererProvider</code> determines which cell renderer is used for
 * which cell. A default renderer is available (
 * <code>ICellRendererProvider.defaultRenderer</code>), but the creation of
 * self-written renderers for specific purposes is assumed. Some default
 * renderers are available in the package
 * <code>org.peertoo.agilegrid.cellrenderers.*</code>.
 * <p>
 * Each column of agile grid can have an individual size while the rows are all
 * of the same height except the first row. Multiple column and row headers are
 * possible. These "fixed" cells will not be scrolled out of sight. The column
 * and row count always starts in the upper left corner with 0, independent of
 * the number of column headers or row headers.
 * <p>
 * It is also possible to span cells over several rows and/or columns. The agile
 * grid asks the ILayoutAdvisor to provide this information via
 * <code>mergeInto(row, col)</code>. This method must return the cell the given
 * cell should be merged with.
 * <p>
 * Changing of content values is possible by implementations of
 * <code>CellEditor</code>. Again the agile grid asks the
 * <code>ICellEditorProvider</code> to provide an instance of CellEditor. Note
 * that there are multiple cell editors available in the package
 * <code>org.peertoo.agilegrid.editors</code>!
 * 
 * Look also into <code>ICompositorStrategy</code> that provides a transparent
 * sorting of cells.
 * 
 * @see org.agilemore.agilegrid.IContentProvider
 * @see org.agilemore.agilegrid.AbstractContentProvider
 * @see org.agilemore.agilegrid.ScalableColumnLayoutAdvisor
 * @see org.agilemore.agilegrid.ICellRenderer
 * @see org.agilemore.agilegrid.CellEditor
 * 
 */
public class AgileGrid extends Canvas {
	/**
	 * Constant denoting the cell above current one (value is 1).
	 */
	public static final int ABOVE = 1;

	/**
	 * Constant denoting the cell below current one (value is 2).
	 */
	public static final int BELOW = 1 << 1;

	/**
	 * Constant denoting the cell to the left of the current one (value is 4).
	 */
	public static final int LEFT = 1 << 2;

	/**
	 * Constant denoting the cell to the right of the current one (value is 8).
	 */
	public static final int RIGHT = 1 << 3;

	/**
	 * Data editing facilities:
	 */
	private AgileGridEditor agileGridEditor;

	/**
	 * Manages the selection of agile grid.
	 */
	private CellSelectionManager cellSelectionManager;

	/**
	 * Provides the renderer for agile grid cell.
	 */
	private ICellRendererProvider cellRendererProvider;

	/**
	 * Provides layout advice.
	 */
	private ILayoutAdvisor layoutAdvisor;

	/**
	 * Provides the content of agile grid cell.
	 */
	private IContentProvider contentProvider;

	/**
	 * Current top row and left column which can be visible and scrollable.
	 */
	protected int topRow;
	protected int leftColumn;

	/**
	 * The cell that the mouse left button is pressed on.
	 */
	private Cell pressedCell;
	private boolean isCaptured;
	private Point mousePoint;

	private int style = SWT.NONE;

	/**
	 * row and column measures.
	 */
	private int rowsVisible;
	private int rowsFullyVisible;
	private int columnsVisible;
	private int columnsFullyVisible;

	/**
	 * The pixel number of grid line.
	 */
	private int linePixels = 1;

	/**
	 * The row and column sizes.
	 */
	private int resizeRowIndex;
	private int resizeRowTop;
	private int resizeColumnIndex;
	private int resizeColumnLeft;

	/**
	 * resize area.
	 */
	private int resizeAreaSize = 4;

	/**
	 * The double click listeners of agile grid cell.
	 */
	private ArrayList<ICellDoubleClickListener> cellDoubleClickListeners;

	/**
	 * The cell resize listeners of agile grid cell.
	 */
	private ArrayList<ICellResizeListener> cellResizeListeners;

	/**
	 * Modify the internal status of agile grid if the layout advisor and
	 * content provider have some change.
	 */
	private PropertyChangeListener propertyChangeListener;

	/**
	 * Cursors to be shown.
	 */
	private Point defaultCursorSize;
	private Cursor defaultCursor;
	private Cursor defaultRowResizeCursor;
	private Cursor defaultColumnResizeCursor;
	private Cursor rowResizeCursor;
	private Cursor columnResizeCursor;

	/**
	 * Native tooltip.
	 */
	private String nativeTooltip;

	/**
	 * Creates a new agile grid.
	 * 
	 * possible styles:
	 * <ul>
	 * <li><b>SWT.V_SCROLL</b> - show vertical scrollbar and allow vertical
	 * scrolling by arrow keys</li> <li><b>SWT.H_SCROLL</b> - show horizontal
	 * scrollbar and allow horizontal scrolling by arrow keys</li> <li>
	 * <b>SWTX.AUTO_SCROLL</b> - Dynamically shows vertical and horizontal
	 * scrollbars when they are necessary.</li> <li>
	 * <b>SWTX.FILL_WITH_LASTCOL</b> - Makes the agile grid enlarge the last
	 * column to always fill all space.</li> <li><b>SWTX.FILL_WITH_DUMMYCOL</b>
	 * - Makes the agile grid fill any remaining space with dummy columns to
	 * fill all space. </li> <li><b>SWT.FLAT</b> - Does not paint a dark outer
	 * border line.</li> <li><b>SWT.MULTI</b> - Sets the "Multi Selection Mode".
	 * In this mode, more than one cell can be selected. The user can achieve
	 * this by shift-click and ctrl-click. The selected cells/rows can be
	 * scattored ofer the complete agile grid. If you pass false, only a single
	 * cell can be selected. </li> <li><b>SWTX.EDIT_ON_KEY</b> - Activates a
	 * possibly present cell editor on every keystroke. (Default: only ENTER).
	 * However, note that editors can specify which events they accept.</li> 
	 * <li> <b>SWTX.MARK_SELECTION_HEADERS</b> - Makes agile grid draw left and
	 * top header cells in a different style when the focused cell is in their
	 * row/column. This mimics the MS Excel behavior that helps find the
	 * currently selected cell(s).</li> <li><b>SWT.HIDE_SELECTION</b> - Hides
	 * the selected cells when the agile grid looses focus.</li> <li>
	 * <b>SWTX.ROW_SELECTION</b> - Selects the whole row when clicking the row
	 * by mouse or navigating by keyboard.</li> <li>
	 * <b>SWTX.COLUMN_SELECTION</b> - Selects the whole column when clicking the
	 * column by mouse or navigating by keyboard.</li>
	 * </ul>
	 * <p>
	 */
	public AgileGrid(Composite parent, int style) {
		// Initialize canvas to draw on.
		super(parent, SWT.NO_BACKGROUND | SWT.NO_REDRAW_RESIZE | style);

		this.style = style;

		topRow = 0;
		leftColumn = 0;
		rowsVisible = 0;
		rowsFullyVisible = 0;
		columnsVisible = 0;
		columnsFullyVisible = 0;
		resizeColumnIndex = Integer.MIN_VALUE;
		resizeRowIndex = Integer.MIN_VALUE;
		resizeRowTop = Integer.MIN_VALUE;
		resizeColumnLeft = Integer.MIN_VALUE;

		pressedCell = Cell.NULLCELL;
		isCaptured = false;
		mousePoint = null;

		cellSelectionManager = createFocusCellManager();
		cellRendererProvider = createCellRendererProvider();
		agileGridEditor = createAgileGridEditor();

		propertyChangeListener = new PropertyChangeListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				String propertyName = evt.getPropertyName();
				if (propertyName == ILayoutAdvisor.ColumnCount) {
					int columnCount = ((Integer) evt.getNewValue()).intValue();
					Cell focusCell = AgileGrid.this.getFocusCell();
					if (focusCell != null && focusCell.column >= columnCount) {
						AgileGrid.this.focusCell(new Cell(AgileGrid.this,
								focusCell.row, columnCount - 1));
					}
					redraw();
				} else if (propertyName == ILayoutAdvisor.ColumnWidth) {
					Map<Integer, Object> map = (Map<Integer, Object>) evt
							.getNewValue();
					Iterator<Integer> keyIter = map.keySet().iterator();
					int columnIndex = Integer.MIN_VALUE;
					while (keyIter.hasNext()) {
						columnIndex = keyIter.next().intValue();
						if (columnIndex >= leftColumn
								&& columnIndex < leftColumn + columnsVisible) {
							redraw();
							break;
						}
					}
				} else if (propertyName == ILayoutAdvisor.LeftHeaderLabel) {
					Map<Integer, Object> map = (Map<Integer, Object>) evt
							.getNewValue();
					Iterator<Integer> keyIter = map.keySet().iterator();
					int rowIndex = Integer.MIN_VALUE;
					while (keyIter.hasNext()) {
						rowIndex = keyIter.next().intValue();
						if (rowIndex >= topRow
								&& rowIndex < topRow + rowsVisible) {
							Rectangle rect = getLeftHeaderArea();
							redraw(rect.x, rect.y, rect.width, rect.height,
									true);
							break;
						}
					}
				} else if (propertyName == ILayoutAdvisor.LeftHeaderVisible) {
					redraw();
				} else if (propertyName == ILayoutAdvisor.LeftHeaderWidth) {
					redraw();
				} else if (propertyName == ILayoutAdvisor.RowCount) {
					int rowCount = ((Integer) evt.getNewValue()).intValue();
					Cell focusCell = AgileGrid.this.getFocusCell();
					if (focusCell != null && focusCell.row >= rowCount) {
						AgileGrid.this.focusCell(new Cell(AgileGrid.this,
								rowCount - 1, focusCell.column));
					}
					redraw();
				} else if (propertyName == ILayoutAdvisor.RowHeight) {
					Map<Integer, Object> map = (Map<Integer, Object>) evt
							.getNewValue();
					Iterator<Integer> keyIter = map.keySet().iterator();
					int rowIndex = Integer.MIN_VALUE;
					while (keyIter.hasNext()) {
						rowIndex = keyIter.next().intValue();
						if (rowIndex >= topRow
								&& rowIndex < topRow + rowsVisible) {
							redraw();
							break;
						}
					}
				} else if (propertyName == ILayoutAdvisor.TopHeaderHeight) {
					redraw();
				} else if (propertyName == ILayoutAdvisor.TopHeaderLabel) {
					Map<Integer, Object> map = (Map<Integer, Object>) evt
							.getNewValue();
					Iterator<Integer> keyIter = map.keySet().iterator();
					int columnIndex = Integer.MIN_VALUE;
					while (keyIter.hasNext()) {
						columnIndex = keyIter.next().intValue();
						if (columnIndex >= leftColumn
								&& columnIndex < leftColumn + columnsVisible) {
							Rectangle rect = getTopHeaderArea();
							redraw(rect.x, rect.y, rect.width, rect.height,
									true);
							break;
						}
					}
				} else if (propertyName == ILayoutAdvisor.TopHeaderVisible) {
					redraw();
				} else if (propertyName == IContentProvider.Content) {
					Map<Cell, Object> map = (Map<Cell, Object>) evt
							.getNewValue();
					Iterator<Cell> keyIter = map.keySet().iterator();
					List<Cell> cellList = new ArrayList<Cell>();
					while (keyIter.hasNext()) {
						cellList.add(keyIter.next());
					}
					if (cellList.size() > 0) {
						redrawCells(cellList.toArray(new Cell[] {}));
					}
				}
			}
		};

		layoutAdvisor = createLayoutAdvisor();
		layoutAdvisor.addPropertyChangeListener(propertyChangeListener);
		contentProvider = createContentProvider();
		contentProvider.addPropertyChangeListener(propertyChangeListener);

		cellDoubleClickListeners = new ArrayList<ICellDoubleClickListener>(7);
		cellResizeListeners = new ArrayList<ICellResizeListener>(7);

		// Listener creation
		hookListeners();

		// handle tooltip initialization:
		nativeTooltip = super.getToolTipText();
		super.setToolTipText("");

		Display display = getDisplay();
		this.defaultRowResizeCursor = new Cursor(display, SWT.CURSOR_SIZENS);
		this.defaultColumnResizeCursor = new Cursor(display, SWT.CURSOR_SIZEWE);

		// apply various style bits:
		if ((this.style & SWTX.AUTO_SCROLL) == SWTX.AUTO_SCROLL) {
			addListener(SWT.Resize, new Listener() {
				public void handleEvent(Event event) {
					updateScrollbarVisibility();
				}
			});
			addCellResizeListener(new ICellResizeListener() {
				public void rowResized(int row, int newHeight) {
					updateScrollbarVisibility();
				}

				public void columnResized(int col, int newWidth) {
					updateScrollbarVisibility();
				}
			});
		}
	}

	/**
	 * Disposes of the resources associated with this agile grid.
	 * 
	 * @see org.eclipse.swt.widgets.Widget#dispose()
	 */
	public void dispose() {
		checkWidget();

		if (this.defaultCursor != null) {
			this.defaultCursor.dispose();
		}

		if (this.defaultRowResizeCursor != null) {
			this.defaultRowResizeCursor.dispose();
		}

		if (this.defaultColumnResizeCursor != null) {
			this.defaultColumnResizeCursor.dispose();
		}

		if (this.rowResizeCursor != null) {
			this.rowResizeCursor.dispose();
		}

		if (this.columnResizeCursor != null) {
			this.columnResizeCursor.dispose();
		}

		SWTResourceManager.dispose();

		super.dispose();
	}

	/**
	 * Hook various SWT event listeners for agile grid.
	 */
	protected void hookListeners() {
		addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent event) {
				onPaint(event);
			}
		});

		addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				redraw();
			}
		});

		addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				onMouseDown(e);
			}

			public void mouseUp(MouseEvent e) {
				onMouseUp(e);
			}

			public void mouseDoubleClick(MouseEvent e) {
				onMouseDoubleClick(e);
			}

		});

		addMouseMoveListener(new MouseMoveListener() {
			public void mouseMove(MouseEvent e) {
				onMouseMove(e);
			}
		});

		addFocusListener(new FocusListener() {

			private Cell[] oldSelection;

			public void focusGained(FocusEvent e) {
				if (!isShowSelectionWithoutFocus() && oldSelection != null) {
					cellSelectionManager.selectCells(oldSelection);
					redrawCells(oldSelection);
					oldSelection = null;
				}
			}

			public void focusLost(FocusEvent e) {
				if (!isShowSelectionWithoutFocus()) {
					oldSelection = cellSelectionManager.getCellSelection();
					cellSelectionManager.clearSelection();
					if (oldSelection != null)
						redrawCells(oldSelection);
				}
			}

		});

		TooltipListener tooltipListener = new TooltipListener();
		addListener(SWT.Dispose, tooltipListener);
		addListener(SWT.KeyDown, tooltipListener);
		addListener(SWT.MouseDown, tooltipListener);
		addListener(SWT.MouseDoubleClick, tooltipListener);
		addListener(SWT.MouseMove, tooltipListener);
		addListener(SWT.MouseHover, tooltipListener);
		addListener(SWT.MouseExit, tooltipListener);

		final ScrollBar verticalBar = getVerticalBar();
		if (verticalBar != null) {
			verticalBar.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					int oldTopRow = topRow;
					topRow = verticalBar.getSelection();
					int totalRowCount = layoutAdvisor.getRowCount();
					if (topRow + rowsFullyVisible - 1 >= totalRowCount) {
						topRow = totalRowCount - columnsFullyVisible;
					}
					if (oldTopRow != topRow) {
						// int y = getRowTop(topRow);
						// Rectangle rect = getClientArea();
						// rect.height = rect.y + rect.height - y;
						// rect.y = y;
						// redraw(rect.x, rect.y, rect.width, rect.height,
						// true);
						redraw();
					}
				}
			});
			verticalBar.addListener(SWT.Selection, tooltipListener);
		}

		final ScrollBar horizontalBar = getHorizontalBar();
		if (horizontalBar != null) {
			horizontalBar.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					int oldLeftCol = leftColumn;
					leftColumn = horizontalBar.getSelection();
					int totalColumnCount = layoutAdvisor.getColumnCount();
					if (leftColumn + columnsFullyVisible - 1 >= totalColumnCount) {
						leftColumn = totalColumnCount - columnsFullyVisible;
					}

					if (oldLeftCol != leftColumn) {
						// int x = getColumnLeft(leftColumn);
						// Rectangle rect = getClientArea();
						// rect.width = rect.x + rect.width - x;
						// rect.x = x;
						// redraw(rect.x, rect.y, rect.width, rect.height,
						// true);
						redraw();
					}
				}
			});
			horizontalBar.addListener(SWT.Selection, tooltipListener);
		}
	}

	/**
	 * Calculates the content area which is located in the scrollable area.
	 * 
	 * @return The content area.
	 */
	private Rectangle getContentArea() {
		Rectangle clientArea = getClientArea();
		Rectangle rightMargin = getRightMargin();
		Rectangle bottomMargin = getBottomMargin();

		if (rightMargin.width > 0) {
			clientArea.width -= rightMargin.width;
		}
		if (bottomMargin.height > 0) {
			clientArea.height -= bottomMargin.height;
		}

		int x = getFixedWidth();
		int y = getFixedHeight();
		int width = clientArea.x + clientArea.width - x;
		int height = clientArea.x + clientArea.height - y;

		return new Rectangle(x, y, width, height);
	}

	/**
	 * Calculates the top header area which is located in the top area of agile
	 * grid.
	 * 
	 * @return The top header area.
	 */
	private Rectangle getTopHeaderArea() {
		Rectangle clientArea = getClientArea();
		Rectangle rightMargin = getRightMargin();
		if (rightMargin.width > 0) {
			clientArea.width -= rightMargin.width;
		}
		int x = linePixels;
		if (layoutAdvisor.isLeftHeaderVisible()) {
			x += layoutAdvisor.getLeftHeaderWidth();
			x += linePixels;
		}
		int y = linePixels;
		int width = clientArea.x + clientArea.width - x;
		int height = layoutAdvisor.getTopHeaderHeight();

		width += linePixels;
		height += linePixels;
		return new Rectangle(x, y, width, height);
	}

	/**
	 * Calculates the left header area which is located in the left area of
	 * agile grid.
	 * 
	 * @return The top header area.
	 */
	private Rectangle getLeftHeaderArea() {
		Rectangle clientArea = getClientArea();
		Rectangle bottomMargin = getBottomMargin();
		if (bottomMargin.height > 0) {
			clientArea.height -= bottomMargin.height;
		}

		int x = linePixels;
		int y = linePixels;
		if (layoutAdvisor.isTopHeaderVisible()) {
			y += layoutAdvisor.getTopHeaderHeight();
			y += linePixels;
		}
		int width = layoutAdvisor.getLeftHeaderWidth();
		int height = clientArea.y + clientArea.height - y;

		width += linePixels;
		height += linePixels;

		return new Rectangle(x, y, width, height);
	}

	private Rectangle getFixedRowScrollableArea() {
		if (layoutAdvisor.getFixedRowCount() <= 0) {
			return new Rectangle(0, 0, 0, 0);
		}

		Rectangle clientArea = getClientArea();
		Rectangle rightMargin = getRightMargin();
		if (rightMargin.width > 0) {
			clientArea.width -= rightMargin.width;
		}

		int x = linePixels;
		if (layoutAdvisor.isLeftHeaderVisible()) {
			x += layoutAdvisor.getLeftHeaderWidth();
			x += linePixels;
		}

		for (int i = 0; i < layoutAdvisor.getFixedColumnCount(); i++) {
			x += getColumnWidth(i);
			x += linePixels;
		}

		int y = linePixels;
		if (layoutAdvisor.isTopHeaderVisible()) {
			y += layoutAdvisor.getTopHeaderHeight();
			y += linePixels;
		}

		int width = clientArea.x + clientArea.width - x;

		int height = 0;
		for (int i = 0; i < layoutAdvisor.getFixedRowCount(); i++) {
			height += layoutAdvisor.getRowHeight(i);
			height += linePixels;
		}

		return new Rectangle(x, y, width, height);
	}

	private Rectangle getFixedColumnScrollableArea() {
		if (layoutAdvisor.getFixedColumnCount() <= 0) {
			return new Rectangle(0, 0, 0, 0);
		}

		Rectangle clientArea = getClientArea();
		Rectangle bottomMargin = getBottomMargin();
		if (bottomMargin.width > 0) {
			clientArea.height -= bottomMargin.height;
		}

		int y = linePixels;
		if (layoutAdvisor.isTopHeaderVisible()) {
			y += layoutAdvisor.getTopHeaderHeight();
			y += linePixels;
		}

		for (int i = 0; i < layoutAdvisor.getFixedRowCount(); i++) {
			y += layoutAdvisor.getRowHeight(i);
			y += linePixels;
		}

		int x = linePixels;
		if (layoutAdvisor.isLeftHeaderVisible()) {
			x += layoutAdvisor.getLeftHeaderWidth();
			x += linePixels;
		}

		int height = clientArea.y + clientArea.height - y;

		int width = 0;
		for (int i = 0; i < layoutAdvisor.getFixedColumnCount(); i++) {
			width += getColumnWidth(i);
			width += linePixels;
		}

		return new Rectangle(x, y, width, height);
	}

	/**
	 * Calculates the right margin of client area of the agile grid which has no
	 * cell to be drawn.
	 * 
	 * @return The right margin of client area of the agile grid.
	 */
	private Rectangle getRightMargin() {
		int lastCol = layoutAdvisor.getColumnCount() - 1;
		if (leftColumn + columnsFullyVisible - 1 == lastCol) {
			Rectangle clientArea = getClientArea();
			int x = getColumnRight(lastCol);
			if (x + 1 + linePixels >= clientArea.x + clientArea.width) {
				return new Rectangle(0, 0, -1, -1);
			}
			int y = 0;
			int width = clientArea.x + clientArea.width - 1 - linePixels - x;
			int height = clientArea.height;
			return new Rectangle(x, y, width, height);
		}
		return new Rectangle(0, 0, -1, -1);
	}

	/**
	 * Calculates the bottom margin of client area of the agile grid which has
	 * no cell to be drawn.
	 * 
	 * @return The bottom margin of client area of the agile grid.
	 */
	private Rectangle getBottomMargin() {
		int lastRow = layoutAdvisor.getRowCount() - 1;
		if (topRow + rowsFullyVisible - 1 == lastRow) {
			Rectangle clientArea = getClientArea();
			int y = getRowBottom(lastRow);
			if (y + 1 + linePixels >= clientArea.y + clientArea.height - 1) {
				return new Rectangle(0, 0, -1, -1);
			}
			int x = 0;
			int width = clientArea.width;
			int height = clientArea.y + clientArea.height - y - 1 - linePixels;
			return new Rectangle(x, y, width, height);
		}
		return new Rectangle(0, 0, -1, -1);
	}

	/**
	 * Redraws the cells given.
	 * 
	 * @param cells
	 *            The cells to be redraw.
	 */
	public void redrawCells(Cell[] cells) {
		if (cells.length <= 0) {
			return;
		}

		int style = getStyle();

		int startRow = Integer.MAX_VALUE;
		int startCol = Integer.MAX_VALUE;
		int endRow = Integer.MIN_VALUE;
		int endCol = Integer.MIN_VALUE;
		int lastRow = topRow + rowsVisible - 1;
		int lastCol = leftColumn + columnsVisible - 1;
		int fixedRowCount = layoutAdvisor.getFixedRowCount();
		int fixedColumnCount = layoutAdvisor.getFixedColumnCount();

		for (int i = 0; i < cells.length; i++) {
			startRow = Math.min(startRow, cells[i].row);
			startCol = Math.min(startCol, cells[i].column);
			endRow = Math.max(endRow, cells[i].row);
			endCol = Math.max(endCol, cells[i].column);

			if (cells[i] instanceof CellRow) {
				for (int j = 0; j < fixedColumnCount; j++) {
					Cell[] overlappedCells = getOverlappedCell(new Cell(this,
							cells[i].row, j));
					for (int k = 0; k < overlappedCells.length; k++) {
						endRow = Math.max(endRow, overlappedCells[k].row);
						endCol = Math.max(endCol, overlappedCells[k].column);
					}
				}
				for (int j = leftColumn; j <= lastCol; j++) {
					Cell[] overlappedCells = getOverlappedCell(new Cell(this,
							cells[i].row, j));
					for (int k = 0; k < overlappedCells.length; k++) {
						endRow = Math.max(endRow, overlappedCells[k].row);
						endCol = Math.max(endCol, overlappedCells[k].column);
					}
				}
			} else if (cells[i] instanceof CellColumn) {
				for (int j = 0; j < fixedRowCount; j++) {
					Cell[] overlappedCells = getOverlappedCell(new Cell(this,
							j, cells[i].column));
					for (int k = 0; k < overlappedCells.length; k++) {
						endRow = Math.max(endRow, overlappedCells[k].row);
						endCol = Math.max(endCol, overlappedCells[k].column);
					}
				}
				for (int j = topRow; j <= lastRow; j++) {
					Cell[] overlappedCells = getOverlappedCell(new Cell(this,
							j, cells[i].column));
					for (int k = 0; k < overlappedCells.length; k++) {
						endRow = Math.max(endRow, overlappedCells[k].row);
						endCol = Math.max(endCol, overlappedCells[k].column);
					}
				}
			} else {
				Cell[] overlappedCells = getOverlappedCell(cells[i]);
				for (int j = 0; j < overlappedCells.length; j++) {
					endRow = Math.max(endRow, overlappedCells[j].row);
					endCol = Math.max(endCol, overlappedCells[j].column);
				}
			}
		}

		if ((style & SWTX.COLUMN_SELECTION) != 0) {
			startRow = 0;
			endRow = lastRow;
		} else {
			if (startRow < 0) {
				startRow = 0;
			}
			if (startRow >= fixedRowCount && startRow < topRow) {
				startRow = topRow;
			}
			if (startRow > lastRow) {
				startRow = lastRow;
			}

			if (endRow < 0) {
				endRow = 0;
			}
			if (endRow > lastRow) {
				endRow = lastRow;
			}
			if (endRow >= fixedRowCount && endRow < topRow) {
				endRow = fixedRowCount - 1;
			}
		}

		if ((style & SWTX.ROW_SELECTION) != 0) {
			startCol = 0;
			endCol = lastCol;
		} else {
			if (startCol < 0) {
				startCol = 0;
			}
			if (startCol >= fixedColumnCount && startCol < leftColumn) {
				startCol = leftColumn;
			}
			if (startCol > lastCol) {
				startCol = lastCol;
			}

			if (endCol < 0) {
				endCol = 0;
			}
			if (endCol > lastCol) {
				endCol = lastCol;
			}
			if (endCol >= fixedColumnCount && endCol < leftColumn) {
				endCol = fixedColumnCount - 1;
			}
		}

		Cell validCell = getValidCell(startRow, startCol);
		Rectangle startRect = getCellRect(validCell.row, validCell.column);
		validCell = getValidCell(endRow, endCol);
		Rectangle endRect = getCellRect(validCell.row, validCell.column);
		Rectangle unionRect = startRect.union(endRect);

		redraw(unionRect.x, unionRect.y, unionRect.width, unionRect.height,
				true);
	}

	/**
	 * Returns the overlapped cells for given cell.
	 * 
	 * @param cell
	 *            The owner cell.
	 * @return the overlapped cells.
	 */
	Cell[] getOverlappedCell(Cell cell) {
		Cell validCell = layoutAdvisor.mergeInto(cell.row, cell.column);
		if (!validCell.equals(cell)) {
			return new Cell[] {};
		}

		int row = cell.row;
		int col = cell.column;

		int totalRowCount = layoutAdvisor.getRowCount();
		int totalColumnCount = layoutAdvisor.getColumnCount();

		Set<Cell> overlappedCells = new HashSet<Cell>();
		// determine the cells that are contained:
		int spanRow = row + 1;
		while (getValidCell(spanRow, col).equals(cell)
				&& spanRow < totalRowCount) {
			overlappedCells.add(new Cell(this, spanRow, col));
			spanRow++;
		}

		int spanCol = col + 1;
		while (getValidCell(row, spanCol).equals(cell)
				&& spanCol < totalColumnCount) {
			overlappedCells.add(new Cell(this, row, spanCol));
			spanCol++;
		}

		return overlappedCells.toArray(new Cell[] {});
	}

	/**
	 * Calculates the whole width of fixed column include head and fixed
	 * column,later means the fixed cell that can be selected.
	 * 
	 * @return The whole width of fixed column.
	 */
	protected int getFixedWidth() {
		int width = linePixels;

		if (layoutAdvisor.isLeftHeaderVisible()) {
			width += layoutAdvisor.getLeftHeaderWidth();
			width += linePixels;
		}

		for (int i = 0; i < layoutAdvisor.getFixedColumnCount(); i++) {
			width += getColumnWidth(i);
			width += linePixels;
		}
		return width;
	}

	/**
	 * Calculates the left coordinate x of the indicated column.
	 * 
	 * @param column
	 *            Index of column.
	 * @return The left coordinate x of the indicated column.
	 */
	protected int getColumnLeft(int column) {
		int x = linePixels;

		if (layoutAdvisor.isLeftHeaderVisible()) {
			if (column == -1) {
				return x;
			}
			x += layoutAdvisor.getLeftHeaderWidth();
			x += linePixels;
		} else {
			if (column == -1) {
				return Integer.MIN_VALUE;
			}
		}

		int fixedColumnCount = layoutAdvisor.getFixedColumnCount();
		if (column < fixedColumnCount) {
			for (int i = 0; i < column; i++) {
				x += getColumnWidth(i);
				x += linePixels;
			}
			return x;
		}

		x = getFixedWidth();
		if (column >= fixedColumnCount && column < leftColumn) {
			for (int i = leftColumn - 1; i >= column; i--) {
				x -= linePixels;
				x -= getColumnWidth(i);
			}
			return x;
		}

		for (int i = leftColumn; i < column; i++) {
			x += getColumnWidth(i);
			x += linePixels;
		}
		return x;
	}

	/**
	 * Calculates the right coordinate x of the indicated column.
	 * 
	 * @param column
	 *            Index of column.
	 * @return The right coordinate x of the indicated column.
	 */
	protected int getColumnRight(int column) {
		if (column < -1)
			return Integer.MIN_VALUE;
		int columnLeft = getColumnLeft(column);
		if (column == -1) {
			return columnLeft + layoutAdvisor.getLeftHeaderWidth() - 1;
		} else {
			return columnLeft + getColumnWidth(column) - 1;
		}
	}

	/**
	 * Calculates the bottom coordinate x of the indicated column.
	 * 
	 * @param row
	 *            Index of row.
	 * @return The bottom coordinate x of the indicated column.
	 */
	protected int getRowBottom(int row) {
		if (row < -1)
			return Integer.MIN_VALUE;

		int rowTop = getRowTop(row);

		if (row == -1) {
			return rowTop + layoutAdvisor.getTopHeaderHeight() - 1;
		} else {
			return rowTop + layoutAdvisor.getRowHeight(row) - 1;
		}

	}

	/**
	 * Calculates the whole height of fixed row include head and fixed row,later
	 * means the fixed cell that can be selected.
	 * 
	 * @return The whole height of fixed row.
	 */
	private int getFixedHeight() {
		int height = linePixels;

		if (layoutAdvisor.isTopHeaderVisible()) {
			height += layoutAdvisor.getTopHeaderHeight();
			height += linePixels;
		}
		for (int i = 0; i < layoutAdvisor.getFixedRowCount(); i++) {
			height += layoutAdvisor.getRowHeight(i);
			height += linePixels;
		}
		return height;
	}

	/**
	 * Calculates the layout metrics.
	 */
	public void calculateMetrics() {
		Rectangle clientArea = getClientArea();
		if (clientArea.isEmpty())
			return;

		if (layoutAdvisor == null) {
			ScrollBar sb = getHorizontalBar();
			if (sb != null) {
				sb.setMinimum(0);
				sb.setMaximum(1);
				sb.setPageIncrement(1);
				sb.setThumb(1);
				sb.setSelection(1);
			}
			sb = getVerticalBar();
			if (sb != null) {
				sb.setMinimum(0);
				sb.setMaximum(1);
				sb.setPageIncrement(1);
				sb.setThumb(1);
				sb.setSelection(1);
			}
			return;
		}

		int fixedRowCount = layoutAdvisor.getFixedRowCount();
		int fixedColumnCount = layoutAdvisor.getFixedColumnCount();
		int totalRowCount = layoutAdvisor.getRowCount();
		int totalColumnCount = layoutAdvisor.getColumnCount();

		topRow = Math.max(topRow, fixedRowCount);
		if (topRow >= totalRowCount) {
			topRow = 0;
		}

		leftColumn = Math.max(leftColumn, fixedColumnCount);
		if (leftColumn >= totalColumnCount) {
			leftColumn = 0;
		}

		columnsVisible = 0;
		columnsFullyVisible = 0;
		if (totalColumnCount > fixedColumnCount) {
			int rightBoundary = clientArea.x + clientArea.width;
			int width = getColumnLeft(leftColumn);
			for (int col = leftColumn; col < totalColumnCount; col++) {
				if (width < rightBoundary) {
					columnsVisible++;
				}
				width += getColumnWidth(col);
				width += linePixels;
				if (width <= rightBoundary) {
					columnsFullyVisible++;
				} else {
					break;
				}
			}
		}

		ScrollBar sb = getHorizontalBar();
		if (sb != null) {
			if (fixedColumnCount >= totalColumnCount) {
				sb.setMinimum(0);
				sb.setMaximum(1);
				sb.setPageIncrement(1);
				sb.setThumb(1);
				sb.setSelection(1);
			} else {
				sb.setMinimum(fixedColumnCount);
				sb.setMaximum(totalColumnCount);
				sb.setIncrement(1);
				sb.setPageIncrement(columnsVisible - fixedColumnCount);
				sb.setThumb(columnsFullyVisible);
				sb.setSelection(leftColumn);
			}
		}

		rowsVisible = 0;
		rowsFullyVisible = 0;
		if (totalRowCount > fixedRowCount) {
			int bottomBoundary = clientArea.y + clientArea.height;
			int height = getRowTop(topRow);
			for (int row = topRow; row < totalRowCount; row++) {
				if (height < bottomBoundary) {
					rowsVisible++;
				}
				height += layoutAdvisor.getRowHeight(row);
				height += linePixels;
				if (height <= bottomBoundary) {
					rowsFullyVisible++;
				} else {
					break;
				}
			}
		}

		sb = getVerticalBar();
		if (sb != null) {
			if (totalRowCount <= fixedRowCount) {
				sb.setMinimum(0);
				sb.setMaximum(1);
				sb.setPageIncrement(1);
				sb.setThumb(1);
				sb.setSelection(1);
			} else {
				sb.setMinimum(fixedRowCount);
				sb.setMaximum(totalRowCount);
				sb.setPageIncrement(rowsVisible - fixedRowCount);
				sb.setIncrement(1);
				sb.setThumb(rowsFullyVisible);
				sb.setSelection(topRow);
			}
		}
	}

	/**
	 * Returns the area that is occupied by the given cell. Does not take into
	 * account any cell span.
	 * 
	 * @param row
	 *            The row of cell.
	 * @param col
	 *            The column of cell.
	 * @return Rectangle The area occupied by the given cell.
	 */
	protected Rectangle getCellRectIgnoreSpan(int row, int col) {
		return getCellRectIgnoreSpan(row, col, getColumnLeft(col), this
				.getRowTop(row));
	}

	/**
	 * Returns the area that is occupied by the given cell. Does not take into
	 * account any cell span.
	 * <p>
	 * This version is an optimization if the x value is known. Use the version
	 * with just col and row as parameter if this is not known.
	 * 
	 * @param row
	 *            The row index
	 * @param col
	 *            The column index
	 * @param startX
	 *            The left horizontal start value.
	 * @param startY
	 *            The top vertical start value.
	 * @return Rectangle The area of a cell
	 */
	protected Rectangle getCellRectIgnoreSpan(int row, int col, int startX,
			int startY) {
		if ((col < 0) || (col >= layoutAdvisor.getColumnCount()))
			return new Rectangle(-1, -1, 0, 0);

		int x = startX;
		int y = startY;

		int width = getColumnWidth(col);
		int height = layoutAdvisor.getRowHeight(row);

		return new Rectangle(x, y, width, height);
	}

	/**
	 * Returns the area that is occupied by the top header cell given.
	 * 
	 * @param col
	 *            The index of column.
	 * @return The area that is occupied by the top header cell given.
	 */
	protected Rectangle getHeaderRowCellRect(int col) {
		return getTopHeaderCellRect(col, getColumnLeft(col));
	}

	/**
	 * Returns the area that is occupied by the top header cell given.
	 * <p>
	 * This version is an optimization if the x value is known, otherwise use
	 * the <code>{@link #getHeaderRowCellRect(int)}</code>.
	 * 
	 * @param col
	 *            The index of column.
	 * @param startX
	 *            The right horizontal start value. Note that this is the end
	 *            value of the last cell + linePixels pixel border.
	 * @return The area that is occupied by the top header cell given.
	 */
	protected Rectangle getTopHeaderCellRect(int col, int startX) {
		int x = startX;
		int y = linePixels;
		int width = getColumnWidth(col);
		int height = layoutAdvisor.getTopHeaderHeight();
		return new Rectangle(x, y, width, height);
	}

	/**
	 * Returns the area that is occupied by the given header column cell.
	 * 
	 * @param row
	 *            The index of row.
	 * @return The area that is occupied by the given header column cell.
	 */
	protected Rectangle getHeaderColumnCellRect(int row) {
		return getLeftHeaderCellRect(row, getRowTop(row));
	}

	/**
	 * Returns the area that is occupied by the given header column cell.
	 * <p>
	 * This version is an optimization if the y value is known, otherwise use
	 * the <code>{@link #getHeaderColumnCellRect(int)}</code>.
	 * 
	 * @param row
	 *            The index of row.
	 * @param startX
	 *            The bottom vertical start value. Note that this is the end
	 *            value of the last cell + linePixels pixel border.
	 * @return The area that is occupied by the given header column cell.
	 */
	protected Rectangle getLeftHeaderCellRect(int row, int startY) {
		int x = linePixels;
		int y = startY;
		int width = layoutAdvisor.getLeftHeaderWidth();
		int height = layoutAdvisor.getRowHeight(row);
		return new Rectangle(x, y, width, height);
	}

	/**
	 * Returns the area that is occupied by the given cell. Respects cell span.
	 * 
	 * @param row
	 *            The row index
	 * @param col
	 *            The column index
	 * @return Rectangle The area the cell content is drawn at.
	 * @throws IllegalArgumentException
	 *             If the given cell is not a cell that is visible, but
	 *             overlapped by a spanning cell. Call getValidCell() first to
	 *             ensure it is a visible cell.
	 */
	public Rectangle getCellRect(int row, int col) {
		checkWidget();

		// be sure that it is a valid cell that is not overlapped by some other:
		Cell valid = getValidCell(row, col);
		if (valid == Cell.NULLCELL || valid.row != row || valid.column != col)
			return new Rectangle(0, 0, 0, 0);

		Rectangle bound = getCellRectIgnoreSpan(row, col);
		int fixedRowCount = layoutAdvisor.getFixedRowCount();
		int totalRowCount = layoutAdvisor.getRowCount();
		int spanRow = row + 1;
		while (getValidCell(spanRow, col).equals(valid)
				&& spanRow < totalRowCount) {
			if (row < fixedRowCount && spanRow >= fixedRowCount
					&& spanRow < topRow) {
				spanRow++;
				continue;
			}
			bound.height += layoutAdvisor.getRowHeight(spanRow);
			bound.height += linePixels;
			spanRow++;
		}

		int spanCol = col + 1;
		int fixedColumnCount = layoutAdvisor.getFixedColumnCount();
		int totalColumnCount = layoutAdvisor.getColumnCount();
		while (getValidCell(row, spanCol).equals(valid)
				&& spanCol < totalColumnCount) {
			if (col < fixedColumnCount && spanCol >= fixedColumnCount
					&& spanCol < leftColumn) {
				spanCol++;
				continue;
			}
			bound.width += getColumnWidth(spanCol);
			bound.width += linePixels;
			spanCol++;
		}
		return bound;
	}

	/**
	 * Paints the client area of agile grid when painting event is occur.
	 * 
	 * @param event
	 *            The painting event.
	 */
	private void onPaint(PaintEvent event) {

		calculateMetrics();

		GC gc = event.gc;

		if (layoutAdvisor != null) {
			int fixedRowCount = layoutAdvisor.getFixedRowCount();
			int fixedColumnCount = layoutAdvisor.getFixedColumnCount();
			boolean isTopHeaderVisible = layoutAdvisor.isTopHeaderVisible();
			boolean isLeftHeaderVisible = layoutAdvisor.isLeftHeaderVisible();
			boolean hasFixedRow = fixedRowCount > 0;
			boolean hasFixedColumn = fixedColumnCount > 0;

			Rectangle intersection = null;
			int style = getStyle();

			Rectangle clipping = gc.getClipping();
			if (isTopHeaderVisible) {
				Rectangle topHeaderArea = getTopHeaderArea();
				intersection = topHeaderArea.intersection(clipping);
				if (!intersection.isEmpty()) {
					gc.setClipping(intersection);
					if (hasFixedColumn) {
						drawTopHeaderCells(gc, intersection, 0,
								fixedColumnCount - 1);
					}
					drawTopHeaderCells(gc, intersection, leftColumn, leftColumn
							+ columnsVisible - 1);
					gc.setClipping(clipping);
				} else if (((style & SWTX.MARK_SELECTION_HEADERS) == SWTX.MARK_SELECTION_HEADERS)
						|| ((style & SWTX.MARK_FOCUS_HEADERS) == SWTX.MARK_FOCUS_HEADERS)) {
					GC headerGC = new GC(this);
					headerGC.setClipping(topHeaderArea);
					if (hasFixedColumn) {
						drawTopHeaderCells(headerGC, headerGC.getClipping(), 0,
								fixedColumnCount - 1);
					}
					drawTopHeaderCells(headerGC, headerGC.getClipping(),
							leftColumn, leftColumn + columnsVisible - 1);
					headerGC.dispose();
				}

			}

			if (isLeftHeaderVisible) {
				Rectangle leftHeaderArea = getLeftHeaderArea();
				intersection = leftHeaderArea.intersection(clipping);
				if (!intersection.isEmpty()) {
					gc.setClipping(intersection);
					if (hasFixedRow) {
						drawLeftHeaderCells(gc, intersection, 0,
								fixedRowCount - 1);
					}
					drawLeftHeaderCells(gc, intersection, topRow, topRow
							+ rowsVisible - 1);
					gc.setClipping(clipping);
				} else if (((style & SWTX.MARK_SELECTION_HEADERS) == SWTX.MARK_SELECTION_HEADERS)
						|| ((style & SWTX.MARK_FOCUS_HEADERS) == SWTX.MARK_FOCUS_HEADERS)) {
					GC headerGC = new GC(this);
					headerGC.setClipping(leftHeaderArea);
					if (hasFixedRow) {
						drawLeftHeaderCells(headerGC, headerGC.getClipping(),
								0, fixedRowCount - 1);
					}
					drawLeftHeaderCells(headerGC, headerGC.getClipping(),
							topRow, topRow + rowsVisible - 1);
					headerGC.dispose();
				}
			}

			if (isTopHeaderVisible && isLeftHeaderVisible) {
				drawTopLeftCell(gc);
			}

			if (hasFixedRow) {
				Rectangle fixedRowScrollableArea = getFixedRowScrollableArea();
				intersection = fixedRowScrollableArea.intersection(clipping);
				if (!intersection.isEmpty()) {
					gc.setClipping(intersection);
					drawCells(gc, intersection, 0, fixedRowCount - 1,
							leftColumn, leftColumn + columnsVisible - 1);
					gc.setClipping(clipping);
				}
			}

			if (hasFixedColumn) {
				Rectangle fixedColumnScrollableArea = getFixedColumnScrollableArea();
				intersection = fixedColumnScrollableArea.intersection(clipping);
				if (!intersection.isEmpty()) {
					gc.setClipping(intersection);
					drawCells(gc, intersection, topRow, topRow + rowsVisible
							- 1, 0, fixedColumnCount - 1);
					gc.setClipping(clipping);
				}
			}

			if (hasFixedRow && hasFixedColumn) {
				drawCells(gc, clipping, 0, fixedRowCount - 1, 0,
						fixedColumnCount - 1);
			}

			Rectangle contentArea = getContentArea();
			intersection = contentArea.intersection(clipping);
			if (!intersection.isEmpty()) {
				gc.setClipping(intersection);
				drawCells(gc, intersection, topRow, topRow + rowsVisible - 1,
						leftColumn, leftColumn + columnsVisible - 1);
				gc.setClipping(clipping);
			}

			int totalRowCount = layoutAdvisor.getRowCount();
			int totalColumnCount = layoutAdvisor.getColumnCount();
			if ((!isTopHeaderVisible && totalRowCount == 0)
					|| (!isLeftHeaderVisible && totalColumnCount == 0)) {
				Rectangle rect = getClientArea();
				gc.fillRectangle(rect);
			} else {
				drawRightMargin(gc);
				drawBottomMargin(gc);
				drawBorderLine(gc);
			}
		} else {
			Rectangle rect = getClientArea();
			gc.fillRectangle(rect);
		}
	}

	/**
	 * Draws the top left cell of agile grid.
	 * 
	 * @param gc
	 *            The graphic context to draw on.
	 */
	protected void drawTopLeftCell(GC gc) {
		int width = layoutAdvisor.getLeftHeaderWidth();
		int height = layoutAdvisor.getTopHeaderHeight();
		Rectangle rect = new Rectangle(linePixels, linePixels, width, height);
		ICellRenderer cellRenderer = cellRendererProvider.getTopHeadRenderer(0);
		cellRenderer.drawCell(gc, rect, -1, -1);
		int x = linePixels + width - 1;
		int y = linePixels + height - 1;
		gc.setBackground(this.getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		gc.fillPolygon(new int[] { x, y, x, y - 8, x - 8, y, x, y });

	}

	/**
	 * Draws the right margin of client area of agile grid.
	 * 
	 * @param gc
	 *            The graphic context to draw on.
	 */
	private void drawRightMargin(GC gc) {
		int lastCol = layoutAdvisor.getColumnCount() - 1;
		if (leftColumn + columnsFullyVisible - 1 == lastCol) {
			Rectangle clientArea = getClientArea();
			int x = getColumnRight(leftColumn + columnsFullyVisible - 1) + 1
					+ linePixels;
			if (x >= clientArea.x + clientArea.width - 1 - linePixels) {
				return;
			}
			int y = linePixels;
			int width = clientArea.x + clientArea.width - linePixels - x + 1;
			int height = layoutAdvisor.getTopHeaderHeight();

			// Fills the remaining space with an additional empty column.
			if ((getStyle() & SWTX.FILL_WITH_DUMMYCOL) != 0) {
				Rectangle rect = null;
				// Draw the right margin of header row.
				if (layoutAdvisor.isTopHeaderVisible()) {
					rect = new Rectangle(x, y, width - 1, height);
					ICellRenderer headerCellRenderer = cellRendererProvider
							.getTopHeadRenderer(lastCol);
					headerCellRenderer.drawCell(gc, rect, -1, lastCol + 1);

					y += layoutAdvisor.getTopHeaderHeight();
					y += linePixels;
				}

				// Draw the right margin of fixed row.
				for (int row = 0; row < layoutAdvisor.getFixedRowCount(); row++) {
					height = layoutAdvisor.getRowHeight(row);
					ICellRenderer fixedCellRenderer = cellRendererProvider
							.getCellRenderer(row, lastCol);
					rect = new Rectangle(x, y, width - 1, height);
					fixedCellRenderer.drawCell(gc, rect, row, lastCol + 1);
					y += height;
					y += linePixels;
				}

				// Draw the right margin of content.
				for (int row = topRow; row <= topRow + rowsVisible; row++) {
					height = layoutAdvisor.getRowHeight(row);
					ICellRenderer defaultRenderer = cellRendererProvider
							.getCellRenderer(row, lastCol);
					rect = new Rectangle(x, y, width - 1, height);
					defaultRenderer.drawCell(gc, rect, row, lastCol + 1);
					y += height;
					y += linePixels;
				}
			} else {
				// draw simple background colored areas
				y = 0;
				height = clientArea.height;
				gc.setBackground(getBackground());
				gc.fillRectangle(x, y, width, height);
			}
		}
	}

	/**
	 * Draws the bottom margin of client area of agile grid.
	 * 
	 * @param gc
	 *            The graphic context to draw on.
	 */
	private void drawBottomMargin(GC gc) {
		int lastRow = layoutAdvisor.getRowCount() - 1;
		if (topRow + rowsFullyVisible - 1 == lastRow) {
			Rectangle clientArea = getClientArea();
			int y = getRowBottom(lastRow) + 1 + linePixels;
			if (y >= clientArea.height - 1) {
				return;
			}
			int x = 0;
			int width = clientArea.width;
			int height = clientArea.height - y;
			gc.setBackground(getBackground());
			gc.fillRectangle(x, y, width, height);
		}
	}

	/**
	 * Draws the border line of agile grid.
	 * 
	 * @param gc
	 *            The graphic context to draw on.
	 */
	private void drawBorderLine(GC gc) {
		if (layoutAdvisor.getRowCount() > 0) {
			Display display = this.getDisplay();
			if ((getStyle() & SWT.FLAT) == 0)
				gc.setForeground(display
						.getSystemColor(SWT.COLOR_WIDGET_DARK_SHADOW));
			else
				gc.setForeground(display
						.getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		}

		boolean drawRightLine = false;
		boolean drawBottomLine = false;

		Rectangle rect = getClientArea();
		int lastRow = layoutAdvisor.getRowCount() - 1;
		int lastCol = layoutAdvisor.getColumnCount() - 1;

		if (topRow + rowsFullyVisible - 1 == lastRow) {
			int y = getRowBottom(lastRow) + 1;
			if (y <= rect.height - 1) {
				rect.height = y + 1;
				drawBottomLine = true;
			}
		}

		if (leftColumn + columnsFullyVisible - 1 == lastCol) {
			int x = getColumnRight(lastCol) + 1;
			int style = getStyle();
			if (x <= rect.width - 1) {
				if ((style & SWTX.FILL_WITH_DUMMYCOL) == 0) {
					rect.width = x + 1;
				}
				drawRightLine = true;
			}
		}

		gc.drawLine(rect.x, rect.y, rect.x + rect.width - 1, rect.y);
		gc.drawLine(rect.x, rect.y, rect.x, rect.y + rect.height - 1);

		if (drawBottomLine) {
			gc.drawLine(rect.x, rect.y + rect.height - 1, rect.x + rect.width
					- 1, rect.y + rect.height - 1);
		}
		if (drawRightLine) {
			gc.drawLine(rect.x + rect.width - 1, rect.y, rect.x + rect.width
					- 1, rect.y + rect.height - 1);
		}

	}

	/**
	 * Draws the cells of top header.
	 * 
	 * @param gc
	 *            The graphic context.
	 * @param clipRect
	 *            The clipping rectangle.
	 * @param fromCol
	 *            Start column index to be drawn.
	 * @param toCol
	 *            End column index to be drawn.
	 */
	protected void drawTopHeaderCells(GC gc, Rectangle clipRect, int fromCol,
			int toCol) {
		int startX = getColumnLeft(fromCol);
		int rightBorder = clipRect.x + clipRect.width;
		for (int col = fromCol; col <= toCol; col++) {
			if (startX >= rightBorder)
				break;
			Rectangle rect = getTopHeaderCellRect(col, startX);
			startX += rect.width;
			startX += linePixels;
			Rectangle intersection = rect.intersection(clipRect);
			if (!intersection.isEmpty()) {
				ICellRenderer cellRenderer = cellRendererProvider
						.getTopHeadRenderer(col);
				cellRenderer.drawCell(gc, rect, -1, col);
			}
		}
	}

	/**
	 * Draws the cells of left header.
	 * 
	 * @param gc
	 *            The graphic context.
	 * @param clipRect
	 *            The clipping rectangle.
	 * @param fromCol
	 *            Start column index to be drawn.
	 * @param toCol
	 *            End column index to be drawn.
	 */
	protected void drawLeftHeaderCells(GC gc, Rectangle clipRect, int fromRow,
			int toRow) {
		int startY = getRowTop(fromRow);
		int bottomBorder = clipRect.y + clipRect.height;
		for (int row = fromRow; row <= toRow; row++) {
			if (startY >= bottomBorder)
				break;
			Rectangle rect = getLeftHeaderCellRect(row, startY);
			startY += rect.height;
			startY += linePixels;
			Rectangle intersection = rect.intersection(clipRect);
			if (!intersection.isEmpty()) {
				ICellRenderer cellRenderer = cellRendererProvider
						.getLeftHeadRenderer(row);
				cellRenderer.drawCell(gc, rect, row, -1);
			}
		}
	}

	/**
	 * Get the cell of top header that is present at position (x,y).
	 * 
	 * @param x
	 *            the x location in agile grid coordinates
	 * @param y
	 *            The y location in agile grid coordinates
	 * @return The cell which is located in the position (x, y), Otherwise
	 *         return Cell.NULLCELL.
	 */
	Cell getTopHeaderCell(int x, int y) {
		if (y < linePixels
				|| y > linePixels + layoutAdvisor.getTopHeaderHeight() - 1) {
			return Cell.NULLCELL;
		}

		int width = linePixels;

		if (layoutAdvisor.isLeftHeaderVisible()) {
			width += layoutAdvisor.getLeftHeaderWidth();
			width += linePixels;
		}

		if (x <= width) {
			return Cell.NULLCELL;
		}

		for (int i = 0; i < layoutAdvisor.getFixedColumnCount(); i++) {
			width += getColumnWidth(i);
			if (x <= width) {
				return new Cell(this, -1, i);
			}
			width += linePixels;
		}

		for (int i = leftColumn; i < layoutAdvisor.getColumnCount(); i++) {
			width += getColumnWidth(i);
			if (x <= width) {
				return new Cell(this, -1, i);
			}
			width += linePixels;
		}

		return Cell.NULLCELL;
	}

	/**
	 * Get the cell of left header that is present at position (x,y).
	 * 
	 * @param x
	 *            the x location in agile grid coordinates
	 * @param y
	 *            The y location in agile grid coordinates
	 * @return The cell which is located in the position (x, y), Otherwise
	 *         return Cell.NULLCELL.
	 */
	Cell getLeftHeaderCell(int x, int y) {
		if (x < linePixels
				|| x > linePixels + layoutAdvisor.getLeftHeaderWidth() - 1) {
			return Cell.NULLCELL;
		}

		int height = linePixels;

		if (layoutAdvisor.isTopHeaderVisible()) {
			height += layoutAdvisor.getTopHeaderHeight();
			height += linePixels;
		}

		if (y <= height) {
			return Cell.NULLCELL;
		}

		for (int i = 0; i < layoutAdvisor.getFixedRowCount(); i++) {
			height += layoutAdvisor.getRowHeight(i);
			if (y <= height) {
				return new Cell(this, i, -1);
			}
			height += linePixels;
		}

		for (int i = leftColumn; i < layoutAdvisor.getColumnCount(); i++) {
			height += layoutAdvisor.getRowHeight(i);
			if (y <= height) {
				return new Cell(this, i, -1);
			}
			height += linePixels;
		}

		return Cell.NULLCELL;
	}

	/**
	 * Get the cell of header included top and left header that is present at
	 * position (x,y).
	 * 
	 * @param x
	 *            the x location in agile grid coordinates
	 * @param y
	 *            The y location in agile grid coordinates
	 * @return The header cell which is located in the position (x, y),
	 *         Otherwise return Cell.NULLCELL.
	 */
	public Cell getHeaderCell(int x, int y) {
		checkWidget();

		int headerColumnWidth = layoutAdvisor.getLeftHeaderWidth();
		int headerRowHeight = layoutAdvisor.getTopHeaderHeight();
		if (x >= linePixels && x <= linePixels + headerColumnWidth
				&& y >= linePixels && y <= linePixels + headerRowHeight) {
			return new Cell(this, -1, -1);
		}

		if (x >= linePixels && x <= linePixels + headerColumnWidth) {
			return getLeftHeaderCell(x, y);
		}
		if (y >= linePixels && y <= linePixels + headerRowHeight) {
			return getTopHeaderCell(x, y);
		}
		return Cell.NULLCELL;
	}

	/**
	 * Returns the area that occupied by the given header cell.
	 * 
	 * @param row
	 *            The row index of header cell.
	 * @param col
	 *            The column index of header cell.
	 * @return The area that occupied by the given header cell.
	 */
	public Rectangle getHeaderCellRect(int row, int col) {
		checkWidget();

		int headerColumnWidth = layoutAdvisor.getLeftHeaderWidth();
		int headerRowHeight = layoutAdvisor.getTopHeaderHeight();
		if (row == -1 && col == -1) {
			return new Rectangle(linePixels, linePixels, headerColumnWidth,
					headerRowHeight);
		}

		if (col == -1 && row >= 0 && row <= layoutAdvisor.getRowCount() - 1) {
			return getHeaderColumnCellRect(row);
		}
		if (row == -1 && col >= 0 && col <= layoutAdvisor.getColumnCount() - 1) {
			return getHeaderRowCellRect(col);
		}
		return new Rectangle(0, 0, -1, -1);
	}

	/**
	 * Draw cells only in the area indicated by parameters.
	 * 
	 * @param gc
	 *            The gc to manipulate.
	 * @param clipRect
	 *            The clip area to draw.
	 * @param fromRow
	 *            The first row to draw.
	 * @param toRow
	 *            The end row to draw.
	 * @param fromCol
	 *            The first column to draw.
	 * @param toCol
	 *            The end column to draw.
	 */
	private void drawCells(GC gc, Rectangle clipRect, int fromRow, int toRow,
			int fromCol, int toCol) {

		agileGridEditor.updateCellEditorBounds();

		int rightBorder = clipRect.x + clipRect.width;
		int bottomBorder = clipRect.y + clipRect.height;

		int startY = this.getRowTop(fromRow);
		for (int row = fromRow; row <= toRow; row++) {
			// the cell coordination [x, y] is cached to avoid the expensive
			// loop.
			int startX = this.getColumnLeft(fromCol);
			for (int col = fromCol; col <= toCol; col++) {
				Rectangle rect = getCellRect(row, col, startX, startY);
				startX += getColumnWidth(col);
				startX += linePixels;

				if (rect.x >= rightBorder)
					break;
				if (rect.y >= bottomBorder)
					return;

				if (rect.isEmpty()) {
					Cell valid = getValidCell(row, col);
					if ((valid.row < topRow && row >= topRow)
							|| (valid.column < leftColumn && col >= leftColumn)) {
						rect = getCellRect(valid.row, valid.column);
						if (!rect.intersection(clipRect).isEmpty()) {
							drawCell(gc, valid.row, valid.column, rect);
						}
					}
				} else {
					if (!rect.intersection(clipRect).isEmpty()) {
						drawCell(gc, row, col, rect);
					}
				}
			}
			startY += layoutAdvisor.getRowHeight(row);
			startY += linePixels;
		}
	}

	/**
	 * Asks the layout advisor to determine if the given cell is overlapped by a
	 * cell that spans several columns/rows. In that case the index of the cell
	 * responsible for the content (in the left upper corner) is returned.
	 * Otherwise the given cell is returned.
	 * 
	 * @param colToCheck
	 *            The column index of the cell to check.
	 * @param rowToCheck
	 *            The row index of the cell to check.
	 * @return The cell that overlaps the given cell, or the given cell if no
	 *         cell overlaps it.
	 * @throws IllegalArgumentException
	 *             If the layout advisor returns cells on
	 *             <code>ILayoutAdvisor.belongsToCell()</code> that are on the
	 *             right or below the given cell.
	 */
	public Cell getValidCell(int rowToCheck, int colToCheck) {
		checkWidget();
		if (rowToCheck < -1 || rowToCheck >= layoutAdvisor.getRowCount()
				|| colToCheck < -1
				|| colToCheck >= layoutAdvisor.getColumnCount()) {
			return Cell.NULLCELL;
		}
		// well, there is no supercell with negative indices, so don't check:
		Cell found = new Cell(this, rowToCheck, colToCheck);
		Cell lastFound = Cell.NULLCELL;
		while (!found.equals(lastFound)) {
			lastFound = found;
			found = layoutAdvisor.mergeInto(found.row, found.column);
			if (found != null
					&& found != Cell.NULLCELL
					&& (found.column > lastFound.column || found.row > lastFound.row))
				throw new IllegalArgumentException(
						"When spanning over several cells, "
								+ "supercells that determine the content of the large cell must "
								+ "always be in the left upper corner!");
			if (found == null || found == Cell.NULLCELL)
				return lastFound;
		}
		return found;
	}

	/**
	 * Draw cells only in the area indicated by parameters.
	 * 
	 * @param gc
	 *            The gc to manipulate.
	 * @param row
	 *            The row index of cell to draw.
	 * @param col
	 *            The column index of cell to draw.
	 * @param rect
	 *            The clip area to draw.
	 */
	protected void drawCell(GC gc, int row, int col, Rectangle rect) {
		if (row < 0 || row >= layoutAdvisor.getRowCount() || col < 0
				|| col >= layoutAdvisor.getColumnCount() || rect.isEmpty()) {
			return;
		}

		ICellRenderer cellRenderer = cellRendererProvider.getCellRenderer(row,
				col);
		cellRenderer.drawCell(gc, rect, row, col);
	}

	/**
	 * Helper method to determine whether the cell at the given position is to
	 * be highlighted because it is a header cell that corresponds to a selected
	 * cell.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return true if the cell should be highlighted, otherwise false.
	 */
	public boolean isHeaderHighlighted(int row, int col) {
		checkWidget();

		int style = getStyle();
		if (((style & SWTX.MARK_SELECTION_HEADERS) != SWTX.MARK_SELECTION_HEADERS)
				&& ((style & SWTX.MARK_FOCUS_HEADERS) != SWTX.MARK_FOCUS_HEADERS))
			return false;

		if ((style & SWTX.MARK_SELECTION_HEADERS) == SWTX.MARK_SELECTION_HEADERS) {
			Cell[] sel = getCellSelection();
			if (sel != null && sel.length > 0) {
				ILayoutAdvisor layoutAdvisor = getLayoutAdvisor();
				if ((style & SWTX.ROW_SELECTION) != 0) {
					if (row == -1 && col >= 0
							&& col < layoutAdvisor.getColumnCount()) {
						return true;
					}
					for (int i = 0; i < sel.length; i++) {
						if (row == sel[i].row) {
							return true;
						}
					}

				} else if ((style & SWTX.COLUMN_SELECTION) != 0) {
					if (col == -1 && row >= 0
							&& row < layoutAdvisor.getRowCount()) {
						return true;
					}
					for (int i = 0; i < sel.length; i++) {
						if (col == sel[i].column) {
							return true;
						}
					}
				} else {
					for (int i = 0; i < sel.length; i++) {
						if (row == sel[i].row || col == sel[i].column)
							return true;
						Cell valid = getValidCell(sel[i].row, col);
						if (valid.column == sel[i].column)
							return true;
						valid = getValidCell(row, sel[i].column);
						if (valid.row == sel[i].row)
							return true;
					}
				}
			}
		} else {
			Cell focusCell = getFocusCell();
			if (focusCell == Cell.NULLCELL)
				return false;
			if (row == focusCell.row || col == focusCell.column)
				return true;
			Cell valid = getValidCell(focusCell.row, col);
			if (valid.column == focusCell.column)
				return true;
			valid = getValidCell(row, focusCell.column);
			if (valid.row == focusCell.row)
				return true;
		}

		return false;
	}

	/**
	 * Is the cell clicked.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return true if the cell is clicked, false otherwise.
	 */
	public boolean isCellPressed(int row, int col) {
		checkWidget();

		Rectangle rect = null;
		if (row >= 0 && col >= 0) {
			rect = getCellRect(row, col);
		} else if (row == -1 || col == -1) {
			rect = getHeaderCellRect(row, col);
		}
		boolean isPressed = mousePoint != null && rect.contains(mousePoint)
				&& isCaptured && row == pressedCell.row
				&& col == pressedCell.column;
		return isPressed;
	}

	/**
	 * Is the cell focused.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return true if the cell is focused, false otherwise.
	 */
	public boolean isFocusCell(int row, int col) {
		checkWidget();

		return cellSelectionManager.isFocusCell(row, col);
	}

	/**
	 * Is the cell editor active.
	 * 
	 * @return true if the cell editor is active, false otherwise.
	 */
	public boolean isCellEditorActive() {
		checkWidget();

		return agileGridEditor.isCellEditorActive();
	}

	/**
	 * Shows the cell as selected.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return If true, cell indicated by (row, col) will be showed as selected,
	 *         otherwise don't show as selected.
	 */
	protected boolean showAsSelected(int row, int col) {
		// A cell with an open editor should be drawn without focus
		if (agileGridEditor.isActiveCell(row, col))
			return false;

		return cellSelectionManager.isCellSelected(row, col)
				&& (isFocusControl() || isShowSelectionWithoutFocus());
	}

	/**
	 * Is the cell selected.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return true if the cell is selected, false otherwise.
	 */
	public boolean isCellSelected(int row, int col) {
		checkWidget();

		return cellSelectionManager.isCellSelected(row, col);
	}

	/**
	 * Sets the default cursor to the given cursor. This instance is saved
	 * internally and displayed whenever no linecursor is shown.
	 * <p>
	 * The difference to setCursor is that this cursor will be preserved over
	 * action cursor changes.
	 * 
	 * @param cursor
	 *            The cursor to use, or <code>null</code> if the OS default
	 *            cursor should be used.
	 * @param size_below_hotspot
	 *            The number of pixels that are needed to paint the cursor below
	 *            and right of the cursor hotspot (that is the actual location
	 *            the cursor is pointing to).
	 *            <p>
	 *            NOTE that this is just there to allow better positioning of
	 *            tooltips. Currently SWT does not provide an API to get the
	 *            size of the cursor. So these values are taken to calculate the
	 *            position of the tooltip. The the tooltip is placed pt.x pixels
	 *            left and pt.y pixels below the mouse location.<br>
	 *            If you don't know the size of the cursor (for example you use
	 *            a default one), set <code>null</code> or
	 *            <code>new Point(-1, -1)</code>.
	 */
	public void setDefaultCursor(Cursor cursor, Point size_below_hotspot) {
		checkWidget();

		if (defaultCursor != null) {
			defaultCursor.dispose();
		}
		defaultCursor = cursor;
		defaultCursorSize = size_below_hotspot;
		setCursor(cursor);
	}

	/**
	 * Returns the row resize cursor, if null, returns the default row resize
	 * cursor.
	 * 
	 * @return the rowResizeCursor
	 */
	public Cursor getRowResizeCursor() {
		checkWidget();

		if (this.rowResizeCursor == null) {
			return this.defaultRowResizeCursor;
		}
		return this.rowResizeCursor;
	}

	/**
	 * Sets the row resize cursor to the given cursor.
	 * 
	 * @param rowResizeCursor
	 *            the rowResizeCursor to set
	 */
	public void setRowResizeCursor(Cursor rowResizeCursor) {
		checkWidget();

		if (this.rowResizeCursor != null) {
			this.rowResizeCursor.dispose();
		}
		this.rowResizeCursor = rowResizeCursor;
	}

	/**
	 * Returns the column resize cursor, if null, returns the default column
	 * resize cursor.
	 * 
	 * @return the columnResizeCursor
	 */
	public Cursor getColumnResizeCursor() {
		checkWidget();

		if (this.columnResizeCursor == null) {
			return this.defaultColumnResizeCursor;
		}
		return this.columnResizeCursor;
	}

	/**
	 * Sets the column resize cursor to the given cursor.
	 * 
	 * @param columnResizeCursor
	 *            the columnResizeCursor to set
	 */
	public void setColumnResizeCursor(Cursor columnResizeCursor) {
		checkWidget();

		if (this.columnResizeCursor != null) {
			this.columnResizeCursor.dispose();
		}
		this.columnResizeCursor = columnResizeCursor;
	}

	/**
	 * Sets the default cursor to the given cursor. This instance is saved
	 * internally and displayed whenever no resizecursor is shown.
	 * <p>
	 * The difference to setCursor is that this cursor will be preserved over
	 * action cursor changes.
	 * 
	 * @param cursor
	 *            The cursor to use, or <code>null</code> if the OS default
	 *            cursor should be used.
	 */
	public void setDefaultRowResizeCursor(Cursor cursor) {
		checkWidget();

		if (defaultRowResizeCursor != null) {
			defaultRowResizeCursor.dispose();
		}
		defaultRowResizeCursor = cursor;
	}

	/**
	 * Sets the default cursor to the given cursor. This instance is saved
	 * internally and displayed whenever no resizecursor is shown.
	 * <p>
	 * The difference to setCursor is that this cursor will be preserved over
	 * action cursor changes.
	 * 
	 * @param cursor
	 *            The cursor to use, or <code>null</code> if the OS default
	 *            cursor should be used.
	 */
	public void setDefaultColumnResizeCursor(Cursor cursor) {
		checkWidget();

		if (defaultColumnResizeCursor != null) {
			defaultColumnResizeCursor.dispose();
		}
		defaultColumnResizeCursor = cursor;

	}

	/**
	 * Calculate column index in coordinate (x,y), which will be used resizing
	 * the width of column.
	 * 
	 * @param x
	 *            The coordinate x.
	 * @param y
	 *            The coordinate y.
	 * @return The column index for resizing.
	 */
	private int getColumnForResize(int x, int y) {
		if (!layoutAdvisor.isTopHeaderVisible() || y <= linePixels
				|| y >= layoutAdvisor.getTopHeaderHeight() + linePixels) {
			return Integer.MIN_VALUE;
		}

		int left = 0;
		int right = 0;
		int pad = resizeAreaSize / 2;

		if (x <= linePixels + layoutAdvisor.getLeftHeaderWidth() + pad) {
			left = getColumnLeft(-1);
			right = left + layoutAdvisor.getLeftHeaderWidth() - 1;
			if (x > right - pad && x < right + 1 + linePixels + pad
					&& layoutAdvisor.isColumnResizable(-1)) {
				return -1;
			}
		} else if (x <= linePixels + getFixedWidth() + pad) {
			int fixedColumnCount = layoutAdvisor.getFixedColumnCount();
			left = getColumnLeft(0);
			for (int i = 0; i < fixedColumnCount; i++) {
				right = left + getColumnWidth(i) - 1;
				if (x > right - pad && x < right + 1 + linePixels + pad
						&& layoutAdvisor.isColumnResizable(i)) {
					return i;
				}
				if (x >= left && x <= right) {
					break;
				}
				left = right + 1 + linePixels;
			}
			return Integer.MIN_VALUE;
		} else {
			int totalColumnCount = layoutAdvisor.getColumnCount();
			left = getColumnLeft(leftColumn);
			for (int i = leftColumn; i <= (leftColumn + columnsVisible - 1)
					&& i < totalColumnCount; i++) {
				right = left + getColumnWidth(i) - 1;
				if (x > right - pad && x < right + 1 + linePixels + pad
						&& layoutAdvisor.isColumnResizable(i)) {
					return i;
				}
				if (x >= left && x <= right) {
					break;
				}
				left = right + 1 + linePixels;
			}
		}

		return Integer.MIN_VALUE;
	}

	/**
	 * Calculate row index in coordinate (x,y), which will be used resizing the
	 * height of row.
	 * 
	 * @param x
	 *            The coordinate x.
	 * @param y
	 *            The coordinate y.
	 * @return The row index for resizing.
	 */
	private int getRowForResize(int x, int y) {
		if (!layoutAdvisor.isLeftHeaderVisible() || x <= linePixels
				|| x >= layoutAdvisor.getLeftHeaderWidth() + linePixels) {
			return Integer.MIN_VALUE;
		}

		int top = 0;
		int bottom = 0;
		int pad = resizeAreaSize / 2;

		if (y <= linePixels + layoutAdvisor.getTopHeaderHeight() + pad) {

			top = getRowTop(-1);
			bottom = top + layoutAdvisor.getTopHeaderHeight() - 1;
			if (y > bottom - pad && y < bottom + 1 + linePixels + pad
					&& layoutAdvisor.isRowResizable(-1)) {
				return -1;
			}
		} else if (y <= linePixels + getFixedHeight() + pad) {
			int fixedRowCount = layoutAdvisor.getFixedRowCount();
			top = getRowTop(0);
			for (int i = 0; i < fixedRowCount; i++) {
				bottom = top + layoutAdvisor.getRowHeight(i) - 1;
				if (y > bottom - pad && y < bottom + 1 + linePixels + pad
						&& layoutAdvisor.isRowResizable(i)) {
					return i;
				}
				if (y >= top && y <= bottom) {
					break;
				}
				top = bottom + 1 + linePixels;
			}
			return Integer.MIN_VALUE;
		} else {
			int totalRowCount = layoutAdvisor.getRowCount();
			top = getRowTop(topRow);
			for (int i = topRow; i <= (topRow + rowsVisible - 1)
					&& i < totalRowCount; i++) {
				bottom = top + layoutAdvisor.getRowHeight(i) - 1;
				if (y > bottom - pad && y < bottom + 1 + linePixels + pad
						&& layoutAdvisor.isRowResizable(i)) {
					return i;
				}
				if (y >= top && y <= bottom) {
					break;
				}
				top = bottom + 1 + linePixels;
			}
		}

		return Integer.MIN_VALUE;
	}

	/**
	 * Calculates the row index for the given coordination y.
	 * 
	 * @param y
	 *            The coordination y.
	 * @return The row index for the given coordination y.
	 */
	private int getRowForY(int y) {
		if (y < 0) {
			return Integer.MIN_VALUE;
		}

		int height = linePixels;

		if (layoutAdvisor.isTopHeaderVisible()) {
			height += layoutAdvisor.getTopHeaderHeight();
			height += linePixels;
			if (y <= height) {
				return -1;
			}
		}

		int fixedRowCount = layoutAdvisor.getFixedRowCount();
		for (int i = 0; i < fixedRowCount; i++) {
			height += layoutAdvisor.getRowHeight(i);
			height += linePixels;
			if (y <= height) {
				return i;
			}
		}

		for (int i = topRow; i <= topRow + rowsVisible - 1; i++) {
			height += layoutAdvisor.getRowHeight(i);
			height += linePixels;
			if (y <= height) {
				return i;
			}
		}

		return Integer.MIN_VALUE;
	}

	/**
	 * Calculates the column index for the given coordination x.
	 * 
	 * @param x
	 *            The coordination x.
	 * @return The column index for the given coordination x.
	 */
	private int getColumnForX(int x) {
		if (x < 0) {
			return Integer.MIN_VALUE;
		}

		int width = linePixels;

		if (layoutAdvisor.isLeftHeaderVisible()) {
			width += layoutAdvisor.getLeftHeaderWidth();
			width += linePixels;
			if (x <= width) {
				return -1;
			}
		}

		int fixedColumnCount = layoutAdvisor.getFixedColumnCount();
		for (int i = 0; i < fixedColumnCount; i++) {
			width += getColumnWidth(i);
			width += linePixels;
			if (x <= width) {
				return i;
			}
		}

		for (int i = leftColumn; i <= leftColumn + columnsVisible - 1; i++) {
			width += getColumnWidth(i);
			width += linePixels;
			if (x <= width) {
				return i;
			}
		}

		return Integer.MIN_VALUE;
	}

	/**
	 * Get the cell that is present at position (x,y). When cells are spanned,
	 * returns the address of the spanned cell.
	 * 
	 * @param x
	 *            the x location in agile grid coordinates
	 * @param y
	 *            The y location in agile grid coordinates
	 * @return The cell which is located in the position (x, y), Otherwise
	 *         return Cell(this, -1, -1),if out of area.
	 */
	public Cell getCell(int x, int y) {
		checkWidget();

		if (layoutAdvisor == null)
			return Cell.NULLCELL;

		int row = getRowForY(y);
		int column = getColumnForX(x);
		if (row != Integer.MIN_VALUE && column != Integer.MIN_VALUE) {
			return new Cell(this, row, column);
		}

		return Cell.NULLCELL;
	}

	/**
	 * Decide whether the cell is visible or not.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return true if cell is visible. false if cell is not visible.
	 */
	public boolean isCellVisible(int row, int col) {
		checkWidget();

		if (layoutAdvisor == null)
			return false;

		if (row < -1
				|| row >= topRow + rowsVisible
				|| (row >= layoutAdvisor.getFixedRowCount() && row < topRow)
				|| col < -1
				|| col >= leftColumn + columnsVisible
				|| (col >= layoutAdvisor.getFixedColumnCount() && col < leftColumn)) {
			return false;
		}

		return true;
	}

	/**
	 * Decide whether the cell is fully visible or not.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return true if cell is fully visible. false if not.
	 */
	public boolean isCellFullyVisible(int row, int col) {
		checkWidget();

		if (layoutAdvisor == null)
			return false;
		return ((col >= leftColumn && col < leftColumn + columnsFullyVisible
				&& row >= topRow && row < topRow + rowsFullyVisible)

		|| (col < layoutAdvisor.getFixedColumnCount() && row < layoutAdvisor
				.getFixedRowCount()));
	}

	/**
	 * Decide whether the row of agile grid is visible or not.
	 * 
	 * @param row
	 *            The row index as seen by agile grid.
	 * @return true if the row is visible, false if not.
	 */
	public boolean isRowVisible(int row) {
		checkWidget();

		if (layoutAdvisor == null)
			return false;
		return ((row >= topRow && row < topRow + rowsVisible) || row < (layoutAdvisor
				.getFixedRowCount()));

	}

	/**
	 * Decide whether the row of agile grid is fully visible or not.
	 * 
	 * @param row
	 *            The row index as seen by agile grid.
	 * @return true if the row is fully visible, false if not.
	 */
	public boolean isRowFullyVisible(int row) {
		checkWidget();

		if (layoutAdvisor == null)
			return false;
		return ((row >= topRow && row < topRow + rowsFullyVisible) || row < layoutAdvisor
				.getFixedRowCount());
	}

	/**
	 * Process the mouse down event.
	 * 
	 * @param e
	 *            The mouse down event.
	 */
	protected void onMouseDown(MouseEvent e) {
		if (e.button == 1) {
			setCapture(true);
			isCaptured = true;

			// Resize column?
			int columnIndex = getColumnForResize(e.x, e.y);
			if (columnIndex >= -1) {
				resizeColumnIndex = columnIndex;
				resizeColumnLeft = getColumnLeft(columnIndex);
				return;
			}

			// Resize row?
			int rowIndex = getRowForResize(e.x, e.y);
			if (rowIndex >= -1) {
				resizeRowIndex = rowIndex;
				resizeRowTop = getRowTop(rowIndex);
				return;
			}
		}

		if (e.button == 1) {
			mousePoint = new Point(e.x, e.y);

			Cell cell = getCell(e.x, e.y);
			if (cell == Cell.NULLCELL) {
				cell = getHeaderCell(e.x, e.y);
				if (cell == Cell.NULLCELL) {
					return;
				}
			}

			// focus change
			pressedCell = cell;

			if (pressedCell.row == -1 || pressedCell.column == -1) {
				Rectangle rect = getHeaderCellRect(cell.row, cell.column);
				rect.width += linePixels;
				rect.height += linePixels;
				redraw(rect.x, rect.y, rect.width, rect.height, true);
			}
		}
	}

	/**
	 * Process mouse double click event.
	 * 
	 * @param e
	 *            the mouse double click event.
	 */
	protected void onMouseDoubleClick(MouseEvent e) {
		if (layoutAdvisor == null)
			return;

		if (e.button == 1) {
			Cell cell = getCell(e.x, e.y);
			if (cell == Cell.NULLCELL)
				return;

			if (e.y < linePixels + layoutAdvisor.getTopHeaderHeight()) {
				// double click in header area
				int columnIndex = getColumnForResize(e.x, e.y);
				if (columnIndex != Integer.MIN_VALUE) {
					resizeColumnOptimal(columnIndex);
					resizeColumnIndex = Integer.MIN_VALUE;
				}
			}

			CellDoubleClickEvent event = new CellDoubleClickEvent(cell, e);
			fireCellDoubleClicked(event);
		}
	}

	/**
	 * Process the mouse move event.
	 * 
	 * @param e
	 *            The mouse move event.
	 */
	protected void onMouseMove(MouseEvent e) {
		if (layoutAdvisor == null)
			return;

		// show resize cursor?
		if ((resizeColumnIndex != Integer.MIN_VALUE)
				|| (getColumnForResize(e.x, e.y) >= -1)) {
			setCursor(this.getColumnResizeCursor());
		} else if ((resizeRowIndex != Integer.MIN_VALUE)
				|| (getRowForResize(e.x, e.y) >= -1)) {
			setCursor(this.getRowResizeCursor());
		} else { // show default cursor:
			setCursor(defaultCursor);
		}

		if (pressedCell.row == -1 || pressedCell.column == -1) {
			mousePoint = new Point(e.x, e.y);
			Rectangle rect = getHeaderCellRect(pressedCell.row,
					pressedCell.column);
			rect.width += linePixels;
			rect.height += linePixels;
			redraw(rect.x, rect.y, rect.width, rect.height, true);
		}

		// extend selection?
		if (e.stateMask == SWT.BUTTON1 && isMultiSelectMode()
				&& !agileGridEditor.isCellEditorActive()
				&& pressedCell.row >= 0 && pressedCell.column >= 0) {
			Cell cell = getCell(e.x, e.y);
			if (cell.row >= 0 && cell.column >= 0) {
				cellSelectionManager.focusCell(cell, (e.stateMask | SWT.SHIFT));
			}
		}

		// Do Resize Column
		if (resizeColumnIndex != Integer.MIN_VALUE) {
			if (resizeColumnIndex == -1) {
				Rectangle rect = getClientArea();
				int oldWidth = layoutAdvisor.getLeftHeaderWidth();
				int newWidth = e.x > rect.width - 5 ? rect.width
						- resizeColumnLeft - 5 : e.x - resizeColumnLeft;
				if (newWidth < 5) {
					newWidth = 5;
				}

				layoutAdvisor.setLeftHeaderWidth(newWidth);
				newWidth = layoutAdvisor.getLeftHeaderWidth();
				if (oldWidth != newWidth) {
					fireColumnResize(resizeColumnIndex, newWidth);
				}
			} else if (resizeColumnIndex >= 0) {
				int oldWidth = getColumnWidth(resizeColumnIndex);
				int newColumnSize = e.x - resizeColumnLeft;
				if (newColumnSize < 5) {
					newColumnSize = 5;
				}

				layoutAdvisor.setColumnWidth(resizeColumnIndex, newColumnSize);
				int newWidth = getColumnWidth(resizeColumnIndex);
				if (oldWidth != newWidth) {
					fireColumnResize(resizeColumnIndex, newWidth);
				}
			}
		}

		// Do Resize Row
		if (resizeRowIndex != Integer.MIN_VALUE) {
			if (resizeRowIndex == -1) {
				Rectangle rect = getClientArea();
				int oldHeight = layoutAdvisor.getTopHeaderHeight();
				int newHeight = e.y > rect.height - 5 ? rect.height - 5
						- resizeRowTop : e.y - resizeRowTop;
				if (newHeight < 5) {
					newHeight = 5;
				}

				layoutAdvisor.setTopHeaderHeight(newHeight);
				newHeight = layoutAdvisor.getTopHeaderHeight();
				if (oldHeight != newHeight) {
					fireRowResize(resizeRowIndex, newHeight);
				}
			} else if (resizeRowIndex >= 0) {
				int oldHeight = layoutAdvisor.getRowHeight(resizeRowIndex);
				int newRowSize = e.y - resizeRowTop;
				if (newRowSize < layoutAdvisor.getRowHeightMinimum())
					newRowSize = layoutAdvisor.getRowHeightMinimum();

				layoutAdvisor.setRowHeight(resizeRowIndex, newRowSize);
				int newHeight = layoutAdvisor.getRowHeight(resizeRowIndex);
				if (oldHeight != newHeight) {
					fireRowResize(resizeRowIndex, newRowSize);
				}
			}
		}
	}

	/**
	 * Process the mouse up event.
	 * 
	 * @param e
	 *            The mouse up event.
	 */
	protected void onMouseUp(MouseEvent e) {
		if (layoutAdvisor == null)
			return;

		setCapture(false);
		isCaptured = false;

		if (pressedCell != Cell.NULLCELL) {
			int row = pressedCell.row;
			int col = pressedCell.column;

			pressedCell = Cell.NULLCELL;
			mousePoint = null;

			if (row == -1 || col == -1) {
				Rectangle rect = getHeaderCellRect(row, col);
				rect.width += linePixels;
				rect.height += linePixels;
				redraw(rect.x, rect.y, rect.width, rect.height, true);
			} else {
				Cell valid = getValidCell(row, col);
				redrawCells(new Cell[] { valid });
			}
		}

		resizeColumnIndex = Integer.MIN_VALUE;
		resizeRowIndex = Integer.MIN_VALUE;
	}

	/**
	 * Listener Class that implements fake tooltips. The tooltip content is
	 * retrieved from the layout advisor.
	 */
	class TooltipListener implements Listener {
		private Shell tip;
		private Label label;
		private Display display;

		/**
		 * Creates a new tooltip listener.
		 */
		public TooltipListener() {
			tip = null;
			label = null;
			display = getDisplay();
		}

		final Listener labelListener = new Listener() {
			public void handleEvent(Event event) {
				Label label = (Label) event.widget;
				Shell shell = label.getShell();
				// forward mouse events directly to the underlying agile grid
				switch (event.type) {
				case SWT.MouseDown:
					Event e = new Event();
					e.item = AgileGrid.this;
					e.button = event.button;
					e.stateMask = event.stateMask;
					notifyListeners(SWT.MouseDown, e);
					// fall through
				default:
					shell.dispose();
					break;
				}
			}
		};

		public void handleEvent(Event event) {
			switch (event.type) {
			case SWT.Dispose:
			case SWT.KeyDown:
			case SWT.MouseDown:
			case SWT.MouseDoubleClick:
			case SWT.MouseMove:
			case SWT.Selection: // scrolling
			case SWT.MouseExit: {
				if (tip == null)
					break;
				tip.dispose();
				tip = null;
				label = null;
				break;
			}
			case SWT.MouseHover: {
				if (tip != null && !tip.isDisposed())
					tip.dispose();

				Cell cell = getCell(event.x, event.y);
				String tooltip = layoutAdvisor
						.getTooltip(cell.row, cell.column);

				// check if there is something to show, and abort otherwise:
				if (((tooltip == null || tooltip.equals("")) && (nativeTooltip == null || nativeTooltip
						.equals("")))
						|| (cell == null || cell.column == Integer.MIN_VALUE || cell.row == Integer.MIN_VALUE)) {
					tip = null;
					label = null;
					return;
				}

				tip = new Shell(getShell(), SWT.ON_TOP);
				GridLayout gl = new GridLayout();
				gl.marginWidth = 2;
				gl.marginHeight = 2;
				tip.setLayout(gl);
				tip.setBackground(display
						.getSystemColor(SWT.COLOR_INFO_BACKGROUND));
				label = new Label(tip, SWT.NONE);
				label.setLayoutData(new GridData(GridData.FILL_BOTH));
				label.setForeground(display
						.getSystemColor(SWT.COLOR_INFO_FOREGROUND));
				label.setBackground(display
						.getSystemColor(SWT.COLOR_INFO_BACKGROUND));
				if (tooltip != null && !tooltip.equals(""))
					label.setText(tooltip);
				else
					label.setText(nativeTooltip);
				label.addListener(SWT.MouseExit, labelListener);
				label.addListener(SWT.MouseDown, labelListener);
				label.addListener(SWT.MouseMove, labelListener);
				Point size = tip.computeSize(SWT.DEFAULT, SWT.DEFAULT);

				int y = 20; // currently the windows default???
				int x = 0;
				if (defaultCursorSize != null && defaultCursorSize.x >= 0
						&& defaultCursorSize.y >= 0) {
					y = defaultCursorSize.y + 1;
					x = -defaultCursorSize.x;
				}
				// place the shell under the mouse, but check that the
				// bounds of the agile grid are not overlapped.
				Rectangle agileGridBounds = AgileGrid.this.getBounds();
				if (event.x + x + size.x > agileGridBounds.x
						+ agileGridBounds.width)
					event.x -= event.x + x + size.x - agileGridBounds.x
							- agileGridBounds.width;
				if (event.y + y + size.y > agileGridBounds.y
						+ agileGridBounds.height)
					event.y -= event.y + y + size.y - agileGridBounds.y
							- agileGridBounds.height;

				Point pt = toDisplay(event.x + x, event.y + y);
				tip.setBounds(pt.x, pt.y, size.x, size.y);
				tip.setVisible(true);
			}
			}
		}
	}

	/**
	 * Sets the global tooltip for the whole agile grid.<br>
	 * Note that this is only shown if the cell has no tooltip set. For tooltips
	 * on cell level (that overwrite this value), look for the method
	 * <code>getTooltipText()</code>.
	 * 
	 * @see org.agilemore.agilegrid.IContentProvider#getTooltip(int, int)
	 * @see org.agilemore.agilegrid.AgileGrid#getToolTipText()
	 * @param tooltip
	 *            The global tooltip for the agile grid.
	 */
	public void setToolTipText(String tooltip) {
		checkWidget();

		nativeTooltip = tooltip;
	}

	/**
	 * Returns the global tooltip for the whole agile grid.<br>
	 * Note that this is not shown when there is a non-empty tooltip for the
	 * cell.
	 * 
	 * @see org.agilemore.agilegrid.AgileGrid#setToolTipText(String)
	 * @see org.agilemore.agilegrid.IContentProvider#getTooltip(int, int)
	 */
	public String getToolTipText() {
		checkWidget();

		return nativeTooltip;
	}

	/**
	 * Resizes the given column to its optimal width.
	 * 
	 * Is also called if user doubleclicks in the resize area of a resizable
	 * column.
	 * 
	 * The optimal width is determined by asking the CellRenderers for the
	 * visible cells of the column for the optimal with and taking the minimum
	 * of the results. Note that the optimal width is only determined for the
	 * visible area of the agile grid because otherwise this could take very
	 * long time.
	 * 
	 * @param column
	 *            The column to resize
	 * @return The optimal with that was determined or Integer.MIN_VALUE, if
	 *         column out of range.
	 */
	public int resizeColumnOptimal(int column) {
		checkWidget();

		int optWidth = 5;
		int width = 0;
		if (column >= 0 && column < layoutAdvisor.getColumnCount()) {
			GC gc = new GC(this);

			if (layoutAdvisor.isTopHeaderVisible()) {
				width = cellRendererProvider.getTopHeadRenderer(column)
						.getOptimalWidth(gc, -1, column);
				if (width > optWidth) {
					optWidth = width;
				}
			}

			for (int i = 0; i < layoutAdvisor.getFixedRowCount(); i++) {
				width = cellRendererProvider.getCellRenderer(i, column)
						.getOptimalWidth(gc, i, column);
				if (width > optWidth) {
					optWidth = width;
				}
			}

			for (int i = topRow; i < topRow + rowsVisible; i++) {
				width = cellRendererProvider.getCellRenderer(i, column)
						.getOptimalWidth(gc, i, column);
				if (width > optWidth) {
					optWidth = width;
				}
			}
			gc.dispose();
			layoutAdvisor.setColumnWidth(column, optWidth);
			return optWidth;
		} else if (column == -1) {
			GC gc = new GC(this);
			for (int i = 0; i < layoutAdvisor.getFixedRowCount(); i++) {
				width = cellRendererProvider.getLeftHeadRenderer(i)
						.getOptimalWidth(gc, i, column);
				if (width > optWidth) {
					optWidth = width;
				}
			}

			for (int i = topRow; i < topRow + rowsVisible; i++) {
				width = cellRendererProvider.getLeftHeadRenderer(i)
						.getOptimalWidth(gc, i, column);
				if (width > optWidth) {
					optWidth = width;
				}
			}

			gc.dispose();
			layoutAdvisor.setLeftHeaderWidth(optWidth);
			return optWidth;
		}
		return Integer.MIN_VALUE;
	}

	/**
	 * Scrolls the agile grid so that the given cell is top left.
	 * 
	 * @param col
	 *            The column index of cell.
	 * @param row
	 *            The row index of cell.
	 */
	public void scroll(int row, int col) {
		checkWidget();

		if (col < 0 || col >= layoutAdvisor.getColumnCount() || row < 0
				|| row >= layoutAdvisor.getRowCount())
			return;

		int oldTopRow = topRow;
		int oldLeftColumn = leftColumn;
		topRow = row;
		leftColumn = col;
		if (oldTopRow != topRow || oldLeftColumn != leftColumn) {
			redraw();
		}
	}

	/**
	 * Scrolls the agile grid so that the focus cell is fully visible.
	 * 
	 * @param needRecalculate
	 *            Need to recalculate the metrics. for example topRow,leftColumn
	 *            etc.
	 */
	public void scrollToFocus(boolean needRecalculate) {
		boolean change = false;

		if (needRecalculate) {
			this.calculateMetrics();
		}

		Cell focusCell = cellSelectionManager.getFocusCell();
		if (focusCell == null || focusCell == Cell.NULLCELL) {
			return;
		}

		// vertical scroll allowed?
		if (getVerticalBar() != null) {
			if (focusCell.row < topRow
					&& focusCell.row >= layoutAdvisor.getFixedRowCount()) {
				topRow = focusCell.row;
				change = true;
			}

			if (rowsFullyVisible > 0
					&& focusCell.row >= topRow + rowsFullyVisible) {
				topRow = focusCell.row - rowsFullyVisible + 1;
				change = true;
			}
		}

		// horizontal scroll allowed?
		if (getHorizontalBar() != null) {
			if (focusCell.column < leftColumn
					&& focusCell.column >= layoutAdvisor.getFixedColumnCount()) {
				leftColumn = focusCell.column;
				change = true;
			}

			if (columnsFullyVisible > 0
					&& focusCell.column >= leftColumn + columnsFullyVisible) {
				leftColumn = focusCell.column - columnsFullyVisible + 1;
				change = true;
			}
		}

		if (change)
			redraw();
	}

	/**
	 * Scrolls the agile grid so that the focus cell is fully visible.
	 */
	public void scrollToFocus() {
		scrollToFocus(false);
	}

	/**
	 * Notify the cell double clicked listener with the cell double clicked
	 * event.
	 * 
	 * @param row
	 *            The row index of double clicked cell.
	 * @param col
	 *            The column index of double clicked cell.
	 * @param statemask
	 *            the state of the keyboard modifier keys at the time the event
	 *            was generated, as defined by the key code constants in class
	 *            <code>SWT</code>.
	 */
	protected void fireCellDoubleClicked(CellDoubleClickEvent event) {
		for (int i = 0; i < cellDoubleClickListeners.size(); i++) {
			((ICellDoubleClickListener) cellDoubleClickListeners.get(i))
					.cellDoubleClicked(event);
		}
	}

	/**
	 * Notify the cell resize listener with the new size of column.
	 * 
	 * @param col
	 *            The column index of resized column.
	 * @param newSize
	 *            The new size of column.
	 */
	protected void fireColumnResize(int col, int newSize) {
		for (int i = 0; i < cellResizeListeners.size(); i++) {
			((ICellResizeListener) cellResizeListeners.get(i)).columnResized(
					col, newSize);
		}
	}

	/**
	 * Notify the row resize listener with the new size of row.
	 * 
	 * @param row
	 *            The column index of resized column.
	 * @param newSize
	 *            The new size of column.
	 */
	protected void fireRowResize(int row, int newSize) {
		for (int i = 0; i < cellResizeListeners.size(); i++) {
			((ICellResizeListener) cellResizeListeners.get(i)).rowResized(row,
					newSize);
		}
	}

	/**
	 * Adds a listener that is notified when a cell is resized. This happens
	 * when the mouse button is released after a resizing.
	 * 
	 * @param listener
	 *            The cell resize listener.
	 */
	public void addCellResizeListener(ICellResizeListener listener) {
		checkWidget();

		cellResizeListeners.add(listener);
	}

	/**
	 * Adds a listener that is notified when a cell is doubleClicked.
	 * 
	 * @param listener
	 *            The cell double clicked listener.
	 */
	public void addCellDoubleClickListener(ICellDoubleClickListener listener) {
		checkWidget();

		cellDoubleClickListeners.add(listener);
	}

	/**
	 * Removes the listener if present. Returns true, if found and removed from
	 * the list of listeners.
	 * 
	 * @param listener
	 *            The cell resize lister.
	 * @return true, if found and removed from the list of listeners, otherwise
	 *         false.
	 */
	public boolean removeCellResizeListener(ICellResizeListener listener) {
		checkWidget();

		return cellResizeListeners.remove(listener);
	}

	/**
	 * Removes the listener if present. Returns true, if found and removed from
	 * the list of listeners.
	 * 
	 * @param listener
	 * @return true, if found and removed from the list of listeners, otherwise
	 *         false.
	 */
	public boolean removeDoubleClickListener(ICellDoubleClickListener listener) {
		checkWidget();

		return cellDoubleClickListeners.remove(listener);
	}
	
	public void clearDoubleClickListeners(){
		cellDoubleClickListeners.clear();
	}

	/**
	 * Returns true if in "Multi Selection Mode". Mode is determined by style
	 * bits in the constructor (SWT.MULTI) or by <code>getStyle()</code>.
	 * 
	 * @return true if in "Multi Selection Mode", otherwise false.
	 */
	public boolean isMultiSelectMode() {
		checkWidget();

		return (getStyle() & SWT.MULTI) == SWT.MULTI;
	}

	/**
	 * Returns true if the selection is kept & shown even if the agile grid has
	 * no focus.
	 * 
	 * @return Returns true if the selection is kept & shown even if the agile
	 *         grid has no focus.
	 * @see #setShowSelectionWithoutFocus(boolean)
	 */
	protected boolean isShowSelectionWithoutFocus() {
		return (getStyle() & SWT.HIDE_SELECTION) != SWT.HIDE_SELECTION;
	}

	/**
	 * Sets the content provider. The content provider provides data to the
	 * agile grid.
	 * 
	 * @param contentProvider
	 *            The IContentProvider instance that provides the agile grid
	 *            with all necessary data!
	 * @see org.agilemore.agilegrid.IContentProvider for more information.
	 */
	public void setContentProvider(IContentProvider contentProvider) {
		checkWidget();

		if (this.contentProvider != null) {
			this.contentProvider
					.removePropertyChangeListener(this.propertyChangeListener);
		}

		this.contentProvider = contentProvider;
		this.contentProvider
				.addPropertyChangeListener(this.propertyChangeListener);

		Cell focusCell = cellSelectionManager.getFocusCell();
		focusCell.column = Integer.MIN_VALUE;
		focusCell.row = Integer.MIN_VALUE;
		cellSelectionManager.clearSelectionWithoutRedraw();

		// implement autoscrolling if needed:
		if ((getStyle() & SWTX.AUTO_SCROLL) == SWTX.AUTO_SCROLL)
			updateScrollbarVisibility();

		redraw();
	}

	/**
	 * returns the current content provider.
	 * 
	 * @return The content provider provides data to the agile grid.
	 */
	public IContentProvider getContentProvider() {
		return contentProvider;
	}

	/**
	 * update the visibility of horizontal or vertical scroll bar.
	 */
	protected void updateScrollbarVisibility() {
		try {
			Rectangle actualSize = getClientArea();
			// vertical:
			boolean showVertBar = false;
			int theoreticalHeight = 1;
			for (int i = 0; i < layoutAdvisor.getRowCount(); i++) {
				theoreticalHeight += layoutAdvisor.getRowHeight(i);
				if (theoreticalHeight > actualSize.height) {
					showVertBar = true;
					break;
				}
			}
			getVerticalBar().setVisible(showVertBar);
			// horizontal:
			int theoreticalWidth = 0;
			for (int i = 0; i < layoutAdvisor.getColumnCount(); i++)
				theoreticalWidth += getColumnWidth(i);
			getHorizontalBar().setVisible(actualSize.width < theoreticalWidth);
		} catch (Exception e) {
			e.printStackTrace(); 
		}
	}

	/**
	 * Calculate the width of column.
	 * 
	 * @param col
	 *            The column index of agile grid.
	 * @return the width of column.
	 */
	public int getColumnWidth(int col) {
		checkWidget();

		if (col == layoutAdvisor.getColumnCount() - 1
				&& (getStyle() & SWTX.FILL_WITH_LASTCOL) != 0) {
			// expand the width to grab all the remaining space with this last
			// col.
			Rectangle clientArea = getClientArea();
			int remaining = clientArea.x + clientArea.width - linePixels
					- getColumnLeft(col);
			return Math.max(remaining, layoutAdvisor.getColumnWidth(col));
		} else
			return layoutAdvisor.getColumnWidth(col);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Widget#getStyle()
	 */
	public int getStyle() {
		checkWidget();

		return style;
	}

	/**
	 * Overwrites the style bits that determine certain behavior of agile grid.
	 * Note that not all style bits can be changed after agile grid is created.
	 * 
	 * @param style
	 *            The updated style bits (ORed together). Possibilities:
	 *            <ul>
	 *            <li><b>SWTX.FILL_WITH_LASTCOL</b> - Makes the agile grid
	 *            enlarge the last column to always fill all space.</li> <li>
	 *            <b>SWTX.FILL_WITH_DUMMYCOL</b> - Makes the agile grid fill any
	 *            remaining space with dummy columns to fill all space.</li> 
	 *            <li><b>SWT.FLAT</b> - Does not paint a dark outer border line.
	 *            </li> <li><b>SWT.MULTI</b> - Sets the "Multi Selection Mode".
	 *            In this mode, more than one cell or row can be selected. The
	 *            user can achieve this by shift-click and ctrl-click. The
	 *            selected cells/rows can be scattored ofer the complete agile
	 *            grid. If you pass false, only a single cell or row can be
	 *            selected. This mode can be combined with the
	 *            "Row Selection Mode".</li> <li><b>SWT.FULL_SELECTION</b> -
	 *            Sets the "Full Selection Mode". In the "Full Selection Mode",
	 *            the agile grid always selects a complete row. Otherwise, each
	 *            individual cell can be selected. This mode can be combined
	 *            with the "Multi Selection Mode".</li> <li>
	 *            <b>SWTX.EDIT_ON_KEY</b> - Activates a possibly present cell
	 *            editor on every keystroke. (Default: only ENTER). However,
	 *            note that editors can specify which events they accept.</li>
	 *            <li><b>SWT.HIDE_SELECTION</b> - Hides the selected cells when
	 *            the agile grid looses focus.</li> <li>
	 *            <b>SWTX.MARK_SELECTION_HEADERS</b> - Makes agile grid draw
	 *            left and top header cells in a different style when the
	 *            focused cell is in their row/column. This mimics the MS Excel
	 *            behavior that helps find the currently selected cell(s).</li>
	 *            </ul>
	 */
	public void setStyle(int style) {
		checkWidget();

		this.style = style;
	}

	/**
	 * Edits the indicated cell.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The Column index of cell.
	 */
	public void editCell(int row, int col) {
		editCell(row, col, null);
	}

	/**
	 * Edits the indicated cell.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @param hint
	 *            The hint used to select cell editor.
	 * 
	 */
	public void editCell(int row, int col, Object hint) {
		Cell cell = new Cell(this, row, col);
		Cell focusCell = getFocusCell();
		if (focusCell.equals(cell) && this.isCellEditorActive()) {
			return;
		}
		cellSelectionManager.focusCell(cell, 0);
		scrollToFocus(true);
		EditorActivationEvent acEvent = new EditorActivationEvent(cell, null);
		triggerEditorActivationEvent(acEvent, hint);
	}

	/**
	 * Cancel editing.
	 */
	public void cancelEditing() {
		agileGridEditor.cancelEditing();
	}

	/**
	 * Apply the value of the active cell editor if one is active.
	 * 
	 */
	public void applyEditorValue() {
		agileGridEditor.applyEditorValue();
	}

	/**
	 * Trigger the editor activation event of agile grid.
	 * 
	 * @param editorActivationEvent
	 *            The editor activation event of agile grid.
	 * @param hint
	 *            The hint used to select cell editor.
	 */
	public void triggerEditorActivationEvent(
			EditorActivationEvent editorActivationEvent, Object hint) {
		agileGridEditor
				.handleEditorActivationEvent(editorActivationEvent, hint);
	}

	/**
	 * Gets the agile grid editor.
	 * 
	 * @return the agileGridEditor The agile grid editor.
	 */
	public AgileGridEditor getAgileGridEditor() {
		checkWidget();

		return agileGridEditor;
	}

	/**
	 * Sets the agile grid editor.
	 * 
	 * @param agileGridEditor
	 *            the agileGridEditor to set
	 */
	public void setAgileGridEditor(AgileGridEditor agileGridEditor) {
		checkWidget();

		this.agileGridEditor = agileGridEditor;
	}

	/**
	 * Creates the agile grid editor used for editing cell contents.
	 * 
	 * @return the editor, or <code>null</code> if this agile grid does not
	 *         support editing cell contents.
	 */
	protected AgileGridEditor createAgileGridEditor() {
		AgileGridEditor editor = new AgileGridEditor(this,
				AgileGridEditor.TABBING_HORIZONTAL);
		return editor;
	}

	/**
	 * Gets the feature of agile grid editor.
	 * 
	 * @return the feature
	 */
	public int getAgileGridEditorFeature() {
		checkWidget();

		if (agileGridEditor != null) {
			return agileGridEditor.getFeature();
		}
		return 0;
	}

	/**
	 * Sets the feature of agile grid editor.
	 * 
	 * @param feature
	 *            the feature to set
	 */
	public void setAgileGridEditorFeature(int feature) {
		checkWidget();

		if (agileGridEditor != null) {
			agileGridEditor.setFeature(feature);
		}
	}

	/**
	 * Sets the cell editor provider.
	 * 
	 * @param cellEditorProvider
	 *            The cell editor provider.
	 */
	public void setCellEditorProvider(ICellEditorProvider cellEditorProvider) {
		checkWidget();

		if (agileGridEditor != null) {
			agileGridEditor.setCellEditorProvider(cellEditorProvider);
		}
	}

	/**
	 * Returns the cell editor provider.
	 * 
	 * @return The cell editor provider.
	 */
	public ICellEditorProvider getCellEditorProvider() {
		checkWidget();

		if (agileGridEditor != null) {
			return agileGridEditor.getCellEditorProvider();
		}
		return null;
	}

	/**
	 * Returns the specified neighbor of this cell, or <code>null</code> if no
	 * neighbor exists in the given direction. Direction constants can be
	 * combined by bitwise OR; for example, this method will return the cell to
	 * the upper-left of the current cell by passing {@link #ABOVE} |
	 * {@link #LEFT}. If <code>sameLevel</code> is <code>true</code>, only cells
	 * in sibling rows (under the same parent) will be considered.
	 * 
	 * @param directionMask
	 *            the direction mask used to identify the requested neighbor
	 *            cell
	 * @param sameLevel
	 *            if <code>true</code>, only consider cells from sibling rows
	 * @return the requested neighbor cell, or <code>null</code> if not found
	 */
	public Cell getNeighbor(Cell cell, int directionMask, boolean sameLevel) {
		checkWidget();

		Cell neighborCell = null;
		Cell ownerCell = layoutAdvisor.mergeInto(cell.row, cell.column);
		if (ownerCell != null)
			cell = ownerCell;

		if ((directionMask & ABOVE) == ABOVE && cell.row > 0) {
			neighborCell = layoutAdvisor.mergeInto(cell.row - 1, cell.column);
			if (neighborCell == null)
				neighborCell = new Cell(this, cell.row - 1, cell.column);
		} else if ((directionMask & BELOW) == BELOW
				&& cell.row < layoutAdvisor.getRowCount() - 1) {
			int incr = 1;
			neighborCell = layoutAdvisor
					.mergeInto(cell.row + incr, cell.column);
			while (cell.equals(neighborCell)) {
				incr++;
				if (cell.row + incr >= layoutAdvisor.getRowCount())
					break;
				neighborCell = layoutAdvisor.mergeInto(cell.row + incr,
						cell.column);
			}
			if (cell.equals(neighborCell))
				neighborCell = null;
		} else if ((directionMask & LEFT) == LEFT && cell.column > 0) {
			neighborCell = layoutAdvisor.mergeInto(cell.row, cell.column - 1);
		} else if ((directionMask & RIGHT) == RIGHT
				&& cell.column < layoutAdvisor.getColumnCount() - 1) {
			int incr = 1;
			neighborCell = layoutAdvisor
					.mergeInto(cell.row, cell.column + incr);
			while (cell.equals(neighborCell)) {
				incr++;
				if (cell.column + incr >= layoutAdvisor.getColumnCount())
					break;
				neighborCell = layoutAdvisor.mergeInto(cell.row, cell.column
						+ incr);
			}
			if (cell.equals(neighborCell))
				neighborCell = null;
		}

		return neighborCell;
	}

	/**
	 * Gets the visible rows.
	 * 
	 * @return the rowsVisible
	 */
	public int getRowsVisible() {
		checkWidget();

		return rowsVisible;
	}

	/**
	 * Gets the fully visible rows.
	 * 
	 * @return the fully visible rows.
	 */
	public int getRowsFullyVisible() {
		checkWidget();

		return rowsFullyVisible;
	}

	/**
	 * Gets the visible columns.
	 * 
	 * @return the columnsVisible
	 */
	public int getColumnsVisible() {
		checkWidget();

		return columnsVisible;
	}

	/**
	 * Get the fully visible columns.
	 * 
	 * @return the fully visible columns.
	 */
	public int getColumnsFullyVisible() {
		checkWidget();

		return columnsFullyVisible;
	}

	/**
	 * Gets the cell renderer provider which provides the cell renderer when
	 * rendering a cell.
	 * 
	 * @return the cellRendererProvider
	 */
	public ICellRendererProvider getCellRendererProvider() {
		checkWidget();

		return cellRendererProvider;
	}

	/**
	 * Sets the cell renderer provider which provides the cell renderer when
	 * rendering a cell.
	 * 
	 * @param cellRendererProvider
	 *            the cellRendererProvider to set
	 */
	public void setCellRendererProvider(
			ICellRendererProvider cellRendererProvider) {
		checkWidget();

		this.cellRendererProvider = cellRendererProvider;
	}

	/**
	 * Sets the layout advisor which provides the layout advice when painting
	 * the agile grid.
	 * 
	 * @return the layoutAdvisor
	 */
	public ILayoutAdvisor getLayoutAdvisor() {
		checkWidget();

		return layoutAdvisor;
	}

	/**
	 * Sets the layout advisor which provides the layout advice when painting
	 * the agile grid.
	 * 
	 * @param layoutAdvisor
	 *            the layoutAdvisor to set
	 */
	public void setLayoutAdvisor(ILayoutAdvisor layoutAdvisor) {
		checkWidget();

		if (this.layoutAdvisor != null) {
			this.layoutAdvisor
					.removePropertyChangeListener(this.propertyChangeListener);
		}

		this.layoutAdvisor = layoutAdvisor;
		this.layoutAdvisor
				.addPropertyChangeListener(this.propertyChangeListener);

		Cell focusCell = cellSelectionManager.getFocusCell();
		focusCell.column = Integer.MIN_VALUE;
		focusCell.row = Integer.MIN_VALUE;
		cellSelectionManager.clearSelectionWithoutRedraw();

		// implement autoscrolling if needed:
		if ((getStyle() & SWTX.AUTO_SCROLL) == SWTX.AUTO_SCROLL)
			updateScrollbarVisibility();

		redraw();
	}

	/**
	 * Gets the content of given cell indicated by index of row and column.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return The content of given cell.
	 */
	public Object getContentAt(int row, int col) {
		checkWidget();

		if (contentProvider == null)
			return "";
		ICompositorStrategy compositorStrategy = layoutAdvisor
				.getCompositorStrategy();
		if (compositorStrategy != null)
			row = compositorStrategy.mapRowIndexToContent(row);
		return contentProvider.getContentAt(row, col);
	}

	/**
	 * Sets the content of given cell indicated by index of row and column.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @param content
	 *            The content that will be set to the indicated cell.
	 */
	public void setContentAt(int row, int col, Object content) {
		checkWidget();

		ICompositorStrategy compositorStrategy = layoutAdvisor
				.getCompositorStrategy();
		if (compositorStrategy != null)
			row = compositorStrategy.mapRowIndexToContent(row);
		contentProvider.setContentAt(row, col, content);
	}

	/**
	 * Create a default layout advisor for agile grid.
	 * 
	 * @return Layout advisor for agile grid.
	 */
	private ILayoutAdvisor createLayoutAdvisor() {
		return new DefaultLayoutAdvisor(this);
	}

	private IContentProvider createContentProvider() {
		return new DefaultContentProvider();
	}

	/**
	 * Create a default cell renderer provider for agile grid.
	 * 
	 * @return Cell renderer provider for agile grid;
	 */
	private ICellRendererProvider createCellRendererProvider() {
		return new DefaultCellRendererProvider(this);
	}

	/**
	 * Create a default focus cell manager for agile grid.
	 * 
	 * @return focus cell manager for agile grid.
	 */
	private CellSelectionManager createFocusCellManager() {
		return new CellSelectionManager(this, new CellNavigationStrategy());
	}

	/**
	 * Sets navigation strategy for agilegrid.
	 * 
	 * @param cellNavigationStrategy
	 *            The cell navigation strategy.
	 */
	public void setCellNavigationStrategy(
			ICellNavigationStrategy cellNavigationStrategy) {
		this.cellSelectionManager.setNavigationStrategy(cellNavigationStrategy);
	}

	/**
	 * Returns the navigation strategy for agilegrid.
	 * 
	 * @return navigationStrategy The cell navigation strategy.
	 */
	public ICellNavigationStrategy getCellNavigationStrategy() {
		return this.cellSelectionManager.getNavigationStrategy();
	}

	/**
	 * Calculate the coordinate y for the row.
	 * 
	 * @param row
	 *            The row number.
	 * @return The coordinate y for the row.
	 */
	private int getRowTop(int row) {
		int y = linePixels;

		if (layoutAdvisor.isTopHeaderVisible()) {
			if (row == -1) {
				return y;
			}
			y += layoutAdvisor.getTopHeaderHeight();
			y += linePixels;
		} else {
			if (row == -1) {
				return Integer.MIN_VALUE;
			}
		}

		int fixedRowCount = layoutAdvisor.getFixedRowCount();
		if (row < fixedRowCount) {
			for (int i = 0; i < row; i++) {
				y += layoutAdvisor.getRowHeight(i);
				y += linePixels;
			}
			return y;
		}

		y = getFixedHeight();
		if (row >= fixedRowCount && row < topRow) {
			for (int i = topRow - 1; i >= row; i--) {
				y -= linePixels;
				y -= layoutAdvisor.getRowHeight(row);
			}
			return y;
		}

		for (int i = topRow; i < row; i++) {
			y += layoutAdvisor.getRowHeight(i);
			y += linePixels;
		}

		return y;
	}

	/**
	 * Returns the area that is occupied by the given cell. Take into account
	 * any cell span.
	 * <p>
	 * This version is an optimization if the x value is known. Use the version
	 * with just col and row as parameter if this is not known.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @param startX
	 *            The left horizontal start value.
	 * @param startY
	 *            The top vertical start value.
	 * @return The area of a cell
	 */
	private Rectangle getCellRect(int row, int col, int startX, int startY) {
		Cell valid = getValidCell(row, col);

		if (valid == Cell.NULLCELL || valid.row != row || valid.column != col) {
			return new Rectangle(0, 0, 0, 0);
		}

		Rectangle bound = getCellRectIgnoreSpan(row, col, startX, startY);
		// determine the cells that are contained:
		int spanRow = row + 1;
		int fixedRowCount = layoutAdvisor.getFixedRowCount();
		int totalRowCount = layoutAdvisor.getRowCount();
		while (getValidCell(spanRow, col).equals(valid)
				&& spanRow < totalRowCount) {
			if (spanRow >= fixedRowCount && spanRow < topRow) {
				spanRow++;
				continue;
			}
			bound.height += layoutAdvisor.getRowHeight(spanRow);
			bound.height += linePixels;
			spanRow++;
		}

		int spanCol = col + 1;
		int fixedColumnCount = layoutAdvisor.getFixedColumnCount();
		int totalColumnCount = layoutAdvisor.getColumnCount();
		while (getValidCell(row, spanCol).equals(valid)
				&& spanCol < totalColumnCount) {
			if (spanCol >= fixedColumnCount && spanCol < leftColumn) {
				spanCol++;
				continue;
			}
			bound.width += getColumnWidth(spanCol);
			bound.width += linePixels;
			spanCol++;
		}

		return bound;
	}

	/**
	 * Adds the selection changed listener.
	 * 
	 * @param selectionChangedListener
	 *            The selection changed listener.
	 */
	public void addSelectionChangedListener(
			ISelectionChangedListener selectionChangedListener) {
		checkWidget();

		cellSelectionManager
				.addSelectionChangedListener(selectionChangedListener);
	}

	/**
	 * Removes the selection changed listener.
	 * 
	 * @param listener
	 *            The selection changed listener.
	 * @return true if removing the selection changed listener success, false
	 *         otherwise.
	 * 
	 * @see org.agilemore.agilegrid.CellSelectionManager#removeSelectionChangedListener(org.agilemore.agilegrid.ISelectionChangedListener)
	 */
	public boolean removeSelectionChangedListener(
			ISelectionChangedListener listener) {
		checkWidget();

		return this.cellSelectionManager
				.removeSelectionChangedListener(listener);
	}

	/**
	 * Adds the focus cell changed listener.
	 * 
	 * @param listener
	 *            The focus cell changed listener.
	 * 
	 * @see org.agilemore.agilegrid.CellSelectionManager#addFocusCellChangedListener(org.agilemore.agilegrid.IFocusCellChangedListener)
	 */
	public void addFocusCellChangedListener(IFocusCellChangedListener listener) {
		checkWidget();

		this.cellSelectionManager.addFocusCellChangedListener(listener);
	}

	/**
	 * Removes the focus cell changed listener.
	 * 
	 * @param listener
	 *            The focus cell changed listener.
	 * 
	 * @return true if removing the focus cell changed listener success, false
	 *         otherwise.
	 * 
	 * @see org.agilemore.agilegrid.CellSelectionManager#removeFocusCellChangedListener(org.agilemore.agilegrid.IFocusCellChangedListener)
	 */
	public boolean removeFocusCellChangedListener(
			IFocusCellChangedListener listener) {
		checkWidget();

		return this.cellSelectionManager
				.removeFocusCellChangedListener(listener);
	}

	/**
	 * Sets the focus cell.
	 * 
	 * @param cell
	 *            The focus cell to be set.
	 */
	public void focusCell(Cell cell) {
		checkWidget();

		cellSelectionManager.focusCell(cell, 0);
	}

	/**
	 * Gets the selected cells.
	 * 
	 * @return the cells selected.
	 */
	public Cell[] getCellSelection() {
		checkWidget();

		return cellSelectionManager.getCellSelection();
	}

	/**
	 * Sets the selected cells.
	 * 
	 * @param cells
	 *            The selected cells.
	 */
	public void selectCells(Cell[] cells) {
		checkWidget();

		cellSelectionManager.selectCells(cells);
	}

	/**
	 * Clears the current selection.
	 */
	public void clearSelection() {
		checkWidget();

		cellSelectionManager.clearSelection();
	}

	/**
	 * Gets the focus cell.
	 * 
	 * @return The focus cell.
	 */
	public Cell getFocusCell() {
		checkWidget();

		return this.cellSelectionManager.getFocusCell();
	}

	/**
	 * Gets the pixels of line width.
	 * 
	 * @return the linePixels
	 */
	public int getLinePixels() {
		checkWidget();

		return this.linePixels;
	}

	/**
	 * Sets the pixels of line width.
	 * 
	 * @param linePixels
	 *            the linePixels to set
	 */
	public void setLinePixels(int linePixels) {
		checkWidget();

		this.linePixels = linePixels;
	}

	/**
	 * Gets the resizing area size.
	 * 
	 * @return the resizeAreaSize
	 */
	int getResizeAreaSize() {
		return this.resizeAreaSize;
	}

	/**
	 * Sets the resizing area size.
	 * 
	 * @param resizeAreaSize
	 *            the resizeAreaSize to set
	 */
	void setResizeAreaSize(int resizeAreaSize) {
		this.resizeAreaSize = resizeAreaSize;
	}

	/**
	 * Adds the given listener, it is to be notified when the cell editor is
	 * activated or deactivated.
	 * 
	 * @param listener
	 *            the listener to add
	 */
	public void addEditorActivationListener(IEditorActivationListener listener) {
		checkWidget();

		if (agileGridEditor != null) {
			agileGridEditor.addEditorActivationListener(listener);
		}
	}

	/**
	 * Removes the given listener.
	 * 
	 * @param listener
	 *            the listener to remove
	 */
	public void removeEditorActivationListener(
			IEditorActivationListener listener) {
		checkWidget();

		if (agileGridEditor != null) {
			agileGridEditor.removeEditorActivationListener(listener);
		}
	}

	/**
	 * Gets the activation strategy of editor.
	 * 
	 * @return the editorActivationStrategy
	 */
	public EditorActivationStrategy getEditorActivationStrategy() {
		checkWidget();

		if (agileGridEditor != null) {
			agileGridEditor.getEditorActivationStrategy();
		}
		return null;
	}

	/**
	 * Sets the activation strategy of editor.
	 * 
	 * @param editorActivationStrategy
	 *            the editorActivationStrategy to set
	 */
	public void setEditorActivationStrategy(
			EditorActivationStrategy editorActivationStrategy) {
		checkWidget();

		if (agileGridEditor != null) {
			agileGridEditor
					.setEditorActivationStrategy(editorActivationStrategy);
		}
	}
}