/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */
package org.agilemore.agilegrid;

import java.util.EventObject;

import org.eclipse.swt.events.MouseEvent;

/**
 * When the cell is double clicked by the left button of mouse, the cell double
 * click event will be fired.
 * 
 * @author fourbroad
 * 
 */
public class CellDoubleClickEvent extends EventObject {

	/**
	 * the double click mouse event.
	 */
	private MouseEvent mouseEvent;

	/**
	 * 
	 */
	private static final long serialVersionUID = -3900294537776357856L;

	/**
	 * the cell double click event constructor.
	 * 
	 * @param source
	 *            The source of event.
	 * @param mouseEvent
	 *            The double click mouse event.
	 */
	public CellDoubleClickEvent(Cell source, MouseEvent mouseEvent) {
		super(source);
		this.mouseEvent = mouseEvent;
	}

	/**
	 * @return the mouseEvent
	 */
	public MouseEvent getMouseEvent() {
		return this.mouseEvent;
	}

	/**
	 * @param mouseEvent the mouseEvent to set
	 */
	public void setMouseEvent(MouseEvent mouseEvent) {
		this.mouseEvent = mouseEvent;
	}

}
