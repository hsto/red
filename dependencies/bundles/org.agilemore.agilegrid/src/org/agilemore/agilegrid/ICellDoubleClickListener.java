/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * When a cell is double clicked, the instance of
 * <code>ICellDoubleClickListener</code> registered with the agile grid will be
 * notified with the instance of <code>CellDoubleClickEvent</code>.
 * 
 * @author fourbroad
 * 
 */
public interface ICellDoubleClickListener {

	/**
	 * Is called if a cell is double clicked.
	 * 
	 * @param event
	 *            The cell double click event.
	 */
	public void cellDoubleClicked(CellDoubleClickEvent event);

}
