/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */
package org.agilemore.agilegrid;

import java.util.EventObject;

/**
 * When focus cell is changed, a instance of this class will be fired and notify
 * the focus cell changed listeners.
 * 
 * @author fourbroad
 * 
 */
public class FocusCellChangedEvent extends EventObject {
	private static final long serialVersionUID = -5047979651521824055L;

	/**
	 * New focus cell.
	 */
	private Cell newFocusCell;

	/**
	 * Old focus cell.
	 */
	private Cell oldFocusCell;

	/**
	 * Creates a new focus cell changed event.
	 * 
	 * @param source
	 *            The source of event.
	 * 
	 * @param newFocusCell
	 *            The new focus cell.
	 * @param oldFocusCell
	 *            The old focus cell.
	 */
	public FocusCellChangedEvent(Object source, Cell newFocusCell,
			Cell oldFocusCell) {
		super(source);
		this.newFocusCell = newFocusCell;
		this.oldFocusCell = oldFocusCell;
	}

	/**
	 * Returns the new focus cell.
	 * 
	 * @return the newFocusCell
	 */
	public Cell getNewFocusCell() {
		return this.newFocusCell;
	}

	/**
	 * Return the old focus cell.
	 * 
	 * @return the oldFocusCell
	 */
	public Cell getOldFocusCell() {
		return this.oldFocusCell;
	}

}
