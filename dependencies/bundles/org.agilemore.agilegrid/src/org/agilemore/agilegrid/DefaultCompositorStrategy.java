/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.util.Collections;
import java.util.Vector;

/**
 * This class provides the default implementations of the methods described by
 * the <code>ICompositorStrategy</code> interface.
 * 
 * @author fourbroad
 * 
 */
public class DefaultCompositorStrategy implements ICompositorStrategy {
	/**
	 * The layout advisor that this class will get some layout information form.
	 */
	private ILayoutAdvisor layoutAdvisor;

	/**
	 * Order of agile grid column.
	 */
	private int ordered = ColumnSortComparator.SORT_NONE;

	/**
	 * Agile grid column to be ordered.
	 */
	private int sortColumn = -1;

	/**
	 * Current agile grid sort comparator.
	 */
	private ColumnSortComparator currentSortComparator = null;

	/**
	 * Row mapping between the agile grid and content.
	 */
	private Vector<Integer> rowMapping;

	/**
	 * Creates a default compositor strategy.
	 * 
	 * @param layoutAdvisor
	 *            The layout advisor.
	 */
	public DefaultCompositorStrategy(ILayoutAdvisor layoutAdvisor) {
		this.layoutAdvisor = layoutAdvisor;

		int numberOfElems = layoutAdvisor.getRowCount()
				- layoutAdvisor.getFixedRowCount();
		rowMapping = new Vector<Integer>(numberOfElems);

		// SORT_NONE is default, so direclty map the rows 1:1
		int fixedRowCount = layoutAdvisor.getFixedRowCount();
		for (int i = 0; i < numberOfElems; i++)
			rowMapping.add(i, new Integer(i + fixedRowCount));
	}

	/**
	 * Resets the row mapping.
	 */
	public void resetRowMapping() {
		int numberOfElems = layoutAdvisor.getRowCount();
		rowMapping = new Vector<Integer>(numberOfElems);
	}

	/**
	 * Returns the current soft state.
	 * 
	 * @return Returns the current sort state of the sorted model. Can be:
	 *         <p>
	 *         <ul>
	 *         <li>SORT_NONE: Unsorted (default)
	 *         <li>SORT_UP: Sorted with largest value up
	 *         <li>SORT_DOWN: Sorted with largest value down.
	 *         </ul>
	 */
	public int getSortState() {
		return ordered;
	}

	/**
	 * Sorts the model elements so that the retrieval methods by index return
	 * the content ordered in the given direction.
	 * <p>
	 * Note: To make the agile grid reflect this sorting, it must be
	 * refreshed/redrawn!
	 * <p>
	 * Note: Often it is desired that there is some visual sign of how the
	 * sorting is.
	 * 
	 * @param comparator
	 *            The ColumnSortComparator that knows how to sort the rows!
	 */
	public void sort(ColumnSortComparator comparator) {
		Collections.sort(rowMapping, comparator);

		ordered = comparator.getSortDirection();

		if (ordered == ColumnSortComparator.SORT_NONE)
			setSortColumn(-1);
		else
			setSortColumn(comparator.getColumnToSortOn());
		currentSortComparator = comparator;
	}

	/**
	 * Returns the column that is used for sorting, or -1 if no sorting is
	 * present or sorting is SORT_NONE.
	 * 
	 * @return The column that is used for sorting.
	 */
	public int getSortColumn() {
		return sortColumn;
	}

	/**
	 * Sets the sort column for this model. Note that this should equal the real
	 * one defined by the comparator given to sort().
	 * 
	 * @param column
	 *            The column that is used for sorting.
	 */
	public void setSortColumn(int column) {
		sortColumn = column;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ICompositorStrategy#mapRowIndexToModel(int)
	 */
	public int mapRowIndexToContent(int shownRow) {
		// we only map non-fixed cells:
		if (shownRow < layoutAdvisor.getFixedRowCount())
			return shownRow;

		// if new elements were added, update the size of the mapping vector.
		if (shownRow - getFixedRowCount() >= rowMapping.size()) {
			int fixedRowCount = layoutAdvisor.getFixedRowCount();
			for (int i = rowMapping.size(); i < layoutAdvisor.getRowCount()
					- fixedRowCount; i++)
				rowMapping.add(i, new Integer(i + fixedRowCount));
		}
		int bodyRow = shownRow - getFixedRowCount();
		if (bodyRow < 0 || bodyRow >= rowMapping.size())
			return shownRow;
		int mappedRow = ((Integer) rowMapping.get(bodyRow)).intValue();
		if (mappedRow >= layoutAdvisor.getRowCount() || mappedRow < 0) {
			resetRowMapping();
			if (currentSortComparator != null)
				sort(currentSortComparator);
			return mapRowIndexToContent(shownRow);
		}
		return mappedRow;
	}

	/* (non-Javadoc)
	 * @see org.agilemore.agilegrid.ICompositorStrategy#mapRowIndexToAgileGrid(int)
	 */
	public int mapRowIndexToAgileGrid(int modelRow) {
		// we only map non-fixed cells:
		if (modelRow < layoutAdvisor.getFixedRowCount())
			return modelRow;

		for (int i = 0; i < rowMapping.size(); i++) {
			Integer im = (Integer) rowMapping.get(i);
			if (im.intValue() == modelRow)
				return i + getFixedRowCount();
		}
		return modelRow;
	}

	/**
	 * Returns the total number of fixed rows in the agile grid. the total number is
	 * the sum of header rows and selectable fixed rows.
	 * 
	 * @return The total number of fixed rows in the agile grid.
	 */
	private int getFixedRowCount() {
		return layoutAdvisor.getFixedRowCount();
	}

}
