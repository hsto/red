/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

/**
 * This class provides the code that makes the agile grid sort when the user clicks
 * on the agile grid header.
 * 
 * @author fourbroad
 */
public class ColumnSortOnClick extends MouseAdapter {

	/**
	 * The agile grid this class works for.
	 */
	private AgileGrid agileGrid;

	/**
	 * The agile grid sort comparator as used to compare the element of content
	 * provider.
	 */
	private ColumnSortComparator sortComparator;

	/**
	 * Creates a new instance of class ColumnSortOnClick.
	 * 
	 * @param agileGrid
	 *            The agile grid this instance is working for.
	 * @param sortComparator
	 *            The agile grid sort comparator.
	 */
	public ColumnSortOnClick(AgileGrid agileGrid, ColumnSortComparator sortComparator) {
		this.agileGrid = agileGrid;
		this.sortComparator = sortComparator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse.swt.events.MouseEvent)
	 */
	@Override
	public void mouseDown(MouseEvent e) {
		if (e.button != 1) {
			return;
		}
		
		Cell cell = agileGrid.getTopHeaderCell(e.x, e.y);
		if (cell == Cell.NULLCELL || !isSortable(cell.column))
			return;

		int left = agileGrid.getColumnLeft(cell.column);
		int right = left + agileGrid.getColumnWidth(cell.column) - 1;
		int pad = agileGrid.getResizeAreaSize() / 2;
		if (e.x - left < pad || right - e.x < pad) {
			return;
		}

		ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
		ICompositorStrategy compositorStrategy = layoutAdvisor
				.getCompositorStrategy();
		if (compositorStrategy != null) {
			// implement the sorting when clicking on the header.
			int type = ColumnSortComparator.SORT_UP;
			if (compositorStrategy.getSortColumn() == cell.column) {
				if (compositorStrategy.getSortState() == ColumnSortComparator.SORT_UP) {
					type = ColumnSortComparator.SORT_DOWN;
				} else if (compositorStrategy.getSortState() == ColumnSortComparator.SORT_DOWN) {
					type = ColumnSortComparator.SORT_NONE;
				}
			}

			// update the comparator properly:
			sortComparator.setColumnToCompare(cell.column);
			sortComparator.setSortDirection(type);

			// perform the sorting
			compositorStrategy.sort(sortComparator);

			// needed to make the resorting visible!
			if(agileGrid.isCellEditorActive()) {
				agileGrid.getAgileGridEditor().cancelEditing();
			}
			agileGrid.redraw();
		}
	}

	/**
	 * Decides whether the mouse clicked column is sortable.
	 * 
	 * @param col
	 *            The column number.
	 * @return <code>true</code> if the column is sortable, <code>false</code>
	 *         otherwise.
	 */
	public boolean isSortable(int col) {
		Cell cell = null;
		ILayoutAdvisor layoutAdvisor = agileGrid.getLayoutAdvisor();
		int totalRowCount = layoutAdvisor.getRowCount();
		for (int row = 0; row < totalRowCount; row++) {
			cell = layoutAdvisor.mergeInto(row, col);
			if (cell != null && (cell.row != row || cell.column != col)) {
				return false;
			}
		}
		return true;
	}
}
