/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * Provides the cell editor to edit the value of agile grid cell.
 * 
 * @author fourbroad
 * 
 */
public interface ICellEditorProvider {

	/**
	 * Is the cell editable
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * 
	 * @return true if editable, false otherwise.
	 */
	public boolean canEdit(int row, int col);

	/**
	 * A agile grid cell will be "in place editable" if this method returns a
	 * valid cell editor for the given cell. For no edit functionality return
	 * null.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return An instance of CellEditor that will be responsible of showing an
	 *         editor control and writing back the changed value by calling
	 *         <code>saveCellEditorValue(CellEditor, Cell)</code>.
	 */
	public CellEditor getCellEditor(int row, int col);

	/**
	 * A agile grid cell will be "in place editable" if this method returns a
	 * valid cell editor for the given cell. For no edit functionality return
	 * null.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @param hint
	 *            The hint used to select cell editor.
	 * @return An instance of CellEditor that will be responsible of showing an
	 *         editor control and writing back the changed value by calling
	 *         <code>saveCellEditorValue(CellEditor, Cell)</code>.
	 */
	public CellEditor getCellEditor(int row, int col, Object hint);

	/**
	 * Initialize the editor. Frameworks like Databinding can hook in here and
	 * provide a customized implementation.
	 * <p>
	 * <b>Standard customers should not overwrite this method but
	 * {@link AbstractCellEditorProvider#getValue(Object)}</b>
	 * </p>
	 * 
	 * @param cellEditor
	 *            the cell editor
	 * @param cell
	 *            the cell the editor is working for
	 */
	public void initializeCellEditorValue(CellEditor cellEditor, Cell cell);

	/**
	 * Save the value of the cell editor back to the model. Frameworks like
	 * Databinding can hook in here and provide a customized implementation.
	 * <p>
	 * <b>Standard customers should not overwrite this method but
	 * {@link AbstractCellEditorProvider#setValue(Object, Object)}</b>
	 * </p>
	 * 
	 * @param cellEditor
	 *            the cell editor
	 * @param cell
	 *            the cell which the editor is working for
	 */
	public void saveCellEditorValue(CellEditor cellEditor, Cell cell);
}
