/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * When the row or column of agile grid is resized, this listener will be notified
 * with the new height or width.
 * 
 * @author fourbroad
 * 
 */
public interface ICellResizeListener {

	/**
	 * Is called when a row is resized. (but not when first row is resized!)
	 * 
	 * @param row
	 *            The row number.
	 * @param newHeight
	 *            The new height of row.
	 */
	public void rowResized(int row, int newHeight);

	/**
	 * Is called when a column is resized.
	 * 
	 * @param col
	 *            The column number.
	 * @param newWidth
	 *            The new width of column.
	 */
	public void columnResized(int col, int newWidth);
}
