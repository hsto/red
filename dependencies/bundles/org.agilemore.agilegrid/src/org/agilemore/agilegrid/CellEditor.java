/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;

/**
 * Abstract base class for cell editors. Implements property change listener
 * handling, and SWT window management.
 * <p>
 * Subclasses implement particular kinds of cell editors. This package contains
 * various specialized cell editors:
 * <ul>
 * <li><code>TextCellEditor</code> - for simple text strings</li>
 * <li><code>ColorCellEditor</code> - for colors</li>
 * <li><code>ComboBoxCellEditor</code> - value selected from drop-down combo box
 * </li>
 * <li><code>CheckboxCellEditor</code> - boolean valued checkbox</li>
 * <li><code>DialogCellEditor</code> - edit value using arbitrary dialog</li>
 * <li><code>PopupCellEditor</code> - edit value using popup box</li>
 * </ul>
 * </p>
 * 
 * @author fourbroad
 * 
 */
public abstract class CellEditor {
	/**
	 * List of cell editor cellEditorListeners (element type:
	 * <code>ICellEditorListener</code>).
	 */
	private ArrayList<ICellEditorListener> cellEditorListeners = new ArrayList<ICellEditorListener>(
			10);

	/**
	 * List of cell editor property change cellEditorListeners (element type:
	 * <code>IPropertyChangeListener</code>).
	 */
	private ArrayList<PropertyChangeListener> propertyChangeListeners = new ArrayList<PropertyChangeListener>(
			10);

	/**
	 * Indicates whether this cell editor's current value is valid.
	 */
	private boolean valid = false;

	/**
	 * Optional cell editor validator; <code>null</code> if none.
	 */
	private ICellEditorValidator validator = null;

	/**
	 * The error message string to display for invalid values; <code>null</code>
	 * if none (that is, the value is valid).
	 */
	private String errorMessage = null;

	/**
	 * Indicates whether this cell editor has been changed recently.
	 */
	private boolean dirty = false;

	/**
	 * This cell editor's control, or <code>null</code> if not created yet.
	 */
	private Control control = null;

	/**
	 * Default cell editor style
	 */
	private static final int defaultStyle = SWT.NONE;

	/**
	 * This cell editor's style
	 */
	private int style = defaultStyle;

	/**
	 * Struct-like layout data for cell editors, with reasonable defaults for
	 * all fields.
	 */
	public static class LayoutData {
		/**
		 * Horizontal alignment; <code>SWT.LEFT</code> by default.
		 */
		public int horizontalAlignment = SWT.LEFT;

		/**
		 * Indicates control grabs additional space; <code>true</code> by
		 * default.
		 */
		public boolean grabHorizontal = true;

		/**
		 * Minimum width in pixels; <code>50</code> pixels by default.
		 */
		public int minimumWidth = 50;
	}

	/**
	 * Property name for the copy action
	 */
	public static final String COPY = "copy"; //$NON-NLS-1$

	/**
	 * Property name for the cut action
	 */
	public static final String CUT = "cut"; //$NON-NLS-1$

	/**
	 * Property name for the delete action
	 */
	public static final String DELETE = "delete"; //$NON-NLS-1$

	/**
	 * Property name for the find action
	 */
	public static final String FIND = "find"; //$NON-NLS-1$

	/**
	 * Property name for the paste action
	 */
	public static final String PASTE = "paste"; //$NON-NLS-1$

	/**
	 * Property name for the redo action
	 */
	public static final String REDO = "redo"; //$NON-NLS-1$

	/**
	 * Property name for the select all action
	 */
	public static final String SELECT_ALL = "selectall"; //$NON-NLS-1$

	/**
	 * Property name for the undo action
	 */
	public static final String UNDO = "undo"; //$NON-NLS-1$

	/**
	 * Cell to be edited.
	 */
	protected Cell cell;

	/**
	 * The agile grid that the cell editor works for.
	 */
	protected AgileGrid agileGrid;

	/**
	 * Area to render the cell editor.
	 */
	protected Rectangle rect;

	/**
	 * Shows tool tip when mouse hover on this cell editor.
	 */
	protected String toolTip;

	/**
	 * Key press listener.
	 */
	private KeyListener keyListener;

	/**
	 * Key traverse listener.
	 */
	private TraverseListener travListener;

	/**
	 * Creates a new cell editor under the given parent control. The cell editor
	 * has no cell validator.
	 * 
	 * @param parent
	 *            the parent control
	 */
	protected CellEditor(AgileGrid parent) {
		this(parent, defaultStyle);
	}

	/**
	 * Creates a new cell editor under the given parent control. The cell editor
	 * has no cell validator.
	 * 
	 * @param parent
	 *            the parent control
	 * @param style
	 *            the style bits
	 */
	protected CellEditor(AgileGrid parent, int style) {
		this.style = style;
		this.agileGrid = parent;
		control = createControl(parent);
		if (control != null && !control.isDisposed()) {
			keyListener = new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					try {
						onKeyPressed(e);
					} catch (Exception ex) {
						// Do nothing
					}
				}
			};
			control.addKeyListener(keyListener);

			travListener = new TraverseListener() {
				public void keyTraversed(TraverseEvent e) {
					onTraverse(e);
				}
			};
			control.addTraverseListener(travListener);

			control.setVisible(false);
		}
	}

	/**
	 * Adds a listener to this cell editor. Has no effect if an identical
	 * listener is already registered.
	 * 
	 * @param listener
	 *            a cell editor listener
	 */
	public void addCellEditorListener(ICellEditorListener listener) {
		cellEditorListeners.add(listener);
	}

	/**
	 * Adds a property change listener to this cell editor. Has no effect if an
	 * identical property change listener is already registered.
	 * 
	 * @param listener
	 *            a property change listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeListeners.add(listener);
	}

	/**
	 * Creates the control for this cell editor under the given parent control.
	 * <p>
	 * This framework method must be implemented by concrete subclasses.
	 * </p>
	 * 
	 * @param parent
	 *            the parent control
	 * @return the new control, or <code>null</code> if this cell editor has no
	 *         control
	 */
	protected abstract Control createControl(AgileGrid parent);

	/**
	 * Hides this cell editor's control. Does nothing if this cell editor is not
	 * visible.
	 */
	public void deactivate() {
		if (control != null && !control.isDisposed()) {
			control.setVisible(false);
		}
	}

	/**
	 * disposes the editor and its components
	 */
	public void dispose() {
		if (control != null) {
			control.removeKeyListener(keyListener);
			control.removeTraverseListener(travListener);

			Control contr = control;
			control = null;
			contr.dispose();
		}
	}

	/**
	 * Returns this cell editor's value.
	 * <p>
	 * This framework method must be implemented by concrete subclasses.
	 * </p>
	 * 
	 * @return the value of this cell editor
	 * @see #getValue
	 */
	protected abstract Object doGetValue();

	/**
	 * Sets the focus to the cell editor's control.
	 * <p>
	 * This framework method must be implemented by concrete subclasses.
	 * </p>
	 * 
	 * @see #setFocus
	 */
	protected void doSetFocus() {
		if (control != null)
			control.setFocus();
	}

	/**
	 * Sets this cell editor's value.
	 * <p>
	 * This framework method must be implemented by concrete subclasses.
	 * </p>
	 * 
	 * @param value
	 *            the value of this cell editor
	 * @see #setValue
	 */
	protected abstract void doSetValue(Object value);

	/**
	 * Notifies all registered cell editor cellEditorListeners of an apply
	 * event. Only cellEditorListeners registered at the time this method is
	 * called are notified.
	 * 
	 * @see ICellEditorListener#applyEditorValue
	 */
	protected void fireApplyEditorValue() {
		for (int i = 0; i < cellEditorListeners.size(); i++) {
			final ICellEditorListener l = cellEditorListeners.get(i);
			// SafeRunnable.run(new SafeRunnable() {
			// public void run() {
			l.applyEditorValue();
			// }
			// });
		}
	}

	/**
	 * Notifies all registered cell editor cellEditorListeners that editing has
	 * been canceled.
	 * 
	 * @see ICellEditorListener#cancelEditor
	 */
	protected void fireCancelEditor() {
		for (int i = 0; i < cellEditorListeners.size(); i++) {
			final ICellEditorListener l = cellEditorListeners.get(i);
			// SafeRunnable.run(new SafeRunnable() {
			// public void run() {
			l.cancelEditor();
			// }
			// });
		}
	}

	/**
	 * Notifies all registered cell editor cellEditorListeners of a value
	 * change.
	 * 
	 * @param oldValidState
	 *            the valid state before the end user changed the value
	 * @param newValidState
	 *            the current valid state
	 * @see ICellEditorListener#editorValueChanged
	 */
	protected void fireEditorValueChanged(final boolean oldValidState,
			final boolean newValidState) {
		for (int i = 0; i < cellEditorListeners.size(); i++) {
			final ICellEditorListener l = cellEditorListeners.get(i);
			// SafeRunnable.run(new SafeRunnable() {
			// public void run() {
			l.editorValueChanged(oldValidState, newValidState);
			// }
			// });
		}
	}

	/**
	 * Notifies all registered property cellEditorListeners of an enablement
	 * change.
	 * 
	 * @param actionId
	 *            the id indicating what action's enablement has changed.
	 */
	protected void fireEnablementChanged(final String actionId) {
		for (int i = 0; i < propertyChangeListeners.size(); i++) {
			final PropertyChangeListener l = propertyChangeListeners.get(i);
			// SafeRunnable.run(new SafeRunnable() {
			// public void run() {
			l
					.propertyChange(new PropertyChangeEvent(this, actionId,
							null, null));
			// }
			// });
		}
	}

	/**
	 * Sets the style bits for this cell editor.
	 * 
	 * @param style
	 *            the SWT style bits for this cell editor
	 */
	public void setStyle(int style) {
		this.style = style;
	}

	/**
	 * Returns the style bits for this cell editor.
	 * 
	 * @return the style for this cell editor
	 */
	public int getStyle() {
		return style;
	}

	/**
	 * Returns the control used to implement this cell editor.
	 * 
	 * @return the control, or <code>null</code> if this cell editor has no
	 *         control
	 */
	public Control getControl() {
		return control;
	}

	/**
	 * Returns the current error message for this cell editor.
	 * 
	 * @return the error message if the cell editor is in an invalid state, and
	 *         <code>null</code> if the cell editor is valid
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Returns a layout data object for this cell editor. This is called each
	 * time the cell editor is activated and controls the layout of the SWT
	 * agile grid editor.
	 * <p>
	 * The default implementation of this method sets the minimum width to the
	 * control's preferred width. Subclasses may extend or reimplement.
	 * </p>
	 * 
	 * @return the layout data object
	 */
	public LayoutData getLayoutData() {
		LayoutData result = new LayoutData();
		Control control = getControl();
		if (control != null) {
			result.minimumWidth = control.computeSize(SWT.DEFAULT, SWT.DEFAULT,
					true).x;
		}
		return result;
	}

	/**
	 * Returns the input validator for this cell editor.
	 * 
	 * @return the input validator, or <code>null</code> if none
	 */
	public ICellEditorValidator getValidator() {
		return validator;
	}

	/**
	 * Returns this cell editor's value provided that it has a valid one.
	 * 
	 * @return the value of this cell editor, or <code>null</code> if the cell
	 *         editor does not contain a valid value
	 */
	public final Object getValue() {
		if (!valid) {
			return null;
		}

		return doGetValue();
	}

	/**
	 * Returns whether this cell editor is activated.
	 * 
	 * @return <code>true</code> if this cell editor's control is currently
	 *         activated, and <code>false</code> if not activated
	 */
	public boolean isActivated() {
		// Use the state of the visible style bit (getVisible()) rather than the
		// window's actual visibility (isVisible()) to get correct handling when
		// an ancestor control goes invisible, see bug 85331.
		return control != null && control.getVisible();
	}

	/**
	 * Returns <code>true</code> if this cell editor is able to perform the copy
	 * action.
	 * <p>
	 * This default implementation always returns <code>false</code>.
	 * </p>
	 * <p>
	 * Subclasses may override
	 * </p>
	 * 
	 * @return <code>true</code> if copy is possible, <code>false</code>
	 *         otherwise
	 */
	public boolean isCopyEnabled() {
		return false;
	}

	/**
	 * Returns whether the given value is valid for this cell editor. This cell
	 * editor's validator (if any) makes the actual determination.
	 * 
	 * @param value
	 *            the value to check for
	 * 
	 * @return <code>true</code> if the value is valid, and <code>false</code>
	 *         if invalid
	 */
	protected boolean isCorrect(Object value) {
		errorMessage = null;
		if (validator == null) {
			return true;
		}

		errorMessage = validator.isValid(value);
		return (errorMessage == null || errorMessage.equals(""));//$NON-NLS-1$
	}

	/**
	 * Returns <code>true</code> if this cell editor is able to perform the cut
	 * action.
	 * <p>
	 * This default implementation always returns <code>false</code>.
	 * </p>
	 * <p>
	 * Subclasses may override
	 * </p>
	 * 
	 * @return <code>true</code> if cut is possible, <code>false</code>
	 *         otherwise
	 */
	public boolean isCutEnabled() {
		return false;
	}

	/**
	 * Returns <code>true</code> if this cell editor is able to perform the
	 * delete action.
	 * <p>
	 * This default implementation always returns <code>false</code>.
	 * </p>
	 * <p>
	 * Subclasses may override
	 * </p>
	 * 
	 * @return <code>true</code> if delete is possible, <code>false</code>
	 *         otherwise
	 */
	public boolean isDeleteEnabled() {
		return false;
	}

	/**
	 * Returns whether the value of this cell editor has changed since the last
	 * call to <code>setValue</code>.
	 * 
	 * @return <code>true</code> if the value has changed, and
	 *         <code>false</code> if unchanged
	 */
	public boolean isDirty() {
		return dirty;
	}

	/**
	 * Marks this cell editor as dirty.
	 * 
	 */
	protected void markDirty() {
		dirty = true;
	}

	/**
	 * Returns <code>true</code> if this cell editor is able to perform the find
	 * action.
	 * <p>
	 * This default implementation always returns <code>false</code>.
	 * </p>
	 * <p>
	 * Subclasses may override
	 * </p>
	 * 
	 * @return <code>true</code> if find is possible, <code>false</code>
	 *         otherwise
	 */
	public boolean isFindEnabled() {
		return false;
	}

	/**
	 * Returns <code>true</code> if this cell editor is able to perform the
	 * paste action.
	 * <p>
	 * This default implementation always returns <code>false</code>.
	 * </p>
	 * <p>
	 * Subclasses may override
	 * </p>
	 * 
	 * @return <code>true</code> if paste is possible, <code>false</code>
	 *         otherwise
	 */
	public boolean isPasteEnabled() {
		return false;
	}

	/**
	 * Returns <code>true</code> if this cell editor is able to perform the redo
	 * action.
	 * <p>
	 * This default implementation always returns <code>false</code>.
	 * </p>
	 * <p>
	 * Subclasses may override
	 * </p>
	 * 
	 * @return <code>true</code> if redo is possible, <code>false</code>
	 *         otherwise
	 */
	public boolean isRedoEnabled() {
		return false;
	}

	/**
	 * Returns <code>true</code> if this cell editor is able to perform the
	 * select all action.
	 * <p>
	 * This default implementation always returns <code>false</code>.
	 * </p>
	 * <p>
	 * Subclasses may override
	 * </p>
	 * 
	 * @return <code>true</code> if select all is possible, <code>false</code>
	 *         otherwise
	 */
	public boolean isSelectAllEnabled() {
		return false;
	}

	/**
	 * Returns <code>true</code> if this cell editor is able to perform the undo
	 * action.
	 * <p>
	 * This default implementation always returns <code>false</code>.
	 * </p>
	 * <p>
	 * Subclasses may override
	 * </p>
	 * 
	 * @return <code>true</code> if undo is possible, <code>false</code>
	 *         otherwise
	 */
	public boolean isUndoEnabled() {
		return false;
	}

	/**
	 * Returns whether this cell editor has a valid value. The default value is
	 * false.
	 * 
	 * @return <code>true</code> if the value is valid, and <code>false</code>
	 *         if invalid
	 * 
	 * @see #setValueValid(boolean)
	 */
	public boolean isValueValid() {
		return valid;
	}

	/**
	 * Processes a focus lost event that occurred in this cell editor.
	 * <p>
	 * The default implementation of this framework method applies the current
	 * value and deactivates the cell editor. Subclasses should call this method
	 * at appropriate times. Subclasses may also extend or reimplement.
	 * </p>
	 */
	protected void focusLost() {
		if (isActivated()) {
			fireApplyEditorValue();
//			deactivate();
		}
	}

	/**
	 * Performs the copy action. This default implementation does nothing.
	 * <p>
	 * Subclasses may override
	 * </p>
	 */
	public void performCopy() {
	}

	/**
	 * Performs the cut action. This default implementation does nothing.
	 * <p>
	 * Subclasses may override
	 * </p>
	 */
	public void performCut() {
	}

	/**
	 * Performs the delete action. This default implementation does nothing.
	 * <p>
	 * Subclasses may override
	 * </p>
	 */
	public void performDelete() {
	}

	/**
	 * Performs the find action. This default implementation does nothing.
	 * <p>
	 * Subclasses may override
	 * </p>
	 */
	public void performFind() {
	}

	/**
	 * Performs the paste action. This default implementation does nothing.
	 * <p>
	 * Subclasses may override
	 * </p>
	 */
	public void performPaste() {
	}

	/**
	 * Performs the redo action. This default implementation does nothing.
	 * <p>
	 * Subclasses may override
	 * </p>
	 */
	public void performRedo() {
	}

	/**
	 * Performs the select all action. This default implementation does nothing.
	 * <p>
	 * Subclasses may override
	 * </p>
	 */
	public void performSelectAll() {
	}

	/**
	 * Performs the undo action. This default implementation does nothing.
	 * <p>
	 * Subclasses may override
	 * </p>
	 */
	public void performUndo() {
	}

	/**
	 * Removes the given listener from this cell editor. Has no affect if an
	 * identical listener is not registered.
	 * 
	 * @param listener
	 *            a cell editor listener
	 */
	public void removeListener(ICellEditorListener listener) {
		cellEditorListeners.remove(listener);
	}

	/**
	 * Removes the given property change listener from this cell editor. Has no
	 * affect if an identical property change listener is not registered.
	 * 
	 * @param listener
	 *            a property change listener
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeListeners.remove(listener);
	}

	/**
	 * Sets or clears the current error message for this cell editor.
	 * <p>
	 * No formatting is done here, the message to be set is expected to be fully
	 * formatted before being passed in.
	 * </p>
	 * 
	 * @param message
	 *            the error message, or <code>null</code> to clear
	 */
	protected void setErrorMessage(String message) {
		errorMessage = message;
	}

	/**
	 * Sets the focus to the cell editor's control.
	 */
	public void setFocus() {
		doSetFocus();
	}

	/**
	 * Sets the input validator for this cell editor.
	 * 
	 * @param validator
	 *            the input validator, or <code>null</code> if none
	 */
	public void setValidator(ICellEditorValidator validator) {
		this.validator = validator;
	}

	/**
	 * Sets this cell editor's value.
	 * 
	 * @param value
	 *            the value of this cell editor
	 */
	public final void setValue(Object value) {
		valid = isCorrect(value);
		dirty = false;
		doSetValue(value);
	}

	/**
	 * Sets the valid state of this cell editor. The default value is false.
	 * Subclasses should call this method on construction.
	 * 
	 * @param valid
	 *            <code>true</code> if the current value is valid, and
	 *            <code>false</code> if invalid
	 * 
	 * @see #isValueValid
	 */
	protected void setValueValid(boolean valid) {
		this.valid = valid;
	}

	/**
	 * The value has changed. Updates the valid state flag, marks this cell
	 * editor as dirty, and notifies all registered cell editor
	 * cellEditorListeners of a value change.
	 * 
	 * @param oldValidState
	 *            the valid state before the end user changed the value
	 * @param newValidState
	 *            the current valid state
	 * @see ICellEditorListener#editorValueChanged
	 */
	protected void valueChanged(boolean oldValidState, boolean newValidState) {
		valid = newValidState;
		dirty = true;
		fireEditorValueChanged(oldValidState, newValidState);
	}

	/**
	 * Activate the editor but also inform the editor which event triggered its
	 * activation. <b>The default implementation simply calls
	 * {@link #activate()}</b>
	 * 
	 * @param activationEvent
	 *            the editor activation event
	 */
	public void activate(EditorActivationEvent activationEvent) {
		cell = (Cell) activationEvent.getSource();
		rect = getRenderArea(cell);
		setBounds(rect);

		if (control != null) {
			control.setToolTipText(toolTip);
			control.addMouseMoveListener(new MouseMoveListener() {
				public void mouseMove(MouseEvent e) {
					agileGrid.setCursor(null);
				}
			});
			control.setVisible(true);

			setFocus();
		}
	}

	/**
	 * Gets the area to render the cell editor.
	 * 
	 * @param cell
	 * @return
	 */
	protected Rectangle getRenderArea(Cell cell) {
		return agileGrid.getCellRect(cell.row, cell.column);
	}

	/**
	 * This method is for internal use in {@link ColumnViewerEditor} to not
	 * break clients who don't implement the {@link ICellEditorListener}
	 * appropiately
	 * 
	 * @return <code>true</code> to indicate that a focus listener has to be
	 *         attached
	 */
	protected boolean dependsOnExternalFocusListener() {
		return true;
	}

	/**
	 * Returns true if the editor has the focus.
	 * 
	 * @return boolean
	 */
	public boolean isFocused() {
		if (control == null)
			return false;
		return control.isFocusControl();
	}

	/**
	 * Updates the bounds of cell editor.
	 * 
	 * @param cell
	 *            The cell to be edited.
	 */
	public void updateBounds(Cell cell) {
		Rectangle rect = this.getRenderArea(cell);
		this.setBounds(rect);
	}

	/**
	 * Sets the editor's position and size
	 * 
	 * @param rect
	 */
	public void setBounds(Rectangle rect) {
		if (control != null) {
			control.setBounds(rect);
		}
	}

	/**
	 * Returns the current bounds of the cell editor.
	 * 
	 * @return The current bounds of the cell editor.
	 */
	public Rectangle getBounds() {
		if (control != null) {
			Rectangle b = control.getBounds();
			return b;
		}
		return new Rectangle(0, 0, 0, 0);
	}

	/**
	 * Processes the key pressed event that occurred in this cell editor.
	 * <p>
	 * The default implementation of this framework method cancels editing when
	 * the ESC key is pressed. When the RETURN key is pressed the current value
	 * is applied and the cell editor deactivates. Subclasses should call this
	 * method at appropriate time. Subclasses may also extend or reimplement.
	 * 
	 * @param keyEvent
	 *            The key pressed event.
	 */
	protected void onKeyPressed(KeyEvent keyEvent) {
		if ((keyEvent.character == '\r')
				&& ((keyEvent.stateMask & SWT.SHIFT) == 0)) {
			this.fireApplyEditorValue();
			deactivate();
		} else if (keyEvent.character == SWT.ESC) {
			this.fireCancelEditor();
		} else {
			agileGrid.scrollToFocus(false);
		}
	}

	/**
	 * Processes the traverse event.
	 * 
	 * @param traverseEvent
	 *            The traverse event.
	 */
	protected void onTraverse(TraverseEvent traverseEvent) {
		// set selection to the appropriate next element:
		int row = cell.row;
		int column = cell.column;
		Cell c = null;
		switch (traverseEvent.keyCode) {
		case SWT.ARROW_LEFT:
			column--;
			break;
		case SWT.ARROW_RIGHT:
			column++;
			c = agileGrid.getValidCell(row, column);
			while (cell.equals(c)) {
				column++;
				c = agileGrid.getValidCell(row, column);
			}
			break;
		case SWT.ARROW_UP:
			row--;
			break;
		case SWT.ARROW_DOWN:
			row++;
			c = agileGrid.getValidCell(row, column);
			while (cell.equals(c)) {
				column++;
				c = agileGrid.getValidCell(row, column);
			}
			break;
		}

		if (row != cell.row || column != cell.column) {
			this.fireApplyEditorValue();
			agileGrid.focusCell(new Cell(agileGrid, row, column));
			agileGrid.scrollToFocus(false);
		}
	}

	/**
	 * Sets the tool tip text.
	 * 
	 * @param toolTip
	 *            The tool tip text.
	 */
	public void setToolTipText(String toolTip) {
		this.toolTip = toolTip;
	}

	/**
	 * Returns the agile grid that this cell editor works for.
	 * 
	 * @return The agile grid
	 */
	public AgileGrid getAgileGrid() {
		return agileGrid;
	}

	/**
	 * Sets the agile grid that this cell editor works for.
	 * 
	 * @param agileGrid
	 *            the agile grid to set
	 */
	public void setAgileGrid(AgileGrid agileGrid) {
		this.agileGrid = agileGrid;
	}
}
