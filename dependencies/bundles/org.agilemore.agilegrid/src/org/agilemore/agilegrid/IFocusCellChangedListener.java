/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */
package org.agilemore.agilegrid;

/**
 * A listener which is notified when a agile grid's focus cell changes.
 * 
 * @see FocusCellChangedEvent
 * 
 * @author fourbroad
 */
public interface IFocusCellChangedListener {

	/**
	 * Notifies that the focus cell has changed.
	 * 
	 * @param event
	 *            event object describing the change
	 */
	public void focusChanged(FocusCellChangedEvent event);
	
}
