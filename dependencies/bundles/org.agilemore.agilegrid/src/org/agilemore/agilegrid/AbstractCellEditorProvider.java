/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

/**
 * Some common functionality and template methods to share between the
 * implementation of ICellEditorProvider.
 * <p>
 * Client never instantiate this class.
 * </p>
 * 
 * @author fourbroad
 * 
 */
public abstract class AbstractCellEditorProvider implements ICellEditorProvider {

	/**
	 * The agile grid this cell editor provider works for.
	 */
	protected AgileGrid agileGrid;

	/**
	 * The smart tool bar which will be shown when cell supports multi cell
	 * editor.
	 */
	private Control smartToolBar;

	/**
	 * The current cell for showing smart tool bar.
	 */
	protected Cell currentCell;

	/**
	 * Constructs a new abstract cell editor provider.
	 * 
	 * @param agileGrid
	 *            The agile grid this cell editor provider works for.
	 */
	public AbstractCellEditorProvider(AgileGrid agileGrid) {
		this(agileGrid, false);
	}

	/**
	 * Constructs a new abstract cell editor provider.
	 * 
	 * @param agileGrid
	 *            The agile grid this cell editor provider works for.
	 * @param enableSmartToolBar
	 *            Enable the smart tool bar.
	 */
	public AbstractCellEditorProvider(AgileGrid agileGrid,
			boolean enableSmartToolBar) {
		this.agileGrid = agileGrid;
		if (enableSmartToolBar) {
			hookListeners(agileGrid);
		}
	}

	/**
	 * The editor to be shown in the area of cell given.
	 * 
	 * @param row
	 *            the row index of cell
	 * @param col
	 *            the column index of cell
	 * @return the CellEditor to be shown.
	 */
	public abstract CellEditor getCellEditor(int row, int col);

	/**
	 * Is the cell editable
	 * 
	 * @param row
	 *            the row index of cell
	 * @param col
	 *            the column index of cell
	 * @return true if editable, false otherwise.
	 */
	@Override
	public abstract boolean canEdit(int row, int col);

	/**
	 * Returns the agile grid this cell editor provider works for.
	 * 
	 * @return The agile grid this cell editor provider works for
	 */
	public AgileGrid getAgileGrid() {
		return agileGrid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.ICellEditorProvider#initializeCellEditorValue(org
	 * .peertoo.agilegrid.CellEditor, org.peertoo.agilegrid.Cell)
	 */
	public void initializeCellEditorValue(CellEditor cellEditor, Cell cell) {
		Object value = getValue(cell.row, cell.column);
		cellEditor.setValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.ICellEditorProvider#saveCellEditorValue(org.peertoo
	 * .agilegrid.CellEditor, org.peertoo.agilegrid.Cell)
	 */
	public void saveCellEditorValue(CellEditor cellEditor, Cell cell) {
		if (cellEditor.isValueValid()) {
			Object value = cellEditor.getValue();
			setValue(cell.row, cell.column, value);
		}
	}

	/**
	 * Get the value to set to the editor
	 * 
	 * <p>
	 * <b>Subclasses should overwrite!</b>
	 * </p>
	 * 
	 * @param row
	 *            the row index of cell.
	 * @param col
	 *            the column index of cell.
	 * @return The value to be edit.
	 */
	public abstract Object getValue(int row, int col);

	/**
	 * Restore the value from the CellEditor
	 * 
	 * <p>
	 * <b>Subclasses should overwrite!</b>
	 * </p>
	 * 
	 * @param row
	 *            the row index of cell
	 * @param col
	 *            the column index of cell
	 * @param value
	 *            the new value to be save.
	 */
	public abstract void setValue(int row, int col, Object value);

	/**
	 * Processes the event of smart tool bar.
	 * 
	 * @param cell
	 *            The current cell showing smart tool bar.
	 */
	protected void processSmartToolBar(Cell cell) {
		if (!this.showSmartToolBar(cell)) {
			if (smartToolBar != null && !smartToolBar.isDisposed()) {
				smartToolBar.dispose();
				smartToolBar = null;
			}
			return;
		}

		if (smartToolBar == null || smartToolBar.isDisposed()) {
			smartToolBar = createSmartToolBar(agileGrid);
			if (smartToolBar == null) {
				return;
			}
			smartToolBar.setVisible(false);
		}

		Rectangle cellRect = agileGrid.getCellRect(cell.row, cell.column);
		Point containerSize = smartToolBar.getSize();
		cellRect.x = cellRect.x + cellRect.width - containerSize.x - 1;
		cellRect.y += 1;
		Cell focusCell = agileGrid.getFocusCell();
		if (focusCell.equals(cell)
				|| ((agileGrid.getStyle() & SWTX.ROW_SELECTION) == SWTX.ROW_SELECTION && focusCell.row == cell.row)) {
			smartToolBar.setBackground(ICellRenderer.COLOR_BGSELECTION);
		} else {
			smartToolBar.setBackground(ICellRenderer.COLOR_BACKGROUND);
		}
		smartToolBar.setLocation(cellRect.x, cellRect.y);
		smartToolBar.setVisible(true);
	}

	/**
	 * Hooks listeners for smart too bar.
	 * 
	 * @param agileGrid
	 *            The agileGrid smart tool bar works for.
	 */
	protected void hookListeners(final AgileGrid agileGrid) {
		Listener smartToolBarListener = new Listener() {
			@Override
			public void handleEvent(Event event) {
				switch (event.type) {
				case SWT.KeyDown:
					currentCell = agileGrid.getFocusCell();
					processSmartToolBar(currentCell);
					break;
				case SWT.Dispose:
				case SWT.Selection: // scrolling
				case SWT.MouseDown:
				case SWT.MouseDoubleClick:
				case SWT.MouseExit:
				case SWT.MouseMove:
				case SWT.MouseHover:
					currentCell = agileGrid.getCell(event.x, event.y);
					processSmartToolBar(currentCell);
				}
			}
		};
		agileGrid.addListener(SWT.Dispose, smartToolBarListener);
		agileGrid.addListener(SWT.KeyDown, smartToolBarListener);
		agileGrid.addListener(SWT.MouseDown, smartToolBarListener);
		agileGrid.addListener(SWT.MouseDoubleClick, smartToolBarListener);
		agileGrid.addListener(SWT.MouseMove, smartToolBarListener);
		agileGrid.addListener(SWT.MouseHover, smartToolBarListener);
		agileGrid.addListener(SWT.MouseExit, smartToolBarListener);
	}

	/**
	 * Creates a new smart tool bar.
	 * 
	 * @param parent
	 *            The parent of smart tool bar.
	 * @return The smart tool bar.
	 */
	protected abstract Composite createSmartToolBar(Composite parent);

	/**
	 * Is the smart tool bar shown.
	 * 
	 * @param cell
	 *            The current cell showing smart tool bar.
	 * @return
	 */
	protected abstract boolean showSmartToolBar(Cell cell);

}
