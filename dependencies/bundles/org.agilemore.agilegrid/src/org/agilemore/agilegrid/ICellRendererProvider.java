/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * ICellRendererProvider provides the rendering information for agile grid.
 * Generally speaking, all functions should return their results as quick as
 * possible, since they might be called a few times when drawing the widget.
 * 
 * @author fourbroad
 * 
 */
public interface ICellRendererProvider {

	/**
	 * Returns the cell renderer for the given cell.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return The cell renderer that is responsible for drawing the cell.
	 */
	public ICellRenderer getCellRenderer(int row, int col);

	/**
	 * Returns the cell renderer for the indicated column of top header.
	 * 
	 * @param col
	 *            The column index.
	 * @return The cell renderer that is responsible for drawing the top header
	 *         cell.
	 */
	public ICellRenderer getTopHeadRenderer(int col);

	/**
	 * Returns the cell renderer for the indicated row of the left header.
	 * 
	 * @param row
	 *            The row index.
	 * @return The cell renderer that is responsible for drawing the left header
	 *         cell.
	 */
	public ICellRenderer getLeftHeadRenderer(int row);
}