/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeListenerProxy;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Some common functionality and template methods to share between
 * implementation of ILayoutAdvisor.
 * </p>
 * <p>
 * Client never instantiate this class.
 * </p>
 * 
 * @author fourbroad
 * 
 */
public abstract class AbstractLayoutAdvisor implements ILayoutAdvisor {

	/**
	 * The agile grid that this layout advisor apply for.
	 */
	protected AgileGrid agileGrid;

	/**
	 * The strategy used to sort the content.
	 */
	ICompositorStrategy compositorStrategy;

	/**
	 * This is a utility class that can be used by layout advisor that support
	 * bound properties. The layout advisor use an instance of this class as a
	 * member field of layout advisor and delegate various work to it.
	 */
	protected PropertyChangeSupport propertyChangeSupport;

	/**
	 * @param agileGrid
	 *            The agile grid that this layout advisor apply for.
	 */
	public AbstractLayoutAdvisor(AgileGrid agileGrid) {
		this.agileGrid = agileGrid;
		propertyChangeSupport = new PropertyChangeSupport(this);
	}

	/**
	 * Get the the current column width.
	 * 
	 * @param col
	 *            The column index.
	 * @return The current column width.
	 */
	public abstract int getColumnWidth(int col);

	/**
	 * Returns the initial column width for the column index given. Note that if
	 * resize is enabled, this value might not be the real width of a column.
	 * The value returned by <code>getColumnWidth()</code> corresponds to the
	 * real width used when painting the agile grid!
	 * 
	 * @param column
	 *            The column index
	 * @return The initial width of the column.
	 */
	public abstract int getInitialColumnWidth(int column);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#getRowHeight(int)
	 */
	public abstract int getRowHeight(int row);

	/**
	 * Returns the initial row height for the row index given. Note that if
	 * resize is enabled, this value might not be the real height of a row. The
	 * value returned by <code>getRowHeight()</code> corresponds to the real
	 * height used when painting the agile grid!
	 * 
	 * @param row
	 *            The row index.
	 * @return Returns the initial row height that should be used on normal
	 *         cells. If resize is enabled, the value returned by
	 *         <code>getRowHeight(int)</code> might not always be this value!
	 */
	public abstract int getInitialRowHeight(int row);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#setColumnWidth(int, int)
	 */
	public void setColumnWidth(int col, int width) {
		int oldWidth = getColumnWidth(col);
		if (width == oldWidth) {
			return;
		}

		doSetColumnWidth(col, width);
		firePropertyChange(ILayoutAdvisor.ColumnWidth, col, oldWidth, width);
	}

	/**
	 * The method which do actually set the width of column.
	 * 
	 * @param col
	 *            The index of column.
	 * @param width
	 *            The width of column indicated.
	 */
	protected abstract void doSetColumnWidth(int col, int width);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#setRowHeight(int, int)
	 */
	public void setRowHeight(int row, int height) {
		int oldHeight = getRowHeight(row);
		if (height == oldHeight) {
			return;
		}
		doSetRowHeight(row, height);
		firePropertyChange(ILayoutAdvisor.RowHeight, row, oldHeight, height);
	}

	/**
	 * The method which do actually set the height of row.
	 * 
	 * @param row
	 *            The index of row.
	 * @param height
	 *            The height of row indicated.
	 */
	protected abstract void doSetRowHeight(int row, int height);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#isFixedCell(int, int)
	 */
	public boolean isFixedCell(int row, int col) {
		return col < getFixedColumnCount() || row < getFixedRowCount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#belongsToCell(int, int)
	 */
	public Cell mergeInto(int row, int col) {
		// FIXME to optimize the performance.
		return new Cell(agileGrid, row, col);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ILayoutAdvisor#getCompositorStrategy()
	 */
	public ICompositorStrategy getCompositorStrategy() {
		return compositorStrategy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.peertoo.agilegrid.ILayoutAdvisor#setCompositorStrategy(org.peertoo
	 * .agilegrid.ICompositorStrategy)
	 */
	public void setCompositorStrategy(ICompositorStrategy compositorStrategy) {
		this.compositorStrategy = compositorStrategy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setColumnCount(int)
	 */
	@Override
	public void setColumnCount(int columnCount) {
		int oldColumnCount = getColumnCount();
		if (columnCount == oldColumnCount) {
			return;
		}
		doSetColumnCount(columnCount);
		firePropertyChange(ILayoutAdvisor.ColumnCount, oldColumnCount,
				columnCount);
	}

	/**
	 * The method which do actually set the column count.
	 * 
	 * @param columnCount
	 *            The tatol column count.
	 */
	protected abstract void doSetColumnCount(int columnCount);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setFixedColumnCount(int)
	 */
	@Override
	public void setFixedColumnCount(int fixedColumnCount) {
		int oldFixedColumnCount = getFixedColumnCount();
		if (fixedColumnCount == oldFixedColumnCount) {
			return;
		}

		doSetFixedColumnCount(fixedColumnCount);
		firePropertyChange(ILayoutAdvisor.FixedColumnCount,
				oldFixedColumnCount, fixedColumnCount);
	}

	/**
	 * The method which do actually set the fixed column count.
	 * 
	 * @param fixedColumnCount
	 *            The fixed column count.
	 */
	protected abstract void doSetFixedColumnCount(int fixedColumnCount);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setFixedRowCount(int)
	 */
	@Override
	public void setFixedRowCount(int fixedRowCount) {
		int oldFixedRowCount = getFixedRowCount();
		if (fixedRowCount == oldFixedRowCount) {
			return;
		}
		doSetFixedRowCount(fixedRowCount);
		firePropertyChange(ILayoutAdvisor.FixedRowCount, oldFixedRowCount,
				fixedRowCount);
	}

	/**
	 * The method which do actually set the fixed row count.
	 * 
	 * @param fixedRowCount
	 *            The fixed row count.
	 */
	protected abstract void doSetFixedRowCount(int fixedRowCount);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setLeftHeaderLabel(int,
	 * java.lang.String)
	 */
	@Override
	public void setLeftHeaderLabel(int row, String label) {
		String oldLabel = getLeftHeaderLabel(row);
		if (oldLabel != label || (oldLabel != null && !oldLabel.equals(label))) {
			doSetLeftHeaderLabel(row, label);
			firePropertyChange(ILayoutAdvisor.LeftHeaderLabel, row, oldLabel,
					label);
		}
	}

	/**
	 * The method which do actually set the left header label of row.
	 * 
	 * @param row
	 *            The index of row.
	 * @param label
	 *            The label of row indicated.
	 */
	protected abstract void doSetLeftHeaderLabel(int row, String label);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setLeftHeaderVisible(boolean)
	 */
	@Override
	public void setLeftHeaderVisible(boolean visible) {
		boolean oldVisible = isLeftHeaderVisible();
		if (visible == oldVisible) {
			return;
		}

		doSetLeftHeaderVisible(visible);
		firePropertyChange(ILayoutAdvisor.LeftHeaderVisible, oldVisible,
				visible);
	}

	/**
	 * The method which do actually set visible status of the left header.
	 * 
	 * @param visible
	 *            true if the left header is visible, false otherwise.
	 */
	protected abstract void doSetLeftHeaderVisible(boolean visible);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setLeftHeaderWidth(int)
	 */
	@Override
	public void setLeftHeaderWidth(int width) {
		int oldWidth = getLeftHeaderWidth();
		if (width == oldWidth) {
			return;
		}

		doSetLeftHeaderWidth(width);
		firePropertyChange(ILayoutAdvisor.LeftHeaderWidth, oldWidth, width);
	}

	/**
	 * The method which do actually set the width of left header.
	 * 
	 * @param width
	 *            The width of left header.
	 */
	protected abstract void doSetLeftHeaderWidth(int width);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setRowCount(int)
	 */
	@Override
	public void setRowCount(int rowCount) {
		int oldRowCount = getRowCount();
		if (rowCount == oldRowCount) {
			return;
		}

		doSetRowCount(rowCount);
		firePropertyChange(ILayoutAdvisor.RowCount, oldRowCount, rowCount);
	}

	/**
	 * The method which do actually set the row count.
	 * 
	 * @param rowCount
	 *            The total row count.
	 */
	protected abstract void doSetRowCount(int rowCount);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setRowHeightMinimum(int)
	 */
	@Override
	public void setRowHeightMinimum(int minimumHeight) {
		int oldMinimumHeight = getRowHeightMinimum();
		if (minimumHeight == oldMinimumHeight) {
			return;
		}

		doSetRowHeightMinimum(minimumHeight);
		firePropertyChange(ILayoutAdvisor.RowHeightMinimum, oldMinimumHeight,
				minimumHeight);
	}

	/**
	 * The method which do actually set the minimum height of a row.
	 * 
	 * @param minimumHeight
	 *            The minimum height of a row.
	 */
	protected abstract void doSetRowHeightMinimum(int minimumHeight);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setRowResizable(int, boolean)
	 */
	@Override
	public void setRowResizable(int row, boolean resizable) {
		boolean oldResizable = isRowResizable(row);
		if (resizable == oldResizable) {
			return;
		}

		doSetRowResizable(row, resizable);
		firePropertyChange(ILayoutAdvisor.RowResizable, row, oldResizable,
				resizable);
	}

	/**
	 * The method which do actually set the resizable status of row indicated.
	 * 
	 * @param row
	 *            The index of row.
	 * @param resizable
	 *            true if the row is resizable, false otherwise.
	 */
	protected abstract void doSetRowResizable(int row, boolean resizable);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setTooltipAt(int, int,
	 * java.lang.String)
	 */
	@Override
	public void setTooltip(int row, int col, String tooltip) {
		String oldTooltip = getTooltip(row, col);
		if (tooltip != oldTooltip
				|| (oldTooltip != null && !oldTooltip.equals(tooltip))) {
			doSetTooltipAt(row, col, tooltip);
			firePropertyChange(ILayoutAdvisor.Tooltip, row, col, oldTooltip,
					tooltip);
		}
	}

	/**
	 * The method which do actually set the tooltip at the cell indicated.
	 * 
	 * @param row
	 *            The index of row.
	 * @param col
	 *            The index of col.
	 * @param tooltip
	 *            The tooltip which will be shown when mouse hover on the cell.
	 */
	protected abstract void doSetTooltipAt(int row, int col, String tooltip);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setTopHeaderHeight(int)
	 */
	@Override
	public void setTopHeaderHeight(int height) {
		int oldHeight = getTopHeaderHeight();
		if (height == oldHeight) {
			return;
		}

		doSetTopHeaderHeight(height);
		firePropertyChange(ILayoutAdvisor.TopHeaderHeight, oldHeight, height);
	}

	/**
	 * The method which do actually set the height of top header.
	 * 
	 * @param height
	 *            The height of top header.
	 */
	protected abstract void doSetTopHeaderHeight(int height);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setTopHeaderLabel(int,
	 * java.lang.String)
	 */
	@Override
	public void setTopHeaderLabel(int col, String label) {
		String oldLabel = getTopHeaderLabel(col);
		if (oldLabel != label || (oldLabel != null && !oldLabel.equals(label))) {
			doSetTopHeaderLabel(col, label);
			firePropertyChange(ILayoutAdvisor.TopHeaderLabel, col, oldLabel,
					label);
		}
	}

	/**
	 * The method which do actually set the label of top header.
	 * 
	 * @param col
	 *            The index of column.
	 * @param label
	 *            The label of column indicated.
	 */
	protected abstract void doSetTopHeaderLabel(int col, String label);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.agilemore.agilegrid.ILayoutAdvisor#setTopHeaderVisible(boolean)
	 */
	@Override
	public void setTopHeaderVisible(boolean visible) {
		boolean oldVisible = isTopHeaderVisible();
		if (oldVisible == visible) {
			return;
		}

		doSetTopHeaderVisible(visible);
		firePropertyChange(ILayoutAdvisor.TopHeaderVisible, oldVisible, visible);
	}

	/**
	 * The method which do actually set the visible status of top header.
	 * 
	 * @param visible
	 *            true if the top header is visible, false otherwise.
	 */
	protected abstract void doSetTopHeaderVisible(boolean visible);

	/**
	 * Add a PropertyChangeListener to the listener list. The listener is
	 * registered for all properties. The same listener object may be added more
	 * than once, and will be called as many times as it is added. If
	 * <code>listener</code> is null, no exception is thrown and no action is
	 * taken.
	 * 
	 * @param listener
	 *            The PropertyChangeListener to be added
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * Add a PropertyChangeListener for a specific property. The listener will
	 * be invoked only when a call on firePropertyChange names that specific
	 * property. The same listener object may be added more than once. For each
	 * property, the listener will be invoked the number of times it was added
	 * for that property. If <code>propertyName</code> or <code>listener</code>
	 * is null, no exception is thrown and no action is taken.
	 * 
	 * @param propertyName
	 *            The name of the property to listen on.
	 * @param listener
	 *            The PropertyChangeListener to be added
	 */
	public void addPropertyChangeListener(String propertyName,
			PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	/**
	 * Report a <code>boolean</code> bound indexed property update to any
	 * registered listeners.
	 * <p>
	 * No event is fired if old and new values are equal.
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * fireIndexedPropertyChange method which takes Object values.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param index
	 *            index of the property element that was changed.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void fireIndexedPropertyChange(String propertyName, int index,
			boolean oldValue, boolean newValue) {
		propertyChangeSupport.fireIndexedPropertyChange(propertyName, index,
				oldValue, newValue);
	}

	/**
	 * Report an <code>int</code> bound indexed property update to any
	 * registered listeners.
	 * <p>
	 * No event is fired if old and new values are equal.
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * fireIndexedPropertyChange method which takes Object values.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param index
	 *            index of the property element that was changed.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void fireIndexedPropertyChange(String propertyName, int index,
			int oldValue, int newValue) {
		propertyChangeSupport.fireIndexedPropertyChange(propertyName, index,
				oldValue, newValue);
	}

	/**
	 * Report a bound indexed property update to any registered listeners.
	 * <p>
	 * No event is fired if old and new values are equal and non-null.
	 * 
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * firePropertyChange method that takes {@code PropertyChangeEvent} value.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param index
	 *            index of the property element that was changed.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void fireIndexedPropertyChange(String propertyName, int index,
			Object oldValue, Object newValue) {
		propertyChangeSupport.fireIndexedPropertyChange(propertyName, index,
				oldValue, newValue);
	}

	/**
	 * Fire an existing PropertyChangeEvent to any registered listeners. No
	 * event is fired if the given event's old and new values are equal and
	 * non-null.
	 * 
	 * @param evt
	 *            The PropertyChangeEvent object.
	 */
	public void firePropertyChange(PropertyChangeEvent evt) {
		propertyChangeSupport.firePropertyChange(evt);
	}

	/**
	 * Report a boolean bound property update to any registered listeners. No
	 * event is fired if old and new are equal.
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * firePropertyChange method that takes Object values.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void firePropertyChange(String propertyName, boolean oldValue,
			boolean newValue) {
		propertyChangeSupport.firePropertyChange(propertyName, oldValue,
				newValue);
	}

	/**
	 * Report an int bound property update to any registered listeners. No event
	 * is fired if old and new are equal.
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * firePropertyChange method that takes Object values.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void firePropertyChange(String propertyName, int oldValue,
			int newValue) {
		propertyChangeSupport.firePropertyChange(propertyName, oldValue,
				newValue);
	}

	/**
	 * Report an int bound property update to any registered listeners. No event
	 * is fired if old and new are equal.
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * firePropertyChange method that takes Object values.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param index
	 *            The index of row or column.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void firePropertyChange(String propertyName, int index,
			int oldValue, int newValue) {
		if (oldValue == newValue)
			return;

		firePropertyChange(propertyName, index, new Integer(oldValue),
				new Integer(newValue));
	}

	/**
	 * Report an int bound property update to any registered listeners. No event
	 * is fired if old and new are equal.
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * firePropertyChange method that takes Object values.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param rowIndex
	 *            The index of row.
	 * @param colIndex
	 *            The index of column.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void firePropertyChange(String propertyName, int rowIndex,
			int colIndex, int oldValue, int newValue) {
		if (oldValue == newValue)
			return;

		firePropertyChange(propertyName, rowIndex, colIndex, new Integer(
				oldValue), new Integer(newValue));
	}

	/**
	 * Report a bound property update to any registered listeners. No event is
	 * fired if old and new are equal and non-null.
	 * 
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * firePropertyChange method that takes {@code PropertyChangeEvent} value.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void firePropertyChange(String propertyName, Object oldValue,
			Object newValue) {
		propertyChangeSupport.firePropertyChange(propertyName, oldValue,
				newValue);
	}

	/**
	 * Report a bound property update to any registered listeners. No event is
	 * fired if old and new are equal and non-null.
	 * 
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * firePropertyChange method that takes {@code PropertyChangeEvent} value.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param index
	 *            The index of row or column.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void firePropertyChange(String propertyName, int index,
			Object oldValue, Object newValue) {
		Integer indexObject = new Integer(index);
		Map<Integer, Object> oldMap = new HashMap<Integer, Object>();
		oldMap.put(indexObject, oldValue);
		Map<Integer, Object> newMap = new HashMap<Integer, Object>();
		newMap.put(indexObject, newValue);
		propertyChangeSupport
				.firePropertyChange(propertyName, oldMap, newMap);
	}

	/**
	 * Report a bound property update to any registered listeners. No event is
	 * fired if old and new are equal and non-null.
	 * 
	 * <p>
	 * This is merely a convenience wrapper around the more general
	 * firePropertyChange method that takes {@code PropertyChangeEvent} value.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property that was changed.
	 * @param rowIndex
	 *            The index of row.
	 * @param colIndex
	 *            The index of column.
	 * @param oldValue
	 *            The old value of the property.
	 * @param newValue
	 *            The new value of the property.
	 */
	public void firePropertyChange(String propertyName, int rowIndex,
			int colIndex, Object oldValue, Object newValue) {
		Cell cell = new Cell(agileGrid, rowIndex, colIndex);
		Map<Cell, Object> oldMap = new HashMap<Cell, Object>();
		oldMap.put(cell, oldValue);
		Map<Cell, Object> newMap = new HashMap<Cell, Object>();
		newMap.put(cell, newValue);
		propertyChangeSupport
				.firePropertyChange(propertyName, oldMap, newMap);
	}

	/**
	 * Returns an array of all the listeners that were added to the
	 * PropertyChangeSupport object with addPropertyChangeListener().
	 * <p>
	 * If some listeners have been added with a named property, then the
	 * returned array will be a mixture of PropertyChangeListeners and
	 * <code>PropertyChangeListenerProxy</code>s. If the calling method is
	 * interested in distinguishing the listeners then it must test each element
	 * to see if it's a <code>PropertyChangeListenerProxy</code>, perform the
	 * cast, and examine the parameter.
	 * 
	 * <pre>
	 * PropertyChangeListener[] listeners = bean.getPropertyChangeListeners();
	 * for (int i = 0; i &lt; listeners.length; i++) {
	 * 	if (listeners[i] instanceof PropertyChangeListenerProxy) {
	 * 		PropertyChangeListenerProxy proxy = (PropertyChangeListenerProxy) listeners[i];
	 * 		if (proxy.getPropertyName().equals(&quot;foo&quot;)) {
	 * 			// proxy is a PropertyChangeListener which was associated
	 * 			// with the property named &quot;foo&quot;
	 * 		}
	 * 	}
	 * }
	 *</pre>
	 * 
	 * @see PropertyChangeListenerProxy
	 * @return all of the <code>PropertyChangeListeners</code> added or an empty
	 *         array if no listeners have been added
	 */
	public PropertyChangeListener[] getPropertyChangeListeners() {
		return propertyChangeSupport.getPropertyChangeListeners();
	}

	/**
	 * Returns an array of all the listeners which have been associated with the
	 * named property.
	 * 
	 * @param propertyName
	 *            The name of the property being listened to
	 * @return all of the <code>PropertyChangeListeners</code> associated with
	 *         the named property. If no such listeners have been added, or if
	 *         <code>propertyName</code> is null, an empty array is returned.
	 */
	public PropertyChangeListener[] getPropertyChangeListeners(
			String propertyName) {
		return propertyChangeSupport.getPropertyChangeListeners(propertyName);
	}

	/**
	 * Check if there are any listeners for a specific property, including those
	 * registered on all properties. If <code>propertyName</code> is null, only
	 * check for listeners registered on all properties.
	 * 
	 * @param propertyName
	 *            the property name.
	 * @return true if there are one or more listeners for the given property
	 */
	public boolean hasListeners(String propertyName) {
		return propertyChangeSupport.hasListeners(propertyName);
	}

	/**
	 * Remove a PropertyChangeListener from the listener list. This removes a
	 * PropertyChangeListener that was registered for all properties. If
	 * <code>listener</code> was added more than once to the same event source,
	 * it will be notified one less time after being removed. If
	 * <code>listener</code> is null, or was never added, no exception is thrown
	 * and no action is taken.
	 * 
	 * @param listener
	 *            The PropertyChangeListener to be removed
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	/**
	 * Remove a PropertyChangeListener for a specific property. If
	 * <code>listener</code> was added more than once to the same event source
	 * for the specified property, it will be notified one less time after being
	 * removed. If <code>propertyName</code> is null, no exception is thrown and
	 * no action is taken. If <code>listener</code> is null, or was never added
	 * for the specified property, no exception is thrown and no action is
	 * taken.
	 * 
	 * @param propertyName
	 *            The name of the property that was listened on.
	 * @param listener
	 *            The PropertyChangeListener to be removed
	 */
	public void removePropertyChangeListener(String propertyName,
			PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(propertyName,
				listener);
	}

}