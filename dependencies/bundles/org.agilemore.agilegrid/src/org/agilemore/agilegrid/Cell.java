/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * The agile grid is composed of cells, cell is a basic unit, which can be located by
 * row and column index.
 * 
 * @author fourbroad
 * 
 */
public class Cell {
	/**
	 * The agile grid that contains this cell.
	 */
	public AgileGrid agileGrid;

	/**
	 * Null object for cell.
	 */
	public static final Cell NULLCELL = new Cell(null, Integer.MIN_VALUE,
			Integer.MIN_VALUE);

	/**
	 * Row index of this cell.
	 */
	public int row;

	/**
	 * Column index of this cell.
	 */
	public int column;

	/**
	 * Copy constructor.
	 * 
	 * @param cell
	 */
	public Cell(Cell cell) {
		this.agileGrid = cell.agileGrid;
		this.row = cell.row;
		this.column = cell.column;
	}

	/**
	 * Cell constructor.
	 * 
	 * @param agileGrid
	 *            The agile grid that contain this cell.
	 * @param row
	 *            The row index of cell.
	 * @param column
	 *            The column index of cell.
	 */
	public Cell(AgileGrid agileGrid, int row, int column) {
		this.agileGrid = agileGrid;
		this.row = row;
		this.column = column;
	}

	/**
	 * Returns the element of content.
	 * 
	 * @return The element of content.
	 */
	public Object getElement() {
		return agileGrid.getContentAt(row, column);
	}

	/**
	 * Sets the element of content.
	 * 
	 * @param element
	 *            The element of content.
	 */
	public void setElement(Object element) {
		agileGrid.setContentAt(row, column, element);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object)
			return true;
		if (object == null)
			return false;
		if (getClass() != object.getClass())
			return false;

		Cell c = (Cell) object;
		return (c.row == this.row) && (c.column == this.column)
				&& (this.agileGrid == c.agileGrid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return row ^ column;
	}

	/**
	 * Returns the agile grid that contains this cell.
	 * 
	 * @return the agile grid that contains this cell.
	 */
	public AgileGrid getAgileGrid() {
		return agileGrid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append('[');
		sb.append(row);
		sb.append(',');
		sb.append(column);
		sb.append(']');
		return sb.toString();
	}

}
