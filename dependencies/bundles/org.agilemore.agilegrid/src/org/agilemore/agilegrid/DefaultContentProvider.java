/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * This adapter class provides default implementations for the methods described
 * by the <code>IContentProvider</code> interface.
 * 
 * <p>
 * Classes that wish to deal with cell content can extend this class and
 * override only the methods that they are interested in.
 */
public class DefaultContentProvider extends AbstractContentProvider {

	/**
	 * Returns the content of the given cell which string form is shown in the
	 * agile grid cell.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return The content of the cell.
	 */
	public Object doGetContentAt(int row, int col) {
		return null;
	}

	/**
	 * Called to change the cell value in the content.
	 * 
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @param value
	 *            The new value to set in the content.
	 */
	public void doSetContentAt(int row, int col, Object value) {
	}
}
