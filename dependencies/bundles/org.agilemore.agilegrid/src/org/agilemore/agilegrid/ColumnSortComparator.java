/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.util.Comparator;

/**
 * Implementations of this class are used when sorting a agile grid.
 * 
 * @author fourbroad
 * 
 */
public abstract class ColumnSortComparator implements Comparator<Integer> {

	/**
	 * The type of compositor.
	 */
	public static final int SORT_NONE = -1;
	public static final int SORT_UP = 1;
	public static final int SORT_DOWN = 2;

	/**
	 * The column index that this sort comparator works for.
	 */
	private int colIndex = Integer.MIN_VALUE;

	/**
	 * The direction of compositor.
	 */
	private int direction = SORT_NONE;

	/**
	 * The agile grid this comparator is working for.
	 */
	private AgileGrid agileGrid;

	/**
	 * Creates a new comparator on the given column of agile grid.
	 * 
	 * @param agileGrid
	 *            The agile grid to compare on.
	 * @param columnIndex
	 *            The column index of agile grid.
	 * @param direction
	 *            The direction of compositor.
	 */
	public ColumnSortComparator(AgileGrid agileGrid, int columnIndex, int direction) {
		this.agileGrid = agileGrid;
		setSortDirection(direction);
		setColumnToCompare(columnIndex);
	}

	/**
	 * Compares two cells. The given integer represent the row numbers to use.
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public final int compare(Integer i1, Integer i2) {
		int row1 = i1.intValue();
		int row2 = i2.intValue();

		if (direction == SORT_NONE) {
			if (row1 > row2)
				return 1;
			if (row1 < row2)
				return -1;
			return 0;
		}

		 IContentProvider contentProvider = agileGrid.getContentProvider();
		 Object content1 = contentProvider.getContentAt(row1, colIndex);
		 Object content2 = contentProvider.getContentAt(row2, colIndex);

		if (direction == SORT_DOWN)
			return -doCompare(content1, content2, row1, row2);
		else
			return doCompare(content1, content2, row1, row2);
	}

	/**
	 * Implement this method to do the actual compare between the two cell
	 * contents.
	 * 
	 * @param o1
	 *            The cell content of the first cell
	 * @param o2
	 *            The cell content of the second cell
	 * @param row1
	 *            The row index where o1 was found in the model.
	 * @param row2
	 *            The row index where o2 was found in the model.
	 * @return Returns an int smaller, equal or larger than 0 if o1 is smaller,
	 *         equal or larger than o2.
	 */
	public abstract int doCompare(Object o1, Object o2, int row1, int row2);

	/**
	 * Sets the column index this comparator operates on.
	 * 
	 * @param column
	 *            the column index to use.
	 */
	public void setColumnToCompare(int column) {
		colIndex = column;
	}

	/**
	 * Returns the sorting direction, either SORT_NONE, SORT_UP or SORT_DOWN.
	 * 
	 * @return The sorting direction.
	 */
	public int getSortDirection() {
		return direction;
	}

	/**
	 * Sets the sorting direction, either SORT_NONE, SORT_UP or SORT_DOWN.
	 * 
	 * @param direction
	 *            The sort direction.
	 */
	public void setSortDirection(int direction) {
		if (direction != SORT_UP && direction != SORT_DOWN
				&& direction != SORT_NONE)
			throw new IllegalArgumentException("Undefined sorting direction: "
					+ direction);

		this.direction = direction;
	}

	/**
	 * Returns the column index that serves as a base for the sorting.
	 * 
	 * @return The column index that serves as a base for the sorting.
	 */
	public int getColumnToSortOn() {
		return colIndex;
	}
}
