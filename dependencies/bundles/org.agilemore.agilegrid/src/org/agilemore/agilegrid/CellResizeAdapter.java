/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * This adapter class provides default implementations for the methods described
 * by the <code>ICellResizeListener</code> interface.
 * <p>
 * Classes that wish to deal with cell resize event can extend this class and
 * override only the methods that they are interested in.
 * </p>
 * 
 * @author fourbroad
 */
public class CellResizeAdapter implements ICellResizeListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ICellResizeListener#columnResized(int, int)
	 */
	@Override
	public void columnResized(int col, int newWidth) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.ICellResizeListener#rowResized(int, int)
	 */
	@Override
	public void rowResized(int row, int newHeight) {

	}
}
