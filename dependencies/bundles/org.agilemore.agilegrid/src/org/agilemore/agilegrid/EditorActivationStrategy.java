/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;

/**
 * This class is responsible to determine if a cell selection event is triggers
 * an editor activation. Implementors can extend and overwrite to implement
 * custom editing behavior
 * 
 * @author fourbroad
 */
public class EditorActivationStrategy {

	/**
	 * The agile grid this class works for.
	 */
	private AgileGrid agileGrid;

	/**
	 * The listener of keyboard activation.
	 */
	private KeyListener keyboardActivationListener;

	/**
	 * Creates a new agile grid editor activation strategy for the given agile
	 * grid.
	 * 
	 * @param agileGrid
	 *            the agile grid the editor support is attached to
	 */
	public EditorActivationStrategy(AgileGrid agileGrid) {
		this(agileGrid, true);
	}

	/**
	 * Creates a new agile grid editor activation strategy for the given agile
	 * grid.
	 * 
	 * @param agileGrid
	 *            the agile grid the editor support is attached to
	 */
	public EditorActivationStrategy(AgileGrid agileGrid,
			boolean keyboardActivation) {
		this.agileGrid = agileGrid;
		setEnableEditorActivationWithKeyboard(keyboardActivation);
	}

	/**
	 * Decides whether the event is a event of editor activation.
	 * 
	 * @param event
	 *            The event of agile grid editor activation triggering the
	 *            action.
	 * @return <code>true</code>if this event should open the editor,
	 *         <code>false</code> otherwise.
	 */
	protected boolean isEditorActivationEvent(EditorActivationEvent event) {
		if (event.eventType == EditorActivationEvent.MOUSE_DOUBLE_CLICK_SELECTION
				&& ((MouseEvent) event.sourceEvent).button == 1) {
			return true;
		}

		if (event.eventType == EditorActivationEvent.PROGRAMMATIC
				|| event.eventType == EditorActivationEvent.TRAVERSAL) {
			return true;
		}

		if (event.sourceEvent.getClass() == KeyEvent.class) {
			KeyEvent keyEvent = (KeyEvent) event.sourceEvent;
			switch (keyEvent.character) {
			case ' ':
			case '\r':
			case SWT.DEL:
			case SWT.BS:
				return true;
			}

			if ((Character.isLetterOrDigit(keyEvent.character)
					|| keyEvent.keyCode == SWT.F2 || keyEvent.keyCode > 32
					&& keyEvent.keyCode < 254 && keyEvent.keyCode != 127)
					&& keyEvent.keyCode != SWT.CTRL
					&& keyEvent.keyCode != SWT.ALT
					&& (keyEvent.stateMask & SWT.CONTROL) == 0
					&& (keyEvent.stateMask & SWT.ALT) == 0) {
				return true;
			}

		}

		return false;
	}

	/**
	 * Returns the cell holding the current focus.
	 * 
	 * @return the cell holding the current focus
	 */
	protected Cell getFocusCell() {
		return agileGrid.getFocusCell();
	}

	/**
	 * Returns the agile grid this class is working for.
	 * 
	 * @return the agile grid working for.
	 */
	public AgileGrid getAgileGrid() {
		return agileGrid;
	}

	/**
	 * Enable activation of cell editors by keyboard.
	 * 
	 * @param enable
	 *            <code>true</code> to enable, <code>false</code> to disable.
	 */
	public void setEnableEditorActivationWithKeyboard(boolean enable) {
		if (enable) {
			if (keyboardActivationListener == null) {
				keyboardActivationListener = new KeyListener() {

					public void keyPressed(KeyEvent e) {
						Cell cell = getFocusCell();
						if (cell != null) {
							agileGrid.triggerEditorActivationEvent(
									new EditorActivationEvent(cell, null, e),
									null);
						}
					}

					public void keyReleased(KeyEvent e) {
					}

				};
				agileGrid.addKeyListener(keyboardActivationListener);
			}
		} else {
			if (keyboardActivationListener != null) {
				agileGrid.removeKeyListener(keyboardActivationListener);
				keyboardActivationListener = null;
			}
		}
	}
}
