/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * Parties interested in activation and deactivation of editors implement this
 * class and implement all of the methods
 * 
 * @author fourbroad
 */
public interface IEditorActivationListener {
	/**
	 * Called before an editor is activated
	 * 
	 * @param event
	 *            the event
	 */
	public abstract void beforeEditorActivated(EditorActivationEvent event);

	/**
	 * Called after an editor has been activated
	 * 
	 * @param event
	 *            the event
	 */
	public abstract void afterEditorActivated(EditorActivationEvent event);

	/**
	 * Called before an editor is deactivated
	 * 
	 * @param event
	 *            the event
	 */
	public abstract void beforeEditorDeactivated(
			EditorDeactivationEvent event);

	/**
	 * Called after an editor is deactivated
	 * 
	 * @param event
	 *            the event
	 */
	public abstract void afterEditorDeactivated(
			EditorDeactivationEvent event);

}
