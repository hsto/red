/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * CellRow represents a row of cells.
 * 
 * @author fourbroad
 * 
 */
public class CellRow extends Cell {

	public CellRow(AgileGrid agileGrid, int row) {
		super(agileGrid, row, Integer.MIN_VALUE);
	}
	
}
