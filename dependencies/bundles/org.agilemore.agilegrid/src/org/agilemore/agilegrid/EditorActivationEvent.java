/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.util.EventObject;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.TraverseEvent;

/**
 * This event is passed on when a cell-editor is going to be activated.
 * 
 * @author fourbroad
 * 
 */
public class EditorActivationEvent extends EventObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * if a key is pressed on a selected cell
	 */
	public static final int KEY_PRESSED = 1;

	/**
	 * if a cell is selected using a single click of the mouse
	 */
	public static final int MOUSE_CLICK_SELECTION = 2;

	/**
	 * if a cell is selected using double clicking of the mouse
	 */
	public static final int MOUSE_DOUBLE_CLICK_SELECTION = 3;

	/**
	 * if a cell is activated using code like e.g
	 * {@link ColumnViewer#editElement(Object, int)}
	 */
	public static final int PROGRAMMATIC = 4;

	/**
	 * is a cell is activated by traversing
	 */
	public static final int TRAVERSAL = 5;

	/**
	 * the original event triggered
	 */
	public EventObject sourceEvent;

	/**
	 * The time the event is triggered
	 */
	public int time;

	/**
	 * The event type triggered:
	 * <ul>
	 * <li>{@link #KEY_PRESSED} if a key is pressed on a selected cell</li>
	 * <li>{@link #MOUSE_CLICK_SELECTION} if a cell is selected using a single
	 * click of the mouse</li>
	 * <li>{@link #MOUSE_DOUBLE_CLICK_SELECTION} if a cell is selected using
	 * double clicking of the mouse</li>
	 * </ul>
	 */
	public int eventType;

	/**
	 * <b>Only set for {@link #KEY_PRESSED}</b>
	 */
	public int keyCode;

	/**
	 * <b>Only set for {@link #KEY_PRESSED}</b>
	 */
	public char character;

	/**
	 * The statemask
	 */
	public int stateMask;

	/**
	 * Cancel the event (=> editor is not activated)
	 */
	public boolean cancel = false;

	/**
	 * The cell editor for current cell.
	 */
	public CellEditor cellEditor;

	/**
	 * This constructor can be used when no event exists. The type set is
	 * {@link #PROGRAMMATIC}
	 * 
	 * @param cell
	 *            the cell
	 */
	public EditorActivationEvent(Cell cell, CellEditor cellEditor) {
		super(cell);
		this.cellEditor = cellEditor;
		eventType = PROGRAMMATIC;
	}

	/**
	 * This constructor is used for all types of mouse events. Currently the
	 * type is can be {@link #MOUSE_CLICK_SELECTION} and
	 * {@link #MOUSE_DOUBLE_CLICK_SELECTION}
	 * 
	 * @param cell
	 *            The cell source of the event
	 * @param cellEditor
	 *            the cell editor for current cell.
	 * @param event
	 *            The mouse event.
	 * @param eventType
	 *            The event type that can be MOUSE_CLICK_SELECTION or
	 *            MOUSE_DOUBLE_CLICK_SELECTION.
	 */
	public EditorActivationEvent(Cell cell, CellEditor cellEditor,
			MouseEvent event, int eventType) {
		super(cell);
		this.cellEditor = cellEditor;
		this.eventType = eventType;
		this.sourceEvent = event;
		this.time = event.time;
	}

	/**
	 * This constructor is used for all types of key events.
	 * 
	 * @param cell
	 *            The cell source of event.
	 * @param cellEditor
	 *            the cell editor for current cell.
	 * @param event
	 *            The key event.
	 */
	public EditorActivationEvent(Cell cell, CellEditor cellEditor,
			KeyEvent event) {
		super(cell);
		this.cellEditor = cellEditor;
		this.eventType = KEY_PRESSED;
		this.sourceEvent = event;
		this.time = 0;
		this.keyCode = event.keyCode;
		this.character = event.character;
		this.stateMask = event.stateMask;
	}

	/**
	 * This constructor is used to mark the activation triggered by a traversal
	 * 
	 * @param cell
	 *            the cell source of the event.
	 * @param cellEditor
	 *            the cell editor for current cell.
	 * @param event
	 *            the traverse event.
	 */
	public EditorActivationEvent(Cell cell, CellEditor cellEditor,
			TraverseEvent event) {
		super(cell);
		this.cellEditor = cellEditor;
		this.eventType = TRAVERSAL;
		this.sourceEvent = event;
	}
}
