/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

/**
 * When painting the agile grid cell, this class will be consulted for the
 * optimal width of cell or called the drawCell method to draw the given cell.
 * 
 * @author fourbroad
 * 
 */
public interface ICellRenderer {

	// The alignment style of cell content.
	public static final int ALIGN_HORIZONTAL_MASK = 0x000F;
	public static final int ALIGN_HORIZONTAL_NONE = 0x0000;
	public static final int ALIGN_HORIZONTAL_LEFT = 0x0001;
	public static final int ALIGN_HORIZONTAL_LEFT_LEFT = ALIGN_HORIZONTAL_LEFT;
	public static final int ALIGN_HORIZONTAL_LEFT_RIGHT = 0x0002;
	public static final int ALIGN_HORIZONTAL_LEFT_CENTER = 0x0003;
	public static final int ALIGN_HORIZONTAL_RIGHT = 0x0004;
	public static final int ALIGN_HORIZONTAL_RIGHT_RIGHT = ALIGN_HORIZONTAL_RIGHT;
	public static final int ALIGN_HORIZONTAL_RIGHT_LEFT = 0x0005;
	public static final int ALIGN_HORIZONTAL_RIGHT_CENTER = 0x0006;
	public static final int ALIGN_HORIZONTAL_CENTER = 0x0007;

	public static final int ALIGN_VERTICAL_MASK = 0x00F0;
	public static final int ALIGN_VERTICAL_TOP = 0x0010;
	public static final int ALIGN_VERTICAL_BOTTOM = 0x0020;
	public static final int ALIGN_VERTICAL_CENTER = 0x0030;

	public static final int WRAP_MASK = 0x0F00;
	public static final int WRAP = 0x0900;

	public static Color COLOR_TEXT = SWTResourceManager
			.getColor(SWT.COLOR_LIST_FOREGROUND);
	public static Color COLOR_BACKGROUND = SWTResourceManager
			.getColor(SWT.COLOR_LIST_BACKGROUND);
	public static Color COLOR_LINE_LIGHTGRAY = SWTResourceManager
			.getColor(SWT.COLOR_WIDGET_BACKGROUND);
	public static Color COLOR_LINE_DARKGRAY = SWTResourceManager
			.getColor(SWT.COLOR_DARK_GRAY);
	public static Color COLOR_COMMENTSIGN = SWTResourceManager
			.getColor(SWT.COLOR_RED);
	public static Color COLOR_BGROWSELECTION = SWTResourceManager
			.getColor(SWT.COLOR_LIST_SELECTION);
	public static Color COLOR_FGROWSELECTION = SWTResourceManager
			.getColor(SWT.COLOR_LIST_SELECTION_TEXT);
	public static Color COLOR_BGSELECTION = SWTResourceManager.getColor(223,
			227, 237);
	public static Color COLOR_FIXEDHIGHLIGHT = SWTResourceManager.getColor(182,
			189, 210);

	/**
	 * Makes a button-like cell.
	 */
	public static final int STYLE_PUSH = 0;

	/**
	 * Makes a flat looking cell.
	 */
	public static final int STYLE_FLAT = 1 << 2;

	/**
	 * Shows a sort indicator in the fixed cell if sorting is active for the
	 * column.
	 * <p>
	 * Has only an effect if the agile grid is sorted.
	 * </p>
	 */
	public static final int INDICATION_SORT = 1 << 3;

	/**
	 * Color fixed cells with focus in a different color.
	 * <p>
	 * Has only an effect on fixed cells when setting the style bit of agile
	 * grid as MARK_SELECTION_HEADERS.
	 * </p>
	 */
	public static final int INDICATION_SELECTION = 1 << 4;

	/**
	 * Color fixed cells with focus in a different color.
	 * <p>
	 * Has only an effect when setting the style bit of agile grid as
	 * MARK_SELECTION_HEADERS..
	 * </p>
	 */
	public static final int INDICATION_SELECTION_ROW = 1 << 5;

	/**
	 * Make the header column show a button-pressed behavior when clicked.
	 */
	public static final int INDICATION_CLICKED = 1 << 6;

	/**
	 * Draws a little triangle in the upper right corner of the cell as an
	 * indication that additional information is available.
	 */
	public static final int INDICATION_COMMENT = 1 << 7;

	/**
	 * Draws a gradient in the figure representing the cell content.
	 */
	public static final int INDICATION_GRADIENT = 1 << 8;

	/**
	 * Draws vertical text and image.
	 */
	public static final int DRAW_VERTICAL = 1 << 9;

	/**
	 * Returns the optimal width of the given cell (used by column resizing)
	 * 
	 * @param gc
	 *            The gc to draw on.
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 * @return the optimal width.
	 */
	public int getOptimalWidth(GC gc, int row, int col);

	/**
	 * This method is called from agile grid to draw a agile grid cell.
	 * 
	 * @param gc
	 *            The gc to draw on
	 * @param rect
	 *            The coordinates and size of the cell (add 1 to width and
	 *            height to include the borders)
	 * @param row
	 *            The row index of cell.
	 * @param col
	 *            The column index of cell.
	 */
	public void drawCell(GC gc, Rectangle rect, int row, int col);

	/**
	 * Sets the alignment of the cell content.
	 * 
	 * @param alignment
	 *            The OR-ed alignment constants for vertical and horizontal
	 *            alignment as defined in ICellRenderer.
	 * @see ALIGN_HORIZONTAL_CENTER
	 * @see ALIGN_HORIZONTAL_LEFT
	 * @see ALIGN_HORIZONTAL_RIGHT
	 * @see ALIGN_VERTICAL_CENTER
	 * @see ALIGN_VERTICAL_TOP
	 * @see ALIGN_VERTICAL_BOTTOM
	 */
	public void setAlignment(int alignment);

	/**
	 * @return Returns the alignment for the cell content. 2 or-ed constants,
	 *         one for horizontal, one for vertical alignment.
	 * @see ALIGN_HORIZONTAL_CENTER
	 * @see ALIGN_HORIZONTAL_LEFT
	 * @see ALIGN_HORIZONTAL_RIGHT
	 * @see ALIGN_VERTICAL_CENTER
	 * @see ALIGN_VERTICAL_TOP
	 * @see ALIGN_VERTICAL_BOTTOM
	 */
	public int getAlignment();

	/**
	 * Set the foreground color used to paint text.
	 * 
	 * @param fgcolor
	 *            The color or <code>null</code> to reset to default (black).
	 *            Note that also the default color can be set using
	 *            <code>setDefaultForeground(Color)</code>
	 * @see #setDefaultForeground(Color)
	 */
	public void setForeground(Color fgcolor);

	/**
	 * Changes the default foreground color that will be used when no other
	 * foreground color is set. (for example when
	 * <code>setForeground(null)</code> is called)
	 * 
	 * @param fgcolor
	 *            The foreground color to use.
	 * @see #setForeground(Color)
	 */
	public void setDefaultForeground(Color fgcolor);

	/**
	 * @return the defaultForeground
	 */
	public Color getDefaultForeground();

	/**
	 * Set the background color that should be used when painting the cell
	 * background.
	 * <p>
	 * If the <code>null</code> value is given, the default color will be used.
	 * The default color is settable using
	 * <code>setDefaultBacktround(Color)</code>
	 * 
	 * @param bgcolor
	 *            The color or <code>null</code> to reset to default.
	 * @see #setDefaultBackground(Color)
	 */
	public void setBackground(Color bgcolor);

	/**
	 * Changes the default background color that will be used when no background
	 * color is set via setBackground().
	 * 
	 * @param bgcolor
	 *            The color for the background.
	 * @see #setBackground(Color)
	 */
	public void setDefaultBackground(Color bgcolor);

	/**
	 * @return the defaultBackground
	 */
	public Color getDefaultBackground();

	/**
	 * @return returns the currently set foreground color. If none was set, the
	 *         default value is returned.
	 */
	public Color getForeground();

	/**
	 * @return returns the currently set background color. If none was set, the
	 *         default value is returned.
	 */
	public Color getBackground();

	/**
	 * Sets the font the renderer will use for drawing its content.
	 * 
	 * @param font
	 *            The font to use. Be aware that you must dispose fonts you have
	 *            created.
	 */
	public void setFont(Font font);

	/**
	 * @return Returns the font the renderer will use to draw the content.
	 */
	public Font getFont();

	/**
	 * @param value
	 *            If true, the comment sign is painted. Else it is omitted.
	 */
	public void setCommentIndication(boolean value);

}