/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

/**
 * This adapter class provides default implementations for the methods described
 * by the <code>IEditorActivationListener</code> interface.
 * 
 * <p>
 * Classes that wish to deal with agile grid editor activation event can extend this
 * class and override only the methods that they are interested in.
 * </p>
 * 
 * @author fourbroad
 * 
 */
public class EditorActivationAdapter implements
		IEditorActivationListener {

	/* (non-Javadoc)
	 * @see org.agilemore.agilegrid.IEditorActivationListener#afterEditorActivated(org.agilemore.agilegrid.EditorActivationEvent)
	 */
	public void afterEditorActivated(EditorActivationEvent event) {

	}

	/* (non-Javadoc)
	 * @see org.agilemore.agilegrid.IEditorActivationListener#afterEditorDeactivated(org.agilemore.agilegrid.EditorDeactivationEvent)
	 */
	public void afterEditorDeactivated(EditorDeactivationEvent event) {

	}

	/* (non-Javadoc)
	 * @see org.agilemore.agilegrid.IEditorActivationListener#beforeEditorActivated(org.agilemore.agilegrid.EditorActivationEvent)
	 */
	public void beforeEditorActivated(EditorActivationEvent event) {

	}

	/* (non-Javadoc)
	 * @see org.agilemore.agilegrid.IEditorActivationListener#beforeEditorDeactivated(org.agilemore.agilegrid.EditorDeactivationEvent)
	 */
	public void beforeEditorDeactivated(EditorDeactivationEvent event) {

	}

}
