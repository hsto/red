/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class defines constants for agile grid logging facilities. In this
 * implementation the constants are initialized based upon the value of the
 * Java System property {@code org.agilemore.agilegrid.Logging}. This implementation
 * defines all of the public constants as {@code final} which enables the
 * JVM/JIT to optimize out the logging code when appropriately configured.
 * <p/>
 * <p/>Alternate implementations of this class could;
 * <p/>
 * <ul>
 * <li>Initialize the public constants with manifest constants, ie.
 * {@code true} or {@code false} which would allow the Java compiler to
 * optimize out logging code at compile time.</li>
 * <p/>
 * <li>Remove the {@code final} qualifier from the constants and provide
 * additional methods to dynamically set the logging configuration at runtime.
 * </li>
 * </ul>
 *
 * To control logging within applications :
 * 
 * <pre>
 * <code>
 * System.setProperty("org.agilemore.agilegrid.Logging", "FINEST");
 * System.setProperty("java.util.logging.config.file", "/home/userhome/logging.properties");
 * </code>
 * </pre>
 *
 * <p/>
 * Sample logging properties :
 * <p/>
 * <pre>
 * <code>
 * # default file output is in user's home directory.
 * java.util.logging.FileHandler.pattern = %h/java%u.log
 * java.util.logging.FileHandler.limit = 50000
 * java.util.logging.FileHandler.count = 1
 * java.util.logging.FileHandler.formatter = java.util.logging.XMLFormatter
 * 
 * # Limit the message that are printed on the console to INFO and above
 * java.util.logging.ConsoleHandler.level = FINEST
 * java.util.logging.ConsoleHandler.formatter = java.util.logging.SimpleFormatter
 *
 * # Facility specific properties.
 * # Provides extra control for each logger.
 * #
 * # For example, set the net.jxta.impi.pipe.PipeResolver logger to only log SEVERE
 * # messages:
 * net.jxta.impi.pipe.PipeResolver.level = FINEST
 * </code>
 * </pre>
 */
public final class Logging {

    /**
     * Our Logger !
     */
    private final static Logger LOG = Logger.getLogger(Logging.class.getName());

    /**
     * The name of the system property from which we will attempt to read our
     * logging configuration.
     */
    public final static String AGILEGRID_LOGGING_PROPERTY = "org.peertoo.agilegrid.Logging";

    /**
     * The default logging level.
     */    
    private final static Level DEFAULT_LOGGING_LEVEL = Level.FINEST;

    /**
     * The logging level for this run.
     */
    public final static Level MIN_SHOW_LEVEL;

    /**
     * Is Level.FINEST enabled?
     */
    public final static boolean SHOW_FINEST;

    /**
     * Is Level.FINER enabled?
     */
    public final static boolean SHOW_FINER;

    /**
     * Is Level.FINE enabled?
     */
    public final static boolean SHOW_FINE;

    /**
     * Is Level.CONFIG enabled?
     */
    public final static boolean SHOW_CONFIG;

    /**
     * Is Level.INFO enabled?
     */
    public final static boolean SHOW_INFO;

    /**
     * Is Level.WARNING enabled?
     */
    public final static boolean SHOW_WARNING;

    /**
     * Is Level.SEVERE enabled?
     */
    public final static boolean SHOW_SEVERE;

    /* Initialize the constants */
    static {
        Level setLevel = DEFAULT_LOGGING_LEVEL;

        try {
            String propertyLevel = System.getProperty(AGILEGRID_LOGGING_PROPERTY);

            if (null != propertyLevel) {
                setLevel = Level.parse(propertyLevel);
            }
        } catch (SecurityException disallowed) {
            LOG.log(Level.WARNING, "Could not read configuration property.", disallowed);
        }

        // Set the default level for the JXTA packages so that everything below
        // inherits our default.
        MIN_SHOW_LEVEL = setLevel;
               
        SHOW_FINEST = MIN_SHOW_LEVEL.intValue() <= Level.FINEST.intValue();
        SHOW_FINER = MIN_SHOW_LEVEL.intValue() <= Level.FINER.intValue();
        SHOW_FINE = MIN_SHOW_LEVEL.intValue() <= Level.FINE.intValue();
        SHOW_CONFIG = MIN_SHOW_LEVEL.intValue() <= Level.CONFIG.intValue();
        SHOW_INFO = MIN_SHOW_LEVEL.intValue() <= Level.INFO.intValue();
        SHOW_WARNING = MIN_SHOW_LEVEL.intValue() <= Level.WARNING.intValue();
        SHOW_SEVERE = MIN_SHOW_LEVEL.intValue() <= Level.SEVERE.intValue();

        if (Logging.SHOW_CONFIG && LOG.isLoggable(Level.CONFIG)) {
            LOG.config("Logging enabled for level : " + MIN_SHOW_LEVEL);
        }
    }

    /**
     * This class is not meant be instantiated.
     */
    private Logging() {}
}
