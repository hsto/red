/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid;

import org.eclipse.swt.SWT;

/**
 * Provides some extensions for the agile grid.
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 * 
 */
public class SWTX {
	/**
	 * 
	 */
	public static final int AUTO_SCROLL = 1 << 10 | SWT.V_SCROLL | SWT.H_SCROLL;

	/**
	 * Style bit for agile grid that ensures the agile grid always covers the
	 * whole space.<br>
	 * Uses the policy of widening the last column.
	 */
	public static final int FILL_WITH_LASTCOL = 1 << 17;

	/**
	 * Style bit for agile grid that ensures the agile grid always covers the
	 * whole space. <br>
	 * Uses the policy to add a dummy column at the end that occupies unused
	 * space.
	 */
	public static final int FILL_WITH_DUMMYCOL = 1 << 18;

	/**
	 * Style bit that makes agile grid select the whole row.
	 */
	public static final int ROW_SELECTION = 1 << 27;

	/**
	 * Style bit that makes agile grid select the whole column.
	 */
	public static final int COLUMN_SELECTION = 1 << 28;

	/**
	 * Style bit that makes agile grid draw left and top header cells in a
	 * different style when the focused cell is in their row/column. This mimics
	 * the MS Excel behavior that helps find the currently focused cell(s).
	 */
	public static final int MARK_FOCUS_HEADERS = 1 << 29;

	/**
	 * Style bit that makes agile grid draw left and top header cells in a
	 * different style when the selection cell is in their row/column. This
	 * mimics the MS Excel behavior that helps find the currently selected
	 * cell(s).
	 */
	public static final int MARK_SELECTION_HEADERS = 1 << 30;

	/**
	 * Style bit that makes agile grid show the grid lines or not.
	 */
	public static final int NOT_SHOW_GRID_LINE = 1 << 31;
}