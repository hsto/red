/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */

package org.agilemore.agilegrid.editors;

import java.text.MessageFormat;

import org.agilemore.agilegrid.CellEditor;
import org.agilemore.agilegrid.AgileGrid;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

/**
 * A cell editor that presents a list of items in a combo box. The cell editor's
 * value is the zero-based index of the selected item.
 * <p>
 * This class may be instantiated; it is not intended to be subclassed.
 * </p>
 */
public class ComboBoxCellEditor extends CellEditor {

	/**
	 * The list of items to present in the combo box.
	 */
	private String items[];

	/**
	 * The zero-based index of the selected item.
	 */
	int selection;

	/**
	 * The custom combo box control.
	 */
	private CCombo combo;

	/**
	 * Default ComboBoxCellEditor style.
	 */
	private static final int defaultStyle = SWT.NONE;

	private Cursor arrowCursor = new Cursor(Display.getDefault(),
			SWT.CURSOR_ARROW);

	/**
	 * Creates a new cell editor with a combo containing no items. Initally, the
	 * cell editor has no cell validator.
	 * 
	 * @param agileGrid
	 *            The agile grid that this cell edit works for.
	 */
	public ComboBoxCellEditor(AgileGrid agileGrid) {
		super(agileGrid, defaultStyle);
	}

	/**
	 * Creates a new cell editor with a combo containing no items. Initally, the
	 * cell editor has no cell validator.
	 * 
	 * @param agileGrid
	 *            The agile grid that this cell edit works for.
	 * @param style
	 *            The style bits.
	 */
	public ComboBoxCellEditor(AgileGrid agileGrid, int style) {
		super(agileGrid, style);
	}

	/**
	 * Creates a new cell editor with a combo containing the given list of
	 * choices and parented under the given control. The cell editor value is
	 * the zero-based index of the selected item. Initially, the cell editor has
	 * no cell validator and the first item in the list is selected.
	 * 
	 * @param agileGrid
	 *            The agile grid that this cell editor works for.
	 * @param items
	 *            The list of strings for the combo box.
	 * @param style
	 *            The style bits.
	 */
	public ComboBoxCellEditor(AgileGrid agileGrid, String[] items, int style) {
		super(agileGrid, style);
		setItems(items);
	}

	/**
	 * Returns the list of choices for the combo box.
	 * 
	 * @return The list of choices for the combo box.
	 */
	public String[] getItems() {
		return this.items;
	}

	/**
	 * Sets the list of choices for the combo box.
	 * 
	 * @param items
	 *            The list of choices for the combo box.
	 */
	public void setItems(String items[]) {
		this.items = items;
		populateComboBoxItems();
	}

	/* (non-Javadoc)
	 * @see org.agilemore.agilegrid.CellEditor#createControl(org.agilemore.agilegrid.AgileGrid)
	 */
	protected Control createControl(AgileGrid agileGrid) {
		combo = new CCombo(agileGrid, this.getStyle());
		combo.setFont(agileGrid.getFont());

		populateComboBoxItems();

		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				applyEditorValueAndDeactivate();
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				selection = combo.getSelectionIndex();
			}

		});

		combo.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				ComboBoxCellEditor.this.focusLost();
			}
		});

//		 combo.setCursor(arrowCursor);
		return combo;
	}

	/**
	 * The <code>ComboBoxCellEditor</code> implementation of this
	 * <code>CellEditor</code> framework method returns the zero-based index
	 * of the current selection.
	 * 
	 * @return The current selection.
	 */
	@Override
	protected Object doGetValue() {
		// return new Integer(selection);
		return combo.getText();
	}

	/**
	 * The <code>ComboBoxCellEditor</code> implementation of this
	 * <code>CellEditor</code> framework method accepts a zero-based index of
	 * a selection.
	 * 
	 * @see org.agilemore.agilegrid.CellEditor#doSetValue(java.lang.Object)
	 */
	@Override
	protected void doSetValue(Object value) {
		if (value instanceof Integer) {
			selection = ((Integer) value).intValue();
			combo.select(selection);
		} else if (value instanceof String) {
			setSelectionToClosestMatch((String) value);
		} else {
			if(value!=null){
				setSelectionToClosestMatch(value.toString());
			}
		}
	}

	/**
	 * Updates the list of choices for the combo box for the current control.
	 */
	private void populateComboBoxItems() {
		if (combo != null && items != null) {
			combo.removeAll();
			for (int i = 0; i < items.length; i++) {
				combo.add(items[i], i);
			}

			setValueValid(true);
			selection = 0;
		}
	}

	/**
	 * Applies the currently selected value and deactivates the cell editor
	 */
	void applyEditorValueAndDeactivate() {
		// must set the selection before getting value
		selection = combo.getSelectionIndex();
		Object newValue = doGetValue();
		markDirty();
		boolean isValid = isCorrect(newValue);
		setValueValid(isValid);

		if (!isValid) {
			// Only format if the 'index' is valid
			if (items.length > 0 && selection >= 0 && selection < items.length) {
				// try to insert the current value into the error message.
				setErrorMessage(MessageFormat.format(getErrorMessage(),
						new Object[] { items[selection] }));
			} else {
				// Since we don't have a valid index, assume we're using an
				// 'edit'
				// combo so format using its text value
				setErrorMessage(MessageFormat.format(getErrorMessage(),
						new Object[] { combo.getText() }));
			}
		}

		fireApplyEditorValue();
		deactivate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.CellEditor#focusLost()
	 */
	@Override
	protected void focusLost() {
		if (isActivated()) {
			this.applyEditorValueAndDeactivate();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.CellEditor#onKeyPressed(org.eclipse.swt.events.KeyEvent)
	 */
	@Override
	protected void onKeyPressed(KeyEvent keyEvent) {
		if (keyEvent.character == SWT.ESC) { // Escape character
			fireCancelEditor();
		} else if (keyEvent.character == SWT.TAB) { // tab key
			applyEditorValueAndDeactivate();
		}
	}

	/**
	 * Overwrite the onTraverse method to ignore arrowup and arrowdown events so
	 * that they get interpreted by the editor control.
	 * <p>
	 * Comment that out if you want the up and down keys move the editor.<br>
	 */
	protected void onTraverse(TraverseEvent e) {
		// set selection to the appropriate next element:
		switch (e.keyCode) {
		case SWT.ARROW_UP: // Go to previous item
		case SWT.ARROW_DOWN: // Go to next item
		{
			// Just don't treat the event
			break;
		}
		default: {
			if (e.detail == SWT.TRAVERSE_ESCAPE
					|| e.detail == SWT.TRAVERSE_RETURN) {
				e.doit = false;
			} else {
				super.onTraverse(e);
			}
			break;
		}
		}
	}

	private void setSelectionToClosestMatch(String content) {
		content = content.toLowerCase();

		String[] citems = combo.getItems();
		String[] items = new String[citems.length];
		for (int i = 0; i < citems.length; i++)
			items[i] = citems[i].toLowerCase();

		for (int length = content.length(); length >= 0; length--) {
			String part = content.substring(0, length);
			for (int i = 0; i < items.length; i++)
				if (items[i].startsWith(part)) {
					combo.select(i);
					return;
				}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.CellEditor#dispose()
	 */
	@Override
	public void dispose() {
		if (arrowCursor != null) {
			arrowCursor.dispose();
			arrowCursor = null;
		}
		super.dispose();
	}

}
