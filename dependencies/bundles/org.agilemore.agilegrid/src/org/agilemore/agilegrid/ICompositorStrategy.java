/*
 * Copyright (C) 2008 by Sihong Zhu
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Sihong Zhu (fourbroad@gmail.com)
 */
package org.agilemore.agilegrid;

/**
 * Provides a transparently sorted content of agile grid. For content calls, the row
 * indices are mapped according to the sorting.
 * 
 * @author fourbroad
 */
public interface ICompositorStrategy {

	/**
	 * Maps the given row index that references a visible row, to one that is
	 * internally used in the content provider. This visual rearrangement leads
	 * to changed shown row indices, but using this method, the content internal
	 * row indices stay the same.
	 * 
	 * @param shownRow
	 *            The row index as displayed by the agile grid.
	 * @return The row index as used in the content provider. This stays
	 *         unchanged even if the visual arrangement is changed (e.g. when
	 *         sorting the agile grid).
	 */
	public int mapRowIndexToContent(int shownRow);

	/**
	 * Maps the given row index from a content internal to one that references
	 * visualized agile grid rows. This is usually used to do something with the
	 * agile grid from within the content provider.
	 * 
	 * @param contentRow
	 *            The row index as used in the content provider.
	 * @return Returns the row index as needed/used by the agile grid to display the
	 *         data.
	 */
	public int mapRowIndexToAgileGrid(int contentRow);

	/**
	 * Returns the column that is used for sorting, or -1 if no sorting is
	 * present or sorting is SORT_NONE.
	 * 
	 * @return The column number that is used for sorting.
	 */
	public int getSortColumn();

	/**
	 * Sets the sort column for the agile grid. Note that this should equal the real
	 * one defined by the comparator given to sort().
	 * 
	 * @param column
	 *            The column number.
	 */
	public void setSortColumn(int column);

	/**
	 * Returns the current sort state of the sorted agile grid. Can be:
	 * <p>
	 * <ul>
	 * <li>SORT_NONE: Unsorted (default)
	 * <li>SORT_UP: Sorted with largest value up
	 * <li>SORT_DOWN: Sorted with largest value down.
	 * </ul>
	 * 
	 * @return The current sort state of the sorted agile grid.
	 */
	public int getSortState();

	/**
	 * Sorts the elements of content provider so that the retrieval methods by
	 * index (e.g. of type <code>method(int row, int col)</code>) return the
	 * content ordered in the given direction.
	 * <p>
	 * Note: To make the agile grid reflect this sorting, it must be
	 * refreshed/redrawn!
	 * <p>
	 * Note: Often it is desired that there is some visual sign of how the
	 * sorting is.
	 * 
	 * @param comparator
	 *            The ColumnSortComparator that knows how to sort the rows!
	 */
	public void sort(ColumnSortComparator comparator);
}
