
Agile Grid  Version 1.1.0.M20081121-0001

URL: http://agilegrid.sourceforge.net/ or http://www.agilemore.com/
_______________________________________________________________________

Agile Grid is a new implement of table control based on the Eclipse SWT. You 
can implement various complex electronic table with agile grid. Agile grid's 
design results from many references to the TableViewer of Eclipse JFace and 
pay attention to the design of agility and extendablity especially. According 
to the requirement of some application, programmers can customize the layout, 
rendering, cell editor and keybord's navigation of agile grid. The main features 
as follow:
1. Different cells may have different rendering style. Agile Grid provides 
   some of rendering class such as TextCellRenderer, HeaderCellRenderer, 
   ButtonCellRenderer, CheckboxCellRenderer etc. Programmer can implement more 
   individualizing rendering class by implementing the interface ICellRenderer.
2. According to the cell content, Agile grid can provides appropriate editor,  
   for examples  editing  text with  TextCellEditor,  providing a series of 
   valid value for selecting with ComboBoxCellEditor, making a boolean choice 
   with CheckboxCellEditor,  using  DialogCellEditor to provide more complex 
   and  intuitionistic editing manner. Programmer can implement more 
   individualizing editing class by extending the abstract class CellEditor.
3. A cell may span over several rows and/or columns.
4. Agile Grid has a left header and a top header, the left header indicates row 
   number, the top header indicates column number, which style and label can be 
   customized. The left and top header can be hidden or visible individually.
5. The indicated row or column can be froze, which still keep visible at all 
   times when scrolling other rows or columns.
6. The grid line can be shown or hidden.

For a detailed function description refer to the api documentation that 
is included in the source files.

Agile Grid is a open source project,  please visit the project through : 
http://sourceforge.net/projects/agilegrid/, Any feedbacks are welcome by the 
author :  Sihong Zhu (fourbroad@gmail.com)
