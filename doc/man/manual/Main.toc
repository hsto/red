\select@language {english}
\contentsline {chapter}{Summary}{i}{chapter*.1}
\contentsline {chapter}{Preface}{iii}{chapter*.2}
\contentsline {chapter}{\numberline {1}Navigating around the views and editors}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Overview of the UI}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Element Explorer View}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}File Explorer View}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Associations View}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Comments View}{4}{section.1.5}
\contentsline {section}{\numberline {1.6}Glossary View}{4}{section.1.6}
\contentsline {section}{\numberline {1.7}Search View}{4}{section.1.7}
\contentsline {section}{\numberline {1.8}Element Editor}{4}{section.1.8}
\contentsline {subsection}{\numberline {1.8.1}Generic Text Editor}{5}{subsection.1.8.1}
\contentsline {section}{\numberline {1.9}Management and Tracing Tab}{5}{section.1.9}
\contentsline {chapter}{\numberline {2}Organizing Workspace}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Project}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Folder}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Document}{9}{section.2.3}
\contentsline {section}{\numberline {2.4}Searching for Elements}{9}{section.2.4}
\contentsline {section}{\numberline {2.5}Sorting and Filtering Elements}{10}{section.2.5}
\contentsline {section}{\numberline {2.6}Moving Elements}{10}{section.2.6}
\contentsline {section}{\numberline {2.7}Renaming Elements}{10}{section.2.7}
\contentsline {section}{\numberline {2.8}Locking Elements}{11}{section.2.8}
\contentsline {section}{\numberline {2.9}Documenting changes}{11}{section.2.9}
\contentsline {section}{\numberline {2.10}Creating references}{12}{section.2.10}
\contentsline {section}{\numberline {2.11}Adding comments}{12}{section.2.11}
\contentsline {chapter}{\numberline {3}Managing Glossary And Reference}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Creating Glossary}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Creating Glossary Entry}{14}{section.3.2}
\contentsline {section}{\numberline {3.3}Creating Glossary Reference}{14}{section.3.3}
\contentsline {section}{\numberline {3.4}Creating Element Reference}{15}{section.3.4}
\contentsline {chapter}{\numberline {4}Specification Elements}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}Vision}{17}{section.4.1}
\contentsline {section}{\numberline {4.2}Assumption}{18}{section.4.2}
\contentsline {section}{\numberline {4.3}Goal}{18}{section.4.3}
\contentsline {section}{\numberline {4.4}Personas and Storyboard}{19}{section.4.4}
\contentsline {section}{\numberline {4.5}Stakeholder}{20}{section.4.5}
\contentsline {section}{\numberline {4.6}Requirements and Acceptance Test}{21}{section.4.6}
\contentsline {section}{\numberline {4.7}Model fragments}{22}{section.4.7}
\contentsline {section}{\numberline {4.8}Test Case}{23}{section.4.8}
\contentsline {section}{\numberline {4.9}Scenario}{23}{section.4.9}
\contentsline {chapter}{\numberline {5}Exporting and Importing other format}{25}{chapter.5}
\contentsline {section}{\numberline {5.1}Exporting report}{25}{section.5.1}
\contentsline {section}{\numberline {5.2}Exporting comments}{26}{section.5.2}
\contentsline {section}{\numberline {5.3}Exporting to MPL (Prolog file)}{26}{section.5.3}
\contentsline {section}{\numberline {5.4}Importing an existing MPL (Prolog file)}{28}{section.5.4}
\contentsline {chapter}{\numberline {6}Model Weaving}{29}{chapter.6}
