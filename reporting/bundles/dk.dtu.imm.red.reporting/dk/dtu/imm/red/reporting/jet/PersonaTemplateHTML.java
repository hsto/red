package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.specificationelements.persona.*;
import dk.dtu.imm.red.specificationelements.persona.storyboard.*;
import dk.dtu.imm.red.specificationelements.*;
import javax.xml.bind.DatatypeConverter;
import java.io.FileOutputStream;
import java.io.File;

public class PersonaTemplateHTML {

  protected static String nl;
  public static synchronized PersonaTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    PersonaTemplateHTML result = new PersonaTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<h4> Persona - ";
  protected final String TEXT_2 = " </h4>" + NL + "<style type=\"text/css\">" + NL + "p{max-width:700px;}" + NL + "</style>" + NL + "<p>" + NL + "Name: ";
  protected final String TEXT_3 = " <br />" + NL + "Age: ";
  protected final String TEXT_4 = " <br />" + NL + "Occupation: ";
  protected final String TEXT_5 = " <br />";
  protected final String TEXT_6 = NL + "\t\t<img src=\"data:image/png;base64,";
  protected final String TEXT_7 = "\" /> <br />" + NL + "\t";
  protected final String TEXT_8 = NL + "\t\t<img src=\"";
  protected final String TEXT_9 = "\" /> <br />" + NL + "\t";
  protected final String TEXT_10 = NL + "\tDescription: ";
  protected final String TEXT_11 = " <br />";
  protected final String TEXT_12 = NL;
  protected final String TEXT_13 = NL + "\tIssues: ";
  protected final String TEXT_14 = " <br />";
  protected final String TEXT_15 = NL;
  protected final String TEXT_16 = NL + "\tNarrative: ";
  protected final String TEXT_17 = " <br />";
  protected final String TEXT_18 = NL;
  protected final String TEXT_19 = NL + "\tStoryboard: <br />" + NL + "\t<table width=\"100%\" border=\"1\">" + NL + "\t\t<tr>" + NL + "\t\t\t<td width=\"66%\" colspan=\"2\">";
  protected final String TEXT_20 = "</td>" + NL + "\t\t\t<td width=\"34%\">";
  protected final String TEXT_21 = "</td>" + NL + "\t\t</tr>" + NL + "\t\t<tr>" + NL + "\t\t\t<td>";
  protected final String TEXT_22 = "</td>" + NL + "\t\t\t<td>";
  protected final String TEXT_23 = "</td>" + NL + "\t\t\t<td>";
  protected final String TEXT_24 = "</td>" + NL + "\t\t</tr>" + NL + "\t\t<tr>" + NL + "\t\t\t<td width=\"34%\">";
  protected final String TEXT_25 = "</td>" + NL + "\t\t\t<td width=\"66%\" colspan=\"2\">";
  protected final String TEXT_26 = "</td>" + NL + "\t\t</tr>\t\t\t" + NL + "\t</table>";
  protected final String TEXT_27 = NL + "</p>";
  protected final String TEXT_28 = NL;

	public String generate(Object argument, boolean inlineImages, String path)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Persona persona = (Persona) argument; 
    stringBuffer.append(TEXT_1);
    stringBuffer.append( persona.getName() );
    stringBuffer.append(TEXT_2);
    stringBuffer.append( persona.getName() );
    stringBuffer.append(TEXT_3);
    stringBuffer.append( persona.getAge() );
    stringBuffer.append(TEXT_4);
    stringBuffer.append( persona.getJob() );
    stringBuffer.append(TEXT_5);
    
if (persona.getPictureData() != null) {
	if (inlineImages) {
	
    stringBuffer.append(TEXT_6);
    stringBuffer.append( DatatypeConverter.printBase64Binary(persona.getPictureData()) );
    stringBuffer.append(TEXT_7);
    
	} else {
		try {
			FileOutputStream fos = new FileOutputStream(path + File.separatorChar + persona.getUniqueID() + ".png");
			fos.write(persona.getPictureData());
			fos.close();
		} catch (Exception e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	
    stringBuffer.append(TEXT_8);
    stringBuffer.append(persona.getUniqueID() + ".png" );
    stringBuffer.append(TEXT_9);
    
	}
}

     
if (persona.getDescription() != null && 
	!persona.getDescription().toString().isEmpty()) {

    stringBuffer.append(TEXT_10);
    stringBuffer.append( persona.getDescription() );
    stringBuffer.append(TEXT_11);
    
}

    stringBuffer.append(TEXT_12);
     
if (persona.getIssues() != null && 
	!persona.getIssues().toString().isEmpty()) {

    stringBuffer.append(TEXT_13);
    stringBuffer.append( persona.getIssues() );
    stringBuffer.append(TEXT_14);
    
}

    stringBuffer.append(TEXT_15);
     
if (persona.getNarrative() != null && 
	!persona.getNarrative().toString().isEmpty()) {

    stringBuffer.append(TEXT_16);
    stringBuffer.append( persona.getNarrative() );
    stringBuffer.append(TEXT_17);
    
}

    stringBuffer.append(TEXT_18);
    
if (persona.getStoryboards().size() > 0) {
	Storyboard storyboard = persona.getStoryboards().get(0);
	String[] imageUrls = new String[storyboard.getPanels().size()];
	for (int i = 0; i < storyboard.getPanels().size(); i++) {
		StoryboardPanel panel = storyboard.getPanels().get(i);
		if (panel.getImageData() != null) {
			if (inlineImages) {
				imageUrls[i] = "<img align=\"center\" src=\"data:image/png;base64," 
					+ DatatypeConverter.printBase64Binary(
						panel.getImageData()) + "\"/><br />" + panel.getCaption();
			} else {
				try {
					FileOutputStream fos = new FileOutputStream(
						path + File.separatorChar + 
						persona.getUniqueID() + "_" + i + ".png");
					fos.write(panel.getImageData());
					fos.close();
					imageUrls[i] = "<img align=\"center\" src=\"" + persona.getUniqueID() + "_" + i + ".png\"/><br />" + panel.getCaption();
				} catch (Exception e) {
					e.printStackTrace(); LogUtil.logError(e);
				}
			}
		}
	}

    stringBuffer.append(TEXT_19);
    stringBuffer.append( storyboard.getIntroduction().toString() );
    stringBuffer.append(TEXT_20);
    stringBuffer.append( imageUrls[0] );
    stringBuffer.append(TEXT_21);
    stringBuffer.append( imageUrls[1] );
    stringBuffer.append(TEXT_22);
    stringBuffer.append( imageUrls[2] );
    stringBuffer.append(TEXT_23);
    stringBuffer.append( imageUrls[3] );
    stringBuffer.append(TEXT_24);
    stringBuffer.append( imageUrls[4] );
    stringBuffer.append(TEXT_25);
    stringBuffer.append( storyboard.getConclusion().toString() );
    stringBuffer.append(TEXT_26);
    
}

    stringBuffer.append(TEXT_27);
    stringBuffer.append(TEXT_28);
    return stringBuffer.toString();
  }
}