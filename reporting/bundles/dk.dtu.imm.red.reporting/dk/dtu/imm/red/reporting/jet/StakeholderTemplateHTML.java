package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.specificationelements.stakeholder.*;

public class StakeholderTemplateHTML
{
  protected static String nl;
  public static synchronized StakeholderTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    StakeholderTemplateHTML result = new StakeholderTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<h4> Stakeholder - ";
  protected final String TEXT_2 = " </h4>" + NL + "<style type=\"text/css\">" + NL + "p{max-width:700px;}" + NL + "</style>" + NL + "<p>" + NL + "Stakeholder: ";
  protected final String TEXT_3 = " <br />" + NL + "Type: ";
  protected final String TEXT_4 = " <br />" + NL + "Exposure ";
  protected final String TEXT_5 = ", " + NL + "Power ";
  protected final String TEXT_6 = ", " + NL + "Urgency ";
  protected final String TEXT_7 = ", " + NL + "Importance ";
  protected final String TEXT_8 = " <br />" + NL + "Description: ";
  protected final String TEXT_9 = " <br />" + NL + "Stake: ";
  protected final String TEXT_10 = " <br />" + NL + "Engagement: ";
  protected final String TEXT_11 = " <br />" + NL + "</p>";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Stakeholder stakeholder = (Stakeholder) argument; 
    stringBuffer.append(TEXT_1);
    stringBuffer.append( stakeholder.getName() );
    stringBuffer.append(TEXT_2);
    stringBuffer.append( stakeholder.getName() );
    stringBuffer.append(TEXT_3);
    stringBuffer.append( stakeholder.getType() );
    stringBuffer.append(TEXT_4);
    stringBuffer.append( stakeholder.getExposure() );
    stringBuffer.append(TEXT_5);
    stringBuffer.append( stakeholder.getPower() );
    stringBuffer.append(TEXT_6);
    stringBuffer.append( stakeholder.getUrgency() );
    stringBuffer.append(TEXT_7);
    stringBuffer.append( stakeholder.getImportance() );
    stringBuffer.append(TEXT_8);
    stringBuffer.append( stakeholder.getDescription() );
    stringBuffer.append(TEXT_9);
    stringBuffer.append( stakeholder.getStake() );
    stringBuffer.append(TEXT_10);
    stringBuffer.append( stakeholder.getEngagement() );
    stringBuffer.append(TEXT_11);
    return stringBuffer.toString();
  }
}
