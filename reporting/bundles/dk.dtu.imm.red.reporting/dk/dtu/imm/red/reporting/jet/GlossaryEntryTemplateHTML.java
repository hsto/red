package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.specificationelements.glossary.*;

public class GlossaryEntryTemplateHTML
{
  protected static String nl;
  public static synchronized GlossaryEntryTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    GlossaryEntryTemplateHTML result = new GlossaryEntryTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<tr>" + NL + "\t<td>";
  protected final String TEXT_2 = "</td>" + NL + "\t<td>";
  protected final String TEXT_3 = "</td>" + NL + "\t<td>";
  protected final String TEXT_4 = "</td>" + NL + "\t<td>";
  protected final String TEXT_5 = "</td>" + NL + "</tr>";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     GlossaryEntry glossaryEntry = (GlossaryEntry) argument; 
    stringBuffer.append(TEXT_1);
    stringBuffer.append(glossaryEntry.getTerm());
    stringBuffer.append(TEXT_2);
    stringBuffer.append(glossaryEntry.getAbbreviations());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(glossaryEntry.getSynonyms());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(glossaryEntry.getDefinition());
    stringBuffer.append(TEXT_5);
    return stringBuffer.toString();
  }
}
