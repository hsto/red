package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.core.project.*;

public class ProjectTemplateHTML
{
  protected static String nl;
  public static synchronized ProjectTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    ProjectTemplateHTML result = new ProjectTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<style type=\"text/css\">" + NL + "p{max-width:700px;}" + NL + "</style>" + NL + "<p>" + NL + "<h2>Authors:</h2>" + NL + "<table border=\"1\">" + NL + "\t<tr>" + NL + "\t\t<td>Name</td>" + NL + "\t\t<td>ID</td>" + NL + "\t\t<td>E-mail</td>" + NL + "\t\t<td>Phone</td>" + NL + "\t\t<td>Skype</td>" + NL + "\t\t<td>Comment</td>" + NL + "\t</tr>";
  protected final String TEXT_2 = NL + "\t<tr>" + NL + "\t\t<td>";
  protected final String TEXT_3 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_4 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_5 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_6 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_7 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_8 = "</td>" + NL + "\t</tr>";
  protected final String TEXT_9 = NL + "</table>" + NL + "</p>" + NL + "<p>" + NL + "<h2>Project dates:</h2>" + NL + "<table border=\"1\">" + NL + "\t<tr>" + NL + "\t\t<td>Date</td>" + NL + "\t\t<td>Comment</td>" + NL + "\t</tr>";
  protected final String TEXT_10 = NL + "\t<tr>" + NL + "\t\t<td>";
  protected final String TEXT_11 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_12 = "</td>" + NL + "\t</tr>";
  protected final String TEXT_13 = NL + "</table>" + NL + "</p>";
  protected final String TEXT_14 = NL + "<h2>Description</h2>" + NL + "<p>";
  protected final String TEXT_15 = NL;
  protected final String TEXT_16 = NL + "</p>";
  protected final String TEXT_17 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Project project = (Project) argument; 
    stringBuffer.append(TEXT_1);
    
for (UserTableEntry entry : project.getUsers()) {

    stringBuffer.append(TEXT_2);
    stringBuffer.append( entry.getUser().getName() );
    stringBuffer.append(TEXT_3);
    stringBuffer.append( entry.getUser().getId() );
    stringBuffer.append(TEXT_4);
    stringBuffer.append( entry.getUser().getEmail() );
    stringBuffer.append(TEXT_5);
    stringBuffer.append( entry.getUser().getPhoneNumber() );
    stringBuffer.append(TEXT_6);
    stringBuffer.append( entry.getUser().getSkype() );
    stringBuffer.append(TEXT_7);
    stringBuffer.append( entry.getComment() );
    stringBuffer.append(TEXT_8);
    
}

    stringBuffer.append(TEXT_9);
    
for (ProjectDate date : project.getProjectDates()) {

    stringBuffer.append(TEXT_10);
    stringBuffer.append( date.getDate() );
    stringBuffer.append(TEXT_11);
    stringBuffer.append( date.getComment() );
    stringBuffer.append(TEXT_12);
    
}

    stringBuffer.append(TEXT_13);
    
if (project.getInformation() != null 
	&& !project.getInformation().toString().isEmpty()) {

    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_15);
    stringBuffer.append( project.getInformation().toString() );
    stringBuffer.append(TEXT_16);
    
}

    stringBuffer.append(TEXT_17);
    return stringBuffer.toString();
  }
}
