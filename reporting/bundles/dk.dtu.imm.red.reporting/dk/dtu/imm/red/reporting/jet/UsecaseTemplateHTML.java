package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.specificationelements.usecase.*;

public class UsecaseTemplateHTML
{
  protected static String nl;
  public static synchronized UsecaseTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    UsecaseTemplateHTML result = new UsecaseTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<h4> Usecase - ";
  protected final String TEXT_2 = " </h4> " + NL + "<table border=\"1\">" + NL + "   <tr>" + NL + "      <td>";
  protected final String TEXT_3 = "</td>" + NL + "      <td>";
  protected final String TEXT_4 = "</td> " + NL + "   </tr>" + NL + "   <tr>" + NL + "      <td>Description</td>" + NL + "\t  <td>";
  protected final String TEXT_5 = "</td>" + NL + "   </tr>" + NL + "   ";
  protected final String TEXT_6 = " " + NL + "\t<tr>" + NL + "\t\t<td>";
  protected final String TEXT_7 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_8 = "</td> " + NL + "\t</tr>" + NL + "\t";
  protected final String TEXT_9 = " " + NL + "     " + NL + "    <tr>" + NL + "      <td>Trigger</td>" + NL + "\t  <td>";
  protected final String TEXT_10 = "</td>" + NL + "   </tr>" + NL + "    <tr>" + NL + "      <td>Parameter</td>" + NL + "\t  <td>";
  protected final String TEXT_11 = "</td>" + NL + "   </tr> " + NL + "   <tr>" + NL + "      <td>Scenarios</td>" + NL + "\t  <td>";
  protected final String TEXT_12 = "</td>" + NL + "   </tr>" + NL + "    <tr>" + NL + "      <td>Result</td>" + NL + "\t  <td>";
  protected final String TEXT_13 = "</td>" + NL + "   </tr>" + NL + "   <tr>" + NL + "      <td>Incidence</td>" + NL + "\t  <td>";
  protected final String TEXT_14 = "</td>" + NL + "   </tr> " + NL + "   <tr>" + NL + "      <td>Duration</td>" + NL + "\t  <td>";
  protected final String TEXT_15 = "</td>" + NL + "   </tr> " + NL + "</table> ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Usecase usecase = (Usecase) argument; 
    stringBuffer.append(TEXT_1);
    stringBuffer.append( usecase.getName() );
    stringBuffer.append(TEXT_2);
    stringBuffer.append(usecase.getId());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(usecase.getName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(usecase.getDescription());
    stringBuffer.append(TEXT_5);
    for (Actor actor : usecase.getPrimaryReferences().getActors()) {
    stringBuffer.append(TEXT_6);
    stringBuffer.append( actor.getName() );
    stringBuffer.append(TEXT_7);
    stringBuffer.append( actor.getId() );
    stringBuffer.append(TEXT_8);
    }
    stringBuffer.append(TEXT_9);
    stringBuffer.append(usecase.getTrigger());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(usecase.getParameter());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(usecase.getDescription());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(usecase.getResult());
    stringBuffer.append(TEXT_13);
    stringBuffer.append(usecase.getIncidence());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(usecase.getDuration());
    stringBuffer.append(TEXT_15);
    return stringBuffer.toString();
  }
}
