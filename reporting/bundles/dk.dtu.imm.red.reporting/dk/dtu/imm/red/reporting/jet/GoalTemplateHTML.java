package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.specificationelements.goal.*;

public class GoalTemplateHTML
{
  protected static String nl;
  public static synchronized GoalTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    GoalTemplateHTML result = new GoalTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<h4> ";
  protected final String TEXT_2 = ", ";
  protected final String TEXT_3 = " </h4>" + NL + "<style type=\"text/css\">" + NL + "p{max-width:700px;}" + NL + "</style>" + NL + "<p>" + NL + "ID: ";
  protected final String TEXT_4 = ", Goal: ";
  protected final String TEXT_5 = " <br />";
  protected final String TEXT_6 = NL;
  protected final String TEXT_7 = " <br />" + NL + "</p>";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Goal goal = (Goal) argument; 
    stringBuffer.append(TEXT_1);
    stringBuffer.append( goal.getName() );
    stringBuffer.append(TEXT_2);
    stringBuffer.append( goal.getGoalID() );
    stringBuffer.append(TEXT_3);
    stringBuffer.append( goal.getGoalID() );
    stringBuffer.append(TEXT_4);
    stringBuffer.append( goal.getName() );
    stringBuffer.append(TEXT_5);
    stringBuffer.append(TEXT_6);
    stringBuffer.append( goal.getExplanation() );
    stringBuffer.append(TEXT_7);
    return stringBuffer.toString();
  }
}
