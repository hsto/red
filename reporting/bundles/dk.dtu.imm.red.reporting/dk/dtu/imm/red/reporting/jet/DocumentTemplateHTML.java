package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.core.element.document.*;

public class DocumentTemplateHTML
{
  protected static String nl;
  public static synchronized DocumentTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    DocumentTemplateHTML result = new DocumentTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<style type=\"text/css\">" + NL + "p{max-width:300px;}" + NL + "</style>" + NL + "<p>";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = " <br />";
  protected final String TEXT_4 = NL + "</p>";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Document document = (Document) argument; 
    stringBuffer.append(TEXT_1);
     if (document.getDocument() != null) { 
    stringBuffer.append(TEXT_2);
    stringBuffer.append( document.getDocument().toString() );
    stringBuffer.append(TEXT_3);
     } 
    stringBuffer.append(TEXT_4);
    return stringBuffer.toString();
  }
}
