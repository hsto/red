package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.specificationelements.requirement.*;
import dk.dtu.imm.red.specificationelements.acceptancetest.*;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.CoreException;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileOutputStream;

public class RequirementTemplateHTML {

  protected static String nl;
  public static synchronized RequirementTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    RequirementTemplateHTML result = new RequirementTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<h4> Requirement - ";
  protected final String TEXT_2 = " </h4>" + NL + "<style type=\"text/css\">" + NL + "p{max-width:700px;}" + NL + "</style>" + NL + "<p>" + NL + "ID: ";
  protected final String TEXT_3 = ", Type: ";
  protected final String TEXT_4 = ", Level: ";
  protected final String TEXT_5 = " <br />";
  protected final String TEXT_6 = NL + "Description: ";
  protected final String TEXT_7 = " <br />";
  protected final String TEXT_8 = NL;
  protected final String TEXT_9 = NL + "Elaboration: ";
  protected final String TEXT_10 = " <br />";
  protected final String TEXT_11 = NL;
  protected final String TEXT_12 = NL + "Remarks: ";
  protected final String TEXT_13 = " <br />";
  protected final String TEXT_14 = NL;
  protected final String TEXT_15 = NL + "<p><i>Acceptance tests for ";
  protected final String TEXT_16 = "</i>" + NL + "<table border=\"1\">" + NL + "\t<tr>" + NL + "\t\t<td>Case ID</td>" + NL + "\t\t<td>Precondition</td>" + NL + "\t\t<td>Action</td>" + NL + "\t\t<td>Postcondition</td>" + NL + "\t</tr>";
  protected final String TEXT_17 = NL + "\t<tr>" + NL + "\t\t<td>";
  protected final String TEXT_18 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_19 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_20 = "</td>" + NL + "\t\t<td>";
  protected final String TEXT_21 = "</td>" + NL + "\t</tr>";
  protected final String TEXT_22 = NL + "</table>" + NL + "</p>";
  protected final String TEXT_23 = NL;
  protected final String TEXT_24 = NL + "<p><i>Requirement model:</i>" + NL + "<p>";
  protected final String TEXT_25 = NL + "\t\t\t\t<img src=\"data:image/png;base64,";
  protected final String TEXT_26 = "\" />";
  protected final String TEXT_27 = NL + "\t\t\t\t<img src=\"";
  protected final String TEXT_28 = "\" /> <br />";
  protected final String TEXT_29 = NL + "</p>" + NL + "</p>" + NL + "" + NL + "" + NL + "</p>";

	public String generate(Object argument, boolean inlineImages, String path)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Requirement requirement = (Requirement) argument;
    stringBuffer.append(TEXT_1);
    stringBuffer.append( requirement.getId() );
    stringBuffer.append(TEXT_2);
    stringBuffer.append(requirement.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(requirement.getType());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(requirement.getAbstractionLevel());
    stringBuffer.append(TEXT_5);

	if (requirement.getDescription() != null && !requirement.getDescription().toString().isEmpty()) {

    stringBuffer.append(TEXT_6);
    stringBuffer.append(requirement.getDescription().toString());
    stringBuffer.append(TEXT_7);

	}

    stringBuffer.append(TEXT_8);

	if (requirement.getDetails() != null && !requirement.getDetails().toString().isEmpty()) {

    stringBuffer.append(TEXT_9);
    stringBuffer.append(requirement.getDetails().toString());
    stringBuffer.append(TEXT_10);

	}

    stringBuffer.append(TEXT_11);

	if (requirement.getRemarks() != null && !requirement.getRemarks().toString().isEmpty()) {

    stringBuffer.append(TEXT_12);
    stringBuffer.append(requirement.getRemarks().toString());
    stringBuffer.append(TEXT_13);

	}

    stringBuffer.append(TEXT_14);

	if (requirement.getAcceptanceTests().size() > 0) {

    stringBuffer.append(TEXT_15);
    stringBuffer.append(requirement.getId());
    stringBuffer.append(TEXT_16);

	for (AcceptanceTest test : requirement.getAcceptanceTests()) {

    stringBuffer.append(TEXT_17);
    stringBuffer.append( test.getCaseID() );
    stringBuffer.append(TEXT_18);
    stringBuffer.append( test.getPrecondition() );
    stringBuffer.append(TEXT_19);
    stringBuffer.append( test.getAction() );
    stringBuffer.append(TEXT_20);
    stringBuffer.append( test.getPostcondition() );
    stringBuffer.append(TEXT_21);

	}

    stringBuffer.append(TEXT_22);

}

    stringBuffer.append(TEXT_23);

	if (requirement.getModelFragment() != null
		&& requirement.getModelFragment().getFragmentDiagram() != null) {

    stringBuffer.append(TEXT_24);

		Diagram diagram = requirement.getModelFragment().getFragmentDiagram();
		CopyToImageUtil imageUtil = new CopyToImageUtil();
		try {
			byte[] fragmentBytes =
				imageUtil.copyToImageByteArray(diagram, 800, 600,
					ImageFileFormat.PNG, new NullProgressMonitor(), null, true);

			String fragmentOut = DatatypeConverter.printBase64Binary(fragmentBytes);


			if (inlineImages) {

    stringBuffer.append(TEXT_25);
    stringBuffer.append( fragmentOut );
    stringBuffer.append(TEXT_26);

			} else {
				try {
					FileOutputStream fos = new FileOutputStream(path + File.separatorChar + requirement.getUniqueID() + ".png");
					fos.write(fragmentBytes);
					fos.close();
				} catch (Exception e) {
					e.printStackTrace(); LogUtil.logError(e);
				}

    stringBuffer.append(TEXT_27);
    stringBuffer.append(requirement.getUniqueID() + ".png" );
    stringBuffer.append(TEXT_28);

			}


		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

    stringBuffer.append(TEXT_29);
    return stringBuffer.toString();
  }
}