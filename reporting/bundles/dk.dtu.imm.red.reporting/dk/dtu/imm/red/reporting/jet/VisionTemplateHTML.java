package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.specificationelements.vision.*;

public class VisionTemplateHTML
{
  protected static String nl;
  public static synchronized VisionTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    VisionTemplateHTML result = new VisionTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<h4> ";
  protected final String TEXT_2 = " </h4>" + NL + "<style type=\"text/css\">" + NL + "p{max-width:300px;}" + NL + "</style>" + NL + "<p>";
  protected final String TEXT_3 = NL;
  protected final String TEXT_4 = " <br />";
  protected final String TEXT_5 = NL;
  protected final String TEXT_6 = " <br />" + NL + "</p>";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Vision vision = (Vision) argument; 
    stringBuffer.append(TEXT_1);
    stringBuffer.append( vision.getName() );
    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    stringBuffer.append( vision.getName() );
    stringBuffer.append(TEXT_4);
    stringBuffer.append(TEXT_5);
    stringBuffer.append( vision.getVision() );
    stringBuffer.append(TEXT_6);
    return stringBuffer.toString();
  }
}
