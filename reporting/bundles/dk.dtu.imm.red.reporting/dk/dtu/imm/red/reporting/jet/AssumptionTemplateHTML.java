package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.specificationelements.assumption.*;

public class AssumptionTemplateHTML
{
  protected static String nl;
  public static synchronized AssumptionTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    AssumptionTemplateHTML result = new AssumptionTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<h4> ";
  protected final String TEXT_2 = " </h4>" + NL + "<style type=\"text/css\">" + NL + "p{max-width:300px;}" + NL + "</style>" + NL + "<p>";
  protected final String TEXT_3 = NL;
  protected final String TEXT_4 = " <br />";
  protected final String TEXT_5 = NL;
  protected final String TEXT_6 = " <br />" + NL + "</p>";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Assumption assumption = (Assumption) argument; 
    stringBuffer.append(TEXT_1);
    stringBuffer.append( assumption.getName() );
    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    stringBuffer.append( assumption.getAssumptionName() );
    stringBuffer.append(TEXT_4);
    stringBuffer.append(TEXT_5);
    stringBuffer.append( assumption.getAssumptionSentence() );
    stringBuffer.append(TEXT_6);
    return stringBuffer.toString();
  }
}
