package dk.dtu.imm.red.reporting.jet;


import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.reporting.ReportHelper;

public class DocumentSummaryTemplateHTML {
	protected static String nl;

	public static synchronized DocumentSummaryTemplateHTML create(String lineSeparator) {
		nl = lineSeparator;
		DocumentSummaryTemplateHTML result = new DocumentSummaryTemplateHTML();
		nl = null;
		return result;
	}

	public final String NL = nl == null ? (System.getProperties()
			.getProperty("line.separator")) : nl;

	public String generate(Object argument,int position, int size) {
//	public String generate(Object argument, boolean inlineImages, String path) {
		//System.out.println("Here");
		final StringBuffer sb = new StringBuffer();
		Document document = (Document) argument;
		sb.append(ReportHelper.insBasicInfo(document));
		String content = "-";
		if(document.getTextContent()!=null)
		{
			content = document.getTextContent().toString();
		}
		if(position==0){
			
			sb.append("<div class=\"wrap\">");
			sb.append("<table border=\"1\">");
			
			sb.append(ReportHelper.insTableRow(true,
						"Document",
						"Abstract",
						"Internal Reference",
						"External File Reference",
						"Web Reference",
						"Content"
			));
		}

		
		String docAbstract="";
		if(document.getAbstract()!=null && !document.getAbstract().toString().isEmpty()){
			docAbstract = document.getAbstract().toString();
		}

		String internalRef ="";
		if(document.getInternalRef()!=null && !document.getInternalRef().getName().isEmpty()){
			internalRef = document.getInternalRef().getName();
		}
		
		sb.append(ReportHelper.insTableRow(false, 
					document.getName(),
					docAbstract,
					internalRef,
					document.getExternalFileRef(),
					document.getWebRef(),
					content
		));
		
		if(position==size-1){
			sb.append("</table>"+NL+"</div>"+NL);
		}
		
		return sb.toString();
		
	}
}
