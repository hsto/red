package dk.dtu.imm.red.reporting.wizards;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.wizards.BaseWizard;
import dk.dtu.imm.red.core.ui.wizards.ChooseElementLocationPage;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.reporting.FolderStructureReport;
import dk.dtu.imm.red.reporting.IReport;
import dk.dtu.imm.red.reporting.SortedStructureReport;
import dk.dtu.imm.red.reporting.TableStructureReport;

public class SimpleReportWizardImpl extends BaseWizard implements SimpleReportWizard{

	protected ChooseFolderPage chooseFolderPage;
	protected DefineFrontPagePage defineFrontPagePage;
	protected ChooseElementLocationPage chooseElementLocationPage;

	protected SimpleReportWizardPresenter presenter;
	protected boolean isProjectSelected;

	@Override
	public void addPages() {
//		addPage(defineFrontPagePage);
		addPage(chooseFolderPage);
		addPage(chooseElementLocationPage);
		super.addPages();
	}

	@Override
	public boolean performFinish() {
		final String fileName = chooseElementLocationPage.getFileName();
		final String path = fileName.substring(0, fileName.lastIndexOf(File.separatorChar));

		IStructuredSelection selection =
				(IStructuredSelection) chooseFolderPage.getSelection();

		if (selection.getFirstElement() instanceof Group == false ) {
			return false;
		}

		final Group parent = (Group) selection.getFirstElement();

		final Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		if(parent instanceof Project){
			EList<dk.dtu.imm.red.core.file.File> files = ((Project) parent).getListofopenedfilesinproject();
			if(files.get(0)!=null){
				isProjectSelected = true;
			}
		}

		ProgressMonitorDialog dialog =
				new ProgressMonitorDialog(Display.getDefault().getActiveShell());
		try {
			dialog.run(false, false, new IRunnableWithProgress() {
				@Override
				public void run(IProgressMonitor monitor)
						throws InvocationTargetException, InterruptedException {
					try {
						monitor.beginTask("Generating report", IProgressMonitor.UNKNOWN);
						IReport report = null;
						FileWriter output = new FileWriter(fileName);
						BufferedWriter writer = new BufferedWriter(output);

						//Select the Structure of the report
						if (chooseFolderPage.useFolderStructure()) {
							report = new FolderStructureReport();
						} else if(chooseFolderPage.useTableStructure()) {
							report = new TableStructureReport();
						}
						else {
							report = new SortedStructureReport();
						}

						String reportString = "";
						if(isProjectSelected)
							reportString = report.generateReport(
									workspace, chooseFolderPage.useInternalImages(),  path);
						else
							reportString = report.generateReport(
								parent, chooseFolderPage.useInternalImages(),  path);
						reportString = reportString.replaceAll("<A href=\"[a-z0-9\\-]*\">", "");
						writer.write(reportString);
						writer.close();

					} catch (IOException e) {
						e.printStackTrace(); LogUtil.logError(e);
					} finally {
						monitor.done();
					}
				}
			});
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

		return true;
	}

	public SimpleReportWizardImpl(IStructuredSelection selection) {
		presenter = new SimpleReportWizardPresenterImpl(this);
		defineFrontPagePage = new DefineFrontPagePage(this);
		chooseFolderPage = new ChooseFolderPage();
		chooseElementLocationPage = new ChooseElementLocationPage("Report", "html");
	}

	@Override
	public boolean canFinish() {
		return defineFrontPagePage.isPageComplete() &&
				chooseFolderPage.isPageComplete() &&
				chooseElementLocationPage.isPageComplete();
	}


}
