package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;

public class StakeholderSummaryTemplateHTML {

	String NL = ReportHelper.NL;
	public static synchronized StakeholderSummaryTemplateHTML create() {
		StakeholderSummaryTemplateHTML result = new StakeholderSummaryTemplateHTML();
		return result;
	}


	public String generate(Object argument,int position, int size) {
		final StringBuffer sb = new StringBuffer();
		Stakeholder stakeholder = (Stakeholder) argument;
		sb.append(ReportHelper.insBasicInfo(stakeholder));
		
		if(position==0){
			sb.append("<div class=\"wrap\">");
			sb.append("<table border=\"1\">");
			sb.append(ReportHelper.insTableRow(true,"Stakeholder","Kind","Exposure","Power","Urgency","Importance"));
		}
		
		sb.append(ReportHelper.insTableRow(false,
				stakeholder.getName(),
				stakeholder.getElementKind(),
				convertToStar(stakeholder.getExposure()),
				convertToStar(stakeholder.getPower()),
				convertToStar(stakeholder.getUrgency()),
				convertToStar(stakeholder.getImportance())
				));

		if(position==size-1){
			sb.append("</table>"+NL+"</div>"+NL);
		}
		String discussion="No data available",
				stake="No data available",
				engagement="No data available";
		if(stakeholder.getLongDescription()!=null)
		{
			discussion= String.valueOf(stakeholder.getLongDescription());
		}
		if(stakeholder.getStake()!=null)
		{
			stake= String.valueOf(stakeholder.getStake());
		}
		if(stakeholder.getEngagement()!=null)
		{
			engagement= String.valueOf(stakeholder.getEngagement());
		}
		sb.append(ReportHelper.insTag("p", "<u>Discussion</u>:\n"+String.valueOf(stakeholder.getLongDescription()), false));
		sb.append(ReportHelper.insTag("p", "<u>Stake</u>:\n"+String.valueOf(stakeholder.getStake()), false));
		sb.append(ReportHelper.insTag("p", "<u>Engagement</u>:\n"+String.valueOf(stakeholder.getEngagement()), false));
		return sb.toString();
	}
	
	private String convertToStar(int numOfStar){
		StringBuffer star=new StringBuffer("");
		for(int i=0;i<numOfStar;i++){
			star.append("*");
		}
		return star.toString();
		
	}
}
