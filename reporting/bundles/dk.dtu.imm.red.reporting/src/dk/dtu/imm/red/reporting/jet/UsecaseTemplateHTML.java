package dk.dtu.imm.red.reporting.jet;

import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.usecase.*;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;

public class UsecaseTemplateHTML {
	protected static String NL=ReportHelper.NL;

	public static synchronized UsecaseTemplateHTML create(String lineSeparator) {
		UsecaseTemplateHTML result = new UsecaseTemplateHTML();
		return result;
	}


	StringBuffer tbl = new StringBuffer();

	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		UseCase usecase = (UseCase) argument;
		sb.append(ReportHelper.insBasicInfo(usecase));
		String tableString = "";
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("ID",usecase.getName()));
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Description",usecase.getDescription()));
		EList<Actor> primaryActors = usecase.getPrimaryActors();
		EList<Actor> secondaryActors = usecase.getSecondaryActors();
		String actors="";
		for(int i=0;i<primaryActors.size();i++)
		{
			actors+=primaryActors.get(i).getName()+"\n";
		}
		for(int i=0;i<secondaryActors.size();i++)
		{
			if(i==0)
			{
				actors+="Secondary Actors"+"\n";
			}
			actors+=secondaryActors.get(i).getName()+"\n";
		}
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Actors",actors));
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Trigger",usecase.getTrigger()));
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Parameter",usecase.getParameter()));
		EList<String> preconditions = usecase.getPreConditions();
		String preconditionString = ""; 
		for(int i=0;i<preconditions.size();i++)
		{
			preconditionString+= preconditions.get(i)+"\n";
		}
		
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Precondition",preconditionString));
		EList<UseCaseScenario> regularScenario= usecase.getPrimaryScenarios();
		EList<UseCaseScenario> variants= usecase.getSecondaryScenarios();
		String regularScenarioString="",variantsString="";
		for(int i=0;i<regularScenario.size();i++)
		{
			regularScenarioString+=regularScenario.get(i).getName()+"\n";
		}
		for(int i=0;i<variants.size();i++)
		{
			variantsString+=variants.get(i).getName()+"\n";
		}
		regularScenarioString = "<b>RegularScenario</b>\n"+regularScenarioString;
		variantsString = "<b>Variants</b>\n"+variantsString;
		tableString+=(ReportHelper.insSideWaysTwoColumnRow(regularScenarioString,variantsString));
		EList<String> PostConditionList=usecase.getPostConditions();
		String postConditions="";
		for(int i=0;i<PostConditionList.size();i++)
		{
			postConditions+=PostConditionList.get(i)+"\n";
		}
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Postcondition",postConditions));
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Result",
				ReportHelper.returnDashIfNull(usecase.getResult())));
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Outcome",
				ReportHelper.returnDashIfNull(usecase.getOutcome())));
		if(usecase.getIncidence()!=null)
		{
			tableString+=(ReportHelper.insSideWaysTwoColumnRow("Incidence",
					ReportHelper.returnDashIfNull(usecase.getIncidence().getValue())));
		} else {
			tableString+=(ReportHelper.insSideWaysTwoColumnRow("Incidence",
					ReportHelper.returnDashIfNull(usecase.getIncidence())));
		}
		if(usecase.getDuration()!=null)
		{
			tableString+=(ReportHelper.insSideWaysTwoColumnRow("Duration",
					ReportHelper.returnDashIfNull(usecase.getDuration().getValue())));
		} else {
			tableString+=(ReportHelper.insSideWaysTwoColumnRow("Duration",
					ReportHelper.returnDashIfNull(usecase.getDuration())));
		}
		//tableString+=(ReportHelper.insSideWaysTwoColumnRow("Duration",usecase.getDuration().toString()));
		
		EList<ElementRelationship> relations = usecase.getRelatesTo();
		String relationString="";
		for(int i=0;i<relations.size();i++)
		{
			relationString+=relations.get(i).getToElement().getName()+"\n";
		}
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Reference",relationString));
		tableString+=(ReportHelper.insSideWaysTwoColumnRow("Remarks",
				ReportHelper.returnDashIfNull(usecase.getLongDescription())));
		sb.append(ReportHelper.insTable("", tableString));
		
		/*
		sb.append(ReportHelper.insBasicInfo(usecase));
		sb.append(ReportHelper.insTag("p", usecase.getLongDescription(), false));
		
		if (usecase.getPreConditions().size() > 0) {
			tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "Pre-Condition"));
			for (String condition : usecase.getPreConditions()) {
				tbl.append(ReportHelper.insTableRow(false, condition));
			}
			sb.append(ReportHelper.insTable("Before", tbl.toString()));

		}

		sb.append(ReportHelper.insTag("p", "Trigger:"+usecase.getTrigger(), false));
		sb.append(ReportHelper.insTag("p", "Parameter:"+usecase.getParameter(), false));

		if (usecase.getPostConditions().size() > 0) {
			tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "Post-Condition"));
			for (String condition : usecase.getPostConditions()) {
				tbl.append(ReportHelper.insTableRow(false, condition));
			}
			sb.append(ReportHelper.insTable("After", tbl.toString()));
		}
		sb.append(ReportHelper.insTag("p", "Result:"+usecase.getResult(), true));
		
		StringBuffer tbl2 = new StringBuffer();
		tbl2.append(ReportHelper.insTableRow(true, "Property","Value","Unit"));
		tbl2.append(ReportHelper.insTableRow(false, "Incidence",
				String.valueOf(usecase.getIncidence().getValue()),
				usecase.getIncidence().getDurationUnit().toString()));
		tbl2.append(ReportHelper.insTableRow(false, "Duration",
				String.valueOf(usecase.getDuration().getValue()),
				usecase.getDuration().getDurationUnit().toString()));		
		tbl2.append(ReportHelper.insTableRow(false, "Benefit",
				String.valueOf(usecase.getBenefit().getValue()),
				usecase.getBenefit().getDurationUnit().toString()));			
		tbl2.append(ReportHelper.insTableRow(false, "Cost",
				String.valueOf(usecase.getCost().getValue()),
				usecase.getCost().getDurationUnit().toString()));			
		sb.append(ReportHelper.insTable("Properties", tbl2.toString()));
		

		if(usecase.getInputOutputList().size()>0){
			tbl2 = new StringBuffer();
			tbl2.append(ReportHelper.insTableRow(true, "Input","Output"));
			for(InputOutput io : usecase.getInputOutputList()){
				tbl2.append(ReportHelper.insTableRow(false, io.getInput(),io.getOutput()));
			}
			sb.append(ReportHelper.insTable("Input Output", tbl2.toString()));
		}

		
		if (usecase.getPrimaryReferences().getActors().size() > 0) {

			tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "Label","Name"));
			for (Actor actor : usecase.getPrimaryReferences().getActors()) {
				tbl.append(ReportHelper.insTableRow(false, actor.getLabel(),actor.getName()));
			}
			sb.append(ReportHelper.insTable("Primary Actors", tbl.toString()));

		}

		if (usecase.getSecondaryReferences().getActors().size() > 0) {

			tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "Label","Name"));
			for (Actor actor : usecase.getSecondaryReferences().getActors()) {
				tbl.append(ReportHelper.insTableRow(false, actor.getLabel(),actor.getName()));
			}
			sb.append(ReportHelper.insTable("Secondary Actors", tbl.toString()));
		}

		if (usecase.getPrimaryReferences().getScenarios().size() > 0) {

			tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "Label","Name"));
			for (Scenario scenario : usecase.getPrimaryReferences()
					.getScenarios()) {
				tbl.append(ReportHelper.insTableRow(false, scenario.getLabel(),scenario.getName()));
			}
			sb.append(ReportHelper.insTable("Primary Scenarios", tbl.toString()));

		}

		if (usecase.getSecondaryReferences().getScenarios().size() > 0) {

			tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "Label","Name"));
			for (Scenario scenario : usecase.getSecondaryReferences()
					.getScenarios()) {
				tbl.append(ReportHelper.insTableRow(false, scenario.getLabel(),scenario.getName()));
			}
			sb.append(ReportHelper.insTable("Secondary Scenarios", tbl.toString()));

		}*/

		return sb.toString();
	}
}
