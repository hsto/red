package dk.dtu.imm.red.reporting.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface SimpleReportWizard extends IBaseWizard{
	
	String ID = "dk.dtu.imm.red.reporting.simplereportwizard";

}
