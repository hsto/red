package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.assumption.*;

public class AssumptionTemplateHTML {
	protected static String NL = ReportHelper.NL;

	public static synchronized AssumptionTemplateHTML create(
			String lineSeparator) {
		AssumptionTemplateHTML result = new AssumptionTemplateHTML();
		return result;
	}


	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		Assumption assumption = (Assumption) argument;
		sb.append(ReportHelper.insBasicInfo(assumption));

		return sb.toString();
	}
}
