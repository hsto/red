package dk.dtu.imm.red.reporting.jet;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.bind.DatatypeConverter;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPanel;

public class PersonaTemplateHTML {

	protected static String NL=ReportHelper.NL;

	public static synchronized PersonaTemplateHTML create(String lineSeparator) {
		PersonaTemplateHTML result = new PersonaTemplateHTML();
		return result;
	}

	public String generate(Object argument, boolean inlineImages, String path) {
		final StringBuffer sb = new StringBuffer();
		Persona persona = (Persona) argument;
		sb.append(ReportHelper.insBasicInfo(persona));
		sb.append(ReportHelper.insTag("p", "age:"+ String.valueOf(persona.getAge()),false));
		sb.append(ReportHelper.insTag("p", "job:"+ String.valueOf(persona.getJob()),false));

		if (persona.getPictureData() != null) {
			if (inlineImages) {

				sb.append(ReportHelper.insInlineImage(persona.getPictureData()));

			} else {
				sb.append(ReportHelper.insFileImage(path, persona.getUniqueID()+".png", persona.getPictureData()));
			}
		}

		if (persona.getIssues() != null
				&& !persona.getIssues().toString().isEmpty()) {

			sb.append(ReportHelper.insTag("p", "<u>Issues:\n</u>"+persona.getIssues().toPlainString(), true));

		}

		if (persona.getCapabilities() != null
				&& !persona.getCapabilities().toString().isEmpty()) {
			sb.append(ReportHelper.insTag("p", "<u>Capabilities:\n</u>"+persona.getCapabilities().toPlainString(), true));

		}
		
		if (persona.getConstraints() != null
				&& !persona.getConstraints().toString().isEmpty()) {
			sb.append(ReportHelper.insTag("p", "<u>Constraints:\n</u>"+persona.getConstraints().toPlainString(), true));

		}
		
		if (persona.getNarrative() != null
				&& !persona.getNarrative().toString().isEmpty()) {
			sb.append(ReportHelper.insTag("p", "<u>Narrative:\n</u>"+persona.getNarrative().toPlainString(), true));

		}		

		if (persona.getStoryboards().size() > 0) {
			Storyboard storyboard = persona.getStoryboards().get(0);
			String[] imageUrls = new String[storyboard.getPanels().size()];
			for (int i = 0; i < storyboard.getPanels().size(); i++) {
				StoryboardPanel panel = storyboard.getPanels().get(i);
				if (panel.getImageData() != null) {
					if (inlineImages) {
						imageUrls[i] = "<img align=\"center\" src=\"data:image/png;base64,"
								+ DatatypeConverter.printBase64Binary(panel
										.getImageData())
								+ "\"/><br />"
								+ panel.getCaption();
					} else {
						try {
							FileOutputStream fos = new FileOutputStream(path
									+ File.separatorChar
									+ persona.getUniqueID() + "_" + i + ".png");
							fos.write(panel.getImageData());
							fos.close();
							imageUrls[i] = "<img align=\"center\" src=\""
									+ persona.getUniqueID() + "_" + i
									+ ".png\"/><br />" + panel.getCaption();
						} catch (Exception e) {
							e.printStackTrace(); LogUtil.logError(e);
						}
					}
				}
			}

			sb.append(NL + "\tStoryboard: <br />" + NL
					+ "\t<table width=\"100%\" border=\"1\">" + NL + "\t\t<tr>" + NL
					+ "\t\t\t<td width=\"66%\" colspan=\"2\">");
			sb
					.append(storyboard.getIntroduction() != null ? storyboard
							.getIntroduction().toString() : "");
			sb.append("</td>" + NL + "\t\t\t<td width=\"34%\">");
			sb.append(imageUrls[0]);
			sb.append("</td>" + NL + "\t\t</tr>" + NL
					+ "\t\t<tr>" + NL + "\t\t\t<td>");
			sb.append(imageUrls[1]);
			sb.append("</td>" + NL + "\t\t\t<td>");
			sb.append(imageUrls[2]);
			sb.append("</td>" + NL + "\t\t\t<td>");
			sb.append(imageUrls[3]);
			sb.append("</td>" + NL + "\t\t</tr>" + NL
					+ "\t\t<tr>" + NL + "\t\t\t<td width=\"34%\">");
			sb.append(imageUrls[4]);
			sb.append("</td>" + NL
					+ "\t\t\t<td width=\"66%\" colspan=\"2\">");
			sb.append(storyboard.getConclusion() != null ? storyboard
					.getConclusion().toString() : "");
			sb.append("</td>" + NL + "\t\t</tr>\t\t\t" + NL
					+ "\t</table>");

		}

		sb.append(NL + "</p>");
		sb.append(NL);
		return sb.toString();
	}
}