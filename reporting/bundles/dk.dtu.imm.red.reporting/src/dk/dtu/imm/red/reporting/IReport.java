package dk.dtu.imm.red.reporting;

import dk.dtu.imm.red.core.element.Element;

public interface IReport {
	
	String generateReport(Element element, boolean inlineImages, String path);

}
