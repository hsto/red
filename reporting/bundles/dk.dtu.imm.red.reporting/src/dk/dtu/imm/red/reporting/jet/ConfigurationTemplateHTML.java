package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.port.Port;

public class ConfigurationTemplateHTML {
	protected static String NL = ReportHelper.NL;

	public static synchronized ConfigurationTemplateHTML create(
			String lineSeparator) {
		ConfigurationTemplateHTML result = new ConfigurationTemplateHTML();
		return result;
	}


	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		Configuration systemStructure = (Configuration) argument;
		sb.append(ReportHelper.insBasicInfo(systemStructure));

		if (systemStructure.getParts().size() > 0) {
			StringBuffer tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "Label","Name"));
			for (Actor actor : systemStructure.getParts()) {		
				tbl.append(ReportHelper.insTableRow(false, actor.getLabel(),actor.getName()));
			}
			sb.append(ReportHelper.insTable("Parts", tbl.toString()));

		}

		if (systemStructure.getInternalConnectors().size() > 0) {

			StringBuffer tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "Label","Name"));
			for (Connector connector : systemStructure.getInternalConnectors()) {
				tbl.append(ReportHelper.insTableRow(false, connector.getLabel(),connector.getName()));
			}
			sb.append(ReportHelper.insTable("Connectors", tbl.toString()));

		}

		return sb.toString();
	}
}
