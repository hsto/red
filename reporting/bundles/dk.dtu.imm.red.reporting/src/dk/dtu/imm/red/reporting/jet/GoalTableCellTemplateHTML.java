package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.goal.Goal;

public class GoalTableCellTemplateHTML {
	protected static String nl;

	public static synchronized GoalTableCellTemplateHTML create(String lineSeparator) {
		nl = lineSeparator;
		GoalTableCellTemplateHTML result = new GoalTableCellTemplateHTML();
		nl = null;
		return result;
	}

	public final String NL = nl == null ? (System.getProperties()
			.getProperty("line.separator")) : nl;

	public String generate(Object argument,int position,int size) {
		final StringBuffer sb = new StringBuffer();
		Goal goal = (Goal) argument;
		if(position==0){
			sb.append("<div class=\"wrap\">");
			sb.append("<table border=\"1\">");
			sb.append(ReportHelper.insTableRow(true,
					"Goal","Kind","Name","Supports","Description"
					));		
		}
		
//		String partOf = "";
//		if(goal.getPartOf()!=null && goal.getPartOf().getLabel()!=null){
//			partOf = goal.getPartOf().getLabel();
//		}
		sb.append(ReportHelper.insTableRow(false,
				goal.getLabel(),
				goal.getElementKind(),
				goal.getName()
//				partOf,
				));	
		if(position==size-1){
			sb.append("</table>"+NL+"</div><br/>"+NL);
		}
		
		return sb.toString();
	}
}
