package dk.dtu.imm.red.reporting.wizards;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;
import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public class ChooseFolderPage extends WizardPage
implements IWizardPage{

	protected Label selectFolderLabel;
	protected ElementTreeViewWidget treeView;
	protected ISelection selection;
	protected Button internalImagesButton;

	protected boolean folderChosen;
	protected boolean internalImages;
	private boolean exportAsTables;
	protected Button folderStructureButton;
	protected Button simpleStructureButton;

	protected boolean useFolderStructure;
	private Button exportElementAsTables;


	public ChooseFolderPage() {
		super("Simple Report");
		setTitle("Choose a folder");
		setDescription("Select an internal folder"
				+ " from where this report"
				+ " should be generated from.\n"
				+ "All elements within the folder will become"
				+ " a part of the generated report");
		folderChosen = false;
		internalImages = false;
		useFolderStructure = true;
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		composite.setLayout(layout);

		GridData descriptionLayoutData = new GridData();
		descriptionLayoutData.horizontalSpan = 3;


		selectFolderLabel = new Label(composite, SWT.NONE);
		selectFolderLabel.setLayoutData(descriptionLayoutData);
		selectFolderLabel
		.setText("Select a folder");

		GridData treeViewerLayoutData = new GridData(SWT.FILL, SWT.FILL, true,
				true);
		treeViewerLayoutData.horizontalSpan = 3;

		Composite treeContainer = new Composite(composite, SWT.NONE);

		GridLayout treeContainerLayout = new GridLayout();
		treeContainerLayout.numColumns = 1;

		treeContainer.setLayout(treeContainerLayout);
		treeContainer.setLayoutData(treeViewerLayoutData);

		setPageComplete(false);

		selection =
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();

		treeView = new ElementTreeViewWidget(true, treeContainer, SWT.BORDER, true,true);
		if (selection != null && !selection.isEmpty()) {
			treeView.setSelection(selection);
			setPageComplete(true);
		}

		treeView.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				if(treeView.getSelection().isEmpty()){
					setPageComplete(false);
				}else{
					setPageComplete(true);
				}
				getContainer().updateButtons();
			}
		});

		folderStructureButton = new Button(composite, SWT.RADIO);
		folderStructureButton.setText("Folder-based structure");
		folderStructureButton.setSelection(true);
		folderStructureButton.setToolTipText(
				"This option produces a report in which \n" +
				"the structure is similar to the folder-structure\n" +
				"shown in the element explorer.");
		folderStructureButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (folderStructureButton.getSelection()) {
					useFolderStructure = true;
				} else {
					useFolderStructure = false;
				}
			}
		});

		simpleStructureButton = new Button(composite, SWT.RADIO);
		simpleStructureButton.setText("Simple structure");
		simpleStructureButton.setToolTipText(
				"This option produces a report in which \n" +
				"the elements are organized by type \n" +
				"(requirement, persona, etc.)");


		exportElementAsTables = new Button(composite, SWT.RADIO);
		exportElementAsTables.setText("Simple Structure using tables");
		exportElementAsTables.setToolTipText(
			 "Same as Simple structure but Specification Elements\n will be exported as HTML tables \nusing their default table representation.");
		exportElementAsTables.addListener(SWT.Selection, new Listener() {


			@Override
			public void handleEvent(Event event) {
				exportAsTables = exportElementAsTables.getSelection();
			}
		});

		internalImagesButton = new Button(composite, SWT.CHECK);
		internalImagesButton.setText("Store images internally in exported file?");
		internalImagesButton.setToolTipText(
				"Storing report images internally in the exported file \n" +
				"reduces the risk of breaking the images in the report, \n" +
				"but is incompatible with opening the report in a word processor \n" +
				"such as Microsoft Word or OpenOffice Writer.");
		internalImagesButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (internalImagesButton.getSelection()) {
					boolean ok =
							MessageDialog.openConfirm(null, "Warning",
									"Storing report images internally in the exported file " +
									"reduces the risk of breaking the images in the report, " +
									"but is incompatible with opening the report in a word processor " +
									"such as Microsoft Word or OpenOffice Writer.");
					if (!ok) {
						internalImagesButton.setSelection(false);
						return;
					}
					internalImages = true;
				} else {
					internalImages = false;
				}

			}
		});

		setControl(composite);

	}

	@Override
	public IWizardPage getNextPage() {
		if (isPageComplete() && isCurrentPage()
				&& getWizard() instanceof IBaseWizard) {

			((IBaseWizard) getWizard()).pageOpenedOrClosed(this);
		}
		return super.getNextPage();
	}

	public ISelection getSelection() {
		return treeView.getSelection();
	}

	public boolean useInternalImages() {
		return internalImages;
	}

	public boolean useFolderStructure() {
		return useFolderStructure;
	}

	public boolean useTableStructure() {
		return exportAsTables;
	}


}
