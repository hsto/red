package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;

public class DiagramTemplateHTML {
	protected static String nl = ReportHelper.NL;
	
	public static synchronized DiagramTemplateHTML create(String lineSeparator)
	{
		DiagramTemplateHTML result = new DiagramTemplateHTML();
		return result;
	}
	
	public String generate(String diagramPath,Object argument)
	{
		final StringBuffer sb = new StringBuffer();
		Diagram d = (Diagram) argument;
		sb.append(ReportHelper.insBasicInfo(d));
		//sb.append("\t\t<img src=\""+diagramPath+"\" />");
		//System.out.println(diagramPath);
		
		sb.append("\t\t<img src=\""+d.getName()+".jpeg"+"\" />");
		return sb.toString();
	}
	
}
