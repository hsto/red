package dk.dtu.imm.red.reporting;

import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.ui.IWorkbenchPart;

import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;

public class DiagramHelper extends WorkbenchPartAction {

//	private VisualEditor editor;
	
	public DiagramHelper(IWorkbenchPart part) {
		super(part);
		// TODO Auto-generated constructor stub
	}
	
	private void performExport(Diagram child,String filePath, int imageFormat)
	{
		String saveLocation = "D:\\SJ\\Red testing project\\12-11-2015\\Test.jpeg";
		int format = SWT.IMAGE_JPEG;
		Diagram d = (Diagram) child;
		VisualDiagram v =d.getVisualDiagram();
		Image img3 =v.getDiagramElement().getIcon();
		
		GC imageGC = new GC(img3);
		ImageData[] imgData = new ImageData[1];
		imgData[0] = img3.getImageData();
		ImageLoader imgLoader = new ImageLoader();
		imgLoader.data = imgData;
		imgLoader.save(saveLocation, format);
		
		imageGC.dispose();
		img3.dispose();
		/*Image img2 = new Image(null, v.getBounds().width, v.getBounds().height);
		System.out.println(d.getVisualDiagram().getDiagramType());
		VisualModelEditorInputImpl editorInput =
				new VisualModelEditorInputImpl(d);
		GC imageGC2 = new GC(img2);
		imageGC2.setBackground();
		imageGC2.setForeground(figureCanvasGC.getForeground());
		imageGC2.setFont(figureCanvasGC.getFont());
		imageGC2.setLineStyle(figureCanvasGC.getLineStyle());
		imageGC2.setLineWidth(figureCanvasGC.getLineWidth());
		
		
		VisualEditorConfigurationBase base = new VisualEditorConfigurationBase() {
			
		};
		editor = new VisualEditor(base);
		GraphicalViewer graphicalViewer = editor.getGraphicalViewer();
		ScalableFreeformRootEditPart rootEditPart = (ScalableFreeformRootEditPart)graphicalViewer.getEditPartRegistry().get(LayerManager.ID);
		IFigure rootFigure = ((LayerManager)rootEditPart).getLayer(LayerConstants.PRINTABLE_LAYERS);
		Rectangle rootFigureBounds = rootFigure.getBounds();		
		
		Control figureCanvas = graphicalViewer.getControl();				
		GC figureCanvasGC = new GC(figureCanvas);		
		
		Image img = new Image(null, rootFigureBounds.width, rootFigureBounds.height);
		GC imageGC = new GC(img);
		imageGC.setBackground(figureCanvasGC.getBackground());
		imageGC.setForeground(figureCanvasGC.getForeground());
		imageGC.setFont(figureCanvasGC.getFont());
		imageGC.setLineStyle(figureCanvasGC.getLineStyle());
		imageGC.setLineWidth(figureCanvasGC.getLineWidth());
		
		Graphics imgGraphics = new SWTGraphics(imageGC);					
		rootFigure.paint(imgGraphics);
						
		ImageData[] imgData = new ImageData[1];
		imgData[0] = img.getImageData();
		
		ImageLoader imgLoader = new ImageLoader();
		imgLoader.data = imgData;
		imgLoader.save(filePath, imageFormat);
		
		figureCanvasGC.dispose();
		imageGC.dispose();
		img.dispose();*/
	}

	@Override
	protected boolean calculateEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	
}
