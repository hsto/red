package dk.dtu.imm.red.reporting;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.reporting.jet.ElementTableViewTemplateHTML;
import dk.dtu.imm.red.reporting.jet.ProjectTemplateHTML;

public class TableStructureReport extends SortedStructureReport {

	//simple structure using tables
	private int cn; // Chapter Counter
	private StringBuffer sb;
	private ElementTableViewTemplateHTML tg;

	@Override
	public String generateReport(Element element, boolean inlineImages, String path) {
		super.generateReport(element, inlineImages, path);
		sb = new StringBuffer(); 
		
		sb.append("<!DOCTYPE html>");
		sb.append("<html>");
		sb.append("<head>"); 
		sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://cdn.ucb.org.br/Scripts/tablesorter/blue/style.css\" />");
		sb.append("<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-1.11.0.min.js\"></script>");
		sb.append("<script type=\"text/javascript\" src=\"http://cdn.ucb.org.br/Scripts/tablesorter/jquery.tablesorter.min.js\"></script>"); 
		sb.append("</head>"); 
		sb.append("<body>");  
		
		tg = new ElementTableViewTemplateHTML();
		
		cn = 1;
		
		if (project != null) {
			sb.append("<div class=\"Title\">" + project.getName() + "</div>");
			sb.append(new ProjectTemplateHTML().generate(project));
		}

		generateHTML(visions, "Vision");
		generateHTML(assumptions, "Assumptions"); 
		generateHTML(stakeholders, "Stakeholders");
		generateHTML(goals, "Goals");
		generateHTML(personas, "Personas");
		generateHTML(requirements, "Requirements");
		generateHTML(testCases, "Test cases"); 		
		generateHTML(useCases, "Use cases"); 
		generateHTML(scenarios, "Scenarios");
		generateHTML(systemStructures, "Configurations");
		generateHTML(actors, "Actors");
		generateHTML(connectors, "Actor Relations"); 
		
		sb.append("</body>" + NL);
		
		//Init JQuery Table Plugin
		sb.append("<script type=\"text/javascript\">"); 
		sb.append("$('table').tablesorter();"); 
		sb.append("</script>");
		sb.append("</html>" + NL);
		
		return sb.toString();  
	}
	
	private void generateHTML(List<?> list, String chapterName) {
		if(list.size() > 0) { 
			sb.append(tg.generate(list, String.valueOf(cn),  chapterName));  
			cn++;
		} 
	}
}
