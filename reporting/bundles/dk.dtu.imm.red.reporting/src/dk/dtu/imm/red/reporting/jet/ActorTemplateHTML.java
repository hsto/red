package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.actor.Actor;

public class ActorTemplateHTML {
	protected static String NL = ReportHelper.NL;

	public static synchronized ActorTemplateHTML create(String lineSeparator) {
		ActorTemplateHTML result = new ActorTemplateHTML();
		return result;
	}

	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		Actor actor = (Actor) argument;
		sb.append(ReportHelper.insBasicInfo(actor));

		if (actor.getBehaviorElements().size() > 0) {
			StringBuffer tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true,"Label","Name"));
			for (SpecificationElement element : actor.getBehaviorElements()) {
				tbl.append(ReportHelper.insTableRow(false, 
						ReportHelper.returnDashIfNull(element.getLabel()),
						ReportHelper.returnDashIfNull(element.getName())
						));
			}
			sb.append(ReportHelper.insTable("Use Case Relations", tbl.toString()));

		}

		StringBuffer tbl = new StringBuffer();
		tbl.append(ReportHelper.insTableRow(true,"Property","Value","Unit"));
		tbl.append(ReportHelper.insTableRow(false, 
				"Cost",
				String.valueOf(actor.getCost().getValue()),
				actor.getCost().getMonetaryUnit().toString()
				));
		sb.append(ReportHelper.insTable("Properties", tbl.toString()));
		
		return sb.toString();
	}
}
