package dk.dtu.imm.red.reporting.wizards;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class DefineFrontPagePage extends WizardPage implements IWizardPage{

	protected Label test;
	protected SimpleReportWizard wizard;
	
	public DefineFrontPagePage(SimpleReportWizard wizard) {
		super("Define Front Page");

		setTitle("Create Front Page");
		setDescription("Use this wizard to create a simple report");

		this.wizard = wizard;
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);
		
		test = new Label(container, SWT.NONE);
		test.setText("TEST");
		
		setPageComplete(true);
		setControl(container);
	}

}
