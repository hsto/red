package dk.dtu.imm.red.reporting.jet;

import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.CaptionedImage;
import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.reporting.ReportHelper;

public class DocumentTemplateHTML {

	protected static String NL = ReportHelper.NL;

	public static synchronized DocumentTemplateHTML create(String lineSeparator) {
		DocumentTemplateHTML result = new DocumentTemplateHTML();
		return result;
	}


	public String generate(Object argument, boolean inlineImages, String path) {
		final StringBuffer sb = new StringBuffer();
		Document document = (Document) argument;
		sb.append(ReportHelper.insBasicInfo(document));
		if(document.getInternalRef() !=null)
		{
			sb.append(ReportHelper.insTag("p", "Internal Ref:"+
					ReportHelper.returnDashIfNull(document.getInternalRef().getName()), false));
		}else
		{
			sb.append(ReportHelper.insTag("p", "Internal Ref: -", false));
		}
		sb.append(ReportHelper.insTag("p", "External Ref: "+
				ReportHelper.returnDashIfNull(document.getExternalFileRef()), false));
		sb.append(ReportHelper.insTag("p", "Web Ref: "+
				ReportHelper.returnDashIfNull(document.getWebRef()), false));
		sb.append(ReportHelper.insTag("p", "Abstract: \n"+
				ReportHelper.returnDashIfNull(document.getAbstract()), false));
		sb.append(ReportHelper.insTag("p", "Content: \n"+
				ReportHelper.returnDashIfNull(document.getTextContent()), false));
		if(document.getCaptionedImageList() != null)
		{
			EList<CaptionedImage> captionedImageList = document.getCaptionedImageList().getCaptionedImage();
			for(CaptionedImage im:captionedImageList){
				sb.append(ReportHelper.insTag("p", im.getCaption(), false));
				sb.append(ReportHelper.insFileImage(path, im.getUniqueID(), im.getImageData()));
			}
		}
		return sb.toString();
	}
}