package dk.dtu.imm.red.reporting;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.reporting.jet.ActorRelationTemplateHTML;
import dk.dtu.imm.red.reporting.jet.ActorTemplateHTML;
import dk.dtu.imm.red.reporting.jet.AssumptionTemplateHTML;
import dk.dtu.imm.red.reporting.jet.ConfigurationTemplateHTML;
import dk.dtu.imm.red.reporting.jet.DiagramTemplateHTML;
import dk.dtu.imm.red.reporting.jet.DocumentTemplateHTML;
import dk.dtu.imm.red.reporting.jet.GlossaryEntryTemplateHTML;
import dk.dtu.imm.red.reporting.jet.GoalTableCellTemplateHTML;
import dk.dtu.imm.red.reporting.jet.PersonaTemplateHTML;
import dk.dtu.imm.red.reporting.jet.ProjectTemplateHTML;
import dk.dtu.imm.red.reporting.jet.RequirementTemplateHTML;
import dk.dtu.imm.red.reporting.jet.ScenarioTemplateHTML;
import dk.dtu.imm.red.reporting.jet.StakeholderTemplateHTML;
import dk.dtu.imm.red.reporting.jet.TestCaseTemplateHTML;
import dk.dtu.imm.red.reporting.jet.UsecaseTemplateHTML;
import dk.dtu.imm.red.reporting.jet.VisionTemplateHTML;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.testcase.TestCase;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.visualmodeling.ui.editors.DiagramExtension;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;

public class FolderStructureReport implements IReport {

	public final String NL = System.getProperties().getProperty(
			"line.separator");
	private StringBuffer indexString = new StringBuffer("");
	private StringBuffer indexString2 = new StringBuffer("");
	private StringBuffer projectInfoString = new StringBuffer("");
	private StringBuffer elementInfoString = new StringBuffer("");
	private Element oldElement=null;
	private boolean areGlossary=false;
	private boolean areGoal=false;
	//private boolean arePersona=false;
	private int chapter=1;
	private int counter=0;
	@Override
	public String generateReport(Element element, boolean inlineImages, 
			String path) {
//		System.out.println("generateReport");
		indexString.append("<h1> Index </h1>");
		StringBuffer reportString = new StringBuffer("");
		projectInfoString.append(getHTMLHeaderAndCSS());
		projectInfoString.append("<body>");
		/*reportString.append(getHTMLHeaderAndCSS());
		reportString.append("<body>");*/
		if (element instanceof Group) {
			getElementsData((Group) element, 0,inlineImages, path);
			/*reportString.append(visitGroup((Group) element, 0, "1",
					inlineImages, path) + NL);*/
		} else {
			/*Folder tempFolder = FolderFactory.eINSTANCE.createFolder();
			tempFolder.getContents().add(element);
			reportString.append(visitGroup(tempFolder, 0, "1", inlineImages,
					path) + NL);*/
		}
		reportString.append("</body>" + NL);
		reportString.append("</html>" + NL);
		projectInfoString.append(indexString);
		projectInfoString.append(indexString2);
		projectInfoString.append(elementInfoString);
		
		//return reportString.toString();
		return projectInfoString.toString();
	}

	private String getHTMLHeaderAndCSS() {
		StringBuffer cssStyle = new StringBuffer("");
		try {

			Bundle bundle = FrameworkUtil.getBundle(this.getClass());
			URL url = FileLocator.find(bundle,
					new Path("css/report_style.css"), null);
			InputStream inputStream = url.openConnection().getInputStream();
//			FileInputStream inputStream = new FileInputStream("css/report_style.css");			
			BufferedReader in = new BufferedReader(new InputStreamReader(
					inputStream));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				cssStyle.append(inputLine + "\n");
			}

			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

		String DOCTYPE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">"+NL;		
		String HTML_STRING = "<html>" + NL + "<head>" + NL
				+ "<style type=\"text/css\">" + NL;
		String HTML_STRING_2 = "</style>" + NL + "</head>" + NL;
		return DOCTYPE+HTML_STRING + cssStyle + HTML_STRING_2;
	}
/*
	private String visitGroup(Group group, int level, String chapter,
			boolean inlineImages, String path) {
//		System.out.println("visitGroup");
		StringBuffer sb = new StringBuffer();

		if (level > 3)
			level = 3;
		
		if (group instanceof Project) {
			level = 0;
			chapter = "1";
			projectInfoString.append("<div class=\"Title\">" + group.getName() + "</div>");
			projectInfoString.append(new ProjectTemplateHTML().generate(group));
			/*sb.append("<div class=\"Title\">" + group.getName() + "</div>");
			sb.append(new ProjectTemplateHTML().generate(group));*/
		/*} else if (group instanceof File) {
			level = 0;
			chapter = "1";
		} else if (group instanceof Glossary) {
			sb.append("<h1 id=\"glossary"+counter+"\"> Glossary </h1>");
			indexString.append("<h3><a href=\"#glossary"+counter+"\" style=\"text-decoration: none;\"> Chapter " + chapter + " - Glossary</a></h3>");
			counter++;
			sb.append("<div class=\"wrap\">");
			sb.append("<table border=\"1\">");
			sb.append("<tr>");
			sb.append("<th>Term</th>");
			sb.append("<th>Abbreviations</th>");
			sb.append("<th>Synonyms</th>");
			sb.append("<th>Definition / Examples</th>");
			sb.append("</tr>");
		}else {
			sb.append("<h" + level + ">" + chapter + ". " + group.getName()
					+ "</h" + level + ">" + NL);
		}

		ArrayList<Group> childGroups = new ArrayList<Group>();

		boolean allAreGoals=false;
		if (allAreGoals=groupContainsOnlyType(group, Goal.class)) {
			// format all goals as a table instead of individually
			int position = 0;
			sb.append("<h1 id=\"goals"+counter+"\"> Goals </h1>");
			indexString.append("<h3><a href=\"#goals"+counter+"\" style=\"text-decoration: none;\"> Chapter " + chapter + " - Goals</a></h3>");
			counter++;
			for (Element child : group.getContents()) {
				sb.append(new GoalTableCellTemplateHTML().generate(child,
						position++, group.getContents().size()));
			}
		}
		boolean allAreAssumptions=false;
		if (allAreAssumptions=groupContainsOnlyType(group, Assumption.class)) {
			// format all goals as a table instead of individually
			int position = 0;
			sb.append("<h1 id=\"assumptions"+counter+"\"> Assumptions </h1>");
			indexString.append("<h3><a href=\"#assumptions"+counter+"\" style=\"text-decoration: none;\"> Chapter " + chapter + " - Assumptions</a></h3>");
			counter++;
			/*for (Element child : group.getContents()) {
				sb.append(new GoalTableCellTemplateHTML().generate(child,
						position++, group.getContents().size()));
			}*/
		/*}
		if(groupContainsOnlyType(group, Stakeholder.class)){
			// format the stakeholders as a summary table first
			int position = 0;
			sb.append("<h1 id=\"stakeholder"+counter+"\"> Stakeholder </h1>");
			indexString.append("<h3><a href=\"#stakeholder"+counter+"\" style=\"text-decoration: none;\"> Chapter " + chapter + " - Stakeholder</a></h3>");
			counter++;
			for (Element child : group.getContents()) {
				sb.append(new StakeholderSummaryTemplateHTML().generate(child,
						position++, group.getContents().size()));
			}			
		}
		
		//Need to be checked
		if(groupContainsOnlyType(group, Document.class)){
			// format the Document as a summary table
			int position = 0;
			for (Element child : group.getContents()) {
				sb.append(new DocumentSummaryTemplateHTML().generate(child,
						position++, group.getContents().size()));
			}			
		}
		
		
		for (Element child : group.getContents()) {
			if (child instanceof Group) {
				childGroups.add((Group) child);
			} else if (child instanceof GlossaryEntry) {
//				System.out.println("GlossaryEntry");
				sb.append(new GlossaryEntryTemplateHTML().generate(child));
			} else if (!allAreGoals && child instanceof Goal) {
//				System.out.println("Goal");
				sb.append(new GoalTemplateHTML().generate(child));
			} else if (child instanceof Persona) {
//				System.out.println("Persona");
				sb.append(new PersonaTemplateHTML().generate(child,
						inlineImages, path));
			} else if (child instanceof Requirement) {
//				System.out.println("Requirement");
				sb.append(new RequirementTemplateHTML().generate(child,
						inlineImages, path));
			} else if (child instanceof Stakeholder) {
//				System.out.println("Stakeholder");
				sb.append(new StakeholderTemplateHTML().generate(child));
			} else if (child instanceof Vision) {
//				System.out.println("Vision");
				sb.append(new VisionTemplateHTML().generate(child));
			} else if (child instanceof Usecase) {
//				System.out.println("Usecase");
				sb.append(new UsecaseTemplateHTML().generate(child));
			} else if (child instanceof Scenario) {
//				System.out.println("Scenario");
				sb.append(new ScenarioTemplateHTML().generate(child,
						inlineImages, path));
			} 
			else if (child instanceof TestCase) {
//				System.out.println("TestCase");
				sb.append(new TestCaseTemplateHTML().generate(child));
			}   
			else if (child instanceof Configuration) {
//				System.out.println("Configuration");
				sb.append(new ConfigurationTemplateHTML().generate(child));
			} 
			else if (child instanceof Actor) {
//				System.out.println("Actor");
				sb.append(new ActorTemplateHTML().generate(child));
			} 
			else if (child instanceof ActorRelation) {
//				System.out.println("ActorRelation");
				sb.append(new ActorRelationTemplateHTML().generate(child));
			}  
			else if (child instanceof Assumption) {
//				System.out.println("Assumption");
				sb.append(new AssumptionTemplateHTML().generate(child));
			} else if (child instanceof Document) {
//				System.out.println("Document");
				sb.append(new DocumentTemplateHTML().generate(child,inlineImages,path));
			} else if (child instanceof Diagram) {
				Diagram d = (Diagram) child;
				System.out.println(d.getVisualDiagram().getDiagramType());
				VisualModelEditorInputImpl editorInput =
						new VisualModelEditorInputImpl(d);
				//Image des = editorInput.getImageDescriptor().getImageData();
				//Image i = d.
			}
		}

		int count = 1;
		for (Group childGroup : childGroups) {

			if (group instanceof Project || group instanceof File) {
				// project or file don't need to be assigned with chapter
				sb.append(visitGroup(childGroup, level + 1,
						String.valueOf(count), inlineImages, path));
				count++;
			} else {
				sb.append(visitGroup(childGroup, level + 1, chapter + "."
						+ count, inlineImages, path));
				count++;
			}
		}

		if (group instanceof Glossary) {
			sb.append("</table>" + NL + "</div>" + NL);
		}
		//sb.append("<h>No hope</h>");
		
		return sb.toString();
	}*/
	
	private String checkForChapter(String indexSeperator)
	{
		if(indexSeperator.equals(""))
		{
			indexSeperator="Chapter : "+chapter+" ";
			chapter++;
		}
		return indexSeperator;
	}
	
	private void getElementData(Element child, int level,
			boolean inlineImages, String path)
	{
		String indexSeperator ="";
		for(int i=level-1;i>0;i--)
		{
			indexSeperator+="--";
		}
		if(areGlossary==true && !(child instanceof GlossaryEntry))
		{
			elementInfoString.append("</table>" + NL + "</div>" + NL);
			areGlossary=false;
		}else if(areGoal==true && !(child instanceof Goal))
		{
			elementInfoString.append("</table>"+NL+"</div><br/>"+NL);
			areGoal=false;
		}
		if (child instanceof GlossaryEntry) {
			areGlossary=true;
			if(!(oldElement instanceof GlossaryEntry))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"glossary"+counter+"\"> Glossary </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#glossary"+counter+"\" style=\"text-decoration: none;\">"+"Glossary</a></h3>");
				}
				counter++;
				elementInfoString.append("<div class=\"wrap\">");
				elementInfoString.append("<table border=\"1\">");
				elementInfoString.append("<tr>");
				elementInfoString.append("<th>Term</th>");
				elementInfoString.append("<th>Abbreviations</th>");
				elementInfoString.append("<th>Synonyms</th>");
				elementInfoString.append("<th>Definition / Examples</th>");
				elementInfoString.append("</tr>");
			}
			elementInfoString.append(new GlossaryEntryTemplateHTML().generate(child));
		} else if (child instanceof Goal) {
			areGoal=true;
			if(!(oldElement instanceof Goal))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"goal"+counter+"\"> Goal </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#goal"+counter+"\" style=\"text-decoration: none;\">"+" Goal</a></h3>");
				}
				counter++;
				elementInfoString.append("<div class=\"wrap\">");
				elementInfoString.append("<table border=\"1\">");
				elementInfoString.append(ReportHelper.insTableRow(true,
						"Goal","Kind","Name","Supports","Description"
						));	
			}
			elementInfoString.append(new GoalTableCellTemplateHTML().generate(child,1,10));
		} else if (child instanceof Persona) {
			//indexSeperator= checkForChapter(indexSeperator);
			if(!(oldElement instanceof Persona))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"persona"+counter+"\"> Persona </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#persona"+counter+"\" style=\"text-decoration: none;\">"+" Persona</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new PersonaTemplateHTML().generate(child,
					inlineImages, path));
		} else if (child instanceof Requirement) {
			if(!(oldElement instanceof Requirement))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"requirement"+counter+"\"> Requirement </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#requirement"+counter+"\" style=\"text-decoration: none;\">"+" Requirement</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new RequirementTemplateHTML().generate(child,
					inlineImages, path));
		} else if (child instanceof Stakeholder) {
			if(!(oldElement instanceof Stakeholder))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"stakeholder"+counter+"\"> Stakeholder </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#stakeholder"+counter+"\" style=\"text-decoration: none;\">"+" Stakeholder</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new StakeholderTemplateHTML().generate(child));
		} else if (child instanceof Vision) {
			if(!(oldElement instanceof Vision))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"vision"+counter+"\"> Vision </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#vision"+counter+"\" style=\"text-decoration: none;\">"+" Vision</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new VisionTemplateHTML().generate(child));
		} else if (child instanceof UseCase) {
			if(!(oldElement instanceof UseCase))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"usecase"+counter+"\"> Usecase </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#usecase"+counter+"\" style=\"text-decoration: none;\">"+" Usecase</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new UsecaseTemplateHTML().generate(child));
		} else if (child instanceof UseCaseScenario) {
			if(!(oldElement instanceof UseCaseScenario))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"scenario"+counter+"\"> Scenario </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#scenario"+counter+"\" style=\"text-decoration: none;\">"+" Scenario</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new ScenarioTemplateHTML().generate(child,
					inlineImages, path));
		} 
		else if (child instanceof TestCase) {
			if(!(oldElement instanceof TestCase))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"testCase"+counter+"\"> TestCase </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#testCase"+counter+"\" style=\"text-decoration: none;\">"+" TestCase</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new TestCaseTemplateHTML().generate(child));
		}  
		else if (child instanceof Configuration) {
			if(!(oldElement instanceof Configuration))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"configuration"+counter+"\"> Configuration </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#configuration"+counter+"\" style=\"text-decoration: none;\">"+" Configuration</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new ConfigurationTemplateHTML().generate(child));
		} 
		else if (child instanceof Actor) {
			if(!(oldElement instanceof Actor))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"actor"+counter+"\"> Actor </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#actor"+counter+"\" style=\"text-decoration: none;\">"+" Actor</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new ActorTemplateHTML().generate(child));
		} 
		else if (child instanceof Connector) {
			if(!(oldElement instanceof Connector))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"actorRelation"+counter+"\"> ActorRelation </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#actorRelation"+counter+"\" style=\"text-decoration: none;\">"+" ActorRelation</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new ActorRelationTemplateHTML().generate(child));
		}  
		else if (child instanceof Assumption) {
			if(!(oldElement instanceof Assumption))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"assumption"+counter+"\"> Assumption </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#assumption"+counter+"\" style=\"text-decoration: none;\">"+" Assumption</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new AssumptionTemplateHTML().generate(child));
		} else if (child instanceof Document) {
			if(!(oldElement instanceof Document))
			{
				//indexSeperator= checkForChapter(indexSeperator);
				elementInfoString.append("<h1 id=\"document"+counter+"\"> Document </h1>");
				if(indexSeperator.equals(""))
				{
					indexString2.append("<h3><a href=\"#document"+counter+"\" style=\"text-decoration: none;\">"+" Document</a></h3>");
				}
				counter++;
			}
			elementInfoString.append(new DocumentTemplateHTML().generate(child,inlineImages,path));
		} else if (child instanceof Diagram) {
			//indexSeperator= checkForChapter(indexSeperator);
			elementInfoString.append("<h1 id=\"diagram"+counter+"\"> Diagram </h1>");
			if(indexSeperator.equals(""))
			{
				indexString2.append("<h3><a href=\"#diagram"+counter+"\" style=\"text-decoration: none;\">"+" Diagram</a></h3>");
			}
			counter++;
			DiagramExtension d = new DiagramExtension();
			String path2=path+"\\"+child.getName()+".jpeg";
			path2=d.saveDiagram(child,path2);
			
			elementInfoString.append(new DiagramTemplateHTML().generate(path2,child));
			//indexSeperator= checkForChapter(indexSeperator);
			/*Diagram d = (Diagram) child;
			System.out.println(d.getVisualDiagram().getDiagramType());
			VisualModelEditorInputImpl editorInput =
					new VisualModelEditorInputImpl(d);
			String saveLocation = "D:\\SJ\\Red testing project\\12-11-2015\\Test.jpeg";
			int format = SWT.IMAGE_JPEG;
			//Diagram d = (Diagram) child;
			VisualDiagram v =d.getVisualDiagram();
			//d.g
			Image img3 =v.getDiagramElement().getIcon();
			GC imageGC = new GC(img3);
			ImageData[] imgData = new ImageData[1];
			imgData[0] = img3.getImageData();
			ImageLoader imgLoader = new ImageLoader();
			imgLoader.data = imgData;
			imgLoader.save(saveLocation, format);
			
			imageGC.dispose();
			img3.dispose();*/
			
			//editorInput.
			//Image des = editorInput.getImageDescriptor().getImageData();
			//Image i = d.
		}
		oldElement=child;
	}
	
	private void getElementsData(Group group, int level,
			boolean inlineImages, String path) {
		if (level > 3)
			level = 3;
		
		if (group instanceof Project) {
			level = 0;
			projectInfoString.append("<div class=\"Title\"> "+ group.getName() + "</div>");
			projectInfoString.append(new ProjectTemplateHTML().generate(group));
			/*sb.append("<div class=\"Title\">" + group.getName() + "</div>");
			sb.append(new ProjectTemplateHTML().generate(group));*/
		}
		if(group instanceof Folder)
		{
			String indexSeperator ="";
			for(int i=level-2;i>0;i--)
			{
				indexSeperator+="--";
			}
			if(indexSeperator.equals(""))
			{
				//indexSeperator="Chapter : "+chapter+" ";
				indexSeperator="Chapter "+chapter+": ";
				chapter++;
				//Only top level folders to be with chapter in index

				elementInfoString.append("<span id=\"folder"+counter+"\"></Span>");
				indexString.append("<h3><a href=\"#folder"+counter+"\" style=\"text-decoration: none;\">"+indexSeperator+ group.getName() + " </a></h3>");
				counter++;
			}
			//indexString.append("<h3><a href=\"#\" style=\"text-decoration: none;\">"+indexSeperator+"  Folder : " + group.getName() + " </a></h3>");
		}
		for (Element child : group.getContents()) {
			if (child instanceof Group) {
				if (!(group instanceof Project)) {
					Group g = (Group) child;
					/*indexString.append("<h3><a href=\"#\" style=\"text-decoration: none;\">  Structure : "+chapter+" - " + g.getName() + " </a></h3>");
					chapter++;*/
				}
				getElementsData((Group) child,level+1,inlineImages,path);
			} else
			{
				getElementData(child, level, inlineImages, path);
			}
		}	
	}

	@SuppressWarnings({"rawtypes","unchecked"})
	public boolean groupContainsOnlyType(Group group, Class clazz) {
		boolean allAreSameType = false;
		for (Element child : group.getContents()) {
			if (clazz.isAssignableFrom(child.getClass())) {
				// this container contains all goals only
				allAreSameType = true;
			} else {
				allAreSameType = false;
				break;
			}
		}
		return allAreSameType;
	}
}
