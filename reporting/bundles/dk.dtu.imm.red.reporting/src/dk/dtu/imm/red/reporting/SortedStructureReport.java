package dk.dtu.imm.red.reporting;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.reporting.jet.ActorRelationTemplateHTML;
import dk.dtu.imm.red.reporting.jet.ActorTemplateHTML;
import dk.dtu.imm.red.reporting.jet.AssumptionTemplateHTML;
import dk.dtu.imm.red.reporting.jet.ConfigurationTemplateHTML;
import dk.dtu.imm.red.reporting.jet.DiagramTemplateHTML;
import dk.dtu.imm.red.reporting.jet.DocumentSummaryTemplateHTML;
import dk.dtu.imm.red.reporting.jet.GlossaryEntryTemplateHTML;
import dk.dtu.imm.red.reporting.jet.GoalTableCellTemplateHTML;
import dk.dtu.imm.red.reporting.jet.PersonaTemplateHTML;
import dk.dtu.imm.red.reporting.jet.ProjectTemplateHTML;
import dk.dtu.imm.red.reporting.jet.RequirementTemplateHTML;
import dk.dtu.imm.red.reporting.jet.ScenarioTemplateHTML;
import dk.dtu.imm.red.reporting.jet.StakeholderSummaryTemplateHTML;
import dk.dtu.imm.red.reporting.jet.TestCaseTemplateHTML;
import dk.dtu.imm.red.reporting.jet.UsecaseTemplateHTML;
import dk.dtu.imm.red.reporting.jet.VisionTemplateHTML;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;
import dk.dtu.imm.red.specificationelements.configuration.Configuration;
import dk.dtu.imm.red.specificationelements.connector.Connector;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.testcase.TestCase;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecase.UseCase;
import dk.dtu.imm.red.specificationelements.vision.Vision;
import dk.dtu.imm.red.visualmodeling.ui.editors.DiagramExtension;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;

public class SortedStructureReport implements IReport {
	
	//simple structure

	public final String NL = System.getProperties().getProperty(
			"line.separator");	
	protected List<Persona> personas = new ArrayList<Persona>();
	protected List<Requirement> requirements = new ArrayList<Requirement>();
	protected List<Stakeholder> stakeholders = new ArrayList<Stakeholder>();
	protected List<Goal> goals = new ArrayList<Goal>();
	public List<Vision> visions = new ArrayList<Vision>();
	protected List<UseCase> useCases = new ArrayList<UseCase>();
	protected List<UseCaseScenario> scenarios = new ArrayList<UseCaseScenario>();
	protected List<Configuration> systemStructures = new ArrayList<Configuration>();
	protected List<Actor> actors = new ArrayList<Actor>();
	protected List<Connector> connectors = new ArrayList<Connector>();
	protected List<Assumption> assumptions = new ArrayList<Assumption>();	
	protected List<GlossaryEntry> glossaryEntries = new ArrayList<GlossaryEntry>();
	protected List<TestCase> testCases = new ArrayList<TestCase>(); 
	protected List<Document> documents = new ArrayList<Document>(); 
	protected List<Diagram> diagrams = new ArrayList<Diagram>();
	protected Project project;

	@Override
	public String generateReport(Element element, boolean inlineImages,  String path) {

		StringBuffer sb = new StringBuffer();
		StringBuffer sbFinal = new StringBuffer();
		String appendixString ="";
		if (element instanceof Group) {
			sort((Group) element);
		} else {
			Folder tempFolder = FolderFactory.eINSTANCE.createFolder();
			tempFolder.getContents().add(element);
			sort(tempFolder);
		}

		int chapter = 1;

		sbFinal.append(getHTMLHeaderAndCSS());
		//chapter++;
		//)Type, Label, Name, Version, Status, Author
		
		String[] headings = {"Type","Label","Name","Version","Last modified","Author"};
		appendixString+=ReportHelper.insTableRow(true,headings);
		
		if (project != null) {
			sbFinal.append("<div class=\"Title\">" + project.getName() + "</div>");
			sbFinal.append(new ProjectTemplateHTML().generate(project));
			
			String type = "Project";
			String name = project.getName();
			String version = project.getVersion();
			String lastSave = project.getLastModified().toString();
			project.getClass().getName();
			String author = "Not saved";
			if(project.getCreator().getName()!=null || project.getCreator().getName() !="")
			{
				author =project.getCreator().getName();
			}
			String[] values = {type,"-",name,version,lastSave,author};
			appendixString+=ReportHelper.insTableRow(false, values);
		} 

		sbFinal.append("<h1> Index </h1>");
		
		if (visions.size() > 0) {
			sb.append("<h1 id=\"visions\"> Chapter " + chapter + ": Visions</h1>");
			sbFinal.append("<h3><a href=\"#visions\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Visions</a></h3>");
			chapter++;
			
			for (Vision vision : visions) {
				sb.append(new VisionTemplateHTML().generate(vision));
				
				String type = "Vision";
				String name = vision.getName();
				String label = vision.getLabel();
				String version = vision.getVersion();
				String lastSave = vision.getLastModified().toString();
				vision.getClass().getName();
				String author = "Not saved";
				if(vision.getCreator().getName()!=null || vision.getCreator().getName() !="")
				{
					author =vision.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}
		if (assumptions.size() > 0) {
			sb.append("<h1 id=\"assumptions\"> Chapter " + chapter + ": Assumptions</h1>");
			sbFinal.append("<h3><a href=\"#assumptions\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Assumptions</a></h3>");
			chapter++;
			
			for (Assumption assumption : assumptions) {
				sb.append(new AssumptionTemplateHTML().generate(assumption));
				
				String type = "Assumption";
				String name = assumption.getName();
				String label = assumption.getLabel();
				String version = assumption.getVersion();
				String lastSave = assumption.getLastModified().toString();
				assumption.getClass().getName();
				String author = "Not saved";
				if(assumption.getCreator().getName()!=null || assumption.getCreator().getName() !="")
				{
					author =assumption.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}
		
		
		if (stakeholders.size() > 0) {
			sb.append("<h1 id=\"stakeholders\"> Chapter " + chapter + ": Stakeholders</h1>");
			sbFinal.append("<h3><a href=\"#stakeholders\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Stakeholders</a></h3>");
			chapter++;
			int position = 0;
			for (Stakeholder stakeholder : stakeholders) {
				sb.append(new StakeholderSummaryTemplateHTML().generate(stakeholder,
						position++, stakeholders.size()));
				
				String type = "Stakeholder";
				String name = stakeholder.getName();
				String label = stakeholder.getLabel();
				String version = stakeholder.getVersion();
				String lastSave = stakeholder.getLastModified().toString();
				stakeholder.getClass().getName();
				String author = "Not saved";
				if(stakeholder.getCreator().getName()!=null || stakeholder.getCreator().getName() !="")
				{
					author =stakeholder.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}		
			/*for (Stakeholder stakeholder : stakeholders) {
				sb.append(new StakeholderTemplateHTML().generate(stakeholder));
			}*/
		}
		if (goals.size() > 0) {
			sb.append("<h1 id=\"goals\"> Chapter " + chapter + ": Goals</h1>");
			sbFinal.append("<h3><a href=\"#goals\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Goals</a></h3>");
			chapter++;
			int position = 0;			
			for (Goal goal : goals) {
				sb.append(new GoalTableCellTemplateHTML().generate(goal,
						position++, goals.size()));
				
				String type = "Goal";
				String name = goal.getName();
				String label = goal.getLabel();
				String version = goal.getVersion();
				String lastSave = goal.getLastModified().toString();
				goal.getClass().getName();
				String author = "Not saved";
				if(goal.getCreator().getName()!=null || goal.getCreator().getName() !="")
				{
					author =goal.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}
		if(diagrams.size()>0)
		{
			boolean there=true;
			for(Diagram d : diagrams)
			{
				if(d.getVisualDiagram().getDiagramType()==DiagramType.GOAL)
				{
					if(there)
					{
						there=false;
						sb.append("<h1 id=\"diagramGoal\"> Chapter " + chapter + ": Goal Diagram</h1>");
						sbFinal.append("<h3><a href=\"#diagramGoal\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Goal Diagram</a></h3>");
						chapter++;
					}
					Element child=d;
					DiagramExtension d3 = new DiagramExtension();
					String path2=path+"\\"+child.getName()+".jpeg";
					path2=d3.saveDiagram(child,path2);
					
					sb.append(new DiagramTemplateHTML().generate(path2,child));
				}
			}
		}
		if (personas.size() > 0) {
			sb.append("<h1 id=\"personas\"> Chapter " + chapter + ": Personas</h1>");
			sbFinal.append("<h3><a href=\"#personas\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Personas</a></h3>");
			chapter++;
			for (Persona persona : personas) {
				sb.append(new PersonaTemplateHTML().generate(persona, inlineImages, path));
			
				String type = "Persona";
				String name = persona.getName();
				String label = persona.getLabel();
				String version = persona.getVersion();
				String lastSave = persona.getLastModified().toString();
				persona.getClass().getName();
				String author = "Not saved";
				if(persona.getCreator().getName()!=null || persona.getCreator().getName() !="")
				{
					author =persona.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}
		if (requirements.size() > 0) {
			sb.append("<h1 id=\"requirements\"> Chapter " + chapter + ": Requirements</h1>");
			sbFinal.append("<h3><a href=\"#requirements\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Requirements</a></h3>");
			chapter++;
			for (Requirement requirement : requirements) {
				sb.append(new RequirementTemplateHTML().generate(requirement, inlineImages, path));
			
				String type = "Requirement";
				String name = requirement.getName();
				String label = requirement.getLabel();
				String version = requirement.getVersion();
				String lastSave = requirement.getLastModified().toString();
				requirement.getClass().getName();
				String author = "Not saved";
				if(requirement.getCreator().getName()!=null || requirement.getCreator().getName() !="")
				{
					author =requirement.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}
		if (testCases.size() > 0) {
			sb.append("<h1 id=\"testCases\"> Chapter " + chapter + ": Test Cases</h1>");
			sbFinal.append("<h3><a href=\"#testCases\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Test Cases</a></h3>");
			
			chapter++;
			for (TestCase testcase : testCases) {
				sb.append(new TestCaseTemplateHTML().generate(testcase));
			
				String type = "TestCase";
				String name = testcase.getName();
				String label = testcase.getLabel();
				String version = testcase.getVersion();
				String lastSave = testcase.getLastModified().toString();
				testcase.getClass().getName();
				String author = "Not saved";
				if(testcase.getCreator().getName()!=null || testcase.getCreator().getName() !="")
				{
					author =testcase.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}
		if (useCases.size() > 0) {
			sb.append("<h1 id=\"usecases\"> Chapter " + chapter + ": Use cases</h1>");
			sbFinal.append("<h3><a href=\"#usecases\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Use cases</a></h3>");
			chapter++;
			for (UseCase useCase : useCases) {
				sb.append(new UsecaseTemplateHTML().generate(useCase));
			
				String type = "Usecase";
				String name = useCase.getName();
				String label = useCase.getLabel();
				String version = useCase.getVersion();
				String lastSave = useCase.getLastModified().toString();
				useCase.getClass().getName();
				String author = "Not saved";
				if(useCase.getCreator().getName()!=null || useCase.getCreator().getName() !="")
				{
					author =useCase.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}
		if(diagrams.size()>0)
		{
			boolean there=true;
			for(Diagram d : diagrams)
			{
				if(d.getVisualDiagram().getDiagramType()==DiagramType.USE_CASE
						|| d.getVisualDiagram().getDiagramType()==DiagramType.USE_CASE_MAP)
				{
					if(there)
					{
						there=false;
						sb.append("<h1 id=\"diagramUseCase\"> Chapter " + chapter + ": Usecase Diagram</h1>");
						sbFinal.append("<h3><a href=\"#diagramUseCase\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Usecase Diagram</a></h3>");
						chapter++;
					}
					Element child=d;
					DiagramExtension d3 = new DiagramExtension();
					String path2=path+"\\"+child.getName()+".jpeg";
					path2=d3.saveDiagram(child,path2);
					
					sb.append(new DiagramTemplateHTML().generate(path2,child));
				}
			}
		}
		if (scenarios.size() > 0) {
			sb.append("<h1 id=\"scenarios\"> Chapter " + chapter + ": Scenarios</h1>");
			sbFinal.append("<h3><a href=\"#scenarios\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Scenarios</a></h3>");
			chapter++;
			for (UseCaseScenario scenario : scenarios) {
				sb.append(new ScenarioTemplateHTML().generate(scenario,
						inlineImages, path));
				
				String type = "Scenario";
				String name = scenario.getName();
				String label = scenario.getLabel();
				String version = scenario.getVersion();
				String lastSave = scenario.getLastModified().toString();
				scenario.getClass().getName();
				String author = "Not saved";
				if(scenario.getCreator().getName()!=null || scenario.getCreator().getName() !="")
				{
					author =scenario.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}

		if (systemStructures.size() > 0) {
			sb.append("<h1 id=\"configurations\"> Chapter " + chapter + ": Configurations</h1>");
			sbFinal.append("<h3><a href=\"#configurations\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Configurations</a></h3>");
			chapter++;
			for (Configuration systemStructure : systemStructures) {
				sb.append(new ConfigurationTemplateHTML().generate(systemStructure));
			
				String type = "Configuration";
				String name = systemStructure.getName();
				String label = systemStructure.getLabel();
				String version = systemStructure.getVersion();
				String lastSave = systemStructure.getLastModified().toString();
				systemStructure.getClass().getName();
				String author = "Not saved";
				if(systemStructure.getCreator().getName()!=null || systemStructure.getCreator().getName() !="")
				{
					author =systemStructure.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}

		if (actors.size() > 0) {
			sb.append("<h1 id=\"actors\"> Chapter " + chapter + ": Actors</h1>");
			sbFinal.append("<h3><a href=\"#actors\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Actors</a></h3>");
			chapter++;
			for (Actor actor: actors) {
				sb.append(new ActorTemplateHTML	().generate(actor));
				
				String type = "Actor";
				String name = actor.getName();
				String label = actor.getLabel();
				String version = actor.getVersion();
				String lastSave = actor.getLastModified().toString();
				actor.getClass().getName();
				String author = "Not saved";
				if(actor.getCreator().getName()!=null || actor.getCreator().getName() !="")
				{
					author =actor.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}

		if (connectors.size() > 0) {
			sb.append("<h1 id=\"actorRelations\"> Chapter " + chapter + ": Actor Relations</h1>");
			sbFinal.append("<h3><a href=\"#actorRelations\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Actor Relations</a></h3>");
			chapter++;
			for (Connector connector: connectors) {
				sb.append(new ActorRelationTemplateHTML().generate(connector));
			
				String type = "ActorRelation";
				String name = connector.getName();
				String label = connector.getLabel();
				String version = connector.getVersion();
				String lastSave = connector.getLastModified().toString();
				connector.getClass().getName();
				String author = "Not saved";
				if(connector.getCreator().getName()!=null || connector.getCreator().getName() !="")
				{
					author =connector.getCreator().getName();
				}
				String[] values = {type,label,name,version,lastSave,author};
				appendixString+=ReportHelper.insTableRow(false, values);
			}
		}

		if (glossaryEntries.size() > 0) {
			sb.append("<h1> Glossary </h1>");
			sb.append("<div class=\"wrap\">");
			sb.append("<table border=\"1\">");
			sb.append("<tr>");
			sb.append("<th>Term</th>");
			sb.append("<th>Abbreviations</th>");
			sb.append("<th>Synonyms</th>");
			sb.append("<th>Definition / Examples</th>");
			sb.append("</tr>");
			for (GlossaryEntry glossaryEntry : glossaryEntries) {
				sb.append(new GlossaryEntryTemplateHTML().generate(glossaryEntry));
			}
			sb.append("</table>" + NL + "</div>" + NL);
		}

		if (documents.size() > 0) {
			sb.append("<h1> Document Appendix </h1>");
			int position = 0;
			for (Document document : documents) {
				sb.append(new DocumentSummaryTemplateHTML().generate(document,position++,documents.size()));
			}
			/*for (Document document : documents) {				
				sb.append(new DocumentTemplateHTML().generate(document,inlineImages,path));
			}*/
		}
		if(diagrams.size()>0)
		{
			boolean there=true;
			for(Diagram d : diagrams)
			{
				if(d.getVisualDiagram().getDiagramType()!=DiagramType.GOAL
						&& d.getVisualDiagram().getDiagramType()!=DiagramType.USE_CASE
						&& d.getVisualDiagram().getDiagramType()!=DiagramType.USE_CASE_MAP)
				{
					if(there)
					{
						there=false;
						sb.append("<h1 id=\"other\"> Chapter " + chapter + ": Diagrams</h1>");
						sbFinal.append("<h3><a href=\"#other\" style=\"text-decoration: none;\"> Chapter " + chapter + ": Diagrams</a></h3>");
						chapter++;
					}
					Element child=d;
					DiagramExtension d3 = new DiagramExtension();
					String path2=path+"\\"+child.getName()+".jpeg";
					path2=d3.saveDiagram(child,path2);
					
					sb.append(new DiagramTemplateHTML().generate(path2,child));
				}
			}
		}
		
		sb.append(ReportHelper.insTag("h4","Table of all specification elements",false));
		sb.append(ReportHelper.insTable("", appendixString));
		sbFinal.append(sb);
		sbFinal.append("</body>" + NL);
		sbFinal.append("</html>" + NL);

		return sbFinal.toString();
	}

	private void sort(Group group) {
		if (group instanceof Project && project == null) {
			this.project = (Project) group;
		}
		for (Element child : group.getContents()) {  
			if (child instanceof Persona) {
				personas.add((Persona) child);
			}else if (child instanceof Requirement) {
				requirements.add((Requirement) child);
			}else if (child instanceof Diagram) {
				diagrams.add((Diagram) child);
			}else if (child instanceof TestCase) {
				testCases.add((TestCase) child);
			}else if (child instanceof Stakeholder) {
				stakeholders.add((Stakeholder) child);
			}else if (child instanceof Goal) {
				goals.add((Goal) child);
			}else if (child instanceof Vision) {
				visions.add((Vision) child);

			}else if (child instanceof UseCase) {
				useCases.add((UseCase) child);
			} 
			else if (child instanceof UseCaseScenario) {
				scenarios.add((UseCaseScenario) child);  
			} 
			else if (child instanceof Configuration) {
				systemStructures.add((Configuration) child);  
			} 
			else if (child instanceof Actor) {
				actors.add((Actor) child);  
			} 
			else if (child instanceof Connector) {
				connectors.add((Connector) child);  
			} 
			else if (child instanceof Assumption) {
				assumptions.add((Assumption) child);
			}else if (child instanceof GlossaryEntry) {
				glossaryEntries.add((GlossaryEntry) child);
			}else if (child instanceof Document) {
				documents.add((Document) child);				
			}else if (child instanceof Group){
				sort((Group) child);
			} 
		}
	}

	private String getHTMLHeaderAndCSS() {
		StringBuffer cssStyle = new StringBuffer("");
		try {

			Bundle bundle = FrameworkUtil.getBundle(this.getClass());
			URL url = FileLocator.find(bundle,
					new Path("css/report_style.css"), null);
			InputStream inputStream = url.openConnection().getInputStream();
//			FileInputStream inputStream = new FileInputStream("css/report_style.css");
			BufferedReader in = new BufferedReader(new InputStreamReader(
					inputStream));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				cssStyle.append(inputLine + "\n");
			}

			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

		String DOCTYPE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">"+NL;		
		String HTML_STRING = "<html>" + NL + "<head>" + NL
				+ "<style type=\"text/css\">" + NL;
		String HTML_STRING_2 = "</style>" + NL + "</head>" + NL;
		return DOCTYPE+HTML_STRING + cssStyle + HTML_STRING_2;
	}	


}
