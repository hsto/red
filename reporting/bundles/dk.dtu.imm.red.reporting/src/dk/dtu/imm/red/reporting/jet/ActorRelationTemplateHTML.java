package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.actor.Actor;
import dk.dtu.imm.red.specificationelements.connector.Connector;

public class ActorRelationTemplateHTML {
	protected static String nl;

	public static synchronized ActorRelationTemplateHTML create(
			String lineSeparator) {
		nl = lineSeparator;
		ActorRelationTemplateHTML result = new ActorRelationTemplateHTML();
		nl = null;
		return result;
	}

	public final String NL = nl == null ? (System.getProperties()
			.getProperty("line.separator")) : nl;
	protected final String TEXT_1 = "<h4> ActorRelation - ";
	protected final String TEXT_2 = " </h4> " + NL + "" + NL + "<p>" + NL
			+ "\tID: ";
	protected final String TEXT_3 = " </br>" + NL + "\tName: ";
	protected final String TEXT_4 = " </br>" + NL + "\tKind: ";
	protected final String TEXT_5 = " </br>" + NL + "\tDescription: ";
	protected final String TEXT_6 = " </br>" + NL + "</p> " + NL + " " + NL
			+ "   <p><i>Properties</i></br></br>" + NL
			+ "   <table border=\"1\">\t" + NL + "    <tr>" + NL
			+ "      <td>Property</td>" + NL + "\t  <td>Value</td>" + NL
			+ "\t  <td>Unit</td>" + NL + "   </tr> " + NL + "   <tr>" + NL
			+ "      <td>Delay</td>" + NL + "\t  <td>";
	protected final String TEXT_7 = "</td>" + NL + "\t  <td>";
	protected final String TEXT_8 = "</td>" + NL + "   </tr> " + NL + "   "
			+ NL + "   <tr>" + NL + "      <td>Latency</td>" + NL + "\t  <td>";
	protected final String TEXT_9 = "</td>" + NL + "\t  <td>";
	protected final String TEXT_10 = "</td>" + NL + "   </tr> " + NL + "   "
			+ NL + "    <tr>" + NL + "      <td>Attitude</td>" + NL
			+ "\t  <td>";
	protected final String TEXT_11 = "</td>" + NL + "\t  <td> - </td>" + NL
			+ "   </tr>  " + NL + "</table>" + NL + "</p>" + NL + " <br>" + NL
			+ " <i>Source</i>" + NL + " <br>" + NL + "<table border=\"1\">\t"
			+ NL + " \t<tr>" + NL + " \t\t<td>ID</td> " + NL
			+ "\t\t<td>Name</td> " + NL + "\t</tr>";
	protected final String TEXT_12 = " " + NL + "\t<tr> " + NL + "\t\t<td>";
	protected final String TEXT_13 = "</td> " + NL + "\t\t<td>";
	protected final String TEXT_14 = "</td>  " + NL + "\t</tr>" + NL + "\t";
	protected final String TEXT_15 = NL + "\t</table> " + NL + "\t</p>\t" + NL
			+ "<br>" + NL + " <i>Target</i>" + NL + "<table border=\"1\">\t"
			+ NL + " \t<tr>" + NL + " \t\t<td>ID</td> " + NL
			+ "\t\t<td>Name</td> " + NL + "\t</tr>";
	protected final String TEXT_16 = " " + NL + "\t<tr> " + NL + "\t\t<td>";
	protected final String TEXT_17 = "</td> " + NL + "\t\t<td>";
	protected final String TEXT_18 = "</td>  " + NL + "\t</tr>" + NL + "\t";
	protected final String TEXT_19 = NL + "\t</table> " + NL + "\t</p>";

	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		Connector actorRelation = (Connector) argument;
		sb.append(ReportHelper.insBasicInfo(actorRelation));
		
		StringBuffer tbl = new StringBuffer();
		tbl.append(ReportHelper.insTableRow(true, "Property","Value","Unit"));
		tbl.append(ReportHelper.insTableRow(false,
				"Attitude",
				String.valueOf(actorRelation.getAttitude().getValue()),
				"-"
				));		
		sb.append(ReportHelper.insTable("Properties",tbl.toString()));
		
		tbl = new StringBuffer();
		tbl.append(ReportHelper.insTableRow(true, "Label","Name"));
		for (Object o : actorRelation.getSource()) {
			Actor a = (Actor) o;
			tbl.append(ReportHelper.insTableRow(false, a.getId(),a.getName()));
		}
		sb.append(ReportHelper.insTable("Source",tbl.toString()));

		tbl = new StringBuffer();
		tbl.append(ReportHelper.insTableRow(true, "Label","Name"));
		for (Object o : actorRelation.getTarget()) {
			Actor a = (Actor) o;
			tbl.append(ReportHelper.insTableRow(false, a.getId(),a.getName()));
		}
		sb.append(ReportHelper.insTable("Target",tbl.toString()));

		return sb.toString();
	}
}

