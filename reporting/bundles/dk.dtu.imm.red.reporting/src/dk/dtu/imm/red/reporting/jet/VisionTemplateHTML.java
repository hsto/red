package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.vision.*;

public class VisionTemplateHTML {

	public static synchronized VisionTemplateHTML create() {
		VisionTemplateHTML result = new VisionTemplateHTML();
		return result;
	}


	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		Vision vision = (Vision) argument;
		
		sb.append(ReportHelper.insBasicInfo(vision));
		sb.append(ReportHelper.insTag("h4", "Mission", false));
		sb.append(ReportHelper.insTag("p", vision.getVision(),true));
		
		sb.append(ReportHelper.insTag("h4", "Expected value of outcome", false));
		sb.append(ReportHelper.insTag("p", vision.getExpectedValueOutcome(),true));
		//table
		String opportunities=null,
				challanges=null,
				sponsors=null,
				adversaries=null,
				groupValues_Commitment=null,
				beliefs=null;
		if(vision.getOpportunities()!=null)
		{
			opportunities = vision.getOpportunities().toString();
		}
		if(vision.getChallenges()!=null)
		{
			challanges = vision.getChallenges().toString();
		}
		if(vision.getSponsors()!=null)
		{
			sponsors = vision.getSponsors().toString();
		}
		if(vision.getAdversaries()!=null)
		{
			adversaries = vision.getAdversaries().toString();
		}
		if(vision.getGroupValuesAndCom()!=null)
		{
			groupValues_Commitment = vision.getGroupValuesAndCom().toString();
		}
		if(vision.getBeliefs()!=null)
		{
			beliefs = vision.getBeliefs().toString();
		}
		String tableString = ReportHelper.insSideWaysTwoColumnRow("Opportunities", opportunities);
		tableString += ReportHelper.insSideWaysTwoColumnRow("Challanges", challanges);
		tableString += ReportHelper.insSideWaysTwoColumnRow("Sponsors", sponsors);
		tableString += ReportHelper.insSideWaysTwoColumnRow("Adversaries", adversaries);
		//tableString += ReportHelper.insSideWaysTwoColumnRow("Group Values_Commitment", groupValues_Commitment);
		//Harald told to remove _Commitment
		tableString += ReportHelper.insSideWaysTwoColumnRow("Group Values", groupValues_Commitment);
		tableString += ReportHelper.insSideWaysTwoColumnRow("Beliefs", beliefs);
		
		sb.append(ReportHelper.insTable("", tableString));
		return sb.toString();
	}
}
