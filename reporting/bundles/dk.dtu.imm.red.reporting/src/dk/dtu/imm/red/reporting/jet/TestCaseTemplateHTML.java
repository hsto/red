package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.testcase.Action;
import dk.dtu.imm.red.specificationelements.testcase.TestCase;

public class TestCaseTemplateHTML {

	public static synchronized TestCaseTemplateHTML create() {
		TestCaseTemplateHTML result = new TestCaseTemplateHTML();
		return result;
	}


	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		TestCase testCase = (TestCase) argument;
		sb.append(ReportHelper.insBasicInfo(testCase));
		sb.append(ReportHelper.insTag("p", "PreConditions: "+
				ReportHelper.returnDashIfNull(testCase.getPreconditions()),true));
		sb.append(ReportHelper.insTag("p", "Input: "+
				ReportHelper.returnDashIfNull(testCase.getInput()),true));
		if (testCase.getActionList().size() > 0) {
			StringBuffer tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "ActionID", "Description"));
			for (Action action : testCase.getActionList()) {
				String actionID = action.getActionId();
				String actionDesc = action.getDescription();
				tbl.append(ReportHelper.insTableRow(false, actionID, actionDesc));
			}
			sb.append(ReportHelper.insTable("Action List", tbl.toString()));
		}
		sb.append(ReportHelper.insTag("p", "PostConditions: "+
				ReportHelper.returnDashIfNull(testCase.getPostconditions()),true));
		sb.append(ReportHelper.insTag("p", "Result: "+
				ReportHelper.returnDashIfNull(testCase.getResult()),true));
		
		return sb.toString();
	}
}
