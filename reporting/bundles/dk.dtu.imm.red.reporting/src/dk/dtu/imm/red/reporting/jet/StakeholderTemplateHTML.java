package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.stakeholder.*;

public class StakeholderTemplateHTML {
	protected static String nl = ReportHelper.NL;

	public static synchronized StakeholderTemplateHTML create(
			String lineSeparator) {
		StakeholderTemplateHTML result = new StakeholderTemplateHTML();
		return result;
	}

	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		Stakeholder stakeholder = (Stakeholder) argument;
		sb.append(ReportHelper.insBasicInfo(stakeholder));
		sb.append(ReportHelper.insTag("p", "Exposure:"+String.valueOf(stakeholder.getExposure())
				+", " + "Power:"+String.valueOf(stakeholder.getPower())
				+", " + "Urgency:"+String.valueOf(stakeholder.getUrgency())
				+", " + "Importance:"+String.valueOf(stakeholder.getImportance())
				, false));
		sb.append(ReportHelper.insTag("p","Description: "+ 
				ReportHelper.returnDashIfNull(stakeholder.getLongDescription()), false));
		sb.append(ReportHelper.insTag("p", "Stake:\n"+
				ReportHelper.returnDashIfNull(stakeholder.getStake()), false));
		sb.append(ReportHelper.insTag("p", "Engagement:\n"+
				ReportHelper.returnDashIfNull(stakeholder.getEngagement()), false));
		
		return sb.toString();
	}
}
