package dk.dtu.imm.red.reporting;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.bind.DatatypeConverter;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.ui.internal.LogUtil;

public class ReportHelper{
	
	public static final String NL = (System.getProperties().getProperty("line.separator"));

	public static String insBasicInfo(Element element){
		StringBuffer sb = new StringBuffer();
		
		String elementType = element.getClass().getSimpleName().replace("Impl", "");
		//use either name or label only
		if(element.getName()!=null && !element.getName().isEmpty()){
			sb.append(insTag("h4",elementType+" - "+element.getName(),false));
		}
		else if(element.getLabel()!=null && !element.getLabel().isEmpty()){
			sb.append(insTag("h4",elementType+" - "+element.getLabel(),false));
		}

		/*if(element.getElementKind()!=null && !element.getElementKind().isEmpty()
				 && !element.getElementKind().equals("unspecified")){
			sb.append(insTag("p","Kind: " + element.getElementKind(),false));
		}
		
		if(element.getPartOf()!=null){
			sb.append(insTag("p","Part Of: " + element.getPartOf().getName(),false));
		}*/
		
		if(element.getDescription()!=null && !element.getDescription().isEmpty()){
			sb.append(insTag("p",element.getDescription(),false));
		}
		
		return sb.toString();
	}
	
	public static String insTable(String tableName, String tbl){
		StringBuffer sb = new StringBuffer();
		
		if(tableName!=null & !tableName.isEmpty()){
			sb.append(ReportHelper.insTag("p", "<i>" +tableName +"</i>", false));
		}
		sb.append("<div class=\"wrap\">");
		//style=\"width:100%\" 
		sb.append("<table border=\"1\">"+NL);	
		sb.append(tbl);
		sb.append("</table>"+NL+"</div><br/>"+NL);
		return sb.toString();
	}
	
	public static String insSideWaysTwoColumnRow(String heading,String data)
	{
		StringBuffer sb = new StringBuffer();
		if(data ==null || data==""|| data.isEmpty()) data="-";
		sb.append("<tr>"+NL);
		sb.append("\t<th>");
		sb.append(heading);
		sb.append("</th>"+NL);
		sb.append("\t<td>");
		sb.append(data);
		sb.append("</td>"+NL);
		sb.append("</tr>"+NL);
		return sb.toString();
	}
	
	public static String insTableRow(boolean isHeader, String... columns ){
		StringBuffer sb = new StringBuffer();
		sb.append("<tr>"+NL);
		if(isHeader){
			for(String col: columns){
				sb.append("\t<th>");
				if(col!=null && !col.isEmpty()){
					sb.append(col);
				}
				else{
					sb.append("-");
				}
				sb.append("</th>"+NL);
			}
		}
		else{
			for(String col: columns){
				sb.append("\t<td>");
				if(col!=null && !col.isEmpty()){
					sb.append(col);
				}
				else{
					sb.append("-");
				}
				sb.append("</td>"+NL);
			}
		}
		sb.append("</tr>"+NL);
		return sb.toString();
	}
	
	public static String insInlineImage(byte[] imageData){
		StringBuffer sb = new StringBuffer();
		sb.append("\t\t<img src=\"data:image/png;base64,");
		sb.append(DatatypeConverter.printBase64Binary(imageData));
		sb.append("\" />");
		return sb.toString();
	}
	
	public static String insFileImage(String directoryPath, String filename, byte[] imageData ){
		StringBuffer sb = new StringBuffer();
		try {
			FileOutputStream fos = new FileOutputStream(directoryPath
					+ File.separatorChar + filename
					+ ".png");
			fos.write(imageData);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace(); LogUtil.logError(e);
		}

		sb.append("\t\t<img src=\""+filename+".png"+"\" />");
		return sb.toString();
	}
	
	public static String returnDashIfNull(Object o){
		if(o==null || (o instanceof String || o==""))
		{
			return "-";
		} else {
			return o.toString();
		}
	}
	
	public static String returnDashIfStringNull(String o){
		String string = (String)o;
		if(string==null)
		{
			return "-";
		}
		else {
			return string.toString();
		}
	}
	
	public static String insTag(String tag, Text textToEnclose, boolean endWithLineBreak){
		if(textToEnclose!=null&& !textToEnclose.toPlainString().isEmpty()){
			return insTag(tag,textToEnclose.toPlainString(), endWithLineBreak);
		}
		return "";
	}
	
	public static String insTag(String tag, String textToEnclose, boolean endWithLineBreak){
		StringBuffer sb = new StringBuffer();
		if(textToEnclose!=null && !textToEnclose.isEmpty()){
			sb.append("<" + tag + ">" + NL + 
				"\t"+textToEnclose + NL +
				"</" + tag + ">");
			sb.append(NL);
			if(endWithLineBreak){
				sb.append(insBreak());
			}
			return sb.toString();
		}
		return "";
				
	}
	
	public static String insBreak(){
		return "<br/>";
	}
}