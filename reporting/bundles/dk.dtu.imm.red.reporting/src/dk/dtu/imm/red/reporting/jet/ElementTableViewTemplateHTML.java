package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import java.util.List;

public class ElementTableViewTemplateHTML {

  protected static String nl;
  public static synchronized ElementTableViewTemplateHTML create(String lineSeparator)
  {
    nl = lineSeparator;
    ElementTableViewTemplateHTML result = new ElementTableViewTemplateHTML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = " " + NL + "<h1>Chapter ";
  protected final String TEXT_2 = "</h1>" + NL;
  protected final String TEXT_3 = NL + "\t<table cellspacing=\"1\" class=\"tablesorter\">   " + NL + "\t<thead> ";
  protected final String TEXT_4 = "\t\t" + NL + "\t\t<tr>";
  protected final String TEXT_5 = "\t\t\t" + NL + "\t \t\t<th>";
  protected final String TEXT_6 = "</th>  ";
  protected final String TEXT_7 = "\t\t" + NL + "\t\t</tr> " + NL + "\t\t</thead> " + NL + "\t\t<tbody> \t";
  protected final String TEXT_8 = "\t" + NL + "\t\t" + NL + "\t\t<tr>";
  protected final String TEXT_9 = "\t" + NL + "\t\t\t\t<td>";
  protected final String TEXT_10 = "</td>  ";
  protected final String TEXT_11 = "\t" + NL + "\t\t</tr> ";
  protected final String TEXT_12 = NL + "\t</tbody>" + NL + "\t</table>\t";

	public String generate(Object argument,String chapterNumber, String chapterName)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(String.valueOf(chapterNumber) + " - " + chapterName );
    stringBuffer.append(TEXT_2);
    
 	List<Element> els = (List<Element>) argument;
 	
	if(els.size() > 0) {

    stringBuffer.append(TEXT_3);
    
		Element firstElement = els.get(0);

    stringBuffer.append(TEXT_4);
    		
		for(ElementColumnDefinition c : firstElement.getColumnRepresentation()) {
			String colName = c.getColumnName(); 

    stringBuffer.append(TEXT_5);
    stringBuffer.append(colName );
    stringBuffer.append(TEXT_6);
    
		}

    stringBuffer.append(TEXT_7);
    
		for(Element e : els) {

    stringBuffer.append(TEXT_8);
    
			for(ElementColumnDefinition c : firstElement.getColumnRepresentation()) { 
				CellHandler<Element> ch = (CellHandler<Element>) c.getCellHandler();
				Object colValue = ch.getAttributeValue(e) != null ? ch.getAttributeValue(e) : " - ";

    stringBuffer.append(TEXT_9);
    stringBuffer.append(colValue.toString() );
    stringBuffer.append(TEXT_10);
    
			}

    stringBuffer.append(TEXT_11);
    
		}

    stringBuffer.append(TEXT_12);
    
	} 

    return stringBuffer.toString();
  }
}