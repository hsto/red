package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenario;
import dk.dtu.imm.red.specificationelements.usecasescenario.UseCaseScenarioElement;
import dk.dtu.imm.red.specificationelements.usecasescenario.Action;
import dk.dtu.imm.red.specificationelements.usecasescenario.OperatorComponent;

import java.util.ArrayList;
import java.util.List;


public class ScenarioTemplateHTML {

	protected static String NL = ReportHelper.NL;

	public static synchronized ScenarioTemplateHTML create(String lineSeparator) {
		ScenarioTemplateHTML result = new ScenarioTemplateHTML();
		return result;
	}


	public String generate(Object argument, boolean inlineImages, String path) {
		final StringBuffer sb = new StringBuffer();
		UseCaseScenario scenario = (UseCaseScenario) argument;
		sb.append(ReportHelper.insBasicInfo(scenario));
		
		List<OperatorComponent> scenarioMap = new ArrayList<OperatorComponent>();

		scenarioMap.add(scenario.getStartOperator());
		List<OperatorComponent> operators = scenario.extractAllExtensionOperators();

		for (OperatorComponent operator : operators) {
			scenarioMap.add(operator);
		}

		for (OperatorComponent operator : scenarioMap) {

			String scenarioName = operator.equals(scenario.getStartOperator()) ? "Main Scenario"
					: "Extension to " + operator.getName();

			StringBuffer tbl = new StringBuffer();
			tbl.append(ReportHelper.insTableRow(true, "Name","Description","Guard","Type","Image"));
			for (UseCaseScenarioElement se : operator.getChildElements()) {
				if (se instanceof OperatorComponent) {
					tbl.append(ReportHelper.insTableRow(false,
							se.getName(),
							se.getDescription(),
							((OperatorComponent) se).getGuard(),
							((OperatorComponent) se).getType().getName(),
							"-"));
				} else {

					Action a = (Action) se;
					String imageURI = " - ";
					if (a.getPictureData() != null) {
						if (inlineImages) {
							imageURI = ReportHelper.insInlineImage(a.getPictureData());
						} else {

							imageURI = ReportHelper.insFileImage(path, a.getName()+".png", a.getPictureData());
							
						}
					}
					tbl.append(ReportHelper.insTableRow(false,
							se.getName(),
							se.getDescription(),
							"-",
							((Action) se).getType().getName(),
							imageURI));

				}

			}
			sb.append(ReportHelper.insTable(scenarioName, tbl.toString()));

		}

		return sb.toString();
	}
}