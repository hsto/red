package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.goal.*;

public class GoalTemplateHTML {
	protected static String NL = ReportHelper.NL;

	public static synchronized GoalTemplateHTML create(String lineSeparator) {
		GoalTemplateHTML result = new GoalTemplateHTML();
		return result;
	}


	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		Goal goal = (Goal) argument;
		sb.append(ReportHelper.insBasicInfo(goal));
		return sb.toString();
	}
}
