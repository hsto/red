package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;

public class RequirementTemplateHTML {

	protected static String nl;

	public static synchronized RequirementTemplateHTML create(
			String lineSeparator) {
		nl = lineSeparator;
		RequirementTemplateHTML result = new RequirementTemplateHTML();
		nl = null;
		return result;
	}

	public final String NL = nl == null ? (System.getProperties()
			.getProperty("line.separator")) : nl;
	protected final String TEXT_1 = "<h4> Requirement - ";
	protected final String TEXT_2 = " </h4>" + NL + "<style type=\"text/css\">"
			+ NL + "p{max-width:700px;}" + NL + "</style>" + NL + "<p>" + NL
			+ "ID: ";
	protected final String TEXT_3 = ", Type: ";
	protected final String TEXT_4 = ", Level: ";
	protected final String TEXT_5 = " <br />";
	protected final String TEXT_6 = NL + "Description: ";
	protected final String TEXT_7 = " <br />";
	protected final String TEXT_8 = NL;
	protected final String TEXT_9 = NL + "Elaboration: ";
	protected final String TEXT_10 = " <br />";
	protected final String TEXT_11 = NL;
	protected final String TEXT_12 = NL + "Remarks: ";
	protected final String TEXT_13 = " <br />";
	protected final String TEXT_14 = NL;
	protected final String TEXT_15 = NL + "<p><i>Acceptance tests for ";
	protected final String TEXT_16 = "</i>" + NL + "<table border=\"1\">" + NL
			+ "\t<tr>" + NL + "\t\t<td>Case ID</td>" + NL
			+ "\t\t<td>Precondition</td>" + NL + "\t\t<td>Action</td>" + NL
			+ "\t\t<td>Postcondition</td>" + NL + "\t</tr>";
	protected final String TEXT_17 = NL + "\t<tr>" + NL + "\t\t<td>";
	protected final String TEXT_18 = "</td>" + NL + "\t\t<td>";
	protected final String TEXT_19 = "</td>" + NL + "\t\t<td>";
	protected final String TEXT_20 = "</td>" + NL + "\t\t<td>";
	protected final String TEXT_21 = "</td>" + NL + "\t</tr>";
	protected final String TEXT_22 = NL + "</table>" + NL + "</p>";
	protected final String TEXT_23 = NL;
	protected final String TEXT_24 = NL + "<p><i>Requirement model:</i>" + NL
			+ "<p>";
	protected final String TEXT_25 = NL
			+ "\t\t\t\t<img src=\"data:image/png;base64,";
	protected final String TEXT_26 = "\" />";
	protected final String TEXT_27 = NL + "\t\t\t\t<img src=\"";
	protected final String TEXT_28 = "\" /> <br />";
	protected final String TEXT_29 = NL + "</p>" + NL + "</p>" + NL + "" + NL
			+ "" + NL + "</p>";

	public String generate(Object argument, boolean inlineImages, String path) {
		final StringBuffer sb = new StringBuffer();
		Requirement requirement = (Requirement) argument;
		sb.append(ReportHelper.insBasicInfo(requirement));

		if(requirement.getAbstractionLevel()!=null && !requirement.getAbstractionLevel().isEmpty()){
			sb.append(ReportHelper.insTag("p", "Level:"+requirement.getAbstractionLevel(), false));
		}

		if (requirement.getDetails() != null
				&& !requirement.getDetails().toString().isEmpty()) {
			sb.append(ReportHelper.insTag("p","<u>Elaboration:</u>\n"+ requirement.getDetails(), true));
		}
		
		//requirement.

		sb.append(TEXT_11);

		if (requirement.getRemarks() != null
				&& !requirement.getRemarks().toString().isEmpty()) {
			sb.append(ReportHelper.insTag("p", "<u>Remarks:</u>\n"+requirement.getRemarks(), true));
		}


//		if (requirement.getAcceptanceTests().size() > 0) {
//
//			sb.append(TEXT_15);
//			sb.append(requirement.getId());
//			sb.append(TEXT_16);
//
//			for (AcceptanceTest test : requirement.getAcceptanceTests()) {
//
//				sb.append(TEXT_17);
//				sb.append(test.getCaseID());
//
//			}

//			sb.append(TEXT_22);

//		}

		sb.append(TEXT_23);

//		if (requirement.getModelFragment() != null
//				&& requirement.getModelFragment().getFragmentDiagram() != null) {
//
//			sb.append(TEXT_24);
//
//			Diagram diagram = requirement.getModelFragment()
//					.getFragmentDiagram();
//			CopyToImageUtil imageUtil = new CopyToImageUtil();
//			byte[] fragmentBytes = new byte[480000];
//			try {
//				try {
//					fragmentBytes = imageUtil.copyToImageByteArray(diagram,
//							800, 600, ImageFileFormat.PNG,
//							new NullProgressMonitor(), null, true);
//				} catch (Exception e) {
//					e.printStackTrace(); LogUtil.logError(e);
//				}
//
//				String fragmentOut = DatatypeConverter
//						.printBase64Binary(fragmentBytes);
//
//				if (inlineImages) {
//
//					sb.append(TEXT_25);
//					sb.append(fragmentOut);
//					sb.append(TEXT_26);
//
//				} else {
//					try {
//						FileOutputStream fos = new FileOutputStream(path
//								+ File.separatorChar
//								+ requirement.getUniqueID() + ".png");
//						fos.write(fragmentBytes);
//						fos.close();
//					} catch (Exception e) {
//						e.printStackTrace(); LogUtil.logError(e);
//					}
//
//					sb.append(TEXT_27);
//					sb.append(requirement.getUniqueID() + ".png");
//					sb.append(TEXT_28);
//
//				}
//
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace(); LogUtil.logError(e);
//			}
//		}

//		sb.append(TEXT_29);
		return sb.toString();
	}
}