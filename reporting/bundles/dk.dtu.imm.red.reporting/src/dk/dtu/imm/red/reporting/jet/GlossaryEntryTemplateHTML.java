package dk.dtu.imm.red.reporting.jet;

import dk.dtu.imm.red.reporting.ReportHelper;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;

public class GlossaryEntryTemplateHTML {

	public static synchronized GlossaryEntryTemplateHTML create() {
		GlossaryEntryTemplateHTML result = new GlossaryEntryTemplateHTML();
		return result;
	}


	public String generate(Object argument) {
		final StringBuffer sb = new StringBuffer();
		GlossaryEntry entry = (GlossaryEntry) argument;

		String discussion="";
		if(entry!=null && entry.getDiscussion()!=null){
			discussion=entry.getDiscussion().toPlainString();
		}
//		String abbreviation = entry.getAbbreviations().toString();
//		abbreviation = abbreviation.substring(1, abbreviation.length()-1);
		String synonym = entry.getSynonyms().toString();
		synonym = synonym.substring(1, synonym.length()-1);

		sb.append(ReportHelper.insTableRow(false,entry.getLabel(),
				entry.getName(),synonym,entry.getDescription(),discussion));

		return sb.toString();
	}
}
