package dk.dtu.imm.red.visualstitching.operation;

import java.io.IOException;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import dk.dtu.imm.red.core.element.util.FileHelper;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class CreateStitchTempFileOperation extends AbstractOperation{

	public static final String stitchTempFileName = "StitchTempFile.red";

	public CreateStitchTempFileOperation() {
		super("Create Stitch File");
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor,info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		//Check for existence of WeaveTempFile in currentWorkspace, if none, create it
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		EList<dk.dtu.imm.red.core.file.File> fileList = workspace.getCurrentlyOpenedFiles();
		for(dk.dtu.imm.red.core.file.File file:fileList){
			if(file.getName().equals(stitchTempFileName)){
				//found tempFile, don't need to create it anymore
				return Status.OK_STATUS;
			}
		}

		Project parentProject = null;
		String filepath = "";
		EList<Project> p=workspace.getCurrentlyOpenedProjects();
		Project currentWorkspaceProject=null;
		if(p!=null && p.size()>0){
			//project should not be zero, it should have prompted the users about project not opened
			currentWorkspaceProject = p.get(0);
			parentProject = currentWorkspaceProject;
			// remove the file part of the path

			filepath = FileHelper.getFilePath(currentWorkspaceProject).replace(currentWorkspaceProject.getName(), "");
			filepath = filepath + stitchTempFileName;
		}


//		file.setName(weaveTempFileName);
//		String fileSep = System.getProperty("file.separator");

		java.io.File physicalFile = new java.io.File(filepath);

		try {
			physicalFile.createNewFile();

		} catch (IOException e) {
			e.printStackTrace(); LogUtil.logError(e);
		}


		ResourceSet resourceSet = parentProject.getResourceSet();
		URI uri = URI.createFileURI(filepath);
		Resource resource = resourceSet.createResource(uri);

		dk.dtu.imm.red.core.file.File file = FileFactory.eINSTANCE.createFile();
		file.setCreator(PreferenceUtil.getUserPreference_User());

		file.setParent(null);
		file.setLongDescription(TextFactory.eINSTANCE.createText(""));
		file.setName(stitchTempFileName);

		resource.getContents().add(file);
		file.save();

		workspace.addOpenedFile(file);

		parentProject.addToListOfFilesInProject(file);
		parentProject.save();

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

}
