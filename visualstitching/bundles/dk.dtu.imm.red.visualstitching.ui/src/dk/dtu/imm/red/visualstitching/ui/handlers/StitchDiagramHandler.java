package dk.dtu.imm.red.visualstitching.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.visualstitching.operation.CreateStitchTempFileOperation;
import dk.dtu.imm.red.visualstitching.ui.StitchingDialog;
import dk.dtu.imm.red.visualstitching.ui.wizards.StitchWizard;

public class StitchDiagramHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		StitchWizard stitchWizard = new StitchWizard();
		ISelection currentSelection = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection();

		WizardDialog selectionDialog = new WizardDialog(
				HandlerUtil.getActiveShell(event), stitchWizard);
		selectionDialog.create();

		selectionDialog.open();

		IStructuredSelection selection = null;
		if (HandlerUtil.getCurrentSelection(event) instanceof IStructuredSelection) {
			selection = (IStructuredSelection) HandlerUtil
					.getCurrentSelection(event);
		}

		IAdaptable info = new IAdaptable() {
			@Override
			public Object getAdapter(
					@SuppressWarnings("rawtypes") final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		if (selectionDialog.getReturnCode() == Dialog.OK) {

			try {
				IWorkbenchOperationSupport operationSupport = PlatformUI
						.getWorkbench().getOperationSupport();

				// create weave temp file into the project
				CreateStitchTempFileOperation createStitchFileOperation = new CreateStitchTempFileOperation();
//				createWeaveFileOperation.addContext(operationSupport
//						.getUndoContext());
				OperationHistoryFactory.getOperationHistory().execute(
						createStitchFileOperation, null, info);

					StitchingDialog stepTracingDialog = new StitchingDialog(
							Display.getDefault().getActiveShell(),
							stitchWizard.getDiagramList());
					stepTracingDialog.open();
//				}

			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogUtil.logError(e);
			}

		}

		return null;
	}

}
