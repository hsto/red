package dk.dtu.imm.red.visualstitching.ui;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.core.commands.operations.ObjectUndoContext;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.visualmodeling.ui.editors.DiagramExtension;
import dk.dtu.imm.red.visualmodeling.ui.util.ColorHelper;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchState;
import dk.dtu.imm.red.visualstitching.operation.StitchOperation;

public class StitchingDialog extends Dialog {

	// the models;
	private java.util.List<Diagram> inputDiagramList;

	private Diagram currentFirstInput;
	private Diagram currentSecondInput;

//	private Diagram prevFirstInput;
//	private Diagram prevSecondInput;

	private Diagram currentOutput;

	private Dialog thisDialog;
	private Text textMessage;
	private Button btnMoveToTop;
	private Button btnMoveUp;
	private Button btnMoveDown;
	private Button btnMoveToBottom;

	private Table tblDiagramList;
	private TableViewer tvDiagramList;

	private TableViewerColumn tvcStatus;
	private TableColumn tcStatus;

	private TableViewerColumn tvcDiagramName;
	private TableColumn tcDiagramName;

	private Button btnDo;
	private Button btnUndo;
	private Button btnTerminate;

	private static final String SINGLE_STEP = "Single Step";
	private static final String AUTOMATIC = "Automatic";
	private Group modeGroup;
	private Button btnSingleStep;
	private Button btnAutomatic;

	private Boolean DELAY_SAVE = true;
	private Button btnSetAsInput;

	private IOperationHistory operationHistory;
	private ObjectUndoContext undoContext;
	/**
	 * Create the dialog.
	 *
	 * @param parentShell
	 */
	public StitchingDialog(Shell parentShell,
			java.util.List<Diagram> inputDiagramList) {
		super(parentShell);

		this.inputDiagramList = inputDiagramList;
		this.undoContext = new ObjectUndoContext(this);
		operationHistory = OperationHistoryFactory.getOperationHistory();

		for(Diagram d : inputDiagramList){
			d.setStitchState(StitchState.INITIAL);
		}
		this.thisDialog = this;
		Window.setDefaultOrientation(SWT.RIGHT);
	}

	/**
	 * Create contents of the dialog.
	 *
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, false));

		Label lblInputDiagrams = new Label(container, SWT.NONE);
		lblInputDiagrams.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				false, false, 1, 1));
		lblInputDiagrams.setText("Diagram List");
		new Label(container, SWT.NONE);

		tvDiagramList = new TableViewer(container, SWT.BORDER
				| SWT.FULL_SELECTION | SWT.MULTI);
		tblDiagramList = tvDiagramList.getTable();
		tblDiagramList.setHeaderVisible(true);
		tblDiagramList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1));

		tvcStatus = new TableViewerColumn(tvDiagramList, SWT.NONE);
		tcStatus = tvcStatus.getColumn();
		tcStatus.setText("Status");
		tcStatus.setWidth(100);

		tvcDiagramName = new TableViewerColumn(tvDiagramList, SWT.NONE);
		tcDiagramName = tvcDiagramName.getColumn();
		tcDiagramName.setWidth(229);
		tcDiagramName.setText("Diagram");

		Composite moveButtonsComposite = new Composite(container, SWT.NONE);
		moveButtonsComposite.setLayout(new GridLayout(1, false));
		GridData gd_moveButtonsComposite = new GridData(SWT.LEFT, SWT.FILL,
				false, false, 1, 1);
		gd_moveButtonsComposite.widthHint = 36;
		moveButtonsComposite.setLayoutData(gd_moveButtonsComposite);

		btnMoveToTop = new Button(moveButtonsComposite, SWT.NONE);
		GridData gd_btnMoveToTop = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_btnMoveToTop.heightHint = 30;
		gd_btnMoveToTop.widthHint = 30;
		btnMoveToTop.setLayoutData(gd_btnMoveToTop);
		btnMoveToTop.setImage(ResourceManager.getPluginImage(
				"dk.dtu.imm.red.visualstitching.ui", "icons/MoveToTop.png"));
		btnMoveToTop.setToolTipText("Move to Top");

		btnMoveUp = new Button(moveButtonsComposite, SWT.NONE);
		GridData gd_btnMoveUp = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_btnMoveUp.heightHint = 30;
		gd_btnMoveUp.widthHint = 30;
		btnMoveUp.setLayoutData(gd_btnMoveUp);
		btnMoveUp.setImage(ResourceManager.getPluginImage(
				"dk.dtu.imm.red.visualstitching.ui", "icons/MoveUp.png"));
		btnMoveUp.setToolTipText("Move Up");

		btnMoveDown = new Button(moveButtonsComposite, SWT.NONE);
		GridData gd_btnMoveDown = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_btnMoveDown.heightHint = 30;
		gd_btnMoveDown.widthHint = 30;
		btnMoveDown.setLayoutData(gd_btnMoveDown);
		btnMoveDown.setImage(ResourceManager.getPluginImage(
				"dk.dtu.imm.red.visualstitchin.ui", "icons/MoveDown.png"));
		btnMoveDown.setToolTipText("Move Down");

		btnMoveToBottom = new Button(moveButtonsComposite, SWT.NONE);
		GridData gd_btnMoveToBottom = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_btnMoveToBottom.heightHint = 30;
		gd_btnMoveToBottom.widthHint = 30;
		btnMoveToBottom.setLayoutData(gd_btnMoveToBottom);
		btnMoveToBottom.setImage(ResourceManager.getPluginImage(
				"dk.dtu.imm.red.visualstitching.ui", "icons/MoveToBottom.png"));
		btnMoveToBottom.setToolTipText("Move To Bottom");

		btnSetAsInput = new Button(moveButtonsComposite, SWT.NONE);
		btnSetAsInput.setToolTipText("Mark selection as input");
		btnSetAsInput.setImage(ResourceManager.getPluginImage("dk.dtu.imm.red.visualdtitching.ui", "icons/SetAsInput.png"));
		GridData gd_btnSetAsInput = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnSetAsInput.widthHint = 30;
		gd_btnSetAsInput.heightHint = 30;
		btnSetAsInput.setLayoutData(gd_btnSetAsInput);
		btnMoveToBottom.setToolTipText("Set selection as next input");

		Composite stitchButtonsComposite = new Composite(container, SWT.NONE);
		stitchButtonsComposite.setLayout(new GridLayout(4, false));
		GridData gd_stitchButtonsComposite = new GridData(SWT.FILL, SWT.CENTER,
				false, false, 2, 1);
		gd_stitchButtonsComposite.heightHint = 55;
		stitchButtonsComposite.setLayoutData(gd_stitchButtonsComposite);

		btnDo = new Button(stitchButtonsComposite, SWT.NONE);
		GridData gd_btnDo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1,
				1);
		gd_btnDo.heightHint = 30;
		gd_btnDo.widthHint = 30;
		btnDo.setLayoutData(gd_btnDo);
		btnDo.setImage(ResourceManager.getPluginImage(
				"dk.dtu.imm.red.visualstitching.ui", "icons/Do.png"));
		btnDo.setToolTipText("Run stitching");

		btnUndo = new Button(stitchButtonsComposite, SWT.NONE);
		btnUndo.setImage(ResourceManager.getPluginImage(
				"dk.dtu.imm.red.visualstitching.ui", "icons/Undo.png"));
		GridData gd_btnUndo = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_btnUndo.heightHint = 30;
		gd_btnUndo.widthHint = 30;
		btnUndo.setLayoutData(gd_btnUndo);
		btnUndo.setToolTipText("Undo last output");

		btnTerminate = new Button(stitchButtonsComposite, SWT.NONE);
		GridData gd_btnTerminate = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_btnTerminate.heightHint = 28;
		gd_btnTerminate.widthHint = 30;
		btnTerminate.setLayoutData(gd_btnTerminate);
		btnTerminate.setImage(ResourceManager.getPluginImage(
				"dk.dtu.imm.red.visualstitching.ui", "icons/Stop.png"));
		btnTerminate.setToolTipText("Terminate Stitching");

		modeGroup = new Group(stitchButtonsComposite, SWT.NONE);
		modeGroup.setText("Stitch Mode");
		GridData gd_modeGroup = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_modeGroup.heightHint = 27;
		gd_modeGroup.widthHint = 205;
		modeGroup.setLayoutData(gd_modeGroup);

		btnSingleStep = new Button(modeGroup, SWT.RADIO);
		btnSingleStep.setSelection(true);
		btnSingleStep.setBounds(10, 19, 90, 16);
		btnSingleStep.setText("Single Step");

		btnAutomatic = new Button(modeGroup, SWT.RADIO);
		btnAutomatic.setBounds(111, 19, 90, 16);
		btnAutomatic.setText("Automatic");

		textMessage = new Text(container, SWT.MULTI | SWT.READ_ONLY | SWT.WRAP
				| SWT.BORDER);
		textMessage.setEditable(false);
		GridData gd_textMessage = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 2, 1);
		gd_textMessage.heightHint = 41;
		textMessage.setLayoutData(gd_textMessage);

		setButtonListeners();
		setTableViewerDoubleClickListener();
		setTableViewerProperties();
		loadInitialData();
		refreshTables();

		return container;
	}

	private void setButtonListeners() {
		btnMoveToTop.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) tvDiagramList
						.getSelection();
				if (selection == null)
					return;

				List<Object> selected = selection.toList();
				for(Object obj : selected){
					Diagram d = (Diagram)obj;
					inputDiagramList.remove(d);
				}
				for(int i=0;i<selected.size();i++){
					inputDiagramList.add(i, (Diagram) selected.get(i));
				}
				refreshTables();
			}
		});

		btnMoveUp.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) tvDiagramList
						.getSelection();
				if (selection == null)
					return;

//				System.out.println("NUM SELECTED = " + selection.size());
				Diagram firstSelection = (Diagram) selection.getFirstElement();
				int idxBeforeFirstSelection=-1;
				for(int i=0;i<inputDiagramList.size();i++){
					if(i+1 < inputDiagramList.size()){
						if(inputDiagramList.get(i+1) == firstSelection){
							idxBeforeFirstSelection = i;
//							System.out.println("indexOfDiagramBeforeFirstSelection:"+indexOfDiagramBeforeFirstSelection);
						}
					}
				}

				if (idxBeforeFirstSelection != -1) {
					Diagram toBeMoved = inputDiagramList.get(idxBeforeFirstSelection);
					inputDiagramList.remove(toBeMoved);
					refreshTables();
					inputDiagramList.add(idxBeforeFirstSelection+selection.size(),toBeMoved);
					refreshTables();
				}
			}
		});

		btnMoveDown.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) tvDiagramList
						.getSelection();
				if (selection == null)
					return;

//				System.out.println("NUM SELECTED = " + selection.size());
				List<Object> selections = selection.toList();
				Diagram lastSelection = (Diagram) selections.get(selections.size()-1);
				int idxAfterLastSelection=-1;
				for(int i=inputDiagramList.size();i>0;i--){
					if(i-1 >0){
						if(inputDiagramList.get(i-1) == lastSelection){
							idxAfterLastSelection = i;
//							System.out.println("indexOfDiagramBeforeFirstSelection:"+indexOfDiagramBeforeFirstSelection);
						}
					}
				}

				if (idxAfterLastSelection != -1 && idxAfterLastSelection!=inputDiagramList.size()) {
					Diagram toBeMoved = inputDiagramList.get(idxAfterLastSelection);
					inputDiagramList.remove(toBeMoved);
					refreshTables();
					inputDiagramList.add(idxAfterLastSelection-selection.size(),toBeMoved);
					refreshTables();
				}
			}
		});

		btnMoveToBottom.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) tvDiagramList
						.getSelection();
				if (selection == null)
					return;

				List<Object> selected = selection.toList();
				for(Object obj : selected){
					Diagram d = (Diagram)obj;
					inputDiagramList.remove(d);
				}
				for(int i=0;i<selected.size();i++){
					inputDiagramList.add(inputDiagramList.size(), (Diagram) selected.get(i));
				}
				refreshTables();
			}
		});


		btnSetAsInput.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				setAsInputClicked();
			}
		});

		btnDo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				stitchButtonClicked();

			}
		});

		btnUndo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				undoButtonClicked();
			}
		});

		btnTerminate.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				close();
			}
		});
	}

	private void setTableViewerDoubleClickListener() {
		IDoubleClickListener doubleClickListener = new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event
						.getSelection();
				Diagram d = (Diagram) selection.getFirstElement();
				DiagramExtension ext = new DiagramExtension();
				ext.openConcreteElement(d);

			}
		};
		tvDiagramList.addDoubleClickListener(doubleClickListener);
	}

	private void setTableViewerProperties() {

		ColumnLabelProvider stitchingStatusLabelProvider = new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Diagram)
					return ((Diagram) element).getStitchState().getLiteral();
				else
					return "-";
			}
		};
		tvcStatus.setLabelProvider(stitchingStatusLabelProvider);

		ColumnLabelProvider diagramLabelProvider = new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Diagram) {
					return ((Diagram) element).getLabel();
				} else
					return "";
			}

			@Override
			public Color getBackground(Object element) {
				// highlight yellow if next input
				// highlight red if error
				if (element instanceof Diagram) {
					Diagram d = (Diagram) element;
					if (d.getStitchState() == StitchState.ERROR) {
						return ColorHelper.red;
					} else if (d.getStitchState() == StitchState.MARKED_AS_INPUT) {
						return ColorHelper.yellow;
					}
					return null;
				}
				return null;
			}

			@Override
			public Color getForeground(Object element) {
				// set as blue text if it is intermediate weaving diagram
				if (element instanceof Diagram) {
					Diagram d = (Diagram) element;
					if (d.getVisualDiagram().isIsStitchOutput()) {
						return ColorHelper.blue;
					}
				}
				return null;
			}
		};
		tvcDiagramName.setLabelProvider(diagramLabelProvider);

	}

	private void setAsInputClicked(){
		IStructuredSelection selection = (IStructuredSelection) tvDiagramList
				.getSelection();
		if (selection.size()<2 )
			textMessage.setText("Not enough diagrams are selected. Please select two non-completed diagrams to set as input");

		Diagram firstDiagram=null;
		Diagram secondDiagram=null;
		for(Object object: selection.toList()){
			if(firstDiagram!=null && secondDiagram!=null)
				break;
			Diagram d = (Diagram) object;
			//find the first two diagram from the selection which is not with state completed
			if(d.getStitchState()!=StitchState.COMPLETED){
				if(firstDiagram==null)
					firstDiagram = d;
				if(secondDiagram==null && d!=firstDiagram)
					secondDiagram= d;
			}
		}
		if(firstDiagram!=null && secondDiagram!=null){
			//clear existing diagrams with status marked as input
			for(Diagram d : inputDiagramList){
				if(d.getStitchState() == StitchState.MARKED_AS_INPUT)
					d.setStitchState(StitchState.INITIAL);
			}
			//set two of the selections as input
			currentFirstInput = firstDiagram;
			currentSecondInput = secondDiagram;
			currentFirstInput.setStitchState(StitchState.MARKED_AS_INPUT);
			currentSecondInput.setStitchState(StitchState.MARKED_AS_INPUT);
			refreshTables();
		}
		else{
			textMessage.setText("Not enough diagrams are selected. Please select two non-completed diagrams to set as input");
		}

	}

	private void loadInitialData() {
		tvDiagramList.setContentProvider(new ArrayContentProvider());
		tvDiagramList.setInput(inputDiagramList);
		findNextInputs();
	}

	private void refreshTables() {
		tvDiagramList.refresh();

		if(checkCurrentOutputHasError()){
			btnSetAsInput.setEnabled(false);
		}
		else
			btnSetAsInput.setEnabled(true);
	}

	private void findNextInputs() {
		if(!findExistingFirstInput()){
			findFirstInput();
		}
		if(!findExistingSecondInput()){
			findSecondInput();
		}
//		System.out.println("CURRENT FIRST INPUT:" + currentFirstInput);
//		System.out.println("CURRENT SECOND INPUT:" + currentSecondInput);
	}

	private boolean findExistingFirstInput(){
		//find if there is already a diagram marked as input
		for (Diagram d : inputDiagramList) {
			if (d.getStitchState() == StitchState.MARKED_AS_INPUT) {
				currentFirstInput = d;
				return true;
			}
		}
		return false;
	}

	private boolean findExistingSecondInput(){
		//find if there is already second diagram marked as input
		for (Diagram d : inputDiagramList) {
			if (d.getStitchState() == StitchState.MARKED_AS_INPUT && d!=currentFirstInput) {
				currentSecondInput = d;
				return true;
			}
		}
		return false;
	}

	private boolean findFirstInput(){
		for (Diagram d : inputDiagramList) {
			//find the first diagram with state initial
			if (d.getStitchState() == StitchState.INITIAL) {
				currentFirstInput = d;
				d.setStitchState(StitchState.MARKED_AS_INPUT);
				return true;
			}
		}
		return false;
	}

	private void findSecondInput(){
		boolean isSequential = PreferenceUtil.getPreferenceString(
				PreferenceConstants.STITCH_PREF_STITCHING_SEQUENCE).equals(
				PreferenceConstants.STITCH_PREF_STITCHING_SEQUENCE_SEQUENTIAL);

		if(isSequential){
			for (int i = inputDiagramList.size() - 1; i >= 0; i--) {
				// search backward from end
				Diagram d = inputDiagramList.get(i);
				if (d.getStitchState() == StitchState.INITIAL && d.getVisualDiagram().isIsStitchOutput()) {
					currentSecondInput = d;
					d.setStitchState(StitchState.MARKED_AS_INPUT);
					return;
				}
			}
			//the previous loop does not find result, search all although it is not intermediate weave diagram
			for (Diagram d : inputDiagramList) {
				if (d.getStitchState() == StitchState.INITIAL
						&& d != currentFirstInput) {
					currentSecondInput = d;
					d.setStitchState(StitchState.MARKED_AS_INPUT);
					break;
				}
			}

		}
		else{
			//tree sequence, always find the next one after the first input
			for (Diagram d : inputDiagramList) {
				if (d.getStitchState() == StitchState.INITIAL
						&& d != currentFirstInput) {
					currentSecondInput = d;
					d.setStitchState(StitchState.MARKED_AS_INPUT);
					break;
				}
			}
		}


	}

	private void stitchButtonClicked() {
		if (btnAutomatic.getSelection()) {
			while (runSingleStitchOperation(DELAY_SAVE)) {
				;
			}
			// only save the diagram now, after all done
			for (File file : WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
					.getCurrentlyOpenedFiles()) {
				file.save();
			}
		} else {
			//step by step, just do the save
			runSingleStitchOperation(!DELAY_SAVE);
		}
	}

	private void undoButtonClicked() {
		try {
			if(operationHistory.canUndo(undoContext)){
				operationHistory.undo(undoContext, new NullProgressMonitor(), null);
//				System.out.println("OPERATION HISTORY : ");
//				for(IUndoableOperation op : operationHistory.getUndoHistory(undoContext)){
//					System.out.println(op.getLabel());
//				}

			textMessage.setText("Undo last output done");

			currentFirstInput.setStitchState(StitchState.INITIAL);
			currentSecondInput.setStitchState(StitchState.INITIAL);
			currentOutput =null;
			findNextInputs();
			refreshTables();

			}

		} catch (ExecutionException e) {
			e.printStackTrace();
			LogUtil.logError(e);
		}
	}

	private boolean runSingleStitchOperation(boolean delaySave) {
		// check if still has remaining diagram to weave
		if (!hasRemainingDiagramToStitch()) {
			textMessage.setText("No more input diagrams for stitching!");
			return false;
		}
		// check if has errors, if yes, delete the current output before reweave
		if (checkCurrentOutputHasError()) {
			inputDiagramList.remove(currentOutput);
			currentOutput.delete();
			currentOutput = null;
		}

		// weave the two diagrams
		if (currentFirstInput != null && currentSecondInput != null) {
			try {
				StitchOperation stitchOperation = new StitchOperation(inputDiagramList,
						currentFirstInput, currentSecondInput, delaySave);
				//set the undo context only within this dialog window
				stitchOperation.addContext(undoContext);

				operationHistory.execute(
						stitchOperation, new NullProgressMonitor(), null);

//				System.out.println("OPERATION HISTORY : ");
//				for(IUndoableOperation op : operationHistory.getUndoHistory(undoContext)){
//					System.out.println(op.getLabel());
//				}

				Diagram outputDiagram = stitchOperation.getOutputDiagram();
				currentOutput = outputDiagram;

				// add the output to the diagram list
				if (!stitchOperation.getMergeExec().isMergeSuccess()) {
					textMessage
							.setText("Stitch fail due to conflict! Please find the logs from the output of last diagram, fix the inputs and re-stitch again!");
					refreshTables();
					return false;
				} else {
					// mark the two input as completed
					textMessage.setText("Stitch success!");
					findNextInputs();
					refreshTables();
				}

			} catch (ExecutionException e) {
				e.printStackTrace();
				LogUtil.logError(e);
				return false;
			}

		}

		return true;
	}

	private boolean hasRemainingDiagramToStitch() {
		int numNotDoneDiagrams = 0;
		for (Diagram d : inputDiagramList) {
			if (d.getStitchState() != StitchState.COMPLETED) {
				numNotDoneDiagrams++;
			}
		}
		if (numNotDoneDiagrams < 2) {
			return false;
		}
		return true;
	}

	private boolean checkCurrentOutputHasError() {
		if (currentOutput != null
				&& currentOutput.getStitchState() == StitchState.ERROR)
			return true;
		return false;
	}

	private int getNumOfUnfinishedDiagram(){
		int numOfUnfinishedDiagrams=0;
		for(Diagram d : inputDiagramList){
			if(d.getStitchState()!=StitchState.COMPLETED){
				numOfUnfinishedDiagrams++;
			}
		}
		return numOfUnfinishedDiagrams;
	}

	/**
	 * Create contents of the button bar.
	 *
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(358, 648);
	}

	@Override
	protected void setShellStyle(int newShellStyle) {
		// make the dialog non modal
		super.setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
		setBlockOnOpen(false);
	}

	@Override
	protected void configureShell(Shell shell) {
		shell.setMinimumSize(new Point(132, 50));
		super.configureShell(shell);
		shell.setText("Stitching Dialog");
	}


	@Override
	public boolean close() {
		boolean returnValue = false;
		int numOfUnfinishedDiagram = getNumOfUnfinishedDiagram();
		if (numOfUnfinishedDiagram > 2) {
			MessageDialog md = new MessageDialog(
					Display.getCurrent().getActiveShell(),
					"Remaining unfinished diagrams",
					null,
					"You still have remaining diagrams to stitch. Are you sure you want to terminate?",
					MessageDialog.CONFIRM, new String[] { "OK", "Cancel" }, 1);
			int result = md.open();

			if (result == Dialog.OK) {
				returnValue = super.close();
			}
		} else {
			returnValue = super.close();
		}
		return returnValue;
	}
}
