package dk.dtu.imm.red.visualstitching.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.ui.widgets.ElementContentProvider;
import dk.dtu.imm.red.core.ui.widgets.ElementLabelProvider;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;

/**
 * Wizard page used for choosing which diagram should be included in a weave.
 * @author HenryLie
 *
 */
public class ChooseDiagramsPage extends WizardPage {

	protected CheckboxTreeViewer elementViewer;
	protected ElementContentProvider contentProvider;
	protected DiagramTypeContentProvider typeContentProvider;


	protected ElementLabelProvider labelProvider;

	public ChooseDiagramsPage() {
		super("Choose Diagrams");

		this.setTitle("Stitch diagrams to form intermediate stitch diagram");
		this.setDescription("Create an intermediate stitch diagram by selecting the diagrams to be stitched together.");

	}

	public List<Diagram> getSelectedDiagrams(){
		List<Diagram> diagramList = new ArrayList<Diagram>();

		Object[] checkedElements = elementViewer.getCheckedElements();
		for (Object element : checkedElements) {
			if (element instanceof Diagram) {
				diagramList.add((Diagram) element);
			}
		}

		return diagramList;
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(2, false));

		elementViewer = new CheckboxTreeViewer(container, SWT.BORDER);
		Tree tree = elementViewer.getTree();
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		contentProvider = new ElementContentProvider(false, new Class[] {Diagram.class}, null, null, false,false);
		typeContentProvider = new DiagramTypeContentProvider();

		labelProvider = new ElementLabelProvider();

		String groupingPreference = PreferenceUtil.getPreferenceString(PreferenceConstants.STITCH_PREF_SEPARATE_MULTISTITCH);
		if(groupingPreference.equals(PreferenceConstants.STITCH_PREF_SEPARATE_MULTISTITCH_BYDIAGRAMTYPE)){
			elementViewer.setContentProvider(typeContentProvider);
		}
		else{
			elementViewer.setContentProvider(contentProvider);
		}

		elementViewer.setLabelProvider(labelProvider);
		elementViewer.setInput(WorkspaceFactory.eINSTANCE.getWorkspaceInstance());


		// Checkstate listener for updating the check/partial check state of elements
				// for example, update the state of a parent when the state of a child is changed
				elementViewer.addCheckStateListener(new ICheckStateListener() {
					@Override
					public void checkStateChanged(CheckStateChangedEvent event) {
						if(event.getElement() instanceof Element){
							Element element = (Element) event.getElement();

							// If the selected element is a group, update the whole
							// subtree beneath this group (may be empty, but we don't care)
							if (element instanceof Group) {
								elementViewer.setGrayed(element, false);
								elementViewer.setChecked(element, event.getChecked());
								elementViewer.setSubtreeChecked(element, event.getChecked());
							}

							// If the element has a parent, correct the state of that parent
							if (element.getParent() != null) {
								Group parent = element.getParent();
								toggleParentEnableState(parent, event.getChecked());
							}
						}

					}

					/**
					 * Corrects the state of a parent element, when a child element state changes.
					 * @param element the parent element
					 * @param enabled the state
					 */
					private void toggleParentEnableState(Group element, boolean enabled) {

						// if the element is checked, and the child element was unchecked
						if (elementViewer.getChecked(element) && !enabled) {
							// then start by unchecking this element
							elementViewer.setChecked(element,  false);

							// If the element has a child which is still selected, then
							// set this element to be partially selected
							for (Element child : element.getContents()) {
								// we only care about groups (folders) and model fragments, ignore all else
								if ((child instanceof Group || child instanceof Diagram)
										&& elementViewer.getChecked(child)) {
									elementViewer.setChecked(element, true);
									elementViewer.setGrayed(element, true);
									break;
								}
							}
						} else if(enabled) {
							// if the element was unchecked, we need to see if all child elements
							// are checked, to see if this element should be partially or fully checked
							boolean allChildrenChecked = true;
							for (Element child : element.getContents()) {
								if ((child instanceof Group || child instanceof Diagram) && !elementViewer.getChecked(child)) {
									allChildrenChecked = false;
								}
							}
							if (allChildrenChecked) {
								elementViewer.setGrayed(element, false);
								elementViewer.setChecked(element, true);
							} else {
								elementViewer.setChecked(element, true);
								elementViewer.setGrayed(element, true);
							}
						}

						// now, update the state of this element's parent
						if (element.getParent() != null
								&& elementViewer.testFindItem(element.getParent()) != null) {
							toggleParentEnableState(element.getParent(), enabled);
						}
					}
				});

				setPageComplete(true);
				setControl(container);

				Button btnSelectAll = new Button(container, SWT.NONE);
				btnSelectAll.addListener(SWT.Selection, new Listener() {
					@Override
					public void handleEvent(Event event) {
						elementViewer.expandAll();
						elementViewer.setAllChecked(true);

					}
				});
				btnSelectAll.setText("Select All");

				Button btnDeselectAll = new Button(container, SWT.NONE);
				btnDeselectAll.addListener(SWT.Selection, new Listener() {
					@Override
					public void handleEvent(Event event) {
						elementViewer.expandAll();
						elementViewer.setAllChecked(false);
					}
				});
				btnDeselectAll.setText("Deselect All");
			}
}
