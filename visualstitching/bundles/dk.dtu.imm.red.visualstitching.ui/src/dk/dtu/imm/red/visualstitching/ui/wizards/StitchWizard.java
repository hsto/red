package dk.dtu.imm.red.visualstitching.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.wizards.BaseWizard;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;

public class StitchWizard extends BaseWizard {

	protected List<Element> selectedElements;
	protected ChooseDiagramsPage chooseDiagramsPage;
	private List<Diagram> diagramList;

	public StitchWizard() {

		selectedElements = new ArrayList<Element>();
		chooseDiagramsPage = new ChooseDiagramsPage();
	}

	@Override
	public void addPages() {
		addPage(chooseDiagramsPage);
	}


	@Override
	public boolean performFinish() {
		diagramList = chooseDiagramsPage.getSelectedDiagrams();
		if (isDiagramListOfSameType()) {
			return true;
		} else {
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Invalid selection", "Selected diagrams have to be of the same type!");
			return false;
		}
	}

	public List<Diagram> getDiagramList(){
		return diagramList;
	}
	
	private boolean isDiagramListOfSameType() { // Issue #228
		if (!diagramList.isEmpty()) {
			DiagramType type = diagramList.get(0).getVisualDiagram().getDiagramType();
			for (Diagram diagram : diagramList) {
				if (!diagram.getVisualDiagram().getDiagramType().equals(type)) {
					return false;
				}
			}
			
		}
		return true;
	}

}
