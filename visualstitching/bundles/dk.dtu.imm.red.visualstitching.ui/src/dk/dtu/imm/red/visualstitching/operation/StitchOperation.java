package dk.dtu.imm.red.visualstitching.operation;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchState;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.impl.visualmodelFactoryImpl;
import dk.dtu.imm.red.visualstitch.impl.MergeElementImpl;
import dk.dtu.imm.red.visualstitch.processor.MergeExecutor;
import dk.dtu.imm.red.visualstitch.processor.MergeLogger;

public class StitchOperation extends AbstractOperation{

	private List<Diagram> inputDiagramList;
	MergeLogger mergeLogger = MergeLogger.instance;
	Diagram diagram1;
	Diagram diagram2;
	Diagram outputDiagram;
	MergeExecutor mergeExec;
	boolean deferredSave=false;


	public StitchOperation(List<Diagram> inputDiagramList, Diagram diagram1, Diagram diagram2, boolean isDeferredSave) {
		super("Stitch operation");
		this.inputDiagramList = inputDiagramList;
		this.diagram1 = diagram1;
		this.diagram2 = diagram2;
		this.deferredSave = isDeferredSave;
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor,info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(diagram1 ==null || diagram2 == null){
			return Status.CANCEL_STATUS;
		}
		System.out.println("START TIME:" + System.currentTimeMillis());
		outputDiagram =  stitchDiagram(diagram1,diagram2);
		System.out.println("END TIME:" + System.currentTimeMillis());

		inputDiagramList.add(inputDiagramList.size(), outputDiagram);

		if(this.getMergeExec().isMergeSuccess()){
			diagram1.setStitchState(StitchState.COMPLETED);
			diagram2.setStitchState(StitchState.COMPLETED);
		}
		else{
			outputDiagram.setStitchState(StitchState.ERROR);
		}

//		if(!mergeExec.isMergeSuccess()){
//			MessageDialog.openError(null, "Error", "A merge conflict has occured. Check the merge log of the last merged element for details");
//		}

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
//		System.out.println("RUNNING UNDO");
//		System.out.println("SETTING " + diagram1.getLabel() + " back to initial");
//		System.out.println("SETTING " + diagram2.getLabel() + " back to initial");
//		System.out.println("DELETING " + outputDiagram.getLabel());
		if(diagram1!=null)
			diagram1.setStitchState(StitchState.INITIAL);
		if(diagram2!=null)
			diagram2.setStitchState(StitchState.INITIAL);
		if(outputDiagram!=null){
			inputDiagramList.remove(outputDiagram);
			outputDiagram.delete();
		}
		return Status.OK_STATUS;
	}

	public Diagram getOutputDiagram(){
		return outputDiagram;
	}


	private Diagram stitchDiagram(Diagram d1, Diagram d2) {

		Diagram outputDiagram = visualmodelFactoryImpl.eINSTANCE.createDiagram();
		VisualDiagram firstVisualDiagram = d1.getVisualDiagram();
		VisualDiagram secondVisualDiagram = d2.getVisualDiagram();

		//create the output diagram specification element
		outputDiagram.setLabel("("+d1.getLabel() + "+" +d2.getLabel()+")");
		outputDiagram.setName("Stitch");
		outputDiagram.setDescription("Auto merged from " + d2.getLabel() + " and " + d1.getLabel());
		VisualDiagram outputVisualDiagram = visualmodelFactoryImpl.eINSTANCE.createVisualDiagram();
		// Issue #228 both diagrams have the same type
		outputVisualDiagram.setDiagramType(firstVisualDiagram.getDiagramType());
		outputVisualDiagram.setIsStitchOutput(true);
		outputDiagram.setVisualDiagram(outputVisualDiagram);
		Dimension diagramSize = new Dimension(0, 0);
		outputDiagram.getVisualDiagram().setBounds(diagramSize);

		//actual merge algorithm
		mergeExec = new MergeExecutor(firstVisualDiagram, secondVisualDiagram);
		outputVisualDiagram = mergeExec.executeMerge();

		if(outputVisualDiagram!=null){
			//outputVisualDiagram.setDiagramType(DiagramType.STITCH);
			// Issue #228 both diagrams have the same type
			outputVisualDiagram.setDiagramType(firstVisualDiagram.getDiagramType());
			outputVisualDiagram.setIsStitchOutput(true);
			outputDiagram.setVisualDiagram(outputVisualDiagram);
		}

		//saving the output diagram specification element
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		EList<dk.dtu.imm.red.core.file.File> fileList = workspace.getCurrentlyOpenedFiles();
		for(dk.dtu.imm.red.core.file.File file : fileList){
			if(file.getName().equals(CreateStitchTempFileOperation.stitchTempFileName)){
				outputDiagram.setParent(file);
			}
		}


		//add source relationship traceability to the diagrams
		outputDiagram.setCreator(PreferenceUtil.getUserPreference_User());
//		outputDiagram.setLifecycleStatus(ElementLifecycle.AUTO_GENERATED);

//		SourceRelationship relationshipSource1 = RelationshipFactory
//				.eINSTANCE.createSourceRelationship();
//		relationshipSource1.setFromElement(outputDiagram);
//		relationshipSource1.setToElement(d1);
//
//		SourceRelationship relationshipSource2 = RelationshipFactory
//				.eINSTANCE.createSourceRelationship();
//		relationshipSource2.setFromElement(outputDiagram);
//		relationshipSource2.setToElement(d2);

		outputDiagram.setMergeLog(mergeLogger.getMergeLog());

		if(!deferredSave){
			outputDiagram.save();

			//need to save d1 and d2 as well to get two way relationship
			d1.save();
			d2.save();
		}


		mergeLogger.flushMergeLog();


		//reset for next weave operation
		MergeElementImpl.resetIDRunningNumber();

		return outputDiagram;
	}

	public MergeExecutor getMergeExec() {
		return mergeExec;
	}

}

