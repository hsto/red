package dk.dtu.imm.red.visualstitching.ui.wizards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.DiagramType;

public class DiagramTypeContentProvider implements ITreeContentProvider {

	//create a partition of various types of diagrams by a virtual folder
	HashMap<Folder, List<Diagram>> diagramMap = new LinkedHashMap<Folder,List<Diagram>>();
	HashMap<String, Folder> diagramTypeStringToFolder = new HashMap<String,Folder>();

	public DiagramTypeContentProvider() {
		List<DiagramType> availableTypes = DiagramType.VALUES;
		for(DiagramType type : availableTypes){
			Folder diagramTypeFolder = FolderFactory.eINSTANCE.createFolder();
			diagramTypeFolder.setName(type.getName());
			diagramMap.put(diagramTypeFolder, new ArrayList<Diagram>());
			diagramTypeStringToFolder.put(type.getName(), diagramTypeFolder);
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputChanged(final Viewer viewer, final Object oldInput,
			final Object newInput) {
	}

	@Override
	public Object[] getElements(final Object inputElement) {
		if (inputElement instanceof Workspace) {
			for(File file : ((Workspace)inputElement).getCurrentlyOpenedFiles()){
				for(Element content: file.getContents()){
					recursiveFindAllDiagrams(content);
				}
			}
		}
		Set<Folder> diagramTypeSet = diagramMap.keySet();
		Folder[] diagramTypeArr = diagramTypeSet.toArray(new Folder[diagramTypeSet.size()]);
		return diagramTypeArr;
	}

	private void recursiveFindAllDiagrams(Element element){
		if(element instanceof Diagram){
			Diagram diagram = ((Diagram)element);
			String diagramType = diagram.getVisualDiagram().getDiagramType().getName();
			Folder folderKey = diagramTypeStringToFolder.get(diagramType);
			List<Diagram> partitionToInsert = diagramMap.get(folderKey);
			partitionToInsert.add(diagram);
		}
		else if(element instanceof Folder){
			Folder folder = (Folder) element;
			for(Element content: folder.getContents()){
				recursiveFindAllDiagrams(content);
			}
		}
	}

	@Override
	public Object[] getChildren(final Object parentElement) {
		List<Element> children = new ArrayList<Element>();
		if(parentElement instanceof Folder){
			List<Diagram> diagramList = diagramMap.get((Folder)parentElement);
			return diagramList.toArray(new Diagram[diagramList.size()]);
		}

		return children.toArray();

	}

	@Override
	public Object getParent(final Object element) {
		if (element instanceof Diagram) {
			for(Entry<Folder,List<Diagram>> entry : diagramMap.entrySet()){
				List<Diagram> diagramList = entry.getValue();
				if(diagramList.contains(element)){
					return entry.getKey();
				}
			}

		}
		return null;
	}

	@Override
	public boolean hasChildren(final Object element) {
		if(element instanceof Folder && diagramMap.get(element).size()>0){
			return true;
		}
		return false;
	}

}
