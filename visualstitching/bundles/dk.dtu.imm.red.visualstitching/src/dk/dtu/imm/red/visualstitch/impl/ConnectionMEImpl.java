/**
 */
package dk.dtu.imm.red.visualstitch.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import dk.dtu.imm.red.visualstitch.ConnectionME;
import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.VisualstitchPackage;
import dk.dtu.imm.red.visualstitch.processor.MatchKB;
import dk.dtu.imm.red.visualstitch.processor.MergeConflictException;
import dk.dtu.imm.red.visualstitch.processor.MergeUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection ME</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.ConnectionMEImpl#getConnEnd1 <em>Conn End1</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.ConnectionMEImpl#getConnEnd2 <em>Conn End2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConnectionMEImpl extends MergeElementImpl implements ConnectionME {
	/**
	 * The cached value of the '{@link #getConnEnd1() <em>Conn End1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnEnd1()
	 * @generated
	 * @ordered
	 */
	protected MergeElement connEnd1;

	/**
	 * The cached value of the '{@link #getConnEnd2() <em>Conn End2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnEnd2()
	 * @generated
	 * @ordered
	 */
	protected MergeElement connEnd2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected ConnectionMEImpl() {
		super();
		getConnEnd1();
		getConnEnd2();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualstitchPackage.Literals.CONNECTION_ME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement getConnEnd1() {
		if (connEnd1 != null && connEnd1.eIsProxy()) {
			InternalEObject oldConnEnd1 = (InternalEObject)connEnd1;
			connEnd1 = (MergeElement)eResolveProxy(oldConnEnd1);
			if (connEnd1 != oldConnEnd1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisualstitchPackage.CONNECTION_ME__CONN_END1, oldConnEnd1, connEnd1));
			}
		}
		return connEnd1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement basicGetConnEnd1() {
		return connEnd1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnEnd1(MergeElement newConnEnd1) {
		MergeElement oldConnEnd1 = connEnd1;
		connEnd1 = newConnEnd1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.CONNECTION_ME__CONN_END1, oldConnEnd1, connEnd1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement getConnEnd2() {
		if (connEnd2 != null && connEnd2.eIsProxy()) {
			InternalEObject oldConnEnd2 = (InternalEObject)connEnd2;
			connEnd2 = (MergeElement)eResolveProxy(oldConnEnd2);
			if (connEnd2 != oldConnEnd2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisualstitchPackage.CONNECTION_ME__CONN_END2, oldConnEnd2, connEnd2));
			}
		}
		return connEnd2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement basicGetConnEnd2() {
		return connEnd2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnEnd2(MergeElement newConnEnd2) {
		MergeElement oldConnEnd2 = connEnd2;
		connEnd2 = newConnEnd2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.CONNECTION_ME__CONN_END2, oldConnEnd2, connEnd2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualstitchPackage.CONNECTION_ME__CONN_END1:
				if (resolve) return getConnEnd1();
				return basicGetConnEnd1();
			case VisualstitchPackage.CONNECTION_ME__CONN_END2:
				if (resolve) return getConnEnd2();
				return basicGetConnEnd2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualstitchPackage.CONNECTION_ME__CONN_END1:
				setConnEnd1((MergeElement)newValue);
				return;
			case VisualstitchPackage.CONNECTION_ME__CONN_END2:
				setConnEnd2((MergeElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.CONNECTION_ME__CONN_END1:
				setConnEnd1((MergeElement)null);
				return;
			case VisualstitchPackage.CONNECTION_ME__CONN_END2:
				setConnEnd2((MergeElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.CONNECTION_ME__CONN_END1:
				return connEnd1 != null;
			case VisualstitchPackage.CONNECTION_ME__CONN_END2:
				return connEnd2 != null;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(", connEnd1: ");
		result.append((getConnEnd1() != null ? getConnEnd1().getId() : ""));
		result.append(", connEnd2: ");
		result.append((getConnEnd2() != null ? getConnEnd2().getId() : ""));
		return super.toString().replaceAll("MergeElement", "ConnectionME") + result.toString();
	}

	@Override
	public MatchResult matchValue(MergeElement m2, MatchKB matchKB) throws MergeConflictException{
		//can match by name if both name is empty
		MatchResult canMatchByName = MergeUtil.canMatchByName(this, m2,MatchResult.TRUE);
		MatchResult canMatchByCustomAttr = MergeUtil.canMatchByCustomAttr(this, m2);

		return threeValAND(canMatchByName,canMatchByCustomAttr);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult matchStructure(MergeElement m2, MatchKB matchKB) throws MergeConflictException{

		MergeElement m1connEnd1 = this.getConnEnd1();
		MergeElement m1connEnd2 = this.getConnEnd2();
		MergeElement m2ConnEnd1 = ((ConnectionME)m2).getConnEnd1();
		MergeElement m2ConnEnd2 = ((ConnectionME)m2).getConnEnd2();


		return threeValOR(
				threeValAND(m1connEnd1.equals(m2ConnEnd1,matchKB),
							m1connEnd2.equals(m2ConnEnd2,matchKB)),
				threeValAND(m1connEnd1.equals(m2ConnEnd2,matchKB),
							m1connEnd2.equals(m2ConnEnd1,matchKB))
							);
	}

} //ConnectionMEImpl
