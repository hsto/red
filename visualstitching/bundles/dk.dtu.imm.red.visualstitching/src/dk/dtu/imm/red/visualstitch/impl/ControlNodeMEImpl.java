/**
 */
package dk.dtu.imm.red.visualstitch.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import dk.dtu.imm.red.visualstitch.ControlNodeME;
import dk.dtu.imm.red.visualstitch.MatchMode;
import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.VisualstitchPackage;
import dk.dtu.imm.red.visualstitch.processor.MatchKB;
import dk.dtu.imm.red.visualstitch.processor.MergeConflictException;
import dk.dtu.imm.red.visualstitch.processor.MergeLogger;
import dk.dtu.imm.red.visualstitch.processor.MergeUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Node ME</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.ControlNodeMEImpl#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.ControlNodeMEImpl#getOutgoing <em>Outgoing</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ControlNodeMEImpl extends MergeElementImpl implements ControlNodeME {
	/**
	 * The cached value of the '{@link #getIncoming() <em>Incoming</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncoming()
	 * @generated
	 * @ordered
	 */
	protected EList<MergeElement> incoming;

	/**
	 * The cached value of the '{@link #getOutgoing() <em>Outgoing</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoing()
	 * @generated
	 * @ordered
	 */
	protected EList<MergeElement> outgoing;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected ControlNodeMEImpl() {
		super();
		getIncoming();
		getOutgoing();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualstitchPackage.Literals.CONTROL_NODE_ME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MergeElement> getIncoming() {
		if (incoming == null) {
			incoming = new EObjectResolvingEList<MergeElement>(MergeElement.class, this, VisualstitchPackage.CONTROL_NODE_ME__INCOMING);
		}
		return incoming;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MergeElement> getOutgoing() {
		if (outgoing == null) {
			outgoing = new EObjectResolvingEList<MergeElement>(MergeElement.class, this, VisualstitchPackage.CONTROL_NODE_ME__OUTGOING);
		}
		return outgoing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualstitchPackage.CONTROL_NODE_ME__INCOMING:
				return getIncoming();
			case VisualstitchPackage.CONTROL_NODE_ME__OUTGOING:
				return getOutgoing();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualstitchPackage.CONTROL_NODE_ME__INCOMING:
				getIncoming().clear();
				getIncoming().addAll((Collection<? extends MergeElement>)newValue);
				return;
			case VisualstitchPackage.CONTROL_NODE_ME__OUTGOING:
				getOutgoing().clear();
				getOutgoing().addAll((Collection<? extends MergeElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.CONTROL_NODE_ME__INCOMING:
				getIncoming().clear();
				return;
			case VisualstitchPackage.CONTROL_NODE_ME__OUTGOING:
				getOutgoing().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.CONTROL_NODE_ME__INCOMING:
				return incoming != null && !incoming.isEmpty();
			case VisualstitchPackage.CONTROL_NODE_ME__OUTGOING:
				return outgoing != null && !outgoing.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(", incoming:[");
		Iterator<MergeElement> iter = incoming.iterator();
		while (iter.hasNext()) {
			result.append(iter.next().getId());
			if (iter.hasNext()) {
				result.append(",");
			}
		}
		result.append("]");
		result.append(", outgoing:[");
		iter = outgoing.iterator();
		while (iter.hasNext()) {
			result.append(iter.next().getId());
			if (iter.hasNext()) {
				result.append(",");
			}
		}
		result.append("]");

		return super.toString().replaceAll("MergeElement", "ControlNodeME") + result.toString();
	}

	/**
	 * @generated NOT
	 */
	MergeLogger mergeLogger = MergeLogger.instance;

	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult match(MergeElement m2, MatchKB matchKB) throws MergeConflictException{
		MatchResult matchStructure = this.matchStructure(m2, matchKB);
		MatchResult matchValue = this.matchValue(m2, matchKB);

		MatchResult finalResult;
		//use three valued OR instead of AND
//		if(matchValue==MatchResult.FALSE)
//			finalResult = MatchResult.FALSE;
//		else
//			finalResult = threeValOR(matchStructure,matchValue);

		finalResult = threeValAND(matchStructure,matchValue);

		//print the comparison result
		String name1;
		String name2;
		if(this.getInputVisualElement()!=null && this.getInputVisualElement().size()>0){
			name1 = this.getInputVisualElement().get(0).getName();
			name2 = m2.getInputVisualElement().get(0).getName();
		}
		else{
			name1 = this.getInputVisualConnection().get(0).getName();
			name2 = m2.getInputVisualConnection().get(0).getName();
		}

		if(finalResult==MatchResult.TRUE){
			mergeLogger.append(MergeLogger.INFO,"FOUND A MATCH between " + MergeUtil.fixedLengthString(this.getType(), 12)
					+ " "+this.getId() + "("+name1+")" +" and " + m2.getId() + "("+name2+")");
		}
		else if(matchValue==MatchResult.FALSE || matchValue==MatchResult.UNKNOWN){
			mergeLogger.append(MergeLogger.INFO,"Comparison does not match between " + MergeUtil.fixedLengthString(this.getType(), 12)
					+ " "+this.getId() + "("+name1+")" +" and " + m2.getId() + "("+name2+")"+ " due to matchValue=" + matchValue);
		}
		else{
			mergeLogger.append(MergeLogger.INFO,"Comparison does not match between " + MergeUtil.fixedLengthString(this.getType(), 12)
					+ " "+this.getId() + "("+name1+")" +" and " + m2.getId() + "("+name2+")"+ " due to matchStructure=" + matchStructure);
		}


		return finalResult;

	}


	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult matchValue(MergeElement m2, MatchKB matchKB) throws MergeConflictException{
		//can not match by name if both name is empty
		MatchResult canMatchByName = MergeUtil.canMatchByName(this, m2,MatchResult.TRUE);
		MatchResult canMatchByCustomAttr = MergeUtil.canMatchByCustomAttr(this, m2);

		return threeValAND(canMatchByName,canMatchByCustomAttr);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult matchStructure(MergeElement m2, MatchKB matchKB) throws MergeConflictException {
		MatchResult incomingEqual = matchSet(this.getIncoming(), ((ControlNodeME)m2).getIncoming(),
				matchKB, MatchMode.AT_LEAST_ONE);
		MatchResult outgoingEqual  = matchSet(this.getOutgoing(), ((ControlNodeME)m2).getOutgoing(),
				matchKB, MatchMode.AT_LEAST_ONE);

		return threeValRelaxedAND(incomingEqual,outgoingEqual);

	}


	/**
	 * @generated NOT
	 */
	protected MatchResult matchSet(Collection<MergeElement> set1, Collection<MergeElement> set2,
			MatchKB matchKB, MatchMode matchMode){
		if (set1.size() == 0 && set2.size() == 0) {
			//no elements
			return MatchResult.TRUE;
		}

		boolean matchAtLeastOne = (matchMode == MatchMode.AT_LEAST_ONE);
		if (!matchAtLeastOne) {
			// if match all the number of elements must be same
			if (set1.size() != set2.size()) {
				return MatchResult.FALSE;
			}
		}

		Set<MergeElement> set1Copy = new HashSet<MergeElement>();
		Set<MergeElement> set2Copy = new HashSet<MergeElement>();
		set1Copy.addAll(set1);
		set2Copy.addAll(set2);

		// check set, if any match, remove from both set
		Iterator<MergeElement> set1CopyIter = set1
				.iterator();
		while (set1CopyIter.hasNext()) {
			MergeElement set1Element = set1CopyIter.next();
			Iterator<MergeElement> set2CopyIter = set2Copy
					.iterator();
			while (set2CopyIter.hasNext()) {
				MergeElement set2Element = set2CopyIter.next();
				if (set1Element.equals(set2Element, matchKB)==MatchResult.TRUE) {
					if (matchAtLeastOne) {
						//for match at least one, just return now after found one match
						set1Copy.clear();
						set2Copy.clear();
						return MatchResult.TRUE;
					} else {
						set1CopyIter.remove();
						set2CopyIter.remove();
					}
				}
			}
		}

		// check remaining, it must be empty
		int inRemaining1 = set1Copy.size();
		int inRemaining2 = set2Copy.size();

		set1Copy.clear();
		set2Copy.clear();

		if(!matchAtLeastOne){
			if(inRemaining1==inRemaining2 && inRemaining1==0){
				return MatchResult.TRUE;
			}
		}
		return MatchResult.UNKNOWN;
	}

} //ControlNodeMEImpl
