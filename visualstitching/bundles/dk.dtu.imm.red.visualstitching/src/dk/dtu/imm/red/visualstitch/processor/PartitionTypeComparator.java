package dk.dtu.imm.red.visualstitch.processor;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class PartitionTypeComparator implements Comparator<String> {

	public static final Map<String,Integer> partitionTypeToPriorityMap = new HashMap<String,Integer>();
	public static int priorityValue=1;

	public PartitionTypeComparator() {
		if(partitionTypeToPriorityMap.size()==0){
			initializePartitionTypeToPriorityMap();
		}
	}

	private void initializePartitionTypeToPriorityMap(){
		//initialize the priority, especially for each type that is a container
		//and needs to be processed for merge first
		partitionTypeToPriorityMap.put(MergeElementType.PACKAGE, new Integer(priorityValue++));
		partitionTypeToPriorityMap.put(MergeElementType.SYSTEM_BOUNDARY, new Integer(priorityValue++));
		partitionTypeToPriorityMap.put(MergeElementType.SYSTEM, new Integer(priorityValue++));
		partitionTypeToPriorityMap.put(MergeElementType.STATE, new Integer(priorityValue++));
		partitionTypeToPriorityMap.put(MergeElementType.CLASS, new Integer(priorityValue++));
		partitionTypeToPriorityMap.put(MergeElementType.ENUM, new Integer(priorityValue++));

		partitionTypeToPriorityMap.put(MergeElementType.ACTION, new Integer(priorityValue++));
		partitionTypeToPriorityMap.put(MergeElementType.STATE, new Integer(priorityValue++));

		partitionTypeToPriorityMap.put(MergeElementType.DECISION_MERGE_NODE, new Integer(priorityValue++));
		partitionTypeToPriorityMap.put(MergeElementType.DECISION_MERGE_STATE, new Integer(priorityValue++));
		partitionTypeToPriorityMap.put(MergeElementType.FORK_JOIN_NODE, new Integer(priorityValue++));
		partitionTypeToPriorityMap.put(MergeElementType.FORK_JOIN_STATE, new Integer(priorityValue++));
		//all the other types will be considered lower priority
	}

	@Override
	public int compare(String partitionType1, String partitionType2) {
		int priority1 = 1000;
		int priority2 = 1000;
		if(partitionTypeToPriorityMap.containsKey(partitionType1)){
			priority1 = partitionTypeToPriorityMap.get(partitionType1);
		}
		if(partitionTypeToPriorityMap.containsKey(partitionType2)){
			priority2 = partitionTypeToPriorityMap.get(partitionType2);
		}
		if(priority1!=priority2){
			return priority1-priority2;
		}
		else{
			//same priority, sort by alphabetical order because treemap does
			//not allow for same value in comparator
			return partitionType1.compareTo(partitionType2);
		}

	}


}
