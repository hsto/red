/**
 */
package dk.dtu.imm.red.visualstitch.HashMapEntry;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.HashMapEntryFactory
 * @model kind="package"
 * @generated
 */
public interface HashMapEntryPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "HashMapEntry";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.HashMapEntry";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "HashMapEntry";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HashMapEntryPackage eINSTANCE = dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToStringMapImpl <em>String To String Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToStringMapImpl
	 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl#getStringToStringMap()
	 * @generated
	 */
	int STRING_TO_STRING_MAP = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>String To String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualElementToStringMapImpl <em>IVisual Element To String Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualElementToStringMapImpl
	 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl#getIVisualElementToStringMap()
	 * @generated
	 */
	int IVISUAL_ELEMENT_TO_STRING_MAP = 1;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT_TO_STRING_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT_TO_STRING_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>IVisual Element To String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT_TO_STRING_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>IVisual Element To String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_ELEMENT_TO_STRING_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualConnectionToStringMapImpl <em>IVisual Connection To String Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualConnectionToStringMapImpl
	 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl#getIVisualConnectionToStringMap()
	 * @generated
	 */
	int IVISUAL_CONNECTION_TO_STRING_MAP = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION_TO_STRING_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION_TO_STRING_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>IVisual Connection To String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION_TO_STRING_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>IVisual Connection To String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVISUAL_CONNECTION_TO_STRING_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToMergeElementMapImpl <em>String To Merge Element Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToMergeElementMapImpl
	 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl#getStringToMergeElementMap()
	 * @generated
	 */
	int STRING_TO_MERGE_ELEMENT_MAP = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_MERGE_ELEMENT_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_MERGE_ELEMENT_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To Merge Element Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_MERGE_ELEMENT_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>String To Merge Element Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_MERGE_ELEMENT_MAP_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To String Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To String Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueDataType="org.eclipse.emf.ecore.EString"
	 * @generated
	 */
	EClass getStringToStringMap();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToStringMap()
	 * @generated
	 */
	EAttribute getStringToStringMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToStringMap()
	 * @generated
	 */
	EAttribute getStringToStringMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>IVisual Element To String Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IVisual Element To String Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement"
	 *        valueDataType="org.eclipse.emf.ecore.EString"
	 * @generated
	 */
	EClass getIVisualElementToStringMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getIVisualElementToStringMap()
	 * @generated
	 */
	EReference getIVisualElementToStringMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getIVisualElementToStringMap()
	 * @generated
	 */
	EAttribute getIVisualElementToStringMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>IVisual Connection To String Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IVisual Connection To String Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection"
	 *        valueDataType="org.eclipse.emf.ecore.EString"
	 * @generated
	 */
	EClass getIVisualConnectionToStringMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getIVisualConnectionToStringMap()
	 * @generated
	 */
	EReference getIVisualConnectionToStringMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getIVisualConnectionToStringMap()
	 * @generated
	 */
	EAttribute getIVisualConnectionToStringMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To Merge Element Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To Merge Element Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueType="dk.dtu.imm.red.visualstitch.MergeElement"
	 * @generated
	 */
	EClass getStringToMergeElementMap();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToMergeElementMap()
	 * @generated
	 */
	EAttribute getStringToMergeElementMap_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToMergeElementMap()
	 * @generated
	 */
	EReference getStringToMergeElementMap_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	HashMapEntryFactory getHashMapEntryFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToStringMapImpl <em>String To String Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToStringMapImpl
		 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl#getStringToStringMap()
		 * @generated
		 */
		EClass STRING_TO_STRING_MAP = eINSTANCE.getStringToStringMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_STRING_MAP__KEY = eINSTANCE.getStringToStringMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_STRING_MAP__VALUE = eINSTANCE.getStringToStringMap_Value();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualElementToStringMapImpl <em>IVisual Element To String Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualElementToStringMapImpl
		 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl#getIVisualElementToStringMap()
		 * @generated
		 */
		EClass IVISUAL_ELEMENT_TO_STRING_MAP = eINSTANCE.getIVisualElementToStringMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_ELEMENT_TO_STRING_MAP__KEY = eINSTANCE.getIVisualElementToStringMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_ELEMENT_TO_STRING_MAP__VALUE = eINSTANCE.getIVisualElementToStringMap_Value();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualConnectionToStringMapImpl <em>IVisual Connection To String Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualConnectionToStringMapImpl
		 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl#getIVisualConnectionToStringMap()
		 * @generated
		 */
		EClass IVISUAL_CONNECTION_TO_STRING_MAP = eINSTANCE.getIVisualConnectionToStringMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVISUAL_CONNECTION_TO_STRING_MAP__KEY = eINSTANCE.getIVisualConnectionToStringMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVISUAL_CONNECTION_TO_STRING_MAP__VALUE = eINSTANCE.getIVisualConnectionToStringMap_Value();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToMergeElementMapImpl <em>String To Merge Element Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToMergeElementMapImpl
		 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl#getStringToMergeElementMap()
		 * @generated
		 */
		EClass STRING_TO_MERGE_ELEMENT_MAP = eINSTANCE.getStringToMergeElementMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_MERGE_ELEMENT_MAP__KEY = eINSTANCE.getStringToMergeElementMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_TO_MERGE_ELEMENT_MAP__VALUE = eINSTANCE.getStringToMergeElementMap_Value();

	}

} //HashMapEntryPackage
