/**
 */
package dk.dtu.imm.red.visualstitch;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Custom Attributes Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getCustomAttributesKind()
 * @model
 * @generated
 */
public enum CustomAttributesKind implements Enumerator {
	/**
	 * The '<em><b>NAME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NAME_VALUE
	 * @generated
	 * @ordered
	 */
	NAME(1, "NAME", "NAME"),

	/**
	 * The '<em><b>ROLE SRC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ROLE_SRC_VALUE
	 * @generated
	 * @ordered
	 */
	ROLE_SRC(2, "ROLE_SRC", "ROLE_SRC"),

	/**
	 * The '<em><b>ROLE TGT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ROLE_TGT_VALUE
	 * @generated
	 * @ordered
	 */
	ROLE_TGT(3, "ROLE_TGT", "ROLE_TGT"),

	/**
	 * The '<em><b>MULTIPLICITY SRC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MULTIPLICITY_SRC_VALUE
	 * @generated
	 * @ordered
	 */
	MULTIPLICITY_SRC(4, "MULTIPLICITY_SRC", "MULTIPLICITY_SRC"),

	/**
	 * The '<em><b>MULTIPLICITY TGT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MULTIPLICITY_TGT_VALUE
	 * @generated
	 * @ordered
	 */
	MULTIPLICITY_TGT(5, "MULTIPLICITY_TGT", "MULTIPLICITY_TGT"),

	/**
	 * The '<em><b>VISIBILITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VISIBILITY_VALUE
	 * @generated
	 * @ordered
	 */
	VISIBILITY(6, "VISIBILITY", "VISIBILITY"),

	/**
	 * The '<em><b>MULTIPLICITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MULTIPLICITY_VALUE
	 * @generated
	 * @ordered
	 */
	MULTIPLICITY(7, "MULTIPLICITY", "MULTIPLICITY"),

	/**
	 * The '<em><b>PARAMETERS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PARAMETERS_VALUE
	 * @generated
	 * @ordered
	 */
	PARAMETERS(8, "PARAMETERS", "PARAMETERS"),

	/**
	 * The '<em><b>RETURN TYPE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RETURN_TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	RETURN_TYPE(9, "RETURN_TYPE", "RETURN_TYPE"),

	/**
	 * The '<em><b>ENTRY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENTRY_VALUE
	 * @generated
	 * @ordered
	 */
	ENTRY(10, "ENTRY", "ENTRY"),

	/**
	 * The '<em><b>DO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(11, "DO", "DO"),

	/**
	 * The '<em><b>EXIT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXIT_VALUE
	 * @generated
	 * @ordered
	 */
	EXIT(12, "EXIT", "EXIT"),

	/**
	 * The '<em><b>TRIGGER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRIGGER_VALUE
	 * @generated
	 * @ordered
	 */
	TRIGGER(13, "TRIGGER", "TRIGGER"),

	/**
	 * The '<em><b>GUARD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GUARD_VALUE
	 * @generated
	 * @ordered
	 */
	GUARD(14, "GUARD", "GUARD"),

	/**
	 * The '<em><b>EFFECT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EFFECT_VALUE
	 * @generated
	 * @ordered
	 */
	EFFECT(15, "EFFECT", "EFFECT"),

	/**
	 * The '<em><b>ACTOR KIND</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACTOR_KIND_VALUE
	 * @generated
	 * @ordered
	 */
	ACTOR_KIND(16, "ACTOR_KIND", "ACTOR_KIND");

	/**
	 * The '<em><b>NAME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NAME</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NAME
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NAME_VALUE = 1;

	/**
	 * The '<em><b>ROLE SRC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ROLE SRC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ROLE_SRC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ROLE_SRC_VALUE = 2;

	/**
	 * The '<em><b>ROLE TGT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ROLE TGT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ROLE_TGT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ROLE_TGT_VALUE = 3;

	/**
	 * The '<em><b>MULTIPLICITY SRC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MULTIPLICITY SRC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MULTIPLICITY_SRC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MULTIPLICITY_SRC_VALUE = 4;

	/**
	 * The '<em><b>MULTIPLICITY TGT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MULTIPLICITY TGT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MULTIPLICITY_TGT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MULTIPLICITY_TGT_VALUE = 5;

	/**
	 * The '<em><b>VISIBILITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VISIBILITY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VISIBILITY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VISIBILITY_VALUE = 6;

	/**
	 * The '<em><b>MULTIPLICITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MULTIPLICITY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MULTIPLICITY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MULTIPLICITY_VALUE = 7;

	/**
	 * The '<em><b>PARAMETERS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PARAMETERS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PARAMETERS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PARAMETERS_VALUE = 8;

	/**
	 * The '<em><b>RETURN TYPE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RETURN TYPE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RETURN_TYPE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RETURN_TYPE_VALUE = 9;

	/**
	 * The '<em><b>ENTRY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ENTRY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENTRY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ENTRY_VALUE = 10;

	/**
	 * The '<em><b>DO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 11;

	/**
	 * The '<em><b>EXIT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EXIT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EXIT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EXIT_VALUE = 12;

	/**
	 * The '<em><b>TRIGGER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRIGGER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRIGGER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRIGGER_VALUE = 13;

	/**
	 * The '<em><b>GUARD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GUARD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GUARD
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GUARD_VALUE = 14;

	/**
	 * The '<em><b>EFFECT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EFFECT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EFFECT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EFFECT_VALUE = 15;

	/**
	 * The '<em><b>ACTOR KIND</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ACTOR KIND</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACTOR_KIND
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ACTOR_KIND_VALUE = 16;

	/**
	 * An array of all the '<em><b>Custom Attributes Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final CustomAttributesKind[] VALUES_ARRAY =
		new CustomAttributesKind[] {
			NAME,
			ROLE_SRC,
			ROLE_TGT,
			MULTIPLICITY_SRC,
			MULTIPLICITY_TGT,
			VISIBILITY,
			MULTIPLICITY,
			PARAMETERS,
			RETURN_TYPE,
			ENTRY,
			DO,
			EXIT,
			TRIGGER,
			GUARD,
			EFFECT,
			ACTOR_KIND,
		};

	/**
	 * A public read-only list of all the '<em><b>Custom Attributes Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<CustomAttributesKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Custom Attributes Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CustomAttributesKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CustomAttributesKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Custom Attributes Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CustomAttributesKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CustomAttributesKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Custom Attributes Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CustomAttributesKind get(int value) {
		switch (value) {
			case NAME_VALUE: return NAME;
			case ROLE_SRC_VALUE: return ROLE_SRC;
			case ROLE_TGT_VALUE: return ROLE_TGT;
			case MULTIPLICITY_SRC_VALUE: return MULTIPLICITY_SRC;
			case MULTIPLICITY_TGT_VALUE: return MULTIPLICITY_TGT;
			case VISIBILITY_VALUE: return VISIBILITY;
			case MULTIPLICITY_VALUE: return MULTIPLICITY;
			case PARAMETERS_VALUE: return PARAMETERS;
			case RETURN_TYPE_VALUE: return RETURN_TYPE;
			case ENTRY_VALUE: return ENTRY;
			case DO_VALUE: return DO;
			case EXIT_VALUE: return EXIT;
			case TRIGGER_VALUE: return TRIGGER;
			case GUARD_VALUE: return GUARD;
			case EFFECT_VALUE: return EFFECT;
			case ACTOR_KIND_VALUE: return ACTOR_KIND;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private CustomAttributesKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //CustomAttributesKind
