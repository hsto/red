/**
 */
package dk.dtu.imm.red.visualstitch;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Node ME</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.ControlNodeME#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.ControlNodeME#getOutgoing <em>Outgoing</em>}</li>
 * </ul>
 * </p>
 *
 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getControlNodeME()
 * @model
 * @generated
 */
public interface ControlNodeME extends MergeElement {
	/**
	 * Returns the value of the '<em><b>Incoming</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualstitch.MergeElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming</em>' reference list.
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getControlNodeME_Incoming()
	 * @model
	 * @generated
	 */
	EList<MergeElement> getIncoming();

	/**
	 * Returns the value of the '<em><b>Outgoing</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualstitch.MergeElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing</em>' reference list.
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getControlNodeME_Outgoing()
	 * @model
	 * @generated
	 */
	EList<MergeElement> getOutgoing();

} // ControlNodeME
