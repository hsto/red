package dk.dtu.imm.red.visualstitch.processor;

import dk.dtu.imm.red.visualmodeling.visualmodel.ConnectionType;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActionNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualActivityFinalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualDecisionMergeNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualFlowFinalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualForkJoinNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualInitialNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualObjectNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualReceiveSignalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.activity.VisualSendSignalNode;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualEnumerationLiteral;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualPackageElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualPort;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystem;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDecisionMergeState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualDeepHistoryState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualFinalState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualForkJoinState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualInitialState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualReceiveState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualSendState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualShallowHistoryState;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualActorElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualSystemBoundaryElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.usecase.VisualUseCaseElement;

public class MergeElementType {

	public static final String USECASE = VisualUseCaseElement.class.getSimpleName();
	public static final String ACTOR = VisualActorElement.class.getSimpleName();
	public static final String SYSTEM_BOUNDARY = VisualSystemBoundaryElement.class.getSimpleName();

	public static final String ASSOCIATION = ConnectionType.ASSOCIATION.getName();
	public static final String GENERALIZATION = ConnectionType.GENERALIZATION.getName();
	public static final String USE_CASE_INCLUDE =  ConnectionType.INCLUDE.getName();
	public static final String USE_CASE_EXTEND =  ConnectionType.EXTEND.getName();

	public static final String CLASS = VisualClassElement.class.getSimpleName();
	public static final String ATTRIBUTE = VisualClassAttribute.class.getSimpleName();
	public static final String OPERATION = VisualClassOperation.class.getSimpleName();
	public static final String ENUM = VisualEnumerationElement.class.getSimpleName();
	public static final String ENUM_LITERALS = VisualEnumerationLiteral.class.getSimpleName();
	public static final String PACKAGE = VisualPackageElement.class.getSimpleName();

	public static final String AGGREGATION = ConnectionType.AGGREGATION.getName();
	public static final String COMPOSITION = ConnectionType.COMPOSITION.getName();

	public static final String ACTION = VisualActionNode.class.getSimpleName();
	public static final String INITIAL_NODE = VisualInitialNode.class.getSimpleName();
	public static final String ACTIVITY_FINAL_NODE = VisualActivityFinalNode.class.getSimpleName();
	public static final String FLOW_FINAL_NODE = VisualFlowFinalNode.class.getSimpleName();
	public static final String DECISION_MERGE_NODE = VisualDecisionMergeNode.class.getSimpleName();
	public static final String FORK_JOIN_NODE = VisualForkJoinNode.class.getSimpleName();
	public static final String SEND_SIGNAL = VisualSendSignalNode.class.getSimpleName();
	public static final String RECEIVE_SIGNAL = VisualReceiveSignalNode.class.getSimpleName();
	public static final String OBJECT_NODE = VisualObjectNode.class.getSimpleName();

	public static final String CONTROL_FLOW = ConnectionType.CONTROL_FLOW.getName();
	public static final String OBJECT_FLOW = ConnectionType.OBJECT_FLOW.getName();

	public static final String STATE = VisualState.class.getSimpleName();
	public static final String INITIAL_STATE = VisualInitialState.class.getSimpleName();
	public static final String FINAL_STATE = VisualFinalState.class.getSimpleName();
	public static final String DECISION_MERGE_STATE = VisualDecisionMergeState.class.getSimpleName();
	public static final String FORK_JOIN_STATE = VisualForkJoinState.class.getSimpleName();
	public static final String SHALLOW_HISTORY_STATE = VisualShallowHistoryState.class.getSimpleName();
	public static final String DEEP_HISTORY_STATE = VisualDeepHistoryState.class.getSimpleName();
	public static final String SEND_STATE = VisualSendState.class.getSimpleName();
	public static final String RECEIVE_STATE = VisualReceiveState.class.getSimpleName();

	public static final String STATE_TRANSITION = ConnectionType.STATE_TRANSITION.getName();


	public static final String SYSTEM_STRUCTURE_ACTOR = VisualSystemStructureActor.class.getSimpleName();
	public static final String SYSTEM = VisualSystem.class.getSimpleName();
	public static final String PORT = VisualPort.class.getSimpleName();


}
