package dk.dtu.imm.red.visualstitch.processor;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.MergeElementResolver;

public class MergeInputReader {
	/**
	 * Class responsible for flattening diagrams and putting them into the unmatchedPartitionMap
	 * before passing it to the processor
	 * At the same time, also calculate the furthest point from (0,0) to reduce the amount of white space
	 * created during layouting of the merged diagrams at the right side of the previous diagram
	 */
	private MergeLogger mergeLogger = MergeLogger.instance;
	private MergeExecutor executor;
	private MergeElementResolver resolver;


	//source diagram (input if run by UI)
	VisualDiagram firstDiagram=null;
	VisualDiagram secondDiagram=null;

	//input if run by unit test
	private MergeElement[] arr;

	//calculate the furthest and nearest point from 0,0, to be used for autolayout of the merged diagrams
	private Point firstDiagramFurthestPoint = new Point(0,0);
	private Point secondDiagramNearestPoint = new Point(Integer.MAX_VALUE,Integer.MAX_VALUE);

	//result of this phase, which is the flattened elements, partitioned into different partition based on the
	//merge element type
	private SortedMap<String, Map<String, MergeElement>> unmatchedPartitionMap=
			new TreeMap<String,Map<String,MergeElement>>(new PartitionTypeComparator());

	public MergeInputReader(MergeExecutor executor, VisualDiagram firstDiagram, VisualDiagram secondDiagram){
		this.executor = executor;
		this.resolver = executor.getResolver();
		this.firstDiagram = firstDiagram;
		this.secondDiagram = secondDiagram;
	}

	//only used for running with junit
	public MergeInputReader(MergeElementResolver resolver, MergeElement[] arr){
		this.resolver = resolver;
		this.arr = arr;
	}


	public void execute(){
		populateDataSets();
		calculateFurthestPointInFirstDiagram();
		calculateNearestPointInSecondDiagram();
	}

	private void populateDataSets(){

		if(firstDiagram!=null && secondDiagram!=null){
			//run from UI
			printSourceDiagramInfo();
			populatePartition();
		}
		else
			//run from unit test
			populatePartitionByArr();
	}

	private void calculateFurthestPointInFirstDiagram(){
		if(firstDiagram!=null){
			for(IVisualElement element : firstDiagram.getElements()){
				int maxX = element.getLocation().x + element.getBounds().width;
				int maxY = element.getLocation().y + element.getBounds().height;

				if(firstDiagramFurthestPoint.x < maxX)
					firstDiagramFurthestPoint.setX(maxX);
				if(firstDiagramFurthestPoint.y < maxY)
					firstDiagramFurthestPoint.setY(maxY);
			}
		}
	}

	private void calculateNearestPointInSecondDiagram(){
		if(secondDiagram!=null){
			for(IVisualElement element : secondDiagram.getElements()){
				int minX = element.getLocation().x;
				int minY = element.getLocation().y;

				if(secondDiagramNearestPoint.x > minX)
					secondDiagramNearestPoint.setX(minX);
				if(secondDiagramNearestPoint.y > minY)
					secondDiagramNearestPoint.setY(minY);
			}
		}
	}

//  Recursively go through the diagram elements.
//  If they have names, put them into first pass Map, partition them by the type
//  If they do not have names, put them into second pass Map, partition them by the type
	private void populatePartition() {
		recursiveFlattenElements(unmatchedPartitionMap, resolver,
				1,firstDiagram.getElements());
		flattenConnections(unmatchedPartitionMap, resolver,
				1,firstDiagram.getDiagramConnections());

		recursiveFlattenElements(unmatchedPartitionMap, resolver,
				2,secondDiagram.getElements());
		flattenConnections(unmatchedPartitionMap, resolver,
				2,secondDiagram.getDiagramConnections());

	}


	private void populatePartitionByArr() {
		for(int i=0;i<arr.length;i++){
			if(arr[i]==null){
				break;
			}
			MergeElement mergeElement = arr[i];
			String type = mergeElement.getType();


			if (!unmatchedPartitionMap.containsKey(type)) {
				// does not contain the partition yet, create the partition based on the type of the element
				Map<String, MergeElement> partition = new LinkedHashMap<String, MergeElement>();
				partition.put(mergeElement.getId(), mergeElement);
				unmatchedPartitionMap.put(type, partition);
			} else {
				// already has the partition, just insert the element with the id as key
				Map<String, MergeElement> partition = unmatchedPartitionMap.get(type);
				partition.put(mergeElement.getId(), mergeElement);
			}


		}


	}

	private void flattenConnections(
			Map<String, Map<String, MergeElement>> unmatchedPartitionMap,
			MergeElementResolver resolver,
			int inputDiagramIndex,
			EList<IVisualConnection> visualConnectionList
			) {
		for (IVisualConnection visualConn : visualConnectionList) {
			MergeElement mergeElement = MergeElementFactory.makeMergeElement(
					MergeUtil.getTypeFromVisualConnection(visualConn),false);

			mergeElement.initialize(resolver, inputDiagramIndex, null, visualConn);


			String type = mergeElement.getType();

			if (!unmatchedPartitionMap.containsKey(type)) {
				// does not contain the partition yet, create the partition based on the type of the element
				Map<String, MergeElement> partition = new LinkedHashMap<String, MergeElement>();
				partition.put(mergeElement.getId(), mergeElement);
				unmatchedPartitionMap.put(type, partition);
			} else {
				// already has the partition, just insert the element with the id as key
				Map<String, MergeElement> IDToMergeElementMap = unmatchedPartitionMap
						.get(type);
				IDToMergeElementMap.put(mergeElement.getId(), mergeElement);
			}

		}
	}

	private void recursiveFlattenElements(
			Map<String, Map<String, MergeElement>> unmatchedPartitionMap,
			MergeElementResolver resolver,
			int inputDiagramIndex,
			EList<IVisualElement> visualElementList
			) {
		for (IVisualElement visualElement : visualElementList) {
			MergeElement mergeElement = MergeElementFactory.makeMergeElement(
					MergeUtil.getTypeFromVisualElement(visualElement),true);
			mergeElement.initialize(resolver, inputDiagramIndex, visualElement, null);

			String type = mergeElement.getType();

			if (!unmatchedPartitionMap.containsKey(type)) {
				// does not contain the partition yet, create the partition based on the type of the element
				Map<String, MergeElement> partition = new LinkedHashMap<String, MergeElement>();
				partition.put(mergeElement.getId(), mergeElement);
				unmatchedPartitionMap.put(type, partition);
			} else {
				// already has the partition, just insert the element with the id as key
				Map<String, MergeElement> partition = unmatchedPartitionMap
						.get(type);
				partition.put(mergeElement.getId(), mergeElement);
			}

			// recursively get children visual elements in the visualElement
			if (visualElement.getElements() != null
					&& visualElement.getElements().size() > 0) {
				recursiveFlattenElements(unmatchedPartitionMap,
						resolver,inputDiagramIndex,visualElement.getElements());
			}
		}
	}


	public void destroy(){
		arr = null;
		unmatchedPartitionMap.clear();
	}

	public Map<String, Map<String, MergeElement>> getUnmatchedPartitionMap() {
		return unmatchedPartitionMap;
	}


	public Point getFurthestPointInFirstDiagram(){
		return firstDiagramFurthestPoint;
	}

	public Point getNearestPointInSecondDiagram(){
		return secondDiagramNearestPoint;
	}


	private void printSourceDiagramInfo() {
		if(firstDiagram!=null && firstDiagram.getDiagramElement()!=null &&
			secondDiagram!=null && secondDiagram.getDiagramElement()!=null
		){
			mergeLogger.append(MergeLogger.DEBUG,"READING THE FOLLOWING DIAGRAM AS MERGE INPUT:" );
			mergeLogger.append(MergeLogger.DEBUG,"FIRST DIAGRAM= " + firstDiagram.getDiagramElement().getLabel() + "-" + firstDiagram.getDiagramElement().getName() );
			mergeLogger.append(MergeLogger.DEBUG,"SECOND DIAGRAM= " + secondDiagram.getDiagramElement().getLabel() + "-" + secondDiagram.getDiagramElement().getName() );
		}

	}
}
