/**
 */
package dk.dtu.imm.red.visualstitch.HashMapEntry.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import dk.dtu.imm.red.visualstitch.HashMapEntry.HashMapEntryFactory;
import dk.dtu.imm.red.visualstitch.HashMapEntry.HashMapEntryPackage;

import dk.dtu.imm.red.visualstitch.VisualstitchPackage;

import dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HashMapEntryPackageImpl extends EPackageImpl implements HashMapEntryPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToStringMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iVisualElementToStringMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iVisualConnectionToStringMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToMergeElementMapEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.HashMapEntryPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private HashMapEntryPackageImpl() {
		super(eNS_URI, HashMapEntryFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link HashMapEntryPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static HashMapEntryPackage init() {
		if (isInited) return (HashMapEntryPackage)EPackage.Registry.INSTANCE.getEPackage(HashMapEntryPackage.eNS_URI);

		// Obtain or create and register package
		HashMapEntryPackageImpl theHashMapEntryPackage = (HashMapEntryPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof HashMapEntryPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new HashMapEntryPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		visualmodelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		VisualstitchPackageImpl theVisualstitchPackage = (VisualstitchPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(VisualstitchPackage.eNS_URI) instanceof VisualstitchPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(VisualstitchPackage.eNS_URI) : VisualstitchPackage.eINSTANCE);

		// Create package meta-data objects
		theHashMapEntryPackage.createPackageContents();
		theVisualstitchPackage.createPackageContents();

		// Initialize created meta-data
		theHashMapEntryPackage.initializePackageContents();
		theVisualstitchPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theHashMapEntryPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(HashMapEntryPackage.eNS_URI, theHashMapEntryPackage);
		return theHashMapEntryPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToStringMap() {
		return stringToStringMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToStringMap_Key() {
		return (EAttribute)stringToStringMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToStringMap_Value() {
		return (EAttribute)stringToStringMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIVisualElementToStringMap() {
		return iVisualElementToStringMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualElementToStringMap_Key() {
		return (EReference)iVisualElementToStringMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualElementToStringMap_Value() {
		return (EAttribute)iVisualElementToStringMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIVisualConnectionToStringMap() {
		return iVisualConnectionToStringMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVisualConnectionToStringMap_Key() {
		return (EReference)iVisualConnectionToStringMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVisualConnectionToStringMap_Value() {
		return (EAttribute)iVisualConnectionToStringMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToMergeElementMap() {
		return stringToMergeElementMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToMergeElementMap_Key() {
		return (EAttribute)stringToMergeElementMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStringToMergeElementMap_Value() {
		return (EReference)stringToMergeElementMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HashMapEntryFactory getHashMapEntryFactory() {
		return (HashMapEntryFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		stringToStringMapEClass = createEClass(STRING_TO_STRING_MAP);
		createEAttribute(stringToStringMapEClass, STRING_TO_STRING_MAP__KEY);
		createEAttribute(stringToStringMapEClass, STRING_TO_STRING_MAP__VALUE);

		iVisualElementToStringMapEClass = createEClass(IVISUAL_ELEMENT_TO_STRING_MAP);
		createEReference(iVisualElementToStringMapEClass, IVISUAL_ELEMENT_TO_STRING_MAP__KEY);
		createEAttribute(iVisualElementToStringMapEClass, IVISUAL_ELEMENT_TO_STRING_MAP__VALUE);

		iVisualConnectionToStringMapEClass = createEClass(IVISUAL_CONNECTION_TO_STRING_MAP);
		createEReference(iVisualConnectionToStringMapEClass, IVISUAL_CONNECTION_TO_STRING_MAP__KEY);
		createEAttribute(iVisualConnectionToStringMapEClass, IVISUAL_CONNECTION_TO_STRING_MAP__VALUE);

		stringToMergeElementMapEClass = createEClass(STRING_TO_MERGE_ELEMENT_MAP);
		createEAttribute(stringToMergeElementMapEClass, STRING_TO_MERGE_ELEMENT_MAP__KEY);
		createEReference(stringToMergeElementMapEClass, STRING_TO_MERGE_ELEMENT_MAP__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		visualmodelPackage thevisualmodelPackage = (visualmodelPackage)EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		VisualstitchPackage theVisualstitchPackage = (VisualstitchPackage)EPackage.Registry.INSTANCE.getEPackage(VisualstitchPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(stringToStringMapEClass, Map.Entry.class, "StringToStringMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToStringMap_Key(), ecorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToStringMap_Value(), ecorePackage.getEString(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iVisualElementToStringMapEClass, Map.Entry.class, "IVisualElementToStringMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIVisualElementToStringMap_Key(), thevisualmodelPackage.getIVisualElement(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualElementToStringMap_Value(), ecorePackage.getEString(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iVisualConnectionToStringMapEClass, Map.Entry.class, "IVisualConnectionToStringMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIVisualConnectionToStringMap_Key(), thevisualmodelPackage.getIVisualConnection(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVisualConnectionToStringMap_Value(), ecorePackage.getEString(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToMergeElementMapEClass, Map.Entry.class, "StringToMergeElementMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToMergeElementMap_Key(), theEcorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStringToMergeElementMap_Value(), theVisualstitchPackage.getMergeElement(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //HashMapEntryPackageImpl
