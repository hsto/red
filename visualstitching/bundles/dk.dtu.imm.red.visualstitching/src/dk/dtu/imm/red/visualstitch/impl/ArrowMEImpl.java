/**
 */
package dk.dtu.imm.red.visualstitch.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import dk.dtu.imm.red.visualstitch.ArrowME;
import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.VisualstitchPackage;
import dk.dtu.imm.red.visualstitch.processor.MatchKB;
import dk.dtu.imm.red.visualstitch.processor.MergeConflictException;
import dk.dtu.imm.red.visualstitch.processor.MergeUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Arrow ME</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.ArrowMEImpl#getSrc <em>Src</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.ArrowMEImpl#getTgt <em>Tgt</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArrowMEImpl extends MergeElementImpl implements ArrowME {
	/**
	 * The cached value of the '{@link #getSrc() <em>Src</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrc()
	 * @generated
	 * @ordered
	 */
	protected MergeElement src;

	/**
	 * The cached value of the '{@link #getTgt() <em>Tgt</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTgt()
	 * @generated
	 * @ordered
	 */
	protected MergeElement tgt;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected ArrowMEImpl() {
		super();
		getSrc();
		getTgt();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualstitchPackage.Literals.ARROW_ME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement getSrc() {
		if (src != null && src.eIsProxy()) {
			InternalEObject oldSrc = (InternalEObject)src;
			src = (MergeElement)eResolveProxy(oldSrc);
			if (src != oldSrc) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisualstitchPackage.ARROW_ME__SRC, oldSrc, src));
			}
		}
		return src;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement basicGetSrc() {
		return src;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSrc(MergeElement newSrc) {
		MergeElement oldSrc = src;
		src = newSrc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.ARROW_ME__SRC, oldSrc, src));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement getTgt() {
		if (tgt != null && tgt.eIsProxy()) {
			InternalEObject oldTgt = (InternalEObject)tgt;
			tgt = (MergeElement)eResolveProxy(oldTgt);
			if (tgt != oldTgt) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisualstitchPackage.ARROW_ME__TGT, oldTgt, tgt));
			}
		}
		return tgt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement basicGetTgt() {
		return tgt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTgt(MergeElement newTgt) {
		MergeElement oldTgt = tgt;
		tgt = newTgt;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.ARROW_ME__TGT, oldTgt, tgt));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualstitchPackage.ARROW_ME__SRC:
				if (resolve) return getSrc();
				return basicGetSrc();
			case VisualstitchPackage.ARROW_ME__TGT:
				if (resolve) return getTgt();
				return basicGetTgt();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualstitchPackage.ARROW_ME__SRC:
				setSrc((MergeElement)newValue);
				return;
			case VisualstitchPackage.ARROW_ME__TGT:
				setTgt((MergeElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.ARROW_ME__SRC:
				setSrc((MergeElement)null);
				return;
			case VisualstitchPackage.ARROW_ME__TGT:
				setTgt((MergeElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.ARROW_ME__SRC:
				return src != null;
			case VisualstitchPackage.ARROW_ME__TGT:
				return tgt != null;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(", src: ");
		result.append((getSrc() != null ? getSrc().getId() : ""));
		result.append(", tgt: ");
		result.append((getTgt() != null ? getTgt().getId() : ""));
		return super.toString().replaceAll("MergeElement", "ArrowME") + result.toString();
	}

	@Override
	public MatchResult matchValue(MergeElement m2, MatchKB matchKB) throws MergeConflictException{
		//consider as true match if both name is empty
		MatchResult canMatchByName = MergeUtil.canMatchByName(this, m2,MatchResult.TRUE);
		MatchResult canMatchByCustomAttr = MergeUtil.canMatchByCustomAttr(this, m2);

		return threeValAND(canMatchByName,canMatchByCustomAttr);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult matchStructure(MergeElement m2, MatchKB matchKB) throws MergeConflictException{

		MergeElement m1Src = this.getSrc();
		MergeElement m1Tgt = this.getTgt();
		MergeElement m2Src = ((ArrowME)m2).getSrc();
		MergeElement m2Tgt = ((ArrowME)m2).getTgt();

		MatchResult srcEqual = m1Src.equals(m2Src,matchKB);
		MatchResult tgtEqual = m1Tgt.equals(m2Tgt,matchKB);

		//relaxing the match for UNKNOWN with at least one TRUE to be considered as TRUE
		MatchResult result = threeValRelaxedAND(srcEqual,tgtEqual);

		return result;
	}

} //ArrowMEImpl
