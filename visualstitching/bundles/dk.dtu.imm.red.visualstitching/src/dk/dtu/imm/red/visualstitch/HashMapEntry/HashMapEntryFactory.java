/**
 */
package dk.dtu.imm.red.visualstitch.HashMapEntry;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualstitch.HashMapEntry.HashMapEntryPackage
 * @generated
 */
public interface HashMapEntryFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HashMapEntryFactory eINSTANCE = dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryFactoryImpl.init();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	HashMapEntryPackage getHashMapEntryPackage();

} //HashMapEntryFactory
