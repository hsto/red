/**
 */
package dk.dtu.imm.red.visualstitch;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualstitch.VisualstitchFactory
 * @model kind="package"
 * @generated
 */
public interface VisualstitchPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "visualstitch";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.visualstitch";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "visualstitch";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VisualstitchPackage eINSTANCE = dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl <em>Merge Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.impl.MergeElementImpl
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getMergeElement()
	 * @generated
	 */
	int MERGE_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__ID = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__NAME = 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__PARENT = 3;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__CHILDREN = 4;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__CUSTOM_ATTRIBUTES = 5;

	/**
	 * The feature id for the '<em><b>Input Diagram Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__INPUT_DIAGRAM_INDEX = 6;

	/**
	 * The feature id for the '<em><b>Input Visual Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__INPUT_VISUAL_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Input Visual Connection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__INPUT_VISUAL_CONNECTION = 8;

	/**
	 * The feature id for the '<em><b>Resolver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT__RESOLVER = 9;

	/**
	 * The number of structural features of the '<em>Merge Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_FEATURE_COUNT = 10;

	/**
	 * The operation id for the '<em>Initialize</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION = 0;

	/**
	 * The operation id for the '<em>Initialize For Unit Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING = 1;

	/**
	 * The number of operations of the '<em>Merge Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.impl.MergeElementResolverImpl <em>Merge Element Resolver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.impl.MergeElementResolverImpl
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getMergeElementResolver()
	 * @generated
	 */
	int MERGE_ELEMENT_RESOLVER = 1;

	/**
	 * The feature id for the '<em><b>Visual Element To ID Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER__VISUAL_ELEMENT_TO_ID_MAP = 0;

	/**
	 * The feature id for the '<em><b>Visual Connection To ID Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER__VISUAL_CONNECTION_TO_ID_MAP = 1;

	/**
	 * The feature id for the '<em><b>ID To Merge Element Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER__ID_TO_MERGE_ELEMENT_MAP = 2;

	/**
	 * The number of structural features of the '<em>Merge Element Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Keep Track Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT_IVISUALELEMENT = 0;

	/**
	 * The operation id for the '<em>Keep Track Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT_IVISUALCONNECTION = 1;

	/**
	 * The operation id for the '<em>Keep Track Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT = 2;

	/**
	 * The operation id for the '<em>Lookup Merge Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__IVISUALELEMENT = 3;

	/**
	 * The operation id for the '<em>Lookup Merge Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__IVISUALCONNECTION = 4;

	/**
	 * The operation id for the '<em>Lookup Merge Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__STRING = 5;

	/**
	 * The operation id for the '<em>Destroy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER___DESTROY = 6;

	/**
	 * The number of operations of the '<em>Merge Element Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_ELEMENT_RESOLVER_OPERATION_COUNT = 7;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.impl.ConnectionMEImpl <em>Connection ME</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.impl.ConnectionMEImpl
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getConnectionME()
	 * @generated
	 */
	int CONNECTION_ME = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__ID = MERGE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__TYPE = MERGE_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__NAME = MERGE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__PARENT = MERGE_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__CHILDREN = MERGE_ELEMENT__CHILDREN;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__CUSTOM_ATTRIBUTES = MERGE_ELEMENT__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Input Diagram Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__INPUT_DIAGRAM_INDEX = MERGE_ELEMENT__INPUT_DIAGRAM_INDEX;

	/**
	 * The feature id for the '<em><b>Input Visual Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__INPUT_VISUAL_ELEMENT = MERGE_ELEMENT__INPUT_VISUAL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Input Visual Connection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__INPUT_VISUAL_CONNECTION = MERGE_ELEMENT__INPUT_VISUAL_CONNECTION;

	/**
	 * The feature id for the '<em><b>Resolver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__RESOLVER = MERGE_ELEMENT__RESOLVER;

	/**
	 * The feature id for the '<em><b>Conn End1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__CONN_END1 = MERGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Conn End2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME__CONN_END2 = MERGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Connection ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME_FEATURE_COUNT = MERGE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Initialize</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION = MERGE_ELEMENT___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION;

	/**
	 * The operation id for the '<em>Initialize For Unit Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING = MERGE_ELEMENT___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING;

	/**
	 * The number of operations of the '<em>Connection ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ME_OPERATION_COUNT = MERGE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.impl.ArrowMEImpl <em>Arrow ME</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.impl.ArrowMEImpl
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getArrowME()
	 * @generated
	 */
	int ARROW_ME = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__ID = MERGE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__TYPE = MERGE_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__NAME = MERGE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__PARENT = MERGE_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__CHILDREN = MERGE_ELEMENT__CHILDREN;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__CUSTOM_ATTRIBUTES = MERGE_ELEMENT__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Input Diagram Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__INPUT_DIAGRAM_INDEX = MERGE_ELEMENT__INPUT_DIAGRAM_INDEX;

	/**
	 * The feature id for the '<em><b>Input Visual Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__INPUT_VISUAL_ELEMENT = MERGE_ELEMENT__INPUT_VISUAL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Input Visual Connection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__INPUT_VISUAL_CONNECTION = MERGE_ELEMENT__INPUT_VISUAL_CONNECTION;

	/**
	 * The feature id for the '<em><b>Resolver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__RESOLVER = MERGE_ELEMENT__RESOLVER;

	/**
	 * The feature id for the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__SRC = MERGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tgt</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME__TGT = MERGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Arrow ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME_FEATURE_COUNT = MERGE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Initialize</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION = MERGE_ELEMENT___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION;

	/**
	 * The operation id for the '<em>Initialize For Unit Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING = MERGE_ELEMENT___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING;

	/**
	 * The number of operations of the '<em>Arrow ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_ME_OPERATION_COUNT = MERGE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.impl.ControlNodeMEImpl <em>Control Node ME</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.impl.ControlNodeMEImpl
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getControlNodeME()
	 * @generated
	 */
	int CONTROL_NODE_ME = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__ID = MERGE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__TYPE = MERGE_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__NAME = MERGE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__PARENT = MERGE_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__CHILDREN = MERGE_ELEMENT__CHILDREN;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__CUSTOM_ATTRIBUTES = MERGE_ELEMENT__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Input Diagram Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__INPUT_DIAGRAM_INDEX = MERGE_ELEMENT__INPUT_DIAGRAM_INDEX;

	/**
	 * The feature id for the '<em><b>Input Visual Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__INPUT_VISUAL_ELEMENT = MERGE_ELEMENT__INPUT_VISUAL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Input Visual Connection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__INPUT_VISUAL_CONNECTION = MERGE_ELEMENT__INPUT_VISUAL_CONNECTION;

	/**
	 * The feature id for the '<em><b>Resolver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__RESOLVER = MERGE_ELEMENT__RESOLVER;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__INCOMING = MERGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME__OUTGOING = MERGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Control Node ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME_FEATURE_COUNT = MERGE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Initialize</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION = MERGE_ELEMENT___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION;

	/**
	 * The operation id for the '<em>Initialize For Unit Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING = MERGE_ELEMENT___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING;

	/**
	 * The number of operations of the '<em>Control Node ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_ME_OPERATION_COUNT = MERGE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.impl.InitialNodeMEImpl <em>Initial Node ME</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.impl.InitialNodeMEImpl
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getInitialNodeME()
	 * @generated
	 */
	int INITIAL_NODE_ME = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__ID = CONTROL_NODE_ME__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__TYPE = CONTROL_NODE_ME__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__NAME = CONTROL_NODE_ME__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__PARENT = CONTROL_NODE_ME__PARENT;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__CHILDREN = CONTROL_NODE_ME__CHILDREN;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__CUSTOM_ATTRIBUTES = CONTROL_NODE_ME__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Input Diagram Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__INPUT_DIAGRAM_INDEX = CONTROL_NODE_ME__INPUT_DIAGRAM_INDEX;

	/**
	 * The feature id for the '<em><b>Input Visual Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__INPUT_VISUAL_ELEMENT = CONTROL_NODE_ME__INPUT_VISUAL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Input Visual Connection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__INPUT_VISUAL_CONNECTION = CONTROL_NODE_ME__INPUT_VISUAL_CONNECTION;

	/**
	 * The feature id for the '<em><b>Resolver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__RESOLVER = CONTROL_NODE_ME__RESOLVER;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__INCOMING = CONTROL_NODE_ME__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME__OUTGOING = CONTROL_NODE_ME__OUTGOING;

	/**
	 * The number of structural features of the '<em>Initial Node ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME_FEATURE_COUNT = CONTROL_NODE_ME_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Initialize</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION = CONTROL_NODE_ME___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION;

	/**
	 * The operation id for the '<em>Initialize For Unit Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING = CONTROL_NODE_ME___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING;

	/**
	 * The number of operations of the '<em>Initial Node ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_ME_OPERATION_COUNT = CONTROL_NODE_ME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.impl.FinalNodeMEImpl <em>Final Node ME</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.impl.FinalNodeMEImpl
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getFinalNodeME()
	 * @generated
	 */
	int FINAL_NODE_ME = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__ID = CONTROL_NODE_ME__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__TYPE = CONTROL_NODE_ME__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__NAME = CONTROL_NODE_ME__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__PARENT = CONTROL_NODE_ME__PARENT;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__CHILDREN = CONTROL_NODE_ME__CHILDREN;

	/**
	 * The feature id for the '<em><b>Custom Attributes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__CUSTOM_ATTRIBUTES = CONTROL_NODE_ME__CUSTOM_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Input Diagram Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__INPUT_DIAGRAM_INDEX = CONTROL_NODE_ME__INPUT_DIAGRAM_INDEX;

	/**
	 * The feature id for the '<em><b>Input Visual Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__INPUT_VISUAL_ELEMENT = CONTROL_NODE_ME__INPUT_VISUAL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Input Visual Connection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__INPUT_VISUAL_CONNECTION = CONTROL_NODE_ME__INPUT_VISUAL_CONNECTION;

	/**
	 * The feature id for the '<em><b>Resolver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__RESOLVER = CONTROL_NODE_ME__RESOLVER;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__INCOMING = CONTROL_NODE_ME__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME__OUTGOING = CONTROL_NODE_ME__OUTGOING;

	/**
	 * The number of structural features of the '<em>Final Node ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME_FEATURE_COUNT = CONTROL_NODE_ME_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Initialize</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION = CONTROL_NODE_ME___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION;

	/**
	 * The operation id for the '<em>Initialize For Unit Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING = CONTROL_NODE_ME___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING;

	/**
	 * The number of operations of the '<em>Final Node ME</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_ME_OPERATION_COUNT = CONTROL_NODE_ME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.CustomAttributesKind <em>Custom Attributes Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.CustomAttributesKind
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getCustomAttributesKind()
	 * @generated
	 */
	int CUSTOM_ATTRIBUTES_KIND = 7;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.MatchMode <em>Match Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.MatchMode
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getMatchMode()
	 * @generated
	 */
	int MATCH_MODE = 8;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.visualstitch.MatchResult <em>Match Result</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.visualstitch.MatchResult
	 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getMatchResult()
	 * @generated
	 */
	int MATCH_RESULT = 9;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualstitch.MergeElement <em>Merge Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Merge Element</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement
	 * @generated
	 */
	EClass getMergeElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualstitch.MergeElement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getId()
	 * @see #getMergeElement()
	 * @generated
	 */
	EAttribute getMergeElement_Id();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualstitch.MergeElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getType()
	 * @see #getMergeElement()
	 * @generated
	 */
	EAttribute getMergeElement_Type();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualstitch.MergeElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getName()
	 * @see #getMergeElement()
	 * @generated
	 */
	EAttribute getMergeElement_Name();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualstitch.MergeElement#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getParent()
	 * @see #getMergeElement()
	 * @generated
	 */
	EReference getMergeElement_Parent();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.visualstitch.MergeElement#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Children</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getChildren()
	 * @see #getMergeElement()
	 * @generated
	 */
	EReference getMergeElement_Children();

	/**
	 * Returns the meta object for the map '{@link dk.dtu.imm.red.visualstitch.MergeElement#getCustomAttributes <em>Custom Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Custom Attributes</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getCustomAttributes()
	 * @see #getMergeElement()
	 * @generated
	 */
	EReference getMergeElement_CustomAttributes();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.visualstitch.MergeElement#getInputDiagramIndex <em>Input Diagram Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Diagram Index</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getInputDiagramIndex()
	 * @see #getMergeElement()
	 * @generated
	 */
	EAttribute getMergeElement_InputDiagramIndex();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.visualstitch.MergeElement#getInputVisualElement <em>Input Visual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Input Visual Element</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getInputVisualElement()
	 * @see #getMergeElement()
	 * @generated
	 */
	EReference getMergeElement_InputVisualElement();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.visualstitch.MergeElement#getInputVisualConnection <em>Input Visual Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Input Visual Connection</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getInputVisualConnection()
	 * @see #getMergeElement()
	 * @generated
	 */
	EReference getMergeElement_InputVisualConnection();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualstitch.MergeElement#getResolver <em>Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resolver</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#getResolver()
	 * @see #getMergeElement()
	 * @generated
	 */
	EReference getMergeElement_Resolver();

	/**
	 * Returns the meta object for the '{@link dk.dtu.imm.red.visualstitch.MergeElement#initialize(dk.dtu.imm.red.visualstitch.MergeElementResolver, int, dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement, dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection) <em>Initialize</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initialize</em>' operation.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#initialize(dk.dtu.imm.red.visualstitch.MergeElementResolver, int, dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement, dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection)
	 * @generated
	 */
	EOperation getMergeElement__Initialize__MergeElementResolver_int_IVisualElement_IVisualConnection();

	/**
	 * Returns the meta object for the '{@link dk.dtu.imm.red.visualstitch.MergeElement#initializeForUnitTest(dk.dtu.imm.red.visualstitch.MergeElementResolver, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initialize For Unit Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initialize For Unit Test</em>' operation.
	 * @see dk.dtu.imm.red.visualstitch.MergeElement#initializeForUnitTest(dk.dtu.imm.red.visualstitch.MergeElementResolver, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getMergeElement__InitializeForUnitTest__MergeElementResolver_int_String_String_String_String_String();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver <em>Merge Element Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Merge Element Resolver</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver
	 * @generated
	 */
	EClass getMergeElementResolver();

	/**
	 * Returns the meta object for the map '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#getVisualElementToIDMap <em>Visual Element To ID Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Visual Element To ID Map</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#getVisualElementToIDMap()
	 * @see #getMergeElementResolver()
	 * @generated
	 */
	EReference getMergeElementResolver_VisualElementToIDMap();

	/**
	 * Returns the meta object for the map '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#getVisualConnectionToIDMap <em>Visual Connection To ID Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Visual Connection To ID Map</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#getVisualConnectionToIDMap()
	 * @see #getMergeElementResolver()
	 * @generated
	 */
	EReference getMergeElementResolver_VisualConnectionToIDMap();

	/**
	 * Returns the meta object for the map '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#getIDToMergeElementMap <em>ID To Merge Element Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>ID To Merge Element Map</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#getIDToMergeElementMap()
	 * @see #getMergeElementResolver()
	 * @generated
	 */
	EReference getMergeElementResolver_IDToMergeElementMap();

	/**
	 * Returns the meta object for the '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#keepTrackOf(java.lang.String, dk.dtu.imm.red.visualstitch.MergeElement, dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement) <em>Keep Track Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Keep Track Of</em>' operation.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#keepTrackOf(java.lang.String, dk.dtu.imm.red.visualstitch.MergeElement, dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement)
	 * @generated
	 */
	EOperation getMergeElementResolver__KeepTrackOf__String_MergeElement_IVisualElement();

	/**
	 * Returns the meta object for the '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#keepTrackOf(java.lang.String, dk.dtu.imm.red.visualstitch.MergeElement, dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection) <em>Keep Track Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Keep Track Of</em>' operation.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#keepTrackOf(java.lang.String, dk.dtu.imm.red.visualstitch.MergeElement, dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection)
	 * @generated
	 */
	EOperation getMergeElementResolver__KeepTrackOf__String_MergeElement_IVisualConnection();

	/**
	 * Returns the meta object for the '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#keepTrackOf(java.lang.String, dk.dtu.imm.red.visualstitch.MergeElement) <em>Keep Track Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Keep Track Of</em>' operation.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#keepTrackOf(java.lang.String, dk.dtu.imm.red.visualstitch.MergeElement)
	 * @generated
	 */
	EOperation getMergeElementResolver__KeepTrackOf__String_MergeElement();

	/**
	 * Returns the meta object for the '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#lookupMergeElement(dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement) <em>Lookup Merge Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Lookup Merge Element</em>' operation.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#lookupMergeElement(dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement)
	 * @generated
	 */
	EOperation getMergeElementResolver__LookupMergeElement__IVisualElement();

	/**
	 * Returns the meta object for the '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#lookupMergeElement(dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection) <em>Lookup Merge Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Lookup Merge Element</em>' operation.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#lookupMergeElement(dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection)
	 * @generated
	 */
	EOperation getMergeElementResolver__LookupMergeElement__IVisualConnection();

	/**
	 * Returns the meta object for the '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#lookupMergeElement(java.lang.String) <em>Lookup Merge Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Lookup Merge Element</em>' operation.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#lookupMergeElement(java.lang.String)
	 * @generated
	 */
	EOperation getMergeElementResolver__LookupMergeElement__String();

	/**
	 * Returns the meta object for the '{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#destroy() <em>Destroy</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Destroy</em>' operation.
	 * @see dk.dtu.imm.red.visualstitch.MergeElementResolver#destroy()
	 * @generated
	 */
	EOperation getMergeElementResolver__Destroy();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualstitch.ConnectionME <em>Connection ME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection ME</em>'.
	 * @see dk.dtu.imm.red.visualstitch.ConnectionME
	 * @generated
	 */
	EClass getConnectionME();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualstitch.ConnectionME#getConnEnd1 <em>Conn End1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conn End1</em>'.
	 * @see dk.dtu.imm.red.visualstitch.ConnectionME#getConnEnd1()
	 * @see #getConnectionME()
	 * @generated
	 */
	EReference getConnectionME_ConnEnd1();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualstitch.ConnectionME#getConnEnd2 <em>Conn End2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conn End2</em>'.
	 * @see dk.dtu.imm.red.visualstitch.ConnectionME#getConnEnd2()
	 * @see #getConnectionME()
	 * @generated
	 */
	EReference getConnectionME_ConnEnd2();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualstitch.ArrowME <em>Arrow ME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arrow ME</em>'.
	 * @see dk.dtu.imm.red.visualstitch.ArrowME
	 * @generated
	 */
	EClass getArrowME();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualstitch.ArrowME#getSrc <em>Src</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Src</em>'.
	 * @see dk.dtu.imm.red.visualstitch.ArrowME#getSrc()
	 * @see #getArrowME()
	 * @generated
	 */
	EReference getArrowME_Src();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.visualstitch.ArrowME#getTgt <em>Tgt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tgt</em>'.
	 * @see dk.dtu.imm.red.visualstitch.ArrowME#getTgt()
	 * @see #getArrowME()
	 * @generated
	 */
	EReference getArrowME_Tgt();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualstitch.ControlNodeME <em>Control Node ME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Node ME</em>'.
	 * @see dk.dtu.imm.red.visualstitch.ControlNodeME
	 * @generated
	 */
	EClass getControlNodeME();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.visualstitch.ControlNodeME#getIncoming <em>Incoming</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming</em>'.
	 * @see dk.dtu.imm.red.visualstitch.ControlNodeME#getIncoming()
	 * @see #getControlNodeME()
	 * @generated
	 */
	EReference getControlNodeME_Incoming();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.visualstitch.ControlNodeME#getOutgoing <em>Outgoing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing</em>'.
	 * @see dk.dtu.imm.red.visualstitch.ControlNodeME#getOutgoing()
	 * @see #getControlNodeME()
	 * @generated
	 */
	EReference getControlNodeME_Outgoing();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualstitch.InitialNodeME <em>Initial Node ME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Initial Node ME</em>'.
	 * @see dk.dtu.imm.red.visualstitch.InitialNodeME
	 * @generated
	 */
	EClass getInitialNodeME();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.visualstitch.FinalNodeME <em>Final Node ME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Final Node ME</em>'.
	 * @see dk.dtu.imm.red.visualstitch.FinalNodeME
	 * @generated
	 */
	EClass getFinalNodeME();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualstitch.CustomAttributesKind <em>Custom Attributes Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Custom Attributes Kind</em>'.
	 * @see dk.dtu.imm.red.visualstitch.CustomAttributesKind
	 * @generated
	 */
	EEnum getCustomAttributesKind();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualstitch.MatchMode <em>Match Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Match Mode</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MatchMode
	 * @generated
	 */
	EEnum getMatchMode();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.visualstitch.MatchResult <em>Match Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Match Result</em>'.
	 * @see dk.dtu.imm.red.visualstitch.MatchResult
	 * @generated
	 */
	EEnum getMatchResult();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VisualstitchFactory getVisualstitchFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl <em>Merge Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.impl.MergeElementImpl
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getMergeElement()
		 * @generated
		 */
		EClass MERGE_ELEMENT = eINSTANCE.getMergeElement();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MERGE_ELEMENT__ID = eINSTANCE.getMergeElement_Id();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MERGE_ELEMENT__TYPE = eINSTANCE.getMergeElement_Type();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MERGE_ELEMENT__NAME = eINSTANCE.getMergeElement_Name();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ELEMENT__PARENT = eINSTANCE.getMergeElement_Parent();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ELEMENT__CHILDREN = eINSTANCE.getMergeElement_Children();

		/**
		 * The meta object literal for the '<em><b>Custom Attributes</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ELEMENT__CUSTOM_ATTRIBUTES = eINSTANCE.getMergeElement_CustomAttributes();

		/**
		 * The meta object literal for the '<em><b>Input Diagram Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MERGE_ELEMENT__INPUT_DIAGRAM_INDEX = eINSTANCE.getMergeElement_InputDiagramIndex();

		/**
		 * The meta object literal for the '<em><b>Input Visual Element</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ELEMENT__INPUT_VISUAL_ELEMENT = eINSTANCE.getMergeElement_InputVisualElement();

		/**
		 * The meta object literal for the '<em><b>Input Visual Connection</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ELEMENT__INPUT_VISUAL_CONNECTION = eINSTANCE.getMergeElement_InputVisualConnection();

		/**
		 * The meta object literal for the '<em><b>Resolver</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ELEMENT__RESOLVER = eINSTANCE.getMergeElement_Resolver();

		/**
		 * The meta object literal for the '<em><b>Initialize</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_ELEMENT___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION = eINSTANCE.getMergeElement__Initialize__MergeElementResolver_int_IVisualElement_IVisualConnection();

		/**
		 * The meta object literal for the '<em><b>Initialize For Unit Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_ELEMENT___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING = eINSTANCE.getMergeElement__InitializeForUnitTest__MergeElementResolver_int_String_String_String_String_String();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.impl.MergeElementResolverImpl <em>Merge Element Resolver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.impl.MergeElementResolverImpl
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getMergeElementResolver()
		 * @generated
		 */
		EClass MERGE_ELEMENT_RESOLVER = eINSTANCE.getMergeElementResolver();

		/**
		 * The meta object literal for the '<em><b>Visual Element To ID Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ELEMENT_RESOLVER__VISUAL_ELEMENT_TO_ID_MAP = eINSTANCE.getMergeElementResolver_VisualElementToIDMap();

		/**
		 * The meta object literal for the '<em><b>Visual Connection To ID Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ELEMENT_RESOLVER__VISUAL_CONNECTION_TO_ID_MAP = eINSTANCE.getMergeElementResolver_VisualConnectionToIDMap();

		/**
		 * The meta object literal for the '<em><b>ID To Merge Element Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_ELEMENT_RESOLVER__ID_TO_MERGE_ELEMENT_MAP = eINSTANCE.getMergeElementResolver_IDToMergeElementMap();

		/**
		 * The meta object literal for the '<em><b>Keep Track Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT_IVISUALELEMENT = eINSTANCE.getMergeElementResolver__KeepTrackOf__String_MergeElement_IVisualElement();

		/**
		 * The meta object literal for the '<em><b>Keep Track Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT_IVISUALCONNECTION = eINSTANCE.getMergeElementResolver__KeepTrackOf__String_MergeElement_IVisualConnection();

		/**
		 * The meta object literal for the '<em><b>Keep Track Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT = eINSTANCE.getMergeElementResolver__KeepTrackOf__String_MergeElement();

		/**
		 * The meta object literal for the '<em><b>Lookup Merge Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__IVISUALELEMENT = eINSTANCE.getMergeElementResolver__LookupMergeElement__IVisualElement();

		/**
		 * The meta object literal for the '<em><b>Lookup Merge Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__IVISUALCONNECTION = eINSTANCE.getMergeElementResolver__LookupMergeElement__IVisualConnection();

		/**
		 * The meta object literal for the '<em><b>Lookup Merge Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__STRING = eINSTANCE.getMergeElementResolver__LookupMergeElement__String();

		/**
		 * The meta object literal for the '<em><b>Destroy</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_ELEMENT_RESOLVER___DESTROY = eINSTANCE.getMergeElementResolver__Destroy();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.impl.ConnectionMEImpl <em>Connection ME</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.impl.ConnectionMEImpl
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getConnectionME()
		 * @generated
		 */
		EClass CONNECTION_ME = eINSTANCE.getConnectionME();

		/**
		 * The meta object literal for the '<em><b>Conn End1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_ME__CONN_END1 = eINSTANCE.getConnectionME_ConnEnd1();

		/**
		 * The meta object literal for the '<em><b>Conn End2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_ME__CONN_END2 = eINSTANCE.getConnectionME_ConnEnd2();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.impl.ArrowMEImpl <em>Arrow ME</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.impl.ArrowMEImpl
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getArrowME()
		 * @generated
		 */
		EClass ARROW_ME = eINSTANCE.getArrowME();

		/**
		 * The meta object literal for the '<em><b>Src</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARROW_ME__SRC = eINSTANCE.getArrowME_Src();

		/**
		 * The meta object literal for the '<em><b>Tgt</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARROW_ME__TGT = eINSTANCE.getArrowME_Tgt();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.impl.ControlNodeMEImpl <em>Control Node ME</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.impl.ControlNodeMEImpl
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getControlNodeME()
		 * @generated
		 */
		EClass CONTROL_NODE_ME = eINSTANCE.getControlNodeME();

		/**
		 * The meta object literal for the '<em><b>Incoming</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE_ME__INCOMING = eINSTANCE.getControlNodeME_Incoming();

		/**
		 * The meta object literal for the '<em><b>Outgoing</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE_ME__OUTGOING = eINSTANCE.getControlNodeME_Outgoing();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.impl.InitialNodeMEImpl <em>Initial Node ME</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.impl.InitialNodeMEImpl
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getInitialNodeME()
		 * @generated
		 */
		EClass INITIAL_NODE_ME = eINSTANCE.getInitialNodeME();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.impl.FinalNodeMEImpl <em>Final Node ME</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.impl.FinalNodeMEImpl
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getFinalNodeME()
		 * @generated
		 */
		EClass FINAL_NODE_ME = eINSTANCE.getFinalNodeME();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.CustomAttributesKind <em>Custom Attributes Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.CustomAttributesKind
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getCustomAttributesKind()
		 * @generated
		 */
		EEnum CUSTOM_ATTRIBUTES_KIND = eINSTANCE.getCustomAttributesKind();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.MatchMode <em>Match Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.MatchMode
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getMatchMode()
		 * @generated
		 */
		EEnum MATCH_MODE = eINSTANCE.getMatchMode();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.visualstitch.MatchResult <em>Match Result</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.visualstitch.MatchResult
		 * @see dk.dtu.imm.red.visualstitch.impl.VisualstitchPackageImpl#getMatchResult()
		 * @generated
		 */
		EEnum MATCH_RESULT = eINSTANCE.getMatchResult();

	}

} //VisualstitchPackage
