/**
 */
package dk.dtu.imm.red.visualstitch;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualstitch.processor.MatchKB;
import dk.dtu.imm.red.visualstitch.processor.MergeConflictException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Merge Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getId <em>Id</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getChildren <em>Children</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getCustomAttributes <em>Custom Attributes</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getInputDiagramIndex <em>Input Diagram Index</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getInputVisualElement <em>Input Visual Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getInputVisualConnection <em>Input Visual Connection</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElement#getResolver <em>Resolver</em>}</li>
 * </ul>
 * </p>
 *
 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement()
 * @model
 * @generated
 */
public interface MergeElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.MergeElement#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.MergeElement#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.MergeElement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(MergeElement)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_Parent()
	 * @model
	 * @generated
	 */
	MergeElement getParent();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.MergeElement#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(MergeElement value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualstitch.MergeElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' reference list.
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_Children()
	 * @model
	 * @generated
	 */
	EList<MergeElement> getChildren();

	/**
	 * Returns the value of the '<em><b>Custom Attributes</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Custom Attributes</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Custom Attributes</em>' map.
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_CustomAttributes()
	 * @model mapType="dk.dtu.imm.red.visualstitch.HashMapEntry.StringToStringMap<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString>"
	 * @generated
	 */
	EMap<String, String> getCustomAttributes();

	/**
	 * Returns the value of the '<em><b>Input Diagram Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Diagram Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Diagram Index</em>' attribute.
	 * @see #setInputDiagramIndex(int)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_InputDiagramIndex()
	 * @model
	 * @generated
	 */
	int getInputDiagramIndex();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.MergeElement#getInputDiagramIndex <em>Input Diagram Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Diagram Index</em>' attribute.
	 * @see #getInputDiagramIndex()
	 * @generated
	 */
	void setInputDiagramIndex(int value);

	/**
	 * Returns the value of the '<em><b>Input Visual Element</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Visual Element</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Visual Element</em>' reference list.
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_InputVisualElement()
	 * @model
	 * @generated
	 */
	EList<IVisualElement> getInputVisualElement();

	/**
	 * Returns the value of the '<em><b>Input Visual Connection</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Visual Connection</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Visual Connection</em>' reference list.
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_InputVisualConnection()
	 * @model
	 * @generated
	 */
	EList<IVisualConnection> getInputVisualConnection();

	/**
	 * Returns the value of the '<em><b>Resolver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resolver</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resolver</em>' reference.
	 * @see #setResolver(MergeElementResolver)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElement_Resolver()
	 * @model
	 * @generated
	 */
	MergeElementResolver getResolver();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.MergeElement#getResolver <em>Resolver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resolver</em>' reference.
	 * @see #getResolver()
	 * @generated
	 */
	void setResolver(MergeElementResolver value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void initialize(MergeElementResolver resolver, int inputDiagramIndex, IVisualElement visualElement, IVisualConnection visualConnection);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void initializeForUnitTest(MergeElementResolver resolver, int inputDiagramIndex, String id, String name, String type, String parentID, String customAttributesCommaSeparated);

	/**
	 * @generated NOT
	 */
	public MatchResult equals(MergeElement m2, MatchKB matchKB);

	/**
	 * @generated NOT
	 */
	public int hashValues();

	/**
	 * @throws MergeConflictException
	 * @generated NOT
	 */
	public MatchResult match(MergeElement m2, MatchKB matchKB) throws MergeConflictException;

	/**
	 * @throws MergeConflictException
	 * @generated NOT
	 */
	public MatchResult matchValue(MergeElement m2, MatchKB matchKB) throws MergeConflictException;

	/**
	 * @throws MergeConflictException
	 * @generated NOT
	 */
	public MatchResult matchStructure(MergeElement m2, MatchKB matchKB) throws MergeConflictException;

	/**
	 * @generated NOT
	 */
	public MatchResult threeValOR(MatchResult... results);

	/**
	 * @generated NOT
	 */
	public MatchResult threeValRelaxedAND(MatchResult... results);

	/**
	 * @generated NOT
	 */
	public MatchResult threeValAND(MatchResult... results);

	/**
	 * @generated NOT
	 */
	public boolean isDecisionMergeForkJoin();

	/**
	 * @generated NOT
	 */
	public boolean isInitial();

	/**
	 * @generated NOT
	 */
	public boolean isFinal();

	/**
	 * @generated NOT
	 */
	public boolean isConnection();
	
} // MergeElement
