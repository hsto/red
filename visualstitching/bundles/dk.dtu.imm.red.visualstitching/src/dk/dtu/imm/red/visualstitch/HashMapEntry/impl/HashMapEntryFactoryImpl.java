/**
 */
package dk.dtu.imm.red.visualstitch.HashMapEntry.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

import dk.dtu.imm.red.visualstitch.HashMapEntry.*;

import dk.dtu.imm.red.visualstitch.MergeElement;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HashMapEntryFactoryImpl extends EFactoryImpl implements HashMapEntryFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static HashMapEntryFactory init() {
		try {
			HashMapEntryFactory theHashMapEntryFactory = (HashMapEntryFactory)EPackage.Registry.INSTANCE.getEFactory(HashMapEntryPackage.eNS_URI);
			if (theHashMapEntryFactory != null) {
				return theHashMapEntryFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new HashMapEntryFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HashMapEntryFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case HashMapEntryPackage.STRING_TO_STRING_MAP: return (EObject)createStringToStringMap();
			case HashMapEntryPackage.IVISUAL_ELEMENT_TO_STRING_MAP: return (EObject)createIVisualElementToStringMap();
			case HashMapEntryPackage.IVISUAL_CONNECTION_TO_STRING_MAP: return (EObject)createIVisualConnectionToStringMap();
			case HashMapEntryPackage.STRING_TO_MERGE_ELEMENT_MAP: return (EObject)createStringToMergeElementMap();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, String> createStringToStringMap() {
		StringToStringMapImpl stringToStringMap = new StringToStringMapImpl();
		return stringToStringMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<IVisualElement, String> createIVisualElementToStringMap() {
		IVisualElementToStringMapImpl iVisualElementToStringMap = new IVisualElementToStringMapImpl();
		return iVisualElementToStringMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<IVisualConnection, String> createIVisualConnectionToStringMap() {
		IVisualConnectionToStringMapImpl iVisualConnectionToStringMap = new IVisualConnectionToStringMapImpl();
		return iVisualConnectionToStringMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, MergeElement> createStringToMergeElementMap() {
		StringToMergeElementMapImpl stringToMergeElementMap = new StringToMergeElementMapImpl();
		return stringToMergeElementMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HashMapEntryPackage getHashMapEntryPackage() {
		return (HashMapEntryPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static HashMapEntryPackage getPackage() {
		return HashMapEntryPackage.eINSTANCE;
	}

} //HashMapEntryFactoryImpl
