package dk.dtu.imm.red.visualstitch.processor;

public class Tuple<TYPE> {
	public final TYPE x;
	public final TYPE y;

	public Tuple(TYPE x, TYPE y) {
		this.x = x;
		this.y = y;
	}

	public TYPE getX() {
		return x;
	}

	public TYPE getY() {
		return y;
	}

}