package dk.dtu.imm.red.visualstitch.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.modelelement.datatype.util.DataTypeParser;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.util.MultiplicityParser;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchConnectionRelationship;
import dk.dtu.imm.red.visualmodeling.visualmodel.StitchElementRelationship;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState;
import dk.dtu.imm.red.visualstitch.ArrowME;
import dk.dtu.imm.red.visualstitch.ConnectionME;
import dk.dtu.imm.red.visualstitch.CustomAttributesKind;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.MergeElementResolver;

public class MergeOutputWriter {

	private MergeExecutor executor;
	private MergeLogger mergeLogger = MergeLogger.instance;

	//create a map for temporarily storing all the output element
	//so that it can be used for setting connections
	private Map<String, IVisualElement> mergeElementIDToOutputVisualElementMap = new HashMap<String,IVisualElement>();
	private VisualDiagram outputDiagram = visualmodelFactory.eINSTANCE.createVisualDiagram();
	private MergeElementResolver resolver;

	private Point furthestPointInFirstDiagram=null;
	private Point nearestPointInSecondDiagram= null;

	private Set<MergeElement> outputSet;

	public MergeOutputWriter(MergeExecutor executor, Set<MergeElement> outputSet, Point furthestPointInFirstDiagram, Point nearestPointInSecondDiagram){
		this.executor = executor;
		this.resolver = executor.getResolver();
		this.outputSet = outputSet;

		//pass in the furthest and nearest point for auto layout purpose
		this.furthestPointInFirstDiagram = furthestPointInFirstDiagram;
		this.nearestPointInSecondDiagram = nearestPointInSecondDiagram;
	}

	public VisualDiagram execute(){
		mergeLogger.append(MergeLogger.INFO, "Number of output elements: " + outputSet.size());
		for(MergeElement m : outputSet){
			//populate the elements
			if(m.getParent()==null){
				if(m.getInputVisualElement().size()>0){
					IVisualElement outputVisualElement = convertMergeElementToVisualElement(m);
					outputDiagram.getElements().add(outputVisualElement);
					outputVisualElement.setParent(outputDiagram);
					mergeElementIDToOutputVisualElementMap.put(m.getId(), outputVisualElement);
					recursiveSave(m,outputVisualElement);
				}
			}
		}

		//populate the connections
		for(MergeElement m : outputSet){
			if(m.getInputVisualConnection().size()>0){
				IVisualConnection outputVisualConnection =convertMergeElementToVisualConnection(m);
				outputDiagram.getDiagramConnections().add(outputVisualConnection);
				String source = "";
				String target = "";
				if(m instanceof ConnectionME){
					source = ((ConnectionME)m).getConnEnd1().getId();
					target = ((ConnectionME)m).getConnEnd2().getId();
				}
				else if(m instanceof ArrowME){
					source = ((ArrowME)m).getSrc().getId();
					target = ((ArrowME)m).getTgt().getId();
				}

				IVisualElement sourceElement = mergeElementIDToOutputVisualElementMap.get(source);
				IVisualElement targetElement = mergeElementIDToOutputVisualElementMap.get(target);
				outputVisualConnection.setSource(sourceElement);
				sourceElement.getConnections().add(outputVisualConnection);
				outputVisualConnection.setTarget(targetElement);
				targetElement.getConnections().add(outputVisualConnection);
			}
		}


		mergeElementIDToOutputVisualElementMap.clear();
		resolver = null;
		return outputDiagram;
	}


	private void recursiveSave(MergeElement node,IVisualElement parentVisualElement){
		List<MergeElement> children = node.getChildren();
		if(children==null)
			return;
		for(MergeElement child : children){
			if(child.getInputVisualElement().size()>0){
				IVisualElement output = convertMergeElementToVisualElement(child);
				parentVisualElement.getElements().add(output);
				output.setParent(parentVisualElement);
				mergeElementIDToOutputVisualElementMap.put(child.getId(), output);
				recursiveSave(child,output);
			}

		}
	}

	private IVisualElement convertMergeElementToVisualElement(MergeElement mergeElement){
		IVisualElement srcVisualElement = mergeElement.getInputVisualElement().get(0);
		IVisualElement visualElement = EcoreUtil.copy(srcVisualElement);
		visualElement.setVisualID(EcoreUtil.generateUUID());
		visualElement.getStitchFrom().clear();
		visualElement.getStitchTo().clear();
		visualElement.setParent(null);
		visualElement.getElements().clear();
		visualElement.getConnections().clear();
		visualElement.setDiagram(outputDiagram);

		if (srcVisualElement instanceof VisualSpecificationElement) {
			SpecificationElement specEl = ((VisualSpecificationElement) srcVisualElement).getSpecificationElement();
			if(specEl!=null){
				visualElement.setName(specEl.getName());
			}
			else{
				visualElement.setName(srcVisualElement.getName());
			}
		} else {
			visualElement.setName(srcVisualElement.getName());
		}

		copyMergeElCustAttrToVisElement(mergeElement, visualElement);
		copyInputVisualElementAsTraceOrigin(mergeElement, visualElement);

		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_AUTO_LAYOUT)){
			//move the elements from the second diagram by a few pixels from the furthest point of the first diagram

			if(mergeElement.getInputDiagramIndex()==2 && mergeElement.getParent()==null){
				performHorizontalCoordinateTranslation(visualElement);
			}
		}
		return visualElement;
	}

	private void copyInputVisualElementAsTraceOrigin(MergeElement mergeElement,
			IVisualElement visualElement) {
		if(mergeElement.getInputVisualElement().size()>0){
			for(IVisualElement origin : mergeElement.getInputVisualElement()){
				StitchElementRelationship stitchRelationship = visualmodelFactory.eINSTANCE.createStitchElementRelationship();
				stitchRelationship.setStitchOrigin(origin);
				stitchRelationship.setStitchOutput(visualElement);
				visualElement.getStitchFrom().add(stitchRelationship);
			}
		}
	}

	private void copyMergeElCustAttrToVisElement(MergeElement mergeElement,
			IVisualElement visualElement) {
		Map<String,String> custAttr = mergeElement.getCustomAttributes().map();
		if(visualElement instanceof VisualClassAttribute){
			VisualClassAttribute attr = (VisualClassAttribute) visualElement;
			for(String key : custAttr.keySet()){
				if(key.equals(CustomAttributesKind.VISIBILITY.getName())){
					attr.setVisibility(Visibility.getByName(custAttr.get(key)));
				}
				else if(key.equals(CustomAttributesKind.MULTIPLICITY.getName())){
					attr.setMultiplicity(MultiplicityParser.parse(custAttr.get(key)));
				}
			}
		}
		else if(visualElement instanceof VisualClassOperation){
			VisualClassOperation op = (VisualClassOperation) visualElement;
			for(String key : custAttr.keySet()){
				if(key.equals(CustomAttributesKind.VISIBILITY.getName())){
					op.setVisibility(Visibility.getByName(custAttr.get(key)));
				}
				else if(key.equals(CustomAttributesKind.MULTIPLICITY.getName())){
					op.setMultiplicity(MultiplicityParser.parse(custAttr.get(key)));
				}
				else if(key.equals(CustomAttributesKind.RETURN_TYPE.getName())){
					op.setType(DataTypeParser.parse(custAttr.get(key)));
				}
				else if(key.equals(CustomAttributesKind.PARAMETERS.getName())){
					op.setParameters(DataTypeParser.parse(custAttr.get(key)));
				}
			}
		}
		else if(visualElement instanceof VisualState){
			VisualState state = (VisualState) visualElement;
			for(String key : custAttr.keySet()){
				if(key.equals(CustomAttributesKind.ENTRY.getName())){
					state.setEntry(custAttr.get(key));
				}
				else if(key.equals(CustomAttributesKind.EXIT.getName())){
					state.setExit(custAttr.get(key));
				}
				else if(key.equals(CustomAttributesKind.DO.getName())){
					state.setDo(custAttr.get(key));
				}
			}
		}
	}



	private IVisualConnection convertMergeElementToVisualConnection(MergeElement mergeElement){
		IVisualConnection visualConnection = EcoreUtil.copy(mergeElement.getInputVisualConnection().get(0));
		visualConnection.setVisualID(EcoreUtil.generateUUID());
		visualConnection.getStitchFrom().clear();
		visualConnection.getStitchTo().clear();
		boolean isSelfConnect=false;
		if(visualConnection.getSource()==visualConnection.getTarget())
			isSelfConnect = true;
		visualConnection.setSource(null);
		visualConnection.setTarget(null);

		copyMergeElCustAttrToVisualConn(mergeElement, visualConnection);
		copyInputVisualConnectionAsTraceOrigin(mergeElement, visualConnection);

		List<Point> bendpointList = new ArrayList<Point>();
		for(Point bendpoint: visualConnection.getBendpoints()){
			Point p = new Point(bendpoint.x, bendpoint.y);
			bendpointList.add(p);
		}
		visualConnection.getBendpoints().clear();

		if(isSelfConnect){
			//copy the bendpoints only if it is a connection to itself, otherwise just
			//discard it because it is causing lots of noise
			visualConnection.getBendpoints().addAll(bendpointList);
		}

		if(PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_AUTO_LAYOUT)){
			//move the elements from the second diagram by a few pixels from the furthest point of the first diagram
			if(mergeElement.getInputDiagramIndex()==2){
				performHorizontalCoordinateTranslation(visualConnection);
			}
		}
		return visualConnection;
	}

	private void copyInputVisualConnectionAsTraceOrigin(MergeElement mergeElement,
			IVisualConnection visualConnection) {
		if(mergeElement.getInputVisualConnection().size()>0){
			for(IVisualConnection origin : mergeElement.getInputVisualConnection()){
				StitchConnectionRelationship stitchRelationship = visualmodelFactory.eINSTANCE.createStitchConnectionRelationship();
				stitchRelationship.setStitchOrigin(origin);
				stitchRelationship.setStitchOutput(visualConnection);
				visualConnection.getStitchFrom().add(stitchRelationship);
			}
		}
	}

	private void copyMergeElCustAttrToVisualConn(MergeElement sourceME,
			IVisualConnection visualConnection) {
		Map<String,String> custAttr = sourceME.getCustomAttributes().map();
		for(String key : custAttr.keySet()){
			if(key.equals(CustomAttributesKind.MULTIPLICITY_SRC.getName())){
				visualConnection.setSourceMultiplicity(custAttr.get(key));
			}
			else if(key.equals(CustomAttributesKind.MULTIPLICITY_TGT.getName())){
				visualConnection.setTargetMultiplicity(custAttr.get(key));
			}
		}
	}


	private void performHorizontalCoordinateTranslation(IVisualElement element){
		int coordTranslationXAmount = furthestPointInFirstDiagram.x - nearestPointInSecondDiagram.x;

		element.setLocation(new Point(element.getLocation().x + coordTranslationXAmount
				,element.getLocation().y ));
	}

	private void performHorizontalCoordinateTranslation(IVisualConnection connection){
		int coordTranslationXAmount = furthestPointInFirstDiagram.x - nearestPointInSecondDiagram.x;

		for(Point bendpoint : connection.getBendpoints()){
			bendpoint.setLocation(new Point(bendpoint.x + coordTranslationXAmount
				,bendpoint.y ));
		}
	}

	public void destroy(){
		mergeElementIDToOutputVisualElementMap.clear();
		outputDiagram=null;
		outputSet.clear();
	}
}
