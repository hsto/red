/**
 */
package dk.dtu.imm.red.visualstitch.impl;

import org.eclipse.emf.ecore.EClass;

import dk.dtu.imm.red.visualstitch.ControlNodeME;
import dk.dtu.imm.red.visualstitch.InitialNodeME;
import dk.dtu.imm.red.visualstitch.MatchMode;
import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.VisualstitchPackage;
import dk.dtu.imm.red.visualstitch.processor.MatchKB;
import dk.dtu.imm.red.visualstitch.processor.MergeConflictException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Initial Node ME</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class InitialNodeMEImpl extends ControlNodeMEImpl implements InitialNodeME {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected InitialNodeMEImpl() {
		super();
		getIncoming();
		getOutgoing();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualstitchPackage.Literals.INITIAL_NODE_ME;
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		return super.toString().replaceAll("ControlNodeME", "InitialNodeME");
	}


	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult matchStructure(MergeElement m2, MatchKB matchKB) throws MergeConflictException {

		MatchResult outgoingEqual  = matchSet(this.getOutgoing(), ((ControlNodeME)m2).getOutgoing(),
				matchKB, MatchMode.AT_LEAST_ONE);

		return 	outgoingEqual;

	}

} //InitialNodeMEImpl
