package dk.dtu.imm.red.visualstitch.processor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.MergeElementResolver;

//KnowledgeBase about the matching of elements
public class MatchKB {
	List<Set<MergeElement>> listOfMatchingSet = new ArrayList<Set<MergeElement>>();

	//positive matches mapping, eg A1 --> A1, A2, A3, A4 where all of them are A
	private Map<MergeElement, Set<MergeElement>> mappingToSetOfMatches = new LinkedHashMap<MergeElement, Set<MergeElement>>();

	//negative matches mapping, eg A1 --> B1, B2, C1 where A1 is a non match to all of those
	private Map<MergeElement, Set<MergeElement>> mappingToSetOfNonMatches = new LinkedHashMap<MergeElement, Set<MergeElement>>();

	private MergeElementResolver resolver;

	public MatchKB(MergeElementResolver resolver) {
		this.resolver = resolver;

	}

	public MergeElementResolver getResolver() {
		return resolver;
	}

	public List<Set<MergeElement>> getListOfMatchingSet() {
		return listOfMatchingSet;
	}

	//only used for testing
	public void addMatchAndNonMatchToKB(List<Tuple<MergeElement>> listOfMatchingTuple,
			List<Tuple<MergeElement>> listOfNonMatchingTuple){
		for(Tuple<MergeElement> tuple : listOfMatchingTuple){
			addMatchingTuple(tuple);
		}
		for(Tuple<MergeElement> tuple : listOfNonMatchingTuple){
			addNonMatchingTuple(tuple);
		}

	}

	public void addMatchingTuple(Tuple<MergeElement> tuple){
		addTupleToMapping(tuple,mappingToSetOfMatches);
		addTupleToListOfMatchingSet(tuple);
	}

	public void addNonMatchingTuple(Tuple<MergeElement> tuple){
		addTupleToMapping(tuple,mappingToSetOfNonMatches);
	}


	private void addTupleToListOfMatchingSet(Tuple<MergeElement> tuple){
		boolean matchingSetFound = false;
		for(Set<MergeElement>  matchingSet : listOfMatchingSet){
			if(matchingSet.contains(tuple.getX())){
				matchingSet.add(tuple.getY());
				matchingSetFound=true;
			}
			else if(matchingSet.contains(tuple.getY())){
				matchingSet.add(tuple.getX());
				matchingSetFound=true;
			}
		}
		if(!matchingSetFound){
			Set<MergeElement> matchingSet = new LinkedHashSet<MergeElement>();
			matchingSet.add(tuple.getX());
			matchingSet.add(tuple.getY());
			listOfMatchingSet.add(matchingSet);
		}
	}

	private void addTupleToMapping(
			Tuple<MergeElement> tuple,
			Map<MergeElement, Set<MergeElement>> mappingToInsert){

		MergeElement x = tuple.getX();
		MergeElement y = tuple.getY();

		Set<MergeElement> set=null;
		if(!mappingToInsert.containsKey(x)){
			set = new HashSet<MergeElement>();
			set.add(y);
			mappingToInsert.put(x, set);
		}
		else{
			set = mappingToInsert.get(x);
			set.add(y);
		}

		if(!mappingToInsert.containsKey(y)){
			set = new HashSet<MergeElement>();
			set.add(x);
			mappingToInsert.put(y, set);
		}
		else{
			set = mappingToInsert.get(y);
			set.add(x);
		}
	}

	// check if m1 and m2 is a match
	public MatchResult isAMatch(MergeElement m1, MergeElement m2) {
		Set<MergeElement> setOfMatches = mappingToSetOfMatches.get(m1);
		Set<MergeElement> setOfNonMatches = mappingToSetOfNonMatches.get(m1);
		if(setOfMatches!=null && setOfMatches.contains(m2)){
			return MatchResult.TRUE;
		}
		else if(setOfNonMatches!=null && setOfNonMatches.contains(m2)){
			return MatchResult.FALSE;
		}
		else{
			return MatchResult.UNKNOWN;
		}
	}

	public void destroy() {
		listOfMatchingSet.clear();

		mappingToSetOfMatches.clear();
		mappingToSetOfNonMatches.clear();
	}

	public String getListOfMatchingSetString(){
		StringBuffer sb= new StringBuffer();
		sb.append("Final list of Matching sets:" + MergeLogger.NL);
		Iterator<Set<MergeElement>> listOfMatchingSetIter = listOfMatchingSet.iterator();
		while(listOfMatchingSetIter.hasNext()){
			Set<MergeElement> matchingSet = listOfMatchingSetIter.next();
			sb.append(matchingSet.iterator().next().getType());
			sb.append("{");
			Iterator<MergeElement> matchingSetIter = matchingSet.iterator();
			while(matchingSetIter.hasNext()){
				MergeElement el = matchingSetIter.next();
				sb.append(el.getId()+"("+el.getName()+")");
				if(matchingSetIter.hasNext())
					sb.append(",");
			}
			sb.append("}"+MergeLogger.NL);
		}
		return sb.toString();
	}

	public String toString(){
		StringBuffer sb= new StringBuffer();
		sb.append("MappingToSetOfMatches:\n");
		for(MergeElement key : mappingToSetOfMatches.keySet()){
			sb.append(key.getId()+"--> [");
			for(MergeElement values : mappingToSetOfMatches.get(key)){
				sb.append(values.getId()+",");
			}
			sb.append("]\n");
		}
		sb.append("MappingToSetOfNonMatches:\n");
		for(MergeElement key : mappingToSetOfNonMatches.keySet()){
			sb.append(key.getId()+"--> [");
			for(MergeElement values : mappingToSetOfNonMatches.get(key)){
				sb.append(values.getId()+",");
			}
			sb.append("]\n");
		}

		return sb.toString();
	}
}
