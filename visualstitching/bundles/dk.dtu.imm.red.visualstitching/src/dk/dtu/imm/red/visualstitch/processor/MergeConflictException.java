package dk.dtu.imm.red.visualstitch.processor;

import java.util.List;

import dk.dtu.imm.red.visualmodeling.visualmodel.Diagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualstitch.MergeElement;

public class MergeConflictException extends Exception {
	private static final long serialVersionUID = 1L;
	private String NL = System.getProperty("line.separator");

	List<MergeElement> conflictSource;
	String message = "";
	String conflictReason="";

	public MergeConflictException() {
		super();
	}

	public MergeConflictException(String message) {
        super(message);
        this.message = message;
	}

	public MergeConflictException(List<MergeElement> conflictSource, String conflictReason){
        super(conflictReason);
        this.conflictReason = conflictReason;
		this.conflictSource=conflictSource;
		StringBuffer msgBuff = new StringBuffer();
		msgBuff.append(NL);
		msgBuff.append("The following elements have merge conflict due to " + conflictReason + ":" + NL);
		for(MergeElement confSource : conflictSource){
			String name="";
			String type="";
			Diagram inputDiagram;
			String inputDiagramLabel="";
			String parentName="";
			if(confSource.getInputVisualElement()!=null && confSource.getInputVisualElement().size()>0){
				IVisualElement inputVisualElement= confSource.getInputVisualElement().get(0);
				name = inputVisualElement.getName();
				type = inputVisualElement.getClass().getSimpleName().replaceAll("Impl", "");

				IVisualElement parentElement = inputVisualElement.getParent();
				parentName = parentElement.getName();

				if(inputVisualElement.getDiagram()==null)
					inputDiagramLabel = parentElement.getDiagram().getDiagramElement().getLabel();
				else
					inputDiagramLabel = inputVisualElement.getDiagram().getDiagramElement().getLabel();
			}
			msgBuff.append(type + " \""+name + "\" which has parent \"" + parentName + "\" from diagram " + inputDiagramLabel+NL);
		}
		this.message = msgBuff.toString();
	}

	@Override
	public String toString(){
		return message;
	}
}
