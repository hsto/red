/**
 */
package dk.dtu.imm.red.visualstitch.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.specificationelements.SpecificationElement;
import dk.dtu.imm.red.specificationelements.modelelement.multiplicity.NoMultiplicity;
import dk.dtu.imm.red.specificationelements.modelelement.signature.Visibility;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualSpecificationElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassAttribute;
import dk.dtu.imm.red.visualmodeling.visualmodel.class_.VisualClassOperation;
import dk.dtu.imm.red.visualmodeling.visualmodel.statemachine.VisualState;
import dk.dtu.imm.red.visualmodeling.visualmodel.systemstructure.VisualSystemStructureActor;
import dk.dtu.imm.red.visualstitch.ArrowME;
import dk.dtu.imm.red.visualstitch.ConnectionME;
import dk.dtu.imm.red.visualstitch.ControlNodeME;
import dk.dtu.imm.red.visualstitch.CustomAttributesKind;
import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.MergeElementResolver;
import dk.dtu.imm.red.visualstitch.VisualstitchPackage;
import dk.dtu.imm.red.visualstitch.HashMapEntry.HashMapEntryPackage;
import dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToStringMapImpl;
import dk.dtu.imm.red.visualstitch.processor.MatchKB;
import dk.dtu.imm.red.visualstitch.processor.MergeConflictException;
import dk.dtu.imm.red.visualstitch.processor.MergeElementType;
import dk.dtu.imm.red.visualstitch.processor.MergeLogger;
import dk.dtu.imm.red.visualstitch.processor.MergeUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Merge Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getId <em>Id</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getCustomAttributes <em>Custom Attributes</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getInputDiagramIndex <em>Input Diagram Index</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getInputVisualElement <em>Input Visual Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getInputVisualConnection <em>Input Visual Connection</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementImpl#getResolver <em>Resolver</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MergeElementImpl extends MinimalEObjectImpl.Container implements MergeElement {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected MergeElement parent;

	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<MergeElement> children;

	/**
	 * The cached value of the '{@link #getCustomAttributes() <em>Custom Attributes</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomAttributes()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> customAttributes;

	/**
	 * The default value of the '{@link #getInputDiagramIndex() <em>Input Diagram Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputDiagramIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int INPUT_DIAGRAM_INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getInputDiagramIndex() <em>Input Diagram Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputDiagramIndex()
	 * @generated
	 * @ordered
	 */
	protected int inputDiagramIndex = INPUT_DIAGRAM_INDEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInputVisualElement() <em>Input Visual Element</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputVisualElement()
	 * @generated
	 * @ordered
	 */
	protected EList<IVisualElement> inputVisualElement;

	/**
	 * The cached value of the '{@link #getInputVisualConnection() <em>Input Visual Connection</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputVisualConnection()
	 * @generated
	 * @ordered
	 */
	protected EList<IVisualConnection> inputVisualConnection;

	/**
	 * The cached value of the '{@link #getResolver() <em>Resolver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResolver()
	 * @generated
	 * @ordered
	 */
	protected MergeElementResolver resolver;

	/**
	 * @generated NOT
	 */
	protected static long ID_RUNNING_NUMBER = 1L;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	protected MergeElementImpl() {
		super();
		// create the custom attributes map
		getCustomAttributes();
		getInputVisualElement();
		getInputVisualConnection();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualstitchPackage.Literals.MERGE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.MERGE_ELEMENT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.MERGE_ELEMENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.MERGE_ELEMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = (MergeElement)eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisualstitchPackage.MERGE_ELEMENT__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(MergeElement newParent) {
		MergeElement oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.MERGE_ELEMENT__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MergeElement> getChildren() {
		if (children == null) {
			children = new EObjectResolvingEList<MergeElement>(MergeElement.class, this, VisualstitchPackage.MERGE_ELEMENT__CHILDREN);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getCustomAttributes() {
		if (customAttributes == null) {
			customAttributes = new EcoreEMap<String,String>(HashMapEntryPackage.Literals.STRING_TO_STRING_MAP, StringToStringMapImpl.class, this, VisualstitchPackage.MERGE_ELEMENT__CUSTOM_ATTRIBUTES);
		}
		return customAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInputDiagramIndex() {
		return inputDiagramIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputDiagramIndex(int newInputDiagramIndex) {
		int oldInputDiagramIndex = inputDiagramIndex;
		inputDiagramIndex = newInputDiagramIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.MERGE_ELEMENT__INPUT_DIAGRAM_INDEX, oldInputDiagramIndex, inputDiagramIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IVisualElement> getInputVisualElement() {
		if (inputVisualElement == null) {
			inputVisualElement = new EObjectResolvingEList<IVisualElement>(IVisualElement.class, this, VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_ELEMENT);
		}
		return inputVisualElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IVisualConnection> getInputVisualConnection() {
		if (inputVisualConnection == null) {
			inputVisualConnection = new EObjectResolvingEList<IVisualConnection>(IVisualConnection.class, this, VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_CONNECTION);
		}
		return inputVisualConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElementResolver getResolver() {
		if (resolver != null && resolver.eIsProxy()) {
			InternalEObject oldResolver = (InternalEObject)resolver;
			resolver = (MergeElementResolver)eResolveProxy(oldResolver);
			if (resolver != oldResolver) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VisualstitchPackage.MERGE_ELEMENT__RESOLVER, oldResolver, resolver));
			}
		}
		return resolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElementResolver basicGetResolver() {
		return resolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResolver(MergeElementResolver newResolver) {
		MergeElementResolver oldResolver = resolver;
		resolver = newResolver;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualstitchPackage.MERGE_ELEMENT__RESOLVER, oldResolver, resolver));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT__CUSTOM_ATTRIBUTES:
				return ((InternalEList<?>)getCustomAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT__ID:
				return getId();
			case VisualstitchPackage.MERGE_ELEMENT__TYPE:
				return getType();
			case VisualstitchPackage.MERGE_ELEMENT__NAME:
				return getName();
			case VisualstitchPackage.MERGE_ELEMENT__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case VisualstitchPackage.MERGE_ELEMENT__CHILDREN:
				return getChildren();
			case VisualstitchPackage.MERGE_ELEMENT__CUSTOM_ATTRIBUTES:
				if (coreType) return getCustomAttributes();
				else return getCustomAttributes().map();
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_DIAGRAM_INDEX:
				return getInputDiagramIndex();
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_ELEMENT:
				return getInputVisualElement();
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_CONNECTION:
				return getInputVisualConnection();
			case VisualstitchPackage.MERGE_ELEMENT__RESOLVER:
				if (resolve) return getResolver();
				return basicGetResolver();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT__ID:
				setId((String)newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__TYPE:
				setType((String)newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__NAME:
				setName((String)newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__PARENT:
				setParent((MergeElement)newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__CHILDREN:
				getChildren().clear();
				getChildren().addAll((Collection<? extends MergeElement>)newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__CUSTOM_ATTRIBUTES:
				((EStructuralFeature.Setting)getCustomAttributes()).set(newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_DIAGRAM_INDEX:
				setInputDiagramIndex((Integer)newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_ELEMENT:
				getInputVisualElement().clear();
				getInputVisualElement().addAll((Collection<? extends IVisualElement>)newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_CONNECTION:
				getInputVisualConnection().clear();
				getInputVisualConnection().addAll((Collection<? extends IVisualConnection>)newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__RESOLVER:
				setResolver((MergeElementResolver)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT__ID:
				setId(ID_EDEFAULT);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__PARENT:
				setParent((MergeElement)null);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__CHILDREN:
				getChildren().clear();
				return;
			case VisualstitchPackage.MERGE_ELEMENT__CUSTOM_ATTRIBUTES:
				getCustomAttributes().clear();
				return;
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_DIAGRAM_INDEX:
				setInputDiagramIndex(INPUT_DIAGRAM_INDEX_EDEFAULT);
				return;
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_ELEMENT:
				getInputVisualElement().clear();
				return;
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_CONNECTION:
				getInputVisualConnection().clear();
				return;
			case VisualstitchPackage.MERGE_ELEMENT__RESOLVER:
				setResolver((MergeElementResolver)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case VisualstitchPackage.MERGE_ELEMENT__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case VisualstitchPackage.MERGE_ELEMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case VisualstitchPackage.MERGE_ELEMENT__PARENT:
				return parent != null;
			case VisualstitchPackage.MERGE_ELEMENT__CHILDREN:
				return children != null && !children.isEmpty();
			case VisualstitchPackage.MERGE_ELEMENT__CUSTOM_ATTRIBUTES:
				return customAttributes != null && !customAttributes.isEmpty();
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_DIAGRAM_INDEX:
				return inputDiagramIndex != INPUT_DIAGRAM_INDEX_EDEFAULT;
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_ELEMENT:
				return inputVisualElement != null && !inputVisualElement.isEmpty();
			case VisualstitchPackage.MERGE_ELEMENT__INPUT_VISUAL_CONNECTION:
				return inputVisualConnection != null && !inputVisualConnection.isEmpty();
			case VisualstitchPackage.MERGE_ELEMENT__RESOLVER:
				return resolver != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case VisualstitchPackage.MERGE_ELEMENT___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION:
				initialize((MergeElementResolver)arguments.get(0), (Integer)arguments.get(1), (IVisualElement)arguments.get(2), (IVisualConnection)arguments.get(3));
				return null;
			case VisualstitchPackage.MERGE_ELEMENT___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING:
				initializeForUnitTest((MergeElementResolver)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer("MergeElement");
		result.append(" (id: ");
		result.append(id);
		result.append(", diagram index: ");
		result.append(inputDiagramIndex);
		result.append(", type: ");
		result.append(type);
		result.append(", name: ");
		result.append(name);
		result.append(", parent: ");
		result.append((getParent() != null ? getParent().getId() : ""));
		result.append(", children: ");

		StringBuffer childrenID = new StringBuffer();
		childrenID.append("[");
		Iterator<MergeElement> iter = getChildren().iterator();
		while (iter.hasNext()) {
			childrenID.append(iter.next().getId());
			if (iter.hasNext()) {
				childrenID.append(",");
			}
		}
		childrenID.append("]");
		result.append(childrenID.toString());
		result.append(", CustomAttributes: ");
		result.append(getCustomAttributes());

		if(inputVisualElement.size()>0){
			result.append(", inputVisualElementSize: " + getInputVisualElement().size());
		}
		if(inputVisualConnection.size()>0){
			result.append(", inputVisualConnectionSize: " + getInputVisualConnection().size());
		}

		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	public static void resetIDRunningNumber() {
		ID_RUNNING_NUMBER = 0;
	}


	/**
	 * @generated NOT
	 */
	//INITIALIZE METHOD BASED ON DIAGRAM'S VISUAL ELEMENT/VISUAL CONNECTION
	public void initialize(MergeElementResolver resolver,
			int inputDiagramIndex, IVisualElement visualElement,
			IVisualConnection visualConnection) {
		this.resolver = resolver;
		this.inputDiagramIndex = inputDiagramIndex;
		this.setId(String.valueOf(ID_RUNNING_NUMBER++));

		if (visualElement != null) {
			if (visualElement instanceof VisualSpecificationElement) {
				VisualSpecificationElement vse = ((VisualSpecificationElement) visualElement);
				if(vse!=null && vse.getSpecificationElement()!=null){
					SpecificationElement specEl = vse.getSpecificationElement();
					this.name = specEl.getName();
				}
				else if(vse!=null && vse.getName()!=null && !vse.getName().isEmpty())
					this.name = vse.getName();
			} else {
				this.name = visualElement.getName();
			}
			this.type = MergeUtil.getTypeFromVisualElement(visualElement);
			resolver.keepTrackOf(this.id, this, visualElement);

			// find the parent visual element for setting the parent merge
			// element
			IVisualElement parentVisualElement = visualElement.getParent();
			MergeElement parentMergeElement =
					resolver.lookupMergeElement(parentVisualElement);
			if (parentMergeElement != null) {
				setParent(parentMergeElement);
				parentMergeElement.getChildren().add(this);
			}

			this.inputVisualElement.add(visualElement);
			populateCustomAttributes(inputVisualElement.get(0));
		} else if (visualConnection != null) {
			this.name = "";
			this.type = MergeUtil.getTypeFromVisualConnection(visualConnection);

			resolver.keepTrackOf(this.id, this, visualConnection);
			this.inputVisualConnection.add(visualConnection);

			MergeElement srcME = resolver.lookupMergeElement(visualConnection.getSource());
			MergeElement tgtME = resolver.lookupMergeElement(visualConnection.getTarget());

			if(this instanceof ConnectionME){
				ConnectionME connMe = (ConnectionME) this;
				connMe.setConnEnd1(srcME);
				connMe.setConnEnd2(tgtME);
			}
			else if(this instanceof ArrowME){
				ArrowME arrowME = (ArrowME) this;
				arrowME.setSrc(srcME);
				arrowME.setTgt(tgtME);
			}

			if(srcME instanceof ControlNodeME){
				((ControlNodeME)srcME).getOutgoing().add(this);
			}

			if(tgtME instanceof ControlNodeME){
				((ControlNodeME)tgtME).getIncoming().add(this);
			}
			populateCustomAttributes(inputVisualConnection.get(0));
		}

	}

	/**
	 * @generated NOT
	 */
	private void populateCustomAttributes(IVisualElement element){
		//TODO: Add the implementation for every different type of visual element

		if(element instanceof VisualClassAttribute){
			VisualClassAttribute attr = (VisualClassAttribute) element;
			if(attr.getVisibility()!=Visibility.UNSPECIFIED)
				customAttributes.put(CustomAttributesKind.VISIBILITY.getName(), attr.getVisibility().getName());
			if(!(attr.getMultiplicity() instanceof NoMultiplicity))
				customAttributes.put(CustomAttributesKind.MULTIPLICITY.getName(), attr.getMultiplicity().toString());
		}
		else if(element instanceof VisualClassOperation){
			VisualClassOperation op = (VisualClassOperation) element;
			if(op.getVisibility()!=Visibility.UNSPECIFIED)
				customAttributes.put(CustomAttributesKind.VISIBILITY.getName(), op.getVisibility().getName());
			if(!(op.getMultiplicity() instanceof NoMultiplicity))
				customAttributes.put(CustomAttributesKind.MULTIPLICITY.getName(), op.getMultiplicity().toString());
			customAttributes.put(CustomAttributesKind.RETURN_TYPE.getName(), op.getType().toString());
			customAttributes.put(CustomAttributesKind.PARAMETERS.getName(), op.getParameters().toString());
		}
		else if(element instanceof VisualState){
			VisualState state = (VisualState) element;
			customAttributes.put(CustomAttributesKind.ENTRY.getName(), state.getEntry());
			customAttributes.put(CustomAttributesKind.EXIT.getName(), state.getExit());
			customAttributes.put(CustomAttributesKind.DO.getName(), state.getDo());
		}
		else if(element instanceof VisualSystemStructureActor){
			VisualSystemStructureActor actor = (VisualSystemStructureActor) element;
			customAttributes.put(CustomAttributesKind.ACTOR_KIND.getName(), actor.getActorKind().getName());
		}
	}

	/**
	 * @generated NOT
	 */
	private void populateCustomAttributes(IVisualConnection connection){
		//TODO: Add the implementation for every different type of visual connection
		if(connection.getSourceMultiplicity()!=null && !connection.getSourceMultiplicity().isEmpty())
			customAttributes.put(CustomAttributesKind.MULTIPLICITY_SRC.getName(), connection.getSourceMultiplicity());
		if(connection.getTargetMultiplicity()!=null && !connection.getTargetMultiplicity().isEmpty())
			customAttributes.put(CustomAttributesKind.MULTIPLICITY_TGT.getName(), connection.getTargetMultiplicity());
	}

	/**
	 * @generated NOT
	 */
	public void initializeForUnitTest(MergeElementResolver resolver,
			int inputDiagramIndex, String id, String name, String type,
			String parentID, String customAttributesSeparated) {
		this.resolver = resolver;
		this.inputDiagramIndex = inputDiagramIndex;
		this.id = id;
		this.name = name;
		this.type = type;
		resolver.keepTrackOf(this.id, this);

		String[] customAttributesArr = customAttributesSeparated
				.split(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
		for (int i = 0; i < customAttributesArr.length; i++) {
			String[] keyValuePair = customAttributesArr[i].split(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			if (keyValuePair.length == 2) {
				customAttributes.put(keyValuePair[0], keyValuePair[1]);
			}
		}

		MergeElement parentMergeElement = resolver.lookupMergeElement(parentID);
		if (parentMergeElement != null) {
			setParent(parentMergeElement);
			parentMergeElement.getChildren().add(this);
		}

	}

	/**
	 * @generated NOT
	 */
	public boolean isDecisionMergeForkJoin() {
		return (
					type.equals(MergeElementType.DECISION_MERGE_NODE)
				||	type.equals(MergeElementType.FORK_JOIN_NODE)
				||	type.equals(MergeElementType.DECISION_MERGE_STATE)
				||	type.equals(MergeElementType.FORK_JOIN_STATE));
	}

	/**
	 * @generated NOT
	 */
	public boolean isInitial() {
		return 		type.equals(MergeElementType.INITIAL_NODE)
				|| 	type.equals(MergeElementType.INITIAL_STATE);
	}

	/**
	 * @generated NOT
	 */
	public boolean isFinal() {
		return 		type.equals(MergeElementType.FINAL_STATE)
				|| 	type.equals(MergeElementType.ACTIVITY_FINAL_NODE)
				|| 	type.equals(MergeElementType.FLOW_FINAL_NODE) ;
	}

	/**
	 * @generated NOT
	 */
	public boolean isConnection() {
		return type.equals(MergeElementType.ASSOCIATION)
			|| type.equals(MergeElementType.CONTROL_FLOW)
			|| type.equals(MergeElementType.OBJECT_FLOW)
			|| type.equals(MergeElementType.STATE_TRANSITION);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean equals(Object b) {
		if(b instanceof MergeElement){
			//check based on id
			MergeElement m2 = (MergeElement) b;
			if(this.getId()==null || m2.getId()==null){
				return false;
			}
			if(this.getId().equals(m2.getId())){
				return true;
			}
			//check based on object
			else if(this==m2){
				return true;
			}
		}
		return false;
	}

	/**
	 * @generated NOT
	 */
	public int hashValues() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((customAttributes == null) ? 0 : customAttributes.hashCode());
		result = prime
				* result
				+ ((inputVisualConnection == null) ? 0 : inputVisualConnection
						.hashCode());
		result = prime
				* result
				+ ((inputVisualElement == null) ? 0 : inputVisualElement
						.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult equals(MergeElement m2, MatchKB matchKB) {
		if(this.equals(m2)){
			return MatchResult.TRUE;
		}
		else if(!this.getType().equals(m2.getType())){
			return MatchResult.FALSE;
		}
		else{
			return matchKB.isAMatch(this, m2);
		}
	}


	/**
	 * @generated NOT
	 */
	MergeLogger mergeLogger = MergeLogger.instance;


	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult match(MergeElement m2, MatchKB matchKB) throws MergeConflictException{

		MatchResult matchStructure = this.matchStructure(m2, matchKB);
		MatchResult matchValue = this.matchValue(m2, matchKB);

		MatchResult finalResult = threeValAND(matchStructure,matchValue);


		//print the comparison result
		String name1;
		String name2;
		if(this.getInputVisualElement()!=null && this.getInputVisualElement().size()>0){
			name1 = this.getInputVisualElement().get(0).getName();
			name2 = m2.getInputVisualElement().get(0).getName();
		}
		else{
			name1 = this.getInputVisualConnection().get(0).getName();
			name2 = m2.getInputVisualConnection().get(0).getName();
		}
		if(finalResult==MatchResult.TRUE){
			mergeLogger.append(MergeLogger.INFO,"FOUND A MATCH between " + MergeUtil.fixedLengthString(this.getType(), 12)
					+ " "+this.getId() + "("+name1+")" +" and " + m2.getId() + "("+name2+")");
		}
		else if(matchValue==MatchResult.FALSE || matchValue==MatchResult.UNKNOWN){
			mergeLogger.append(MergeLogger.INFO,"Comparison does not match between " + MergeUtil.fixedLengthString(this.getType(), 12)
					+ " "+this.getId() + "("+name1+")" +" and " + m2.getId() + "("+name2+")"+ " due to matchValue=" + matchValue);
		}
		else{
			mergeLogger.append(MergeLogger.INFO,"Comparison does not match between " + MergeUtil.fixedLengthString(this.getType(), 12)
					+ " "+this.getId() + "("+name1+")" +" and " + m2.getId() + "("+name2+")"+ " due to matchStructure=" + matchStructure);
		}


		//also do a check that parent are not going to cause merge conflict
		if(finalResult==MatchResult.TRUE){
			if(checkParentConflict(m2,matchKB)){
				mergeLogger.append(MergeLogger.INFO,"Subelements conflict ignored between "+
				this.getId() + "("+name1+")" +" and " + m2.getId() + "("+name2+")");
				finalResult= MatchResult.FALSE;
			}
		}




		return finalResult;

	}

	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult matchValue(MergeElement m2, MatchKB matchKB) throws MergeConflictException{
		//can not match by name if both name is empty
		MatchResult canMatchByName = MergeUtil.canMatchByName(this, m2,MatchResult.FALSE);
		MatchResult canMatchByCustomAttr = MergeUtil.canMatchByCustomAttr(this, m2);

		return threeValAND(canMatchByName,canMatchByCustomAttr);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult matchStructure(MergeElement m2, MatchKB matchKB) throws MergeConflictException{
		return MatchResult.TRUE;
	}


	public boolean checkParentConflict(MergeElement m2, MatchKB matchKB) throws MergeConflictException{
		//if options enabled, return true so that conflicts will be ignored
		//if options disabled, check if merge exception will occur and throw it

		boolean allowConflictingSubElements = PreferenceUtil.getPreferenceBoolean(PreferenceConstants.STITCH_PREF_ALLOW_CONFLICTING_SUBELEMENTS);

		//run check for parent conflict
		MergeElement m1Parent = this.getParent();
		MergeElement m2Parent = m2.getParent();

		if(m1Parent==null || m2Parent==null)
			return false;

		if(allowConflictingSubElements){
//			Allow the following
//			Operations/properties of different classes
//			-          Use cases of different systems
//			-          Subsystems of different systems

			if( (this.getType().equals(MergeElementType.OPERATION) && parent.getType().equals(MergeElementType.CLASS) )||
				(this.getType().equals(MergeElementType.ATTRIBUTE) && parent.getType().equals(MergeElementType.CLASS) ) ||
				(this.getType().equals(MergeElementType.USECASE) && parent.getType().equals(MergeElementType.SYSTEM_BOUNDARY)) ||
				(this.getType().equals(MergeElementType.SYSTEM_BOUNDARY) && parent.getType().equals(MergeElementType.SYSTEM_BOUNDARY)) ||
				(this.getType().equals(MergeElementType.SYSTEM) && parent.getType().equals(MergeElementType.SYSTEM))
				){
				if(m1Parent.match(m2Parent, matchKB)==MatchResult.FALSE)
					//parent non matching, skip conflicts by return TRUE
					return true;
				else if(m1Parent.match(m2Parent, matchKB)==MatchResult.TRUE)
					//parent matching, just do as normal, add them to the matchKB
					return false;
			}
		}

		if(m1Parent.match(m2Parent, matchKB)==MatchResult.FALSE){
			List<MergeElement> conflictSource = new ArrayList<MergeElement>();
			conflictSource.add(this);
			conflictSource.add(m2);
			MergeConflictException ex = new MergeConflictException(conflictSource,"unmatchable parent");
			mergeLogger.append(MergeLogger.INFO,"EXCEPTION - Merge conflict detected." + ex.toString());
			throw ex;
		}

		return false;

	}

	/**
	 * @generated NOT
	 */
	public MatchResult threeValOR(MatchResult... results){
		MatchResult[] res = new MatchResult[results.length];
		int countOfFalse = 0;
		for(int i=0;i<res.length;i++){
			if(results[i]==MatchResult.TRUE){
				//return true if any is true
				return MatchResult.TRUE;
			}
			else if(results[i]==MatchResult.FALSE){
				countOfFalse++;
			}
		}

		if(countOfFalse==results.length){
			//return false if all is false
			return MatchResult.FALSE;
		}

		return MatchResult.UNKNOWN;
	}

	/**
	 * @generated NOT
	 */
	public MatchResult threeValRelaxedAND(MatchResult... results){
		boolean hasOneTrue = false;
		MatchResult[] res = new MatchResult[results.length];
		for(int i=0;i<res.length;i++){
			if(results[i]==MatchResult.FALSE){
				//return false if any is false
				return MatchResult.FALSE;
			}
			else if(results[i]==MatchResult.TRUE){
				hasOneTrue = true;
			}
		}
		if(hasOneTrue)
			return MatchResult.TRUE;
		else
			return MatchResult.UNKNOWN;
	}

	/**
	 * @generated NOT
	 */
	public MatchResult threeValAND(MatchResult... results){
		MatchResult[] res = new MatchResult[results.length];
		int countOfTrue = 0;
		for(int i=0;i<res.length;i++){
			if(results[i]==MatchResult.FALSE){
				//return false if any is false
				return MatchResult.FALSE;
			}
			else if(results[i]==MatchResult.TRUE){
				countOfTrue++;
			}
		}

		if(countOfTrue==results.length){
			//return true if all is true
			return MatchResult.TRUE;
		}

		return MatchResult.UNKNOWN;

	}

} //MergeElementImpl
