/**
 */
package dk.dtu.imm.red.visualstitch.util;

import dk.dtu.imm.red.visualstitch.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage
 * @generated
 */
public class VisualstitchSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VisualstitchPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualstitchSwitch() {
		if (modelPackage == null) {
			modelPackage = VisualstitchPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case VisualstitchPackage.MERGE_ELEMENT: {
				MergeElement mergeElement = (MergeElement)theEObject;
				T result = caseMergeElement(mergeElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER: {
				MergeElementResolver mergeElementResolver = (MergeElementResolver)theEObject;
				T result = caseMergeElementResolver(mergeElementResolver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualstitchPackage.CONNECTION_ME: {
				ConnectionME connectionME = (ConnectionME)theEObject;
				T result = caseConnectionME(connectionME);
				if (result == null) result = caseMergeElement(connectionME);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualstitchPackage.ARROW_ME: {
				ArrowME arrowME = (ArrowME)theEObject;
				T result = caseArrowME(arrowME);
				if (result == null) result = caseMergeElement(arrowME);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualstitchPackage.CONTROL_NODE_ME: {
				ControlNodeME controlNodeME = (ControlNodeME)theEObject;
				T result = caseControlNodeME(controlNodeME);
				if (result == null) result = caseMergeElement(controlNodeME);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualstitchPackage.INITIAL_NODE_ME: {
				InitialNodeME initialNodeME = (InitialNodeME)theEObject;
				T result = caseInitialNodeME(initialNodeME);
				if (result == null) result = caseControlNodeME(initialNodeME);
				if (result == null) result = caseMergeElement(initialNodeME);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualstitchPackage.FINAL_NODE_ME: {
				FinalNodeME finalNodeME = (FinalNodeME)theEObject;
				T result = caseFinalNodeME(finalNodeME);
				if (result == null) result = caseControlNodeME(finalNodeME);
				if (result == null) result = caseMergeElement(finalNodeME);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Merge Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Merge Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMergeElement(MergeElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Merge Element Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Merge Element Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMergeElementResolver(MergeElementResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection ME</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection ME</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionME(ConnectionME object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Arrow ME</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Arrow ME</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrowME(ArrowME object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Node ME</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Node ME</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlNodeME(ControlNodeME object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Initial Node ME</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Initial Node ME</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInitialNodeME(InitialNodeME object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Final Node ME</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Final Node ME</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFinalNodeME(FinalNodeME object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //VisualstitchSwitch
