/**
 */
package dk.dtu.imm.red.visualstitch.impl;

import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelPackage;

import dk.dtu.imm.red.visualstitch.ArrowME;
import dk.dtu.imm.red.visualstitch.ConnectionME;
import dk.dtu.imm.red.visualstitch.ControlNodeME;
import dk.dtu.imm.red.visualstitch.CustomAttributesKind;
import dk.dtu.imm.red.visualstitch.FinalNodeME;

import dk.dtu.imm.red.visualstitch.HashMapEntry.HashMapEntryPackage;

import dk.dtu.imm.red.visualstitch.HashMapEntry.impl.HashMapEntryPackageImpl;

import dk.dtu.imm.red.visualstitch.InitialNodeME;
import dk.dtu.imm.red.visualstitch.MatchMode;
import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.MergeElementResolver;
import dk.dtu.imm.red.visualstitch.VisualstitchFactory;
import dk.dtu.imm.red.visualstitch.VisualstitchPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VisualstitchPackageImpl extends EPackageImpl implements VisualstitchPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mergeElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mergeElementResolverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionMEEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arrowMEEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlNodeMEEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass initialNodeMEEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass finalNodeMEEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum customAttributesKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum matchModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum matchResultEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VisualstitchPackageImpl() {
		super(eNS_URI, VisualstitchFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link VisualstitchPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static VisualstitchPackage init() {
		if (isInited) return (VisualstitchPackage)EPackage.Registry.INSTANCE.getEPackage(VisualstitchPackage.eNS_URI);

		// Obtain or create and register package
		VisualstitchPackageImpl theVisualstitchPackage = (VisualstitchPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof VisualstitchPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new VisualstitchPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		visualmodelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		HashMapEntryPackageImpl theHashMapEntryPackage = (HashMapEntryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HashMapEntryPackage.eNS_URI) instanceof HashMapEntryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HashMapEntryPackage.eNS_URI) : HashMapEntryPackage.eINSTANCE);

		// Create package meta-data objects
		theVisualstitchPackage.createPackageContents();
		theHashMapEntryPackage.createPackageContents();

		// Initialize created meta-data
		theVisualstitchPackage.initializePackageContents();
		theHashMapEntryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theVisualstitchPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VisualstitchPackage.eNS_URI, theVisualstitchPackage);
		return theVisualstitchPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMergeElement() {
		return mergeElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMergeElement_Id() {
		return (EAttribute)mergeElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMergeElement_Type() {
		return (EAttribute)mergeElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMergeElement_Name() {
		return (EAttribute)mergeElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeElement_Parent() {
		return (EReference)mergeElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeElement_Children() {
		return (EReference)mergeElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeElement_CustomAttributes() {
		return (EReference)mergeElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMergeElement_InputDiagramIndex() {
		return (EAttribute)mergeElementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeElement_InputVisualElement() {
		return (EReference)mergeElementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeElement_InputVisualConnection() {
		return (EReference)mergeElementEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeElement_Resolver() {
		return (EReference)mergeElementEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMergeElement__Initialize__MergeElementResolver_int_IVisualElement_IVisualConnection() {
		return mergeElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMergeElement__InitializeForUnitTest__MergeElementResolver_int_String_String_String_String_String() {
		return mergeElementEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMergeElementResolver() {
		return mergeElementResolverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeElementResolver_VisualElementToIDMap() {
		return (EReference)mergeElementResolverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeElementResolver_VisualConnectionToIDMap() {
		return (EReference)mergeElementResolverEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMergeElementResolver_IDToMergeElementMap() {
		return (EReference)mergeElementResolverEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMergeElementResolver__KeepTrackOf__String_MergeElement_IVisualElement() {
		return mergeElementResolverEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMergeElementResolver__KeepTrackOf__String_MergeElement_IVisualConnection() {
		return mergeElementResolverEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMergeElementResolver__KeepTrackOf__String_MergeElement() {
		return mergeElementResolverEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMergeElementResolver__LookupMergeElement__IVisualElement() {
		return mergeElementResolverEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMergeElementResolver__LookupMergeElement__IVisualConnection() {
		return mergeElementResolverEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMergeElementResolver__LookupMergeElement__String() {
		return mergeElementResolverEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMergeElementResolver__Destroy() {
		return mergeElementResolverEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionME() {
		return connectionMEEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionME_ConnEnd1() {
		return (EReference)connectionMEEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionME_ConnEnd2() {
		return (EReference)connectionMEEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArrowME() {
		return arrowMEEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrowME_Src() {
		return (EReference)arrowMEEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrowME_Tgt() {
		return (EReference)arrowMEEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlNodeME() {
		return controlNodeMEEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlNodeME_Incoming() {
		return (EReference)controlNodeMEEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlNodeME_Outgoing() {
		return (EReference)controlNodeMEEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInitialNodeME() {
		return initialNodeMEEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFinalNodeME() {
		return finalNodeMEEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCustomAttributesKind() {
		return customAttributesKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMatchMode() {
		return matchModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMatchResult() {
		return matchResultEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualstitchFactory getVisualstitchFactory() {
		return (VisualstitchFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		mergeElementEClass = createEClass(MERGE_ELEMENT);
		createEAttribute(mergeElementEClass, MERGE_ELEMENT__ID);
		createEAttribute(mergeElementEClass, MERGE_ELEMENT__TYPE);
		createEAttribute(mergeElementEClass, MERGE_ELEMENT__NAME);
		createEReference(mergeElementEClass, MERGE_ELEMENT__PARENT);
		createEReference(mergeElementEClass, MERGE_ELEMENT__CHILDREN);
		createEReference(mergeElementEClass, MERGE_ELEMENT__CUSTOM_ATTRIBUTES);
		createEAttribute(mergeElementEClass, MERGE_ELEMENT__INPUT_DIAGRAM_INDEX);
		createEReference(mergeElementEClass, MERGE_ELEMENT__INPUT_VISUAL_ELEMENT);
		createEReference(mergeElementEClass, MERGE_ELEMENT__INPUT_VISUAL_CONNECTION);
		createEReference(mergeElementEClass, MERGE_ELEMENT__RESOLVER);
		createEOperation(mergeElementEClass, MERGE_ELEMENT___INITIALIZE__MERGEELEMENTRESOLVER_INT_IVISUALELEMENT_IVISUALCONNECTION);
		createEOperation(mergeElementEClass, MERGE_ELEMENT___INITIALIZE_FOR_UNIT_TEST__MERGEELEMENTRESOLVER_INT_STRING_STRING_STRING_STRING_STRING);

		mergeElementResolverEClass = createEClass(MERGE_ELEMENT_RESOLVER);
		createEReference(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER__VISUAL_ELEMENT_TO_ID_MAP);
		createEReference(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER__VISUAL_CONNECTION_TO_ID_MAP);
		createEReference(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER__ID_TO_MERGE_ELEMENT_MAP);
		createEOperation(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT_IVISUALELEMENT);
		createEOperation(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT_IVISUALCONNECTION);
		createEOperation(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT);
		createEOperation(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__IVISUALELEMENT);
		createEOperation(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__IVISUALCONNECTION);
		createEOperation(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__STRING);
		createEOperation(mergeElementResolverEClass, MERGE_ELEMENT_RESOLVER___DESTROY);

		connectionMEEClass = createEClass(CONNECTION_ME);
		createEReference(connectionMEEClass, CONNECTION_ME__CONN_END1);
		createEReference(connectionMEEClass, CONNECTION_ME__CONN_END2);

		arrowMEEClass = createEClass(ARROW_ME);
		createEReference(arrowMEEClass, ARROW_ME__SRC);
		createEReference(arrowMEEClass, ARROW_ME__TGT);

		controlNodeMEEClass = createEClass(CONTROL_NODE_ME);
		createEReference(controlNodeMEEClass, CONTROL_NODE_ME__INCOMING);
		createEReference(controlNodeMEEClass, CONTROL_NODE_ME__OUTGOING);

		initialNodeMEEClass = createEClass(INITIAL_NODE_ME);

		finalNodeMEEClass = createEClass(FINAL_NODE_ME);

		// Create enums
		customAttributesKindEEnum = createEEnum(CUSTOM_ATTRIBUTES_KIND);
		matchModeEEnum = createEEnum(MATCH_MODE);
		matchResultEEnum = createEEnum(MATCH_RESULT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		HashMapEntryPackage theHashMapEntryPackage = (HashMapEntryPackage)EPackage.Registry.INSTANCE.getEPackage(HashMapEntryPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		visualmodelPackage thevisualmodelPackage = (visualmodelPackage)EPackage.Registry.INSTANCE.getEPackage(visualmodelPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theHashMapEntryPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		connectionMEEClass.getESuperTypes().add(this.getMergeElement());
		arrowMEEClass.getESuperTypes().add(this.getMergeElement());
		controlNodeMEEClass.getESuperTypes().add(this.getMergeElement());
		initialNodeMEEClass.getESuperTypes().add(this.getControlNodeME());
		finalNodeMEEClass.getESuperTypes().add(this.getControlNodeME());

		// Initialize classes, features, and operations; add parameters
		initEClass(mergeElementEClass, MergeElement.class, "MergeElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMergeElement_Id(), theEcorePackage.getEString(), "id", null, 0, 1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMergeElement_Type(), ecorePackage.getEString(), "type", null, 0, 1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMergeElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMergeElement_Parent(), this.getMergeElement(), null, "parent", null, 0, 1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMergeElement_Children(), this.getMergeElement(), null, "children", null, 0, -1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMergeElement_CustomAttributes(), theHashMapEntryPackage.getStringToStringMap(), null, "customAttributes", null, 0, -1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMergeElement_InputDiagramIndex(), ecorePackage.getEInt(), "inputDiagramIndex", null, 0, 1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMergeElement_InputVisualElement(), thevisualmodelPackage.getIVisualElement(), null, "inputVisualElement", null, 0, -1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMergeElement_InputVisualConnection(), thevisualmodelPackage.getIVisualConnection(), null, "inputVisualConnection", null, 0, -1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMergeElement_Resolver(), this.getMergeElementResolver(), null, "resolver", null, 0, 1, MergeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getMergeElement__Initialize__MergeElementResolver_int_IVisualElement_IVisualConnection(), null, "initialize", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMergeElementResolver(), "resolver", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "inputDiagramIndex", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thevisualmodelPackage.getIVisualElement(), "visualElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thevisualmodelPackage.getIVisualConnection(), "visualConnection", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMergeElement__InitializeForUnitTest__MergeElementResolver_int_String_String_String_String_String(), null, "initializeForUnitTest", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMergeElementResolver(), "resolver", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "inputDiagramIndex", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "id", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "name", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "type", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "parentID", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "customAttributesCommaSeparated", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mergeElementResolverEClass, MergeElementResolver.class, "MergeElementResolver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMergeElementResolver_VisualElementToIDMap(), theHashMapEntryPackage.getIVisualElementToStringMap(), null, "visualElementToIDMap", null, 0, -1, MergeElementResolver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMergeElementResolver_VisualConnectionToIDMap(), theHashMapEntryPackage.getIVisualConnectionToStringMap(), null, "visualConnectionToIDMap", null, 0, -1, MergeElementResolver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMergeElementResolver_IDToMergeElementMap(), theHashMapEntryPackage.getStringToMergeElementMap(), null, "IDToMergeElementMap", null, 0, -1, MergeElementResolver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMergeElementResolver__KeepTrackOf__String_MergeElement_IVisualElement(), null, "keepTrackOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "id", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMergeElement(), "mergeElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thevisualmodelPackage.getIVisualElement(), "visualElement", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMergeElementResolver__KeepTrackOf__String_MergeElement_IVisualConnection(), null, "keepTrackOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "id", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMergeElement(), "mergeElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thevisualmodelPackage.getIVisualConnection(), "visualConnection", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMergeElementResolver__KeepTrackOf__String_MergeElement(), null, "keepTrackOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "id", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMergeElement(), "mergeElement", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMergeElementResolver__LookupMergeElement__IVisualElement(), this.getMergeElement(), "lookupMergeElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thevisualmodelPackage.getIVisualElement(), "visualElement", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMergeElementResolver__LookupMergeElement__IVisualConnection(), this.getMergeElement(), "lookupMergeElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thevisualmodelPackage.getIVisualConnection(), "visualConnection", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMergeElementResolver__LookupMergeElement__String(), this.getMergeElement(), "lookupMergeElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "id", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMergeElementResolver__Destroy(), null, "destroy", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(connectionMEEClass, ConnectionME.class, "ConnectionME", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectionME_ConnEnd1(), this.getMergeElement(), null, "connEnd1", null, 0, 1, ConnectionME.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionME_ConnEnd2(), this.getMergeElement(), null, "connEnd2", null, 0, 1, ConnectionME.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(arrowMEEClass, ArrowME.class, "ArrowME", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArrowME_Src(), this.getMergeElement(), null, "src", null, 0, 1, ArrowME.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArrowME_Tgt(), this.getMergeElement(), null, "tgt", null, 0, 1, ArrowME.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(controlNodeMEEClass, ControlNodeME.class, "ControlNodeME", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getControlNodeME_Incoming(), this.getMergeElement(), null, "incoming", null, 0, -1, ControlNodeME.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControlNodeME_Outgoing(), this.getMergeElement(), null, "outgoing", null, 0, -1, ControlNodeME.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(initialNodeMEEClass, InitialNodeME.class, "InitialNodeME", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(finalNodeMEEClass, FinalNodeME.class, "FinalNodeME", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(customAttributesKindEEnum, CustomAttributesKind.class, "CustomAttributesKind");
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.NAME);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.ROLE_SRC);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.ROLE_TGT);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.MULTIPLICITY_SRC);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.MULTIPLICITY_TGT);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.VISIBILITY);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.MULTIPLICITY);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.PARAMETERS);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.RETURN_TYPE);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.ENTRY);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.DO);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.EXIT);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.TRIGGER);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.GUARD);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.EFFECT);
		addEEnumLiteral(customAttributesKindEEnum, CustomAttributesKind.ACTOR_KIND);

		initEEnum(matchModeEEnum, MatchMode.class, "MatchMode");
		addEEnumLiteral(matchModeEEnum, MatchMode.AT_LEAST_ONE);
		addEEnumLiteral(matchModeEEnum, MatchMode.ALL);

		initEEnum(matchResultEEnum, MatchResult.class, "MatchResult");
		addEEnumLiteral(matchResultEEnum, MatchResult.TRUE);
		addEEnumLiteral(matchResultEEnum, MatchResult.FALSE);
		addEEnumLiteral(matchResultEEnum, MatchResult.UNKNOWN);

		// Create resource
		createResource(eNS_URI);
	}

} //VisualstitchPackageImpl
