package dk.dtu.imm.red.visualstitch.processor;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;

public enum MergeLogger {

	//set as singleton enum
	instance;

	public static String INFO = "INFO";
	public static String DEBUG = "DEBUG";
	private StringBuffer mergeLog = new StringBuffer();
	public static String NL = System.getProperty("line.separator");
	private boolean logInRCPMode = true;

	public void append(String mergeLogLevel, String s){
		boolean prefIsDebug=false;
		String storedLogLevel = PreferenceUtil.getPreferenceString(PreferenceConstants.STITCH_PREF_MERGE_LOG_LEVEL);
		if(storedLogLevel.equals(DEBUG))
			prefIsDebug = true;


		if(logInRCPMode){
			if(prefIsDebug || (!prefIsDebug && mergeLogLevel.equals(INFO))){
				mergeLog.append(s + NL);
			}
		}
		else{
			System.out.println(s);
		}
	}

	public String getMergeLog(){
		return mergeLog.toString();
	}

	public void flushMergeLog(){
		mergeLog = new StringBuffer();
	}

	public void setLogInRCPMode(boolean value){
		this.logInRCPMode = value;
	}
}
