/**
 */
package dk.dtu.imm.red.visualstitch.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.MergeElementResolver;
import dk.dtu.imm.red.visualstitch.VisualstitchPackage;
import dk.dtu.imm.red.visualstitch.HashMapEntry.HashMapEntryPackage;
import dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualConnectionToStringMapImpl;
import dk.dtu.imm.red.visualstitch.HashMapEntry.impl.IVisualElementToStringMapImpl;
import dk.dtu.imm.red.visualstitch.HashMapEntry.impl.StringToMergeElementMapImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Merge Element Resolver</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementResolverImpl#getVisualElementToIDMap <em>Visual Element To ID Map</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementResolverImpl#getVisualConnectionToIDMap <em>Visual Connection To ID Map</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.impl.MergeElementResolverImpl#getIDToMergeElementMap <em>ID To Merge Element Map</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MergeElementResolverImpl extends MinimalEObjectImpl.Container implements MergeElementResolver {
	/**
	 * The cached value of the '{@link #getVisualElementToIDMap() <em>Visual Element To ID Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisualElementToIDMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<IVisualElement, String> visualElementToIDMap;

	/**
	 * The cached value of the '{@link #getVisualConnectionToIDMap() <em>Visual Connection To ID Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisualConnectionToIDMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<IVisualConnection, String> visualConnectionToIDMap;

	/**
	 * The cached value of the '{@link #getIDToMergeElementMap() <em>ID To Merge Element Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDToMergeElementMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, MergeElement> idToMergeElementMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected MergeElementResolverImpl() {
		super();
		getIDToMergeElementMap();
		getVisualConnectionToIDMap();
		getVisualElementToIDMap();


	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualstitchPackage.Literals.MERGE_ELEMENT_RESOLVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<IVisualElement, String> getVisualElementToIDMap() {
		if (visualElementToIDMap == null) {
			visualElementToIDMap = new EcoreEMap<IVisualElement,String>(HashMapEntryPackage.Literals.IVISUAL_ELEMENT_TO_STRING_MAP, IVisualElementToStringMapImpl.class, this, VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_ELEMENT_TO_ID_MAP);
		}
		return visualElementToIDMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<IVisualConnection, String> getVisualConnectionToIDMap() {
		if (visualConnectionToIDMap == null) {
			visualConnectionToIDMap = new EcoreEMap<IVisualConnection,String>(HashMapEntryPackage.Literals.IVISUAL_CONNECTION_TO_STRING_MAP, IVisualConnectionToStringMapImpl.class, this, VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_CONNECTION_TO_ID_MAP);
		}
		return visualConnectionToIDMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, MergeElement> getIDToMergeElementMap() {
		if (idToMergeElementMap == null) {
			idToMergeElementMap = new EcoreEMap<String,MergeElement>(HashMapEntryPackage.Literals.STRING_TO_MERGE_ELEMENT_MAP, StringToMergeElementMapImpl.class, this, VisualstitchPackage.MERGE_ELEMENT_RESOLVER__ID_TO_MERGE_ELEMENT_MAP);
		}
		return idToMergeElementMap;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_ELEMENT_TO_ID_MAP:
				return ((InternalEList<?>)getVisualElementToIDMap()).basicRemove(otherEnd, msgs);
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_CONNECTION_TO_ID_MAP:
				return ((InternalEList<?>)getVisualConnectionToIDMap()).basicRemove(otherEnd, msgs);
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__ID_TO_MERGE_ELEMENT_MAP:
				return ((InternalEList<?>)getIDToMergeElementMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_ELEMENT_TO_ID_MAP:
				if (coreType) return getVisualElementToIDMap();
				else return getVisualElementToIDMap().map();
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_CONNECTION_TO_ID_MAP:
				if (coreType) return getVisualConnectionToIDMap();
				else return getVisualConnectionToIDMap().map();
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__ID_TO_MERGE_ELEMENT_MAP:
				if (coreType) return getIDToMergeElementMap();
				else return getIDToMergeElementMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_ELEMENT_TO_ID_MAP:
				((EStructuralFeature.Setting)getVisualElementToIDMap()).set(newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_CONNECTION_TO_ID_MAP:
				((EStructuralFeature.Setting)getVisualConnectionToIDMap()).set(newValue);
				return;
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__ID_TO_MERGE_ELEMENT_MAP:
				((EStructuralFeature.Setting)getIDToMergeElementMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_ELEMENT_TO_ID_MAP:
				getVisualElementToIDMap().clear();
				return;
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_CONNECTION_TO_ID_MAP:
				getVisualConnectionToIDMap().clear();
				return;
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__ID_TO_MERGE_ELEMENT_MAP:
				getIDToMergeElementMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_ELEMENT_TO_ID_MAP:
				return visualElementToIDMap != null && !visualElementToIDMap.isEmpty();
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__VISUAL_CONNECTION_TO_ID_MAP:
				return visualConnectionToIDMap != null && !visualConnectionToIDMap.isEmpty();
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER__ID_TO_MERGE_ELEMENT_MAP:
				return idToMergeElementMap != null && !idToMergeElementMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT_IVISUALELEMENT:
				keepTrackOf((String)arguments.get(0), (MergeElement)arguments.get(1), (IVisualElement)arguments.get(2));
				return null;
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT_IVISUALCONNECTION:
				keepTrackOf((String)arguments.get(0), (MergeElement)arguments.get(1), (IVisualConnection)arguments.get(2));
				return null;
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER___KEEP_TRACK_OF__STRING_MERGEELEMENT:
				keepTrackOf((String)arguments.get(0), (MergeElement)arguments.get(1));
				return null;
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__IVISUALELEMENT:
				return lookupMergeElement((IVisualElement)arguments.get(0));
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__IVISUALCONNECTION:
				return lookupMergeElement((IVisualConnection)arguments.get(0));
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER___LOOKUP_MERGE_ELEMENT__STRING:
				return lookupMergeElement((String)arguments.get(0));
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER___DESTROY:
				destroy();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}
	
	/**
	 * @generated NOT
	 */
	public void keepTrackOf(String id, MergeElement mergeElement, IVisualElement visualElement) {
		idToMergeElementMap.put(id, mergeElement);
		visualElementToIDMap.put(visualElement, id);
	}

	/**
	 * @generated NOT
	 */
	public void keepTrackOf(String id, MergeElement mergeElement, IVisualConnection visualConnection) {
		idToMergeElementMap.put(id, mergeElement);
		visualConnectionToIDMap.put(visualConnection, id);
	}

	/**
	 * @generated NOT
	 */
	public void keepTrackOf(String id, MergeElement mergeElement) {
		idToMergeElementMap.put(id, mergeElement);
	}

	/**
	 * @generated NOT
	 */
	public MergeElement lookupMergeElement(IVisualElement visualElement) {
		String id = visualElementToIDMap.get(visualElement);
		if(id!=null){
			return idToMergeElementMap.get(id);
		}
		return null;
	}

	/**
	 * @generated NOT
	 */
	public MergeElement lookupMergeElement(IVisualConnection visualConnection) {
		String id = visualConnectionToIDMap.get(visualConnection);
		if(id!=null){
			return idToMergeElementMap.get(id);
		}
		return null;
	}

	/**
	 * @generated NOT
	 */
	public MergeElement lookupMergeElement(String id) {
		if(id!=null){
			return idToMergeElementMap.get(id.trim());
		}
		return null;
	}

	/**
	 * @generated NOT
	 */
	public void destroy(){
		visualConnectionToIDMap.clear();
		visualConnectionToIDMap.clear();
		idToMergeElementMap.clear();
	}

} //MergeElementResolverImpl
