package dk.dtu.imm.red.visualstitch.processor;

import java.text.SimpleDateFormat;
import java.util.Date;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.MergeElementResolver;
import dk.dtu.imm.red.visualstitch.VisualstitchFactory;

public class MergeExecutor {

	// for looking up merge element based on the id or the contained visual
	// element/visual connection
	private MergeElementResolver resolver = VisualstitchFactory.eINSTANCE
			.createMergeElementResolver();
	private MergeLogger mergeLogger = MergeLogger.instance;

	VisualDiagram firstDiagram = null;
	VisualDiagram secondDiagram = null;
	VisualDiagram outputDiagram = null;

	private MergeInputReader inputReader;
	private MergeProcessor processor;
	private MergeOutputWriter outputWriter;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");

	private boolean mergeSuccess;

	public MergeExecutor(VisualDiagram firstDiagram, VisualDiagram secondDiagram) {
		this.firstDiagram = firstDiagram;
		this.secondDiagram = secondDiagram;
		mergeSuccess = true;
	}

	public VisualDiagram executeMerge(){
		// phase 1 - flattening
		long startTime = System.currentTimeMillis();
		mergeLogger.append(MergeLogger.DEBUG,"Start time: " + sdf.format(new Date(startTime)));
		inputReader = new MergeInputReader(this, firstDiagram, secondDiagram);
		inputReader.execute();
		mergeLogger.append("INFO","Flattening phase - total number of MergeElements :"+ resolver.getIDToMergeElementMap().size());

		try{
		// phase 2 & 3 - match and merge
		processor = new MergeProcessor(this,
				inputReader.getUnmatchedPartitionMap());
		processor.execute();
		}
		catch(MergeConflictException ex){
			mergeSuccess = false;
			//only set the flag, but still continue to write the output
		}

		// phase 4 - output
		outputWriter = new MergeOutputWriter(this, processor.getOutputSet(),
				inputReader.getFurthestPointInFirstDiagram(),
				inputReader.getNearestPointInSecondDiagram());
		outputDiagram = outputWriter.execute();

		long endTime = System.currentTimeMillis();

		mergeLogger.append(MergeLogger.DEBUG,"End time: " + sdf.format(new Date(endTime)));
		mergeLogger.append(MergeLogger.INFO,"Total execution time: " + (endTime-startTime)+ " ms");

		return outputDiagram;
	}

	public MergeExecutor() {
	}

	public void executeMergeUnitTest(MergeElement[] arr) throws MergeConflictException{
		inputReader = new MergeInputReader(resolver, arr);
		inputReader.execute();

		// phase 2 & 3 - match and merge
		processor = new MergeProcessor(this,
				inputReader.getUnmatchedPartitionMap());
		processor.execute();
	}

	public MergeElementResolver getResolver() {
		return resolver;
	}

	public boolean isMergeSuccess() {
		return mergeSuccess;
	}

	public MergeInputReader getInputReader() {
		return inputReader;
	}

	public MergeProcessor getProcessor() {
		return processor;
	}

	public MergeOutputWriter getOutputWriter() {
		return outputWriter;
	}

	public void destroy(){
		if(resolver!=null)
			resolver.destroy();
		if(inputReader!=null)
			inputReader.destroy();
		if(processor!=null)
			processor.destroy();
		if(outputWriter!=null)
			outputWriter.destroy();
	}
}
