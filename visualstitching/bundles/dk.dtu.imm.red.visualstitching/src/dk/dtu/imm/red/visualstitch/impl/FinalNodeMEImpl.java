/**
 */
package dk.dtu.imm.red.visualstitch.impl;

import org.eclipse.emf.ecore.EClass;

import dk.dtu.imm.red.visualstitch.ControlNodeME;
import dk.dtu.imm.red.visualstitch.FinalNodeME;
import dk.dtu.imm.red.visualstitch.MatchMode;
import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.VisualstitchPackage;
import dk.dtu.imm.red.visualstitch.processor.MatchKB;
import dk.dtu.imm.red.visualstitch.processor.MergeConflictException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Final Node ME</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class FinalNodeMEImpl extends ControlNodeMEImpl implements FinalNodeME {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected FinalNodeMEImpl() {
		super();
		getIncoming();
		getOutgoing();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualstitchPackage.Literals.FINAL_NODE_ME;
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		return super.toString().replaceAll("ControlNodeME", "FinalNodeME");
	}

	/**
	 * @generated NOT
	 */
	@Override
	public MatchResult matchStructure(MergeElement m2, MatchKB matchKB) throws MergeConflictException {

		MatchResult incomingEqual = matchSet(this.getIncoming(), ((ControlNodeME)m2).getIncoming(),
				matchKB, MatchMode.AT_LEAST_ONE);

		return incomingEqual;

	}

} //FinalNodeMEImpl
