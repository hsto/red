/**
 */
package dk.dtu.imm.red.visualstitch.impl;

import dk.dtu.imm.red.visualstitch.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VisualstitchFactoryImpl extends EFactoryImpl implements VisualstitchFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VisualstitchFactory init() {
		try {
			VisualstitchFactory theVisualstitchFactory = (VisualstitchFactory)EPackage.Registry.INSTANCE.getEFactory(VisualstitchPackage.eNS_URI);
			if (theVisualstitchFactory != null) {
				return theVisualstitchFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VisualstitchFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualstitchFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case VisualstitchPackage.MERGE_ELEMENT: return createMergeElement();
			case VisualstitchPackage.MERGE_ELEMENT_RESOLVER: return createMergeElementResolver();
			case VisualstitchPackage.CONNECTION_ME: return createConnectionME();
			case VisualstitchPackage.ARROW_ME: return createArrowME();
			case VisualstitchPackage.CONTROL_NODE_ME: return createControlNodeME();
			case VisualstitchPackage.INITIAL_NODE_ME: return createInitialNodeME();
			case VisualstitchPackage.FINAL_NODE_ME: return createFinalNodeME();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case VisualstitchPackage.CUSTOM_ATTRIBUTES_KIND:
				return createCustomAttributesKindFromString(eDataType, initialValue);
			case VisualstitchPackage.MATCH_MODE:
				return createMatchModeFromString(eDataType, initialValue);
			case VisualstitchPackage.MATCH_RESULT:
				return createMatchResultFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case VisualstitchPackage.CUSTOM_ATTRIBUTES_KIND:
				return convertCustomAttributesKindToString(eDataType, instanceValue);
			case VisualstitchPackage.MATCH_MODE:
				return convertMatchModeToString(eDataType, instanceValue);
			case VisualstitchPackage.MATCH_RESULT:
				return convertMatchResultToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElement createMergeElement() {
		MergeElementImpl mergeElement = new MergeElementImpl();
		return mergeElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeElementResolver createMergeElementResolver() {
		MergeElementResolverImpl mergeElementResolver = new MergeElementResolverImpl();
		return mergeElementResolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionME createConnectionME() {
		ConnectionMEImpl connectionME = new ConnectionMEImpl();
		return connectionME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrowME createArrowME() {
		ArrowMEImpl arrowME = new ArrowMEImpl();
		return arrowME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlNodeME createControlNodeME() {
		ControlNodeMEImpl controlNodeME = new ControlNodeMEImpl();
		return controlNodeME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialNodeME createInitialNodeME() {
		InitialNodeMEImpl initialNodeME = new InitialNodeMEImpl();
		return initialNodeME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FinalNodeME createFinalNodeME() {
		FinalNodeMEImpl finalNodeME = new FinalNodeMEImpl();
		return finalNodeME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomAttributesKind createCustomAttributesKindFromString(EDataType eDataType, String initialValue) {
		CustomAttributesKind result = CustomAttributesKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCustomAttributesKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MatchMode createMatchModeFromString(EDataType eDataType, String initialValue) {
		MatchMode result = MatchMode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMatchModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MatchResult createMatchResultFromString(EDataType eDataType, String initialValue) {
		MatchResult result = MatchResult.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMatchResultToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualstitchPackage getVisualstitchPackage() {
		return (VisualstitchPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static VisualstitchPackage getPackage() {
		return VisualstitchPackage.eINSTANCE;
	}

} //VisualstitchFactoryImpl
