package dk.dtu.imm.red.visualstitch.processor;

import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.VisualstitchFactory;

public class MergeElementFactory {

	public static MergeElement makeMergeElement(String type){
		return makeMergeElement(type,true);
	}


	public static MergeElement makeMergeElement(String type, boolean defaultAsElement){
		MergeElement m=null;


		//USE CASE DIAGRAM
		if(type.equals(MergeElementType.ASSOCIATION))
			m = VisualstitchFactory.eINSTANCE.createConnectionME();

		else if(type.equals(MergeElementType.GENERALIZATION)
			|| 	type.equals(MergeElementType.USE_CASE_INCLUDE)
			|| 	type.equals(MergeElementType.USE_CASE_EXTEND)
			||	type.equals(MergeElementType.AGGREGATION)
			|| 	type.equals(MergeElementType.COMPOSITION)
			||	type.equals(MergeElementType.OBJECT_FLOW)
			||	type.equals(MergeElementType.CONTROL_FLOW)
			||	type.equals(MergeElementType.STATE_TRANSITION))
			m = VisualstitchFactory.eINSTANCE.createArrowME();

		//ACTIVITY DIAGRAM
		else if(type.equals(MergeElementType.INITIAL_NODE)
				||	type.equals(MergeElementType.INITIAL_STATE))
			m = VisualstitchFactory.eINSTANCE.createInitialNodeME();

		else if(type.equals(MergeElementType.ACTIVITY_FINAL_NODE)
			||	type.equals(MergeElementType.FLOW_FINAL_NODE)
			||	type.equals(MergeElementType.FINAL_STATE))
			m = VisualstitchFactory.eINSTANCE.createFinalNodeME();

		else if(type.equals(MergeElementType.DECISION_MERGE_NODE)
			||	type.equals(MergeElementType.FORK_JOIN_NODE)
			||	type.equals(MergeElementType.DECISION_MERGE_STATE)
			||	type.equals(MergeElementType.FORK_JOIN_STATE))
			m = VisualstitchFactory.eINSTANCE.createControlNodeME();

		else{
			//DEFAULT TO GENERIC TYPE IN THE CASE THEY FOR THOSE WHICH ARE NOT DEFINED
			if(defaultAsElement)
				m = VisualstitchFactory.eINSTANCE.createMergeElement();
			else
				m = VisualstitchFactory.eINSTANCE.createConnectionME();
		}

		m.setType(type);

		return m;
	}

}
