/**
 */
package dk.dtu.imm.red.visualstitch;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Merge Element Resolver</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#getVisualElementToIDMap <em>Visual Element To ID Map</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#getVisualConnectionToIDMap <em>Visual Connection To ID Map</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.MergeElementResolver#getIDToMergeElementMap <em>ID To Merge Element Map</em>}</li>
 * </ul>
 * </p>
 *
 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElementResolver()
 * @model
 * @generated
 */
public interface MergeElementResolver extends EObject {
	/**
	 * Returns the value of the '<em><b>Visual Element To ID Map</b></em>' map.
	 * The key is of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visual Element To ID Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visual Element To ID Map</em>' map.
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElementResolver_VisualElementToIDMap()
	 * @model mapType="dk.dtu.imm.red.visualstitch.HashMapEntry.IVisualElementToStringMap<dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement, org.eclipse.emf.ecore.EString>"
	 * @generated
	 */
	EMap<IVisualElement, String> getVisualElementToIDMap();

	/**
	 * Returns the value of the '<em><b>Visual Connection To ID Map</b></em>' map.
	 * The key is of type {@link dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visual Connection To ID Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visual Connection To ID Map</em>' map.
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElementResolver_VisualConnectionToIDMap()
	 * @model mapType="dk.dtu.imm.red.visualstitch.HashMapEntry.IVisualConnectionToStringMap<dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection, org.eclipse.emf.ecore.EString>"
	 * @generated
	 */
	EMap<IVisualConnection, String> getVisualConnectionToIDMap();

	/**
	 * Returns the value of the '<em><b>ID To Merge Element Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link dk.dtu.imm.red.visualstitch.MergeElement},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID To Merge Element Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID To Merge Element Map</em>' map.
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getMergeElementResolver_IDToMergeElementMap()
	 * @model mapType="dk.dtu.imm.red.visualstitch.HashMapEntry.StringToMergeElementMap<org.eclipse.emf.ecore.EString, dk.dtu.imm.red.visualstitch.MergeElement>"
	 * @generated
	 */
	EMap<String, MergeElement> getIDToMergeElementMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void keepTrackOf(String id, MergeElement mergeElement, IVisualElement visualElement);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void keepTrackOf(String id, MergeElement mergeElement, IVisualConnection visualConnection);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void keepTrackOf(String id, MergeElement mergeElement);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	MergeElement lookupMergeElement(IVisualElement visualElement);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	MergeElement lookupMergeElement(IVisualConnection visualConnection);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	MergeElement lookupMergeElement(String id);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void destroy();

} // MergeElementResolver
