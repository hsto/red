/**
 */
package dk.dtu.imm.red.visualstitch;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage
 * @generated
 */
public interface VisualstitchFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VisualstitchFactory eINSTANCE = dk.dtu.imm.red.visualstitch.impl.VisualstitchFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Merge Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Merge Element</em>'.
	 * @generated
	 */
	MergeElement createMergeElement();

	/**
	 * Returns a new object of class '<em>Merge Element Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Merge Element Resolver</em>'.
	 * @generated
	 */
	MergeElementResolver createMergeElementResolver();

	/**
	 * Returns a new object of class '<em>Connection ME</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection ME</em>'.
	 * @generated
	 */
	ConnectionME createConnectionME();

	/**
	 * Returns a new object of class '<em>Arrow ME</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Arrow ME</em>'.
	 * @generated
	 */
	ArrowME createArrowME();

	/**
	 * Returns a new object of class '<em>Control Node ME</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Control Node ME</em>'.
	 * @generated
	 */
	ControlNodeME createControlNodeME();

	/**
	 * Returns a new object of class '<em>Initial Node ME</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Initial Node ME</em>'.
	 * @generated
	 */
	InitialNodeME createInitialNodeME();

	/**
	 * Returns a new object of class '<em>Final Node ME</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Final Node ME</em>'.
	 * @generated
	 */
	FinalNodeME createFinalNodeME();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	VisualstitchPackage getVisualstitchPackage();

} //VisualstitchFactory
