/**
 */
package dk.dtu.imm.red.visualstitch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arrow ME</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.ArrowME#getSrc <em>Src</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.ArrowME#getTgt <em>Tgt</em>}</li>
 * </ul>
 * </p>
 *
 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getArrowME()
 * @model
 * @generated
 */
public interface ArrowME extends MergeElement {
	/**
	 * Returns the value of the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src</em>' reference.
	 * @see #setSrc(MergeElement)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getArrowME_Src()
	 * @model
	 * @generated
	 */
	MergeElement getSrc();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.ArrowME#getSrc <em>Src</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src</em>' reference.
	 * @see #getSrc()
	 * @generated
	 */
	void setSrc(MergeElement value);

	/**
	 * Returns the value of the '<em><b>Tgt</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgt</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tgt</em>' reference.
	 * @see #setTgt(MergeElement)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getArrowME_Tgt()
	 * @model
	 * @generated
	 */
	MergeElement getTgt();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.ArrowME#getTgt <em>Tgt</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tgt</em>' reference.
	 * @see #getTgt()
	 * @generated
	 */
	void setTgt(MergeElement value);

} // ArrowME
