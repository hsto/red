package dk.dtu.imm.red.visualstitch.processor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import dk.dtu.imm.red.visualstitch.ArrowME;
import dk.dtu.imm.red.visualstitch.ConnectionME;
import dk.dtu.imm.red.visualstitch.ControlNodeME;
import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.MergeElementResolver;

public class MergeProcessor {

	//output data and the corresponding set of problem set which can't be merged due to different parent
	private Set<MergeElement> outputSet = new LinkedHashSet<MergeElement>();
	private List<MergeConflictException> listOfMergeConflict = new ArrayList<MergeConflictException>();

	//for storing the KnowledgeBase about all matches
	private MatchKB matchKB;
	private MergeElementResolver resolver;
	private MergeLogger mergeLogger = MergeLogger.instance;

	private Map<String, Map<String, MergeElement>> unmatchedPartitionMap;
	private MergeExecutor executor;

	public MergeProcessor(MergeExecutor executor,  Map<String, Map<String, MergeElement>> unmatchedPartitionMap) {
		this.executor = executor;
		this.resolver = executor.getResolver();
		matchKB = new MatchKB(resolver);
		this.unmatchedPartitionMap = unmatchedPartitionMap;
	}


	public void execute() throws MergeConflictException{
		//printing and populate first pass list of matching set
		printUnmatchedElement(unmatchedPartitionMap);

		List<Tuple<MergeElement>> listOfMatchesInThisPhase = new ArrayList<Tuple<MergeElement>>();
		boolean converge = false;

		try{
			//find matching, using fixed-point algorithm to terminate when no more matches
			while(!converge){
				findMatch(unmatchedPartitionMap,matchKB,listOfMatchesInThisPhase);

				if(listOfMatchesInThisPhase.isEmpty()){
					//set converge to true for exit condition
					converge = true;
					mergeLogger.append(MergeLogger.INFO,"FIND-MATCH TERMINATE!");
				}

				removeMatchFromPartition(unmatchedPartitionMap,listOfMatchesInThisPhase);
				listOfMatchesInThisPhase.clear();

			}
		}
		catch(MergeConflictException ex){
			//write the conflict log into the output element
			mergeLogger.append(MergeLogger.INFO,listOfMergeConflict.toString());
			throw ex;
		}

		//merge all matches
		mergeAllMatch();
		printOutputSet(outputSet);
	}


	private void findMatch(
			Map<String, Map<String, MergeElement>> unmatchedPartitionMap,
			MatchKB matchKB,
			List<Tuple<MergeElement>> listOfMatchesInThisPhase
			) throws MergeConflictException{

		mergeLogger.append(MergeLogger.INFO,".........  NEW FIND-MATCH PHASE .........");

		for (Map<String, MergeElement> partition : unmatchedPartitionMap
				.values()) {
			MergeElement[] arr = new MergeElement[partition.values().size()];
			partition.values().toArray(arr);

			for (int i=0;i<arr.length;i++){
				//split 2d array diagonally and compare only one side
				for(int j=i;j<arr.length;j++){
					if(i==j)
						continue;
					//skip comparison against element in same diagrams
					if(arr[i].getInputDiagramIndex()==arr[j].getInputDiagramIndex())
						continue;

					MatchResult matchResult = arr[i].match(arr[j], matchKB);
					if(matchResult==MatchResult.TRUE){
						Tuple<MergeElement> tuple = new Tuple<MergeElement>(arr[i],arr[j]);
						matchKB.addMatchingTuple(tuple);
						listOfMatchesInThisPhase.add(tuple);
					}
					else if(matchResult==MatchResult.FALSE){
						Tuple<MergeElement> tuple = new Tuple<MergeElement>(arr[i],arr[j]);
						matchKB.addNonMatchingTuple(tuple);
					}

				}
			}
		}
	}

	private void removeMatchFromPartition(
			Map<String, Map<String, MergeElement>> unmatchedPartitionMap2,
			List<Tuple<MergeElement>> listOfMatchingTuple) {
		for (Tuple<MergeElement> tuple : listOfMatchingTuple) {
			MergeElement x = tuple.getX();
			String type = x.getType();
			Map<String, MergeElement> partition = unmatchedPartitionMap2
					.get(type);
			partition.remove(x.getId());

			MergeElement y = tuple.getY();
			type = y.getType();
			partition = unmatchedPartitionMap2.get(type);
			partition.remove(y.getId());
		}

	}


	//go through the list of matching set, and merge them so that every matching set only have 1 element as output
	private void mergeAllMatch(){
		mergeLogger.append(MergeLogger.INFO,"---------  FINAL MATCH KNOWLEDGE BASE ----------");
		mergeLogger.append(MergeLogger.INFO, matchKB.getListOfMatchingSetString());

		mergeLogger.append(MergeLogger.DEBUG,"---------  MERGING THE RESULT ----------");
		//put all unmatched elements to the output set
		for(Map<String,MergeElement> partition : unmatchedPartitionMap.values()){
			for(MergeElement m : partition.values()){
				outputSet.add(m);

				String name="";
				if(m.getInputVisualElement()!=null && m.getInputVisualElement().size()>0){
					name = m.getInputVisualElement().get(0).getName();
				}
				else{
					name = m.getInputVisualConnection().get(0).getName();
				}
				mergeLogger.append(MergeLogger.DEBUG,"Kicking unmatched element " + m.getId() + "("+name+")"+ " to output set");
			}
		}

		//merge all the matched elements until one left and then kick out to output set
		Iterator<Set<MergeElement>> listOfMatchingSetIter = matchKB.getListOfMatchingSet().iterator();
		while(listOfMatchingSetIter.hasNext()){
			Set<MergeElement> matchingSet = listOfMatchingSetIter.next();

			Iterator<MergeElement> matchingSetIter = matchingSet.iterator();
			MergeElement m1 = matchingSetIter.next();
			while(matchingSetIter.hasNext()){

				MergeElement m2 = matchingSetIter.next();

				//set default merging src and target
				MergeElement mergeSrc=m2;
				MergeElement mergeTgt=m1;

//				System.out.println(m1+"----- M1 hash value is " + m1.hashValues());
//				System.out.println(m2+"----- M2 hash value is " + m2.hashValues());

				//identify the right merge source and merge target. Element with parent is root is preferred to be merged
				if(m2.getParent()==null){
					//use default mergeSrc and mergeTgt
				}
				else if(m1.getParent()==null){
					mergeSrc = m1;
					mergeTgt = m2;
				}
				else if(m1.getParent().equals(m2.getParent(),matchKB)==MatchResult.TRUE){
					if(m1.hashValues() < m2.hashValues()){
						mergeSrc = m1;
						mergeTgt = m2;
					}
					else{
						//use default mergeSrc and mergeTgt
					}
				}
				else{
					//conflict, this code should not be entered because we already checked for problems
					mergeLogger.append(MergeLogger.INFO,"Something Unexpected happen during merge of " + m1 + " AND " + m2);
				}


				//copy from mergeSrc to mergeTgt
				mergeLogger.append(MergeLogger.DEBUG,"Merging "+ mergeSrc.getId() + " to " + mergeTgt.getId());

				List<MergeElement> mergeSrcChildren = mergeSrc.getChildren();
				if(mergeSrcChildren!=null){
					for(MergeElement child : mergeSrc.getChildren()){
						child.setParent(mergeTgt);
						mergeTgt.getChildren().add(child);
					}
				}

				//clear all references in mergeSrc
				if(mergeSrc.getParent()!=null){
					mergeSrc.getParent().getChildren().remove(mergeSrc);
				}
				mergeSrc.setParent(null);
				mergeSrc.getChildren().clear();

				MergeUtil.copyValues(mergeSrc,mergeTgt);
				MergeUtil.copyInputVisElOrConn(mergeSrc, mergeTgt);
				updateReferences(mergeSrc,mergeTgt,unmatchedPartitionMap,matchKB);

				if(!matchingSetIter.hasNext()){
					//last element to process, kick out the mergeTgt to outputSet
					outputSet.add(mergeTgt);
					mergeLogger.append(MergeLogger.DEBUG,"Kicking " + mergeTgt.getId() + " to outputSet");
				}
				//set m1 before advancing pointer
				m1 = mergeTgt;
			}
		}

	}

	private void updateReferences(MergeElement mergeSrc, MergeElement mergeTgt,
			Map<String, Map<String, MergeElement>> unmatchedPartitionMap,
			 MatchKB matchKB){
		Set<MergeElement> allMergeElement = new HashSet<MergeElement>();

		for(Map<String,MergeElement> partition : unmatchedPartitionMap.values()){
			allMergeElement.addAll(partition.values());
		}
		for(Set<MergeElement> matchingSet : matchKB.getListOfMatchingSet()){
			allMergeElement.addAll(matchingSet);
		}

		//go through all merge element with references and if they
		//contain references to mergeSrc, change it to mergeTgt instead
		for(MergeElement me : allMergeElement){

			//check connection ends
			if(me instanceof ConnectionME){
				MergeElement connEnd1 = ((ConnectionME) me).getConnEnd1();
				if(connEnd1==mergeSrc){
					((ConnectionME) me).setConnEnd1(mergeTgt);
				}
				MergeElement connEnd2 = ((ConnectionME) me).getConnEnd2();
				if(connEnd2==mergeSrc){
					((ConnectionME) me).setConnEnd2(mergeTgt);
				}
			}

			//check src tgt
			if(me instanceof ArrowME){
				MergeElement arrowSrc = ((ArrowME) me).getSrc();
				if(arrowSrc==mergeSrc){
					((ArrowME) me).setSrc(mergeTgt);
				}
				MergeElement arrowTgt = ((ArrowME) me).getTgt();
				if(arrowTgt==mergeSrc){
					((ArrowME) me).setTgt(mergeTgt);
				}
			}

			//check incoming outgoing
			if(me instanceof ControlNodeME){
				List<MergeElement> incomings = ((ControlNodeME) me).getIncoming();
				if(incomings.contains(mergeSrc)){
					incomings.remove(mergeSrc);
					incomings.add(mergeTgt);
				}
				List<MergeElement> outgoings = ((ControlNodeME) me).getOutgoing();
				if(outgoings.contains(mergeSrc)){
					outgoings.remove(mergeSrc);
					outgoings.add(mergeTgt);
				}
			}


		}

		allMergeElement.clear();
	}


	// --------------- GETTER AND SETTER ----------------------
	public MatchKB getMatchKB() {
		return matchKB;
	}

	public Set<MergeElement> getOutputSet() {
		return outputSet;
	}

	public List<MergeConflictException> getListOfMergeConflict() {
		return listOfMergeConflict;
	}

	public void destroy(){
		unmatchedPartitionMap.clear();
		outputSet.clear();
		listOfMergeConflict.clear();
		resolver.destroy();
		matchKB.destroy();
	}





	// --------------- METHODS FOR PRINTING ----------------------


	public void printUnmatchedElement(Map<String, Map<String, MergeElement>> unmatchedPartitionMap) {
		mergeLogger.append(MergeLogger.DEBUG,"--------- UNMATCHED PARTITION CONTENTS: ---------");
		for (Entry<String, Map<String, MergeElement>> entry : unmatchedPartitionMap
				.entrySet()) {
			mergeLogger.append(MergeLogger.DEBUG,"TYPE:" + entry.getKey());
			Map<String, MergeElement> map = entry.getValue();
			for (MergeElement mergeElement : map.values()) {
				mergeLogger.append(MergeLogger.DEBUG,mergeElement.toString());
			}
		}
	}

	public void printOutputSet(Set<MergeElement> outputSet){
		mergeLogger.append(MergeLogger.DEBUG,"--------- OUTPUT SET: ---------");
		SortedSet<MergeElement> sortedOutput = new TreeSet<MergeElement>(
		new Comparator<MergeElement>(){
			@Override
			public int compare(MergeElement m1, MergeElement m2) {
				if(StringUtils.isNumeric(m1.getId()) && StringUtils.isNumeric(m2.getId())){
					return Integer.parseInt(m1.getId())-Integer.parseInt(m2.getId());
				}
				return m1.getId().compareTo(m2.getId());
			}
		}
		);

		sortedOutput.addAll(outputSet);
		for(MergeElement m: sortedOutput){
			mergeLogger.append(MergeLogger.DEBUG,m.toString());
		}
	}

}
