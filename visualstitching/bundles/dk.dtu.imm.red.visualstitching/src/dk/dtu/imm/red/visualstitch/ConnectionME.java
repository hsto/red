/**
 */
package dk.dtu.imm.red.visualstitch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection ME</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.visualstitch.ConnectionME#getConnEnd1 <em>Conn End1</em>}</li>
 *   <li>{@link dk.dtu.imm.red.visualstitch.ConnectionME#getConnEnd2 <em>Conn End2</em>}</li>
 * </ul>
 * </p>
 *
 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getConnectionME()
 * @model
 * @generated
 */
public interface ConnectionME extends MergeElement {
	/**
	 * Returns the value of the '<em><b>Conn End1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conn End1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conn End1</em>' reference.
	 * @see #setConnEnd1(MergeElement)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getConnectionME_ConnEnd1()
	 * @model
	 * @generated
	 */
	MergeElement getConnEnd1();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.ConnectionME#getConnEnd1 <em>Conn End1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conn End1</em>' reference.
	 * @see #getConnEnd1()
	 * @generated
	 */
	void setConnEnd1(MergeElement value);

	/**
	 * Returns the value of the '<em><b>Conn End2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conn End2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conn End2</em>' reference.
	 * @see #setConnEnd2(MergeElement)
	 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getConnectionME_ConnEnd2()
	 * @model
	 * @generated
	 */
	MergeElement getConnEnd2();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.visualstitch.ConnectionME#getConnEnd2 <em>Conn End2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conn End2</em>' reference.
	 * @see #getConnEnd2()
	 * @generated
	 */
	void setConnEnd2(MergeElement value);

} // ConnectionME
