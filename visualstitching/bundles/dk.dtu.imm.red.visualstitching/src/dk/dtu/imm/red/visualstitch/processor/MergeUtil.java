package dk.dtu.imm.red.visualstitch.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualstitch.CustomAttributesKind;
import dk.dtu.imm.red.visualstitch.MatchResult;
import dk.dtu.imm.red.visualstitch.MergeElement;

public class MergeUtil {

	public static final String CUSTOM_ATTRIBUTES_SEPARATOR = "%%";
	public static final String CUSTOM_ATTRIBUTES_VALUE_SEPARATOR = ":=";


	public static String getTypeFromVisualElement(IVisualElement element) {
		return element.getClass().getSimpleName().replaceAll("Impl", "");
	}

	public static String getTypeFromVisualConnection(IVisualConnection connection) {
		if(connection.getType()!=null)
			return connection.getType();
		else
			return connection.getClass().getSimpleName().replaceAll("Impl", "");
	}

	// --------------- METHODS FOR MATCHING ----------------------
	public static MatchResult canMatchByName(MergeElement m1, MergeElement m2,
			MatchResult emptyNameBehavior) {

		String name1 = m1.getName();
		String name2 = m2.getName();

		// both empty
		if ((name1 == null || name1.isEmpty())
				&& (name2 == null || name2.isEmpty()))
			return emptyNameBehavior;
		// any one is empty
		else if (name1 == null || name1.isEmpty() || name2 == null
				|| name2.isEmpty())
			return MatchResult.TRUE;
		else if (isSimilarString(name1, name2))
			return MatchResult.TRUE;
		else
			return MatchResult.FALSE;
	}

	public static boolean isSimilarString(String str1, String str2) {
		int editDistance = StringUtils.getLevenshteinDistance(str1
				.toUpperCase().trim(), str2.toUpperCase().trim());

		int editDistanceThreshold = PreferenceUtil
				.getPreferenceInt(PreferenceConstants.STITCH_PREF_EDIT_DISTANCE_THRESHOLD);
		String threshold = PreferenceUtil
				.getPreferenceString(PreferenceConstants.STITCH_PREF_EDIT_DISTANCE_THRESHOLD);
		return editDistance <= editDistanceThreshold;
	}

	public static boolean isSimilarString(String str1, String str2,
			int editDistanceThreshold) {
		int editDistance = StringUtils.getLevenshteinDistance(str1
				.toUpperCase().trim(), str2.toUpperCase().trim());
		return editDistance <= editDistanceThreshold;
	}

	public static MatchResult canMatchByCustomAttr(MergeElement m1,
			MergeElement m2) throws MergeConflictException {
		Map<String, String> customAttr = m1.getCustomAttributes().map();
		for (String key : customAttr.keySet()) {
			String value = customAttr.get(key);
			String value2 = m2.getCustomAttributes().get(key);

			if (key.equals(CustomAttributesKind.VISIBILITY.getName())) {
				// special handling for operation and attribute visibility
				// if they do not match, it is a merge conflict
				if (value != null && !value.isEmpty() && value2 != null
						&& !value2.isEmpty() && !value.equals(value2)) {
					List<MergeElement> conflictSource = new ArrayList<MergeElement>();
					conflictSource.add(m1);
					conflictSource.add(m2);
					throw new MergeConflictException(conflictSource,
							"Unmatchable visibility");
				}
			}

			if (value2 != null) {
				if (!isSimilarString(value, value2)) {
					return MatchResult.FALSE;
				}
			}
		}

		return MatchResult.TRUE;
	}

	// --------------- METHODS FOR MERGING ----------------------
	public static void copyValues(MergeElement mergeSrc, MergeElement mergeTgt) {
		for (String srcKey : mergeSrc.getCustomAttributes().keySet()) {
			String srcValue = mergeSrc.getCustomAttributes().get(srcKey);
			String tgtKey = mergeTgt.getCustomAttributes().get(srcKey);
			if (tgtKey == null) {
				// key does not exist
				mergeTgt.getCustomAttributes().put(srcKey, srcValue);
			}
			// key already exist, do nothing
		}
	}

	public static void copyInputVisElOrConn(MergeElement mergeSrc,
			MergeElement mergeTgt) {
		if (mergeTgt.getInputVisualElement().size() > 0) {
			mergeTgt.getInputVisualElement().add(
					mergeSrc.getInputVisualElement().get(0));
		} else if (mergeTgt.getInputVisualConnection().size() > 0) {
			mergeTgt.getInputVisualConnection().add(
					mergeSrc.getInputVisualConnection().get(0));
		}
	}

	public static String fixedLengthString(String string, int length) {
		return String.format("%1$" + length + "s", string);
	}

}
