/**
 */
package dk.dtu.imm.red.visualstitch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Final Node ME</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.visualstitch.VisualstitchPackage#getFinalNodeME()
 * @model
 * @generated
 */
public interface FinalNodeME extends ControlNodeME {
} // FinalNodeME
