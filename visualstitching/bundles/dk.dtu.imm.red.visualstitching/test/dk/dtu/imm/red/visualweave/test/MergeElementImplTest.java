package dk.dtu.imm.red.visualweave.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;
import dk.dtu.imm.red.visualweave.MergeElement;
import dk.dtu.imm.red.visualweave.MergeElementResolver;
import dk.dtu.imm.red.visualweave.VisualweaveFactory;
import dk.dtu.imm.red.visualweave.processor.MergeElementFactory;
import dk.dtu.imm.red.visualweave.processor.MergeElementType;

public class MergeElementImplTest {

	MergeElementResolver resolver = VisualweaveFactory.eINSTANCE.createMergeElementResolver();
	MergeElement m1,m2,m3,m4,m5,m6,m7,m8,m9;
	VisualElement v1,v2;
	VisualElement ve1,ve2;
	VisualConnection v3,ve3;
	VisualDiagram d1, d2;


	@Before
	public void setUp() throws Exception {
		v1 = visualmodelFactory.eINSTANCE.createVisualElement();
		v2 = visualmodelFactory.eINSTANCE.createVisualElement();
		v2.setParent(v1);
		v1.getElements().add(v1);
		v3 = visualmodelFactory.eINSTANCE.createVisualConnection();
		v3.setSource(v1);
		v3.setTarget(v2);

		d1 = visualmodelFactory.eINSTANCE.createVisualDiagram();
		d1.getElements().add(v1);
		d1.getElements().add(v2);
		d1.getDiagramConnections().add(v3);

		ve1 = visualmodelFactory.eINSTANCE.createVisualElement();
		ve2 = visualmodelFactory.eINSTANCE.createVisualElement();
		ve2.setParent(ve1);
		ve1.getElements().add(ve1);
		ve1.getElements().add(ve2);
		ve3 = visualmodelFactory.eINSTANCE.createVisualConnection();
		ve3.setSource(ve1);
		ve3.setTarget(ve2);

		d2 = visualmodelFactory.eINSTANCE.createVisualDiagram();
		d2.getElements().add(ve1);
		d2.getDiagramConnections().add(ve3);

		m1 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m2 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m3 = MergeElementFactory.makeMergeElement(MergeElementType.ASSOCIATION);
		m4 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m5 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m6 = MergeElementFactory.makeMergeElement(MergeElementType.ASSOCIATION);

		m7 = MergeElementFactory.makeMergeElement(MergeElementType.PACKAGE);
		m8 = MergeElementFactory.makeMergeElement(MergeElementType.PACKAGE);
		m9 = MergeElementFactory.makeMergeElement(MergeElementType.DECISION_MERGE_NODE);

	}

	@Test
	public void testInitialize() {
		m1.initialize(resolver, 1, v1, null);
		m2.initialize(resolver, 1, v2, null);
		m3.initialize(resolver, 1, null, v3);

		Assert.assertSame(v1,m1.getInputVisualElement().get(0));
		Assert.assertSame(v2,m2.getInputVisualElement().get(0));
		Assert.assertSame(v3,m3.getInputVisualConnection().get(0));
		Assert.assertSame(m2.getParent(),m1);
		Assert.assertSame(m1.getChildren().get(0),m2);


		m4.initialize(resolver, 2, ve1, null);
		m5.initialize(resolver, 2, ve2, null);
		m6.initialize(resolver, 2, null, ve3);

		Assert.assertSame(ve1,m4.getInputVisualElement().get(0));
		Assert.assertSame(ve2,m5.getInputVisualElement().get(0));
		Assert.assertSame(m5.getParent(),m4);
		Assert.assertSame(m4.getChildren().get(0),m5);
		Assert.assertSame(ve3,m6.getInputVisualConnection().get(0));

	}

	@Test
	public void testSetParent() {
		m6.setParent(m7);
		Assert.assertTrue(m6.getParent()==m7);
	}

	@Test
	public void testAddChild() {
		m8.getChildren().add(m7);
		Assert.assertTrue(m8.getChildren().contains(m7));
	}

}
