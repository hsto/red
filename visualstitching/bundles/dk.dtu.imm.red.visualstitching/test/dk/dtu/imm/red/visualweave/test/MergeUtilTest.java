package dk.dtu.imm.red.visualweave.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.IVisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;
import dk.dtu.imm.red.visualmodeling.visualmodel.goal.GoalFactory;
import dk.dtu.imm.red.visualweave.MatchResult;
import dk.dtu.imm.red.visualweave.MergeElement;
import dk.dtu.imm.red.visualweave.MergeElementResolver;
import dk.dtu.imm.red.visualweave.processor.MergeConflictException;
import dk.dtu.imm.red.visualweave.processor.MergeElementFactory;
import dk.dtu.imm.red.visualweave.processor.MergeElementType;
import dk.dtu.imm.red.visualweave.processor.MergeLogger;
import dk.dtu.imm.red.visualweave.processor.MergeProcessor;
import dk.dtu.imm.red.visualweave.processor.MergeUtil;

public class MergeUtilTest {
    @Rule public TestName name = new TestName();

    MergeLogger mergeLogger = MergeLogger.instance;

	@Before
	public void setUp() throws Exception {
		mergeLogger.setLogInRCPMode(false);
	}

//	@Test
//	public void testPopulatePartition() {
//		MergeProcessor processor = new MergeProcessor();
//		VisualDiagram d1 = visualmodelFactory.eINSTANCE.createVisualDiagram();
//		IVisualElement layer1 = GoalFactory.eINSTANCE.createVisualGoalLayer();
//		IVisualElement goal1 = GoalFactory.eINSTANCE.createVisualGoalElement();
//		IVisualElement goal2 = GoalFactory.eINSTANCE.createVisualGoalElement();
//		IVisualElement goal3 = GoalFactory.eINSTANCE.createVisualGoalElement();
//		IVisualConnection conn1 = visualmodelFactory.eINSTANCE.createVisualConnection();
//		conn1.setSource(goal1);
//		conn1.setTarget(goal2);
//
//		d1.getElements().add(layer1);
//		d1.getElements().add(goal1);
//		d1.getElements().add(goal2);
//		layer1.getElements().add(goal3);
//		d1.getDiagramConnections().add(conn1);
//
//		VisualDiagram d2 = visualmodelFactory.eINSTANCE.createVisualDiagram();
//		MergeUtil.populatePartition(
//				processor.getUnmatchedPartitionMap(), processor.getResolver(),
//				d1, d2);
//
//		 Map<String, Map<String, MergeElement>> unmatchedPartitionMap = processor.getUnmatchedPartitionMap();
//		 Assert.assertTrue(unmatchedPartitionMap.size()==3);
//
//		 Map<String, MergeElement> allElements = new HashMap<String,MergeElement>();
//		 for(Map<String,MergeElement> partition : unmatchedPartitionMap.values()){
//			 allElements.putAll(partition);
//		 }
//		 Assert.assertTrue(allElements.size()==5);
//	}

	@Test
	public void testCanMatchByName() {
		MergeElement m1 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m1.setName("Librarian");
		MergeElement m2 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m2.setName("librarian");
		MergeElement m3 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m3.setName("");
		MergeElement m4 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m4.setName("");
		Assert.assertTrue(MergeUtil.canMatchByName(m1, m2,MatchResult.TRUE)==MatchResult.TRUE);
		Assert.assertTrue(MergeUtil.canMatchByName(m1, m3,MatchResult.TRUE)==MatchResult.TRUE);
		Assert.assertTrue(MergeUtil.canMatchByName(m3, m4,MatchResult.FALSE)==MatchResult.FALSE);

	}

	@Test
	public void testIsSimilarString() {
		String s1 = "Class1";
		String s2 = "class1";
		String s3 = "Class2";
		String s4 = "Cla";

		String s5 = "CheckOutBooksFromSystem";
		String s6 = "Check_OutBook from_Sytem";

		Assert.assertTrue(MergeUtil.isSimilarString(s1, s2,0));
		Assert.assertFalse(MergeUtil.isSimilarString(s1, s3,0));
		Assert.assertTrue(MergeUtil.isSimilarString(s1, s3,1));
		Assert.assertFalse(MergeUtil.isSimilarString(s1, s4,1));
		Assert.assertTrue(MergeUtil.isSimilarString(s5, s6,5));
	}

	@Test
	public void testCanMatchByProperties() {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"",MergeElementType.ASSOCIATION,-1,
				testUtil.genCustAttrAssoc("", "", "1", "1..*"));

		testUtil.add(1,2,"",MergeElementType.ASSOCIATION,-1,
				testUtil.genCustAttrAssoc("roleSrc", "roleTgt", "", "1..*"));

		testUtil.add(1,3,"",MergeElementType.ASSOCIATION,-1,
				testUtil.genCustAttrAssoc("aRole", "myOtherRole", "2", "1..*"));

		try {
			testUtil.executeMerge();
		} catch (MergeConflictException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MergeElement m1 = testUtil.getResolver().lookupMergeElement("1");
		MergeElement m2 = testUtil.getResolver().lookupMergeElement("2");
		MergeElement m3 = testUtil.getResolver().lookupMergeElement("3");

		try{
			Assert.assertTrue(MergeUtil.canMatchByCustomAttr(m1, m2)==MatchResult.TRUE);
			Assert.assertTrue(MergeUtil.canMatchByCustomAttr(m1, m3)==MatchResult.FALSE);
			Assert.assertTrue(MergeUtil.canMatchByCustomAttr(m2, m3)==MatchResult.FALSE);
		}
		catch(MergeConflictException ex){
			mergeLogger.append(MergeLogger.INFO,ex.toString());
		}
	}

	@Test
	public void testHasBeenMatched() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"A",MergeElementType.CLASS,-1, "");
		testUtil.add(2,51,"a",MergeElementType.CLASS,-1,"");
		testUtil.add(2,52,"xyz",MergeElementType.CLASS,-1,"");

		testUtil.executeMerge();

		MergeElementResolver resolver = testUtil.getResolver();
		MergeElement m1 = resolver.lookupMergeElement("1");
		MergeElement m2 = resolver.lookupMergeElement("51");
		MergeElement m3 = resolver.lookupMergeElement("52");

		Assert.assertTrue(m1.equals(m2,testUtil.getProcessor().getMatchKB())==MatchResult.TRUE);
		Assert.assertFalse(m1.equals(m3,testUtil.getProcessor().getMatchKB())==MatchResult.TRUE);
	}


}
