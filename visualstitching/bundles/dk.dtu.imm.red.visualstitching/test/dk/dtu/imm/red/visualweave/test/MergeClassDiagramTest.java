package dk.dtu.imm.red.visualweave.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import dk.dtu.imm.red.visualweave.MergeElement;
import dk.dtu.imm.red.visualweave.MergeElementResolver;
import dk.dtu.imm.red.visualweave.processor.MergeConflictException;
import dk.dtu.imm.red.visualweave.processor.MergeElementType;
import dk.dtu.imm.red.visualweave.processor.MergeLogger;

public class MergeClassDiagramTest {
    @Rule public TestName name = new TestName();
    MergeLogger mergeLogger = MergeLogger.instance;


	@Before
	public void setUp() throws Exception {
		mergeLogger.setLogInRCPMode(false);
	}

	@Test
	public void testClassDiag1() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(2,51,"medium",MergeElementType.CLASS,-1,"");

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		testUtil.destroy();
	}

	@Test
	public void testClassDiag2() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(2,52,"Librarian",MergeElementType.CLASS,-1,"");

		testUtil.executeMerge();
		mergeLogger.append(MergeLogger.DEBUG,testUtil.getProcessor().getMatchKB().toString());
		MergeElementResolver resolver = testUtil.getResolver();
		MergeElement m1 = resolver.lookupMergeElement("1");
		MergeElement m2 = resolver.lookupMergeElement("52");
		mergeLogger.append(MergeLogger.DEBUG,"IS A MATCH:"+testUtil.getProcessor().getMatchKB().isAMatch(m1,m2));
		Assert.assertFalse(testUtil.checkMatch(1, 52));
		testUtil.destroy();
	}

	@Test
	public void testClassDiag3() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"title",MergeElementType.ATTRIBUTE,1, "");


		testUtil.add(2,51,"Medium",MergeElementType.CLASS,-1,"");
		testUtil.add(2,53,"publisher",MergeElementType.ATTRIBUTE,51, "");

		testUtil.executeMerge();

		mergeLogger.append(MergeLogger.DEBUG,testUtil.getProcessor().getMatchKB().toString());
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkParent(2, 1));
		Assert.assertTrue(testUtil.checkParent(53, 1));

		testUtil.destroy();
	}

	@Test
	public void testClassDiag4() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"title",MergeElementType.ATTRIBUTE,1, "");


		testUtil.add(2,51,"medium",MergeElementType.CLASS,-1,"");
		testUtil.add(2,52,"Title",MergeElementType.ATTRIBUTE,51, "");

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkParent(2, 1));

		testUtil.destroy();
	}

	@Test
	public void testClassDiag5() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"checkout",MergeElementType.ATTRIBUTE,1, testUtil.genCustAttrOperation("", "String title","boolean"));


		testUtil.add(2,51,"Medium",MergeElementType.CLASS,-1,"");
		testUtil.add(2,53,"checkout",MergeElementType.ATTRIBUTE,51, testUtil.genCustAttrOperation("", "int id","boolean"));

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertFalse(testUtil.checkMatch(2, 53));
		Assert.assertTrue(testUtil.checkParent(2, 1));
		Assert.assertTrue(testUtil.checkParent(53, 1));

		testUtil.destroy();
	}

	@Test(expected=MergeConflictException.class)
	public void testClassDiag6() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"title",MergeElementType.ATTRIBUTE,1, testUtil.genCustAttrOperation("+", "",""));


		testUtil.add(2,51,"Medium",MergeElementType.CLASS,-1,"");
		testUtil.add(2,52,"title",MergeElementType.ATTRIBUTE,51, testUtil.genCustAttrOperation("-", "",""));

		testUtil.executeMerge();
		testUtil.destroy();
	}

	@Test
	public void testClassDiag7() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"title",MergeElementType.ATTRIBUTE,1, "");

		testUtil.add(2,51,"Medium",MergeElementType.CLASS,-1,"");
		testUtil.add(2,53,"Librarian",MergeElementType.CLASS,-1, "");
		testUtil.add(2,54,"",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(54, 51, 53);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkParent(2, 1));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();
	}

	@Test
	public void testClassDiag8() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,11,"medium",MergeElementType.CLASS,-1, "");

		testUtil.add(2,51,"Medium",MergeElementType.CLASS,-1,"");
		testUtil.add(2,52,"Librarian",MergeElementType.CLASS,-1, "");
		testUtil.add(2,53,"",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(53, 51, 52);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(11, 51));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();
	}

	@Test
	public void testClassDiag9() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"Publisher",MergeElementType.CLASS,-1, "");
		testUtil.add(1,3,"publish",MergeElementType.ASSOCIATION,-1,"");
		testUtil.assignSrcAndTarget(3, 1, 2);

		testUtil.add(2,51,"Medium",MergeElementType.CLASS,-1,"");
		testUtil.add(2,52,"Publisher",MergeElementType.CLASS,-1, "");
		testUtil.add(2,53,"",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(53, 51, 52);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3,53));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();
	}


	@Test
	public void testClassDiag10() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"Author",MergeElementType.CLASS,-1, "");
		testUtil.add(1,3,"author",MergeElementType.ASSOCIATION,-1,"");
		testUtil.add(1,4,"co-author",MergeElementType.ASSOCIATION,-1,"");
		testUtil.assignSrcAndTarget(3, 1, 2);
		testUtil.assignSrcAndTarget(4, 1, 2);

		testUtil.add(2,51,"Medium",MergeElementType.CLASS,-1,"");
		testUtil.add(2,52,"Author",MergeElementType.CLASS,-1, "");
		testUtil.add(2,53,"",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(53, 51, 52);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3,53));
		Assert.assertFalse(testUtil.checkMatch(3,4));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();
	}

	@Test
	public void testClassDiag11() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"Author",MergeElementType.CLASS,-1, "");
		testUtil.add(1,3,"written by",MergeElementType.ASSOCIATION,-1,
				testUtil.genCustAttrAssoc("", "", "0..*", "1"));
		testUtil.assignSrcAndTarget(3, 1, 2);

		testUtil.add(2,51,"Medium",MergeElementType.CLASS,-1,"");
		testUtil.add(2,52,"Author",MergeElementType.CLASS,-1, "");
		testUtil.add(2,53,"",MergeElementType.ASSOCIATION,-1,
				testUtil.genCustAttrAssoc("medium", "author", "", ""));
		testUtil.assignSrcAndTarget(53, 51, 52);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3,53));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();
	}

	@Test
	public void testClassDiag12() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"Author",MergeElementType.CLASS,-1, "");
		testUtil.add(1,3,"coauthor",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(3, 1, 2);

		testUtil.add(2,50,"Library",MergeElementType.PACKAGE,-1,"");
		testUtil.add(2,51,"Medium",MergeElementType.CLASS,50,"");
		testUtil.add(2,52,"Author",MergeElementType.CLASS,50, "");
		testUtil.add(2,54,"author",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(54, 51, 52);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertFalse(testUtil.checkMatch(3, 54));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();
	}


	@Test
	public void testClassDiag13() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,3,"Medium",MergeElementType.CLASS,-1, "");

		testUtil.add(2,51,"Library",MergeElementType.PACKAGE,-1, "");
		testUtil.add(2,52,"Catalog",MergeElementType.PACKAGE,51, "");
		testUtil.add(2,53,"Medium",MergeElementType.CLASS,52, "");

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		Assert.assertTrue(testUtil.checkParent(53, 52));
		Assert.assertTrue(testUtil.checkParent(52, 51));

		testUtil.destroy();
	}

	@Test(expected=MergeConflictException.class)
	public void testClassDiag14() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Library",MergeElementType.PACKAGE,-1, "");
		testUtil.add(1,3,"Medium",MergeElementType.CLASS,1, "");

		testUtil.add(2,52,"Catalog",MergeElementType.PACKAGE,-1, "");
		testUtil.add(2,53,"Medium",MergeElementType.CLASS,52, "");

		testUtil.executeMerge();
		testUtil.destroy();

	}

	@Test
	public void testClassDiag15() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,2,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,3,"relatedTo",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(3,2, 2);

		testUtil.add(2,51,"Catalog",MergeElementType.PACKAGE,-1, "");
		testUtil.add(2,52,"Medium",MergeElementType.CLASS,51, "");

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkParent(52, 51));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();

	}

	@Test
	public void testClassDiag16() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"",MergeElementType.CLASS,-1, "");

		testUtil.add(2,52,"",MergeElementType.CLASS,-1, "");

		testUtil.executeMerge();
		Assert.assertFalse(testUtil.checkMatch(1, 52));
		testUtil.destroy();

	}


	@Test
	public void testClassDiag18() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Catalog",MergeElementType.CLASS,-1, "");
		testUtil.add(1,2,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(1,3,"Book",MergeElementType.CLASS,-1, "");
		testUtil.add(1,4,"Article",MergeElementType.CLASS,-1, "");
		testUtil.add(1,5,"Consist of",MergeElementType.COMPOSITION,-1, "");
		testUtil.assignSrcAndTarget(5, 1, 2);
		testUtil.add(1,6,"",MergeElementType.GENERALIZATION,-1, "");
		testUtil.assignSrcAndTarget(6, 3, 2);
		testUtil.add(1,7,"",MergeElementType.GENERALIZATION,-1, "");
		testUtil.assignSrcAndTarget(7, 4, 2);

		testUtil.add(1,31,"Member",MergeElementType.CLASS,-1, "");
		testUtil.add(1,32,"Faculty",MergeElementType.CLASS,-1, "");
		testUtil.add(1,33,"Student",MergeElementType.CLASS,-1, "");
		testUtil.add(1,34,"",MergeElementType.GENERALIZATION,-1, "");
		testUtil.assignSrcAndTarget(34,32, 31);
		testUtil.add(1,35,"",MergeElementType.GENERALIZATION,-1, "");
		testUtil.assignSrcAndTarget(35, 33, 32);

		testUtil.add(2,51,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(2,52,"title",MergeElementType.ATTRIBUTE,-1, "");
		testUtil.add(2,53,"author",MergeElementType.ATTRIBUTE,-1, "");
		testUtil.add(2,54,"publishedDate",MergeElementType.ATTRIBUTE,-1, "");

		testUtil.add(2,71,"Librarian",MergeElementType.CLASS,-1, "");
		testUtil.add(2,72,"Medium",MergeElementType.CLASS,-1, "");
		testUtil.add(2,73,"Member",MergeElementType.CLASS,-1, "");
		testUtil.add(2,74,"issued by",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(74, 72, 71);
		testUtil.add(2,75,"reserved by",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(75, 72, 73);
		testUtil.add(2,76,"manages",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(76, 71, 73);

		testUtil.executeMerge();
		testUtil.destroy();

	}

//	@Test
//	public void testComplexTest(){
//		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
//
//		testUtil.add(1,1,"P",MergeElementType.PACKAGE,-1, "");
//		testUtil.add(1,2,"A",MergeElementType.CLASS,1, "");
//		testUtil.add(1,3,"C",MergeElementType.CLASS,1, "");
//		testUtil.add(1,4,"P",MergeElementType.PACKAGE,-1, "");
//		testUtil.add(1,5,"B",MergeElementType.CLASS,4, "");
//		testUtil.add(1,6,"D",MergeElementType.ATTRIBUTE,5, "");
//
//		testUtil.add(1,7,"S",MergeElementType.PACKAGE,-1, "");
//		testUtil.add(1,8,"R",MergeElementType.PACKAGE,7, "");
//		testUtil.add(1,9,"P",MergeElementType.PACKAGE,8, "");
//		testUtil.add(1,10,"B",MergeElementType.CLASS,9,  "");
//		testUtil.add(1,11,"D",MergeElementType.ATTRIBUTE,10, "");
//		//associate A to B
//		testUtil.add(1,12,"",MergeElementType.ASSOCIATION,-1, "");
//		testUtil.assignSrcAndTarget(12,2,10);
//		//associate C to B
//		testUtil.add(1,13,"Assoc1",MergeElementType.ASSOCIATION,-1, "");
//		testUtil.assignSrcAndTarget(13,3,10);
//		testUtil.add(1,14,"Assoc2",MergeElementType.ASSOCIATION,-1, "");
//		testUtil.assignSrcAndTarget(14,3,10);
//
//
//		//second Diagram, class diagram contents
//		testUtil.add(2,50,"R",MergeElementType.PACKAGE,-1, "");
//		testUtil.add(2,51,"P",MergeElementType.PACKAGE,50, "");
//		testUtil.add(2,52,"B",MergeElementType.CLASS,51, "");
//		testUtil.add(2,53,"D",MergeElementType.ATTRIBUTE,52, "");
//		testUtil.add(2,54,"C",MergeElementType.CLASS,51, "");
//		testUtil.add(2,55,"E",MergeElementType.ATTRIBUTE,54, "");
//		//associate B to C
//		testUtil.add(2,56,"Assoc2",MergeElementType.ASSOCIATION,-1, "");
//		testUtil.assignSrcAndTarget(56,52,54);
//		testUtil.executeMerge();
//
//		Assert.assertTrue(testUtil.checkMatch(1,4,9,51));
//		Assert.assertTrue(testUtil.checkMatch(8,50));
//		Assert.assertTrue(testUtil.checkMatch(3,54));
//		Assert.assertTrue(testUtil.checkMatch(5,10,52));
//		Assert.assertTrue(testUtil.checkMatch(6,11,53));
//		Assert.assertTrue(testUtil.checkMatch(14,56));
//		Assert.assertTrue(testUtil.checkConnectionsAreValid());
//
//		testUtil.destroy();
//
//	}
}
