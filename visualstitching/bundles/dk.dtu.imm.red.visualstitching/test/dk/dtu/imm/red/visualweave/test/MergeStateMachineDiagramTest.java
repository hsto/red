package dk.dtu.imm.red.visualweave.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import dk.dtu.imm.red.visualweave.processor.MergeConflictException;
import dk.dtu.imm.red.visualweave.processor.MergeElementType;

public class MergeStateMachineDiagramTest {
    @Rule public TestName name = new TestName();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testStateMachine1() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Available",MergeElementType.STATE,-1, testUtil.genCustAttrState("setTimer", "", ""));
		testUtil.add(2,51,"available",MergeElementType.STATE,-1, testUtil.genCustAttrState("", "signalAvailable", ""));

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertFalse(testUtil.checkIsMergeConflict());
		testUtil.destroy();
	}

	@Test
	public void testStateMachine2() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Available",MergeElementType.STATE,-1, testUtil.genCustAttrState("setTimer", "", ""));
		testUtil.add(2,52,"available",MergeElementType.STATE,-1, testUtil.genCustAttrState("markAvailable", "", ""));

		testUtil.executeMerge();
		Assert.assertFalse(testUtil.checkMatch(1, 52));
		Assert.assertFalse(testUtil.checkIsMergeConflict());
		testUtil.destroy();
	}

	@Test
	public void testStateMachine3() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Available",MergeElementType.STATE,-1, "");
		testUtil.add(2,52,"reserved",MergeElementType.STATE,-1, "");

		testUtil.executeMerge();
		Assert.assertFalse(testUtil.checkMatch(1, 52));
		Assert.assertFalse(testUtil.checkIsMergeConflict());
		testUtil.destroy();
	}


	@Test
	public void testStateMachine4() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Available",MergeElementType.STATE,-1, "");
		testUtil.add(1,2,"reserved",MergeElementType.STATE,-1, "");
		testUtil.add(1,3,"",MergeElementType.STATE_TRANSITION,-1,
				testUtil.genCustAttrStateTrans("reserve", "copies>1", "status=reserved"));
		testUtil.add(1,4,"",MergeElementType.STATE_TRANSITION,-1,
				testUtil.genCustAttrStateTrans("returned", "", ""));
		testUtil.assignSrcAndTarget(3, 1, 2);
		testUtil.assignSrcAndTarget(4, 2, 1);


		testUtil.add(2,51,"Available",MergeElementType.STATE,-1, "");
		testUtil.add(2,52,"reserved",MergeElementType.STATE,-1, "");
		testUtil.add(2,53,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(2,54,"",MergeElementType.STATE_TRANSITION,-1,
				testUtil.genCustAttrStateTrans("returned", "", "status=available"));
		testUtil.assignSrcAndTarget(53, 51, 52);
		testUtil.assignSrcAndTarget(54, 52, 51);


		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		Assert.assertTrue(testUtil.checkMatch(4, 54));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		Assert.assertFalse(testUtil.checkIsMergeConflict());
		testUtil.destroy();
	}


	@Test
	public void testStateMachine5() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Available",MergeElementType.STATE,-1, "");
		testUtil.add(1,2,"reserved",MergeElementType.STATE,-1, "");
		testUtil.add(1,3,"",MergeElementType.STATE_TRANSITION,-1,
				testUtil.genCustAttrStateTrans("", "copies>1", ""));
		testUtil.add(1,4,"",MergeElementType.STATE_TRANSITION,-1,
				testUtil.genCustAttrStateTrans("returned", "", ""));
		testUtil.assignSrcAndTarget(3, 1, 2);
		testUtil.assignSrcAndTarget(4, 2, 1);


		testUtil.add(2,51,"Available",MergeElementType.STATE,-1, "");
		testUtil.add(2,52,"reserved",MergeElementType.STATE,-1, "");
		testUtil.add(2,55,"",MergeElementType.STATE_TRANSITION,-1,
				testUtil.genCustAttrStateTrans("", "status=available", ""));
		testUtil.add(2,54,"",MergeElementType.STATE_TRANSITION,-1,
				testUtil.genCustAttrStateTrans("returned", "", "status=available"));
		testUtil.assignSrcAndTarget(55, 51, 52);
		testUtil.assignSrcAndTarget(54, 52, 51);


		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertFalse(testUtil.checkMatch(3, 55));
		Assert.assertTrue(testUtil.checkMatch(4, 54));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		Assert.assertFalse(testUtil.checkIsMergeConflict());
		testUtil.destroy();
	}


	@Test
	public void testStateMachine6() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"",MergeElementType.INITIAL_STATE,-1, "");
		testUtil.add(1,2,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,3,"idle",MergeElementType.STATE,-1, "");
		testUtil.add(1,4,"maintain",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,5,"maintenance",MergeElementType.STATE,-1, "");
		testUtil.add(1,6,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,7,"card inserted",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,8,"active",MergeElementType.STATE,-1, "");
		testUtil.add(1,9,"",MergeElementType.INITIAL_STATE,8, "");
		testUtil.add(1,10,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,11,"validating",MergeElementType.STATE,8, "");
		testUtil.add(1,12,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,13,"processing",MergeElementType.STATE,8, "");
		testUtil.add(1,14,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,15,"printing",MergeElementType.STATE,8, "");
		testUtil.add(1,16,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.assignSrcAndTarget(2, 1, 3);
		testUtil.assignSrcAndTarget(4, 3, 5);
		testUtil.assignSrcAndTarget(6, 5, 3);
		testUtil.assignSrcAndTarget(7, 3, 8);
		testUtil.assignSrcAndTarget(10, 9, 11);
		testUtil.assignSrcAndTarget(12, 11, 13);
		testUtil.assignSrcAndTarget(14, 13, 15);
		testUtil.assignSrcAndTarget(16, 15, 3);

		testUtil.add(2,55,"printing",MergeElementType.STATE,-1, "");
		testUtil.add(2,60,"out of paper",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(2,61,"halted",MergeElementType.STATE,-1, "");
		testUtil.add(2,62,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(2,64,"error",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(2,63,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(2,53,"processing",MergeElementType.STATE,-1, "");
		testUtil.assignSrcAndTarget(60, 55, 61);
		testUtil.assignSrcAndTarget(62, 61, 55);
		testUtil.assignSrcAndTarget(64, 53, 61);
		testUtil.assignSrcAndTarget(63, 61, 53);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(13, 53));
		Assert.assertTrue(testUtil.checkMatch(15, 55));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		Assert.assertFalse(testUtil.checkIsMergeConflict());
		testUtil.destroy();
	}

	@Test
	public void testStateMachine7() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"",MergeElementType.INITIAL_STATE,-1, "");
		testUtil.add(1,2,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,3,"operational",MergeElementType.STATE,-1,"");
		testUtil.add(1,4,"history",MergeElementType.DEEP_HISTORY_STATE,3,"");
		testUtil.add(1,5,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,6,"stopped",MergeElementType.STATE,3,"");
		testUtil.add(1,7,"play",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,8,"active",MergeElementType.STATE,3,"");
		testUtil.add(1,9,"running",MergeElementType.STATE,8,"");
		testUtil.add(1,10,"pause",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,11,"paused",MergeElementType.STATE,8,"");
		testUtil.add(1,12,"play",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,13,"stop",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,14,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,15,"",MergeElementType.FINAL_STATE,-1,"");
		testUtil.add(1,16,"flip",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(1,17,"flipped",MergeElementType.STATE,-1,"");
		testUtil.add(1,18,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.assignSrcAndTarget(2, 1, 3);
		testUtil.assignSrcAndTarget(5, 4, 6);
		testUtil.assignSrcAndTarget(7, 6, 9);
		testUtil.assignSrcAndTarget(10, 9, 11);
		testUtil.assignSrcAndTarget(12, 11, 9);
		testUtil.assignSrcAndTarget(13, 8, 6);
		testUtil.assignSrcAndTarget(14, 3, 15);
		testUtil.assignSrcAndTarget(16, 3, 17);
		testUtil.assignSrcAndTarget(18, 17, 3);

		testUtil.add(2,58,"active",MergeElementType.STATE,-1,"");
		testUtil.add(2,59,"running",MergeElementType.STATE,58,"");
		testUtil.add(2,60,"",MergeElementType.INITIAL_STATE,58, "");
		testUtil.add(2,61,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(2,62,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(2,63,"processing",MergeElementType.STATE,58,"");
		testUtil.add(2,64,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(2,65,"busy",MergeElementType.STATE,58,"");
		testUtil.add(2,66,"",MergeElementType.STATE_TRANSITION,-1,"");
		testUtil.add(2,67,"",MergeElementType.FINAL_STATE,-1,"");
		testUtil.assignSrcAndTarget(61, 60, 59);
		testUtil.assignSrcAndTarget(62, 59, 63);
		testUtil.assignSrcAndTarget(64, 63, 65);
		testUtil.assignSrcAndTarget(66, 58, 67);

		testUtil.executeMerge();
//		Assert.assertTrue(testUtil.checkMatch(8, 58));
//		Assert.assertTrue(testUtil.checkMatch(9, 59));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		Assert.assertFalse(testUtil.checkIsMergeConflict());

		testUtil.destroy();
	}
}
