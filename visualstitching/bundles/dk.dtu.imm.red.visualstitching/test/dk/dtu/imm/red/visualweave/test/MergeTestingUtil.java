package dk.dtu.imm.red.visualweave.test;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.mutable.MutableInt;

import dk.dtu.imm.red.visualweave.ArrowME;
import dk.dtu.imm.red.visualweave.ConnectionME;
import dk.dtu.imm.red.visualweave.ControlNodeME;
import dk.dtu.imm.red.visualweave.CustomAttributesKind;
import dk.dtu.imm.red.visualweave.MatchResult;
import dk.dtu.imm.red.visualweave.MergeElement;
import dk.dtu.imm.red.visualweave.MergeElementResolver;
import dk.dtu.imm.red.visualweave.processor.MatchKB;
import dk.dtu.imm.red.visualweave.processor.MergeConflictException;
import dk.dtu.imm.red.visualweave.processor.MergeElementFactory;
import dk.dtu.imm.red.visualweave.processor.MergeExecutor;
import dk.dtu.imm.red.visualweave.processor.MergeLogger;
import dk.dtu.imm.red.visualweave.processor.MergeProcessor;
import dk.dtu.imm.red.visualweave.processor.MergeUtil;

public class MergeTestingUtil {
	static String NAME = CustomAttributesKind.NAME.getName();
	static String ROLE_SRC = CustomAttributesKind.ROLE_SRC.getName();
	static String ROLE_TGT = CustomAttributesKind.ROLE_TGT.getName();
	static String MULTIP_SRC = CustomAttributesKind.MULTIPLICITY_SRC.getName();
	static String MULTIP_TGT = CustomAttributesKind.MULTIPLICITY_TGT.getName();
	static String VISIBILITY = CustomAttributesKind.VISIBILITY.getName();
	static String PARAMETERS = CustomAttributesKind.PARAMETERS.getName();
	static String RETURN_TYPE = CustomAttributesKind.RETURN_TYPE.getName();
	static String ENTRY = CustomAttributesKind.ENTRY.getName();
	static String DO = CustomAttributesKind.DO.getName();
	static String EXIT = CustomAttributesKind.EXIT.getName();
	static String TRIGGER = CustomAttributesKind.TRIGGER.getName();
	static String GUARD = CustomAttributesKind.GUARD.getName();
	static String EFFECT = CustomAttributesKind.EFFECT.getName();

	private MutableInt i=null;
	private MergeExecutor executor=null;
	private MergeElementResolver resolver = null;
	private MergeElement[] arr = null;

    MergeLogger mergeLogger = MergeLogger.instance;


	public MergeTestingUtil(String testCaseName){
		mergeLogger.append(MergeLogger.DEBUG,"");
		mergeLogger.append(MergeLogger.DEBUG,"### TEST CASE : " + testCaseName + " ###");
		i=new MutableInt(0);
		executor = new MergeExecutor();
		resolver = executor.getResolver();
		arr = new MergeElement[30000];
	}

	public MergeElement add(
			int inputDiagramIndex,
			int id,
			String name,
			String type,
			int parentID,
			String customAttributesSeparated
			){
		MergeElement me = MergeElementFactory.makeMergeElement(type);
		me.initializeForUnitTest(executor.getResolver(), inputDiagramIndex,
				String.valueOf(id), name, type, String.valueOf(parentID), customAttributesSeparated);
		arr[i.intValue()] = me;
		i.add(1);
		return me;
	}

	public int offset(int offset){
		return i.intValue()+offset;
	}

	public void executeMerge() throws MergeConflictException{
		executor.executeMergeUnitTest(arr);
	}


	public boolean checkMatch(int... ids){
		MergeElement[] arr = new MergeElement[ids.length];
		int i=0;
		for(int id: ids){
			arr[i++] = resolver.lookupMergeElement(id+"");
		}

		for(int j=0;j<arr.length;j++){
			if(j<arr.length-1){
				MatchKB matchKB = executor.getProcessor().getMatchKB();
				if(arr[j].equals(arr[j+1], matchKB)==MatchResult.FALSE
						|| arr[j].equals(arr[j+1], matchKB)==MatchResult.UNKNOWN)
					return false;
			}
		}
		return true;
	}

	public boolean checkParent(int childID, int parentID){
		MergeElement child = resolver.lookupMergeElement(String.valueOf(childID));
		MergeElement parent = resolver.lookupMergeElement(String.valueOf(parentID));
		if(child == null || parent == null){
			return false;
		}
		return parent.getChildren().contains(child) && child.getParent()==parent;

	}

	public boolean checkIsMergeConflict(){
		return executor.getProcessor().getListOfMergeConflict().size()>0;
	}


	public boolean checkConnectionsAreValid(){
		for(MergeElement m : executor.getProcessor().getOutputSet()){
			MergeElement src=null;
			MergeElement tgt=null;
			if(m instanceof ConnectionME){
				 src = ((ConnectionME) m).getConnEnd1();
				 tgt = ((ConnectionME) m).getConnEnd2();
			}
			else if(m instanceof ArrowME){
				 src = ((ArrowME) m).getSrc();
				 tgt = ((ArrowME) m).getTgt();
			}

			if(src==null || tgt ==null){
				continue;
			}

			if(!executor.getProcessor().getOutputSet().contains(src) ||
				!executor.getProcessor().getOutputSet().contains(tgt)){
				return false;
			}

		}
		return true;
	}

	public boolean checkIncomingOutgoingsAreValid(){
		for(MergeElement m : executor.getProcessor().getOutputSet()){
			if(m.isDecisionMergeForkJoin()){
				List<MergeElement> incomingList = ((ControlNodeME)m).getIncoming();
				List<MergeElement> outgoingList = ((ControlNodeME)m).getOutgoing();

				for(MergeElement incoming : incomingList){
					MergeElement tgt = ((ArrowME)incoming).getTgt();
					if(tgt!=m){
						return false;
					}
				}

				for(MergeElement outgoing : outgoingList){
					MergeElement src = ((ArrowME)outgoing).getSrc();
					if(src!=m){
						return false;
					}
				}
			}
		}
		return true;
	}

	public int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}

	public void assignSrcAndTarget(int curElId, int src, int tgt){
		MergeElement me = resolver.lookupMergeElement(String.valueOf(curElId));
		MergeElement srcME = resolver.lookupMergeElement(String.valueOf(src));
		MergeElement tgtME = resolver.lookupMergeElement(String.valueOf(tgt));

		if(srcME == null){
			System.err.println("Unable to find" + src);
		}
		if(tgtME== null){
			System.err.println("Unable to find" + tgt);
		}

		if(me instanceof ArrowME){
			((ArrowME)me).setSrc(srcME);
			((ArrowME)me).setTgt(tgtME);
		}
		else if(me instanceof ConnectionME){
			((ConnectionME)me).setConnEnd1(srcME);
			((ConnectionME)me).setConnEnd2(tgtME);
		}

		if(srcME instanceof ControlNodeME){
			((ControlNodeME)srcME).getOutgoing().add(me);
		}
		if(tgtME instanceof ControlNodeME){
			((ControlNodeME)tgtME).getIncoming().add(me);
		}
	}


	public String genCustAttrOperation(String visibility, String parameters, String returnType){
		StringBuffer sb = new StringBuffer();
		if(visibility!=null && !visibility.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(VISIBILITY);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(visibility);
		}
		if(parameters!=null && !parameters.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(PARAMETERS);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(parameters);
		}
		if(returnType!=null && !returnType.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(RETURN_TYPE);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(returnType);
		}
		return sb.toString();
	}

	public String genCustAttrAssoc(String roleSrc, String roleTgt, String multiplicitySrc, String multiplicityTgt){
		StringBuffer sb = new StringBuffer();
		if(roleSrc!=null && !roleSrc.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(ROLE_SRC);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(roleSrc);
		}

		if(roleTgt!=null && !roleTgt.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(ROLE_TGT);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(roleTgt);
		}

		if(multiplicitySrc!=null && !multiplicitySrc.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(MULTIP_SRC);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(multiplicitySrc);
		}

		if(multiplicityTgt!=null && !multiplicityTgt.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(MULTIP_TGT);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(multiplicityTgt);
		}

		return sb.toString();
	}

	public String genCustAttrState(String entry, String _do, String exit){
		StringBuffer sb = new StringBuffer();
		if(entry!=null && !entry.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(ENTRY);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(entry);
		}
		if(_do!=null && !_do.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(DO);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(_do);
		}
		if(exit!=null && !exit.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(EXIT);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(exit);
		}
		return sb.toString();
	}

	public String genCustAttrStateTrans(String trigger, String guard, String effect){
		StringBuffer sb = new StringBuffer();
		if(trigger!=null && !trigger.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(TRIGGER);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(trigger);
		}
		if(guard!=null && !guard.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(GUARD);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(guard);
		}
		if(effect!=null && !effect.isEmpty()){
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_SEPARATOR);
			sb.append(EFFECT);
			sb.append(MergeUtil.CUSTOM_ATTRIBUTES_VALUE_SEPARATOR);
			sb.append(effect);
		}
		return sb.toString();
	}


	public MergeProcessor getProcessor(){
		return executor.getProcessor();
	}

	public MergeElementResolver getResolver(){
		return resolver;
	}

	public void destroy(){
		executor.destroy();
		resolver.destroy();
	}
}
