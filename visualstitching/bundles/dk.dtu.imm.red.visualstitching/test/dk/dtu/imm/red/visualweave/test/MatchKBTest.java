package dk.dtu.imm.red.visualweave.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import dk.dtu.imm.red.visualweave.MatchResult;
import dk.dtu.imm.red.visualweave.MergeElement;
import dk.dtu.imm.red.visualweave.processor.MatchKB;
import dk.dtu.imm.red.visualweave.processor.MergeElementFactory;
import dk.dtu.imm.red.visualweave.processor.MergeElementType;
import dk.dtu.imm.red.visualweave.processor.Tuple;

public class MatchKBTest {

	MatchKB matchKB = new MatchKB(null);
	MergeElement a1 = MergeElementFactory.makeMergeElement(MergeElementType.ACTION);
	MergeElement a2 = MergeElementFactory.makeMergeElement(MergeElementType.ACTION);

	MergeElement b1 = MergeElementFactory.makeMergeElement(MergeElementType.ACTION);
	MergeElement b2 = MergeElementFactory.makeMergeElement(MergeElementType.ACTION);
	MergeElement b3 = MergeElementFactory.makeMergeElement(MergeElementType.ACTION);

	MergeElement c1 = MergeElementFactory.makeMergeElement(MergeElementType.ACTION);

	@Test
	public void testAddToKB() {

		a1.setId("a1");
		a2.setId("a2");
		b1.setId("b1");
		b2.setId("b2");
		b3.setId("b3");
		c1.setId("c1");
		List<Tuple<MergeElement>> listOfMatchingTuple = new ArrayList<Tuple<MergeElement>>();
		List<Tuple<MergeElement>> listOfNonMatchingTuple = new ArrayList<Tuple<MergeElement>>();

		listOfMatchingTuple.add(new Tuple<MergeElement>(a1,a2));
		listOfMatchingTuple.add(new Tuple<MergeElement>(b1,b2));
		listOfMatchingTuple.add(new Tuple<MergeElement>(b2,b3));
		listOfMatchingTuple.add(new Tuple<MergeElement>(b1,b3));

		listOfNonMatchingTuple.add(new Tuple<MergeElement>(a1,b1));
		listOfNonMatchingTuple.add(new Tuple<MergeElement>(a1,b2));
		listOfNonMatchingTuple.add(new Tuple<MergeElement>(a1,b3));

		listOfNonMatchingTuple.add(new Tuple<MergeElement>(a2,b1));
		listOfNonMatchingTuple.add(new Tuple<MergeElement>(a2,b2));
		listOfNonMatchingTuple.add(new Tuple<MergeElement>(a2,b3));

		matchKB.addMatchAndNonMatchToKB(listOfMatchingTuple,listOfNonMatchingTuple);

		Assert.assertTrue(a1.equals(a2,matchKB)==MatchResult.TRUE);
		Assert.assertTrue(a2.equals(a1,matchKB)==MatchResult.TRUE);

		Assert.assertTrue(b1.equals(b2,matchKB)==MatchResult.TRUE);
		Assert.assertTrue(b1.equals(b3,matchKB)==MatchResult.TRUE);
		Assert.assertTrue(b2.equals(b1,matchKB)==MatchResult.TRUE);
		Assert.assertTrue(b2.equals(b3,matchKB)==MatchResult.TRUE);
		Assert.assertTrue(b3.equals(b1,matchKB)==MatchResult.TRUE);
		Assert.assertTrue(b3.equals(b2,matchKB)==MatchResult.TRUE);

		Assert.assertTrue(a1.equals(b1,matchKB)==MatchResult.FALSE);
		Assert.assertTrue(a2.equals(c1,matchKB)==MatchResult.UNKNOWN);

	}

}
