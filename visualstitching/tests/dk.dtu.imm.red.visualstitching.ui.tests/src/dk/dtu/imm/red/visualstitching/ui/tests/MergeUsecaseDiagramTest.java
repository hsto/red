package dk.dtu.imm.red.visualstitching.ui.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import dk.dtu.imm.red.visualstitch.processor.MergeConflictException;
import dk.dtu.imm.red.visualstitch.processor.MergeElementType;

@Ignore("Requires maintenance") 
public class MergeUsecaseDiagramTest {
    @Rule public TestName name = new TestName();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testUsecase1() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"search item",MergeElementType.USECASE,-1, "");
		testUtil.add(2,51,"Search item",MergeElementType.USECASE,-1,"");

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		testUtil.destroy();
	}

	@Test
	public void testUsecase2() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"search item",MergeElementType.USECASE,-1, "");
		testUtil.add(2,52,"reserve books",MergeElementType.USECASE,-1,"");

		testUtil.executeMerge();
		Assert.assertFalse(testUtil.checkMatch(1, 52));
		testUtil.destroy();
	}

	@Test
	public void testUsecase3() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"librarian",MergeElementType.ACTOR,-1, "");
		testUtil.add(1,2," ",MergeElementType.GENERALIZATION,-1, "");
		testUtil.add(1,3,"chief librarian",MergeElementType.ACTOR,-1, "");
		testUtil.add(1,4,"make reservation",MergeElementType.USECASE,-1, "");
		testUtil.add(1,5,"terminate account",MergeElementType.USECASE,-1, "");
		testUtil.add(1,6," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(1,7," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(2, 3, 1);
		testUtil.assignSrcAndTarget(6, 1, 4);
		testUtil.assignSrcAndTarget(7, 5, 3);


		testUtil.add(2,51,"librarian",MergeElementType.ACTOR,-1, "");
		testUtil.add(2,52," ",MergeElementType.GENERALIZATION,-1, "");
		testUtil.add(2,53,"student librarian",MergeElementType.ACTOR,-1, "");
		testUtil.add(2,54," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(2,55,"extend loan",MergeElementType.USECASE,-1, "");
		testUtil.assignSrcAndTarget(52, 53, 51);
		testUtil.assignSrcAndTarget(54, 53, 55);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();
	}

	@Test
	public void testUsecase4() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"borrower",MergeElementType.ACTOR,-1, "");
		testUtil.add(1,2,"LMS",MergeElementType.SYSTEM_BOUNDARY,-1, "");
		testUtil.add(1,3,"borrow books",MergeElementType.USECASE,2, "");
		testUtil.add(1,4,"check fine",MergeElementType.USECASE,2, "");
		testUtil.add(1,5," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(1,6," ",MergeElementType.USE_CASE_INCLUDE,-1, "");
		testUtil.assignSrcAndTarget(5, 1, 3);
		testUtil.assignSrcAndTarget(6, 3, 4);

		testUtil.add(2,51,"borrower",MergeElementType.ACTOR,-1, "");
		testUtil.add(2,52,"borrow books",MergeElementType.USECASE,-1, "");
		testUtil.add(2,53,"check reservation",MergeElementType.USECASE,-1, "");
		testUtil.add(2,54," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(2,55," ",MergeElementType.USE_CASE_INCLUDE,-1, "");
		testUtil.assignSrcAndTarget(54, 51, 52);
		testUtil.assignSrcAndTarget(55, 52, 53);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(3, 52));
		Assert.assertTrue(testUtil.checkParent(3, 2));
		Assert.assertTrue(testUtil.checkParent(4, 2));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();
	}


	@Test(expected=MergeConflictException.class)
	public void testUsecase5() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"borrower",MergeElementType.ACTOR,-1, "");
		testUtil.add(1,2,"library",MergeElementType.SYSTEM_BOUNDARY,-1, "");
		testUtil.add(1,3,"borrow books",MergeElementType.USECASE,2, "");
		testUtil.add(1,4," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(4, 1, 3);


		testUtil.add(2,51,"borrower",MergeElementType.ACTOR,-1, "");
		testUtil.add(2,52,"LMS",MergeElementType.SYSTEM_BOUNDARY,-1, "");
		testUtil.add(2,53,"borrow books",MergeElementType.USECASE,52, "");
		testUtil.add(2,54," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.assignSrcAndTarget(54, 51, 53);

		testUtil.executeMerge();
		testUtil.destroy();
	}


	@Test
	public void testUsecase6() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"user",MergeElementType.ACTOR,-1, "");
		testUtil.add(1,2,"general user",MergeElementType.ACTOR,-1, "");
		testUtil.add(1,3,"registered user",MergeElementType.ACTOR,-1, "");
		testUtil.add(1,4," ",MergeElementType.GENERALIZATION,-1, "");
		testUtil.add(1,5," ",MergeElementType.GENERALIZATION,-1, "");
		testUtil.add(1,6,"LMS",MergeElementType.SYSTEM_BOUNDARY,-1, "");
		testUtil.add(1,7,"make reservation",MergeElementType.USECASE,6, "");
		testUtil.add(1,8,"search book",MergeElementType.USECASE,6, "");
		testUtil.add(1,9,"renew loan",MergeElementType.USECASE,6, "");
		testUtil.add(1,10,"check reservation",MergeElementType.USECASE,6, "");
		testUtil.add(1,11," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(1,12," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(1,13," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(1,14," ",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(1,15," ",MergeElementType.USE_CASE_INCLUDE,-1, "");
		testUtil.add(1,16," ",MergeElementType.USE_CASE_INCLUDE,-1, "");
		testUtil.assignSrcAndTarget(4, 2, 1);
		testUtil.assignSrcAndTarget(5, 3, 1);
		testUtil.assignSrcAndTarget(11, 1, 7);
		testUtil.assignSrcAndTarget(12, 1, 8);
		testUtil.assignSrcAndTarget(13, 1, 9);
		testUtil.assignSrcAndTarget(14, 1, 10);
		testUtil.assignSrcAndTarget(15, 7, 8);
		testUtil.assignSrcAndTarget(16, 9, 10);


		testUtil.add(2,51,"user",MergeElementType.ACTOR,-1, "");
		testUtil.add(2,52,"librarian",MergeElementType.ACTOR,-1, "");
		testUtil.add(2,53,"",MergeElementType.GENERALIZATION,-1, "");
		testUtil.add(2,54,"LMS",MergeElementType.SYSTEM_BOUNDARY,-1, "");
		testUtil.add(2,55,"issue book",MergeElementType.USECASE,54, "");
		testUtil.add(2,56,"",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(2,57,"search book",MergeElementType.USECASE,54, "");
		testUtil.add(2,58,"",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(2,59,"",MergeElementType.ASSOCIATION,-1, "");
		testUtil.add(2,60,"extend loan",MergeElementType.USECASE,54, "");
		testUtil.add(2,61,"check reservation",MergeElementType.USECASE,54, "");
		testUtil.add(2,62," ",MergeElementType.USE_CASE_INCLUDE,-1, "");
		testUtil.assignSrcAndTarget(53, 52, 51);
		testUtil.assignSrcAndTarget(56, 51, 55);
		testUtil.assignSrcAndTarget(58, 51, 57);
		testUtil.assignSrcAndTarget(59, 51, 60);
		testUtil.assignSrcAndTarget(62, 61, 60);

		testUtil.executeMerge();

		Assert.assertTrue(testUtil.checkMatch(6, 54));
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(8, 57));
		Assert.assertTrue(testUtil.checkMatch(10, 61));
		Assert.assertTrue(testUtil.checkMatch(12, 58));
		Assert.assertTrue(testUtil.checkParent(7, 6));
		Assert.assertTrue(testUtil.checkParent(8, 6));
		Assert.assertTrue(testUtil.checkParent(55, 6));
		Assert.assertTrue(testUtil.checkParent(9, 6));
		Assert.assertTrue(testUtil.checkParent(10, 6));
		Assert.assertTrue(testUtil.checkParent(60, 6));


		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();

	}
}
