package dk.dtu.imm.red.visualstitching.ui.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.visualmodeling.visualmodel.VisualConnection;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualDiagram;
import dk.dtu.imm.red.visualmodeling.visualmodel.VisualElement;
import dk.dtu.imm.red.visualmodeling.visualmodel.visualmodelFactory;
import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.MergeElementResolver;
import dk.dtu.imm.red.visualstitch.VisualstitchFactory;
import dk.dtu.imm.red.visualstitch.processor.MergeElementFactory;
import dk.dtu.imm.red.visualstitch.processor.MergeElementType;

public class MergeElementResolverTest {

	MergeElementResolver resolver = VisualstitchFactory.eINSTANCE.createMergeElementResolver();
	MergeElement m1,m2,m3,m4,m5,m6;
	VisualElement v1,v2;
	VisualElement ve1,ve2;
	VisualConnection v3,ve3;
	VisualDiagram d1, d2;

	@Before
	public void setUp() throws Exception {

		v1 = visualmodelFactory.eINSTANCE.createVisualElement();
		v2 = visualmodelFactory.eINSTANCE.createVisualElement();
		v2.setParent(v1);
		v1.getElements().add(v1);
		v3 = visualmodelFactory.eINSTANCE.createVisualConnection();
		v3.setSource(v1);
		v3.setTarget(v2);

		d1 = visualmodelFactory.eINSTANCE.createVisualDiagram();
		d1.getElements().add(v1);
		d1.getElements().add(v2);
		d1.getDiagramConnections().add(v3);

		ve1 = visualmodelFactory.eINSTANCE.createVisualElement();
		ve2 = visualmodelFactory.eINSTANCE.createVisualElement();
		ve2.setParent(ve1);
		ve1.getElements().add(ve1);
		ve1.getElements().add(ve2);
		ve3 = visualmodelFactory.eINSTANCE.createVisualConnection();
		ve3.setSource(ve1);
		ve3.setTarget(ve2);

		d2 = visualmodelFactory.eINSTANCE.createVisualDiagram();
		d2.getElements().add(ve1);
		d2.getDiagramConnections().add(ve3);

		m1 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m2 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m3 = MergeElementFactory.makeMergeElement(MergeElementType.ASSOCIATION);
		m4 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m5 = MergeElementFactory.makeMergeElement(MergeElementType.CLASS);
		m6 = MergeElementFactory.makeMergeElement(MergeElementType.ASSOCIATION);

	}

	public void keepTrackOfAllObjects(){
		resolver.keepTrackOf("1", m1,v1);
		resolver.keepTrackOf("2", m2,v2);
		resolver.keepTrackOf("3", m3,v3);
		resolver.keepTrackOf("4", m4,ve1);
		resolver.keepTrackOf("5", m5,ve2);
		resolver.keepTrackOf("6", m6,ve3);

	}

	@Test
	public void testLookupMergeElementIVisualElement() {
		keepTrackOfAllObjects();
		Assert.assertTrue(resolver.getIDToMergeElementMap().size()==6);
		Assert.assertTrue(resolver.getVisualElementToIDMap().size()==4);
		Assert.assertTrue(resolver.getVisualConnectionToIDMap().size()==2);

		Assert.assertSame(m1, resolver.lookupMergeElement("1"));
		Assert.assertSame(m1, resolver.lookupMergeElement(v1));
		Assert.assertSame(m3, resolver.lookupMergeElement(v3));
	}

//	@Test
//	public void testPopulatePartition() {
//		Map<String, Map<String, MergeElement>> ummatchedPartitionMap =
//				new LinkedHashMap<String,Map<String,MergeElement>>();
//		MergeUtil.populatePartition(ummatchedPartitionMap, resolver, d1,
//				d2);
//		Assert.assertTrue(ummatchedPartitionMap.size()==2);
//	}


}
