package dk.dtu.imm.red.visualstitching.ui.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import dk.dtu.imm.red.visualstitch.processor.MergeConflictException;
import dk.dtu.imm.red.visualstitch.processor.MergeElementType;
import dk.dtu.imm.red.visualstitch.processor.MergeLogger;

@Ignore("Requires maintenance") 
public class MergeActivityDiagramTest {
    @Rule public TestName name = new TestName();
    MergeLogger mergeLogger = MergeLogger.instance;


	@Before
	public void setUp() throws Exception {
		mergeLogger.setLogInRCPMode(false);
	}

	@Test
	public void testActivity1() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"login",MergeElementType.ACTION,-1, "");
		testUtil.add(1,2,"Checkout book",MergeElementType.ACTION,-1, "");
		testUtil.add(1,3,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(3,1,2);

		testUtil.add(2,51,"login",MergeElementType.ACTION,-1,"");
		testUtil.add(2,52,"checkout book",MergeElementType.ACTION,-1,"");
		testUtil.add(2,53,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(53,51,52);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3, 53));

		testUtil.destroy();
	}

	@Test
	public void testActivity2() throws MergeConflictException {
		//flow from initial to action
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"",MergeElementType.INITIAL_NODE,-1, "");
		testUtil.add(1,2,"checkout book",MergeElementType.ACTION,-1, "");
		testUtil.add(1,3,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(3,1,2);

		testUtil.add(2,51,"",MergeElementType.INITIAL_NODE,-1, "");
		testUtil.add(2,52,"checkout book",MergeElementType.ACTION,-1,"");
		testUtil.add(2,53,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(53,51,52);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		testUtil.destroy();
	}

	@Test
	public void testActivity3() throws MergeConflictException {
		//flow from action to final
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Checkout book",MergeElementType.ACTION,-1, "");
		testUtil.add(1,2,"",MergeElementType.ACTIVITY_FINAL_NODE,-1, "");
		testUtil.add(1,3,"",MergeElementType.CONTROL_FLOW,-1,"");
		testUtil.assignSrcAndTarget(3,1,2);

		testUtil.add(2,51,"Checkout book",MergeElementType.ACTION,-1,"");
		testUtil.add(2,52,"",MergeElementType.ACTIVITY_FINAL_NODE,-1, "");
		testUtil.add(2,53,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(53,51,52);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		testUtil.destroy();
	}


	@Test
	public void testActivity4() throws MergeConflictException {
		//flow from action to decision and then to action
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"validate library card",MergeElementType.ACTION,-1, "");
		testUtil.add(1,2,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,3,"register membership",MergeElementType.ACTION,-1, "");
		testUtil.add(1,4,"show library menu",MergeElementType.ACTION,-1, "");
		testUtil.add(1,5,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(5,1,2);
		testUtil.add(1,6,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(6,2,3);
		testUtil.add(1,7,"",MergeElementType.CONTROL_FLOW,-1,"");
		testUtil.assignSrcAndTarget(7,2,4);

		testUtil.add(2,51,"validate library card",MergeElementType.ACTION,-1, "");
		testUtil.add(2,52,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,53,"register membership",MergeElementType.ACTION,-1, "");
		testUtil.add(2,59,"show reservation",MergeElementType.ACTION,-1, "");
		testUtil.add(2,55,"",MergeElementType.CONTROL_FLOW,-1,"");
		testUtil.assignSrcAndTarget(55,51,52);
		testUtil.add(2,56,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(56,52,53);
		testUtil.add(2,58,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(58,52,59);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		Assert.assertTrue(testUtil.checkMatch(5, 55));
		Assert.assertTrue(testUtil.checkMatch(6, 56));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		Assert.assertTrue(testUtil.checkIncomingOutgoingsAreValid());
		testUtil.destroy();
	}

	@Test
	public void testActivity5() throws MergeConflictException {
		//flow from action to decision and then to action
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"validate library card",MergeElementType.ACTION,-1, "");
		testUtil.add(1,2,"Is valid",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,3,"register membership",MergeElementType.ACTION,-1, "");
		testUtil.add(1,4,"show library menu",MergeElementType.ACTION,-1, "");
		testUtil.add(1,5,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(5,1,2);
		testUtil.add(1,6,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(6,2,3);
		testUtil.add(1,7,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(7,2,4);

		testUtil.add(2,51,"validate library card",MergeElementType.ACTION,-1, "");
		testUtil.add(2,52,"Is not valid",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,53,"register membership",MergeElementType.ACTION,-1, "");
		testUtil.add(2,54,"show reservation",MergeElementType.ACTION,-1, "");
		testUtil.add(2,55,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(55,51,52);
		testUtil.add(2,56,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(56,52,53);
		testUtil.add(2,57,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(57,52,54);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		Assert.assertTrue(testUtil.checkIncomingOutgoingsAreValid());
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		testUtil.destroy();
	}

	@Test
	public void testActivity6() throws MergeConflictException {
		//how to handle
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"validate library card",MergeElementType.ACTION,-1, "");
		testUtil.add(1,2,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,3,"register membership",MergeElementType.ACTION,-1, "");
		testUtil.add(1,4,"show library menu",MergeElementType.ACTION,-1, "");
		testUtil.add(1,5,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(5,1,2);
		testUtil.add(1,6,"invalid card",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(6,2,3);
		testUtil.add(1,7,"valid card",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(7,2,4);

		testUtil.add(2,51,"insert card",MergeElementType.ACTION,-1, "");
		testUtil.add(2,52,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,53,"register membership",MergeElementType.ACTION,-1, "");
		testUtil.add(2,54,"show reservation",MergeElementType.ACTION,-1, "");
		testUtil.add(2,55,"",MergeElementType.CONTROL_FLOW,-1,"");
		testUtil.assignSrcAndTarget(55,51,52);
		testUtil.add(2,56,"card unreadable",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(56,52,53);
		testUtil.add(2,57,"card OK",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(57,52,54);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		Assert.assertFalse(testUtil.checkMatch(6, 56));
		Assert.assertFalse(testUtil.checkMatch(5, 55));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		Assert.assertTrue(testUtil.checkIncomingOutgoingsAreValid());
		testUtil.destroy();
	}

	@Test
	public void testActivity7() throws MergeConflictException {
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Reserve book",MergeElementType.ACTION,-1, "");
		testUtil.add(1,2,"Pay fine",MergeElementType.ACTION,-1, "");
		testUtil.add(1,3,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,4,"Show library menu",MergeElementType.ACTION,-1, "");
		testUtil.add(1,5,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(5,1,3);
		testUtil.add(1,6,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(6,2,3);
		testUtil.add(1,7,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(7,3,4);

		testUtil.add(2,51,"Reserve book",MergeElementType.ACTION,-1, "");
		testUtil.add(2,52,"Extend loan",MergeElementType.ACTION,-1, "");
		testUtil.add(2,53,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,54,"show library menu",MergeElementType.ACTION,-1, "");
		testUtil.add(2,55,"",MergeElementType.CONTROL_FLOW,-1,"");
		testUtil.assignSrcAndTarget(55, 51,53);
		testUtil.add(2,56,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(56,52,53);
		testUtil.add(2,57,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(57,53,54);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		Assert.assertTrue(testUtil.checkMatch(4, 54));
		Assert.assertTrue(testUtil.checkMatch(5, 55));
		Assert.assertTrue(testUtil.checkMatch(7, 57));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		Assert.assertTrue(testUtil.checkIncomingOutgoingsAreValid());

		testUtil.destroy();
	}


	@Test
	public void testActivity8() throws MergeConflictException {
		//from initial to decision and from decision to final
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"",MergeElementType.INITIAL_NODE,-1, "");
		testUtil.add(1,2,"checkout successful?",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,3,"",MergeElementType.ACTIVITY_FINAL_NODE,-1, "");
		testUtil.add(1,4,"redo checkout",MergeElementType.ACTION,-1, "");
		testUtil.add(1,5,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(5,1,2);
		testUtil.add(1,6,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(6,2,3);
		testUtil.add(1,7,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(7,2,4);
		testUtil.add(1,8,"Finish checkout",MergeElementType.ACTION,-1, "");
		testUtil.add(1,9,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(9,4,8);

		testUtil.add(2,51,"",MergeElementType.INITIAL_NODE,-1, "");
		testUtil.add(2,52,"checkout successful?",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,53,"",MergeElementType.ACTIVITY_FINAL_NODE,-1, "");
		testUtil.add(2,54,"redo checkout",MergeElementType.ACTION,-1, "");
		testUtil.add(2,55,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(55,51,52);
		testUtil.add(2,56,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(56,52,53);
		testUtil.add(2,57,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(57,52,54);
		testUtil.add(2,58,"Notify librarian",MergeElementType.ACTION,-1, "");
		testUtil.add(2,59,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(59,54,58);

		testUtil.executeMerge();
		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		Assert.assertTrue(testUtil.checkMatch(5, 55));
		Assert.assertTrue(testUtil.checkMatch(6, 56));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		Assert.assertTrue(testUtil.checkIncomingOutgoingsAreValid());

		testUtil.destroy();
	}

	@Test
	public void testActivity9() throws MergeConflictException {
		//from decision to decision
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"Input username and password",MergeElementType.ACTION,-1, "");
		testUtil.add(1,2,"username exist?",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,3,"password correct?",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,4,"Show library menu",MergeElementType.ACTION,-1, "");
		testUtil.add(1,5,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(5,1,2);
		testUtil.add(1,6,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(6,2,1);
		testUtil.add(1,7,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(7,2,3);
		testUtil.add(1,8,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(8,3,1);
		testUtil.add(1,9,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(9,3,4);


		testUtil.add(2,51,"Input username and password",MergeElementType.ACTION,-1, "");
		testUtil.add(2,52,"username exist?",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,53,"password correct?",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,54,"Show library menu",MergeElementType.ACTION,-1, "");
		testUtil.add(2,55,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(55,51,52);
		testUtil.add(2,56,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(56,52,51);
		testUtil.add(2,57,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(57,52,53);
		testUtil.add(2,58,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(58,53,51);
		testUtil.add(2,59,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(59,53,54);

		testUtil.executeMerge();
		mergeLogger.append(MergeLogger.DEBUG,testUtil.getProcessor().getMatchKB().toString());

		Assert.assertTrue(testUtil.checkMatch(1, 51));
		Assert.assertTrue(testUtil.checkMatch(2, 52));
		Assert.assertTrue(testUtil.checkMatch(3, 53));
		Assert.assertTrue(testUtil.checkMatch(4, 54));
		Assert.assertTrue(testUtil.checkMatch(5, 55));
		Assert.assertTrue(testUtil.checkMatch(6, 56));
		Assert.assertTrue(testUtil.checkMatch(7, 57));
		Assert.assertTrue(testUtil.checkMatch(8, 58));
		Assert.assertTrue(testUtil.checkMatch(9, 59));
		Assert.assertTrue(testUtil.checkConnectionsAreValid());
		Assert.assertTrue(testUtil.checkIncomingOutgoingsAreValid());

		testUtil.destroy();
	}

	@Test
	public void testActivity10() throws MergeConflictException{
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());
		testUtil.add(1,1,"",MergeElementType.INITIAL_NODE,-1, "");
		testUtil.add(1,2,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,3,"receive order",MergeElementType.ACTION,-1, "");
		testUtil.add(1,4,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,5,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,6,"order rejected",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,7,"order accepted",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,8,"fill order",MergeElementType.ACTION,-1, "");
		testUtil.add(1,9,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,10,"",MergeElementType.FORK_JOIN_NODE,-1, "");
		testUtil.add(1,11,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,12,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,13,"send invoice",MergeElementType.ACTION,-1, "");
		testUtil.add(1,14,"ship order",MergeElementType.ACTION,-1, "");
		testUtil.add(1,15,"",MergeElementType.OBJECT_FLOW,-1, "");
		testUtil.add(1,16,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,17,"ship order",MergeElementType.OBJECT_NODE,-1, "");
		testUtil.add(1,18,"",MergeElementType.OBJECT_FLOW,-1, "");
		testUtil.add(1,19,"accept payment",MergeElementType.ACTION,-1, "");
		testUtil.add(1,20,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,21,"",MergeElementType.FORK_JOIN_NODE,-1, "");
		testUtil.add(1,22,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,23,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,24,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,25,"close order",MergeElementType.ACTION,-1, "");
		testUtil.add(1,26,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,27,"",MergeElementType.ACTIVITY_FINAL_NODE,-1, "");
		testUtil.assignSrcAndTarget(2, 1, 3);
		testUtil.assignSrcAndTarget(4, 3, 5);
		testUtil.assignSrcAndTarget(6, 5, 23);
		testUtil.assignSrcAndTarget(7, 5, 8);
		testUtil.assignSrcAndTarget(9, 8, 10);
		testUtil.assignSrcAndTarget(11, 10, 13);
		testUtil.assignSrcAndTarget(12, 10, 14);
		testUtil.assignSrcAndTarget(15, 13, 17);
		testUtil.assignSrcAndTarget(16, 14, 21);
		testUtil.assignSrcAndTarget(18, 17, 19);
		testUtil.assignSrcAndTarget(20, 19, 21);
		testUtil.assignSrcAndTarget(22, 21, 23);
		testUtil.assignSrcAndTarget(24, 23, 25);
		testUtil.assignSrcAndTarget(26, 25, 27);

		testUtil.add(2,51,"",MergeElementType.INITIAL_NODE,-1, "");
		testUtil.add(2,52,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,53,"",MergeElementType.FORK_JOIN_NODE,-1, "");
		testUtil.add(2,54,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,55,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,56,"check inventory",MergeElementType.ACTION,-1, "");
		testUtil.add(2,57,"validate customer standing",MergeElementType.ACTION,-1, "");
		testUtil.add(2,58,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,59,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,60,"",MergeElementType.FORK_JOIN_NODE,-1, "");
		testUtil.add(2,61,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,62,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,63,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,64,"handle exception",MergeElementType.ACTION,-1, "");
		testUtil.add(2,65,"OK",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,66,"",MergeElementType.FORK_JOIN_NODE,-1, "");
		testUtil.add(2,67,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,68,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,69,"ship order",MergeElementType.ACTION,-1, "");
		testUtil.add(2,70,"send invoice",MergeElementType.ACTION,-1, "");
		testUtil.add(2,71,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,72,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,73,"",MergeElementType.FORK_JOIN_NODE,-1, "");
		testUtil.add(2,74,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,75,"",MergeElementType.ACTIVITY_FINAL_NODE,-1, "");
		testUtil.assignSrcAndTarget(52, 51, 53);
		testUtil.assignSrcAndTarget(54, 53, 56);
		testUtil.assignSrcAndTarget(55, 53, 57);
		testUtil.assignSrcAndTarget(58, 56, 60);
		testUtil.assignSrcAndTarget(59, 57, 60);
		testUtil.assignSrcAndTarget(61, 60, 62);
		testUtil.assignSrcAndTarget(63, 62, 64);
		testUtil.assignSrcAndTarget(65, 62, 66);
		testUtil.assignSrcAndTarget(67, 66, 69);
		testUtil.assignSrcAndTarget(68, 66, 70);
		testUtil.assignSrcAndTarget(71, 69, 73);
		testUtil.assignSrcAndTarget(72, 70, 73);
		testUtil.assignSrcAndTarget(74, 73, 75);

		testUtil.executeMerge();
		mergeLogger.append(MergeLogger.DEBUG,testUtil.getProcessor().getMatchKB().toString());
		Assert.assertTrue(testUtil.checkMatch(13,70));
		Assert.assertTrue(testUtil.checkMatch(14,69));
		Assert.assertTrue(testUtil.checkMatch(11,68));
		Assert.assertTrue(testUtil.checkMatch(12,67));
		Assert.assertTrue(testUtil.checkMatch(16,71));
		Assert.assertTrue(testUtil.checkMatch(10,66));
		Assert.assertTrue(testUtil.checkMatch(21,73));
		testUtil.checkConnectionsAreValid();
	}

	@Test
	public void testActivity11() throws MergeConflictException{
		MergeTestingUtil testUtil = new MergeTestingUtil(name.getMethodName());

		//first diagram, activity diagram contents
		//for initial,final,decision,merge,fork,join, custom attributes incoming and outgoing
		//will be automatically populated based on the controlFlow
		testUtil.add(1,1,"",MergeElementType.INITIAL_NODE,-1, "");
		testUtil.add(1,2,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,3,"enquire books",MergeElementType.ACTION,-1, "");
		testUtil.add(1,4,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,5,"check book availability",MergeElementType.ACTION,-1, "");
		testUtil.add(1,6,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,7,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,8,"not available",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,9,"book not available",MergeElementType.ACTION,-1, "");
		testUtil.add(1,10,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,11,"",MergeElementType.ACTIVITY_FINAL_NODE,-1, "");
		testUtil.add(1,12,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,13,"validate member",MergeElementType.ACTION,-1, "");
		testUtil.add(1,14,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,15,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,16,"not valid",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,17,"register member",MergeElementType.ACTION,-1, "");
		testUtil.add(1,18,"valid",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,19,"check number of books issued to member",MergeElementType.ACTION,-1, "");
		testUtil.add(1,20,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,21,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(1,22,"maximum quota exceeded",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,23,"books not issued",MergeElementType.ACTION,-1, "");
		testUtil.add(1,24,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,25,"issue book",MergeElementType.ACTION,-1, "");
		testUtil.add(1,26,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,27,"add member, book, and issue detail",MergeElementType.ACTION,-1, "");
		testUtil.add(1,28,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,29,"update book status",MergeElementType.ACTION,-1, "");
		testUtil.add(1,30,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(1,31,"",MergeElementType.ACTIVITY_FINAL_NODE,-1, "");
		testUtil.add(1,32,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.assignSrcAndTarget(2, 1, 3);
		testUtil.assignSrcAndTarget(4, 3, 5);
		testUtil.assignSrcAndTarget(6, 5, 7);
		testUtil.assignSrcAndTarget(8, 7, 9);
		testUtil.assignSrcAndTarget(10, 9, 11);
		testUtil.assignSrcAndTarget(12, 7, 13);
		testUtil.assignSrcAndTarget(14, 13, 15);
		testUtil.assignSrcAndTarget(16, 15, 17);
		testUtil.assignSrcAndTarget(18, 15, 19);
		testUtil.assignSrcAndTarget(20, 19, 21);
		testUtil.assignSrcAndTarget(22, 21, 23);
		testUtil.assignSrcAndTarget(24, 21, 25);
		testUtil.assignSrcAndTarget(26, 25, 27);
		testUtil.assignSrcAndTarget(28, 27, 29);
		testUtil.assignSrcAndTarget(30, 29, 31);
		testUtil.assignSrcAndTarget(32, 23, 31);

		testUtil.add(2,51,"",MergeElementType.INITIAL_NODE,-1, "");
		testUtil.add(2,52,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,53,"validate member",MergeElementType.ACTION,-1, "");
		testUtil.add(2,54,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,55,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,56,"else",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,74,"valid member",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,57,"display message not valid member",MergeElementType.ACTION,-1, "");
		testUtil.add(2,58,"get issue details",MergeElementType.ACTION,-1, "");
		testUtil.add(2,59,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,60,"get member type",MergeElementType.ACTION,-1, "");
		testUtil.add(2,61,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,62,"",MergeElementType.DECISION_MERGE_NODE,-1, "");
		testUtil.add(2,63,"no fine",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,64,"else",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,65,"add fine member details to bill",MergeElementType.ACTION,-1, "");
		testUtil.add(2,66,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,67,"create bill",MergeElementType.ACTION,-1, "");
		testUtil.add(2,68,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,69,"update book status",MergeElementType.ACTION,-1, "");
		testUtil.add(2,70,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,71,"update number of books issued",MergeElementType.ACTION,-1, "");
		testUtil.add(2,72,"",MergeElementType.CONTROL_FLOW,-1, "");
		testUtil.add(2,73,"",MergeElementType.ACTIVITY_FINAL_NODE,-1, "");
		testUtil.assignSrcAndTarget(52, 51, 53);
		testUtil.assignSrcAndTarget(54, 53, 55);
		testUtil.assignSrcAndTarget(56, 55, 57);
		testUtil.assignSrcAndTarget(74, 55, 58);
		testUtil.assignSrcAndTarget(59, 58, 60);
		testUtil.assignSrcAndTarget(61, 60, 62);
		testUtil.assignSrcAndTarget(63, 62, 69);
		testUtil.assignSrcAndTarget(64, 62, 65);
		testUtil.assignSrcAndTarget(66, 65, 67);
		testUtil.assignSrcAndTarget(68, 67, 69);
		testUtil.assignSrcAndTarget(70, 69, 71);
		testUtil.assignSrcAndTarget(72, 71, 73);

		testUtil.executeMerge();

		Assert.assertTrue(testUtil.checkMatch(13,53));
		Assert.assertTrue(testUtil.checkMatch(29,69));
		Assert.assertTrue(testUtil.checkMatch(14,54));
		Assert.assertTrue(testUtil.checkMatch(15,55));

		Assert.assertTrue(testUtil.checkConnectionsAreValid());
//		Assert.assertTrue(testUtil.checkIncomingOutgoingsAreValid());

		testUtil.destroy();
	}
}
