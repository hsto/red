package dk.dtu.imm.red.visualstitching.ui.tests;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.visualstitch.MergeElement;
import dk.dtu.imm.red.visualstitch.processor.MergeElementType;
import dk.dtu.imm.red.visualstitch.processor.MergeLogger;
import dk.dtu.imm.red.visualstitch.processor.PartitionTypeComparator;

public class PartitionTypeComparatorTest {
	private PartitionTypeComparator comparator = new PartitionTypeComparator();
	private TreeMap<String, Map<String, MergeElement>> unmatchedPartitionMap=
			new TreeMap<String,Map<String,MergeElement>>(comparator);

	MergeLogger mergeLogger = MergeLogger.instance;


	@Before
	public void setUp() throws Exception {
		mergeLogger.setLogInRCPMode(false);

		unmatchedPartitionMap.put(MergeElementType.CLASS, new HashMap<String,MergeElement>());
		unmatchedPartitionMap.put(MergeElementType.PACKAGE, new HashMap<String,MergeElement>());
		unmatchedPartitionMap.put(MergeElementType.ATTRIBUTE, new HashMap<String,MergeElement>());
		unmatchedPartitionMap.put(MergeElementType.SYSTEM_BOUNDARY, new HashMap<String,MergeElement>());
		unmatchedPartitionMap.put(MergeElementType.ASSOCIATION, new HashMap<String,MergeElement>());
		unmatchedPartitionMap.put(MergeElementType.STATE, new HashMap<String,MergeElement>());
		unmatchedPartitionMap.put(MergeElementType.CLASS, new HashMap<String,MergeElement>());
		unmatchedPartitionMap.put(MergeElementType.ENUM, new HashMap<String,MergeElement>());

	}

	@Test
	public void testPartition() {
		mergeLogger.append(MergeLogger.DEBUG,unmatchedPartitionMap.toString());
		String firstKey = unmatchedPartitionMap.firstKey();
		Assert.assertSame(firstKey, MergeElementType.PACKAGE);

		String nextKey = unmatchedPartitionMap.higherKey(firstKey);
		Assert.assertSame(nextKey, MergeElementType.SYSTEM_BOUNDARY);

		nextKey = unmatchedPartitionMap.higherKey(nextKey);
		Assert.assertSame(nextKey, MergeElementType.CLASS);

		nextKey = unmatchedPartitionMap.higherKey(nextKey);
		Assert.assertSame(nextKey, MergeElementType.ENUM);

		nextKey = unmatchedPartitionMap.higherKey(nextKey);
		Assert.assertSame(nextKey, MergeElementType.STATE);

		nextKey = unmatchedPartitionMap.higherKey(nextKey);
		Assert.assertSame(nextKey, MergeElementType.ASSOCIATION);

		nextKey = unmatchedPartitionMap.higherKey(nextKey);
		Assert.assertSame(nextKey, MergeElementType.ATTRIBUTE);
	}

}
