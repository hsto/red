<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xmi="http://www.omg.org/XMI"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:file="dk.dtu.imm.red.core.file"
	>

	<xsl:param name="outputFileName" />

	<!--Identity Transform (making a copy)-->
	<xsl:template match="@*|node()">
		<xsl:copy>
		<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<!--replace file with file:File, add cost and benefit-->
    <xsl:template match="/File">
		<xsl:element name="file:{local-name()}">
			<xsl:apply-templates select="@*|node()" />
			<cost name="Cost" kind=""/>
			<benefit name="Cost" kind=""/>
		</xsl:element>
    </xsl:template>

	<!--replace file name with the output filename-->
	<xsl:template match="/file:File/@name">
        <xsl:attribute name="name">
            <xsl:value-of select="$outputFileName"/>
        </xsl:attribute>
    </xsl:template>


	<!-- Vision - Change of attributes -->
	<xsl:template match="//contents[@xsi:type='vision:Vision']">
		<contents>
			<xsl:attribute name="label">
				<xsl:value-of select="@title"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()|@*"/>
		</contents>
	</xsl:template>
	<!-- Vision - Change of elements -->


	<!-- Assumption - Change of attributes -->
	<xsl:template match="//contents[@xsi:type='assumption:Assumption']">
		<contents>
			<xsl:attribute name="name">
				<xsl:value-of select="@assumptionName"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()|@*"/>
		</contents>
	</xsl:template>
	<!-- Assumption - Change of elements -->
    <xsl:template match="//contents[@xsi:type='assumption:Assumption']/assumptionSentence">
        <assumptionText>
			<xsl:apply-templates select="@*|node()" />
		</assumptionText>
    </xsl:template>


	<!-- Stakeholder - Change of attributes -->
	<xsl:template match="//contents[@xsi:type='stakeholder:Stakeholder']">
		<contents>
			<xsl:attribute name="elementKind">
				<xsl:value-of select="@type"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()|@*"/>
		</contents>
	</xsl:template>
	<!-- Stakeholder - Change of elements -->
    <xsl:template match="//contents[@xsi:type='stakeholder:Stakeholder']/description">
        <longDescription>
			<xsl:apply-templates select="@*|node()" />
		</longDescription>
    </xsl:template>


	<!-- Persona - Change of attributes -->
	<!-- Persona - Change of elements -->
    <xsl:template match="//contents[@xsi:type='persona:Persona']/description">
        <longDescription>
			<xsl:apply-templates select="@*|node()" />
		</longDescription>
    </xsl:template>


	<!-- Goal - Change of attributes -->
	<xsl:template match="//contents[@xsi:type='goal:Goal']">
		<contents>
			<xsl:attribute name="label">
				<xsl:value-of select="@goalID"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="@goalSentence"/>
			</xsl:attribute>
			<xsl:attribute name="elementKind">
				<xsl:value-of select="@level"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()|@*"/>
		</contents>
	</xsl:template>
	<!-- Goal - Change of elements -->


	<!-- Requirement - Change of attributes -->
	<xsl:template match="//contents[@xsi:type='requirement:Requirement']">
		<contents>
			<xsl:attribute name="label">
				<xsl:value-of select="@name"/>
			</xsl:attribute>
			<xsl:attribute name="description">
				<xsl:value-of select="@requirement"/>
			</xsl:attribute>
			<xsl:attribute name="elementKind">
				<xsl:value-of select="@type"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()|@*"/>
		</contents>
	</xsl:template>
	<!-- Requirement - Change of elements -->
    <xsl:template match="//contents[@xsi:type='requirement:Requirement']/description">
        <longDescription>
			<xsl:apply-templates select="@*|node()" />
		</longDescription>
    </xsl:template>


	<!-- TestCase - Change of attributes -->
	<xsl:template match="//contents[@xsi:type='testcase:TestCase']">
		<contents>
			<xsl:attribute name="label">
				<xsl:value-of select="@testCaseID"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="@testCaseName"/>
			</xsl:attribute>
			<xsl:attribute name="elementKind">
				<xsl:value-of select="@type"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()|@*"/>
		</contents>
	</xsl:template>
	<!-- TestCase - Change of elements -->
    <xsl:template match="//contents[@xsi:type='testcase:TestCase']/description">
        <longDescription>
			<xsl:apply-templates select="@*|node()" />
		</longDescription>
    </xsl:template>


	<!-- Document - Change of attributes -->
	<xsl:template match="//contents[@xsi:type='document:Document']">
		<contents>
			<xsl:attribute name="label">
				<xsl:value-of select="@id"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="@title"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()|@*"/>
		</contents>
	</xsl:template>


	<!-- GlossaryEntry - Change of attributes -->
	<xsl:template match="//contents[@xsi:type='glossary:GlossaryEntry']">
		<contents>
			<xsl:attribute name="label">
				<xsl:value-of select="abbreviations"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="@term"/>
			</xsl:attribute>
			<xsl:attribute name="description">
				<!-- just copy first, and do the clean up in the java code -->
				<xsl:value-of select="definition/fragments/@text"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()|@*"/>
		</contents>
	</xsl:template>


</xsl:stylesheet>