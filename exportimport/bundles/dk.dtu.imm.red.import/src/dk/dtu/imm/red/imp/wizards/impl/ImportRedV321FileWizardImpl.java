package dk.dtu.imm.red.imp.wizards.impl;

import java.io.File;

import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.element.handlers.LoadFileHandler;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.ui.wizards.ChooseFileLocation;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.imp.util.XMLParser;
import dk.dtu.imm.red.imp.wizards.ImportRedV321FileWizard;

public class ImportRedV321FileWizardImpl extends BaseNewWizard implements ImportRedV321FileWizard {

	
	String fileSep = System.getProperty("file.separator");

	protected ChooseFileLocation chooseFileLocation = new ChooseFileLocation(
			"Choose the location of the Red V3.2.1 file for importing",
			"Using the Browse-button, select a location"
					+ " on your computer where this file" + " is stored.",
			new String[] { "*.red" });

	public ImportRedV321FileWizardImpl() {
		super("", null);

	}

	@Override
	public void addPages() {
		addPage(chooseFileLocation);
	}

	@Override
	public boolean performFinish() {
		try {
			
			java.io.File dataFile = new File(chooseFileLocation.getFileName());

			EList<Project> pList=WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects();
			Project currentProject = pList.get(0);
			java.io.File projectFile = new File(currentProject.getUri());

			java.io.File outputXML = new File(projectFile.getParent() + fileSep + dataFile.getName());

			if (!XMLParser.transformXML(dataFile, outputXML)) {
				//unsuccesful in transforming
				System.out.println("Error in transforming");
				return false;
			}

			//Add the file to the project and load it only if it is new file
			dk.dtu.imm.red.core.file.File file = LoadFileHandler.LoadFiles(outputXML.getName(), outputXML.getParentFile().getPath()+fileSep);
			currentProject.addToListOfFilesInProject(file);
			currentProject.save();


			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			LogUtil.logError(ex);
			return false;
		}
	}


	@Override
	public boolean canFinish() {
		return chooseFileLocation.isPageComplete();
	}

}
