package dk.dtu.imm.red.imp.util;

import java.io.File;
import java.io.FileOutputStream;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.Visitor;
import org.dom4j.VisitorSupport;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import dk.dtu.imm.red.core.ui.internal.LogUtil;

public class XMLParser {
	
	private final static String ENCODING = "ASCII";
	private final static String GLOSSARY_PREFIX = "glossary";
	private final static String GLOSSARY_OLD_NS = "dk.dtu.imm.red.glossary";
	private final static String GLOSSARY_NEW_NS = "dk.dtu.imm.red.specificationelements.glossary";
	
	
	public static boolean transformXML(File dataFile, File outputXML) {
		Document doc;
		try {
			if(!outputXML.exists()){
            	outputXML.createNewFile();
            }
			
			doc = new SAXReader().read(dataFile);
			Namespace from = Namespace.get(GLOSSARY_PREFIX, GLOSSARY_OLD_NS);
		    Namespace to = Namespace.get(GLOSSARY_PREFIX, GLOSSARY_NEW_NS);
		    Visitor visitor = new NamesapceChangingVisitor(from, to);
		    doc.accept(visitor);
		    //System.out.println(doc.asXML());
		    
		    OutputFormat outputFormat = new OutputFormat();
		    outputFormat.setEncoding(ENCODING);
		    XMLWriter writer = new XMLWriter(new FileOutputStream(outputXML), outputFormat);
		    writer.write(doc);
		    writer.close();
		    
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.logError(e);
			return false;
		}
	    return true;
	}
}

class NamesapceChangingVisitor extends VisitorSupport {
	private Namespace from;
	private Namespace to;

	public NamesapceChangingVisitor(Namespace from, Namespace to) {
		this.from = from;
		this.to = to;
	}

	public void visit(Element node) {
		Namespace ns = node.getNamespaceForPrefix(from.getPrefix());
		if (ns != null) {
			node.remove(ns);
			node.addNamespace(to.getPrefix(), to.getURI());
		}
	}

}
