package dk.dtu.imm.red.imp.xslt;

import java.io.File;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;
// For write operation


public class SaxonXMLTransformer {


    public static boolean transformXML(File datafile, InputStream stylesheet,  File outputXML){

    	Document document;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            if(!outputXML.exists()){
            	outputXML.createNewFile();
            }

            Processor proc = new Processor(false);
            XsltCompiler comp = proc.newXsltCompiler();
            XsltExecutable exp = comp.compile(new StreamSource(stylesheet));
            XdmNode source = proc.newDocumentBuilder().build(datafile);
            Serializer out = new Serializer();
            out.setOutputProperty(Serializer.Property.METHOD, "xml");
            out.setOutputProperty(Serializer.Property.INDENT, "yes");
            out.setOutputFile(outputXML);
            XsltTransformer trans = exp.load();
            trans.setInitialContextNode(source);
            trans.setParameter(new QName("outputFileName"), new XdmAtomicValue(outputXML.getName()));     
            trans.setDestination(out);
            trans.transform();
            return true;
        } catch (Exception ex) {
        	ex.printStackTrace();
        	LogUtil.logError(ex);
        	return false;
        }
    } // main
}
