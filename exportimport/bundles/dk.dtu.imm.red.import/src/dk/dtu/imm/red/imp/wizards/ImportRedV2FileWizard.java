package dk.dtu.imm.red.imp.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface ImportRedV2FileWizard extends IBaseWizard{

	String ID = "dk.dtu.imm.red.import.redv2.newwizard";

	void init(IWorkbench workbench, IStructuredSelection currentSelection);

}
