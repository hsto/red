package dk.dtu.imm.red.imp.wizards.impl;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.element.handlers.LoadFileHandler;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.ui.wizards.ChooseFileLocation;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.imp.wizards.ImportRedV2FileWizard;
import dk.dtu.imm.red.imp.xslt.SaxonXMLTransformer;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;

public class ImportRedV2FileWizardImpl extends BaseNewWizard implements
		ImportRedV2FileWizard {

	String xsltFilePath = "xslt/transformRedV2.xslt";
	String fileSep = System.getProperty("file.separator");

	protected ChooseFileLocation chooseFileLocation = new ChooseFileLocation(
			"Choose the location of the Red V2 file for importing",
			"Using the Browse-button, select a location"
					+ " on your computer where this file" + " is stored.",
			new String[] { "*.red" });

	public ImportRedV2FileWizardImpl() {
		super("", null);

	}

	@Override
	public void addPages() {
		addPage(chooseFileLocation);
//		addPage(inputFileNamePage);


	}

	@Override
	public boolean performFinish() {
		try {

			//Most of the elements can be converted by just performing XSL transformation of the input file
			Bundle bundle = FrameworkUtil.getBundle(this.getClass());
			URL url = FileLocator.find(bundle, new Path(xsltFilePath), null);
			InputStream stylesheet = url.openConnection().getInputStream();


			java.io.File dataFile = new File(chooseFileLocation.getFileName());

			EList<Project> pList=WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects();
			Project currentProject = pList.get(0);
			java.io.File projectFile = new File(currentProject.getUri());

			java.io.File outputXML = new File(projectFile.getParent() + fileSep + dataFile.getName());


			if(!SaxonXMLTransformer.transformXML(dataFile, stylesheet, outputXML)){
				//unsuccesful in transforming
				System.out.println("Error in transforming");
				return false;
			}


			//Add the file to the project and load it only if it is new file
			dk.dtu.imm.red.core.file.File file = LoadFileHandler.LoadFiles(outputXML.getName(), outputXML.getParentFile().getPath()+fileSep);
			currentProject.addToListOfFilesInProject(file);
			currentProject.save();

			//Special handling for model fragments that reside within requirements
//			findAndCreateModelFragments(file);

			if(file!=null)
				cleanUpContent(file);

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			LogUtil.logError(ex);
			return false;
		}
	}


	//Find Requirement element which contains model fragments and create new ModelFragmentDocument out of it
//	private void findAndCreateModelFragments(dk.dtu.imm.red.core.file.File file){
//		List<Requirement> requirementList = new ArrayList<Requirement>();
//		recursiveFindRequirementWithModelFragment(file,requirementList);
//		for(Requirement req:requirementList){
//			createModelFragmentFromRequirement(req);
//		}
//
//	}

//	private void recursiveFindRequirementWithModelFragment(Group group, List<Requirement> requirementList){
//		List<Element> childList = group.getContents();
//
//		for(Element el : childList){
//			if(el instanceof Group){
//				recursiveFindRequirementWithModelFragment((Group)el,requirementList);
//			}
//			else if(el instanceof Requirement){
//				Requirement requirement = (Requirement) el;
//				ModelFragment mdFrag = requirement.getModelFragment();
//				if(mdFrag!=null){
//					requirementList.add(requirement);
//				}
//			}
//		}
//
//	}


//	private void createModelFragmentFromRequirement(Requirement requirement){
//		ModelFragmentDocument mfDoc = ModelFragmentFactory.eINSTANCE.createModelFragmentDocument();
//
//		mfDoc.setLabel(requirement.getLabel());
//		mfDoc.setName(requirement.getName());
//		mfDoc.setParent(requirement.getParent());
//
//		if (mfDoc.getModelFragment() == null) {
//			mfDoc.setModelFragment(ModelelementFactory.eINSTANCE.createModelFragment());
//		}
//
//		Package fragment = requirement.getModelFragment().getFragment();
//		Diagram diagram = requirement.getModelFragment().getFragmentDiagram();
//		if (fragment == null) {
//			fragment = ModelelementFactory.eINSTANCE.createPackage();
//			fragment.setName(mfDoc.getName());
//		}
//
//		mfDoc.getModelFragment().setFragment(EcoreUtil.copy(fragment));
//		mfDoc.getModelFragment().setFragmentDiagram(EcoreUtil.copy(diagram));
//
//		mfDoc.save();
//	}


	@Override
	public boolean canFinish() {
		return chooseFileLocation.isPageComplete();
	}


	//special handling for glossary, to change the text to plain string from html escape string
	private void cleanUpContent(Element el){
		if(el instanceof Group){
			Group group = (Group) el;
			recursiveCleanUp(group);
		}
	}

	private void recursiveCleanUp(Group group){
		List<Element> childList = group.getContents();
		for(Element el : childList){
			if(el instanceof Group){
				recursiveCleanUp((Group)el);
			}
			else if(el instanceof GlossaryEntry){
				GlossaryEntry entry = (GlossaryEntry) el;
				String description = entry.getDescription();
				//transform html escape format back to normal, and remove the html tags
				description = StringEscapeUtils.unescapeHtml(toPlainString(description));
				entry.setDescription(description);
				entry.save();
			}
		}
	}

	private String toPlainString(String htmlString){
		if(htmlString==null || htmlString.isEmpty()){
			return "";
		}
		return htmlString.replaceAll("\\<[^\\>]*\\>", "");
	}



}
