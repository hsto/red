package dk.dtu.imm.red.imp.wizards.impl;

import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.imp.wizards.ImportRedV2FileWizard;

public class DefineProjectPage extends WizardPage implements IWizardPage{

	protected Label projectTitle;
	protected Text titleTextBox;
	protected Label projectLabel;
	protected RichTextEditor descriptionEditor;
	protected ImportRedV2FileWizard wizard;

	public DefineProjectPage(ImportRedV2FileWizard wizard) {
		super("Define Project");

		setTitle("Create New Project");
		setDescription("Use this wizard to create a new project");

		this.wizard = wizard;
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(2, false);

		composite.setLayout(layout);
		
		projectTitle = new Label(composite, SWT.NONE);
		projectTitle.setText("Title");
		titleTextBox = new Text(composite, SWT.BORDER);

		projectLabel = new Label(composite, SWT.NONE);
		projectLabel.setText("Project description");

		GridData descriptionGridData = new GridData();
		descriptionGridData.horizontalSpan = 2;
		descriptionGridData.grabExcessHorizontalSpace = true;
		descriptionGridData.grabExcessVerticalSpace = true;
		descriptionGridData.horizontalAlignment = SWT.FILL;
		descriptionGridData.verticalAlignment = SWT.FILL;


		descriptionEditor = new RichTextEditor(composite, SWT.BORDER, null, null);
		descriptionEditor.setLayoutData(descriptionGridData);
		
		setPageComplete(true);
		setControl(composite);
	}
	@Override
	public IWizardPage getNextPage() {
		//wizard.setDocumentName(nameTextBox.getText());
		return super.getNextPage();
	}

	public String getDescription() {
		return descriptionEditor.getText();
	}

	public String getProjectName() {
		return titleTextBox.getText();
	}
}
