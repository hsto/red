package dk.dtu.imm.red.imp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.IHandlerService;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.imp.wizards.ImportRedV321FileWizard;
import dk.dtu.imm.red.imp.wizards.impl.ImportRedV321FileWizardImpl;

public class ImportRedV321FileHandler extends AbstractHandler implements IHandler {
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		try {
			//check if any open project and force to open one
			if (!(WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
					.getCurrentlyOpenedProjects().size() > 0)) {
				MessageDialog dialog = new MessageDialog(
						HandlerUtil.getActiveShell(event),
						"No Open Project",
						null,
						"There is no project opened yet, Do you want to create a new one or open an existing one?",
						MessageDialog.CONFIRM, new String[] { "New Project",
								"Open Existing Project" }, 0);
				int result = dialog.open();
				IWorkbenchWindow window = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow();

				IHandlerService handlerService = (IHandlerService) window
						.getService(IHandlerService.class);
				try {
					if (result==0) {
						handlerService.executeCommand(
								"dk.dtu.imm.red.core.project.newproject", null);
					} else if(result==1){
						handlerService.executeCommand(
								"dk.dtu.imm.red.core.openproject", null);
					}
					else{
						return null;
					}
				} catch (Exception e) {
					e.printStackTrace();
					LogUtil.logError(e);
				}			
			}
			
			
			
			ImportRedV321FileWizard wizard = new ImportRedV321FileWizardImpl();
			ISelection currentSelection = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getSelectionService()
					.getSelection();
			wizard.init(PlatformUI.getWorkbench(),
					(IStructuredSelection) currentSelection);

			WizardDialog dialog = new WizardDialog(
					HandlerUtil.getActiveShell(event), (IWizard) wizard);
			dialog.create();

			dialog.open();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
