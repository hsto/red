package dk.dtu.imm.red.export.jet;

import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;

/**Class which translate a Requirement specification element into prolog statements.
 * @author HenryLie
 *
 */
public class RequirementTemplatePL extends ElementTemplatePL{
	protected static String nl = System.getProperties().getProperty("line.separator");

	protected MPLExport export;

	public RequirementTemplatePL(MPLExport export){
		super(export);
		this.export = export;
	}

	/**Translate the requirement specification element into a String containing the prolog statements
	 * It will also generate the list of ID containing the AcceptanceTest and ModelFragments, however the translation of the
	 * AcceptanceTest and ModelFragments are done out of this class
	 *
	 * @param persona		Persona specification element to be translated
	 * @return				The output translated string containing the prolog statements
	 */
	public String generate(Requirement requirement) {
		final StringBuffer sb = new StringBuffer();

//		ArrayList<String> acceptanceTestIDList = new ArrayList<String>();
//		if(requirement.getAcceptanceTests()!=null && !requirement.getAcceptanceTests().isEmpty()){
//			for(AcceptanceTest test: requirement.getAcceptanceTests()){
//				acceptanceTestIDList.add(export.getPrologID(test.getUniqueID()));
//				super.addToOwnedMemberIDList(acceptanceTestIDList);
//			}
//		}

//		ArrayList<String> modelFragmentIDList = new ArrayList<String>();
//		if(requirement.getModelFragment()!=null && requirement.getModelFragment().getFragment()!=null
//				&& !requirement.getModelFragment().getFragment().getContents().isEmpty()){
//			for(Element element: requirement.getModelFragment().getFragment().getContents()){
//				if(element instanceof ModelElement){
//					modelFragmentIDList.add(export.getPrologID(element.getUniqueID()));
//					super.addToOwnedMemberIDList(modelFragmentIDList);
//				}
//			}
//		}


		MPLAppender.declareMetaClass(sb, "requirement", export.getPrologID(requirement.getUniqueID()));

		super.appendBasicAttributes(sb,true,requirement);

		MPLAppender.appendAttribute(sb,"abstractionLevel",requirement.getAbstractionLevel());
		if(requirement.getDetails()!=null && !requirement.getDetails().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb,"elaboration",requirement.getDetails().toPlainString());
		}
		if(requirement.getRemarks()!=null){
			MPLAppender.appendAttribute(sb,"remarks",requirement.getRemarks().toPlainString());
		}

//		if(!acceptanceTestIDList.isEmpty()){
//			MPLAppender.appendIDArrays(sb, "acceptanceTest", acceptanceTestIDList);
//		}
//		if(!modelFragmentIDList.isEmpty()){
//			MPLAppender.appendIDArrays(sb, "modelFragmentContent", modelFragmentIDList);
//		}


		MPLAppender.closeMetaClass(sb);
		return sb.toString();
	}


}
