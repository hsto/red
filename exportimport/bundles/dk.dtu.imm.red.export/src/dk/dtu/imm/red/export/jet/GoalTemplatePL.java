package dk.dtu.imm.red.export.jet;

import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;
import dk.dtu.imm.red.specificationelements.goal.Goal;

/**Class which translate a Goal specification element into prolog statements.
 * @author HenryLie
 *
 */
public class GoalTemplatePL extends ElementTemplatePL{
	protected static String nl = System.getProperties().getProperty("line.separator");	

	protected MPLExport export;
	
	public GoalTemplatePL(MPLExport export){
		super(export);
		this.export = export;
	}
	
	/**Translate the goal element into a String containing the prolog statements
	 * 
	 * @param goal			Goal specification element to be translated
	 * @return				The output translated string containing the prolog statements
	 */	
	public String generate(Goal goal) {
		final StringBuffer sb = new StringBuffer();
		MPLAppender.declareMetaClass(sb, "goal", export.getPrologID(goal.getUniqueID()));
		appendBasicAttributes(sb,true,goal);
		MPLAppender.closeMetaClass(sb);
		return sb.toString();
	}
	

}
