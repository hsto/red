package dk.dtu.imm.red.export.jet;

import java.util.ArrayList;

import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;

/**Class which translate a Persona specification element into prolog statements.
 * @author HenryLie
 *
 */
public class PersonaTemplatePL extends ElementTemplatePL{
	protected static String nl = System.getProperties().getProperty("line.separator");	

	protected MPLExport export;
	
	public PersonaTemplatePL(MPLExport export){
		super(export);
		this.export = export;
	}
	
	/**Translate the persona specification element into a String containing the prolog statements
	 * It will also generate the list of ID containing the storyboards, however the translation of the
	 * storyboards are done out of this class
	 * 
	 * @param persona		Persona specification element to be translated
	 * @return				The output translated string containing the prolog statements
	 */	
	public String generate(Persona persona) {
		
		ArrayList<String> storyboardIDList = new ArrayList<String>();
		if(persona.getStoryboards()!=null && persona.getStoryboards().size()!=0){
			Storyboard storyboard = persona.getStoryboards().get(0);
			storyboardIDList.add(export.getPrologID(storyboard.getUniqueID()));
			super.addToOwnedMemberIDList(storyboardIDList);
		}
		
		final StringBuffer sb = new StringBuffer();
		MPLAppender.declareMetaClass(sb, "persona", export.getPrologID(persona.getUniqueID()));
		super.appendBasicAttributes(sb,true,persona);
		MPLAppender.appendAttribute(sb,"job",persona.getJob());
		MPLAppender.appendAttribute(sb,"age",persona.getAge().toString());

		if(persona.getCapabilities()!=null && !persona.getCapabilities().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb,"capabilities",persona.getCapabilities().toPlainString());
		}
		if(persona.getConstraints()!=null && !persona.getConstraints().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb,"constraints",persona.getConstraints().toPlainString());
		}			
		MPLAppender.appendAttribute(sb,"pictureURI",persona.getPictureURI());		
		

		if(!storyboardIDList.isEmpty()){
			MPLAppender.appendIDArrays(sb, "storyboard", storyboardIDList);
		}

		
		storyboardIDList.clear();
		storyboardIDList=null;
		
		MPLAppender.closeMetaClass(sb);
		return sb.toString();
	}
}
