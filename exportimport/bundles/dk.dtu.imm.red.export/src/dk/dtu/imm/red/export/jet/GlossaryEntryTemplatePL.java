package dk.dtu.imm.red.export.jet;

import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;

/**Class which translate a GlossaryEntry specification element into prolog statements.
 * @author HenryLie
 *
 */
public class GlossaryEntryTemplatePL extends ElementTemplatePL{
	protected static String nl = System.getProperties().getProperty("line.separator");

	protected MPLExport export;

	public GlossaryEntryTemplatePL(MPLExport export){
		super(export);
		this.export = export;
	}

	/**Translate the glossary entry element into a String containing the prolog statements
	 *
	 * @param entry			GlossaryEntry specification element to be translated
	 * @return				The output translated string containing the prolog statements
	 */
	public String generate(GlossaryEntry entry) {
		final StringBuffer sb = new StringBuffer();
		MPLAppender.declareMetaClass(sb, "glossaryEntry", export.getPrologID(entry.getUniqueID()));
		appendBasicAttributes(sb,true,entry);
//		MPLAppender.appendAttribute(sb,"term",entry.getTerm());
//		if(entry.getAbbreviations()!=null && !entry.getAbbreviations().isEmpty()){
//			MPLAppender.appendAttribute(sb,"abbreviations",entry.getAbbreviations().toString());
//		}
//		if(entry.getDefinition()!=null && !entry.getDefinition().toPlainString().isEmpty()){
//			MPLAppender.appendAttribute(sb,"definition",entry.getDefinition().toPlainString());
//		}
		if(entry.getSynonyms()!=null && !entry.getSynonyms().toString().isEmpty()){
			MPLAppender.appendAttribute(sb,"synonyms",entry.getSynonyms().toString());
		}
		if(entry.getDiscussion()!=null && !entry.getDiscussion().getFragments().isEmpty()){
			MPLAppender.appendAttribute(sb,"discussion",entry.getDiscussion().toPlainString());
		}
		MPLAppender.closeMetaClass(sb);
		return sb.toString();
	}


}
