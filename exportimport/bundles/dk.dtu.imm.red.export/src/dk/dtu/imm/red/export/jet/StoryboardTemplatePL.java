package dk.dtu.imm.red.export.jet;

import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.persona.storyboard.StoryboardPanel;

/**Class which translate a Storyboard specification element into prolog statements.
 * @author HenryLie
 *
 */
public class StoryboardTemplatePL extends ElementTemplatePL{
	protected static String nl = System.getProperties().getProperty("line.separator");	

	protected MPLExport export;
	
	public StoryboardTemplatePL(MPLExport export){
		super(export);
		this.export = export;
	}
	
	/**Translate the storyboard element into a String containing the prolog statements
	 * 
	 * @param storyboard	Storyboard specification element to be translated
	 * @return				The output translated string containing the prolog statements
	 */	
	public String generate(Storyboard storyboard) {
		final StringBuffer sb = new StringBuffer();
		MPLAppender.declareMetaClass(sb, "storyboard", export.getPrologID(storyboard.getUniqueID()));
		appendBasicAttributes(sb,true,storyboard);
		if(storyboard.getIntroduction()!=null && storyboard.getIntroduction().toPlainString()!=null
				&& !storyboard.getIntroduction().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb, "introduction", storyboard.getIntroduction().toPlainString());
		}
		if(storyboard.getPanels()!=null && !storyboard.getPanels().isEmpty()){
			int panelIndex=0;
			for(StoryboardPanel panel : storyboard.getPanels()){
				if(panel.getCaption()!=null && !panel.getCaption().isEmpty()){
					String caption = panel.getCaption();
					MPLAppender.appendAttribute(sb, "caption"+panelIndex, caption);
				}
				panelIndex++;
			}
		}
		if(storyboard.getConclusion()!=null && storyboard.getConclusion().toPlainString()!=null
				&& !storyboard.getConclusion().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb, "conclusion", storyboard.getConclusion().toPlainString());
		}
		MPLAppender.closeMetaClass(sb);
		return sb.toString();
	}
	

}
