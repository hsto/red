package dk.dtu.imm.red.export;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Utility class which is used to write some fixed format of prolog metaclasses
 * by appending it into a string buffer. It supports declaration of module,
 * declaration and closing of the declaration of metaclasses, appending of attribute,
 * and appending of attribute which is an array type
 *  
 * @author HenryLie
 *
 */
public class MPLAppender {

	protected static String nl = System.getProperties().getProperty("line.separator");	

	/**Declare a Prolog module into a string buffer by providing it with a moduleName
	 * 
	 * @param sb 			StringBuffer for the output destination
	 * @param moduleName	Name of the module to be declared
	 */
	public static void declareModule(StringBuffer sb, String moduleName){
		sb.append(":-module('" + moduleName + "',[])."+nl);
	}
	
	/**Starts declaration of a metaclass into a string buffer by providing it with the metaclass name and
	 * a prologID. It should be followed by appending some other attributes and closing the metaclass to 
	 * make the metaclass complete 
	 * 
	 * @param sb				StringBuffer for the output destination
	 * @param metaclassName		Name of the metaclass to be declared
	 * @param prologID			prolog identifier. It should be a number type
	 */
	public static void declareMetaClass(StringBuffer sb, String metaclassName, String prologID){
		sb.append("me("+metaclassName+"-" + prologID + ",[");
	}
	
	/**Close the declaration of a metaclass into a string buffer.
	 * @param sb	StringBuffer for the output destination
	 */
	public static void closeMetaClass(StringBuffer sb){
		sb.append("])."+nl);
	}
	
	/**Append an attribute into the output string buffer. If it is the first attribute to be declared, first should be set to true
	 * so that a comma in front will be omitted, otherwise all the subsequent attribute needs to have the comma in front as separator
	 * @param sb				StringBuffer for the output destination
	 * @param first				Boolean variable to indicate if it is the first attribute, so that a comma in front is not required
	 * @param attributeName		Name of the attribute of the metaclass
	 * @param attributeValue	Value of the attribute of the metaclass
	 */
	public static void appendAttribute(StringBuffer sb, boolean first, String attributeName,String attributeValue){
		if(attributeValue!=null && !attributeValue.isEmpty()){
			if(!first){
				sb.append(",");
			}
			if(attributeValue.contains("'")){
				attributeValue = attributeValue.replaceAll("'", "\\\\'");
			}			
			sb.append(attributeName+"-'"+attributeValue+"'");
		}
	}
	
	/**Similar to the other appendAttribute method, but with variable "first" set to false
	 * @param sb					
	 * @param attributeName		Name of the attribute of the metaclass
	 * @param attributeValue	Value of the attribute of the metaclass
	 */
	public static void appendAttribute(StringBuffer sb, String attributeName,String attributeValue){
		appendAttribute(sb,false,attributeName,attributeValue);
	}	
	
	/**Append an array type ID attribute into the output string buffer. It can be used to append attribute which contains
	 * array values, such as ownedMember-ids, relatedTo-ids etc
	 * @param sb				StringBuffer for the output destination
	 * @param first				Boolean variable to indicate if it is the first attribute, so that a comma in front is not required
	 * @param attributeName		Name of the attribute of the metaclass which contains an array value
	 * @param idList			Array value containing the list of ID
	 */
	public static void appendIDArrays(StringBuffer sb, boolean first, String attributeName, ArrayList<String> idList){
		if(idList!=null && !idList.isEmpty()){		
			if(!first){
				sb.append(",");
			}
			
			class MyIntComparator implements Comparator<String>{
			    @Override
			    public int compare(String o1, String o2) {
			    	
			    	int i1= Integer.parseInt(o1);
			    	int i2= Integer.parseInt(o2);
			        return (i1<i2 ? -1 : (i1==i2 ? 0 : 1));
			    }
			} 
			
			Collections.sort(idList,new MyIntComparator());
			
			sb.append(attributeName+"-ids"+idList.toString().replaceAll(" ", ""));
		}
	}
	
	/**Similar to the other appendIDArrays method but with variable "first" being set to false
	 * @param sb				StringBuffer for the output destination
	 * @param attributeName		Name of the attribute of the metaclass which contains an array value
	 * @param idList			Array value containing the list of ID
	 */
	public static void appendIDArrays(StringBuffer sb, String attributeName, ArrayList<String> idList){
		appendIDArrays(sb, false, attributeName, idList);
	}
	
}
