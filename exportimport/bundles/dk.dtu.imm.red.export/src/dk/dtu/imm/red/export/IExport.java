package dk.dtu.imm.red.export;

import dk.dtu.imm.red.core.element.Element;

public interface IExport {
	
	/**Export the Element into a specific file path
	 * @param element	Root element to start the export function
	 * @param path		File path of the export
	 * @return
	 */
	String exportFile(Element element, String path);

}
