//package dk.dtu.imm.red.export.jet;
//
//import dk.dtu.imm.red.export.MPLAppender;
//import dk.dtu.imm.red.export.MPLExport;
//import dk.dtu.imm.red.modelelement.ModelElement;
//
///**Class which translate a ModelElement specification element into prolog statements.
// * @author HenryLie
// *
// */
//public class ModelElementTemplatePL extends ElementTemplatePL{
//	protected static String nl = System.getProperties().getProperty("line.separator");
//
//	protected MPLExport export;
//
//	public ModelElementTemplatePL(MPLExport export){
//		super(export);
//		this.export = export;
//	}
//
//	/**Translate the model element into a String containing the prolog statements
//	 *
//	 * @param element		ModelElement specification element to be translated
//	 * @return				The output translated string containing the prolog statements
//	 */
//	public String generate(ModelElement element) {
//		final StringBuffer sb = new StringBuffer();
//		MPLAppender.declareMetaClass(sb, "modelElement", export.getPrologID(element.getUniqueID()));
//		appendBasicAttributes(sb,true,element);
//		MPLAppender.appendAttribute(sb,"type",element.getClass().getSimpleName());
//		MPLAppender.closeMetaClass(sb);
//		return sb.toString();
//	}
//
//
//}
