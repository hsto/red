package dk.dtu.imm.red.export;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.mutable.MutableInt;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.export.jet.AssumptionTemplatePL;
import dk.dtu.imm.red.export.jet.DocumentTemplatePL;
import dk.dtu.imm.red.export.jet.GlossaryEntryTemplatePL;
import dk.dtu.imm.red.export.jet.GoalTemplatePL;
import dk.dtu.imm.red.export.jet.PersonaTemplatePL;
import dk.dtu.imm.red.export.jet.RequirementTemplatePL;
import dk.dtu.imm.red.export.jet.StakeholderTemplatePL;
import dk.dtu.imm.red.export.jet.StoryboardTemplatePL;
import dk.dtu.imm.red.export.jet.VisionTemplatePL;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;
import dk.dtu.imm.red.specificationelements.glossary.Glossary;
import dk.dtu.imm.red.specificationelements.glossary.GlossaryEntry;
import dk.dtu.imm.red.specificationelements.goal.Goal;
import dk.dtu.imm.red.specificationelements.persona.Persona;
import dk.dtu.imm.red.specificationelements.persona.storyboard.Storyboard;
import dk.dtu.imm.red.specificationelements.requirement.Requirement;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;
import dk.dtu.imm.red.specificationelements.vision.Vision;

/**Class which translate all the element's in RED into prolog statements
 *
 * @author HenryLie
 *
 */
public class MPLExport implements IExport {

	private HashMap<String, String> uniqueIDtoPrologIDMap = new HashMap<String, String>();
	protected static String nl = System.getProperties().getProperty(
			"line.separator");

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * dk.dtu.imm.red.export.IExport#exportFile(dk.dtu.imm.red.core.element.
	 * Element, java.lang.String)
	 */
	@Override
	public String exportFile(Element element, String path) {
		StringBuffer sb = new StringBuffer();
		MutableInt id = new MutableInt(0);

		// first pass traversal
		if (element instanceof Group) { // container element
			firstPassVisit((Group) element, id);
		} else { // leaf element
			Folder tempFolder = FolderFactory.eINSTANCE.createFolder();
			tempFolder.getContents().add(element);
			firstPassVisit(tempFolder, id);
		}

		// second pass traversal
		String moduleName = element.getName();
		if(element.getName().endsWith(".red") || element.getName().endsWith(".redproject")){
			moduleName = moduleName.replaceAll(".redproject", "");
			moduleName = moduleName.replaceAll(".red", "");
		}
		MPLAppender.declareModule(sb, moduleName);

		String text;
		if (element instanceof Group) { // container element
			text = secondPassVisit((Group) element);
		} else { // leaf element
			Folder tempFolder = FolderFactory.eINSTANCE.createFolder();
			tempFolder.getContents().add(element);
			text = secondPassVisit(tempFolder);
		}

		// System.out.println("uniqueIDtoPrologIDMap{"+uniqueIDtoPrologIDMap.size()+"}:");
		// System.out.println(uniqueIDtoPrologIDMap);

		// clear the hashMap
		uniqueIDtoPrologIDMap.clear();
		uniqueIDtoPrologIDMap = null;

		return sb.toString() + text;

	}

	/**First pass tree traversal to all the elements and assign them with unique prolog identifiers,
	 * storing them in a map with the key is the element's unique id, and the value is the unique prolog identifier
	 * Use MutableInt to increment the id value via passing by reference because java primitive Int variable
	 * is not mutable. This is similar to declaring the id as static so that it can be updated in any
	 * part of the method call.
	 * First pass traversal is required because some of the element may reference other element, so the export to prolog
	 * cannot be done without assigning all the identifiers first.
	 * Parameter group is used because it is a container element which can be file, project, workspace, folder etc.
	 * Any other specification element which resides within another specification element (for example storyboard,
	 * acceptance test, model fragments need to be handled by special handling.
	 *
	 * @param group		The element to start the traversal.
	 * @param id		Counter for prolog ID
	 */
	private void firstPassVisit(Group group, MutableInt id) {
		if (!uniqueIDtoPrologIDMap.containsKey(group.getUniqueID())) {
			uniqueIDtoPrologIDMap.put(group.getUniqueID(), id.toString());
		}
		id.increment();

		System.out.println("FirstPassVisit: Group:"+group);

		List<Element> childList = new ArrayList<Element>();
		if(group instanceof Project){
			Project p = (Project) group;
			childList.addAll(p.getListofopenedfilesinproject());
		}
		else{
			childList.addAll(group.getContents());
		}

//		for (Element child : group.getContents()) {
		for (Element child : childList) {
			if (child instanceof Group) {
				firstPassVisit((Group) child, id);
			} else {
				// this is an element which is not group
				if (!uniqueIDtoPrologIDMap.containsKey(child.getUniqueID())) {
					uniqueIDtoPrologIDMap.put(child.getUniqueID(),
							id.toString());
				}
				id.increment();

				// Now, we also need to assign ID for part of the element which
				// is multivalued/composite attribute
				// (or another element on its own) as well, for example:
				// commentlist, changelist, relatedby, relatesto,
				// and also for storyboard, acceptance test, and model fragments

				// traverse and generate prolog ID for the comments
				if (child.getCommentlist() != null
						&& child.getCommentlist().getComments() != null
						&& !child.getCommentlist().getComments().isEmpty()) {
					for (Comment comment : child.getCommentlist().getComments()) {
						if (!uniqueIDtoPrologIDMap.containsKey(comment
								.getUniqueID())) {
							uniqueIDtoPrologIDMap.put(comment.getUniqueID(),
									id.toString());
						}
						id.increment();
					}
				}

				// traverse and generate prolog ID for the change list
				if (child.getChangeList() != null
						&& child.getChangeList().getComments() != null
						&& !child.getChangeList().getComments().isEmpty()) {
					for (Comment comment : child.getChangeList().getComments()) {
						if (!uniqueIDtoPrologIDMap.containsKey(comment
								.getUniqueID())) {
							uniqueIDtoPrologIDMap.put(comment.getUniqueID(),
									id.toString());
						}
						id.increment();
					}
				}

				/* Commented because it is redundant because the relation is captured
				 * by the RelatesTo relationship as well
				 *
				 * //traverse and generate prolog ID for the related-by
				 * relationships if(child.getRelatedBy()!=null &&
				 * child.getRelatedBy().size()!=0){ for (ElementRelationship ref
				 * : child.getRelatedBy()){
				 * if(!uniqueIDtoPrologIDMap.containsKey(ref.getUniqueID())){
				 * uniqueIDtoPrologIDMap.put(ref.getUniqueID(), id.toString());
				 * } id.increment(); } }
				 */

				// traverse and generate prolog ID for the relates-to
				// relationships
				if (child.getRelatesTo() != null
						&& child.getRelatesTo().size() != 0) {
					for (ElementRelationship ref : child.getRelatesTo()) {
						if (!uniqueIDtoPrologIDMap.containsKey(ref
								.getUniqueID())) {
							uniqueIDtoPrologIDMap.put(ref.getUniqueID(),
									id.toString());
						}
						id.increment();
					}
				}

				// traverse storyboard
				if (child instanceof Persona) {
					Persona persona = (Persona) child;
					if (persona.getStoryboards() != null
							&& persona.getStoryboards().size() != 0) {
						Storyboard storyboard = persona.getStoryboards().get(0);
						if (!uniqueIDtoPrologIDMap.containsKey(storyboard
								.getUniqueID())) {
							uniqueIDtoPrologIDMap.put(storyboard.getUniqueID(),
									id.toString());
						}
						id.increment();
					}
				}

				// traverse acceptance test
//				if (child instanceof Requirement) {
//					Requirement requirement = (Requirement) child;
//					if (requirement.getAcceptanceTests() != null
//							&& requirement.getAcceptanceTests().size() != 0) {
//						for (AcceptanceTest test : requirement
//								.getAcceptanceTests()) {
//							if (!uniqueIDtoPrologIDMap.containsKey(test
//									.getUniqueID())) {
//								uniqueIDtoPrologIDMap.put(test.getUniqueID(),
//										id.toString());
//							}
//							id.increment();
//						}
//					}
//				}

				// traverse model fragments
//				if (child instanceof Requirement) {
//					Requirement requirement = (Requirement) child;
//					if (requirement.getModelFragment() != null) {
//						ModelFragment fragment = requirement.getModelFragment();
//						Package p = fragment.getFragment();
//						if (p != null) {
//							for (Object obj : p.getContents()) {
//								if (obj instanceof Element) {
//									Element el = (Element) obj;
//									if (!uniqueIDtoPrologIDMap.containsKey(el
//											.getUniqueID())) {
//										uniqueIDtoPrologIDMap
//												.put(el.getUniqueID(),
//														id.toString());
//									}
//									id.increment();
//								}
//							}
//						}
//					}
//				}

			} // end handling for non Group elements

		}

		childList.clear();childList = null;
	}



	/**Second pass traversal to print all the element into Prolog MPL file. It use the MPLAppender
	 * to construct the prolog statements and use the id from the hashmap which has been collected
	 * from the first pass visit
	 *
	 * @param group			Group to start the traversal
	 * @return 				The string output result containing all the Prolog statements
	 */
	private String secondPassVisit(Group group) {
		StringBuffer sb = new StringBuffer();
		String curId = uniqueIDtoPrologIDMap.get(group.getUniqueID());


		List<Element> childList = new ArrayList<Element>();
		if(group instanceof Project){
			Project p = (Project) group;
			childList.addAll(p.getListofopenedfilesinproject());
		}
		else{
			childList.addAll(group.getContents());
		}



		// construct list of ownedMembers
		ArrayList<Group> childGroups = new ArrayList<Group>();
		ArrayList<String> ownedMember = new ArrayList<String>();

//		for (Element child : group.getContents()) {
		for (Element child : childList) {
			String childId = uniqueIDtoPrologIDMap.get(child.getUniqueID());
			if (!ownedMember.contains(childId)) {
				ownedMember.add(childId);
			}
			if (child instanceof Group) {
				childGroups.add((Group) child);
			}
		}

		String groupType = "";
		if (group instanceof File) {
			groupType = "file";
		} else if (group instanceof Folder) {
			groupType = "folder";
		} else if (group instanceof Project) {
			groupType = "project";
		} else if (group instanceof Glossary) {
			groupType = "glossary";
		}
		MPLAppender.declareMetaClass(sb, groupType, curId);
		MPLAppender.appendAttribute(sb, true, "name", group.getName());
		MPLAppender.appendIDArrays(sb, "ownedMember", ownedMember);
		ownedMember.clear();
		ownedMember=null;
		MPLAppender.appendAttribute(sb, "timeCreated", group.getTimeCreated()
				.toString());
		MPLAppender.appendAttribute(sb, "lastModified", group.getLastModified()
				.toString());
		MPLAppender.appendAttribute(sb, "uri", group.getUri());
		MPLAppender.closeMetaClass(sb);

//		for (Element child : group.getContents()) {
		for (Element child : childList) {
			if (child instanceof GlossaryEntry) {
				sb.append(new GlossaryEntryTemplatePL(this).generate((GlossaryEntry) child));
			} else if (child instanceof Goal) {
				sb.append(new GoalTemplatePL(this).generate((Goal) child));
			} else if (child instanceof Persona) {
				sb.append(new PersonaTemplatePL(this).generate((Persona) child));
			} else if (child instanceof Requirement) {
				sb.append(new RequirementTemplatePL(this).generate((Requirement) child));
			} else if (child instanceof Stakeholder) {
				sb.append(new StakeholderTemplatePL(this).generate((Stakeholder) child));
			} else if (child instanceof Vision) {
				sb.append(new VisionTemplatePL(this).generate((Vision) child));
			} else if (child instanceof Assumption) {
				sb.append(new AssumptionTemplatePL(this).generate((Assumption) child));
			} else if (child instanceof Document) {
				sb.append(new DocumentTemplatePL(this).generate((Document) child));
			}

			// append all the list of comments metaclasses
			if (child.getCommentlist() != null
					&& child.getCommentlist().getComments() != null
					&& child.getCommentlist().getComments().size() != 0) {
				for (Comment comment : child.getCommentlist().getComments()) {
					String prologID = getPrologID(comment.getUniqueID());
					if (prologID != null && !prologID.isEmpty()) {
						MPLAppender.declareMetaClass(sb, "comment", prologID);
						;
						MPLAppender.appendAttribute(sb, true, "author", comment
								.getAuthor().getName());
						if (comment.getTimeCreated() != null
								&& !comment.getTimeCreated().toString()
										.isEmpty()) {
							MPLAppender.appendAttribute(sb, "timeCreated",
									comment.getTimeCreated().toString());
						}
						if (comment.getText() != null
								&& !comment.getText().toPlainString().isEmpty()) {
							MPLAppender.appendAttribute(sb, "comment", comment
									.getText().toPlainString());
						}
						MPLAppender.closeMetaClass(sb);
					}

				}
			}

			// append all the list of changeLists metaclasses
			if (child.getChangeList() != null
					&& child.getChangeList().getComments() != null
					&& child.getChangeList().getComments().size() != 0) {
				for (Comment comment : child.getChangeList().getComments()) {
					String prologID = getPrologID(comment.getUniqueID());
					if (prologID != null && !prologID.isEmpty()) {
						MPLAppender.declareMetaClass(sb, "changelog", prologID);
						;
						MPLAppender.appendAttribute(sb, true, "author", comment
								.getAuthor().getName());
						if (comment.getTimeCreated() != null
								&& !comment.getTimeCreated().toString()
										.isEmpty()) {
							MPLAppender.appendAttribute(sb, "timeCreated",
									comment.getTimeCreated().toString());
						}
						if (comment.getText() != null
								&& !comment.getText().toPlainString().isEmpty()) {
							MPLAppender.appendAttribute(sb, "comment", comment
									.getText().toPlainString());
						}
						MPLAppender.closeMetaClass(sb);
					}

				}
			}

			// append all the list of relations metaclasses
			if (child.getRelatesTo() != null && !child.getRelatesTo().isEmpty()) {
				for (ElementRelationship relation : child.getRelatesTo()) {
					String prologID = getPrologID(relation.getUniqueID());
					if (prologID != null && !prologID.isEmpty()) {
						MPLAppender.declareMetaClass(sb, "relation", prologID);
						Element toEl = relation.getToElement();
						String toElementPrologID = getPrologID(toEl
								.getUniqueID());
						String fromElementPrologID = getPrologID(child
								.getUniqueID());
						MPLAppender.appendAttribute(sb, true, "relatesToID",
								toElementPrologID);
						MPLAppender.appendAttribute(sb, "relatedByID",
								fromElementPrologID);
						MPLAppender.appendAttribute(sb, "relationshipType",
								relation.getClass().getSimpleName());
						MPLAppender.closeMetaClass(sb);
					}
				}
			}

			// Now append all the problematic specification elements which
			// resides within another specification elements
			// such as storyboard, acceptance test and model fragments
			if (child instanceof Persona) {
				Persona persona = (Persona) child;
				if (persona.getStoryboards() != null
						&& !persona.getStoryboards().isEmpty()) {
					Storyboard storyboard = (Storyboard) persona
							.getStoryboards().get(0);
					sb.append(new StoryboardTemplatePL(this)
							.generate((Storyboard) storyboard));
				}

			}
			if (child instanceof Requirement) {
//				Requirement requirement = (Requirement) child;
//				if (requirement.getAcceptanceTests() != null
//						&& !requirement.getAcceptanceTests().isEmpty()) {
//					for (AcceptanceTest test : requirement.getAcceptanceTests()) {
//						sb.append(new AcceptanceTestTemplatePL(this)
//								.generate(test));
//					}
//				}

//				if (requirement.getModelFragment() != null
//						&& requirement.getModelFragment().getFragment() != null
//						&& !requirement.getModelFragment().getFragment()
//								.getContents().isEmpty()) {
//					for (Element element : requirement.getModelFragment()
//							.getFragment().getContents()) {
//						if (element instanceof ModelElement) {
//							sb.append(new ModelElementTemplatePL(this)
//									.generate((ModelElement) element));
//						}
//					}
//				}
			}

			// traverse deeper if it is a group
			if (child instanceof Group) {
				sb.append(secondPassVisit((Group) child));
			}

		}

		return sb.toString();

	}


	/**Use the hashmap collected from the first pass visit to return the mapping from the element's
	 * unique id to the assigned prolog id
	 *
	 * @param uniqueID		Element's unique ID
	 * @return				the mapping to the assigned prolog ID
	 */
	public String getPrologID(String uniqueID) {
		if (uniqueIDtoPrologIDMap.containsKey(uniqueID)) {
			return uniqueIDtoPrologIDMap.get(uniqueID).toString();
		} else
			return "";

	}

}
