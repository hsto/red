package dk.dtu.imm.red.export.jet;

import java.util.ArrayList;
import java.util.HashSet;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;

/**Class which translate the common attributes for any specification element into prolog statements.
 * @author HenryLie
 *
 */
public class ElementTemplatePL {

	protected static String nl = System.getProperties().getProperty("line.separator");	

	protected MPLExport export;
	protected ArrayList<String> ownedMemberIDList = new ArrayList<String>();;
	
	
	public ElementTemplatePL(MPLExport export){
		this.export = export;
	}	
	
	/**Add the list of identifiers to ownedMemberIDList for exporting of the basic attributes later
	 * @param list
	 */
	public void addToOwnedMemberIDList(ArrayList<String> list){
		if(!list.isEmpty()){
			for(String s:list){
				if(!this.ownedMemberIDList.contains(s)){
					this.ownedMemberIDList.add(s);
				}
			}
		}
	}
	
	/**Translate all the common basic attributes such as name, uri, timecreated and timemodified.
	 * We also generate list of identifiers for the comments, changelist, relatedBy and relatedTo, 
	 * however the implementation of each of those will be handled outside of the scope of this element
	 * 
	 * @param sb			StringBuffer containing the prolog statements 
	 * @param first			Boolean variable to indicate if these basic attributes are the first to be appended, thus comma is not required
	 * @param element		Element to be translated to prolog
	 */
	public void appendBasicAttributes(StringBuffer sb, boolean first, Element element) {
		
		//start with finding out the child members, eg comments, changeID, relationships and add them to the list
		ArrayList<String> commentIDList = new ArrayList<String>();		
		if(element.getCommentlist()!=null && element.getCommentlist().getComments()!=null && 
				element.getCommentlist().getComments().size()!=0){

			for(Comment comment: element.getCommentlist().getComments()){
				if(export.getPrologID(comment.getUniqueID())!=""){
					commentIDList.add(export.getPrologID(comment.getUniqueID()));
					addToOwnedMemberIDList(commentIDList);
				}
			}
		}
		
		ArrayList<String> changeIDList = new ArrayList<String>();
		if(element.getChangeList()!=null && element.getChangeList().getComments()!=null && 
				element.getChangeList().getComments().size()!=0){

			for(Comment comment: element.getChangeList().getComments()){
				if(export.getPrologID(comment.getUniqueID())!=""){
					changeIDList.add(export.getPrologID(comment.getUniqueID()));
					addToOwnedMemberIDList(changeIDList);
				}
			}
		}		
		
		ArrayList<String> relationshipSourceList = new ArrayList<String>();
		if(element.getRelatedBy()!=null && !element.getRelatedBy().isEmpty()){
			for(ElementRelationship relation: element.getRelatedBy()){
				if(export.getPrologID(relation.getUniqueID())!=""){
					relationshipSourceList.add(export.getPrologID(relation.getUniqueID()));
					addToOwnedMemberIDList(relationshipSourceList);					
				}
			}
		}	
		
		ArrayList<String> relationshipTargetList = new ArrayList<String>();
		if(element.getRelatesTo()!=null && !element.getRelatesTo().isEmpty()){
			for(ElementRelationship relation: element.getRelatesTo()){
				if(export.getPrologID(relation.getUniqueID())!=""){
					relationshipTargetList.add(export.getPrologID(relation.getUniqueID()));
					addToOwnedMemberIDList(relationshipTargetList);	
				}
			}
		}
		
		//Start exporting the elements
		
		if(first){
			MPLAppender.appendAttribute(sb,true,"name",element.getName());
			MPLAppender.appendAttribute(sb,false,"label",element.getLabel());			
			MPLAppender.appendAttribute(sb,false,"kind",element.getElementKind());
//			if(element.getPartOf()!=null){
//				MPLAppender.appendAttribute(sb,false,"partOf",element.getPartOf().getName());
//			}
			MPLAppender.appendAttribute(sb,false,"description",element.getDescription());
			
		}
		else{
			MPLAppender.appendAttribute(sb,true,"name",element.getName());
			MPLAppender.appendAttribute(sb,false,"label",element.getLabel());			
			MPLAppender.appendAttribute(sb,false,"kind",element.getElementKind());
//			if(element.getPartOf()!=null){
//				MPLAppender.appendAttribute(sb,false,"partOf",element.getPartOf().getName());
//			}
			MPLAppender.appendAttribute(sb,false,"description",element.getDescription());			
		}
		MPLAppender.appendAttribute(sb,"uri",element.getUri());
		if(element.getTimeCreated()!=null && !element.getTimeCreated().toString().isEmpty()){
			MPLAppender.appendAttribute(sb,"timeCreated",element.getTimeCreated().toString());
		}
		if(element.getLastModified()!=null && !element.getLastModified().toString().isEmpty()){
			MPLAppender.appendAttribute(sb,"lastModified",element.getLastModified().toString());
		}
				
		
		if(!ownedMemberIDList.isEmpty()){
			MPLAppender.appendIDArrays(sb,"ownedMember",ownedMemberIDList);
		}		
		if(!commentIDList.isEmpty()){
			MPLAppender.appendIDArrays(sb,"comments",commentIDList);
		}
		if(!changeIDList.isEmpty()){
			MPLAppender.appendIDArrays(sb,"changeList",changeIDList);
		}
		if(!relationshipSourceList.isEmpty()){
			MPLAppender.appendIDArrays(sb,"relSrc",relationshipSourceList);
		}
		if(!relationshipTargetList.isEmpty()){
			MPLAppender.appendIDArrays(sb,"relTgt",relationshipTargetList);
		}
		
		//garbage collection
		ownedMemberIDList.clear();
		ownedMemberIDList=null;
		commentIDList.clear();
		commentIDList=null;
		changeIDList.clear();
		changeIDList=null;
		relationshipSourceList.clear();
		relationshipSourceList=null;
		relationshipTargetList.clear();
		relationshipTargetList=null;
		
	}
	
}
