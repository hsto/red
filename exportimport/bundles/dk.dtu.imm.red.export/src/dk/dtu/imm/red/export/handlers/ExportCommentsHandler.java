package dk.dtu.imm.red.export.handlers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.comment.CommentExporter;
import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class ExportCommentsHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		FileDialog fileDialog = 
				new FileDialog(Display.getCurrent().getActiveShell(), SWT.SAVE);
		
		fileDialog.setFilterExtensions(new String[] {".csv"});
		
		String filename = fileDialog.open();
		
		if (filename == null) {
			return null;
		}
		
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		
		List<EObject> selectedElements = new ArrayList<EObject>();
		
		if (selection != null && selection instanceof IStructuredSelection) {
			Iterator<EObject> it = ((IStructuredSelection) selection).iterator();
			
			while (it.hasNext()) {
				selectedElements.add(it.next());
			}
		} else {
			selectedElements.add(WorkspaceFactory.eINSTANCE.getWorkspaceInstance());
		}
		
		selectedElements = EcoreUtil.filterDescendants(selectedElements);
		
		StringBuffer output = new StringBuffer();
		
		
		output.append("object,location_type,element_type,element_name,element_number,"
				+ "line_number,path,diagram,content,remark_level,s-number\n"
				);
		
		for (EObject object : selectedElements) {
			if (object instanceof Element) {
				Element element = (Element) object;
				
				CommentExporter exporter = CommentFactory.eINSTANCE.createCommentExporter();
				//go through each of the element and call the acceptVisitor
				//in the case if it is GroupImpl, the acceptVisitor will call the children's acceptVisitor
				element.acceptVisitor(exporter);
				//in the element's acceptVisitor, the visitor(in this case CommentExporterImpl) visit()
				//method will automatically be called
				
				output.append(exporter.getOutput());
			}
		}
		
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}
			
			FileWriter fw = new FileWriter(file);
			fw.append(output.toString());
			fw.flush();
			fw.close();
		} catch (IOException e) {
			MessageDialog.openError(null, "Error while exporting comments.", 
					"An error occured while exporting the comments. \n" +
					"The error message is: " + e.getMessage());
		}
		
		return null;
	}

}
