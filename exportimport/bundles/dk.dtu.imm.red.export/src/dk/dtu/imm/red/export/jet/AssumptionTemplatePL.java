package dk.dtu.imm.red.export.jet;

import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;
import dk.dtu.imm.red.specificationelements.assumption.Assumption;

/**Class which translate an Assumption specification element into prolog statements.
 * 
 * @author HenryLie
 *
 */
public class AssumptionTemplatePL extends ElementTemplatePL{
	protected static String nl = System.getProperties().getProperty("line.separator");	

	protected MPLExport export;
	
	public AssumptionTemplatePL(MPLExport export){
		super(export);
		this.export = export;
	}
	
	/**Translate the assumption element into a String containing the prolog statements
	 * 
	 * @param assumption	Assumption specification element to be translated
	 * @return				The output translated string containing the prolog statements
	 */
	public String generate(Assumption assumption) {
		final StringBuffer sb = new StringBuffer();
		MPLAppender.declareMetaClass(sb, "assumption", export.getPrologID(assumption.getUniqueID()));
		appendBasicAttributes(sb,true,assumption);
		
		MPLAppender.closeMetaClass(sb);
		return sb.toString();
	}
	

}
