package dk.dtu.imm.red.export.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.export.wizards.ExportToMPLWizard;
import dk.dtu.imm.red.export.wizards.ExportToMPLWizardImpl;

public class ExportToMPLHandler extends AbstractHandler{
	
	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		
		ExportToMPLWizardImpl wizard = new ExportToMPLWizardImpl((IStructuredSelection) currentSelection);

		WizardDialog dialog = new WizardDialog(
				HandlerUtil.getActiveShell(event), wizard);
		dialog.create();
		
		dialog.open();
		
		return null;
	}
}
