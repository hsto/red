package dk.dtu.imm.red.export.wizards;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.wizards.BaseWizard;
import dk.dtu.imm.red.core.ui.wizards.ChooseContainerPage;
import dk.dtu.imm.red.core.ui.wizards.ChooseElementLocationPage;
import dk.dtu.imm.red.export.MPLExport;

public class ExportToMPLWizardImpl extends BaseWizard implements
		ExportToMPLWizard {

	protected ExportToMPLWizardPresenter presenter;
	protected ChooseContainerPage chooseContainerPage;
	protected ChooseElementLocationPage chooseElementLocationPage;

	public ExportToMPLWizardImpl(IStructuredSelection selection) {
		presenter = new ExportToMPLWizardPresenterImpl(this);
		chooseContainerPage = new ChooseContainerPage("Export to Prolog MPL");
		chooseElementLocationPage = new ChooseElementLocationPage("Report","pl");
	}

	@Override
	public void addPages() {
		addPage(chooseContainerPage);
		addPage(chooseElementLocationPage);
		super.addPages();
	}

	@Override
	public boolean performFinish() {

		final String fileName = chooseElementLocationPage.getFileName();
		final String path = fileName.substring(0,
				fileName.lastIndexOf(File.separatorChar));

		IStructuredSelection selection = (IStructuredSelection) chooseContainerPage
				.getSelection();

		if (selection.getFirstElement() instanceof Group == false) {
			return false;
		}

		final Group parent = (Group) selection.getFirstElement();

		ProgressMonitorDialog dialog = new ProgressMonitorDialog(Display
				.getDefault().getActiveShell());
		try {
			dialog.run(false, false, new IRunnableWithProgress() {
				@Override
				public void run(IProgressMonitor monitor)
						throws InvocationTargetException, InterruptedException {
					try {
						MPLExport export = new MPLExport();
						monitor.beginTask("Generating prolog file",
								IProgressMonitor.UNKNOWN);
						FileWriter output = new FileWriter(fileName);
						BufferedWriter writer = new BufferedWriter(output);
						String exportString = export.exportFile(parent, path);
						writer.write(exportString);
						writer.close();

					} catch (IOException e) {
						e.printStackTrace(); LogUtil.logError(e);
					} finally {
						monitor.done();
					}
				}
			});
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
		return true;
	}

	@Override
	public boolean canFinish() {
		return chooseContainerPage.isPageComplete()
				&& chooseElementLocationPage.isPageComplete();
	}

}
