package dk.dtu.imm.red.export.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface ExportToMPLWizard extends IBaseWizard{
	
	String ID = "dk.dtu.imm.red.export.exporttomplwizard";

}

