package dk.dtu.imm.red.export.jet;

import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;
import dk.dtu.imm.red.specificationelements.stakeholder.Stakeholder;

/**Class which translate a Stakeholder specification element into prolog statements.
 * @author HenryLie
 *
 */
public class StakeholderTemplatePL extends ElementTemplatePL{
	protected static String nl = System.getProperties().getProperty("line.separator");	

	protected MPLExport export;
	
	public StakeholderTemplatePL(MPLExport export){
		super(export);
		this.export = export;
	}
	
	/**Translate the stakeholder element into a String containing the prolog statements
	 * 
	 * @param stakeholder	Stakeholder specification element to be translated
	 * @return				The output translated string containing the prolog statements
	 */	
	public String generate(Stakeholder stakeholder) {
		final StringBuffer sb = new StringBuffer();
		MPLAppender.declareMetaClass(sb, "stakeholder", export.getPrologID(stakeholder.getUniqueID()));	
		appendBasicAttributes(sb,true,stakeholder);
		MPLAppender.appendAttribute(sb,"exposure",stakeholder.getExposure()+"");
		MPLAppender.appendAttribute(sb,"power",stakeholder.getPower()+"");
		MPLAppender.appendAttribute(sb,"urgency",stakeholder.getUrgency()+"");
		MPLAppender.appendAttribute(sb,"importance",stakeholder.getImportance()+"");
		if(stakeholder.getLongDescription()!=null && !stakeholder.getLongDescription().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb,"longDescription",stakeholder.getLongDescription().toPlainString());
		}
		if(stakeholder.getStake()!=null && !stakeholder.getStake().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb,"stake",stakeholder.getStake().toPlainString());
		}
		if(stakeholder.getEngagement()!=null && !stakeholder.getEngagement().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb,"engagement",stakeholder.getEngagement().toPlainString());
		}
		MPLAppender.closeMetaClass(sb);
		return sb.toString();
	}
}
