package dk.dtu.imm.red.export.jet;

import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;
import dk.dtu.imm.red.specificationelements.vision.Vision;

/**Class which translate a Vision specification element into prolog statements.
 * @author HenryLie
 *
 */
public class VisionTemplatePL extends ElementTemplatePL{
	protected static String nl = System.getProperties().getProperty("line.separator");	

	protected MPLExport export;
	
	public VisionTemplatePL(MPLExport export){
		super(export);
		this.export = export;
	}
	
	/**Translate the storyboard element into a String containing the prolog statements
	 * 
	 * @param vision		Vision specification element to be translated
	 * @return				The output translated string containing the prolog statements
	 */	
	public String generate(Vision vision) {
		final StringBuffer sb = new StringBuffer();
		MPLAppender.declareMetaClass(sb, "vision", export.getPrologID(vision.getUniqueID()));
		appendBasicAttributes(sb,true,vision);
		if(vision.getVision()!=null && !vision.getVision().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb,"visionText",vision.getVision().toPlainString());
		}
		MPLAppender.closeMetaClass(sb);
		return sb.toString();
	}
	

}
