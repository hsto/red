package dk.dtu.imm.red.export.jet;

import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.export.MPLAppender;
import dk.dtu.imm.red.export.MPLExport;

/**Class which translate a Document specification element into prolog statements.
 * @author HenryLie
 *
 */
public class DocumentTemplatePL extends ElementTemplatePL{
	protected static String nl = System.getProperties().getProperty("line.separator");	

	protected MPLExport export;
	
	public DocumentTemplatePL(MPLExport export){
		super(export);
		this.export = export;
	}
	
	/**Translate the document element into a String containing the prolog statements
	 * 
	 * @param document		Document specification element to be translated
	 * @return				The output translated string containing the prolog statements
	 */	
	public String generate(Document document) {
		final StringBuffer sb = new StringBuffer();
		MPLAppender.declareMetaClass(sb, "document", export.getPrologID(document.getUniqueID()));
		super.appendBasicAttributes(sb,true,document);
//		if(document.getDocument()!=null && !document.getDocument().toPlainString().isEmpty()){
//			MPLAppender.appendAttribute(sb,"documentText",document.getDocument().toPlainString());
//		}
//		MPLAppender.appendAttribute(sb,"name",document.getName());
		if(document.getInternalRef()!=null && !document.getInternalRef().getName().isEmpty()){
			MPLAppender.appendAttribute(sb,"internalReference",document.getInternalRef().getName());
		}
		MPLAppender.appendAttribute(sb,"externalFileReference",document.getExternalFileRef());
		MPLAppender.appendAttribute(sb,"webReference",document.getWebRef());
		if(document.getAbstract()!=null && !document.getAbstract().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb,"abstract",document.getAbstract().toPlainString());
		}
		if(document.getTextContent()!=null && !document.getTextContent().toPlainString().isEmpty()){
			MPLAppender.appendAttribute(sb,"textContent",document.getTextContent().toPlainString());
		}
		MPLAppender.closeMetaClass(sb);
		return sb.toString();
	}
	

}
