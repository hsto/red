package dk.dtu.imm.red.core.element.impl;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.folder.FolderFactory;

public class ElementImplTest {
	private Element element;

	@Before
	public void setUp() throws Exception {
		element = new ElementImpl() {};
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test that when a new element is created, the creation time
	 * is set and correct. We test by creating a folder, which is of
	 * an instance of element (since Element is an abstract class).
	 * @throws InterruptedException 
	 */
	@Test
	public void testNewElementTimeCreated() throws InterruptedException {
		
		Date first = new Date();
		
		Thread.sleep(100);
		
		Element element = FolderFactory.eINSTANCE.createFolder();
		
		Thread.sleep(100);
		
		Date second = new Date();
		
		// Assert that timeCreated is set
		assertNotNull(element.getTimeCreated());
		
		// Assert that the time created is after 'first' and before 'second'
		assertTrue(first.before(element.getTimeCreated()));
		assertTrue(second.after(element.getTimeCreated()));
	}
	
	@Test
	public void testOldIssuesDeleted() {
		element.checkStructure();
		int firstNumberOfComments = element.getCommentlist().getComments().size();

		element.checkStructure();
		int secondNumberOfComments = element.getCommentlist().getComments().size();
		
		assertEquals(firstNumberOfComments, secondNumberOfComments);
	}
	
	@Test
	public void testNoLabelNoNameCheck() {
		element.setLabel("SomeLabel");
		element.setName("Some name");
		
		element.checkStructure();
		int numberOfCommentsWithNoIssue = element.getCommentlist().getComments().size();
		
		element.setLabel(null);
		element.setName("");
		element.checkStructure();
		int numberOfCommentsWithIssue = element.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithNoIssue + 1, numberOfCommentsWithIssue);
	}
	
	@Test
	public void testNoLabelCheck() {
		element.setLabel("SomeLabel");
		element.setName("Some name");
		
		element.checkStructure();
		int numberOfCommentsWithNoIssue = element.getCommentlist().getComments().size();
		
		element.setLabel("");
		element.checkStructure();
		int numberOfCommentsWithIssue = element.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithNoIssue + 1, numberOfCommentsWithIssue);
	}
	
	@Test
	public void testNoNameCheck() {
		element.setLabel("SomeLabel");
		element.setName("Some name");
		
		element.checkStructure();
		int numberOfCommentsWithNoIssue = element.getCommentlist().getComments().size();
		
		element.setName(null);
		element.checkStructure();
		int numberOfCommentsWithIssue = element.getCommentlist().getComments().size();
		
		assertEquals(numberOfCommentsWithNoIssue + 1, numberOfCommentsWithIssue);
	}
}
