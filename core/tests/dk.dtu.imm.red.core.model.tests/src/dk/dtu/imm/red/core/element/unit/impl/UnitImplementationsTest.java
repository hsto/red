package dk.dtu.imm.red.core.element.unit.impl;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import dk.dtu.imm.red.core.element.unit.Unit;
import dk.dtu.imm.red.core.element.unit.UnitFactory;



public class UnitImplementationsTest {

	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testUnitUniqueness() {
		Unit unit1 = TimeUnitImpl.SECOND();
		Unit unit2 = TimeUnitImpl.SECOND();
		
		assertThat(unit1, not(sameInstance(unit2)));
	}

	@Test
	public void testUnitCompatibility() {
		Unit timeUnit1 = TimeUnitImpl.SECOND();
		Unit timeUnit2 = TimeUnitImpl.WEEK();
		Unit baseUnit = UnitFactory.eINSTANCE.createBaseUnit();
		
		assertTrue(timeUnit1.isCompatibleWith(timeUnit2));
		assertFalse(timeUnit1.isCompatibleWith(baseUnit));
	}

	@Test
	public void testDivisionalUnitCompatibility() {
		Unit sizeTimeUnit1 = UnitFactory.eINSTANCE.createDivisionalUnit(
				SizeUnitImpl.KILOBYTE(), TimeUnitImpl.MILLISECOND());
		Unit sizeTimeUnit2 = UnitFactory.eINSTANCE.createDivisionalUnit(
				SizeUnitImpl.GIGABYTE(), TimeUnitImpl.DAY());
		Unit baseTimeUnit = UnitFactory.eINSTANCE.createDivisionalUnit(
				UnitFactory.eINSTANCE.createBaseUnit(), TimeUnitImpl.MILLISECOND());
		
		assertTrue(sizeTimeUnit1.isCompatibleWith(sizeTimeUnit2));
		assertFalse(sizeTimeUnit1.isCompatibleWith(baseTimeUnit));
	}
	
	@Test
	public void testDivisionalConversionFactor() {
		Unit nominatorUnit = UnitFactory.eINSTANCE.createBaseUnit();
		nominatorUnit.setSymbol("km");
		nominatorUnit.setConversionFactor(new BigDecimal(1000));
		Unit denominatorUnit = UnitFactory.eINSTANCE.createBaseUnit();
		denominatorUnit.setSymbol("h");
		denominatorUnit.setConversionFactor(new BigDecimal(3600));
		BigDecimal expectedResult = new BigDecimal("0.2778");
		
		Unit resultUnit = UnitFactory.eINSTANCE.createDivisionalUnit(nominatorUnit, denominatorUnit);
		
		assertEquals(expectedResult.doubleValue(), resultUnit.getConversionFactor().doubleValue(), 0.0001);
	}
	
	@Test
	public void testConversion() {
		Unit inputUnit = TimeUnitImpl.WEEK();
		Unit outputUnit = TimeUnitImpl.DAY();
		BigDecimal inputValue = new BigDecimal(3);
		BigDecimal expectedResult = new BigDecimal(21);
		
		BigDecimal result = inputUnit.convertTo(inputValue, outputUnit);
		
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void testConversionVerySmallValue() {
		Unit inputUnit = SizeUnitImpl.TERABYTE();
		Unit outputUnit = SizeUnitImpl.KILOBYTE();
		BigDecimal inputValue = new BigDecimal(0.000000005);
		BigDecimal expectedResult = new BigDecimal(5);
		
		BigDecimal result = inputUnit.convertTo(inputValue, outputUnit);
		
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void testConversionVeryLargeValue() {
		Unit inputUnit = SizeUnitImpl.KILOBYTE();
		Unit outputUnit = SizeUnitImpl.TERABYTE();
		BigDecimal inputValue = new BigDecimal("5000002150");
		BigDecimal expectedResult = new BigDecimal("5.00000215");
		
		BigDecimal result = inputUnit.convertTo(inputValue, outputUnit);
		
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void testConversionDivisional() {
		Unit inputUnit = UnitFactory.eINSTANCE.createDivisionalUnit(SizeUnitImpl.KILOBYTE(), TimeUnitImpl.MILLISECOND());
		Unit outputUnit = UnitFactory.eINSTANCE.createDivisionalUnit(SizeUnitImpl.TERABYTE(), TimeUnitImpl.DAY());
		BigDecimal inputValue = new BigDecimal("10");
		BigDecimal expectedResult = new BigDecimal("0.864");
		
		BigDecimal result = inputUnit.convertTo(inputValue, outputUnit);
		
		assertEquals(expectedResult, result);
	}
}


