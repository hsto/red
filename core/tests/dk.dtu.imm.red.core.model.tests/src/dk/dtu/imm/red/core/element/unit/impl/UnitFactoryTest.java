package dk.dtu.imm.red.core.element.unit.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.unit.DivisionalUnit;
import dk.dtu.imm.red.core.element.unit.Unit;
import dk.dtu.imm.red.core.element.unit.UnitFactory;



public class UnitFactoryTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDivisionalUnitInitialization() {
		Unit nominator = SizeUnitImpl.KILOBYTE();
		Unit denominator = TimeUnitImpl.DAY();
		
		DivisionalUnit unit = UnitFactory.eINSTANCE.createDivisionalUnit(nominator, denominator);
		
		assertEquals(nominator, unit.getNominatorUnit());
		assertEquals(denominator, unit.getDenominatorUnit());
	}
}


