package dk.dtu.imm.red.core.element.relationship;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RelationshipKindTest {

	// class DummyElement extends ElementImpl { }

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAsymmetricRelationshipKind() {

		/*
		 * Check if active relationship kinds have even values their reverse
		 * have these values +1
		 */

		for (RelationshipKind kind : RelationshipKind.values()) {
			switch (kind) {
			case FEATURES_IN:
				;

			case CONTRIBUTES_COST_TO:
				;

			case CONTRIBUTES_PERFORMANCE_TO:
				;

			case CONTRIBUTES_RISK_TO:
				;

			case SUPPORTS:
				;

			case ELABORATES:
				;

			case EXTENDS:
				;

			case ILLUSTRATES:
				;

			case INCLUDES:
				;

			case DERIVES_TO:
				;

			case IS_KIND_OF:
				;

			case IS_PART_OF:
				;

			case LEADS_TO:
				;

			case JUSTIFIES:
				;

			case PRECEDES:
				;

			case REFERS_TO:
				;

			case REFINES_TO:
				assertEquals("The value of active kinds is odd", kind.getValue() % 2, 1);
				assertEquals("The value of a passive kind is +1 to its active oposite",
						RelationshipKind.getReverseRelationshipKind(kind).getValue(), kind.getValue() + 1);
				break;
			default:
				;
			}
		}
	}

	@Test
	public void testGetActiveKinds() {
		Set<RelationshipKind> set = RelationshipKind.getActiveKinds();

		for (RelationshipKind kind : RelationshipKind.values()) {
			switch (kind) {
			case FEATURES_IN:
				;

			case CONTRIBUTES_COST_TO:
				;

			case CONTRIBUTES_PERFORMANCE_TO:
				;

			case CONTRIBUTES_RISK_TO:
				;

			case SUPPORTS:
				;

			case ELABORATES:
				;

			case EXTENDS:
				;

			case ILLUSTRATES:
				;

			case INCLUDES:
				;

			case DERIVES_TO:
				;

			case IS_KIND_OF:
				;

			case IS_PART_OF:
				;

			case LEADS_TO:
				;

			case JUSTIFIES:
				;

			case PRECEDES:
				;

			case REFERS_TO:
				;

			case REFINES_TO:
				assertTrue("Active is included in ActiveKinds", set.contains(kind));
				break;

			default:
				assertFalse("Passive and asymmetrical are not included in ActiveKinds", set.contains(kind));
				;
			}
		}
	}
}
