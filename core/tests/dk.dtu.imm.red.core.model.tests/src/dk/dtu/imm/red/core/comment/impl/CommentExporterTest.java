package dk.dtu.imm.red.core.comment.impl;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentCategory;
import dk.dtu.imm.red.core.comment.CommentExporter;
import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.user.User;
import dk.dtu.imm.red.core.user.UserFactory;

public class CommentExporterTest {
	
	private Element element1;
	private Element element2;
	private Comment comment1;
	private Comment comment2;
	private Comment comment3;
	
	private Folder folder;
	
	private User user1;
	private User user2;
	private User user3;
	
	private Date date;
	
	private CommentExporter exporter;

	private class DummyElement extends ElementImpl {
		
	}

	@Before
	public void setUp() throws Exception {
		date = new Date();
		
		exporter = CommentFactory.eINSTANCE.createCommentExporter();
		
		element1 = new DummyElement();
		element1.setCommentlist(CommentFactory.eINSTANCE.createCommentList());
		element1.setName("element1");
		
		element2 = new DummyElement();
		element2.setCommentlist(CommentFactory.eINSTANCE.createCommentList());
		element2.setName("element2");
		
		folder = FolderFactory.eINSTANCE.createFolder();
		folder.setCommentlist(CommentFactory.eINSTANCE.createCommentList());
		folder.setName("folder1");
		
		user1 = UserFactory.eINSTANCE.createUser();
		user1.setName("user1");
		
		user2 = UserFactory.eINSTANCE.createUser();
		user2.setName("user2");
		
		user3 = UserFactory.eINSTANCE.createUser();
		user3.setName("user3");
		
		comment1 = CommentFactory.eINSTANCE.createComment();
		comment1.setAuthor(user1);
		comment1.setCategory(CommentCategory.UNRESOLVED);
		comment1.setText(TextFactory.eINSTANCE.createText("comment1"));
		comment1.setTimeCreated(date);
		
		comment2 = CommentFactory.eINSTANCE.createComment();
		comment2.setAuthor(user2);
		comment2.setCategory(CommentCategory.UNRESOLVED);
		comment2.setText(TextFactory.eINSTANCE.createText("comment2"));
		comment2.setTimeCreated(date);
		
		comment3 = CommentFactory.eINSTANCE.createComment();
		comment3.setAuthor(user3);
		comment3.setCategory(CommentCategory.UNRESOLVED);
		comment3.setText(TextFactory.eINSTANCE.createText("comment3"));
		comment3.setTimeCreated(date);
		
	}

	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * The format for comment export is
	 * elementID,time,elementName,path,commentAuthor,commentText,commentType
	 * 
	 * @see CommentExporterImpl#visit(Element)
	 * @author Maciej Kucharek
	 */
	private void testOutput(Element rootElement, String output) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
		// split the output into lines, each containing a comment
		String[] lines = output.split("\n");
		Map<String, List<String>> commentMap = new HashMap<String, List<String>>();
		
		// put the comments into a map, indexed by the element they are contained
		// by
		for (String l : lines) {
			// get the ID of the comment's element
			String id = l.substring(0, l.indexOf(","));
			
			// if the map entry is not initialized with a list, do so
			if (!commentMap.containsKey(id)) {
				commentMap.put(id, new ArrayList<String>());
			}
			
			// store the comment in the list
			commentMap.get(id).add(l);
		}
		
		// now go through every element
		Folder folder = FolderFactory.eINSTANCE.createFolder();
		folder.getContents().add(rootElement);
		TreeIterator<EObject> iterator = folder.eAllContents();
		while (iterator.hasNext()) {
			EObject object = iterator.next();
			if (object instanceof Element == false) {
				continue;
			}
			Element element = (Element) object;
			
			if (element.getCommentlist() != null && element.getCommentlist().getComments().size() > 0) {
				List<String> elementComments = commentMap.get(element.getUniqueID());
				assertEquals(element.getCommentlist().getComments().size(), 
						elementComments.size());
				// go through every comment in the list
				for (int i = 0; i < element.getCommentlist().getComments().size(); i++) {
					Comment comment = element.getCommentlist().getComments().get(i);
					String exportedComment = elementComments.get(i);
					
					// TODO: the hardcoded separator is ',' - what if a user puts it in any of the fields (like commentText) ?
					
					// splitting the string using hardcoded separator
					String[] fields = exportedComment.split(",");
					
					// making sure we obtain the expected number of arguments (again, it won't work with comments containing ',')
					Assert.assertEquals(7, fields.length);
					
					assertEquals(element.getUniqueID(), fields[0]);
					assertEquals(sdf.format(comment.getTimeCreated()), fields[1]);
					assertEquals("\"" + element.getName() + "\"", fields[2]);
					// fields[3] is a path - cannot test it here
					assertEquals("\"" + comment.getAuthor().getName() + "\"", fields[4]);
					assertEquals("\"" + comment.getText().toString() + "\"", fields[5]);
					assertEquals(comment.getCategory().getLiteral(), fields[6]);
					
				}
			}
		}
	}

	@Ignore("Requires maintenance") @Test
	public void testSingleElement() {
		
		element1.getCommentlist().getComments().add(comment1);
		
		element1.acceptVisitor(exporter);
		String output = exporter.getOutput();
		
		testOutput(element1, output);
	}
	
	@Ignore("Requires maintenance") @Test
	public void testSingleElementTwoComments() {
		
		element1.getCommentlist().getComments().add(comment1);
		element1.getCommentlist().getComments().add(comment2);
		
		element1.acceptVisitor(exporter);
		String output = exporter.getOutput();
		
		testOutput(element1, output);
	}
	
	@Ignore("Requires maintenance") @Test
	public void testMultipleElementsMultipleComments() {
		folder.getContents().add(element1);
		folder.getContents().add(element2);
		
		folder.getCommentlist().getComments().add(comment1);
		element1.getCommentlist().getComments().add(comment2);
		element2.getCommentlist().getComments().add(comment3);
		
		folder.acceptVisitor(exporter);
		String output = exporter.getOutput();
		
		testOutput(folder, output);
	}

}
