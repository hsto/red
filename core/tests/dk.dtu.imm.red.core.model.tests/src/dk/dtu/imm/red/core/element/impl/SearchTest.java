package dk.dtu.imm.red.core.element.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.group.impl.GroupImpl;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.element.impl.SearchParameterImpl;

public class SearchTest {
	
	private Element element1;
	private Element element2;
	private SearchParameter param;
	private Group groupA;
	private Group groupB;
	private Group groupC;
	private Group groupD;

	class DummyElement extends ElementImpl { }
	class DummyGroup extends GroupImpl { }

	@Before
	public void setUp() throws Exception {
		element1 = new DummyElement();
		element1.setName("testname");
		
		element2 = new DummyElement();
		element2.setName("testname2");
		
		groupA = new DummyGroup();
		groupA.setName("testgroupA");
		
		groupB = new DummyGroup();
		groupB.setName("testgroupB");
		
		groupC = new DummyGroup();
		groupC.setName("testgroupC");
		
		groupD = new DummyGroup();
		groupD.setName("testgroupD");
		
		param = new SearchParameterImpl();
		param.setSearchString("testname");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSimpleNameSearch() {
		
		SearchResultWrapper wrap = element1.search(param);
		
		assertNotNull(wrap);
		assertEquals(element1, wrap.getElement());
		assertEquals(1, wrap.getElementResults().size());
		SearchResult result = wrap.getElementResults().get(0);
		assertEquals(ElementPackage.Literals.ELEMENT__NAME, result.getAttribute());
	}
	
	@Test
	public void testNameSearchGroup() {
		groupA.getContents().add(element1);
		
		SearchResultWrapper wrap = groupA.search(param);
		
		assertNotNull(wrap);
		assertEquals(groupA, wrap.getElement());
		assertEquals(0, wrap.getElementResults().size());
		assertEquals(1, wrap.getChildResults().size());
		
		SearchResultWrapper childWrap = wrap.getChildResults().get(0);
		assertEquals(element1, childWrap.getElement());
		assertEquals(1, childWrap.getElementResults().size());
		
		SearchResult result = childWrap.getElementResults().get(0);
		assertEquals(ElementPackage.Literals.ELEMENT__NAME, result.getAttribute());
	}
	
	@Test
	public void testComplexGroupStructureSearch() {
		
		groupA.getContents().add(groupB);
		groupA.getContents().add(groupC);
		groupB.getContents().add(groupD);
		
		groupC.setName("testname");
		groupD.setName("testname");
		
		SearchResultWrapper wrap = groupA.search(param);
		
		assertEquals(groupA, wrap.getElement());
		assertEquals(0, wrap.getElementResults().size());
		assertEquals(2, wrap.getChildResults().size());
		
		SearchResultWrapper cwrap1 = wrap.getChildResults().get(0);
		SearchResultWrapper cwrap2 = wrap.getChildResults().get(1);
		
		assertEquals(groupB, cwrap1.getElement());
		assertTrue(cwrap1.getElementResults().isEmpty());
		assertFalse(cwrap1.getChildResults().isEmpty());
		
		SearchResultWrapper ccwrap1 = cwrap1.getChildResults().get(0);
		assertEquals(groupD, ccwrap1.getElement());
		
		
		assertEquals(groupC, cwrap2.getElement());
		assertFalse(cwrap2.getElementResults().isEmpty());
		
	}
	
	@Test
	public void testComplexGroupStructureSearch2() {
		
		groupA.getContents().add(groupB);
		groupA.getContents().add(groupC);
		groupB.getContents().add(groupD);
		
		groupD.setName("testname");
		
		SearchResultWrapper wrap = groupA.search(param);
		
		assertEquals(groupA, wrap.getElement());
		assertEquals(0, wrap.getElementResults().size());
		assertEquals(1, wrap.getChildResults().size());
		
		SearchResultWrapper cwrap1 = wrap.getChildResults().get(0);
		
		assertEquals(groupB, cwrap1.getElement());
		assertTrue(cwrap1.getElementResults().isEmpty());
		assertFalse(cwrap1.getChildResults().isEmpty());
		
		SearchResultWrapper ccwrap1 = cwrap1.getChildResults().get(0);
		assertEquals(groupD, ccwrap1.getElement());
	}

}
