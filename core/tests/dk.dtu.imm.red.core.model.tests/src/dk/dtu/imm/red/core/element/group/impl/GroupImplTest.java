package dk.dtu.imm.red.core.element.group.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.impl.GroupImpl;
import dk.dtu.imm.red.core.element.impl.ElementImpl;

public class GroupImplTest {

	/**
	 * A stub group class which can be used to test the abstract class
	 * GroupImpl.
	 */
	private class StubGroup extends GroupImpl {
	}

	/**
	 * A stub element class which can be used as replacement for ElementImpl.
	 */
	private class StubElement extends ElementImpl {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test that the findDescendantByUniqueID works when a group has the
	 * searched for element as direct descendant.
	 */
	@Test
	public void testFindDescendantDirectChild() {
		StubGroup group = new StubGroup();

		StubElement element1 = new StubElement();
		String uuid = element1.getUniqueID();
		assertFalse(uuid.isEmpty());

		group.getContents().add(element1);

		Element foundElement = group.findDescendantByUniqueID(uuid);
		assertEquals(element1, foundElement);
	}

	/**
	 * Test that the findDescendantByUniqueID works when a group does not have
	 * the searched for element as descendant.
	 */
	@Test
	public void testFindDescendantNoDirectChild() {
		StubGroup group = new StubGroup();

		StubElement element1 = new StubElement();
		String uuid = element1.getUniqueID();
		assertFalse(uuid.isEmpty());

		group.getContents().add(element1);

		Element foundElement = group.findDescendantByUniqueID("abc");
		assertNull(foundElement);
	}

	/**
	 * Test that the findDescendantByUniqueID works when a group has the
	 * searched for element as the grandchild
	 */
	@Test
	public void testFindDescendantGrandChild() {
		StubGroup root = new StubGroup();
		StubGroup group = new StubGroup();

		root.getContents().add(group);

		StubElement element1 = new StubElement();
		String uuid = element1.getUniqueID();
		assertFalse(uuid.isEmpty());

		group.getContents().add(element1);

		Element foundElement = group.findDescendantByUniqueID(uuid);
		assertEquals(element1, foundElement);
	}

}
