package dk.dtu.imm.red.core.element.impl;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.unit.impl.TimeUnitImpl;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.QuantityItem;
import dk.dtu.imm.red.core.element.QuantityLabel;
import dk.dtu.imm.red.core.element.unit.BaseUnit;
import dk.dtu.imm.red.core.element.unit.TimeUnit;


public class QuantityItemTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testQuantityItemInitialization() {
		QuantityItem quantityItem = ElementFactory.eINSTANCE.createQuantityItem();
		
		assertNotNull(quantityItem.getLabel());
		assertEquals(BigDecimal.ZERO, quantityItem.getValue());
		assertNotNull(quantityItem.getUnit());
	}

	@Test
	public void testQuantityItemSetLabel() {
		QuantityItem quantityItem = ElementFactory.eINSTANCE.createQuantityItem();
		
		quantityItem.setLabel(QuantityLabel.CAPACITY);
		
		assertEquals(QuantityLabel.CAPACITY, quantityItem.getLabel());
		assertThat(quantityItem.getUnit(), instanceOf(BaseUnit.class));
		
		quantityItem.setLabel(QuantityLabel.THROUGHPUT);
		
		assertEquals(QuantityLabel.THROUGHPUT, quantityItem.getLabel());
		// The unit for capacity may change in a future version, 
		// but at least it shouldn't be in time units
		assertThat(quantityItem.getUnit(), not(instanceOf(BaseUnit.class))); 
	}
	
	@Test
	public void testQuantityItemGetValidUnits() {
		Object[] expectedUnits = TimeUnitImpl.SECOND().getSiblingUnits().toArray();
		
		QuantityItem quantityItem = ElementFactory.eINSTANCE.createQuantityItem();
		
		quantityItem.setLabel(QuantityLabel.DELAY);
		
		assertArrayEquals(expectedUnits, quantityItem.getValidUnits().toArray());
	}
}


