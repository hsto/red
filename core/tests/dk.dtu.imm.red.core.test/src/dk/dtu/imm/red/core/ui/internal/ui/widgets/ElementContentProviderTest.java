package dk.dtu.imm.red.core.ui.internal.ui.widgets;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.ui.widgets.ElementContentProvider;

public class ElementContentProviderTest {

	/**
	 * The content provider under test.
	 */
	private ElementContentProvider contentProvider;
	private Folder root;

	/**
	 * All initialization before a test case is run.
	 * 
	 * @throws Exception
	 *             any exceptions thrown during setup
	 */
	@Before
	public void setUp() throws Exception {
		contentProvider = new ElementContentProvider();
		root = FolderFactory.eINSTANCE.createFolder();
	}

	/**
	 * Cleanup after a test case is run.
	 * 
	 * @throws Exception
	 *             any exceptions thrown during teardown
	 */
	@After
	public void tearDown() throws Exception {
		contentProvider = null;
	}

	/**
	 * Test that if a folder has no children, the content provider provides no
	 * children.
	 */
	@Test
	public void testFolderNoChildrenGetChildren() {
		assertEquals(0, contentProvider.getChildren(root).length);
	}

	/**
	 * Test that if a folder has two children, the content provider provides
	 * those children.
	 */
	@Test
	public void testFolder2ChildrenGetChildren() {
		root.getContents().add(FolderFactory.eINSTANCE.createFolder());
		root.getContents().add(FolderFactory.eINSTANCE.createFolder());

		assertEquals(2, contentProvider.getChildren(root).length);
	}

	/**
	 * Test that if a folder has no parent, no parent is provided by the content
	 * provider.
	 */
	@Test
	public void testFolderRootGetParent() {
		assertEquals(null, contentProvider.getParent(root));
	}

	/**
	 * Test that the parent provided for a subfolder is the root, and that the
	 * subfolder is provided as a child for the root.
	 */
	@Test
	public void testSubFolderGetParent() {
		Folder subFolder = FolderFactory.eINSTANCE.createFolder();
		root.getContents().add(subFolder);

		assertEquals(root, contentProvider.getParent(subFolder));
		assertEquals(subFolder, contentProvider.getChildren(root)[0]);
	}

}
