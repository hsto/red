package dk.dtu.imm.red.core.ui.internal.navigation.implementation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Display;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.ui.internal.navigation.implementation.ElementDragListener;

public class DNDTest {

	private TreeViewer viewer;
	private ElementDragListener dragListener;

	@Before
	public void setUp() throws Exception {
		viewer = new TreeViewer(Display.getCurrent().getActiveShell());
		dragListener = new ElementDragListener(viewer);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFileNotDraggable() {
		assertFalse(dragListener.canDragElement(
				FileFactory.eINSTANCE.createFile()));
	}
	
	@Test
	public void testFolderDraggable() {
		assertTrue(dragListener.canDragElement(
				FolderFactory.eINSTANCE.createFolder()));
	}

}
