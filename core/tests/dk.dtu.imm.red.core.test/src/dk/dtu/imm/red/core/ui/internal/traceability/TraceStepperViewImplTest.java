package dk.dtu.imm.red.core.ui.internal.traceability;

import static org.junit.Assert.*;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.ui.internal.traceability.stepper.views.impl.TraceStepperViewImpl;

public class TraceStepperViewImplTest {

	private TraceStepperViewImpl stepper; 
	
	@Before
	public void setUp() {
		stepper = new TraceStepperViewImpl();
		Display display = Display.getDefault();
		Shell shell = new Shell(display);
		
		stepper.setTest(true);
		stepper.createPartControl(shell);
	}
	
	@Test
	public void testRadioButtons(){
		assertNotNull("RadioElement initialized", stepper.getRadioElement());
		assertNotNull("RadioRelationship initialized", stepper.getRadioRelationship());
	}
	
	@Test
	public void testFields(){
		assertNotNull("FieldElement initialized", stepper.getFieldElement());
		assertNotNull("FieldRelationship initialized", stepper.getFieldRelationship());
	}
	
	@Test
	public void testFocusOnSearchBox(){
		stepper.setFocusOnSearchBox();
	}
	
	@Test
	public void testAddElementToHistory(){
		
		Element element  = new ElementImpl(){};	
		element.setName("Element 1");
		stepper.addElementToHistory(element);
	}
}
