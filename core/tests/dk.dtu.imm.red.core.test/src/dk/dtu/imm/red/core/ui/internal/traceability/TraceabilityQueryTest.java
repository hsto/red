package dk.dtu.imm.red.core.ui.internal.traceability;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.element.relationship.ElementReference;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.element.relationship.impl.ElementReferenceImpl;
import dk.dtu.imm.red.core.ui.internal.traceability.util.Configuration;
import dk.dtu.imm.red.core.ui.internal.traceability.util.TraceabilityQueryEngine;

public class TraceabilityQueryTest {

	TraceabilityQueryEngine engine;
	Element element;

	/**
	 * Stub classes which can be used as replacement for model classes.
	 */
	private class StubElementReference extends ElementReferenceImpl {
	}

	private class UseCase extends ElementImpl {

	}

	private class Assumption extends ElementImpl {

	}

	private class Requirement extends ElementImpl {

	}

	private class Stakeholder extends ElementImpl {

	}

	private class Goal extends ElementImpl {

	}

	@Before
	public void setUp() {
		engine = new TraceabilityQueryEngine();

		// Build test graph
		// Create elements
		element = new UseCase();
		element.setLabel("e1");
		element.setName("Element 1");
		Element element2 = new Assumption();
		element2.setLabel("e2");
		element2.setName("Element 2");
		Element element3 = new Requirement();
		element3.setLabel("e3");
		element3.setName("Element 3");
		Element element4 = new Assumption();
		element4.setLabel("e4");
		element4.setName("Element 4");
		Element element5 = new Requirement();
		element5.setLabel("e5");
		element5.setName("Element 5");
		Element element6 = new Stakeholder();
		element6.setLabel("e6");
		element6.setName("Element 6");
		Element element7 = new Goal();
		element7.setLabel("e7");
		element7.setName("Element 7");
		Element element8 = new UseCase();
		element8.setLabel("e8");
		element8.setName("Element 8");
		Element element9 = new UseCase();
		element9.setLabel("e9");
		element9.setName("Element 9");

		// Create relationships
		ElementReference ref12 = new StubElementReference();
		ref12.setFromElement(element);
		ref12.setToElement(element2);
		ref12.setRelationshipKind(RelationshipKind.REFINES_TO);
		element.getRelatesTo().add(ref12);
		element2.getRelatedBy().add(ref12);

		ElementReference ref13 = new StubElementReference();
		ref13.setFromElement(element);
		ref13.setToElement(element3);
		ref13.setRelationshipKind(RelationshipKind.DERIVES_TO);
		element.getRelatesTo().add(ref13);
		element3.getRelatedBy().add(ref13);

		ElementReference ref18 = new StubElementReference();
		ref18.setFromElement(element);
		ref18.setToElement(element8);
		ref18.setRelationshipKind(RelationshipKind.EXTENDS);
		element.getRelatesTo().add(ref18);
		element8.getRelatedBy().add(ref18);

		ElementReference ref19 = new StubElementReference();
		ref19.setFromElement(element);
		ref19.setToElement(element9);
		ref19.setRelationshipKind(RelationshipKind.CONFLICTS_WITH);
		element.getRelatesTo().add(ref19);
		element9.getRelatedBy().add(ref19);

		ElementReference ref25 = new StubElementReference();
		ref25.setFromElement(element2);
		ref25.setToElement(element5);
		ref25.setRelationshipKind(RelationshipKind.JUSTIFIES);
		element2.getRelatesTo().add(ref25);
		element5.getRelatedBy().add(ref25);

		ElementReference ref42 = new StubElementReference();
		ref42.setFromElement(element4);
		ref42.setToElement(element2);
		ref42.setRelationshipKind(RelationshipKind.IS_KIND_OF);
		element4.getRelatesTo().add(ref42);
		element2.getRelatedBy().add(ref42);

		ElementReference ref61 = new StubElementReference();
		ref61.setFromElement(element6);
		ref61.setToElement(element);
		ref61.setRelationshipKind(RelationshipKind.IS_ASSOCIATED_TO);
		element6.getRelatesTo().add(ref61);
		element.getRelatedBy().add(ref61);

		ElementReference ref62 = new StubElementReference();
		ref62.setFromElement(element6);
		ref62.setToElement(element2);
		ref62.setRelationshipKind(RelationshipKind.IS_ASSOCIATED_TO);
		element6.getRelatesTo().add(ref62);
		element2.getRelatedBy().add(ref62);

		ElementReference ref71 = new StubElementReference();
		ref71.setFromElement(element7);
		ref71.setToElement(element);
		ref71.setRelationshipKind(RelationshipKind.LEADS_TO);
		element7.getRelatesTo().add(ref71);
		element.getRelatedBy().add(ref71);

		ElementReference ref98 = new StubElementReference();
		ref98.setFromElement(element9);
		ref98.setToElement(element8);
		ref98.setRelationshipKind(RelationshipKind.EXTENDS);
		element9.getRelatesTo().add(ref98);
		element8.getRelatedBy().add(ref98);
	}

	@Test
	public void testSimpleDiscovery() {
		List<Object> result = null;
		Set<RelationshipKind> relationshipKindFilter = new HashSet<RelationshipKind>();
		Set<String> elementKindFilter = new HashSet<String>();
		boolean reverseRelationshipFilter = false;
		boolean reverseElementFilter = false;
		boolean join = true;
		boolean start = true;

		Configuration config = new Configuration(relationshipKindFilter, elementKindFilter, -1,
				reverseRelationshipFilter, reverseElementFilter, join, start);
		config.setDepthLimit(-1);
		config.setElementKindFilter(elementKindFilter);
		config.setJoin(join);
		config.setRelationshipKindFilter(relationshipKindFilter);
		config.setReverseElementFilter(reverseElementFilter);
		config.setReverseRelationshipFilter(reverseRelationshipFilter);
		config.setStart(start);
		assertFalse(config.isReverseElementFilter());
		engine.setConfiguration(config);
		result = engine.query(element);

		assertNotNull("Result list is not null", result);
		assertFalse("Result list contains elements", result.isEmpty());
		System.out.println("Test SimpleDiscovery");
//		for (Object e : result)
//			System.out.println(((Element) e).getElementHashId());
		assertEquals("All elements have been detected", 9, result.size());
	}

	@Test
	public void testSimpleDiscoveryDepth1() {
		List<Object> result = null;
		Set<RelationshipKind> relationshipKindFilter = new HashSet<RelationshipKind>();
		Set<String> elementKindFilter = new HashSet<String>();
		boolean reverseRelationshipFilter = false;
		boolean reverseElementFilter = false;
		boolean join = true;
		boolean start = true;
		int depthlimit = 1;

//		System.out.println(start + " and " + join);

		Configuration config = new Configuration(relationshipKindFilter, elementKindFilter, depthlimit,
				reverseRelationshipFilter, reverseElementFilter, join, start);
		engine.setConfiguration(config);
		result = engine.query(element);

		assertNotNull("Result list is not null", result);
		assertFalse("Result list contains elements", result.isEmpty());
		System.out.println("Test SimpleDiscoveryDepth1");
//		for (Object e : result)
//			System.out.println(((Element) e).getElementHashId());
		assertEquals("All elements have been detected", 7, result.size());
	}

	@Test
	public void testSimpleDiscoveryDepth3() {
		List<Object> result = null;
		Set<RelationshipKind> relationshipKindFilter = new HashSet<RelationshipKind>();
		Set<String> elementKindFilter = new HashSet<String>();
		boolean reverseRelationshipFilter = false;
		boolean reverseElementFilter = false;
		boolean join = true;
		boolean start = true;
		int depthlimit = 2;

//		System.out.println(start + " and " + join);

		Configuration config = new Configuration(relationshipKindFilter, elementKindFilter, depthlimit,
				reverseRelationshipFilter, reverseElementFilter, join, start);
		engine.setConfiguration(config);
		result = engine.query(element);

		assertNotNull("Result list is not null", result);
		assertFalse("Result list contains elements", result.isEmpty());
		System.out.println("Test SimpleDiscoveryDepth3");
//		for (Object e : result)
//			System.out.println(((Element) e).getElementHashId());
		assertEquals("All elements have been detected", 9, result.size());
	}

//	@Test
	public void testFilteredElementKindDiscovery() {
		List<Object> result = new ArrayList<Object>();
		Set<RelationshipKind> relationshipKindFilter = new HashSet<RelationshipKind>();
		Set<String> elementKindFilter = new HashSet<String>();
		elementKindFilter.add("UseCase");
		elementKindFilter.add("Goal");
		boolean reverseRelationshipFilter = false;
		boolean reverseElementFilter = false;
		boolean join = true;
		boolean start = true;

		engine.setConfiguration(new Configuration(relationshipKindFilter, elementKindFilter, -1,
				reverseRelationshipFilter, reverseElementFilter, join, start));
		result = engine.query(element);

		assertNotNull("Result list is not null", result);
		assertFalse("Result list contains elements", result.isEmpty());
		System.out.println("Test FilteredRelationshipKindDiscovery");
//		for (Object e : result)
//			System.out.println(((Element) e).getElementHashId());
		assertEquals("All elements have been detected", 4, result.size());
	}

	@Test
	public void testFilteredRelationshipKindDiscovery() {
		List<Object> result = new ArrayList<Object>();
		Set<RelationshipKind> relationshipKindFilter = new HashSet<RelationshipKind>();
		relationshipKindFilter.add(RelationshipKind.REFINES_TO);
		relationshipKindFilter.add(RelationshipKind.JUSTIFIES);

		Set<String> elementKindFilter = new HashSet<String>();
		boolean reverseRelationshipFilter = false;
		boolean reverseElementFilter = false;
		boolean join = true;
		boolean start = true;

		engine.setConfiguration(new Configuration(relationshipKindFilter, elementKindFilter, -1,
				reverseRelationshipFilter, reverseElementFilter, join, start));
		result = engine.query(element);

		assertNotNull("Result list is not null", result);
		assertFalse("Result list contains elements", result.isEmpty());
		System.out.println("Test FilteredRelationshipKindDiscovery");
//		for (Object e : result)
//			System.out.println(((Element) e).getElementHashId());
		assertEquals("All elements have been detected", 3, result.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRelationshipKindDiscoveryLeveled() {
		// For level testing
		Map<Integer, Integer> entryCounter = new HashMap<Integer, Integer>();
		entryCounter.put(0, 1);
		entryCounter.put(1, 6);
		entryCounter.put(2, 4);
		entryCounter.put(3, 1);

		List<Object> result = new ArrayList<Object>();
		Set<RelationshipKind> relationshipKindFilter = new HashSet<RelationshipKind>();
		Set<String> elementKindFilter = new HashSet<String>();
		boolean reverseRelationshipFilter = false;
		boolean reverseElementFilter = false;
		boolean join = false;
		boolean start = true;

		engine.setConfiguration(new Configuration(relationshipKindFilter, elementKindFilter, -1,
				reverseRelationshipFilter, reverseElementFilter, join, start));
		result = engine.query(element);

		System.out.println("Test RelationshipKindDiscoveryLeveled");
		assertNotNull("Result list is not null", result);
		assertFalse("Result list contains elements", result.isEmpty());
		assertEquals("Result list contains lists", result.get(1).getClass(), HashSet.class);
		assertEquals("Whole depth has been discovered", 4, result.size());

		for (int i = 0; i < result.size(); i++) {
//			Set<Element> elementSet = (Set<Element>) result.get(i);
//			System.out.print("Level " + i + ": ");
//			for (Element e : elementSet)
//				System.out.print(e.getName() + " ");
//			System.out.println("");
			assertEquals("All elements in depth level discovered", (int) entryCounter.get(i),
					((Set<Element>) result.get(i)).size());
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testActiveRelationshipKindDiscoveryLeveled() {
		// For level testing
		Map<Integer, Integer> entryCounter = new HashMap<Integer, Integer>();
		entryCounter.put(0, 1);
		entryCounter.put(1, 5);
		entryCounter.put(2, 3);
		entryCounter.put(3, 1);

		List<Object> result = new ArrayList<Object>();
		Set<RelationshipKind> relationshipKindFilter = new HashSet<RelationshipKind>();
		Set<String> elementKindFilter = new HashSet<String>();
		relationshipKindFilter.addAll(RelationshipKind.getActiveKinds());
		relationshipKindFilter.addAll(RelationshipKind.getSymmetricalKinds());
		boolean reverseRelationshipFilter = false;
		boolean reverseElementFilter = false;
		boolean join = false;
		boolean start = true;

		engine.setConfiguration(new Configuration(relationshipKindFilter, elementKindFilter, -1,
				reverseRelationshipFilter, reverseElementFilter, join, start));

		result = engine.query(element);

		System.out.println("Test ActiveRelationshipKindDiscoveryLeveled");
		assertNotNull("Result list is not null", result);
		assertFalse("Result list contains elements", result.isEmpty());
		assertEquals("Result list contains lists", result.get(1).getClass(), HashSet.class);
		assertEquals("Whole depth has been discovered", 4, result.size());

		for (int i = 0; i < result.size(); i++) {
//			Set<Element> elementSet = (Set<Element>) result.get(i);
//			System.out.print("Level " + i + ": ");
//			for (Element e : elementSet)
//				System.out.print(e.getName() + " ");
//			System.out.println("");
			assertEquals("All elements in depth level discovered", (int) entryCounter.get(i),
					((Set<Element>) result.get(i)).size());
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testPassiveRelationshipKindDiscoveryLeveled() {
		// For level testing
		Map<Integer, Integer> entryCounter = new HashMap<Integer, Integer>();
		entryCounter.put(0, 1);
		entryCounter.put(1, 3);
		entryCounter.put(2, 1);
		entryCounter.put(3, 2);

		List<Object> result = new ArrayList<Object>();
		Set<RelationshipKind> relationshipKindFilter = new HashSet<RelationshipKind>();
		Set<String> elementKindFilter = new HashSet<String>();
		relationshipKindFilter.addAll(RelationshipKind.getPassiveKinds());
		relationshipKindFilter.addAll(RelationshipKind.getSymmetricalKinds());
		boolean reverseRelationshipFilter = false;
		boolean reverseElementFilter = false;
		boolean join = false;
		boolean start = true;

		engine.setConfiguration(new Configuration(relationshipKindFilter, elementKindFilter, -1,
				reverseRelationshipFilter, reverseElementFilter, join, start));

		result = engine.query(element);

		System.out.println("Test PassiveRelationshipKindDiscoveryLeveled");
		assertNotNull("Result list is not null", result);
		assertFalse("Result list contains elements", result.isEmpty());
		assertEquals("Result list contains lists", result.get(1).getClass(), HashSet.class);
		assertEquals("Whole depth has been discovered", 4, result.size());

		for (int i = 0; i < result.size(); i++) {
//			Set<Element> elementSet = (Set<Element>) result.get(i);
//			System.out.print("Level " + i + ": ");
//			for (Element e : elementSet)
//				System.out.print(e.getName() + " ");
//			System.out.println("");
			assertEquals("All elements in depth level discovered", (int) entryCounter.get(i),
					((Set<Element>) result.get(i)).size());
		}
	}
}
