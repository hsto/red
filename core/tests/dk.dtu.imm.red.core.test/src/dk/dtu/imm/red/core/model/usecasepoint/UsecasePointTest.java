package dk.dtu.imm.red.core.model.usecasepoint;

 

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test; 

import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.project.ProjectFactory;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointFactory;
 

public class UsecasePointTest {
	
	private UsecasePoint subject;
	
	@Before
	public void setUp() throws Exception {
		
		Project p = ProjectFactory.eINSTANCE.createProject(); 
		subject = UsecasepointFactory.eINSTANCE.createUsecasePoint(); 
		p.setEffort(subject); 
	}
	

	@Ignore("Requires implementation") @Test
	public void testManagementFactorCalculation() {
		fail("Not yet implemented");
	}
	
	@Ignore("Requires maintenance") @Test
	public void testTechnologyFactorCalculation() { 
		assertEquals(0.99, subject.getTechnologyFactor().getSum(), 0);
	}
	
	@Ignore("Requires implementation") @Test
	public void testRequirementFactorCalculation() {
		fail("Not yet implemented");
	}

}
