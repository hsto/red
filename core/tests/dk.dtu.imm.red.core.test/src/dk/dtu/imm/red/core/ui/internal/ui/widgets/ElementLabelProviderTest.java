package dk.dtu.imm.red.core.ui.internal.ui.widgets;

import static org.junit.Assert.assertEquals;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;

import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.ui.widgets.ElementLabelProvider;

public class ElementLabelProviderTest {

	/**
	 * The label provider under test.
	 */
	private ElementLabelProvider labelProvider;

	/**
	 * All initialization before a test case is run.
	 * 
	 * @throws Exception
	 *             any exceptions thrown during setup
	 */
	@Before
	public void setUp() throws Exception {
		labelProvider = new ElementLabelProvider();
	}

	/**
	 * Cleanup after a test case is run.
	 * 
	 * @throws Exception
	 *             any exceptions thrown during teardown
	 */
	@After
	public void tearDown() throws Exception {
		labelProvider = null;
	}

	/**
	 * Test that the label provided for a folder is correct.
	 */
	@Test
	public void testFolderLabelCorrect() {
		Folder folder = FolderFactory.eINSTANCE.createFolder();
		folder.setName("folder1");

		assertEquals("folder1", labelProvider.getText(folder));
	}

	/**
	 * Test that a correct image is returned for a given folder.
	 */
	@Test
	public void testFolderImageCorrect() {
		Folder folder = FolderFactory.eINSTANCE.createFolder();
		folder.setName("folder1");
		folder.setIconURI("icons/testicon.gif");

		Bundle bundle = Platform.getBundle("dk.dtu.imm.red.core.test");
		ImageDescriptor imgDescriptor = ImageDescriptor
				.createFromURL(FileLocator.find(bundle, new Path(
						"icons/testicon.gif"), null));
		Image testImage = imgDescriptor.createImage();

		Image providedImage = labelProvider.getImage(folder);

		// We just test on the type right now. To be correct, we need a more
		// precise way of comparing two images. Using image.getImageData().data
		// does not work
		assertEquals(testImage.type, providedImage.type);
	}

}
