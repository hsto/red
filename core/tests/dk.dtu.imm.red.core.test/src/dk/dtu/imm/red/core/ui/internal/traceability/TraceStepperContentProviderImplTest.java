package dk.dtu.imm.red.core.ui.internal.traceability;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.element.relationship.ElementReference;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.element.relationship.impl.ElementReferenceImpl;
import dk.dtu.imm.red.core.ui.internal.traceability.stepper.views.impl.TraceStepperContentProviderImpl;

public class TraceStepperContentProviderImplTest {

	private TraceStepperContentProviderImpl provider;

	@Before
	public void setUp() throws Exception {
		provider = new TraceStepperContentProviderImpl();
		assertNotNull("TraceStepperContentProviderImpl instance could be created", provider);
	}

	@Test
	public void testSetFocusElement() {
		Element focusElement = new ElementImpl() {
		};
		focusElement.setLabel("e1");
		focusElement.setName("Element 1");

		provider.setFocusElement(focusElement);
	}

	@Test
	public void testGetRelationshipKindList() {
		String[] list = provider.getRelationshipKindList();

		assertNotNull("Relationship kinds not null", list);
		assertTrue("Relationship kinds not empty", list.length > 0);
	}

	// Skipped because it requires user interaction
//	@Test(expected = NullPointerException.class)
//	public void testGetContent() {
//		assertNull(provider.getContent());
//	}

	@Test
	public void testGetElementKindList() {
		String[] list = provider.getElementKindList();

		assertNotNull("Element kinds not null", list);
		assertTrue("Element kinds not empty", list.length > 0);
	}

	@Test
	public void testGetDataForSourceTable() {
		Element focusElement = new ElementImpl() {
		};
		provider.setFocusElement(focusElement);

		assertNotNull("Focus element has been set", provider.getFocusElement());

		List<Element> list = provider.getDataForSourceTable();
		assertNotNull("Source table data not null", list);
		assertEquals("The returned list has no elements, since focusElement has no links", list.size(), 0);
	}

	@Test
	public void testGetDataForTargetTable() {
		Element focusElement = new ElementImpl() {
		};
		provider.setFocusElement(focusElement);

		List<Element> list = provider.getDataForTargetTable();
		assertNotNull("Source table data not null", list);
		assertEquals("The returned list has no elements, since focusElement has no links", list.size(), 0);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetToColumnDefinition() {
		Element focusElement = new ElementImpl() {
		};
		focusElement.setLabel("e1");
		focusElement.setName("Element 1");
		ElementColumnDefinition[] definition = provider.getToColumnDefinition();

		assertNotNull("ElementColumnDefinition definied", definition);
		assertEquals("Definition for all 3 columns given", 3, definition.length);

		assertEquals("First column named 'Element'", "Element", definition[0].getColumnName());

		assertEquals("Second column named 'Type'", "Type", definition[1].getColumnName());
		assertEquals("Handler: Second column returns", "Element",
				((CellHandler<Element>) definition[1].getCellHandler()).getAttributeValue(focusElement));

		assertEquals("Third column named 'Relationship'", "Relationship", definition[2].getColumnName());
		assertNull("Handler: Second column returns", definition[2].getCellHandler().getAttributeValue(null));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetFromColumnDefinition() {
		Element focusElement = new ElementImpl() {
		};
		focusElement.setLabel("e1");
		focusElement.setName("Element 1");
		ElementColumnDefinition[] definition = provider.getFromColumnDefinition();

		assertNotNull("ElementColumnDefinition definied", definition);
		assertEquals("Definition for all 3 columns given", 3, definition.length);

		assertEquals("First column named 'Element'", "Element", definition[0].getColumnName());

		assertEquals("Second column named 'Type'", "Type", definition[1].getColumnName());
		assertEquals("Handler: Second column returns", "Element",
				((CellHandler<Element>) definition[1].getCellHandler()).getAttributeValue(focusElement));

		assertEquals("Third column named 'Relationship'", "Relationship", definition[2].getColumnName());
		assertNull("Handler: Second column returns", definition[2].getCellHandler().getAttributeValue(null));
	}

	@Test
	public void testApplyFilter() {
		Element focusElement = new ElementImpl() {
		};
		focusElement.setLabel("e1");
		focusElement.setName("Element 1");
		Element element2 = new ElementImpl() {
		};
		element2.setLabel("e2");
		element2.setName("Element 2");

		ElementReference reference = new ElementReferenceImpl() {
		};
		reference.setRelationshipKind(RelationshipKind.IS_ASSOCIATED_TO);
		reference.setFromElement(focusElement);
		reference.setToElement(element2);

		focusElement.getRelatesTo().add(reference);
		element2.getRelatedBy().add(reference);

		List<ElementRelationship> unfilteredRelationships = new ArrayList<ElementRelationship>();
		List<ElementRelationship> filteredRelationships = new ArrayList<ElementRelationship>();

		unfilteredRelationships.add(reference);
		
		provider.setElementFilter("Element", true);
		provider.setRelationshipFilter("is associated to", true);
		provider.applyFilter(unfilteredRelationships, filteredRelationships, true);
		
		assertTrue("Relationship has been added to filtered list", filteredRelationships.contains(reference));
	}
	
}
