package dk.dtu.imm.red.core.ui.internal.comment.views.impl;

import static org.junit.Assert.assertNotNull;

import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.dtu.imm.red.core.ui.internal.comment.views.impl.CommentViewImpl;

/**
 * Unit test for the comment view
 * 
 * @author Anders Friis
 * 
 */
public final class CommentViewTest {

	/**
	 * The view that is tested
	 */
	private CommentViewImpl view;

	/**
	 * Sets up any initial things before test cases are run.
	 * 
	 * @throws Exception
	 *             If any exceptions happen during startup.
	 */
	@Before
	public void setUp() throws Exception {
		view = (CommentViewImpl) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.showView(CommentViewImpl.ID);
	}

	/**
	 * Clear up after all tests have run.
	 * 
	 * @throws Exception
	 *             If any exceptions happen during cleanup.
	 */
	@After
	public void tearDown() throws Exception {
		view.dispose();
	}

	/**
	 * Test that there exists a help button for comments
	 */
	@Test
	public void testHelpButtonExists() {
		assertNotNull(view.helpButton);
	}

	/**
	 * Test that there exists a button for adding a comment
	 */
	@Test
	public void testAddCommentButtonExists() {
		assertNotNull(view.addCommentButton);
	}

	/**
	 * Test that there exists a button for deleting a comment
	 */
	@Test
	public void testDeleteCommentButtonExists() {
		assertNotNull(view.deleteCommentButton);
	}

	/**
	 * Test that there exists a table to show comments.
	 */
	@Test
	public void testAssociationsTableExists() {
		assertNotNull(view.commentsTable);
	}

}
