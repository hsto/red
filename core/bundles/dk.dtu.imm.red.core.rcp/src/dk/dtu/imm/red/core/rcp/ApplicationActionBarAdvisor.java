package dk.dtu.imm.red.core.rcp;


import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.StatusLineContributionItem;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

import dk.dtu.imm.red.core.ui.StatusLineContributionItemWithPropertyListener;

public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
	
	public static final String DATE = "dk.dtu.imm.red.core.statusbar.date";
	public static final String SAVE_PROGRESS = 
			"dk.dtu.imm.red.core.statusbar.saveprogress";
	public static final String AUTHOR = "dk.dtu.imm.red.core.statusbar.author";
	public static final String RESPONSIBLE = 
			"dk.dtu.imm.red.core.statusbar.responsible";
	public static final String ELEMENT_STATUS = 
			"dk.dtu.imm.red.core.statusbar.elementstatus";
	public static final String LAST_MODIFIED = 
			"dk.dtu.imm.red.core.statusbar.lastmodified";
	public static final String CREATED = "dk.dtu.imm.red.core.statusbar.created";
	public static final String PRIORITY = "dk.dtu.imm.red.core.statusbar.priority";

	private StatusLineContributionItem authorItem;
	private StatusLineContributionItem responsibleItem;
	private StatusLineContributionItem elementStatusItem;
	private StatusLineContributionItem lastModifiedItem;
	private StatusLineContributionItem priorityItem;
	
	public ApplicationActionBarAdvisor(final IActionBarConfigurer configurer) {
		super(configurer);
	}

	@Override
	protected void makeActions(final IWorkbenchWindow window) {
		register(ActionFactory.UNDO.create(window));
		register(ActionFactory.REDO.create(window));
		register(ActionFactory.HELP_SEARCH.create(window));
		register(ActionFactory.DYNAMIC_HELP.create(window));
		register(ActionFactory.HELP_CONTENTS.create(window));
		
	}

	@Override
	protected void fillStatusLine(IStatusLineManager statusLine) {
		super.fillStatusLine(statusLine);
		
		authorItem = new StatusLineContributionItemWithPropertyListener(AUTHOR);
		statusLine.add(authorItem);
		
		responsibleItem = new StatusLineContributionItemWithPropertyListener(RESPONSIBLE);
		statusLine.insertAfter(AUTHOR, responsibleItem);
		
		elementStatusItem = new StatusLineContributionItemWithPropertyListener(ELEMENT_STATUS);
		statusLine.insertAfter(RESPONSIBLE, elementStatusItem);
		
		lastModifiedItem = new StatusLineContributionItemWithPropertyListener(LAST_MODIFIED);
		statusLine.insertAfter(ELEMENT_STATUS, lastModifiedItem);
		
		priorityItem = new StatusLineContributionItemWithPropertyListener(PRIORITY);
		statusLine.insertAfter(LAST_MODIFIED, priorityItem);
		
	}

}
