package dk.dtu.imm.red.core.rcp;


import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.internal.LogUtil;

/**
 * This class controls all aspects of the application's execution
 */
public class Application implements IApplication {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.
	 * IApplicationContext)
	 */
	@Override
	public Object start(IApplicationContext context) throws Exception {
		Display display = PlatformUI.createDisplay();
		try {
			int returnCode = PlatformUI.createAndRunWorkbench(display,
					new ApplicationWorkbenchAdvisor());
			if (returnCode == PlatformUI.RETURN_RESTART) {
				return IApplication.EXIT_RESTART;
			} else if (returnCode == PlatformUI.RETURN_EMERGENCY_CLOSE) { // Unclear when it's reached. Saves in case it reaches. 
				if (PlatformUI.getWorkbench().getActiveWorkbenchWindow() != null) {
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().saveAllEditors(false);
				}
				return IApplication.EXIT_OK;
			} else {
				return IApplication.EXIT_OK;
			}
		} catch (Exception e) { // Issue #114 Exception wrapper
			
			MessageDialog md = new MessageDialog(Display
					.getCurrent().getActiveShell(), "Application Crashed", null,
					 "Application Crashed! \n"
					 + "Please send the log in the follwing path {RED root}/workspace/.metadata/.log to technical support!",
					MessageDialog.INFORMATION,
					new String[] { "save and restart", "exit without saving" }, 0);
			
			md.open();
			
			if (md.getReturnCode() == Dialog.OK) {
				
				LogUtil.logError("Apllication crashed");
				LogUtil.logError(e);
				try {
					if (PlatformUI.getWorkbench().getActiveWorkbenchWindow() != null) {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().saveAllEditors(false);
					}				
				} catch (Exception ex) {
					LogUtil.logError("Apllication crashed - Saving failed");
					LogUtil.logError(ex);
				}
				
				return IApplication.EXIT_RESTART;
			} else {
				return IApplication.EXIT_OK;
			}
			
			
		} finally {
			display.dispose();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	@Override
	public void stop() {
		if (!PlatformUI.isWorkbenchRunning())
			return;
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			@Override
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
	}
}
