package dk.dtu.imm.red.core.rcp;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.eclipse.ui.internal.dialogs.WorkbenchWizardElement;
import org.eclipse.ui.internal.wizards.AbstractExtensionWizardRegistry;
import org.eclipse.ui.wizards.IWizardCategory;
import org.eclipse.ui.wizards.IWizardDescriptor;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FilePackage;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	private static final String MEMENTO_TYPE_FILE = "FILE";
	private static final String MEMENTO_TYPE_CURRENT_FILES = "CURRENT_FILES";
	private static final String MEMENTO_TYPE_RECENT_FILES = "RECENT_FILES";
	private static final String MEMENTO_WORKSPACE_PASSWORD = "WORKSPACE_PASSWORD";

	public ApplicationWorkbenchWindowAdvisor(
			IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	@Override
	public ActionBarAdvisor createActionBarAdvisor(
			IActionBarConfigurer configurer) {
		return new ApplicationActionBarAdvisor(configurer);
	}

	@Override
	public void preWindowOpen() {
        IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
        //configurer.setInitialSize(new Point(400, 300));
        configurer.setShowCoolBar(true);
        configurer.setShowStatusLine(true);
        configurer.setTitle("Requirements Engineering Editor (RED)");
//        configurer.setShowProgressIndicator(true);
    }
    
    @Override
    public void postWindowOpen() {
    	super.postWindowOpen();
    	
    	// Maximize window on application startup
    	getWindowConfigurer().getWindow().getShell().setMaximized(true);
    	
    	removeUnwantedWizards();

    }
    
    
    
    
    /**
     * Removes all unwanted wizards from the new wizard, export and import registries.
     */
    @SuppressWarnings("restriction")
	private void removeUnwantedWizards() {
    	AbstractExtensionWizardRegistry newWizardRegistry = 
    			(AbstractExtensionWizardRegistry) WorkbenchPlugin.getDefault()
    			.getNewWizardRegistry();
    	AbstractExtensionWizardRegistry exportWizardRegistry = 
    			(AbstractExtensionWizardRegistry) WorkbenchPlugin.getDefault()
    			.getExportWizardRegistry();
    	AbstractExtensionWizardRegistry importWizardRegistry = 
    			(AbstractExtensionWizardRegistry) WorkbenchPlugin.getDefault()
    			.getImportWizardRegistry();
    	
    	AbstractExtensionWizardRegistry[] registries = 
    			new AbstractExtensionWizardRegistry[] 
    					{ newWizardRegistry, importWizardRegistry, 
    						exportWizardRegistry };
    	
    	for (AbstractExtensionWizardRegistry wizardRegistry : registries) {
	    	IWizardCategory[] categories  = wizardRegistry.getRootCategory()
	    			.getCategories();
	    	
	    	for (IWizardDescriptor wizard : getAllWizards(categories)) { 
	    		WorkbenchWizardElement wizardElement = 
	    				(WorkbenchWizardElement) wizard;
	    		
	    		if (!isWizardAllowed(wizardElement.getId())) {
	    			wizardRegistry.removeExtension(wizardElement
	    					.getConfigurationElement().getDeclaringExtension(), 
	    					new Object[] {wizardElement});
	    		}
	    	}
    	}
    }
    
    /**
     * Gets all wizards in a given array of categories.
     * @param categories array of categories containing wizards
     * @return all wizards in these categories
     */
    private IWizardDescriptor[] getAllWizards(
    		final IWizardCategory... categories) {
    	List<IWizardDescriptor> results = new ArrayList<IWizardDescriptor>();
    	
    	for (IWizardCategory category : categories) { 
    		results.addAll(Arrays.asList(category.getWizards()));
    		results.addAll(Arrays.asList(
    				getAllWizards(category.getCategories())));
    	}
    	
    	return results.toArray(new IWizardDescriptor[0]);
    }
    
    /**
     * All disallowed wizards.
     */
    protected static final List<String> FILE_NEW__DISALLOWED_WIZARDS 
    	= Collections.unmodifiableList(Arrays.asList(new String[] {
    		"org.eclipse.ui.wizards.new.project",
    		"org.eclipse.ui.wizards.new.folder",
    		"org.eclipse.ui.wizards.new.file"
    }));
    
    /**
     * Replies whether a wizard is in the FILE_NEW_DISALLOWED_WIZARDS
     * list or not.
     * @param wizardID the wizard to check if allowed
     * @return whether the wizard is allowed or not
     */
    private boolean isWizardAllowed(final String wizardID) {
    	return wizardID.contains("dk.dtu.imm.red");
    	
    }
    
    @Override
    public IStatus saveState(final IMemento memento) {
    	IMemento recentFiles = memento.createChild(MEMENTO_TYPE_RECENT_FILES);
    	IMemento currentFiles = memento.createChild(MEMENTO_TYPE_CURRENT_FILES);
    	
    	IMemento workspacePassword = memento.createChild(MEMENTO_WORKSPACE_PASSWORD);
    	workspacePassword.putString("password", 
    			WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getPassword());

    	for (File openFile : WorkspaceFactory.eINSTANCE
    			.getWorkspaceInstance().getCurrentlyOpenedFiles()) {
    		IMemento file = currentFiles.createChild(MEMENTO_TYPE_FILE);
    		file.putString("uri", openFile.getUri());
    	}

    	for (File recentFile : WorkspaceFactory.eINSTANCE
    			.getWorkspaceInstance().getRecentlyOpenedFiles()) {
    		IMemento file = recentFiles.createChild(MEMENTO_TYPE_FILE);
    		file.putString("uri", recentFile.getUri());
    	}
    	return super.saveState(memento);
    }
    
    @Override
    public IStatus restoreState(final IMemento memento) {
    	IMemento open = memento.getChild(MEMENTO_TYPE_CURRENT_FILES);
    	IMemento[] openFiles = open.getChildren(MEMENTO_TYPE_FILE);
    	
    	EPackage.Registry.INSTANCE.put(FilePackage.eNS_URI, FilePackage.eINSTANCE);
    	
    	for (IMemento openFile : openFiles) {
    		String uri = openFile.getString("uri");
    		ResourceSet resourceSet = new ResourceSetImpl();
    		Resource resource = 
    				resourceSet.createResource(URI.createFileURI(uri));
    		
    		try {
				resource.load(Collections.EMPTY_MAP);
			} catch (IOException e) {
				 continue;
			}
    		
    		if (resource.getContents().size() == 0
    				|| !(resource.getContents().get(0) instanceof File)) {
    			continue;
    		}
    		
    		File file = (File) resource.getContents().get(0);
    		WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
    			.addOpenedFile(file);
    		file.save();
    	}
    	
    	IMemento workspacePasswordMemento = memento.getChild(MEMENTO_WORKSPACE_PASSWORD);
    	if (workspacePasswordMemento != null) {
    		String password = workspacePasswordMemento.getString("password");
    		WorkspaceFactory.eINSTANCE.getWorkspaceInstance().setPassword(password);
    		WorkspaceFactory.eINSTANCE.getWorkspaceInstance().save();
    	}
    		
    	
    	return super.restoreState(memento);
    }
}
