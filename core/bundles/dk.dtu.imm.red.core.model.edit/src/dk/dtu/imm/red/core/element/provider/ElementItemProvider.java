/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.id.IdFactory;
import dk.dtu.imm.red.core.element.relationship.RelationshipFactory;
import dk.dtu.imm.red.core.properties.PropertiesFactory;
import dk.dtu.imm.red.core.provider.CoreEditPlugin;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.user.UserFactory;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.core.element.Element} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ElementItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIconURIPropertyDescriptor(object);
			addIconPropertyDescriptor(object);
			addLabelPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addElementKindPropertyDescriptor(object);
			addDescriptionPropertyDescriptor(object);
			addPriorityPropertyDescriptor(object);
			addTimeCreatedPropertyDescriptor(object);
			addLastModifiedPropertyDescriptor(object);
			addLastModifiedByPropertyDescriptor(object);
			addVersionPropertyDescriptor(object);
			addUniqueIDPropertyDescriptor(object);
			addRelatedByPropertyDescriptor(object);
			addUriPropertyDescriptor(object);
			addWorkPackagePropertyDescriptor(object);
			addDeadlinePropertyDescriptor(object);
			addLockStatusPropertyDescriptor(object);
			addLockPasswordPropertyDescriptor(object);
			addEstimatedComplexityPropertyDescriptor(object);
			addRiskPropertyDescriptor(object);
			addLifeCyclePhasePropertyDescriptor(object);
			addStatePropertyDescriptor(object);
			addManagementDecisionPropertyDescriptor(object);
			addQaAssessmentPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Icon URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIconURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_iconURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_iconURI_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__ICON_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Icon feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIconPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_icon_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_icon_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__ICON,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_label_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_label_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__LABEL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_name_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Element Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElementKindPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_elementKind_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_elementKind_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__ELEMENT_KIND,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_description_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_description_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__DESCRIPTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Priority feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPriorityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_priority_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_priority_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__PRIORITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Time Created feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimeCreatedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_timeCreated_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_timeCreated_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__TIME_CREATED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Last Modified feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastModifiedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_lastModified_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_lastModified_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__LAST_MODIFIED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Last Modified By feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastModifiedByPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_lastModifiedBy_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_lastModifiedBy_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__LAST_MODIFIED_BY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_version_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_version_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Unique ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUniqueIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_uniqueID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_uniqueID_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__UNIQUE_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Related By feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRelatedByPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_relatedBy_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_relatedBy_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__RELATED_BY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUriPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_uri_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_uri_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Work Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWorkPackagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_workPackage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_workPackage_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__WORK_PACKAGE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Deadline feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeadlinePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_deadline_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_deadline_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__DEADLINE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Lock Status feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLockStatusPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_lockStatus_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_lockStatus_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__LOCK_STATUS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Lock Password feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLockPasswordPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_lockPassword_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_lockPassword_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__LOCK_PASSWORD,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Estimated Complexity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEstimatedComplexityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_estimatedComplexity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_estimatedComplexity_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__ESTIMATED_COMPLEXITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Risk feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRiskPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_risk_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_risk_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__RISK,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Life Cycle Phase feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLifeCyclePhasePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_lifeCyclePhase_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_lifeCyclePhase_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__LIFE_CYCLE_PHASE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the State feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_state_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_state_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__STATE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Management Decision feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addManagementDecisionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_managementDecision_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_managementDecision_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__MANAGEMENT_DECISION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Qa Assessment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addQaAssessmentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Element_qaAssessment_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Element_qaAssessment_feature", "_UI_Element_type"),
				 ElementPackage.Literals.ELEMENT__QA_ASSESSMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__COMMENTLIST);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__CREATOR);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__VISIBLE_ID);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__RELATES_TO);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__CHANGE_LIST);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__RESPONSIBLE_USER);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__COST);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__BENEFIT);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__MANAGEMENT_DISCUSSION);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__DELETION_LISTENERS);
			childrenFeatures.add(ElementPackage.Literals.ELEMENT__QUANTITIES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean hasChildren(Object object) {
		return hasChildren(object, true);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Element)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Element_type") :
			getString("_UI_Element_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Element.class)) {
			case ElementPackage.ELEMENT__ICON_URI:
			case ElementPackage.ELEMENT__ICON:
			case ElementPackage.ELEMENT__LABEL:
			case ElementPackage.ELEMENT__NAME:
			case ElementPackage.ELEMENT__ELEMENT_KIND:
			case ElementPackage.ELEMENT__DESCRIPTION:
			case ElementPackage.ELEMENT__PRIORITY:
			case ElementPackage.ELEMENT__TIME_CREATED:
			case ElementPackage.ELEMENT__LAST_MODIFIED:
			case ElementPackage.ELEMENT__VERSION:
			case ElementPackage.ELEMENT__UNIQUE_ID:
			case ElementPackage.ELEMENT__URI:
			case ElementPackage.ELEMENT__WORK_PACKAGE:
			case ElementPackage.ELEMENT__DEADLINE:
			case ElementPackage.ELEMENT__LOCK_STATUS:
			case ElementPackage.ELEMENT__LOCK_PASSWORD:
			case ElementPackage.ELEMENT__ESTIMATED_COMPLEXITY:
			case ElementPackage.ELEMENT__RISK:
			case ElementPackage.ELEMENT__LIFE_CYCLE_PHASE:
			case ElementPackage.ELEMENT__STATE:
			case ElementPackage.ELEMENT__MANAGEMENT_DECISION:
			case ElementPackage.ELEMENT__QA_ASSESSMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ElementPackage.ELEMENT__COMMENTLIST:
			case ElementPackage.ELEMENT__CREATOR:
			case ElementPackage.ELEMENT__VISIBLE_ID:
			case ElementPackage.ELEMENT__RELATES_TO:
			case ElementPackage.ELEMENT__CHANGE_LIST:
			case ElementPackage.ELEMENT__RESPONSIBLE_USER:
			case ElementPackage.ELEMENT__COST:
			case ElementPackage.ELEMENT__BENEFIT:
			case ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION:
			case ElementPackage.ELEMENT__DELETION_LISTENERS:
			case ElementPackage.ELEMENT__QUANTITIES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__COMMENTLIST,
				 CommentFactory.eINSTANCE.createCommentList()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__CREATOR,
				 UserFactory.eINSTANCE.createUser()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__VISIBLE_ID,
				 IdFactory.eINSTANCE.createVisibleElementID()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__RELATES_TO,
				 RelationshipFactory.eINSTANCE.createElementReference()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__RELATES_TO,
				 RelationshipFactory.eINSTANCE.createGlossaryEntryReference()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__CHANGE_LIST,
				 CommentFactory.eINSTANCE.createCommentList()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__RESPONSIBLE_USER,
				 UserFactory.eINSTANCE.createUser()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__COST,
				 PropertiesFactory.eINSTANCE.createProperty()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__BENEFIT,
				 PropertiesFactory.eINSTANCE.createProperty()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__MANAGEMENT_DISCUSSION,
				 TextFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__DELETION_LISTENERS,
				 ElementFactory.eINSTANCE.createDeletionListener()));

		newChildDescriptors.add
			(createChildParameter
				(ElementPackage.Literals.ELEMENT__QUANTITIES,
				 ElementFactory.eINSTANCE.createQuantityItem()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ElementPackage.Literals.ELEMENT__COMMENTLIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CHANGE_LIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CREATOR ||
			childFeature == ElementPackage.Literals.ELEMENT__RESPONSIBLE_USER ||
			childFeature == ElementPackage.Literals.ELEMENT__COST ||
			childFeature == ElementPackage.Literals.ELEMENT__BENEFIT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return CoreEditPlugin.INSTANCE;
	}

}
