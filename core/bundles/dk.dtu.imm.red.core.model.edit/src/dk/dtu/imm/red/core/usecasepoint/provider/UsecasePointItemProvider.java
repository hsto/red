/**
 */
package dk.dtu.imm.red.core.usecasepoint.provider;


import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.properties.PropertiesFactory;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointFactory;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UsecasePointItemProvider
	extends FactorItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsecasePointItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTotalCostPropertyDescriptor(object);
			addUsecasePointsPropertyDescriptor(object);
			addTotalCostManualPropertyDescriptor(object);
			addTotalBenefitManualPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Total Cost feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTotalCostPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UsecasePoint_totalCost_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UsecasePoint_totalCost_feature", "_UI_UsecasePoint_type"),
				 UsecasepointPackage.Literals.USECASE_POINT__TOTAL_COST,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Usecase Points feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUsecasePointsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UsecasePoint_usecasePoints_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UsecasePoint_usecasePoints_feature", "_UI_UsecasePoint_type"),
				 UsecasepointPackage.Literals.USECASE_POINT__USECASE_POINTS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Total Cost Manual feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTotalCostManualPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UsecasePoint_totalCostManual_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UsecasePoint_totalCostManual_feature", "_UI_UsecasePoint_type"),
				 UsecasepointPackage.Literals.USECASE_POINT__TOTAL_COST_MANUAL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Total Benefit Manual feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTotalBenefitManualPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UsecasePoint_totalBenefitManual_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UsecasePoint_totalBenefitManual_feature", "_UI_UsecasePoint_type"),
				 UsecasepointPackage.Literals.USECASE_POINT__TOTAL_BENEFIT_MANUAL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UsecasepointPackage.Literals.USECASE_POINT__MANAGEMENT_FACTOR);
			childrenFeatures.add(UsecasepointPackage.Literals.USECASE_POINT__PRODUCTIVITY_FACTOR);
			childrenFeatures.add(UsecasepointPackage.Literals.USECASE_POINT__REQUIREMENT_FACTOR);
			childrenFeatures.add(UsecasepointPackage.Literals.USECASE_POINT__TECHNOLOGY_FACTOR);
			childrenFeatures.add(UsecasepointPackage.Literals.USECASE_POINT__COST_PER_USECASE_POINT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns UsecasePoint.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UsecasePoint"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((UsecasePoint)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_UsecasePoint_type") :
			getString("_UI_UsecasePoint_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UsecasePoint.class)) {
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST:
			case UsecasepointPackage.USECASE_POINT__USECASE_POINTS:
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST_MANUAL:
			case UsecasepointPackage.USECASE_POINT__TOTAL_BENEFIT_MANUAL:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR:
			case UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR:
			case UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR:
			case UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR:
			case UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UsecasepointPackage.Literals.USECASE_POINT__MANAGEMENT_FACTOR,
				 UsecasepointFactory.eINSTANCE.createManagementFactor()));

		newChildDescriptors.add
			(createChildParameter
				(UsecasepointPackage.Literals.USECASE_POINT__PRODUCTIVITY_FACTOR,
				 UsecasepointFactory.eINSTANCE.createProductivityFactor()));

		newChildDescriptors.add
			(createChildParameter
				(UsecasepointPackage.Literals.USECASE_POINT__REQUIREMENT_FACTOR,
				 UsecasepointFactory.eINSTANCE.createRequirementFactor()));

		newChildDescriptors.add
			(createChildParameter
				(UsecasepointPackage.Literals.USECASE_POINT__TECHNOLOGY_FACTOR,
				 UsecasepointFactory.eINSTANCE.createTechnologyFactor()));

		newChildDescriptors.add
			(createChildParameter
				(UsecasepointPackage.Literals.USECASE_POINT__COST_PER_USECASE_POINT,
				 PropertiesFactory.eINSTANCE.createProperty()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ElementPackage.Literals.ELEMENT__COMMENTLIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CHANGE_LIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CREATOR ||
			childFeature == ElementPackage.Literals.ELEMENT__RESPONSIBLE_USER ||
			childFeature == ElementPackage.Literals.ELEMENT__COST ||
			childFeature == ElementPackage.Literals.ELEMENT__BENEFIT ||
			childFeature == UsecasepointPackage.Literals.USECASE_POINT__COST_PER_USECASE_POINT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
