/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.group.provider;


import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.element.ElementFactory;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.document.DocumentFactory;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.group.GroupPackage;
import dk.dtu.imm.red.core.element.provider.ElementItemProvider;
import dk.dtu.imm.red.core.element.relationship.RelationshipFactory;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.project.ProjectFactory;
import dk.dtu.imm.red.core.provider.CoreEditPlugin;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointFactory;
import dk.dtu.imm.red.core.user.UserFactory;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

/**
 * This is the item provider adapter for a {@link dk.dtu.imm.red.core.element.group.Group} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class GroupItemProvider
	extends ElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(GroupPackage.Literals.GROUP__CONTENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Group)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Group_type") :
			getString("_UI_Group_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Group.class)) {
			case GroupPackage.GROUP__CONTENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 CommentFactory.eINSTANCE.createComment()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 CommentFactory.eINSTANCE.createIssueComment()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 ElementFactory.eINSTANCE.createCaptionedImage()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 RelationshipFactory.eINSTANCE.createElementReference()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 RelationshipFactory.eINSTANCE.createGlossaryEntryReference()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 DocumentFactory.eINSTANCE.createDocument()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 UserFactory.eINSTANCE.createUser()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 FolderFactory.eINSTANCE.createFolder()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 WorkspaceFactory.eINSTANCE.createWorkspace()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 FileFactory.eINSTANCE.createFile()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 ProjectFactory.eINSTANCE.createProject()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 UsecasepointFactory.eINSTANCE.createUsecasePoint()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 UsecasepointFactory.eINSTANCE.createWeightedFactor()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 UsecasepointFactory.eINSTANCE.createManagementFactor()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 UsecasepointFactory.eINSTANCE.createTechnologyFactor()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 UsecasepointFactory.eINSTANCE.createProductivityFactor()));

		newChildDescriptors.add
			(createChildParameter
				(GroupPackage.Literals.GROUP__CONTENTS,
				 UsecasepointFactory.eINSTANCE.createRequirementFactor()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ElementPackage.Literals.ELEMENT__COMMENTLIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CHANGE_LIST ||
			childFeature == ElementPackage.Literals.ELEMENT__CREATOR ||
			childFeature == ElementPackage.Literals.ELEMENT__RESPONSIBLE_USER ||
			childFeature == GroupPackage.Literals.GROUP__CONTENTS ||
			childFeature == ElementPackage.Literals.ELEMENT__RELATES_TO ||
			childFeature == ElementPackage.Literals.ELEMENT__COST ||
			childFeature == ElementPackage.Literals.ELEMENT__BENEFIT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return CoreEditPlugin.INSTANCE;
	}

}
