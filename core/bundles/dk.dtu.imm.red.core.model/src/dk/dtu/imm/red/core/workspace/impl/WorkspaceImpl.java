/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.workspace.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.impl.GroupImpl;
import dk.dtu.imm.red.core.element.util.FileHelper;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspacePackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Workspace</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.workspace.impl.WorkspaceImpl#getRecentlyOpenedFiles <em>Recently Opened Files</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.workspace.impl.WorkspaceImpl#getCurrentlyOpenedFiles <em>Currently Opened Files</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.workspace.impl.WorkspaceImpl#getRecentFilesMaxLength <em>Recent Files Max Length</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.workspace.impl.WorkspaceImpl#getPassword <em>Password</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.workspace.impl.WorkspaceImpl#getCurrentlyOpenedProjects <em>Currently Opened Projects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WorkspaceImpl extends GroupImpl implements Workspace {
	
	/**
	 * The cached value of the '{@link #getRecentlyOpenedFiles() <em>Recently Opened Files</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecentlyOpenedFiles()
	 * @generated
	 * @ordered
	 */
	protected EList<File> recentlyOpenedFiles;
	/**
	 * The cached value of the '{@link #getCurrentlyOpenedFiles() <em>Currently Opened Files</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentlyOpenedFiles()
	 * @generated
	 * @ordered
	 */
	protected EList<File> currentlyOpenedFiles;
	/**
	 * The default value of the '{@link #getRecentFilesMaxLength() <em>Recent Files Max Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecentFilesMaxLength()
	 * @generated
	 * @ordered
	 */
	protected static final int RECENT_FILES_MAX_LENGTH_EDEFAULT = 5;
	/**
	 * The cached value of the '{@link #getRecentFilesMaxLength() <em>Recent Files Max Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecentFilesMaxLength()
	 * @generated
	 * @ordered
	 */
	protected int recentFilesMaxLength = RECENT_FILES_MAX_LENGTH_EDEFAULT;
	/**
	 * The default value of the '{@link #getPassword() <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassword()
	 * @generated
	 * @ordered
	 */
	protected static final String PASSWORD_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getPassword() <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassword()
	 * @generated
	 * @ordered
	 */
	protected String password = PASSWORD_EDEFAULT;
	/**
	 * The cached value of the '{@link #getCurrentlyOpenedProjects() <em>Currently Opened Projects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentlyOpenedProjects()
	 * @generated
	 * @ordered
	 */
	protected EList<Project> currentlyOpenedProjects;
	/**
	 * Cache of elements, mapped by their unique ID.
	 * Used for quick lookup
	 * @generated NOT
	 */
	protected EList<Project> recentlyOpenedProjects;
	Map<String, Element> elementCache = new HashMap<String, Element>();
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected WorkspaceImpl() {
		super();
		
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.createResource(
				URI.createGenericURI("workspace", "workspace", "workspace"));
		resource.getContents().add(this);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkspacePackage.Literals.WORKSPACE;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<File> getRecentlyOpenedFiles() {
		if (recentlyOpenedFiles == null) {
			recentlyOpenedFiles = new EObjectResolvingEList<File>(File.class, this, WorkspacePackage.WORKSPACE__RECENTLY_OPENED_FILES);
		}
		return recentlyOpenedFiles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<File> getCurrentlyOpenedFiles() {
		if (currentlyOpenedFiles == null) {
			currentlyOpenedFiles = new EObjectResolvingEList<File>(File.class, this, WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_FILES);
		}
		return currentlyOpenedFiles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRecentFilesMaxLength() {
		return recentFilesMaxLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPassword(String newPassword) {
		String oldPassword = password;
		password = newPassword;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkspacePackage.WORKSPACE__PASSWORD, oldPassword, password));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Project> getCurrentlyOpenedProjects() {
		if (currentlyOpenedProjects == null) {
			currentlyOpenedProjects = new EObjectResolvingEList<Project>(Project.class, this, WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_PROJECTS);
		}
		return currentlyOpenedProjects;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */	
	public EList<Project> getRecentlyOpenedProjects() {
		if (recentlyOpenedProjects == null) {
			recentlyOpenedProjects = new EObjectResolvingEList<Project>(Project.class, this, WorkspacePackage.WORKSPACE__RECENTLY_OPENED_FILES);
		}
		return recentlyOpenedProjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addOpenedFile(final File file) {
		int index = -1;
		for (int i = 0; i  < getCurrentlyOpenedFiles().size(); i++) {
			File f = getCurrentlyOpenedFiles().get(i);
			if (FileHelper.getFilePath(f).equals(FileHelper.getFilePath(file))) {
				index = i;
			}
		}
		
		if (index != -1) {
			getCurrentlyOpenedFiles().remove(index);
		}
		
		getCurrentlyOpenedFiles().add(file);
		
		// If the file is already in the recent-files list, 
		// remove it and re-add it to the top
		index = -1;
		for (int i = 0; i < getRecentlyOpenedFiles().size(); i++) {
			File f = getRecentlyOpenedFiles().get(i);
			if (f.getUniqueID().equals(file.getUniqueID())) {
				index = i;
			}
		}
		
		if (index != -1) {
			getRecentlyOpenedFiles().remove(index);
		}
		
		getRecentlyOpenedFiles().add(0, file);
		
		file.setParent(this);
		
		while (getRecentlyOpenedFiles().size() > recentFilesMaxLength) {
			getRecentlyOpenedFiles().remove(recentFilesMaxLength);
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */	
	public void addOpenedProject(final Project project) {
		int index = -1;
		for (int i = 0; i  < getCurrentlyOpenedProjects().size(); i++) {
			Project p = getCurrentlyOpenedProjects().get(i);
			if (FileHelper.getFilePath(p).equals(FileHelper.getFilePath(project))) {
				index = i;
			}
		}
		
		if (index != -1) {
			getCurrentlyOpenedProjects().remove(index);
		}
		
		getCurrentlyOpenedProjects().add(project);
		
		project.setParent(this);
		while (getRecentlyOpenedProjects().size() > recentFilesMaxLength) {
			getRecentlyOpenedProjects().remove(recentFilesMaxLength);
			
		}
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkspacePackage.WORKSPACE__RECENTLY_OPENED_FILES:
				return getRecentlyOpenedFiles();
			case WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_FILES:
				return getCurrentlyOpenedFiles();
			case WorkspacePackage.WORKSPACE__RECENT_FILES_MAX_LENGTH:
				return getRecentFilesMaxLength();
			case WorkspacePackage.WORKSPACE__PASSWORD:
				return getPassword();
			case WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_PROJECTS:
				return getCurrentlyOpenedProjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkspacePackage.WORKSPACE__RECENTLY_OPENED_FILES:
				getRecentlyOpenedFiles().clear();
				getRecentlyOpenedFiles().addAll((Collection<? extends File>)newValue);
				return;
			case WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_FILES:
				getCurrentlyOpenedFiles().clear();
				getCurrentlyOpenedFiles().addAll((Collection<? extends File>)newValue);
				return;
			case WorkspacePackage.WORKSPACE__PASSWORD:
				setPassword((String)newValue);
				return;
			case WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_PROJECTS:
				getCurrentlyOpenedProjects().clear();
				getCurrentlyOpenedProjects().addAll((Collection<? extends Project>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkspacePackage.WORKSPACE__RECENTLY_OPENED_FILES:
				getRecentlyOpenedFiles().clear();
				return;
			case WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_FILES:
				getCurrentlyOpenedFiles().clear();
				return;
			case WorkspacePackage.WORKSPACE__PASSWORD:
				setPassword(PASSWORD_EDEFAULT);
				return;
			case WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_PROJECTS:
				getCurrentlyOpenedProjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkspacePackage.WORKSPACE__RECENTLY_OPENED_FILES:
				return recentlyOpenedFiles != null && !recentlyOpenedFiles.isEmpty();
			case WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_FILES:
				return currentlyOpenedFiles != null && !currentlyOpenedFiles.isEmpty();
			case WorkspacePackage.WORKSPACE__RECENT_FILES_MAX_LENGTH:
				return recentFilesMaxLength != RECENT_FILES_MAX_LENGTH_EDEFAULT;
			case WorkspacePackage.WORKSPACE__PASSWORD:
				return PASSWORD_EDEFAULT == null ? password != null : !PASSWORD_EDEFAULT.equals(password);
			case WorkspacePackage.WORKSPACE__CURRENTLY_OPENED_PROJECTS:
				return currentlyOpenedProjects != null && !currentlyOpenedProjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (recentFilesMaxLength: ");
		result.append(recentFilesMaxLength);
		result.append(", password: ");
		result.append(password);
		result.append(')');
		return result.toString();
	}

	/**
	 * See implementation in Group for details.
	 * @generated NOT
	 */
	@Override
	public Element findDescendantByUniqueID(final String uniqueID) {
		Element result = elementCache.get(uniqueID);
		
		// if the result is not contained in a resource or anything, then
		// it has been deleted, so remove it from the cache and don't return
		// it.
		if (result != null && (result.eResource() == null || result.eContainer() == null)) {
			elementCache.remove(uniqueID);
			result = null;
		}
		
		if (result == null) {
			result = super.findDescendantByUniqueID(uniqueID);
			elementCache.put(uniqueID, result);
		}
		
		return result;
	}
	
	@Override
	public void save() {
	}

	@Override
	public void addOpenedWorkspace(Workspace workspace) {
		// TODO Auto-generated method stub
		
	}

} // WorkspaceImpl
