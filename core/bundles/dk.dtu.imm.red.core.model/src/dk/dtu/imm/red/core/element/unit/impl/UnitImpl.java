/**
 */
package dk.dtu.imm.red.core.element.unit.impl;

import dk.dtu.imm.red.core.element.unit.Unit;
import dk.dtu.imm.red.core.element.unit.UnitPackage;
import dk.dtu.imm.red.core.element.unit.impl.UnitImpl;

import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.unit.impl.UnitImpl#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.unit.impl.UnitImpl#getConversionFactor <em>Conversion Factor</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class UnitImpl extends EObjectImpl implements Unit {
	/**
	 * The default value of the '{@link #getSymbol() <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected static final String SYMBOL_EDEFAULT = "units";

	/**
	 * The cached value of the '{@link #getSymbol() <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected String symbol = SYMBOL_EDEFAULT;

	/**
	 * The default value of the '{@link #getConversionFactor() <em>Conversion Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConversionFactor()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal CONVERSION_FACTOR_EDEFAULT = new BigDecimal("1");

	/**
	 * The cached value of the '{@link #getConversionFactor() <em>Conversion Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConversionFactor()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal conversionFactor = CONVERSION_FACTOR_EDEFAULT;

	/**
	 * @generated NOT
	 */
	private static final int MIN_VALUE_SCALE = 15;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.UNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(String newSymbol) {
		String oldSymbol = symbol;
		symbol = newSymbol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UnitPackage.UNIT__SYMBOL, oldSymbol, symbol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getConversionFactor() {
		return conversionFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConversionFactor(BigDecimal newConversionFactor) {
		BigDecimal oldConversionFactor = conversionFactor;
		conversionFactor = newConversionFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UnitPackage.UNIT__CONVERSION_FACTOR, oldConversionFactor, conversionFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public abstract EList<Unit> getSiblingUnits();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isCompatibleWith(Unit that) {
		return this.getClass() == that.getClass();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public BigDecimal convertTo(BigDecimal value, Unit outputUnit) {
		if (!this.isCompatibleWith(outputUnit)) {
			throw new IllegalArgumentException(
					String.format("Units %s and %s are not compatible.", this.toString(), outputUnit.toString()));
		}
		
		BigDecimal thisConvFactor = ((UnitImpl) this).getConversionFactor();
		BigDecimal thatConvFactor = ((UnitImpl) outputUnit).getConversionFactor();

		int scale = getCalculationScale(thisConvFactor.scale(), thatConvFactor.scale());

		BigDecimal baseValue = thisConvFactor.multiply(value);
		BigDecimal result = baseValue.divide(thatConvFactor, scale, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UnitPackage.UNIT__SYMBOL:
				return getSymbol();
			case UnitPackage.UNIT__CONVERSION_FACTOR:
				return getConversionFactor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UnitPackage.UNIT__SYMBOL:
				setSymbol((String)newValue);
				return;
			case UnitPackage.UNIT__CONVERSION_FACTOR:
				setConversionFactor((BigDecimal)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UnitPackage.UNIT__SYMBOL:
				setSymbol(SYMBOL_EDEFAULT);
				return;
			case UnitPackage.UNIT__CONVERSION_FACTOR:
				setConversionFactor(CONVERSION_FACTOR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UnitPackage.UNIT__SYMBOL:
				return SYMBOL_EDEFAULT == null ? symbol != null : !SYMBOL_EDEFAULT.equals(symbol);
			case UnitPackage.UNIT__CONVERSION_FACTOR:
				return CONVERSION_FACTOR_EDEFAULT == null ? conversionFactor != null : !CONVERSION_FACTOR_EDEFAULT.equals(conversionFactor);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (symbol: ");
		result.append(symbol);
		result.append(", conversionFactor: ");
		result.append(conversionFactor);
		result.append(')');
		return result.toString();
	}

	/**
	 * @generated NOT
	 */
	protected int getCalculationScale(int leftScale, int rightScale) {
		int scale = MIN_VALUE_SCALE;
		scale = Math.max(leftScale, scale);
		scale = Math.max(rightScale, scale);

		return scale;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Unit) {
			Unit that = (Unit) obj;
			
			if (this.getSymbol().equals(that.getSymbol())
					&& this.getConversionFactor().equals(that.getConversionFactor())) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public int hashCode() {
		return 17 + getSymbol().hashCode() + getConversionFactor().hashCode();
	}

} //UnitImpl
