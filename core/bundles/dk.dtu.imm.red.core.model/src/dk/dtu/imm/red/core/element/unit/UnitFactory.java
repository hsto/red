/**
 */
package dk.dtu.imm.red.core.element.unit;

import org.eclipse.emf.ecore.EFactory;

import dk.dtu.imm.red.core.element.unit.DivisionalUnit;
import dk.dtu.imm.red.core.element.unit.Unit;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.element.unit.UnitPackage
 * @generated
 */
public interface UnitFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UnitFactory eINSTANCE = dk.dtu.imm.red.core.element.unit.impl.UnitFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Base Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Unit</em>'.
	 * @generated
	 */
	BaseUnit createBaseUnit();

	/**
	 * Returns a new object of class '<em>Time Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time Unit</em>'.
	 * @generated
	 */
	TimeUnit createTimeUnit();

	/**
	 * Returns a new object of class '<em>Size Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Size Unit</em>'.
	 * @generated
	 */
	SizeUnit createSizeUnit();

	/**
	 * Returns a new object of class '<em>Divisional Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Divisional Unit</em>'.
	 * @generated
	 */
	DivisionalUnit createDivisionalUnit();

	/**
	 * @generated NOT
	 */
	DivisionalUnit createDivisionalUnit(Unit nominatorUnit, Unit denominatorUnit);

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UnitPackage getUnitPackage();

} //UnitFactory
