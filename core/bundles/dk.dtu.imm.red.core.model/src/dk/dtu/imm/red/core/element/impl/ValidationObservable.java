package dk.dtu.imm.red.core.element.impl;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.imm.red.core.element.Element;

public class ValidationObservable {
	
	private static ValidationObservable instance;
	private List<ValidationObserver> observers = new ArrayList<ValidationObserver>();
	
	
	private ValidationObservable() {
		
	}
	
	public static ValidationObservable getInstance() {
		
		if(instance== null) {
			instance = new ValidationObservable();
		}
		
		return instance;
	}
	
	public void addObserver(ValidationObserver o) {
		observers.add(o);
	}
	
	public void modelChanged(Element e) {
		 for(ValidationObserver o : observers) {
			 o.modelChanged(e);
		 }
	}

	 

}
