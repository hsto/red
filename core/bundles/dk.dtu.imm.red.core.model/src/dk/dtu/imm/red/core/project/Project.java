/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.project;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.ResourceSet;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.element.*;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.project.Project#getUsers <em>Users</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.Project#getProjectDates <em>Project Dates</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.Project#getInformation <em>Information</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.Project#getListofopenedfilesinproject <em>Listofopenedfilesinproject</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.Project#getEffort <em>Effort</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.Project#getLicense <em>License</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.project.ProjectPackage#getProject()
 * @model
 * @generated
 */
public interface Project extends Group {

	/**
	 * Returns the value of the '<em><b>Users</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.project.UserTableEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Users</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Users</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.project.ProjectPackage#getProject_Users()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<UserTableEntry> getUsers();

	/**
	 * Returns the value of the '<em><b>Project Dates</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.project.ProjectDate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Dates</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project Dates</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.project.ProjectPackage#getProject_ProjectDates()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<ProjectDate> getProjectDates();

	/**
	 * Returns the value of the '<em><b>Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Information</em>' containment reference.
	 * @see #setInformation(Text)
	 * @see dk.dtu.imm.red.core.project.ProjectPackage#getProject_Information()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getInformation();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.project.Project#getInformation <em>Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Information</em>' containment reference.
	 * @see #getInformation()
	 * @generated
	 */
	void setInformation(Text value);

	/**
	 * Returns the value of the '<em><b>Listofopenedfilesinproject</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.file.File}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Listofopenedfilesinproject</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Listofopenedfilesinproject</em>' reference list.
	 * @see dk.dtu.imm.red.core.project.ProjectPackage#getProject_Listofopenedfilesinproject()
	 * @model
	 * @generated
	 */
	EList<File> getListofopenedfilesinproject();

	/**
	 * Returns the value of the '<em><b>Effort</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Effort</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Effort</em>' containment reference.
	 * @see #setEffort(UsecasePoint)
	 * @see dk.dtu.imm.red.core.project.ProjectPackage#getProject_Effort()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	UsecasePoint getEffort();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.project.Project#getEffort <em>Effort</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Effort</em>' containment reference.
	 * @see #getEffort()
	 * @generated
	 */
	void setEffort(UsecasePoint value);
	
	/**
	 * Returns the value of the '<em><b>License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>License</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>License</em>' attribute.
	 * @see #setLicense(String)
	 * @see dk.dtu.imm.red.core.project.ProjectPackage#getProject_License()
	 * @model
	 * @generated
	 */
	String getLicense();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.project.Project#getLicense <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>License</em>' attribute.
	 * @see #getLicense()
	 * @generated
	 */
	void setLicense(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void addToListOfFilesInProject(File fileToAdd);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void createDefaultUsecasePoint();

	 
	
} // Project
