/**
 */
package dk.dtu.imm.red.core.element;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Captioned Image List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.CaptionedImageList#getCaptionedImage <em>Captioned Image</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.ElementPackage#getCaptionedImageList()
 * @model
 * @generated
 */
public interface CaptionedImageList extends EObject {
	/**
	 * Returns the value of the '<em><b>Captioned Image</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.element.CaptionedImage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Captioned Image</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Captioned Image</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getCaptionedImageList_CaptionedImage()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<CaptionedImage> getCaptionedImage();

} // CaptionedImageList
