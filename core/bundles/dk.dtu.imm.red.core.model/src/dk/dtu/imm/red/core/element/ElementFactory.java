/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.element.ElementPackage
 * @generated
 */
public interface ElementFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	ElementFactory eINSTANCE = dk.dtu.imm.red.core.element.impl.ElementFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Custom Attribute</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Custom Attribute</em>'.
	 * @generated
	 */
	<T> CustomAttribute<T> createCustomAttribute();

	/**
	 * Returns a new object of class '<em>Search Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Search Parameter</em>'.
	 * @generated
	 */
	SearchParameter createSearchParameter();

	/**
	 * Returns a new object of class '<em>Search Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Search Result</em>'.
	 * @generated
	 */
	SearchResult createSearchResult();

	/**
	 * Returns a new object of class '<em>Search Result Wrapper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Search Result Wrapper</em>'.
	 * @generated
	 */
	SearchResultWrapper createSearchResultWrapper();

	/**
	 * Returns a new object of class '<em>Captioned Image</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Captioned Image</em>'.
	 * @generated
	 */
	CaptionedImage createCaptionedImage();

	/**
	 * Returns a new object of class '<em>Captioned Image List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Captioned Image List</em>'.
	 * @generated
	 */
	CaptionedImageList createCaptionedImageList();

	/**
	 * Returns a new object of class '<em>Deletion Listener</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deletion Listener</em>'.
	 * @generated
	 */
	DeletionListener createDeletionListener();

	/**
	 * Returns a new object of class '<em>Quantity Item</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quantity Item</em>'.
	 * @generated
	 */
	QuantityItem createQuantityItem();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ElementPackage getElementPackage();

} // ElementFactory
