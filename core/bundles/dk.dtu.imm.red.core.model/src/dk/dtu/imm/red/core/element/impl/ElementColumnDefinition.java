package dk.dtu.imm.red.core.element.impl; 

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EReference;

public class ElementColumnDefinition {
	
	public enum ColumnBehaviour {
		CellEdit,
		ComboSelector,
		ReferenceSelector,
		IconDisplay,
		ReferenceDisplay
	}
	
	private String columnName = "Item";
	private Class<?> columnType = String.class;
	private ColumnBehaviour columnBehaviour = ColumnBehaviour.CellEdit;  

	private Integer columnWidth = 100;
	private boolean editable = true;   
	private String[] getterMethod = null;
	private String[] referenceGetterMethod = null;
	private String[] cellItems = new String[] {};  
	private boolean handleCellEditManually = false;  
	private CellHandler<?> cellHandler = null;
	private String attributeName; 

	public static String[] enumToStringArray(@SuppressWarnings("rawtypes") Enum[] values) {
	    
	    return enumToStringArray(values, false);
	}
	
	public static String[] enumToStringArray(@SuppressWarnings("rawtypes") Enum[] values, boolean useLiterals) {
	    
	    String[] results = new String[values.length];

	    for (int i = 0; i < values.length; i++) {
	    	
	    	if (useLiterals) results[i] = values[i].toString();
	    	else results[i] = values[i].name();
	    }

	    return results;
	}

	public CellHandler<?> getCellHandler() {
		return cellHandler;
	}

	public ElementColumnDefinition setCellHandler(CellHandler<?> cellHandler) {
		this.cellHandler = cellHandler;
		return this;
	}

	public String getColumnName() {
		return columnName;
	}
	
	public ElementColumnDefinition setHeaderName(String columnName) {
		this.columnName = columnName;
		return this;
	}
	public Class<?> getColumnType() {
		return columnType;
	}
	
	public ElementColumnDefinition setColumnType(Class<?> columnType) {
		this.columnType = columnType;
		return this;
	}
	
	public Integer getColumnWidth() {
		return columnWidth;
	}
	
	public ElementColumnDefinition setColumnWidth(Integer columnWidth) {
		this.columnWidth = columnWidth;
		return this;
	} 
	
	public boolean isEditable() {
		return editable;
	}

	public ElementColumnDefinition setEditable(boolean editable) {
		this.editable = editable;
		return this;
	}  

	public String[] getGetterMethod() {
		return getterMethod;
	}

	public ElementColumnDefinition setGetterMethod(String[] getterMethod) {
		this.getterMethod = getterMethod;
		return this;
	}  

	public String[] getReferenceGetterMethod() {
		return referenceGetterMethod;
	}

	public ElementColumnDefinition setReferenceGetterMethod(String[] getterMethod) {
		this.referenceGetterMethod = getterMethod;
		return this;
	}
	
	public String[] getCellItems() {
		return cellItems;
	}

	public ElementColumnDefinition setCellItems(String[] cellItems) {
		this.cellItems = cellItems;
		return this;
	}
	
	public boolean isHandleCellEditManually() {
		return handleCellEditManually;
	}

	public ElementColumnDefinition setHandleCellEditManually(boolean handleCellEditManually) {
		this.handleCellEditManually = handleCellEditManually;
		return this;
	}
	
	public ColumnBehaviour getColumnBehaviour() {
		return columnBehaviour;
	}

	public ElementColumnDefinition setColumnBehaviour(ColumnBehaviour columnBehaviour) {
		this.columnBehaviour = columnBehaviour;
		return this;
	}

	public ElementColumnDefinition setEMFLiteral(EAttribute ea) {
		this.attributeName = ea.getName();
		this.columnType = ea.getEAttributeType().getInstanceClass();
		return this;
	}
	
	public ElementColumnDefinition setEMFLiteral(EEnum ea) {
		this.attributeName = ea.getName();
		this.columnType = String.class;
		return this;
	}
	
	public ElementColumnDefinition setEMFLiteral(EReference ef) {
		this.attributeName = ef.getName();
		this.columnType = ef.getEType().getInstanceClass();
		return this;
	}
	
	public String getAttributeName() {
		return attributeName;
	}
}
