/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.id;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.element.id.IdFactory
 * @model kind="package"
 * @generated
 */
public interface IdPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "id";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.core.element.id";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "id";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	IdPackage eINSTANCE = dk.dtu.imm.red.core.element.id.impl.IdPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.id.impl.VisibleElementIDImpl <em>Visible Element ID</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.id.impl.VisibleElementIDImpl
	 * @see dk.dtu.imm.red.core.element.id.impl.IdPackageImpl#getVisibleElementID()
	 * @generated
	 */
	int VISIBLE_ELEMENT_ID = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int VISIBLE_ELEMENT_ID__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int VISIBLE_ELEMENT_ID__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Visible Element ID</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISIBLE_ELEMENT_ID_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.id.VisibleIDType <em>Visible ID Type</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.id.VisibleIDType
	 * @see dk.dtu.imm.red.core.element.id.impl.IdPackageImpl#getVisibleIDType()
	 * @generated
	 */
	int VISIBLE_ID_TYPE = 1;

	/**
	 * Returns the meta object for class '
	 * {@link dk.dtu.imm.red.core.element.id.VisibleElementID
	 * <em>Visible Element ID</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Visible Element ID</em>'.
	 * @see dk.dtu.imm.red.core.element.id.VisibleElementID
	 * @generated
	 */
	EClass getVisibleElementID();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.id.VisibleElementID#getType <em>Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dk.dtu.imm.red.core.element.id.VisibleElementID#getType()
	 * @see #getVisibleElementID()
	 * @generated
	 */
	EAttribute getVisibleElementID_Type();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.id.VisibleElementID#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see dk.dtu.imm.red.core.element.id.VisibleElementID#getValue()
	 * @see #getVisibleElementID()
	 * @generated
	 */
	EAttribute getVisibleElementID_Value();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.element.id.VisibleIDType <em>Visible ID Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Visible ID Type</em>'.
	 * @see dk.dtu.imm.red.core.element.id.VisibleIDType
	 * @generated
	 */
	EEnum getVisibleIDType();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IdFactory getIdFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.id.impl.VisibleElementIDImpl <em>Visible Element ID</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.id.impl.VisibleElementIDImpl
		 * @see dk.dtu.imm.red.core.element.id.impl.IdPackageImpl#getVisibleElementID()
		 * @generated
		 */
		EClass VISIBLE_ELEMENT_ID = eINSTANCE.getVisibleElementID();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISIBLE_ELEMENT_ID__TYPE = eINSTANCE.getVisibleElementID_Type();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISIBLE_ELEMENT_ID__VALUE = eINSTANCE.getVisibleElementID_Value();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.id.VisibleIDType <em>Visible ID Type</em>}' enum.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.id.VisibleIDType
		 * @see dk.dtu.imm.red.core.element.id.impl.IdPackageImpl#getVisibleIDType()
		 * @generated
		 */
		EEnum VISIBLE_ID_TYPE = eINSTANCE.getVisibleIDType();

	}

} // IdPackage
