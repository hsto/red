/**
 */
package dk.dtu.imm.red.core.usecasepoint.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.properties.Property;
import dk.dtu.imm.red.core.usecasepoint.ManagementFactor;
import dk.dtu.imm.red.core.usecasepoint.ProductivityFactor;
import dk.dtu.imm.red.core.usecasepoint.RequirementFactor;
import dk.dtu.imm.red.core.usecasepoint.TechnologyFactor;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointFactory;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Usecase Point</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.UsecasePointImpl#getManagementFactor <em>Management Factor</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.UsecasePointImpl#getProductivityFactor <em>Productivity Factor</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.UsecasePointImpl#getRequirementFactor <em>Requirement Factor</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.UsecasePointImpl#getTechnologyFactor <em>Technology Factor</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.UsecasePointImpl#getCostPerUsecasePoint <em>Cost Per Usecase Point</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.UsecasePointImpl#getTotalCost <em>Total Cost</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.UsecasePointImpl#getUsecasePoints <em>Usecase Points</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.UsecasePointImpl#getTotalCostManual <em>Total Cost Manual</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.UsecasePointImpl#getTotalBenefitManual <em>Total Benefit Manual</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UsecasePointImpl extends FactorImpl implements UsecasePoint {
	/**
	 * The cached value of the '{@link #getManagementFactor() <em>Management Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManagementFactor()
	 * @generated
	 * @ordered
	 */
	protected ManagementFactor managementFactor;

	/**
	 * The cached value of the '{@link #getProductivityFactor() <em>Productivity Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProductivityFactor()
	 * @generated
	 * @ordered
	 */
	protected ProductivityFactor productivityFactor;

	/**
	 * The cached value of the '{@link #getRequirementFactor() <em>Requirement Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequirementFactor()
	 * @generated
	 * @ordered
	 */
	protected RequirementFactor requirementFactor;

	/**
	 * The cached value of the '{@link #getTechnologyFactor() <em>Technology Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTechnologyFactor()
	 * @generated
	 * @ordered
	 */
	protected TechnologyFactor technologyFactor;

	/**
	 * The cached value of the '{@link #getCostPerUsecasePoint() <em>Cost Per Usecase Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCostPerUsecasePoint()
	 * @generated
	 * @ordered
	 */
	protected Property costPerUsecasePoint;

	/**
	 * The default value of the '{@link #getTotalCost() <em>Total Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalCost()
	 * @generated
	 * @ordered
	 */
	protected static final double TOTAL_COST_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getTotalCost() <em>Total Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalCost()
	 * @generated
	 * @ordered
	 */
	protected double totalCost = TOTAL_COST_EDEFAULT;

	/**
	 * The default value of the '{@link #getUsecasePoints() <em>Usecase Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsecasePoints()
	 * @generated
	 * @ordered
	 */
	protected static final double USECASE_POINTS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getUsecasePoints() <em>Usecase Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsecasePoints()
	 * @generated
	 * @ordered
	 */
	protected double usecasePoints = USECASE_POINTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTotalCostManual() <em>Total Cost Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalCostManual()
	 * @generated
	 * @ordered
	 */
	protected static final double TOTAL_COST_MANUAL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getTotalCostManual() <em>Total Cost Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalCostManual()
	 * @generated
	 * @ordered
	 */
	protected double totalCostManual = TOTAL_COST_MANUAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getTotalBenefitManual() <em>Total Benefit Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalBenefitManual()
	 * @generated
	 * @ordered
	 */
	protected static final double TOTAL_BENEFIT_MANUAL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getTotalBenefitManual() <em>Total Benefit Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalBenefitManual()
	 * @generated
	 * @ordered
	 */
	protected double totalBenefitManual = TOTAL_BENEFIT_MANUAL_EDEFAULT;

	private Project project;

	private double effort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected UsecasePointImpl() {
		super(); 
	}  

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasepointPackage.Literals.USECASE_POINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ManagementFactor getManagementFactor() {
		if (managementFactor != null && managementFactor.eIsProxy()) {
			InternalEObject oldManagementFactor = (InternalEObject)managementFactor;
			managementFactor = (ManagementFactor)eResolveProxy(oldManagementFactor);
			if (managementFactor != oldManagementFactor) {
				InternalEObject newManagementFactor = (InternalEObject)managementFactor;
				NotificationChain msgs = oldManagementFactor.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR, null, null);
				if (newManagementFactor.eInternalContainer() == null) {
					msgs = newManagementFactor.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR, oldManagementFactor, managementFactor));
			}
		}
		return managementFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ManagementFactor basicGetManagementFactor() {
		return managementFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetManagementFactor(ManagementFactor newManagementFactor, NotificationChain msgs) {
		ManagementFactor oldManagementFactor = managementFactor;
		managementFactor = newManagementFactor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR, oldManagementFactor, newManagementFactor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setManagementFactor(ManagementFactor newManagementFactor) {
		if (newManagementFactor != managementFactor) {
			NotificationChain msgs = null;
			if (managementFactor != null)
				msgs = ((InternalEObject)managementFactor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR, null, msgs);
			if (newManagementFactor != null)
				msgs = ((InternalEObject)newManagementFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR, null, msgs);
			msgs = basicSetManagementFactor(newManagementFactor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR, newManagementFactor, newManagementFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductivityFactor getProductivityFactor() {
		if (productivityFactor != null && productivityFactor.eIsProxy()) {
			InternalEObject oldProductivityFactor = (InternalEObject)productivityFactor;
			productivityFactor = (ProductivityFactor)eResolveProxy(oldProductivityFactor);
			if (productivityFactor != oldProductivityFactor) {
				InternalEObject newProductivityFactor = (InternalEObject)productivityFactor;
				NotificationChain msgs = oldProductivityFactor.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR, null, null);
				if (newProductivityFactor.eInternalContainer() == null) {
					msgs = newProductivityFactor.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR, oldProductivityFactor, productivityFactor));
			}
		}
		return productivityFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductivityFactor basicGetProductivityFactor() {
		return productivityFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProductivityFactor(ProductivityFactor newProductivityFactor, NotificationChain msgs) {
		ProductivityFactor oldProductivityFactor = productivityFactor;
		productivityFactor = newProductivityFactor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR, oldProductivityFactor, newProductivityFactor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProductivityFactor(ProductivityFactor newProductivityFactor) {
		if (newProductivityFactor != productivityFactor) {
			NotificationChain msgs = null;
			if (productivityFactor != null)
				msgs = ((InternalEObject)productivityFactor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR, null, msgs);
			if (newProductivityFactor != null)
				msgs = ((InternalEObject)newProductivityFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR, null, msgs);
			msgs = basicSetProductivityFactor(newProductivityFactor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR, newProductivityFactor, newProductivityFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementFactor getRequirementFactor() {
		if (requirementFactor != null && requirementFactor.eIsProxy()) {
			InternalEObject oldRequirementFactor = (InternalEObject)requirementFactor;
			requirementFactor = (RequirementFactor)eResolveProxy(oldRequirementFactor);
			if (requirementFactor != oldRequirementFactor) {
				InternalEObject newRequirementFactor = (InternalEObject)requirementFactor;
				NotificationChain msgs = oldRequirementFactor.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR, null, null);
				if (newRequirementFactor.eInternalContainer() == null) {
					msgs = newRequirementFactor.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR, oldRequirementFactor, requirementFactor));
			}
		}
		return requirementFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementFactor basicGetRequirementFactor() {
		return requirementFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequirementFactor(RequirementFactor newRequirementFactor, NotificationChain msgs) {
		RequirementFactor oldRequirementFactor = requirementFactor;
		requirementFactor = newRequirementFactor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR, oldRequirementFactor, newRequirementFactor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequirementFactor(RequirementFactor newRequirementFactor) {
		if (newRequirementFactor != requirementFactor) {
			NotificationChain msgs = null;
			if (requirementFactor != null)
				msgs = ((InternalEObject)requirementFactor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR, null, msgs);
			if (newRequirementFactor != null)
				msgs = ((InternalEObject)newRequirementFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR, null, msgs);
			msgs = basicSetRequirementFactor(newRequirementFactor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR, newRequirementFactor, newRequirementFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TechnologyFactor getTechnologyFactor() {
		if (technologyFactor != null && technologyFactor.eIsProxy()) {
			InternalEObject oldTechnologyFactor = (InternalEObject)technologyFactor;
			technologyFactor = (TechnologyFactor)eResolveProxy(oldTechnologyFactor);
			if (technologyFactor != oldTechnologyFactor) {
				InternalEObject newTechnologyFactor = (InternalEObject)technologyFactor;
				NotificationChain msgs = oldTechnologyFactor.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR, null, null);
				if (newTechnologyFactor.eInternalContainer() == null) {
					msgs = newTechnologyFactor.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR, oldTechnologyFactor, technologyFactor));
			}
		}
		return technologyFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TechnologyFactor basicGetTechnologyFactor() {
		return technologyFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTechnologyFactor(TechnologyFactor newTechnologyFactor, NotificationChain msgs) {
		TechnologyFactor oldTechnologyFactor = technologyFactor;
		technologyFactor = newTechnologyFactor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR, oldTechnologyFactor, newTechnologyFactor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTechnologyFactor(TechnologyFactor newTechnologyFactor) {
		if (newTechnologyFactor != technologyFactor) {
			NotificationChain msgs = null;
			if (technologyFactor != null)
				msgs = ((InternalEObject)technologyFactor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR, null, msgs);
			if (newTechnologyFactor != null)
				msgs = ((InternalEObject)newTechnologyFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR, null, msgs);
			msgs = basicSetTechnologyFactor(newTechnologyFactor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR, newTechnologyFactor, newTechnologyFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getCostPerUsecasePoint() {
		if (costPerUsecasePoint != null && costPerUsecasePoint.eIsProxy()) {
			InternalEObject oldCostPerUsecasePoint = (InternalEObject)costPerUsecasePoint;
			costPerUsecasePoint = (Property)eResolveProxy(oldCostPerUsecasePoint);
			if (costPerUsecasePoint != oldCostPerUsecasePoint) {
				InternalEObject newCostPerUsecasePoint = (InternalEObject)costPerUsecasePoint;
				NotificationChain msgs = oldCostPerUsecasePoint.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT, null, null);
				if (newCostPerUsecasePoint.eInternalContainer() == null) {
					msgs = newCostPerUsecasePoint.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT, oldCostPerUsecasePoint, costPerUsecasePoint));
			}
		}
		return costPerUsecasePoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetCostPerUsecasePoint() {
		return costPerUsecasePoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCostPerUsecasePoint(Property newCostPerUsecasePoint, NotificationChain msgs) {
		Property oldCostPerUsecasePoint = costPerUsecasePoint;
		costPerUsecasePoint = newCostPerUsecasePoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT, oldCostPerUsecasePoint, newCostPerUsecasePoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCostPerUsecasePoint(Property newCostPerUsecasePoint) {
		if (newCostPerUsecasePoint != costPerUsecasePoint) {
			NotificationChain msgs = null;
			if (costPerUsecasePoint != null)
				msgs = ((InternalEObject)costPerUsecasePoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT, null, msgs);
			if (newCostPerUsecasePoint != null)
				msgs = ((InternalEObject)newCostPerUsecasePoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT, null, msgs);
			msgs = basicSetCostPerUsecasePoint(newCostPerUsecasePoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT, newCostPerUsecasePoint, newCostPerUsecasePoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * To avoid computation overhead Do not call this directly. Use getSum(), and then call this. 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTotalCost() {
		return totalCost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTotalCost(double newTotalCost) {
		double oldTotalCost = totalCost;
		totalCost = newTotalCost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__TOTAL_COST, oldTotalCost, totalCost));
	}
 
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getUsecasePoints() {
		return usecasePoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsecasePoints(double newUsecasePoints) {
		double oldUsecasePoints = usecasePoints;
		usecasePoints = newUsecasePoints;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__USECASE_POINTS, oldUsecasePoints, usecasePoints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTotalCostManual() {
		return totalCostManual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTotalCostManual(double newTotalCostManual) {
		double oldTotalCostManual = totalCostManual;
		totalCostManual = newTotalCostManual;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__TOTAL_COST_MANUAL, oldTotalCostManual, totalCostManual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTotalBenefitManual() {
		return totalBenefitManual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTotalBenefitManual(double newTotalBenefitManual) {
		double oldTotalBenefitManual = totalBenefitManual;
		totalBenefitManual = newTotalBenefitManual;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.USECASE_POINT__TOTAL_BENEFIT_MANUAL, oldTotalBenefitManual, totalBenefitManual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR:
				return basicSetManagementFactor(null, msgs);
			case UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR:
				return basicSetProductivityFactor(null, msgs);
			case UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR:
				return basicSetRequirementFactor(null, msgs);
			case UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR:
				return basicSetTechnologyFactor(null, msgs);
			case UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT:
				return basicSetCostPerUsecasePoint(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR:
				if (resolve) return getManagementFactor();
				return basicGetManagementFactor();
			case UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR:
				if (resolve) return getProductivityFactor();
				return basicGetProductivityFactor();
			case UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR:
				if (resolve) return getRequirementFactor();
				return basicGetRequirementFactor();
			case UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR:
				if (resolve) return getTechnologyFactor();
				return basicGetTechnologyFactor();
			case UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT:
				if (resolve) return getCostPerUsecasePoint();
				return basicGetCostPerUsecasePoint();
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST:
				return getTotalCost();
			case UsecasepointPackage.USECASE_POINT__USECASE_POINTS:
				return getUsecasePoints();
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST_MANUAL:
				return getTotalCostManual();
			case UsecasepointPackage.USECASE_POINT__TOTAL_BENEFIT_MANUAL:
				return getTotalBenefitManual();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR:
				setManagementFactor((ManagementFactor)newValue);
				return;
			case UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR:
				setProductivityFactor((ProductivityFactor)newValue);
				return;
			case UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR:
				setRequirementFactor((RequirementFactor)newValue);
				return;
			case UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR:
				setTechnologyFactor((TechnologyFactor)newValue);
				return;
			case UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT:
				setCostPerUsecasePoint((Property)newValue);
				return;
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST:
				setTotalCost((Double)newValue);
				return;
			case UsecasepointPackage.USECASE_POINT__USECASE_POINTS:
				setUsecasePoints((Double)newValue);
				return;
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST_MANUAL:
				setTotalCostManual((Double)newValue);
				return;
			case UsecasepointPackage.USECASE_POINT__TOTAL_BENEFIT_MANUAL:
				setTotalBenefitManual((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR:
				setManagementFactor((ManagementFactor)null);
				return;
			case UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR:
				setProductivityFactor((ProductivityFactor)null);
				return;
			case UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR:
				setRequirementFactor((RequirementFactor)null);
				return;
			case UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR:
				setTechnologyFactor((TechnologyFactor)null);
				return;
			case UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT:
				setCostPerUsecasePoint((Property)null);
				return;
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST:
				setTotalCost(TOTAL_COST_EDEFAULT);
				return;
			case UsecasepointPackage.USECASE_POINT__USECASE_POINTS:
				setUsecasePoints(USECASE_POINTS_EDEFAULT);
				return;
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST_MANUAL:
				setTotalCostManual(TOTAL_COST_MANUAL_EDEFAULT);
				return;
			case UsecasepointPackage.USECASE_POINT__TOTAL_BENEFIT_MANUAL:
				setTotalBenefitManual(TOTAL_BENEFIT_MANUAL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasepointPackage.USECASE_POINT__MANAGEMENT_FACTOR:
				return managementFactor != null;
			case UsecasepointPackage.USECASE_POINT__PRODUCTIVITY_FACTOR:
				return productivityFactor != null;
			case UsecasepointPackage.USECASE_POINT__REQUIREMENT_FACTOR:
				return requirementFactor != null;
			case UsecasepointPackage.USECASE_POINT__TECHNOLOGY_FACTOR:
				return technologyFactor != null;
			case UsecasepointPackage.USECASE_POINT__COST_PER_USECASE_POINT:
				return costPerUsecasePoint != null;
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST:
				return totalCost != TOTAL_COST_EDEFAULT;
			case UsecasepointPackage.USECASE_POINT__USECASE_POINTS:
				return usecasePoints != USECASE_POINTS_EDEFAULT;
			case UsecasepointPackage.USECASE_POINT__TOTAL_COST_MANUAL:
				return totalCostManual != TOTAL_COST_MANUAL_EDEFAULT;
			case UsecasepointPackage.USECASE_POINT__TOTAL_BENEFIT_MANUAL:
				return totalBenefitManual != TOTAL_BENEFIT_MANUAL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (totalCost: ");
		result.append(totalCost);
		result.append(", usecasePoints: ");
		result.append(usecasePoints);
		result.append(", totalCostManual: ");
		result.append(totalCostManual);
		result.append(", totalBenefitManual: ");
		result.append(totalBenefitManual);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */ 
	@Override 
	public double getSum() { 
		double m = getManagementFactor().getSum();
		double t = getTechnologyFactor().getSum();
		double r = getRequirementFactor().getSum();
		double p = getProductivityFactor().getSum();
		
		double effort = m * t * r * p; 
		setTotalCost(effort * getCostPerUsecasePoint().getValue());
		
		
		return effort; 
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */ 
	@Override 
	public double getSum(Map<String, List<Element>> requirementFactorElementMap) { 
		
		//Standard factors
		double m = getManagementFactor().getSum();
		double t = getTechnologyFactor().getSum();
		double r = getRequirementFactor().getSum(requirementFactorElementMap);
		double p = getProductivityFactor().getSum(); 
		
		//Additional computations (cost, manual cost etc.)
		usecasePoints = m * t * r;
		effort = usecasePoints * p;  
		setTotalCost(effort * getCostPerUsecasePoint().getValue());
		setTotalCostManual(calculateManualCost(requirementFactorElementMap)); 
		setTotalBenefitManual(calculateManualBenefit(requirementFactorElementMap));
		return effort; 
	} 
	
	/** 
	 * @generated NOT
	 */ 
	private double calculateManualCost(Map<String, List<Element>> requirementFactorElementMap) {
		
		double sum = 0;
		
		for(List<Element> elements : requirementFactorElementMap.values()) {
			for(Element el : elements) {
				sum += el.getCost().getValue();
			}
		}
		
		return sum;
	}
	
	/** 
	 * @generated NOT
	 */ 
	private double calculateManualBenefit(Map<String, List<Element>> requirementFactorElementMap) {
		
		double sum = 0;
		
		for(List<Element> elements : requirementFactorElementMap.values()) {
			for(Element el : elements) {
				sum += el.getBenefit().getValue();
			}
		}
		
		return sum;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Map<String, List<Element>> getElementMap(List<Element> contents) {
		Map<String, List<Element>> elementMap = new HashMap<String, List<Element>>();
		getEClassOfSpecialTypeRec(elementMap, contents, true);
		return elementMap;
	}  

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private void getEClassOfSpecialTypeRec(Map<String, List<Element>> elementMap,  List<Element> content, boolean recursive) { 
		
		for(Object o : content.toArray()) {  
			
			String className = o.getClass().getName();
			if(elementMap.get(className) == null) elementMap.put(className, new ArrayList<Element>());
			
			elementMap.get(className).add((Element) o); 
			
			if(recursive && o instanceof Group) {
				getEClassOfSpecialTypeRec(elementMap, ((Group) o).getContents(), recursive);
			}
		} 
	} 

} //UsecasePointImpl
