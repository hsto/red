/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.file.impl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;

import dk.dtu.imm.red.core.element.group.impl.GroupImpl;
import dk.dtu.imm.red.core.element.util.FileHelper;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FilePackage;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.workspace.Workspace;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>File</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.file.impl.FileImpl#getLongDescription <em>Long Description</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.file.impl.FileImpl#getLicense <em>License</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FileImpl extends GroupImpl implements File {
	/**
	 * The cached value of the '{@link #getLongDescription() <em>Long Description</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getLongDescription()
	 * @generated
	 * @ordered
	 */
	protected Text longDescription;

	/**
	 * The default value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected static final String LICENSE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected String license = LICENSE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected FileImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FilePackage.Literals.FILE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Text getLongDescription() {
		if (longDescription != null && longDescription.eIsProxy()) {
			InternalEObject oldLongDescription = (InternalEObject)longDescription;
			longDescription = (Text)eResolveProxy(oldLongDescription);
			if (longDescription != oldLongDescription) {
				InternalEObject newLongDescription = (InternalEObject)longDescription;
				NotificationChain msgs = oldLongDescription.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FilePackage.FILE__LONG_DESCRIPTION, null, null);
				if (newLongDescription.eInternalContainer() == null) {
					msgs = newLongDescription.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FilePackage.FILE__LONG_DESCRIPTION, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FilePackage.FILE__LONG_DESCRIPTION, oldLongDescription, longDescription));
			}
		}
		return longDescription;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetLongDescription() {
		return longDescription;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLongDescription(Text newLongDescription, NotificationChain msgs) {
		Text oldLongDescription = longDescription;
		longDescription = newLongDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FilePackage.FILE__LONG_DESCRIPTION, oldLongDescription, newLongDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setLongDescription(Text newLongDescription) {
		if (newLongDescription != longDescription) {
			NotificationChain msgs = null;
			if (longDescription != null)
				msgs = ((InternalEObject)longDescription).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FilePackage.FILE__LONG_DESCRIPTION, null, msgs);
			if (newLongDescription != null)
				msgs = ((InternalEObject)newLongDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FilePackage.FILE__LONG_DESCRIPTION, null, msgs);
			msgs = basicSetLongDescription(newLongDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FilePackage.FILE__LONG_DESCRIPTION, newLongDescription, newLongDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLicense() {
		return license;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLicense(String newLicense) {
		String oldLicense = license;
		license = newLicense;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FilePackage.FILE__LICENSE, oldLicense, license));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FilePackage.FILE__LONG_DESCRIPTION:
				return basicSetLongDescription(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FilePackage.FILE__LONG_DESCRIPTION:
				if (resolve) return getLongDescription();
				return basicGetLongDescription();
			case FilePackage.FILE__LICENSE:
				return getLicense();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FilePackage.FILE__LONG_DESCRIPTION:
				setLongDescription((Text)newValue);
				return;
			case FilePackage.FILE__LICENSE:
				setLicense((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FilePackage.FILE__LONG_DESCRIPTION:
				setLongDescription((Text)null);
				return;
			case FilePackage.FILE__LICENSE:
				setLicense(LICENSE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FilePackage.FILE__LONG_DESCRIPTION:
				return longDescription != null;
			case FilePackage.FILE__LICENSE:
				return LICENSE_EDEFAULT == null ? license != null : !LICENSE_EDEFAULT.equals(license);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (license: ");
		result.append(license);
		result.append(')');
		return result.toString();
	}

	@Override
	public void delete() {
		if (getParent() instanceof Workspace) {
			Workspace workspace = (Workspace) getParent();
			workspace.getCurrentlyOpenedFiles().remove(this);

			EList<Project> projects = workspace.getCurrentlyOpenedProjects();
			if (projects.size() > 0) {
				Project project = projects.get(0);
				project.getListofopenedfilesinproject().remove(this);
				project.save();
			}
		}

		Resource resource = eResource();

		if (resource != null) {
			try {
				resource.delete(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String getIconURI() {
		return "icons/file.png";
	}

	@Override
	public void save() {
		this.eSetDeliver(false);
		this.setLastModified(new Date());
		this.eSetDeliver(true);
		this.setLicense(GroupImpl.LICENSE);

		if (name == null) {
			return;
		}

		Resource currentResource = eResource();

		if (currentResource == null) {
			throw new IllegalStateException("File is not in a Resource.");
		}

		if (name.equals(FileHelper.getFileName(this))) {
			// The file name has not changed, so we simply save the resource.
			try {
				Map options = new HashMap();
				options.put(XMLResource.OPTION_PROCESS_DANGLING_HREF, XMLResource.OPTION_PROCESS_DANGLING_HREF_DISCARD);
				options.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
				currentResource.save(options);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			// The file name has changed, so we save the file with the new file
			// name and delete the old file (if it exists).

			String directoryPath = FileHelper.getDirectoryPath(this);
			String newFilePath = FileHelper.join(directoryPath, name);
			java.io.File newPhysicalFile = new java.io.File(newFilePath);

			if (newPhysicalFile.exists()) {
				throw new IllegalStateException("A file already exists at the specified path.");
			}

			ResourceSet resourceSet = getResourceSet();
			URI newUri = URI.createFileURI(newFilePath);
			Resource newResource = resourceSet.createResource(newUri);

			newResource.getContents().add(this);

			try {
				newResource.save(null);
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				currentResource.delete(null);
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (getParent() != null && getParent() instanceof Workspace) {
				Workspace workspace = (Workspace) getParent();
				EList<Project> projects = workspace.getCurrentlyOpenedProjects();
				if (projects.size() > 0) {
					Project project = projects.get(0);
					project.save();
				}
			}
		}
	}

} // FileImpl
