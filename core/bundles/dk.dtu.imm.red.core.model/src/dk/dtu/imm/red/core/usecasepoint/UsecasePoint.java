/**
 */
package dk.dtu.imm.red.core.usecasepoint;

import java.util.List;
import java.util.Map;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.properties.Property;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Usecase Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getManagementFactor <em>Management Factor</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getProductivityFactor <em>Productivity Factor</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getRequirementFactor <em>Requirement Factor</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getTechnologyFactor <em>Technology Factor</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getCostPerUsecasePoint <em>Cost Per Usecase Point</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getTotalCost <em>Total Cost</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getUsecasePoints <em>Usecase Points</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getTotalCostManual <em>Total Cost Manual</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getTotalBenefitManual <em>Total Benefit Manual</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint()
 * @model
 * @generated
 */
public interface UsecasePoint extends Factor {
	/**
	 * Returns the value of the '<em><b>Management Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Management Factor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Management Factor</em>' containment reference.
	 * @see #setManagementFactor(ManagementFactor)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint_ManagementFactor()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	ManagementFactor getManagementFactor();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getManagementFactor <em>Management Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Management Factor</em>' containment reference.
	 * @see #getManagementFactor()
	 * @generated
	 */
	void setManagementFactor(ManagementFactor value);

	/**
	 * Returns the value of the '<em><b>Productivity Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Productivity Factor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Productivity Factor</em>' containment reference.
	 * @see #setProductivityFactor(ProductivityFactor)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint_ProductivityFactor()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	ProductivityFactor getProductivityFactor();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getProductivityFactor <em>Productivity Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Productivity Factor</em>' containment reference.
	 * @see #getProductivityFactor()
	 * @generated
	 */
	void setProductivityFactor(ProductivityFactor value);

	/**
	 * Returns the value of the '<em><b>Requirement Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement Factor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement Factor</em>' containment reference.
	 * @see #setRequirementFactor(RequirementFactor)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint_RequirementFactor()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	RequirementFactor getRequirementFactor();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getRequirementFactor <em>Requirement Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requirement Factor</em>' containment reference.
	 * @see #getRequirementFactor()
	 * @generated
	 */
	void setRequirementFactor(RequirementFactor value);

	/**
	 * Returns the value of the '<em><b>Technology Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Technology Factor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Technology Factor</em>' containment reference.
	 * @see #setTechnologyFactor(TechnologyFactor)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint_TechnologyFactor()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	TechnologyFactor getTechnologyFactor();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getTechnologyFactor <em>Technology Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Technology Factor</em>' containment reference.
	 * @see #getTechnologyFactor()
	 * @generated
	 */
	void setTechnologyFactor(TechnologyFactor value); 
	/**
	 * Returns the value of the '<em><b>Cost Per Usecase Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost Per Usecase Point</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost Per Usecase Point</em>' containment reference.
	 * @see #setCostPerUsecasePoint(Property)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint_CostPerUsecasePoint()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Property getCostPerUsecasePoint();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getCostPerUsecasePoint <em>Cost Per Usecase Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost Per Usecase Point</em>' containment reference.
	 * @see #getCostPerUsecasePoint()
	 * @generated
	 */
	void setCostPerUsecasePoint(Property value);

	/**
	 * Returns the value of the '<em><b>Total Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Cost</em>' attribute.
	 * @see #setTotalCost(double)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint_TotalCost()
	 * @model
	 * @generated
	 */
	double getTotalCost();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getTotalCost <em>Total Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Cost</em>' attribute.
	 * @see #getTotalCost()
	 * @generated
	 */
	void setTotalCost(double value);

	/**
	 * Returns the value of the '<em><b>Usecase Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Usecase Points</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usecase Points</em>' attribute.
	 * @see #setUsecasePoints(double)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint_UsecasePoints()
	 * @model
	 * @generated
	 */
	double getUsecasePoints();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getUsecasePoints <em>Usecase Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Usecase Points</em>' attribute.
	 * @see #getUsecasePoints()
	 * @generated
	 */
	void setUsecasePoints(double value);

	/**
	 * Returns the value of the '<em><b>Total Cost Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Cost Manual</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Cost Manual</em>' attribute.
	 * @see #setTotalCostManual(double)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint_TotalCostManual()
	 * @model
	 * @generated
	 */
	double getTotalCostManual();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getTotalCostManual <em>Total Cost Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Cost Manual</em>' attribute.
	 * @see #getTotalCostManual()
	 * @generated
	 */
	void setTotalCostManual(double value);

	/**
	 * Returns the value of the '<em><b>Total Benefit Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Benefit Manual</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Benefit Manual</em>' attribute.
	 * @see #setTotalBenefitManual(double)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getUsecasePoint_TotalBenefitManual()
	 * @model
	 * @generated
	 */
	double getTotalBenefitManual();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.UsecasePoint#getTotalBenefitManual <em>Total Benefit Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Benefit Manual</em>' attribute.
	 * @see #getTotalBenefitManual()
	 * @generated
	 */
	void setTotalBenefitManual(double value);

	double getSum(Map<String, List<Element>> requirementFactorElementMap);

	/** 
	 * @generated NOT
	 */
	Map<String, List<Element>> getElementMap(List<Element> contents);

} // UsecasePoint
