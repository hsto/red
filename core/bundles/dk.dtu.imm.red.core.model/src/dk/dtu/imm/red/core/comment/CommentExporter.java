/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.comment;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.Visitor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exporter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.core.comment.CommentPackage#getCommentExporter()
 * @model
 * @generated
 */
public interface CommentExporter extends Visitor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void visit(Element object);

} // CommentExporter
