/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.comment.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import dk.dtu.imm.red.core.CorePackage;
import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentCategory;
import dk.dtu.imm.red.core.comment.CommentExporter;
import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.comment.CommentPackage;
import dk.dtu.imm.red.core.comment.IssueComment;
import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.contribution.ContributionPackage;
import dk.dtu.imm.red.core.element.contribution.impl.ContributionPackageImpl;
import dk.dtu.imm.red.core.element.document.DocumentPackage;
import dk.dtu.imm.red.core.element.document.impl.DocumentPackageImpl;
import dk.dtu.imm.red.core.element.group.GroupPackage;
import dk.dtu.imm.red.core.element.group.impl.GroupPackageImpl;
import dk.dtu.imm.red.core.element.id.IdPackage;
import dk.dtu.imm.red.core.element.id.impl.IdPackageImpl;
import dk.dtu.imm.red.core.element.impl.ElementPackageImpl;
import dk.dtu.imm.red.core.element.relationship.RelationshipPackage;
import dk.dtu.imm.red.core.element.relationship.impl.RelationshipPackageImpl;
import dk.dtu.imm.red.core.element.unit.UnitPackage;
import dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl;
import dk.dtu.imm.red.core.file.FilePackage;
import dk.dtu.imm.red.core.file.impl.FilePackageImpl;
import dk.dtu.imm.red.core.folder.FolderPackage;
import dk.dtu.imm.red.core.folder.impl.FolderPackageImpl;
import dk.dtu.imm.red.core.impl.CorePackageImpl;
import dk.dtu.imm.red.core.project.ProjectPackage;
import dk.dtu.imm.red.core.project.impl.ProjectPackageImpl;
import dk.dtu.imm.red.core.properties.PropertiesPackage;
import dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl;
import dk.dtu.imm.red.core.text.TextPackage;
import dk.dtu.imm.red.core.text.impl.TextPackageImpl;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;
import dk.dtu.imm.red.core.usecasepoint.impl.UsecasepointPackageImpl;
import dk.dtu.imm.red.core.user.UserPackage;
import dk.dtu.imm.red.core.user.impl.UserPackageImpl;
import dk.dtu.imm.red.core.validation.ValidationPackage;
import dk.dtu.imm.red.core.validation.impl.ValidationPackageImpl;
import dk.dtu.imm.red.core.workspace.WorkspacePackage;
import dk.dtu.imm.red.core.workspace.impl.WorkspacePackageImpl;



/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class CommentPackageImpl extends EPackageImpl implements CommentPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commentListEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commentExporterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass issueCommentEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum commentCategoryEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum issueSeverityEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.core.comment.CommentPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CommentPackageImpl() {
		super(eNS_URI, CommentFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link CommentPackage#eINSTANCE} when
	 * that field is accessed. Clients should not invoke it directly. Instead,
	 * they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CommentPackage init() {
		if (isInited) return (CommentPackage)EPackage.Registry.INSTANCE.getEPackage(CommentPackage.eNS_URI);

		// Obtain or create and register package
		CommentPackageImpl theCommentPackage = (CommentPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CommentPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CommentPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		ElementPackageImpl theElementPackage = (ElementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI) instanceof ElementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI) : ElementPackage.eINSTANCE);
		GroupPackageImpl theGroupPackage = (GroupPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GroupPackage.eNS_URI) instanceof GroupPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GroupPackage.eNS_URI) : GroupPackage.eINSTANCE);
		ContributionPackageImpl theContributionPackage = (ContributionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ContributionPackage.eNS_URI) instanceof ContributionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ContributionPackage.eNS_URI) : ContributionPackage.eINSTANCE);
		RelationshipPackageImpl theRelationshipPackage = (RelationshipPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationshipPackage.eNS_URI) instanceof RelationshipPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationshipPackage.eNS_URI) : RelationshipPackage.eINSTANCE);
		IdPackageImpl theIdPackage = (IdPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdPackage.eNS_URI) instanceof IdPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdPackage.eNS_URI) : IdPackage.eINSTANCE);
		DocumentPackageImpl theDocumentPackage = (DocumentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DocumentPackage.eNS_URI) instanceof DocumentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DocumentPackage.eNS_URI) : DocumentPackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		TextPackageImpl theTextPackage = (TextPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI) instanceof TextPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI) : TextPackage.eINSTANCE);
		UserPackageImpl theUserPackage = (UserPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI) instanceof UserPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI) : UserPackage.eINSTANCE);
		FolderPackageImpl theFolderPackage = (FolderPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FolderPackage.eNS_URI) instanceof FolderPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FolderPackage.eNS_URI) : FolderPackage.eINSTANCE);
		WorkspacePackageImpl theWorkspacePackage = (WorkspacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WorkspacePackage.eNS_URI) instanceof WorkspacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WorkspacePackage.eNS_URI) : WorkspacePackage.eINSTANCE);
		FilePackageImpl theFilePackage = (FilePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FilePackage.eNS_URI) instanceof FilePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FilePackage.eNS_URI) : FilePackage.eINSTANCE);
		ProjectPackageImpl theProjectPackage = (ProjectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ProjectPackage.eNS_URI) instanceof ProjectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ProjectPackage.eNS_URI) : ProjectPackage.eINSTANCE);
		PropertiesPackageImpl thePropertiesPackage = (PropertiesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PropertiesPackage.eNS_URI) instanceof PropertiesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PropertiesPackage.eNS_URI) : PropertiesPackage.eINSTANCE);
		UsecasepointPackageImpl theUsecasepointPackage = (UsecasepointPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UsecasepointPackage.eNS_URI) instanceof UsecasepointPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UsecasepointPackage.eNS_URI) : UsecasepointPackage.eINSTANCE);

		// Create package meta-data objects
		theCommentPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theElementPackage.createPackageContents();
		theGroupPackage.createPackageContents();
		theContributionPackage.createPackageContents();
		theRelationshipPackage.createPackageContents();
		theIdPackage.createPackageContents();
		theDocumentPackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theTextPackage.createPackageContents();
		theUserPackage.createPackageContents();
		theFolderPackage.createPackageContents();
		theWorkspacePackage.createPackageContents();
		theFilePackage.createPackageContents();
		theProjectPackage.createPackageContents();
		thePropertiesPackage.createPackageContents();
		theUsecasepointPackage.createPackageContents();

		// Initialize created meta-data
		theCommentPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theElementPackage.initializePackageContents();
		theGroupPackage.initializePackageContents();
		theContributionPackage.initializePackageContents();
		theRelationshipPackage.initializePackageContents();
		theIdPackage.initializePackageContents();
		theDocumentPackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theTextPackage.initializePackageContents();
		theUserPackage.initializePackageContents();
		theFolderPackage.initializePackageContents();
		theWorkspacePackage.initializePackageContents();
		theFilePackage.initializePackageContents();
		theProjectPackage.initializePackageContents();
		thePropertiesPackage.initializePackageContents();
		theUsecasepointPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCommentPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CommentPackage.eNS_URI, theCommentPackage);
		return theCommentPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCommentList() {
		return commentListEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCommentList_Comments() {
		return (EReference)commentListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getComment() {
		return commentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getComment_Category() {
		return (EAttribute)commentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getComment_Text() {
		return (EReference)commentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getComment_Author() {
		return (EReference)commentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommentExporter() {
		return commentExporterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIssueComment() {
		return issueCommentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIssueComment_Severity() {
		return (EAttribute)issueCommentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getCommentCategory() {
		return commentCategoryEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getIssueSeverity() {
		return issueSeverityEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CommentFactory getCommentFactory() {
		return (CommentFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		commentListEClass = createEClass(COMMENT_LIST);
		createEReference(commentListEClass, COMMENT_LIST__COMMENTS);

		commentEClass = createEClass(COMMENT);
		createEAttribute(commentEClass, COMMENT__CATEGORY);
		createEReference(commentEClass, COMMENT__TEXT);
		createEReference(commentEClass, COMMENT__AUTHOR);

		commentExporterEClass = createEClass(COMMENT_EXPORTER);

		issueCommentEClass = createEClass(ISSUE_COMMENT);
		createEAttribute(issueCommentEClass, ISSUE_COMMENT__SEVERITY);

		// Create enums
		commentCategoryEEnum = createEEnum(COMMENT_CATEGORY);
		issueSeverityEEnum = createEEnum(ISSUE_SEVERITY);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ElementPackage theElementPackage = (ElementPackage)EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI);
		TextPackage theTextPackage = (TextPackage)EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI);
		UserPackage theUserPackage = (UserPackage)EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		commentEClass.getESuperTypes().add(theElementPackage.getElement());
		commentExporterEClass.getESuperTypes().add(theElementPackage.getVisitor());
		issueCommentEClass.getESuperTypes().add(this.getComment());

		// Initialize classes and features; add operations and parameters
		initEClass(commentListEClass, CommentList.class, "CommentList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommentList_Comments(), this.getComment(), null, "comments", null, 0, -1, CommentList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(commentEClass, Comment.class, "Comment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComment_Category(), this.getCommentCategory(), "category", null, 0, 1, Comment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComment_Text(), theTextPackage.getText(), null, "text", null, 1, 1, Comment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComment_Author(), theUserPackage.getUser(), null, "author", null, 1, 1, Comment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(commentExporterEClass, CommentExporter.class, "CommentExporter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(commentExporterEClass, null, "visit", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theElementPackage.getElement(), "object", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(issueCommentEClass, IssueComment.class, "IssueComment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIssueComment_Severity(), this.getIssueSeverity(), "severity", null, 0, 1, IssueComment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(commentCategoryEEnum, CommentCategory.class, "CommentCategory");
		addEEnumLiteral(commentCategoryEEnum, CommentCategory.OBSOLETE);
		addEEnumLiteral(commentCategoryEEnum, CommentCategory.RESOLVED);
		addEEnumLiteral(commentCategoryEEnum, CommentCategory.UNRESOLVED);
		addEEnumLiteral(commentCategoryEEnum, CommentCategory.NOTICE);

		initEEnum(issueSeverityEEnum, IssueSeverity.class, "IssueSeverity");
		addEEnumLiteral(issueSeverityEEnum, IssueSeverity.OK);
		addEEnumLiteral(issueSeverityEEnum, IssueSeverity.OBSERVATION);
		addEEnumLiteral(issueSeverityEEnum, IssueSeverity.WARNING);
		addEEnumLiteral(issueSeverityEEnum, IssueSeverity.WEAKNESS);
		addEEnumLiteral(issueSeverityEEnum, IssueSeverity.MISTAKE);
		addEEnumLiteral(issueSeverityEEnum, IssueSeverity.ERROR);
	}

} // CommentPackageImpl
