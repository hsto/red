/**
 */
package dk.dtu.imm.red.core.usecasepoint.impl;

import java.util.List;

import dk.dtu.imm.red.core.usecasepoint.ManagementFactor; 
import dk.dtu.imm.red.core.usecasepoint.UsecasepointFactory;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactor;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactorValue;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Management Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ManagementFactorImpl extends WeightedFactorContainerImpl implements ManagementFactor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ManagementFactorImpl() {
		super();
	} 

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasepointPackage.Literals.MANAGEMENT_FACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */ 
	@Override 
	public double getSum() { 
		double sum = 0;
		
		for(WeightedFactor w : getWeightedFactors()) {
			sum += w.getSum();
		} 
		
		double k1 = 0.8875;
		double k2 = 0.025;
		
		return k1 + (k2 * sum); 
	}


} //ManagementFactorImpl
