/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.user;

import dk.dtu.imm.red.core.element.contribution.Contribution;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>User</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.user.User#getId <em>Id</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.user.User#getSkype <em>Skype</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.user.User#getEmail <em>Email</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.user.User#getInitials <em>Initials</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.user.User#getPhoneNumber <em>Phone Number</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.user.UserPackage#getUser()
 * @model
 * @generated
 */
public interface User extends Contribution {
	/**
	 * Returns the value of the '<em><b>Email</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Email</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Email</em>' attribute.
	 * @see #setEmail(String)
	 * @see dk.dtu.imm.red.core.user.UserPackage#getUser_Email()
	 * @model
	 * @generated
	 */
	String getEmail();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.user.User#getEmail <em>Email</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Email</em>' attribute.
	 * @see #getEmail()
	 * @generated
	 */
	void setEmail(String value);

	/**
	 * Returns the value of the '<em><b>Initials</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initials</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Initials</em>' attribute.
	 * @see #setInitials(String)
	 * @see dk.dtu.imm.red.core.user.UserPackage#getUser_Initials()
	 * @model
	 * @generated
	 */
	String getInitials();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.user.User#getInitials
	 * <em>Initials</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Initials</em>' attribute.
	 * @see #getInitials()
	 * @generated
	 */
	void setInitials(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see dk.dtu.imm.red.core.user.UserPackage#getUser_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.user.User#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Phone Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Phone Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Phone Number</em>' attribute.
	 * @see #setPhoneNumber(String)
	 * @see dk.dtu.imm.red.core.user.UserPackage#getUser_PhoneNumber()
	 * @model
	 * @generated
	 */
	String getPhoneNumber();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.user.User#getPhoneNumber <em>Phone Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Phone Number</em>' attribute.
	 * @see #getPhoneNumber()
	 * @generated
	 */
	void setPhoneNumber(String value);

	/**
	 * Returns the value of the '<em><b>Skype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Skype</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skype</em>' attribute.
	 * @see #setSkype(String)
	 * @see dk.dtu.imm.red.core.user.UserPackage#getUser_Skype()
	 * @model
	 * @generated
	 */
	String getSkype();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.user.User#getSkype <em>Skype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Skype</em>' attribute.
	 * @see #getSkype()
	 * @generated
	 */
	void setSkype(String value);

} // User
