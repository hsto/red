/**
 */
package dk.dtu.imm.red.core.usecasepoint.impl;

import dk.dtu.imm.red.core.CorePackage;

import dk.dtu.imm.red.core.comment.CommentPackage;

import dk.dtu.imm.red.core.comment.impl.CommentPackageImpl;

import dk.dtu.imm.red.core.element.ElementPackage;

import dk.dtu.imm.red.core.element.contribution.ContributionPackage;

import dk.dtu.imm.red.core.element.contribution.impl.ContributionPackageImpl;

import dk.dtu.imm.red.core.element.document.DocumentPackage;

import dk.dtu.imm.red.core.element.document.impl.DocumentPackageImpl;

import dk.dtu.imm.red.core.element.group.GroupPackage;

import dk.dtu.imm.red.core.element.group.impl.GroupPackageImpl;

import dk.dtu.imm.red.core.element.id.IdPackage;

import dk.dtu.imm.red.core.element.id.impl.IdPackageImpl;

import dk.dtu.imm.red.core.element.impl.ElementPackageImpl;

import dk.dtu.imm.red.core.element.relationship.RelationshipPackage;

import dk.dtu.imm.red.core.element.relationship.impl.RelationshipPackageImpl;

import dk.dtu.imm.red.core.element.unit.UnitPackage;
import dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl;
import dk.dtu.imm.red.core.file.FilePackage;

import dk.dtu.imm.red.core.file.impl.FilePackageImpl;

import dk.dtu.imm.red.core.folder.FolderPackage;

import dk.dtu.imm.red.core.folder.impl.FolderPackageImpl;

import dk.dtu.imm.red.core.impl.CorePackageImpl;

import dk.dtu.imm.red.core.project.ProjectPackage;

import dk.dtu.imm.red.core.project.impl.ProjectPackageImpl;

import dk.dtu.imm.red.core.properties.PropertiesPackage;

import dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl;

import dk.dtu.imm.red.core.text.TextPackage;

import dk.dtu.imm.red.core.text.impl.TextPackageImpl;

import dk.dtu.imm.red.core.usecasepoint.Factor;
import dk.dtu.imm.red.core.usecasepoint.ManagementFactor; 
import dk.dtu.imm.red.core.usecasepoint.ProductivityFactor;
import dk.dtu.imm.red.core.usecasepoint.RequirementFactor;
import dk.dtu.imm.red.core.usecasepoint.TechnologyFactor;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointFactory;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;

import dk.dtu.imm.red.core.usecasepoint.WeightedFactor;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactorContainer;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactorValue;
import dk.dtu.imm.red.core.user.UserPackage;

import dk.dtu.imm.red.core.user.impl.UserPackageImpl;

import dk.dtu.imm.red.core.workspace.WorkspacePackage;

import dk.dtu.imm.red.core.workspace.impl.WorkspacePackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UsecasepointPackageImpl extends EPackageImpl implements UsecasepointPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass factorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass usecasePointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass managementFactorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass weightedFactorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass weightedFactorContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass productivityFactorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass technologyFactorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requirementFactorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum weightedFactorValueEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UsecasepointPackageImpl() {
		super(eNS_URI, UsecasepointFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UsecasepointPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UsecasepointPackage init() {
		if (isInited) return (UsecasepointPackage)EPackage.Registry.INSTANCE.getEPackage(UsecasepointPackage.eNS_URI);

		// Obtain or create and register package
		UsecasepointPackageImpl theUsecasepointPackage = (UsecasepointPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UsecasepointPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UsecasepointPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		CommentPackageImpl theCommentPackage = (CommentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CommentPackage.eNS_URI) instanceof CommentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CommentPackage.eNS_URI) : CommentPackage.eINSTANCE);
		ElementPackageImpl theElementPackage = (ElementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI) instanceof ElementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI) : ElementPackage.eINSTANCE);
		GroupPackageImpl theGroupPackage = (GroupPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GroupPackage.eNS_URI) instanceof GroupPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GroupPackage.eNS_URI) : GroupPackage.eINSTANCE);
		ContributionPackageImpl theContributionPackage = (ContributionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ContributionPackage.eNS_URI) instanceof ContributionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ContributionPackage.eNS_URI) : ContributionPackage.eINSTANCE);
		RelationshipPackageImpl theRelationshipPackage = (RelationshipPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationshipPackage.eNS_URI) instanceof RelationshipPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationshipPackage.eNS_URI) : RelationshipPackage.eINSTANCE);
		IdPackageImpl theIdPackage = (IdPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdPackage.eNS_URI) instanceof IdPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdPackage.eNS_URI) : IdPackage.eINSTANCE);
		DocumentPackageImpl theDocumentPackage = (DocumentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DocumentPackage.eNS_URI) instanceof DocumentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DocumentPackage.eNS_URI) : DocumentPackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		TextPackageImpl theTextPackage = (TextPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI) instanceof TextPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI) : TextPackage.eINSTANCE);
		UserPackageImpl theUserPackage = (UserPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI) instanceof UserPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI) : UserPackage.eINSTANCE);
		FolderPackageImpl theFolderPackage = (FolderPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FolderPackage.eNS_URI) instanceof FolderPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FolderPackage.eNS_URI) : FolderPackage.eINSTANCE);
		WorkspacePackageImpl theWorkspacePackage = (WorkspacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WorkspacePackage.eNS_URI) instanceof WorkspacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WorkspacePackage.eNS_URI) : WorkspacePackage.eINSTANCE);
		FilePackageImpl theFilePackage = (FilePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FilePackage.eNS_URI) instanceof FilePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FilePackage.eNS_URI) : FilePackage.eINSTANCE);
		ProjectPackageImpl theProjectPackage = (ProjectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ProjectPackage.eNS_URI) instanceof ProjectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ProjectPackage.eNS_URI) : ProjectPackage.eINSTANCE);
		PropertiesPackageImpl thePropertiesPackage = (PropertiesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PropertiesPackage.eNS_URI) instanceof PropertiesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PropertiesPackage.eNS_URI) : PropertiesPackage.eINSTANCE);

		// Create package meta-data objects
		theUsecasepointPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theCommentPackage.createPackageContents();
		theElementPackage.createPackageContents();
		theGroupPackage.createPackageContents();
		theContributionPackage.createPackageContents();
		theRelationshipPackage.createPackageContents();
		theIdPackage.createPackageContents();
		theDocumentPackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theTextPackage.createPackageContents();
		theUserPackage.createPackageContents();
		theFolderPackage.createPackageContents();
		theWorkspacePackage.createPackageContents();
		theFilePackage.createPackageContents();
		theProjectPackage.createPackageContents();
		thePropertiesPackage.createPackageContents();

		// Initialize created meta-data
		theUsecasepointPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theCommentPackage.initializePackageContents();
		theElementPackage.initializePackageContents();
		theGroupPackage.initializePackageContents();
		theContributionPackage.initializePackageContents();
		theRelationshipPackage.initializePackageContents();
		theIdPackage.initializePackageContents();
		theDocumentPackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theTextPackage.initializePackageContents();
		theUserPackage.initializePackageContents();
		theFolderPackage.initializePackageContents();
		theWorkspacePackage.initializePackageContents();
		theFilePackage.initializePackageContents();
		theProjectPackage.initializePackageContents();
		thePropertiesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUsecasepointPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UsecasepointPackage.eNS_URI, theUsecasepointPackage);
		return theUsecasepointPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFactor() {
		return factorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFactor_Sum() {
		return (EAttribute)factorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUsecasePoint() {
		return usecasePointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUsecasePoint_ManagementFactor() {
		return (EReference)usecasePointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUsecasePoint_ProductivityFactor() {
		return (EReference)usecasePointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUsecasePoint_RequirementFactor() {
		return (EReference)usecasePointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUsecasePoint_TechnologyFactor() {
		return (EReference)usecasePointEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUsecasePoint_CostPerUsecasePoint() {
		return (EReference)usecasePointEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUsecasePoint_TotalCost() {
		return (EAttribute)usecasePointEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUsecasePoint_UsecasePoints() {
		return (EAttribute)usecasePointEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUsecasePoint_TotalCostManual() {
		return (EAttribute)usecasePointEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUsecasePoint_TotalBenefitManual() {
		return (EAttribute)usecasePointEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getManagementFactor() {
		return managementFactorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWeightedFactor() {
		return weightedFactorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWeightedFactor_FactorValue() {
		return (EAttribute)weightedFactorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWeightedFactor_Weight() {
		return (EAttribute)weightedFactorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWeightedFactorContainer() {
		return weightedFactorContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWeightedFactorContainer_WeightedFactors() {
		return (EReference)weightedFactorContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProductivityFactor() {
		return productivityFactorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTechnologyFactor() {
		return technologyFactorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequirementFactor() {
		return requirementFactorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getWeightedFactorValue() {
		return weightedFactorValueEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsecasepointFactory getUsecasepointFactory() {
		return (UsecasepointFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		factorEClass = createEClass(FACTOR);
		createEAttribute(factorEClass, FACTOR__SUM);

		usecasePointEClass = createEClass(USECASE_POINT);
		createEReference(usecasePointEClass, USECASE_POINT__MANAGEMENT_FACTOR);
		createEReference(usecasePointEClass, USECASE_POINT__PRODUCTIVITY_FACTOR);
		createEReference(usecasePointEClass, USECASE_POINT__REQUIREMENT_FACTOR);
		createEReference(usecasePointEClass, USECASE_POINT__TECHNOLOGY_FACTOR);
		createEReference(usecasePointEClass, USECASE_POINT__COST_PER_USECASE_POINT);
		createEAttribute(usecasePointEClass, USECASE_POINT__TOTAL_COST);
		createEAttribute(usecasePointEClass, USECASE_POINT__USECASE_POINTS);
		createEAttribute(usecasePointEClass, USECASE_POINT__TOTAL_COST_MANUAL);
		createEAttribute(usecasePointEClass, USECASE_POINT__TOTAL_BENEFIT_MANUAL);

		weightedFactorEClass = createEClass(WEIGHTED_FACTOR);
		createEAttribute(weightedFactorEClass, WEIGHTED_FACTOR__FACTOR_VALUE);
		createEAttribute(weightedFactorEClass, WEIGHTED_FACTOR__WEIGHT);

		weightedFactorContainerEClass = createEClass(WEIGHTED_FACTOR_CONTAINER);
		createEReference(weightedFactorContainerEClass, WEIGHTED_FACTOR_CONTAINER__WEIGHTED_FACTORS);

		managementFactorEClass = createEClass(MANAGEMENT_FACTOR);

		technologyFactorEClass = createEClass(TECHNOLOGY_FACTOR);

		productivityFactorEClass = createEClass(PRODUCTIVITY_FACTOR);

		requirementFactorEClass = createEClass(REQUIREMENT_FACTOR);

		// Create enums
		weightedFactorValueEEnum = createEEnum(WEIGHTED_FACTOR_VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ElementPackage theElementPackage = (ElementPackage)EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		PropertiesPackage thePropertiesPackage = (PropertiesPackage)EPackage.Registry.INSTANCE.getEPackage(PropertiesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		factorEClass.getESuperTypes().add(theElementPackage.getElement());
		usecasePointEClass.getESuperTypes().add(this.getFactor());
		weightedFactorEClass.getESuperTypes().add(this.getFactor());
		weightedFactorContainerEClass.getESuperTypes().add(this.getFactor());
		managementFactorEClass.getESuperTypes().add(this.getWeightedFactorContainer());
		technologyFactorEClass.getESuperTypes().add(this.getWeightedFactorContainer());
		productivityFactorEClass.getESuperTypes().add(this.getFactor());
		requirementFactorEClass.getESuperTypes().add(this.getFactor());

		// Initialize classes and features; add operations and parameters
		initEClass(factorEClass, Factor.class, "Factor", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFactor_Sum(), theEcorePackage.getEDouble(), "sum", null, 0, 1, Factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(usecasePointEClass, UsecasePoint.class, "UsecasePoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUsecasePoint_ManagementFactor(), this.getManagementFactor(), null, "managementFactor", null, 0, 1, UsecasePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUsecasePoint_ProductivityFactor(), this.getProductivityFactor(), null, "productivityFactor", null, 0, 1, UsecasePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUsecasePoint_RequirementFactor(), this.getRequirementFactor(), null, "requirementFactor", null, 0, 1, UsecasePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUsecasePoint_TechnologyFactor(), this.getTechnologyFactor(), null, "technologyFactor", null, 0, 1, UsecasePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUsecasePoint_CostPerUsecasePoint(), thePropertiesPackage.getProperty(), null, "costPerUsecasePoint", null, 0, 1, UsecasePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUsecasePoint_TotalCost(), theEcorePackage.getEDouble(), "totalCost", null, 0, 1, UsecasePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUsecasePoint_UsecasePoints(), theEcorePackage.getEDouble(), "usecasePoints", null, 0, 1, UsecasePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUsecasePoint_TotalCostManual(), theEcorePackage.getEDouble(), "totalCostManual", null, 0, 1, UsecasePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUsecasePoint_TotalBenefitManual(), theEcorePackage.getEDouble(), "totalBenefitManual", null, 0, 1, UsecasePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(weightedFactorEClass, WeightedFactor.class, "WeightedFactor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWeightedFactor_FactorValue(), this.getWeightedFactorValue(), "factorValue", null, 0, 1, WeightedFactor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWeightedFactor_Weight(), theEcorePackage.getEDouble(), "weight", null, 0, 1, WeightedFactor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(weightedFactorContainerEClass, WeightedFactorContainer.class, "WeightedFactorContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWeightedFactorContainer_WeightedFactors(), this.getWeightedFactor(), null, "weightedFactors", null, 0, -1, WeightedFactorContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(managementFactorEClass, ManagementFactor.class, "ManagementFactor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(technologyFactorEClass, TechnologyFactor.class, "TechnologyFactor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(productivityFactorEClass, ProductivityFactor.class, "ProductivityFactor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(requirementFactorEClass, RequirementFactor.class, "RequirementFactor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(weightedFactorValueEEnum, WeightedFactorValue.class, "WeightedFactorValue");
		addEEnumLiteral(weightedFactorValueEEnum, WeightedFactorValue.SMALL);
		addEEnumLiteral(weightedFactorValueEEnum, WeightedFactorValue.MEDIUM);
		addEEnumLiteral(weightedFactorValueEEnum, WeightedFactorValue.LARGE);
	}

} //UsecasepointPackageImpl
