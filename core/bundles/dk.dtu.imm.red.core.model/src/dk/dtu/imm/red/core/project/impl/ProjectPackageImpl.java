/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.project.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import dk.dtu.imm.red.core.CorePackage;
import dk.dtu.imm.red.core.comment.CommentPackage;
import dk.dtu.imm.red.core.comment.impl.CommentPackageImpl;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.contribution.ContributionPackage;
import dk.dtu.imm.red.core.element.contribution.impl.ContributionPackageImpl;
import dk.dtu.imm.red.core.element.document.DocumentPackage;
import dk.dtu.imm.red.core.element.document.impl.DocumentPackageImpl;
import dk.dtu.imm.red.core.element.group.GroupPackage;
import dk.dtu.imm.red.core.element.group.impl.GroupPackageImpl;
import dk.dtu.imm.red.core.element.id.IdPackage;
import dk.dtu.imm.red.core.element.id.impl.IdPackageImpl;
import dk.dtu.imm.red.core.element.impl.ElementPackageImpl;
import dk.dtu.imm.red.core.element.relationship.RelationshipPackage;
import dk.dtu.imm.red.core.element.relationship.impl.RelationshipPackageImpl;
import dk.dtu.imm.red.core.element.unit.UnitPackage;
import dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl;
import dk.dtu.imm.red.core.file.FilePackage;
import dk.dtu.imm.red.core.file.impl.FilePackageImpl;
import dk.dtu.imm.red.core.folder.FolderPackage;
import dk.dtu.imm.red.core.folder.impl.FolderPackageImpl;
import dk.dtu.imm.red.core.impl.CorePackageImpl;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.project.ProjectDate;
import dk.dtu.imm.red.core.project.ProjectFactory;
import dk.dtu.imm.red.core.project.ProjectPackage;
import dk.dtu.imm.red.core.project.UserTableEntry;
import dk.dtu.imm.red.core.properties.PropertiesPackage;
import dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl;
import dk.dtu.imm.red.core.text.TextPackage;
import dk.dtu.imm.red.core.text.impl.TextPackageImpl;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;
import dk.dtu.imm.red.core.usecasepoint.impl.UsecasepointPackageImpl;
import dk.dtu.imm.red.core.user.UserPackage;
import dk.dtu.imm.red.core.user.impl.UserPackageImpl;
import dk.dtu.imm.red.core.validation.ValidationPackage;
import dk.dtu.imm.red.core.validation.impl.ValidationPackageImpl;
import dk.dtu.imm.red.core.workspace.WorkspacePackage;
import dk.dtu.imm.red.core.workspace.impl.WorkspacePackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProjectPackageImpl extends EPackageImpl implements ProjectPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userTableEntryEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectDateEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.core.project.ProjectPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ProjectPackageImpl() {
		super(eNS_URI, ProjectFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ProjectPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ProjectPackage init() {
		if (isInited) return (ProjectPackage)EPackage.Registry.INSTANCE.getEPackage(ProjectPackage.eNS_URI);

		// Obtain or create and register package
		ProjectPackageImpl theProjectPackage = (ProjectPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ProjectPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ProjectPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		CommentPackageImpl theCommentPackage = (CommentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CommentPackage.eNS_URI) instanceof CommentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CommentPackage.eNS_URI) : CommentPackage.eINSTANCE);
		ElementPackageImpl theElementPackage = (ElementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI) instanceof ElementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI) : ElementPackage.eINSTANCE);
		GroupPackageImpl theGroupPackage = (GroupPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GroupPackage.eNS_URI) instanceof GroupPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GroupPackage.eNS_URI) : GroupPackage.eINSTANCE);
		ContributionPackageImpl theContributionPackage = (ContributionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ContributionPackage.eNS_URI) instanceof ContributionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ContributionPackage.eNS_URI) : ContributionPackage.eINSTANCE);
		RelationshipPackageImpl theRelationshipPackage = (RelationshipPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationshipPackage.eNS_URI) instanceof RelationshipPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationshipPackage.eNS_URI) : RelationshipPackage.eINSTANCE);
		IdPackageImpl theIdPackage = (IdPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdPackage.eNS_URI) instanceof IdPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdPackage.eNS_URI) : IdPackage.eINSTANCE);
		DocumentPackageImpl theDocumentPackage = (DocumentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DocumentPackage.eNS_URI) instanceof DocumentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DocumentPackage.eNS_URI) : DocumentPackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		TextPackageImpl theTextPackage = (TextPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI) instanceof TextPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI) : TextPackage.eINSTANCE);
		UserPackageImpl theUserPackage = (UserPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI) instanceof UserPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI) : UserPackage.eINSTANCE);
		FolderPackageImpl theFolderPackage = (FolderPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FolderPackage.eNS_URI) instanceof FolderPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FolderPackage.eNS_URI) : FolderPackage.eINSTANCE);
		WorkspacePackageImpl theWorkspacePackage = (WorkspacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WorkspacePackage.eNS_URI) instanceof WorkspacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WorkspacePackage.eNS_URI) : WorkspacePackage.eINSTANCE);
		FilePackageImpl theFilePackage = (FilePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FilePackage.eNS_URI) instanceof FilePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FilePackage.eNS_URI) : FilePackage.eINSTANCE);
		PropertiesPackageImpl thePropertiesPackage = (PropertiesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PropertiesPackage.eNS_URI) instanceof PropertiesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PropertiesPackage.eNS_URI) : PropertiesPackage.eINSTANCE);
		UsecasepointPackageImpl theUsecasepointPackage = (UsecasepointPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UsecasepointPackage.eNS_URI) instanceof UsecasepointPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UsecasepointPackage.eNS_URI) : UsecasepointPackage.eINSTANCE);

		// Create package meta-data objects
		theProjectPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theCommentPackage.createPackageContents();
		theElementPackage.createPackageContents();
		theGroupPackage.createPackageContents();
		theContributionPackage.createPackageContents();
		theRelationshipPackage.createPackageContents();
		theIdPackage.createPackageContents();
		theDocumentPackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theTextPackage.createPackageContents();
		theUserPackage.createPackageContents();
		theFolderPackage.createPackageContents();
		theWorkspacePackage.createPackageContents();
		theFilePackage.createPackageContents();
		thePropertiesPackage.createPackageContents();
		theUsecasepointPackage.createPackageContents();

		// Initialize created meta-data
		theProjectPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theCommentPackage.initializePackageContents();
		theElementPackage.initializePackageContents();
		theGroupPackage.initializePackageContents();
		theContributionPackage.initializePackageContents();
		theRelationshipPackage.initializePackageContents();
		theIdPackage.initializePackageContents();
		theDocumentPackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theTextPackage.initializePackageContents();
		theUserPackage.initializePackageContents();
		theFolderPackage.initializePackageContents();
		theWorkspacePackage.initializePackageContents();
		theFilePackage.initializePackageContents();
		thePropertiesPackage.initializePackageContents();
		theUsecasepointPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theProjectPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ProjectPackage.eNS_URI, theProjectPackage);
		return theProjectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProject() {
		return projectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProject_Users() {
		return (EReference)projectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProject_ProjectDates() {
		return (EReference)projectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProject_Information() {
		return (EReference)projectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProject_Listofopenedfilesinproject() {
		return (EReference)projectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProject_Effort() {
		return (EReference)projectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProject_License() {
		return (EAttribute)projectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUserTableEntry() {
		return userTableEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUserTableEntry_User() {
		return (EReference)userTableEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserTableEntry_Comment() {
		return (EAttribute)userTableEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProjectDate() {
		return projectDateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProjectDate_Date() {
		return (EAttribute)projectDateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProjectDate_Comment() {
		return (EAttribute)projectDateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProjectFactory getProjectFactory() {
		return (ProjectFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		projectEClass = createEClass(PROJECT);
		createEReference(projectEClass, PROJECT__USERS);
		createEReference(projectEClass, PROJECT__PROJECT_DATES);
		createEReference(projectEClass, PROJECT__INFORMATION);
		createEReference(projectEClass, PROJECT__LISTOFOPENEDFILESINPROJECT);
		createEReference(projectEClass, PROJECT__EFFORT);
		createEAttribute(projectEClass, PROJECT__LICENSE);

		userTableEntryEClass = createEClass(USER_TABLE_ENTRY);
		createEReference(userTableEntryEClass, USER_TABLE_ENTRY__USER);
		createEAttribute(userTableEntryEClass, USER_TABLE_ENTRY__COMMENT);

		projectDateEClass = createEClass(PROJECT_DATE);
		createEAttribute(projectDateEClass, PROJECT_DATE__DATE);
		createEAttribute(projectDateEClass, PROJECT_DATE__COMMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GroupPackage theGroupPackage = (GroupPackage)EPackage.Registry.INSTANCE.getEPackage(GroupPackage.eNS_URI);
		TextPackage theTextPackage = (TextPackage)EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI);
		FilePackage theFilePackage = (FilePackage)EPackage.Registry.INSTANCE.getEPackage(FilePackage.eNS_URI);
		UsecasepointPackage theUsecasepointPackage = (UsecasepointPackage)EPackage.Registry.INSTANCE.getEPackage(UsecasepointPackage.eNS_URI);
		UserPackage theUserPackage = (UserPackage)EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		projectEClass.getESuperTypes().add(theGroupPackage.getGroup());

		// Initialize classes and features; add operations and parameters
		initEClass(projectEClass, Project.class, "Project", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProject_Users(), this.getUserTableEntry(), null, "users", null, 0, -1, Project.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProject_ProjectDates(), this.getProjectDate(), null, "projectDates", null, 0, -1, Project.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProject_Information(), theTextPackage.getText(), null, "information", null, 0, 1, Project.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProject_Listofopenedfilesinproject(), theFilePackage.getFile(), null, "Listofopenedfilesinproject", null, 0, -1, Project.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProject_Effort(), theUsecasepointPackage.getUsecasePoint(), null, "effort", null, 0, 1, Project.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProject_License(), ecorePackage.getEString(), "license", null, 0, 1, Project.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(projectEClass, null, "addToListOfFilesInProject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theFilePackage.getFile(), "fileToAdd", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(projectEClass, null, "createDefaultUsecasePoint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(userTableEntryEClass, UserTableEntry.class, "UserTableEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUserTableEntry_User(), theUserPackage.getUser(), null, "user", null, 0, 1, UserTableEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserTableEntry_Comment(), theEcorePackage.getEString(), "comment", null, 0, 1, UserTableEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(projectDateEClass, ProjectDate.class, "ProjectDate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProjectDate_Date(), theEcorePackage.getEString(), "date", null, 0, 1, ProjectDate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProjectDate_Comment(), theEcorePackage.getEString(), "comment", null, 0, 1, ProjectDate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //ProjectPackageImpl
