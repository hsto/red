/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Lock Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.element.ElementPackage#getLockType()
 * @model
 * @generated
 */
public enum LockType implements Enumerator {
	/**
	 * The '<em><b>LOCK LIKE CONTAINER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOCK_LIKE_CONTAINER_VALUE
	 * @generated
	 * @ordered
	 */
	LOCK_LIKE_CONTAINER(0, "LOCK_LIKE_CONTAINER", "Like Parent"), /**
	 * The '<em><b>LOCK NOTHING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOCK_NOTHING_VALUE
	 * @generated
	 * @ordered
	 */
	LOCK_NOTHING(1, "LOCK_NOTHING", "Editable"), /**
	 * The '<em><b>LOCK COMMENTING ONLY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOCK_COMMENTING_ONLY_VALUE
	 * @generated
	 * @ordered
	 */
	LOCK_COMMENTING_ONLY(2, "LOCK_COMMENTING_ONLY", "Commenting Only"), /**
	 * The '<em><b>LOCK CONTENTS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOCK_CONTENTS_VALUE
	 * @generated
	 * @ordered
	 */
	LOCK_CONTENTS(3, "LOCK_CONTENTS", "Locked");

	/**
	 * The '<em><b>LOCK LIKE CONTAINER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LOCK LIKE CONTAINER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LOCK_LIKE_CONTAINER
	 * @model literal="Like Parent"
	 * @generated
	 * @ordered
	 */
	public static final int LOCK_LIKE_CONTAINER_VALUE = 0;

	/**
	 * The '<em><b>LOCK NOTHING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LOCK NOTHING</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LOCK_NOTHING
	 * @model literal="Editable"
	 * @generated
	 * @ordered
	 */
	public static final int LOCK_NOTHING_VALUE = 1;

	/**
	 * The '<em><b>LOCK COMMENTING ONLY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LOCK COMMENTING ONLY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LOCK_COMMENTING_ONLY
	 * @model literal="Commenting Only"
	 * @generated
	 * @ordered
	 */
	public static final int LOCK_COMMENTING_ONLY_VALUE = 2;

	/**
	 * The '<em><b>LOCK CONTENTS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LOCK CONTENTS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LOCK_CONTENTS
	 * @model literal="Locked"
	 * @generated
	 * @ordered
	 */
	public static final int LOCK_CONTENTS_VALUE = 3;

	/**
	 * An array of all the '<em><b>Lock Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final LockType[] VALUES_ARRAY =
		new LockType[] {
			LOCK_LIKE_CONTAINER,
			LOCK_NOTHING,
			LOCK_COMMENTING_ONLY,
			LOCK_CONTENTS,
		};

	/**
	 * A public read-only list of all the '<em><b>Lock Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<LockType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Lock Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static LockType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LockType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Lock Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static LockType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LockType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Lock Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static LockType get(int value) {
		switch (value) {
			case LOCK_LIKE_CONTAINER_VALUE: return LOCK_LIKE_CONTAINER;
			case LOCK_NOTHING_VALUE: return LOCK_NOTHING;
			case LOCK_COMMENTING_ONLY_VALUE: return LOCK_COMMENTING_ONLY;
			case LOCK_CONTENTS_VALUE: return LOCK_CONTENTS;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private LockType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //LockType
