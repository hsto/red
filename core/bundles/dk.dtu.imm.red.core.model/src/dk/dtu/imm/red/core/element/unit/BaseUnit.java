/**
 */
package dk.dtu.imm.red.core.element.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.core.element.unit.UnitPackage#getBaseUnit()
 * @model
 * @generated
 */
public interface BaseUnit extends Unit {
} // BaseUnit
