/**
 */
package dk.dtu.imm.red.core.element.document.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import dk.dtu.imm.red.core.element.CaptionedImage;
import dk.dtu.imm.red.core.element.CaptionedImageList;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.element.document.DocumentPackage;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.text.Text;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.document.impl.DocumentImpl#getAbstract <em>Abstract</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.impl.DocumentImpl#getInternalRef <em>Internal Ref</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.impl.DocumentImpl#getExternalFileRef <em>External File Ref</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.impl.DocumentImpl#getWebRef <em>Web Ref</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.impl.DocumentImpl#getTextContent <em>Text Content</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.impl.DocumentImpl#getCaptionedImageList <em>Captioned Image List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DocumentImpl extends ElementImpl implements Document {
	/**
	 * The cached value of the '{@link #getAbstract() <em>Abstract</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstract()
	 * @generated
	 * @ordered
	 */
	protected Text abstract_;

	/**
	 * The cached value of the '{@link #getInternalRef() <em>Internal Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalRef()
	 * @generated
	 * @ordered
	 */
	protected Element internalRef;

	/**
	 * The default value of the '{@link #getExternalFileRef() <em>External File Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalFileRef()
	 * @generated
	 * @ordered
	 */
	protected static final String EXTERNAL_FILE_REF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExternalFileRef() <em>External File Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalFileRef()
	 * @generated
	 * @ordered
	 */
	protected String externalFileRef = EXTERNAL_FILE_REF_EDEFAULT;

	/**
	 * The default value of the '{@link #getWebRef() <em>Web Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWebRef()
	 * @generated
	 * @ordered
	 */
	protected static final String WEB_REF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWebRef() <em>Web Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWebRef()
	 * @generated
	 * @ordered
	 */
	protected String webRef = WEB_REF_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTextContent() <em>Text Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTextContent()
	 * @generated
	 * @ordered
	 */
	protected Text textContent;

	/**
	 * The cached value of the '{@link #getCaptionedImageList() <em>Captioned Image List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCaptionedImageList()
	 * @generated
	 * @ordered
	 */
	protected CaptionedImageList captionedImageList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DocumentPackage.Literals.DOCUMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getAbstract() {
		if (abstract_ != null && abstract_.eIsProxy()) {
			InternalEObject oldAbstract = (InternalEObject)abstract_;
			abstract_ = (Text)eResolveProxy(oldAbstract);
			if (abstract_ != oldAbstract) {
				InternalEObject newAbstract = (InternalEObject)abstract_;
				NotificationChain msgs = oldAbstract.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__ABSTRACT, null, null);
				if (newAbstract.eInternalContainer() == null) {
					msgs = newAbstract.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__ABSTRACT, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DocumentPackage.DOCUMENT__ABSTRACT, oldAbstract, abstract_));
			}
		}
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstract(Text newAbstract, NotificationChain msgs) {
		Text oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DocumentPackage.DOCUMENT__ABSTRACT, oldAbstract, newAbstract);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(Text newAbstract) {
		if (newAbstract != abstract_) {
			NotificationChain msgs = null;
			if (abstract_ != null)
				msgs = ((InternalEObject)abstract_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__ABSTRACT, null, msgs);
			if (newAbstract != null)
				msgs = ((InternalEObject)newAbstract).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__ABSTRACT, null, msgs);
			msgs = basicSetAbstract(newAbstract, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DocumentPackage.DOCUMENT__ABSTRACT, newAbstract, newAbstract));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element getInternalRef() {
		if (internalRef != null && internalRef.eIsProxy()) {
			InternalEObject oldInternalRef = (InternalEObject)internalRef;
			internalRef = (Element)eResolveProxy(oldInternalRef);
			if (internalRef != oldInternalRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DocumentPackage.DOCUMENT__INTERNAL_REF, oldInternalRef, internalRef));
			}
		}
		return internalRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element basicGetInternalRef() {
		return internalRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalRef(Element newInternalRef) {
		Element oldInternalRef = internalRef;
		internalRef = newInternalRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DocumentPackage.DOCUMENT__INTERNAL_REF, oldInternalRef, internalRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExternalFileRef() {
		return externalFileRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalFileRef(String newExternalFileRef) {
		String oldExternalFileRef = externalFileRef;
		externalFileRef = newExternalFileRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DocumentPackage.DOCUMENT__EXTERNAL_FILE_REF, oldExternalFileRef, externalFileRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getWebRef() {
		return webRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWebRef(String newWebRef) {
		String oldWebRef = webRef;
		webRef = newWebRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DocumentPackage.DOCUMENT__WEB_REF, oldWebRef, webRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getTextContent() {
		if (textContent != null && textContent.eIsProxy()) {
			InternalEObject oldTextContent = (InternalEObject)textContent;
			textContent = (Text)eResolveProxy(oldTextContent);
			if (textContent != oldTextContent) {
				InternalEObject newTextContent = (InternalEObject)textContent;
				NotificationChain msgs = oldTextContent.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__TEXT_CONTENT, null, null);
				if (newTextContent.eInternalContainer() == null) {
					msgs = newTextContent.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__TEXT_CONTENT, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DocumentPackage.DOCUMENT__TEXT_CONTENT, oldTextContent, textContent));
			}
		}
		return textContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetTextContent() {
		return textContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTextContent(Text newTextContent, NotificationChain msgs) {
		Text oldTextContent = textContent;
		textContent = newTextContent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DocumentPackage.DOCUMENT__TEXT_CONTENT, oldTextContent, newTextContent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTextContent(Text newTextContent) {
		if (newTextContent != textContent) {
			NotificationChain msgs = null;
			if (textContent != null)
				msgs = ((InternalEObject)textContent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__TEXT_CONTENT, null, msgs);
			if (newTextContent != null)
				msgs = ((InternalEObject)newTextContent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__TEXT_CONTENT, null, msgs);
			msgs = basicSetTextContent(newTextContent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DocumentPackage.DOCUMENT__TEXT_CONTENT, newTextContent, newTextContent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaptionedImageList getCaptionedImageList() {
		if (captionedImageList != null && captionedImageList.eIsProxy()) {
			InternalEObject oldCaptionedImageList = (InternalEObject)captionedImageList;
			captionedImageList = (CaptionedImageList)eResolveProxy(oldCaptionedImageList);
			if (captionedImageList != oldCaptionedImageList) {
				InternalEObject newCaptionedImageList = (InternalEObject)captionedImageList;
				NotificationChain msgs = oldCaptionedImageList.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST, null, null);
				if (newCaptionedImageList.eInternalContainer() == null) {
					msgs = newCaptionedImageList.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST, oldCaptionedImageList, captionedImageList));
			}
		}
		return captionedImageList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaptionedImageList basicGetCaptionedImageList() {
		return captionedImageList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCaptionedImageList(CaptionedImageList newCaptionedImageList, NotificationChain msgs) {
		CaptionedImageList oldCaptionedImageList = captionedImageList;
		captionedImageList = newCaptionedImageList;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST, oldCaptionedImageList, newCaptionedImageList);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCaptionedImageList(CaptionedImageList newCaptionedImageList) {
		if (newCaptionedImageList != captionedImageList) {
			NotificationChain msgs = null;
			if (captionedImageList != null)
				msgs = ((InternalEObject)captionedImageList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST, null, msgs);
			if (newCaptionedImageList != null)
				msgs = ((InternalEObject)newCaptionedImageList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST, null, msgs);
			msgs = basicSetCaptionedImageList(newCaptionedImageList, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST, newCaptionedImageList, newCaptionedImageList));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DocumentPackage.DOCUMENT__ABSTRACT:
				return basicSetAbstract(null, msgs);
			case DocumentPackage.DOCUMENT__TEXT_CONTENT:
				return basicSetTextContent(null, msgs);
			case DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST:
				return basicSetCaptionedImageList(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DocumentPackage.DOCUMENT__ABSTRACT:
				if (resolve) return getAbstract();
				return basicGetAbstract();
			case DocumentPackage.DOCUMENT__INTERNAL_REF:
				if (resolve) return getInternalRef();
				return basicGetInternalRef();
			case DocumentPackage.DOCUMENT__EXTERNAL_FILE_REF:
				return getExternalFileRef();
			case DocumentPackage.DOCUMENT__WEB_REF:
				return getWebRef();
			case DocumentPackage.DOCUMENT__TEXT_CONTENT:
				if (resolve) return getTextContent();
				return basicGetTextContent();
			case DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST:
				if (resolve) return getCaptionedImageList();
				return basicGetCaptionedImageList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DocumentPackage.DOCUMENT__ABSTRACT:
				setAbstract((Text)newValue);
				return;
			case DocumentPackage.DOCUMENT__INTERNAL_REF:
				setInternalRef((Element)newValue);
				return;
			case DocumentPackage.DOCUMENT__EXTERNAL_FILE_REF:
				setExternalFileRef((String)newValue);
				return;
			case DocumentPackage.DOCUMENT__WEB_REF:
				setWebRef((String)newValue);
				return;
			case DocumentPackage.DOCUMENT__TEXT_CONTENT:
				setTextContent((Text)newValue);
				return;
			case DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST:
				setCaptionedImageList((CaptionedImageList)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DocumentPackage.DOCUMENT__ABSTRACT:
				setAbstract((Text)null);
				return;
			case DocumentPackage.DOCUMENT__INTERNAL_REF:
				setInternalRef((Element)null);
				return;
			case DocumentPackage.DOCUMENT__EXTERNAL_FILE_REF:
				setExternalFileRef(EXTERNAL_FILE_REF_EDEFAULT);
				return;
			case DocumentPackage.DOCUMENT__WEB_REF:
				setWebRef(WEB_REF_EDEFAULT);
				return;
			case DocumentPackage.DOCUMENT__TEXT_CONTENT:
				setTextContent((Text)null);
				return;
			case DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST:
				setCaptionedImageList((CaptionedImageList)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DocumentPackage.DOCUMENT__ABSTRACT:
				return abstract_ != null;
			case DocumentPackage.DOCUMENT__INTERNAL_REF:
				return internalRef != null;
			case DocumentPackage.DOCUMENT__EXTERNAL_FILE_REF:
				return EXTERNAL_FILE_REF_EDEFAULT == null ? externalFileRef != null : !EXTERNAL_FILE_REF_EDEFAULT.equals(externalFileRef);
			case DocumentPackage.DOCUMENT__WEB_REF:
				return WEB_REF_EDEFAULT == null ? webRef != null : !WEB_REF_EDEFAULT.equals(webRef);
			case DocumentPackage.DOCUMENT__TEXT_CONTENT:
				return textContent != null;
			case DocumentPackage.DOCUMENT__CAPTIONED_IMAGE_LIST:
				return captionedImageList != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (externalFileRef: ");
		result.append(externalFileRef);
		result.append(", webRef: ");
		result.append(webRef);
		result.append(')');
		return result.toString();
	}

	@Override
	public String getIconURI() {
		return "icons/document.png";
	}

	//Issue #31
	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__LABEL,
				this.getLabel()==null ? "" : this.getLabel().toString());
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__NAME,
				this.getName()==null ? "" : this.getName().toString());
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__ABSTRACT,
				this.getAbstract()==null ? "" : this.getAbstract().toString());
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__INTERNAL_REF,
				this.getInternalRef()==null ? "" : this.getInternalRef().toString());
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__EXTERNAL_FILE_REF,
				this.getExternalFileRef()==null ? "" : this.getExternalFileRef().toString());
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__WEB_REF,
				this.getWebRef()==null ? "" : this.getWebRef().toString());
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__TEXT_CONTENT,
				this.getTextContent()==null ? "" : this.getTextContent().toString());
		if(this.getCaptionedImageList()!=null && this.getCaptionedImageList().getCaptionedImage()!=null){
			for(CaptionedImage image:this.getCaptionedImageList().getCaptionedImage()){
				attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__CAPTIONED_IMAGE_LIST,
						image.getCaption()==null ? "" : image.getCaption().toString());
			}
		}

		return super.search(param, attributesToSearch);
	}

} //DocumentImpl
