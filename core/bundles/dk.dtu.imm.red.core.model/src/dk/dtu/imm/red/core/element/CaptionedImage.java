/**
 */
package dk.dtu.imm.red.core.element;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Captioned Image</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.CaptionedImage#getCaption <em>Caption</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.CaptionedImage#getImageData <em>Image Data</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.ElementPackage#getCaptionedImage()
 * @model
 * @generated
 */
public interface CaptionedImage extends Element {
	/**
	 * Returns the value of the '<em><b>Caption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Caption</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Caption</em>' attribute.
	 * @see #setCaption(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getCaptionedImage_Caption()
	 * @model
	 * @generated
	 */
	String getCaption();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.CaptionedImage#getCaption <em>Caption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Caption</em>' attribute.
	 * @see #getCaption()
	 * @generated
	 */
	void setCaption(String value);

	/**
	 * Returns the value of the '<em><b>Image Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Data</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Data</em>' attribute.
	 * @see #setImageData(byte[])
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getCaptionedImage_ImageData()
	 * @model
	 * @generated
	 */
	byte[] getImageData();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.CaptionedImage#getImageData <em>Image Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Data</em>' attribute.
	 * @see #getImageData()
	 * @generated
	 */
	void setImageData(byte[] value);

} // CaptionedImage
