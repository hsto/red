/**
 */
package dk.dtu.imm.red.core.usecasepoint.impl;

import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.properties.Kind;
import dk.dtu.imm.red.core.properties.PropertiesFactory;
import dk.dtu.imm.red.core.properties.Property;
import dk.dtu.imm.red.core.usecasepoint.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UsecasepointFactoryImpl extends EFactoryImpl implements UsecasepointFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UsecasepointFactory init() {
		try {
			UsecasepointFactory theUsecasepointFactory = (UsecasepointFactory)EPackage.Registry.INSTANCE.getEFactory(UsecasepointPackage.eNS_URI);
			if (theUsecasepointFactory != null) {
				return theUsecasepointFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UsecasepointFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsecasepointFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UsecasepointPackage.USECASE_POINT: return createUsecasePoint();
			case UsecasepointPackage.WEIGHTED_FACTOR: return createWeightedFactor();
			case UsecasepointPackage.MANAGEMENT_FACTOR: return createManagementFactor();
			case UsecasepointPackage.TECHNOLOGY_FACTOR: return createTechnologyFactor();
			case UsecasepointPackage.PRODUCTIVITY_FACTOR: return createProductivityFactor();
			case UsecasepointPackage.REQUIREMENT_FACTOR: return createRequirementFactor();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case UsecasepointPackage.WEIGHTED_FACTOR_VALUE:
				return createWeightedFactorValueFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case UsecasepointPackage.WEIGHTED_FACTOR_VALUE:
				return convertWeightedFactorValueToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public UsecasePoint createUsecasePoint() {
		UsecasePointImpl usecasePoint = new UsecasePointImpl(); 
		return usecasePoint;
	} 

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ManagementFactor createManagementFactor() {
		ManagementFactorImpl managementFactor = new ManagementFactorImpl();
		return managementFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WeightedFactor createWeightedFactor() {
		WeightedFactorImpl weightedFactor = new WeightedFactorImpl();
		return weightedFactor;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public WeightedFactor createTechnologyWeightedFactor() {
		//Should be in a Factory method
				WeightedFactor mfa = new WeightedFactorImpl() { 
					@Override
					public double getSum() {
						return this.getWeight() * getFactorValue().getValue();
					} 
				};
		return mfa;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductivityFactor createProductivityFactor() {
		ProductivityFactorImpl productivityFactor = new ProductivityFactorImpl();
		return productivityFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TechnologyFactor createTechnologyFactor() {
		TechnologyFactorImpl technologyFactor = new TechnologyFactorImpl();
		return technologyFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementFactor createRequirementFactor() {
		RequirementFactorImpl requirementFactor = new RequirementFactorImpl();
		return requirementFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WeightedFactorValue createWeightedFactorValueFromString(EDataType eDataType, String initialValue) {
		WeightedFactorValue result = WeightedFactorValue.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertWeightedFactorValueToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsecasepointPackage getUsecasepointPackage() {
		return (UsecasepointPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UsecasepointPackage getPackage() {
		return UsecasepointPackage.eINSTANCE;
	}

} //UsecasepointFactoryImpl
