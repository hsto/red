/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.id;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Visible Element ID</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.id.VisibleElementID#getType <em>Type</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.id.VisibleElementID#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.id.IdPackage#getVisibleElementID()
 * @model
 * @generated
 */
public interface VisibleElementID extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.id.VisibleIDType}.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.id.VisibleIDType
	 * @see #setType(VisibleIDType)
	 * @see dk.dtu.imm.red.core.element.id.IdPackage#getVisibleElementID_Type()
	 * @model
	 * @generated
	 */
	VisibleIDType getType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.id.VisibleElementID#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.id.VisibleIDType
	 * @see #getType()
	 * @generated
	 */
	void setType(VisibleIDType value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.id.IdPackage#getVisibleElementID_Value()
	 * @model changeable="false"
	 * @generated
	 */
	String getValue();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void updateValue(String newValue);

} // VisibleElementID
