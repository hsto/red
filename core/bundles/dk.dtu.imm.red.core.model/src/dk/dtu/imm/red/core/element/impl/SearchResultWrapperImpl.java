/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Search Result Wrapper</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.impl.SearchResultWrapperImpl#getElementResults <em>Element Results</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.impl.SearchResultWrapperImpl#getElement <em>Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.impl.SearchResultWrapperImpl#getChildResults <em>Child Results</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SearchResultWrapperImpl extends EObjectImpl implements SearchResultWrapper {
	/**
	 * The cached value of the '{@link #getElementResults() <em>Element Results</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementResults()
	 * @generated
	 * @ordered
	 */
	protected EList<SearchResult> elementResults;

	/**
	 * The cached value of the '{@link #getElement() <em>Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement()
	 * @generated
	 * @ordered
	 */
	protected Element element;

	/**
	 * The cached value of the '{@link #getChildResults() <em>Child Results</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildResults()
	 * @generated
	 * @ordered
	 */
	protected EList<SearchResultWrapper> childResults;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SearchResultWrapperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ElementPackage.Literals.SEARCH_RESULT_WRAPPER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SearchResult> getElementResults() {
		if (elementResults == null) {
			elementResults = new EObjectResolvingEList<SearchResult>(SearchResult.class, this, ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT_RESULTS);
		}
		return elementResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element getElement() {
		if (element != null && element.eIsProxy()) {
			InternalEObject oldElement = (InternalEObject)element;
			element = (Element)eResolveProxy(oldElement);
			if (element != oldElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT, oldElement, element));
			}
		}
		return element;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element basicGetElement() {
		return element;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElement(Element newElement) {
		Element oldElement = element;
		element = newElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT, oldElement, element));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SearchResultWrapper> getChildResults() {
		if (childResults == null) {
			childResults = new EObjectResolvingEList<SearchResultWrapper>(SearchResultWrapper.class, this, ElementPackage.SEARCH_RESULT_WRAPPER__CHILD_RESULTS);
		}
		return childResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT_RESULTS:
				return getElementResults();
			case ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT:
				if (resolve) return getElement();
				return basicGetElement();
			case ElementPackage.SEARCH_RESULT_WRAPPER__CHILD_RESULTS:
				return getChildResults();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT_RESULTS:
				getElementResults().clear();
				getElementResults().addAll((Collection<? extends SearchResult>)newValue);
				return;
			case ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT:
				setElement((Element)newValue);
				return;
			case ElementPackage.SEARCH_RESULT_WRAPPER__CHILD_RESULTS:
				getChildResults().clear();
				getChildResults().addAll((Collection<? extends SearchResultWrapper>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT_RESULTS:
				getElementResults().clear();
				return;
			case ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT:
				setElement((Element)null);
				return;
			case ElementPackage.SEARCH_RESULT_WRAPPER__CHILD_RESULTS:
				getChildResults().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT_RESULTS:
				return elementResults != null && !elementResults.isEmpty();
			case ElementPackage.SEARCH_RESULT_WRAPPER__ELEMENT:
				return element != null;
			case ElementPackage.SEARCH_RESULT_WRAPPER__CHILD_RESULTS:
				return childResults != null && !childResults.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SearchResultWrapperImpl
