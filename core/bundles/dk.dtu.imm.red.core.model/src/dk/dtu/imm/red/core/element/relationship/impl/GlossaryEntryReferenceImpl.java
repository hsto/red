/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.relationship.impl;

import org.eclipse.emf.ecore.EClass;

import dk.dtu.imm.red.core.element.relationship.GlossaryEntryReference;
import dk.dtu.imm.red.core.element.relationship.RelationshipPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Glossary Entry Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GlossaryEntryReferenceImpl extends ElementRelationshipImpl implements GlossaryEntryReference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GlossaryEntryReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RelationshipPackage.Literals.GLOSSARY_ENTRY_REFERENCE;
	}

} //GlossaryEntryReferenceImpl
