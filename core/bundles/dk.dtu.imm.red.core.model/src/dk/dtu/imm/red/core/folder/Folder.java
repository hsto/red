/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.folder;

import dk.dtu.imm.red.core.element.group.Group;


/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Folder</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.folder.Folder#getSpecialType <em>Special Type</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.folder.FolderPackage#getFolder()
 * @model
 * @generated
 */
public interface Folder extends Group {

	/**
	 * Returns the value of the '<em><b>Special Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Special Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Special Type</em>' attribute.
	 * @see #setSpecialType(String)
	 * @see dk.dtu.imm.red.core.folder.FolderPackage#getFolder_SpecialType()
	 * @model
	 * @generated
	 */
	String getSpecialType();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.folder.Folder#getSpecialType <em>Special Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Special Type</em>' attribute.
	 * @see #getSpecialType()
	 * @generated
	 */
	void setSpecialType(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	double computeRequirementsFactor(boolean recursive);
} // Folder
