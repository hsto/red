/**
 */
package dk.dtu.imm.red.core.usecasepoint.util;

import dk.dtu.imm.red.core.element.Checkable;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.usecasepoint.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage
 * @generated
 */
public class UsecasepointSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UsecasepointPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsecasepointSwitch() {
		if (modelPackage == null) {
			modelPackage = UsecasepointPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case UsecasepointPackage.FACTOR: {
				Factor factor = (Factor)theEObject;
				T result = caseFactor(factor);
				if (result == null) result = caseElement(factor);
				if (result == null) result = caseCheckable(factor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UsecasepointPackage.USECASE_POINT: {
				UsecasePoint usecasePoint = (UsecasePoint)theEObject;
				T result = caseUsecasePoint(usecasePoint);
				if (result == null) result = caseFactor(usecasePoint);
				if (result == null) result = caseElement(usecasePoint);
				if (result == null) result = caseCheckable(usecasePoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UsecasepointPackage.WEIGHTED_FACTOR: {
				WeightedFactor weightedFactor = (WeightedFactor)theEObject;
				T result = caseWeightedFactor(weightedFactor);
				if (result == null) result = caseFactor(weightedFactor);
				if (result == null) result = caseElement(weightedFactor);
				if (result == null) result = caseCheckable(weightedFactor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UsecasepointPackage.WEIGHTED_FACTOR_CONTAINER: {
				WeightedFactorContainer weightedFactorContainer = (WeightedFactorContainer)theEObject;
				T result = caseWeightedFactorContainer(weightedFactorContainer);
				if (result == null) result = caseFactor(weightedFactorContainer);
				if (result == null) result = caseElement(weightedFactorContainer);
				if (result == null) result = caseCheckable(weightedFactorContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UsecasepointPackage.MANAGEMENT_FACTOR: {
				ManagementFactor managementFactor = (ManagementFactor)theEObject;
				T result = caseManagementFactor(managementFactor);
				if (result == null) result = caseWeightedFactorContainer(managementFactor);
				if (result == null) result = caseFactor(managementFactor);
				if (result == null) result = caseElement(managementFactor);
				if (result == null) result = caseCheckable(managementFactor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UsecasepointPackage.TECHNOLOGY_FACTOR: {
				TechnologyFactor technologyFactor = (TechnologyFactor)theEObject;
				T result = caseTechnologyFactor(technologyFactor);
				if (result == null) result = caseWeightedFactorContainer(technologyFactor);
				if (result == null) result = caseFactor(technologyFactor);
				if (result == null) result = caseElement(technologyFactor);
				if (result == null) result = caseCheckable(technologyFactor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UsecasepointPackage.PRODUCTIVITY_FACTOR: {
				ProductivityFactor productivityFactor = (ProductivityFactor)theEObject;
				T result = caseProductivityFactor(productivityFactor);
				if (result == null) result = caseFactor(productivityFactor);
				if (result == null) result = caseElement(productivityFactor);
				if (result == null) result = caseCheckable(productivityFactor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UsecasepointPackage.REQUIREMENT_FACTOR: {
				RequirementFactor requirementFactor = (RequirementFactor)theEObject;
				T result = caseRequirementFactor(requirementFactor);
				if (result == null) result = caseFactor(requirementFactor);
				if (result == null) result = caseElement(requirementFactor);
				if (result == null) result = caseCheckable(requirementFactor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Factor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Factor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFactor(Factor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Usecase Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Usecase Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUsecasePoint(UsecasePoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Management Factor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Management Factor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseManagementFactor(ManagementFactor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Weighted Factor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Weighted Factor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWeightedFactor(WeightedFactor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Weighted Factor Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Weighted Factor Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWeightedFactorContainer(WeightedFactorContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Productivity Factor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Productivity Factor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProductivityFactor(ProductivityFactor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Technology Factor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Technology Factor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTechnologyFactor(TechnologyFactor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requirement Factor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requirement Factor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequirementFactor(RequirementFactor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Checkable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Checkable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCheckable(Checkable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElement(Element object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //UsecasepointSwitch
