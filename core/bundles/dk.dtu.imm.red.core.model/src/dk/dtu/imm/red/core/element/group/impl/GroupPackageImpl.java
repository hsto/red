/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.group.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import dk.dtu.imm.red.core.CorePackage;
import dk.dtu.imm.red.core.comment.CommentPackage;
import dk.dtu.imm.red.core.comment.impl.CommentPackageImpl;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.contribution.ContributionPackage;
import dk.dtu.imm.red.core.element.contribution.impl.ContributionPackageImpl;
import dk.dtu.imm.red.core.element.document.DocumentPackage;
import dk.dtu.imm.red.core.element.document.impl.DocumentPackageImpl;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.group.GroupFactory;
import dk.dtu.imm.red.core.element.group.GroupPackage;
import dk.dtu.imm.red.core.element.id.IdPackage;
import dk.dtu.imm.red.core.element.id.impl.IdPackageImpl;
import dk.dtu.imm.red.core.element.impl.ElementPackageImpl;
import dk.dtu.imm.red.core.element.relationship.RelationshipPackage;
import dk.dtu.imm.red.core.element.relationship.impl.RelationshipPackageImpl;
import dk.dtu.imm.red.core.element.unit.UnitPackage;
import dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl;
import dk.dtu.imm.red.core.file.FilePackage;
import dk.dtu.imm.red.core.file.impl.FilePackageImpl;
import dk.dtu.imm.red.core.folder.FolderPackage;
import dk.dtu.imm.red.core.folder.impl.FolderPackageImpl;
import dk.dtu.imm.red.core.impl.CorePackageImpl;
import dk.dtu.imm.red.core.project.ProjectPackage;
import dk.dtu.imm.red.core.project.impl.ProjectPackageImpl;
import dk.dtu.imm.red.core.properties.PropertiesPackage;
import dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl;
import dk.dtu.imm.red.core.text.TextPackage;
import dk.dtu.imm.red.core.text.impl.TextPackageImpl;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;
import dk.dtu.imm.red.core.usecasepoint.impl.UsecasepointPackageImpl;
import dk.dtu.imm.red.core.user.UserPackage;
import dk.dtu.imm.red.core.user.impl.UserPackageImpl;
import dk.dtu.imm.red.core.validation.ValidationPackage;
import dk.dtu.imm.red.core.validation.impl.ValidationPackageImpl;
import dk.dtu.imm.red.core.workspace.WorkspacePackage;
import dk.dtu.imm.red.core.workspace.impl.WorkspacePackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class GroupPackageImpl extends EPackageImpl implements GroupPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.dtu.imm.red.core.element.group.GroupPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GroupPackageImpl() {
		super(eNS_URI, GroupFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link GroupPackage#eINSTANCE} when
	 * that field is accessed. Clients should not invoke it directly. Instead,
	 * they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GroupPackage init() {
		if (isInited) return (GroupPackage)EPackage.Registry.INSTANCE.getEPackage(GroupPackage.eNS_URI);

		// Obtain or create and register package
		GroupPackageImpl theGroupPackage = (GroupPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GroupPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GroupPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		CommentPackageImpl theCommentPackage = (CommentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CommentPackage.eNS_URI) instanceof CommentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CommentPackage.eNS_URI) : CommentPackage.eINSTANCE);
		ElementPackageImpl theElementPackage = (ElementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI) instanceof ElementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI) : ElementPackage.eINSTANCE);
		ContributionPackageImpl theContributionPackage = (ContributionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ContributionPackage.eNS_URI) instanceof ContributionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ContributionPackage.eNS_URI) : ContributionPackage.eINSTANCE);
		RelationshipPackageImpl theRelationshipPackage = (RelationshipPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationshipPackage.eNS_URI) instanceof RelationshipPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationshipPackage.eNS_URI) : RelationshipPackage.eINSTANCE);
		IdPackageImpl theIdPackage = (IdPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdPackage.eNS_URI) instanceof IdPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdPackage.eNS_URI) : IdPackage.eINSTANCE);
		DocumentPackageImpl theDocumentPackage = (DocumentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DocumentPackage.eNS_URI) instanceof DocumentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DocumentPackage.eNS_URI) : DocumentPackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		TextPackageImpl theTextPackage = (TextPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI) instanceof TextPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TextPackage.eNS_URI) : TextPackage.eINSTANCE);
		UserPackageImpl theUserPackage = (UserPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI) instanceof UserPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UserPackage.eNS_URI) : UserPackage.eINSTANCE);
		FolderPackageImpl theFolderPackage = (FolderPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FolderPackage.eNS_URI) instanceof FolderPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FolderPackage.eNS_URI) : FolderPackage.eINSTANCE);
		WorkspacePackageImpl theWorkspacePackage = (WorkspacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WorkspacePackage.eNS_URI) instanceof WorkspacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WorkspacePackage.eNS_URI) : WorkspacePackage.eINSTANCE);
		FilePackageImpl theFilePackage = (FilePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FilePackage.eNS_URI) instanceof FilePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FilePackage.eNS_URI) : FilePackage.eINSTANCE);
		ProjectPackageImpl theProjectPackage = (ProjectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ProjectPackage.eNS_URI) instanceof ProjectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ProjectPackage.eNS_URI) : ProjectPackage.eINSTANCE);
		PropertiesPackageImpl thePropertiesPackage = (PropertiesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PropertiesPackage.eNS_URI) instanceof PropertiesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PropertiesPackage.eNS_URI) : PropertiesPackage.eINSTANCE);
		UsecasepointPackageImpl theUsecasepointPackage = (UsecasepointPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UsecasepointPackage.eNS_URI) instanceof UsecasepointPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UsecasepointPackage.eNS_URI) : UsecasepointPackage.eINSTANCE);

		// Create package meta-data objects
		theGroupPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theCommentPackage.createPackageContents();
		theElementPackage.createPackageContents();
		theContributionPackage.createPackageContents();
		theRelationshipPackage.createPackageContents();
		theIdPackage.createPackageContents();
		theDocumentPackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theTextPackage.createPackageContents();
		theUserPackage.createPackageContents();
		theFolderPackage.createPackageContents();
		theWorkspacePackage.createPackageContents();
		theFilePackage.createPackageContents();
		theProjectPackage.createPackageContents();
		thePropertiesPackage.createPackageContents();
		theUsecasepointPackage.createPackageContents();

		// Initialize created meta-data
		theGroupPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theCommentPackage.initializePackageContents();
		theElementPackage.initializePackageContents();
		theContributionPackage.initializePackageContents();
		theRelationshipPackage.initializePackageContents();
		theIdPackage.initializePackageContents();
		theDocumentPackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theTextPackage.initializePackageContents();
		theUserPackage.initializePackageContents();
		theFolderPackage.initializePackageContents();
		theWorkspacePackage.initializePackageContents();
		theFilePackage.initializePackageContents();
		theProjectPackage.initializePackageContents();
		thePropertiesPackage.initializePackageContents();
		theUsecasepointPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGroupPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GroupPackage.eNS_URI, theGroupPackage);
		return theGroupPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGroup() {
		return groupEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGroup_Contents() {
		return (EReference)groupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GroupFactory getGroupFactory() {
		return (GroupFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		groupEClass = createEClass(GROUP);
		createEReference(groupEClass, GROUP__CONTENTS);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ElementPackage theElementPackage = (ElementPackage)EPackage.Registry.INSTANCE.getEPackage(ElementPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		groupEClass.getESuperTypes().add(theElementPackage.getElement());

		// Initialize classes and features; add operations and parameters
		initEClass(groupEClass, Group.class, "Group", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGroup_Contents(), theElementPackage.getElement(), theElementPackage.getElement_Parent(), "contents", null, 0, -1, Group.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(groupEClass, ecorePackage.getEBoolean(), "isValidContent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theElementPackage.getElement(), "content", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(groupEClass, ecorePackage.getEBoolean(), "isValidContent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getJavaClass(), "type", 0, 1, IS_UNIQUE, IS_ORDERED);
	}

} // GroupPackageImpl
