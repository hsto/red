/**
 */
package dk.dtu.imm.red.core.usecasepoint;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Weighted Factor Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.WeightedFactorContainer#getWeightedFactors <em>Weighted Factors</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getWeightedFactorContainer()
 * @model abstract="true"
 * @generated
 */
public interface WeightedFactorContainer extends Factor {
	/**
	 * Returns the value of the '<em><b>Weighted Factors</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.usecasepoint.WeightedFactor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Weighted Factors</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Weighted Factors</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getWeightedFactorContainer_WeightedFactors()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<WeightedFactor> getWeightedFactors();

} // WeightedFactorContainer
