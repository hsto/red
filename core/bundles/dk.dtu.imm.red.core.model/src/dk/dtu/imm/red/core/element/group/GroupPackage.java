/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.group;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import dk.dtu.imm.red.core.element.ElementPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.element.group.GroupFactory
 * @model kind="package"
 * @generated
 */
public interface GroupPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "group";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.core.element.group";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "group";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	GroupPackage eINSTANCE = dk.dtu.imm.red.core.element.group.impl.GroupPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.group.impl.GroupImpl <em>Group</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.group.impl.GroupImpl
	 * @see dk.dtu.imm.red.core.element.group.impl.GroupPackageImpl#getGroup()
	 * @generated
	 */
	int GROUP = 0;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUP__ICON_URI = ElementPackage.ELEMENT__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUP__ICON = ElementPackage.ELEMENT__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__LABEL = ElementPackage.ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__NAME = ElementPackage.ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__ELEMENT_KIND = ElementPackage.ELEMENT__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__DESCRIPTION = ElementPackage.ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__PRIORITY = ElementPackage.ELEMENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__COMMENTLIST = ElementPackage.ELEMENT__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUP__TIME_CREATED = ElementPackage.ELEMENT__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__LAST_MODIFIED = ElementPackage.ELEMENT__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__LAST_MODIFIED_BY = ElementPackage.ELEMENT__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUP__CREATOR = ElementPackage.ELEMENT__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__VERSION = ElementPackage.ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__VISIBLE_ID = ElementPackage.ELEMENT__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUP__UNIQUE_ID = ElementPackage.ELEMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__RELATES_TO = ElementPackage.ELEMENT__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUP__RELATED_BY = ElementPackage.ELEMENT__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUP__PARENT = ElementPackage.ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__URI = ElementPackage.ELEMENT__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__WORK_PACKAGE = ElementPackage.ELEMENT__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__CHANGE_LIST = ElementPackage.ELEMENT__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__RESPONSIBLE_USER = ElementPackage.ELEMENT__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__DEADLINE = ElementPackage.ELEMENT__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__LOCK_STATUS = ElementPackage.ELEMENT__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__LOCK_PASSWORD = ElementPackage.ELEMENT__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__ESTIMATED_COMPLEXITY = ElementPackage.ELEMENT__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__COST = ElementPackage.ELEMENT__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__BENEFIT = ElementPackage.ELEMENT__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__RISK = ElementPackage.ELEMENT__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__LIFE_CYCLE_PHASE = ElementPackage.ELEMENT__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__STATE = ElementPackage.ELEMENT__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__MANAGEMENT_DECISION = ElementPackage.ELEMENT__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__QA_ASSESSMENT = ElementPackage.ELEMENT__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__MANAGEMENT_DISCUSSION = ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__DELETION_LISTENERS = ElementPackage.ELEMENT__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__QUANTITIES = ElementPackage.ELEMENT__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__CONTENTS = ElementPackage.ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Group</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GROUP_FEATURE_COUNT = ElementPackage.ELEMENT_FEATURE_COUNT + 1;

	/**
	 * Returns the meta object for class '
	 * {@link dk.dtu.imm.red.core.element.group.Group <em>Group</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Group</em>'.
	 * @see dk.dtu.imm.red.core.element.group.Group
	 * @generated
	 */
	EClass getGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.core.element.group.Group#getContents <em>Contents</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contents</em>'.
	 * @see dk.dtu.imm.red.core.element.group.Group#getContents()
	 * @see #getGroup()
	 * @generated
	 */
	EReference getGroup_Contents();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GroupFactory getGroupFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.group.impl.GroupImpl <em>Group</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.group.impl.GroupImpl
		 * @see dk.dtu.imm.red.core.element.group.impl.GroupPackageImpl#getGroup()
		 * @generated
		 */
		EClass GROUP = eINSTANCE.getGroup();

		/**
		 * The meta object literal for the '<em><b>Contents</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference GROUP__CONTENTS = eINSTANCE.getGroup_Contents();

	}

} // GroupPackage
