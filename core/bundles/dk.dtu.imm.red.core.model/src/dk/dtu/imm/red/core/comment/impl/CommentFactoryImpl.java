/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.comment.impl;

import dk.dtu.imm.red.core.comment.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentCategory;
import dk.dtu.imm.red.core.comment.CommentExporter;
import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.comment.CommentPackage;


/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class CommentFactoryImpl extends EFactoryImpl implements CommentFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static CommentFactory init() {
		try {
			CommentFactory theCommentFactory = (CommentFactory)EPackage.Registry.INSTANCE.getEFactory(CommentPackage.eNS_URI);
			if (theCommentFactory != null) {
				return theCommentFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CommentFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public CommentFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CommentPackage.COMMENT_LIST: return createCommentList();
			case CommentPackage.COMMENT: return createComment();
			case CommentPackage.COMMENT_EXPORTER: return createCommentExporter();
			case CommentPackage.ISSUE_COMMENT: return createIssueComment();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case CommentPackage.COMMENT_CATEGORY:
				return createCommentCategoryFromString(eDataType, initialValue);
			case CommentPackage.ISSUE_SEVERITY:
				return createIssueSeverityFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case CommentPackage.COMMENT_CATEGORY:
				return convertCommentCategoryToString(eDataType, instanceValue);
			case CommentPackage.ISSUE_SEVERITY:
				return convertIssueSeverityToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CommentList createCommentList() {
		CommentListImpl commentList = new CommentListImpl();
		return commentList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Comment createComment() {
		CommentImpl comment = new CommentImpl();
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommentExporter createCommentExporter() {
		CommentExporterImpl commentExporter = new CommentExporterImpl();
		return commentExporter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IssueComment createIssueComment() {
		IssueCommentImpl issueComment = new IssueCommentImpl();
		return issueComment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CommentCategory createCommentCategoryFromString(EDataType eDataType,
			String initialValue) {
		CommentCategory result = CommentCategory.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCommentCategoryToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IssueSeverity createIssueSeverityFromString(EDataType eDataType, String initialValue) {
		IssueSeverity result = IssueSeverity.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIssueSeverityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CommentPackage getCommentPackage() {
		return (CommentPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CommentPackage getPackage() {
		return CommentPackage.eINSTANCE;
	}

} // CommentFactoryImpl
