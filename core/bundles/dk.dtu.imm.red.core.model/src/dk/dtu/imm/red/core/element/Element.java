/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element;

import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.id.VisibleElementID;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.properties.Property;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.user.User;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Element</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getIconURI <em>Icon URI</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getIcon <em>Icon</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getLabel <em>Label</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getElementKind <em>Element Kind</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getDescription <em>Description</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getPriority <em>Priority</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getCommentlist <em>Commentlist</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getTimeCreated <em>Time Created</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getLastModified <em>Last Modified</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getLastModifiedBy <em>Last Modified By</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getCreator <em>Creator</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getVersion <em>Version</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getVisibleID <em>Visible ID</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getUniqueID <em>Unique ID</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getRelatesTo <em>Relates To</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getRelatedBy <em>Related By</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getUri <em>Uri</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getWorkPackage <em>Work Package</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getChangeList <em>Change List</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getResponsibleUser <em>Responsible User</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getDeadline <em>Deadline</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getLockStatus <em>Lock Status</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getLockPassword <em>Lock Password</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getEstimatedComplexity <em>Estimated Complexity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getCost <em>Cost</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getBenefit <em>Benefit</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getRisk <em>Risk</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getLifeCyclePhase <em>Life Cycle Phase</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getState <em>State</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getManagementDecision <em>Management Decision</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getQaAssessment <em>Qa Assessment</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getManagementDiscussion <em>Management Discussion</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getDeletionListeners <em>Deletion Listeners</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.Element#getQuantities <em>Quantities</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement()
 * @model abstract="true"
 * @generated
 */
public interface Element extends Checkable {
	/**
	 * Returns the value of the '<em><b>Icon URI</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon URI</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Icon URI</em>' attribute.
	 * @see #setIconURI(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_IconURI()
	 * @model
	 * @generated
	 */
	String getIconURI();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getIconURI <em>Icon URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon URI</em>' attribute.
	 * @see #getIconURI()
	 * @generated
	 */
	void setIconURI(String value);

	/**
	 * Returns the value of the '<em><b>Icon</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Icon</em>' attribute.
	 * @see #setIcon(Image)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Icon()
	 * @model dataType="dk.dtu.imm.red.core.Image"
	 * @generated
	 */
	Image getIcon();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getIcon <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon</em>' attribute.
	 * @see #getIcon()
	 * @generated
	 */
	void setIcon(Image value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Kind</em>' attribute.
	 * @see #setElementKind(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_ElementKind()
	 * @model
	 * @generated
	 */
	String getElementKind();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getElementKind <em>Element Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Kind</em>' attribute.
	 * @see #getElementKind()
	 * @generated
	 */
	void setElementKind(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Priority</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.Priority}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.Priority
	 * @see #setPriority(Priority)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Priority()
	 * @model
	 * @generated
	 */
	Priority getPriority();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getPriority <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.Priority
	 * @see #getPriority()
	 * @generated
	 */
	void setPriority(Priority value);

	/**
	 * Returns the value of the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commentlist</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commentlist</em>' containment reference.
	 * @see #setCommentlist(CommentList)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Commentlist()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	CommentList getCommentlist();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getCommentlist <em>Commentlist</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Commentlist</em>' containment reference.
	 * @see #getCommentlist()
	 * @generated
	 */
	void setCommentlist(CommentList value);

	/**
	 * Returns the value of the '<em><b>Time Created</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Created</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Time Created</em>' attribute.
	 * @see #setTimeCreated(Date)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_TimeCreated()
	 * @model
	 * @generated
	 */
	Date getTimeCreated();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getTimeCreated <em>Time Created</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Time Created</em>' attribute.
	 * @see #getTimeCreated()
	 * @generated
	 */
	void setTimeCreated(Date value);

	/**
	 * Returns the value of the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Modified</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Modified</em>' attribute.
	 * @see #setLastModified(Date)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_LastModified()
	 * @model
	 * @generated
	 */
	Date getLastModified();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getLastModified <em>Last Modified</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Modified</em>' attribute.
	 * @see #getLastModified()
	 * @generated
	 */
	void setLastModified(Date value);

	/**
	 * Returns the value of the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Modified By</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Modified By</em>' reference.
	 * @see #setLastModifiedBy(User)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_LastModifiedBy()
	 * @model
	 * @generated
	 */
	User getLastModifiedBy();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getLastModifiedBy <em>Last Modified By</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Modified By</em>' reference.
	 * @see #getLastModifiedBy()
	 * @generated
	 */
	void setLastModifiedBy(User value);

	/**
	 * Returns the value of the '<em><b>Creator</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Creator</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Creator</em>' reference.
	 * @see #setCreator(User)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Creator()
	 * @model required="true"
	 * @generated
	 */
	User getCreator();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getCreator <em>Creator</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Creator</em>' containment reference.
	 * @see #getCreator()
	 * @generated
	 */
	void setCreator(User value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * The default value is <code>"1.0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Version()
	 * @model default="1.0.0"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visible ID</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visible ID</em>' containment reference.
	 * @see #setVisibleID(VisibleElementID)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_VisibleID()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	VisibleElementID getVisibleID();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getVisibleID <em>Visible ID</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Visible ID</em>' containment reference.
	 * @see #getVisibleID()
	 * @generated
	 */
	void setVisibleID(VisibleElementID value);

	/**
	 * Returns the value of the '<em><b>Unique ID</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unique ID</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Unique ID</em>' attribute.
	 * @see #setUniqueID(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_UniqueID()
	 * @model
	 * @generated
	 */
	String getUniqueID();

	/**
	 * Sets the value of the '
	 * {@link dk.dtu.imm.red.core.element.Element#getUniqueID
	 * <em>Unique ID</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Unique ID</em>' attribute.
	 * @see #getUniqueID()
	 * @generated
	 */
	void setUniqueID(String value);

	/**
	 * Returns the value of the '<em><b>Relates To</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.element.relationship.ElementRelationship}.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.core.element.relationship.ElementRelationship#getFromElement <em>From Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relates To</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relates To</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_RelatesTo()
	 * @see dk.dtu.imm.red.core.element.relationship.ElementRelationship#getFromElement
	 * @model opposite="fromElement" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<ElementRelationship> getRelatesTo();

	/**
	 * Returns the value of the '<em><b>Related By</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.element.relationship.ElementRelationship}.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.core.element.relationship.ElementRelationship#getToElement <em>To Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related By</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related By</em>' reference list.
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_RelatedBy()
	 * @see dk.dtu.imm.red.core.element.relationship.ElementRelationship#getToElement
	 * @model opposite="toElement"
	 * @generated
	 */
	EList<ElementRelationship> getRelatedBy();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.core.element.group.Group#getContents <em>Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(Group)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Parent()
	 * @see dk.dtu.imm.red.core.element.group.Group#getContents
	 * @model opposite="contents" unsettable="true"
	 * @generated
	 */
	Group getParent();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Group value);

	/**
	 * Returns the value of the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uri</em>' attribute.
	 * @see #setUri(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Uri()
	 * @model transient="true"
	 * @generated
	 */
	@Deprecated
	String getUri();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getUri <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uri</em>' attribute.
	 * @see #getUri()
	 * @generated
	 */
	@Deprecated
	void setUri(String value);

	/**
	 * Returns the value of the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Work Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Work Package</em>' attribute.
	 * @see #setWorkPackage(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_WorkPackage()
	 * @model
	 * @generated
	 */
	String getWorkPackage();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getWorkPackage <em>Work Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Work Package</em>' attribute.
	 * @see #getWorkPackage()
	 * @generated
	 */
	void setWorkPackage(String value);

	/**
	 * Returns the value of the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change List</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change List</em>' containment reference.
	 * @see #setChangeList(CommentList)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_ChangeList()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	CommentList getChangeList();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getChangeList <em>Change List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change List</em>' containment reference.
	 * @see #getChangeList()
	 * @generated
	 */
	void setChangeList(CommentList value);

	/**
	 * Returns the value of the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible User</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible User</em>' containment reference.
	 * @see #setResponsibleUser(User)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_ResponsibleUser()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	User getResponsibleUser();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getResponsibleUser <em>Responsible User</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responsible User</em>' containment reference.
	 * @see #getResponsibleUser()
	 * @generated
	 */
	void setResponsibleUser(User value);

	/**
	 * Returns the value of the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deadline</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deadline</em>' attribute.
	 * @see #setDeadline(Date)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Deadline()
	 * @model
	 * @generated
	 */
	Date getDeadline();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getDeadline <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deadline</em>' attribute.
	 * @see #getDeadline()
	 * @generated
	 */
	void setDeadline(Date value);

	/**
	 * Returns the value of the '<em><b>Lock Status</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.LockType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lock Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lock Status</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.LockType
	 * @see #setLockStatus(LockType)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_LockStatus()
	 * @model
	 * @generated
	 */
	LockType getLockStatus();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getLockStatus <em>Lock Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lock Status</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.LockType
	 * @see #getLockStatus()
	 * @generated
	 */
	void setLockStatus(LockType value);

	/**
	 * Returns the value of the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lock Password</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lock Password</em>' attribute.
	 * @see #setLockPassword(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_LockPassword()
	 * @model
	 * @generated
	 */
	String getLockPassword();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getLockPassword <em>Lock Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lock Password</em>' attribute.
	 * @see #getLockPassword()
	 * @generated
	 */
	void setLockPassword(String value);

	/**
	 * Returns the value of the '<em><b>Estimated Complexity</b></em>' attribute.
	 * The default value is <code>"none"</code>.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.ComplexityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Estimated Complexity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Estimated Complexity</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.ComplexityType
	 * @see #setEstimatedComplexity(ComplexityType)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_EstimatedComplexity()
	 * @model default="none"
	 * @generated
	 */
	ComplexityType getEstimatedComplexity();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getEstimatedComplexity <em>Estimated Complexity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Estimated Complexity</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.ComplexityType
	 * @see #getEstimatedComplexity()
	 * @generated
	 */
	void setEstimatedComplexity(ComplexityType value);

	/**
	 * Returns the value of the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost</em>' containment reference.
	 * @see #setCost(Property)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Cost()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Property getCost();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getCost <em>Cost</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost</em>' containment reference.
	 * @see #getCost()
	 * @generated
	 */
	void setCost(Property value);

	/**
	 * Returns the value of the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Benefit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Benefit</em>' containment reference.
	 * @see #setBenefit(Property)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Benefit()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Property getBenefit();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getBenefit <em>Benefit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Benefit</em>' containment reference.
	 * @see #getBenefit()
	 * @generated
	 */
	void setBenefit(Property value);

	/**
	 * Returns the value of the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Risk</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Risk</em>' attribute.
	 * @see #setRisk(int)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Risk()
	 * @model
	 * @generated
	 */
	int getRisk();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getRisk <em>Risk</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Risk</em>' attribute.
	 * @see #getRisk()
	 * @generated
	 */
	void setRisk(int value);

	/**
	 * Returns the value of the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.LifeCyclePhase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Life Cycle Phase</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Life Cycle Phase</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.LifeCyclePhase
	 * @see #setLifeCyclePhase(LifeCyclePhase)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_LifeCyclePhase()
	 * @model
	 * @generated
	 */
	LifeCyclePhase getLifeCyclePhase();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getLifeCyclePhase <em>Life Cycle Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Life Cycle Phase</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.LifeCyclePhase
	 * @see #getLifeCyclePhase()
	 * @generated
	 */
	void setLifeCyclePhase(LifeCyclePhase value);

	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.State
	 * @see #setState(State)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_State()
	 * @model
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getState <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.State
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

	/**
	 * Returns the value of the '<em><b>Management Decision</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.ManagementDecision}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Management Decision</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Management Decision</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.ManagementDecision
	 * @see #setManagementDecision(ManagementDecision)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_ManagementDecision()
	 * @model
	 * @generated
	 */
	ManagementDecision getManagementDecision();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getManagementDecision <em>Management Decision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Management Decision</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.ManagementDecision
	 * @see #getManagementDecision()
	 * @generated
	 */
	void setManagementDecision(ManagementDecision value);

	/**
	 * Returns the value of the '<em><b>Qa Assessment</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.QAAssessment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qa Assessment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qa Assessment</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.QAAssessment
	 * @see #setQaAssessment(QAAssessment)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_QaAssessment()
	 * @model
	 * @generated
	 */
	QAAssessment getQaAssessment();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getQaAssessment <em>Qa Assessment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qa Assessment</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.QAAssessment
	 * @see #getQaAssessment()
	 * @generated
	 */
	void setQaAssessment(QAAssessment value);

	/**
	 * Returns the value of the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Management Discussion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Management Discussion</em>' containment reference.
	 * @see #setManagementDiscussion(Text)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_ManagementDiscussion()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getManagementDiscussion();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.Element#getManagementDiscussion <em>Management Discussion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Management Discussion</em>' containment reference.
	 * @see #getManagementDiscussion()
	 * @generated
	 */
	void setManagementDiscussion(Text value);

	/**
	 * Returns the value of the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.element.DeletionListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deletion Listeners</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deletion Listeners</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_DeletionListeners()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<DeletionListener> getDeletionListeners();

	/**
	 * Returns the value of the '<em><b>Quantities</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.element.QuantityItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantities</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantities</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getElement_Quantities()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<QuantityItem> getQuantities();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	ResourceSet getResourceSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean validate(DiagnosticChain diagnostic, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void save();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void delete();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void acceptVisitor(Visitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Element findDescendantByUniqueID(String uniqueID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SearchResultWrapper search(SearchParameter param);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	double computedEffort();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getStructeredIdType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void undoDelete();

	/** 
	 * @generated NOT
	 */
	ElementColumnDefinition[] getColumnRepresentation(Class<?> type);
	/** 
	 * @generated NOT
	 */
	ElementColumnDefinition[] getColumnRepresentation();

	 //@generated NOT
	String getPlainDisplayName();
	
	 //@generated NOT
	String getComplexDisplayName();
	
	//@generated NOT
	String getElementHashId();
	
	 //@generated NOT
//	 void getPartOfName(Element parent, StringBuffer complexName, Integer level);

	
} // Element