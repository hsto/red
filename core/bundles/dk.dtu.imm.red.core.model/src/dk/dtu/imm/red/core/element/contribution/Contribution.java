/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.contribution;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import dk.dtu.imm.red.core.element.CustomAttribute;
import dk.dtu.imm.red.core.element.Element;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Contribution</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.contribution.Contribution#getCustomAttributes <em>Custom Attributes</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.contribution.ContributionPackage#getContribution()
 * @model abstract="true"
 * @generated
 */
public interface Contribution extends Element {
	/**
	 * Returns the value of the '<em><b>Custom Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.element.CustomAttribute}&lt;org.eclipse.emf.ecore.EObject>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Custom Attributes</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Custom Attributes</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.element.contribution.ContributionPackage#getContribution_CustomAttributes()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<CustomAttribute<EObject>> getCustomAttributes();

} // Contribution
