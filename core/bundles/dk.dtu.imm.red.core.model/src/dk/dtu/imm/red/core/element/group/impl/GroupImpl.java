/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.group.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.Visitor;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.group.GroupPackage;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;

/**
 * <!-- begin-user-doc --> An implementation of the model obje import
 * dk.dtu.imm.red.core.element.Element; import
 * dk.dtu.imm.red.core.element.ElementPackage; import
 * dk.dtu.imm.red.core.element.group.Group; import
 * dk.dtu.imm.red.core.element.group.GroupPackage; import
 * dk.dtu.imm.red.core.element.implementation.ElementImpl; ct '
 * <em><b>Group</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.group.impl.GroupImpl#getContents <em>Contents</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class GroupImpl extends ElementImpl implements Group {
	
	protected static final String LICENSE = "Apache 2.0 (c) Harald Stoerrle";
	
	/**
	 * The cached value of the '{@link #getContents() <em>Contents</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getContents()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> contents;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GroupPackage.Literals.GROUP;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Element> getContents() {
		if (contents == null) {
			contents = new EObjectContainmentWithInverseEList
					.Resolving<Element>(Element.class, this, GroupPackage.GROUP__CONTENTS, ElementPackage.ELEMENT__PARENT) {
				
				/**
				 * validate that added elements are valid.
				 */
				@Override
				protected Element validate(int index, Element object) {
					Element element = super.validate(index, object);
					
					if (!isValidContent(element)) {
						throw new ArrayStoreException();
					} else {
						return element;
					}
				}
			};
		}
		return contents;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isValidContent(Element content) {
		if (getParent() != null) {
			return getParent().isValidContent(content);
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> Search through this group's descendants
	 * recursively. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 * @return a descendant with the given uniqueid, or null if no such
	 *         descendant exists
	 */
	public Element findDescendantByUniqueID(final String uniqueID) {
		
		if (this.getUniqueID().equals(uniqueID)) {
			return this;
		}
		
		for (ElementRelationship rel : getRelatesTo()) {
			if (rel.getUniqueID().equals(uniqueID)) {
				return rel;
			}
		}
		
		for (ElementRelationship rel : getRelatedBy()) {
			if (rel.getUniqueID().equals(uniqueID)) {
				return rel;
			}
		}

		for (Element child : getContents()) {
			Element result = child.findDescendantByUniqueID(uniqueID);
			if (result != null) {
				return result;
			}
		}

		return null;
	}

	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isValidContent(Class type) {
		if (getParent() != null) {
			return getParent().isValidContent(type);
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GroupPackage.GROUP__CONTENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getContents()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GroupPackage.GROUP__CONTENTS:
				return ((InternalEList<?>)getContents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GroupPackage.GROUP__CONTENTS:
				return getContents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GroupPackage.GROUP__CONTENTS:
				getContents().clear();
				getContents().addAll((Collection<? extends Element>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GroupPackage.GROUP__CONTENTS:
				getContents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GroupPackage.GROUP__CONTENTS:
				return contents != null && !contents.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public void acceptVisitor(Visitor visitor) {
		for (Element child : getContents()) {
			child.acceptVisitor(visitor);
		}
		
		super.acceptVisitor(visitor);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public SearchResultWrapper search(SearchParameter param) {
		boolean searchHit = false;
		SearchResultWrapper wrapper = super.search(param);
		
		if (wrapper != null) {
			searchHit = true;
		} else {
			wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		}
		wrapper.setElement(this);
		
		for (Element child : getContents()) {
			SearchResultWrapper childWrapper = child.search(param);
			if (childWrapper != null) {
				wrapper.getChildResults().add(childWrapper);
			}
		}
		
		if (searchHit || !wrapper.getChildResults().isEmpty()) {
			return wrapper;
		} else {
			return null;
		}
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void checkChildren() {
		super.checkChildren();
		
		for (Element element : getContents()) {
			element.checkStructure();
			element.save();
			element.checkChildren();
		}
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void clearIssues() {
		super.clearIssues();
		
		for (Element element : getContents()) {
			element.clearIssues();
			element.save();
		}
	}

} // GroupImpl
