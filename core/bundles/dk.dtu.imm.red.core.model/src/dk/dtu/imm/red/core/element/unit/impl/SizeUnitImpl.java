/**
 */
package dk.dtu.imm.red.core.element.unit.impl;

import dk.dtu.imm.red.core.element.unit.SizeUnit;
import dk.dtu.imm.red.core.element.unit.UnitPackage;
import dk.dtu.imm.red.core.element.unit.Unit;
import dk.dtu.imm.red.core.element.unit.impl.SizeUnitImpl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Size Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SizeUnitImpl extends UnitImpl implements SizeUnit {
	// New copies should be returned on each call to avoid shared containment problems 
	public static SizeUnit KILOBYTE() { return new SizeUnitImpl("kB", "1e3"); }
	public static SizeUnit MEGABYTE() { return new SizeUnitImpl("MB", "1e6"); }
	public static SizeUnit GIGABYTE() { return new SizeUnitImpl("GB", "1e9"); }
	public static SizeUnit TERABYTE() { return new SizeUnitImpl("TB", "1e12"); }
	
	/**
	 * @generated NOT
	 */
	public static Unit[] units() {
		return new Unit[]{
				KILOBYTE(),
				MEGABYTE(),
				GIGABYTE(),
				TERABYTE()};
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SizeUnitImpl() {
		super();
	}

	/**
	 * @generated NOT
	 */
	private SizeUnitImpl(String symbol, String conversionFactor) {
		this.symbol = symbol;
		this.conversionFactor = new BigDecimal(conversionFactor);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.SIZE_UNIT;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public EList<Unit> getSiblingUnits() {
		List<Unit> unitList = Arrays.asList(SizeUnitImpl.units());
		return new BasicEList<Unit>(unitList);
	}

} //SizeUnitImpl
