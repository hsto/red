/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.text;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Fragment</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.text.TextFragment#getContainedText <em>Contained Text</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.text.TextPackage#getTextFragment()
 * @model abstract="true"
 * @generated
 */
public interface TextFragment extends EObject {

	/**
	 * Returns the value of the '<em><b>Contained Text</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.core.text.Text#getFragments <em>Fragments</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained Text</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Text</em>' container reference.
	 * @see #setContainedText(Text)
	 * @see dk.dtu.imm.red.core.text.TextPackage#getTextFragment_ContainedText()
	 * @see dk.dtu.imm.red.core.text.Text#getFragments
	 * @model opposite="fragments" transient="false"
	 * @generated
	 */
	Text getContainedText();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.text.TextFragment#getContainedText <em>Contained Text</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contained Text</em>' container reference.
	 * @see #getContainedText()
	 * @generated
	 */
	void setContainedText(Text value);
} // TextFragment
