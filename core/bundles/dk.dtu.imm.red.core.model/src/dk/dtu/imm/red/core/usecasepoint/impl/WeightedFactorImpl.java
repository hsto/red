/**
 */
package dk.dtu.imm.red.core.usecasepoint.impl;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactor;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactorValue;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Weighted Factor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.WeightedFactorImpl#getFactorValue <em>Factor Value</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.WeightedFactorImpl#getWeight <em>Weight</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WeightedFactorImpl extends FactorImpl implements WeightedFactor {
	/**
	 * The default value of the '{@link #getFactorValue() <em>Factor Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFactorValue()
	 * @generated
	 * @ordered
	 */
	protected static final WeightedFactorValue FACTOR_VALUE_EDEFAULT = WeightedFactorValue.SMALL;

	/**
	 * The cached value of the '{@link #getFactorValue() <em>Factor Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFactorValue()
	 * @generated
	 * @ordered
	 */
	protected WeightedFactorValue factorValue = FACTOR_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected static final double WEIGHT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected double weight = WEIGHT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WeightedFactorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasepointPackage.Literals.WEIGHTED_FACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WeightedFactorValue getFactorValue() {
		return factorValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFactorValue(WeightedFactorValue newFactorValue) {
		WeightedFactorValue oldFactorValue = factorValue;
		factorValue = newFactorValue == null ? FACTOR_VALUE_EDEFAULT : newFactorValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.WEIGHTED_FACTOR__FACTOR_VALUE, oldFactorValue, factorValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWeight(double newWeight) {
		double oldWeight = weight;
		weight = newWeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasepointPackage.WEIGHTED_FACTOR__WEIGHT, oldWeight, weight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasepointPackage.WEIGHTED_FACTOR__FACTOR_VALUE:
				return getFactorValue();
			case UsecasepointPackage.WEIGHTED_FACTOR__WEIGHT:
				return getWeight();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasepointPackage.WEIGHTED_FACTOR__FACTOR_VALUE:
				setFactorValue((WeightedFactorValue)newValue);
				return;
			case UsecasepointPackage.WEIGHTED_FACTOR__WEIGHT:
				setWeight((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasepointPackage.WEIGHTED_FACTOR__FACTOR_VALUE:
				setFactorValue(FACTOR_VALUE_EDEFAULT);
				return;
			case UsecasepointPackage.WEIGHTED_FACTOR__WEIGHT:
				setWeight(WEIGHT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasepointPackage.WEIGHTED_FACTOR__FACTOR_VALUE:
				return factorValue != FACTOR_VALUE_EDEFAULT;
			case UsecasepointPackage.WEIGHTED_FACTOR__WEIGHT:
				return weight != WEIGHT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (factorValue: ");
		result.append(factorValue);
		result.append(", weight: ");
		result.append(weight);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */ 
	@Override 
	public double getSum() { 
		double sum = 1 + 0.1 * getWeight() * (3 - getFactorValue().getValue()); 
		return sum; 
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public ElementColumnDefinition[] getColumnRepresentation()  { 
		return new ElementColumnDefinition[] {  
				new ElementColumnDefinition()
				.setHeaderName("Name") 
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(false)
				.setColumnWidth(200)
				.setCellHandler(new CellHandler<WeightedFactor>() {

					@Override
					public Object getAttributeValue(WeightedFactor item) {
						return item.getName();
					}

					@Override
					public void setAttributeValue(WeightedFactor item, Object value) {
						return;
					} 
				}),
				new ElementColumnDefinition()
				.setHeaderName("Scoring") 
				.setColumnBehaviour(ColumnBehaviour.ComboSelector)
				.setCellItems(ElementColumnDefinition.enumToStringArray(WeightedFactorValue.values()))
				.setEditable(true)
				.setColumnWidth(50)
				.setCellHandler(new CellHandler<WeightedFactor>() {

					@Override
					public Object getAttributeValue(WeightedFactor item) {
						return item.getFactorValue().name().toString();
					}

					@Override
					public void setAttributeValue(WeightedFactor item, Object value) {
						item.setFactorValue(WeightedFactorValue.valueOf(value.toString().toUpperCase()));
					} 
				}),
				new ElementColumnDefinition()
				.setHeaderName("Weight") 
				.setColumnWidth(40)
				.setColumnBehaviour(ColumnBehaviour.CellEdit)
				.setEditable(true)
				.setCellHandler(new CellHandler<WeightedFactor>() {

					@Override
					public Object getAttributeValue(WeightedFactor item) {
						return item.getWeight();
					} 

					@Override
					public void setAttributeValue(WeightedFactor item, Object value) { 
						try {
							item.setWeight(Double.parseDouble(value.toString()));
						}
						catch(NumberFormatException n) { 
						} 
					} 
				})
		};
	}

} //WeightedFactorImpl
