/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.relationship.impl;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.relationship.ElementReference;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.element.relationship.RelationshipPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Element Reference</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>
 * {@link dk.dtu.imm.red.core.element.relationship.impl.ElementReferenceImpl#getRelationshipKind
 * <em>Relationship Kind</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElementReferenceImpl extends ElementRelationshipImpl implements ElementReference {

	/**
	 * The default value of the '{@link #getRelationshipKind()
	 * <em>Relationship Kind</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRelationshipKind()
	 * @generated
	 * @ordered
	 */
	protected static final RelationshipKind RELATIONSHIP_KIND_EDEFAULT = RelationshipKind.IS_ASSOCIATED_TO;
	/**
	 * The cached value of the '{@link #getRelationshipKind()
	 * <em>Relationship Kind</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRelationshipKind()
	 * @generated
	 * @ordered
	 */
	protected RelationshipKind relationshipKind = RELATIONSHIP_KIND_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ElementReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RelationshipPackage.Literals.ELEMENT_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public RelationshipKind getRelationshipKind() {
		return relationshipKind;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRelationshipKind(RelationshipKind newRelationshipKind) {
		RelationshipKind oldRelationshipKind = relationshipKind;
		relationshipKind = newRelationshipKind == null ? RELATIONSHIP_KIND_EDEFAULT : newRelationshipKind;
		timeCreated = new Date();
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					RelationshipPackage.ELEMENT_REFERENCE__RELATIONSHIP_KIND, oldRelationshipKind, relationshipKind));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case RelationshipPackage.ELEMENT_REFERENCE__RELATIONSHIP_KIND:
			return getRelationshipKind();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case RelationshipPackage.ELEMENT_REFERENCE__RELATIONSHIP_KIND:
			setRelationshipKind((RelationshipKind) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case RelationshipPackage.ELEMENT_REFERENCE__RELATIONSHIP_KIND:
			setRelationshipKind(RELATIONSHIP_KIND_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case RelationshipPackage.ELEMENT_REFERENCE__RELATIONSHIP_KIND:
			return relationshipKind != RELATIONSHIP_KIND_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (relationshipKind: ");
		result.append(relationshipKind);
		result.append(')');
		return result.toString();
	}

	/**
	 * Perform a simple type check for object
	 */
	public void checkAsymmetricRelationshipKind() {
		if (toElement != null && getFromElement() != null) {
			if (!(relationshipKind.equals(RelationshipKind.CONFLICTS_WITH)
					&& relationshipKind.equals(RelationshipKind.IS_ASSOCIATED_TO)
					&& relationshipKind.equals(RelationshipKind.IS_RELATED_TO)
					&& relationshipKind.equals(RelationshipKind.IS_STITCHED_TO))
					&& (relationshipKind.getValue() % 2 == 0)) {
				Element buffer = getFromElement();
				setFromElement(toElement);
				toElement = buffer;
				relationshipKind = RelationshipKind.getReverseRelationshipKind(relationshipKind);
			}
		} else {
			// TODO: set error message
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void checkStructure() {
		checkAsymmetricRelationshipKind();
		
		if (relationshipKind.equals(RelationshipKind.ILLUSTRATES)) {
			if (!getFromElement().eClass().getName().equals("Diagram")) {
				addIssueComment(IssueSeverity.MISTAKE,
						RelationshipKind.ILLUSTRATES.getName() + " can only be link type for Diagram elements");
			}
		}
	}

} // ElementReferenceImpl
