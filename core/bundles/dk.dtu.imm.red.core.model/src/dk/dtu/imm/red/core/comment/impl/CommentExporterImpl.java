/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.comment.impl;

import java.text.SimpleDateFormat;

import org.eclipse.emf.ecore.EClass;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentExporter;
import dk.dtu.imm.red.core.comment.CommentPackage;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.VisitorImpl;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exporter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CommentExporterImpl extends VisitorImpl implements CommentExporter {
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommentExporterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CommentPackage.Literals.COMMENT_EXPORTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * The format for comment export is
	 * elementID,time,elementName,path,commentAuthor,commentText,commentType
	 */
	public void visit(Element object) {
		
		
		if (object.getCommentlist() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
			String id = object.getUniqueID();
			
			String path = object.getName();
			Element temp = object;
			while (temp.getParent() != null && 
					temp.getParent() instanceof File == false) {
				temp = temp.getParent();
				path = temp.getName() + "/" + path;
			}
			
			Project project =null;
			Element rootElement = null;
			while (temp.getParent() != null && 
					temp.getParent() instanceof Project == false) {
				rootElement=temp;
				temp = temp.getParent();
			}
			
			//keep track of the project users to userID (which hopefully should contain the student id
//			EList<UserTableEntry> users = project.getUsers();
//			HashMap<String,String> userNameToIDMap=new HashMap<String,String>();
//			if(users!=null){
//				for(UserTableEntry user:users){
//					userNameToIDMap.put(user.getUser().getName(), user.getUser().getId());
//				}
//			}
			
			
			for (Comment comment : object.getCommentlist().getComments()) {
				
				if(project!=null){
					buf.append(project.getName());
				}
				else{
					buf.append(rootElement.getName()+",");
				}	//object
				
				buf.append("document"+",");//location_type
				String interfName = object.getClass().getSimpleName();
				for(Class interf: object.getClass().getInterfaces()){
					if(object.getClass().getSimpleName().contains(interf.getSimpleName())){
						interfName=interf.getSimpleName();
					}
				}
				buf.append(interfName+",");//element_type
				
				
				if (object.getName() != null) {
					buf.append("\"" + object.getName() + "\",");
				} else {
					buf.append("\"\",");
				}			//element_name	

				buf.append(1+",");//element_number
				buf.append(""+",");//line_number
				
				if (path != null) {
					buf.append("\"" + path + "\",");
				} else {
					buf.append("\"\",");
				}	//path			
				
				buf.append(""+",");//diagram
				
				if (comment.getText() != null) {
					buf.append("\"" + comment.getText().toString() + "\",");
				} else {
					buf.append("\"\",");
				}//content				
				
				buf.append("comment"+",");//remark_level
				
				if (comment.getAuthor() != null && comment.getAuthor().getName() != null) {
					buf.append("\"" + comment.getAuthor().getName() + "\",");				
				} else {
					buf.append("\"\",");
				}//s_number
				
				buf.append("\n");
				
//				buf.append(id + ",");
//				buf.append(sdf.format(comment.getTimeCreated()) + ",");
//				if (object.getName() != null) {
//					buf.append("\"" + object.getName() + "\",");
//				} else {
//					buf.append("\"\",");
//				}
//				
//				if (path != null) {
//					buf.append("\"" + path + "\",");
//				} else {
//					buf.append("\"\",");
//				}
//				
//				if (comment.getAuthor() != null && comment.getAuthor().getName() != null) {
//					buf.append("\"" + comment.getAuthor().getName() + "\",");
//				} else {
//					buf.append("\"\",");
//				}
//				
//				if (comment.getText() != null) {
//					buf.append("\"" + comment.getText().toString() + "\",");
//				} else {
//					buf.append("\"\",");
//				}
//				
//				if (comment.getCategory() != null) {
//					buf.append(comment.getCategory().getLiteral() + "\n");
//				}
				
			}
		}
		
	}

} //CommentExporterImpl
