/**
 */
package dk.dtu.imm.red.core.element.unit.impl;

import dk.dtu.imm.red.core.element.unit.TimeUnit;
import dk.dtu.imm.red.core.element.unit.UnitPackage;
import dk.dtu.imm.red.core.element.unit.Unit;
import dk.dtu.imm.red.core.element.unit.impl.TimeUnitImpl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TimeUnitImpl extends UnitImpl implements TimeUnit {
	// New copies should be returned on each call to avoid shared containment problems 
	public static TimeUnit MILLISECOND() { return new TimeUnitImpl("ms", "0.001"); }
	public static TimeUnit SECOND() { return new TimeUnitImpl("s", "1"); }
	public static TimeUnit DAY() { return new TimeUnitImpl("d", "86400"); }
	public static TimeUnit WEEK() { return new TimeUnitImpl("w", "604800"); }
	
	/**
	 * @generated NOT
	 */
	public static Unit[] units() {
		return new Unit[]{
				MILLISECOND(),
				SECOND(),
				DAY(),
				WEEK()};
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeUnitImpl() {
		super();
	}

	/**
	 * @generated NOT
	 */
	private TimeUnitImpl(String symbol, String conversionFactor) {
		this.symbol = symbol;
		this.conversionFactor = new BigDecimal(conversionFactor);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.TIME_UNIT;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public EList<Unit> getSiblingUnits() {
		List<Unit> unitList = Arrays.asList(TimeUnitImpl.units());
		return new BasicEList<Unit>(unitList);
	}

} //TimeUnitImpl
