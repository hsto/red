/**
 */
package dk.dtu.imm.red.core.element.impl;

import dk.dtu.imm.red.core.element.CaptionedImage;
import dk.dtu.imm.red.core.element.CaptionedImageList;
import dk.dtu.imm.red.core.element.ElementPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Captioned Image List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.impl.CaptionedImageListImpl#getCaptionedImage <em>Captioned Image</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CaptionedImageListImpl extends EObjectImpl implements CaptionedImageList {
	/**
	 * The cached value of the '{@link #getCaptionedImage() <em>Captioned Image</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCaptionedImage()
	 * @generated
	 * @ordered
	 */
	protected EList<CaptionedImage> captionedImage;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaptionedImageListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ElementPackage.Literals.CAPTIONED_IMAGE_LIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CaptionedImage> getCaptionedImage() {
		if (captionedImage == null) {
			captionedImage = new EObjectContainmentEList.Resolving<CaptionedImage>(CaptionedImage.class, this, ElementPackage.CAPTIONED_IMAGE_LIST__CAPTIONED_IMAGE);
		}
		return captionedImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ElementPackage.CAPTIONED_IMAGE_LIST__CAPTIONED_IMAGE:
				return ((InternalEList<?>)getCaptionedImage()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ElementPackage.CAPTIONED_IMAGE_LIST__CAPTIONED_IMAGE:
				return getCaptionedImage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ElementPackage.CAPTIONED_IMAGE_LIST__CAPTIONED_IMAGE:
				getCaptionedImage().clear();
				getCaptionedImage().addAll((Collection<? extends CaptionedImage>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ElementPackage.CAPTIONED_IMAGE_LIST__CAPTIONED_IMAGE:
				getCaptionedImage().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ElementPackage.CAPTIONED_IMAGE_LIST__CAPTIONED_IMAGE:
				return captionedImage != null && !captionedImage.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CaptionedImageListImpl
