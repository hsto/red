/**
 */
package dk.dtu.imm.red.core.usecasepoint;

import dk.dtu.imm.red.core.element.Element;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.Factor#getSum <em>Sum</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getFactor()
 * @model abstract="true"
 * @generated
 */
public interface Factor extends Element {
	/**
	 * Returns the value of the '<em><b>Sum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sum</em>' attribute.
	 * @see #setSum(double)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getFactor_Sum()
	 * @model
	 * @generated
	 */
	double getSum();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.Factor#getSum <em>Sum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sum</em>' attribute.
	 * @see #getSum()
	 * @generated
	 */
	void setSum(double value);

} // Factor
