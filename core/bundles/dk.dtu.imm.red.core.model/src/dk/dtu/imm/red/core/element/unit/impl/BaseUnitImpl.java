/**
 */
package dk.dtu.imm.red.core.element.unit.impl;

import dk.dtu.imm.red.core.element.unit.BaseUnit;
import dk.dtu.imm.red.core.element.unit.UnitPackage;
import dk.dtu.imm.red.core.element.unit.Unit;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BaseUnitImpl extends UnitImpl implements BaseUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.BASE_UNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Unit> getSiblingUnits() {
		BasicEList<Unit> list = new BasicEList<Unit>();
		list.add(this);
		return list;
	}

} //BaseUnitImpl
