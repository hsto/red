/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.project;

import org.eclipse.emf.ecore.EObject;

import dk.dtu.imm.red.core.user.User;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Table Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.project.UserTableEntry#getUser <em>User</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.UserTableEntry#getComment <em>Comment</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.project.ProjectPackage#getUserTableEntry()
 * @model
 * @generated
 */
public interface UserTableEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' containment reference.
	 * @see #setUser(User)
	 * @see dk.dtu.imm.red.core.project.ProjectPackage#getUserTableEntry_User()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	User getUser();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.project.UserTableEntry#getUser <em>User</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' containment reference.
	 * @see #getUser()
	 * @generated
	 */
	void setUser(User value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' attribute.
	 * @see #setComment(String)
	 * @see dk.dtu.imm.red.core.project.ProjectPackage#getUserTableEntry_Comment()
	 * @model
	 * @generated
	 */
	String getComment();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.project.UserTableEntry#getComment <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' attribute.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(String value);

} // UserTableEntry
