/**
 */
package dk.dtu.imm.red.core.element.unit;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.unit.Unit#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.unit.Unit#getConversionFactor <em>Conversion Factor</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.unit.UnitPackage#getUnit()
 * @model abstract="true"
 * @generated
 */
public interface Unit extends EObject {
	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' attribute.
	 * The default value is <code>"units"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' attribute.
	 * @see #setSymbol(String)
	 * @see dk.dtu.imm.red.core.element.unit.UnitPackage#getUnit_Symbol()
	 * @model default="units" required="true"
	 * @generated
	 */
	String getSymbol();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.unit.Unit#getSymbol <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' attribute.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(String value);

	/**
	 * Returns the value of the '<em><b>Conversion Factor</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conversion Factor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conversion Factor</em>' attribute.
	 * @see #setConversionFactor(BigDecimal)
	 * @see dk.dtu.imm.red.core.element.unit.UnitPackage#getUnit_ConversionFactor()
	 * @model default="1" required="true"
	 * @generated
	 */
	BigDecimal getConversionFactor();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.unit.Unit#getConversionFactor <em>Conversion Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conversion Factor</em>' attribute.
	 * @see #getConversionFactor()
	 * @generated
	 */
	void setConversionFactor(BigDecimal value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Unit> getSiblingUnits();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isCompatibleWith(Unit that);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	BigDecimal convertTo(BigDecimal value, Unit outputUnit);

} // Unit
