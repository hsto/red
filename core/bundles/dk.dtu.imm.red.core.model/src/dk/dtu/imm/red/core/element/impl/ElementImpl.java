/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.impl;

import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.comment.IssueComment;
import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.element.ComplexityType;
import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.LifeCyclePhase;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.element.ManagementDecision;
import dk.dtu.imm.red.core.element.Priority;
import dk.dtu.imm.red.core.element.QAAssessment;
import dk.dtu.imm.red.core.element.QuantityItem;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.State;
import dk.dtu.imm.red.core.element.Visitor;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.group.GroupPackage;
import dk.dtu.imm.red.core.element.id.VisibleElementID;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.element.relationship.RelationshipPackage;
import dk.dtu.imm.red.core.element.util.ElementValidator;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.properties.Kind;
import dk.dtu.imm.red.core.properties.PropertiesFactory;
import dk.dtu.imm.red.core.properties.Property;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.user.User;
import dk.dtu.imm.red.core.user.UserFactory;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Element</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getIconURI
 * <em>Icon URI</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getIcon <em>Icon</em>
 * }</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getLabel
 * <em>Label</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getName <em>Name</em>
 * }</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getElementKind
 * <em>Element Kind</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getDescription
 * <em>Description</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getPriority
 * <em>Priority</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getCommentlist
 * <em>Commentlist</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getTimeCreated
 * <em>Time Created</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getLastModified
 * <em>Last Modified</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getLastModifiedBy
 * <em>Last Modified By</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getCreator
 * <em>Creator</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getVersion
 * <em>Version</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getVisibleID
 * <em>Visible ID</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getUniqueID
 * <em>Unique ID</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getRelatesTo
 * <em>Relates To</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getRelatedBy
 * <em>Related By</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getParent
 * <em>Parent</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getUri <em>Uri</em>}
 * </li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getWorkPackage
 * <em>Work Package</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getChangeList
 * <em>Change List</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getResponsibleUser
 * <em>Responsible User</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getDeadline
 * <em>Deadline</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getLockStatus
 * <em>Lock Status</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getLockPassword
 * <em>Lock Password</em>}</li>
 * <li>
 * {@link dk.dtu.imm.red.core.element.impl.ElementImpl#getEstimatedComplexity
 * <em>Estimated Complexity</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getCost <em>Cost</em>
 * }</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getBenefit
 * <em>Benefit</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getRisk <em>Risk</em>
 * }</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getLifeCyclePhase
 * <em>Life Cycle Phase</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getState
 * <em>State</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getManagementDecision
 * <em>Management Decision</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getQaAssessment
 * <em>Qa Assessment</em>}</li>
 * <li>
 * {@link dk.dtu.imm.red.core.element.impl.ElementImpl#getManagementDiscussion
 * <em>Management Discussion</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getDeletionListeners
 * <em>Deletion Listeners</em>}</li>
 * <li>{@link dk.dtu.imm.red.core.element.impl.ElementImpl#getQuantities
 * <em>Quantities</em>}</li> 
 *
 * @generated
 */
public abstract class ElementImpl extends EObjectImpl implements Element {

	/**
	 * The default value of the '{@link #getIconURI() <em>Icon URI</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getIconURI()
	 * @generated
	 * @ordered
	 */
	protected static final String ICON_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIconURI() <em>Icon URI</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getIconURI()
	 * @generated
	 * @ordered
	 */
	protected String iconURI = ICON_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getIcon() <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getIcon()
	 * @generated
	 * @ordered
	 */
	protected static final Image ICON_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIcon() <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getIcon()
	 * @generated
	 * @ordered
	 */
	protected Image icon = ICON_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getElementKind() <em>Element Kind</em>}
	 * ' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getElementKind()
	 * @generated
	 * @ordered
	 */
	protected static final String ELEMENT_KIND_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getElementKind() <em>Element Kind</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getElementKind()
	 * @generated
	 * @ordered
	 */
	protected String elementKind = ELEMENT_KIND_EDEFAULT;
	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;
	/**
	 * The default value of the '{@link #getPriority() <em>Priority</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected static final Priority PRIORITY_EDEFAULT = Priority.HIGH;

	/**
	 * The cached value of the '{@link #getPriority() <em>Priority</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected Priority priority = PRIORITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCommentlist() <em>Commentlist</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCommentlist()
	 * @generated
	 * @ordered
	 */
	protected CommentList commentlist;

	/**
	 * The default value of the '{@link #getTimeCreated() <em>Time Created</em>}
	 * ' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTimeCreated()
	 * @generated
	 * @ordered
	 */
	protected static final Date TIME_CREATED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimeCreated() <em>Time Created</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTimeCreated()
	 * @generated
	 * @ordered
	 */
	protected Date timeCreated = TIME_CREATED_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastModified()
	 * <em>Last Modified</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLastModified()
	 * @generated
	 * @ordered
	 */
	protected static final Date LAST_MODIFIED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastModified() <em>Last Modified</em>
	 * }' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLastModified()
	 * @generated
	 * @ordered
	 */
	protected Date lastModified = LAST_MODIFIED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLastModifiedBy()
	 * <em>Last Modified By</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLastModifiedBy()
	 * @generated
	 * @ordered
	 */
	protected User lastModifiedBy;

	/**
	 * The cached value of the '{@link #getCreator() <em>Creator</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCreator()
	 * @generated
	 * @ordered
	 */
	protected User creator;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = "1.0.0";

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVisibleID() <em>Visible ID</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getVisibleID()
	 * @generated
	 * @ordered
	 */
	protected VisibleElementID visibleID;

	/**
	 * The default value of the '{@link #getUniqueID() <em>Unique ID</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getUniqueID()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIQUE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUniqueID() <em>Unique ID</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getUniqueID()
	 * @generated
	 * @ordered
	 */
	protected String uniqueID = UNIQUE_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRelatesTo() <em>Relates To</em>}'
	 * containment reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRelatesTo()
	 * @generated
	 * @ordered
	 */
	protected EList<ElementRelationship> relatesTo;

	/**
	 * The cached value of the '{@link #getRelatedBy() <em>Related By</em>}'
	 * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRelatedBy()
	 * @generated
	 * @ordered
	 */
	protected EList<ElementRelationship> relatedBy;

	/**
	 * The default value of the '{@link #getUri() <em>Uri</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getUri()
	 * @generated
	 * @ordered
	 */
	protected static final String URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUri() <em>Uri</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @see #getUri()
	 * @generated
	 * @ordered
	 */
	@Deprecated
	protected String uri = URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorkPackage() <em>Work Package</em>}
	 * ' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getWorkPackage()
	 * @generated
	 * @ordered
	 */
	protected static final String WORK_PACKAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWorkPackage() <em>Work Package</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getWorkPackage()
	 * @generated
	 * @ordered
	 */
	protected String workPackage = WORK_PACKAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getChangeList() <em>Change List</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getChangeList()
	 * @generated
	 * @ordered
	 */
	protected CommentList changeList;

	/**
	 * The cached value of the '{@link #getResponsibleUser()
	 * <em>Responsible User</em>}' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getResponsibleUser()
	 * @generated
	 * @ordered
	 */
	protected User responsibleUser;

	/**
	 * The default value of the '{@link #getDeadline() <em>Deadline</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected static final Date DEADLINE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDeadline() <em>Deadline</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected Date deadline = DEADLINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLockStatus() <em>Lock Status</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLockStatus()
	 * @generated
	 * @ordered
	 */
	protected static final LockType LOCK_STATUS_EDEFAULT = LockType.LOCK_LIKE_CONTAINER;

	/**
	 * The cached value of the '{@link #getLockStatus() <em>Lock Status</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLockStatus()
	 * @generated
	 * @ordered
	 */
	protected LockType lockStatus = LOCK_STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getLockPassword()
	 * <em>Lock Password</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLockPassword()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCK_PASSWORD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLockPassword() <em>Lock Password</em>
	 * }' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLockPassword()
	 * @generated
	 * @ordered
	 */
	protected String lockPassword = LOCK_PASSWORD_EDEFAULT;

	/**
	 * The default value of the '{@link #getEstimatedComplexity()
	 * <em>Estimated Complexity</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getEstimatedComplexity()
	 * @generated
	 * @ordered
	 */
	protected static final ComplexityType ESTIMATED_COMPLEXITY_EDEFAULT = ComplexityType.NONE;

	/**
	 * The cached value of the '{@link #getEstimatedComplexity()
	 * <em>Estimated Complexity</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getEstimatedComplexity()
	 * @generated
	 * @ordered
	 */
	protected ComplexityType estimatedComplexity = ESTIMATED_COMPLEXITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCost() <em>Cost</em>}' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCost()
	 * @generated
	 * @ordered
	 */
	protected Property cost;
	/**
	 * The cached value of the '{@link #getBenefit() <em>Benefit</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getBenefit()
	 * @generated
	 * @ordered
	 */
	protected Property benefit;

	/**
	 * The default value of the '{@link #getRisk() <em>Risk</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRisk()
	 * @generated
	 * @ordered
	 */
	protected static final int RISK_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getRisk() <em>Risk</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRisk()
	 * @generated
	 * @ordered
	 */
	protected int risk = RISK_EDEFAULT;
	/**
	 * The default value of the '{@link #getLifeCyclePhase()
	 * <em>Life Cycle Phase</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLifeCyclePhase()
	 * @generated
	 * @ordered
	 */
	protected static final LifeCyclePhase LIFE_CYCLE_PHASE_EDEFAULT = LifeCyclePhase.ELABORATION;
	/**
	 * The cached value of the '{@link #getLifeCyclePhase()
	 * <em>Life Cycle Phase</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLifeCyclePhase()
	 * @generated
	 * @ordered
	 */
	protected LifeCyclePhase lifeCyclePhase = LIFE_CYCLE_PHASE_EDEFAULT;
	/**
	 * The default value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected static final State STATE_EDEFAULT = State.INCOMPLETE;
	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected State state = STATE_EDEFAULT;
	/**
	 * The default value of the '{@link #getManagementDecision()
	 * <em>Management Decision</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getManagementDecision()
	 * @generated
	 * @ordered
	 */
	protected static final ManagementDecision MANAGEMENT_DECISION_EDEFAULT = ManagementDecision.GENUINE;
	/**
	 * The cached value of the '{@link #getManagementDecision()
	 * <em>Management Decision</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getManagementDecision()
	 * @generated
	 * @ordered
	 */
	protected ManagementDecision managementDecision = MANAGEMENT_DECISION_EDEFAULT;
	/**
	 * The default value of the '{@link #getQaAssessment()
	 * <em>Qa Assessment</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getQaAssessment()
	 * @generated
	 * @ordered
	 */
	protected static final QAAssessment QA_ASSESSMENT_EDEFAULT = QAAssessment.APPROVED;
	/**
	 * The cached value of the '{@link #getQaAssessment() <em>Qa Assessment</em>
	 * }' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getQaAssessment()
	 * @generated
	 * @ordered
	 */
	protected QAAssessment qaAssessment = QA_ASSESSMENT_EDEFAULT;
	/**
	 * The cached value of the '{@link #getManagementDiscussion()
	 * <em>Management Discussion</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getManagementDiscussion()
	 * @generated
	 * @ordered
	 */
	protected Text managementDiscussion;
	/**
	 * The hash ID of the element. It is created of type[label]name.
	 */
	private String elementHashId = null;

	/**
	 * The cached value of the '{@link #getDeletionListeners()
	 * <em>Deletion Listeners</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDeletionListeners()
	 * @generated
	 * @ordered
	 */
	protected EList<DeletionListener> deletionListeners;

	/**
	 * The cached value of the '{@link #getQuantities() <em>Quantities</em>}'
	 * containment reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getQuantities()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityItem> quantities;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	protected ElementImpl() {
		super();

		this.setTimeCreated(new Date());
		this.setUniqueID(UUID.randomUUID().toString());
		if (getCost() == null)
			setCost(PropertiesFactory.eINSTANCE.createProperty());
		if (getBenefit() == null)
			setBenefit(PropertiesFactory.eINSTANCE.createProperty());

		getCost().setName("Cost");
		getCost().setKind(Kind.VALUE);

		getBenefit().setName("Cost");
		getBenefit().setKind(Kind.VALUE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ElementPackage.Literals.ELEMENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getIconURI() {
		return iconURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setIconURI(String newIconURI) {
		String oldIconURI = iconURI;
		iconURI = newIconURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__ICON_URI, oldIconURI,
					iconURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public Image getIcon() {
		return getIcon(FrameworkUtil.getBundle(this.getClass()));
	}

	/**
	 * Gets the icon for this element, given the ID of the bundle which contains
	 * the icon.
	 *
	 * @param pluginID
	 *            the bundle containing the icon
	 * @return the icon of the element
	 */
	protected Image getIcon(final Bundle bundle) {
		if (this.icon == null && getIconURI() != null) {
			URL url = FileLocator.find(bundle, new Path(getIconURI()), null);

			ImageDescriptor defaultImageDescriptor = ImageDescriptor.createFromURL(url);
			icon = defaultImageDescriptor.createImage();
		}

		return icon;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setIcon(Image newIcon) {
		Image oldIcon = icon;
		icon = newIcon;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__ICON, oldIcon, icon));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getElementKind() {
		return elementKind;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setElementKind(String newElementKind) {
		String oldElementKind = elementKind;
		elementKind = newElementKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__ELEMENT_KIND, oldElementKind,
					elementKind));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__DESCRIPTION, oldDescription,
					description));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Priority getPriority() {
		return priority;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPriority(Priority newPriority) {
		Priority oldPriority = priority;
		priority = newPriority == null ? PRIORITY_EDEFAULT : newPriority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__PRIORITY, oldPriority,
					priority));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public CommentList getCommentlist() {
		if (commentlist == null) {
			setCommentlist(CommentFactory.eINSTANCE.createCommentList());
		}
		return commentlist;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CommentList basicGetCommentlist() {
		return commentlist;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetCommentlist(CommentList newCommentlist, NotificationChain msgs) {
		CommentList oldCommentlist = commentlist;
		commentlist = newCommentlist;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ElementPackage.ELEMENT__COMMENTLIST, oldCommentlist, newCommentlist);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setCommentlist(CommentList newCommentlist) {
		if (newCommentlist != commentlist) {
			NotificationChain msgs = null;
			if (commentlist != null)
				msgs = ((InternalEObject) commentlist).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__COMMENTLIST, null, msgs);
			if (newCommentlist != null)
				msgs = ((InternalEObject) newCommentlist).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__COMMENTLIST, null, msgs);
			msgs = basicSetCommentlist(newCommentlist, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__COMMENTLIST, newCommentlist,
					newCommentlist));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setTimeCreated(Date newTimeCreated) {
		Date oldTimeCreated = timeCreated;
		timeCreated = newTimeCreated;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__TIME_CREATED, oldTimeCreated,
					timeCreated));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setLastModified(Date newLastModified) {
		Date oldLastModified = lastModified;
		lastModified = newLastModified;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__LAST_MODIFIED,
					oldLastModified, lastModified));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public User getLastModifiedBy() {
		if (lastModifiedBy != null && lastModifiedBy.eIsProxy()) {
			InternalEObject oldLastModifiedBy = (InternalEObject) lastModifiedBy;
			lastModifiedBy = (User) eResolveProxy(oldLastModifiedBy);
			if (lastModifiedBy != oldLastModifiedBy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.ELEMENT__LAST_MODIFIED_BY,
							oldLastModifiedBy, lastModifiedBy));
			}
		}
		return lastModifiedBy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public User basicGetLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setLastModifiedBy(User newLastModifiedBy) {
		User oldLastModifiedBy = lastModifiedBy;
		lastModifiedBy = newLastModifiedBy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__LAST_MODIFIED_BY,
					oldLastModifiedBy, lastModifiedBy));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public User getCreator() {
		if (creator != null && creator.eIsProxy()) {
			InternalEObject oldCreator = (InternalEObject) creator;
			creator = (User) eResolveProxy(oldCreator);
			if (creator != oldCreator) {
				InternalEObject newCreator = (InternalEObject) creator;
				NotificationChain msgs = oldCreator.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__CREATOR, null, null);
				if (newCreator.eInternalContainer() == null) {
					msgs = newCreator.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__CREATOR, null,
							msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.ELEMENT__CREATOR,
							oldCreator, creator));
			}
		}
		return creator;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public User basicGetCreator() {
		return creator;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetCreator(User newCreator, NotificationChain msgs) {
		User oldCreator = creator;
		creator = newCreator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ElementPackage.ELEMENT__CREATOR, oldCreator, newCreator);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setCreator(User newCreator) {
		if (newCreator != creator) {
			NotificationChain msgs = null;
			if (creator != null)
				msgs = ((InternalEObject) creator).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__CREATOR, null, msgs);
			if (newCreator != null)
				msgs = ((InternalEObject) newCreator).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__CREATOR, null, msgs);
			msgs = basicSetCreator(newCreator, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__CREATOR, newCreator,
					newCreator));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__VERSION, oldVersion,
					version));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public VisibleElementID getVisibleID() {
		if (visibleID != null && visibleID.eIsProxy()) {
			InternalEObject oldVisibleID = (InternalEObject) visibleID;
			visibleID = (VisibleElementID) eResolveProxy(oldVisibleID);
			if (visibleID != oldVisibleID) {
				InternalEObject newVisibleID = (InternalEObject) visibleID;
				NotificationChain msgs = oldVisibleID.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__VISIBLE_ID, null, null);
				if (newVisibleID.eInternalContainer() == null) {
					msgs = newVisibleID.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__VISIBLE_ID,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.ELEMENT__VISIBLE_ID,
							oldVisibleID, visibleID));
			}
		}
		return visibleID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public VisibleElementID basicGetVisibleID() {
		return visibleID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetVisibleID(VisibleElementID newVisibleID, NotificationChain msgs) {
		VisibleElementID oldVisibleID = visibleID;
		visibleID = newVisibleID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ElementPackage.ELEMENT__VISIBLE_ID, oldVisibleID, newVisibleID);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setVisibleID(VisibleElementID newVisibleID) {
		if (newVisibleID != visibleID) {
			NotificationChain msgs = null;
			if (visibleID != null)
				msgs = ((InternalEObject) visibleID).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__VISIBLE_ID, null, msgs);
			if (newVisibleID != null)
				msgs = ((InternalEObject) newVisibleID).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__VISIBLE_ID, null, msgs);
			msgs = basicSetVisibleID(newVisibleID, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__VISIBLE_ID, newVisibleID,
					newVisibleID));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getUniqueID() {
		return uniqueID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setUniqueID(String newUniqueID) {
		String oldUniqueID = uniqueID;
		uniqueID = newUniqueID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__UNIQUE_ID, oldUniqueID,
					uniqueID));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ElementRelationship> getRelatesTo() {
		if (relatesTo == null) {
			relatesTo = new EObjectContainmentWithInverseEList.Resolving<ElementRelationship>(ElementRelationship.class,
					this, ElementPackage.ELEMENT__RELATES_TO, RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT);
		}
		return relatesTo;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ElementRelationship> getRelatedBy() {
		if (relatedBy == null) {
			relatedBy = new EObjectWithInverseResolvingEList<ElementRelationship>(ElementRelationship.class, this,
					ElementPackage.ELEMENT__RELATED_BY, RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT);
		}
		return relatedBy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Group getParent() {
		if (eContainerFeatureID() != ElementPackage.ELEMENT__PARENT)
			return null;
		return (Group) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Group basicGetParent() {
		if (eContainerFeatureID() != ElementPackage.ELEMENT__PARENT)
			return null;
		return (Group) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public NotificationChain basicSetParent(Group newParent, NotificationChain msgs) {
		// getResourceSet().getResources().remove(eResource());
		msgs = eBasicSetContainer((InternalEObject) newParent, ElementPackage.ELEMENT__PARENT, msgs);
		// getResourceSet().getResources().add(eResource());

		// getResourceSet().getResources().add(eResource());
		// eResource().setURI(getElementURI());
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setParent(Group newParent) {
		if (newParent != eInternalContainer()
				|| (eContainerFeatureID() != ElementPackage.ELEMENT__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject) newParent).eInverseAdd(this, GroupPackage.GROUP__CONTENTS, Group.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__PARENT, newParent,
					newParent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public String getUri() {
		if (uri == null && getParent() != null) {
			return getParent().getUri();
		} else {
			return uri;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setUri(String newUri) {
		String oldUri = uri;
		uri = newUri;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__URI, oldUri, uri));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getWorkPackage() {
		return workPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setWorkPackage(String newWorkPackage) {
		String oldWorkPackage = workPackage;
		workPackage = newWorkPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__WORK_PACKAGE, oldWorkPackage,
					workPackage));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CommentList getChangeList() {
		if (changeList != null && changeList.eIsProxy()) {
			InternalEObject oldChangeList = (InternalEObject) changeList;
			changeList = (CommentList) eResolveProxy(oldChangeList);
			if (changeList != oldChangeList) {
				InternalEObject newChangeList = (InternalEObject) changeList;
				NotificationChain msgs = oldChangeList.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__CHANGE_LIST, null, null);
				if (newChangeList.eInternalContainer() == null) {
					msgs = newChangeList.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__CHANGE_LIST,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.ELEMENT__CHANGE_LIST,
							oldChangeList, changeList));
			}
		}
		return changeList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CommentList basicGetChangeList() {
		return changeList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetChangeList(CommentList newChangeList, NotificationChain msgs) {
		CommentList oldChangeList = changeList;
		changeList = newChangeList;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ElementPackage.ELEMENT__CHANGE_LIST, oldChangeList, newChangeList);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setChangeList(CommentList newChangeList) {
		if (newChangeList != changeList) {
			NotificationChain msgs = null;
			if (changeList != null)
				msgs = ((InternalEObject) changeList).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__CHANGE_LIST, null, msgs);
			if (newChangeList != null)
				msgs = ((InternalEObject) newChangeList).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__CHANGE_LIST, null, msgs);
			msgs = basicSetChangeList(newChangeList, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__CHANGE_LIST, newChangeList,
					newChangeList));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public User getResponsibleUser() {
		if (responsibleUser != null && responsibleUser.eIsProxy()) {
			InternalEObject oldResponsibleUser = (InternalEObject) responsibleUser;
			responsibleUser = (User) eResolveProxy(oldResponsibleUser);
			if (responsibleUser != oldResponsibleUser) {
				InternalEObject newResponsibleUser = (InternalEObject) responsibleUser;
				NotificationChain msgs = oldResponsibleUser.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__RESPONSIBLE_USER, null, null);
				if (newResponsibleUser.eInternalContainer() == null) {
					msgs = newResponsibleUser.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__RESPONSIBLE_USER, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.ELEMENT__RESPONSIBLE_USER,
							oldResponsibleUser, responsibleUser));
			}
		}
		return responsibleUser;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public User basicGetResponsibleUser() {
		return responsibleUser;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetResponsibleUser(User newResponsibleUser, NotificationChain msgs) {
		User oldResponsibleUser = responsibleUser;
		responsibleUser = newResponsibleUser;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ElementPackage.ELEMENT__RESPONSIBLE_USER, oldResponsibleUser, newResponsibleUser);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setResponsibleUser(User newResponsibleUser) {
		if (newResponsibleUser != responsibleUser) {
			NotificationChain msgs = null;
			if (responsibleUser != null)
				msgs = ((InternalEObject) responsibleUser).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__RESPONSIBLE_USER, null, msgs);
			if (newResponsibleUser != null)
				msgs = ((InternalEObject) newResponsibleUser).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__RESPONSIBLE_USER, null, msgs);
			msgs = basicSetResponsibleUser(newResponsibleUser, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__RESPONSIBLE_USER,
					newResponsibleUser, newResponsibleUser));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Date getDeadline() {
		return deadline;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDeadline(Date newDeadline) {
		Date oldDeadline = deadline;
		deadline = newDeadline;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__DEADLINE, oldDeadline,
					deadline));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public LockType getLockStatus() {
		if ((lockStatus == null || lockStatus == LockType.LOCK_LIKE_CONTAINER) && getParent() != null) {
			return getParent().getLockStatus();
		}
		return lockStatus;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setLockStatus(LockType newLockStatus) {
		LockType oldLockStatus = lockStatus;
		lockStatus = newLockStatus == null ? LOCK_STATUS_EDEFAULT : newLockStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__LOCK_STATUS, oldLockStatus,
					lockStatus));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getLockPassword() {
		return lockPassword;
	}

	public ComplexityType getEstimatedComplexity() {
		return estimatedComplexity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setEstimatedComplexity(ComplexityType newEstimatedComplexity) {
		ComplexityType oldEstimatedComplexity = estimatedComplexity;
		estimatedComplexity = newEstimatedComplexity == null ? ESTIMATED_COMPLEXITY_EDEFAULT : newEstimatedComplexity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__ESTIMATED_COMPLEXITY,
					oldEstimatedComplexity, estimatedComplexity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setLockPassword(String newLockPassword) {
		String oldLockPassword = lockPassword;
		lockPassword = newLockPassword;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__LOCK_PASSWORD,
					oldLockPassword, lockPassword));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Property getCost() {
		if (cost != null && cost.eIsProxy()) {
			InternalEObject oldCost = (InternalEObject) cost;
			cost = (Property) eResolveProxy(oldCost);
			if (cost != oldCost) {
				InternalEObject newCost = (InternalEObject) cost;
				NotificationChain msgs = oldCost.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__COST, null, null);
				if (newCost.eInternalContainer() == null) {
					msgs = newCost.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__COST, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.ELEMENT__COST, oldCost,
							cost));
			}
		}
		return cost;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Property basicGetCost() {
		return cost;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetCost(Property newCost, NotificationChain msgs) {
		Property oldCost = cost;
		cost = newCost;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__COST,
					oldCost, newCost);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public ResourceSet getResourceSet() {
		if (eResource() != null) {
			return eResource().getResourceSet();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setCost(Property newCost) {
		if (newCost != cost) {
			NotificationChain msgs = null;
			if (cost != null)
				msgs = ((InternalEObject) cost).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__COST, null, msgs);
			if (newCost != null)
				msgs = ((InternalEObject) newCost).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__COST, null, msgs);
			msgs = basicSetCost(newCost, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__COST, newCost, newCost));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Property getBenefit() {
		if (benefit != null && benefit.eIsProxy()) {
			InternalEObject oldBenefit = (InternalEObject) benefit;
			benefit = (Property) eResolveProxy(oldBenefit);
			if (benefit != oldBenefit) {
				InternalEObject newBenefit = (InternalEObject) benefit;
				NotificationChain msgs = oldBenefit.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__BENEFIT, null, null);
				if (newBenefit.eInternalContainer() == null) {
					msgs = newBenefit.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__BENEFIT, null,
							msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.ELEMENT__BENEFIT,
							oldBenefit, benefit));
			}
		}
		return benefit;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Property basicGetBenefit() {
		return benefit;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetBenefit(Property newBenefit, NotificationChain msgs) {
		Property oldBenefit = benefit;
		benefit = newBenefit;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ElementPackage.ELEMENT__BENEFIT, oldBenefit, newBenefit);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setBenefit(Property newBenefit) {
		if (newBenefit != benefit) {
			NotificationChain msgs = null;
			if (benefit != null)
				msgs = ((InternalEObject) benefit).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__BENEFIT, null, msgs);
			if (newBenefit != null)
				msgs = ((InternalEObject) newBenefit).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__BENEFIT, null, msgs);
			msgs = basicSetBenefit(newBenefit, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__BENEFIT, newBenefit,
					newBenefit));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getRisk() {
		return risk;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRisk(int newRisk) {
		int oldRisk = risk;
		risk = newRisk;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__RISK, oldRisk, risk));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LifeCyclePhase getLifeCyclePhase() {
		return lifeCyclePhase;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setLifeCyclePhase(LifeCyclePhase newLifeCyclePhase) {
		LifeCyclePhase oldLifeCyclePhase = lifeCyclePhase;
		lifeCyclePhase = newLifeCyclePhase == null ? LIFE_CYCLE_PHASE_EDEFAULT : newLifeCyclePhase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__LIFE_CYCLE_PHASE,
					oldLifeCyclePhase, lifeCyclePhase));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State getState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setState(State newState) {
		State oldState = state;
		state = newState == null ? STATE_EDEFAULT : newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ManagementDecision getManagementDecision() {
		return managementDecision;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setManagementDecision(ManagementDecision newManagementDecision) {
		ManagementDecision oldManagementDecision = managementDecision;
		managementDecision = newManagementDecision == null ? MANAGEMENT_DECISION_EDEFAULT : newManagementDecision;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__MANAGEMENT_DECISION,
					oldManagementDecision, managementDecision));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public QAAssessment getQaAssessment() {
		return qaAssessment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setQaAssessment(QAAssessment newQaAssessment) {
		QAAssessment oldQaAssessment = qaAssessment;
		qaAssessment = newQaAssessment == null ? QA_ASSESSMENT_EDEFAULT : newQaAssessment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__QA_ASSESSMENT,
					oldQaAssessment, qaAssessment));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Text getManagementDiscussion() {
		if (managementDiscussion != null && managementDiscussion.eIsProxy()) {
			InternalEObject oldManagementDiscussion = (InternalEObject) managementDiscussion;
			managementDiscussion = (Text) eResolveProxy(oldManagementDiscussion);
			if (managementDiscussion != oldManagementDiscussion) {
				InternalEObject newManagementDiscussion = (InternalEObject) managementDiscussion;
				NotificationChain msgs = oldManagementDiscussion.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION, null, null);
				if (newManagementDiscussion.eInternalContainer() == null) {
					msgs = newManagementDiscussion.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION, oldManagementDiscussion,
							managementDiscussion));
			}
		}
		return managementDiscussion;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Text basicGetManagementDiscussion() {
		return managementDiscussion;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetManagementDiscussion(Text newManagementDiscussion, NotificationChain msgs) {
		Text oldManagementDiscussion = managementDiscussion;
		managementDiscussion = newManagementDiscussion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION, oldManagementDiscussion, newManagementDiscussion);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setManagementDiscussion(Text newManagementDiscussion) {
		if (newManagementDiscussion != managementDiscussion) {
			NotificationChain msgs = null;
			if (managementDiscussion != null)
				msgs = ((InternalEObject) managementDiscussion).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION, null, msgs);
			if (newManagementDiscussion != null)
				msgs = ((InternalEObject) newManagementDiscussion).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION, null, msgs);
			msgs = basicSetManagementDiscussion(newManagementDiscussion, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION,
					newManagementDiscussion, newManagementDiscussion));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<DeletionListener> getDeletionListeners() {
		if (deletionListeners == null) {
			deletionListeners = new EObjectContainmentEList.Resolving<DeletionListener>(DeletionListener.class, this,
					ElementPackage.ELEMENT__DELETION_LISTENERS);
		}
		return deletionListeners;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<QuantityItem> getQuantities() {
		if (quantities == null) {
			quantities = new EObjectContainmentEList.Resolving<QuantityItem>(QuantityItem.class, this,
					ElementPackage.ELEMENT__QUANTITIES);
		}
		return quantities;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public boolean validate(DiagnosticChain diagnostic, Map<Object, Object> context) {

		if (true) {
			if (diagnostic != null) {
				diagnostic.add(new BasicDiagnostic(Diagnostic.ERROR, ElementValidator.DIAGNOSTIC_SOURCE,
						ElementValidator.ELEMENT__VALIDATE,
						EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic",
								new Object[] { "validate", EObjectValidator.getObjectLabel(this, context) }),
						new Object[] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public void save() {
		eSetDeliver(false);
		setLastModified(new Date());
		eSetDeliver(true);

		if (getParent() != null && !(getParent() instanceof Workspace)) {
			getParent().save();
		}

	}

	/**
	 * resource.eSetDeliver(false); resource.getContents().add(this);
	 * resource.eSetDeliver(true);
	 *
	 * try { resource.save(Collections.EMPTY_MAP);
	 *
	 * } catch (IOException e) {
	 *
	 * ValidationObservable.getInstance().modelChanged(this); }
	 *
	 *
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public void delete() {
		Set<ElementRelationship> deletedRelationships = new HashSet<ElementRelationship>();

		for (ElementRelationship relationship : getRelatedBy()) {
			deletedRelationships.add(relationship);
		}

		for (ElementRelationship relationship : deletedRelationships) {
			// fix dangling reference at the relatedBy element
			relationship.getFromElement().getRelatesTo().remove(relationship);
			relationship.getToElement().getRelatedBy().remove(relationship);
		}

		if (getParent() != null) {
			Group parent = getParent();
			parent.getContents().remove(this);
			parent.save();
		}

		List<DeletionListener> deletionListeners = new ArrayList<DeletionListener>(getDeletionListeners());
		for (DeletionListener listener : deletionListeners) {
			listener.notifyOfDeletion();
		}

		for (File file : WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedFiles()) {
			file.save();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public void acceptVisitor(Visitor visitor) {
		for (ElementRelationship rel : getRelatesTo()) {
			rel.acceptVisitor(visitor);
		}
		for (ElementRelationship rel : getRelatedBy()) {
			rel.acceptVisitor(visitor);
		}

		visitor.visit(this);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public Element findDescendantByUniqueID(String uniqueID) {
		if (this.getUniqueID().equals(uniqueID)) {
			return this;
		} else {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public SearchResultWrapper search(SearchParameter param) {
		if (param != null && param.getSearchString() != null && param.getSearchString().equals(getName())) {
			SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
			SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
			result.setAttribute(ElementPackage.Literals.ELEMENT__NAME);

			wrapper.getElementResults().add(result);
			wrapper.setElement(this);
			return wrapper;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public double computedEffort() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public String getStructeredIdType() {
		return this.getClass().getSimpleName().substring(0, 1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void undoDelete() {
		List<DeletionListener> deletionListeners = new ArrayList<DeletionListener>(getDeletionListeners());
		for (DeletionListener listener : deletionListeners) {
			listener.notifyOfUndo();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void checkStructure() {
		clearIssues();

		// Check 1c
		if ((getLabel() == null || getLabel().equals("")) && (getName() == null || getName().equals(""))) {
			addIssueComment(IssueSeverity.MISTAKE, "Both 'Label' and 'Name' are empty");
		}
		// Check 1a
		else if (getName() == null || getName().equals("")) {
			addIssueComment(IssueSeverity.WEAKNESS, "'Name' is empty");
		}
		// Check 1b
		else if (getLabel() == null || getLabel().equals("")) {
			addIssueComment(IssueSeverity.WARNING, "'Label' is empty");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void checkChildren() {
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void check() {
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void clearIssues() {
		Iterator<Comment> iterator = getCommentlist().getComments().iterator();
		while (iterator.hasNext()) {
			if (iterator.next() instanceof IssueComment) {
				iterator.remove();
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ElementPackage.ELEMENT__RELATES_TO:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRelatesTo()).basicAdd(otherEnd, msgs);
		case ElementPackage.ELEMENT__RELATED_BY:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRelatedBy()).basicAdd(otherEnd, msgs);
		case ElementPackage.ELEMENT__PARENT:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParent((Group) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ElementPackage.ELEMENT__COMMENTLIST:
			return basicSetCommentlist(null, msgs);
		case ElementPackage.ELEMENT__CREATOR:
			return basicSetCreator(null, msgs);
		case ElementPackage.ELEMENT__VISIBLE_ID:
			return basicSetVisibleID(null, msgs);
		case ElementPackage.ELEMENT__RELATES_TO:
			return ((InternalEList<?>) getRelatesTo()).basicRemove(otherEnd, msgs);
		case ElementPackage.ELEMENT__RELATED_BY:
			return ((InternalEList<?>) getRelatedBy()).basicRemove(otherEnd, msgs);
		case ElementPackage.ELEMENT__PARENT:
			return basicSetParent(null, msgs);
		case ElementPackage.ELEMENT__CHANGE_LIST:
			return basicSetChangeList(null, msgs);
		case ElementPackage.ELEMENT__RESPONSIBLE_USER:
			return basicSetResponsibleUser(null, msgs);
		case ElementPackage.ELEMENT__COST:
			return basicSetCost(null, msgs);
		case ElementPackage.ELEMENT__BENEFIT:
			return basicSetBenefit(null, msgs);
		case ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION:
			return basicSetManagementDiscussion(null, msgs);
		case ElementPackage.ELEMENT__DELETION_LISTENERS:
			return ((InternalEList<?>) getDeletionListeners()).basicRemove(otherEnd, msgs);
		case ElementPackage.ELEMENT__QUANTITIES:
			return ((InternalEList<?>) getQuantities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case ElementPackage.ELEMENT__PARENT:
			return eInternalContainer().eInverseRemove(this, GroupPackage.GROUP__CONTENTS, Group.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ElementPackage.ELEMENT__ICON_URI:
			return getIconURI();
		case ElementPackage.ELEMENT__ICON:
			return getIcon();
		case ElementPackage.ELEMENT__LABEL:
			return getLabel();
		case ElementPackage.ELEMENT__NAME:
			return getName();
		case ElementPackage.ELEMENT__ELEMENT_KIND:
			return getElementKind();
		case ElementPackage.ELEMENT__DESCRIPTION:
			return getDescription();
		case ElementPackage.ELEMENT__PRIORITY:
			return getPriority();
		case ElementPackage.ELEMENT__COMMENTLIST:
			if (resolve)
				return getCommentlist();
			return basicGetCommentlist();
		case ElementPackage.ELEMENT__TIME_CREATED:
			return getTimeCreated();
		case ElementPackage.ELEMENT__LAST_MODIFIED:
			return getLastModified();
		case ElementPackage.ELEMENT__LAST_MODIFIED_BY:
			if (resolve)
				return getLastModifiedBy();
			return basicGetLastModifiedBy();
		case ElementPackage.ELEMENT__CREATOR:
			if (resolve)
				return getCreator();
			return basicGetCreator();
		case ElementPackage.ELEMENT__VERSION:
			return getVersion();
		case ElementPackage.ELEMENT__VISIBLE_ID:
			if (resolve)
				return getVisibleID();
			return basicGetVisibleID();
		case ElementPackage.ELEMENT__UNIQUE_ID:
			return getUniqueID();
		case ElementPackage.ELEMENT__RELATES_TO:
			return getRelatesTo();
		case ElementPackage.ELEMENT__RELATED_BY:
			return getRelatedBy();
		case ElementPackage.ELEMENT__PARENT:
			if (resolve)
				return getParent();
			return basicGetParent();
		case ElementPackage.ELEMENT__URI:
			return getUri();
		case ElementPackage.ELEMENT__WORK_PACKAGE:
			return getWorkPackage();
		case ElementPackage.ELEMENT__CHANGE_LIST:
			if (resolve)
				return getChangeList();
			return basicGetChangeList();
		case ElementPackage.ELEMENT__RESPONSIBLE_USER:
			if (resolve)
				return getResponsibleUser();
			return basicGetResponsibleUser();
		case ElementPackage.ELEMENT__DEADLINE:
			return getDeadline();
		case ElementPackage.ELEMENT__LOCK_STATUS:
			return getLockStatus();
		case ElementPackage.ELEMENT__LOCK_PASSWORD:
			return getLockPassword();
		case ElementPackage.ELEMENT__ESTIMATED_COMPLEXITY:
			return getEstimatedComplexity();
		case ElementPackage.ELEMENT__COST:
			if (resolve)
				return getCost();
			return basicGetCost();
		case ElementPackage.ELEMENT__BENEFIT:
			if (resolve)
				return getBenefit();
			return basicGetBenefit();
		case ElementPackage.ELEMENT__RISK:
			return getRisk();
		case ElementPackage.ELEMENT__LIFE_CYCLE_PHASE:
			return getLifeCyclePhase();
		case ElementPackage.ELEMENT__STATE:
			return getState();
		case ElementPackage.ELEMENT__MANAGEMENT_DECISION:
			return getManagementDecision();
		case ElementPackage.ELEMENT__QA_ASSESSMENT:
			return getQaAssessment();
		case ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION:
			if (resolve)
				return getManagementDiscussion();
			return basicGetManagementDiscussion();
		case ElementPackage.ELEMENT__DELETION_LISTENERS:
			return getDeletionListeners();
		case ElementPackage.ELEMENT__QUANTITIES:
			return getQuantities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ElementPackage.ELEMENT__ICON_URI:
			setIconURI((String) newValue);
			return;
		case ElementPackage.ELEMENT__ICON:
			setIcon((Image) newValue);
			return;
		case ElementPackage.ELEMENT__LABEL:
			setLabel((String) newValue);
			return;
		case ElementPackage.ELEMENT__NAME:
			setName((String) newValue);
			return;
		case ElementPackage.ELEMENT__ELEMENT_KIND:
			setElementKind((String) newValue);
			return;
		case ElementPackage.ELEMENT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case ElementPackage.ELEMENT__PRIORITY:
			setPriority((Priority) newValue);
			return;
		case ElementPackage.ELEMENT__COMMENTLIST:
			setCommentlist((CommentList) newValue);
			return;
		case ElementPackage.ELEMENT__TIME_CREATED:
			setTimeCreated((Date) newValue);
			return;
		case ElementPackage.ELEMENT__LAST_MODIFIED:
			setLastModified((Date) newValue);
			return;
		case ElementPackage.ELEMENT__LAST_MODIFIED_BY:
			setLastModifiedBy((User) newValue);
			return;
		case ElementPackage.ELEMENT__CREATOR:
			setCreator((User) newValue);
			return;
		case ElementPackage.ELEMENT__VERSION:
			setVersion((String) newValue);
			return;
		case ElementPackage.ELEMENT__VISIBLE_ID:
			setVisibleID((VisibleElementID) newValue);
			return;
		case ElementPackage.ELEMENT__UNIQUE_ID:
			setUniqueID((String) newValue);
			return;
		case ElementPackage.ELEMENT__RELATES_TO:
			getRelatesTo().clear();
			getRelatesTo().addAll((Collection<? extends ElementRelationship>) newValue);
			return;
		case ElementPackage.ELEMENT__RELATED_BY:
			getRelatedBy().clear();
			getRelatedBy().addAll((Collection<? extends ElementRelationship>) newValue);
			return;
		case ElementPackage.ELEMENT__PARENT:
			setParent((Group) newValue);
			return;
		case ElementPackage.ELEMENT__URI:
			setUri((String) newValue);
			return;
		case ElementPackage.ELEMENT__WORK_PACKAGE:
			setWorkPackage((String) newValue);
			return;
		case ElementPackage.ELEMENT__CHANGE_LIST:
			setChangeList((CommentList) newValue);
			return;
		case ElementPackage.ELEMENT__RESPONSIBLE_USER:
			setResponsibleUser((User) newValue);
			return;
		case ElementPackage.ELEMENT__DEADLINE:
			setDeadline((Date) newValue);
			return;
		case ElementPackage.ELEMENT__LOCK_STATUS:
			setLockStatus((LockType) newValue);
			return;
		case ElementPackage.ELEMENT__LOCK_PASSWORD:
			setLockPassword((String) newValue);
			return;
		case ElementPackage.ELEMENT__ESTIMATED_COMPLEXITY:
			setEstimatedComplexity((ComplexityType) newValue);
			return;
		case ElementPackage.ELEMENT__COST:
			setCost((Property) newValue);
			return;
		case ElementPackage.ELEMENT__BENEFIT:
			setBenefit((Property) newValue);
			return;
		case ElementPackage.ELEMENT__RISK:
			setRisk((Integer) newValue);
			return;
		case ElementPackage.ELEMENT__LIFE_CYCLE_PHASE:
			setLifeCyclePhase((LifeCyclePhase) newValue);
			return;
		case ElementPackage.ELEMENT__STATE:
			setState((State) newValue);
			return;
		case ElementPackage.ELEMENT__MANAGEMENT_DECISION:
			setManagementDecision((ManagementDecision) newValue);
			return;
		case ElementPackage.ELEMENT__QA_ASSESSMENT:
			setQaAssessment((QAAssessment) newValue);
			return;
		case ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION:
			setManagementDiscussion((Text) newValue);
			return;
		case ElementPackage.ELEMENT__DELETION_LISTENERS:
			getDeletionListeners().clear();
			getDeletionListeners().addAll((Collection<? extends DeletionListener>) newValue);
			return;
		case ElementPackage.ELEMENT__QUANTITIES:
			getQuantities().clear();
			getQuantities().addAll((Collection<? extends QuantityItem>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ElementPackage.ELEMENT__ICON_URI:
			setIconURI(ICON_URI_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__ICON:
			setIcon(ICON_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__LABEL:
			setLabel(LABEL_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__NAME:
			setName(NAME_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__ELEMENT_KIND:
			setElementKind(ELEMENT_KIND_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__PRIORITY:
			setPriority(PRIORITY_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__COMMENTLIST:
			setCommentlist((CommentList) null);
			return;
		case ElementPackage.ELEMENT__TIME_CREATED:
			setTimeCreated(TIME_CREATED_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__LAST_MODIFIED:
			setLastModified(LAST_MODIFIED_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__LAST_MODIFIED_BY:
			setLastModifiedBy((User) null);
			return;
		case ElementPackage.ELEMENT__CREATOR:
			setCreator((User) null);
			return;
		case ElementPackage.ELEMENT__VERSION:
			setVersion(VERSION_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__VISIBLE_ID:
			setVisibleID((VisibleElementID) null);
			return;
		case ElementPackage.ELEMENT__UNIQUE_ID:
			setUniqueID(UNIQUE_ID_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__RELATES_TO:
			getRelatesTo().clear();
			return;
		case ElementPackage.ELEMENT__RELATED_BY:
			getRelatedBy().clear();
			return;
		case ElementPackage.ELEMENT__PARENT:
			setParent((Group) null);
			return;
		case ElementPackage.ELEMENT__URI:
			setUri(URI_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__WORK_PACKAGE:
			setWorkPackage(WORK_PACKAGE_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__CHANGE_LIST:
			setChangeList((CommentList) null);
			return;
		case ElementPackage.ELEMENT__RESPONSIBLE_USER:
			setResponsibleUser((User) null);
			return;
		case ElementPackage.ELEMENT__DEADLINE:
			setDeadline(DEADLINE_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__LOCK_STATUS:
			setLockStatus(LOCK_STATUS_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__LOCK_PASSWORD:
			setLockPassword(LOCK_PASSWORD_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__ESTIMATED_COMPLEXITY:
			setEstimatedComplexity(ESTIMATED_COMPLEXITY_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__COST:
			setCost((Property) null);
			return;
		case ElementPackage.ELEMENT__BENEFIT:
			setBenefit((Property) null);
			return;
		case ElementPackage.ELEMENT__RISK:
			setRisk(RISK_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__LIFE_CYCLE_PHASE:
			setLifeCyclePhase(LIFE_CYCLE_PHASE_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__STATE:
			setState(STATE_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__MANAGEMENT_DECISION:
			setManagementDecision(MANAGEMENT_DECISION_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__QA_ASSESSMENT:
			setQaAssessment(QA_ASSESSMENT_EDEFAULT);
			return;
		case ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION:
			setManagementDiscussion((Text) null);
			return;
		case ElementPackage.ELEMENT__DELETION_LISTENERS:
			getDeletionListeners().clear();
			return;
		case ElementPackage.ELEMENT__QUANTITIES:
			getQuantities().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ElementPackage.ELEMENT__ICON_URI:
			return ICON_URI_EDEFAULT == null ? iconURI != null : !ICON_URI_EDEFAULT.equals(iconURI);
		case ElementPackage.ELEMENT__ICON:
			return ICON_EDEFAULT == null ? icon != null : !ICON_EDEFAULT.equals(icon);
		case ElementPackage.ELEMENT__LABEL:
			return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
		case ElementPackage.ELEMENT__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case ElementPackage.ELEMENT__ELEMENT_KIND:
			return ELEMENT_KIND_EDEFAULT == null ? elementKind != null : !ELEMENT_KIND_EDEFAULT.equals(elementKind);
		case ElementPackage.ELEMENT__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case ElementPackage.ELEMENT__PRIORITY:
			return priority != PRIORITY_EDEFAULT;
		case ElementPackage.ELEMENT__COMMENTLIST:
			return commentlist != null;
		case ElementPackage.ELEMENT__TIME_CREATED:
			return TIME_CREATED_EDEFAULT == null ? timeCreated != null : !TIME_CREATED_EDEFAULT.equals(timeCreated);
		case ElementPackage.ELEMENT__LAST_MODIFIED:
			return LAST_MODIFIED_EDEFAULT == null ? lastModified != null : !LAST_MODIFIED_EDEFAULT.equals(lastModified);
		case ElementPackage.ELEMENT__LAST_MODIFIED_BY:
			return lastModifiedBy != null;
		case ElementPackage.ELEMENT__CREATOR:
			return creator != null;
		case ElementPackage.ELEMENT__VERSION:
			return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		case ElementPackage.ELEMENT__VISIBLE_ID:
			return visibleID != null;
		case ElementPackage.ELEMENT__UNIQUE_ID:
			return UNIQUE_ID_EDEFAULT == null ? uniqueID != null : !UNIQUE_ID_EDEFAULT.equals(uniqueID);
		case ElementPackage.ELEMENT__RELATES_TO:
			return relatesTo != null && !relatesTo.isEmpty();
		case ElementPackage.ELEMENT__RELATED_BY:
			return relatedBy != null && !relatedBy.isEmpty();
		case ElementPackage.ELEMENT__PARENT:
			return basicGetParent() != null;
		case ElementPackage.ELEMENT__URI:
			return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
		case ElementPackage.ELEMENT__WORK_PACKAGE:
			return WORK_PACKAGE_EDEFAULT == null ? workPackage != null : !WORK_PACKAGE_EDEFAULT.equals(workPackage);
		case ElementPackage.ELEMENT__CHANGE_LIST:
			return changeList != null;
		case ElementPackage.ELEMENT__RESPONSIBLE_USER:
			return responsibleUser != null;
		case ElementPackage.ELEMENT__DEADLINE:
			return DEADLINE_EDEFAULT == null ? deadline != null : !DEADLINE_EDEFAULT.equals(deadline);
		case ElementPackage.ELEMENT__LOCK_STATUS:
			return lockStatus != LOCK_STATUS_EDEFAULT;
		case ElementPackage.ELEMENT__LOCK_PASSWORD:
			return LOCK_PASSWORD_EDEFAULT == null ? lockPassword != null : !LOCK_PASSWORD_EDEFAULT.equals(lockPassword);
		case ElementPackage.ELEMENT__ESTIMATED_COMPLEXITY:
			return estimatedComplexity != ESTIMATED_COMPLEXITY_EDEFAULT;
		case ElementPackage.ELEMENT__COST:
			return cost != null;
		case ElementPackage.ELEMENT__BENEFIT:
			return benefit != null;
		case ElementPackage.ELEMENT__RISK:
			return risk != RISK_EDEFAULT;
		case ElementPackage.ELEMENT__LIFE_CYCLE_PHASE:
			return lifeCyclePhase != LIFE_CYCLE_PHASE_EDEFAULT;
		case ElementPackage.ELEMENT__STATE:
			return state != STATE_EDEFAULT;
		case ElementPackage.ELEMENT__MANAGEMENT_DECISION:
			return managementDecision != MANAGEMENT_DECISION_EDEFAULT;
		case ElementPackage.ELEMENT__QA_ASSESSMENT:
			return qaAssessment != QA_ASSESSMENT_EDEFAULT;
		case ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION:
			return managementDiscussion != null;
		case ElementPackage.ELEMENT__DELETION_LISTENERS:
			return deletionListeners != null && !deletionListeners.isEmpty();
		case ElementPackage.ELEMENT__QUANTITIES:
			return quantities != null && !quantities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (iconURI: ");
		result.append(iconURI);
		result.append(", icon: ");
		result.append(icon);
		result.append(", label: ");
		result.append(label);
		result.append(", name: ");
		result.append(name);
		result.append(", elementKind: ");
		result.append(elementKind);
		result.append(", description: ");
		result.append(description);
		result.append(", priority: ");
		result.append(priority);
		result.append(", timeCreated: ");
		result.append(timeCreated);
		result.append(", lastModified: ");
		result.append(lastModified);
		result.append(", version: ");
		result.append(version);
		result.append(", uniqueID: ");
		result.append(uniqueID);
		result.append(", uri: ");
		result.append(uri);
		result.append(", workPackage: ");
		result.append(workPackage);
		result.append(", deadline: ");
		result.append(deadline);
		result.append(", lockStatus: ");
		result.append(lockStatus);
		result.append(", lockPassword: ");
		result.append(lockPassword);
		result.append(", estimatedComplexity: ");
		result.append(estimatedComplexity);
		result.append(", risk: ");
		result.append(risk);
		result.append(", lifeCyclePhase: ");
		result.append(lifeCyclePhase);
		result.append(", state: ");
		result.append(state);
		result.append(", managementDecision: ");
		result.append(managementDecision);
		result.append(", qaAssessment: ");
		result.append(qaAssessment);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean equals(Object arg0) {
		if (arg0 instanceof Element) {
			return getUniqueID().equals(((Element) arg0).getUniqueID());
		}
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	protected SearchResultWrapper search(SearchParameter param, Map<EStructuralFeature, String> attributesToSearch) {

		// TODO: figure out if we could live without the code below
		// if (OpenSearchDialog.isSearchCanceled()) {
		// return null;
		// }

		attributesToSearch.put(ElementPackage.Literals.ELEMENT__NAME, this.getName());
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__LABEL, this.getLabel());
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__ELEMENT_KIND, this.getElementKind());
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__DESCRIPTION, this.getDescription());

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(this);

		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			if (feature instanceof EReference) {
				value = value.replaceAll("\r\n", "");
				value = value.replaceAll("&nbsp;", " ");
				value = value.replaceAll("<BR>", "\n");
				value = value.replaceAll("<br/>", "\n");
				value = value.replaceAll("</P>", "\n\n");
				value = value.replaceAll("</p>", "\n\n");
				value = value.replaceAll("\\<[^>]*>", "");
				value = value.replaceAll("\r\n", "\n");

			}

			int index = 0;
			if (value == null) {
				continue;
			}
			value = value.toUpperCase(Locale.US);
			if (param != null) {

				int hitIndex = value.indexOf(param.getSearchString().toUpperCase(Locale.US), index);
				// find all occurences of the search string in this attribute
				while (hitIndex != -1) {
					SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
					result.setElement(this);
					result.setAttribute(feature);
					result.setStartIndex(hitIndex);
					result.setEndIndex(hitIndex + param.getSearchString().length());

					wrapper.getElementResults().add(result);

					hitIndex = value.indexOf(param.getSearchString().toUpperCase(Locale.US), result.getEndIndex());
				}
			}
		}

		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}

	/**
	 * @generated NOT
	 */
	public static boolean validateObject(EObject eObject) {
		// Diagnostician diagnostic = new Diagnostician() {
		//
		// @Override
		// public String getObjectLabel(EObject eObject) {
		// // TODO Auto-generated method stub
		// return ((Element) eObject).getName();
		// }
		// };
		Diagnostic d = Diagnostician.INSTANCE.validate(eObject);
		if (d.getSeverity() == Diagnostic.ERROR || d.getSeverity() == Diagnostic.WARNING) {
			System.err.println(d.getMessage());
			for (Iterator i = d.getChildren().iterator(); i.hasNext();) {
				Diagnostic childDiagnostic = (Diagnostic) i.next();
				switch (childDiagnostic.getSeverity()) {
				case Diagnostic.ERROR:
				case Diagnostic.WARNING:
					System.err.println("\t" + childDiagnostic.getMessage());
				}
			}
			return false;
		}
		return true;
	}

	protected void createDiagnosticWarning(String message, DiagnosticChain diagnostic) {
		if (diagnostic != null) {
			diagnostic.add(new BasicDiagnostic(Diagnostic.WARNING, ElementValidator.DIAGNOSTIC_SOURCE,
					ElementValidator.ELEMENT__VALIDATE, message, new Object[] { this }));
		}
	}

	protected void createDiagnosticError(String message, DiagnosticChain diagnostic) {
		if (diagnostic != null) {
			diagnostic.add(new BasicDiagnostic(Diagnostic.ERROR, ElementValidator.DIAGNOSTIC_SOURCE,
					ElementValidator.ELEMENT__VALIDATE, message, new Object[] { this }));
		}
	}

	protected void createDiagnosticInfo(String message, DiagnosticChain diagnostic) {
		if (diagnostic != null) {
			diagnostic.add(new BasicDiagnostic(Diagnostic.INFO, ElementValidator.DIAGNOSTIC_SOURCE,
					ElementValidator.ELEMENT__VALIDATE, message, new Object[] { this }));
		}
	}
	// @generated NOT

	// @generated NOT
	@Override
	public ElementColumnDefinition[] getColumnRepresentation(Class<?> type) {

		//
		if (type.equals(Element.class)) {
			return new ElementColumnDefinition[] { new ElementColumnDefinition().setHeaderName("Name")
					.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME).setColumnBehaviour(ColumnBehaviour.CellEdit)
					.setEditable(true).setCellHandler(new CellHandler<Element>() {

						@Override
						public Object getAttributeValue(Element item) {
							return item.getName();
						}

						@Override
						public void setAttributeValue(Element item, Object value) {
							item.setName((String) value);
						}
					}),
					new ElementColumnDefinition().setHeaderName("Creator")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__CREATOR)
							.setColumnBehaviour(ColumnBehaviour.CellEdit).setEditable(true)
							.setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getCreator() != null ? item.getCreator().getName() : "";
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									if (item.getCreator() == null) {
										User creator = UserFactory.eINSTANCE.createUser();
										creator.setName(value.toString());
										item.setCreator(creator);
									} else {
										item.getCreator().setName(value.toString());
									}
								}
							}),
					new ElementColumnDefinition().setHeaderName("Responsible User")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__NAME)
							.setColumnBehaviour(ColumnBehaviour.CellEdit).setEditable(true)
							.setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getResponsibleUser() != null ? item.getResponsibleUser().getName() : "";
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									item.setName((String) value);

									if (item.getResponsibleUser() == null) {
										User responsible = UserFactory.eINSTANCE.createUser();
										responsible.setName(value.toString());
										item.setResponsibleUser(responsible);
									} else {
										item.getResponsibleUser().setName(value.toString());
									}
								}
							}),
					new ElementColumnDefinition().setHeaderName("Work Package")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__WORK_PACKAGE)
							.setColumnBehaviour(ColumnBehaviour.CellEdit).setEditable(true)
							.setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getWorkPackage();
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									item.setWorkPackage(value.toString());
								}
							}),
					new ElementColumnDefinition().setHeaderName("Version")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__VERSION)
							.setColumnBehaviour(ColumnBehaviour.CellEdit).setEditable(true)
							.setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getVersion();
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									item.setVersion(value.toString());
								}
							}),
					new ElementColumnDefinition().setHeaderName("State")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__STATE)
							.setColumnBehaviour(ColumnBehaviour.ComboSelector)
							.setCellItems(ElementColumnDefinition.enumToStringArray(State.values())).setEditable(true)
							.setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getState().getName();
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									item.setState(State.valueOf(value.toString().toUpperCase()));
								}
							}),
					new ElementColumnDefinition().setHeaderName("Priority")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__PRIORITY)
							.setColumnBehaviour(ColumnBehaviour.CellEdit)
							.setCellItems(ElementColumnDefinition.enumToStringArray(Priority.values()))
							.setEditable(true).setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getPriority().getName();
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									item.setPriority(Priority.valueOf(value.toString().toUpperCase()));
								}
							}),
					new ElementColumnDefinition().setHeaderName("Creation Date")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__TIME_CREATED)
							.setColumnBehaviour(ColumnBehaviour.CellEdit).setEditable(true)
							.setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getTimeCreated() != null ? item.getTimeCreated().toString() : "";
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									Date d;
									try {
										d = DateFormat.getDateInstance().parse(value.toString());
										item.setTimeCreated(d);
									} catch (ParseException e) {
									}
								}
							}),
					new ElementColumnDefinition().setHeaderName("Last Save")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__LAST_MODIFIED)
							.setColumnBehaviour(ColumnBehaviour.CellEdit).setEditable(true)
							.setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getLastModified() != null ? item.getLastModified().toString() : "";
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									Date d;
									try {
										d = DateFormat.getDateInstance().parse(value.toString());
										item.setLastModified(d);
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							}),
					new ElementColumnDefinition().setHeaderName("Deadline")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__DEADLINE)
							.setColumnBehaviour(ColumnBehaviour.CellEdit).setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getDeadline() != null ? item.getDeadline().toString() : "";
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									Date d;
									try {
										d = DateFormat.getDateInstance().parse(value.toString());
										item.setDeadline(d);
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							}),
					new ElementColumnDefinition().setHeaderName("Lockstatus")
							.setEMFLiteral(ElementPackage.Literals.ELEMENT__LOCK_STATUS)
							.setColumnBehaviour(ColumnBehaviour.ComboSelector)
							.setCellItems(ElementColumnDefinition.enumToStringArray(LockType.values()))
							.setEditable(true).setCellHandler(new CellHandler<Element>() {

								@Override
								public Object getAttributeValue(Element item) {
									return item.getLockStatus().getName();
								}

								@Override
								public void setAttributeValue(Element item, Object value) {
									item.setLockStatus(LockType.valueOf(value.toString().toUpperCase()));
								}
							}), };
		} else {
			return getColumnRepresentation();
		}
	}

	//@generated NOT
	 @Override
	 public ElementColumnDefinition[] getColumnRepresentation() {
			return new ElementColumnDefinition[] {
					new ElementColumnDefinition()
					.setHeaderName("ID")
					.setEMFLiteral(ElementPackage.Literals.ELEMENT__ESTIMATED_COMPLEXITY)
					.setColumnBehaviour(ColumnBehaviour.CellEdit)
					.setEditable(true)
					.setCellHandler(new CellHandler<Element>() {

						@Override
						public Object getAttributeValue(Element item) {
							return item.getEstimatedComplexity();
						}

						@Override
						public void setAttributeValue(Element item, Object value ) {
							item.setEstimatedComplexity(ComplexityType.valueOf(value.toString()));
						}
				})};
	 }

	// @generated NOT
	public String getPlainDisplayName() {

		if (this.getLabel() != null && !this.getLabel().isEmpty() && this.getName() != null
				&& !this.getName().isEmpty()) {
			return this.getLabel() + " - " + this.getName();
		} else if (this.getLabel() != null && !this.getLabel().isEmpty()) {
			return this.getLabel();
		} else if (this.getName() != null && !this.getName().isEmpty()) {
			return this.getName();
		} else {
			return "";
		}

	}

	 //@generated NOT
	 public String getComplexDisplayName(){
		 if(this.getLabel()!=null){
			 return this.getLabel();
		 }
		 return this.getName();
	 }

	/**
	 * @generated NOT
	 */
	protected void addIssueComment(IssueSeverity severity, String message) {
		IssueComment comment = CommentFactory.eINSTANCE.createIssueComment();
		Text text = TextFactory.eINSTANCE.createText(message);

		comment.setSeverity(severity);
		comment.setText(text);

		getCommentlist().getComments().add(comment);
	}

	/**
	 * Generates a hash ID of the form type[label]name
	 * 
	 * @return String elementHashId
	 */
	public String getElementHashId() {
		// Get the name of the java file
		if (elementHashId == null) {

			String cName = this.getClass().getName();
			String[] cNameSep = cName.split("\\.");
			cName = cNameSep[cNameSep.length - 1];

			// cut off the "Impl" part of the file name and create the HashID
			StringBuilder builder;
			builder = new StringBuilder(cName.substring(0, cName.length() - "Impl".length()));
			builder.append('[');
			if (this.getLabel() != null)
				builder.append(this.getLabel());
			builder.append(']');
			if (this.name != null)
				builder.append(this.name);
			elementHashId = builder.toString();
		}
		return elementHashId;
	}
} // ElementImpl
