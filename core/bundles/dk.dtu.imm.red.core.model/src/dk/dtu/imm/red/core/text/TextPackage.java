/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.text;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.text.TextFactory
 * @model kind="package"
 * @generated
 */
public interface TextPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "text";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.core.text";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "text";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	TextPackage eINSTANCE = dk.dtu.imm.red.core.text.impl.TextPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.text.impl.TextImpl <em>Text</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.text.impl.TextImpl
	 * @see dk.dtu.imm.red.core.text.impl.TextPackageImpl#getText()
	 * @generated
	 */
	int TEXT = 0;

	/**
	 * The feature id for the '<em><b>Fragments</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT__FRAGMENTS = 0;

	/**
	 * The number of structural features of the '<em>Text</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TEXT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.text.impl.TextFragmentImpl <em>Fragment</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.text.impl.TextFragmentImpl
	 * @see dk.dtu.imm.red.core.text.impl.TextPackageImpl#getTextFragment()
	 * @generated
	 */
	int TEXT_FRAGMENT = 1;

	/**
	 * The feature id for the '<em><b>Contained Text</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT_FRAGMENT__CONTAINED_TEXT = 0;

	/**
	 * The number of structural features of the '<em>Fragment</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TEXT_FRAGMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.text.impl.FormattedTextImpl <em>Formatted Text</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see dk.dtu.imm.red.core.text.impl.FormattedTextImpl
	 * @see dk.dtu.imm.red.core.text.impl.TextPackageImpl#getFormattedText()
	 * @generated
	 */
	int FORMATTED_TEXT = 2;

	/**
	 * The feature id for the '<em><b>Contained Text</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMATTED_TEXT__CONTAINED_TEXT = TEXT_FRAGMENT__CONTAINED_TEXT;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FORMATTED_TEXT__TEXT = TEXT_FRAGMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Formatted Text</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMATTED_TEXT_FEATURE_COUNT = TEXT_FRAGMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.text.impl.ReferenceImpl <em>Reference</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.text.impl.ReferenceImpl
	 * @see dk.dtu.imm.red.core.text.impl.TextPackageImpl#getReference()
	 * @generated
	 */
	int REFERENCE = 3;

	/**
	 * The feature id for the '<em><b>Contained Text</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__CONTAINED_TEXT = TEXT_FRAGMENT__CONTAINED_TEXT;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int REFERENCE__BODY = TEXT_FRAGMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FEATURE_COUNT = TEXT_FRAGMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.text.impl.CrossReferenceImpl <em>Cross Reference</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see dk.dtu.imm.red.core.text.impl.CrossReferenceImpl
	 * @see dk.dtu.imm.red.core.text.impl.TextPackageImpl#getCrossReference()
	 * @generated
	 */
	int CROSS_REFERENCE = 4;

	/**
	 * The feature id for the '<em><b>Contained Text</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CROSS_REFERENCE__CONTAINED_TEXT = REFERENCE__CONTAINED_TEXT;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CROSS_REFERENCE__BODY = REFERENCE__BODY;

	/**
	 * The feature id for the '<em><b>Refers To</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CROSS_REFERENCE__REFERS_TO = REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cross Reference</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CROSS_REFERENCE_FEATURE_COUNT = REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '<em>Type</em>' data type.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.text.Text
	 * @see dk.dtu.imm.red.core.text.impl.TextPackageImpl#getTextType()
	 * @generated
	 */
	int TEXT_TYPE = 5;

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.text.Text <em>Text</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Text</em>'.
	 * @see dk.dtu.imm.red.core.text.Text
	 * @generated
	 */
	EClass getText();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.core.text.Text#getFragments <em>Fragments</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fragments</em>'.
	 * @see dk.dtu.imm.red.core.text.Text#getFragments()
	 * @see #getText()
	 * @generated
	 */
	EReference getText_Fragments();

	/**
	 * Returns the meta object for class '
	 * {@link dk.dtu.imm.red.core.text.TextFragment <em>Fragment</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Fragment</em>'.
	 * @see dk.dtu.imm.red.core.text.TextFragment
	 * @generated
	 */
	EClass getTextFragment();

	/**
	 * Returns the meta object for the container reference '{@link dk.dtu.imm.red.core.text.TextFragment#getContainedText <em>Contained Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Contained Text</em>'.
	 * @see dk.dtu.imm.red.core.text.TextFragment#getContainedText()
	 * @see #getTextFragment()
	 * @generated
	 */
	EReference getTextFragment_ContainedText();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.text.FormattedText <em>Formatted Text</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formatted Text</em>'.
	 * @see dk.dtu.imm.red.core.text.FormattedText
	 * @generated
	 */
	EClass getFormattedText();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.text.FormattedText#getText <em>Text</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see dk.dtu.imm.red.core.text.FormattedText#getText()
	 * @see #getFormattedText()
	 * @generated
	 */
	EAttribute getFormattedText_Text();

	/**
	 * Returns the meta object for class '
	 * {@link dk.dtu.imm.red.core.text.Reference <em>Reference</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Reference</em>'.
	 * @see dk.dtu.imm.red.core.text.Reference
	 * @generated
	 */
	EClass getReference();

	/**
	 * Returns the meta object for the attribute '
	 * {@link dk.dtu.imm.red.core.text.Reference#getBody <em>Body</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Body</em>'.
	 * @see dk.dtu.imm.red.core.text.Reference#getBody()
	 * @see #getReference()
	 * @generated
	 */
	EAttribute getReference_Body();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.text.CrossReference <em>Cross Reference</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cross Reference</em>'.
	 * @see dk.dtu.imm.red.core.text.CrossReference
	 * @generated
	 */
	EClass getCrossReference();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.core.text.CrossReference#getRefersTo <em>Refers To</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refers To</em>'.
	 * @see dk.dtu.imm.red.core.text.CrossReference#getRefersTo()
	 * @see #getCrossReference()
	 * @generated
	 */
	EReference getCrossReference_RefersTo();

	/**
	 * Returns the meta object for data type '{@link dk.dtu.imm.red.core.text.Text <em>Type</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Type</em>'.
	 * @see dk.dtu.imm.red.core.text.Text
	 * @model instanceClass="dk.dtu.imm.red.core.text.Text"
	 * @generated
	 */
	EDataType getTextType();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TextFactory getTextFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.text.impl.TextImpl <em>Text</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.text.impl.TextImpl
		 * @see dk.dtu.imm.red.core.text.impl.TextPackageImpl#getText()
		 * @generated
		 */
		EClass TEXT = eINSTANCE.getText();

		/**
		 * The meta object literal for the '<em><b>Fragments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference TEXT__FRAGMENTS = eINSTANCE.getText_Fragments();

		/**
		 * The meta object literal for the '
		 * {@link dk.dtu.imm.red.core.text.implementation.TextFragmentImpl
		 * <em>Fragment</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see dk.dtu.imm.red.core.text.implementation.TextFragmentImpl
		 * @see dk.dtu.imm.red.core.text.implementation.TextPackageImpl#getTextFragment()
		 * @generated
		 */
		EClass TEXT_FRAGMENT = eINSTANCE.getTextFragment();

		/**
		 * The meta object literal for the '<em><b>Contained Text</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEXT_FRAGMENT__CONTAINED_TEXT = eINSTANCE.getTextFragment_ContainedText();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.text.impl.FormattedTextImpl <em>Formatted Text</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see dk.dtu.imm.red.core.text.impl.FormattedTextImpl
		 * @see dk.dtu.imm.red.core.text.impl.TextPackageImpl#getFormattedText()
		 * @generated
		 */
		EClass FORMATTED_TEXT = eINSTANCE.getFormattedText();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FORMATTED_TEXT__TEXT = eINSTANCE.getFormattedText_Text();

		/**
		 * The meta object literal for the '
		 * {@link dk.dtu.imm.red.core.text.implementation.ReferenceImpl
		 * <em>Reference</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see dk.dtu.imm.red.core.text.implementation.ReferenceImpl
		 * @see dk.dtu.imm.red.core.text.implementation.TextPackageImpl#getReference()
		 * @generated
		 */
		EClass REFERENCE = eINSTANCE.getReference();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCE__BODY = eINSTANCE.getReference_Body();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.text.impl.CrossReferenceImpl <em>Cross Reference</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see dk.dtu.imm.red.core.text.impl.CrossReferenceImpl
		 * @see dk.dtu.imm.red.core.text.impl.TextPackageImpl#getCrossReference()
		 * @generated
		 */
		EClass CROSS_REFERENCE = eINSTANCE.getCrossReference();

		/**
		 * The meta object literal for the '<em><b>Refers To</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference CROSS_REFERENCE__REFERS_TO = eINSTANCE.getCrossReference_RefersTo();

		/**
		 * The meta object literal for the '<em>Type</em>' data type. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see dk.dtu.imm.red.core.text.Text
		 * @see dk.dtu.imm.red.core.text.implementation.TextPackageImpl#getTextType()
		 * @generated
		 */
		EDataType TEXT_TYPE = eINSTANCE.getTextType();

	}

} // TextPackage
