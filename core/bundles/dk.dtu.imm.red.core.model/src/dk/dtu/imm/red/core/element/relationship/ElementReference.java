/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.relationship;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Element Reference</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.relationship.ElementReference#getRelationshipKind <em>Relationship Kind</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage#getElementReference()
 * @model
 * @generated
 */
public interface ElementReference extends ElementRelationship {

	/**
	 * Returns the value of the '<em><b>Relationship Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.relationship.RelationshipKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationship Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationship Kind</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.relationship.RelationshipKind
	 * @see #setRelationshipKind(RelationshipKind)
	 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage#getElementReference_RelationshipKind()
	 * @model
	 * @generated
	 */
	RelationshipKind getRelationshipKind();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.relationship.ElementReference#getRelationshipKind <em>Relationship Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relationship Kind</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.relationship.RelationshipKind
	 * @see #getRelationshipKind()
	 * @generated
	 */
	void setRelationshipKind(RelationshipKind value);
} // ElementReference
