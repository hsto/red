/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.relationship;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Glossary Entry Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage#getGlossaryEntryReference()
 * @model
 * @generated
 */
public interface GlossaryEntryReference extends ElementRelationship {
} // GlossaryEntryReference
