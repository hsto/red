/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.text;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Cross Reference</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.text.CrossReference#getRefersTo <em>Refers To</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.text.TextPackage#getCrossReference()
 * @model
 * @generated
 */
public interface CrossReference extends Reference {
	/**
	 * Returns the value of the '<em><b>Refers To</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refers To</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Refers To</em>' reference.
	 * @see #setRefersTo(Element)
	 * @see dk.dtu.imm.red.core.text.TextPackage#getCrossReference_RefersTo()
	 * @model required="true"
	 * @generated
	 */
	ElementRelationship getRefersTo();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.text.CrossReference#getRefersTo <em>Refers To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refers To</em>' reference.
	 * @see #getRefersTo()
	 * @generated
	 */
	void setRefersTo(ElementRelationship value);

} // CrossReference
