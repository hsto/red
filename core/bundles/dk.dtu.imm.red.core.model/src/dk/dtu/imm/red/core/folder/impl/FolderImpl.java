/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.folder.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.impl.GroupImpl;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderPackage;
import dk.dtu.imm.red.core.usecasepoint.RequirementFactor;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointFactory; 
import org.eclipse.emf.common.notify.Notification;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Folder</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.folder.impl.FolderImpl#getSpecialType <em>Special Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FolderImpl extends GroupImpl implements Folder {
	/**
	 * The default value of the '{@link #getSpecialType() <em>Special Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialType()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIAL_TYPE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getSpecialType() <em>Special Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialType()
	 * @generated
	 * @ordered
	 */
	protected String specialType = SPECIAL_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected FolderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FolderPackage.Literals.FOLDER;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecialType() {
		return specialType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialType(String newSpecialType) {
		String oldSpecialType = specialType;
		specialType = newSpecialType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FolderPackage.FOLDER__SPECIAL_TYPE, oldSpecialType, specialType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double computeRequirementsFactor(boolean recursive) {  
		Map<String, List<Element>> elementMap = new HashMap<String, List<Element>>(); 
		getEClassOfSpecialTypeRec(elementMap, this, recursive); 
		
		RequirementFactor rf = UsecasepointFactory.eINSTANCE.createRequirementFactor();
		return rf.getSum(elementMap);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private void getEClassOfSpecialTypeRec(Map<String, List<Element>> elementMap,  Folder rootFolder, boolean recursive) { 
		
		for(Object o : rootFolder.getContents().toArray()) {  
			
			String className = o.getClass().getName();
			if(elementMap.get(className) == null) elementMap.put(className, new ArrayList<Element>());
			
			elementMap.get(className).add((Element) o); 
			
			if(recursive && o instanceof Folder) {
				getEClassOfSpecialTypeRec(elementMap, (Folder) o, recursive);
			}
		} 
	}  

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FolderPackage.FOLDER__SPECIAL_TYPE:
				return getSpecialType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FolderPackage.FOLDER__SPECIAL_TYPE:
				setSpecialType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FolderPackage.FOLDER__SPECIAL_TYPE:
				setSpecialType(SPECIAL_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FolderPackage.FOLDER__SPECIAL_TYPE:
				return SPECIAL_TYPE_EDEFAULT == null ? specialType != null : !SPECIAL_TYPE_EDEFAULT.equals(specialType);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (specialType: ");
		result.append(specialType);
		result.append(')');
		return result.toString();
	}

	@Override
	public String getIconURI() {
		return "/icons/folder.png";
	}

} // FolderImpl
