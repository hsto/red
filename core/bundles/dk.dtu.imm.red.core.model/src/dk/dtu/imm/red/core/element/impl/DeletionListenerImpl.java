/**
 */
package dk.dtu.imm.red.core.element.impl;

import dk.dtu.imm.red.core.element.DeletionListener;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deletion Listener</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.impl.DeletionListenerImpl#getHostingObject <em>Hosting Object</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.impl.DeletionListenerImpl#getHostingFeature <em>Hosting Feature</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.impl.DeletionListenerImpl#getHandledObject <em>Handled Object</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeletionListenerImpl extends EObjectImpl implements DeletionListener {
	/**
	 * The cached value of the '{@link #getHostingObject() <em>Hosting Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHostingObject()
	 * @generated
	 * @ordered
	 */
	protected EObject hostingObject;
	/**
	 * The cached value of the '{@link #getHostingFeature() <em>Hosting Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHostingFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature hostingFeature;
	/**
	 * The cached value of the '{@link #getHandledObject() <em>Handled Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandledObject()
	 * @generated
	 * @ordered
	 */
	protected EObject handledObject;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeletionListenerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ElementPackage.Literals.DELETION_LISTENER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getHostingObject() {
		if (hostingObject != null && hostingObject.eIsProxy()) {
			InternalEObject oldHostingObject = (InternalEObject)hostingObject;
			hostingObject = eResolveProxy(oldHostingObject);
			if (hostingObject != oldHostingObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.DELETION_LISTENER__HOSTING_OBJECT, oldHostingObject, hostingObject));
			}
		}
		return hostingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetHostingObject() {
		return hostingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHostingObject(EObject newHostingObject) {
		EObject oldHostingObject = hostingObject;
		hostingObject = newHostingObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.DELETION_LISTENER__HOSTING_OBJECT, oldHostingObject, hostingObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getHostingFeature() {
		if (hostingFeature != null && hostingFeature.eIsProxy()) {
			InternalEObject oldHostingFeature = (InternalEObject)hostingFeature;
			hostingFeature = (EStructuralFeature)eResolveProxy(oldHostingFeature);
			if (hostingFeature != oldHostingFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.DELETION_LISTENER__HOSTING_FEATURE, oldHostingFeature, hostingFeature));
			}
		}
		return hostingFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetHostingFeature() {
		return hostingFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHostingFeature(EStructuralFeature newHostingFeature) {
		EStructuralFeature oldHostingFeature = hostingFeature;
		hostingFeature = newHostingFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.DELETION_LISTENER__HOSTING_FEATURE, oldHostingFeature, hostingFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getHandledObject() {
		if (handledObject != null && handledObject.eIsProxy()) {
			InternalEObject oldHandledObject = (InternalEObject)handledObject;
			handledObject = eResolveProxy(oldHandledObject);
			if (handledObject != oldHandledObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ElementPackage.DELETION_LISTENER__HANDLED_OBJECT, oldHandledObject, handledObject));
			}
		}
		return handledObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetHandledObject() {
		return handledObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHandledObject(EObject newHandledObject) {
		EObject oldHandledObject = handledObject;
		handledObject = newHandledObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ElementPackage.DELETION_LISTENER__HANDLED_OBJECT, oldHandledObject, handledObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void notifyOfDeletion() {
		Object feature = hostingObject.eGet(getHostingFeature());
		
		if (feature instanceof EList) {
			EList<?> featureList = (EList<?>) feature;
			featureList.remove(handledObject);
		}
		else {
			hostingObject.eSet(getHostingFeature(), null);
		}
		
		if (hostingObject instanceof Element) {
			((Element)hostingObject).save();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void notifyOfUndo() {
		Object feature = hostingObject.eGet(getHostingFeature());
		
		if (feature instanceof EList<?>) {
			EList<EObject> featureList = (EList<EObject>) feature;
			featureList.add(handledObject);
		}
		else if (feature instanceof EObject) {
			hostingObject.eSet(getHostingFeature(), null);
		}
		
		if (hostingObject instanceof Element) {
			((Element)hostingObject).save();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ElementPackage.DELETION_LISTENER__HOSTING_OBJECT:
				if (resolve) return getHostingObject();
				return basicGetHostingObject();
			case ElementPackage.DELETION_LISTENER__HOSTING_FEATURE:
				if (resolve) return getHostingFeature();
				return basicGetHostingFeature();
			case ElementPackage.DELETION_LISTENER__HANDLED_OBJECT:
				if (resolve) return getHandledObject();
				return basicGetHandledObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ElementPackage.DELETION_LISTENER__HOSTING_OBJECT:
				setHostingObject((EObject)newValue);
				return;
			case ElementPackage.DELETION_LISTENER__HOSTING_FEATURE:
				setHostingFeature((EStructuralFeature)newValue);
				return;
			case ElementPackage.DELETION_LISTENER__HANDLED_OBJECT:
				setHandledObject((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ElementPackage.DELETION_LISTENER__HOSTING_OBJECT:
				setHostingObject((EObject)null);
				return;
			case ElementPackage.DELETION_LISTENER__HOSTING_FEATURE:
				setHostingFeature((EStructuralFeature)null);
				return;
			case ElementPackage.DELETION_LISTENER__HANDLED_OBJECT:
				setHandledObject((EObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ElementPackage.DELETION_LISTENER__HOSTING_OBJECT:
				return hostingObject != null;
			case ElementPackage.DELETION_LISTENER__HOSTING_FEATURE:
				return hostingFeature != null;
			case ElementPackage.DELETION_LISTENER__HANDLED_OBJECT:
				return handledObject != null;
		}
		return super.eIsSet(featureID);
	}
} //DeletionListenerImpl
