/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.text;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Text</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.text.Text#getFragments <em>Fragments</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.text.TextPackage#getText()
 * @model
 * @generated
 */
public interface Text extends EObject {
	/**
	 * Returns the value of the '<em><b>Fragments</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.text.TextFragment}.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.core.text.TextFragment#getContainedText <em>Contained Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fragments</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fragments</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.text.TextPackage#getText_Fragments()
	 * @see dk.dtu.imm.red.core.text.TextFragment#getContainedText
	 * @model opposite="containedText" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<TextFragment> getFragments();

	public abstract String toPlainString();

} // Text
