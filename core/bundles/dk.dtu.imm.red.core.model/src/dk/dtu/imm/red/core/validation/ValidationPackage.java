/**
 */
package dk.dtu.imm.red.core.validation;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.validation.ValidationFactory
 * @model kind="package"
 * @generated
 */
public interface ValidationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "validation";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.core.model.validation";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "validation";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ValidationPackage eINSTANCE = dk.dtu.imm.red.core.validation.impl.ValidationPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.validation.impl.ErrorMessageImpl <em>Error Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.validation.impl.ErrorMessageImpl
	 * @see dk.dtu.imm.red.core.validation.impl.ValidationPackageImpl#getErrorMessage()
	 * @generated
	 */
	int ERROR_MESSAGE = 0;

	/**
	 * The feature id for the '<em><b>Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_MESSAGE__MESSAGE = 0;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_MESSAGE__SEVERITY = 1;

	/**
	 * The feature id for the '<em><b>Element Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_MESSAGE__ELEMENT_REFERENCE = 2;

	/**
	 * The number of structural features of the '<em>Error Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_MESSAGE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.validation.Severity <em>Severity</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.validation.Severity
	 * @see dk.dtu.imm.red.core.validation.impl.ValidationPackageImpl#getSeverity()
	 * @generated
	 */
	int SEVERITY = 1;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.validation.ErrorMessage <em>Error Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Error Message</em>'.
	 * @see dk.dtu.imm.red.core.validation.ErrorMessage
	 * @generated
	 */
	EClass getErrorMessage();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.validation.ErrorMessage#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message</em>'.
	 * @see dk.dtu.imm.red.core.validation.ErrorMessage#getMessage()
	 * @see #getErrorMessage()
	 * @generated
	 */
	EAttribute getErrorMessage_Message();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.validation.ErrorMessage#getSeverity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Severity</em>'.
	 * @see dk.dtu.imm.red.core.validation.ErrorMessage#getSeverity()
	 * @see #getErrorMessage()
	 * @generated
	 */
	EAttribute getErrorMessage_Severity();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.core.validation.ErrorMessage#getElementReference <em>Element Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Element Reference</em>'.
	 * @see dk.dtu.imm.red.core.validation.ErrorMessage#getElementReference()
	 * @see #getErrorMessage()
	 * @generated
	 */
	EReference getErrorMessage_ElementReference();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.validation.Severity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Severity</em>'.
	 * @see dk.dtu.imm.red.core.validation.Severity
	 * @generated
	 */
	EEnum getSeverity();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ValidationFactory getValidationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.validation.impl.ErrorMessageImpl <em>Error Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.validation.impl.ErrorMessageImpl
		 * @see dk.dtu.imm.red.core.validation.impl.ValidationPackageImpl#getErrorMessage()
		 * @generated
		 */
		EClass ERROR_MESSAGE = eINSTANCE.getErrorMessage();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ERROR_MESSAGE__MESSAGE = eINSTANCE.getErrorMessage_Message();

		/**
		 * The meta object literal for the '<em><b>Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ERROR_MESSAGE__SEVERITY = eINSTANCE.getErrorMessage_Severity();

		/**
		 * The meta object literal for the '<em><b>Element Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ERROR_MESSAGE__ELEMENT_REFERENCE = eINSTANCE.getErrorMessage_ElementReference();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.validation.Severity <em>Severity</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.validation.Severity
		 * @see dk.dtu.imm.red.core.validation.impl.ValidationPackageImpl#getSeverity()
		 * @generated
		 */
		EEnum SEVERITY = eINSTANCE.getSeverity();

	}

} //ValidationPackage
