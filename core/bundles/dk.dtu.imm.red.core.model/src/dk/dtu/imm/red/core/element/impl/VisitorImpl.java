/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.Visitor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class VisitorImpl extends EObjectImpl implements Visitor {
	
	protected StringBuffer buf = new StringBuffer();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisitorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ElementPackage.Literals.VISITOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void visit(Object object) {
		try {
			for (Method method : this.getClass().getMethods()) {
				if ("visit".equals(method.getName())) {
					if (method.getParameterTypes()[0].isAssignableFrom(object.getClass()) && method.getParameterTypes()[0] != Object.class) {
						method.invoke(this, new Object[] {object});
						break;
					}
				}
			}
			
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void defaultVisit(Object object) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getOutput() {
		return buf.toString();
	}

} //VisitorImpl
