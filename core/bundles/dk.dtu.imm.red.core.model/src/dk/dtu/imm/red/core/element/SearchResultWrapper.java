/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Search Result Wrapper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.SearchResultWrapper#getElementResults <em>Element Results</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.SearchResultWrapper#getElement <em>Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.SearchResultWrapper#getChildResults <em>Child Results</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchResultWrapper()
 * @model
 * @generated
 */
public interface SearchResultWrapper extends EObject {
	/**
	 * Returns the value of the '<em><b>Element Results</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.element.SearchResult}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Results</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Results</em>' reference list.
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchResultWrapper_ElementResults()
	 * @model
	 * @generated
	 */
	EList<SearchResult> getElementResults();

	/**
	 * Returns the value of the '<em><b>Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element</em>' reference.
	 * @see #setElement(Element)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchResultWrapper_Element()
	 * @model
	 * @generated
	 */
	Element getElement();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.SearchResultWrapper#getElement <em>Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element</em>' reference.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(Element value);

	/**
	 * Returns the value of the '<em><b>Child Results</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.element.SearchResultWrapper}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Results</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Results</em>' reference list.
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchResultWrapper_ChildResults()
	 * @model
	 * @generated
	 */
	EList<SearchResultWrapper> getChildResults();

} // SearchResultWrapper
