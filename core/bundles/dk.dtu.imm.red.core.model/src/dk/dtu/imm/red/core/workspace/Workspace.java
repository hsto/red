/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.workspace;

import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Workspace</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.workspace.Workspace#getRecentlyOpenedFiles <em>Recently Opened Files</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.workspace.Workspace#getCurrentlyOpenedFiles <em>Currently Opened Files</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.workspace.Workspace#getRecentFilesMaxLength <em>Recent Files Max Length</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.workspace.Workspace#getPassword <em>Password</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.workspace.Workspace#getCurrentlyOpenedProjects <em>Currently Opened Projects</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.workspace.WorkspacePackage#getWorkspace()
 * @model
 * @generated
 */
public interface Workspace extends Group {

	/**
	 * Returns the value of the '<em><b>Recently Opened Files</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.file.File}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recently Opened Files</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recently Opened Files</em>' reference list.
	 * @see dk.dtu.imm.red.core.workspace.WorkspacePackage#getWorkspace_RecentlyOpenedFiles()
	 * @model
	 * @generated
	 */
	EList<File> getRecentlyOpenedFiles();
	EList<Project> getRecentlyOpenedProjects();
	/**
	 * Returns the value of the '<em><b>Currently Opened Files</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.file.File}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Currently Opened Files</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Currently Opened Files</em>' reference list.
	 * @see dk.dtu.imm.red.core.workspace.WorkspacePackage#getWorkspace_CurrentlyOpenedFiles()
	 * @model
	 * @generated
	 */
	EList<File> getCurrentlyOpenedFiles();

	/**
	 * Returns the value of the '<em><b>Recent Files Max Length</b></em>' attribute.
	 * The default value is <code>"5"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recent Files Max Length</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recent Files Max Length</em>' attribute.
	 * @see dk.dtu.imm.red.core.workspace.WorkspacePackage#getWorkspace_RecentFilesMaxLength()
	 * @model default="5" changeable="false"
	 * @generated
	 */
	int getRecentFilesMaxLength();

	/**
	 * Returns the value of the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Password</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Password</em>' attribute.
	 * @see #setPassword(String)
	 * @see dk.dtu.imm.red.core.workspace.WorkspacePackage#getWorkspace_Password()
	 * @model
	 * @generated
	 */
	String getPassword();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.workspace.Workspace#getPassword <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Password</em>' attribute.
	 * @see #getPassword()
	 * @generated
	 */
	void setPassword(String value);

    /**
	 * Returns the value of the '<em><b>Currently Opened Projects</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.project.Project}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Currently Opened Projects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Currently Opened Projects</em>' reference list.
	 * @see dk.dtu.imm.red.core.workspace.WorkspacePackage#getWorkspace_CurrentlyOpenedProjects()
	 * @model
	 * @generated
	 */
	EList<Project> getCurrentlyOpenedProjects();

				void addOpenedWorkspace(Workspace workspace);
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='int index = -1;\r\nfor (int i = 0; i  < getCurrentlyOpenedFiles().size(); i++) {\r\n\tFile f = getCurrentlyOpenedFiles().get(i);\r\n\tif (f.getUri().equals(file.getUri())) {\r\n\t\tindex = i;\r\n\t}\r\n}\r\n\r\nif (index != -1) {\r\n\tgetCurrentlyOpenedFiles().remove(index);\r\n}\r\n\r\ngetCurrentlyOpenedFiles().add(file);\r\n\r\n// If the file is already in the recent-files list, \r\n// remove it and re-add it to the top\r\nindex = -1;\r\nfor (int i = 0; i < getRecentlyOpenedFiles().size(); i++) {\r\n\tFile f = getRecentlyOpenedFiles().get(i);\r\n\tif (f.getUniqueID().equals(file.getUniqueID())) {\r\n\t\tindex = i;\r\n\t}\r\n}\r\n\r\nif (index != -1) {\r\n\tgetRecentlyOpenedFiles().remove(index);\r\n}\r\n\r\ngetRecentlyOpenedFiles().add(0, file);\r\n\r\nfile.setParent(this);\r\n\r\nwhile (getRecentlyOpenedFiles().size() > recentFilesMaxLength) {\r\n\tgetRecentlyOpenedFiles().remove(recentFilesMaxLength);\r\n}'"
	 * @generated
	 */
	void addOpenedFile(File file);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */	
	void addOpenedProject(Project project);

	
} // Workspace
