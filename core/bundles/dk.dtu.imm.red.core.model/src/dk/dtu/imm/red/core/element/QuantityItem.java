/**
 */
package dk.dtu.imm.red.core.element;

import dk.dtu.imm.red.core.element.unit.Unit;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quantity Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.QuantityItem#getLabel <em>Label</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.QuantityItem#getValue <em>Value</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.QuantityItem#getUnit <em>Unit</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.ElementPackage#getQuantityItem()
 * @model
 * @generated
 */
public interface QuantityItem extends EObject {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.element.QuantityLabel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.QuantityLabel
	 * @see #setLabel(QuantityLabel)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getQuantityItem_Label()
	 * @model
	 * @generated
	 */
	QuantityLabel getLabel();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.QuantityItem#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see dk.dtu.imm.red.core.element.QuantityLabel
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(QuantityLabel value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(BigDecimal)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getQuantityItem_Value()
	 * @model
	 * @generated
	 */
	BigDecimal getValue();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.QuantityItem#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' containment reference.
	 * @see #setUnit(Unit)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getQuantityItem_Unit()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Unit getUnit();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.QuantityItem#getUnit <em>Unit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' containment reference.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(Unit value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Unit> getValidUnits();

} // QuantityItem
