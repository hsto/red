/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.relationship.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.element.relationship.RelationshipPackage;
import dk.dtu.imm.red.core.text.CrossReference;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Element Relationship</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.relationship.impl.ElementRelationshipImpl#getFromElement <em>From Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.relationship.impl.ElementRelationshipImpl#getToElement <em>To Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.relationship.impl.ElementRelationshipImpl#getRelevance <em>Relevance</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.relationship.impl.ElementRelationshipImpl#getReferencingTexts <em>Referencing Texts</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ElementRelationshipImpl extends ElementImpl implements
		ElementRelationship {
	/**
	 * The cached value of the '{@link #getToElement() <em>To Element</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getToElement()
	 * @generated
	 * @ordered
	 */
	protected Element toElement;

	/**
	 * The default value of the '{@link #getRelevance() <em>Relevance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelevance()
	 * @generated
	 * @ordered
	 */
	protected static final String RELEVANCE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getRelevance() <em>Relevance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelevance()
	 * @generated
	 * @ordered
	 */
	protected String relevance = RELEVANCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReferencingTexts() <em>Referencing Texts</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencingTexts()
	 * @generated
	 * @ordered
	 */
	protected EList<CrossReference> referencingTexts;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementRelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RelationshipPackage.Literals.ELEMENT_RELATIONSHIP;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Element getFromElement() {
		if (eContainerFeatureID() != RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT) return null;
		return (Element)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element basicGetFromElement() {
		if (eContainerFeatureID() != RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT) return null;
		return (Element)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFromElement(Element newFromElement,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFromElement, RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFromElement(Element newFromElement) {
		if (newFromElement != eInternalContainer() || (eContainerFeatureID() != RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT && newFromElement != null)) {
			if (EcoreUtil.isAncestor(this, newFromElement))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFromElement != null)
				msgs = ((InternalEObject)newFromElement).eInverseAdd(this, ElementPackage.ELEMENT__RELATES_TO, Element.class, msgs);
			msgs = basicSetFromElement(newFromElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT, newFromElement, newFromElement));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Element getToElement() {
		if (toElement != null && toElement.eIsProxy()) {
			InternalEObject oldToElement = (InternalEObject)toElement;
			toElement = (Element)eResolveProxy(oldToElement);
			if (toElement != oldToElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT, oldToElement, toElement));
			}
		}
		return toElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Element basicGetToElement() {
		return toElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetToElement(Element newToElement,
			NotificationChain msgs) {
		Element oldToElement = toElement;
		toElement = newToElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT, oldToElement, newToElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setToElement(Element newToElement) {
		if (newToElement != toElement) {
			NotificationChain msgs = null;
			if (toElement != null)
				msgs = ((InternalEObject)toElement).eInverseRemove(this, ElementPackage.ELEMENT__RELATED_BY, Element.class, msgs);
			if (newToElement != null)
				msgs = ((InternalEObject)newToElement).eInverseAdd(this, ElementPackage.ELEMENT__RELATED_BY, Element.class, msgs);
			msgs = basicSetToElement(newToElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT, newToElement, newToElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRelevance() {
		return relevance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelevance(String newRelevance) {
		String oldRelevance = relevance;
		relevance = newRelevance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RelationshipPackage.ELEMENT_RELATIONSHIP__RELEVANCE, oldRelevance, relevance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CrossReference> getReferencingTexts() {
		if (referencingTexts == null) {
			referencingTexts = new EObjectResolvingEList<CrossReference>(CrossReference.class, this, RelationshipPackage.ELEMENT_RELATIONSHIP__REFERENCING_TEXTS);
		}
		return referencingTexts;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFromElement((Element)otherEnd, msgs);
			case RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT:
				if (toElement != null)
					msgs = ((InternalEObject)toElement).eInverseRemove(this, ElementPackage.ELEMENT__RELATED_BY, Element.class, msgs);
				return basicSetToElement((Element)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT:
				return basicSetFromElement(null, msgs);
			case RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT:
				return basicSetToElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT:
				return eInternalContainer().eInverseRemove(this, ElementPackage.ELEMENT__RELATES_TO, Element.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT:
				if (resolve) return getFromElement();
				return basicGetFromElement();
			case RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT:
				if (resolve) return getToElement();
				return basicGetToElement();
			case RelationshipPackage.ELEMENT_RELATIONSHIP__RELEVANCE:
				return getRelevance();
			case RelationshipPackage.ELEMENT_RELATIONSHIP__REFERENCING_TEXTS:
				return getReferencingTexts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT:
				setFromElement((Element)newValue);
				return;
			case RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT:
				setToElement((Element)newValue);
				return;
			case RelationshipPackage.ELEMENT_RELATIONSHIP__RELEVANCE:
				setRelevance((String)newValue);
				return;
			case RelationshipPackage.ELEMENT_RELATIONSHIP__REFERENCING_TEXTS:
				getReferencingTexts().clear();
				getReferencingTexts().addAll((Collection<? extends CrossReference>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT:
				setFromElement((Element)null);
				return;
			case RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT:
				setToElement((Element)null);
				return;
			case RelationshipPackage.ELEMENT_RELATIONSHIP__RELEVANCE:
				setRelevance(RELEVANCE_EDEFAULT);
				return;
			case RelationshipPackage.ELEMENT_RELATIONSHIP__REFERENCING_TEXTS:
				getReferencingTexts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RelationshipPackage.ELEMENT_RELATIONSHIP__FROM_ELEMENT:
				return basicGetFromElement() != null;
			case RelationshipPackage.ELEMENT_RELATIONSHIP__TO_ELEMENT:
				return toElement != null;
			case RelationshipPackage.ELEMENT_RELATIONSHIP__RELEVANCE:
				return RELEVANCE_EDEFAULT == null ? relevance != null : !RELEVANCE_EDEFAULT.equals(relevance);
			case RelationshipPackage.ELEMENT_RELATIONSHIP__REFERENCING_TEXTS:
				return referencingTexts != null && !referencingTexts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void delete() {
		if (toElement != null && toElement.getRelatedBy() != null) {
			this.toElement.getRelatedBy().remove(this);
		}
		super.delete();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (relevance: ");
		result.append(relevance);
		result.append(')');
		return result.toString();
	}

} // ElementRelationshipImpl
