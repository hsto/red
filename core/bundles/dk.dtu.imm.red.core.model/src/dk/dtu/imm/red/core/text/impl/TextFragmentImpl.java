/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.text.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.text.TextFragment;
import dk.dtu.imm.red.core.text.TextPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Fragment</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.text.impl.TextFragmentImpl#getContainedText <em>Contained Text</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TextFragmentImpl extends EObjectImpl implements
		TextFragment {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TextFragmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TextPackage.Literals.TEXT_FRAGMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getContainedText() {
		if (eContainerFeatureID() != TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT) return null;
		return (Text)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetContainedText() {
		if (eContainerFeatureID() != TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT) return null;
		return (Text)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainedText(Text newContainedText, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContainedText, TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainedText(Text newContainedText) {
		if (newContainedText != eInternalContainer() || (eContainerFeatureID() != TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT && newContainedText != null)) {
			if (EcoreUtil.isAncestor(this, newContainedText))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainedText != null)
				msgs = ((InternalEObject)newContainedText).eInverseAdd(this, TextPackage.TEXT__FRAGMENTS, Text.class, msgs);
			msgs = basicSetContainedText(newContainedText, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT, newContainedText, newContainedText));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainedText((Text)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT:
				return basicSetContainedText(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT:
				return eInternalContainer().eInverseRemove(this, TextPackage.TEXT__FRAGMENTS, Text.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT:
				if (resolve) return getContainedText();
				return basicGetContainedText();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT:
				setContainedText((Text)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT:
				setContainedText((Text)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TextPackage.TEXT_FRAGMENT__CONTAINED_TEXT:
				return basicGetContainedText() != null;
		}
		return super.eIsSet(featureID);
	}

} // TextFragmentImpl
