/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.relationship;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage
 * @generated
 */
public interface RelationshipFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	RelationshipFactory eINSTANCE = dk.dtu.imm.red.core.element.relationship.impl.RelationshipFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Element Reference</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Element Reference</em>'.
	 * @generated
	 */
	ElementReference createElementReference();

	/**
	 * Returns a new object of class '<em>Glossary Entry Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Glossary Entry Reference</em>'.
	 * @generated
	 */
	GlossaryEntryReference createGlossaryEntryReference();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	RelationshipPackage getRelationshipPackage();

} // RelationshipFactory
