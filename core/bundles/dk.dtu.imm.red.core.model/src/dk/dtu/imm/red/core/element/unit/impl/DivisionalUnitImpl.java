/**
 */
package dk.dtu.imm.red.core.element.unit.impl;

import dk.dtu.imm.red.core.element.unit.DivisionalUnit;
import dk.dtu.imm.red.core.element.unit.Unit;
import dk.dtu.imm.red.core.element.unit.UnitPackage;
import dk.dtu.imm.red.core.element.unit.impl.DivisionalUnitImpl;
import dk.dtu.imm.red.core.element.unit.impl.UnitImpl;

import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Divisional Unit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.unit.impl.DivisionalUnitImpl#getNominatorUnit <em>Nominator Unit</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.unit.impl.DivisionalUnitImpl#getDenominatorUnit <em>Denominator Unit</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DivisionalUnitImpl extends UnitImpl implements DivisionalUnit {
	/**
	 * The cached value of the '{@link #getNominatorUnit() <em>Nominator Unit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNominatorUnit()
	 * @generated
	 * @ordered
	 */
	protected Unit nominatorUnit;

	/**
	 * The cached value of the '{@link #getDenominatorUnit() <em>Denominator Unit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDenominatorUnit()
	 * @generated
	 * @ordered
	 */
	protected Unit denominatorUnit;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DivisionalUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.DIVISIONAL_UNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Unit getNominatorUnit() {
		if (nominatorUnit != null && nominatorUnit.eIsProxy()) {
			InternalEObject oldNominatorUnit = (InternalEObject)nominatorUnit;
			nominatorUnit = (Unit)eResolveProxy(oldNominatorUnit);
			if (nominatorUnit != oldNominatorUnit) {
				InternalEObject newNominatorUnit = (InternalEObject)nominatorUnit;
				NotificationChain msgs = oldNominatorUnit.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT, null, null);
				if (newNominatorUnit.eInternalContainer() == null) {
					msgs = newNominatorUnit.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT, oldNominatorUnit, nominatorUnit));
			}
		}
		return EcoreUtil.copy(nominatorUnit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unit basicGetNominatorUnit() {
		return nominatorUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNominatorUnit(Unit newNominatorUnit, NotificationChain msgs) {
		Unit oldNominatorUnit = nominatorUnit;
		nominatorUnit = newNominatorUnit;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT, oldNominatorUnit, newNominatorUnit);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNominatorUnit(Unit newNominatorUnit) {
		if (newNominatorUnit != nominatorUnit) {
			NotificationChain msgs = null;
			if (nominatorUnit != null)
				msgs = ((InternalEObject)nominatorUnit).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT, null, msgs);
			if (newNominatorUnit != null)
				msgs = ((InternalEObject)newNominatorUnit).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT, null, msgs);
			msgs = basicSetNominatorUnit(newNominatorUnit, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT, newNominatorUnit, newNominatorUnit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Unit getDenominatorUnit() {
		if (denominatorUnit != null && denominatorUnit.eIsProxy()) {
			InternalEObject oldDenominatorUnit = (InternalEObject)denominatorUnit;
			denominatorUnit = (Unit)eResolveProxy(oldDenominatorUnit);
			if (denominatorUnit != oldDenominatorUnit) {
				InternalEObject newDenominatorUnit = (InternalEObject)denominatorUnit;
				NotificationChain msgs = oldDenominatorUnit.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT, null, null);
				if (newDenominatorUnit.eInternalContainer() == null) {
					msgs = newDenominatorUnit.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT, oldDenominatorUnit, denominatorUnit));
			}
		}
		return EcoreUtil.copy(denominatorUnit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unit basicGetDenominatorUnit() {
		return denominatorUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDenominatorUnit(Unit newDenominatorUnit, NotificationChain msgs) {
		Unit oldDenominatorUnit = denominatorUnit;
		denominatorUnit = newDenominatorUnit;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT, oldDenominatorUnit, newDenominatorUnit);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDenominatorUnit(Unit newDenominatorUnit) {
		if (newDenominatorUnit != denominatorUnit) {
			NotificationChain msgs = null;
			if (denominatorUnit != null)
				msgs = ((InternalEObject)denominatorUnit).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT, null, msgs);
			if (newDenominatorUnit != null)
				msgs = ((InternalEObject)newDenominatorUnit).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT, null, msgs);
			msgs = basicSetDenominatorUnit(newDenominatorUnit, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT, newDenominatorUnit, newDenominatorUnit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT:
				return basicSetNominatorUnit(null, msgs);
			case UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT:
				return basicSetDenominatorUnit(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT:
				if (resolve) return getNominatorUnit();
				return basicGetNominatorUnit();
			case UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT:
				if (resolve) return getDenominatorUnit();
				return basicGetDenominatorUnit();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT:
				setNominatorUnit((Unit)newValue);
				return;
			case UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT:
				setDenominatorUnit((Unit)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT:
				setNominatorUnit((Unit)null);
				return;
			case UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT:
				setDenominatorUnit((Unit)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UnitPackage.DIVISIONAL_UNIT__NOMINATOR_UNIT:
				return nominatorUnit != null;
			case UnitPackage.DIVISIONAL_UNIT__DENOMINATOR_UNIT:
				return denominatorUnit != null;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public EList<Unit> getSiblingUnits() {
		BasicEList<Unit> unitList = new BasicEList<Unit>(
				getNominatorUnit().getSiblingUnits().size() * getDenominatorUnit().getSiblingUnits().size());

		for (Unit nomUnit : getNominatorUnit().getSiblingUnits()) {
			for (Unit denUnit : getDenominatorUnit().getSiblingUnits()) {
				DivisionalUnitImpl newUnit = new DivisionalUnitImpl();
				newUnit.basicSetNominatorUnit(nomUnit, null);
				newUnit.basicSetDenominatorUnit(denUnit, null);
				unitList.add(newUnit);
			}
		}

		return unitList;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public BigDecimal getConversionFactor() {
		BigDecimal nomConvFactor = ((UnitImpl) getNominatorUnit()).getConversionFactor();
		BigDecimal denConvFactor = ((UnitImpl) getDenominatorUnit()).getConversionFactor();
		
		int scale = getCalculationScale(nomConvFactor.scale(), denConvFactor.scale());

		return nomConvFactor.divide(denConvFactor, scale, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean isCompatibleWith(Unit that) {
		if (super.isCompatibleWith(that)) {
			DivisionalUnit thatDiv = (DivisionalUnit) that;
			if (this.getNominatorUnit().isCompatibleWith(thatDiv.getNominatorUnit())
					&& this.getDenominatorUnit().isCompatibleWith(thatDiv.getDenominatorUnit())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String getSymbol() {
		return getNominatorUnit().getSymbol() + "/" + getDenominatorUnit().getSymbol();
	}

} //DivisionalUnitImpl
