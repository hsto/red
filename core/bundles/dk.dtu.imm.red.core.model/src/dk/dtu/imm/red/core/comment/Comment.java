/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.comment;

import dk.dtu.imm.red.core.element.Element;
import java.util.Date;

import org.eclipse.emf.ecore.EObject;

import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.user.User;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Comment</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.comment.Comment#getCategory <em>Category</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.comment.Comment#getText <em>Text</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.comment.Comment#getAuthor <em>Author</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.comment.CommentPackage#getComment()
 * @model
 * @generated
 */
public interface Comment extends Element {
	/**
	 * Returns the value of the '<em><b>Category</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.comment.CommentCategory}.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Category</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' attribute.
	 * @see dk.dtu.imm.red.core.comment.CommentCategory
	 * @see #setCategory(CommentCategory)
	 * @see dk.dtu.imm.red.core.comment.CommentPackage#getComment_Category()
	 * @model
	 * @generated
	 */
	CommentCategory getCategory();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.comment.Comment#getCategory <em>Category</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' attribute.
	 * @see dk.dtu.imm.red.core.comment.CommentCategory
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(CommentCategory value);

	/**
	 * Returns the value of the '<em><b>Text</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text</em>' reference isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Text</em>' reference.
	 * @see #setText(Text)
	 * @see dk.dtu.imm.red.core.comment.CommentPackage#getComment_Text()
	 * @model required="true"
	 * @generated
	 */
	Text getText();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.comment.Comment#getText <em>Text</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' containment reference.
	 * @see #getText()
	 * @generated
	 */
	void setText(Text value);

	/**
	 * Returns the value of the '<em><b>Author</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Author</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Author</em>' reference.
	 * @see #setAuthor(User)
	 * @see dk.dtu.imm.red.core.comment.CommentPackage#getComment_Author()
	 * @model required="true"
	 * @generated
	 */
	User getAuthor();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.comment.Comment#getAuthor <em>Author</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Author</em>' containment reference.
	 * @see #getAuthor()
	 * @generated
	 */
	void setAuthor(User value);

} // Comment
