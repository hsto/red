/**
 */
package dk.dtu.imm.red.core.comment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Issue Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.comment.IssueComment#getSeverity <em>Severity</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.comment.CommentPackage#getIssueComment()
 * @model
 * @generated
 */
public interface IssueComment extends Comment {
	/**
	 * Returns the value of the '<em><b>Severity</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.comment.IssueSeverity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Severity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Severity</em>' attribute.
	 * @see dk.dtu.imm.red.core.comment.IssueSeverity
	 * @see #setSeverity(IssueSeverity)
	 * @see dk.dtu.imm.red.core.comment.CommentPackage#getIssueComment_Severity()
	 * @model
	 * @generated
	 */
	IssueSeverity getSeverity();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.comment.IssueComment#getSeverity <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Severity</em>' attribute.
	 * @see dk.dtu.imm.red.core.comment.IssueSeverity
	 * @see #getSeverity()
	 * @generated
	 */
	void setSeverity(IssueSeverity value);

} // IssueComment
