package dk.dtu.imm.red.core.element.impl; 

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EReference;

public class ReflColumnDefinition {
	
	public enum ColumnBehaviour {
		CellEdit,
		ComboSelector,
		ReferenceSelector,
		IconDisplay
	}
	
	private String columnName = "Item";
	private Class<?> columnType = String.class;
	private ColumnBehaviour columnBehaviour = ColumnBehaviour.CellEdit;  

	private Integer columnWidth = 100;
	private boolean editable = true;   
	private String[] getterMethod = null;
	private String[] cellItems = new String[] {};  
	private boolean handleCellEditManually = false;  
	private CellHandler<?> cellHandler = null;
	private String attributeName; 

	public static String[] enumToStringArray(@SuppressWarnings("rawtypes") Enum[] values) {
	    
	    String[] names = new String[values.length];

	    for (int i = 0; i < values.length; i++) {
	        names[i] = values[i].name();
	    }

	    return names;
	}

	public CellHandler<?> getCellHandler() {
		return cellHandler;
	}

	public ReflColumnDefinition setCellHandler(CellHandler<?> cellHandler) {
		this.cellHandler = cellHandler;
		return this;
	}

	public String getColumnName() {
		return columnName;
	}
	
	public ReflColumnDefinition setHeaderName(String columnName) {
		this.columnName = columnName;
		return this;
	}
	public Class<?> getColumnType() {
		return columnType;
	}
	
	public ReflColumnDefinition setColumnType(Class<?> columnType) {
		this.columnType = columnType;
		return this;
	}
	
	public Integer getColumnWidth() {
		return columnWidth;
	}
	
	public ReflColumnDefinition setColumnWidth(Integer columnWidth) {
		this.columnWidth = columnWidth;
		return this;
	} 
	
	public boolean isEditable() {
		return editable;
	}

	public ReflColumnDefinition setEditable(boolean editable) {
		this.editable = editable;
		return this;
	}  

	public String[] getGetterMethod() {
		return getterMethod;
	}

	public ReflColumnDefinition setGetterMethod(String[] getterMethod) {
		this.getterMethod = getterMethod;
		return this;
	}
	
	public String[] getCellItems() {
		return cellItems;
	}

	public ReflColumnDefinition setCellItems(String[] cellItems) {
		this.cellItems = cellItems;
		return this;
	}
	
	public boolean isHandleCellEditManually() {
		return handleCellEditManually;
	}

	public ReflColumnDefinition setHandleCellEditManually(boolean handleCellEditManually) {
		this.handleCellEditManually = handleCellEditManually;
		return this;
	}
	
	public ColumnBehaviour getColumnBehaviour() {
		return columnBehaviour;
	}

	public ReflColumnDefinition setColumnBehaviour(ColumnBehaviour columnBehaviour) {
		this.columnBehaviour = columnBehaviour;
		return this;
	}

	public ReflColumnDefinition setEMFLiteral(EAttribute ea) {
		this.attributeName = ea.getName();
		this.columnType = ea.getEAttributeType().getInstanceClass();
		return this;
	}
	
	public ReflColumnDefinition setEMFLiteral(EReference ef) {
		this.attributeName = ef.getName();
		this.columnType = ef.getEType().getInstanceClass();
		return this;
	}
	
	public String getAttributeName() {
		return attributeName;
	}
}
