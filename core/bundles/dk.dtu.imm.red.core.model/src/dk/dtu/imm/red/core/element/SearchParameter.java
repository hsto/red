/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Search Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.SearchParameter#getSearchString <em>Search String</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchParameter()
 * @model
 * @generated
 */
public interface SearchParameter extends EObject {
	/**
	 * Returns the value of the '<em><b>Search String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Search String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Search String</em>' attribute.
	 * @see #setSearchString(String)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchParameter_SearchString()
	 * @model
	 * @generated
	 */
	String getSearchString();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.SearchParameter#getSearchString <em>Search String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Search String</em>' attribute.
	 * @see #getSearchString()
	 * @generated
	 */
	void setSearchString(String value);

} // SearchParameter
