/**
 */
package dk.dtu.imm.red.core.usecasepoint.impl;

import java.util.List;
import java.util.Map;

import dk.dtu.imm.red.core.element.ComplexityType;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.usecasepoint.RequirementFactor;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requirement Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequirementFactorImpl extends FactorImpl implements RequirementFactor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequirementFactorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasepointPackage.Literals.REQUIREMENT_FACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override 
	public double getSum(Map<String, List<Element>> elementMap) {
		double totalSum = 0;
		
		//Sum up each distinct type, and multiply them
		for(List<Element> l : elementMap.values()) {
			double complexitySum = 0;
			for(Element e : l) {
				ComplexityType c = e.getEstimatedComplexity();
				complexitySum += c == ComplexityType.LARGE ? 15 : 
					c == ComplexityType.MEDIUM ? 10 : 
						c == ComplexityType.SMALL ? 5 : 
							0;
			}
			
			if(complexitySum != 0) {
				totalSum += complexitySum;  
			} 
		}
		
		return totalSum; 
	}

} //RequirementFactorImpl
