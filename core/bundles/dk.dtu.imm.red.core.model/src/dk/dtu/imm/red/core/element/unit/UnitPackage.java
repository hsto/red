/**
 */
package dk.dtu.imm.red.core.element.unit;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.element.unit.UnitFactory
 * @model kind="package"
 * @generated
 */
public interface UnitPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "unit";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.core.element.unit";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "unit";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UnitPackage eINSTANCE = dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.unit.impl.UnitImpl <em>Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.unit.impl.UnitImpl
	 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getUnit()
	 * @generated
	 */
	int UNIT = 0;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__SYMBOL = 0;

	/**
	 * The feature id for the '<em><b>Conversion Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__CONVERSION_FACTOR = 1;

	/**
	 * The number of structural features of the '<em>Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.unit.impl.BaseUnitImpl <em>Base Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.unit.impl.BaseUnitImpl
	 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getBaseUnit()
	 * @generated
	 */
	int BASE_UNIT = 1;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_UNIT__SYMBOL = UNIT__SYMBOL;

	/**
	 * The feature id for the '<em><b>Conversion Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_UNIT__CONVERSION_FACTOR = UNIT__CONVERSION_FACTOR;

	/**
	 * The number of structural features of the '<em>Base Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_UNIT_FEATURE_COUNT = UNIT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.unit.impl.TimeUnitImpl <em>Time Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.unit.impl.TimeUnitImpl
	 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getTimeUnit()
	 * @generated
	 */
	int TIME_UNIT = 2;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_UNIT__SYMBOL = UNIT__SYMBOL;

	/**
	 * The feature id for the '<em><b>Conversion Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_UNIT__CONVERSION_FACTOR = UNIT__CONVERSION_FACTOR;

	/**
	 * The number of structural features of the '<em>Time Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_UNIT_FEATURE_COUNT = UNIT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.unit.impl.SizeUnitImpl <em>Size Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.unit.impl.SizeUnitImpl
	 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getSizeUnit()
	 * @generated
	 */
	int SIZE_UNIT = 3;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_UNIT__SYMBOL = UNIT__SYMBOL;

	/**
	 * The feature id for the '<em><b>Conversion Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_UNIT__CONVERSION_FACTOR = UNIT__CONVERSION_FACTOR;

	/**
	 * The number of structural features of the '<em>Size Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_UNIT_FEATURE_COUNT = UNIT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.unit.impl.DivisionalUnitImpl <em>Divisional Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.unit.impl.DivisionalUnitImpl
	 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getDivisionalUnit()
	 * @generated
	 */
	int DIVISIONAL_UNIT = 4;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVISIONAL_UNIT__SYMBOL = UNIT__SYMBOL;

	/**
	 * The feature id for the '<em><b>Conversion Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVISIONAL_UNIT__CONVERSION_FACTOR = UNIT__CONVERSION_FACTOR;

	/**
	 * The feature id for the '<em><b>Nominator Unit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVISIONAL_UNIT__NOMINATOR_UNIT = UNIT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Denominator Unit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVISIONAL_UNIT__DENOMINATOR_UNIT = UNIT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Divisional Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVISIONAL_UNIT_FEATURE_COUNT = UNIT_FEATURE_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.unit.Unit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unit</em>'.
	 * @see dk.dtu.imm.red.core.element.unit.Unit
	 * @generated
	 */
	EClass getUnit();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.unit.Unit#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Symbol</em>'.
	 * @see dk.dtu.imm.red.core.element.unit.Unit#getSymbol()
	 * @see #getUnit()
	 * @generated
	 */
	EAttribute getUnit_Symbol();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.unit.Unit#getConversionFactor <em>Conversion Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Conversion Factor</em>'.
	 * @see dk.dtu.imm.red.core.element.unit.Unit#getConversionFactor()
	 * @see #getUnit()
	 * @generated
	 */
	EAttribute getUnit_ConversionFactor();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.unit.BaseUnit <em>Base Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Unit</em>'.
	 * @see dk.dtu.imm.red.core.element.unit.BaseUnit
	 * @generated
	 */
	EClass getBaseUnit();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.unit.TimeUnit <em>Time Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Unit</em>'.
	 * @see dk.dtu.imm.red.core.element.unit.TimeUnit
	 * @generated
	 */
	EClass getTimeUnit();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.unit.SizeUnit <em>Size Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Size Unit</em>'.
	 * @see dk.dtu.imm.red.core.element.unit.SizeUnit
	 * @generated
	 */
	EClass getSizeUnit();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.unit.DivisionalUnit <em>Divisional Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Divisional Unit</em>'.
	 * @see dk.dtu.imm.red.core.element.unit.DivisionalUnit
	 * @generated
	 */
	EClass getDivisionalUnit();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.unit.DivisionalUnit#getNominatorUnit <em>Nominator Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nominator Unit</em>'.
	 * @see dk.dtu.imm.red.core.element.unit.DivisionalUnit#getNominatorUnit()
	 * @see #getDivisionalUnit()
	 * @generated
	 */
	EReference getDivisionalUnit_NominatorUnit();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.unit.DivisionalUnit#getDenominatorUnit <em>Denominator Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Denominator Unit</em>'.
	 * @see dk.dtu.imm.red.core.element.unit.DivisionalUnit#getDenominatorUnit()
	 * @see #getDivisionalUnit()
	 * @generated
	 */
	EReference getDivisionalUnit_DenominatorUnit();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UnitFactory getUnitFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.unit.impl.UnitImpl <em>Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.unit.impl.UnitImpl
		 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getUnit()
		 * @generated
		 */
		EClass UNIT = eINSTANCE.getUnit();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIT__SYMBOL = eINSTANCE.getUnit_Symbol();

		/**
		 * The meta object literal for the '<em><b>Conversion Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIT__CONVERSION_FACTOR = eINSTANCE.getUnit_ConversionFactor();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.unit.impl.BaseUnitImpl <em>Base Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.unit.impl.BaseUnitImpl
		 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getBaseUnit()
		 * @generated
		 */
		EClass BASE_UNIT = eINSTANCE.getBaseUnit();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.unit.impl.TimeUnitImpl <em>Time Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.unit.impl.TimeUnitImpl
		 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getTimeUnit()
		 * @generated
		 */
		EClass TIME_UNIT = eINSTANCE.getTimeUnit();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.unit.impl.SizeUnitImpl <em>Size Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.unit.impl.SizeUnitImpl
		 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getSizeUnit()
		 * @generated
		 */
		EClass SIZE_UNIT = eINSTANCE.getSizeUnit();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.unit.impl.DivisionalUnitImpl <em>Divisional Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.unit.impl.DivisionalUnitImpl
		 * @see dk.dtu.imm.red.core.element.unit.impl.UnitPackageImpl#getDivisionalUnit()
		 * @generated
		 */
		EClass DIVISIONAL_UNIT = eINSTANCE.getDivisionalUnit();

		/**
		 * The meta object literal for the '<em><b>Nominator Unit</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIVISIONAL_UNIT__NOMINATOR_UNIT = eINSTANCE.getDivisionalUnit_NominatorUnit();

		/**
		 * The meta object literal for the '<em><b>Denominator Unit</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIVISIONAL_UNIT__DENOMINATOR_UNIT = eINSTANCE.getDivisionalUnit_DenominatorUnit();

	}

} //UnitPackage
