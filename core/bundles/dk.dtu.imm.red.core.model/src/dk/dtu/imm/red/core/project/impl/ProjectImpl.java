/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.project.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.group.impl.GroupImpl;
import dk.dtu.imm.red.core.element.util.FileHelper;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.project.ProjectDate;
import dk.dtu.imm.red.core.project.ProjectPackage;
import dk.dtu.imm.red.core.project.UserTableEntry;
import dk.dtu.imm.red.core.properties.Kind;
import dk.dtu.imm.red.core.properties.PropertiesFactory;
import dk.dtu.imm.red.core.properties.Property;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.usecasepoint.ManagementFactor;
import dk.dtu.imm.red.core.usecasepoint.ProductivityFactor;
import dk.dtu.imm.red.core.usecasepoint.TechnologyFactor;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointFactory;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactor;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactorValue;
import dk.dtu.imm.red.core.workspace.Workspace;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Project</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.project.impl.ProjectImpl#getUsers <em>Users</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.impl.ProjectImpl#getProjectDates <em>Project Dates</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.impl.ProjectImpl#getInformation <em>Information</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.impl.ProjectImpl#getListofopenedfilesinproject <em>Listofopenedfilesinproject</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.impl.ProjectImpl#getEffort <em>Effort</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.project.impl.ProjectImpl#getLicense <em>License</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProjectImpl extends GroupImpl implements Project {
	/**
	 * The cached value of the '{@link #getUsers() <em>Users</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getUsers()
	 * @generated
	 * @ordered
	 */
	protected EList<UserTableEntry> users;

	/**
	 * The cached value of the '{@link #getProjectDates() <em>Project Dates</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getProjectDates()
	 * @generated
	 * @ordered
	 */
	protected EList<ProjectDate> projectDates;

	/**
	 * The cached value of the '{@link #getInformation() <em>Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInformation()
	 * @generated
	 * @ordered
	 */
	protected Text information;

	/**
	 * The cached value of the '{@link #getListofopenedfilesinproject() <em>Listofopenedfilesinproject</em>}' reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getListofopenedfilesinproject()
	 * @generated
	 * @ordered
	 */
	protected EList<File> listofopenedfilesinproject;
	protected EList<File> filesinproject;
	protected EList<File> fileList;

	/**
	 * The cached value of the '{@link #getEffort() <em>Effort</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEffort()
	 * @generated
	 * @ordered
	 */
	protected UsecasePoint effort;

	/**
	 * The default value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected static final String LICENSE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected String license = LICENSE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected ProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProjectPackage.Literals.PROJECT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UserTableEntry> getUsers() {
		if (users == null) {
			users = new EObjectContainmentEList.Resolving<UserTableEntry>(UserTableEntry.class, this, ProjectPackage.PROJECT__USERS);
		}
		return users;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProjectDate> getProjectDates() {
		if (projectDates == null) {
			projectDates = new EObjectContainmentEList.Resolving<ProjectDate>(ProjectDate.class, this, ProjectPackage.PROJECT__PROJECT_DATES);
		}
		return projectDates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text getInformation() {
		if (information != null && information.eIsProxy()) {
			InternalEObject oldInformation = (InternalEObject)information;
			information = (Text)eResolveProxy(oldInformation);
			if (information != oldInformation) {
				InternalEObject newInformation = (InternalEObject)information;
				NotificationChain msgs = oldInformation.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ProjectPackage.PROJECT__INFORMATION, null, null);
				if (newInformation.eInternalContainer() == null) {
					msgs = newInformation.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProjectPackage.PROJECT__INFORMATION, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ProjectPackage.PROJECT__INFORMATION, oldInformation, information));
			}
		}
		return information;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text basicGetInformation() {
		return information;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInformation(Text newInformation, NotificationChain msgs) {
		Text oldInformation = information;
		information = newInformation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProjectPackage.PROJECT__INFORMATION, oldInformation, newInformation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInformation(Text newInformation) {
		if (newInformation != information) {
			NotificationChain msgs = null;
			if (information != null)
				msgs = ((InternalEObject)information).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ProjectPackage.PROJECT__INFORMATION, null, msgs);
			if (newInformation != null)
				msgs = ((InternalEObject)newInformation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProjectPackage.PROJECT__INFORMATION, null, msgs);
			msgs = basicSetInformation(newInformation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ProjectPackage.PROJECT__INFORMATION, newInformation, newInformation));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<File> getListofopenedfilesinproject() {
		if (listofopenedfilesinproject == null) {
			listofopenedfilesinproject = new EObjectResolvingEList<File>(File.class, this, ProjectPackage.PROJECT__LISTOFOPENEDFILESINPROJECT);
		}
		return listofopenedfilesinproject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @NOT generated
	 */
	public UsecasePoint getEffort() {
		if (effort != null && effort.eIsProxy()) {
			InternalEObject oldEffort = (InternalEObject) effort;
			effort = (UsecasePoint) eResolveProxy(oldEffort);
			if (effort != oldEffort) {
				InternalEObject newEffort = (InternalEObject) effort;
				NotificationChain msgs = oldEffort.eInverseRemove(this, EOPPOSITE_FEATURE_BASE
					- ProjectPackage.PROJECT__EFFORT, null, null);
				if (newEffort.eInternalContainer() == null) {
					msgs = newEffort.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProjectPackage.PROJECT__EFFORT, null,
						msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ProjectPackage.PROJECT__EFFORT,
						oldEffort, effort));
			}
		}
		return effort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UsecasePoint basicGetEffort() {
		return effort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEffort(UsecasePoint newEffort, NotificationChain msgs) {
		UsecasePoint oldEffort = effort;
		effort = newEffort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProjectPackage.PROJECT__EFFORT, oldEffort, newEffort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
		int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ProjectPackage.PROJECT__USERS:
				return ((InternalEList<?>)getUsers()).basicRemove(otherEnd, msgs);
			case ProjectPackage.PROJECT__PROJECT_DATES:
				return ((InternalEList<?>)getProjectDates()).basicRemove(otherEnd, msgs);
			case ProjectPackage.PROJECT__INFORMATION:
				return basicSetInformation(null, msgs);
			case ProjectPackage.PROJECT__EFFORT:
				return basicSetEffort(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ProjectPackage.PROJECT__USERS:
				return getUsers();
			case ProjectPackage.PROJECT__PROJECT_DATES:
				return getProjectDates();
			case ProjectPackage.PROJECT__INFORMATION:
				if (resolve) return getInformation();
				return basicGetInformation();
			case ProjectPackage.PROJECT__LISTOFOPENEDFILESINPROJECT:
				return getListofopenedfilesinproject();
			case ProjectPackage.PROJECT__EFFORT:
				if (resolve) return getEffort();
				return basicGetEffort();
			case ProjectPackage.PROJECT__LICENSE:
				return getLicense();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ProjectPackage.PROJECT__USERS:
				getUsers().clear();
				getUsers().addAll((Collection<? extends UserTableEntry>)newValue);
				return;
			case ProjectPackage.PROJECT__PROJECT_DATES:
				getProjectDates().clear();
				getProjectDates().addAll((Collection<? extends ProjectDate>)newValue);
				return;
			case ProjectPackage.PROJECT__INFORMATION:
				setInformation((Text)newValue);
				return;
			case ProjectPackage.PROJECT__LISTOFOPENEDFILESINPROJECT:
				getListofopenedfilesinproject().clear();
				getListofopenedfilesinproject().addAll((Collection<? extends File>)newValue);
				return;
			case ProjectPackage.PROJECT__EFFORT:
				setEffort((UsecasePoint)newValue);
				return;
			case ProjectPackage.PROJECT__LICENSE:
				setLicense((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ProjectPackage.PROJECT__USERS:
				getUsers().clear();
				return;
			case ProjectPackage.PROJECT__PROJECT_DATES:
				getProjectDates().clear();
				return;
			case ProjectPackage.PROJECT__INFORMATION:
				setInformation((Text)null);
				return;
			case ProjectPackage.PROJECT__LISTOFOPENEDFILESINPROJECT:
				getListofopenedfilesinproject().clear();
				return;
			case ProjectPackage.PROJECT__EFFORT:
				setEffort((UsecasePoint)null);
				return;
			case ProjectPackage.PROJECT__LICENSE:
				setLicense(LICENSE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ProjectPackage.PROJECT__USERS:
				return users != null && !users.isEmpty();
			case ProjectPackage.PROJECT__PROJECT_DATES:
				return projectDates != null && !projectDates.isEmpty();
			case ProjectPackage.PROJECT__INFORMATION:
				return information != null;
			case ProjectPackage.PROJECT__LISTOFOPENEDFILESINPROJECT:
				return listofopenedfilesinproject != null && !listofopenedfilesinproject.isEmpty();
			case ProjectPackage.PROJECT__EFFORT:
				return effort != null;
			case ProjectPackage.PROJECT__LICENSE:
				return LICENSE_EDEFAULT == null ? license != null : !LICENSE_EDEFAULT.equals(license);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (license: ");
		result.append(license);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean isValidContent(Element content) {
		if (content instanceof Project) {
			return false;
		}
		return super.isValidContent(content);
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {
		return super.search(param);
	}

	@Override
	public String getIconURI() {

		return "icons/project.png";
	}

	@Override
	public void delete() {

		if (getParent() instanceof Workspace) {

			((Workspace) getParent()).getCurrentlyOpenedProjects().remove(this);

		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setEffort(UsecasePoint newEffort) {
		if (newEffort != effort) {
			NotificationChain msgs = null;
			if (effort != null)
				msgs = ((InternalEObject)effort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ProjectPackage.PROJECT__EFFORT, null, msgs);
			if (newEffort != null)
				msgs = ((InternalEObject)newEffort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProjectPackage.PROJECT__EFFORT, null, msgs);
			msgs = basicSetEffort(newEffort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ProjectPackage.PROJECT__EFFORT, newEffort, newEffort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLicense() {
		return license;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLicense(String newLicense) {
		String oldLicense = license;
		license = newLicense;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ProjectPackage.PROJECT__LICENSE, oldLicense, license));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<File> getListOfFilesInProject() {
		if (this.listofopenedfilesinproject == null) {
			this.listofopenedfilesinproject = new EObjectResolvingEList<File>(
				File.class, this,
				ProjectPackage.PROJECT__LISTOFOPENEDFILESINPROJECT);

		}
		return this.listofopenedfilesinproject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void addToListOfFilesInProject(File fileToAdd) {
		if (this.listofopenedfilesinproject == null) {
			this.listofopenedfilesinproject = new EObjectResolvingEList<File>(
				File.class, this,
				ProjectPackage.PROJECT__LISTOFOPENEDFILESINPROJECT);

		}
		this.listofopenedfilesinproject.add(fileToAdd);

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void createDefaultUsecasePoint() {
		UsecasePoint ucp = UsecasepointFactory.eINSTANCE.createUsecasePoint();
		
		//Create Management Factor
		ManagementFactor mf = UsecasepointFactory.eINSTANCE.createManagementFactor();
		List<WeightedFactor> ml = mf.getWeightedFactors(); 
		
		ml.add(createWeightedFactor("Application experience", 0.5));
		ml.add(createWeightedFactor("Chief architect", 0.5));
		ml.add(createWeightedFactor("Motivation", 1.0));
		ml.add(createWeightedFactor("Stable requirements", 2.0));
		ml.add(createWeightedFactor("Part-time workers", -1.0));
		ml.add(createWeightedFactor("Programming language / SDK", -1.0));  
		
		ucp.setManagementFactor(mf);
		
		//Create Technology Factor
		TechnologyFactor tfa = UsecasepointFactory.eINSTANCE.createTechnologyFactor(); 
		List<WeightedFactor> tf = tfa.getWeightedFactors(); 
	 
		tf.add(createWeightedFactor("Distributed System", 2.0));
		tf.add(createWeightedFactor("Performance and load requirements", 1.0));
		tf.add(createWeightedFactor("Efficiency of the user interface", 1.0));
		tf.add(createWeightedFactor("Complexity of business rules and calculations", 1.0));
		tf.add(createWeightedFactor("Reusability", 1.0));
		tf.add(createWeightedFactor("Easy to install", 0.5));
		tf.add(createWeightedFactor("Easy to use", 0.5));
		tf.add(createWeightedFactor("Portability", 2.0));
		tf.add(createWeightedFactor("Easy to change", 1.0));
		tf.add(createWeightedFactor("System avalilability", 1.0));
		tf.add(createWeightedFactor("Special security features", 1.0));
		tf.add(createWeightedFactor("Direct access for third parties", 1.0));
		tf.add(createWeightedFactor("Special user training facilities", 1.0)); 
		
		ucp.setTechnologyFactor(tfa);
		
		//Create Productivity Factor
		ProductivityFactor pf = UsecasepointFactory.eINSTANCE.createProductivityFactor();
		pf.setSum(20.0);
		ucp.setProductivityFactor(pf); 
		
		//Create Requirement Factor
		ucp.setRequirementFactor(UsecasepointFactory.eINSTANCE.createRequirementFactor()); 
		
		//Set Cost Per Use Case Point 
		Property cPerUCP = PropertiesFactory.eINSTANCE.createProperty(); 
		cPerUCP.setName("Cost Per Use Case Point");
		cPerUCP.setKind(Kind.VALUE);
		ucp.setCostPerUsecasePoint(cPerUCP);
		
		setEffort(ucp); 
	}
	
	private WeightedFactor createWeightedFactor(String name, double weight) {
		WeightedFactor w =  UsecasepointFactory.eINSTANCE.createWeightedFactor();
		w.setFactorValue(WeightedFactorValue.MEDIUM);
		w.setName(name);
		w.setWeight(weight);
	
		return w;
	} 

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void removeFromListOfFilesInProject(File fileToRemove) {
		if (this.listofopenedfilesinproject != null) {
			this.listofopenedfilesinproject.remove(fileToRemove);
		}

	}

	@Override
	public void save() {
		this.eSetDeliver(false);
		this.setLastModified(new Date());
		this.eSetDeliver(true);
		this.setLicense(GroupImpl.LICENSE);
		
		if (name == null) {
			return;
		}
		
		Resource resource = eResource();
		
		if (resource == null) {
			throw new IllegalStateException("Project is not in a Resource.");
		}

		if (name.equals(FileHelper.getFileName(this))) {
			// The file name has not changed, so we simply save the resource.
			try {
				resource.save(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			// The file name has changed, so we save the file with the new file
			// name and delete the old file (if it exists).
			
			String directoryPath = FileHelper.getDirectoryPath(this);
			String newFilePath = FileHelper.join(directoryPath, name);
			java.io.File newPhysicalFile = new java.io.File(newFilePath);

			if (newPhysicalFile.exists()) {
				throw new IllegalStateException("A file already exists at the specified path.");
			}

			ResourceSet resourceSet = getResourceSet();
			URI newUri = URI.createFileURI(newFilePath);
			Resource newResource = resourceSet.createResource(newUri);

			newResource.getContents().add(this);

			try {
				newResource.save(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				resource.delete(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

} // ProjectImpl

