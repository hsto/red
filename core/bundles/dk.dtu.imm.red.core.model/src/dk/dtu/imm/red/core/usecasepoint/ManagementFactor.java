/**
 */
package dk.dtu.imm.red.core.usecasepoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Management Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getManagementFactor()
 * @model
 * @generated
 */
public interface ManagementFactor extends WeightedFactorContainer {

} // ManagementFactor
