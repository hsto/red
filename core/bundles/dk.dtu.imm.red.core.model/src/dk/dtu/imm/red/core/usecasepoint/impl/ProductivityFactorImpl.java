/**
 */
package dk.dtu.imm.red.core.usecasepoint.impl;

import dk.dtu.imm.red.core.usecasepoint.ProductivityFactor;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Productivity Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProductivityFactorImpl extends FactorImpl implements ProductivityFactor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc --> 
	 */
	protected ProductivityFactorImpl() {
		super();  
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasepointPackage.Literals.PRODUCTIVITY_FACTOR;
	}

} //ProductivityFactorImpl
