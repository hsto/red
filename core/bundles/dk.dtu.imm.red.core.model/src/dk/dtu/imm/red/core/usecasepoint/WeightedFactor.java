/**
 */
package dk.dtu.imm.red.core.usecasepoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Weighted Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.WeightedFactor#getFactorValue <em>Factor Value</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.WeightedFactor#getWeight <em>Weight</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getWeightedFactor()
 * @model
 * @generated
 */
public interface WeightedFactor extends Factor {
	/**
	 * Returns the value of the '<em><b>Factor Value</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.usecasepoint.WeightedFactorValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Factor Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Factor Value</em>' attribute.
	 * @see dk.dtu.imm.red.core.usecasepoint.WeightedFactorValue
	 * @see #setFactorValue(WeightedFactorValue)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getWeightedFactor_FactorValue()
	 * @model
	 * @generated
	 */
	WeightedFactorValue getFactorValue();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.WeightedFactor#getFactorValue <em>Factor Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Factor Value</em>' attribute.
	 * @see dk.dtu.imm.red.core.usecasepoint.WeightedFactorValue
	 * @see #getFactorValue()
	 * @generated
	 */
	void setFactorValue(WeightedFactorValue value);

	/**
	 * Returns the value of the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Weight</em>' attribute.
	 * @see #setWeight(double)
	 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getWeightedFactor_Weight()
	 * @model
	 * @generated
	 */
	double getWeight();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.usecasepoint.WeightedFactor#getWeight <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Weight</em>' attribute.
	 * @see #getWeight()
	 * @generated
	 */
	void setWeight(double value);

} // WeightedFactor
