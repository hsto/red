/**
 */
package dk.dtu.imm.red.core.element.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Size Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.core.element.unit.UnitPackage#getSizeUnit()
 * @model
 * @generated
 */
public interface SizeUnit extends Unit {
} // SizeUnit
