/**
 */
package dk.dtu.imm.red.core.properties;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.properties.PropertiesFactory
 * @model kind="package"
 * @generated
 */
public interface PropertiesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "properties";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.core.properties";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "properties";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PropertiesPackage eINSTANCE = dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.properties.impl.PropertyImpl
	 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__VALUE = 1;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__KIND = 2;

	/**
	 * The feature id for the '<em><b>Benefit Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__BENEFIT_UNIT = 3;

	/**
	 * The feature id for the '<em><b>Duration Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__DURATION_UNIT = 4;

	/**
	 * The feature id for the '<em><b>Monetary Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__MONETARY_UNIT = 5;

	/**
	 * The feature id for the '<em><b>Measurement Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__MEASUREMENT_KIND = 6;

	/**
	 * The feature id for the '<em><b>Value Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__VALUE_KIND = 7;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.properties.MonetaryUnit <em>Monetary Unit</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.properties.MonetaryUnit
	 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getMonetaryUnit()
	 * @generated
	 */
	int MONETARY_UNIT = 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.properties.BenefitUnit <em>Benefit Unit</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.properties.BenefitUnit
	 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getBenefitUnit()
	 * @generated
	 */
	int BENEFIT_UNIT = 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.properties.DurationUnit <em>Duration Unit</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.properties.DurationUnit
	 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getDurationUnit()
	 * @generated
	 */
	int DURATION_UNIT = 3;


	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.properties.ValueKind <em>Value Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.properties.ValueKind
	 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getValueKind()
	 * @generated
	 */
	int VALUE_KIND = 4;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.properties.MeasurementKind <em>Measurement Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.properties.MeasurementKind
	 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getMeasurementKind()
	 * @generated
	 */
	int MEASUREMENT_KIND = 5;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.properties.Kind <em>Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.properties.Kind
	 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getKind()
	 * @generated
	 */
	int KIND = 6;

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.properties.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see dk.dtu.imm.red.core.properties.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.properties.Property#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.dtu.imm.red.core.properties.Property#getName()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Name();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.properties.Property#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see dk.dtu.imm.red.core.properties.Property#getValue()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.properties.Property#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see dk.dtu.imm.red.core.properties.Property#getKind()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Kind();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.properties.Property#getBenefitUnit <em>Benefit Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Benefit Unit</em>'.
	 * @see dk.dtu.imm.red.core.properties.Property#getBenefitUnit()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_BenefitUnit();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.properties.Property#getDurationUnit <em>Duration Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration Unit</em>'.
	 * @see dk.dtu.imm.red.core.properties.Property#getDurationUnit()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_DurationUnit();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.properties.Property#getMonetaryUnit <em>Monetary Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Monetary Unit</em>'.
	 * @see dk.dtu.imm.red.core.properties.Property#getMonetaryUnit()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_MonetaryUnit();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.properties.Property#getMeasurementKind <em>Measurement Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Measurement Kind</em>'.
	 * @see dk.dtu.imm.red.core.properties.Property#getMeasurementKind()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_MeasurementKind();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.properties.Property#getValueKind <em>Value Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Kind</em>'.
	 * @see dk.dtu.imm.red.core.properties.Property#getValueKind()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_ValueKind();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.properties.MonetaryUnit <em>Monetary Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Monetary Unit</em>'.
	 * @see dk.dtu.imm.red.core.properties.MonetaryUnit
	 * @generated
	 */
	EEnum getMonetaryUnit();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.properties.BenefitUnit <em>Benefit Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Benefit Unit</em>'.
	 * @see dk.dtu.imm.red.core.properties.BenefitUnit
	 * @generated
	 */
	EEnum getBenefitUnit();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.properties.DurationUnit <em>Duration Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Duration Unit</em>'.
	 * @see dk.dtu.imm.red.core.properties.DurationUnit
	 * @generated
	 */
	EEnum getDurationUnit();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.properties.ValueKind <em>Value Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Value Kind</em>'.
	 * @see dk.dtu.imm.red.core.properties.ValueKind
	 * @generated
	 */
	EEnum getValueKind();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.properties.MeasurementKind <em>Measurement Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Measurement Kind</em>'.
	 * @see dk.dtu.imm.red.core.properties.MeasurementKind
	 * @generated
	 */
	EEnum getMeasurementKind();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.properties.Kind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Kind</em>'.
	 * @see dk.dtu.imm.red.core.properties.Kind
	 * @generated
	 */
	EEnum getKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PropertiesFactory getPropertiesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.properties.impl.PropertyImpl
		 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__NAME = eINSTANCE.getProperty_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__VALUE = eINSTANCE.getProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__KIND = eINSTANCE.getProperty_Kind();

		/**
		 * The meta object literal for the '<em><b>Benefit Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__BENEFIT_UNIT = eINSTANCE.getProperty_BenefitUnit();

		/**
		 * The meta object literal for the '<em><b>Duration Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__DURATION_UNIT = eINSTANCE.getProperty_DurationUnit();

		/**
		 * The meta object literal for the '<em><b>Monetary Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__MONETARY_UNIT = eINSTANCE.getProperty_MonetaryUnit();

		/**
		 * The meta object literal for the '<em><b>Measurement Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__MEASUREMENT_KIND = eINSTANCE.getProperty_MeasurementKind();

		/**
		 * The meta object literal for the '<em><b>Value Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__VALUE_KIND = eINSTANCE.getProperty_ValueKind();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.properties.MonetaryUnit <em>Monetary Unit</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.properties.MonetaryUnit
		 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getMonetaryUnit()
		 * @generated
		 */
		EEnum MONETARY_UNIT = eINSTANCE.getMonetaryUnit();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.properties.BenefitUnit <em>Benefit Unit</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.properties.BenefitUnit
		 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getBenefitUnit()
		 * @generated
		 */
		EEnum BENEFIT_UNIT = eINSTANCE.getBenefitUnit();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.properties.DurationUnit <em>Duration Unit</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.properties.DurationUnit
		 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getDurationUnit()
		 * @generated
		 */
		EEnum DURATION_UNIT = eINSTANCE.getDurationUnit();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.properties.ValueKind <em>Value Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.properties.ValueKind
		 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getValueKind()
		 * @generated
		 */
		EEnum VALUE_KIND = eINSTANCE.getValueKind();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.properties.MeasurementKind <em>Measurement Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.properties.MeasurementKind
		 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getMeasurementKind()
		 * @generated
		 */
		EEnum MEASUREMENT_KIND = eINSTANCE.getMeasurementKind();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.properties.Kind <em>Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.properties.Kind
		 * @see dk.dtu.imm.red.core.properties.impl.PropertiesPackageImpl#getKind()
		 * @generated
		 */
		EEnum KIND = eINSTANCE.getKind();

	}

} //PropertiesPackage
