/**
 */
package dk.dtu.imm.red.core.validation.impl;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.validation.ErrorMessage;
import dk.dtu.imm.red.core.validation.Severity;
import dk.dtu.imm.red.core.validation.ValidationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Error Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.validation.impl.ErrorMessageImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.validation.impl.ErrorMessageImpl#getSeverity <em>Severity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.validation.impl.ErrorMessageImpl#getElementReference <em>Element Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ErrorMessageImpl extends EObjectImpl implements ErrorMessage {
	/**
	 * The default value of the '{@link #getMessage() <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected String message = MESSAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSeverity() <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeverity()
	 * @generated
	 * @ordered
	 */
	protected static final Severity SEVERITY_EDEFAULT = Severity.INPUT;

	/**
	 * The cached value of the '{@link #getSeverity() <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeverity()
	 * @generated
	 * @ordered
	 */
	protected Severity severity = SEVERITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getElementReference() <em>Element Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementReference()
	 * @generated
	 * @ordered
	 */
	protected Element elementReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ErrorMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ValidationPackage.Literals.ERROR_MESSAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessage(String newMessage) {
		String oldMessage = message;
		message = newMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ValidationPackage.ERROR_MESSAGE__MESSAGE, oldMessage, message));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Severity getSeverity() {
		return severity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSeverity(Severity newSeverity) {
		Severity oldSeverity = severity;
		severity = newSeverity == null ? SEVERITY_EDEFAULT : newSeverity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ValidationPackage.ERROR_MESSAGE__SEVERITY, oldSeverity, severity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element getElementReference() {
		if (elementReference != null && elementReference.eIsProxy()) {
			InternalEObject oldElementReference = (InternalEObject)elementReference;
			elementReference = (Element)eResolveProxy(oldElementReference);
			if (elementReference != oldElementReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ValidationPackage.ERROR_MESSAGE__ELEMENT_REFERENCE, oldElementReference, elementReference));
			}
		}
		return elementReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element basicGetElementReference() {
		return elementReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementReference(Element newElementReference) {
		Element oldElementReference = elementReference;
		elementReference = newElementReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ValidationPackage.ERROR_MESSAGE__ELEMENT_REFERENCE, oldElementReference, elementReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ValidationPackage.ERROR_MESSAGE__MESSAGE:
				return getMessage();
			case ValidationPackage.ERROR_MESSAGE__SEVERITY:
				return getSeverity();
			case ValidationPackage.ERROR_MESSAGE__ELEMENT_REFERENCE:
				if (resolve) return getElementReference();
				return basicGetElementReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ValidationPackage.ERROR_MESSAGE__MESSAGE:
				setMessage((String)newValue);
				return;
			case ValidationPackage.ERROR_MESSAGE__SEVERITY:
				setSeverity((Severity)newValue);
				return;
			case ValidationPackage.ERROR_MESSAGE__ELEMENT_REFERENCE:
				setElementReference((Element)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ValidationPackage.ERROR_MESSAGE__MESSAGE:
				setMessage(MESSAGE_EDEFAULT);
				return;
			case ValidationPackage.ERROR_MESSAGE__SEVERITY:
				setSeverity(SEVERITY_EDEFAULT);
				return;
			case ValidationPackage.ERROR_MESSAGE__ELEMENT_REFERENCE:
				setElementReference((Element)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ValidationPackage.ERROR_MESSAGE__MESSAGE:
				return MESSAGE_EDEFAULT == null ? message != null : !MESSAGE_EDEFAULT.equals(message);
			case ValidationPackage.ERROR_MESSAGE__SEVERITY:
				return severity != SEVERITY_EDEFAULT;
			case ValidationPackage.ERROR_MESSAGE__ELEMENT_REFERENCE:
				return elementReference != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (message: ");
		result.append(message);
		result.append(", severity: ");
		result.append(severity);
		result.append(')');
		return result.toString();
	}

} //ErrorMessageImpl
