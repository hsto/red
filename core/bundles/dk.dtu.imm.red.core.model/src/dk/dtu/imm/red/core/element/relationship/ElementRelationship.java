/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.relationship;

import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.text.CrossReference;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Element Relationship</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.relationship.ElementRelationship#getFromElement <em>From Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.relationship.ElementRelationship#getToElement <em>To Element</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.relationship.ElementRelationship#getRelevance <em>Relevance</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.relationship.ElementRelationship#getReferencingTexts <em>Referencing Texts</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage#getElementRelationship()
 * @model abstract="true"
 * @generated
 */
public interface ElementRelationship extends Element {
	/**
	 * Returns the value of the '<em><b>From Element</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.core.element.Element#getRelatesTo <em>Relates To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Element</em>' container reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Element</em>' container reference.
	 * @see #setFromElement(Element)
	 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage#getElementRelationship_FromElement()
	 * @see dk.dtu.imm.red.core.element.Element#getRelatesTo
	 * @model opposite="relatesTo" required="true" transient="false"
	 * @generated
	 */
	Element getFromElement();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.relationship.ElementRelationship#getFromElement <em>From Element</em>}' container reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>From Element</em>' container reference.
	 * @see #getFromElement()
	 * @generated
	 */
	void setFromElement(Element value);

	/**
	 * Returns the value of the '<em><b>To Element</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.core.element.Element#getRelatedBy <em>Related By</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Element</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Element</em>' reference.
	 * @see #setToElement(Element)
	 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage#getElementRelationship_ToElement()
	 * @see dk.dtu.imm.red.core.element.Element#getRelatedBy
	 * @model opposite="relatedBy" required="true"
	 * @generated
	 */
	Element getToElement();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.relationship.ElementRelationship#getToElement <em>To Element</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>To Element</em>' reference.
	 * @see #getToElement()
	 * @generated
	 */
	void setToElement(Element value);

	/**
	 * Returns the value of the '<em><b>Relevance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relevance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relevance</em>' attribute.
	 * @see #setRelevance(String)
	 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage#getElementRelationship_Relevance()
	 * @model
	 * @generated
	 */
	String getRelevance();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.relationship.ElementRelationship#getRelevance <em>Relevance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relevance</em>' attribute.
	 * @see #getRelevance()
	 * @generated
	 */
	void setRelevance(String value);

	/**
	 * Returns the value of the '<em><b>Referencing Texts</b></em>' reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.text.CrossReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referencing Texts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referencing Texts</em>' reference list.
	 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage#getElementRelationship_ReferencingTexts()
	 * @model
	 * @generated
	 */
	EList<CrossReference> getReferencingTexts();

} // ElementRelationship
