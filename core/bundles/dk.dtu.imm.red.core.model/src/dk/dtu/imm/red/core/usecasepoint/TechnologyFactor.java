/**
 */
package dk.dtu.imm.red.core.usecasepoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Technology Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getTechnologyFactor()
 * @model
 * @generated
 */
public interface TechnologyFactor extends WeightedFactorContainer {
} // TechnologyFactor
