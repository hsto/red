package dk.dtu.imm.red.core.element.impl;

import dk.dtu.imm.red.core.element.Element;

public interface ValidationObserver {
	public void modelChanged(Element e);
}
