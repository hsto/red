/**
 */
package dk.dtu.imm.red.core.element.unit.impl;

import dk.dtu.imm.red.core.element.unit.*;
import dk.dtu.imm.red.core.element.unit.DivisionalUnit;
import dk.dtu.imm.red.core.element.unit.Unit;
import dk.dtu.imm.red.core.element.unit.impl.DivisionalUnitImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UnitFactoryImpl extends EFactoryImpl implements UnitFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UnitFactory init() {
		try {
			UnitFactory theUnitFactory = (UnitFactory)EPackage.Registry.INSTANCE.getEFactory(UnitPackage.eNS_URI);
			if (theUnitFactory != null) {
				return theUnitFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UnitFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UnitPackage.BASE_UNIT: return createBaseUnit();
			case UnitPackage.TIME_UNIT: return createTimeUnit();
			case UnitPackage.SIZE_UNIT: return createSizeUnit();
			case UnitPackage.DIVISIONAL_UNIT: return createDivisionalUnit();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseUnit createBaseUnit() {
		BaseUnitImpl baseUnit = new BaseUnitImpl();
		return baseUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeUnit createTimeUnit() {
		TimeUnitImpl timeUnit = new TimeUnitImpl();
		return timeUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeUnit createSizeUnit() {
		SizeUnitImpl sizeUnit = new SizeUnitImpl();
		return sizeUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DivisionalUnit createDivisionalUnit() {
		DivisionalUnitImpl divisionalUnit = new DivisionalUnitImpl();
		return divisionalUnit;
	}
	
	/**
	 * @generated NOT
	 */
	public DivisionalUnit createDivisionalUnit(Unit nominatorUnit, Unit denominatorUnit) {
		DivisionalUnitImpl divisionalUnit = new DivisionalUnitImpl();
		divisionalUnit.setNominatorUnit(nominatorUnit);
		divisionalUnit.setDenominatorUnit(denominatorUnit);
		return divisionalUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitPackage getUnitPackage() {
		return (UnitPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UnitPackage getPackage() {
		return UnitPackage.eINSTANCE;
	}

} //UnitFactoryImpl
