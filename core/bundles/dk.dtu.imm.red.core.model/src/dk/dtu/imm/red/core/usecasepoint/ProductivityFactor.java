/**
 */
package dk.dtu.imm.red.core.usecasepoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Productivity Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getProductivityFactor()
 * @model
 * @generated
 */
public interface ProductivityFactor extends Factor {
} // ProductivityFactor
