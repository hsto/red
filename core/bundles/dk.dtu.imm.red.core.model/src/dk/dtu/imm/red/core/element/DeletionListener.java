/**
 */
package dk.dtu.imm.red.core.element;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deletion Listener</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.DeletionListener#getHostingObject <em>Hosting Object</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.DeletionListener#getHostingFeature <em>Hosting Feature</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.DeletionListener#getHandledObject <em>Handled Object</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.ElementPackage#getDeletionListener()
 * @model
 * @generated
 */
public interface DeletionListener extends EObject {
	/**
	 * Returns the value of the '<em><b>Hosting Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hosting Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hosting Object</em>' reference.
	 * @see #setHostingObject(EObject)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getDeletionListener_HostingObject()
	 * @model
	 * @generated
	 */
	EObject getHostingObject();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.DeletionListener#getHostingObject <em>Hosting Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hosting Object</em>' reference.
	 * @see #getHostingObject()
	 * @generated
	 */
	void setHostingObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Hosting Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hosting Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hosting Feature</em>' reference.
	 * @see #setHostingFeature(EStructuralFeature)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getDeletionListener_HostingFeature()
	 * @model
	 * @generated
	 */
	EStructuralFeature getHostingFeature();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.DeletionListener#getHostingFeature <em>Hosting Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hosting Feature</em>' reference.
	 * @see #getHostingFeature()
	 * @generated
	 */
	void setHostingFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Handled Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handled Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handled Object</em>' reference.
	 * @see #setHandledObject(EObject)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getDeletionListener_HandledObject()
	 * @model
	 * @generated
	 */
	EObject getHandledObject();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.DeletionListener#getHandledObject <em>Handled Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handled Object</em>' reference.
	 * @see #getHandledObject()
	 * @generated
	 */
	void setHandledObject(EObject value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void notifyOfDeletion();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void notifyOfUndo();

} // DeletionListener
