/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.comment;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import dk.dtu.imm.red.core.element.ElementPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.comment.CommentFactory
 * @model kind="package"
 * @generated
 */
public interface CommentPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "comment";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.core.comment";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "comment";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	CommentPackage eINSTANCE = dk.dtu.imm.red.core.comment.impl.CommentPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.comment.impl.CommentListImpl <em>List</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.comment.impl.CommentListImpl
	 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getCommentList()
	 * @generated
	 */
	int COMMENT_LIST = 0;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_LIST__COMMENTS = 0;

	/**
	 * The number of structural features of the '<em>List</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int COMMENT_LIST_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.comment.impl.CommentImpl <em>Comment</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.comment.impl.CommentImpl
	 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getComment()
	 * @generated
	 */
	int COMMENT = 1;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__ICON_URI = ElementPackage.ELEMENT__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__ICON = ElementPackage.ELEMENT__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__LABEL = ElementPackage.ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__NAME = ElementPackage.ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__ELEMENT_KIND = ElementPackage.ELEMENT__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__DESCRIPTION = ElementPackage.ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__PRIORITY = ElementPackage.ELEMENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__COMMENTLIST = ElementPackage.ELEMENT__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int COMMENT__TIME_CREATED = ElementPackage.ELEMENT__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__LAST_MODIFIED = ElementPackage.ELEMENT__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__LAST_MODIFIED_BY = ElementPackage.ELEMENT__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__CREATOR = ElementPackage.ELEMENT__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__VERSION = ElementPackage.ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__VISIBLE_ID = ElementPackage.ELEMENT__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__UNIQUE_ID = ElementPackage.ELEMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__RELATES_TO = ElementPackage.ELEMENT__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__RELATED_BY = ElementPackage.ELEMENT__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__PARENT = ElementPackage.ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__URI = ElementPackage.ELEMENT__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__WORK_PACKAGE = ElementPackage.ELEMENT__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__CHANGE_LIST = ElementPackage.ELEMENT__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__RESPONSIBLE_USER = ElementPackage.ELEMENT__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__DEADLINE = ElementPackage.ELEMENT__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__LOCK_STATUS = ElementPackage.ELEMENT__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__LOCK_PASSWORD = ElementPackage.ELEMENT__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__ESTIMATED_COMPLEXITY = ElementPackage.ELEMENT__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__COST = ElementPackage.ELEMENT__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__BENEFIT = ElementPackage.ELEMENT__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__RISK = ElementPackage.ELEMENT__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__LIFE_CYCLE_PHASE = ElementPackage.ELEMENT__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__STATE = ElementPackage.ELEMENT__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__MANAGEMENT_DECISION = ElementPackage.ELEMENT__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__QA_ASSESSMENT = ElementPackage.ELEMENT__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__MANAGEMENT_DISCUSSION = ElementPackage.ELEMENT__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__DELETION_LISTENERS = ElementPackage.ELEMENT__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__QUANTITIES = ElementPackage.ELEMENT__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int COMMENT__CATEGORY = ElementPackage.ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int COMMENT__TEXT = ElementPackage.ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Author</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int COMMENT__AUTHOR = ElementPackage.ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Comment</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int COMMENT_FEATURE_COUNT = ElementPackage.ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.comment.impl.CommentExporterImpl <em>Exporter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.comment.impl.CommentExporterImpl
	 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getCommentExporter()
	 * @generated
	 */
	int COMMENT_EXPORTER = 2;

	/**
	 * The number of structural features of the '<em>Exporter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_EXPORTER_FEATURE_COUNT = ElementPackage.VISITOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.comment.impl.IssueCommentImpl <em>Issue Comment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.comment.impl.IssueCommentImpl
	 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getIssueComment()
	 * @generated
	 */
	int ISSUE_COMMENT = 3;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__ICON_URI = COMMENT__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__ICON = COMMENT__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__LABEL = COMMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__NAME = COMMENT__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__ELEMENT_KIND = COMMENT__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__DESCRIPTION = COMMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__PRIORITY = COMMENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__COMMENTLIST = COMMENT__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__TIME_CREATED = COMMENT__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__LAST_MODIFIED = COMMENT__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__LAST_MODIFIED_BY = COMMENT__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__CREATOR = COMMENT__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__VERSION = COMMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__VISIBLE_ID = COMMENT__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__UNIQUE_ID = COMMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__RELATES_TO = COMMENT__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__RELATED_BY = COMMENT__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__PARENT = COMMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__URI = COMMENT__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__WORK_PACKAGE = COMMENT__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__CHANGE_LIST = COMMENT__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__RESPONSIBLE_USER = COMMENT__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__DEADLINE = COMMENT__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__LOCK_STATUS = COMMENT__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__LOCK_PASSWORD = COMMENT__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__ESTIMATED_COMPLEXITY = COMMENT__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__COST = COMMENT__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__BENEFIT = COMMENT__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__RISK = COMMENT__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__LIFE_CYCLE_PHASE = COMMENT__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__STATE = COMMENT__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__MANAGEMENT_DECISION = COMMENT__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__QA_ASSESSMENT = COMMENT__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__MANAGEMENT_DISCUSSION = COMMENT__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__DELETION_LISTENERS = COMMENT__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__QUANTITIES = COMMENT__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__CATEGORY = COMMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Text</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__TEXT = COMMENT__TEXT;

	/**
	 * The feature id for the '<em><b>Author</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__AUTHOR = COMMENT__AUTHOR;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT__SEVERITY = COMMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Issue Comment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUE_COMMENT_FEATURE_COUNT = COMMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.comment.CommentCategory <em>Category</em>}' enum.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.comment.CommentCategory
	 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getCommentCategory()
	 * @generated
	 */
	int COMMENT_CATEGORY = 4;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.comment.IssueSeverity <em>Issue Severity</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.comment.IssueSeverity
	 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getIssueSeverity()
	 * @generated
	 */
	int ISSUE_SEVERITY = 5;

	/**
	 * Returns the meta object for class '
	 * {@link dk.dtu.imm.red.core.comment.CommentList <em>List</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>List</em>'.
	 * @see dk.dtu.imm.red.core.comment.CommentList
	 * @generated
	 */
	EClass getCommentList();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.core.comment.CommentList#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see dk.dtu.imm.red.core.comment.CommentList#getComments()
	 * @see #getCommentList()
	 * @generated
	 */
	EReference getCommentList_Comments();

	/**
	 * Returns the meta object for class '
	 * {@link dk.dtu.imm.red.core.comment.Comment <em>Comment</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Comment</em>'.
	 * @see dk.dtu.imm.red.core.comment.Comment
	 * @generated
	 */
	EClass getComment();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.comment.Comment#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see dk.dtu.imm.red.core.comment.Comment#getCategory()
	 * @see #getComment()
	 * @generated
	 */
	EAttribute getComment_Category();

	/**
	 * Returns the meta object for the reference '
	 * {@link dk.dtu.imm.red.core.comment.Comment#getText <em>Text</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Text</em>'.
	 * @see dk.dtu.imm.red.core.comment.Comment#getText()
	 * @see #getComment()
	 * @generated
	 */
	EReference getComment_Text();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.comment.Comment#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Author</em>'.
	 * @see dk.dtu.imm.red.core.comment.Comment#getAuthor()
	 * @see #getComment()
	 * @generated
	 */
	EReference getComment_Author();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.comment.CommentExporter <em>Exporter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exporter</em>'.
	 * @see dk.dtu.imm.red.core.comment.CommentExporter
	 * @generated
	 */
	EClass getCommentExporter();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.comment.IssueComment <em>Issue Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Issue Comment</em>'.
	 * @see dk.dtu.imm.red.core.comment.IssueComment
	 * @generated
	 */
	EClass getIssueComment();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.comment.IssueComment#getSeverity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Severity</em>'.
	 * @see dk.dtu.imm.red.core.comment.IssueComment#getSeverity()
	 * @see #getIssueComment()
	 * @generated
	 */
	EAttribute getIssueComment_Severity();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.comment.CommentCategory <em>Category</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Category</em>'.
	 * @see dk.dtu.imm.red.core.comment.CommentCategory
	 * @generated
	 */
	EEnum getCommentCategory();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.comment.IssueSeverity <em>Issue Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Issue Severity</em>'.
	 * @see dk.dtu.imm.red.core.comment.IssueSeverity
	 * @generated
	 */
	EEnum getIssueSeverity();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CommentFactory getCommentFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.comment.impl.CommentListImpl <em>List</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.comment.impl.CommentListImpl
		 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getCommentList()
		 * @generated
		 */
		EClass COMMENT_LIST = eINSTANCE.getCommentList();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference COMMENT_LIST__COMMENTS = eINSTANCE.getCommentList_Comments();

		/**
		 * The meta object literal for the '
		 * {@link dk.dtu.imm.red.core.comment.implementation.CommentImpl
		 * <em>Comment</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see dk.dtu.imm.red.core.comment.implementation.CommentImpl
		 * @see dk.dtu.imm.red.core.comment.implementation.CommentPackageImpl#getComment()
		 * @generated
		 */
		EClass COMMENT = eINSTANCE.getComment();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMENT__CATEGORY = eINSTANCE.getComment_Category();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMENT__TEXT = eINSTANCE.getComment_Text();

		/**
		 * The meta object literal for the '<em><b>Author</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMENT__AUTHOR = eINSTANCE.getComment_Author();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.comment.impl.CommentExporterImpl <em>Exporter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.comment.impl.CommentExporterImpl
		 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getCommentExporter()
		 * @generated
		 */
		EClass COMMENT_EXPORTER = eINSTANCE.getCommentExporter();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.comment.impl.IssueCommentImpl <em>Issue Comment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.comment.impl.IssueCommentImpl
		 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getIssueComment()
		 * @generated
		 */
		EClass ISSUE_COMMENT = eINSTANCE.getIssueComment();

		/**
		 * The meta object literal for the '<em><b>Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISSUE_COMMENT__SEVERITY = eINSTANCE.getIssueComment_Severity();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.comment.CommentCategory <em>Category</em>}' enum.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.comment.CommentCategory
		 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getCommentCategory()
		 * @generated
		 */
		EEnum COMMENT_CATEGORY = eINSTANCE.getCommentCategory();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.comment.IssueSeverity <em>Issue Severity</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.comment.IssueSeverity
		 * @see dk.dtu.imm.red.core.comment.impl.CommentPackageImpl#getIssueSeverity()
		 * @generated
		 */
		EEnum ISSUE_SEVERITY = eINSTANCE.getIssueSeverity();

	}

} // CommentPackage
