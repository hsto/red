/**
 */
package dk.dtu.imm.red.core.properties.impl;

import dk.dtu.imm.red.core.properties.BenefitUnit;
import dk.dtu.imm.red.core.properties.DurationUnit;
import dk.dtu.imm.red.core.properties.Kind;
import dk.dtu.imm.red.core.properties.MeasurementKind;
import dk.dtu.imm.red.core.properties.MonetaryUnit;
import dk.dtu.imm.red.core.properties.PropertiesPackage;
import dk.dtu.imm.red.core.properties.Property;

import dk.dtu.imm.red.core.properties.ValueKind;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl#getValue <em>Value</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl#getBenefitUnit <em>Benefit Unit</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl#getDurationUnit <em>Duration Unit</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl#getMonetaryUnit <em>Monetary Unit</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl#getMeasurementKind <em>Measurement Kind</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.impl.PropertyImpl#getValueKind <em>Value Kind</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PropertyImpl extends EObjectImpl implements Property {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final double VALUE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected double value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final Kind KIND_EDEFAULT = Kind.MEASUREMENT;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected Kind kind = KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #getBenefitUnit() <em>Benefit Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBenefitUnit()
	 * @generated
	 * @ordered
	 */
	protected static final BenefitUnit BENEFIT_UNIT_EDEFAULT = BenefitUnit.BENEFIT;

	/**
	 * The cached value of the '{@link #getBenefitUnit() <em>Benefit Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBenefitUnit()
	 * @generated
	 * @ordered
	 */
	protected BenefitUnit benefitUnit = BENEFIT_UNIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDurationUnit() <em>Duration Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurationUnit()
	 * @generated
	 * @ordered
	 */
	protected static final DurationUnit DURATION_UNIT_EDEFAULT = DurationUnit.MICROSECOND;

	/**
	 * The cached value of the '{@link #getDurationUnit() <em>Duration Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurationUnit()
	 * @generated
	 * @ordered
	 */
	protected DurationUnit durationUnit = DURATION_UNIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMonetaryUnit() <em>Monetary Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonetaryUnit()
	 * @generated
	 * @ordered
	 */
	protected static final MonetaryUnit MONETARY_UNIT_EDEFAULT = MonetaryUnit.DKK;

	/**
	 * The cached value of the '{@link #getMonetaryUnit() <em>Monetary Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonetaryUnit()
	 * @generated
	 * @ordered
	 */
	protected MonetaryUnit monetaryUnit = MONETARY_UNIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMeasurementKind() <em>Measurement Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasurementKind()
	 * @generated
	 * @ordered
	 */
	protected static final MeasurementKind MEASUREMENT_KIND_EDEFAULT = MeasurementKind.INCIDENCE;

	/**
	 * The cached value of the '{@link #getMeasurementKind() <em>Measurement Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasurementKind()
	 * @generated
	 * @ordered
	 */
	protected MeasurementKind measurementKind = MEASUREMENT_KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #getValueKind() <em>Value Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueKind()
	 * @generated
	 * @ordered
	 */
	protected static final ValueKind VALUE_KIND_EDEFAULT = ValueKind.COST;

	/**
	 * The cached value of the '{@link #getValueKind() <em>Value Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueKind()
	 * @generated
	 * @ordered
	 */
	protected ValueKind valueKind = VALUE_KIND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PropertiesPackage.Literals.PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertiesPackage.PROPERTY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(double newValue) {
		double oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertiesPackage.PROPERTY__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Kind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKind(Kind newKind) {
		Kind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertiesPackage.PROPERTY__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BenefitUnit getBenefitUnit() {
		return benefitUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBenefitUnit(BenefitUnit newBenefitUnit) {
		BenefitUnit oldBenefitUnit = benefitUnit;
		benefitUnit = newBenefitUnit == null ? BENEFIT_UNIT_EDEFAULT : newBenefitUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertiesPackage.PROPERTY__BENEFIT_UNIT, oldBenefitUnit, benefitUnit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DurationUnit getDurationUnit() {
		return durationUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDurationUnit(DurationUnit newDurationUnit) {
		DurationUnit oldDurationUnit = durationUnit;
		durationUnit = newDurationUnit == null ? DURATION_UNIT_EDEFAULT : newDurationUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertiesPackage.PROPERTY__DURATION_UNIT, oldDurationUnit, durationUnit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonetaryUnit getMonetaryUnit() {
		return monetaryUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonetaryUnit(MonetaryUnit newMonetaryUnit) {
		MonetaryUnit oldMonetaryUnit = monetaryUnit;
		monetaryUnit = newMonetaryUnit == null ? MONETARY_UNIT_EDEFAULT : newMonetaryUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertiesPackage.PROPERTY__MONETARY_UNIT, oldMonetaryUnit, monetaryUnit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MeasurementKind getMeasurementKind() {
		return measurementKind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeasurementKind(MeasurementKind newMeasurementKind) {
		MeasurementKind oldMeasurementKind = measurementKind;
		measurementKind = newMeasurementKind == null ? MEASUREMENT_KIND_EDEFAULT : newMeasurementKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertiesPackage.PROPERTY__MEASUREMENT_KIND, oldMeasurementKind, measurementKind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueKind getValueKind() {
		return valueKind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueKind(ValueKind newValueKind) {
		ValueKind oldValueKind = valueKind;
		valueKind = newValueKind == null ? VALUE_KIND_EDEFAULT : newValueKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertiesPackage.PROPERTY__VALUE_KIND, oldValueKind, valueKind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PropertiesPackage.PROPERTY__NAME:
				return getName();
			case PropertiesPackage.PROPERTY__VALUE:
				return getValue();
			case PropertiesPackage.PROPERTY__KIND:
				return getKind();
			case PropertiesPackage.PROPERTY__BENEFIT_UNIT:
				return getBenefitUnit();
			case PropertiesPackage.PROPERTY__DURATION_UNIT:
				return getDurationUnit();
			case PropertiesPackage.PROPERTY__MONETARY_UNIT:
				return getMonetaryUnit();
			case PropertiesPackage.PROPERTY__MEASUREMENT_KIND:
				return getMeasurementKind();
			case PropertiesPackage.PROPERTY__VALUE_KIND:
				return getValueKind();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PropertiesPackage.PROPERTY__NAME:
				setName((String)newValue);
				return;
			case PropertiesPackage.PROPERTY__VALUE:
				setValue((Double)newValue);
				return;
			case PropertiesPackage.PROPERTY__KIND:
				setKind((Kind)newValue);
				return;
			case PropertiesPackage.PROPERTY__BENEFIT_UNIT:
				setBenefitUnit((BenefitUnit)newValue);
				return;
			case PropertiesPackage.PROPERTY__DURATION_UNIT:
				setDurationUnit((DurationUnit)newValue);
				return;
			case PropertiesPackage.PROPERTY__MONETARY_UNIT:
				setMonetaryUnit((MonetaryUnit)newValue);
				return;
			case PropertiesPackage.PROPERTY__MEASUREMENT_KIND:
				setMeasurementKind((MeasurementKind)newValue);
				return;
			case PropertiesPackage.PROPERTY__VALUE_KIND:
				setValueKind((ValueKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PropertiesPackage.PROPERTY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case PropertiesPackage.PROPERTY__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case PropertiesPackage.PROPERTY__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case PropertiesPackage.PROPERTY__BENEFIT_UNIT:
				setBenefitUnit(BENEFIT_UNIT_EDEFAULT);
				return;
			case PropertiesPackage.PROPERTY__DURATION_UNIT:
				setDurationUnit(DURATION_UNIT_EDEFAULT);
				return;
			case PropertiesPackage.PROPERTY__MONETARY_UNIT:
				setMonetaryUnit(MONETARY_UNIT_EDEFAULT);
				return;
			case PropertiesPackage.PROPERTY__MEASUREMENT_KIND:
				setMeasurementKind(MEASUREMENT_KIND_EDEFAULT);
				return;
			case PropertiesPackage.PROPERTY__VALUE_KIND:
				setValueKind(VALUE_KIND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PropertiesPackage.PROPERTY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case PropertiesPackage.PROPERTY__VALUE:
				return value != VALUE_EDEFAULT;
			case PropertiesPackage.PROPERTY__KIND:
				return kind != KIND_EDEFAULT;
			case PropertiesPackage.PROPERTY__BENEFIT_UNIT:
				return benefitUnit != BENEFIT_UNIT_EDEFAULT;
			case PropertiesPackage.PROPERTY__DURATION_UNIT:
				return durationUnit != DURATION_UNIT_EDEFAULT;
			case PropertiesPackage.PROPERTY__MONETARY_UNIT:
				return monetaryUnit != MONETARY_UNIT_EDEFAULT;
			case PropertiesPackage.PROPERTY__MEASUREMENT_KIND:
				return measurementKind != MEASUREMENT_KIND_EDEFAULT;
			case PropertiesPackage.PROPERTY__VALUE_KIND:
				return valueKind != VALUE_KIND_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", value: ");
		result.append(value);
		result.append(", kind: ");
		result.append(kind);
		result.append(", benefitUnit: ");
		result.append(benefitUnit);
		result.append(", durationUnit: ");
		result.append(durationUnit);
		result.append(", monetaryUnit: ");
		result.append(monetaryUnit);
		result.append(", measurementKind: ");
		result.append(measurementKind);
		result.append(", valueKind: ");
		result.append(valueKind);
		result.append(')');
		return result.toString();
	}

} //PropertyImpl
