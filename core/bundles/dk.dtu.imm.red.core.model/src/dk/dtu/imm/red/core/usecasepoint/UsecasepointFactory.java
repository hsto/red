/**
 */
package dk.dtu.imm.red.core.usecasepoint;

import org.eclipse.emf.ecore.EFactory;

import dk.dtu.imm.red.core.project.Project;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage
 * @generated
 */
public interface UsecasepointFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UsecasepointFactory eINSTANCE = dk.dtu.imm.red.core.usecasepoint.impl.UsecasepointFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Usecase Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Usecase Point</em>'.
	 * @generated
	 */
	UsecasePoint createUsecasePoint();
	 
	 

	/**
	 * Returns a new object of class '<em>Management Factor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Management Factor</em>'.
	 * @generated
	 */
	ManagementFactor createManagementFactor();

	/**
	 * Returns a new object of class '<em>Weighted Factor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Weighted Factor</em>'.
	 * @generated
	 */
	WeightedFactor createWeightedFactor();


	/**
	 * Returns a new object of class '<em>Productivity Factor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Productivity Factor</em>'.
	 * @generated
	 */
	ProductivityFactor createProductivityFactor();

	/**
	 * Returns a new object of class '<em>Technology Factor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Technology Factor</em>'.
	 * @generated
	 */
	TechnologyFactor createTechnologyFactor();

	/**
	 * Returns a new object of class '<em>Requirement Factor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requirement Factor</em>'.
	 * @generated
	 */
	RequirementFactor createRequirementFactor();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UsecasepointPackage getUsecasepointPackage();


	WeightedFactor createTechnologyWeightedFactor();

} //UsecasepointFactory
