/**
 */
package dk.dtu.imm.red.core.usecasepoint.impl;

import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactor;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactorContainer; 
import java.util.Collection; 
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

 
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
 
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Weighted Factor Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.usecasepoint.impl.WeightedFactorContainerImpl#getWeightedFactors <em>Weighted Factors</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class WeightedFactorContainerImpl extends FactorImpl implements WeightedFactorContainer {
	/**
	 * The cached value of the '{@link #getWeightedFactors() <em>Weighted Factors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeightedFactors()
	 * @generated
	 * @ordered
	 */
	protected EList<WeightedFactor> weightedFactors;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WeightedFactorContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasepointPackage.Literals.WEIGHTED_FACTOR_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WeightedFactor> getWeightedFactors() {
		if (weightedFactors == null) {
			weightedFactors = new EObjectContainmentEList.Resolving<WeightedFactor>(WeightedFactor.class, this, UsecasepointPackage.WEIGHTED_FACTOR_CONTAINER__WEIGHTED_FACTORS);
		}
		return weightedFactors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasepointPackage.WEIGHTED_FACTOR_CONTAINER__WEIGHTED_FACTORS:
				return ((InternalEList<?>)getWeightedFactors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasepointPackage.WEIGHTED_FACTOR_CONTAINER__WEIGHTED_FACTORS:
				return getWeightedFactors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasepointPackage.WEIGHTED_FACTOR_CONTAINER__WEIGHTED_FACTORS:
				getWeightedFactors().clear();
				getWeightedFactors().addAll((Collection<? extends WeightedFactor>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasepointPackage.WEIGHTED_FACTOR_CONTAINER__WEIGHTED_FACTORS:
				getWeightedFactors().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasepointPackage.WEIGHTED_FACTOR_CONTAINER__WEIGHTED_FACTORS:
				return weightedFactors != null && !weightedFactors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //WeightedFactorContainerImpl
