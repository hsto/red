/**
 */
package dk.dtu.imm.red.core.usecasepoint;

import java.util.List;
import java.util.Map;

import dk.dtu.imm.red.core.element.*;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage#getRequirementFactor()
 * @model
 * @generated
 */
public interface RequirementFactor extends Factor {
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */ 
	double getSum(Map<String, List<Element>> elementMap);
	
} // RequirementFactor
