/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.workspace;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import dk.dtu.imm.red.core.element.group.GroupPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.workspace.WorkspaceFactory
 * @model kind="package"
 * @generated
 */
public interface WorkspacePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workspace";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.core.workspace";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workspace";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	WorkspacePackage eINSTANCE = dk.dtu.imm.red.core.workspace.impl.WorkspacePackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.workspace.impl.WorkspaceImpl <em>Workspace</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.workspace.impl.WorkspaceImpl
	 * @see dk.dtu.imm.red.core.workspace.impl.WorkspacePackageImpl#getWorkspace()
	 * @generated
	 */
	int WORKSPACE = 0;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__ICON_URI = GroupPackage.GROUP__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__ICON = GroupPackage.GROUP__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__LABEL = GroupPackage.GROUP__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__NAME = GroupPackage.GROUP__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__ELEMENT_KIND = GroupPackage.GROUP__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__DESCRIPTION = GroupPackage.GROUP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__PRIORITY = GroupPackage.GROUP__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__COMMENTLIST = GroupPackage.GROUP__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__TIME_CREATED = GroupPackage.GROUP__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__LAST_MODIFIED = GroupPackage.GROUP__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__LAST_MODIFIED_BY = GroupPackage.GROUP__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__CREATOR = GroupPackage.GROUP__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__VERSION = GroupPackage.GROUP__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__VISIBLE_ID = GroupPackage.GROUP__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__UNIQUE_ID = GroupPackage.GROUP__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__RELATES_TO = GroupPackage.GROUP__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__RELATED_BY = GroupPackage.GROUP__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__PARENT = GroupPackage.GROUP__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__URI = GroupPackage.GROUP__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__WORK_PACKAGE = GroupPackage.GROUP__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__CHANGE_LIST = GroupPackage.GROUP__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__RESPONSIBLE_USER = GroupPackage.GROUP__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__DEADLINE = GroupPackage.GROUP__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__LOCK_STATUS = GroupPackage.GROUP__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__LOCK_PASSWORD = GroupPackage.GROUP__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__ESTIMATED_COMPLEXITY = GroupPackage.GROUP__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__COST = GroupPackage.GROUP__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__BENEFIT = GroupPackage.GROUP__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__RISK = GroupPackage.GROUP__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__LIFE_CYCLE_PHASE = GroupPackage.GROUP__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__STATE = GroupPackage.GROUP__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__MANAGEMENT_DECISION = GroupPackage.GROUP__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__QA_ASSESSMENT = GroupPackage.GROUP__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__MANAGEMENT_DISCUSSION = GroupPackage.GROUP__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__DELETION_LISTENERS = GroupPackage.GROUP__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__QUANTITIES = GroupPackage.GROUP__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__CONTENTS = GroupPackage.GROUP__CONTENTS;

	/**
	 * The feature id for the '<em><b>Recently Opened Files</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__RECENTLY_OPENED_FILES = GroupPackage.GROUP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Currently Opened Files</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__CURRENTLY_OPENED_FILES = GroupPackage.GROUP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Recent Files Max Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__RECENT_FILES_MAX_LENGTH = GroupPackage.GROUP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__PASSWORD = GroupPackage.GROUP_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Currently Opened Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSPACE__CURRENTLY_OPENED_PROJECTS = GroupPackage.GROUP_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Workspace</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int WORKSPACE_FEATURE_COUNT = GroupPackage.GROUP_FEATURE_COUNT + 5;

	/**
	 * Returns the meta object for class '
	 * {@link dk.dtu.imm.red.core.workspace.Workspace <em>Workspace</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Workspace</em>'.
	 * @see dk.dtu.imm.red.core.workspace.Workspace
	 * @generated
	 */
	EClass getWorkspace();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.core.workspace.Workspace#getRecentlyOpenedFiles <em>Recently Opened Files</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Recently Opened Files</em>'.
	 * @see dk.dtu.imm.red.core.workspace.Workspace#getRecentlyOpenedFiles()
	 * @see #getWorkspace()
	 * @generated
	 */
	EReference getWorkspace_RecentlyOpenedFiles();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.core.workspace.Workspace#getCurrentlyOpenedFiles <em>Currently Opened Files</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Currently Opened Files</em>'.
	 * @see dk.dtu.imm.red.core.workspace.Workspace#getCurrentlyOpenedFiles()
	 * @see #getWorkspace()
	 * @generated
	 */
	EReference getWorkspace_CurrentlyOpenedFiles();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.workspace.Workspace#getRecentFilesMaxLength <em>Recent Files Max Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Recent Files Max Length</em>'.
	 * @see dk.dtu.imm.red.core.workspace.Workspace#getRecentFilesMaxLength()
	 * @see #getWorkspace()
	 * @generated
	 */
	EAttribute getWorkspace_RecentFilesMaxLength();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.workspace.Workspace#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see dk.dtu.imm.red.core.workspace.Workspace#getPassword()
	 * @see #getWorkspace()
	 * @generated
	 */
	EAttribute getWorkspace_Password();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.core.workspace.Workspace#getCurrentlyOpenedProjects <em>Currently Opened Projects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Currently Opened Projects</em>'.
	 * @see dk.dtu.imm.red.core.workspace.Workspace#getCurrentlyOpenedProjects()
	 * @see #getWorkspace()
	 * @generated
	 */
	EReference getWorkspace_CurrentlyOpenedProjects();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkspaceFactory getWorkspaceFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '
		 * {@link dk.dtu.imm.red.core.workspace.implementation.WorkspaceImpl
		 * <em>Workspace</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see dk.dtu.imm.red.core.workspace.implementation.WorkspaceImpl
		 * @see dk.dtu.imm.red.core.workspace.implementation.WorkspacePackageImpl#getWorkspace()
		 * @generated
		 */
		EClass WORKSPACE = eINSTANCE.getWorkspace();
		/**
		 * The meta object literal for the '<em><b>Recently Opened Files</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORKSPACE__RECENTLY_OPENED_FILES = eINSTANCE.getWorkspace_RecentlyOpenedFiles();
		/**
		 * The meta object literal for the '<em><b>Currently Opened Files</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORKSPACE__CURRENTLY_OPENED_FILES = eINSTANCE.getWorkspace_CurrentlyOpenedFiles();
		/**
		 * The meta object literal for the '<em><b>Recent Files Max Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORKSPACE__RECENT_FILES_MAX_LENGTH = eINSTANCE.getWorkspace_RecentFilesMaxLength();
		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORKSPACE__PASSWORD = eINSTANCE.getWorkspace_Password();
		/**
		 * The meta object literal for the '<em><b>Currently Opened Projects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORKSPACE__CURRENTLY_OPENED_PROJECTS = eINSTANCE.getWorkspace_CurrentlyOpenedProjects();

	}

} // WorkspacePackage
