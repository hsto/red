/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.group;

import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.Element;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Group</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.group.Group#getContents <em>Contents</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.group.GroupPackage#getGroup()
 * @model abstract="true"
 * @generated
 */
public interface Group extends Element {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValidContent(Element content);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model typeDataType="dk.dtu.imm.red.core.JavaClass"
	 * @generated
	 */
	boolean isValidContent(Class type);

	/**
	 * Returns the value of the '<em><b>Contents</b></em>' containment reference list.
	 * The list contents are of type {@link dk.dtu.imm.red.core.element.Element}.
	 * It is bidirectional and its opposite is '{@link dk.dtu.imm.red.core.element.Element#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contents</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contents</em>' containment reference list.
	 * @see dk.dtu.imm.red.core.element.group.GroupPackage#getGroup_Contents()
	 * @see dk.dtu.imm.red.core.element.Element#getParent
	 * @model opposite="parent" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<Element> getContents();

} // Group
