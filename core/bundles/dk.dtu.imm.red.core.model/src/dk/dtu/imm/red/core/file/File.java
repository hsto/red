/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.file;

import org.eclipse.emf.ecore.resource.ResourceSet;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.text.Text;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.file.File#getLongDescription <em>Long Description</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.file.File#getLicense <em>License</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.file.FilePackage#getFile()
 * @model
 * @generated
 */
public interface File extends Group {

	/**
	 * Returns the value of the '<em><b>Long Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Long Description</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Long Description</em>' containment reference.
	 * @see #setLongDescription(Text)
	 * @see dk.dtu.imm.red.core.file.FilePackage#getFile_LongDescription()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getLongDescription();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.file.File#getLongDescription <em>Long Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Long Description</em>' containment reference.
	 * @see #getLongDescription()
	 * @generated
	 */
	void setLongDescription(Text value);

	/**
	 * Returns the value of the '<em><b>License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>License</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>License</em>' attribute.
	 * @see #setLicense(String)
	 * @see dk.dtu.imm.red.core.file.FilePackage#getFile_License()
	 * @model
	 * @generated
	 */
	String getLicense();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.file.File#getLicense <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>License</em>' attribute.
	 * @see #getLicense()
	 * @generated
	 */
	void setLicense(String value);

	
} // File
