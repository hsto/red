/**
 */
package dk.dtu.imm.red.core.element.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Divisional Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.unit.DivisionalUnit#getNominatorUnit <em>Nominator Unit</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.unit.DivisionalUnit#getDenominatorUnit <em>Denominator Unit</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.unit.UnitPackage#getDivisionalUnit()
 * @model
 * @generated
 */
public interface DivisionalUnit extends Unit {
	/**
	 * Returns the value of the '<em><b>Nominator Unit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nominator Unit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nominator Unit</em>' containment reference.
	 * @see #setNominatorUnit(Unit)
	 * @see dk.dtu.imm.red.core.element.unit.UnitPackage#getDivisionalUnit_NominatorUnit()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Unit getNominatorUnit();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.unit.DivisionalUnit#getNominatorUnit <em>Nominator Unit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nominator Unit</em>' containment reference.
	 * @see #getNominatorUnit()
	 * @generated
	 */
	void setNominatorUnit(Unit value);

	/**
	 * Returns the value of the '<em><b>Denominator Unit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Denominator Unit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Denominator Unit</em>' containment reference.
	 * @see #setDenominatorUnit(Unit)
	 * @see dk.dtu.imm.red.core.element.unit.UnitPackage#getDivisionalUnit_DenominatorUnit()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Unit getDenominatorUnit();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.unit.DivisionalUnit#getDenominatorUnit <em>Denominator Unit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Denominator Unit</em>' containment reference.
	 * @see #getDenominatorUnit()
	 * @generated
	 */
	void setDenominatorUnit(Unit value);

} // DivisionalUnit
