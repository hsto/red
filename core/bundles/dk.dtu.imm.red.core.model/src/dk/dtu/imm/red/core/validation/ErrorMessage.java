/**
 */
package dk.dtu.imm.red.core.validation;

import dk.dtu.imm.red.core.element.Element;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Error Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.validation.ErrorMessage#getMessage <em>Message</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.validation.ErrorMessage#getSeverity <em>Severity</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.validation.ErrorMessage#getElementReference <em>Element Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see dk.dtu.imm.red.core.validation.ValidationPackage#getErrorMessage()
 * @model
 * @generated
 */
public interface ErrorMessage extends EObject {
	/**
	 * Returns the value of the '<em><b>Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' attribute.
	 * @see #setMessage(String)
	 * @see dk.dtu.imm.red.core.validation.ValidationPackage#getErrorMessage_Message()
	 * @model
	 * @generated
	 */
	String getMessage();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.validation.ErrorMessage#getMessage <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' attribute.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(String value);

	/**
	 * Returns the value of the '<em><b>Severity</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.validation.Severity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Severity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Severity</em>' attribute.
	 * @see dk.dtu.imm.red.core.validation.Severity
	 * @see #setSeverity(Severity)
	 * @see dk.dtu.imm.red.core.validation.ValidationPackage#getErrorMessage_Severity()
	 * @model
	 * @generated
	 */
	Severity getSeverity();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.validation.ErrorMessage#getSeverity <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Severity</em>' attribute.
	 * @see dk.dtu.imm.red.core.validation.Severity
	 * @see #getSeverity()
	 * @generated
	 */
	void setSeverity(Severity value);

	/**
	 * Returns the value of the '<em><b>Element Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Reference</em>' reference.
	 * @see #setElementReference(Element)
	 * @see dk.dtu.imm.red.core.validation.ValidationPackage#getErrorMessage_ElementReference()
	 * @model
	 * @generated
	 */
	Element getElementReference();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.validation.ErrorMessage#getElementReference <em>Element Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Reference</em>' reference.
	 * @see #getElementReference()
	 * @generated
	 */
	void setElementReference(Element value);

} // ErrorMessage
