/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Search Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.SearchResult#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.SearchResult#getStartIndex <em>Start Index</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.SearchResult#getEndIndex <em>End Index</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.SearchResult#getElement <em>Element</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchResult()
 * @model
 * @generated
 */
public interface SearchResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' reference.
	 * @see #setAttribute(EStructuralFeature)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchResult_Attribute()
	 * @model
	 * @generated
	 */
	EStructuralFeature getAttribute();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.SearchResult#getAttribute <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute</em>' reference.
	 * @see #getAttribute()
	 * @generated
	 */
	void setAttribute(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Start Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Index</em>' attribute.
	 * @see #setStartIndex(int)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchResult_StartIndex()
	 * @model
	 * @generated
	 */
	int getStartIndex();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.SearchResult#getStartIndex <em>Start Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Index</em>' attribute.
	 * @see #getStartIndex()
	 * @generated
	 */
	void setStartIndex(int value);

	/**
	 * Returns the value of the '<em><b>End Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Index</em>' attribute.
	 * @see #setEndIndex(int)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchResult_EndIndex()
	 * @model
	 * @generated
	 */
	int getEndIndex();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.SearchResult#getEndIndex <em>End Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Index</em>' attribute.
	 * @see #getEndIndex()
	 * @generated
	 */
	void setEndIndex(int value);

	/**
	 * Returns the value of the '<em><b>Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element</em>' reference.
	 * @see #setElement(Element)
	 * @see dk.dtu.imm.red.core.element.ElementPackage#getSearchResult_Element()
	 * @model
	 * @generated
	 */
	Element getElement();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.SearchResult#getElement <em>Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element</em>' reference.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(Element value);

} // SearchResult
