package dk.dtu.imm.red.core.element.impl;

 
public abstract class CellHandler<T> { 
	public abstract Object getAttributeValue(T item); 	
	public abstract void setAttributeValue(T item, Object value); 
	public String[] getComboValues(Object item) {
		return null;
	}
}
 