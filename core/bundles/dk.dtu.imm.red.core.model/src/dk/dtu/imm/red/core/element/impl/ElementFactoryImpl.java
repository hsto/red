/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element.impl;

import dk.dtu.imm.red.core.element.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import dk.dtu.imm.red.core.element.CaptionedImage;
import dk.dtu.imm.red.core.element.CaptionedImageList;
import dk.dtu.imm.red.core.element.ComplexityType;
import dk.dtu.imm.red.core.element.CustomAttribute;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.LifeCyclePhase;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.element.ManagementDecision;
import dk.dtu.imm.red.core.element.Priority;
import dk.dtu.imm.red.core.element.QAAssessment;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.State;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class ElementFactoryImpl extends EFactoryImpl implements ElementFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static ElementFactory init() {
		try {
			ElementFactory theElementFactory = (ElementFactory)EPackage.Registry.INSTANCE.getEFactory(ElementPackage.eNS_URI);
			if (theElementFactory != null) {
				return theElementFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ElementFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public ElementFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ElementPackage.CUSTOM_ATTRIBUTE: return createCustomAttribute();
			case ElementPackage.SEARCH_PARAMETER: return createSearchParameter();
			case ElementPackage.SEARCH_RESULT: return createSearchResult();
			case ElementPackage.SEARCH_RESULT_WRAPPER: return createSearchResultWrapper();
			case ElementPackage.CAPTIONED_IMAGE: return createCaptionedImage();
			case ElementPackage.CAPTIONED_IMAGE_LIST: return createCaptionedImageList();
			case ElementPackage.DELETION_LISTENER: return createDeletionListener();
			case ElementPackage.QUANTITY_ITEM: return createQuantityItem();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ElementPackage.PRIORITY:
				return createPriorityFromString(eDataType, initialValue);
			case ElementPackage.LOCK_TYPE:
				return createLockTypeFromString(eDataType, initialValue);
			case ElementPackage.COMPLEXITY_TYPE:
				return createComplexityTypeFromString(eDataType, initialValue);
			case ElementPackage.LIFE_CYCLE_PHASE:
				return createLifeCyclePhaseFromString(eDataType, initialValue);
			case ElementPackage.STATE:
				return createStateFromString(eDataType, initialValue);
			case ElementPackage.MANAGEMENT_DECISION:
				return createManagementDecisionFromString(eDataType, initialValue);
			case ElementPackage.QA_ASSESSMENT:
				return createQAAssessmentFromString(eDataType, initialValue);
			case ElementPackage.QUANTITY_LABEL:
				return createQuantityLabelFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ElementPackage.PRIORITY:
				return convertPriorityToString(eDataType, instanceValue);
			case ElementPackage.LOCK_TYPE:
				return convertLockTypeToString(eDataType, instanceValue);
			case ElementPackage.COMPLEXITY_TYPE:
				return convertComplexityTypeToString(eDataType, instanceValue);
			case ElementPackage.LIFE_CYCLE_PHASE:
				return convertLifeCyclePhaseToString(eDataType, instanceValue);
			case ElementPackage.STATE:
				return convertStateToString(eDataType, instanceValue);
			case ElementPackage.MANAGEMENT_DECISION:
				return convertManagementDecisionToString(eDataType, instanceValue);
			case ElementPackage.QA_ASSESSMENT:
				return convertQAAssessmentToString(eDataType, instanceValue);
			case ElementPackage.QUANTITY_LABEL:
				return convertQuantityLabelToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public <T> CustomAttribute<T> createCustomAttribute() {
		CustomAttributeImpl<T> customAttribute = new CustomAttributeImpl<T>();
		return customAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SearchParameter createSearchParameter() {
		SearchParameterImpl searchParameter = new SearchParameterImpl();
		return searchParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SearchResult createSearchResult() {
		SearchResultImpl searchResult = new SearchResultImpl();
		return searchResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SearchResultWrapper createSearchResultWrapper() {
		SearchResultWrapperImpl searchResultWrapper = new SearchResultWrapperImpl();
		return searchResultWrapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaptionedImage createCaptionedImage() {
		CaptionedImageImpl captionedImage = new CaptionedImageImpl();
		return captionedImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaptionedImageList createCaptionedImageList() {
		CaptionedImageListImpl captionedImageList = new CaptionedImageListImpl();
		return captionedImageList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeletionListener createDeletionListener() {
		DeletionListenerImpl deletionListener = new DeletionListenerImpl();
		return deletionListener;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantityItem createQuantityItem() {
		QuantityItemImpl quantityItem = new QuantityItemImpl();
		return quantityItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Priority createPriorityFromString(EDataType eDataType, String initialValue) {
		Priority result = Priority.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPriorityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockType createLockTypeFromString(EDataType eDataType, String initialValue) {
		LockType result = LockType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLockTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexityType createComplexityTypeFromString(EDataType eDataType, String initialValue) {
		ComplexityType result = ComplexityType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertComplexityTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LifeCyclePhase createLifeCyclePhaseFromString(EDataType eDataType, String initialValue) {
		LifeCyclePhase result = LifeCyclePhase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLifeCyclePhaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State createStateFromString(EDataType eDataType, String initialValue) {
		State result = State.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ManagementDecision createManagementDecisionFromString(EDataType eDataType, String initialValue) {
		ManagementDecision result = ManagementDecision.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertManagementDecisionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QAAssessment createQAAssessmentFromString(EDataType eDataType, String initialValue) {
		QAAssessment result = QAAssessment.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertQAAssessmentToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantityLabel createQuantityLabelFromString(EDataType eDataType, String initialValue) {
		QuantityLabel result = QuantityLabel.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertQuantityLabelToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElementPackage getElementPackage() {
		return (ElementPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ElementPackage getPackage() {
		return ElementPackage.eINSTANCE;
	}

} // ElementFactoryImpl
