/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package dk.dtu.imm.red.core.element;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.dtu.imm.red.core.element.ElementFactory
 * @model kind="package"
 * @generated
 */
public interface ElementPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "element";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "dk.dtu.imm.red.core.element";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "element";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	ElementPackage eINSTANCE = dk.dtu.imm.red.core.element.impl.ElementPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.Checkable <em>Checkable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.Checkable
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getCheckable()
	 * @generated
	 */
	int CHECKABLE = 9;

	/**
	 * The number of structural features of the '<em>Checkable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKABLE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.ElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.ElementImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getElement()
	 * @generated
	 */
	int ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ELEMENT__ICON_URI = CHECKABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ELEMENT__ICON = CHECKABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__LABEL = CHECKABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__NAME = CHECKABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__ELEMENT_KIND = CHECKABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__DESCRIPTION = CHECKABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__PRIORITY = CHECKABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__COMMENTLIST = CHECKABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ELEMENT__TIME_CREATED = CHECKABLE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__LAST_MODIFIED = CHECKABLE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__LAST_MODIFIED_BY = CHECKABLE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ELEMENT__CREATOR = CHECKABLE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__VERSION = CHECKABLE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__VISIBLE_ID = CHECKABLE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ELEMENT__UNIQUE_ID = CHECKABLE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__RELATES_TO = CHECKABLE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ELEMENT__RELATED_BY = CHECKABLE_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ELEMENT__PARENT = CHECKABLE_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__URI = CHECKABLE_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__WORK_PACKAGE = CHECKABLE_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__CHANGE_LIST = CHECKABLE_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__RESPONSIBLE_USER = CHECKABLE_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__DEADLINE = CHECKABLE_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__LOCK_STATUS = CHECKABLE_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__LOCK_PASSWORD = CHECKABLE_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__ESTIMATED_COMPLEXITY = CHECKABLE_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__COST = CHECKABLE_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__BENEFIT = CHECKABLE_FEATURE_COUNT + 27;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__RISK = CHECKABLE_FEATURE_COUNT + 28;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__LIFE_CYCLE_PHASE = CHECKABLE_FEATURE_COUNT + 29;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__STATE = CHECKABLE_FEATURE_COUNT + 30;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__MANAGEMENT_DECISION = CHECKABLE_FEATURE_COUNT + 31;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__QA_ASSESSMENT = CHECKABLE_FEATURE_COUNT + 32;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__MANAGEMENT_DISCUSSION = CHECKABLE_FEATURE_COUNT + 33;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__DELETION_LISTENERS = CHECKABLE_FEATURE_COUNT + 34;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__QUANTITIES = CHECKABLE_FEATURE_COUNT + 35;

	/**
	 * The number of structural features of the '<em>Element</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATURE_COUNT = CHECKABLE_FEATURE_COUNT + 36;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.CustomAttributeImpl <em>Custom Attribute</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.CustomAttributeImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getCustomAttribute()
	 * @generated
	 */
	int CUSTOM_ATTRIBUTE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CUSTOM_ATTRIBUTE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CUSTOM_ATTRIBUTE__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Custom Attribute</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_ATTRIBUTE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.VisitorImpl <em>Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.VisitorImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getVisitor()
	 * @generated
	 */
	int VISITOR = 2;

	/**
	 * The number of structural features of the '<em>Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.SearchParameterImpl <em>Search Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.SearchParameterImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getSearchParameter()
	 * @generated
	 */
	int SEARCH_PARAMETER = 3;

	/**
	 * The feature id for the '<em><b>Search String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_PARAMETER__SEARCH_STRING = 0;

	/**
	 * The number of structural features of the '<em>Search Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_PARAMETER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.SearchResultImpl <em>Search Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.SearchResultImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getSearchResult()
	 * @generated
	 */
	int SEARCH_RESULT = 4;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_RESULT__ATTRIBUTE = 0;

	/**
	 * The feature id for the '<em><b>Start Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_RESULT__START_INDEX = 1;

	/**
	 * The feature id for the '<em><b>End Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_RESULT__END_INDEX = 2;

	/**
	 * The feature id for the '<em><b>Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_RESULT__ELEMENT = 3;

	/**
	 * The number of structural features of the '<em>Search Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_RESULT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.SearchResultWrapperImpl <em>Search Result Wrapper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.SearchResultWrapperImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getSearchResultWrapper()
	 * @generated
	 */
	int SEARCH_RESULT_WRAPPER = 5;

	/**
	 * The feature id for the '<em><b>Element Results</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_RESULT_WRAPPER__ELEMENT_RESULTS = 0;

	/**
	 * The feature id for the '<em><b>Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_RESULT_WRAPPER__ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Child Results</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_RESULT_WRAPPER__CHILD_RESULTS = 2;

	/**
	 * The number of structural features of the '<em>Search Result Wrapper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEARCH_RESULT_WRAPPER_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.CaptionedImageImpl <em>Captioned Image</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.CaptionedImageImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getCaptionedImage()
	 * @generated
	 */
	int CAPTIONED_IMAGE = 6;

	/**
	 * The feature id for the '<em><b>Icon URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__ICON_URI = ELEMENT__ICON_URI;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__ICON = ELEMENT__ICON;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__LABEL = ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__NAME = ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Element Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__ELEMENT_KIND = ELEMENT__ELEMENT_KIND;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__DESCRIPTION = ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__PRIORITY = ELEMENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Commentlist</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__COMMENTLIST = ELEMENT__COMMENTLIST;

	/**
	 * The feature id for the '<em><b>Time Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__TIME_CREATED = ELEMENT__TIME_CREATED;

	/**
	 * The feature id for the '<em><b>Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__LAST_MODIFIED = ELEMENT__LAST_MODIFIED;

	/**
	 * The feature id for the '<em><b>Last Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__LAST_MODIFIED_BY = ELEMENT__LAST_MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__CREATOR = ELEMENT__CREATOR;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__VERSION = ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Visible ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__VISIBLE_ID = ELEMENT__VISIBLE_ID;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__UNIQUE_ID = ELEMENT__UNIQUE_ID;

	/**
	 * The feature id for the '<em><b>Relates To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__RELATES_TO = ELEMENT__RELATES_TO;

	/**
	 * The feature id for the '<em><b>Related By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__RELATED_BY = ELEMENT__RELATED_BY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__PARENT = ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__URI = ELEMENT__URI;

	/**
	 * The feature id for the '<em><b>Work Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__WORK_PACKAGE = ELEMENT__WORK_PACKAGE;

	/**
	 * The feature id for the '<em><b>Change List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__CHANGE_LIST = ELEMENT__CHANGE_LIST;

	/**
	 * The feature id for the '<em><b>Responsible User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__RESPONSIBLE_USER = ELEMENT__RESPONSIBLE_USER;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__DEADLINE = ELEMENT__DEADLINE;

	/**
	 * The feature id for the '<em><b>Lock Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__LOCK_STATUS = ELEMENT__LOCK_STATUS;

	/**
	 * The feature id for the '<em><b>Lock Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__LOCK_PASSWORD = ELEMENT__LOCK_PASSWORD;

	/**
	 * The feature id for the '<em><b>Estimated Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__ESTIMATED_COMPLEXITY = ELEMENT__ESTIMATED_COMPLEXITY;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__COST = ELEMENT__COST;

	/**
	 * The feature id for the '<em><b>Benefit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__BENEFIT = ELEMENT__BENEFIT;

	/**
	 * The feature id for the '<em><b>Risk</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__RISK = ELEMENT__RISK;

	/**
	 * The feature id for the '<em><b>Life Cycle Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__LIFE_CYCLE_PHASE = ELEMENT__LIFE_CYCLE_PHASE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__STATE = ELEMENT__STATE;

	/**
	 * The feature id for the '<em><b>Management Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__MANAGEMENT_DECISION = ELEMENT__MANAGEMENT_DECISION;

	/**
	 * The feature id for the '<em><b>Qa Assessment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__QA_ASSESSMENT = ELEMENT__QA_ASSESSMENT;

	/**
	 * The feature id for the '<em><b>Management Discussion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__MANAGEMENT_DISCUSSION = ELEMENT__MANAGEMENT_DISCUSSION;

	/**
	 * The feature id for the '<em><b>Deletion Listeners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__DELETION_LISTENERS = ELEMENT__DELETION_LISTENERS;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__QUANTITIES = ELEMENT__QUANTITIES;

	/**
	 * The feature id for the '<em><b>Caption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__CAPTION = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Image Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE__IMAGE_DATA = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Captioned Image</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.CaptionedImageListImpl <em>Captioned Image List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.CaptionedImageListImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getCaptionedImageList()
	 * @generated
	 */
	int CAPTIONED_IMAGE_LIST = 7;

	/**
	 * The feature id for the '<em><b>Captioned Image</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE_LIST__CAPTIONED_IMAGE = 0;

	/**
	 * The number of structural features of the '<em>Captioned Image List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPTIONED_IMAGE_LIST_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.DeletionListenerImpl <em>Deletion Listener</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.DeletionListenerImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getDeletionListener()
	 * @generated
	 */
	int DELETION_LISTENER = 8;

	/**
	 * The feature id for the '<em><b>Hosting Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETION_LISTENER__HOSTING_OBJECT = 0;

	/**
	 * The feature id for the '<em><b>Hosting Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETION_LISTENER__HOSTING_FEATURE = 1;

	/**
	 * The feature id for the '<em><b>Handled Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETION_LISTENER__HANDLED_OBJECT = 2;

	/**
	 * The number of structural features of the '<em>Deletion Listener</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETION_LISTENER_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.impl.QuantityItemImpl <em>Quantity Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.impl.QuantityItemImpl
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getQuantityItem()
	 * @generated
	 */
	int QUANTITY_ITEM = 10;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ITEM__LABEL = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ITEM__VALUE = 1;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ITEM__UNIT = 2;

	/**
	 * The number of structural features of the '<em>Quantity Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ITEM_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.Priority <em>Priority</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.Priority
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getPriority()
	 * @generated
	 */
	int PRIORITY = 11;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.LockType <em>Lock Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.LockType
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getLockType()
	 * @generated
	 */
	int LOCK_TYPE = 12;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.ComplexityType <em>Complexity Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.ComplexityType
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getComplexityType()
	 * @generated
	 */
	int COMPLEXITY_TYPE = 13;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.LifeCyclePhase <em>Life Cycle Phase</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.LifeCyclePhase
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getLifeCyclePhase()
	 * @generated
	 */
	int LIFE_CYCLE_PHASE = 14;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.State <em>State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.State
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getState()
	 * @generated
	 */
	int STATE = 15;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.ManagementDecision <em>Management Decision</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.ManagementDecision
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getManagementDecision()
	 * @generated
	 */
	int MANAGEMENT_DECISION = 16;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.QAAssessment <em>QA Assessment</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.QAAssessment
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getQAAssessment()
	 * @generated
	 */
	int QA_ASSESSMENT = 17;

	/**
	 * The meta object id for the '{@link dk.dtu.imm.red.core.element.QuantityLabel <em>Quantity Label</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.dtu.imm.red.core.element.QuantityLabel
	 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getQuantityLabel()
	 * @generated
	 */
	int QUANTITY_LABEL = 18;

	/**
	 * Returns the meta object for class '
	 * {@link dk.dtu.imm.red.core.element.Element <em>Element</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Element</em>'.
	 * @see dk.dtu.imm.red.core.element.Element
	 * @generated
	 */
	EClass getElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getIconURI <em>Icon URI</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Icon URI</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getIconURI()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_IconURI();

	/**
	 * Returns the meta object for the attribute '
	 * {@link dk.dtu.imm.red.core.element.Element#getIcon <em>Icon</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Icon</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getIcon()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Icon();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getLabel()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Label();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getName()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getElementKind <em>Element Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Kind</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getElementKind()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_ElementKind();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getDescription()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Description();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getPriority <em>Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Priority</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getPriority()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Priority();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.Element#getCommentlist <em>Commentlist</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Commentlist</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getCommentlist()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Commentlist();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getTimeCreated <em>Time Created</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Created</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getTimeCreated()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_TimeCreated();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getLastModified <em>Last Modified</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Modified</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getLastModified()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_LastModified();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.core.element.Element#getLastModifiedBy <em>Last Modified By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Last Modified By</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getLastModifiedBy()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_LastModifiedBy();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.Element#getCreator <em>Creator</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Creator</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getCreator()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Creator();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getVersion()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Version();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.Element#getVisibleID <em>Visible ID</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Visible ID</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getVisibleID()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_VisibleID();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getUniqueID <em>Unique ID</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unique ID</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getUniqueID()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_UniqueID();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.core.element.Element#getRelatesTo <em>Relates To</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relates To</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getRelatesTo()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_RelatesTo();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.core.element.Element#getRelatedBy <em>Related By</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Related By</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getRelatedBy()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_RelatedBy();

	/**
	 * Returns the meta object for the container reference '{@link dk.dtu.imm.red.core.element.Element#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getParent()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Parent();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getUri()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Uri();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getWorkPackage <em>Work Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Work Package</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getWorkPackage()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_WorkPackage();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.Element#getChangeList <em>Change List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Change List</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getChangeList()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_ChangeList();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.Element#getResponsibleUser <em>Responsible User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Responsible User</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getResponsibleUser()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_ResponsibleUser();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getDeadline()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getLockStatus <em>Lock Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lock Status</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getLockStatus()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_LockStatus();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getLockPassword <em>Lock Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lock Password</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getLockPassword()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_LockPassword();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getEstimatedComplexity <em>Estimated Complexity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Estimated Complexity</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getEstimatedComplexity()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_EstimatedComplexity();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.Element#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cost</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getCost()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Cost();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.Element#getBenefit <em>Benefit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Benefit</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getBenefit()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Benefit();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getRisk <em>Risk</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Risk</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getRisk()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Risk();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getLifeCyclePhase <em>Life Cycle Phase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Life Cycle Phase</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getLifeCyclePhase()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_LifeCyclePhase();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getState()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_State();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getManagementDecision <em>Management Decision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Management Decision</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getManagementDecision()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_ManagementDecision();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.Element#getQaAssessment <em>Qa Assessment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Qa Assessment</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getQaAssessment()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_QaAssessment();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.Element#getManagementDiscussion <em>Management Discussion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Management Discussion</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getManagementDiscussion()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_ManagementDiscussion();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.core.element.Element#getDeletionListeners <em>Deletion Listeners</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Deletion Listeners</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getDeletionListeners()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_DeletionListeners();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.core.element.Element#getQuantities <em>Quantities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Quantities</em>'.
	 * @see dk.dtu.imm.red.core.element.Element#getQuantities()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Quantities();

	/**
	 * Returns the meta object for class '
	 * {@link dk.dtu.imm.red.core.element.CustomAttribute
	 * <em>Custom Attribute</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Custom Attribute</em>'.
	 * @see dk.dtu.imm.red.core.element.CustomAttribute
	 * @generated
	 */
	EClass getCustomAttribute();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.CustomAttribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.dtu.imm.red.core.element.CustomAttribute#getName()
	 * @see #getCustomAttribute()
	 * @generated
	 */
	EAttribute getCustomAttribute_Name();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.CustomAttribute#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see dk.dtu.imm.red.core.element.CustomAttribute#getValue()
	 * @see #getCustomAttribute()
	 * @generated
	 */
	EAttribute getCustomAttribute_Value();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.Visitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitor</em>'.
	 * @see dk.dtu.imm.red.core.element.Visitor
	 * @generated
	 */
	EClass getVisitor();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.SearchParameter <em>Search Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Search Parameter</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchParameter
	 * @generated
	 */
	EClass getSearchParameter();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.SearchParameter#getSearchString <em>Search String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Search String</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchParameter#getSearchString()
	 * @see #getSearchParameter()
	 * @generated
	 */
	EAttribute getSearchParameter_SearchString();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.SearchResult <em>Search Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Search Result</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchResult
	 * @generated
	 */
	EClass getSearchResult();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.core.element.SearchResult#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchResult#getAttribute()
	 * @see #getSearchResult()
	 * @generated
	 */
	EReference getSearchResult_Attribute();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.SearchResult#getStartIndex <em>Start Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Index</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchResult#getStartIndex()
	 * @see #getSearchResult()
	 * @generated
	 */
	EAttribute getSearchResult_StartIndex();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.SearchResult#getEndIndex <em>End Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Index</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchResult#getEndIndex()
	 * @see #getSearchResult()
	 * @generated
	 */
	EAttribute getSearchResult_EndIndex();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.core.element.SearchResult#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Element</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchResult#getElement()
	 * @see #getSearchResult()
	 * @generated
	 */
	EReference getSearchResult_Element();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.SearchResultWrapper <em>Search Result Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Search Result Wrapper</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchResultWrapper
	 * @generated
	 */
	EClass getSearchResultWrapper();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.core.element.SearchResultWrapper#getElementResults <em>Element Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Element Results</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchResultWrapper#getElementResults()
	 * @see #getSearchResultWrapper()
	 * @generated
	 */
	EReference getSearchResultWrapper_ElementResults();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.core.element.SearchResultWrapper#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Element</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchResultWrapper#getElement()
	 * @see #getSearchResultWrapper()
	 * @generated
	 */
	EReference getSearchResultWrapper_Element();

	/**
	 * Returns the meta object for the reference list '{@link dk.dtu.imm.red.core.element.SearchResultWrapper#getChildResults <em>Child Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Child Results</em>'.
	 * @see dk.dtu.imm.red.core.element.SearchResultWrapper#getChildResults()
	 * @see #getSearchResultWrapper()
	 * @generated
	 */
	EReference getSearchResultWrapper_ChildResults();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.CaptionedImage <em>Captioned Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Captioned Image</em>'.
	 * @see dk.dtu.imm.red.core.element.CaptionedImage
	 * @generated
	 */
	EClass getCaptionedImage();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.CaptionedImage#getCaption <em>Caption</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Caption</em>'.
	 * @see dk.dtu.imm.red.core.element.CaptionedImage#getCaption()
	 * @see #getCaptionedImage()
	 * @generated
	 */
	EAttribute getCaptionedImage_Caption();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.CaptionedImage#getImageData <em>Image Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image Data</em>'.
	 * @see dk.dtu.imm.red.core.element.CaptionedImage#getImageData()
	 * @see #getCaptionedImage()
	 * @generated
	 */
	EAttribute getCaptionedImage_ImageData();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.CaptionedImageList <em>Captioned Image List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Captioned Image List</em>'.
	 * @see dk.dtu.imm.red.core.element.CaptionedImageList
	 * @generated
	 */
	EClass getCaptionedImageList();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.dtu.imm.red.core.element.CaptionedImageList#getCaptionedImage <em>Captioned Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Captioned Image</em>'.
	 * @see dk.dtu.imm.red.core.element.CaptionedImageList#getCaptionedImage()
	 * @see #getCaptionedImageList()
	 * @generated
	 */
	EReference getCaptionedImageList_CaptionedImage();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.DeletionListener <em>Deletion Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deletion Listener</em>'.
	 * @see dk.dtu.imm.red.core.element.DeletionListener
	 * @generated
	 */
	EClass getDeletionListener();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.core.element.DeletionListener#getHostingObject <em>Hosting Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hosting Object</em>'.
	 * @see dk.dtu.imm.red.core.element.DeletionListener#getHostingObject()
	 * @see #getDeletionListener()
	 * @generated
	 */
	EReference getDeletionListener_HostingObject();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.core.element.DeletionListener#getHostingFeature <em>Hosting Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hosting Feature</em>'.
	 * @see dk.dtu.imm.red.core.element.DeletionListener#getHostingFeature()
	 * @see #getDeletionListener()
	 * @generated
	 */
	EReference getDeletionListener_HostingFeature();

	/**
	 * Returns the meta object for the reference '{@link dk.dtu.imm.red.core.element.DeletionListener#getHandledObject <em>Handled Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Handled Object</em>'.
	 * @see dk.dtu.imm.red.core.element.DeletionListener#getHandledObject()
	 * @see #getDeletionListener()
	 * @generated
	 */
	EReference getDeletionListener_HandledObject();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.Checkable <em>Checkable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Checkable</em>'.
	 * @see dk.dtu.imm.red.core.element.Checkable
	 * @generated
	 */
	EClass getCheckable();

	/**
	 * Returns the meta object for class '{@link dk.dtu.imm.red.core.element.QuantityItem <em>Quantity Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quantity Item</em>'.
	 * @see dk.dtu.imm.red.core.element.QuantityItem
	 * @generated
	 */
	EClass getQuantityItem();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.QuantityItem#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see dk.dtu.imm.red.core.element.QuantityItem#getLabel()
	 * @see #getQuantityItem()
	 * @generated
	 */
	EAttribute getQuantityItem_Label();

	/**
	 * Returns the meta object for the attribute '{@link dk.dtu.imm.red.core.element.QuantityItem#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see dk.dtu.imm.red.core.element.QuantityItem#getValue()
	 * @see #getQuantityItem()
	 * @generated
	 */
	EAttribute getQuantityItem_Value();

	/**
	 * Returns the meta object for the containment reference '{@link dk.dtu.imm.red.core.element.QuantityItem#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Unit</em>'.
	 * @see dk.dtu.imm.red.core.element.QuantityItem#getUnit()
	 * @see #getQuantityItem()
	 * @generated
	 */
	EReference getQuantityItem_Unit();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.element.Priority <em>Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Priority</em>'.
	 * @see dk.dtu.imm.red.core.element.Priority
	 * @generated
	 */
	EEnum getPriority();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.element.LockType <em>Lock Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Lock Type</em>'.
	 * @see dk.dtu.imm.red.core.element.LockType
	 * @generated
	 */
	EEnum getLockType();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.element.ComplexityType <em>Complexity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Complexity Type</em>'.
	 * @see dk.dtu.imm.red.core.element.ComplexityType
	 * @generated
	 */
	EEnum getComplexityType();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.element.LifeCyclePhase <em>Life Cycle Phase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Life Cycle Phase</em>'.
	 * @see dk.dtu.imm.red.core.element.LifeCyclePhase
	 * @generated
	 */
	EEnum getLifeCyclePhase();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.element.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>State</em>'.
	 * @see dk.dtu.imm.red.core.element.State
	 * @generated
	 */
	EEnum getState();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.element.ManagementDecision <em>Management Decision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Management Decision</em>'.
	 * @see dk.dtu.imm.red.core.element.ManagementDecision
	 * @generated
	 */
	EEnum getManagementDecision();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.element.QAAssessment <em>QA Assessment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>QA Assessment</em>'.
	 * @see dk.dtu.imm.red.core.element.QAAssessment
	 * @generated
	 */
	EEnum getQAAssessment();

	/**
	 * Returns the meta object for enum '{@link dk.dtu.imm.red.core.element.QuantityLabel <em>Quantity Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Quantity Label</em>'.
	 * @see dk.dtu.imm.red.core.element.QuantityLabel
	 * @generated
	 */
	EEnum getQuantityLabel();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ElementFactory getElementFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '
		 * {@link dk.dtu.imm.red.core.element.implementation.ElementImpl
		 * <em>Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see dk.dtu.imm.red.core.element.implementation.ElementImpl
		 * @see dk.dtu.imm.red.core.element.implementation.ElementPackageImpl#getElement()
		 * @generated
		 */
		EClass ELEMENT = eINSTANCE.getElement();

		/**
		 * The meta object literal for the '<em><b>Icon URI</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__ICON_URI = eINSTANCE.getElement_IconURI();

		/**
		 * The meta object literal for the '<em><b>Icon</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__ICON = eINSTANCE.getElement_Icon();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__LABEL = eINSTANCE.getElement_Label();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__NAME = eINSTANCE.getElement_Name();

		/**
		 * The meta object literal for the '<em><b>Element Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__ELEMENT_KIND = eINSTANCE.getElement_ElementKind();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__DESCRIPTION = eINSTANCE.getElement_Description();

		/**
		 * The meta object literal for the '<em><b>Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__PRIORITY = eINSTANCE.getElement_Priority();

		/**
		 * The meta object literal for the '<em><b>Commentlist</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__COMMENTLIST = eINSTANCE.getElement_Commentlist();

		/**
		 * The meta object literal for the '<em><b>Time Created</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__TIME_CREATED = eINSTANCE.getElement_TimeCreated();

		/**
		 * The meta object literal for the '<em><b>Last Modified</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__LAST_MODIFIED = eINSTANCE.getElement_LastModified();

		/**
		 * The meta object literal for the '<em><b>Last Modified By</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__LAST_MODIFIED_BY = eINSTANCE.getElement_LastModifiedBy();

		/**
		 * The meta object literal for the '<em><b>Creator</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__CREATOR = eINSTANCE.getElement_Creator();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__VERSION = eINSTANCE.getElement_Version();

		/**
		 * The meta object literal for the '<em><b>Visible ID</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__VISIBLE_ID = eINSTANCE.getElement_VisibleID();

		/**
		 * The meta object literal for the '<em><b>Unique ID</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__UNIQUE_ID = eINSTANCE.getElement_UniqueID();

		/**
		 * The meta object literal for the '<em><b>Relates To</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__RELATES_TO = eINSTANCE.getElement_RelatesTo();

		/**
		 * The meta object literal for the '<em><b>Related By</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__RELATED_BY = eINSTANCE.getElement_RelatedBy();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__PARENT = eINSTANCE.getElement_Parent();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__URI = eINSTANCE.getElement_Uri();

		/**
		 * The meta object literal for the '<em><b>Work Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__WORK_PACKAGE = eINSTANCE.getElement_WorkPackage();

		/**
		 * The meta object literal for the '<em><b>Change List</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__CHANGE_LIST = eINSTANCE.getElement_ChangeList();

		/**
		 * The meta object literal for the '<em><b>Responsible User</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__RESPONSIBLE_USER = eINSTANCE.getElement_ResponsibleUser();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__DEADLINE = eINSTANCE.getElement_Deadline();

		/**
		 * The meta object literal for the '<em><b>Lock Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__LOCK_STATUS = eINSTANCE.getElement_LockStatus();

		/**
		 * The meta object literal for the '<em><b>Lock Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__LOCK_PASSWORD = eINSTANCE.getElement_LockPassword();

		/**
		 * The meta object literal for the '<em><b>Estimated Complexity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__ESTIMATED_COMPLEXITY = eINSTANCE.getElement_EstimatedComplexity();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__COST = eINSTANCE.getElement_Cost();

		/**
		 * The meta object literal for the '<em><b>Benefit</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__BENEFIT = eINSTANCE.getElement_Benefit();

		/**
		 * The meta object literal for the '<em><b>Risk</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__RISK = eINSTANCE.getElement_Risk();

		/**
		 * The meta object literal for the '<em><b>Life Cycle Phase</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__LIFE_CYCLE_PHASE = eINSTANCE.getElement_LifeCyclePhase();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__STATE = eINSTANCE.getElement_State();

		/**
		 * The meta object literal for the '<em><b>Management Decision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__MANAGEMENT_DECISION = eINSTANCE.getElement_ManagementDecision();

		/**
		 * The meta object literal for the '<em><b>Qa Assessment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__QA_ASSESSMENT = eINSTANCE.getElement_QaAssessment();

		/**
		 * The meta object literal for the '<em><b>Management Discussion</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__MANAGEMENT_DISCUSSION = eINSTANCE.getElement_ManagementDiscussion();

		/**
		 * The meta object literal for the '<em><b>Deletion Listeners</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__DELETION_LISTENERS = eINSTANCE.getElement_DeletionListeners();

		/**
		 * The meta object literal for the '<em><b>Quantities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__QUANTITIES = eINSTANCE.getElement_Quantities();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.impl.CustomAttributeImpl <em>Custom Attribute</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.impl.CustomAttributeImpl
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getCustomAttribute()
		 * @generated
		 */
		EClass CUSTOM_ATTRIBUTE = eINSTANCE.getCustomAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOM_ATTRIBUTE__NAME = eINSTANCE.getCustomAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOM_ATTRIBUTE__VALUE = eINSTANCE.getCustomAttribute_Value();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.impl.VisitorImpl <em>Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.impl.VisitorImpl
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getVisitor()
		 * @generated
		 */
		EClass VISITOR = eINSTANCE.getVisitor();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.impl.SearchParameterImpl <em>Search Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.impl.SearchParameterImpl
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getSearchParameter()
		 * @generated
		 */
		EClass SEARCH_PARAMETER = eINSTANCE.getSearchParameter();

		/**
		 * The meta object literal for the '<em><b>Search String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEARCH_PARAMETER__SEARCH_STRING = eINSTANCE.getSearchParameter_SearchString();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.impl.SearchResultImpl <em>Search Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.impl.SearchResultImpl
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getSearchResult()
		 * @generated
		 */
		EClass SEARCH_RESULT = eINSTANCE.getSearchResult();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEARCH_RESULT__ATTRIBUTE = eINSTANCE.getSearchResult_Attribute();

		/**
		 * The meta object literal for the '<em><b>Start Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEARCH_RESULT__START_INDEX = eINSTANCE.getSearchResult_StartIndex();

		/**
		 * The meta object literal for the '<em><b>End Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEARCH_RESULT__END_INDEX = eINSTANCE.getSearchResult_EndIndex();

		/**
		 * The meta object literal for the '<em><b>Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEARCH_RESULT__ELEMENT = eINSTANCE.getSearchResult_Element();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.impl.SearchResultWrapperImpl <em>Search Result Wrapper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.impl.SearchResultWrapperImpl
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getSearchResultWrapper()
		 * @generated
		 */
		EClass SEARCH_RESULT_WRAPPER = eINSTANCE.getSearchResultWrapper();

		/**
		 * The meta object literal for the '<em><b>Element Results</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEARCH_RESULT_WRAPPER__ELEMENT_RESULTS = eINSTANCE.getSearchResultWrapper_ElementResults();

		/**
		 * The meta object literal for the '<em><b>Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEARCH_RESULT_WRAPPER__ELEMENT = eINSTANCE.getSearchResultWrapper_Element();

		/**
		 * The meta object literal for the '<em><b>Child Results</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEARCH_RESULT_WRAPPER__CHILD_RESULTS = eINSTANCE.getSearchResultWrapper_ChildResults();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.impl.CaptionedImageImpl <em>Captioned Image</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.impl.CaptionedImageImpl
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getCaptionedImage()
		 * @generated
		 */
		EClass CAPTIONED_IMAGE = eINSTANCE.getCaptionedImage();

		/**
		 * The meta object literal for the '<em><b>Caption</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CAPTIONED_IMAGE__CAPTION = eINSTANCE.getCaptionedImage_Caption();

		/**
		 * The meta object literal for the '<em><b>Image Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CAPTIONED_IMAGE__IMAGE_DATA = eINSTANCE.getCaptionedImage_ImageData();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.impl.CaptionedImageListImpl <em>Captioned Image List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.impl.CaptionedImageListImpl
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getCaptionedImageList()
		 * @generated
		 */
		EClass CAPTIONED_IMAGE_LIST = eINSTANCE.getCaptionedImageList();

		/**
		 * The meta object literal for the '<em><b>Captioned Image</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAPTIONED_IMAGE_LIST__CAPTIONED_IMAGE = eINSTANCE.getCaptionedImageList_CaptionedImage();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.impl.DeletionListenerImpl <em>Deletion Listener</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.impl.DeletionListenerImpl
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getDeletionListener()
		 * @generated
		 */
		EClass DELETION_LISTENER = eINSTANCE.getDeletionListener();

		/**
		 * The meta object literal for the '<em><b>Hosting Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DELETION_LISTENER__HOSTING_OBJECT = eINSTANCE.getDeletionListener_HostingObject();

		/**
		 * The meta object literal for the '<em><b>Hosting Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DELETION_LISTENER__HOSTING_FEATURE = eINSTANCE.getDeletionListener_HostingFeature();

		/**
		 * The meta object literal for the '<em><b>Handled Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DELETION_LISTENER__HANDLED_OBJECT = eINSTANCE.getDeletionListener_HandledObject();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.Checkable <em>Checkable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.Checkable
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getCheckable()
		 * @generated
		 */
		EClass CHECKABLE = eINSTANCE.getCheckable();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.impl.QuantityItemImpl <em>Quantity Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.impl.QuantityItemImpl
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getQuantityItem()
		 * @generated
		 */
		EClass QUANTITY_ITEM = eINSTANCE.getQuantityItem();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUANTITY_ITEM__LABEL = eINSTANCE.getQuantityItem_Label();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUANTITY_ITEM__VALUE = eINSTANCE.getQuantityItem_Value();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUANTITY_ITEM__UNIT = eINSTANCE.getQuantityItem_Unit();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.Priority <em>Priority</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.Priority
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getPriority()
		 * @generated
		 */
		EEnum PRIORITY = eINSTANCE.getPriority();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.LockType <em>Lock Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.LockType
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getLockType()
		 * @generated
		 */
		EEnum LOCK_TYPE = eINSTANCE.getLockType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.ComplexityType <em>Complexity Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.ComplexityType
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getComplexityType()
		 * @generated
		 */
		EEnum COMPLEXITY_TYPE = eINSTANCE.getComplexityType();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.LifeCyclePhase <em>Life Cycle Phase</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.LifeCyclePhase
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getLifeCyclePhase()
		 * @generated
		 */
		EEnum LIFE_CYCLE_PHASE = eINSTANCE.getLifeCyclePhase();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.State <em>State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.State
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getState()
		 * @generated
		 */
		EEnum STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.ManagementDecision <em>Management Decision</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.ManagementDecision
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getManagementDecision()
		 * @generated
		 */
		EEnum MANAGEMENT_DECISION = eINSTANCE.getManagementDecision();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.QAAssessment <em>QA Assessment</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.QAAssessment
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getQAAssessment()
		 * @generated
		 */
		EEnum QA_ASSESSMENT = eINSTANCE.getQAAssessment();

		/**
		 * The meta object literal for the '{@link dk.dtu.imm.red.core.element.QuantityLabel <em>Quantity Label</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.dtu.imm.red.core.element.QuantityLabel
		 * @see dk.dtu.imm.red.core.element.impl.ElementPackageImpl#getQuantityLabel()
		 * @generated
		 */
		EEnum QUANTITY_LABEL = eINSTANCE.getQuantityLabel();

	}

} // ElementPackage
