/**
 */
package dk.dtu.imm.red.core.usecasepoint.impl;

import java.awt.GraphicsDevice.WindowTranslucency;
import java.util.List;

import dk.dtu.imm.red.core.usecasepoint.TechnologyFactor;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointFactory;
import dk.dtu.imm.red.core.usecasepoint.UsecasepointPackage;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactor;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactorValue;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Technology Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TechnologyFactorImpl extends WeightedFactorContainerImpl implements TechnologyFactor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TechnologyFactorImpl() {
		super();
	} 

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasepointPackage.Literals.TECHNOLOGY_FACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */ 
	@Override 
	public double getSum() {  
		
/*		Formula from
 * 		Revised Use Case Point Method -
		Effort Estimation in Development Projects
		for Business Applications*/
		
		// TCF = 0.58 + (0.01 x TFactor) 
		
		double sum = 0;
		
		for(WeightedFactor w : getWeightedFactors()) {
			sum += w.getSum();
		} 
		
		double k1 = 0.58;
		double k2 = 0.01;
		
		return k1 + (k2 * sum);
	}

} //TechnologyFactorImpl
