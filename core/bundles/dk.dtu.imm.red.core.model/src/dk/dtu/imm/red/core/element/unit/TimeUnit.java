/**
 */
package dk.dtu.imm.red.core.element.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.dtu.imm.red.core.element.unit.UnitPackage#getTimeUnit()
 * @model
 * @generated
 */
public interface TimeUnit extends Unit {
} // TimeUnit
