/**
 */
package dk.dtu.imm.red.core.element.document;

import dk.dtu.imm.red.core.element.CaptionedImageList;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.text.Text;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.element.document.Document#getAbstract <em>Abstract</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.Document#getInternalRef <em>Internal Ref</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.Document#getExternalFileRef <em>External File Ref</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.Document#getWebRef <em>Web Ref</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.Document#getTextContent <em>Text Content</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.element.document.Document#getCaptionedImageList <em>Captioned Image List</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.element.document.DocumentPackage#getDocument()
 * @model
 * @generated
 */
public interface Document extends Element {
	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' containment reference.
	 * @see #setAbstract(Text)
	 * @see dk.dtu.imm.red.core.element.document.DocumentPackage#getDocument_Abstract()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getAbstract();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.document.Document#getAbstract <em>Abstract</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' containment reference.
	 * @see #getAbstract()
	 * @generated
	 */
	void setAbstract(Text value);

	/**
	 * Returns the value of the '<em><b>Internal Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Ref</em>' reference.
	 * @see #setInternalRef(Element)
	 * @see dk.dtu.imm.red.core.element.document.DocumentPackage#getDocument_InternalRef()
	 * @model
	 * @generated
	 */
	Element getInternalRef();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.document.Document#getInternalRef <em>Internal Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Ref</em>' reference.
	 * @see #getInternalRef()
	 * @generated
	 */
	void setInternalRef(Element value);

	/**
	 * Returns the value of the '<em><b>External File Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External File Ref</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External File Ref</em>' attribute.
	 * @see #setExternalFileRef(String)
	 * @see dk.dtu.imm.red.core.element.document.DocumentPackage#getDocument_ExternalFileRef()
	 * @model
	 * @generated
	 */
	String getExternalFileRef();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.document.Document#getExternalFileRef <em>External File Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External File Ref</em>' attribute.
	 * @see #getExternalFileRef()
	 * @generated
	 */
	void setExternalFileRef(String value);

	/**
	 * Returns the value of the '<em><b>Web Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Web Ref</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Web Ref</em>' attribute.
	 * @see #setWebRef(String)
	 * @see dk.dtu.imm.red.core.element.document.DocumentPackage#getDocument_WebRef()
	 * @model
	 * @generated
	 */
	String getWebRef();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.document.Document#getWebRef <em>Web Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Web Ref</em>' attribute.
	 * @see #getWebRef()
	 * @generated
	 */
	void setWebRef(String value);

	/**
	 * Returns the value of the '<em><b>Text Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text Content</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text Content</em>' containment reference.
	 * @see #setTextContent(Text)
	 * @see dk.dtu.imm.red.core.element.document.DocumentPackage#getDocument_TextContent()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Text getTextContent();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.document.Document#getTextContent <em>Text Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text Content</em>' containment reference.
	 * @see #getTextContent()
	 * @generated
	 */
	void setTextContent(Text value);

	/**
	 * Returns the value of the '<em><b>Captioned Image List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Captioned Image List</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Captioned Image List</em>' containment reference.
	 * @see #setCaptionedImageList(CaptionedImageList)
	 * @see dk.dtu.imm.red.core.element.document.DocumentPackage#getDocument_CaptionedImageList()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	CaptionedImageList getCaptionedImageList();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.element.document.Document#getCaptionedImageList <em>Captioned Image List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Captioned Image List</em>' containment reference.
	 * @see #getCaptionedImageList()
	 * @generated
	 */
	void setCaptionedImageList(CaptionedImageList value);

} // Document
