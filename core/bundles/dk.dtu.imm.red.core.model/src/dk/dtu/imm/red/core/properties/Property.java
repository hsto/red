/**
 */
package dk.dtu.imm.red.core.properties;

import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.dtu.imm.red.core.properties.Property#getName <em>Name</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.Property#getValue <em>Value</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.Property#getKind <em>Kind</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.Property#getBenefitUnit <em>Benefit Unit</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.Property#getDurationUnit <em>Duration Unit</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.Property#getMonetaryUnit <em>Monetary Unit</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.Property#getMeasurementKind <em>Measurement Kind</em>}</li>
 *   <li>{@link dk.dtu.imm.red.core.properties.Property#getValueKind <em>Value Kind</em>}</li>
 * </ul>
 *
 * @see dk.dtu.imm.red.core.properties.PropertiesPackage#getProperty()
 * @model
 * @generated
 */
public interface Property extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.dtu.imm.red.core.properties.PropertiesPackage#getProperty_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.properties.Property#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(double)
	 * @see dk.dtu.imm.red.core.properties.PropertiesPackage#getProperty_Value()
	 * @model
	 * @generated
	 */
	double getValue();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.properties.Property#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(double value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.properties.Kind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.Kind
	 * @see #setKind(Kind)
	 * @see dk.dtu.imm.red.core.properties.PropertiesPackage#getProperty_Kind()
	 * @model
	 * @generated
	 */
	Kind getKind();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.properties.Property#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.Kind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(Kind value);

	/**
	 * Returns the value of the '<em><b>Benefit Unit</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.properties.BenefitUnit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Benefit Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Benefit Unit</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.BenefitUnit
	 * @see #setBenefitUnit(BenefitUnit)
	 * @see dk.dtu.imm.red.core.properties.PropertiesPackage#getProperty_BenefitUnit()
	 * @model
	 * @generated
	 */
	BenefitUnit getBenefitUnit();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.properties.Property#getBenefitUnit <em>Benefit Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Benefit Unit</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.BenefitUnit
	 * @see #getBenefitUnit()
	 * @generated
	 */
	void setBenefitUnit(BenefitUnit value);

	/**
	 * Returns the value of the '<em><b>Duration Unit</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.properties.DurationUnit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration Unit</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.DurationUnit
	 * @see #setDurationUnit(DurationUnit)
	 * @see dk.dtu.imm.red.core.properties.PropertiesPackage#getProperty_DurationUnit()
	 * @model
	 * @generated
	 */
	DurationUnit getDurationUnit();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.properties.Property#getDurationUnit <em>Duration Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration Unit</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.DurationUnit
	 * @see #getDurationUnit()
	 * @generated
	 */
	void setDurationUnit(DurationUnit value);

	/**
	 * Returns the value of the '<em><b>Monetary Unit</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.properties.MonetaryUnit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Monetary Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monetary Unit</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.MonetaryUnit
	 * @see #setMonetaryUnit(MonetaryUnit)
	 * @see dk.dtu.imm.red.core.properties.PropertiesPackage#getProperty_MonetaryUnit()
	 * @model
	 * @generated
	 */
	MonetaryUnit getMonetaryUnit();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.properties.Property#getMonetaryUnit <em>Monetary Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Monetary Unit</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.MonetaryUnit
	 * @see #getMonetaryUnit()
	 * @generated
	 */
	void setMonetaryUnit(MonetaryUnit value);

	/**
	 * Returns the value of the '<em><b>Measurement Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.properties.MeasurementKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measurement Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measurement Kind</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.MeasurementKind
	 * @see #setMeasurementKind(MeasurementKind)
	 * @see dk.dtu.imm.red.core.properties.PropertiesPackage#getProperty_MeasurementKind()
	 * @model
	 * @generated
	 */
	MeasurementKind getMeasurementKind();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.properties.Property#getMeasurementKind <em>Measurement Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Measurement Kind</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.MeasurementKind
	 * @see #getMeasurementKind()
	 * @generated
	 */
	void setMeasurementKind(MeasurementKind value);

	/**
	 * Returns the value of the '<em><b>Value Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.dtu.imm.red.core.properties.ValueKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Kind</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.ValueKind
	 * @see #setValueKind(ValueKind)
	 * @see dk.dtu.imm.red.core.properties.PropertiesPackage#getProperty_ValueKind()
	 * @model
	 * @generated
	 */
	ValueKind getValueKind();

	/**
	 * Sets the value of the '{@link dk.dtu.imm.red.core.properties.Property#getValueKind <em>Value Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Kind</em>' attribute.
	 * @see dk.dtu.imm.red.core.properties.ValueKind
	 * @see #getValueKind()
	 * @generated
	 */
	void setValueKind(ValueKind value);

} // Property
