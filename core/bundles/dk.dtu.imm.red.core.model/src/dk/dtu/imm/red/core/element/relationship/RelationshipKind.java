/**
 */
package dk.dtu.imm.red.core.element.relationship;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>Kind</b></em>', and utility methods for working with them. <!--
 * end-user-doc -->
 * 
 * @see dk.dtu.imm.red.core.element.relationship.RelationshipPackage#getRelationshipKind()
 * @model
 * @generated
 */
public enum RelationshipKind implements Enumerator {
	/**
	 * The '<em><b>Association</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #ASSOCIATION_VALUE
	 * @generated
	 * @ordered
	 */
	IS_ASSOCIATED_TO(0, "is_associated_to", "is associated to"),
	/**
	 * The '<em><b>Appears in</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #APPEARS_IN_VALUE
	 * @generated
	 * @ordered
	 */
	FEATURES_IN(1, "features_in", "features in"),
	/**
	 * The '<em><b>Describes</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #DESCRIBES_VALUE
	 * @generated
	 * @ordered
	 */
	IS_FEATURING(2, "is_featuring", "is featuring"),
	/**
	 * The '<em><b>Contributes cost to</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #CONTRIBUTES_COST_TO_VALUE
	 * @generated
	 * @ordered
	 */
	CONTRIBUTES_COST_TO(3, "contributes_cost_to", "contributes cost to"),

	/**
	 * The '<em><b>Cost comes from</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #COST_COMES_FROM_VALUE
	 * @generated
	 * @ordered
	 */
	COST_IS_CONTRIBUTED_BY(4, "cost_is_contributed_by", "cost is contributed by"),

	/**
	 * The '<em><b>Contributes performance to</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #CONTRIBUTES_PERFORMANCE_TO_VALUE
	 * @generated
	 * @ordered
	 */
	CONTRIBUTES_PERFORMANCE_TO(5, "contributes_performance_to", "contributes performance to"),

	/**
	 * The '<em><b>Performance comes from</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #PERFORMANCE_COMES_FROM_VALUE
	 * @generated
	 * @ordered
	 */
	PERFORMANCE_IS_CONTRIBUTED_BY(6, "performance_is_contributed_by", "performance is contributed by"),

	/**
	 * The '<em><b>Contributes risk to</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #CONTRIBUTES_RISK_TO_VALUE
	 * @generated
	 * @ordered
	 */
	CONTRIBUTES_RISK_TO(7, "contributes_risk_to", "contributes risk to"),

	/**
	 * The '<em><b>Risk comes from</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #RISK_COMES_FROM_VALUE
	 * @generated
	 * @ordered
	 */
	RISK_IS_CONTRIBUTED_BY(8, "risk_is_contributed_by", "risk is contributed by"),

	/**
	 * The '<em><b>Supports</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #SUPPORTS_VALUE
	 * @generated
	 * @ordered
	 */
	SUPPORTS(9, "supports", "supports"),
	/**
	 * The '<em><b>Depends on</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #DEPENDS_ON_VALUE
	 * @generated
	 * @ordered
	 */
	IS_SUPPORTED_BY(10, "is_supported_by", "is supported by"),

	/**
	 * The '<em><b>Expands to</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #EXPANDS_TO_VALUE
	 * @generated
	 * @ordered
	 */
	ELABORATES(11, "elaborates", "elaborates"),

	/**
	 * The '<em><b>Is elaborated from</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #IS_ELABORATED_FROM_VALUE
	 * @generated
	 * @ordered
	 */
	IS_ELABORATED_FROM(12, "is_elaborated_from", "is elaborated from"),

	/**
	 * The '<em><b>Extends</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #EXTENDS_VALUE
	 * @generated
	 * @ordered
	 */
	EXTENDS(13, "extends", "extends"),
	/**
	 * The '<em><b>Is extended by</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #IS_EXTENDED_BY_VALUE
	 * @generated
	 * @ordered
	 */
	IS_EXTENDED_BY(14, "is_extended_by", "is extended by"),
	/**
	 * The '<em><b>Illustrates</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #ILLUSTRATES_VALUE
	 * @generated
	 * @ordered
	 */
	ILLUSTRATES(15, "illustrates", "illustrates"),
	/**
	 * The '<em><b>Is illustrate by</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #IS_ILLUSTRATE_BY_VALUE
	 * @generated
	 * @ordered
	 */
	IS_ILLUSTRATED_BY(16, "is_illustrated_by", "is illustrated by"),
	/**
	 * The '<em><b>Includes</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #INCLUDES_VALUE
	 * @generated
	 * @ordered
	 */
	INCLUDES(17, "includes", "includes"),
	/**
	 * The '<em><b>Is included in</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #IS_INCLUDED_IN_VALUE
	 * @generated
	 * @ordered
	 */
	IS_INCLUDED_IN(18, "is_included_in", "is included in"),

	/**
	 * The '<em><b>Gives rise to</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #GIVES_RISE_TO_VALUE
	 * @generated
	 * @ordered
	 */
	DERIVES_TO(19, "derives_to", "dervies to"),

	/**
	 * The '<em><b>Is derived from</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #IS_DERIVED_FROM_VALUE
	 * @generated
	 * @ordered
	 */
	IS_DERIVED_FROM(20, "is_derived_from", "is derived from"),

	/**
	 * The '<em><b>Is kind of</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_KIND_OF_VALUE
	 * @generated
	 * @ordered
	 */
	IS_KIND_OF(21, "is_kind_of", "is kind of"),

	/**
	 * The '<em><b>Is generalization of</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #IS_GENERALIZATION_OF_VALUE
	 * @generated
	 * @ordered
	 */
	IS_GENERALIZATION_OF(22, "is_generalization_of", "is generalization of"),

	/**
	 * The '<em><b>Contains</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #CONTAINS_VALUE
	 * @generated
	 * @ordered
	 */
	CONTAINS(23, "contains", "contains"),

	/**
	 * The '<em><b>Is part of</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_PART_OF_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PART_OF(24, "is_part_of", "is part of"),

	/**
	 * The '<em><b>Is source of</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #IS_SOURCE_OF_VALUE
	 * @generated
	 * @ordered
	 */
	LEADS_TO(25, "leads_to", "leads to"),

	/**
	 * The '<em><b>Originates from</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #ORIGINATES_FROM_VALUE
	 * @generated
	 * @ordered
	 */
	COMES_FROM(26, "comes_from", "comes from"),

	/**
	 * The '<em><b>Justifies</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #JUSTIFIES_VALUE
	 * @generated
	 * @ordered
	 */
	JUSTIFIES(27, "justifies", "justifies"),
	/**
	 * The '<em><b>Is based on</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_BASED_ON_VALUE
	 * @generated
	 * @ordered
	 */
	IS_JUSTIFIED_BY(28, "is_justified_by", "is justified by"),

	/**
	 * The '<em><b>Precedes</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #PRECEDES_VALUE
	 * @generated
	 * @ordered
	 */
	PRECEDES(29, "precedes", "precedes"),

	/**
	 * The '<em><b>Supersedes</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #SUPERSEDES_VALUE
	 * @generated
	 * @ordered
	 */
	SUPERSEDES(30, "supersedes", "supersedes"),

	/**
	 * The '<em><b>Refer to</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #REFER_TO_VALUE
	 * @generated
	 * @ordered
	 */
	REFERS_TO(31, "refer_to", "refers to"),
	/**
	 * The '<em><b>Is used in</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_USED_IN_VALUE
	 * @generated
	 * @ordered
	 */
	IS_REFERRED_BY(32, "is_referred_by", "is referred by"),

	/**
	 * The '<em><b>Refines to</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #REFINES_TO_VALUE
	 * @generated
	 * @ordered
	 */
	REFINES_TO(33, "refines_to", "refines to"),
	/**
	 * The '<em><b>Is refined from</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #IS_REFINED_FROM_VALUE
	 * @generated
	 * @ordered
	 */
	IS_REFINED_FROM(34, "is_refined_from", "is refined from"),

	/**
	 * The '<em><b>Conflicts with</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #CONFLICTS_WITH_VALUE
	 * @generated
	 * @ordered
	 */
	CONFLICTS_WITH(35, "conflicts_with", "conflicts with"),
	/**
	 * 
	 * /** The '<em><b>Is related to</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #IS_RELATED_TO_VALUE
	 * @generated
	 * @ordered
	 */
	IS_RELATED_TO(36, "is_related_to", "is related to"),
	/**
	 * The '<em><b>Is stitched to</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #IS_STITCHED_TO_VALUE
	 * @generated
	 * @ordered
	 */
	IS_STITCHED_TO(37, "is_stitched_to", "is stitched to");

	/**
	 * The '<em><b>Association</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Association</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_ASSOCIATED_TO
	 * @model name="association"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ASSOCIATED_TO_VALUE = 0;

	/**
	 * The '<em><b>Appears in</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Appears in</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #FEATURES_IN
	 * @model name="appears_in" literal="appears in"
	 * @generated
	 * @ordered
	 */
	public static final int FEATURES_IN_VALUE = 1;

	/**
	 * The '<em><b>Describes</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Describes</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_FEATURING
	 * @model name="describes"
	 * @generated
	 * @ordered
	 */
	public static final int IS_FEATURING_VALUE = 2;

	/**
	 * The '<em><b>Contributes cost to</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Contributes cost to</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #CONTRIBUTES_COST_TO
	 * @model name="contributes_cost_to" literal="contributes cost to"
	 * @generated
	 * @ordered
	 */
	public static final int CONTRIBUTES_COST_TO_VALUE = 3;

	/**
	 * The '<em><b>Cost comes from</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Cost comes from</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #COST_IS_CONTRIBUTED_BY
	 * @model name="cost_comes_from" literal="cost comes from"
	 * @generated
	 * @ordered
	 */
	public static final int COST_IS_CONTRIBUTED_BY_VALUE = 4;

	/**
	 * The '<em><b>Contributes performance to</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Contributes performance to</b></em>' literal
	 * object isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #CONTRIBUTES_PERFORMANCE_TO
	 * @model name="contributes_performance_to" literal=
	 *        "contributes performance to"
	 * @generated
	 * @ordered
	 */
	public static final int CONTRIBUTES_PERFORMANCE_TO_VALUE = 5;

	/**
	 * The '<em><b>Performance comes from</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Performance comes from</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #PERFORMANCE_IS_CONTRIBUTED_BY
	 * @model name="performance_comes_from" literal="performance comes from"
	 * @generated
	 * @ordered
	 */
	public static final int PERFORMANCE_IS_CONTRIBUTED_BY_VALUE = 6;

	/**
	 * The '<em><b>Contributes risk to</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Contributes risk to</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #CONTRIBUTES_RISK_TO
	 * @model name="contributes_risk_to" literal="contributes risk to"
	 * @generated
	 * @ordered
	 */
	public static final int CONTRIBUTES_RISK_TO_VALUE = 7;

	/**
	 * The '<em><b>Risk comes from</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Risk comes from</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #RISK_IS_CONTRIBUTED_BY
	 * @model name="risk_comes_from" literal="risk comes from"
	 * @generated
	 * @ordered
	 */
	public static final int RISK_IS_CONTRIBUTED_BY_VALUE = 8;

	/**
	 * The '<em><b>Supports</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Supports</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #SUPPORTS
	 * @model name="supports"
	 * @generated
	 * @ordered
	 */
	public static final int SUPPORTS_VALUE = 9;

	/**
	 * The '<em><b>Depends on</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Depends on</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_SUPPORTED_BY
	 * @model name="depends_on" literal="depends on"
	 * @generated
	 * @ordered
	 */
	public static final int IS_SUPPORTED_BY_VALUE = 10;

	/**
	 * The '<em><b>Expands to</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Expands to</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #ELABORATES
	 * @model name="expands_to" literal="expands to"
	 * @generated
	 * @ordered
	 */
	public static final int ELABORATES_VALUE = 11;

	/**
	 * The '<em><b>Is elaborated from</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is elaborated from</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_ELABORATED_FROM
	 * @model name="is_elaborated_from" literal="is elaborated from"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ELABORATED_FROM_VALUE = 12;

	/**
	 * The '<em><b>Extends</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Extends</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #EXTENDS
	 * @model name="extends"
	 * @generated
	 * @ordered
	 */
	public static final int EXTENDS_VALUE = 13;

	/**
	 * The '<em><b>Is extended by</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Is extended by</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_EXTENDED_BY
	 * @model name="is_extended_by" literal="is extended by"
	 * @generated
	 * @ordered
	 */
	public static final int IS_EXTENDED_BY_VALUE = 14;

	/**
	 * The '<em><b>Illustrates</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Illustrates</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #ILLUSTRATES
	 * @model name="illustrates"
	 * @generated
	 * @ordered
	 */
	public static final int ILLUSTRATES_VALUE = 15;

	/**
	 * The '<em><b>Is illustrate by</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Is illustrate by</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_ILLUSTRATED_BY
	 * @model name="is_illustrate_by" literal="is illustrated by"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ILLUSTRATED_BY_VALUE = 16;

	/**
	 * The '<em><b>Includes</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Includes</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #INCLUDES
	 * @model name="includes"
	 * @generated
	 * @ordered
	 */
	public static final int INCLUDES_VALUE = 17;

	/**
	 * The '<em><b>Is included in</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Is included in</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_INCLUDED_IN
	 * @model name="is_included_in" literal="is included in"
	 * @generated
	 * @ordered
	 */
	public static final int IS_INCLUDED_IN_VALUE = 18;

	/**
	 * The '<em><b>Gives rise to</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Gives rise to</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #DERIVES_TO
	 * @model name="gives_rise_to" literal="gives rise to"
	 * @generated
	 * @ordered
	 */
	public static final int DERIVES_TO_VALUE = 19;

	/**
	 * The '<em><b>Is derived from</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Is derived from</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_DERIVED_FROM
	 * @model name="is_derived_from" literal="is derived from"
	 * @generated
	 * @ordered
	 */
	public static final int IS_DERIVED_FROM_VALUE = 20;

	/**
	 * The '<em><b>Is kind of</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is kind of</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_KIND_OF
	 * @model name="is_kind_of" literal="is kind of"
	 * @generated
	 * @ordered
	 */
	public static final int IS_KIND_OF_VALUE = 21;

	/**
	 * The '<em><b>Is generalization of</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is generalization of</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_GENERALIZATION_OF
	 * @model name="is_generalization_of" literal="is generalization of"
	 * @generated
	 * @ordered
	 */
	public static final int IS_GENERALIZATION_OF_VALUE = 22;

	/**
	 * The '<em><b>Contains</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Contains</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #CONTAINS
	 * @model name="contains"
	 * @generated
	 * @ordered
	 */
	public static final int CONTAINS_VALUE = 23;

	/**
	 * The '<em><b>Is part of</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is part of</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_PART_OF
	 * @model name="is_part_of" literal="is part of"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PART_OF_VALUE = 24;

	/**
	 * The '<em><b>Is source of</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is source of</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #LEADS_TO
	 * @model name="is_source_of" literal="is source of"
	 * @generated
	 * @ordered
	 */
	public static final int LEADS_TO_VALUE = 25;

	/**
	 * The '<em><b>Originates from</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Originates from</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #COMES_FROM
	 * @model name="originates_from" literal="originates from"
	 * @generated
	 * @ordered
	 */
	public static final int COMES_FROM_VALUE = 26;

	/**
	 * The '<em><b>Justifies</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Justifies</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #JUSTIFIES
	 * @model name="justifies"
	 * @generated
	 * @ordered
	 */
	public static final int JUSTIFIES_VALUE = 27;

	/**
	 * The '<em><b>Is based on</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is based on</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_JUSTIFIED_BY
	 * @model name="is_based_on" literal="is based on"
	 * @generated
	 * @ordered
	 */
	public static final int IS_JUSTIFIED_BY_VALUE = 28;

	/**
	 * The '<em><b>Precedes</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Precedes</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #PRECEDES
	 * @model name="precedes"
	 * @generated
	 * @ordered
	 */
	public static final int PRECEDES_VALUE = 29;

	/**
	 * The '<em><b>Supersedes</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Supersedes</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #SUPERSEDES
	 * @model name="supersedes"
	 * @generated
	 * @ordered
	 */
	public static final int SUPERSEDES_VALUE = 30;

	/**
	 * The '<em><b>Refer to</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Refer to</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #REFERS_TO
	 * @model name="refer_to" literal="refers to"
	 * @generated
	 * @ordered
	 */
	public static final int REFERS_TO_VALUE = 31;

	/**
	 * The '<em><b>Is used in</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is used in</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_REFERRED_BY
	 * @model name="is_used_in" literal="is used in"
	 * @generated
	 * @ordered
	 */
	public static final int IS_REFERRED_BY_VALUE = 32;

	/**
	 * The '<em><b>Refines to</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Refines to</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #REFINES_TO
	 * @model name="refines_to" literal="refines to"
	 * @generated
	 * @ordered
	 */
	public static final int REFINES_TO_VALUE = 33;

	/**
	 * The '<em><b>Is refined from</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Is refined from</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_REFINED_FROM
	 * @model name="is_refined_from" literal="is refined from"
	 * @generated
	 * @ordered
	 */
	public static final int IS_REFINED_FROM_VALUE = 34;

	/**
	 * The '<em><b>Results from</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Results from</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #RESULTS_FROM
	 * @model name="results_from" literal="results from"
	 * @generated
	 * @ordered
	 */
	public static final int RESULTS_FROM_VALUE = 35;

	/**
	 * The '<em><b>Leads to</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Leads to</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_RESULTING_IN
	 * @model name="leads_to" literal="leads to"
	 * @generated
	 * @ordered
	 */
	public static final int IS_RESULTING_IN_VALUE = 36;

	/**
	 * The '<em><b>Conflicts with</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Conflicts with</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #CONFLICTS_WITH
	 * @model name="conflicts_with" literal="conflicts with"
	 * @generated
	 * @ordered
	 */
	public static final int CONFLICTS_WITH_VALUE = 37;

	/**
	 * The '<em><b>Is related to</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Is related to</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_RELATED_TO
	 * @model name="is_related_to" literal="is related to"
	 * @generated
	 * @ordered
	 */
	public static final int IS_RELATED_TO_VALUE = 38;

	/**
	 * The '<em><b>Is stitched to</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Is stitched to</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #IS_STITCHED_TO
	 * @model name="is_stitched_to" literal="is stitched to"
	 * @generated
	 * @ordered
	 */
	public static final int IS_STITCHED_TO_VALUE = 39;

	/**
	 * An array of all the '<em><b>Kind</b></em>' enumerators. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final RelationshipKind[] VALUES_ARRAY = new RelationshipKind[] { IS_ASSOCIATED_TO, FEATURES_IN,
			IS_FEATURING, CONTRIBUTES_COST_TO, COST_IS_CONTRIBUTED_BY, CONTRIBUTES_PERFORMANCE_TO,
			PERFORMANCE_IS_CONTRIBUTED_BY, CONTRIBUTES_RISK_TO, RISK_IS_CONTRIBUTED_BY, SUPPORTS, IS_SUPPORTED_BY,
			IS_ELABORATED_FROM, ELABORATES, EXTENDS, IS_EXTENDED_BY, ILLUSTRATES, IS_ILLUSTRATED_BY, INCLUDES,
			IS_INCLUDED_IN, IS_DERIVED_FROM, DERIVES_TO, IS_KIND_OF, IS_GENERALIZATION_OF, IS_PART_OF, CONTAINS,
			LEADS_TO, COMES_FROM, JUSTIFIES, IS_JUSTIFIED_BY, PRECEDES, SUPERSEDES, REFERS_TO, REFINES_TO,
			IS_REFINED_FROM, IS_REFERRED_BY, CONFLICTS_WITH, IS_RELATED_TO, IS_STITCHED_TO, };

	/**
	 * A public read-only list of all the '<em><b>Kind</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<RelationshipKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Kind</b></em>' literal with the specified literal
	 * value. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param literal
	 *            the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RelationshipKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RelationshipKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Kind</b></em>' literal with the specified name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param name
	 *            the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RelationshipKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RelationshipKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	public static Set<RelationshipKind> getActiveKinds() {
		Set<RelationshipKind> suckThisBitch = new HashSet<>();
		for (int i = 1; i < CONFLICTS_WITH_VALUE; i++) {
			if (i % 2 == 1)
				suckThisBitch.add(get(i));
		}
		return suckThisBitch;
	}

	public static Set<RelationshipKind> getPassiveKinds() {
		Set<RelationshipKind> passive = new HashSet<>();
		for (int i = 1; i < CONFLICTS_WITH_VALUE; i++) {
			if (i % 2 == 0)
				passive.add(get(i));
		}
		return passive;
	}

	public static Set<RelationshipKind> getSymmetricalKinds() {
		Set<RelationshipKind> symmetrical = new HashSet<>();
		symmetrical.add(IS_ASSOCIATED_TO);
		symmetrical.add(CONFLICTS_WITH);
		symmetrical.add(IS_RELATED_TO);
		symmetrical.add(IS_STITCHED_TO);
		return symmetrical;
	}

	/**
	 * Returns the '<em><b>Kind</b></em>' literal with the specified integer
	 * value. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RelationshipKind get(int value) {
		switch (value) {
		case IS_ASSOCIATED_TO_VALUE:
			return IS_ASSOCIATED_TO;
		case FEATURES_IN_VALUE:
			return FEATURES_IN;
		case IS_FEATURING_VALUE:
			return IS_FEATURING;
		case CONTRIBUTES_COST_TO_VALUE:
			return CONTRIBUTES_COST_TO;
		case COST_IS_CONTRIBUTED_BY_VALUE:
			return COST_IS_CONTRIBUTED_BY;
		case CONTRIBUTES_PERFORMANCE_TO_VALUE:
			return CONTRIBUTES_PERFORMANCE_TO;
		case PERFORMANCE_IS_CONTRIBUTED_BY_VALUE:
			return PERFORMANCE_IS_CONTRIBUTED_BY;
		case CONTRIBUTES_RISK_TO_VALUE:
			return CONTRIBUTES_RISK_TO;
		case RISK_IS_CONTRIBUTED_BY_VALUE:
			return RISK_IS_CONTRIBUTED_BY;
		case SUPPORTS_VALUE:
			return SUPPORTS;
		case IS_SUPPORTED_BY_VALUE:
			return IS_SUPPORTED_BY;
		case IS_ELABORATED_FROM_VALUE:
			return IS_ELABORATED_FROM;
		case ELABORATES_VALUE:
			return ELABORATES;
		case EXTENDS_VALUE:
			return EXTENDS;
		case IS_EXTENDED_BY_VALUE:
			return IS_EXTENDED_BY;
		case ILLUSTRATES_VALUE:
			return ILLUSTRATES;
		case IS_ILLUSTRATED_BY_VALUE:
			return IS_ILLUSTRATED_BY;
		case INCLUDES_VALUE:
			return INCLUDES;
		case IS_INCLUDED_IN_VALUE:
			return IS_INCLUDED_IN;
		case IS_DERIVED_FROM_VALUE:
			return IS_DERIVED_FROM;
		case DERIVES_TO_VALUE:
			return DERIVES_TO;
		case IS_KIND_OF_VALUE:
			return IS_KIND_OF;
		case IS_GENERALIZATION_OF_VALUE:
			return IS_GENERALIZATION_OF;
		case IS_PART_OF_VALUE:
			return IS_PART_OF;
		case CONTAINS_VALUE:
			return CONTAINS;
		case LEADS_TO_VALUE:
			return LEADS_TO;
		case COMES_FROM_VALUE:
			return COMES_FROM;
		case JUSTIFIES_VALUE:
			return JUSTIFIES;
		case IS_JUSTIFIED_BY_VALUE:
			return IS_JUSTIFIED_BY;
		case PRECEDES_VALUE:
			return PRECEDES;
		case SUPERSEDES_VALUE:
			return SUPERSEDES;
		case REFERS_TO_VALUE:
			return REFERS_TO;
		case REFINES_TO_VALUE:
			return REFINES_TO;
		case IS_REFINED_FROM_VALUE:
			return IS_REFINED_FROM;
		case IS_REFERRED_BY_VALUE:
			return IS_REFERRED_BY;
		case CONFLICTS_WITH_VALUE:
			return CONFLICTS_WITH;
		case IS_RELATED_TO_VALUE:
			return IS_RELATED_TO;
		case IS_STITCHED_TO_VALUE:
			return IS_STITCHED_TO;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	private RelationshipKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string
	 * representation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

	public static RelationshipKind getReverseRelationshipKind(RelationshipKind kind) {

		switch (kind) {
		case FEATURES_IN:
			return IS_FEATURING;
		case IS_FEATURING:
			return FEATURES_IN;

		case CONTRIBUTES_COST_TO:
			return COST_IS_CONTRIBUTED_BY;
		case COST_IS_CONTRIBUTED_BY:
			return CONTRIBUTES_COST_TO;

		case CONTRIBUTES_PERFORMANCE_TO:
			return PERFORMANCE_IS_CONTRIBUTED_BY;
		case PERFORMANCE_IS_CONTRIBUTED_BY:
			return CONTRIBUTES_PERFORMANCE_TO;

		case CONTRIBUTES_RISK_TO:
			return RISK_IS_CONTRIBUTED_BY;
		case RISK_IS_CONTRIBUTED_BY:
			return CONTRIBUTES_RISK_TO;

		case SUPPORTS:
			return IS_SUPPORTED_BY;
		case IS_SUPPORTED_BY:
			return SUPPORTS;

		case IS_ELABORATED_FROM:
			return ELABORATES;
		case ELABORATES:
			return IS_ELABORATED_FROM;

		case EXTENDS:
			return IS_EXTENDED_BY;
		case IS_EXTENDED_BY:
			return EXTENDS;

		case ILLUSTRATES:
			return IS_ILLUSTRATED_BY;
		case IS_ILLUSTRATED_BY:
			return ILLUSTRATES;

		case INCLUDES:
			return IS_INCLUDED_IN;
		case IS_INCLUDED_IN:
			return INCLUDES;

		case IS_DERIVED_FROM:
			return DERIVES_TO;
		case DERIVES_TO:
			return IS_DERIVED_FROM;

		case IS_KIND_OF:
			return IS_GENERALIZATION_OF;
		case IS_GENERALIZATION_OF:
			return IS_KIND_OF;

		case IS_PART_OF:
			return CONTAINS;
		case CONTAINS:
			return IS_PART_OF;

		case LEADS_TO:
			return COMES_FROM;
		case COMES_FROM:
			return LEADS_TO;

		case JUSTIFIES:
			return IS_JUSTIFIED_BY;
		case IS_JUSTIFIED_BY:
			return JUSTIFIES;

		case PRECEDES:
			return SUPERSEDES;
		case SUPERSEDES:
			return PRECEDES;

		case REFERS_TO:
			return IS_REFERRED_BY;
		case IS_REFERRED_BY:
			return REFERS_TO;

		case REFINES_TO:
			return IS_REFINED_FROM;
		case IS_REFINED_FROM:
			return REFINES_TO;

		default:
			return IS_ASSOCIATED_TO;
		}

	}

	public static List<RelationshipKind> getRelationshipKindByClassType(String classType) {
		List<RelationshipKind> kindByClass = new ArrayList<>();

		kindByClass.addAll(getRelationshipKindForSpecificationElement());

		switch (classType) {
		case "Actor":
			kindByClass.add(RelationshipKind.FEATURES_IN);
			break;
		case "Assumption":
			kindByClass.add(RelationshipKind.SUPPORTS);
			kindByClass.add(getReverseRelationshipKind(RelationshipKind.SUPPORTS));
			kindByClass.add(RelationshipKind.JUSTIFIES);
			kindByClass.add(RelationshipKind.CONFLICTS_WITH);
			break;
		case "Goal":
			kindByClass.add(RelationshipKind.SUPPORTS);
			kindByClass.add(getReverseRelationshipKind(RelationshipKind.SUPPORTS));
			kindByClass.add(RelationshipKind.JUSTIFIES);
			kindByClass.add(getReverseRelationshipKind(RelationshipKind.JUSTIFIES));
			kindByClass.add(RelationshipKind.CONFLICTS_WITH);
			break;
		case "Persona":
			kindByClass.add(RelationshipKind.FEATURES_IN);
			break;
		case "Requirement":
			kindByClass.add(getReverseRelationshipKind(RelationshipKind.JUSTIFIES));
			break;
		case "UseCase":
			kindByClass.add(RelationshipKind.EXTENDS);
			kindByClass.add(getReverseRelationshipKind(RelationshipKind.EXTENDS));
			kindByClass.add(RelationshipKind.INCLUDES);
			kindByClass.add(getReverseRelationshipKind(INCLUDES));
			break;
		case "Scenario":
			kindByClass.add(getReverseRelationshipKind(FEATURES_IN));
			break;
		case "Diagram":
			kindByClass.add(RelationshipKind.IS_STITCHED_TO);
			break;
		case "Vision":
			kindByClass.add(RelationshipKind.JUSTIFIES);
			break;
		case "GlossaryTerm":
			kindByClass.add(getReverseRelationshipKind(RelationshipKind.REFERS_TO));
			break;
		case "Glossary":
			kindByClass.add(getReverseRelationshipKind(RelationshipKind.REFERS_TO));
			break;
		default:
			kindByClass.removeAll(getRelationshipKindForSpecificationElement());
			kindByClass.addAll(getActiveKinds());
			kindByClass.addAll(getPassiveKinds());
			kindByClass.addAll(getSymmetricalKinds());
		}

		return kindByClass;
	}

	private static List<RelationshipKind> getRelationshipKindForSpecificationElement() {
		List<RelationshipKind> kinds = new ArrayList<>();

		kinds.add(RelationshipKind.CONTRIBUTES_COST_TO);
		kinds.add(getReverseRelationshipKind(RelationshipKind.CONTRIBUTES_COST_TO));
		kinds.add(RelationshipKind.CONTRIBUTES_PERFORMANCE_TO);
		kinds.add(getReverseRelationshipKind(RelationshipKind.CONTRIBUTES_PERFORMANCE_TO));
		kinds.add(RelationshipKind.CONTRIBUTES_RISK_TO);
		kinds.add(getReverseRelationshipKind(RelationshipKind.CONTRIBUTES_RISK_TO));
		kinds.add(RelationshipKind.ELABORATES);
		kinds.add(getReverseRelationshipKind(RelationshipKind.ELABORATES));
		kinds.add(RelationshipKind.DERIVES_TO);
		kinds.add(getReverseRelationshipKind(RelationshipKind.DERIVES_TO));
		kinds.add(RelationshipKind.IS_KIND_OF);
		kinds.add(getReverseRelationshipKind(RelationshipKind.IS_KIND_OF));
		kinds.add(RelationshipKind.CONTAINS);
		kinds.add(getReverseRelationshipKind(RelationshipKind.CONTAINS));
		kinds.add(RelationshipKind.LEADS_TO);
		kinds.add(getReverseRelationshipKind(RelationshipKind.LEADS_TO));
		kinds.add(RelationshipKind.PRECEDES);
		kinds.add(getReverseRelationshipKind(RelationshipKind.PRECEDES));
		kinds.add(RelationshipKind.REFERS_TO);
		kinds.add(getReverseRelationshipKind(RelationshipKind.ILLUSTRATES));
		kinds.add(RelationshipKind.IS_ASSOCIATED_TO);
		kinds.add(RelationshipKind.IS_RELATED_TO);

		return kinds;
	}

} // RelationshipKind
