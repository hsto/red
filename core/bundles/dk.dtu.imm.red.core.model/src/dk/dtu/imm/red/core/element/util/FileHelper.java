package dk.dtu.imm.red.core.element.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

public class FileHelper {

	public static String getFilePath(EObject eObject) {
		if (eObject == null) {
			return null;
		}

		Resource eResource = eObject.eResource();

		if (eResource == null) {
			return null;
		}

		URI uri = eResource.getURI();

		if (uri == null) {
			return null;
		}

		return uri.toFileString();
	}

	public static File getPhysicalFile(EObject eObject) {
		String filePath = getFilePath(eObject);

		if (filePath == null) {
			return null;
		}

		return new File(filePath);
	}

	public static String getFileName(EObject eObject) {
		File physicalFile = getPhysicalFile(eObject);
		
		if (physicalFile == null) {
			return null;
		}

		return physicalFile.getName();
	}
	
	public static String getDirectoryPath(EObject eObject) {
		File physicalFile = getPhysicalFile(eObject);
		File parentPhysicalFile = physicalFile.getParentFile();
		
		if (parentPhysicalFile == null) {
			return null;
		}
		
		return parentPhysicalFile.getAbsolutePath();
	}
	
	/**
	 * Join a list of paths ensuring that each path is separated by a separator.
	 * @param paths The list of paths to be joined.
	 * @return The joined path.
	 */
	public static String join(List<String> paths) {
		StringBuilder sb = new StringBuilder(paths.get(0));
		for (int i = 1; i < paths.size(); i++) {
			String previousPath = paths.get(i-1);
			String path = paths.get(i);
			if (!previousPath.endsWith(File.separator)) {
				sb.append(File.separator);
			}
			sb.append(path);
		}
		return sb.toString();
	}
	
	/**
	 * Join a list of paths ensuring that each path is separated by a separator.
	 * @param paths The list of paths to be joined.
	 * @return The joined path.
	 */
	public static String join(String... paths) {
		return join(Arrays.asList(paths));
	}

}
