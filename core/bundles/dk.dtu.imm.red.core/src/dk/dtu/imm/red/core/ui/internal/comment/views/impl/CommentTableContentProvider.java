package dk.dtu.imm.red.core.ui.internal.comment.views.impl;

import java.text.SimpleDateFormat;

import org.agilemore.agilegrid.DefaultContentProvider;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentCategory;
import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.comment.IssueComment;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.text.TextFactory;

public class CommentTableContentProvider extends DefaultContentProvider {
	
	private CommentList commentList;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	
	public CommentTableContentProvider() {
		super();
	}
	
	public void setContent(CommentList commentList) {
		this.commentList = commentList;
	}
	
	@Override
	public void doSetContentAt(int row, int col, Object value) {
		if (commentList == null) {
			return;
		}
		
		Comment comment = commentList.getComments().get(row);
		
		if (comment == null) {
			super.doSetContentAt(row, col, value);
		}
		
		switch (col) {
			case CommentTableLayoutAdviser.DATE_COLUMN:
				break;
			case CommentTableLayoutAdviser.AUTHOR_COLUMN:
				comment.getAuthor().setName((String) value);
				break;
			case CommentTableLayoutAdviser.COMMENT_COLUMN:
				Text text = TextFactory.eINSTANCE.createText(value.toString());
				comment.setText(text);
				break;
			case CommentTableLayoutAdviser.STATUS_COLUMN:
				comment.setCategory(CommentCategory.get((String) value));
				break;
			default:
				doSetContentAt(row, col, value);
		}
	}
	
	@Override
	public Object doGetContentAt(int row, int col) {
		if (commentList == null) {
			return null;
		}

		if (row < 0 || row >= commentList.getComments().size()) {
			return null;
		}

		Comment comment = commentList.getComments().get(row);

		if (comment == null) {
			return super.doGetContentAt(row, col);
		}
		switch (col) {
		case CommentTableLayoutAdviser.DATE_COLUMN:
			return sdf.format(comment.getTimeCreated());
		case CommentTableLayoutAdviser.AUTHOR_COLUMN:
			if (comment instanceof IssueComment) {
				return "Generated";
			} else if (comment.getAuthor() != null) {
				return comment.getAuthor().getName();
			} else {
				return "";
			}
		case CommentTableLayoutAdviser.COMMENT_COLUMN:
			return comment.getText().toString();
		case CommentTableLayoutAdviser.STATUS_COLUMN:
			if (comment instanceof IssueComment)
				return ((IssueComment) comment).getSeverity().getLiteral();
			else
				return comment.getCategory().getLiteral();
		default:
			return super.doGetContentAt(row, col);
		}
	}
}
