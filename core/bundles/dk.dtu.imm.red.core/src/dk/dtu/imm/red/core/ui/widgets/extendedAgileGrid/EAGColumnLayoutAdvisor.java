package dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid; 
import org.agilemore.agilegrid.ScalableColumnLayoutAdvisor;

public class EAGColumnLayoutAdvisor<T> extends ScalableColumnLayoutAdvisor   {	

	private EAGPresenter<T> presenter;

	public EAGColumnLayoutAdvisor(ExtendedAgileGrid<T> reflAgileGrid,
			EAGPresenter<T> presenter) {
		super(reflAgileGrid);
		this.presenter = presenter;
	}  
	 
	@Override
	public int getRowCount() {  
		return ((ExtendedAgileGrid<T>)this.agileGrid).getPresenter().getDataSourceView().size();
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int getColumnCount() { 
		int columnCount = ((ExtendedAgileGrid)this.agileGrid).getPresenter().getColumnDefinitions().length;
		return columnCount;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int getInitialColumnWidth(final int column) {  
		
		int columnWidth =   ((ExtendedAgileGrid)this.agileGrid).getPresenter().getColumnDefinitions()[column].getColumnWidth();
		return  columnWidth;
	}
	
	@Override
	public String getTopHeaderLabel(final int col) { 
		if(col <= presenter.getColumnDefinitions().length) {
			return presenter.getColumnDefinitions()[col].getColumnName();
		}
		else {
			return super.getTopHeaderLabel(col);
		} 
	}  
	
	 
	
	
	 
	
}
