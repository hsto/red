package dk.dtu.imm.red.core.ui.internal.effortestimation.views;

import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Map;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.views.IViewPresenter;

public interface EffortEstimationViewPresenter extends IViewPresenter, PropertyChangeListener {

	void inputChanged(Element editorElement); 
	Element getInput(); 
	Project getFoldersProject(); 
	Map<String, List<Element>> getRequirementElementMap(List<Element> content); 
}
