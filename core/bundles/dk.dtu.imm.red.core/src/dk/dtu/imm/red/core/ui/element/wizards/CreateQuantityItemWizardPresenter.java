package dk.dtu.imm.red.core.ui.element.wizards;

import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;
import dk.dtu.imm.red.core.element.QuantityLabel;

public interface CreateQuantityItemWizardPresenter extends IWizardPresenter {
	
	void wizardFinished();

	QuantityLabel getselectedLabel();
	
	void setSelectedLabel(QuantityLabel selectedLabel);
}
