package dk.dtu.imm.red.core.ui;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.editors.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

public class AgileTableTextCellEditor extends TextCellEditor {

	public AgileTableTextCellEditor(AgileGrid agileGrid) {
		this(agileGrid, SWT.MULTI);
	}
	
	static int count = 0;
	
	public AgileTableTextCellEditor(AgileGrid agileGrid, int style) {
		super(agileGrid, style);
		
		text.addListener(SWT.KeyUp, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (event.character == '\r' && (event.stateMask & SWT.ALT) != 0) {
					int position = text.getCaretPosition();
					StringBuffer sb = new StringBuffer(doGetValue().toString());
					sb.insert(position, "\r\n");
					doSetValue(sb.toString());
					text.setSelection(position+2, position+2);
				}
			}
		});
	}
	
}
