package dk.dtu.imm.red.core.ui.internal.effortestimation.views.impl;

import java.beans.PropertyChangeEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set; 

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListenerImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentCategory;
import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.impl.ValidationObservable;
import dk.dtu.imm.red.core.element.impl.ValidationObserver;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.internal.comment.views.CommentView;
import dk.dtu.imm.red.core.ui.internal.comment.views.CommentViewPresenter;
import dk.dtu.imm.red.core.ui.internal.effortestimation.views.EffortEstimationView;
import dk.dtu.imm.red.core.ui.internal.effortestimation.views.EffortEstimationViewPresenter;
import dk.dtu.imm.red.core.ui.internal.validation.views.ValidationView;
import dk.dtu.imm.red.core.ui.internal.validation.views.ValidationViewPresenter;
import dk.dtu.imm.red.core.ui.views.BaseViewPresenter;
import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.user.User;
import dk.dtu.imm.red.core.user.UserFactory;
import dk.dtu.imm.red.core.validation.ErrorMessage;
import dk.dtu.imm.red.core.validation.Severity;
import dk.dtu.imm.red.core.validation.ValidationFactory;
import dk.dtu.imm.red.core.validation.impl.ErrorMessageImpl;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;  

public final class EffortEstimationViewPresenterImpl extends BaseViewPresenter 
	implements EffortEstimationViewPresenter { 

	/**
	 * the view of this presenter.
	 */
	protected EffortEstimationView view;
	protected Element shownElement; 
	private EffortEstimationViewModel viewModel;
	private Element element;
	private UsecasePoint usecasePoint;  

	public EffortEstimationViewPresenterImpl(final EffortEstimationView view, final EffortEstimationViewModel viewModel) {
		super(); 
		this.view = view;  
		this.viewModel = viewModel;  
	} 
	
	@Override
	public Project getFoldersProject() { 
		List<Project> currentlyOpenedProjects = WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects(); 
		Project p =  currentlyOpenedProjects.size() > 0 ? currentlyOpenedProjects.get(0) : null; 
		
		if(p!= null) {			
			if (p.getEffort() == null) {
				p.createDefaultUsecasePoint();
			}			
			usecasePoint = p.getEffort();
			return p;
		}
		
		return null;
	
	}  

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		// TODO Auto-generated method stub
		
	}  

	@Override
	public void inputChanged(Element editorElement) {
		this.element = editorElement;
		
	}
	
	@Override
	public Map<String, List<Element>> getRequirementElementMap(List<Element> content) {
		
		return usecasePoint.getElementMap(content);
		
//		if(element instanceof Project) {
//			Workspace projectWorkspace = (Workspace) element.eContainer();
//			return usecasePoint.getElementMap(projectWorkspace.getContents());
//		}
//		else if(element instanceof Group) {
//			return usecasePoint.getElementMap(((Group) element).getContents());
//		} 
//		else {
//			List<Element> l = new ArrayList<Element>();
//			l.add(element);
//			return usecasePoint.getElementMap(l);
//		} 
	} 

	@Override
	public Element getInput() { 
		return this.element;
	}  
}
