package dk.dtu.imm.red.core.ui.internal.validation.views;

import org.eclipse.ui.IViewPart;

public interface ValidationView extends IViewPart { 
	String ID = "dk.dtu.imm.red.core.validation.view"; 
	void updateValidationView();
	void validationRequested(); 
} 