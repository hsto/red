package dk.dtu.imm.red.core.ui.internal.element.handlers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLParserPoolImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.ElementPackageImpl;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.ui.internal.project.editors.impl.filesListData;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

/**
 * Handler for the load file. Opens a file dialog, which the user browse through
 * to load .
 *
 * @author Ahmed Fikry
 *
 */

public class LoadFileHandler extends AbstractHandler implements IHandler {
	private JList listbox;
	private DefaultListModel listModel = new DefaultListModel();
	protected File listoffile;
	public static LoadFileHandler s;
	static ResourceSet resourceSet;

	String _path = "";

	@SuppressWarnings("resource")
	public Object execute(ExecutionEvent event) {

		FileDialog dialog = new FileDialog(HandlerUtil.getActiveShell(event),
			SWT.OPEN | SWT.MULTI);
		dialog.setText("Load File");

		String workspacePath = PreferenceUtil.getPreferenceString(PreferenceConstants.PROJECT_PREF_WORKSPACE_PATH);
		if (workspacePath != null && !workspacePath.isEmpty()) {
			dialog.setFilterPath(workspacePath);
		}

		String[] filterExtensions = new String[] { "*.red" };

		// dialog.setFilterExtensions(filterExtensions);
		// dialog.getFilterExtensions();
		String filenameAndpath = dialog.open();

		if (filenameAndpath == null) {
			return null;
		}

		String[] listOffilesNames = dialog.getFileNames();
		String path = filenameAndpath.replace(listOffilesNames[0], "");
		list_References.clear();
		for (int i = 0; i < listOffilesNames.length; i++) {
			FilesReferences(listOffilesNames[i], path);
		}

		String projectName = "";
		java.io.File dir = new java.io.File(path);
		java.io.File[] pro = dir.listFiles();

		EList<Project> p = WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
			.getCurrentlyOpenedProjects();
		for (int d = 0; d < pro.length; d++) {

			if (pro[d].getName().toLowerCase().endsWith(".redproject")) {
				projectName = pro[d].getName();

				if ((p.size() == 0)) {

					openProject(path, projectName);
				}

				filesListData.ProjectNameAndPath = path + projectName;
				filesListData.path = path;

			}
		}
		ArrayList<String> names = new ArrayList<>();

		if (listOffilesNames.length > 0) {
			ArrayList<String> uniqueIDsInProject = new ArrayList<>();
			for (File file : p.get(0).getListofopenedfilesinproject()) {
				uniqueIDsInProject.add(file.getUniqueID());
			}
			// for (int i = 0; i <
			// p.get(0).getListofopenedfilesinproject().size(); i++) {
			// listoffile = (File) p.get(0).getListofopenedfilesinproject()
			// .get(i);
			//
			// names.add(listoffile.getName());
			// }
			for (String fileName : listOffilesNames) {
				ResourceSet resourceSet = new ResourceSetImpl();
				URI uri = URI.createFileURI(path + fileName);
				Resource resource = resourceSet.getResource(uri, true);
				if (resource == null || resource.getContents().size() == 0
					|| !(resource.getContents().get(0) instanceof File)) {
					MessageDialog md = new MessageDialog(Display.getCurrent()
						.getActiveShell(), "Load File", null,
						"The file you want to load is not a valid RED-file",
						MessageDialog.INFORMATION, new String[] { "ok" }, 1);
					md.open();
					return null;
				}
				File file = (File) resource.getContents().get(0);
				String uniqueID = file.getUniqueID();
				resource.unload();
				if (uniqueIDsInProject.contains(uniqueID)) {
					LoadFiles(fileName, path);
				} else {
					MessageDialog md = new MessageDialog(Display.getCurrent()
						.getActiveShell(), "Load File", null,
						"The file you want to load does not belong to "
							+ p.get(0).getName(),
						MessageDialog.INFORMATION, new String[] { "ok" }, 1);
					md.open();
					return null;
				}
			}
			// for (int x = 0; x < listOffilesNames.length; x++) {
			// if (names.contains(listOffilesNames[x])
			// && listoffile.getUri().equals(
			// path + listoffile.getName())) {
			// LoadFiles(listOffilesNames[x], path);
			// } else {
			// MessageDialog md = new MessageDialog(Display.getCurrent()
			// .getActiveShell(), "Load File", null,
			// "The file you want to load does not belong to "
			// + p.get(0).getName(),
			// MessageDialog.INFORMATION, new String[] { "ok" }, 1);
			// md.open();
			// return null;
			// }
			// }

		}
		if (list_References.size() > listOffilesNames.length) {
			MessageDialog md = new MessageDialog(
				Display.getCurrent().getActiveShell(),
				"Load Refernces",
				null,
				"The selected file has refernces to other files in this project, would you like to load them?",
				MessageDialog.QUESTION, new String[] { "Yes", "No" }, 1);
			int response = md.open();
			if (response == md.OK) {
				int[] arr = new int[listOffilesNames.length];
				for (int z = 0; z < listOffilesNames.length; z++) {
					int fileIndex = list_References
						.indexOf(listOffilesNames[z]);
					arr[z] = fileIndex;
				}
				for (int j = 0; j < arr.length; j++) {
					list_References.remove(arr[j]);
				}
				ListUi(list_References, path);
			}
		}

		return null;
	}

	private void openProject(String path, String projectName) {
		ElementPackageImpl.init();

		resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.getResource(
			URI.createFileURI(path + projectName), true);

		resource.eAdapters().add(new EContentAdapter() {
			@Override
			public void notifyChanged(Notification notification) {
				// TODO Auto-generated method stub
				super.notifyChanged(notification);
			}
		});

		try {
			Map<Object, Object> loadOptions = new HashMap<Object, Object>();
			loadOptions.put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, Boolean.TRUE);
			resource.load(loadOptions);
			;
		} catch (IOException e) {
			e.printStackTrace();
			LogUtil.logError(e);
		}

		Project project = (Project) resource.getContents().get(0);
		WorkspaceFactory.eINSTANCE.getWorkspaceInstance().addOpenedProject(
			project);
		ArrayList<Element> loadedElements = new ArrayList<Element>();
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();

		for (EObject content : resource.getContents()) {
			if (content instanceof Element) {
				if (!workspace.getContents().contains(content)) {
					loadedElements.add((Element) content);
				}
			}
		}

		workspace.getContents().addAll(loadedElements);

		TreeIterator<EObject> iterator = resource.getAllContents();

		// Now inform all listening plugins that elements have been opened
		while (iterator.hasNext()) {
			EObject object = iterator.next();
			if (!(object instanceof Element)) {
				continue;
			}
			final Element element = (Element) object;

			// Set the URI of the file, in case it has been moved from where
			// it was saved.
			element.setUri(path + projectName);
		}
	}

	public static File LoadFiles(String filename, String filePath) {
		File file;
		ElementPackageImpl.init();

		resourceSet = WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
			.getCurrentlyOpenedProjects().get(0).getResourceSet();
		// Add by Henry to ignore unknown feature (attributes in a file)
		resourceSet.getLoadOptions().put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, Boolean.TRUE);

		Resource resource = resourceSet.getResource(URI
			.createFileURI(filePath + filename), true);


		// add by Henry to unload the resource and reload it again
		resource.unload();
		resource.eAdapters().add(new EContentAdapter() {
			@Override
			public void notifyChanged(Notification notification) {
				// TODO Auto-generated method stub
				super.notifyChanged(notification);
			}
		});

		try {
			Map<Object, Object> loadOptions = new HashMap<Object, Object>();
			loadOptions.put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, Boolean.TRUE);
			resource.load(loadOptions);
		} catch (IOException e) {
			e.printStackTrace();
			LogUtil.logError(e);
		}

		if (resource.getContents().size() == 0) {
			return null;
		}

		file = (File) resource.getContents().get(0);
		WorkspaceFactory.eINSTANCE.getWorkspaceInstance().addOpenedFile(file);

		ArrayList<Element> loadedElements = new ArrayList<Element>();
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();

		for (EObject content : resource.getContents()) {
			if (content instanceof Element) {
				if (!workspace.getContents().contains(content)) {
					loadedElements.add((Element) content);
				}
			}
		}

		workspace.getContents().addAll(loadedElements);

		TreeIterator<EObject> iterator = resource.getAllContents();

		// Now inform all listening plugins that elements have been opened
		while (iterator.hasNext()) {
			EObject object = iterator.next();
			if (!(object instanceof Element)) {
				continue;
			}
			final Element element = (Element) object;

			// get all reletionships in element
			for (int p = 0; p < element.getRelatesTo().size(); p++) {
				element.getRelatesTo().get(p).getToElement();
				element.getRelatesTo();
			}
			for (int p = 0; p < element.getRelatedBy().size(); p++) {
				element.getRelatedBy().get(p).getFromElement();
				element.getRelatedBy();
			}

			IConfigurationElement[] configurationElements = Platform
				.getExtensionRegistry().getConfigurationElementsFor(
					"dk.dtu.imm.red.core.load");
		}

		return file;
	}

	public ArrayList<String> list_References = new ArrayList<String>();

	// Getting all the references in a list
	public ArrayList<String> FilesReferences(String fileName, String path) {
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory
				.newInstance();
			DocumentBuilder documentBuilder = documentFactory
				.newDocumentBuilder();
			Document doc = documentBuilder.parse(new FileInputStream(path
				+ fileName));

			if (!list_References.contains(fileName)) {
				list_References.add(fileName);
				NodeList nList = doc.getElementsByTagName("toElement");
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
						int indexOfSharp = eElement.getAttribute("href")
							.indexOf("#");
						if (!list_References.contains(eElement.getAttribute(
							"href").substring(0, indexOfSharp))) {
							list_References.add(eElement.getAttribute("href")
								.substring(0, indexOfSharp));
							// list_References.add(eElement.getAttribute("href").replace("#/",
							// ""));
						}
					}
				}

				int fileIndex = list_References.indexOf(fileName) + 1;
				if (list_References.size() > fileIndex)
					FilesReferences(list_References.get(fileIndex), path);
			}
		} catch (Exception ex) {

		}

		return list_References;
	}

	ArrayList<String> selected;

	public void ListUi(ArrayList<String> fileList, String path) {
		_path = path;
		final JFrame frame = new JFrame();
		Dimension d = new Dimension(400, 300);
		frame.setPreferredSize(d);

		JPanel content = new JPanel();
		JPanel controls = new JPanel();
		JButton Reload = new JButton("Reload");
		JButton SelectAll = new JButton("Select All");

		BorderLayout l = new BorderLayout();
		content.setLayout(l);
		listbox = new JList(fileList.toArray());
		listbox.setBackground(Color.WHITE);
		JScrollPane listS = new JScrollPane(listbox);
		listS.setSize(200, 200);
		content.add(listS);
		controls.add(SelectAll);
		controls.add(Reload);
		controls.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		JLabel label = new JLabel(
			"Plese select one or a set of files from the list below to reload\n\n.");
		label.setPreferredSize(new Dimension(50, 50));
		frame.add(label, BorderLayout.PAGE_START);
		frame.add(content, BorderLayout.CENTER);
		frame.add(controls, BorderLayout.SOUTH);
		Image image = Toolkit.getDefaultToolkit().getImage(
			getClass().getResource("/icons/red_icon_128.png"));
		frame.setIconImage(image);

		frame.setTitle("Reload References");

		frame.pack();
		frame.setLocationRelativeTo(null);
		content.setVisible(true);
		frame.setVisible(true);

		SelectAll.addActionListener((new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				int start = 0;
				int end = listbox.getModel().getSize() - 1;
				if (end >= 0) {
					listbox.setSelectionInterval(start, end);
				}
			}
		}));
		Reload.addActionListener((new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selected = new ArrayList();
				selected.addAll(listbox.getSelectedValuesList());

				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						for (int i = 0; i < selected.size(); i++) {

							LoadFiles(selected.get(i), _path);
						}
						frame.dispose();
					}
				});
			}
		}));

	}

	public String FilesLockStatus(String filePathAndName) {
		java.io.File localFile = new java.io.File(filePathAndName);
		if (!localFile.exists())
			return "File has been moved";

		String LockStatus = "";
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory
				.newInstance();
			DocumentBuilder documentBuilder = documentFactory
				.newDocumentBuilder();
			Document doc = documentBuilder.parse(new FileInputStream(
				filePathAndName));

			NodeList nList = doc.getElementsByTagName("file:File");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
					LockStatus = eElement.getAttribute("lockStatus");
				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
			System.exit(1);
		}
		filesListData.lock = LockStatus;

		return LockStatus;
	}

	public String FilesDescription(String filePathAndName)
	{
		java.io.File localFile = new java.io.File(filePathAndName);
		if (!localFile.exists())
			return "File has been moved";

		String filedescription = "";
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			Document doc = documentBuilder.parse(new FileInputStream(filePathAndName));

			NodeList nList = doc.getElementsByTagName("fragments");

			for (int temp = 0; temp < nList.getLength(); temp++)
			{
				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE)
				{
					org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
					// eElement.getFirstChild();
					// getChildNodes();

					filedescription = eElement.getAttribute("text").replace("<P>", "").replace("</P>", "");

				}
			}
		} catch (Exception ex)
		{
			System.out.println(ex);
			System.exit(1);
		}
		// filesListData.lock = filedescription;

		return filedescription;
	}

}
