package dk.dtu.imm.red.core.ui.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.file.wizards.impl.CreateFileWizardImpl;

public abstract class BaseNewWizard extends BaseWizard implements INewWizard {

	//Page for adding basic information (label,name,description)
	protected ElementBasicInfoPage elementBasicInfoPage;
	//Page for choosing the location to put new specification element
	//when it is used for creating folder or glossary, ask for display name
	//otherwise by default don't ask for anything
	protected DisplayInfoPage displayInfoPage;	
	
	//Page for asking a new filename, only for creating new file
	protected InputFilenamePage inputFileNamePage;
	
	//Page for browsing a location to save something, usually only for saving report or export
	protected ChooseElementLocationPage chooseElementLocationPage; 
	
	//Page for choosing directory, usually used for a new project
	protected ChooseDirectoryLocation chooseDirectoryLocation;	
	
	protected Class clazz;
	
	
	public BaseNewWizard(String type, Class clazz, boolean askForDisplayName) {
		
		elementBasicInfoPage = new ElementBasicInfoPage(type);
		displayInfoPage = new DisplayInfoPage(type, askForDisplayName);
		inputFileNamePage = new InputFilenamePage();
		chooseDirectoryLocation = new ChooseDirectoryLocation();
		chooseElementLocationPage = new ChooseElementLocationPage(type);
		this.clazz = clazz;
	}
	
	public BaseNewWizard(String type, Class clazz) {
		this(type, clazz, false);
	}
	
	@Override
	public void addPages() {
		//super.addPages();
		
		addPage(elementBasicInfoPage);
		
		addPage(displayInfoPage);
		
		addPage(inputFileNamePage);		
			
		addPage(chooseElementLocationPage);
		
		addPage(chooseDirectoryLocation);

	}
	
	@Override
	public void pageOpenedOrClosed(final IWizardPage fromPage) {
		//should be obsolete now
//		if (fromPage == displayInfoPage) {
//			String displayName = displayInfoPage.getDisplayName();
//			chooseElementLocationPage.setInitialFileName(displayName);
//		}
	}
	
	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		if (page == displayInfoPage) {
			//for elements other than file, displayInfoPage should always be the last page in the wizard
			return null;
		}		
		else if (page == inputFileNamePage) {
			//for file creation or fileInput, inputFileNamePage should always be the last page in the wizard
			return null;
		}
		return super.getNextPage(page);
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		
	}
	
	@Override
	public boolean canFinish() {
		if (displayInfoPage.isPageComplete()) {
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();
			Group selectedParent = (Group) selection.getFirstElement();
			
			// if there is a selected parent, then we must obey its constraints.
			// otherwise, we don't care
			if (selectedParent != null) {
				return selectedParent.isValidContent(clazz);
			}
		}
		
		// let the client decide
		return true;
	}

}
