package dk.dtu.imm.red.core.ui.internal.validation.views.impl;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;

import dk.dtu.imm.red.core.ui.internal.validation.views.ValidationView;

public class ValidationComposite extends Composite {
	private Composite validationsTablePlaceholder;
	
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 * @param validationView 
	 */
	public ValidationComposite(Composite parent, int style, ValidationView validationView) {
		super(parent, style);
		setLayout(new GridLayout(3, false));  
		
		Label lblErrors = new Label(this, SWT.NONE);
		lblErrors.setText("Errors"); 
		
		Label lblWarnings = new Label(this, SWT.NONE);
		lblWarnings.setText("Warnings");
		
		Label lblInformation = new Label(this, SWT.NONE);
		lblInformation.setText("Information"); 
		
		validationsTablePlaceholder = new Composite(this, SWT.NONE); 
		validationsTablePlaceholder.setLayout(new GridLayout(1, false));
		validationsTablePlaceholder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1)); 
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	public Composite getValidationsTablePlaceholder() {
		return validationsTablePlaceholder;
	}


}
