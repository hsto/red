package dk.dtu.imm.red.core.ui.internal.validation.views;

import java.beans.PropertyChangeListener;

import dk.dtu.imm.red.core.ui.views.IViewPresenter;

public interface ValidationViewPresenter extends IViewPresenter, PropertyChangeListener {

	void validateModel();
	
}
