package dk.dtu.imm.red.core.ui.internal.project.wizards.impl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.project.operations.CreateProjectOperation;
import dk.dtu.imm.red.core.ui.internal.project.wizards.NewProjectWizard;
import dk.dtu.imm.red.core.ui.internal.project.wizards.NewProjectWizardPresenter;
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;

public class NewProjectWizardPresenterImpl extends BaseWizardPresenter
	implements NewProjectWizardPresenter{

	public NewProjectWizardPresenterImpl(final NewProjectWizard newProjectWizard) {
		super();
	}
	
	@Override
	public void wizardFinished(String name, String projectDescription,
			 String path) {
		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
				.getOperationSupport();

		CreateProjectOperation operation = 
				new CreateProjectOperation(name, projectDescription, path);

		operation.addContext(operationSupport.getUndoContext());

		IAdaptable info = new IAdaptable() {
			@Override
			public Object getAdapter(final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		try {
			OperationHistoryFactory.getOperationHistory().execute(operation, null,
					info);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
		
	}

}
