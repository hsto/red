package dk.dtu.imm.red.core.ui.wizards;

import java.io.File;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;


public class ChooseFileLocation extends WizardPage
		implements IWizardPage {

	protected Label locationLabel;
	protected Text locationText;
	protected Button browseButton;
	protected Text titleTextBox;

	protected Label validationLabel;

	protected String initialDirectoryName;
	protected String[] filterExtensions;


	public ChooseFileLocation(String title, String description, String[] filterExtensions ) {
		super("Choose File Location");
		setTitle(title);
		setDescription(description);
		this.filterExtensions = filterExtensions;
	}


	//@Override
	public void createControl(final Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(3, false);
		container.setLayout(layout);

		locationLabel = new Label(container, SWT.NONE);
		locationLabel.setText("File:");

		GridData locationTextLayout = new GridData();
		locationTextLayout.grabExcessHorizontalSpace = true;
		locationTextLayout.horizontalAlignment = SWT.FILL;
		locationTextLayout.minimumWidth = 150;
		locationText  = new Text(container, SWT.BORDER);
		locationText.setLayoutData(locationTextLayout);
		locationText.addListener(SWT.Modify, new Listener() {
			public void handleEvent(Event event) {
				if (locationText.getText() != null
						&& !locationText.getText().trim().isEmpty())
				{
					boolean validFilePath = false;

					File file = new File(locationText.getText());

					if (file.isFile())
					{
						setPageComplete(true);
						validationLabel.setVisible(false);
					} else
					{
						setPageComplete(false);
						validationLabel.setVisible(true);
					}

				}
			}
		});

		GridData validationLabelLayout = new GridData();
		validationLabelLayout.horizontalSpan = 3;
		validationLabel = new Label(container, SWT.NONE);
		validationLabel.setText("Path is invalid, please choose another location!");
		validationLabel.setLayoutData(validationLabelLayout);
		validationLabel.setVisible(false);


		browseButton = new Button(container, SWT.NONE);
		browseButton.setText("Browse");
		browseButton.addListener(SWT.Selection, new Listener() {

			public void handleEvent(final Event event) {
				FileDialog dialog = new FileDialog(parent.getShell(),
						SWT.OPEN | SWT.MULTI);
				dialog.setText("Choose File");

				dialog.setFilterExtensions(filterExtensions);
				dialog.getFilterExtensions();

				String workspacePath = PreferenceUtil.getPreferenceString(PreferenceConstants.PROJECT_PREF_WORKSPACE_PATH);
				if(workspacePath!=null && !workspacePath.isEmpty()){
					File f = new File(workspacePath);
					if(f.exists()){
						dialog.setFilterPath(workspacePath);
					}
				}

				String inputFilePath = dialog.open();

				if (inputFilePath == null) {
					return;
				}

				File inputFile = new File(inputFilePath);

				if (inputFile.exists())
				{
					setPageComplete(true);
					validationLabel.setVisible(false);
				} else
				{
					setPageComplete(false);
					validationLabel.setVisible(true);
				}

				if (inputFilePath != null) {
					// Set the text box to the new selection
					locationText.setText(inputFilePath);
					locationText.notifyListeners(SWT.Modify, null);
				}
			}
		});


		this.setPageComplete(false);
		this.setControl(container);

	}

	public String getFileName() {
		if (isPageComplete()) {
			return locationText.getText();
		} else {
			return null;
		}
	}

}

























