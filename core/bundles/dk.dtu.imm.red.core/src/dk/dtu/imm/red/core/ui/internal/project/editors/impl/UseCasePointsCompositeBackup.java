package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import dk.dtu.imm.red.core.ui.widgets.PropertySelection;
import dk.dtu.imm.red.core.ui.widgets.UsecasePointSummary;

public class UseCasePointsCompositeBackup extends Composite {
	private Combo cmbCapability;
	private Combo cmbTeamwork;
	private Combo cmbStaffContinuity;
	private Combo cmbSpecificationQuality;
	private Combo cmbProcessMaturity;
	private Combo cmbDeadlinePressure;
	private Combo cmbRequirementsStability;
	private Combo cmbNumberOfStakeholder;
	private Combo cmbSystemIntegrationChallenges;
	private Spinner spCapability;
	private Spinner spTeamwork;
	private Spinner spStaffContinuity;
	private Spinner spSpecificationQuality;
	private Spinner spProcessMaturity;
	private Spinner spDeadlinePressure;
	private Spinner spRequirementsStability;
	private Spinner spNumberOfStakeholders;
	private Spinner spSystemIntegrationChanges;
	private Spinner spProductivityFactor;
	private PropertySelection propertycostPerUsecasePoint; 
	

	private Combo cmbDistributedSystem;
	private Spinner spDistributedSystem;
	private Combo cmbPerformanceObjective;
	private Spinner spPerformanceObjective;
	private Combo cmbEndUserEffiency;
	private Spinner spEndUserEffiency;
	private Combo cmbComplexProcessing;
	private Spinner spComplexProcessing;
	private Combo cmbReusableCode;
	private Spinner spReusableCode;
	private Combo cmbEasyToInstall;
	private Spinner spEasyToInstall;
	private Combo cmbEasyToUse;
	private Spinner spEasyToUse;
	private Combo cmbPortable;
	private Spinner spPortable;
	private Combo cmbEasyToChange;
	private Spinner spEasyToChange;
	private Combo cmbConcurrentUse;
	private Spinner spConcurrentUse;
	private Combo cmbAccessForThirdParties;
	private Spinner spAccessForThirdParties;
	private Combo cmbTrainingNeeds;
	private Spinner spTrainingNeeds;

	

	private UsecasePointSummary cmpUsecasePointSummary; 
	

	private Combo cmbSecurity;
	private Spinner spSecurity;

	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public UseCasePointsCompositeBackup(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Composite composite_1 = new Composite(this, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		composite_1.setLayout(new GridLayout(2, true));
		
		Composite composite_2 = new Composite(composite_1, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		GridLayout gl_composite_2 = new GridLayout(1, false);
		gl_composite_2.horizontalSpacing = 0;
		gl_composite_2.marginHeight = 0;
		gl_composite_2.verticalSpacing = 0;
		gl_composite_2.marginWidth = 0;
		composite_2.setLayout(gl_composite_2);
		
		Group grpManagementFactor = new Group(composite_2, SWT.NONE);
		grpManagementFactor.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		grpManagementFactor.setText("Management Factor");
		grpManagementFactor.setLayout(new GridLayout(2, false)); 
		
		Label lblCapabilitiesOfChief = new Label(grpManagementFactor, SWT.NONE);
		lblCapabilitiesOfChief.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblCapabilitiesOfChief.setText("Capabilities of Chief Designer");
		
		Label lblWeightCapabilitiesOfChief = new Label(grpManagementFactor, SWT.NONE);
		lblWeightCapabilitiesOfChief.setText("Weight");
		
		cmbCapability = new Combo(grpManagementFactor, SWT.READ_ONLY);
		GridData gd_cmbCapability = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbCapability.widthHint = 200;
		cmbCapability.setLayoutData(gd_cmbCapability);
		
		spCapability = new Spinner(grpManagementFactor, SWT.BORDER);
		spCapability.setMaximum(999999999);
		spCapability.setDigits(2);
		
		Label lblTeamwork = new Label(grpManagementFactor, SWT.NONE);
		lblTeamwork.setText("Teamwork");
		
		Label lblWeightTeamwork = new Label(grpManagementFactor, SWT.NONE);
		lblWeightTeamwork.setText("Weight");
		
		
		cmbTeamwork = new Combo(grpManagementFactor, SWT.READ_ONLY);
		GridData gd_cmbTeamwork = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbTeamwork.widthHint = 200;
		cmbTeamwork.setLayoutData(gd_cmbTeamwork);
		cmbTeamwork.setBounds(0, 0, 155, 23);
		
		spTeamwork = new Spinner(grpManagementFactor, SWT.BORDER);
		spTeamwork.setMaximum(999999999);
		spTeamwork.setDigits(2);
		
		Label lblStaffContinuity = new Label(grpManagementFactor, SWT.NONE); 
		lblStaffContinuity.setText("Staff Continuity");
		
		Label lblWeightStaffContinuity = new Label(grpManagementFactor, SWT.NONE); 
		lblWeightStaffContinuity.setText("Weight");
		
		
		cmbStaffContinuity = new Combo(grpManagementFactor, SWT.READ_ONLY);
		GridData gd_cmbStaffContinuity = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbStaffContinuity.widthHint = 200;
		cmbStaffContinuity.setLayoutData(gd_cmbStaffContinuity);
		cmbStaffContinuity.setBounds(0, 0, 91, 23);
		
		spStaffContinuity = new Spinner(grpManagementFactor, SWT.BORDER);
		spStaffContinuity.setMaximum(999999999);
		spStaffContinuity.setDigits(2);
		
		Label lblSpecificationQuality = new Label(grpManagementFactor, SWT.NONE);
		lblSpecificationQuality.setText("Specification Quality");
		lblSpecificationQuality.setBounds(0, 0, 155, 15);
		
		Label lblWeightSpecificationQuality = new Label(grpManagementFactor, SWT.NONE);
		lblWeightSpecificationQuality.setText("Weight");
		
		cmbSpecificationQuality = new Combo(grpManagementFactor, SWT.READ_ONLY);
		GridData gd_cmbSpecificationQuality = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbSpecificationQuality.widthHint = 200;
		cmbSpecificationQuality.setLayoutData(gd_cmbSpecificationQuality);
		cmbSpecificationQuality.setBounds(0, 0, 91, 23);
		
		spSpecificationQuality = new Spinner(grpManagementFactor, SWT.BORDER);
		spSpecificationQuality.setMaximum(999999999);
		spSpecificationQuality.setDigits(2);
		
		Label lblProcessMaturity = new Label(grpManagementFactor, SWT.NONE);
		lblProcessMaturity.setText("Process Maturity");
		lblProcessMaturity.setBounds(0, 0, 155, 15);
		
		Label lblWeightProcessMaturity = new Label(grpManagementFactor, SWT.NONE);
		lblWeightProcessMaturity.setText("Weight");
		
		cmbProcessMaturity = new Combo(grpManagementFactor, SWT.READ_ONLY);
		GridData gd_cmbProcessMaturity = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbProcessMaturity.widthHint = 200;
		cmbProcessMaturity.setLayoutData(gd_cmbProcessMaturity);
		cmbProcessMaturity.setBounds(0, 0, 91, 23);
		
		spProcessMaturity = new Spinner(grpManagementFactor, SWT.BORDER);
		spProcessMaturity.setMaximum(999999999);
		spProcessMaturity.setDigits(2);
		
		Label lblDeadLinePressure = new Label(grpManagementFactor, SWT.NONE);
		lblDeadLinePressure.setText("Deadline Pressure");
		lblDeadLinePressure.setBounds(0, 0, 155, 15);
		
		Label lblWeightDeadlinePressure = new Label(grpManagementFactor, SWT.NONE);
		lblWeightDeadlinePressure.setText("Weight");
		
		cmbDeadlinePressure = new Combo(grpManagementFactor, SWT.READ_ONLY);
		GridData gd_cmbDeadlinePressure = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbDeadlinePressure.widthHint = 200;
		cmbDeadlinePressure.setLayoutData(gd_cmbDeadlinePressure);
		cmbDeadlinePressure.setBounds(0, 0, 91, 23);
		
		spDeadlinePressure = new Spinner(grpManagementFactor, SWT.BORDER);
		spDeadlinePressure.setMaximum(999999999);
		spDeadlinePressure.setDigits(2);
		
		Label lblRequirementsStability = new Label(grpManagementFactor, SWT.NONE);
		lblRequirementsStability.setText("Requirements Stability");
		lblRequirementsStability.setBounds(0, 0, 155, 15);
		
		Label lblWeightRequreimentStability = new Label(grpManagementFactor, SWT.NONE);
		lblWeightRequreimentStability.setText("Weight");
		
		cmbRequirementsStability = new Combo(grpManagementFactor, SWT.READ_ONLY);
		GridData gd_cmbRequirementsStability = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbRequirementsStability.widthHint = 200;
		cmbRequirementsStability.setLayoutData(gd_cmbRequirementsStability);
		cmbRequirementsStability.setBounds(0, 0, 91, 23);
		
		spRequirementsStability = new Spinner(grpManagementFactor, SWT.BORDER);
		spRequirementsStability.setMaximum(999999999);
		spRequirementsStability.setDigits(2);
		
		Label lblNumberOfStakeholders = new Label(grpManagementFactor, SWT.NONE);
		lblNumberOfStakeholders.setText("Number of Stakeholders");
		lblNumberOfStakeholders.setBounds(0, 0, 155, 15);
		
		Label lblWeightNumberOfStakeholders = new Label(grpManagementFactor, SWT.NONE);
		lblWeightNumberOfStakeholders.setText("Weight");
		
		cmbNumberOfStakeholder = new Combo(grpManagementFactor, SWT.READ_ONLY);
		GridData gd_cmbNumberOfStakeholder = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbNumberOfStakeholder.widthHint = 200;
		cmbNumberOfStakeholder.setLayoutData(gd_cmbNumberOfStakeholder);
		cmbNumberOfStakeholder.setBounds(0, 0, 91, 23);
		
		spNumberOfStakeholders = new Spinner(grpManagementFactor, SWT.BORDER);
		spNumberOfStakeholders.setMaximum(999999999);
		spNumberOfStakeholders.setDigits(2);
		
		Label lblSystemIntegrationChallenges = new Label(grpManagementFactor, SWT.NONE);
		lblSystemIntegrationChallenges.setText("System Integration Challenges     ");
		lblSystemIntegrationChallenges.setBounds(0, 0, 155, 15);
		
		Label lblWeightSystemIntegrationChanges = new Label(grpManagementFactor, SWT.NONE);
		lblWeightSystemIntegrationChanges.setText("Weight");
		
		cmbSystemIntegrationChallenges = new Combo(grpManagementFactor, SWT.READ_ONLY);
		
		GridData gd_cmbSystemIntegrationChallenges = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cmbSystemIntegrationChallenges.widthHint = 200;
		cmbSystemIntegrationChallenges.setLayoutData(gd_cmbSystemIntegrationChallenges);
		cmbSystemIntegrationChallenges.setBounds(0, 0, 91, 23);
		
		spSystemIntegrationChanges = new Spinner(grpManagementFactor, SWT.BORDER);
		spSystemIntegrationChanges.setMaximum(999999999);
		spSystemIntegrationChanges.setDigits(2);
		
		Group grpProductivityFactor = new Group(composite_2, SWT.NONE);
		grpProductivityFactor.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		grpProductivityFactor.setLayout(new GridLayout(2, false));
		grpProductivityFactor.setText("Productivity Factor");
		
		spProductivityFactor = new Spinner(grpProductivityFactor, SWT.BORDER);
		spProductivityFactor.setMaximum(999999999);
		spProductivityFactor.setDigits(2);
		
		Label lblManhoursPerUcp = new Label(grpProductivityFactor, SWT.NONE);
		lblManhoursPerUcp.setText("Hours per Use Case Point");
		
		Group gpCost = new Group(composite_2, SWT.NONE);
		gpCost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		gpCost.setText("Cost Factor");
		gpCost.setLayout(new GridLayout(1, false));
		
		propertycostPerUsecasePoint = new PropertySelection(gpCost, SWT.NONE);
		propertycostPerUsecasePoint.getLblUnitDescription().setText("Cost per hour");
		GridData gridData_1 = (GridData) propertycostPerUsecasePoint.getCmbKind().getLayoutData();
		gridData_1.horizontalAlignment = SWT.LEFT;
		gridData_1.grabExcessVerticalSpace = false;
		gridData_1.grabExcessHorizontalSpace = false;
		GridData gridData_3 = (GridData) propertycostPerUsecasePoint.getCmbUnit().getLayoutData();
		gridData_3.horizontalAlignment = SWT.LEFT;
		gridData_3.grabExcessHorizontalSpace = true;
		gridData_3.grabExcessVerticalSpace = false;
		((GridData) propertycostPerUsecasePoint.getCmbKind().getLayoutData()).exclude = true;
		propertycostPerUsecasePoint.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		Group grpGrptechnologyfactor = new Group(composite_1, SWT.NONE);
		grpGrptechnologyfactor.setLayout(new GridLayout(2, false));
		grpGrptechnologyfactor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpGrptechnologyfactor.setText("Technology Factor");
		
		Label lblDistributedSystem = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblDistributedSystem.setText("Distributed system");
		lblDistributedSystem.setBounds(0, 0, 226, 15);
		
		Label label_1 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_1.setText("Weight");
		
		cmbDistributedSystem = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		GridData gd_cmbDistributedSystem = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbDistributedSystem.widthHint = 200;
		cmbDistributedSystem.setLayoutData(gd_cmbDistributedSystem);
		cmbDistributedSystem.setBounds(0, 0, 226, 23);
		
		spDistributedSystem = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spDistributedSystem.setMaximum(999999999);
		spDistributedSystem.setDigits(2);
		spDistributedSystem.setBounds(0, 0, 86, 22);
		
		Label lblPerformanceObjective = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblPerformanceObjective.setText("Performance objective");
		lblPerformanceObjective.setBounds(0, 0, 155, 15);
		
		Label label_3 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_3.setText("Weight");
		label_3.setBounds(0, 0, 38, 15);
		
		cmbPerformanceObjective = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbPerformanceObjective.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbPerformanceObjective.setBounds(0, 0, 226, 23);
		
		spPerformanceObjective = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spPerformanceObjective.setMaximum(999999999);
		spPerformanceObjective.setDigits(2);
		spPerformanceObjective.setBounds(0, 0, 86, 22);
		
		Label lblEnduserEfficiency = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblEnduserEfficiency.setText("End-user efficiency");
		
		Label label_5 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_5.setText("Weight");
		
		cmbEndUserEffiency = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbEndUserEffiency.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spEndUserEffiency = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spEndUserEffiency.setMaximum(999999999);
		spEndUserEffiency.setDigits(2);
		
		Label lblComplexProcessing = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblComplexProcessing.setText("Complex processing");
		
		Label label_7 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_7.setText("Weight");
		
		cmbComplexProcessing = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbComplexProcessing.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spComplexProcessing = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spComplexProcessing.setMaximum(999999999);
		spComplexProcessing.setDigits(2);
		
		Label lblReusableCode = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblReusableCode.setText("Reusable code");
		
		Label label_9 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_9.setText("Weight");
		
		cmbReusableCode = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbReusableCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spReusableCode = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spReusableCode.setMaximum(999999999);
		spReusableCode.setDigits(2);
		
		Label lblEasyToInstall = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblEasyToInstall.setText("Easy to install");
		
		Label label_11 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_11.setText("Weight");
		
		cmbEasyToInstall = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbEasyToInstall.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spEasyToInstall = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spEasyToInstall.setMaximum(999999999);
		spEasyToInstall.setDigits(2);
		
		Label lblEasyToUse = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblEasyToUse.setText("Easy to use");
		
		Label label_13 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_13.setText("Weight");
		
		cmbEasyToUse = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbEasyToUse.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spEasyToUse = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spEasyToUse.setMaximum(999999999);
		spEasyToUse.setDigits(2);
		
		Label lblPortable = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblPortable.setText("Portable");
		
		Label label_15 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_15.setText("Weight");
		
		cmbPortable = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbPortable.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spPortable = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spPortable.setMaximum(999999999);
		spPortable.setDigits(2);
		
		Label lblEasyToChange = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblEasyToChange.setText("Easy to change");
		
		Label label_17 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_17.setText("Weight");
		
		cmbEasyToChange = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbEasyToChange.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spEasyToChange = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spEasyToChange.setMaximum(999999999);
		spEasyToChange.setDigits(2);
		
		Label lblConcurrentUse = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblConcurrentUse.setText("Concurrent use");
		
		Label label_19 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_19.setText("Weight");
		
		cmbConcurrentUse = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbConcurrentUse.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spConcurrentUse = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spConcurrentUse.setMaximum(999999999);
		spConcurrentUse.setDigits(2);
		
		Label lblSecurity = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblSecurity.setText("Security");
		
		Label label_21 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_21.setText("Weight");
		
		cmbSecurity = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbSecurity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spSecurity = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spSecurity.setMaximum(999999999);
		spSecurity.setDigits(2);
		
		Label lblAccessForThird = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblAccessForThird.setText("Access for third parties");
		
		Label label_23 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_23.setText("Weight");
		
		cmbAccessForThirdParties = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbAccessForThirdParties.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spAccessForThirdParties = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spAccessForThirdParties.setMaximum(999999999);
		spAccessForThirdParties.setDigits(2);
		
		Label lblTrainingNeeds = new Label(grpGrptechnologyfactor, SWT.NONE);
		lblTrainingNeeds.setText("Training needs");
		
		Label label_25 = new Label(grpGrptechnologyfactor, SWT.NONE);
		label_25.setText("Weight");
		
		cmbTrainingNeeds = new Combo(grpGrptechnologyfactor, SWT.READ_ONLY);
		cmbTrainingNeeds.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		spTrainingNeeds = new Spinner(grpGrptechnologyfactor, SWT.BORDER);
		spTrainingNeeds.setMaximum(999999999);
		spTrainingNeeds.setDigits(2);
		
		
		cmpUsecasePointSummary = new UsecasePointSummary(this, SWT.NONE);
		cmpUsecasePointSummary.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	} 

	public Combo getCmbCapability() {
		return cmbCapability;
	}

	public Combo getCmbTeamwork() {
		return cmbTeamwork;
	}

	public Combo getCmbStaffContinuity() {
		return cmbStaffContinuity;
	}

	public Combo getCmbSpecificationQuality() {
		return cmbSpecificationQuality;
	}

	public Combo getCmbProcessMaturity() {
		return cmbProcessMaturity;
	}

	public Combo getCmbDeadlinePressure() {
		return cmbDeadlinePressure;
	}

	public Combo getCmbRequirementsStability() {
		return cmbRequirementsStability;
	}

	public Combo getCmbNumberOfStakeholder() {
		return cmbNumberOfStakeholder;
	}

	public Combo getCmbSystemIntegrationChallenges() {
		return cmbSystemIntegrationChallenges;
	}

	public Spinner getSpCapability() {
		return spCapability;
	}

	public Spinner getSpTeamwork() {
		return spTeamwork;
	}

	public Spinner getSpStaffContinuity() {
		return spStaffContinuity;
	}

	public Spinner getSpSpecificationQuality() {
		return spSpecificationQuality;
	}

	public Spinner getSpProcessMaturity() {
		return spProcessMaturity;
	}

	public Spinner getSpDeadlinePressure() {
		return spDeadlinePressure;
	}

	public Spinner getSpRequirementsStability() {
		return spRequirementsStability;
	}

	public Spinner getSpNumberOfStakeholders() {
		return spNumberOfStakeholders;
	}
	
	public Spinner getSpSystemIntegrationChanges() {
		return spSystemIntegrationChanges;
	} 
 
	public Spinner getSpProductivityFactor() {
		return spProductivityFactor;
	}
	
	public UsecasePointSummary getCmpUsecasePointSummary() {
		return cmpUsecasePointSummary;
	}
	
	public PropertySelection getPropertycostPerUsecasePoint() {
		return propertycostPerUsecasePoint;
	}

	public Combo getCmbDistributedSystem() {
		return cmbDistributedSystem;
	}

	public Spinner getSpDistributedSystem() {
		return spDistributedSystem;
	}

	public Combo getCmbPerformanceObjective() {
		return cmbPerformanceObjective;
	}

	public Spinner getSpPerformanceObjective() {
		return spPerformanceObjective;
	}

	public Combo getCmbEndUserEffiency() {
		return cmbEndUserEffiency;
	}

	public Spinner getSpEndUserEffiency() {
		return spEndUserEffiency;
	}

	public Combo getCmbComplexProcessing() {
		return cmbComplexProcessing;
	}

	public Spinner getSpComplexProcessing() {
		return spComplexProcessing;
	}

	public Combo getCmbReusableCode() {
		return cmbReusableCode;
	}

	public Spinner getSpReusableCode() {
		return spReusableCode;
	}

	public Combo getCmbEasyToInstall() {
		return cmbEasyToInstall;
	}

	public Spinner getSpEasyToInstall() {
		return spEasyToInstall;
	}

	public Combo getCmbEasyToUse() {
		return cmbEasyToUse;
	}

	public Spinner getSpEasyToUse() {
		return spEasyToUse;
	}

	public Combo getCmbPortable() {
		return cmbPortable;
	}

	public Spinner getSpPortable() {
		return spPortable;
	}

	public Combo getCmbEasyToChange() {
		return cmbEasyToChange;
	}

	public Spinner getSpEasyToChange() {
		return spEasyToChange;
	}

	public Combo getCmbConcurrentUse() {
		return cmbConcurrentUse;
	}

	public Spinner getSpConcurrentUse() {
		return spConcurrentUse;
	}

	public Combo getCmbAccessForThirdParties() {
		return cmbAccessForThirdParties;
	}

	public Spinner getSpAccessForThirdParties() {
		return spAccessForThirdParties;
	}

	public Combo getCmbTrainingNeeds() {
		return cmbTrainingNeeds;
	}

	public Spinner getSpTrainingNeeds() {
		return spTrainingNeeds;
	}
	
	public Combo getCmbSecurity() {
		return cmbSecurity;
	}

	public Spinner getSpSecurity() {
		return spSecurity;
	}

}
