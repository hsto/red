package dk.dtu.imm.red.core.ui.internal.preferences;

import java.util.List;

import org.agilemore.agilegrid.DefaultContentProvider;

import dk.dtu.imm.red.core.ui.editors.BaseEditor;

public class ServersTableContentProvider extends DefaultContentProvider {

	private BaseEditor editor;
	
	private List<ServersTableEntry> servers;

	public ServersTableContentProvider() {
		super();
	}

	public ServersTableContentProvider(ServersPreferencePage serversPreferencePage) {
		// TODO Auto-generated constructor stub
	}

	public void setContent(List<ServersTableEntry> servers) {
		this.servers = servers;
	}

	@Override
	public void doSetContentAt(int row, int col, Object value) {
		ServersTableEntry serversEntry = servers.get(row);

		if (serversEntry == null) {
			super.doSetContentAt(row, col, value);
		}

		switch (col) {
			case ServersTableLayoutAdviser.NAME_COLUMN:
				serversEntry.setName((String) value);
			break;		
			case ServersTableLayoutAdviser.SERVICE_COLUMN:
				serversEntry.setService((String) value);
				break;
			case ServersTableLayoutAdviser.SERVER_COLUMN:
				serversEntry.setServer((String) value);
				break;
			case ServersTableLayoutAdviser.USERNAME_COLUMN:
				serversEntry.setUsername((String) value);
				break;
			case ServersTableLayoutAdviser.PASSWORD_COLUMN:
				serversEntry.setPassword((String) value);
				break;
			case ServersTableLayoutAdviser.EXTRA_PARAMS_COLUMN:
				serversEntry.setExtraParams((String) value);
				break;				
			default:
				doSetContentAt(row, col, value);
		}
		
	}

	@Override
	public Object doGetContentAt(int row, int col) {

		ServersTableEntry serversEntry = servers.get(row);

		if (serversEntry == null) {
			return super.doGetContentAt(row, col);
		}

		switch (col) {
			case ServersTableLayoutAdviser.NAME_COLUMN:
				return serversEntry.getName();
			case ServersTableLayoutAdviser.SERVICE_COLUMN:
				return serversEntry.getService();
			case ServersTableLayoutAdviser.SERVER_COLUMN:
				return serversEntry.getServer();
			case ServersTableLayoutAdviser.USERNAME_COLUMN:
				return serversEntry.getUsername();
			case ServersTableLayoutAdviser.PASSWORD_COLUMN:
				return serversEntry.getPassword();
			case ServersTableLayoutAdviser.EXTRA_PARAMS_COLUMN:
				return serversEntry.getExtraParams();				
			default:
				return doGetContentAt(row, col);
		}
	}
}
