package dk.dtu.imm.red.core.ui.internal.effortestimation.views.impl;  

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.editors.IBaseEditor;
import dk.dtu.imm.red.core.ui.internal.PartAdapter;
import dk.dtu.imm.red.core.ui.internal.effortestimation.views.EffortEstimationView;
import dk.dtu.imm.red.core.ui.internal.effortestimation.views.EffortEstimationViewPresenter;
import dk.dtu.imm.red.core.ui.views.BaseView;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
 

/**
 * View used for displaying effort estimation.
 * 
 * @author Johan Paaske Nielsen
 * 
 */
public final class EffortEstimationViewImpl extends BaseView implements EffortEstimationView  {  
	
	private EffortEstimationComposite composite; 
	private EffortEstimationViewModel viewModel = new EffortEstimationViewModel();
	private IEditorPart part;
	private IBaseEditor activeEditor;
 
	public EffortEstimationViewImpl() {
		presenter = new EffortEstimationViewPresenterImpl(this, viewModel); 
	}
	
	@Override
	public void createPartControl(Composite parent) { 
		
		 
		composite = new EffortEstimationComposite(parent, SWT.NONE, (EffortEstimationView) this); 
		//composite = SWTUtils.wrapInScrolledComposite(parent, composite); 
		super.createPartControl(parent);

		if (getSite().getPage() != null) { 
			getSite().getPage().addPartListener(new PartActivationAdapter(this));  
		}
	}   
	
	@Override
	public void updateValues() { 
	 
		String precisionFormat = "%.2f"; 
		UsecasePoint up = ((EffortEstimationViewPresenter)presenter).getFoldersProject().getEffort(); 
		 
		viewModel.setElement(up); 
		
		if(up!= null) {
			EffortEstimationViewPresenter ePresenter =  ((EffortEstimationViewPresenter)presenter); 
			
			Map<String, List<Element>> elementMap = null;
			
			if(activeEditor instanceof EffortEstimationEditor) {
				List<Element> elements = ((EffortEstimationEditor) activeEditor).getEffortEstimationContent();
				elementMap = ePresenter.getRequirementElementMap(elements);
			}
			else {
				ArrayList<Element> elements = new ArrayList<Element>();
				elements.add(activeEditor.getEditorElement());
				elementMap = ePresenter.getRequirementElementMap(elements);
			}
		
			composite.getLblTitle().setText(String.format("Effort Estimation for %s", activeEditor.getEditorElement().getName()));			
			
			composite.getLblManagementFactorValue().setText(String.format(precisionFormat, up.getManagementFactor().getSum()));
			composite.getLblTechnologyFactorValue().setText(String.format(precisionFormat, up.getTechnologyFactor().getSum()));
			composite.getLblProductivityFactorValue().setText(String.format(precisionFormat, up.getProductivityFactor().getSum()));  
			composite.getLblRequirementFactorValue().setText(String.format(precisionFormat, up.getRequirementFactor().getSum(elementMap)));
			composite.getLblEffortValue().setText(String.format("%.2f Man Hours", up.getSum(elementMap)));  
			composite.getLblUCPValue().setText(String.format(precisionFormat, up.getUsecasePoints()));
			
			double totalComputedCost = up.getTotalCost();
			double totalAssignedCost = up.getTotalCostManual();
			double totalBenefit = up.getTotalBenefitManual();
			
			composite.getLblCostValue().setText(String.format("%.2f %s", totalComputedCost, up.getCostPerUsecasePoint().getMonetaryUnit().name())); 
			composite.getLblManualCostValue().setText(String.format("%.2f %s", totalAssignedCost, up.getCostPerUsecasePoint().getMonetaryUnit().name()));
			composite.getLblEstimatedBenefitValue().setText(String.format("%.2f %s", totalBenefit, up.getCostPerUsecasePoint().getMonetaryUnit().name()));
			
			double[] values = { totalComputedCost, totalAssignedCost, totalBenefit};
			composite.updateValues(values); 
		} 
	} 
	 
	private final class PartActivationAdapter extends PartAdapter {

		private Element previousInput = null;
		
		private BaseView view=null; 
		
		public PartActivationAdapter(BaseView view){
			super();
			this.view = view;
		} 
		
		@Override
		public void partActivated(final IWorkbenchPartReference partRef) { 
			if(!getViewSite().getPage().isPartVisible(view)){
				//return if the comment view is not visible
				return;
			}

			part = getSite().getPage().getActiveEditor();
			if(part instanceof IBaseEditor) {
				activeEditor = (IBaseEditor) part;
				if (activeEditor!=null && activeEditor.getEditorElement() != null) {
					((EffortEstimationViewPresenter) presenter).inputChanged(activeEditor.getEditorElement()); 
					
					if(previousInput != activeEditor.getEditorElement()) {
						previousInput = activeEditor.getEditorElement();
						composite.setVisible(true);
						updateValues();
					}
					
				}
				else if (activeEditor==null) {
					((EffortEstimationViewPresenter) presenter).inputChanged(null);
					composite.setVisible(false);
				}
			}
		}

		@Override
		public void partClosed(final IWorkbenchPartReference partRef) { 
		 
			if(!getViewSite().getPage().isPartVisible(view)){
				//return if the comment view is not visible 
				return;
			} 

			IWorkbenchPart activatedPart = partRef.getPart(false);
			if (activatedPart != null && activatedPart instanceof IBaseEditor) {
				IBaseEditor activatedEditor = (IBaseEditor) activatedPart;

				if (((EffortEstimationViewPresenter) presenter).getInput()
						== activatedEditor.getEditorElement()) {

					((EffortEstimationViewPresenter) presenter).inputChanged(null);
					composite.setVisible(false);
					previousInput = null;
				}
			}
		}

	} 
}
