package dk.dtu.imm.red.core.ui.internal.element.document.editors.impl;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.element.CaptionedImage;
import dk.dtu.imm.red.core.element.CaptionedImageList;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.ui.internal.element.document.editors.DocumentEditor;
import dk.dtu.imm.red.core.ui.internal.element.document.editors.DocumentEditorPresenter;

public class DocumentEditorPresenterImpl extends BaseEditorPresenter implements 
	DocumentEditorPresenter{

	protected DocumentEditor documentEditor;
	
	private Element documentInternalRef;
	private String documentExternalRef;
	private String documentWebRef;
	private String documentAbstract;
	private String documentTextContent;
	private ArrayList<Image> imageList;
	private ArrayList<String> captionList;
		
	public DocumentEditorPresenterImpl(DocumentEditor documentEditor, Document element){
		super(element);
		this.documentEditor = documentEditor;
		this.element = element;
	}
	
	@Override
	public void save() {
		
		super.save();
		Document document = (Document) element;
		document.setAbstract(TextFactory.eINSTANCE.createText(documentAbstract));
		document.setInternalRef(documentInternalRef);
		document.setExternalFileRef(documentExternalRef);
		if(!documentWebRef.equals("http://")){
				document.setWebRef(documentWebRef);
		}
		document.setTextContent(TextFactory.eINSTANCE.createText(documentTextContent));
		int index=0;
		CaptionedImageList captionedImageList = ElementFactory.eINSTANCE.createCaptionedImageList();
		for(Image image: imageList){
			String caption = captionList.get(index);
			if(image!=null || (caption!=null && !caption.isEmpty())){
				CaptionedImage captionedImage = ElementFactory.eINSTANCE.createCaptionedImage();
				captionedImage.setImageData(SWTUtils.convertImageToByteArrayData(image));
				if(caption!=null && !caption.isEmpty()){
					captionedImage.setCaption(captionList.get(index));
				}
				else{
					captionedImage.setCaption("");
				}
				captionedImageList.getCaptionedImage().add(captionedImage);
				index++;
			}
		}
		document.setCaptionedImageList(captionedImageList);
		document.save();
	}
	
	public void setDocumentAbstract(String documentAbstract){
		this.documentAbstract=documentAbstract;
	}
	
	public void setDocumentTextContent(String documentTextContent){
		this.documentTextContent=documentTextContent;
	}
	
	public void setDocumentInternalRef(Element element){
		this.documentInternalRef=element;
	}
	
	public void setDocumentExternalRef(String reference){
		this.documentExternalRef=reference;
	}
	
	public void setDocumentWebRef(String reference){
		this.documentWebRef=reference;
	}
	
	public void setImageList(ArrayList<Image> imageList){
		this.imageList=imageList;
	}
	
	public void setCaptionList(ArrayList<String> captionList){
		this.captionList=captionList;
	}
	
	
	
}

