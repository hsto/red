package dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.DefaultCellRendererProvider;
import org.agilemore.agilegrid.ICellRenderer;
import org.agilemore.agilegrid.renderers.CheckboxCellRenderer;
import org.agilemore.agilegrid.renderers.TextCellRenderer;

import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.ui.ImageCellRenderer;
 

public class EAGCellRendererProvider extends DefaultCellRendererProvider  { 
	
	ICellRenderer imageRenderer = new ImageCellRenderer(agileGrid);
	ICellRenderer textCellRenderer = new TextCellRenderer(agileGrid); 
	ICellRenderer checkboxCellRenderer = new CheckboxCellRenderer(agileGrid, CheckboxCellRenderer.INDICATION_CLICKED);
 
	public EAGCellRendererProvider(AgileGrid agileGrid, EAGPresenter<?> presenter) {
		super(agileGrid); 
	} 
	
	@Override
	public ICellRenderer getCellRenderer(int row, int col) { 
		EAGPresenter presenter = ((ExtendedAgileGrid)this.agileGrid).getPresenter();
		ElementColumnDefinition selectedColumn = presenter.getColumnDefinitions()[col];
		
		if (selectedColumn.getColumnBehaviour() == ColumnBehaviour.IconDisplay	) {
			return imageRenderer;
		} else {
				
			if(selectedColumn.getColumnType().equals(boolean.class)) {
				return checkboxCellRenderer;
			} 
			else {
				return textCellRenderer;
			} 
        } 
	} 
}
