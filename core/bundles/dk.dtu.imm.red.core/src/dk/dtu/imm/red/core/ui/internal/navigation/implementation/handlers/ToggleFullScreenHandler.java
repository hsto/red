package dk.dtu.imm.red.core.ui.internal.navigation.implementation.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler2;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class ToggleFullScreenHandler extends AbstractHandler
		implements
			IHandler2 {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbench wb = PlatformUI.getWorkbench();
		IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
		boolean isFullScreen = win.getShell().getFullScreen();
		if(isFullScreen){
			win.getShell().setFullScreen(false);
		}
		else{
			win.getShell().setFullScreen(true);
		}
		return null;
	}

}
