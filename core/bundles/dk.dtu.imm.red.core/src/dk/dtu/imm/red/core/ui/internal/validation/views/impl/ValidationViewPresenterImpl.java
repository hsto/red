package dk.dtu.imm.red.core.ui.internal.validation.views.impl;

import java.beans.PropertyChangeEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Iterator;
import java.util.Set; 
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListenerImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentCategory;
import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.impl.ValidationObservable;
import dk.dtu.imm.red.core.element.impl.ValidationObserver;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.internal.comment.views.CommentView;
import dk.dtu.imm.red.core.ui.internal.comment.views.CommentViewPresenter;
import dk.dtu.imm.red.core.ui.internal.validation.views.ValidationView;
import dk.dtu.imm.red.core.ui.internal.validation.views.ValidationViewPresenter;
import dk.dtu.imm.red.core.ui.views.BaseViewPresenter;
import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;
import dk.dtu.imm.red.core.user.User;
import dk.dtu.imm.red.core.user.UserFactory;
import dk.dtu.imm.red.core.validation.ErrorMessage;
import dk.dtu.imm.red.core.validation.Severity;
import dk.dtu.imm.red.core.validation.ValidationFactory;
import dk.dtu.imm.red.core.validation.impl.ErrorMessageImpl;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory; 

public final class ValidationViewPresenterImpl extends BaseViewPresenter 
	implements ValidationViewPresenter {

	/**
	 * the view of this presenter.
	 */
	protected ValidationView view;
	protected Element shownElement; 
	private ValidationViewModel viewModel;  

	public ValidationViewPresenterImpl(final ValidationView view, final ValidationViewModel viewModel) {
		super(); 
		this.view = view;  
		this.viewModel = viewModel; 
		
//		ValidationObservable.getInstance().addObserver(new ValidationObserver() {
//			
//			@Override
//			public void modelChanged(Element e) {
//				
//				ErrorMessage em = ValidationFactory.eINSTANCE.createErrorMessage();
//				em.setMessage("Test"); 
//				viewModel.getValidationMessages().add(em);
//				view.updateValidationView(); 
//			}
//		});
	} 
	
	private void validateWorkspace(Group g) {
		
		for(Element e : g.getContents()) { 
			if(e instanceof Group) {
				validateWorkspace(((Group) e));
			}
		}
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateModel() {
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		validateWorkspace(workspace); 
	}  
}
