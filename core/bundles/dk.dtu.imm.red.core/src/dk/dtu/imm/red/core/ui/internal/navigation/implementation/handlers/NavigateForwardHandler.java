package dk.dtu.imm.red.core.ui.internal.navigation.implementation.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler2;

import dk.dtu.imm.red.core.ui.internal.NavigationManager;

public class NavigateForwardHandler extends AbstractHandler implements IHandler2 {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		NavigationManager.openForwardEditor();
		
		return null;
	}
	
	@Override
	public boolean isEnabled() {
		return NavigationManager.forwardHistorySize() > 0;
	}
	

}
