package dk.dtu.imm.red.core.ui.editors;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import dk.dtu.imm.red.core.element.Element;

public class BaseEditorInput implements IEditorInput {
	
	protected Element element;
	
	public BaseEditorInput(Element element) {
		this.element = element;
	}

	@Override
	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ImageDescriptor getImageDescriptor() {
		return ImageDescriptor.getMissingImageDescriptor();
	}

	@Override
	public String getName() {
		return element.getName();
	}

	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	@Override
	public String getToolTipText() {
		return "The editor for " + element.getName();
	}
	
	public Element getElement() {
		return element;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BaseEditorInput) {
			BaseEditorInput other = (BaseEditorInput) obj;
			if(other.element==null){
				return false;
			}
			return (other.element.equals(this.element));
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return element.hashCode();
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

}
