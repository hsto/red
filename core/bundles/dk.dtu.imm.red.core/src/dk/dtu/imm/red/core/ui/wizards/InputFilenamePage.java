package dk.dtu.imm.red.core.ui.wizards;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class InputFilenamePage extends WizardPage
		implements IWizardPage{
	protected Label displayNameLabel;
	protected Text displayNameText;
	
	protected boolean askDisplayName;
	protected boolean displayNameModified;
	
	public InputFilenamePage() {
		super("Display Information");
		setTitle("Choose filename");
		setDescription("Choose a name for the file in this project"); 
//				+ "If no selection is made, the " + elementType
//				+ " will not be placed in an existing project" 
//				+ " or folder, and you will need to specify a "
//				+ " save location.");
		displayNameModified = false;
	}
		

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout();
		composite.setLayout(layout);


		GridData displayNameLayoutData = new GridData();
		displayNameLayoutData.horizontalAlignment = SWT.FILL;
		displayNameLayoutData.heightHint = 15;
		displayNameLayoutData.widthHint = 286;
		
		displayNameLabel = new Label(composite, SWT.NONE);
		displayNameLabel.setText("Enter the filename (without file extension):");
		displayNameLabel.setLayoutData(displayNameLayoutData);
		setPageComplete(false);
		setControl(composite);
		
		GridData displayNameLayoutData2 = new GridData();
		displayNameLayoutData2.horizontalAlignment = SWT.FILL;
		displayNameLayoutData2.heightHint = 15;
		displayNameLayoutData2.widthHint = 266;
		
		displayNameText = new Text(composite, SWT.BORDER);
		displayNameText.setLayoutData(displayNameLayoutData2);
		displayNameText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(final ModifyEvent e) {
				displayNameModified = true;
				if (displayNameText.getText().trim().isEmpty()) {
					setPageComplete(false);
					
				} else {
					setPageComplete(true);
				}
			}
		});
		
	}
	
	@Override
	public IWizardPage getNextPage() {
		if (isPageComplete() && isCurrentPage() 
				&& getWizard() instanceof IBaseWizard) {
			
			((IBaseWizard) getWizard()).pageOpenedOrClosed(this);
		}
		return super.getNextPage();
	}
	
	

	public void setDisplayName(String text) {
		if (!displayNameModified && displayNameText != null) {
			displayNameText.setText(text);
			displayNameText.notifyListeners(SWT.Modify, null);
			displayNameModified = false;
		}
		
	}
	
	public String getDisplayName() {
		if (displayNameText != null) {
			return displayNameText.getText();
		} else {
			return "";
		}
	}	

}
