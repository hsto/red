package dk.dtu.imm.red.core.ui.internal.element.actions;

import org.eclipse.epf.richtext.IRichText;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.actions.RichTextAction;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizard;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardPresenter;
import dk.dtu.imm.red.core.ui.element.wizards.impl.CreateElementReferenceWizardImpl;
import dk.dtu.imm.red.core.ui.internal.Activator;

public class AddElementReferenceAction extends RichTextAction {
	
	protected Element selectedElement;
	
	public AddElementReferenceAction(IRichText richText) {
		super(richText, IAction.AS_PUSH_BUTTON);
		setToolTipText("Create an element reference");
		setImageDescriptor(Activator
				.getImageDescriptor("/icons/AddLink.gif"));
	}

	@Override
	public void execute(IRichText richText) {
		CreateElementReferenceWizard wizard = 
				new CreateElementReferenceWizardImpl();
		wizard.init(PlatformUI.getWorkbench(), 
				(IStructuredSelection) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection());

		WizardDialog dialog = 
				new WizardDialog(Display.getCurrent().getActiveShell(), 
						wizard);
					
		dialog.open();
		
		selectedElement = ((CreateElementReferenceWizardPresenter) wizard.getPresenter())
				.getselectedElement();
		
		if (selectedElement != null) {
			richText.executeCommand(RichTextCommand.ADD_LINK, selectedElement.getUniqueID());
		}
		
	}

}
