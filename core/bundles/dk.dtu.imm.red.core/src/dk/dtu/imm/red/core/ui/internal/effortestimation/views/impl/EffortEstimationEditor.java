package dk.dtu.imm.red.core.ui.internal.effortestimation.views.impl;

import java.util.List;
import java.util.Map;

import dk.dtu.imm.red.core.element.Element;

public interface EffortEstimationEditor { 
	List<Element> getEffortEstimationContent(); 
}
