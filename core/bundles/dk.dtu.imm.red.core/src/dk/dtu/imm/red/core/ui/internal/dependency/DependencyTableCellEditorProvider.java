package dk.dtu.imm.red.core.ui.internal.dependency;

import java.util.ArrayList;
import java.util.List;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.CellEditor;
import org.agilemore.agilegrid.DefaultCellEditorProvider;
import org.agilemore.agilegrid.editors.ComboBoxCellEditor;
import org.eclipse.swt.SWT;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.ui.AgileTableTextCellEditor;
import dk.dtu.imm.red.core.ui.editors.IBaseEditor.Dependency;

/**
 * The Cell Editor Provider for the associations table. The primary purpose of
 * this class is to tell which columns are editable.
 * 
 * @author Jakob Kragelund
 *
 */
public class DependencyTableCellEditorProvider extends DefaultCellEditorProvider {

	private static final int COMMENT_COLUMN = 3;
	private static final int ELEMENT_NAME_COLUMN = 2;
	private static final int DEPENDENCY_KIND_COLUMN = 1;
	private static final int ICON_COLUMN = 0;

	protected List<Dependency> relationsRelatedBy;
	protected String refferedElementType;

	public DependencyTableCellEditorProvider(AgileGrid agileGrid, String refferedElementType) {
		super(agileGrid);
		this.refferedElementType = refferedElementType;
	}

	@Override
	public CellEditor getCellEditor(int row, int col) {
		if (col == DEPENDENCY_KIND_COLUMN) {
			List<RelationshipKind> kindsList = RelationshipKind.getRelationshipKindByClassType(refferedElementType);
			RelationshipKind[] kinds = new RelationshipKind[kindsList.size()];
			kinds = kindsList.toArray(kinds);
			String[] values = new String[kinds.length];
			for (int i = 0; i < kinds.length; i++) {
				values[i] = kinds[i].getLiteral();
			}
			return new ComboBoxCellEditor(agileGrid, values, SWT.NONE);
		}
		return new AgileTableTextCellEditor(agileGrid);
	}

	@Override
	public boolean canEdit(final int row, final int col) {
		// if (row < relationsRelatedBy.size()) {
		// return false;
		// } else {
		// // Only the comment-column is editable.
		// return (col == COMMENT_COLUMN || col == DEPENDENCY_KIND_COLUMN);
		// }
		// Only the comment-column is editable.
		return (col == COMMENT_COLUMN || col == DEPENDENCY_KIND_COLUMN);
	}

	public void setRelatedByContent(List<Dependency> relationsRelatedBy) {
		this.relationsRelatedBy = relationsRelatedBy;
	}

	private RelationshipKind[] filterRelationshipKinds(RelationshipKind[] kinds) {
		List<RelationshipKind> filteredKinds = new ArrayList<>();

		RelationshipKind[] filteredKindsArr = new RelationshipKind[filteredKinds.size()];
		filteredKindsArr = filteredKinds.toArray(filteredKindsArr);

		return filteredKindsArr;
	}

}
