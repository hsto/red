package dk.dtu.imm.red.core.ui.internal.element.document.wizards.impl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.element.document.operations.CreateDocumentOperation;
import dk.dtu.imm.red.core.ui.internal.element.document.wizards.NewDocumentWizard;
import dk.dtu.imm.red.core.ui.internal.element.document.wizards.NewDocumentWizardPresenter;
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;

public class NewDocumentWizardPresenterImpl extends BaseWizardPresenter
	implements NewDocumentWizardPresenter{

	public NewDocumentWizardPresenterImpl(final NewDocumentWizard newDocumentWizard) {
		super();
	}
	
	@Override
	public void wizardFinished(String label, String name, String description, Group parent, String path) {

		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
				.getOperationSupport();

		CreateDocumentOperation operation = 
				new CreateDocumentOperation(label, name, description, parent, path);

		operation.addContext(operationSupport.getUndoContext());

		IAdaptable info = new IAdaptable() {
			@Override
			public Object getAdapter(final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		try {
			OperationHistoryFactory.getOperationHistory().execute(operation, null,
					info);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

}
