package dk.dtu.imm.red.core.ui.internal.file.wizards;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;

public interface CreateFileWizardPresenter extends IWizardPresenter {
	
	void wizardFinished(String name, Group parent, String path);

}
