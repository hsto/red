package dk.dtu.imm.red.core.ui.internal.widgets;


import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LifeCyclePhase;
import dk.dtu.imm.red.core.element.State;

public class ElementLifeCycleDialog extends Dialog {
	
	private Element element; 
	private Trigger trigger;
	private Text lifeCycleField;
	private Text stateField;
	private String changelogMessage;
	
	private Table table;

	public ElementLifeCycleDialog(Shell parentShell, Element element, Trigger trigger, Text lifeCycleField, Text stateField) {
		super(parentShell);
		this.element = element;
		this.trigger = trigger;
		this.lifeCycleField = lifeCycleField;
		this.stateField = stateField;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
		String elementType = element.getClass().getSimpleName().replace("Impl", "");
		String intro = "You are about to advance the status of \"" + elementType + 
				": " + element.getLabel() + " - " + element.getName() + "\"";
		String current =  "from: \"" + element.getLifeCyclePhase() + " - " + element.getState() + "\"";
		
		
		
		Label label = new Label(container, SWT.NONE);
		label.setText(intro + "\n" + current + "\n" + "to:");
		
		
		table = new Table(container, SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL);
		table.setHeaderVisible(true);
	    String[] titles = { "Life cycle", "State"};
	    TableColumn column0 = new TableColumn(table, SWT.NULL);
	    column0.setText(titles[0]);
	    TableColumn column1 = new TableColumn(table, SWT.NULL);
	    column1.setText(titles[1]);
	    
	    if (trigger == Trigger.ADVANCE_LIFE_CYCLE_PHASE) {
	    	fillNextLifeCyclePhaseTable();
	    } else if (trigger == Trigger.ADVANCE_STATE) {
	    	fillNextStateTable();
	    }
	    
	    table.getColumn(0).pack();
	    table.getColumn(1).pack();
		
	    Label label2 = new Label(container, SWT.NONE);
	    label2.setText("Which cannot be undone!");
		
		return container;
		
	}
	
	private void fillNextLifeCyclePhaseTable() {
		LifeCyclePhase currentPhase = LifeCyclePhase.get(lifeCycleField.getText());
		State currentState = State.get(stateField.getText());
		
		switch (currentPhase.getValue()) {
		case LifeCyclePhase.ELABORATION_VALUE:
			addRow(LifeCyclePhase.ASSESSMENT, State.UNDER_REVIEW);
			break;
		case LifeCyclePhase.ASSESSMENT_VALUE:
			addRow(LifeCyclePhase.PLANNING, State.PENDING);
			break;
		case LifeCyclePhase.PLANNING_VALUE:
			if (currentState != State.DUPLICATE && currentState != State.OBSOLETE && currentState != State.REJECTED) {
				addRow(LifeCyclePhase.CONSTRUCTION, State.UNDER_CONSTRUCTION);				
			}
			break;
		case LifeCyclePhase.CONSTRUCTION_VALUE:
			addRow(LifeCyclePhase.PLANNING, State.PENDING);
			break;
		default:			
			break;
		}
	}
	
	private void fillNextStateTable()  {
		LifeCyclePhase currentPhase = LifeCyclePhase.get(lifeCycleField.getText());
		State currentState = State.get(stateField.getText());
		
		if (currentPhase == LifeCyclePhase.ELABORATION) {
			
			switch (currentState.getValue()) {
			case State.INCOMPLETE_VALUE:
				addRow(LifeCyclePhase.ELABORATION, State.COMPLETED);
				break;
			case State.COMPLETED_VALUE:
				addRow(LifeCyclePhase.ASSESSMENT, State.UNDER_REVIEW);
				break;
			default:
				break;
			}
		}
		
		else if (currentPhase == LifeCyclePhase.ASSESSMENT) {
			
			switch (currentState.getValue()) {
			case State.UNDER_REVIEW_VALUE:
				addRow(LifeCyclePhase.ASSESSMENT, State.REVIEWED);
				break;
			case State.REVIEWED_VALUE:
				addRow(LifeCyclePhase.PLANNING, State.PENDING);
				break;
			default:
				break;
			}
		}
		
		else if (currentPhase == LifeCyclePhase.PLANNING) {
			
			switch (currentState.getValue()) {
			case State.PENDING_VALUE:
				addRow(LifeCyclePhase.PLANNING, State.SCHEDULED);
				addRow(LifeCyclePhase.PLANNING, State.DUPLICATE);
				addRow(LifeCyclePhase.PLANNING, State.OBSOLETE);
				addRow(LifeCyclePhase.PLANNING, State.REJECTED);
				break;
			case State.SCHEDULED_VALUE:
				addRow(LifeCyclePhase.CONSTRUCTION, State.UNDER_CONSTRUCTION);
				break;
			default:
				break;
			}			
		}
		
		else if (currentPhase == LifeCyclePhase.CONSTRUCTION) {
			
			switch (currentState.getValue()) {
			case State.UNDER_CONSTRUCTION_VALUE:
				addRow(LifeCyclePhase.CONSTRUCTION, State.UNDER_TEST);
				break;
			case State.UNDER_TEST_VALUE:
				addRow(LifeCyclePhase.CONSTRUCTION, State.UNDER_CONSTRUCTION);
				addRow(LifeCyclePhase.CONSTRUCTION, State.RESOLVED);
				break;
			default:
				break;
			}
		}
	}
	

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID, "Abort", false);		
		createButton(parent, IDialogConstants.OK_ID, "Proceed", true);
	}

	@Override
	protected void okPressed() {
		TableItem[] selection = table.getSelection();
		if (selection != null) {
			String newPhase = selection[0].getText(0);
			String newState = selection[0].getText(1);
			changelogMessage = "from \"" + lifeCycleField.getText() + " - " + stateField.getText() + "\" to \"" + newPhase + " - " + newState + "\"";  
			lifeCycleField.setText(newPhase);
			stateField.setText(newState);			
		}
		super.okPressed();
	}
	
	public String getChangelogMessage() {
		return changelogMessage;
	}
	
	public static enum Trigger {
		ADVANCE_LIFE_CYCLE_PHASE, ADVANCE_STATE;
	}
	
	private void addRow(LifeCyclePhase phase, State state) {
		TableItem item = new TableItem(table, SWT.NULL);
	    item.setText(0, phase.getLiteral());
	    item.setText(1, state.getLiteral());
	}

}
