/*
 * @author Johan Paaske Nielsen
 * 
 * Helper class for Searching attributes for an Editor
 */
package dk.dtu.imm.red.core.ui.editors;

 
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EStructuralFeature; 

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
 

public class SearchEditorHelper {
	
	public static SearchResultWrapper search(SearchParameter param, 
			Map<EStructuralFeature, String> attributesToSearch, 
			Element element) { 
		
		//Add Default Search Parameter
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__NAME, element.getName()); 
		
		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(element); 

		// Now, search through all attributes, recording all search hits along the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()-1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(), result.getEndIndex());
			}
		}

		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
		
	}

}
