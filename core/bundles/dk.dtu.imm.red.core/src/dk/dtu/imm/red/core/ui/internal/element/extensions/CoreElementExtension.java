package dk.dtu.imm.red.core.ui.internal.element.extensions;

import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.element.document.editors.DocumentEditor;
import dk.dtu.imm.red.core.ui.internal.element.document.editors.impl.DocumentEditorInputImpl;
import dk.dtu.imm.red.core.ui.internal.file.editors.FileEditor;
import dk.dtu.imm.red.core.ui.internal.file.editors.FileEditorInput;
import dk.dtu.imm.red.core.ui.internal.project.editors.ProjectEditor;
import dk.dtu.imm.red.core.ui.internal.project.editors.impl.ProjectEditorInputImpl;

public class CoreElementExtension implements IElementExtensionPoint{

	@Override
	public void openElement(Element element) {
		
		try {
			if (element instanceof Document) {
			DocumentEditorInputImpl editorInput = new DocumentEditorInputImpl(
					(Document) element);
			PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(editorInput, DocumentEditor.ID);
			} else if (element instanceof Project){
				ProjectEditorInputImpl editorInput = new ProjectEditorInputImpl(
						(Project) element);
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(editorInput, ProjectEditor.ID);
				
			}
			else if (element instanceof File){
				FileEditorInput editorInput = new FileEditorInput(
						(File) element);
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(editorInput, FileEditor.ID);}
		} catch (PartInitException e) {
			// TODO: handle exception
			e.printStackTrace(); LogUtil.logError(e);
		}
		
	}

	@Override
	public void deleteElement(Element element) {
		// Remove the element from it's parent, if not done already
		if (element.getParent() != null) {
			element.getParent().getContents().remove(element);
		}

		// Close all editors for this element
		IEditorReference[] editors = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.getEditorReferences();
		for (IEditorReference editor : editors) {
			// if the editor is opened for the given element
			if (editor.getPart(false) instanceof BaseEditor
					&& ((BaseEditor) editor.getPart(false)).getEditorElement() == element) {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getActivePage()
						.closeEditor(editor.getEditor(false), false);
			}
		}
		
	}

	@Override
	public void restoreElement(Element element) {
		// TODO Auto-generated method stub
		
	}

}
