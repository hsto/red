package dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ColumnSortComparator;

public class EAGTableSortComparator extends ColumnSortComparator {

	public EAGTableSortComparator(AgileGrid agileGrid, int columnIndex,
			int direction) {
		super(agileGrid, columnIndex, direction);
	}

	@SuppressWarnings("unchecked")
	@Override
	public int doCompare(Object o1, Object o2, int row1, int row2) {
		if(o1 == null || o2 == null) return 0;
		
		if(o1 instanceof Comparable<?> && o2 instanceof Comparable<?>) {
			return ((Comparable<Object>) o1).compareTo( (Comparable<Object>)o2);
		}
		else {
			return 0;
		}
	}
}
