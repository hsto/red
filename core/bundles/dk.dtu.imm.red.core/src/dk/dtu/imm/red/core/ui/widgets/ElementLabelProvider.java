package dk.dtu.imm.red.core.ui.widgets;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.edit.provider.ComposedImage;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.osgi.framework.Bundle;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.ui.internal.Activator;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;

public class ElementLabelProvider implements ILabelProvider {

	protected Bundle bundle;

	public ElementLabelProvider() {
		bundle = Platform.getBundle(Activator.PLUGIN_ID);
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public Image getImage(Object element) {
		 if (element instanceof Element) {
			 Image elementIcon = ((Element) element).getIcon();
			 Image lockIcon = null;
			 ComposedImage composedIcon = null;
			 Image returnIcon;
			 LockType lockStatus = ((Element) element).getLockStatus();

			 switch (lockStatus) {
				 case LOCK_CONTENTS:
					 lockIcon = Activator.getDefault().getImageRegistry()
					 	.get(Activator.LOCK_COMPLETE_ICON);
					 break;
				 case LOCK_COMMENTING_ONLY:
					 lockIcon = Activator.getDefault().getImageRegistry()
					 	.get(Activator.LOCK_PARTIAL_ICON);
					 break;
			 }

			 if (lockIcon != null) {
				 List<Object> images = new ArrayList<Object>(2);
				 if (elementIcon != null) {
					 images.add(elementIcon);
				 }
				 lockIcon = new Image(Display.getCurrent(), lockIcon.getImageData().scaledTo(12, 12));
				 images.add(lockIcon);
				 composedIcon = new ComposedImage(images);
				 returnIcon = ExtendedImageRegistry.INSTANCE.getImage(composedIcon);
			 } else {
				 returnIcon = elementIcon;
			 }
			 return returnIcon;
		 }
		return null;
	}

	@Override
	public String getText(final Object element) {

		String labelPreference = PreferenceUtil.getPreferenceString(PreferenceConstants.PROJECT_PREF_LABELS);
		if(labelPreference.equals("complex")){
			if (element instanceof Element) {
				Element el = (Element) element;
				return el.getComplexDisplayName();
			}
		}
		else{
			if (element instanceof Element) {
				Element el = (Element) element;
				return el.getPlainDisplayName();
			}
		}
		return "No name";
	}

}
