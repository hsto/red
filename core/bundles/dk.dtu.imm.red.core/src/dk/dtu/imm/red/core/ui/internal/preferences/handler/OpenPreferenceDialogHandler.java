package dk.dtu.imm.red.core.ui.internal.preferences.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.preference.IPreferenceNode;
import org.eclipse.jface.preference.IPreferencePage;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.preference.PreferenceNode;
import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.core.ui.internal.preferences.ManualResizePreferenceDialog;
import dk.dtu.imm.red.core.ui.internal.preferences.MiscPreferencePage;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.ui.internal.preferences.ProjectPreferencePage;
import dk.dtu.imm.red.core.ui.internal.preferences.ServersPreferencePage;
import dk.dtu.imm.red.core.ui.internal.preferences.UserPreferencePage;
import dk.dtu.imm.red.core.ui.internal.preferences.StitchPreferencePage;

public class OpenPreferenceDialogHandler extends AbstractHandler implements IHandler {

	private PreferenceStore store;


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IPreferencePage userPreferencePage = new UserPreferencePage();
		IPreferencePage projectPreferencePage = new ProjectPreferencePage();
		IPreferencePage serversPreferencePage = new ServersPreferencePage();
		IPreferencePage stitchPreferencePage = new StitchPreferencePage();
		IPreferencePage miscPreferencePage = new MiscPreferencePage();

		PreferenceManager mgr = new PreferenceManager();

		IPreferenceNode pref1 = new PreferenceNode("userPreferencePage", userPreferencePage);
		IPreferenceNode pref2 = new PreferenceNode("projectPreferencePage", projectPreferencePage);
		IPreferenceNode pref3 = new PreferenceNode("serversPreferencePage", serversPreferencePage);
		IPreferenceNode pref4 = new PreferenceNode("stitchPreferencePage", stitchPreferencePage);
		IPreferenceNode pref5 = new PreferenceNode("miscPreferencePage", miscPreferencePage);


		mgr.addToRoot(pref1);
		mgr.addToRoot(pref2);
		mgr.addToRoot(pref3);
		mgr.addToRoot(pref4);
		mgr.addToRoot(pref5);

		ManualResizePreferenceDialog dialog = new ManualResizePreferenceDialog(Display.getCurrent()
				.getActiveShell(), mgr);
		dialog.setMinimumPageSize(400, 400);
		dialog.setSelectedNode("userPreferencePage");

		store = PreferenceUtil.getPreferenceStore();
	    dialog.setPreferenceStore(store);

		dialog.create();
		dialog.setMessage(userPreferencePage.getTitle());
		dialog.open();

		return null;
	}




}
