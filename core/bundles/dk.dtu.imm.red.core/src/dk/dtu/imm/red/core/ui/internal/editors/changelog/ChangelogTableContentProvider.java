package dk.dtu.imm.red.core.ui.internal.editors.changelog;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.agilemore.agilegrid.DefaultContentProvider;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.text.Text;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.user.UserFactory;

public class ChangelogTableContentProvider extends DefaultContentProvider {

	private static final int AUTHOR_COLUMN = 3;
	private static final int COMMENT_COLUMN = 2;
	private static final int VERSION_COLUMN = 1;
	private static final int DATE_COLUMN = 0;

	private CommentList commentList;

	private SimpleDateFormat sdf;
	
	private BaseEditor editor;


	public ChangelogTableContentProvider(BaseEditor baseEditor) {
		super();
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		this.editor = baseEditor;
	}

	public void setContent(CommentList commentList) {
		this.commentList = commentList;
	}

	@Override
	public void doSetContentAt(int row, int col, Object value) {
		Comment comment = commentList.getComments().get(row);

		if (comment == null) {
			super.doSetContentAt(row, col, value);
		}

		switch (col) {
			case DATE_COLUMN:
				try {
					comment.setTimeCreated(sdf.parse((String) value));
				} catch (ParseException e) {
				}
				break;
			case AUTHOR_COLUMN:
				if (comment.getAuthor() == null) {
					comment.setAuthor(UserFactory.eINSTANCE.createUser());
				}
				comment.getAuthor().setName((String) value);
				break;
			case COMMENT_COLUMN:
				Text text = TextFactory.eINSTANCE.createText((String) value);
				comment.setText(text);
				break;
			case VERSION_COLUMN:
				comment.setVersion((String) value);
				break;
			default:
				doSetContentAt(row, col, value);
		}
		
		editor.markAsDirty();
	}

	@Override
	public Object doGetContentAt(int row, int col) {

		Comment comment = commentList.getComments().get(row);

		if (comment == null) {
			return super.doGetContentAt(row, col);
		}

		switch (col) {
			case DATE_COLUMN:
				return sdf.format(comment.getTimeCreated());
			case AUTHOR_COLUMN:
				if (comment.getAuthor() != null) {
					return comment.getAuthor().getName();
				} else {
					return "";
				}
			case COMMENT_COLUMN:
				if (comment.getText() == null) {
					return "";
				} else {
					return comment.getText().toString();
				}
			case VERSION_COLUMN:
				return comment.getVersion();
			default:
				return doGetContentAt(row, col);
		}
	}
}
