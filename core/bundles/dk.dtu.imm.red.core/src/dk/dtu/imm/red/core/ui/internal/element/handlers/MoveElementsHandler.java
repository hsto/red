package dk.dtu.imm.red.core.ui.internal.element.handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.ui.internal.element.operations.MoveElementsOperation;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class MoveElementsHandler extends AbstractHandler implements IHandler {
	
	public static final String ID = "dk.dtu.imm.red.core.moveelements";
	public static final String TARGET_PARAM_ID = "dk.dtu.imm.red.core.moveelements.targetelement";
	public static final String LOCATION_PARAM_ID = "dk.dtu.imm.red.core.moveelements.location";

	@Override
	public Object execute(final ExecutionEvent event) 
			throws ExecutionException {

		// Get the target and location
		String targetID = event
				.getParameter(TARGET_PARAM_ID);
		// We use ON as default location
		int location = ViewerDropAdapter.LOCATION_ON;
		int index = 0;
		try {
			location = Integer.parseInt(event.getParameter(LOCATION_PARAM_ID));
		} catch (NumberFormatException e) {
		}

		// find the target element in the workspace
		Element target = WorkspaceFactory.eINSTANCE
				.getWorkspaceInstance().findDescendantByUniqueID(targetID);
		
		// if the drop location is before or after the target
		if (location != ViewerDropAdapter.LOCATION_ON) {
			// then get the index of the target in the parent's contents list
			index = target.getParent().getContents().indexOf(target);
			
			// if the target is a group, then we do not want to put the element
			// into the group. it should go in the parent
			// (if it is not a group, it is handled later)
			if (target instanceof Group) {
				target = target.getParent();
			}
			
			// if the drop location is after the target, increment the index
			if (location == ViewerDropAdapter.LOCATION_AFTER) {
				index++;
			}
		} 
		
		// If the target is not a group, set target to parent
		if (false == target instanceof Group && target != null) {
			if (location == ViewerDropAdapter.LOCATION_ON) {
				throw new ExecutionException("Invalid drop target");
			}
			target = ((Element) target).getParent();
		}
		if(!(target instanceof Group)|| !(target instanceof Element))
			return null;

		// get all moved elements
		List<Element> movedElements = new ArrayList<Element>();

		if (event.getTrigger() instanceof Event
				&& ((Event) event.getTrigger()).data instanceof List<?>) {
			movedElements.addAll((Collection<? extends Element>) ((Event) event
					.getTrigger()).data);
			 try
			 {
			  for (int i = 0; i < movedElements.size(); i++) 
				{TreeIterator<EObject> iterator = movedElements.get(i).eAllContents();

				// Now inform all listening plugins that elements have been opened
				while (iterator.hasNext()) {
					EObject object = iterator.next();
					if (!(object instanceof Element)) {
						continue;
					}
					final Element element = (Element) object;

					element.setUri(target.getUri());
					
				}
				 movedElements.get(i).setUri(target.getUri());
			}}
			catch (Exception e) {}
		
		}

		// and call the undoable operation
		MoveElementsOperation operation = new MoveElementsOperation((Group) target,
				movedElements, index);
		operation.addContext(PlatformUI.getWorkbench().getOperationSupport()
				.getUndoContext());

		IStatus operationStatus = 
				OperationHistoryFactory.getOperationHistory()
					.execute(operation, null, null);
		 for(File file:WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedFiles()){
	    	   file.save();
	       }
		if (operationStatus != Status.OK_STATUS) {
			return Boolean.FALSE;
		}

		return null;
	}

}
