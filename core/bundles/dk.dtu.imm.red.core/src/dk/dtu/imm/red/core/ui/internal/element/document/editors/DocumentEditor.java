package dk.dtu.imm.red.core.ui.internal.element.document.editors;

import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface DocumentEditor extends IEditorPart, IBaseEditor{

	public static final String ID = "dk.dtu.imm.red.core.document.documenteditor";
}
