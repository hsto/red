package dk.dtu.imm.red.core.ui.internal.navigation.implementation;

import java.util.List;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchActionConstants;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.navigation.FileExplorer;
import dk.dtu.imm.red.core.ui.views.BaseView;
import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;

public class FileExplorerImpl extends BaseView implements FileExplorer {
	
	protected ElementTreeViewWidget treeView;
	
	@Override
	public void createPartControl(final Composite parent) {
		
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(parent.getLayout());

		treeView = new ElementTreeViewWidget(false, container, SWT.MULTI
				| SWT.BORDER, true,true);

		treeView.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(final DoubleClickEvent event) {
				ITreeSelection selection = (ITreeSelection) treeView
						.getSelection();
				List<Element> selectedElements = selection.toList();

				((FileExplorerPresenter) presenter)
					.elementDoubleClicked(selectedElements);
			}
		});

		// Set up drag and drop listeners
		int operations = DND.DROP_MOVE | DND.DROP_COPY;
		Transfer[] transferTypes = 
				new Transfer[] { TextTransfer.getInstance() };
		treeView.addDragSupportListener(operations, transferTypes,
				new ElementDragListener(treeView.getViewer()));
		treeView.addDropSupportListener(operations, transferTypes,
				new ElementDropListener(treeView.getViewer()));

		// Set up context menu
		MenuManager contextMenuManager = new MenuManager();
		contextMenuManager.add(new Separator(
				IWorkbenchActionConstants.MB_ADDITIONS));
		treeView.getTree().setMenu(
				contextMenuManager.createContextMenu(treeView.getTree()));

		getSite().registerContextMenu(contextMenuManager, treeView);
		getSite().setSelectionProvider(treeView);
		
		presenter = new FileExplorerPresenter();
		
		super.createPartControl(parent);
	}

	public void refreshViewParts(){
		treeView.getViewer().refresh();		
	}
	
	public ElementTreeViewWidget getTreeView() {
		return treeView;
	}
}
