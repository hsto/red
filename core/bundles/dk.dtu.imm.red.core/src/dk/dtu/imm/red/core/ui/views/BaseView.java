package dk.dtu.imm.red.core.ui.views;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.operations.RedoActionHandler;
import org.eclipse.ui.operations.UndoActionHandler;
import org.eclipse.ui.part.ViewPart;

public abstract class BaseView extends ViewPart implements IViewPart {

	protected BaseViewPresenter presenter;

	@Override
	public void createPartControl(Composite parent) {
		
		// Set up undo/redo handlers
    	UndoActionHandler undoAction = new UndoActionHandler(getViewSite(), 
    			presenter.getUndoContext());
    	RedoActionHandler redoAction = new RedoActionHandler(getViewSite(), 
    			presenter.getUndoContext());
    	
    	IActionBars actionBars = getViewSite().getActionBars();
    	actionBars.setGlobalActionHandler(
    			ActionFactory.UNDO.getId(), undoAction);
    	actionBars.setGlobalActionHandler(
    			ActionFactory.REDO.getId(), redoAction);

	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
	
	public BaseViewPresenter getPresenter() {
		return presenter;
	}
	
}
