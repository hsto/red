package dk.dtu.imm.red.core.ui.internal.traceability.util;

import org.eclipse.emf.common.util.Enumerator;

public enum SpecificationElementType implements Enumerator {
	ACTOR(0, "Actor", "Actor"),
	ASSUMPTION(1, "Assumption","Assumption"),
	GLOSSARYENTRY(2, "GlossaryEntry", "GlossaryEntry"),
	GOAL(3, "Goal", "Goal"),
	MODELELEMENT(4, "ModelElement", "ModelElement"),
	PERSONA(5, "Persona", "Persona"),
	REQUIREMENT(6, "Requirement", "Requirement"),
	STAKEHOLDER(7, "Stakeholder", "Stakeholder"),
	SYSTEM(8, "System", "System"),
	TESTCASE(9, "TestCase", "TestCase"),
	UCSCENARIO(10, "UCScenario", "UCScenario"),
	USECASE(11, "UseCase", "UseCase"),
	VISION(12, "Vision", "Vision"),
	SCENARIO(13, "Scenario", "Scenario"),
	DIAGRAM(14, "Diagram", "Diagram");
	
	private int value;
	private String name;
	private String literal;
	
	private SpecificationElementType(int value, String name, String literal){
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getValue() {
		return value;
	}

	@Override
	public String getLiteral() {
		return literal;
	}
}
