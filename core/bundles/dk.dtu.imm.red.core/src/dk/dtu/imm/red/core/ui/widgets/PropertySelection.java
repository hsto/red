package dk.dtu.imm.red.core.ui.widgets;

import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo; 

import dk.dtu.imm.red.core.properties.DurationUnit;
import dk.dtu.imm.red.core.properties.Kind;
 
import dk.dtu.imm.red.core.properties.MeasurementKind;
import dk.dtu.imm.red.core.properties.MonetaryUnit;
import dk.dtu.imm.red.core.properties.Property;
import dk.dtu.imm.red.core.properties.ValueKind;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class PropertySelection extends Composite {

	private PropertySelectionPresenter presenter;


	private Property property; 
	public Label getLblUnitDescription() {
		return lblUnitDescription;
	}
	private Spinner spinner;
	private Combo cmbUnit;
	private Listener modifyListener;
	private Object[] values; 
	private Combo cmbKind;
	private Label lblUnitDescription;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public PropertySelection(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(5, false));
		
		
		spinner = new Spinner(this, SWT.BORDER);
		spinner.setIncrement(1);
		spinner.setMaximum(99999999);
		spinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int selection = spinner.getSelection();
		        int digits = spinner.getDigits();
				boolean hasChanged = presenter.updateValue((selection / Math.pow(10, digits)));
				if(hasChanged) modifyListener.handleEvent(null);
			}
		});
		GridData gd_spinner = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_spinner.widthHint = 30;
		spinner.setLayoutData(gd_spinner);
		
		
		cmbUnit = new Combo(this, SWT.READ_ONLY);
		cmbUnit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String selection = cmbUnit.getItem(cmbUnit.getSelectionIndex()).toUpperCase();
				
				boolean hasChanged;
				 
				if(property.getKind() == Kind.MEASUREMENT)		{
					hasChanged = presenter.updateDurationUnit( DurationUnit.valueOf(selection) );
				} else {
					hasChanged = presenter.updateMonetaryUnit( MonetaryUnit.valueOf(selection) );
				}   		 
			
			    if(hasChanged) modifyListener.handleEvent(null);
			}
		});
		GridData gd_cmbUnit = new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1);
		gd_cmbUnit.heightHint = 50;
		gd_cmbUnit.minimumWidth = 50;
		gd_cmbUnit.widthHint = 20;
		cmbUnit.setLayoutData(gd_cmbUnit);
		
		cmbKind = new Combo(this, SWT.READ_ONLY);
		GridData gd_cmbKind = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		gd_cmbKind.heightHint = 50;
		gd_cmbKind.minimumWidth = 50;
		cmbKind.setLayoutData(gd_cmbKind);
		cmbKind.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String selection = cmbKind.getItem(cmbKind.getSelectionIndex()).toUpperCase();

				boolean hasChanged = false;
				
				if(property.getKind() == Kind.MEASUREMENT) {
					hasChanged = presenter.updateMeasurement(MeasurementKind.valueOf(selection.toUpperCase()));
				} else if(property.getKind() == Kind.VALUE) {
					hasChanged = presenter.updateValue(ValueKind.valueOf(selection.toUpperCase()));
				}
				
				if(hasChanged) modifyListener.handleEvent(null);
			}
		});
		new Label(this, SWT.NONE);
		
		lblUnitDescription = new Label(this, SWT.NONE);

	}
	
	public void fillData(Property property) {
		if(property == null) throw new IllegalArgumentException("Property object is not set");
		this.property = property; 
		presenter = new PropertySelectionPresenter(property); 
		spinner.setSelection((int)property.getValue());
		
		//Fill enum values into combo
		fillUnitCombo(); 
	}
	
	private void fillUnitCombo() { 
		
		//Clear combo
		cmbUnit.setItems(new String[] {});
		
		//Fill UNITS
		values = property.getKind() == Kind.MEASUREMENT ? DurationUnit.values() :   MonetaryUnit.values();
	    
		for(Object unit : values) {
			String name = ((Enumerator)unit).getName();
			cmbUnit.add(name); 
		} 
		
		String name = property.getKind() == Kind.MEASUREMENT ? property.getDurationUnit().getName() :  property.getMonetaryUnit().getName() ;
		cmbUnit.select(cmbUnit.indexOf(name)); 
		
		//FILL KIND
		values = property.getKind() == Kind.MEASUREMENT ? MeasurementKind.values() :  ValueKind.values();
	    //Load names into combo
		for(Object unit : values) {
			String name1 = ((Enumerator)unit).getName();
			cmbKind.add(name1); 
		} 
		
		name = property.getKind() == Kind.MEASUREMENT ? property.getMeasurementKind().getName() :  property.getValueKind().getName() ;
		cmbKind.select(cmbKind.indexOf(name)); 
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public PropertySelectionPresenter getPresenter() {
		return presenter;
	}

	public PropertySelection setPresenter(PropertySelectionPresenter presenter) {
		this.presenter = presenter;
		return this;
	}

	public Property getProperty() {
		return property;
	}

	public PropertySelection setProperty(Property property) {
		this.property = property;
		return this;
	} 
	
	public Spinner getSpinner() {
		return spinner;
	} 

	public Combo getCombo() {
		return cmbUnit;
	}  
	
	public Listener getModifyListener() {
		return modifyListener;
	}

	public PropertySelection setModifyListener(Listener modifyListener) {
		this.modifyListener = modifyListener;
		return this;
	}
	
	public Combo getCmbUnit() {
		return cmbUnit;
	}

	public Object[] getValues() {
		return values;
	}

	public Combo getCmbKind() {
		return cmbKind;
	}
}
