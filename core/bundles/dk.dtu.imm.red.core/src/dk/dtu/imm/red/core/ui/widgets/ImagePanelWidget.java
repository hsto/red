package dk.dtu.imm.red.core.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.core.ui.internal.widgets.ImagePanelWidgetPresenter;

/**
 * 
 * This class was modified and the author does not take credit for the original content. The class had to be updated 
 * with a getOriginal method that simply returns the image in its unmodified state. This is the state where no scaling had been applied. 
 * 
 * 
 * @author Johan Flod s102456
 *
 */

public class ImagePanelWidget extends Composite{

	protected Button helpButton;
	protected Canvas imageCanvas;
	protected Text imageTextBox;
	protected Button changeImageButton; 
	protected Button defaultImageButton;
	protected ImagePanelWidgetPresenter presenter;
	protected Composite imageContainer;
	
	public ImagePanelWidget(Composite parent, int style, boolean includeCaption, 
			String containerText, Image image, String caption) {
		super(parent, style);
		this.setLayout(new FillLayout());
	
		presenter = new ImagePanelWidgetPresenter(this, image);
		
		Group container = new Group(this, SWT.NONE);
		container.setText(containerText);
		GridLayout imageLayout = new GridLayout();
		imageLayout.numColumns = 2;
		container.setLayout(imageLayout);

		GridData captionGridData = new GridData();
		captionGridData.horizontalSpan = 2;
		captionGridData.grabExcessHorizontalSpace = true;
		captionGridData.horizontalAlignment = SWT.FILL;
		captionGridData.heightHint = 45;
		
		imageContainer = new Composite(container, SWT.NONE);
		imageContainer.setLayout(new GridLayout());
		imageCanvas = new Canvas(imageContainer, SWT.BORDER);
		GridData imageGridData = new GridData();
		imageGridData.horizontalSpan = 2;
		imageGridData.grabExcessHorizontalSpace = true;
		imageGridData.horizontalAlignment = SWT.FILL;
		imageGridData.grabExcessVerticalSpace = true;
		imageGridData.verticalAlignment = SWT.FILL;
		imageGridData.heightHint = 100;
		imageGridData.widthHint = 100;
		imageContainer.setLayoutData(imageGridData);
		imageCanvas.setLayoutData(imageGridData);
		imageCanvas.addPaintListener(new PaintListener() {
		
			@Override
			public void paintControl(PaintEvent e) {
				Image image = presenter.getImage();
				int xCoordinat = presenter.centerImageHorizontal();
				int yCoordinat = presenter.centerImageVertical();
				if (image != null) {
					e.gc.drawImage(image, xCoordinat, yCoordinat);
				} else {
					e.gc.drawRectangle(5, 5, 
						imageCanvas.getBounds().width - 14, 
						imageCanvas.getBounds().height - 14);
				}
			}
		});
		if (includeCaption) {			
			imageTextBox = new Text(container, 
					SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
			imageTextBox.setLayoutData(captionGridData);	
			imageTextBox.setText(caption);
			imageTextBox.setToolTipText("Caption for " + containerText);
			imageTextBox.addListener(SWT.Modify, new Listener() {
				@Override
				public void handleEvent(Event event) {
					notifyListeners(SWT.Modify, event);
				}
			});
		}

		changeImageButton = new Button(container, SWT.NONE);
		changeImageButton.setText("Change");
		changeImageButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				presenter.loadNewImage();
				notifyListeners(SWT.Modify, event);
			}
		});

		defaultImageButton = new Button(container, SWT.NONE);
		defaultImageButton.setText("Clear");

		defaultImageButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				presenter.switchToDefaultImage();
				notifyListeners(SWT.Modify, event);
			}
		});
		
		refreshImage();
		
	}
	
	public void refreshImage() {
		imageCanvas.layout(true);
		imageCanvas.redraw();

	}
	public Image getOriginalImage()
	{
		return presenter.getOriginallImage();
	}
	
	public Image getImage(){
		
		return presenter.getImage();
	}
	
	public void setImage(Image newImage) {
		presenter.setImage(newImage);
	}
	
	public String getCaption(){
		
		return imageTextBox.getText();
	}
	
	public void setCaption(String newCaption) {
		imageTextBox.setText(newCaption == null ? "" : newCaption);
	}

	public Control getImageCanvas() {
		return imageCanvas;
	}
	
	public Composite getImageContainer() {
		return imageContainer;
	}
	
	public Button getChangeImageButton() {
		return changeImageButton;
	}

	public Button getDefaultImageButton() {
		return defaultImageButton;
	}
}
