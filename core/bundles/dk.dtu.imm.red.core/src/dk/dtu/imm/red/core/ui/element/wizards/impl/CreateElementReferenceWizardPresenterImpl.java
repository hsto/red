package dk.dtu.imm.red.core.ui.element.wizards.impl;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardPresenter;
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;

public class CreateElementReferenceWizardPresenterImpl
		extends	BaseWizardPresenter  
		implements CreateElementReferenceWizardPresenter {

	protected Element selectedElement;
	@Override
	public void wizardFinished() {
		// TODO Auto-generated method stub

	}

	@Override
	public Element getselectedElement() {
		return selectedElement;
	}

	@Override
	public void setSelectedLabel(Element selectedElement) {
		this.selectedElement = selectedElement;
		
	}

}
