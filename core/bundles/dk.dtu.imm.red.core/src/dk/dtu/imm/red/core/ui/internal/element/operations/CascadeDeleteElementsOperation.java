package dk.dtu.imm.red.core.ui.internal.element.operations;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.ExtensionHelper;
import dk.dtu.imm.red.core.ui.internal.Activator;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.widgets.CascadeDeleteElementsMessageDialog;

/**
 * Undoable operation for deleting elements.
 *
 * @author Luai Issue #119
 * 
 */
public class CascadeDeleteElementsOperation extends AbstractOperation {

	protected CascadeDeleteElementsMessageDialog singleElementConfirmDeleteDialog;
	protected List<Element> selectedElements;
	protected List<Group> selectedParents;
	public List<Command> visualModelingcommands;
	private boolean triggeredFromVisualModeling;
	
	public CascadeDeleteElementsOperation(List<Element> selectedElements, boolean triggeredFromVisualModeling) {
		super("Cascaded Delete");

//		String singleElementConfirmQuestion = "Cascaded Delete removes all occurrences in the diagram! Are you sure you want to delete: " + selectedElement.getName() +"";
//		
//		singleElementConfirmDeleteDialog = new MessageDialog(Display
//				.getCurrent().getActiveShell(), "Confirm Delete", null,
//				singleElementConfirmQuestion, MessageDialog.QUESTION,
//				new String[] { "Yes", "No" }, 1);
		
		
		String singleElementConfirmQuestion = "Deleting will cascade to all linked elements";
		
		singleElementConfirmDeleteDialog = new CascadeDeleteElementsMessageDialog(Display
				.getCurrent().getActiveShell(), "Confirm Delete", null,
				singleElementConfirmQuestion, MessageDialog.QUESTION,
				new String[] { "Ok", "Cancel"}, 0);

		this.selectedElements = selectedElements;
		this.selectedParents = new ArrayList<Group>();
		for (Element element : selectedElements) {
			 selectedParents.add(element.getParent());
		}
		this.visualModelingcommands = new ArrayList<Command>();
		this.triggeredFromVisualModeling = triggeredFromVisualModeling;
	}

	@Override
	public IStatus execute(final IProgressMonitor monitor, final IAdaptable info) throws ExecutionException {

		int dialogResponse = -1;
		dialogResponse = singleElementConfirmDeleteDialog.open();
		
		// 0 == OK
		if (dialogResponse != 0) {
			return Status.CANCEL_STATUS;
		}
		
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(final IProgressMonitor monitor, final IAdaptable info) throws ExecutionException {

		
		if (singleElementConfirmDeleteDialog.isInlikedSelected() && triggeredFromVisualModeling) { // Unlink by aborting the deleting
			return Status.OK_STATUS;
		}
		
		if (!singleElementConfirmDeleteDialog.isInlikedSelected()) { // Unlink should not cascade
			
			visualModelingcommands = ExtensionHelper.runCommand("dk.dtu.imm.red.visualmodeling.ui.cascadeDiagramsOnDelete", selectedElements);
			for (Command command : visualModelingcommands) {
				command.execute();
			}			
		}
		
		for (final Element selectedElement : selectedElements) {
		
			selectedElement.delete();
	
			IConfigurationElement[] configurationElements = Platform
					.getExtensionRegistry().getConfigurationElementsFor(
							"dk.dtu.imm.red.core.element");
	
			try {
				for (IConfigurationElement configurationElement : configurationElements) {
	
					final Object extension = configurationElement
							.createExecutableExtension("class");
	
					if (extension instanceof IElementExtensionPoint) {
						ISafeRunnable runnable = new ISafeRunnable() {
	
							@Override
							public void handleException(final Throwable exception) {
								exception.printStackTrace();
							}
	
							@Override
							public void run() throws Exception {
								((IElementExtensionPoint) extension)
										.deleteElement(selectedElement);
							}
						};
						runnable.run();
					}
				}
			} catch (CoreException e) {
				e.printStackTrace();
				LogUtil.logError(e);
				return new Status(Status.ERROR, Activator.PLUGIN_ID,
						"Exception while deleting: " + e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
				LogUtil.logError(e);
				return new Status(Status.ERROR, Activator.PLUGIN_ID,
						"Exception while deleting: " + e.getMessage());
			}
		}
		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {

		if (singleElementConfirmDeleteDialog.isInlikedSelected() && triggeredFromVisualModeling) { // Unlink by aborting the deleting
			return Status.OK_STATUS;
		}
		
		for (int i = 0; i < selectedElements.size(); i++) {
		
			final Element element = selectedElements.get(i);
			final Group parent = selectedParents.get(i);
			if (parent != null) {
				parent.getContents().add(element);
			}
			
			if (i == 0) { // Should only run one time
				for (Command command : visualModelingcommands) {
					command.undo();
				}				
			}
	
			IConfigurationElement[] configurationElements = Platform
					.getExtensionRegistry().getConfigurationElementsFor(
							"dk.dtu.imm.red.core.element");
	
			try {
				for (IConfigurationElement configurationElement : configurationElements) {
	
					final Object extension = configurationElement
							.createExecutableExtension("class");
	
					if (extension instanceof IElementExtensionPoint) {
						ISafeRunnable runnable = new ISafeRunnable() {
	
							@Override
							public void handleException(final Throwable exception) {
								exception.printStackTrace();
							}
	
							@Override
							public void run() throws Exception {
								((IElementExtensionPoint) extension)
										.restoreElement(element);
							}
						};
						runnable.run();
					}
				}
			} catch (CoreException e) {
				e.printStackTrace();
				LogUtil.logError(e);
				return new Status(Status.ERROR, Activator.PLUGIN_ID,
						"Exception while deleting: " + e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
				LogUtil.logError(e);
				return new Status(Status.ERROR, Activator.PLUGIN_ID,
						"Exception while deleting: " + e.getMessage());
			}
		}

		return Status.OK_STATUS;

	}

}
