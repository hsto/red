package dk.dtu.imm.red.core.ui.internal.preferences;

import java.io.IOException;

import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.jface.preference.PathEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.BooleanFieldEditor;

public class MiscPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {
	

	/**
	 * Create the preference page.
	 */
	public MiscPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		setTitle("Misc Preference");
	}

	/**
	 * Create contents of the preference page.
	 */
	@Override
	protected void createFieldEditors() {
		
		Composite composite = getFieldEditorParent();

		addField(new BooleanFieldEditor("dummy", "Reserved for future development...", BooleanFieldEditor.DEFAULT, getFieldEditorParent()));
	}

	/**
	 * Initialize the preference page.
	 */
	public void init(IWorkbench workbench) {
		// Initialize the preference page
	}

}
