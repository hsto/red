package dk.dtu.imm.red.core.ui.element.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage; 
import org.eclipse.ui.IWorkbench; 
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizard;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardPresenter;
import dk.dtu.imm.red.core.ui.wizards.BaseWizard;

public class CreateElementReferenceWizardImpl extends BaseWizard
		implements CreateElementReferenceWizard {
	
	protected CreateElementReferencePageImpl createElementReferencePage;
	protected Class[] typesShown;
	protected String title;
	protected String description;
	
	public CreateElementReferenceWizardImpl() {
		this(null, null);
	}
	
	public CreateElementReferenceWizardImpl(String title, String description) {
		this(new Class[] {}, null, null);
	}
	
	public CreateElementReferenceWizardImpl(Class[] typesShown) {
		this(typesShown, null, null);
	} 
	
	public CreateElementReferenceWizardImpl(Class[] typesShown, String title, String description) {
		presenter = new CreateElementReferenceWizardPresenterImpl();

		this.typesShown = typesShown;
		this.title = title;
		this.description = description;
	}
	
	
	
	@Override
	public boolean performFinish() {

		Element selectedElement = 
				createElementReferencePage.getSelectedElement();
		
		if(selectedElement == null){
			return true;
		}else{
			((CreateElementReferenceWizardPresenter) presenter)
				.setSelectedLabel(selectedElement);
			return true;
		}
			
	}

	@Override
	public void pageOpenedOrClosed(IWizardPage fromPage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		createElementReferencePage = new CreateElementReferencePageImpl(typesShown, title, description);
		presenter = new CreateElementReferenceWizardPresenterImpl();
		
	}
	@Override
	public void addPages() {
		addPage(createElementReferencePage);
	}

	
}
