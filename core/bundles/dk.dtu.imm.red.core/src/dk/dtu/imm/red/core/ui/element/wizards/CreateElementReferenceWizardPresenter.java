package dk.dtu.imm.red.core.ui.element.wizards;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;

public interface CreateElementReferenceWizardPresenter extends IWizardPresenter {
	
	void wizardFinished();

	Element getselectedElement();
	
	void setSelectedLabel(Element selectedElement);
}
