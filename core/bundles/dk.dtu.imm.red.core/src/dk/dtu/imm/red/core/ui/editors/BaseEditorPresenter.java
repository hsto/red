package dk.dtu.imm.red.core.ui.editors;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LifeCyclePhase;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.element.ManagementDecision;
import dk.dtu.imm.red.core.element.Priority;
import dk.dtu.imm.red.core.element.QAAssessment;
import dk.dtu.imm.red.core.element.State;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.ui.ExtensionHelper;
import dk.dtu.imm.red.core.ui.editors.IBaseEditor.Dependency;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.element.extensions.ElementLinkActionExtension;
import dk.dtu.imm.red.core.ui.presenters.BasePresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.core.user.User;
import dk.dtu.imm.red.core.user.UserFactory;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.core.element.QuantityLabel;
import dk.dtu.imm.red.core.ui.element.wizards.CreateQuantityItemWizardFactory;
import dk.dtu.imm.red.core.element.QuantityItem;
import dk.dtu.imm.red.core.element.unit.BaseUnit;
import dk.dtu.imm.red.core.element.unit.Unit;
import dk.dtu.imm.red.core.element.unit.UnitFactory;

public abstract class BaseEditorPresenter extends BasePresenter implements IEditorPresenter {

	private static final int TEXTFIELD_MAX_LENGTH = 17;
	private static final int VERSION_MAX_LENGTH = 10;
	private static final int DATE_MAX_LENGTH = 10;

	protected Element element;

	protected String author;
	protected String deadline;
	protected String priority;
	protected String responsibleUser;
	protected String state;
	protected String lifeCyclePhase; // #231
	protected String managementDecision;
	protected String qaAssessment;
	protected String version;
	protected String workpackage;
	protected CommentList changelog;
	protected EAGPresenter<QuantityItem> quantityPresenter;

	private int undefined = -999;
	protected int cost = undefined;
	protected int benefit = undefined;
	protected int risk = undefined;

	protected LockType lockType;

	// created by Henry
	public BaseEditorPresenter(Element element) {
		this.element = element;
		final Element finalElement = element;

		this.quantityPresenter = new EAGPresenter<QuantityItem>() {

			@Override
			public void AddItem() {
				QuantityItem item = getNewItem();
				QuantityLabel label = CreateQuantityItemWizardFactory.createQuantityItem(finalElement.getQuantities());
				if (label != null) {
					item.setLabel(label);
					super.AddItem(item);
				}
			}
		};
		quantityPresenter.setReflectedEClassByName("dk.dtu.imm.red.core.element", "QuantityItem");// For
																									// Create,Delete
		quantityPresenter.setReflectedClass(QuantityItem.class); // For element
																	// selector
	}

	@Override
	public void dispose() {
	}

	@Override
	public void saveFileData() {
		if (element.getCreator() == null) {
			User creator = UserFactory.eINSTANCE.createUser();
			creator.setName(author);
			element.setCreator(creator);
		} else {
			element.getCreator().setName(author);
		}

		if (element.getResponsibleUser() == null) {
			User responsible = UserFactory.eINSTANCE.createUser();
			responsible.setName(responsibleUser);
			element.setResponsibleUser(responsible);
		} else {
			element.getResponsibleUser().setName(responsibleUser);
		}

		if (priority != "") {
			element.setPriority(Priority.get(priority));
		} else {
			element.setPriority(null);
		}

		// if (state != "") {
		// element.setLifecycleStatus(ElementLifecycle.get(state));
		// } else {
		// element.setLifecycleStatus(null);
		// }
		if (state != "") {
			element.setState(State.get(state));
		} else {
			element.setState(null);
		}

		if (lifeCyclePhase != "") {
			element.setLifeCyclePhase(LifeCyclePhase.get(lifeCyclePhase));
		} else {
			element.setLifeCyclePhase(null);
		}

		if (managementDecision != "") {
			element.setManagementDecision(ManagementDecision.get(managementDecision));
		} else {
			element.setManagementDecision(null);
		}

		if (qaAssessment != "") {
			element.setQaAssessment(QAAssessment.get(qaAssessment));
		} else {
			element.setQaAssessment(null);
		}

		element.setVersion(version);
		element.setWorkPackage(workpackage);
		element.setChangeList(changelog);
		LockType oldLock = element.getLockStatus();
		element.setLockStatus(lockType);
		if (oldLock != lockType) {
			// if the lock status is changed, change the password as well
			// Fix issue #17, which caused element to be locked even when
			// password is not set
			if (WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getPassword() == null) {
				element.setLockPassword("");
			} else {
				element.setLockPassword(WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getPassword());
			}

		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (deadline != null && deadline != "") {
			try {
				element.setDeadline(sdf.parse(deadline));
			} catch (ParseException e) {
				element.setDeadline(null);
			}
		}

		if (element.getCost() != null && cost != undefined) {
			element.getCost().setValue(cost);
		}
		if (element.getBenefit() != null && benefit != undefined) {
			element.getBenefit().setValue(benefit);
		}
		if (risk != undefined) {
			element.setRisk(risk);
		}

	}

	@Override
	public void save() {

		if (element.getCreator() == null) {
			User creator = UserFactory.eINSTANCE.createUser();
			creator.setName(author);
			element.setCreator(creator);
		} else {
			element.getCreator().setName(author);
		}

		if (element.getResponsibleUser() == null) {
			User responsible = UserFactory.eINSTANCE.createUser();
			responsible.setName(responsibleUser);
			element.setResponsibleUser(responsible);
		} else {
			element.getResponsibleUser().setName(responsibleUser);
		}

		if (priority != "") {
			element.setPriority(Priority.get(priority));
		} else {
			element.setPriority(null);
		}

		if (state != "") {
			element.setState(State.get(state));
		} else {
			element.setState(null);
		}

		if (lifeCyclePhase != "") {
			element.setLifeCyclePhase(LifeCyclePhase.get(lifeCyclePhase));
		} else {
			element.setLifeCyclePhase(null);
		}

		if (!managementDecision.equals("")) {
			element.setManagementDecision(ManagementDecision.get(managementDecision));
		} else {
			element.setManagementDecision(null);
		}

		if (qaAssessment != "") {
			element.setQaAssessment(QAAssessment.get(qaAssessment));
		} else {
			element.setQaAssessment(null);
		}

		element.setVersion(version);
		element.setWorkPackage(workpackage);
		element.setChangeList(changelog);
		LockType oldLock = element.getLockStatus();
		element.setLockStatus(lockType);
		if (oldLock != lockType) {
			// if the lock status is changed, change the password as well
			// Fix issue #17, which caused element to be locked even when
			// password is not set
			if (WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getPassword() == null) {
				element.setLockPassword("");
			} else {
				element.setLockPassword(WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getPassword());
			}

		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (deadline != null && deadline != "") {
			try {
				element.setDeadline(sdf.parse(deadline));
			} catch (ParseException e) {
				element.setDeadline(null);
			}
		}

		if (element.getCost() != null && cost != undefined) {
			element.getCost().setValue(cost);
		}
		if (element.getBenefit() != null && benefit != undefined) {
			element.getBenefit().setValue(benefit);
		}
		if (risk != undefined) {
			element.setRisk(risk);
		}
	}

	@Override
	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}

	@Override
	public void setPriority(String priority) {
		this.priority = priority;
	}

	@Override
	public void setResponsibleUser(String responsibleUser) {
		this.responsibleUser = responsibleUser;
	}

	@Override
	public void setState(String state) {
		this.state = state;
	}

	@Override
	public void setLifeCyclePhase(String lifeCyclePhase) {
		this.lifeCyclePhase = lifeCyclePhase;

	}

	@Override
	public void setManagementDecision(String managementDecision) {
		this.managementDecision = managementDecision;
	}

	@Override
	public void setQAAssessment(String qaAssessment) {
		this.qaAssessment = qaAssessment;
	}

	@Override
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public void setWorkpackage(String workpackage) {
		this.workpackage = workpackage;
	}

	@Override
	public void setChangelog(CommentList changelog) {
		this.changelog = changelog;
	}

	@Override
	public void setLockType(LockType lockType) {
		this.lockType = lockType;
	}

	@Override
	public void setCost(int cost) {
		this.cost = cost;
	}

	@Override
	public void setBenefit(int benefit) {
		this.benefit = benefit;
	}

	@Override
	public void setRisk(int risk) {
		this.risk = risk;
	}

	@Override
	public int getAuthorMaxLength() {
		return TEXTFIELD_MAX_LENGTH;
	}

	@Override
	public int getResponsibleUserMaxLength() {
		return TEXTFIELD_MAX_LENGTH;
	}

	@Override
	public int getWorkpackageMaxLength() {
		return TEXTFIELD_MAX_LENGTH;
	}

	@Override
	public int getDeadlineMaxLength() {
		return DATE_MAX_LENGTH;
	}

	@Override
	public int getVersionMaxLength() {
		return VERSION_MAX_LENGTH;
	}

	@Override
	public void setSpecElementBasicInfo(String label, String name, String kind, String description) {
		element.setLabel(label);
		element.setName(name);
		element.setElementKind(kind);
		element.setDescription(description);

		/**
		 * Issue #119 @Luai
		 */
		try {
			ExtensionHelper.getCommand("dk.dtu.imm.red.visualmodeling.ui.cascadeDiagramsOnRename").run();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.logError(e);
		}
	}

	@Override
	public void setElementIconURI(String URI) {
		element.setIcon(null);
		element.setIconURI(URI);
	}

	@Override
	public void addDependency(BaseEditor editor) {
		Element selectedElement = editor.getElementReference();
		// If the selected element is null or the reference is to the element
		// itself, do nothing
		if (selectedElement == null || selectedElement == editor.getEditorElement()) {
			return;
		}

		for (Dependency reference : editor.getDependencies()) {
			// If the referenced element already has a relationship to this
			// element
			// do nothing.
			if (reference.targetElement.getUniqueID().equals(selectedElement.getUniqueID())) {
				return;
			}
		}

		for (ElementRelationship relationship : editor.getEditorElement().getRelatedBy()) {
			// If the editor element is already the target of another
			// relationship with the selected element do nothing.
			if (relationship.getFromElement().getUniqueID().equals(selectedElement.getUniqueID())) {
				return;
			}
		}

		Dependency reference = new Dependency(RelationshipKind.IS_ASSOCIATED_TO, selectedElement.getIcon(),
				selectedElement, "");

		editor.getDependencies().add(reference);
		editor.markAsDirty();
	}

	@Override
	public void deleteDependency(BaseEditor editor, Dependency dependency, boolean relatedBy) {
		if (relatedBy) {
			editor.getDependenciesRelatedBy().remove(dependency);
		} else {
			editor.getDependencies().remove(dependency);
		}
		editor.markAsDirty();
	}

	@Override
	public void openDependencyElement(BaseEditor editor, Dependency dependency) {
		if (dependency != null) {
			Element target = dependency.targetElement;
			if (target != null) {
				ElementLinkActionExtension.elementClicked(target);
			}
		}
	}

	public ElementColumnDefinition[] getQuantityColumnDefinition() {
		return new ElementColumnDefinition[] {
				// Name column
				new ElementColumnDefinition().setHeaderName("Name").setColumnType(String.class).setColumnWidth(100)
						.setColumnBehaviour(ColumnBehaviour.ReferenceSelector).setEditable(false)
						.setGetterMethod(new String[] { "getLabel", "getLiteral" }),

				// Value column
				new ElementColumnDefinition().setHeaderName("Value").setColumnType(String.class).setColumnWidth(120)
						.setColumnBehaviour(ColumnBehaviour.CellEdit).setEditable(true)
						.setCellHandler(new CellHandler<QuantityItem>() {

							@Override
							public Object getAttributeValue(QuantityItem item) {
								return item.getValue();
							}

							@Override
							public void setAttributeValue(QuantityItem item, Object value) {
								try {
									item.setValue(new BigDecimal(value.toString()));
								} catch (NumberFormatException ex) {
									// Do nothing on bad input
								}
							}
						}),

				// Kind column
				new ElementColumnDefinition().setHeaderName("Unit").setColumnType(Unit.class)
						.setColumnBehaviour(ColumnBehaviour.ComboSelector)
						.setCellHandler(new CellHandler<QuantityItem>() {

							@Override
							public Object getAttributeValue(QuantityItem item) {
								return item.getUnit().getSymbol();
							}

							@Override
							public void setAttributeValue(QuantityItem item, Object value) {
								String stringValue = (String) value;
								if (item.getUnit() instanceof BaseUnit && !stringValue.isEmpty()) {
									BaseUnit newUnit = UnitFactory.eINSTANCE.createBaseUnit();
									newUnit.setSymbol(stringValue);
									item.setUnit(newUnit);
								} else {
									Unit newUnit = null;
									for (Unit unit : item.getValidUnits()) {
										if (value.equals(unit.getSymbol())) {
											newUnit = unit;
											break;
										}
									}

									if (newUnit != null)
										item.setUnit(newUnit);
								}
							}

							@Override
							public String[] getComboValues(Object item) {
								List<Unit> unitList = ((QuantityItem) item).getValidUnits();
								String[] unitNames = new String[unitList.size()];

								for (int i = 0; i < unitList.size(); i++) {
									unitNames[i] = unitList.get(i).getSymbol();
								}

								return unitNames;
							}
						}) };
	}
}
