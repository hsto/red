package dk.dtu.imm.red.core.ui.internal.project.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.util.FileHelper;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.project.ProjectFactory;
import dk.dtu.imm.red.core.ui.internal.element.extensions.CoreElementExtension;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.ui.internal.project.editors.impl.filesListData;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class CreateProjectOperation extends AbstractOperation {

	protected String projectDescription;
	protected Group parent;
	protected String path;
	protected String name;
	protected CoreElementExtension extension;

	protected Project project;

	public CreateProjectOperation(String name, String projectDescription, String path) {
		super("Create Project");

		this.projectDescription = projectDescription;
		this.path = path;
		this.name = name;

		extension = new CoreElementExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
		throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
		throws ExecutionException {
		String filePath = FileHelper.join(path, name + ".redproject");
		URI fileUri = URI.createFileURI(filePath);
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.createResource(fileUri);

		project = ProjectFactory.eINSTANCE.createProject();
		project.setCreator(PreferenceUtil.getUserPreference_User());
		project.setName(name + ".redproject");
		project.setParent(null);
		
		resource.getContents().add(project);
		project.save();

		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		workspace.addOpenedProject(project);
		filesListData.path = path.replace(name + ".redproject", "");
		filesListData.ProjectNameAndPath = path;
		extension.openElement(project);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
		throws ExecutionException {
		if (parent != null) {
			parent.getContents().remove(project);
			extension.deleteElement(project);
			extension.deleteElement(project.getListofopenedfilesinproject().get(0));

			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}
}
