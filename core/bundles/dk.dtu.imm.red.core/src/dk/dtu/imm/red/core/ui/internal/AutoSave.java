package dk.dtu.imm.red.core.ui.internal;

import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;


public enum AutoSave {
	INSTANCE;
	
	private Timer timer;
	
	public void setAndStartTimer(String period) {
		int millisecond = convertToMillisecond(period);
		if (millisecond < 1) {
			resetTimer();
		} else {
			if (timer != null) {
				resetTimer();
			}
			timer = new Timer();
			TimerTask autoSaveTask = new AutoSaveTask();
			timer.schedule(autoSaveTask, millisecond, millisecond);			
		}
	}
	
	private void resetTimer() {
		timer.cancel();
		timer = null;
	}
	
	private int convertToMillisecond(String period) {
		int i = 0;
		try {
			i = Integer.parseInt(period);
		} catch (Exception e) {
			
		}
		return i * 60 * 1000;
	}

	class AutoSaveTask extends TimerTask {
		
		@Override
		public void run() {
			PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
				public void run() {
					IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
					if (window != null) {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().saveAllEditors(false);
					}
				}
			});
			
		}

	}

}