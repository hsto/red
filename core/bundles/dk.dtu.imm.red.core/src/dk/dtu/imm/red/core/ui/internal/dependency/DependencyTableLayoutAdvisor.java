package dk.dtu.imm.red.core.ui.internal.dependency;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ScalableColumnLayoutAdvisor;

/**
 * The layout advisor for the associations table in the persona editor.
 * @author Jakob Kragelund
 *
 */
public class DependencyTableLayoutAdvisor 
	extends ScalableColumnLayoutAdvisor {

	private static final int DEFAULT_COLUMN_WEIGHT = 50;
	private static final int COMMENT_COLUMN_WEIGHT = 77;
	private static final int NAME_COLUMN_WEIGHT = 20;
	private static final int DEPENDENCY_KIND_COLUMN_WEIGHT = 10;
	private static final int ICON_COLUMN_WEIGHT = 3;
	/**
	 * Indices of the columns in the table.
	 */
	private static final int COMMENT_COLUMN = 3;
	private static final int ELEMENT_NAME_COLUMN = 2;
	private static final int DEPENDENCY_KIND_COLUMN = 1;
	private static final int ICON_COLUMN = 0;
	
	/**
	 * Number of columns in the table.
	 */
	private static final int NUM_COLUMNS = 4;
	
	/**
	 * Number of rows currently in the table.
	 */
	private int rowCount;
	
	/**
	 * The names of the table columns.
	 */
	private String[] columnNames;
	

	/**
	 * Creates a new layout advisor for the associations table.
	 * @param agileGrid the table
	 * @param columnNames the names of the columns
	 */
	public DependencyTableLayoutAdvisor(final AgileGrid agileGrid, 
			final String[] columnNames) {
		super(agileGrid);
		
		rowCount = 0;
		this.columnNames = columnNames;
		
	}
	
	
	@Override
	public int getColumnCount() {
		return NUM_COLUMNS;
	}
	
	/*
	 * Returns the name of the given column.
	 * (non-Javadoc)
	 * @see org.agilemore.agilegrid.DefaultLayoutAdvisor#getTopHeaderLabel(int)
	 */
	@Override
	public String getTopHeaderLabel(final int col) {
		switch (col) {
			case ICON_COLUMN:
				return columnNames[ICON_COLUMN];
			case DEPENDENCY_KIND_COLUMN:
				return columnNames[DEPENDENCY_KIND_COLUMN];
			case ELEMENT_NAME_COLUMN:
				return columnNames[ELEMENT_NAME_COLUMN];
			case COMMENT_COLUMN:
				return columnNames[COMMENT_COLUMN];
			default:
				return super.getTopHeaderLabel(col);
		}
	}
	
	
	@Override
	public int getRowCount() {
		return rowCount;
	}
	
	@Override
	public void setRowCount(final int rowCount) {
		this.rowCount = rowCount;
	}
	
	@Override
	public int getInitialColumnWidth(final int column) {
		switch(column) {
			case ICON_COLUMN:
				return ICON_COLUMN_WEIGHT;
			case DEPENDENCY_KIND_COLUMN:
				return DEPENDENCY_KIND_COLUMN_WEIGHT;
			case ELEMENT_NAME_COLUMN:
				return NAME_COLUMN_WEIGHT;
			case COMMENT_COLUMN:
				return COMMENT_COLUMN_WEIGHT;
			default:
				return DEFAULT_COLUMN_WEIGHT;
		}
	}
	
}
