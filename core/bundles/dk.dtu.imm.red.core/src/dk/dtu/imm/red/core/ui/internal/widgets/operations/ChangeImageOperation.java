package dk.dtu.imm.red.core.ui.internal.widgets.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.internal.widgets.ImagePanelWidgetPresenter;

/**
 * Operation which contains logic for changing the image of an image widget.
 * If the new image should be loaded from an external file, the operation
 * opens a file selection dialog and uses this image. 
 * This behavior is controlled by a boolean passed in the constructor.
 * @author Jakob Kragelund
 *
 */
public class ChangeImageOperation extends AbstractOperation {

	/**
	 * The presenter of the persona editor, used as callback 
	 * for returning the loaded image.
	 */
	protected ImagePanelWidgetPresenter presenter;
	
	/**
	 * The previous image, used for undo-purposes.
	 */
	protected Image oldImage;
	
	/**
	 * The new image, used for redo-purposes.
	 */
	protected Image newImage;
	
	/**
	 * Boolean which indicates whether a new image should be
	 * opened, or the image should be cleared.
	 */
	protected boolean openNewImage;

	/**
	 * Creates a new Change Image operation, which can be executed by the
	 * operation history.
	 * @param imagePanelWidgetPresenter the presenter for the persona editor, 
	 * used for updating the persona image
	 * @param openNewImage true if a new image should be loaded, false if the 
	 * default image should be used.
	 */
	public ChangeImageOperation(
			final ImagePanelWidgetPresenter imagePanelWidgetPresenter,
			final boolean openNewImage) {
		super("Change image");
		this.presenter = imagePanelWidgetPresenter;
		this.openNewImage = openNewImage;
	}

	@Override
	public IStatus execute(final IProgressMonitor monitor, 
			final IAdaptable info)
			throws ExecutionException {

		if (openNewImage) {
			String newFileName = null;
			FileDialog imageFileChooser = 
					new FileDialog(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(), SWT.OPEN);

			imageFileChooser.setText("Select image file");
			imageFileChooser.setFilterExtensions(new String[]{
			"*.gif; *.jpg; *.png; *.bmp; *.JPEG;"});

			newFileName = imageFileChooser.open();

			if (newFileName == null) {
				return Status.CANCEL_STATUS;
			}

			ImageLoader imageLoader = new ImageLoader();
			ImageData[] imageDataArray = imageLoader.load(newFileName);

			if (imageDataArray != null && imageDataArray.length > 0) {
				newImage = new Image(Display.getCurrent(), imageDataArray[0]);
			} else {
				return Status.CANCEL_STATUS;
			}
		} else {
			newImage = null;
		}
		
		return redo(monitor, info);
	}
	
	@Override
	public boolean canRedo() {
		return !presenter.isDisposed();
	}
	
	@Override
	public boolean canUndo() {
		return !presenter.isDisposed();
	}

	@Override
	public IStatus redo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		// Store the old image, so we can undo back to it
		oldImage = presenter.getImage();

		presenter.setImage(newImage);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		presenter.setImage(oldImage);

		return Status.OK_STATUS;
	}

}
