package dk.dtu.imm.red.core.ui.internal.preferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.Cell;
import org.agilemore.agilegrid.ISelectionChangedListener;
import org.agilemore.agilegrid.SWTX;
import org.agilemore.agilegrid.SelectionChangedEvent;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.preferences.servercomms.HTTPConnectionUtil;

public class ServersPreferencePage extends PreferencePage  {

	Composite parent;
	AgileGrid serversTable;
	ServersTableContentProvider serversTableContentProvider;
	ServersTableLayoutAdviser serversTableLayoutAdviser ;
	ArrayList<ServersTableEntry> serversEntryList = new ArrayList<ServersTableEntry>();
	
	Button btnAdd;
	Button btnRemove;
	Button btnTest;
//	Button btnClear;

	/**
	 * Create the preference page.
	 */
	public ServersPreferencePage() {
		setTitle("Servers Preference");
	}


	@Override
	protected Control createContents(final Composite parent) {
		this.parent =parent;
		Composite composite = new Composite(parent, SWT.NULL);
		composite.setLayout(new GridLayout(4, false));
		
		Label lblServers = new Label(composite, SWT.NONE);
		lblServers.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		lblServers.setText("Servers List");
		
		serversTable = new AgileGrid(composite, SWTX.ROW_SELECTION | SWT.V_SCROLL);
		serversTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		serversTableContentProvider = new ServersTableContentProvider();
		serversTableLayoutAdviser = new ServersTableLayoutAdviser(serversTable);
		serversTable.setContentProvider(serversTableContentProvider);
		serversTableContentProvider.setContent(serversEntryList);
		serversTable.setLayoutAdvisor(serversTableLayoutAdviser);
		serversTable.setCellEditorProvider(new ServersTableCellEditorProvider(serversTable));
		
		serversTable.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				if (serversTable.getCellSelection().length > 0) {
					btnRemove.setEnabled(true);
				} else {
					btnRemove.setEnabled(false);
				}
			}
		});
		
		btnAdd = new Button(composite, SWT.NONE);
		btnAdd.setText("+");
		
		btnAdd.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				ServersTableEntry entry = new ServersTableEntry();
				serversEntryList.add(entry);
				serversTableLayoutAdviser.setRowCount(serversEntryList.size());
				serversTable.redraw();
			}
		});
		
		btnRemove = new Button(composite, SWT.NONE);
		btnRemove.setText("-");
		
		btnRemove.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Cell[] selectedCells = serversTable.getCellSelection();
				Set<ServersTableEntry> selectedServers = new HashSet<ServersTableEntry>();

				for (Cell cell : selectedCells) {
					selectedServers.add(
							serversEntryList.get(cell.row));
				}

				serversEntryList.removeAll(selectedServers);

				serversTableLayoutAdviser.setRowCount(
						serversEntryList.size());
				serversTable.clearSelection();
				btnRemove.setEnabled(false);				
				serversTable.redraw();
			}
		});
		
		btnTest = new Button(composite, SWT.NONE);
		btnTest.setText("Test");
		btnTest.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Cell[] selectedCells = serversTable.getCellSelection();
				ServersTableEntry selectedServer = serversEntryList.get(selectedCells[0].row);
				
				if(selectedServer.getService().toUpperCase().equals("HTTP")){
					HTTPConnectionUtil httpUtil = new HTTPConnectionUtil();
					try {
						httpUtil.sendGet(selectedServer.getServer(), selectedServer.getUsername(), selectedServer.getPassword());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace(); LogUtil.logError(e);
					}
				}
			}
		});
		
//		btnClear = new Button(composite, SWT.NONE);
//		btnClear.setText("Clear");
//		btnClear.addListener(SWT.Selection, new Listener() {
//			@Override
//			public void handleEvent(Event event) {
//				ServersTableEntry entry = new ServersTableEntry();
//				serversEntryList.clear();
//				serversTableLayoutAdviser.setRowCount(serversEntryList.size());
//				serversTable.redraw();
//			}
//		});
		
		initializeValues();
		return composite;
	}
	
	private void initializeValues() {
		IPreferenceStore store = getPreferenceStore();
		if(store!=null){
			for(int i=0;i<store.getInt(PreferenceConstants.SERVERS_PREF_ARRAYSIZE);i++){
				if(serversEntryList.size()<i+1){
					serversEntryList.add(new ServersTableEntry());
				}
				String name = store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_NAME);
				String service = store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_SERVICES);
				String server = store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_SERVER);
				String username = store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_USERNAME);
				String password = store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_PASSWORD);
				String extraParams = store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_EXTRA_PARAMS);

	
				if( (name!=null & !name.isEmpty()) ||
					(service!=null & !service.isEmpty()) ||
					(server!=null & !server.isEmpty()) ||
					(username!=null & !username.isEmpty()) ||
					(password!=null & !password.isEmpty()) ||
					(extraParams!=null & !extraParams.isEmpty()) 
						){
					serversEntryList.get(i).setName(store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_NAME));
					serversEntryList.get(i).setService(store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_SERVICES));
					serversEntryList.get(i).setServer(store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_SERVER));
					serversEntryList.get(i).setUsername(store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_USERNAME));
					serversEntryList.get(i).setPassword(store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_PASSWORD));
					serversEntryList.get(i).setExtraParams(store.getString("List["+i+"]."+PreferenceConstants.SERVERS_PREF_EXTRA_PARAMS));

				}
				
			}
			serversTableLayoutAdviser.setRowCount(serversEntryList.size());
			serversTable.redraw();
		}
		
	}
	
	private void storeValues() {
		IPreferenceStore store = getPreferenceStore();
		if(store!=null){
		
			int previousServersEntryListSize = store.getInt(PreferenceConstants.SERVERS_PREF_ARRAYSIZE);
			
			for(int i=0;i<serversEntryList.size();i++){
				String curName = serversEntryList.get(i).getName();
				String curService = serversEntryList.get(i).getService();
				String curServer = serversEntryList.get(i).getServer();
				String curUsername = serversEntryList.get(i).getUsername();
				String curPassword = serversEntryList.get(i).getPassword();
				String curExtraParams = serversEntryList.get(i).getExtraParams();
				
				if(curName!=null){
					store.setValue("List["+i+"]."+PreferenceConstants.SERVERS_PREF_NAME, curName);
				}
				if(curService!=null){
					store.setValue("List["+i+"]."+PreferenceConstants.SERVERS_PREF_SERVICES, curService);
				}
				if(curServer!=null){
					store.setValue("List["+i+"]."+PreferenceConstants.SERVERS_PREF_SERVER, curServer);
				}
				if(curUsername!=null){
					store.setValue("List["+i+"]."+PreferenceConstants.SERVERS_PREF_USERNAME, curUsername);
				}
				if(curPassword!=null){
					store.setValue("List["+i+"]."+PreferenceConstants.SERVERS_PREF_PASSWORD, curPassword);
				}
				if(curExtraParams!=null){
					store.setValue("List["+i+"]."+PreferenceConstants.SERVERS_PREF_EXTRA_PARAMS, curExtraParams);
				}
			}
			if(previousServersEntryListSize>serversEntryList.size()){
				//clear unused servers entry from the preference store
				for(int j=serversEntryList.size();j<previousServersEntryListSize;j++){
					store.setValue("List["+j+"]."+PreferenceConstants.SERVERS_PREF_NAME,"");
					store.setValue("List["+j+"]."+PreferenceConstants.SERVERS_PREF_SERVICES,"");
					store.setValue("List["+j+"]."+PreferenceConstants.SERVERS_PREF_SERVER, "");
					store.setValue("List["+j+"]."+PreferenceConstants.SERVERS_PREF_USERNAME, "");
					store.setValue("List["+j+"]."+PreferenceConstants.SERVERS_PREF_PASSWORD, "");
					store.setValue("List["+j+"]."+PreferenceConstants.SERVERS_PREF_EXTRA_PARAMS,"");
				}
			}
			
			store.setValue(PreferenceConstants.SERVERS_PREF_ARRAYSIZE, serversEntryList.size());
		}
	}

	@Override
	public boolean performOk() {
		storeValues();
		return super.performOk();
	}
}
