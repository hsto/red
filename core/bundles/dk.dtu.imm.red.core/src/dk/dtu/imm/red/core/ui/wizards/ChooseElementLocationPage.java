package dk.dtu.imm.red.core.ui.wizards;

import java.io.File;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;

public class ChooseElementLocationPage extends WizardPage
		implements IWizardPage {

	protected Label locationLabel;
	protected Text locationText;
	protected Button browseButton;

	protected Label validationLabel;

	protected Group optionsGroup;

	protected Label overwriteLabel;
	protected Button overwriteButton;

	protected String initialFileName;

	protected String fileExtension;

	public ChooseElementLocationPage(String elementType, String fileExtension) {
		super("Choose Element Location");
		setTitle("Choose a save location");
		setDescription("Using the Browse-button, select a location"
				+ " on your computer where this " + elementType
				+ " should be stored.");

		initialFileName = null;
		this.fileExtension = fileExtension;
	}

	public ChooseElementLocationPage(String elementType) {
		this(elementType, "red");
	}

	public void setInitialFileName(String fileName) {
		this.initialFileName = fileName;
	}

	@Override
	public void createControl(final Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(3, false);
		container.setLayout(layout);

		locationLabel = new Label(container, SWT.NONE);
		locationLabel.setText("Location:");

		GridData locationTextLayout = new GridData();
		locationTextLayout.grabExcessHorizontalSpace = true;
		locationTextLayout.horizontalAlignment = SWT.FILL;
		locationTextLayout.minimumWidth = 150;
		locationText = new Text(container, SWT.BORDER);
		locationText.setLayoutData(locationTextLayout);
		locationText.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (locationText.getText() != null
						&& !locationText.getText().trim().isEmpty()) {

					boolean validFilePath = false;

					File file = new File(locationText.getText());
					if (file.isFile() ) {
						validFilePath = true;
					}

					try {
						validFilePath = file.createNewFile();
						file.delete();
					} catch (Exception e) {
						validFilePath = false;
					}

					if (validFilePath) {
						setPageComplete(true);
						validationLabel.setVisible(false);
					} else {
						setPageComplete(false);
						validationLabel.setVisible(true);
					}
				}
			}
		});

		GridData validationLabelLayout = new GridData();
		validationLabelLayout.horizontalSpan = 3;
		validationLabel = new Label(container, SWT.NONE);
		validationLabel.setText("Path is invalid");
		validationLabel.setLayoutData(validationLabelLayout);
		validationLabel.setVisible(false);

		browseButton = new Button(container, SWT.NONE);
		browseButton.setText("Browse");
		browseButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				FileDialog dialog = new FileDialog(
					 ChooseElementLocationPage.this.getShell(), SWT.SAVE);
				dialog.setText("Choose location");

				String workspacePath = PreferenceUtil.getPreferenceString(PreferenceConstants.PROJECT_PREF_WORKSPACE_PATH);
				if(workspacePath!=null && !workspacePath.isEmpty()){
					File f = new File(workspacePath);
					if(f.exists()){
						dialog.setFilterPath(workspacePath);
					}
				}
				String[] filterExtensions = { "*." + fileExtension };
				dialog.setFilterExtensions(filterExtensions);
				dialog.setFileName(initialFileName);

				String selected = dialog.open();
				if (selected == null) {
					return;
				}
				locationText.setText(selected);
				locationText.notifyListeners(SWT.Modify, null);
			}
		});

//		GridData optionsGroupLayoutData = new GridData();
//		optionsGroupLayoutData.horizontalSpan = 3;
//		GridLayout optionsGroupLayout = new GridLayout(2, false);
//
//		optionsGroup = new Group(container, SWT.SHADOW_ETCHED_IN);
//		optionsGroup.setText("Options");
//		optionsGroup.setLayoutData(optionsGroupLayoutData);
//		optionsGroup.setLayout(optionsGroupLayout);
//
//		overwriteButton = new Button(optionsGroup, SWT.CHECK);
//		overwriteButton.setText("Overwrite existing file without warning");

		this.setPageComplete(false);
		this.setControl(container);
	}

	public String getFileName() {
		if (isPageComplete()) {
			return locationText.getText();
		} else {
			return null;
		}
	}

}
