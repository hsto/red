package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import dk.dtu.imm.red.core.ui.internal.project.editors.ProjectEditor;
import dk.dtu.imm.red.core.ui.widgets.PropertySelection;

public class UseCasePointsComposite extends Composite {
	private Spinner spProductivityFactor;
	private PropertySelection propertycostPerUsecasePoint; 
	private Group grpManagementFactor;
	private Group grpGrptechnologyfactor;
	private Button btnCreateDefaultValues;
	private Composite composite;
	private Label lblDefaultUsecasePoint;

	
	public Label getLblDefaultUsecasePoint() {
		return lblDefaultUsecasePoint;
	}

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public UseCasePointsComposite(final Composite parent, int style, final ProjectEditor editorParent) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Composite composite_1 = new Composite(this, SWT.NONE);
		GridData gd_composite1 = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_composite1.heightHint = 350;
		composite_1.setLayoutData(gd_composite1);
		composite_1.setLayout(new GridLayout(2, true));
		
		grpManagementFactor = new Group(composite_1, SWT.NONE);
		grpManagementFactor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpManagementFactor.setText("Management Factor");
		grpManagementFactor.setLayout(new GridLayout(1, false));
		 
		
		grpGrptechnologyfactor = new Group(composite_1, SWT.NONE);
		grpGrptechnologyfactor.setLayout(new GridLayout(1, false));
		grpGrptechnologyfactor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpGrptechnologyfactor.setText("Technology Factor");
		
		Composite composite_2 = new Composite(this, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, false, false, 1, 1));
		GridLayout gl_composite_2 = new GridLayout(2, true);
		gl_composite_2.horizontalSpacing = 0;
		gl_composite_2.marginHeight = 0;
		gl_composite_2.verticalSpacing = 0;
		gl_composite_2.marginWidth = 0;
		composite_2.setLayout(gl_composite_2);
		
		Group grpProductivityFactor = new Group(composite_2, SWT.NONE);
		grpProductivityFactor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpProductivityFactor.setLayout(new GridLayout(2, false));
		grpProductivityFactor.setText("Productivity Factor");
		
		spProductivityFactor = new Spinner(grpProductivityFactor, SWT.BORDER);
		spProductivityFactor.setMaximum(999999999);
		spProductivityFactor.setDigits(2);
		
		Label lblManhoursPerUcp = new Label(grpProductivityFactor, SWT.NONE);
		lblManhoursPerUcp.setText("Hours per Use Case Point");
		
		Group gpCost = new Group(composite_2, SWT.NONE);
		gpCost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		gpCost.setText("Cost Factor");
		gpCost.setLayout(new GridLayout(1, false));
		
		propertycostPerUsecasePoint = new PropertySelection(gpCost, SWT.NONE);
		propertycostPerUsecasePoint.getLblUnitDescription().setText("Cost per hour");
		GridData gridData_1 = (GridData) propertycostPerUsecasePoint.getCmbKind().getLayoutData();
		gridData_1.horizontalAlignment = SWT.LEFT;
		gridData_1.grabExcessVerticalSpace = false;
		gridData_1.grabExcessHorizontalSpace = false;
		GridData gridData_3 = (GridData) propertycostPerUsecasePoint.getCmbUnit().getLayoutData();
		gridData_3.horizontalAlignment = SWT.LEFT;
		gridData_3.grabExcessHorizontalSpace = true;
		gridData_3.grabExcessVerticalSpace = false;
		((GridData) propertycostPerUsecasePoint.getCmbKind().getLayoutData()).exclude = true;
		propertycostPerUsecasePoint.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite.setLayout(new GridLayout(2, false));
		
		btnCreateDefaultValues = new Button(composite, SWT.NONE);
		btnCreateDefaultValues.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				editorParent.createDefaultUsecasePoint();
			}
		});
		btnCreateDefaultValues.setText("Reset To Default");
		 
		
		lblDefaultUsecasePoint = new Label(composite, SWT.NONE);
		lblDefaultUsecasePoint.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblDefaultUsecasePoint.setBounds(0, 0, 55, 15); 
		
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	} 


 
	public Spinner getSpProductivityFactor() {
		return spProductivityFactor;
	}
	
 
	public PropertySelection getPropertycostPerUsecasePoint() {
		return propertycostPerUsecasePoint;
	}
	
	public Group getGrpManagementFactor() {
		return grpManagementFactor;
	}

	public Group getGrpGrptechnologyfactor() {
		return grpGrptechnologyfactor;
	}
}
