package dk.dtu.imm.red.core.ui.internal.file.operations;

import java.io.IOException;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

//import sun.security.jca.GetInstance.Instance;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.element.extensions.CoreElementExtension;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

//import dk.dtu.imm.red.core.ui.internal.element.extensions.CoreElementExtension;

public class NewFileOperation extends AbstractOperation {

	protected String name;
	protected String filePath;
	protected Group parent;
	protected File file;
	protected Project project;
	protected CoreElementExtension extension;

	public NewFileOperation(String name, Group parent, String path) {
		super("New File");

		this.name = name;
		this.filePath = path;
		this.parent = parent;
		this.project = (Project) parent;
		extension = new CoreElementExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
		throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
		throws ExecutionException {

		java.io.File physicalFile = new java.io.File(filePath);

		if (physicalFile.exists()) {
			// Obviously we don't want to blindly overwrite an existing file.
			// This should have been checked much earlier (e.g. in a wizard).
			return Status.CANCEL_STATUS;
		}
		
		ResourceSet resourceSet = project.getResourceSet();
		URI uri = URI.createFileURI(filePath);
		Resource resource = resourceSet.createResource(uri);

		File file = FileFactory.eINSTANCE.createFile();
		file.setCreator(PreferenceUtil.getUserPreference_User());

		file.setParent(null);
		file.setLongDescription(TextFactory.eINSTANCE.createText(""));
		file.setName(name + ".red");
		
		resource.getContents().add(file);
		file.save();
		
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		workspace.addOpenedFile(file);
		extension.openElement(file);

		project.addToListOfFilesInProject(file);
		project.save();

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
		throws ExecutionException {
		if (parent != null) {
			parent.getContents().remove(file);
			extension.deleteElement(file);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public boolean canUndo() {
		return parent != null;
	}

}
