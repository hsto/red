package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import java.util.List;

import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.project.ProjectDate;
import dk.dtu.imm.red.core.project.UserTableEntry;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.ui.internal.project.editors.ProjectEditor;
import dk.dtu.imm.red.core.ui.internal.project.editors.ProjectEditorPresenter;
import dk.dtu.imm.red.core.usecasepoint.Factor;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactor;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactorValue;

public class ProjectEditorPresenterImpl extends BaseEditorPresenter implements ProjectEditorPresenter{

	protected ProjectEditor projectEditor;
	
//	protected String name;
	protected List<ProjectDate> dates;
	protected List<UserTableEntry> userEntries;
	
	public ProjectEditorPresenterImpl(ProjectEditor projectEditor, Project element){
		super(element);
		this.projectEditor = projectEditor;
		this.element = element;
		
		
	}
	
	@Override
	public void save() {
		super.save();
		
		Project project = (Project) element;
//		project.setName(name);
		project.getProjectDates().clear();
		project.getProjectDates().addAll(dates);
		project.getUsers().clear();
		project.getUsers().addAll(userEntries);
		
		project.save();
	}
	
	@Override
	public void setDates(List<ProjectDate> dates) {
		this.dates = dates;
	}
	
	@Override
	public void setUsers(List<UserTableEntry> users) {
		this.userEntries = users;
	}
	
//	@Override
//	public void setName(String text) {
//		this.name = text;
//	}

	@Override
	public void setManagementFactorAspectValue(WeightedFactor mfa,
			int enumOrdinalValue) { 
		mfa.setFactorValue(WeightedFactorValue.VALUES.get(enumOrdinalValue)); 
	}

	@Override
	public void setManagementFactorAspectWeight(WeightedFactor mfa,
			double weight) {
		mfa.setWeight(weight);
	}

	@Override
	public void setFactorValue(Factor f, double selection) {
		f.setSum(selection);
	}

	@Override
	public void createDefaultUsecasePoint() {
		Project project = (Project) element;
		project.createDefaultUsecasePoint();
	}
	
}
	

 
