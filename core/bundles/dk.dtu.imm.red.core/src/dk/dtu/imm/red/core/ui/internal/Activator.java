package dk.dtu.imm.red.core.ui.internal;

import java.io.File;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	public static final String tmpDirPath;

	public static final String HELP_ICON = "HELP_ICON";

	public static final String CALENDAR_ICON = "CALENDAR_ICON";

	public static final String NAVIGATE_FORWARD_ICON = "NAVIGATE_FORWARD_ICON";
	
	public static final String LOCK_COMPLETE_ICON = "LOCK_COMPLETE_ICON";
	public static final String LOCK_PARTIAL_ICON = "LOCK_PARTIAL_ICON";
	public static final String LOCK_UNLOCK_ICON = "LOCK_UNLOCK_ICON";
	
	public static final String HISTORY_OLD_ICON = "HISTORY_OLD_ICON"; 
	public static final String HISTORY_NEW_ICON = "HISTORY_NEW_ICON"; 

	// The plug-in ID
	public static final String PLUGIN_ID = "dk.dtu.imm.red.core"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	// setting valid tmpDirPath
	static {
		String tempdir = System.getProperty("java.io.tmpdir");

		StringBuilder sb = new StringBuilder();
		sb.append(tempdir);

		if ( !(tempdir.endsWith("/") || tempdir.endsWith("\\")) )
			   sb.append(File.separatorChar);

		sb.append("RED");
		sb.append(File.separatorChar);

		tmpDirPath = sb.toString();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		// clean up any old modelelement diagram temp directory
		File tmpDir = new File(tmpDirPath);

		if (!tmpDir.exists()) {
			tmpDir.mkdirs();
		}

		File lockFile = new File(tmpDir, "lock.lck");
		if (!lockFile.exists()) {
			File[] files = tmpDir.listFiles();
			if (files != null) {
				for (File f : files) {
					f.delete();
				}
			}
			lockFile.createNewFile();
			lockFile.deleteOnExit();
		}

		//Fix the annoying error message due to editors not closed during shutdown
		IWorkbench workbench = PlatformUI.getWorkbench();
		final IWorkbenchPage activePage = workbench.getActiveWorkbenchWindow().getActivePage();

		workbench.addWorkbenchListener( new IWorkbenchListener()
		{
		    public boolean preShutdown( IWorkbench workbench, boolean forced )
		    {
		        activePage.closeEditors( activePage.getEditorReferences(), true);
		        return true;
		    }

		    public void postShutdown( IWorkbench workbench )
		    {

		    }
		});

		PreferenceUtil.setDefaultPreferenceValue();
		
		// Auto save
		String period = PreferenceUtil.getPreferenceString(PreferenceConstants.PROJECT_PREF_AUTOSAVE);
		AutoSave.INSTANCE.setAndStartTimer(period);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 *
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		super.initializeImageRegistry(reg);

		ImageDescriptor calendarImage = getImageDescriptor("icons/calendar.gif");
		ImageDescriptor helpImage = getImageDescriptor("icons/help.gif");
		ImageDescriptor navigateForwardImage = getImageDescriptor("icons/navigate_forward.png");
		
		ImageDescriptor lockPartialIcon = getImageDescriptor("icons/lock_partial.png");
		ImageDescriptor lockCompleteIcon = getImageDescriptor("icons/lock_complete.png");
		ImageDescriptor lockUnlockIcon = getImageDescriptor("icons/lock_unlock.png");
		
		ImageDescriptor historyOldIcon = getImageDescriptor("icons/history_arrow_left.png");
		ImageDescriptor historyNewIcon = getImageDescriptor("icons/history_arrow_right.png");

		reg.put(CALENDAR_ICON, calendarImage);
		reg.put(HELP_ICON, helpImage);
		reg.put(NAVIGATE_FORWARD_ICON, navigateForwardImage);
		
		reg.put(LOCK_COMPLETE_ICON, lockCompleteIcon);
		reg.put(LOCK_PARTIAL_ICON, lockPartialIcon);
		reg.put(LOCK_UNLOCK_ICON, lockUnlockIcon);
		
		reg.put(HISTORY_OLD_ICON, historyOldIcon);
		reg.put(HISTORY_NEW_ICON, historyNewIcon);
	}

}
