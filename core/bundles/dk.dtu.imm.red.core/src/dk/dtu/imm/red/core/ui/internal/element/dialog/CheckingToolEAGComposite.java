package dk.dtu.imm.red.core.ui.internal.element.dialog;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import dk.dtu.imm.red.core.comment.IssueComment;
import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;

public class CheckingToolEAGComposite extends EAGComposite<IssueComment> {

	public CheckingToolEAGComposite(Composite parent, int style) {
		super(parent, style, null);

		// Hide unneeded controls
		GridData hideGrid = new GridData(0, 0);
		hideGrid.exclude = true;
		btnAdd.setVisible(false);
		btnAdd.setLayoutData(hideGrid);
		chkRecursiveView.setVisible(false);
		chkRecursiveView.setLayoutData(hideGrid);
		composite_1.setVisible(false);
		composite_1.setLayoutData(hideGrid);
		
		//Severity filter
		Label filterLabel = new Label(buttonComposite, SWT.RIGHT);
		filterLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		filterLabel.setText("Hide issues with severity less than");
		
		final Combo severityCombo = new Combo(buttonComposite, SWT.READ_ONLY);
		severityCombo.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		List<IssueSeverity> severityList = new LinkedList<IssueSeverity>(IssueSeverity.VALUES);
		severityList.remove(IssueSeverity.OK);
		String[] severityNames = new String[severityList.size()];
		for (int i = 0; i < severityList.size(); i++) {
			severityNames[i] = severityList.get(i).getLiteral();
		}
		severityCombo.setItems(severityNames);
		severityCombo.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				String selection = severityCombo.getItem(severityCombo.getSelectionIndex());
				IssueSeverity minimumSeverity = IssueSeverity.get(selection);
				
				if (presenter instanceof CheckingToolEAGPresenter) {
					((CheckingToolEAGPresenter) presenter).setMinimumSeverity(minimumSeverity);
					
					agReflAgileGrid.redraw();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		severityCombo.select(0);
	}
}
