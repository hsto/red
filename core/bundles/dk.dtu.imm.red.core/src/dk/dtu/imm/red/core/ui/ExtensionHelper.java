package dk.dtu.imm.red.core.ui;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.commands.Command;

import dk.dtu.imm.red.core.ui.internal.LogUtil;

public class ExtensionHelper {
	
	/**
	 * @Luai
	 */
	public static ISafeRunnable getCommand(String id, Object trigger) {
		return getExtension(id, Collections.EMPTY_MAP, trigger);
	}
	
	public static ISafeRunnable getCommand(String id) {
		return getExtension(id, Collections.EMPTY_MAP, null);
	}
	
	public static ISafeRunnable getCommand(String id, Map<Integer, Object> param, Object trigger) {
		return getExtension(id, param, trigger);
	}
	
	public static ISafeRunnable getCommand(String id, Map<Integer,Object> param) {
		return getExtension(id, Collections.EMPTY_MAP, null);
	}
	
	public static Object[] runObjects(String id, Object trigger) {	
		 Object[] obj = (Object[]) runExtension(id, Collections.EMPTY_MAP, trigger);		 
		 return obj;
	}
	
	public static Object[] runObjects(String id) {
		Object[] obj = (Object[]) runExtension(id, Collections.EMPTY_MAP, null);		 
		return obj;
	}
	
	public static List<Command> runCommand(String id, Object trigger) {	
		 List<Command> commands = (List<Command>) runExtension(id, Collections.EMPTY_MAP, trigger);		 
		 return commands;
	}
	
	public static List<Command> runCommand(String id) {
		List<Command> commands = (List<Command>) runExtension(id, Collections.EMPTY_MAP, null);		 
		 return commands;
	}
	
	private static ISafeRunnable getExtension(String id, final Map param , final Object trigger) {
		IConfigurationElement[] configurationElements = Platform.getExtensionRegistry().getConfigurationElementsFor("org.eclipse.ui.commands");
		
		try {
			for (IConfigurationElement configurationElement : configurationElements) {

				if(configurationElement.getAttribute("id").equalsIgnoreCase(id))
				{
					final Object extension = configurationElement.createExecutableExtension("defaultHandler");
					
					if (extension instanceof AbstractHandler) {
						
						ISafeRunnable runnable = new ISafeRunnable() {

							@Override
							public void handleException(final Throwable exception) {
								exception.printStackTrace();
							}

							@Override
							public void run() throws Exception {							
								((AbstractHandler) extension).execute(new ExecutionEvent(null, param, trigger, null));			
							}
						};
						
						return runnable;
					}
				}
			}
		} catch (CoreException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (Exception e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
		
		return null;
	}
	
	private static Object runExtension(String id, final Map<Integer, Object> param, final Object trigger) {
		IConfigurationElement[] configurationElements = Platform.getExtensionRegistry().getConfigurationElementsFor("org.eclipse.ui.commands");
		final Object[] obj = new Object[1];
		
		try {
			for (IConfigurationElement configurationElement : configurationElements) {

				if(configurationElement.getAttribute("id").equalsIgnoreCase(id))
				{
					final Object extension = configurationElement.createExecutableExtension("defaultHandler");
					
					if (extension instanceof AbstractHandler) {
						
						final CountDownLatch latch = new CountDownLatch(1);
						ISafeRunnable runnable = new ISafeRunnable() {

							@Override
							public void handleException(final Throwable exception) {
								exception.printStackTrace();
							}

							@Override
							public void run() throws Exception {
								obj[0] = ((AbstractHandler) extension).execute(new ExecutionEvent(null, param, trigger, null));
								latch.countDown(); // Release wait
							}
						};
						runnable.run();
						latch.await(); // wait for release
						break;
					}
				}
			}
		} catch (CoreException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (Exception e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
		
		return obj[0];
	}

}
