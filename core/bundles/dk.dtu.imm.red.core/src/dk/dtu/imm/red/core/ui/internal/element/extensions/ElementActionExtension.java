package dk.dtu.imm.red.core.ui.internal.element.extensions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.epf.richtext.IRichText;
import org.eclipse.epf.richtext.actions.IActionExtension;
import org.eclipse.epf.richtext.actions.RichTextAction;

import dk.dtu.imm.red.core.ui.internal.element.actions.AddElementReferenceAction;

public class ElementActionExtension implements IActionExtension{

	@Override
	public List<RichTextAction> getActions(IRichText richText) {
		List<RichTextAction> actions = new ArrayList<RichTextAction>();
		actions.add(new AddElementReferenceAction(richText));

		return actions;
	}

}
