package dk.dtu.imm.red.core.ui.internal;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class Perspective implements IPerspectiveFactory {

	private IPageLayout layout;
    
	/** The standard perspective used in the application. */
	public static final String PERSPECTIVE_ID = "dk.dtu.imm.red.core.perspective";
	
	/** Left folder's id. */
	public static final String FI_LEFT = PERSPECTIVE_ID + ".leftFolder";
	
	/** Right folder's id. */
	public static final String FI_RIGHT = PERSPECTIVE_ID + ".rightFolder";
	
	private static final float LEFT_FOLDER_RATIO = 0.2f;
	private static final float RIGHT_FOLDER_RATIO = 0.7f;


    /**
     * We replace the editor area with three application folders that can be used for placing views.
     * 
     * @param the layout
     */
	@Override
    public void createInitialLayout(IPageLayout layout) {
        this.layout = layout; 
        
        layout.createFolder( FI_LEFT, IPageLayout.LEFT, LEFT_FOLDER_RATIO, layout.getEditorArea());
        layout.createFolder( FI_RIGHT, IPageLayout.RIGHT, RIGHT_FOLDER_RATIO, layout.getEditorArea());
    }
 
    /**
     * @return Returns the layout.
     */
    public IPageLayout getLayout() {
        return layout;
    }
}
