package dk.dtu.imm.red.core.ui.internal.element.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.ExtensionHelper;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
 

public class CloseElementHandler extends AbstractHandler implements IHandler {
	public ResourceSet resourceSet;
	protected Project project;
	protected Folder folder;
	protected Element element;
	

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if ((WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
				.getCurrentlyOpenedProjects().size() > 0)) {

			int result = Dialog.CANCEL;
			
			if(event.getCommand().getId().equals("dk.dtu.imm.red.core.openproject")){
				result = Dialog.OK;
			}
			else{
				//only show message dialog when it is not called from "open project"
				MessageDialog md = new MessageDialog(
						Display.getCurrent().getActiveShell(),
						"Close project",
						null,
						"Are you sure you want to close your project?",
						MessageDialog.CONFIRM, new String[] { "OK", "Cancel" }, 1);
	
				result = md.open();
			}
			
			
			if(result == Dialog.OK){
				Workspace workspace =null;
				if (((workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance())
						.getCurrentlyOpenedProjects().size() > 0)) {
					
					workspace.getCurrentlyOpenedProjects().clear();
					workspace.getCurrentlyOpenedFiles().clear();
					workspace.getContents().clear();
					
					IWorkbench workbench = PlatformUI.getWorkbench();
					IWorkbenchPage activePage = workbench.getActiveWorkbenchWindow().getActivePage();
			        activePage.closeEditors( activePage.getEditorReferences(), true);
			        
			        try {
						ExtensionHelper.getCommand("dk.dtu.imm.red.specificationelements.glossary.ui.resetGlossaryManger").run();
					} catch (Exception e) {
						e.printStackTrace(); LogUtil.logError(e);
					}
				}
			}
		}

		return null;

		
	}
		
}
