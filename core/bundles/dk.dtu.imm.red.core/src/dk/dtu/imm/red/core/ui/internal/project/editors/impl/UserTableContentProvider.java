package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import java.util.List;

import org.agilemore.agilegrid.DefaultContentProvider;

import dk.dtu.imm.red.core.project.UserTableEntry;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;

public class UserTableContentProvider extends DefaultContentProvider {

	private static final int NAME_COLUMN = 0;
	private static final int ID_COLUMN = 1;
	private static final int EMAIL_COLUMN = 2;
	private static final int PHONE_COLUMN = 3;
	private static final int SKYPE_COLUMN = 4;
	private static final int COMMENT_COLUMN = 5;

	private BaseEditor editor;
	
	private List<UserTableEntry> users;

	public UserTableContentProvider(BaseEditor baseEditor) {
		super();
		this.editor = baseEditor;
	}

	public void setContent(List<UserTableEntry> users) {
		this.users = users;
	}

	@Override
	public void doSetContentAt(int row, int col, Object value) {
		UserTableEntry userEntry = users.get(row);

		if (userEntry == null) {
			super.doSetContentAt(row, col, value);
		}

		switch (col) {
			case NAME_COLUMN:
				userEntry.getUser().setName((String) value);
				break;
			case ID_COLUMN:
				userEntry.getUser().setId((String) value);
				break;
			case EMAIL_COLUMN:
				userEntry.getUser().setEmail((String) value);
				break;
			case PHONE_COLUMN:
				userEntry.getUser().setPhoneNumber((String) value);
				break;
			case SKYPE_COLUMN:
				userEntry.getUser().setSkype((String) value);
				break;
			case COMMENT_COLUMN:
				userEntry.setComment((String) value);
				break;
			default:
				doSetContentAt(row, col, value);
		}
		
		editor.markAsDirty();
	}

	@Override
	public Object doGetContentAt(int row, int col) {

		UserTableEntry userEntry = users.get(row);

		if (userEntry == null) {
			return super.doGetContentAt(row, col);
		}

		switch (col) {
			case NAME_COLUMN:
				return userEntry.getUser().getName();
			case COMMENT_COLUMN:
				return userEntry.getComment();
			case EMAIL_COLUMN:
				return userEntry.getUser().getEmail();
			case ID_COLUMN:
				return userEntry.getUser().getId();
			case PHONE_COLUMN:
				return userEntry.getUser().getPhoneNumber();
			case SKYPE_COLUMN:
				return userEntry.getUser().getSkype();
			default:
				return doGetContentAt(row, col);
		}
	}
}
