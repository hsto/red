package dk.dtu.imm.red.core.ui.internal.search.views;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;

public class SearchLabelProvider implements ILabelProvider {

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof SearchResultWrapper) {
			return ((SearchResultWrapper) element).getElement().getIcon();
		}
		return null;
	}

	@Override
	public String getText(Object element) {
		if (element instanceof SearchResultWrapper) {
			return ((SearchResultWrapper) element).getElement().getName(); 
		} else if (element instanceof SearchResult) {
			return ((SearchResult) element).getAttribute().getName();
		}
		
		return "!!";
	}
}
