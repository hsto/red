/*
 * @author Ahmed Fikry
 */
package dk.dtu.imm.red.core.ui.internal.file.editors;

import java.util.List;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.project.ProjectDate;
import dk.dtu.imm.red.core.project.UserTableEntry;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditorPresenter;
import dk.dtu.imm.red.core.ui.internal.project.editors.ProjectEditor;
import dk.dtu.imm.red.core.ui.internal.project.editors.ProjectEditorPresenter;
import dk.dtu.imm.red.core.ui.internal.project.editors.impl.ModelProvide;

public class FileEditorPresenterImpl extends BaseEditorPresenter implements
		FileEditorPresenter {

	protected FileEditor fileEditor;
	protected Project project;

	protected String longDescription;
	protected String name;

	public FileEditorPresenterImpl(FileEditor fileEditor, File element) {
		super(element);
		this.fileEditor = fileEditor;
		this.element = element;
		longDescription = "";
		
	}

	@Override
	public void setLongDescription(String text) {
		this.longDescription = text;

	}

	@Override
	public void save() {
		super.saveFileData();
//super.save();
		File file = (File) element;
		
	    
		file.setLongDescription(TextFactory.eINSTANCE.createText(longDescription));
	    
		file.save();
		
		
	}

}
