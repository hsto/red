package dk.dtu.imm.red.core.ui.views;

import dk.dtu.imm.red.core.ui.presenters.IPresenter;

public interface IViewPresenter extends IPresenter {

}
