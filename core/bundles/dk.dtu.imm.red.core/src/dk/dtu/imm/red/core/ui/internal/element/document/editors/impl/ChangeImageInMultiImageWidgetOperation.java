package dk.dtu.imm.red.core.ui.internal.element.document.editors.impl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;
import dk.dtu.imm.red.core.ui.internal.widgets.ImagePanelWidgetPresenter;

public class ChangeImageInMultiImageWidgetOperation extends AbstractOperation {
	protected MultiImageWidgetPresenter presenter;
	protected Image oldImage;
	protected Image newImage;
	protected boolean openNewImage;
	protected int index;

	public ChangeImageInMultiImageWidgetOperation(
			final MultiImageWidgetPresenter multiImageWidgetPresenter,
			final int index,
			final boolean openNewImage) {
		super("Change image");
		this.index =index;
		this.presenter = multiImageWidgetPresenter;
		this.openNewImage = openNewImage;
	}

	@Override
	public IStatus execute(final IProgressMonitor monitor,
			final IAdaptable info)
			throws ExecutionException {

		if (openNewImage) {
			String newFileName = null;
			FileDialog imageFileChooser =
					new FileDialog(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(), SWT.OPEN);

			imageFileChooser.setText("Select image file");
			imageFileChooser.setFilterExtensions(new String[]{
			"*.gif; *.jpg; *.png; *.bmp; *.JPEG;"});


			newFileName = imageFileChooser.open();

			if (newFileName == null) {
				return Status.CANCEL_STATUS;
			}

			ImageLoader imageLoader = new ImageLoader();
			ImageData[] imageDataArray = imageLoader.load(newFileName);

			if (imageDataArray != null && imageDataArray.length > 0) {
				newImage = new Image(Display.getCurrent(), imageDataArray[0]);
			} else {
				return Status.CANCEL_STATUS;
			}
		} else {
			newImage = null;
		}

		return redo(monitor, info);
	}

	@Override
	public boolean canRedo() {
		return !presenter.isDisposed();
	}

	@Override
	public boolean canUndo() {
		return !presenter.isDisposed();
	}

	@Override
	public IStatus redo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		// Store the old image, so we can undo back to it
		oldImage = presenter.getImage(index);

		presenter.setImage(index,newImage);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		presenter.setImage(index,oldImage);

		return Status.OK_STATUS;
	}


}
