package dk.dtu.imm.red.core.ui.internal.preferences;

import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class UserPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	
	//IPreferenceStore defaultPrefStore = Activator.getDefault().getPreferenceStore();
	IPreferenceStore defaultPrefStore = new PreferenceStore("C:/test.pref");
		
	StringFieldEditor firstNameEditor;
	StringFieldEditor middleNameEditor;
	StringFieldEditor lastNameEditor;
	StringFieldEditor initialsEditor;
	StringFieldEditor idEditor;
	StringFieldEditor emailEditor;
	ComboFieldEditor roleEditor;
	ColorFieldEditor reviewColorEditor;
	StringFieldEditor passwordEditor;
	
	/**
	 * Create the preference page.
	 */
	public UserPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		setTitle("User Preference");
	}

	/**
	 * Create contents of the preference page.
	 */
	@Override
	protected void createFieldEditors() {
		{
			Composite composite = getFieldEditorParent();
			// Create the field editors
			firstNameEditor = new StringFieldEditor(PreferenceConstants.USER_PREF_FIRST_NAME, "First Name", -1, StringFieldEditor.VALIDATE_ON_KEY_STROKE, getFieldEditorParent());
			addField(firstNameEditor);
			
		}
		{
			middleNameEditor = new StringFieldEditor(PreferenceConstants.USER_PREF_MIDDLE_NAME, "Middle Name", -1, StringFieldEditor.VALIDATE_ON_KEY_STROKE, getFieldEditorParent());
			addField(middleNameEditor);
		}
		{
			lastNameEditor = new StringFieldEditor(PreferenceConstants.USER_PREF_LAST_NAME, "Last Name", -1, StringFieldEditor.VALIDATE_ON_KEY_STROKE, getFieldEditorParent());
			addField(lastNameEditor);
		}
		{
			initialsEditor = new StringFieldEditor(PreferenceConstants.USER_PREF_INITIALS, "Initials", -1, StringFieldEditor.VALIDATE_ON_KEY_STROKE, getFieldEditorParent());
			addField(initialsEditor);
		}
		{
			idEditor = new StringFieldEditor(PreferenceConstants.USER_PREF_ID, "ID", -1, StringFieldEditor.VALIDATE_ON_KEY_STROKE, getFieldEditorParent());
			addField(idEditor);
		}
		{
			emailEditor = new StringFieldEditor(PreferenceConstants.USER_PREF_EMAIL, "Email", -1, StringFieldEditor.VALIDATE_ON_KEY_STROKE, getFieldEditorParent());
			addField(emailEditor);
		}
		{
			roleEditor = new ComboFieldEditor(PreferenceConstants.USER_PREF_ROLE, "Role", new String[][]{{"author", "author"}, {"reviewer", "reviewer"}, {"manager", "manager"}, {"supervisor", "supervisor"}}, getFieldEditorParent());
			addField(roleEditor);
		}
		{
			reviewColorEditor = new ColorFieldEditor(PreferenceConstants.USER_PREF_REVIEW_COLOR, "Review Color", getFieldEditorParent());
			addField(reviewColorEditor);
		}
		{
			passwordEditor = new StringFieldEditor(PreferenceConstants.USER_PREF_PASSWORD, "Password", -1, StringFieldEditor.VALIDATE_ON_KEY_STROKE, getFieldEditorParent()){
				@Override
			    protected void doFillIntoGrid(Composite parent, int numColumns) {
			        super.doFillIntoGrid(parent, numColumns);

			        getTextControl().setEchoChar('*');
			    }
			};
			addField(passwordEditor);
			
		}
		
	}

	/**
	 * Initialize the preference page.
	 */
	public void init(IWorkbench workbench) {

	}
	
	@Override
	protected void checkState(){
		setValid(true);
	}
	
	@Override	
    public boolean performOk() {
		boolean success = super.performOk();
		
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		workspace.setPassword(passwordEditor.getStringValue());
		workspace.save();
		return success;
	}

}
