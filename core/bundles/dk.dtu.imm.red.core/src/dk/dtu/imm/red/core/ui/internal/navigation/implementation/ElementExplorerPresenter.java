package dk.dtu.imm.red.core.ui.internal.navigation.implementation;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.navigation.ElementExplorer;
import dk.dtu.imm.red.core.ui.views.BaseViewPresenter;

/**
 * Presenter for the element explorer view. Manages all logic for the element
 * explorer view.
 * 
 * @author Jakob Kragelund
 * 
 */
public class ElementExplorerPresenter extends BaseViewPresenter {

	/**
	 * The view, which this presenter manages logic for.
	 */
	protected ElementExplorer elementExplorer;

	/**
	 * Creates a presenter for the given element explorer view.
	 * 
	 * @param elementExplorer
	 *            the view for this presenter
	 */
	public ElementExplorerPresenter(final ElementExplorer elementExplorer) {
		this.elementExplorer = elementExplorer;
	}

	/**
	 * Processes double-click events for the element explorer view
	 * by sending a message to all plugins extending this plugin extension.
	 * @param elements
	 *            the elements double-clicked (or otherwise activated)
	 */
	public void elementDoubleClicked(final List<Element> elements) {
		IConfigurationElement[] configurationElements = Platform
				.getExtensionRegistry().getConfigurationElementsFor(
						"dk.dtu.imm.red.core.element");
		try {
			for (IConfigurationElement configurationElement 
					: configurationElements) {

				final Object extension = configurationElement
						.createExecutableExtension("class");

				if (extension instanceof IElementExtensionPoint) {
					ISafeRunnable runnable = new ISafeRunnable() {

						@Override
						public void handleException(final Throwable exception) {
							exception.printStackTrace();
						}

						@Override
						public void run() throws Exception {
							for (Element element : elements) {
								((IElementExtensionPoint) extension)
										.openElement(element);
							}
						}
					};
					runnable.run();
				}
			}
		} catch (CoreException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (Exception e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

}
