package dk.dtu.imm.red.core.ui.internal.editors.changelog;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ScalableColumnLayoutAdvisor;

public class ChangelogTableLayoutAdviser extends ScalableColumnLayoutAdvisor{


	private static final int DATE_COLUMN = 0;
	private static final int VERSION_COLUMN = 1;
	private static final int COMMENT_COLUMN = 2;
	private static final int AUTHOR_COLUMN = 3;
	private static final int NUM_COLUMNS = 4;
	private int rowCount;
	
	public ChangelogTableLayoutAdviser(AgileGrid agileGrid) {
		super(agileGrid);

		rowCount = 0;
	}
	
	@Override
	public int getColumnCount() {
		// Date, author, comment and version
		return NUM_COLUMNS;
	}

	@Override
	public String getTopHeaderLabel(final int col) {
		switch (col) {
		case DATE_COLUMN:
			return "Date";
		case AUTHOR_COLUMN:
			return "Author";
		case COMMENT_COLUMN:
			return "Comment";
		case VERSION_COLUMN:
			return "Version";
		default:
			return super.getTopHeaderLabel(col);
		}
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	@Override
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	@Override
	public int getInitialColumnWidth(int column) {
		switch (column) {
			case AUTHOR_COLUMN:
				return 25;
			case VERSION_COLUMN:
				return 10;
			case DATE_COLUMN:
				return 15;
			case COMMENT_COLUMN:
				return 50;
			default:
				return 0;
		}
	}
}
