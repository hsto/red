package dk.dtu.imm.red.core.ui.element.wizards.impl;

import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;
import dk.dtu.imm.red.core.element.QuantityLabel;
import dk.dtu.imm.red.core.ui.element.wizards.CreateQuantityItemWizardPresenter;

public class CreateQuantityItemWizardPresenterImpl
		extends	BaseWizardPresenter  
		implements CreateQuantityItemWizardPresenter {

	protected QuantityLabel selectedLabel;
	@Override
	public void wizardFinished() {
		// TODO Auto-generated method stub

	}

	@Override
	public QuantityLabel getselectedLabel() {
		return selectedLabel;
	}

	@Override
	public void setSelectedLabel(QuantityLabel selectedLabel) {
		this.selectedLabel = selectedLabel;
		
	}

}
