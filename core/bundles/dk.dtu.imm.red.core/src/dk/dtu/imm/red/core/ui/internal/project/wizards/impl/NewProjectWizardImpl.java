package dk.dtu.imm.red.core.ui.internal.project.wizards.impl;

import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.project.wizards.NewProjectWizard;
import dk.dtu.imm.red.core.ui.internal.project.wizards.NewProjectWizardPresenter;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.ui.wizards.ChooseDirectoryLocation;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class NewProjectWizardImpl extends BaseNewWizard implements NewProjectWizard{

	protected DefineProjectPage defineProjectPage;
	protected NewProjectWizardPresenter presenter;
	//protected ChooseDirectoryLocation chooseDirectoryLocation; 
	public NewProjectWizardImpl() {
		super("Project", Project.class);
		presenter = new NewProjectWizardPresenterImpl(this);
		defineProjectPage = new DefineProjectPage(this);
		
	}
	
	@Override
	public void addPages() {
		addPage(defineProjectPage);
		addPage(chooseDirectoryLocation);
	}

	@Override
	public boolean performFinish() {
		
		String projectName = defineProjectPage.getProjectName();

		String projectDescription = defineProjectPage.getDescription();

		String direc = chooseDirectoryLocation.getDirectoryName();

		presenter.wizardFinished(projectName, projectDescription, direc);
		return true;
	}
	
	@Override
	public boolean canFinish() {
		return defineProjectPage.isPageComplete() 
				&& chooseDirectoryLocation.isPageComplete();
	}
}
