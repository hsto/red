package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.Cell;
import org.agilemore.agilegrid.ISelectionChangedListener;
import org.agilemore.agilegrid.SWTX;
import org.agilemore.agilegrid.SelectionChangedEvent;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.impl.FileImpl;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.project.ProjectDate;
import dk.dtu.imm.red.core.project.ProjectFactory;
import dk.dtu.imm.red.core.project.UserTableEntry;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.effortestimation.views.impl.EffortEstimationEditor;
import dk.dtu.imm.red.core.ui.internal.element.handlers.LoadFileHandler;
import dk.dtu.imm.red.core.ui.internal.element.operations.DeleteElementsOperation;
import dk.dtu.imm.red.core.ui.internal.file.handlers.CreateFileHandler;
import dk.dtu.imm.red.core.ui.internal.file.wizards.impl.CreateFileWizardImpl;
import dk.dtu.imm.red.core.ui.internal.project.editors.ProjectEditor;
import dk.dtu.imm.red.core.ui.internal.project.editors.ProjectEditorPresenter;
import dk.dtu.imm.red.core.ui.widgets.PropertySelection;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.ExtendedAgileGrid;
import dk.dtu.imm.red.core.usecasepoint.Factor;
import dk.dtu.imm.red.core.usecasepoint.ManagementFactor;
import dk.dtu.imm.red.core.usecasepoint.TechnologyFactor;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;
import dk.dtu.imm.red.core.usecasepoint.WeightedFactor;
import dk.dtu.imm.red.core.user.User;
import dk.dtu.imm.red.core.user.UserFactory;
import dk.dtu.imm.red.core.workspace.Workspace;

public class ProjectEditorImpl extends BaseEditor implements ProjectEditor, EffortEstimationEditor {



	protected ElementBasicInfoContainer basicInfoContainer;
	private int effortPageIndex;
	private UseCasePointsComposite useCasePointcomposite;
	protected int projectPageIndex;
	protected int fileInfopage;
	protected String selection;
	protected FileImpl fl;
	protected File file;
//	protected Label titleLabel;
//	protected Text titleText;
    protected LoadFileHandler lfh;
	protected AgileGrid userTable;
	protected UserTableContentProvider userTableContentProvider;
	protected UserTableLayoutAdviser userTableLayoutAdviser;
	protected CreateFileWizardImpl filewizard;
	protected CreateFileHandler filehandler;
	protected AgileGrid dateTable;
	protected ProjectDateTableContentProvider dateTableContentProvider;
	protected ProjectDateTableLayoutAdviser dateTableLayoutAdviser;
	protected AgileGrid FileInfoTable;
	protected List<UserTableEntry> users;
	protected List<ProjectDate> dates;
	protected Button addUserButton;
	protected Button deleteUserButton;
	protected Button deleteDateButton;
	protected Button addDateButton;
	protected Button addFileButton;
	protected Button RemoveFileButton;
	protected Button RefreshFileButton;
	protected Button RefreshButton;
	protected Button LoadButton;
	protected TableRow row;
	private ExtendedAgileGrid<WeightedFactor> eagManagementFactor;
	private ExtendedAgileGrid<WeightedFactor> eagTechnologyFactor;

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Project.html";
	}

	@Override
	public void doSave(IProgressMonitor monitor) {

		ProjectEditorPresenter presenter = (ProjectEditorPresenter) this.presenter;

		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		
//		presenter.setName(titleText.getText());
		presenter.setUsers(users);
		presenter.setDates(dates);

		super.doSave(monitor);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);

		presenter = new ProjectEditorPresenterImpl(this, (Project) element);
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void fillInitialData() {

		users = new ArrayList<UserTableEntry>();
		dates = new ArrayList<ProjectDate>();

		Project project = (Project) element;

		basicInfoContainer.fillInitialData(project, new String[]{});
		
		for (UserTableEntry entry : project.getUsers()) {
			users.add(EcoreUtil.copy(entry));
		}

		userTableContentProvider.setContent(users);
		userTableLayoutAdviser.setRowCount(users.size());
		userTable.redraw();

		for (ProjectDate date : project.getProjectDates()) {
			dates.add(EcoreUtil.copy(date));
		}

		dateTableContentProvider.setContent(dates);
		dateTableLayoutAdviser.setRowCount(dates.size());
		dateTable.redraw();

//		titleText.setText(project.getName());

		if(project.getEffort() != null) {
			loadEffortEstimation(project);
		} else {
			useCasePointcomposite.getLblDefaultUsecasePoint().setText(
					"Use Case Point has not been loaded for this project. Click the button to add default values");
		}

		isDirty = false;
		firePropertyChange(PROP_DIRTY);

	}

	private void loadEffortEstimation(Project project) {
		//Load Effort Estimation Page
		UseCasePointsComposite c =  useCasePointcomposite;

		//Load Management and Technology Factor
		ManagementFactor ma = project.getEffort().getManagementFactor();
		TechnologyFactor ta = project.getEffort().getTechnologyFactor();

		eagManagementFactor.getPresenter().getDefaultColumnDefinition();
		eagManagementFactor.getPresenter().setDataSource(ma.getWeightedFactors(), false);
		eagTechnologyFactor.getPresenter().getDefaultColumnDefinition();
		eagTechnologyFactor.getPresenter().setDataSource(ta.getWeightedFactors(), false);
		eagManagementFactor.setDataSource();
		eagTechnologyFactor.setDataSource();
		//Load Productivity Factor
		loadedSpinnerFactorWidget(project.getEffort().getProductivityFactor(), useCasePointcomposite.getSpProductivityFactor());

		//Load Cost Factor
		useCasePointcomposite.getPropertycostPerUsecasePoint()
		.setModifyListener(modifyListener)
		.fillData(project.getEffort().getCostPerUsecasePoint());

		loadPropertyListener(useCasePointcomposite.getPropertycostPerUsecasePoint());
		refreshUsecasePointValues();
	}

	private void loadedSpinnerFactorWidget(final Factor f, final Spinner spValue) {

		final ProjectEditorPresenter presenter = (ProjectEditorPresenter) this.presenter;

		//Add Weight value
		spValue.setSelection((int) f.getSum() * 100);

		//Create event handler
		spValue.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				double val =  ((double) spValue.getSelection()) / 100.0;
				presenter.setFactorValue(f, val);
				refreshUsecasePointValues();
				modifyListener.handleEvent(null);
			}
		});
	}

	private void loadPropertyListener(final PropertySelection propertySelection) {

		//Create event handler
		propertySelection.getSpinner().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				refreshUsecasePointValues();
			}
		});

		//Create event handler
		propertySelection.getCombo().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				refreshUsecasePointValues();
			}
		});
	}

	private void refreshUsecasePointValues() {
		Project project = (Project) element;
		UsecasePoint ef = project.getEffort();
		UseCasePointsComposite c =  useCasePointcomposite;
	}

	@Override
	protected void createPages() {
		projectPageIndex = addPage(createProjectPage(getContainer()));
		setPageText(projectPageIndex, "Summary");

		fileInfopage = addPage(createFileInfo(getContainer()));
		setPageText(fileInfopage, "File");

		effortPageIndex = addPage(createEffortPage(getContainer()));
		setPageText(effortPageIndex, "Project Settings");

		super.createPages();

		fillInitialData();
	}

	private Composite createProjectPage(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(1, true);
		composite.setLayout(layout);

		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);
		
//		titleLabel = new Label(composite, SWT.NONE);
//		titleLabel.setText("Project title");
//
//		titleText = new Text(composite, SWT.BORDER);
//		GridData titleLayoutData = new GridData(100, SWT.DEFAULT);
//		titleText.setLayoutData(titleLayoutData);
//		titleText.addListener(SWT.Modify, modifyListener);

		Group userGroup = new Group(composite, SWT.NONE);
		GridData userLayoutData = new GridData(SWT.FILL, SWT.BEGINNING, true, false);
		userLayoutData.heightHint = 300;
		userGroup.setLayoutData(userLayoutData);
		userGroup.setText("Project Team");
		userGroup.setLayout(new GridLayout(2, false));
		userTable = new AgileGrid(userGroup, SWTX.ROW_SELECTION | SWT.V_SCROLL);
		userTableContentProvider = new UserTableContentProvider(this);
		userTableLayoutAdviser = new UserTableLayoutAdviser(userTable);
		userTable.setContentProvider(userTableContentProvider);
		userTable.setLayoutAdvisor(userTableLayoutAdviser);
		userTable.setCellEditorProvider(new TableCellEditorProvider(userTable));
		GridData userTableLayoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		userTableLayoutData.horizontalSpan = 2;
		userTable.setLayoutData(userTableLayoutData);
		userTable.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				if (userTable.getCellSelection().length > 0) {
					deleteUserButton.setEnabled(true);
				} else {
					deleteUserButton.setEnabled(false);
				}
			}
		});

		addUserButton = new Button(userGroup, SWT.NONE);
		addUserButton.setText("Add Member");
		addUserButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				User user = UserFactory.eINSTANCE.createUser();
				UserTableEntry entry = ProjectFactory.eINSTANCE.createUserTableEntry();
				entry.setUser(user);
				users.add(entry);
				userTableLayoutAdviser.setRowCount(users.size());
				userTable.redraw();
				markAsDirty();
			}
		});
		deleteUserButton = new Button(userGroup, SWT.NONE);
		deleteUserButton.setText("Delete Member");
		deleteUserButton.setEnabled(false);
		deleteUserButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Cell[] selectedCells = userTable.getCellSelection();
				Set<UserTableEntry> selectedUsers = new HashSet<UserTableEntry>();

				for (Cell cell : selectedCells) {
					selectedUsers.add(
							users.get(cell.row));
				}

				users.removeAll(selectedUsers);

				userTableLayoutAdviser.setRowCount(
						users.size());
				userTable.clearSelection();
				deleteUserButton.setEnabled(false);
				userTable.redraw();
				markAsDirty();
			}
		});

		Group dateGroup = new Group(composite, SWT.NONE);
		GridData dateLayoutData = new GridData(SWT.FILL, SWT.BEGINNING, true, false);
		dateLayoutData.heightHint = 200;
		dateGroup.setLayoutData(dateLayoutData);
		dateGroup.setText("Project dates");
		dateGroup.setLayout(new GridLayout(2, false));
		dateTable = new AgileGrid(dateGroup, SWTX.ROW_SELECTION | SWT.V_SCROLL);
		dateTableContentProvider = new ProjectDateTableContentProvider(this);
		dateTableLayoutAdviser = new ProjectDateTableLayoutAdviser(userTable);
		dateTable.setContentProvider(dateTableContentProvider);
		dateTable.setLayoutAdvisor(dateTableLayoutAdviser);
		dateTable.setCellEditorProvider(new TableCellEditorProvider(dateTable));
		GridData dateTableLayoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		dateTableLayoutData.horizontalSpan = 2;
		dateTable.setLayoutData(dateTableLayoutData);

		dateTable.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				if (dateTable.getCellSelection().length > 0) {
					deleteDateButton.setEnabled(true);
				} else {
					deleteDateButton.setEnabled(false);
				}
			}
		});

		addDateButton = new Button(dateGroup, SWT.NONE);
		addDateButton.setText("Add Date");
		addDateButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				ProjectDate date = ProjectFactory.eINSTANCE.createProjectDate();
				dates.add(date);
				dateTableLayoutAdviser.setRowCount(dates.size());
				dateTable.redraw();
				markAsDirty();
			}
		});
		deleteDateButton = new Button(dateGroup, SWT.NONE);
		deleteDateButton.setText("Delete Date");
		deleteDateButton.setEnabled(false);
		deleteDateButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Cell[] selectedCells = dateTable.getCellSelection();
				Set<ProjectDate> selectedDates = new HashSet<ProjectDate>();

				for (Cell cell : selectedCells) {
					selectedDates.add(
							dates.get(cell.row));
				}

				dates.removeAll(selectedDates);

				dateTableLayoutAdviser.setRowCount(
						dates.size());
				dateTable.clearSelection();
				deleteDateButton.setEnabled(false);
				dateTable.redraw();
				markAsDirty();
			}
		});

		return composite;
	}

	private Composite createEffortPage(Composite parent){
		useCasePointcomposite = new UseCasePointsComposite(parent, SWT.NONE, this);

		eagManagementFactor = new ExtendedAgileGrid<WeightedFactor>(useCasePointcomposite.getGrpManagementFactor(), SWT.NONE, modifyListener);
		eagManagementFactor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		eagManagementFactor.setPresenter(new EAGPresenter<WeightedFactor>());
		eagManagementFactor.getPresenter().setReflectedEClassByName("dk.dtu.imm.red.core.usecasepoint", "WeightedFactor");

		eagTechnologyFactor = new ExtendedAgileGrid<WeightedFactor>(useCasePointcomposite.getGrpGrptechnologyfactor(), SWT.NONE, modifyListener);
		eagTechnologyFactor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		eagTechnologyFactor.setPresenter(new EAGPresenter<WeightedFactor>());
		eagTechnologyFactor.getPresenter().setReflectedEClassByName("dk.dtu.imm.red.core.usecasepoint", "WeightedFactor");

		return SWTUtils.wrapInScrolledComposite(parent, useCasePointcomposite);
	}
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		CreateFileWizardImpl wizard = new CreateFileWizardImpl();
		ISelection currentSelection = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection();
		wizard.init(PlatformUI.getWorkbench(),
				(IStructuredSelection) currentSelection);
		WizardDialog dialog = new WizardDialog(Display.getCurrent()
				.getActiveShell(), wizard);

		dialog.create();
		dialog.open();
		refreshTable();

		return null;
	}

	public static void refreshTable() {
		try {
			ModelProvide.getInstance().UpdateAllFilesInfo();
			table.refresh();
		} catch (Exception e) {
			return;
		}
	}

	private Object execute() throws ExecutionException {

		return execute(null);
	}

	static TableViewer table;

	private Composite createFileInfo(final Composite parent) {
		final Group tcContainer = new Group(parent, SWT.NONE);
		tcContainer.setText("Files In Project");

		GridLayout tcLayout = new GridLayout(1, false);
		tcContainer.setLayout(tcLayout);

		table = new TableViewer(tcContainer, SWT.MULTI | SWT.BORDER
				| SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION);
		Composite buttonContainer = new Composite(tcContainer, SWT.NONE);
		buttonContainer.setLayout(new RowLayout(SWT.HORIZONTAL));
		GridData buttonContainerGridData = new GridData();
		buttonContainerGridData.horizontalSpan = 2;
		buttonContainer.setLayoutData(buttonContainerGridData);

		GridData labelLayoutData = new GridData();
		Label labelLayoutDat = new Label(tcContainer, SWT.NONE);
		labelLayoutData.horizontalSpan = 1;
		labelLayoutData.grabExcessHorizontalSpace = true;
		labelLayoutData.horizontalAlignment = SWT.FILL;
		labelLayoutDat.setText("References");
		labelLayoutDat.setLayoutData(labelLayoutData);


		final org.eclipse.swt.widgets.List ListBox = new org.eclipse.swt.widgets.List(
				tcContainer, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.WRAP);

		GridData textLayoutData = new GridData();
		textLayoutData.verticalSpan = 2;
		textLayoutData.grabExcessVerticalSpace = true;
		textLayoutData.grabExcessHorizontalSpace = true;
		textLayoutData.horizontalAlignment = 30;
		textLayoutData.verticalAlignment = 130;
		textLayoutData.widthHint = 150;
		textLayoutData.heightHint = 240;
		ListBox.setLayoutData(textLayoutData);

		Composite buttonContainer2 = new Composite(tcContainer, SWT.NONE);
		buttonContainer2.setLayout(new RowLayout(SWT.HORIZONTAL));
		GridData buttonContainer2GridData = textLayoutData;
		buttonContainer2GridData.horizontalSpan = 1;
		buttonContainer2.setLayoutData(buttonContainer2GridData);

		table.getTable().setHeaderVisible(true);
		table.getTable().setLinesVisible(true);
		table.getTable().setSize(400, 400);

		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = SWT.FILL;
		data.heightHint = 300;
		data.widthHint = 600;
		data.horizontalSpan = 2;
		table.getTable().setLayoutData(data);

		final TableViewerColumn FileName = new TableViewerColumn(table,
				SWT.NONE);

		final TableColumn column = FileName.getColumn();
		column.setText("FileName");
		column.setWidth(150);
		column.setResizable(true);
		FileName.setLabelProvider(new TrimLabelProvider());

		final TableViewerColumn FilePath = new TableViewerColumn(table,
				SWT.NONE);
		final TableColumn columnPath = FilePath.getColumn();
		columnPath.setText("Path");
		columnPath.setWidth(250);
		columnPath.setResizable(true);
		FilePath.setLabelProvider(new TrimLabelProvider());

		final TableViewerColumn columnDiscription = new TableViewerColumn(
				table, SWT.NONE);
		columnDiscription.getColumn().setText("Description");
		columnDiscription.getColumn().setWidth(150);
		columnDiscription.getColumn().setResizable(true);
		columnDiscription.setLabelProvider(new ColumnLabelProvider() {
			public String getText(Object element) {
				TableRow p = (TableRow) element;

				return p.getDescription();
			}

			@Override
			public void update(ViewerCell cell) {
				cell.setText(((TableRow) cell.getElement()).getDescription());

			}
		});

		columnDiscription
				.setEditingSupport(new DescriptionEditingSupport(table));

		final TableViewerColumn columnLock = new TableViewerColumn(table,
				SWT.NONE);
		columnLock.getColumn().setText("Status");
		columnLock.getColumn().setWidth(150);
		columnLock.getColumn().setResizable(true);
		columnLock.setLabelProvider(new TrimLabelProvider());


		table.setContentProvider(new ArrayContentProvider());
		table.setInput(ModelProvide.getInstance().GetProjectFiles());
		table.getControl().setFocus();

		table.getTable().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {

				int selected = table.getTable().getSelectionIndex();

				selection = table.getTable().getItem(selected).getText();
				java.io.File fi = new java.io.File(filesListData.path
						+ selection);
				if (!fi.exists())
					return;
				LoadFileHandler lfh = new LoadFileHandler();
				lfh.list_References.clear();
				lfh.FilesReferences(selection, filesListData.path);
				for (int j = 0; j < lfh.list_References.size(); j++) {
					String f = lfh.list_References.get(j);
					if (f.contains(selection)) {

						lfh.list_References.remove(selection);
					}
				}
				ListBox.removeAll();

				for (int i = 0, n = lfh.list_References.size(); i < n; i++) {

					ListBox.add(lfh.list_References.get(i));

				}
			}
		});
//		Composite buttonContainer = new Composite(tcContainer, SWT.NONE);
//		buttonContainer.setLayout(new RowLayout(SWT.HORIZONTAL));
//		GridData buttonContainerGridData = new GridData();
//		buttonContainerGridData.horizontalSpan = 2;
//		buttonContainer.setLayoutData(buttonContainerGridData);

		addFileButton = new Button(buttonContainer, SWT.PUSH);
		addFileButton.setText("New File");

		addFileButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				try {
					execute();

				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace(); LogUtil.logError(e);
				}

			}
		});

		RemoveFileButton = new Button(buttonContainer, SWT.NONE);
		RemoveFileButton.setText("Unload");
		RemoveFileButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {

				try {
					execute2();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace(); LogUtil.logError(e);
				}

			}
		});
		RefreshFileButton = new Button(buttonContainer, SWT.NONE);
		RefreshFileButton.setText("Refresh");
		RefreshFileButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				table.refresh();
			}
		});
		LoadButton = new Button(buttonContainer, SWT.NONE);
		LoadButton.setText("Load File");
		LoadButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				try {
					int selected = table.getTable().getSelectionIndex();
					String selection = table.getTable().getItem(selected)
							.getText();
					LoadFileHandler.LoadFiles(selection, filesListData.path);
				} catch (Exception e1) {
					return;
				}
				table.refresh();
			}
		});
		Button LoadReferencesButton = new Button(buttonContainer2, SWT.NONE);
		LoadReferencesButton.setText("Load References");
		LoadReferencesButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				try {

					String[] selection = ListBox.getSelection();

					LoadFileHandler lfh = new LoadFileHandler();
					lfh.list_References.clear();
					for (int i = 0; i < selection.length; i++) {
						lfh.FilesReferences(selection[i], filesListData.path);
					}
					if (selection.length > 0)
						for (int x = 0; x < selection.length; x++) {
							lfh.LoadFiles(selection[x], filesListData.path);
						}
					else {
						return;
					}
					if (lfh.list_References.size() > selection.length) {
						MessageDialog md = new MessageDialog(Display
								.getCurrent().getActiveShell(), "Load Refernces", null,
								"The selected file has refernces to other files in this project, would you like to load them?", MessageDialog.QUESTION,
								new String[] { "Yes", "No" }, 1);
						int response = md.open();
						if (response == md.OK) {
							int[] arr = new int[selection.length];
							for (int z = 0; z < selection.length; z++) {
								int fileIndex = lfh.list_References
										.indexOf(selection[z]);
								arr[z] = fileIndex;
							}
							for (int j = 0; j < arr.length; j++) {
								lfh.list_References.remove(arr[j]);
							}

							lfh.ListUi(lfh.list_References, filesListData.path);
						}
					}

				} catch (Exception e) { // TODO Auto-generated catch block
					e.printStackTrace(); LogUtil.logError(e);
				}
				markAsDirty();
				ListBox.update();
			}
		});

		return tcContainer;
	}

	private Object execute2() throws ExecutionException {

		return execute2(null);
	}

public Object execute2(final ExecutionEvent event)throws ExecutionException {
		ISelection selectedObjects;
		selectedObjects= (ISelection) table.getSelection();
		if (selectedObjects instanceof IStructuredSelection) {
		List<Element> selectedElements = new ArrayList<Element>();
		Object f=((IStructuredSelection) selectedObjects).getFirstElement();
		TableRow r = new TableRow(null);
		r = (TableRow) f;
		selectedElements.add(r.file);
		DeleteElementsOperation operation =
				new DeleteElementsOperation(selectedElements);
		operation.addContext(PlatformUI.getWorkbench().getOperationSupport()
				.getUndoContext());
		try {
			OperationHistoryFactory.getOperationHistory()
				.execute(operation, null, null);
		} catch (org.eclipse.core.commands.ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

	}

	return null;
}


	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();

		// put all the attributes which we want to search through
		// into the attributesToSearchMap
		//TODO projects attributes
		super.addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);

		// Now, search through all attributes, recording all search hits along the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()-1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(), result.getEndIndex());
			}
		}

		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {
			return null;
		} else {
			return wrapper;
		}
	}
	
	@Override
	public void selectText(EStructuralFeature feature, int startIndex, int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,projectPageIndex,basicInfoContainer);
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.core.project.projecteditor";
	}

	@Override
	public void createDefaultUsecasePoint() {
		 ((ProjectEditorPresenter)presenter).createDefaultUsecasePoint();
		 loadEffortEstimation((Project) element);
		 modifyListener.handleEvent(null);
	}

	@Override
	public List<Element> getEffortEstimationContent() {
		Workspace projectWorkspace = (Workspace) element.eContainer();
 		return projectWorkspace.getContents();
	}

}
