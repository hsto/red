package dk.dtu.imm.red.core.ui.internal.element.document.editors;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;

public interface DocumentEditorPresenter extends IEditorPresenter{
	
	public void setDocumentAbstract(String documentAbstract);
	
	public void setDocumentInternalRef(Element element);
	
	public void setDocumentExternalRef(String reference);
	
	public void setDocumentWebRef(String reference);
	
	public void setDocumentTextContent(String documentContent);
	
	public void setImageList(ArrayList<Image> imageList);
	
	public void setCaptionList(ArrayList<String> captionList);
}
