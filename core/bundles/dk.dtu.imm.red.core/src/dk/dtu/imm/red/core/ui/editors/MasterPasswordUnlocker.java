package dk.dtu.imm.red.core.ui.editors;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.element.impl.VisitorImpl;

public class MasterPasswordUnlocker extends VisitorImpl {

	@Override
	public void visit(Object object) {
		// TODO Auto-generated method stub
		if(object instanceof Element){
			Element element = (Element) object;
			if(element.getLockPassword()!=null && !element.getLockPassword().isEmpty()){
				element.setLockStatus(LockType.LOCK_NOTHING);
				element.setLockPassword("");
				element.save();
			}
		}
		
	}

}
