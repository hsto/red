package dk.dtu.imm.red.core.ui.internal.project.editors;

import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface ProjectEditor extends IEditorPart, IBaseEditor{

	public static final String ID = "dk.dtu.imm.red.core.project.projecteditor";
	void createDefaultUsecasePoint();
}
