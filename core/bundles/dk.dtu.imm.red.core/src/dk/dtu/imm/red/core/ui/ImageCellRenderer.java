package dk.dtu.imm.red.core.ui;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.renderers.AbstractCellRenderer;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;


public class ImageCellRenderer extends AbstractCellRenderer {

	public ImageCellRenderer(AgileGrid agileGrid) {
		super(agileGrid);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.peertoo.agilegrid.renderers.DefaultCellRenderer#doDrawCellContent(org.eclipse.swt.graphics.GC,
	 *      org.eclipse.swt.graphics.Rectangle, int, int)
	 */
	@Override
	protected void doDrawCellContent(GC gc, Rectangle rect, int row, int col) {
		Object object = agileGrid.getContentAt(row, col);
		if (object instanceof Image) {
			Image image = (Image) object;
			Rectangle imageRect = image.getBounds();
			gc.drawImage(image, imageRect.x, imageRect.y, imageRect.width,
					imageRect.height, rect.x, rect.y, imageRect.width, imageRect.height);
		}
	}
}