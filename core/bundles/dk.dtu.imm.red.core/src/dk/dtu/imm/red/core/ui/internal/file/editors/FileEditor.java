package dk.dtu.imm.red.core.ui.internal.file.editors;

import org.eclipse.ui.IEditorPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface FileEditor extends IEditorPart, IBaseEditor{


		public static final String ID = "dk.dtu.imm.red.core.file.fileeditor";
	}


