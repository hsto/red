package dk.dtu.imm.red.core.ui.internal.traceability.util;

import java.util.Queue;
import java.util.Set;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;

/**
 * 
 * @author Sebastian Dittmann
 *
 */

public class Configuration {

	private Set<RelationshipKind> relationshipKindFilter;
	private Set<String> elementKindFilter;
	private int depthLimit;
	private boolean reverseRelationshipFilter;
	private boolean reverseElementFilter;
	private boolean join;
	private boolean start;

	// Hide from usage
	private Configuration() {
	}

	public Configuration(Set<RelationshipKind> relationshipKindFilter, Set<String> elementKindFilter, int depthLimit,
			boolean reverseRelationshipFilter, boolean reverseElementFilter, boolean join, boolean start) {
		super();
		this.relationshipKindFilter = relationshipKindFilter;
		this.elementKindFilter = elementKindFilter;
		this.depthLimit = depthLimit;
		this.reverseRelationshipFilter = reverseRelationshipFilter;
		this.reverseElementFilter = reverseElementFilter;
		this.join = join;
		this.start = start;
	}

	public Set<RelationshipKind> getRelationshipKindFilter() {
		return relationshipKindFilter;
	}

	public void setRelationshipKindFilter(Set<RelationshipKind> relationshipKindFilter) {
		this.relationshipKindFilter = relationshipKindFilter;
	}

	public Set<String> getElementKindFilter() {
		return elementKindFilter;
	}

	public void setElementKindFilter(Set<String> elementKindFilter) {
		this.elementKindFilter = elementKindFilter;
	}

	public int getDepthLimit() {
		if (depthLimit >= 0)
			return depthLimit;
		else
			return -1;
	}

	public void setDepthLimit(int depthLimit) {
		this.depthLimit = depthLimit;
	}

	public boolean isReverseElementFilter() {
		return reverseElementFilter;
	}

	public void setReverseElementFilter(boolean reverse) {
		this.reverseElementFilter = reverse;
	}

	public boolean isReverseRelationshipFilter() {
		return reverseRelationshipFilter;
	}

	public void setReverseRelationshipFilter(boolean reverse) {
		this.reverseRelationshipFilter = reverse;
	}

	public boolean isJoin() {
		return join;
	}

	public void setJoin(boolean join) {
		this.join = join;
	}

	public boolean isStart() {
		return start;
	}

	public void setStart(boolean start) {
		this.start = start;
	}

}
