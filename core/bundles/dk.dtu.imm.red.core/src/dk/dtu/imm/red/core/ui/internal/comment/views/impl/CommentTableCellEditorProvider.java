package dk.dtu.imm.red.core.ui.internal.comment.views.impl;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.CellEditor;
import org.agilemore.agilegrid.DefaultCellEditorProvider;
import org.agilemore.agilegrid.editors.ComboBoxCellEditor;
import org.eclipse.swt.SWT;

import dk.dtu.imm.red.core.comment.CommentCategory;
import dk.dtu.imm.red.core.comment.IssueComment;
import dk.dtu.imm.red.core.ui.AgileTableTextCellEditor;

public class CommentTableCellEditorProvider extends DefaultCellEditorProvider {
	
	private String[] options;
	private CommentViewImpl commentView;

	public CommentTableCellEditorProvider(AgileGrid agileGrid, CommentViewImpl commentView) {
		super(agileGrid);
		
		this.commentView = commentView;
		
		options = new String[CommentCategory.values().length];
		
		for (int i = 0; i < options.length; i++) {
			options[i] = CommentCategory.get(i).getLiteral();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.agilemore.agilegrid.DefaultCellEditorProvider#canEdit(int, int)
	 */
	@Override
	public boolean canEdit(int row, int col) {
		
		if (commentView.commentList.getComments().get(row) instanceof IssueComment) {
			return false;
		}
		
		if (commentView.isLocked()) {
			return false;
		}
		
		if (col == CommentTableLayoutAdviser.DATE_COLUMN) {
			return false;
		}
		
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.agilemore.agilegrid.DefaultCellEditorProvider#getCellEditor(int, int)
	 */
	@Override
	public CellEditor getCellEditor(int row, int col) {
		if (col == CommentTableLayoutAdviser.STATUS_COLUMN) {
			ComboBoxCellEditor editor = new ComboBoxCellEditor(this.agileGrid,
					SWT.READ_ONLY);
			editor.setItems(options);
			return editor;
		} else {
			return new AgileTableTextCellEditor(agileGrid);
		}
	}
	
	

}
