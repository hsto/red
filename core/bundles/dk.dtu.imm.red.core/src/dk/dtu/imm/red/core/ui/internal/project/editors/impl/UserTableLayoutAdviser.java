package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ScalableColumnLayoutAdvisor;

public class UserTableLayoutAdviser extends ScalableColumnLayoutAdvisor{


	private static final int NAME_COLUMN = 0;
	private static final int ID_COLUMN = 1;
	private static final int EMAIL_COLUMN = 2;
	private static final int PHONE_COLUMN = 3;
	private static final int SKYPE_COLUMN = 4;
	private static final int COMMENT_COLUMN = 5;
	private static final int NUM_COLUMNS = 6;
	private int rowCount;
	
	public UserTableLayoutAdviser(AgileGrid agileGrid) {
		super(agileGrid);

		rowCount = 0;
	}
	
	@Override
	public int getColumnCount() {
		return NUM_COLUMNS;
	}

	@Override
	public String getTopHeaderLabel(final int col) {
		switch (col) {
		case NAME_COLUMN:
			return "Name";
		case ID_COLUMN:
			return "ID";
		case EMAIL_COLUMN:
			return "E-Mail";
		case PHONE_COLUMN:
			return "Phone";
		case SKYPE_COLUMN:
			return "Skype";
		case COMMENT_COLUMN:
			return "Comment";
		default:
			return super.getTopHeaderLabel(col);
		}
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	@Override
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	@Override
	public int getInitialColumnWidth(int column) {
		switch (column) {
			case NAME_COLUMN:
				return 15;
			case ID_COLUMN:
				return 10;
			case EMAIL_COLUMN:
				return 15;
			case PHONE_COLUMN:
				return 10;
			case SKYPE_COLUMN:
				return 10;
			case COMMENT_COLUMN:
				return 40;
			default:
				return 0;
		}
	}
}
