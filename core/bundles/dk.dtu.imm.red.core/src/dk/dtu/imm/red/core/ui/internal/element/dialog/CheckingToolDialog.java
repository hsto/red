package dk.dtu.imm.red.core.ui.internal.element.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.IssueComment;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;


public class CheckingToolDialog extends Dialog {
	
	private CheckingToolEAGComposite compComments;
	private CheckingToolEAGPresenter presenter;

	public CheckingToolDialog(Shell parentShell) {
		super(parentShell);
		
		presenter = new CheckingToolEAGPresenter();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, false));
		
		//Issue table
		compComments = new CheckingToolEAGComposite(container, SWT.NONE);
		compComments.setPresenter(getCommentsPresenter());
		compComments.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		compComments.setColumns(getCommentsColumnDefenitions())
			.setDataSource(getAllComments(WorkspaceFactory.eINSTANCE.getWorkspaceInstance()));
		
		//Issue control buttons
		SashForm buttonSash = new SashForm(container, SWT.HORIZONTAL);
		
		Button checkAllButton = new Button(buttonSash, SWT.NONE);
		checkAllButton.setText("Check all");
		checkAllButton.setToolTipText("Run all checks on all elements.");
		final CheckResultBubble resultBubble = new CheckResultBubble(checkAllButton, getWorkspace());
		checkAllButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkAll();
				resultBubble.show();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				checkAll();
				resultBubble.show();
			}
		});
		
		Button clearAllButton = new Button(buttonSash, SWT.NONE);
		clearAllButton.setText("Clear all");
		clearAllButton.setToolTipText("Clear all issue messages from all elements.");
		clearAllButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearAll();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				clearAll();
			}
		});

		return container;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Checking tool");
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(900, 600);
	}
	
	private void checkAll() {
		getWorkspace().checkChildren();
		
		compComments.setDataSource(getAllComments(getWorkspace()));
		compComments.update();
	}
	
	private void clearAll() {
		getWorkspace().clearIssues();
		
		compComments.setDataSource(getAllComments(getWorkspace()));
		compComments.update();
	}
	
	private Workspace getWorkspace() {
		return WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
	}
	
	private EAGPresenter<IssueComment> getCommentsPresenter() {
		return presenter;
	}
	
	private ElementColumnDefinition[] getCommentsColumnDefenitions() {
		return new ElementColumnDefinition[] { 
				//Name column
				new ElementColumnDefinition()
				.setHeaderName("Name")
				.setColumnType(String.class)
				.setColumnWidth(35)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"eContainer", "eContainer", "getName"})
				.setReferenceGetterMethod(new String[] {"eContainer", "eContainer"}),
				
				//Type column
				new ElementColumnDefinition()
				.setHeaderName("Element type")
				.setColumnType(String.class)
				.setColumnWidth(20)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setCellHandler(new CellHandler<IssueComment>() {

					@Override
					public Object getAttributeValue(IssueComment item) {
						if (item.eContainer() != null
								&& item.eContainer().eContainer() != null
								) {
							return item.eContainer().eContainer().eClass().getName();
						}
						else {
							return "";
						}
					}

					@Override
					public void setAttributeValue(IssueComment item, Object value) {
						//This column cannot be edited
					}
				}),
				
				//Description column
				new ElementColumnDefinition()
				.setHeaderName("Description")
				.setColumnType(String.class)
				.setEditable(false)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getText"})
				.setReferenceGetterMethod(new String[] {"eContainer", "eContainer"}),
				
				//Severity column
				new ElementColumnDefinition()
				.setHeaderName("Severity")
				.setColumnType(String.class)
				.setColumnWidth(15)
				.setEditable(false)
				.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
				.setGetterMethod(new String[] {"getSeverity"})
				.setReferenceGetterMethod(new String[] {"eContainer", "eContainer"})
				 };
	}
	
	private List<IssueComment> getAllComments(Element root) {
		List<IssueComment> results = new ArrayList<IssueComment>();
		
		for (Comment comment : root.getCommentlist().getComments()) {
			if (comment instanceof IssueComment) {
				results.add((IssueComment) comment);
			}
		}
		
		if (root instanceof Group) {
			Group group = (Group) root;
			
			for (Element child : group.getContents()) {
				results.addAll(getAllComments(child));
			}
		}
		
		return results;
	}
}
