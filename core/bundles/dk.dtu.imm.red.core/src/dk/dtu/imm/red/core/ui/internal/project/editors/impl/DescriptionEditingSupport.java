/*
 * @author Ahmed Fikry
 */
package dk.dtu.imm.red.core.ui.internal.project.editors.impl;
import java.beans.PropertyChangeListener;
//import org.eclipse.jface.viewers.CellEditor;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;



public class DescriptionEditingSupport extends EditingSupport {

  private final TableViewer viewer;
  private final CellEditor editor;

  public DescriptionEditingSupport(TableViewer viewer) {
    super(viewer);
    this.viewer = viewer;
    this.editor = new TextCellEditor(viewer.getTable());
  }

  @Override
  protected CellEditor getCellEditor(Object element) {
    return editor;
  }

  @Override
  protected boolean canEdit(Object element) {
    return true;
  }

  @Override
  protected Object getValue(Object element) {
    return ((TableRow) element).getDescription();
  }

  @Override
  protected void setValue(Object element, Object value) 
  {
    ((TableRow) element).setDescription(String.valueOf(value));
    
    viewer.update(element, null);
    
  }
} 
