package dk.dtu.imm.red.core.ui.internal.folder.wizards;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;

public interface CreateFolderWizardPresenter extends IWizardPresenter {
	
	void wizardFinished(String name, Group parent, String path);

}
