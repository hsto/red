package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ScalableColumnLayoutAdvisor;

public class ProjectDateTableLayoutAdviser extends ScalableColumnLayoutAdvisor{


	private static final int DATE_COLUMN = 0;
	private static final int COMMENT_COLUMN = 1;
	private static final int NUM_COLUMNS = 2;
	private int rowCount;
	
	public ProjectDateTableLayoutAdviser(AgileGrid agileGrid) {
		super(agileGrid);

		rowCount = 0;
	}
	
	@Override
	public int getColumnCount() {
		// Date and comment
		return NUM_COLUMNS;
	}

	@Override
	public String getTopHeaderLabel(final int col) {
		switch (col) {
		case DATE_COLUMN:
			return "Date";
		case COMMENT_COLUMN:
			return "Comment";
		default:
			return super.getTopHeaderLabel(col);
		}
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	@Override
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	@Override
	public int getInitialColumnWidth(int column) {
		switch (column) {
			case DATE_COLUMN:
				return 15;
			case COMMENT_COLUMN:
				return 85;
			default:
				return 0;
		}
	}
}
