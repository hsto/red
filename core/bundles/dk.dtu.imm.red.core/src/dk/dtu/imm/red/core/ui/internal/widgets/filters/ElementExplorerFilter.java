package dk.dtu.imm.red.core.ui.internal.widgets.filters;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;

public class ElementExplorerFilter extends ViewerFilter {
	
	protected String filter = "";

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (element instanceof Group) {
			if (((Group) element).getName() == null) {
				((Group) element).setName("");
			}
			
			if (((Group) element).getName().toLowerCase().contains(filter.toLowerCase())) {
				return true;
			}
			
			for (Element child : ((Group) element).getContents()) {
				if (select(viewer, element, child)) {
					return true;
				}
			}
			return false;
		} 
		else if (element instanceof Element) {
			if (((Element) element).getName() != null) {
				return ((Element) element).getName().contains(filter);
			}
		}
		return true;
	}
	
	public void setFilterString(String filter) {
		this.filter = filter;
	}
}
