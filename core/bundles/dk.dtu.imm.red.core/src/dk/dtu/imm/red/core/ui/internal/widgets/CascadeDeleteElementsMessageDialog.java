package dk.dtu.imm.red.core.ui.internal.widgets;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class CascadeDeleteElementsMessageDialog extends MessageDialog {

	protected boolean isInlikedSelected = false;
	
	public CascadeDeleteElementsMessageDialog(Shell parentShell, String dialogTitle, Image dialogTitleImage, String dialogMessage, int dialogImageType, String[] dialogButtonLabels, int defaultIndex) {
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, dialogButtonLabels, defaultIndex);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
		Button unlinkCheckBox = new Button(container, SWT.CHECK);
		unlinkCheckBox.setText("Unlink before proceeding");
		unlinkCheckBox.setSelection(isInlikedSelected);
		unlinkCheckBox.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				super.widgetSelected(e);
				isInlikedSelected = !isInlikedSelected;
			}
			
		});
		
		
		return container;

	}
	
	public boolean isInlikedSelected() {
		return isInlikedSelected;
	}

}
