/*
 * @author Ahmed Fikry
 */
package dk.dtu.imm.red.core.ui.internal.file.editors;

import dk.dtu.imm.red.core.file.File;

import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;


public class FileEditorInput extends BaseEditorInput {
	
	public FileEditorInput(File file) {
		super(file);
	}

	public boolean exists() {
		return false;
	}

	public String getName() {
		return "File editor";
	}

	public String getToolTipText() {
		return "This is the file editor";
	}
}

