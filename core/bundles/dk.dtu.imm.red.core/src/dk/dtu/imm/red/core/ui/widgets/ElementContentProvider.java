package dk.dtu.imm.red.core.ui.widgets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.workspace.Workspace;

public class ElementContentProvider implements ITreeContentProvider {

	protected boolean showGroupsOnly;
	
	protected Class<Element>[] typesShown;
	protected Class<Element>[] typesExcluded;
	protected Element[] excludedElements;
    protected Project project;
    protected File file;
	protected boolean showFiles;
	protected boolean showProjects;
	/**
	 * Create a new element content provider. The default behavior is to provide
	 * all elements.
	 */
	public ElementContentProvider() {
		this(false, null);
	}
	
	/**
	 * Create a new element content provider, which shows all groups and all
	 * elements of the given type.
	 */
	public ElementContentProvider(Class<Element>[] type) {
		this(false, type, null, null, false,false);
	}

	/**
	 * Create a new element content provider, which only provides groups, not
	 * leaf elements.
	 * 
	 * @param showGroupsOnly
	 * @param typesShown 
	 */
	public ElementContentProvider(boolean showGroupsOnly, Class<Element>[] typesShown) {
		this(showGroupsOnly, typesShown, null, null, false, false);
	}
	
	public ElementContentProvider(boolean showGroupsOnly, Class<Element>[] typesShown, 
			Class<Element>[] typesExcluded, Element[] excludedElements, boolean showFiles,boolean showPojects) {
		this.showFiles = showFiles;
		this.showGroupsOnly = showGroupsOnly;
		this.typesShown = typesShown;
		this.typesExcluded = typesExcluded;
		this.excludedElements = excludedElements;
		this.showProjects = showPojects;
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputChanged(final Viewer viewer, final Object oldInput,
			final Object newInput) {
	}

	@Override
	public Object[] getElements(final Object inputElement) {
		if (inputElement instanceof Element) {
			return getChildren(inputElement);
		}
		return null;
	}
	
	private boolean isShownClass(Class<?> clazz) {
		if (typesShown.length == 0) {
			return true;
		}
		
		for (int i = 0; i < typesShown.length; i++) {
			if (typesShown[i].isAssignableFrom(clazz) && !isExcludedClass(clazz)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isExcludedClass(Class<?> clazz) {
		if (typesExcluded != null) {
			for (Class <?> excludedType : typesExcluded) {
				if (excludedType.isAssignableFrom(clazz)) return true;
			}
		}
		
		return false;
	}

	@Override
	public Object[] getChildren(final Object parentElement) {
		List<Element> children = new ArrayList<Element>();

		if (parentElement instanceof Workspace) {
			for (Project p : ((Workspace) parentElement).getCurrentlyOpenedProjects()) 
			{
				if(showProjects)
					
					children.add(p);
				    
				
			}
			for (File f : ((Workspace) parentElement)
					.getCurrentlyOpenedFiles()) {
				if (showFiles) {
					children.add(f);
				} else {
					if (showGroupsOnly) {
						for (Element child : f.getContents()) {
							if (child instanceof Group 
									&& (typesShown == null 
									|| isShownClass(child.getClass()))) {
								
								children.add((Group) child);
							}
						}
					} else { // otherwise, add all children
						for (Element child : f.getContents()) {
							if (child instanceof Group || typesShown == null 
									|| isShownClass(child.getClass())) {
								
								children.add(child);
							}
						}
					}
				}
			}
			
		} else if (parentElement instanceof Group) { 
			// If the parent element is a group, it might have children
			Group parentGroup = (Group) parentElement;
			// If we only show groups in the treeview, only add group children
			// to the children's list
			if (showGroupsOnly) {
				for (Element child : parentGroup.getContents()) {
					if (child instanceof Group 
							&& (typesShown == null 
							|| isShownClass(child.getClass()))) {
						children.add((Group) child);
					}
				}
			} else { // otherwise, add all children
				for (Element child : parentGroup.getContents()) {
					if (child instanceof Group || typesShown == null || isShownClass(child.getClass())) {
						children.add(child);
					}
				}
			}
		}
		
		if (excludedElements != null) {
			children.removeAll(Arrays.asList(excludedElements));
		}
		
		return children.toArray();
	}

	@Override
	public Object getParent(final Object element) {
		if (element instanceof Element) {
			return ((Element) element).getParent();
		}
		return null;
	}

	@Override
	public boolean hasChildren(final Object element) {
		if (element instanceof Group) {
			if (showGroupsOnly) {
				return getChildren(element).length > 0;
			} else {
				return !((Group) element).getContents().isEmpty();
			}
		}
		return false;
	}

}
