package dk.dtu.imm.red.core.ui.presenters;

import org.eclipse.core.commands.operations.IUndoContext;

public interface IPresenter {

	public abstract void setUndoContext(final IUndoContext newContext);

	public abstract IUndoContext getUndoContext();

}
