package dk.dtu.imm.red.core.ui.internal.validation.views.impl;   
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT; 
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite; 

 








import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.ui.internal.validation.views.ValidationView;
import dk.dtu.imm.red.core.ui.internal.validation.views.ValidationViewPresenter;
import dk.dtu.imm.red.core.ui.views.BaseView; 
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.ExtendedAgileGrid;
import dk.dtu.imm.red.core.validation.ErrorMessage; 

/**
 * View used for displaying domain specific validation messages.
 * 
 * @author Johan Paaske Nielsen
 * 
 */
public final class ValidationViewImpl extends BaseView implements ValidationView  {  
	
	private ValidationComposite composite;
	private ExtendedAgileGrid<ErrorMessage> validationView;
	private ValidationViewModel viewModel = new ValidationViewModel();
 
	public ValidationViewImpl() {
		presenter = new ValidationViewPresenterImpl(this, viewModel);
	}
	
	@Override
	public void createPartControl(Composite parent) { 
		composite = new ValidationComposite(parent, SWT.NONE, (ValidationView) this);
		validationView =  new ExtendedAgileGrid<ErrorMessage>(composite.getValidationsTablePlaceholder(), SWT.NONE, null);
		validationView.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true)); 
		validationView.setPresenter(new EAGPresenter<ErrorMessage>());
		validationView.getPresenter().setDataSource(viewModel.getValidationMessages(), false);
		validationView.getPresenter().setColumnDefinitions(new ElementColumnDefinition[] { 
				new ElementColumnDefinition()
					.setHeaderName("")
					.setColumnWidth(100)
					.setEditable(false)
					.setCellHandler(new CellHandler<ErrorMessage>() {
						
						@Override
						public Object getAttributeValue(ErrorMessage item) {
							return item.getMessage();
						}

						@Override
						public void setAttributeValue(ErrorMessage item,
								Object value) {};
					})
		});
		validationView.setDataSource();

	}  
	
	@Override
	public void validationRequested() {
		((ValidationViewPresenter)presenter).validateModel();
	}
	
	@Override
	public void updateValidationView() {
		validationView.getPresenter().setDataSource(viewModel.getValidationMessages(), false);
		validationView.setDataSource(); 
	}
	
	protected String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Validation.html";
	} 
}
