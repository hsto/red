package dk.dtu.imm.red.core.ui.internal.navigation.implementation;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.TransferData;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.LogUtil;

public class ElementTransfer extends ByteArrayTransfer {
	private static ElementTransfer instance = new ElementTransfer();
	private static final String TYPE_NAME = "element-transfer-format";
	private static final int TYPEID = registerType(TYPE_NAME);

	private ElementTransfer() {
	}

	public static ElementTransfer getInstance() {
		return instance;
	}

	@Override
	protected int[] getTypeIds() {
		return new int[] { TYPEID };
	}

	@Override
	protected String[] getTypeNames() {
		return new String[] { TYPE_NAME };
	}

	protected List<Element> fromByteArray(byte[] bytes) {
		DataInputStream in = new DataInputStream(
				new ByteArrayInputStream(bytes));

		try {

			Resource resource = new XMLResourceImpl();
			resource.load(new ByteArrayInputStream(bytes), null);

			List<Element> elements = new ArrayList<Element>();
			elements.addAll((Collection<? extends Element>) resource
					.getContents());

			return elements;

		} catch (IOException e) {
			return null;
		}
	}

	protected byte[] toByteArray(Element[] elements) {
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		Resource resource = new XMLResourceImpl();

		for (Element element : elements) {
			resource.getContents().add(element);
		}

		try {
			resource.save(byteOut, null);
		} catch (IOException e) {
			e.printStackTrace(); LogUtil.logError(e);
			return null;
		}

		return byteOut.toByteArray();
	}

	@Override
	protected void javaToNative(Object object, TransferData transferData) {
		byte[] bytes = toByteArray((Element[]) object);

		if (bytes != null) {
			super.javaToNative(bytes, transferData);
		}
	}

	@Override
	protected Object nativeToJava(TransferData transferData) {
		byte[] bytes = (byte[]) super.nativeToJava(transferData);

		return fromByteArray(bytes);
	}

}
