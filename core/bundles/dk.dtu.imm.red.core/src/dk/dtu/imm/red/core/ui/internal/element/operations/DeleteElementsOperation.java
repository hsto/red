package dk.dtu.imm.red.core.ui.internal.element.operations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.internal.Activator;
import dk.dtu.imm.red.core.ui.internal.LogUtil;

/**
 * Undoable operation for deleting elements.
 *
 * @author Jakob Kragelund
 *
 */
public class DeleteElementsOperation extends AbstractOperation {

	protected MessageDialog singleElementConfirmDeleteDialog;
	protected MessageDialog multipleElementsConfirmDeleteDialog;
	protected List<Element> deletedElements;
	protected List<Element> selectedElements;

	/**
	 * This map contains the expanded selection, which means all the selected
	 * elements as well as their children. Each element is mapped to its
	 * parent, so when undoing, the parent can be restored.
	 */
	private Map<Element, Group> expandedSelectedElementsParentMap;

	public DeleteElementsOperation(List<Element> selectedElements) {
		super("Delete");

		String singleElementConfirmQuestion =
				"Are you sure you want to delete: " + selectedElements.get(0).getName() +"";
		String multipleElementsConfirmQuestion =
				"Are you sure you want to delete these elements?";

		singleElementConfirmDeleteDialog = new MessageDialog(Display
				.getCurrent().getActiveShell(), "Confirm Delete", null,
				singleElementConfirmQuestion, MessageDialog.QUESTION,
				new String[] { "Yes", "No" }, 1);

		multipleElementsConfirmDeleteDialog = new MessageDialog(Display
				.getCurrent().getActiveShell(), "Confirm Delete", null,
				multipleElementsConfirmQuestion, MessageDialog.QUESTION,
				new String[] { "Yes", "No" }, 1);

		this.selectedElements = selectedElements;
		expandedSelectedElementsParentMap = new HashMap<Element, Group>();
	}

	@Override
	public IStatus execute(final IProgressMonitor monitor,
			final IAdaptable info) throws ExecutionException {

		// Expand all the selected objects to include their children,
		// unless the selected object is a file (we just want those
		// removed from the workspace, not deleted content)
		for (Element element : selectedElements) {
			if (element instanceof File) {
				expandedSelectedElementsParentMap.put(element,
						element.getParent());
				continue;
			}
			List<Element> expandedElements = expandElement(element);
			for (Element expandedElement : expandedElements) {
				expandedSelectedElementsParentMap.put(expandedElement,
						expandedElement.getParent());
			}
		}

		int dialogResponse = -1;

		if (selectedElements.size() == 1) {
			dialogResponse = singleElementConfirmDeleteDialog.open();
		} else if (selectedElements.size() > 1) {
			dialogResponse = multipleElementsConfirmDeleteDialog.open();
		}

		// 0 == OK
		if (dialogResponse != 0) {
			return Status.CANCEL_STATUS;
		}

		return redo(monitor, info);
	}

	@Override
	public IStatus redo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		// Now inform all listening plugins that elements have been deleted
		for (final Element element
				: expandedSelectedElementsParentMap.keySet()) {
			element.delete();

			IConfigurationElement[] configurationElements = Platform
					.getExtensionRegistry()
					.getConfigurationElementsFor(
							"dk.dtu.imm.red.core.element");

			try {
				for (IConfigurationElement configurationElement
						: configurationElements) {

					final Object extension = configurationElement
							.createExecutableExtension("class");

					if (extension instanceof IElementExtensionPoint) {
						ISafeRunnable runnable = new ISafeRunnable() {

							@Override
							public void handleException(
									final Throwable exception) {
								exception.printStackTrace();
							}

							@Override
							public void run() throws Exception {
								((IElementExtensionPoint) extension)
								.deleteElement(element);
							}
						};
						runnable.run();
					}
				}
			} catch (CoreException e) {
				e.printStackTrace(); LogUtil.logError(e);
				return new Status(Status.ERROR, Activator.PLUGIN_ID,
						"Exception while deleting: " + e.getMessage());
			} catch (Exception e) {
				e.printStackTrace(); LogUtil.logError(e);
				return new Status(Status.ERROR, Activator.PLUGIN_ID,
						"Exception while deleting: " + e.getMessage());
			}
		}

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		for (Entry<Element, Group> entry
				: expandedSelectedElementsParentMap.entrySet()) {
			final Element element = (Element) entry.getKey();
			final Group parent = (Group) entry.getValue();
			if(parent != null) {
				parent.getContents().add(element);
				parent.save();
			}
			element.undoDelete();


			IConfigurationElement[] configurationElements = Platform
					.getExtensionRegistry()
					.getConfigurationElementsFor(
							"dk.dtu.imm.red.core.element");

			try {
				for (IConfigurationElement configurationElement
						: configurationElements) {

					final Object extension = configurationElement
							.createExecutableExtension("class");

					if (extension instanceof IElementExtensionPoint) {
						ISafeRunnable runnable = new ISafeRunnable() {

							@Override
							public void handleException(
									final Throwable exception) {
								exception.printStackTrace();
							}

							@Override
							public void run() throws Exception {
								((IElementExtensionPoint) extension)
								.restoreElement(element);
							}
						};
						runnable.run();
					}
				}
			} catch (CoreException e) {
				e.printStackTrace(); LogUtil.logError(e);
				return new Status(Status.ERROR, Activator.PLUGIN_ID,
						"Exception while deleting: " + e.getMessage());
			} catch (Exception e) {
				e.printStackTrace(); LogUtil.logError(e);
				return new Status(Status.ERROR, Activator.PLUGIN_ID,
						"Exception while deleting: " + e.getMessage());
			}
		}
		return Status.OK_STATUS;

	}



	/**
	 * Expands an element recursively, so it returns a flattened list containing
	 * all the elements in the tree, of which this element is a root. If this
	 * element is not a group, or if it has no children, the returned list will
	 * contain the element itself.
	 *
	 * @param element
	 *            The element to be expanded
	 * @return A flattened list of the tree in which the element is the root
	 */
	private List<Element> expandElement(final Element element) {
		List<Element> expandedElements = new ArrayList<Element>();
		expandedElements.add(element);
		if (element instanceof Group) {
			for (Element child : ((Group) element).getContents()) {
				expandedElements.addAll(expandElement(child));
			}
		}
		return expandedElements;
	}

}
