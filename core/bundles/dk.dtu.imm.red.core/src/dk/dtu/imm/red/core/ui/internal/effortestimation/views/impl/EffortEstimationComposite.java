package dk.dtu.imm.red.core.ui.internal.effortestimation.views.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.swtchart.Chart;
import org.swtchart.IBarSeries;
import org.swtchart.ISeries.SeriesType;

import dk.dtu.imm.red.core.ui.internal.effortestimation.views.EffortEstimationView;

public class EffortEstimationComposite extends Composite { 

	private Label lblTechnologyFactorValue;
	private Label lblUCPValue;
	private Label lblManagementFactorValue;
	private Label lblProductivityFactorValue;
	private Label lblTotalEffort;
	private Label lblCostValue;
	private Label lblManualCostValue;
	private Label lblEffortValue;
	private Label lblEstimatedBenefitValue;
	private IBarSeries barSeries;
	private Chart chart;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 * @param effortEstimationView 
	 */
	public EffortEstimationComposite(Composite parent, int style, final EffortEstimationView effortEstimationView) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		this.setVisible(false);
		
		ScrolledComposite scrolledComposite = new ScrolledComposite(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setMinHeight(300);
		scrolledComposite.setExpandHorizontal(true);
		GridData gd_scrolledComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_scrolledComposite.minimumWidth = 50;
		gd_scrolledComposite.minimumHeight = 50;
		gd_scrolledComposite.heightHint = 366;
		scrolledComposite.setLayoutData(gd_scrolledComposite);
		scrolledComposite.setExpandVertical(true);
		
		Composite composite_1 = new Composite(scrolledComposite, SWT.NONE);
		composite_1.setLayout(new GridLayout(1, false));
		
		lblTitle = new Label(composite_1, SWT.NONE);
		lblTitle.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblTitle.setText("N/A");
		
		cmpCalculated = new Composite(composite_1, SWT.NONE);
		cmpCalculated.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		cmpCalculated.setLayout(new GridLayout(1, true));
		
		
		Group grpCalculatedFactors = new Group(cmpCalculated, SWT.NONE);
		grpCalculatedFactors.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		grpCalculatedFactors.setLayout(new GridLayout(1, true));
		grpCalculatedFactors.setText("Calculated Factors"); 
		
		Button btnRefreshValues = new Button(grpCalculatedFactors, SWT.NONE);
		btnRefreshValues.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				effortEstimationView.updateValues();
			}
		});
		btnRefreshValues.setText("Refresh Values");
		
		Composite composite = new Composite(grpCalculatedFactors, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite.setLayout(new GridLayout(2, true));
		
		Label lblRequirementFactor = new Label(composite, SWT.NONE);
		lblRequirementFactor.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblRequirementFactor.setSize(122, 15);
		lblRequirementFactor.setText("Requirement Factor (R)"); 
		
		
		lblRequirementFactorValue = new Label(composite, SWT.NONE);
		lblRequirementFactorValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblRequirementFactorValue.setText("N/A");
		lblRequirementFactorValue.setSize(368, 15);
		
		Label lblManagementFactor = new Label(composite, SWT.NONE);
		lblManagementFactor.setText("Management Factor (M)");
		
		lblManagementFactorValue = new Label(composite, SWT.NONE);
		lblManagementFactorValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblManagementFactorValue.setText("N/A");
		
		Label lblTechnologyFactor = new Label(composite, SWT.NONE);
		lblTechnologyFactor.setText("Technology Factor (T)");
		
		lblTechnologyFactorValue = new Label(composite, SWT.NONE);
		lblTechnologyFactorValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblTechnologyFactorValue.setText("N/A");
		
		Label lblProductivityFactor = new Label(composite, SWT.NONE);
		lblProductivityFactor.setText("Productivity Factor (P)");
		
		lblProductivityFactorValue = new Label(composite, SWT.NONE);
		lblProductivityFactorValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblProductivityFactorValue.setText("N/A");
		
		Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
	 
		
		Label lblUCP = new Label(composite, SWT.NONE);
		
		lblUCP.setText("Use Case Points ( R×T×M)");
		
		lblUCPValue = new Label(composite, SWT.NONE);
		lblUCPValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblUCPValue.setText("N/A");
		lblUCPValue.setSize(308, 21);
		lblTotalEffort = new Label(composite, SWT.NONE);
		
		lblTotalEffort.setText("Effort (UCP×P)");
		
		lblEffortValue = new Label(composite, SWT.NONE);
		lblEffortValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblEffortValue.setText("N/A");
		lblEffortValue.setBounds(0, 0, 76, 21);
		
		Label lblComputedCost = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		lblComputedCost.setText("Computed Cost");
		lblComputedCost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
	 
		Label lblCost = new Label(composite, SWT.NONE);
		lblCost.setText("Estimated Cost (UCP)");
	 
		
		lblCostValue = new Label(composite, SWT.NONE);
		lblCostValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblCostValue.setText("N/A");
		
		Label label_1 = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
	 
		
		Label lblManualCost = new Label(composite, SWT.NONE);
		
		lblManualCost.setText("Estimated Cost (Assigned)");
		
		lblManualCostValue = new Label(composite, SWT.NONE);
		lblManualCostValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblManualCostValue.setText("N/A'");
		
		Label lblEstimatedBenefitmanual = new Label(composite, SWT.NONE);
		lblEstimatedBenefitmanual.setText("Estimated Benefit (Assigned)");
		
		lblEstimatedBenefitValue = new Label(composite, SWT.NONE);
		lblEstimatedBenefitValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblEstimatedBenefitValue.setText("N/A");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
		cmpChart = new Composite(composite_1, SWT.NONE);
		cmpChart.setLayout(new GridLayout(1, false));
		GridData gd_cmpChart = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_cmpChart.heightHint = 300;
		gd_cmpChart.minimumHeight = 300;
		cmpChart.setLayoutData(gd_cmpChart);
		scrolledComposite.setContent(composite_1);
		scrolledComposite.setMinSize(composite_1.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		createChart(cmpChart);
	}
	
	 /**
     * create the chart.
     * 
     * @param parent
     *            The parent composite
     * @return The created chart
     */
    public Chart createChart(Composite parent) {

        // create a chart
         chart = new Chart(parent, SWT.NONE);
        GridData gd_chart = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_chart.minimumHeight = 100;
        gd_chart.heightHint = 100;
        chart.setLayoutData(gd_chart);

        // set titles
        chart.getTitle().setText("Cost-Benefit Visualisation");
        chart.getAxisSet().getXAxis(0).getTitle().setText("Type");
        chart.getAxisSet().getYAxis(0).getTitle().setText("Value"); 
        chart.getAxisSet().getXAxis(0).enableCategory(true);
        chart.getAxisSet().getXAxis(0).setCategorySeries(
                new String[] { "Computed Cost", "Assigned Cost", "Assigned Benefit"});

    

        return chart;
    }
    
    public void updateValues( double[] ySeries) { 
    	 
       // create bar series
       barSeries = (IBarSeries) chart.getSeriesSet().createSeries(
              SeriesType.BAR, "1");
       barSeries.setYSeries(ySeries); 
       barSeries.setVisibleInLegend(false);
       barSeries.getLabel().setFormat("##.00");
       barSeries.getLabel().setVisible(true);
       
       Color color = new Color(Display.getDefault(), 80, 240, 180);
       barSeries.setBarColor(color);
       
	   chart.getAxisSet().adjustRange();
	   chart.redraw();
	  
    } 
   

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	} 
	
	private Label lblRequirementFactorValue;
	private Label lblTitle;
	private Composite cmpCalculated;
	private Composite cmpChart;
	public Composite getCmpCalculated() {
		return cmpCalculated;
	}

	public Label getLblTitle() {
		return lblTitle;
	}

	public Label getLblRequirementFactorValue() {
		return lblRequirementFactorValue;
	}

	public Label getLblTechnologyFactorValue() {
		return lblTechnologyFactorValue;
	}

	public Label getLblUCPValue() {
		return lblUCPValue;
	}

	public Label getLblManagementFactorValue() {
		return lblManagementFactorValue;
	}

	public Label getLblProductivityFactorValue() {
		return lblProductivityFactorValue;
	}

	public Label getLblTotalEffort() {
		return lblTotalEffort;
	}

	public Label getLblCostValue() {
		return lblCostValue;
	}

	public Label getLblManualCostValue() {
		return lblManualCostValue;
	}

	public Label getLblEffortValue() {
		return lblEffortValue;
	}

	public Label getLblEstimatedBenefitValue() {
		return lblEstimatedBenefitValue;
	}
}
