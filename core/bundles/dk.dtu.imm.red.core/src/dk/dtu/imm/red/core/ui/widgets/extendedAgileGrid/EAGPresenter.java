package dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.ExtendedMetaData;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementImpl;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.ui.internal.element.extensions.ElementLinkActionExtension;
import dk.dtu.imm.red.core.ui.presenters.IPresenter;

public class EAGPresenter<T> implements IPresenter {

	private List<T> dataSourceView = null;
	private List<T> dataSource = null;
	private HashMap<Method, Method> methodMap = new LinkedHashMap<Method, Method>();
	private Class<?> reflectedClass;
	private EClass reflectedEClass;
	private boolean recursiveView;

	public boolean isRecursiveView() {
		return recursiveView;
	}

	public EAGPresenter() {
		dataSourceView = new ArrayList<T>();
	}

	public List<T> getDataSource() {
		return dataSource;
	}

	public void setDataSource(List<T> dataSource, boolean recursive) {
		recursiveView = recursive;
		this.dataSource = dataSource;
		updateDataSourceFilter();
	}

	public void updateDataSourceFilter() {
		this.dataSourceView.clear();
		getFilterDataSourceRec(this.dataSource, this.dataSourceView);
	}

	@SuppressWarnings("unchecked")
	public void getFilterDataSourceRec(List<T> inp, List<T> currentFilteredList) {
		if (this.dataSourceView == null)
			this.dataSourceView = new ArrayList<T>();

		// If reflected class has been set. Filter content by that type,
		// This prevents incompatible types from showing up,
		// if they're in the data source

		for (T item : inp) {
			// View Recursively

			if (recursiveView && Group.class.isAssignableFrom(item.getClass())) {
				getFilterDataSourceRec((List<T>) ((Group) item).getContents(), currentFilteredList);
			}

			if (reflectedEClass != null) {
				if (reflectedEClass.getInstanceClass().isAssignableFrom(item.getClass())) {
					currentFilteredList.add(item);
				}
			} else {
				currentFilteredList.add(item);
			}
		}
	}

	public Class<?> getReflectedClass() {
		return reflectedClass;
	}

	public void setReflectedClass(Class<?> reflectedClass) {
		this.reflectedClass = reflectedClass;
	}

	public void setReflectedClass(EClass reflectedClass) {
		if (reflectedClass != null) {
			this.reflectedEClass = reflectedClass;
			this.reflectedClass = reflectedClass.getInstanceClass();
		}
	}

	public EClass getReflectedEClass() {
		return reflectedEClass;
	}

	public void setReflectedEClassByName(String ns, String type) {
		ExtendedMetaData modelMetaData = new BasicExtendedMetaData(EPackage.Registry.INSTANCE);
		EClass ecl = (EClass) modelMetaData.getType(ns, type);

		setReflectedClass(ecl);
	}

	private ElementColumnDefinition[] columnDefinitions;

	public ElementColumnDefinition[] getColumnDefinitionsByReflection(Class<?> fromClass) {
		this.reflectedClass = fromClass;
		updateMethodMap(fromClass);

		java.util.List<ElementColumnDefinition> cols = new ArrayList<ElementColumnDefinition>();

		// Sets which types are allowed to be displayed in the Agile grid view
		Set<String> allowedTypes = new HashSet<String>(Arrays.asList(String.class.getName(), int.class.getName(),
				Integer.class.getName(), float.class.getName(), double.class.getName(), long.class.getName(),
				boolean.class.getName(), short.class.getName(), byte.class.getName(), Enum.class.getName()));

		for (Method getterMethod : methodMap.keySet()) {

			Class<?> returnType = getterMethod.getReturnType();

			// If the return type is not in the allowed type hash set, continue
			// the for loop

			// Types can derive from Enum. Since Enums are allowed, we allow
			// them.
			if (!allowedTypes.contains(returnType.getName()) && !Enum.class.isAssignableFrom(returnType)) {
				continue;
			}

			String methodName = getterMethod.getName();
			String displayName = methodName;

			// Extract the attribute name, from the getter method name
			if (methodName.startsWith("get") && methodName.length() > 3) {
				displayName = displayName.substring(3, displayName.length());
			}

			// If the method do not have a setter method, set it to non editable
			boolean isEditable = methodMap.get(getterMethod) != null;

			ElementColumnDefinition rc = new ElementColumnDefinition().setHeaderName(displayName)
					.setColumnType(returnType).setEditable(isEditable).setGetterMethod(new String[] { methodName });

			// Check if its a ENUM type, and add enum items to column
			if (Enum.class.isAssignableFrom(getterMethod.getReturnType())) {

				java.util.List<Object> enumList = new ArrayList<Object>();
				for (Object enumConstant : getterMethod.getReturnType().getEnumConstants()) {
					Object a = ((Enum) enumConstant).toString();
					enumList.add(a);
				}

				rc.setCellItems(enumList.toArray(new String[enumList.size()]));
			}

			cols.add(rc);
		}

		columnDefinitions = cols.toArray(new ElementColumnDefinition[cols.size()]);
		return columnDefinitions;
	}

	private HashMap<Method, Method> updateMethodMap(Class<?> fromClass) {
		methodMap.clear();

		// Get getters
		for (Method m : fromClass.getDeclaredMethods()) {

			// All public methods, that don't return void, are assumed getters
			if (Modifier.isPublic(m.getModifiers()) && m.getReturnType().getName() != "void") {
				methodMap.put(m, null);
			}
		}

		// Get setters
		for (Method m : fromClass.getDeclaredMethods()) {

			// All public methods, that return void, are assumed setters
			if (Modifier.isPublic(m.getModifiers()) && m.getReturnType().getName() == "void") {
				try {
					Method getter = fromClass.getMethod(getGetterNameFromSetterName(m.getName()), null);
					methodMap.put(getter, m);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}

		return methodMap;
	}

	private String getGetterNameFromSetterName(String setterName) {
		return "get" + (setterName.length() > 3 ? setterName.substring(3, setterName.length()) : "");
	}

	// CREATE DELETE
	public void AddItem() {
		T o = getNewItem();
		if (o != null)
			dataSource.add(o);
		this.dataSourceView.clear();
		getFilterDataSourceRec(dataSource, dataSourceView);
	}

	@SuppressWarnings("unchecked")
	public void AddItem(Object o) {
		if (o != null)
			dataSource.add((T) o);
		this.dataSourceView.clear();
		getFilterDataSourceRec(dataSource, dataSourceView);
	}

	@SuppressWarnings("unchecked")
	public T getNewItem() {

		// If EClass has been provided. Use EMF factory
		if (reflectedEClass != null) {
			EFactory f = (EFactory) EPackage.Registry.INSTANCE.getEFactory(reflectedEClass.getEPackage().getNsURI());
			T newItem = (T) f.create(reflectedEClass);

			// If name is null, it will cause an exception in the element
			// explorer.
			if (newItem instanceof Element) {
				((Element) newItem).setName("New item");
			}

			return newItem;
		}

		return null;

	}

	public void DeleteItem(List<Integer> selectedIndexes) {
		List<Object> selectedItems = getSelectedItems(selectedIndexes);
		dataSource.removeAll(selectedItems);
		this.dataSourceView.clear();
		getFilterDataSourceRec(dataSource, dataSourceView);
	}

	public void openItem(List<Integer> selectedIndexes, int columnIndex) {
		List<Object> selectedItems = getSelectedItems(selectedIndexes);
		for (Object o : selectedItems) {
			if (o != null) {
				Object referencedObject = o;
				if (columnIndex >= 0 && columnDefinitions[columnIndex].getReferenceGetterMethod() != null) {
					referencedObject = callMethods(o, null, columnDefinitions[columnIndex].getReferenceGetterMethod());
				}

				// Open elements from table (used in Checking Composite and
				// Traceability Stepper)
				//
				// If SpecificationElements are listed directly in a table, this
				// check detects them and separates the handling from issue
				// messages
				if (canOpenItem(referencedObject) && !(referencedObject instanceof Folder)) {
					ElementLinkActionExtension.elementClicked((Element) referencedObject);
				} else if (canOpenItem(o) && (referencedObject instanceof Folder)) {
					ElementLinkActionExtension.elementClicked((Element) o);
				}
			}
		}
	}

	public void getCellReferenceObject(List<Integer> selectedIndexes, int columnIndex) {
		List<Object> selectedItems = getSelectedItems(selectedIndexes);
		for (Object o : selectedItems) {
			if (o != null) {
				Object referencedObject = o;
				if (columnIndex >= 0 && columnDefinitions[columnIndex].getReferenceGetterMethod() != null) {
					referencedObject = callMethods(o, null, columnDefinitions[columnIndex].getReferenceGetterMethod());
				}
			}
		}
	}

	// READ
	@SuppressWarnings("unchecked")
	public Object doGetContentAt(final int row, final int col) {
		if (columnDefinitions == null)
			throw new IllegalArgumentException("No columns have been set");
		Object o = dataSourceView.get(row);

		ElementColumnDefinition column = columnDefinitions[col];
		CellHandler<T> ch = (CellHandler<T>) column.getCellHandler();
		// Invoke Strongly typed getter, if it has been set
		if (ch != null) {
			return ch.getAttributeValue((T) o);
		} else {
			// If getter method have not been set, just return the toString()
			// representation
			String[] getterMethod = column.getGetterMethod();
			if (getterMethod == null) {
				return o.toString();
			}

			// Otherwise, invoke methods and retrieve value
			return callMethods(o, null, column.getGetterMethod());
		}
	}

	private Object callMethods(Object res, Object parameter, String... methodNames) {
		for (String methodName : methodNames) {
			Method method;
			try {
				if (res == null)
					return null;
				method = res.getClass().getMethod(methodName);
				res = method.invoke(res);
			} catch (Exception e) {
				throw new IllegalArgumentException("Could not invoke getter method defined in object: " + res.toString()
						+ " in column at: " + methodName, e);
			}
		}

		return res;
	}

	// UPDATE
	@SuppressWarnings("unchecked")
	public boolean doSetContentAt(int row, int col, Object value) throws IllegalArgumentException {
		if (reflectedClass == null)
			throw new IllegalArgumentException(
					"Cannot set content automatically, without knowing what kind of class it is");

		Object o = dataSourceView.get(row);
		ElementColumnDefinition column = columnDefinitions[col];
		CellHandler<T> ch = (CellHandler<T>) column.getCellHandler();

		try {
			if (ch != null) {
				ch.setAttributeValue((T) o, value);
			} else {

				String[] getterMethod = column.getGetterMethod();

				// If it is a primitive type. Change it directly
				if (getterMethod == null) {
					dataSource.set(row, (T) value);
					updateDataSourceFilter();
					return true;
				}

				Method getter = reflectedClass.getMethod(getGetterNameFromSetterName(getterMethod[0]), null);

				// If the value has not changed, don't do anything
				Object prevValue = getter.invoke(o, null);
				if (prevValue != null && prevValue.equals(value))
					return false;

				// Special case for enums. Extract the Enum value from the
				// string
				Class<?> returnType = getter.getReturnType();

				// Convert the reference to the appropiate type,
				// since values are String for non appropiate types like
				// integers or enums.
				// Does not seems like the best way to do it, but haven't found
				// a better way so far
				if (Enum.class.isAssignableFrom(returnType)) {
					Class<Enum<?>> enumReturnType = (Class<Enum<?>>) returnType;

					boolean enumFoundByString = false;

					for (Enum<?> e : enumReturnType.getEnumConstants()) {
						if (e.toString().equals(value)) {
							value = e;
							enumFoundByString = true;
						}
					}

					// If the String cannot be found in the Enum class, don't do
					// anything
					if (enumFoundByString == false)
						return false;
				} else if (int.class.isAssignableFrom(returnType)) {
					value = Integer.parseInt(value.toString());
				} else if (Integer.class.isAssignableFrom(returnType)) {
					value = Integer.parseInt(value.toString());
				} else if (Long.class.isAssignableFrom(returnType)) {
					value = Long.parseLong(value.toString());
				} else if (short.class.isAssignableFrom(returnType)) {
					value = Short.parseShort(value.toString());
				} else if (double.class.isAssignableFrom(returnType)) {
					value = Double.parseDouble(value.toString());
				} else if (byte.class.isAssignableFrom(returnType)) {
					value = Byte.parseByte(value.toString());
				} else if (float.class.isAssignableFrom(returnType)) {
					value = Float.parseFloat(value.toString());
				}

				Method setter = methodMap.get(getter);

				if (setter != null) {
					setter.invoke(o, value);
				}

			}
		} catch (NumberFormatException ne) {
			// A Wrong type has been used. Eg. String in an Integer return type.
			return false;
		} catch (IllegalArgumentException ne) {
			// A Wrong type has been used. Eg. String in an Enum return type.
			return false;
		} catch (Exception e) {
			throw new IllegalArgumentException("Could not set argument. " + e.getMessage(), e);
		}

		return true;
	}

	public boolean canOpenSelection(List<Integer> selectedIndexes) {

		boolean canOpenItem = false;

		for (Object o : getSelectedItems(selectedIndexes)) {
			if (canOpenItem(o))
				canOpenItem = true;
		}
		return canOpenItem;
	}

	private boolean canOpenItem(Object o) {
		return o instanceof Element;
	}

	@Override
	public void setUndoContext(IUndoContext newContext) {
		// TODO Auto-generated method stub

	}

	@Override
	public IUndoContext getUndoContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Object> getSelectedItems(List<Integer> selectedIndexes) {
		List<Object> items = new ArrayList<Object>();
		if (selectedIndexes.size() > dataSourceView.size())
			return items;
		for (Integer i : selectedIndexes) {
			items.add(dataSourceView.get((int) i));
		}

		return items;
	}

	public boolean handleCellEdit(int row, int col) {
		// TODO Auto-generated method stub
		return false;
	}

	public void rowSelectionUpdated(boolean rowSelected, int selectedIndex) {
		// TODO Auto-generated method stub

	}

	public ElementColumnDefinition[] getColumnDefinitions() {
		return columnDefinitions;
	}

	public EAGPresenter<T> setColumnDefinitions(ElementColumnDefinition[] columnDefinitions) {
		this.columnDefinitions = columnDefinitions;
		return this;
	}

	public List<T> getDataSourceView() {
		return dataSourceView;
	}

	public ElementColumnDefinition[] getDefaultColumnDefinition() {

		ElementImpl o = (ElementImpl) getNewItem();
		columnDefinitions = o.getColumnRepresentation();

		if (columnDefinitions == null) {
			return getColumnDefinitionsByReflection(reflectedClass);
		}

		return columnDefinitions;
	}

	public ElementColumnDefinition[] getSpecificColumnDefinition(Class<?> fromClass) {

		ElementImpl o = (ElementImpl) getNewItem();
		columnDefinitions = o.getColumnRepresentation(fromClass);

		if (columnDefinitions == null) {
			return getColumnDefinitionsByReflection(reflectedClass);
		}
		return columnDefinitions;
	}

	public Double calculateSumForColumn(List<Double> values) {

		Double sum = 0.0;

		for (double r : values) {
			sum += r;
		}

		return sum;
	}
}
