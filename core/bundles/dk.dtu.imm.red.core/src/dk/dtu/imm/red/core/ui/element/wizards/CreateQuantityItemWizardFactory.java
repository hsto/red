package dk.dtu.imm.red.core.ui.element.wizards;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.QuantityItem;
import dk.dtu.imm.red.core.element.QuantityLabel;
import dk.dtu.imm.red.core.ui.element.wizards.impl.CreateQuantityItemWizardImpl;

public class CreateQuantityItemWizardFactory { 
	
	public static QuantityLabel createQuantityItem(EList<QuantityItem> items) {
		CreateQuantityItemWizard wizard = 
				new CreateQuantityItemWizardImpl(items);
		wizard.init(PlatformUI.getWorkbench(), 
				(IStructuredSelection) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection());

		WizardDialog dialog = 
				new WizardDialog(Display.getCurrent().getActiveShell(), 
						wizard);
					
		dialog.open();
		
		QuantityLabel selectedLabel = 
				((CreateQuantityItemWizardPresenter) wizard.getPresenter())
				.getselectedLabel();
		
		return selectedLabel;
	}
}
