package dk.dtu.imm.red.core.ui.internal.project.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.ui.internal.project.wizards.impl.NewProjectWizardImpl;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class CreateProjectHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if ((WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects().size() > 0)) {
			MessageDialog md = new MessageDialog(Display.getCurrent().getActiveShell(), "Open project", null,
					"Please close the current opened project in order to create a new one!", MessageDialog.INFORMATION,
					new String[] { "ok" }, 1);

			md.open();

			return null;
		}
		
		NewProjectWizardImpl wizard = new NewProjectWizardImpl();
		ISelection currentSelection = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService()
				.getSelection();
		wizard.init(PlatformUI.getWorkbench(), (IStructuredSelection) currentSelection);

		WizardDialog dialog = new WizardDialog(HandlerUtil.getActiveShell(event), wizard);
		dialog.create();

		dialog.open();

		return null;
	}
}
