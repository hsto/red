package dk.dtu.imm.red.core.ui.element.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.element.wizards.impl.CreateElementReferenceWizardImpl;

public class CreateElementReferenceWizardFactory { 
	public static Object createElementReference(Class<?> filterClass) {
		return createElementReference(filterClass, null, null);
	}
	public static Object createElementReference(Class<?> filterClass, String dialogTitle, String dialogDescription) {
		return createElementReference(new Class[] { filterClass }, dialogTitle, dialogDescription);
	}
	
	public static Object createElementReference(Class<?>[] filterClasses) {
		return createElementReference(filterClasses, null, null);
	}
	
	public static Object createElementReference(Class<?>[] filterClasses, String dialogTitle, String dialogDescription) {
		CreateElementReferenceWizard wizard = 
				new CreateElementReferenceWizardImpl(filterClasses, dialogTitle, dialogDescription);
		wizard.init(PlatformUI.getWorkbench(), 
				(IStructuredSelection) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection());

		WizardDialog dialog = 
				new WizardDialog(Display.getCurrent().getActiveShell(), 
						wizard);
					
		dialog.open();
		
		Element selectedElement = 
				((CreateElementReferenceWizardPresenter) wizard.getPresenter())
				.getselectedElement();
		
		return selectedElement;
	}
}
