package dk.dtu.imm.red.core.ui.internal.navigation;

import org.eclipse.ui.IViewPart;

public interface FileExplorer extends IViewPart {

	String ID = "dk.dtu.imm.red.core.navigation.fileexplorer";
}
