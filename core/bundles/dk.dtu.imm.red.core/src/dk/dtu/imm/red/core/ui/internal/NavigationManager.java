package dk.dtu.imm.red.core.ui.internal;

import java.util.AbstractMap.SimpleEntry;
import java.util.Stack;

import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.services.IEvaluationService;

import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;

public final class NavigationManager implements IStartup {

	private static Stack<SimpleEntry<String, IEditorInput>> forwardParts;
	private static Stack<SimpleEntry<String, IEditorInput>> backwardParts;
	private static boolean isNavigatingBack = false;

	@Override
	public void earlyStartup() {

		final IWorkbench workbench = PlatformUI.getWorkbench();
		workbench.getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
				if (window != null) {

					// This is the listener which keeps track of which editors
					// are
					// deactivated, to be used for navigation purposes
					forwardParts = new Stack<SimpleEntry<String, IEditorInput>>();
					backwardParts = new Stack<SimpleEntry<String, IEditorInput>>();
					window.getActivePage().addPartListener(new PartAdapter() {
						@Override
						public void partDeactivated(
								IWorkbenchPartReference partRef) {
							if (partRef.getPart(false) instanceof BaseEditor) {
								BaseEditor editor = (BaseEditor) partRef
										.getPart(false);
								if (!isNavigatingBack) {
									backwardParts
											.push(new SimpleEntry<String, IEditorInput>(
													editor.getEditorID(),
													editor.getEditorInput()));
								} else if (isNavigatingBack) {
									forwardParts
											.push(new SimpleEntry<String, IEditorInput>(
													editor.getEditorID(),
													editor.getEditorInput()));
								}
							}
						}
					});
				
				} else {
					LogUtil.logWarning("NavigationManager not started properly");
					
				}
			}
		});

	}

	public static void openPreviousEditor() {
		if (backwardParts.isEmpty()) {
			return;
		}
		SimpleEntry<String, IEditorInput> entry = backwardParts.pop();
		if (((BaseEditorInput) entry.getValue()).getElement().getParent() == null) {
			openPreviousEditor();
			return;
		}

		isNavigatingBack = true;
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(entry.getValue(), entry.getKey());
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

		isNavigatingBack = false;

		IEvaluationService evalService = (IEvaluationService) PlatformUI
				.getWorkbench().getService(IEvaluationService.class);
		evalService
				.requestEvaluation("dk.dtu.imm.red.core.navigation.canGoBackward");
		evalService
				.requestEvaluation("dk.dtu.imm.red.core.navigation.canGoForward");
	}

	public static int backHistorySize() {
		if (backwardParts != null) {
			return backwardParts.size();
		} else {
			return 0;
		}
	}

	public static int forwardHistorySize() {
		if (forwardParts != null) {
			return forwardParts.size();
		} else {
			return 0;
		}
	}

	public static void openForwardEditor() {
		if (forwardParts.isEmpty()) {
			return;
		}
		SimpleEntry<String, IEditorInput> entry = forwardParts.pop();

		if (((BaseEditorInput) entry.getValue()).getElement().getParent() == null) {
			openForwardEditor();
			return;
		}

		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(entry.getValue(), entry.getKey());
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

		IEvaluationService evalService = (IEvaluationService) PlatformUI
				.getWorkbench().getService(IEvaluationService.class);
		evalService
				.requestEvaluation("dk.dtu.imm.red.core.navigation.canGoBackward");
		evalService
				.requestEvaluation("dk.dtu.imm.red.core.navigation.canGoForward");
	}

}
