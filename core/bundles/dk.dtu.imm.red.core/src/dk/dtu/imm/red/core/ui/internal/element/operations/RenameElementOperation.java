package dk.dtu.imm.red.core.ui.internal.element.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Display;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.ExtensionHelper;
import dk.dtu.imm.red.core.ui.internal.LogUtil;

public class RenameElementOperation extends AbstractOperation{

	protected Element selectedElement;
	protected String oldName;
	protected InputDialog renameDialog;
	
	public RenameElementOperation(Element selectedElement){
		super("Rename");
		this.selectedElement = selectedElement;
		this.oldName = selectedElement.getName();
		
		renameDialog = new InputDialog(Display
				.getCurrent().getActiveShell(), "Rename", "New name:", 
				selectedElement.getName(), null);		
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		
		int dialogResponse = -1;
		dialogResponse = renameDialog.open();
		
		// 0 == OK
		if (dialogResponse != 0) {
			return Status.CANCEL_STATUS;
		}
		
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		
		selectedElement.setName(renameDialog.getValue());
		
		/**
		 * Issue #119 @Luai
		 */
		try {
			ExtensionHelper.getCommand("dk.dtu.imm.red.visualmodeling.ui.cascadeDiagramsOnRename").run();
		} catch (Exception e) {			
			e.printStackTrace(); LogUtil.logError(e);
		}
		
		
		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		
		selectedElement.setName(oldName);
		
		try {
			ExtensionHelper.getCommand("dk.dtu.imm.red.visualmodeling.ui.cascadeDiagramsOnRename").run();
		} catch (Exception e) {			
			e.printStackTrace(); LogUtil.logError(e);
		}
		
		return Status.OK_STATUS;
	}
	
}
