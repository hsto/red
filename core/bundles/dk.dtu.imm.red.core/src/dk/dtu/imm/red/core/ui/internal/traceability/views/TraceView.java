package dk.dtu.imm.red.core.ui.internal.traceability.views;

import org.eclipse.ui.IViewPart;

public interface TraceView extends IViewPart{
	String ID = "dk.dtu.imm.red.core.traceability.view"; 
	void updateTraceabilityView();
	void tracingRequested(); 
}
