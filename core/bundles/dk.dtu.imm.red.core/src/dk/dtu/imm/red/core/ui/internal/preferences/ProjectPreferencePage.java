package dk.dtu.imm.red.core.ui.internal.preferences;

import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.internal.AutoSave;
import dk.dtu.imm.red.core.ui.internal.navigation.implementation.ElementExplorerImpl;
import dk.dtu.imm.red.core.ui.internal.navigation.implementation.FileExplorerImpl;

public class ProjectPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {


	/**
	 * Create the preference page.
	 */
	public ProjectPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		setTitle("Project Preference");

	}


	/**
	 * Create contents of the preference page.
	 */
	@Override
	protected void createFieldEditors() {

		Composite composite = getFieldEditorParent();
		addField(new DirectoryFieldEditor(PreferenceConstants.PROJECT_PREF_WORKSPACE_PATH, "Workspace Location", getFieldEditorParent()));
		addField(new FileFieldEditor(PreferenceConstants.PROJECT_PREF_LOG_PATH, "Log Path", getFieldEditorParent()));
		addField(new ComboFieldEditor(PreferenceConstants.PROJECT_PREF_LOG_LEVEL, "Log Level", new String[][]{{"ERROR", "ERROR"}, {"WARNING", "WARNING"}, {"NOTIFICATION", "NOTIFICATION"}, {"DEBUG", "DEBUG"}}, getFieldEditorParent()));
		addField(new ComboFieldEditor(PreferenceConstants.PROJECT_PREF_LABELS, "Labels", new String[][]{{"plain", "plain"}, {"complex", "complex"}}, getFieldEditorParent()));
		addField(new ComboFieldEditor(
				PreferenceConstants.PROJECT_PREF_AUTOSAVE,
				"Auto Save",
				new String[][] {
						{ "off", PreferenceConstants.PROJECT_PREF_AUTOSAVE_OFF },
						{ "2 min", PreferenceConstants.PROJECT_PREF_AUTOSAVE_2MIN },
						{ "5 min", PreferenceConstants.PROJECT_PREF_AUTOSAVE_5MIN },
						{ "10 min", PreferenceConstants.PROJECT_PREF_AUTOSAVE_10MIN },
						{ "20 min", PreferenceConstants.PROJECT_PREF_AUTOSAVE_20MIN },
						{ "40 min", PreferenceConstants.PROJECT_PREF_AUTOSAVE_40MIN },
						{ "60 min", PreferenceConstants.PROJECT_PREF_AUTOSAVE_60MIN } },
				getFieldEditorParent()));

	}

	/**
	 * Initialize the preference page.
	 */
	public void init(IWorkbench workbench) {
		// Initialize the preference page
	}

	@Override
	public boolean performOk() {
		boolean success = super.performOk();

		IViewPart elementExplorerView = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
		.getActivePage().findView("dk.dtu.imm.red.core.navigation.elementexplorer");

		if(elementExplorerView!=null){
			((ElementExplorerImpl)elementExplorerView).refreshViewParts();
		}

		IViewPart fileExplorerView = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
		.getActivePage().findView("dk.dtu.imm.red.core.navigation.fileexplorer");

		if(fileExplorerView!=null){
			((FileExplorerImpl)fileExplorerView).refreshViewParts();
		}
		
		String period = PreferenceUtil.getPreferenceString(PreferenceConstants.PROJECT_PREF_AUTOSAVE);
		AutoSave.INSTANCE.setAndStartTimer(period);

		return success;
	}
}
