package dk.dtu.imm.red.core.ui.internal;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;


public class LogUtil {
	public static void logError(String msg) {
		IStatus st = new Status(IStatus.ERROR, Activator.PLUGIN_ID, msg);
		Activator.getDefault().getLog().log(st);

	}

	public static void logError(Exception ex) {
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		String exceptionDetails = sw.toString();
		IStatus st = new Status(IStatus.ERROR, Activator.PLUGIN_ID, exceptionDetails);
		Activator.getDefault().getLog().log(st);
	}

	public static void logInfo(String msg) {
		IStatus st = new Status(IStatus.INFO, Activator.PLUGIN_ID, msg);
		Activator.getDefault().getLog().log(st);
	}

	public static void logWarning(String msg) {
		IStatus st = new Status(IStatus.WARNING, Activator.PLUGIN_ID, msg);
		Activator.getDefault().getLog().log(st);
	}

}
