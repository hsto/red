package dk.dtu.imm.red.core.ui.internal.navigation;

import org.eclipse.ui.IViewPart;

public interface ElementExplorer extends IViewPart {

	String ID = "dk.dtu.imm.red.core.navigation.elementexplorer";

}
