package dk.dtu.imm.red.core.ui.editors;

import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Listener;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;

public interface IBaseEditor {
	
	/**
	 * Wrapper class for an association.
	 * @author Jakob Kragelund @Luai changed from association
	 *
	 */	
	public class Dependency {
		public RelationshipKind dependencyKind;
		public String comment;
		public Image icon;
		public Element targetElement;
		
		public Dependency(RelationshipKind dependencyKind, Image icon, Element targetElement, String comment) {
			this.dependencyKind = dependencyKind;			
			this.icon = icon;
			this.targetElement = targetElement;
			this.comment = comment;
		}
	}

	Element getEditorElement();
	IEditorPresenter getPresenter();
	void markAsDirty();
	String getHelpResourceURL();
	void refresh();
	void selectText(EStructuralFeature feature, int startIndex, int endIndex);
	SearchResultWrapper search(SearchParameter param);
	Listener getModifyListener();
	List<Dependency> getDependencies();
	List<Dependency> getDependenciesRelatedBy();
}
