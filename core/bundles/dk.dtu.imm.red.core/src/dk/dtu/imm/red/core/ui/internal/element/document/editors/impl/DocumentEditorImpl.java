package dk.dtu.imm.red.core.ui.internal.element.document.editors.impl;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.EditorSelectionDialog;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.internal.WorkbenchPage;

import dk.dtu.imm.red.core.element.CaptionedImageList;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.element.document.DocumentKind;
import dk.dtu.imm.red.core.element.document.DocumentPackage;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizard;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardPresenter;
import dk.dtu.imm.red.core.ui.element.wizards.impl.CreateElementReferenceWizardImpl;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.element.document.editors.DocumentEditor;
import dk.dtu.imm.red.core.ui.internal.element.document.editors.DocumentEditorPresenter;
import dk.dtu.imm.red.core.ui.internal.element.extensions.ElementLinkActionExtension;

public class DocumentEditorImpl extends BaseEditor implements DocumentEditor {

	/**
	 * Index of the document page.
	 */
	protected int summaryPageIndex = 0;
	protected int contentPageIndex = 1;

	// UI elements
	protected ElementBasicInfoContainer basicInfoContainer;

	protected Text documentInternalRef;
	protected Element documentInternalRefElement;
	protected Text documentExternalRef;
	protected Text documentWebRef;
	protected RichTextEditor documentAbstractEditor;
	protected RichTextEditor documentContentEditor;
	protected MultiImageWidgetPanel multiImageWidgetPanel;

	@Override
	public String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Document.html";
	}

	@Override
	// set the attributes into the presenter and call super.dosave which will
	// call the parent presenter's
	// and the current presenter's save
	public void doSave(IProgressMonitor monitor) {

		DocumentEditorPresenter presenter = (DocumentEditorPresenter) this.presenter;
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		presenter.setDocumentAbstract(documentAbstractEditor.getText());
		presenter.setDocumentInternalRef(documentInternalRefElement);
		presenter.setDocumentExternalRef(documentExternalRef.getText());
		presenter.setDocumentWebRef(documentWebRef.getText());
		presenter.setDocumentTextContent(documentContentEditor.getText());
		presenter.setImageList(multiImageWidgetPanel.getImagesList());
		presenter.setCaptionList(multiImageWidgetPanel.getCaptionsList());
		super.doSave(monitor);
	}

	@Override
	// set the presenter with the element
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);

		presenter = new DocumentEditorPresenterImpl(this, (Document) element);
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	// Fill the UI Elements
	protected void fillInitialData() {
		Document document = (Document) element;

		DocumentKind[] kinds = DocumentKind.values();
		String[] kindStrings = new String[kinds.length];
		for(int i=0;i<kinds.length;i++){
			kindStrings[i] = kinds[i].getName();
		}
		basicInfoContainer.fillInitialData(document, kindStrings);

		if (document.getAbstract() != null) {
			documentAbstractEditor.setText(document.getAbstract().toString());
		}
		if (document.getInternalRef() != null) {
			Element referredElement = document.getInternalRef();
			documentInternalRef.setText(referredElement.getName());
			documentInternalRefElement = referredElement;
		}
		if (document.getExternalFileRef() != null) {
				documentExternalRef.setText(document.getExternalFileRef()
						.toString());
		}
		if (document.getWebRef() != null) {
			if (document.getWebRef().isEmpty()) {
				documentWebRef.setText("http://");
			} else {
				documentWebRef.setText(document.getWebRef()
						.toString());
			}
		}
		if (document.getTextContent() != null) {
			documentContentEditor.setText(document.getTextContent().toString());
		}
		if (document.getCaptionedImageList() != null) {
			CaptionedImageList imageWithCaptionList = document
					.getCaptionedImageList();
			multiImageWidgetPanel.setAllImagesAndCaptions(imageWithCaptionList);
		}

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	protected void createPages() {
		summaryPageIndex = addPage(createDocumentSummaryPage(getContainer()));
		setPageText(summaryPageIndex, "Summary");

		contentPageIndex = addPage(createDocumentContentPage(getContainer()));
		setPageText(contentPageIndex, "Content");

		super.createPages();

		fillInitialData();
	}

	private Composite createDocumentSummaryPage(Composite parent) {

		Composite composite = new Composite(parent, SWT.NULL);
		composite.setLayout(new GridLayout(4, false));

		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=4;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (Element) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);



//		Label lblId = new Label(composite, SWT.NONE);
//		lblId.setText("ID");
//
//		documentID = new Text(composite, SWT.BORDER);
//		GridData gd_textID = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1,
//				1);
//		gd_textID.widthHint = 196;
//		documentID.setLayoutData(gd_textID);
//		documentID.addListener(SWT.Modify, modifyListener);
//
//		new Label(composite, SWT.NONE);
//		new Label(composite, SWT.NONE);
//
//		Label lblTitle = new Label(composite, SWT.NONE);
//		lblTitle.setText("Title");
//
//		documentTitle = new Text(composite, SWT.BORDER);
//		documentTitle.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
//				false, 3, 1));
//		documentTitle.addListener(SWT.Modify, modifyListener);

		Label lblAbstract = new Label(composite, SWT.NONE);
		lblAbstract.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 2, 1));
		lblAbstract.setText("Abstract");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		documentAbstractEditor = new RichTextEditor(composite, SWT.BORDER
				| SWT.WRAP | SWT.V_SCROLL, getEditorSite(), commandListener);
		GridData gd_textAbstract = new GridData(SWT.FILL, SWT.BOTTOM, true,
				false, 4, 1);
		gd_textAbstract.widthHint = 350;
		gd_textAbstract.heightHint = 300;
		gd_textAbstract.minimumWidth = 100;
		documentAbstractEditor.setLayoutData(gd_textAbstract);
		documentAbstractEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		Label lblInternalRef = new Label(composite, SWT.NONE);
		lblInternalRef.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 2, 1));
		lblInternalRef.setText("Reference Inside Project");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		documentInternalRef = new Text(composite, SWT.BORDER);
		GridData gd_textInternalRef = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 2, 1);
		gd_textInternalRef.widthHint = 350;
		documentInternalRef.setEditable(false);
		documentInternalRef.setLayoutData(gd_textInternalRef);
		documentInternalRef.addListener(SWT.Modify, modifyListener);

		Button btnAddInternalRef = new Button(composite, SWT.NONE);
		btnAddInternalRef.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				CreateElementReferenceWizard wizard = new CreateElementReferenceWizardImpl(
						null);
				wizard.init(PlatformUI.getWorkbench(),
						(IStructuredSelection) PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow()
								.getSelectionService().getSelection());

				WizardDialog dialog = new WizardDialog(Display.getCurrent()
						.getActiveShell(), wizard);

				dialog.open();

				Element selectedElement = ((CreateElementReferenceWizardPresenter) wizard
						.getPresenter()).getselectedElement();
				if (selectedElement != null) {
					documentInternalRefElement = selectedElement;
					documentInternalRef.setText(documentInternalRefElement
							.getName());
				}
				else{
					documentInternalRefElement = null;
					documentInternalRef.setText("");
				}
			}
		});
		btnAddInternalRef.setText("Choose");

		Button btnOpenInternalRef = new Button(composite, SWT.NONE);
		btnOpenInternalRef.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (documentInternalRefElement != null) {
					ElementLinkActionExtension
							.elementClicked(documentInternalRefElement);
				}
			}
		});
		btnOpenInternalRef.setText("Open");

		Label lblExternalRef = new Label(composite, SWT.NONE);
		lblExternalRef.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 2, 1));
		lblExternalRef.setText("Reference Outside Project (Local File)");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		documentExternalRef = new Text(composite, SWT.BORDER);
		documentExternalRef.setEnabled(false);
		documentExternalRef.setEditable(false);
		documentExternalRef.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 2, 1));
		documentExternalRef.addListener(SWT.Modify, modifyListener);

		Button btnChoose = new Button(composite, SWT.NONE);
		btnChoose.setText("Choose");

		final Shell parentShell = parent.getShell();
		btnChoose.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(parentShell, SWT.OPEN);
				fd.setText("Open");
//				fd.setFilterPath("C:/");
				String[] filterExt = { "*.*" };
				fd.setFilterExtensions(filterExt);
				String selected = fd.open();

				if(selected!=null && !selected.isEmpty()){
					documentExternalRef.setText(selected);
				}
				else{
					boolean resetExternalRefElement = MessageDialog.openConfirm(Display.getCurrent().getActiveShell(),
							"Clear reference", "You have not selected any element. Do you want to clear your reference?");
					if(resetExternalRefElement){
						documentExternalRef.setText("");
					}

				}
			}
		});

		Button btnOpenLocalFile = new Button(composite, SWT.NONE);
		btnOpenLocalFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String externalDocPath = documentExternalRef.getText();
				if (externalDocPath != null && !externalDocPath.isEmpty()) {
					 String extension = externalDocPath.substring(externalDocPath.lastIndexOf("."));
					 if(extension!=null && !extension.isEmpty()){
						 Program p = Program.findProgram(extension);
						 Program.launch(externalDocPath);
					 }
					 else {
							getSite().getShell().getDisplay()
									.asyncExec(new Runnable() {
										public void run() {
											MessageDialog
													.openError(
															getSite().getShell(),
															"Unable to open the file with the default editor",
															"Unable to open the file with the default editor");
										}
									});
						}
				}
			}
		});
		btnOpenLocalFile.setText("Open");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Button btnOpenLocalFileWith = new Button(composite, SWT.NONE);
		btnOpenLocalFileWith.setText("Open With...");
		btnOpenLocalFileWith.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				String externalDocPath = documentExternalRef.getText();
				if (externalDocPath != null && !externalDocPath.isEmpty()) {
					IWorkbench wb = PlatformUI.getWorkbench();
					IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
					File f = new File(externalDocPath);

					if (f.isFile() && f.exists()) {
						IPath ipath = new Path(f.getAbsolutePath());
						IFileStore fileLocation = EFS.getLocalFileSystem()
								.getStore(ipath);
						FileStoreEditorInput fileStoreEditorInput = new FileStoreEditorInput(
								fileLocation);
						IWorkbenchPage page = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getActivePage();
						EditorSelectionDialog dialog = new EditorSelectionDialog(
								win.getShell());
						if (dialog.open() == Window.OK) {
							IEditorDescriptor editor = dialog
									.getSelectedEditor();
							if (editor != null) {
								try {
									if (editor.isOpenExternal()) {
										((WorkbenchPage) page)
												.openEditorFromDescriptor(
														fileStoreEditorInput,
														editor, true, null);
									} else {
										String editorId = editor == null ? IEditorRegistry.SYSTEM_EXTERNAL_EDITOR_ID
												: editor.getId();
										int MATCH_BOTH = IWorkbenchPage.MATCH_INPUT
												| IWorkbenchPage.MATCH_ID;
										((WorkbenchPage) page).openEditor(
												fileStoreEditorInput, editorId,
												true, MATCH_BOTH);
									}
								} catch (PartInitException e) {
									e.printStackTrace(); LogUtil.logError(e);;
								}

							}
						}
					} else {
						getSite().getShell().getDisplay()
								.asyncExec(new Runnable() {
									public void run() {
										MessageDialog
												.openError(
														getSite().getShell(),
														"Unable to open the file with the selected editor",
														"Unable to open the file with the selected editor");
									}
								});
					}
				}

			}

		});

		Label lblWebReference = new Label(composite, SWT.NONE);
		lblWebReference.setText("Reference from Web");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		documentWebRef = new Text(composite, SWT.BORDER);
		documentWebRef.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 2, 1));
		new Label(composite, SWT.NONE);
		documentWebRef.addListener(SWT.Modify, modifyListener);
		documentWebRef.setText("http://");

		Button btnOpenWeb = new Button(composite, SWT.NONE);
		btnOpenWeb.setText("Open");

		btnOpenWeb.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				String webPath = documentWebRef.getText();
				if (webPath != null && !webPath.isEmpty()) {
					URL link;
					try {
						link = new URL(webPath);
						PlatformUI.getWorkbench().getBrowserSupport()
								.getExternalBrowser().openURL(link);
					} catch (MalformedURLException e1) {
						getSite().getShell().getDisplay()
								.asyncExec(new Runnable() {
									public void run() {
										MessageDialog
												.openError(
														getSite().getShell(),
														"Provided URL is not a valid URL syntax",
														"Provided URL is not a valid URL syntax");
									}
								});
					} catch (PartInitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace(); LogUtil.logError(e);
					}
				}
			}
		});

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	private Composite createDocumentContentPage(Composite parent) {

		Composite composite = new Composite(parent, SWT.NULL);
		composite.setLayout(new GridLayout(1, false));

		Label lblTextSection = new Label(composite, SWT.NONE);
		lblTextSection.setText("Text Section");

		documentContentEditor = new RichTextEditor(composite, SWT.BORDER
				| SWT.WRAP | SWT.V_SCROLL, null, null);
		GridData gdData = new GridData(SWT.FILL, SWT.BOTTOM, true, false, 4, 1);
		gdData.widthHint = 800;
		gdData.heightHint = 300;
		documentContentEditor.setLayoutData(gdData);
		documentContentEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		multiImageWidgetPanel = new MultiImageWidgetPanel(composite, 0,
				new Point(350, 350), 2);
		GridData gdData2 = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1);
		gdData2.widthHint = 800;
		multiImageWidgetPanel.setLayoutData(gdData2);
		multiImageWidgetPanel.addListener(SWT.Modify, modifyListener);

		ScrolledComposite sc = SWTUtils.wrapInScrolledComposite(parent,
				composite);
		multiImageWidgetPanel.setParentScrolledComposite(sc);

		return sc;
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {

		SearchResultWrapper wrapper = ElementFactory.eINSTANCE
				.createSearchResultWrapper();
		wrapper.setElement(element);

		Map<EStructuralFeature, String> attributesToSearch = new HashMap<EStructuralFeature, String>();
		super.addBasicAttributesToSearch(basicInfoContainer, attributesToSearch);
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__ABSTRACT,
				documentAbstractEditor.getText());
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__INTERNAL_REF,
				documentInternalRef.getText());
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__EXTERNAL_FILE_REF,
				documentExternalRef.getText());
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__WEB_REF,
				documentWebRef.getText());
		attributesToSearch.put(DocumentPackage.Literals.DOCUMENT__TEXT_CONTENT,
				documentContentEditor.getText());

		// Now, search through all attributes, recording all search hits along
		// the way
		for (Entry<EStructuralFeature, String> entry : attributesToSearch
				.entrySet()) {
			EStructuralFeature feature = entry.getKey();
			String value = entry.getValue();

			int index = 0;
			int hitIndex = value.indexOf(param.getSearchString(), index);
			// find all occurences of the search string in this attribute
			while (hitIndex != -1) {
				SearchResult result = ElementFactory.eINSTANCE
						.createSearchResult();
				result.setAttribute(feature);
				result.setStartIndex(hitIndex);
				result.setEndIndex(hitIndex + param.getSearchString().length()
						- 1);
				wrapper.getElementResults().add(result);

				hitIndex = value.indexOf(param.getSearchString(),
						result.getEndIndex());
			}
		}
		// if we didn't find any results, return an empty wrapper
		if (wrapper.getElementResults().isEmpty()) {

			return null;
		} else {
			return wrapper;
		}
	}

	@Override
	public void selectText(EStructuralFeature feature, int startIndex,
			int endIndex) {
		super.selectTextBasicInfo(feature,startIndex,endIndex,summaryPageIndex,basicInfoContainer);
		if (feature == DocumentPackage.Literals.DOCUMENT__ABSTRACT) {
			setActivePage(summaryPageIndex);
			documentAbstractEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] { "" + startIndex, "" + endIndex });
		} else if (feature == DocumentPackage.Literals.DOCUMENT__INTERNAL_REF) {
			setActivePage(summaryPageIndex);
			documentInternalRef.setSelection(startIndex, endIndex);
		} else if (feature == DocumentPackage.Literals.DOCUMENT__EXTERNAL_FILE_REF) {
			setActivePage(summaryPageIndex);
			documentExternalRef.setSelection(startIndex, endIndex);
		} else if (feature == DocumentPackage.Literals.DOCUMENT__WEB_REF) {
			setActivePage(summaryPageIndex);
			documentWebRef.setSelection(startIndex, endIndex);
		} else if (feature == DocumentPackage.Literals.DOCUMENT__TEXT_CONTENT) {
			setActivePage(summaryPageIndex);
			documentContentEditor.executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] { "" + startIndex, "" + endIndex });
		}
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.core.document.documenteditor";
	}

}
