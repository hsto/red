package dk.dtu.imm.red.core.ui.internal.element.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class ChangeWorkspacePasswordHandler extends AbstractHandler
		implements
			IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		
		String oldPassword = workspace.getPassword();
		if (oldPassword == null) {
			oldPassword = "";
		}
		
		
		InputDialog inputDialog = new InputDialog(HandlerUtil.getActiveShell(event), 
				"Change Workspace Password", "Enter the new workspace password", 
				oldPassword, null);
		
		if (inputDialog.open() == Dialog.OK) {
			String newPassword = inputDialog.getValue();
			workspace.setPassword(newPassword);
			workspace.save();
		}
		
		
		return null;
	}

}
