package dk.dtu.imm.red.core.ui.internal.traceability.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.relationship.ElementReference;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.impl.FileImpl;
import dk.dtu.imm.red.core.folder.impl.FolderImpl;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class TraceabilityQueryEngine {

	private Configuration config;

	// set up hash map to navigate fast over all elements in the workspace
	private HashMap<String, Element> elements;
	private HashMap<Element, Integer> depthMap;
	private HashMap<Element, Element> previous;

	public TraceabilityQueryEngine() {
	}

	public boolean loadElementsFromWorkspace() {
		FileImpl projectFile = getProjectResource();
		// TODO: Check for possible occurrence of errors

		if (projectFile == null)
			return false;

		scanProject(projectFile);

		if (elements != null)
			return true;
		else
			return false;
	}

	public Configuration getConfiguration() {
		return config;
	}

	public void setConfiguration(Configuration config) {
		this.config = config;
	}

	/*
	 * Recursive creation of hash map of whole project content.
	 */
	private void scanProject(FileImpl projectFile) {
		Object[] container = projectFile.getContents().toArray();
		Iterator<Element> folderContent = ((FolderImpl) container[0]).getContents().iterator();
		Element pointer;
		elements = new HashMap<>();

		// TODO: Finalize
		while (folderContent.hasNext()) {
			pointer = folderContent.next();
			elements.put(pointer.getElementHashId(), pointer);
		}
	}

	private FileImpl getProjectResource() {
		EList<Project> projects = WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects();
		if (projects != null && !projects.isEmpty()) {

			Project project = projects.get(0);
			EList<File> fileList = project.getListofopenedfilesinproject();

			ResourceSet resourceSet = project.getResourceSet();

			URI fileUri = URI.createFileURI(fileList.get(0).getUri());
			Resource resource = resourceSet.getResource(fileUri, true);

			return (FileImpl) resource.getContents().get(0);
		} else {
			return null;
		}
	}

	public List<Object> query(Element startElement) {
		Queue<Element> queue = new LinkedList<Element>();
		queue.add(startElement);
		List<Object> result = new ArrayList<>();
		Set<Element> visitedElements = new HashSet<>();
		previous = new HashMap<>();
		depthMap = new HashMap<>();

		previous.put(startElement, null);
		depthMap.put(startElement, 0);

		// TODO: Check config if initialized, else throw exception
		
		if (config != null) {
			if (config.isStart() && config.isJoin()) {
				if ((!config.isReverseElementFilter() && isMember(config.getElementKindFilter(), startElement))
						|| (config.isReverseElementFilter() && !isMember(config.getElementKindFilter(), startElement)))
					result.add(startElement);
			} else if (config.isStart() && !config.isJoin()) {
				if ((!config.isReverseElementFilter() && isMember(config.getElementKindFilter(), startElement))
						|| (config.isReverseElementFilter()
								&& !isMember(config.getElementKindFilter(), startElement))) {
					Set<Element> level0 = new HashSet<>();
					level0.add(startElement);
					result.add(level0);
				}
			}

			return recursiveBreadthFirstSearch(queue, result, visitedElements, config.getRelationshipKindFilter(),
					config.getElementKindFilter(), config.getDepthLimit(), config.isReverseRelationshipFilter(),
					config.isReverseElementFilter(), config.isJoin(), config.isStart());
		} else
			return null;

	}

	/**
	 * 
	 * /** Find closure containing all elements with the given parameters on the
	 * requirements artifact graph. Performs search with depth limitation.
	 *
	 * 
	 * @param queue
	 * @param result
	 * @param visitedElements
	 *            Elements that have already been visited (initialize empty)
	 * @param relationshipKindFilter
	 *            Relationship kinds that have to be filtered
	 * @param elementKindFilter
	 *            Element kinds that have to be filtered
	 * @param depthLimit
	 *            Limit for the depth of the search
	 * @param reverseRelationshipFilter
	 *            True, if filter should excluded the filtered kinds
	 * @param join
	 *            True, if all results are written in the same list. False, if
	 *            for each depth level a separate list shall be created.
	 * @param start
	 * @return
	 */
	private List<Object> recursiveBreadthFirstSearch(Queue<Element> queue, List<Object> result,
			Set<Element> visitedElements, Set<RelationshipKind> relationshipKindFilter, Set<String> elementKindFilter,
			int depthLimit, boolean reverseRelationshipFilter, boolean reverseElementFilter, boolean join,
			boolean start) {

		Element pointer = queue.poll();
		int depth;

		// If no element is left to visit, the closure for the current element
		// is fulfilled
		if (pointer == null)
			return result;

		depth = depthMap.get(pointer);
		visitedElements.add(pointer);

		// Check if depth limit has been reached
		if (depth == depthLimit)
			return result;

		// Check for active and passive relationships if the related elements
		// are needed to be visit or if they are excluded by the filters

		// For circle detection every element that has been found has to be
		// added to the result list
		for (ElementRelationship e : pointer.getRelatesTo()) {

			if (((!reverseRelationshipFilter && !relationshipKindFilter.isEmpty()
					&& !isMember(relationshipKindFilter, ((ElementReference) e).getRelationshipKind()))
					|| (reverseRelationshipFilter && !relationshipKindFilter.isEmpty()
							&& isMember(relationshipKindFilter, ((ElementReference) e).getRelationshipKind())))
					|| ((!reverseElementFilter && !elementKindFilter.isEmpty()
							&& !isMember(elementKindFilter, e.getToElement()))
							|| (reverseElementFilter && !elementKindFilter.isEmpty()
									&& isMember(elementKindFilter, e.getToElement()))))
				continue;

			// If list is to be separated into levels
			// and checked element is not the element from which the pointer has
			// been discovered
			// and the element has not been visited yet
			if ((!join && previous.get(e.getFromElement()) != null
					&& e.getToElement() != previous.get(e.getFromElement())
					|| (!join && previous.get(e.getFromElement()) == null))) {
				if ((start && (result.size() < depth + 2)) || (!start && (result.size() < depth + 1))) {
					Set<Element> elementList = new HashSet<>();
					elementList.add(e.getToElement());
					result.add(elementList);
				} else {
					@SuppressWarnings("unchecked")
					Set<Element> elementList = (Set<Element>) result.get(depth + 1);

					// Only one occurrence per depth level is recorded
					if (!elementList.contains(e.getToElement()))
						elementList.add(e.getToElement());
				}

				// else just create normal list
			} else if (((previous.get(e.getFromElement()) == null) || (previous.get(e.getFromElement()) != null
					&& e.getToElement() != previous.get(e.getFromElement()))) && !result.contains(e.getToElement())
					&& !visitedElements.contains(e.getToElement())) {
				result.add(e.getToElement());
			}

			// If element has not been visited yet, add to queue
			if (!visitedElements.contains(e.getToElement())) {
				queue.add(e.getToElement());
				previous.put(e.getToElement(), pointer);
				depthMap.put(e.getToElement(), depth + 1);
			}
		}

		for (ElementRelationship e : pointer.getRelatedBy()) {

			// If element or relationship is filtered by defined filters, skip
			// it
			// Check if the relationship is filtered out,
			if (((!reverseRelationshipFilter && !relationshipKindFilter.isEmpty()
					&& !isMember(relationshipKindFilter,
							RelationshipKind.getReverseRelationshipKind(((ElementReference) e).getRelationshipKind())))
					// the filter excludes contained types
					|| (reverseRelationshipFilter && !relationshipKindFilter.isEmpty()
							&& isMember(relationshipKindFilter,
									RelationshipKind
											.getReverseRelationshipKind(((ElementReference) e).getRelationshipKind()))))
					// or if the element is filtered out
					|| ((!reverseElementFilter && !elementKindFilter.isEmpty()
							&& !isMember(elementKindFilter, e.getFromElement()))
							|| (reverseElementFilter && !elementKindFilter.isEmpty()
									&& isMember(elementKindFilter, e.getFromElement()))))

				continue;

			// Record all relatives from the current element
			if ((!join && previous.get(e.getToElement()) != null
					&& e.getFromElement() != previous.get(e.getToElement()))
					|| (!join && previous.get(e.getToElement()) == null)) {
				// Check for depth + 2 since first entry is empty or
				if ((start && (result.size() < depth + 2)) || (!start && (result.size() < depth + 1))) {
					Set<Element> elementList = new HashSet<>();
					elementList.add(e.getFromElement());
					result.add(elementList);
				} else {
					@SuppressWarnings("unchecked")
					Set<Element> elementList = (Set<Element>) result.get(depth + 1);

					// Only one occurrence per depth level is recorded
					if (!elementList.contains(e.getFromElement()))
						elementList.add(e.getFromElement());
				}

			} else if ((previous.get(e.getToElement()) != null && e.getFromElement() != previous.get(e.getToElement())
					&& !result.contains(e.getFromElement()) && !visitedElements.contains(e.getFromElement()))
					|| previous.get(e.getToElement()) == null) {
				result.add(e.getFromElement());
			}

			if (!visitedElements.contains(e.getFromElement())) {
				previous.put(e.getFromElement(), pointer);
				queue.add(e.getFromElement());
			}
			depthMap.put(e.getFromElement(), depth + 1);
		}

		// Else next queued element has to be checked
		recursiveBreadthFirstSearch(queue, result, visitedElements, relationshipKindFilter, elementKindFilter,
				depthLimit, reverseRelationshipFilter, reverseElementFilter, join, start);

		return result;
	}

	public List<Element> cut(List<Object> traceA, List<Object> traceB) {
		// TODO: check for cut of two element traces (both joined and unjoined
		// lists)
		return null;
	}

	/**
	 * 
	 * @param elementKindFilter
	 * @param element
	 * @return
	 */
	public boolean isMember(Set<String> elementKindFilter, Element element) {
		if (!elementKindFilter.isEmpty()) {
			return elementKindFilter.contains(element.eClass().getName());
		} else {
			return true;
		}
	}

	/**
	 * Check if element relationship is element of filtered relationship kinds.
	 * 
	 * @param relationshipKindFilter
	 * @param element
	 * @return
	 */
	public boolean isMember(Set<RelationshipKind> relationshipKindFilter, RelationshipKind element) {
		if (!relationshipKindFilter.isEmpty()) {
			return relationshipKindFilter.contains(element);
		} else {
			return true;
		}
	}
}
