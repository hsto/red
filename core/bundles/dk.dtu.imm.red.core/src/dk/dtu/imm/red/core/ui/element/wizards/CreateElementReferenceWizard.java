package dk.dtu.imm.red.core.ui.element.wizards; 
import org.eclipse.ui.INewWizard;  
import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface CreateElementReferenceWizard extends INewWizard, IBaseWizard { 
}
