package dk.dtu.imm.red.core.ui.internal.file.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.IHandlerService;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.file.wizards.impl.CreateFileWizardImpl;
import dk.dtu.imm.red.core.ui.internal.project.editors.impl.ProjectEditorImpl;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

/**
 * Handler for the Create New File command. Opens a wizard, which guides the
 * user through creating a new file (name and placement).
 * 
 * @author Ahmed Fikry
 * 
 */
public class CreateFileHandler extends AbstractHandler implements IHandler {

	public static String ID = "dk.dtu.imm.red.core.file.newfile";

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		if (!(WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
				.getCurrentlyOpenedProjects().size() > 0)) {
			MessageDialog dialog = new MessageDialog(
					HandlerUtil.getActiveShell(event),
					"No Open Project",
					null,
					"There is no project opened yet, Do you want to create a new one or open an existing one?",
					MessageDialog.CONFIRM, new String[] { "New Project",
							"Open Existing Project" }, 0);
			int result = dialog.open();
			IWorkbenchWindow window = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow();

			IHandlerService handlerService = (IHandlerService) window
					.getService(IHandlerService.class);
			try {
				if (result==0) {
					handlerService.executeCommand(
							"dk.dtu.imm.red.core.project.newproject", null);
				} else if(result==1){
					handlerService.executeCommand(
							"dk.dtu.imm.red.core.openproject", null);
				}
			} catch (ExecutionException e) {
				e.printStackTrace();
				LogUtil.logError(e);
			} catch (NotDefinedException e) {
				e.printStackTrace();
				LogUtil.logError(e);
			} catch (NotEnabledException e) {
				e.printStackTrace();
				LogUtil.logError(e);
			} catch (NotHandledException e) {
				e.printStackTrace();
				LogUtil.logError(e);
			}
			return null;
		}

		CreateFileWizardImpl wizard = new CreateFileWizardImpl();
		ISelection currentSelection = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService()
				.getSelection();
		wizard.init(PlatformUI.getWorkbench(),
				(IStructuredSelection) currentSelection);

		WizardDialog dialog = new WizardDialog(
				HandlerUtil.getActiveShell(event), wizard);
		dialog.create();

		dialog.open();
		ProjectEditorImpl.refreshTable();

		return null;
	}

}
