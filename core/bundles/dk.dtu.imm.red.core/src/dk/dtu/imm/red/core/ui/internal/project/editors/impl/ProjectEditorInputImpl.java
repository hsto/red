package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;

public class ProjectEditorInputImpl extends BaseEditorInput{
	
	public ProjectEditorInputImpl(Project project) {
		super(project);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Document editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for document " + element.getName();
	}

}
