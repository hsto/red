package dk.dtu.imm.red.core.ui.internal.navigation;

import org.eclipse.core.expressions.PropertyTester;

import dk.dtu.imm.red.core.ui.internal.NavigationManager;

public class NavigationPropertyTester extends PropertyTester {
	
	public static final String PROPERTY_CAN_GO_BACK = "canGoBack";
	public static final String PROPERTY_CAN_GO_FORWARD = "canGoForward";

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		if (PROPERTY_CAN_GO_BACK.equals(property)) {
			return NavigationManager.backHistorySize() > 0;
		} else if (PROPERTY_CAN_GO_FORWARD.equals(property)) {
			return NavigationManager.forwardHistorySize() > 0;
		}
		return false;
	}

}
