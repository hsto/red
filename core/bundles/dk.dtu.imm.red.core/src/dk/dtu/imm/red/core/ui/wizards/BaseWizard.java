package dk.dtu.imm.red.core.ui.wizards;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

public abstract class BaseWizard extends Wizard 
	implements IWizard, IBaseWizard {

	/**
	 * The presenter for this wizard.
	 */
	protected IWizardPresenter presenter;

	@Override
	public IWizardPresenter getPresenter() {
		return presenter;
	}
	
	@Override
	public void pageOpenedOrClosed(IWizardPage fromPage) {
	}

}
