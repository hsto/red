package dk.dtu.imm.red.core.ui.internal.element.handlers;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.RadioState;
import org.eclipse.ui.menus.UIElement;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.ui.internal.element.operations.ChangeLockOperation;

public class LockHandler extends AbstractHandler implements IHandler, IElementUpdater {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
	
		String currentState = event.getParameter(RadioState.PARAMETER_ID);
		
		if (currentState.equals(event.getCommand().getState(RadioState.STATE_ID).getValue())) {
			return null;
		}
		
		ISelection selection = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		Element selectedElement = null;
		if (selection != null && selection instanceof IStructuredSelection) {
			selectedElement = (Element) ((IStructuredSelection) selection).getFirstElement();
		} else {
			return null;
		}
		
		LockType oldLock = selectedElement.getLockStatus();
		LockType newLock = null;
		
		if (currentState.equals(LockType.LOCK_NOTHING.getLiteral())) {
			newLock = LockType.LOCK_NOTHING;
		} else if (currentState.equals(LockType.LOCK_COMMENTING_ONLY.getLiteral())) {
			newLock = LockType.LOCK_COMMENTING_ONLY;
		} else if (currentState.equals(LockType.LOCK_CONTENTS.getLiteral())) {
			newLock = LockType.LOCK_CONTENTS;
		} else {
			newLock = LockType.LOCK_LIKE_CONTAINER;
		}
		
		ChangeLockOperation operation = 
				new ChangeLockOperation(oldLock, newLock, selectedElement);
		operation.addContext(PlatformUI.getWorkbench().getOperationSupport()
				.getUndoContext());
		OperationHistoryFactory.getOperationHistory().execute(operation, null, null);

		HandlerUtil.updateRadioState(event.getCommand(), currentState);
		return null;
	}
	
	@Override
	public void updateElement(UIElement element, Map parameters) {
		
		if (element == null) {
			return;
		}
		
		ISelection selection = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		Element selectedElement = null;
		if (selection != null && selection instanceof IStructuredSelection) {
			selectedElement = (Element) ((IStructuredSelection) selection).getFirstElement();
		} else {
			return;
		}
		
		if (selectedElement == null) {
			return;
		}
		
		if (parameters.get("org.eclipse.ui.commands.radioStateParameter").equals(LockType.LOCK_NOTHING.getLiteral())) {
			element.setChecked(selectedElement.getLockStatus() == LockType.LOCK_NOTHING);
		} else if (parameters.get("org.eclipse.ui.commands.radioStateParameter").equals(LockType.LOCK_COMMENTING_ONLY.getLiteral())) {
			element.setChecked(selectedElement.getLockStatus() == LockType.LOCK_COMMENTING_ONLY);
		} else if (parameters.get("org.eclipse.ui.commands.radioStateParameter").equals(LockType.LOCK_CONTENTS.getLiteral())) {
			element.setChecked(selectedElement.getLockStatus() == LockType.LOCK_CONTENTS);
		} else if (parameters.get("org.eclipse.ui.commands.radioStateParameter").equals(LockType.LOCK_LIKE_CONTAINER.getLiteral())) {
			element.setChecked(selectedElement.getLockStatus() == LockType.LOCK_LIKE_CONTAINER ||
					selectedElement.getLockStatus() == null);
		}
	}

}
