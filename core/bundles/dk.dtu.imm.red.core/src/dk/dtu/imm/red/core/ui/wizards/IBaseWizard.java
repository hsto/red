package dk.dtu.imm.red.core.ui.wizards;

import org.eclipse.jface.wizard.IWizardPage;

public interface IBaseWizard {
	
	/**
	 * Get the presenter for this wizard.
	 * @return the wizard's presenter
	 */
	IWizardPresenter getPresenter();
	
	void pageOpenedOrClosed(IWizardPage fromPage);

}
