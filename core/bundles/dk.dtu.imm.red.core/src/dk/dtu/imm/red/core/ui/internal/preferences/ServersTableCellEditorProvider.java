package dk.dtu.imm.red.core.ui.internal.preferences;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.CellEditor;
import org.agilemore.agilegrid.DefaultCellEditorProvider;
import org.agilemore.agilegrid.editors.ComboBoxCellEditor;
import org.eclipse.swt.SWT;

import dk.dtu.imm.red.core.ui.AgileTableTextCellEditor;

/**
 * The Cell Editor Provider for the associations table.
 * The primary purpose of this class is to tell which columns are editable.
 * @author Jakob Kragelund
 *
 */
public class ServersTableCellEditorProvider extends DefaultCellEditorProvider {

	public ServersTableCellEditorProvider(AgileGrid agileGrid) {
		super(agileGrid);
	}
	
	@Override
	public CellEditor getCellEditor(int row, int col) {
		if(col==ServersTableLayoutAdviser.SERVICE_COLUMN){
			String[] choices = {"http", "https", "spdy"};
			return new ComboBoxCellEditor(agileGrid,choices,SWT.NONE);
		}
		else{
			return new AgileTableTextCellEditor(agileGrid);
		}
	}
	
	@Override
	public boolean canEdit(final int row, final int col) {
		// Only the comment-column is editable.
		return true;
	}
	
	
	
}
