package dk.dtu.imm.red.core.ui.internal.folder.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class NewFolderOperation extends AbstractOperation {
	
	protected String name;
	protected String path;
	protected Group parent;
	protected Folder folder;

	public NewFolderOperation(String folderName, Group parent, String path) {
		super("New Folder");
		
		this.name = folderName;
		this.path = path;
		this.parent = parent;
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		
		if (folder == null) {
			folder = FolderFactory.eINSTANCE.createFolder();
			folder.setName(name);
		}
		
		if (parent != null) {
			parent.getContents().add(folder);
//			folder.setParent(parent);
		} 
		
		folder.save();
		
		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		
		if(parent != null) {
			parent.getContents().remove(folder);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}
	
	@Override
	public boolean canUndo() {
		return parent != null;
	}

}
