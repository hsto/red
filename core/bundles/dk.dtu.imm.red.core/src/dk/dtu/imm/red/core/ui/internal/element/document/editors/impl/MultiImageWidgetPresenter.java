package dk.dtu.imm.red.core.ui.internal.element.document.editors.impl;

import java.util.ArrayList;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.presenters.BasePresenter;


//Implementation of MultiImageWidgetPresenter, large part of the code are taken from ImagePanelWidgetPresenter
//and adapted to multiple panel
public class MultiImageWidgetPresenter extends BasePresenter {

	ArrayList<Image> imageList;
	MultiImageWidgetPanel multiImageWidgetPanel;
	
	
	public MultiImageWidgetPresenter(MultiImageWidgetPanel widget, ArrayList<Image> imageList){
		this.multiImageWidgetPanel = widget;
		this.imageList = imageList;
	}
	
	public ArrayList<Image> getImageList(){
		return imageList;
	}
	
	public Image getImage(int i){
		if(imageList.size()>i){
			return imageList.get(i);
		}
		return null;
	}
	
	public void addImage(Image image){
		imageList.add(image);
	}
	
	public void removeImage(int i){
		imageList.remove(i);
	}

	public void loadNewImage(int index) {
		changeImage(index, true);
		multiImageWidgetPanel.refreshImage(index);
	}
	
	/**
	 * Change the image, either to a new image loaded from the
	 * file system, or the default image.
	 * @param openNewImage True if a new image should be loaded, false if the
	 * default image should be used.
	 */
	private void changeImage(int index, final boolean openNewImage) {
		ChangeImageInMultiImageWidgetOperation operation = 
				new ChangeImageInMultiImageWidgetOperation(this, index, openNewImage);
		operation.addContext(getUndoContext());

		try {
			OperationHistoryFactory.getOperationHistory()
				.execute(operation, null, null);
		} catch (ExecutionException e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

	/**
	 * Sets the image in the widget to the specified image.
	 * @param newImage the new image to be shown in the widget
	 */
	public void setImage(int index, final Image newImage) {
		if(index>=imageList.size()){
			imageList.add(imageList.size(),scaleImageToBounds(index,newImage));
		}
		else{
			imageList.set(index,scaleImageToBounds(index,newImage));
		}
		multiImageWidgetPanel.refreshImage(index);
	}
	
	/**
	 * Scales the given image to fit inside the bounds of the widget.
	 * @param image the image to scale to the widget
	 * @return the scaled image
	 */
	private Image scaleImageToBounds(int index, Image image) {
		if (image == null) {
			return image;
		}
		
		int width = image.getBounds().width;
		int height = image.getBounds().height;
		int canvasWidth = multiImageWidgetPanel.getImageCanvas(index).getBounds().width;
		int canvasHeight = multiImageWidgetPanel.getImageCanvas(index).getBounds().height;
		
		if(canvasWidth==0 || canvasHeight==0){
			canvasWidth = multiImageWidgetPanel.getDefaultCanvasSize().x;
			canvasHeight = multiImageWidgetPanel.getDefaultCanvasSize().y;
		}
		
		if (width > canvasWidth || height > canvasHeight) {
			int newWidth = width;
			int newHeight = height;
			float ratio = ((float) width) / ((float) height);

			if (width > canvasWidth) {
				newWidth = canvasWidth;
				newHeight = (int) (((float) newWidth) / ratio);
			}
			if (newHeight > canvasHeight) {
				newHeight = canvasHeight;
				newWidth = (int) (((float) newHeight) * ratio);
			}

			image = new Image(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell().getDisplay(), image
					.getImageData().scaledTo(newWidth, newHeight));
		}
		return image;
	}
	
	public int centerImageHorizontal(int indexOfImage){
		
		int xCoordinat = 0;
		
		Image image = imageList.get(indexOfImage);
		if (image != null){

			int imageWidth = image.getBounds().width;
			int canvasWidth = multiImageWidgetPanel.getImageCanvas(indexOfImage).getBounds().width;

			if (imageWidth < canvasWidth){
				xCoordinat = (canvasWidth - imageWidth) / 2;
			}
		}
		return xCoordinat;
	}
	
	public int centerImageVertical(int indexOfImage) {
		
		int yCoordinat = 0;
		Image image = imageList.get(indexOfImage);
		if (image != null){
		
			int imageHeight = image.getBounds().height;
			int canvasHeight = multiImageWidgetPanel.getImageCanvas(indexOfImage).getBounds().height;

			if (imageHeight < canvasHeight){
				yCoordinat = (canvasHeight - imageHeight) / 2;
			}
		}
		
		return yCoordinat;
	}

	public boolean isDisposed() {
		return multiImageWidgetPanel.isDisposed();
	}

}
