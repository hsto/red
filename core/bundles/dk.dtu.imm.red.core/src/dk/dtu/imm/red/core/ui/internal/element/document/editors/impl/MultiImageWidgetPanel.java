package dk.dtu.imm.red.core.ui.internal.element.document.editors.impl;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.emf.common.util.EList;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import dk.dtu.imm.red.core.element.CaptionedImage;
import dk.dtu.imm.red.core.element.CaptionedImageList;
import dk.dtu.imm.red.core.ui.SWTUtils;

public class MultiImageWidgetPanel extends Composite {
	
	private Button btnNewPictureSection;
	private MultiImageWidgetPresenter presenter;
	private Composite mainPanelComposite ;
	private Listener listener;
	
	
	//List of UI component to keep track of
	private ArrayList<Composite> imagePanelList = new ArrayList<Composite>();
	private ArrayList<Canvas> imageCanvasList = new ArrayList<Canvas>();
	private ArrayList<Text> imageCaptionList = new ArrayList<Text>();
	private HashMap<Control,Composite> widgetControlsToParent = new HashMap<Control,Composite>();
	private Composite parentContainer;
	
	private ScrolledComposite parentScrolledComposite;
	
	private Point defaultCanvasSize = new Point(350,350);

	private Label lblPictureSection;
	private Text text;
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MultiImageWidgetPanel(Composite parent, int style, Point defaultCanvasSize, int defaultNumOfImage) {
		super(parent, SWT.NONE);
		this.parentContainer = parent;

		
		presenter = new MultiImageWidgetPresenter(this,new ArrayList<Image>());
		imagePanelList = new ArrayList<Composite>();
		this.defaultCanvasSize=defaultCanvasSize;
		
		setLayout(new GridLayout(1, false));

		lblPictureSection = new Label(this, SWT.NONE);
		lblPictureSection.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblPictureSection.setText("Picture Section");
		
		
		mainPanelComposite = new Composite(this, SWT.NONE);
		mainPanelComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		mainPanelComposite.setLayout(new GridLayout(2, true));

		
		btnNewPictureSection = new Button(this, SWT.NONE);
		btnNewPictureSection.setText("Add New Image Panel");
		
		btnNewPictureSection.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addNewImageSection();
				updateScrollPaneView();
			}
		});
		btnNewPictureSection.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		btnNewPictureSection.setText("Add New Picture");
		

		
		for(int i=0;i<defaultNumOfImage;i++){
			addNewImageSection();
		}

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	private void addNewImageSection(){
		
		presenter.addImage(null);

		Composite imageSection = new Composite(mainPanelComposite, SWT.BORDER);
		imagePanelList.add(imageSection);

		GridLayout gl_Composite = new GridLayout(4, false);
		gl_Composite.marginTop = 10;
		gl_Composite.marginRight = 10;
		gl_Composite.marginLeft = 10;
		gl_Composite.marginBottom = 10;
		imageSection.setLayout(gl_Composite);
		
		final Canvas imageCanvas = new Canvas(imageSection, SWT.BORDER);
		GridData gd_imageCanvas = new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1);
		gd_imageCanvas.heightHint = defaultCanvasSize.y;
		gd_imageCanvas.widthHint =defaultCanvasSize.x;
		imageCanvas.setLayoutData(gd_imageCanvas);
		imageCanvasList.add(imageCanvas);
		widgetControlsToParent.put(imageCanvas, imageSection);
		
		Text imageCaption = new Text(imageSection, SWT.BORDER);
		imageCaption.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));
		if(listener!=null){
			imageCaption.addListener(SWT.Modify, listener);
		}

		
		imageCaptionList.add(imageCaption);
		
		Button btnChange = new Button(imageSection, SWT.NONE);
		btnChange.setText("Change");
		btnChange.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = getIndexInThePanel((Control)e.getSource());
				presenter.loadNewImage(index);
				mainPanelComposite.notifyListeners(SWT.Modify, new Event());
			}
		});
		widgetControlsToParent.put(btnChange, imageSection);
		
		Button btnDelete = new Button(imageSection, SWT.NONE);
		btnDelete.setText("Delete");
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {				
				Composite parent = widgetControlsToParent.get(e.getSource());
				int index = getIndexInThePanel((Control)e.getSource());
				presenter.removeImage(index);
				//get the parent, remove all the child from the list and destroy the widget
				for(Control child:parent.getChildren()){
					if(child instanceof Canvas){
						imageCanvasList.remove(child);
					}
					if(child instanceof Text){
						imageCaptionList.remove(child);
					}
					imagePanelList.remove(parent);
				}
				parent.dispose();
				updateScrollPaneView();
			}
		});
		widgetControlsToParent.put(btnDelete, imageSection);

		
		imageCanvas.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				int index = getIndexInThePanel((Control)e.getSource());
				if(index>=0 && index<presenter.getImageList().size()){
					Image image = presenter.getImage(index);
					int xCoordinat = presenter.centerImageHorizontal(index);
					int yCoordinat = presenter.centerImageVertical(index);
					if (image != null) {
						e.gc.drawImage(image, xCoordinat, yCoordinat);
					} else {
						e.gc.drawRectangle(5, 5, 
							imageCanvas.getBounds().width - 14, 
							imageCanvas.getBounds().height - 14);
					}
				}
			}
		});
		updateScrollPaneView();
	}
	
	public void updateScrollPaneView(){
		Point newParentSize = parentContainer.computeSize(
				SWT.DEFAULT, SWT.DEFAULT);

		
		if(parentScrolledComposite!=null){
			parentScrolledComposite.setMinSize(newParentSize);
			parentScrolledComposite.layout(true, true);
		}

		mainPanelComposite.notifyListeners(SWT.Modify, new Event());
	}
	
	public int getIndexInThePanel(Control control){
		Composite parent = widgetControlsToParent.get(control);
		for(int i=0;i<imagePanelList.size();i++){
			if(imagePanelList.get(i)==parent){
				return i;
			}
		}
		return -1;
	}
	
	public Canvas getImageCanvas(int index){
		return imageCanvasList.get(index);
	}
	
	public void refreshImage(int index){
		imageCanvasList.get(index).layout(true);
		imageCanvasList.get(index).redraw();
	}
	
	public void setAllImagesAndCaptions(CaptionedImageList captionedImageList){
		
		EList<CaptionedImage> captionedImages = captionedImageList.getCaptionedImage();
		int index=0;
		for(CaptionedImage captionedImage:captionedImages){
			byte[] imageData = captionedImage.getImageData();
			String caption = captionedImage.getCaption();
			if(index>=imagePanelList.size()){
				addNewImageSection();
			}
			if(imageData!=null){
				presenter.setImage(index, SWTUtils.convertByteImageDataToImage(imageData));
			}
			else{
				presenter.setImage(index, null);
			}
			imageCaptionList.get(index).setText(caption);
			index++;
			
		}
		
	}
	
	public ArrayList<Image> getImagesList(){
		return presenter.getImageList();
	}
	
	public ArrayList<String> getCaptionsList(){
		ArrayList<String> captionStringList = new ArrayList<String>(); 
		for(Text t: imageCaptionList){
			captionStringList.add(t.getText());
		}
		return captionStringList;
	}

	public Point getDefaultCanvasSize(){
		return defaultCanvasSize;
	}

	@Override
	public void addListener(int eventType, Listener listener){
		this.listener = listener;
		mainPanelComposite.addListener(eventType, listener);
		for(Text caption: imageCaptionList){
			caption.addListener(eventType, listener);
		}
	}

	public ScrolledComposite getParentScrolledComposite() {
		return parentScrolledComposite;
	}

	public void setParentScrolledComposite(ScrolledComposite parentScrolledComposite) {
		this.parentScrolledComposite = parentScrolledComposite;
	}

	
	
}

