package dk.dtu.imm.red.core.ui.internal.folder.wizards.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.INewWizard;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.file.wizards.impl.CreateFileWizardPresenterImpl;
import dk.dtu.imm.red.core.ui.internal.folder.wizards.CreateFolderWizard;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class CreateFolderWizardImpl extends BaseNewWizard
		implements CreateFolderWizard, INewWizard {
	
	public CreateFolderWizardImpl() {
		super("Folder", Folder.class, true);
		presenter = new CreateFolderWizardPresenterImpl();
	}
	
	
	@Override
	public void addPages() {
//		super.addPages();
		addPage(displayInfoPage);
	}

	@Override
	public boolean performFinish() {
		
		String name = displayInfoPage.getDisplayName();

		Group parent = null;
		
		String path = "";
		
		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
					instanceof IStructuredSelection) {
			
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();
			
			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}
				
		
		((CreateFolderWizardPresenterImpl) presenter)
			.wizardFinished(name, parent, path);
		
		return true;
	}
	
	@Override
	public boolean canFinish() {
		return super.canFinish() && displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}

}
