package dk.dtu.imm.red.core.ui.internal.project.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewProjectWizard extends IBaseWizard{

	String ID = "dk.dtu.imm.red.core.element.project.newwizard";
}
