package dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid;

import java.util.ArrayList;
import java.util.List;

import org.agilemore.agilegrid.Cell;
import org.agilemore.agilegrid.CellDoubleClickEvent;
import org.agilemore.agilegrid.ICellDoubleClickListener;
import org.agilemore.agilegrid.SWTX;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardFactory;

import org.eclipse.swt.widgets.Label;

public class EAGComposite<T> extends Composite {

	protected EAGPresenter<T> presenter = null;
	protected final ExtendedAgileGrid<T> agReflAgileGrid;
	protected Composite buttonComposite;
	protected Button btnAdd;
	protected Button btnDelete;
	protected Button btnOpen;
	private ElementColumnDefinition[] columns;
	private boolean elementSelectorOnAdd = false;
	protected Button chkRecursiveView;
	protected Composite composite_1;
	protected EAGSumWidget sumWidget1;
	protected EAGSumWidget sumWidget2;
	private String addDialogTitle;
	private String addDialogDescription;

	public EAGPresenter<T> getPresenter() {
		return presenter;
	}

	public ExtendedAgileGrid<T> getAgReflAgileGrid() {
		return agReflAgileGrid;
	}

	public EAGComposite(Composite parent, int style, final Listener modifyListener) {
		this(parent, style, modifyListener, null, null);
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public EAGComposite(Composite parent, int style, final Listener modifyListener, EObject element,
			final Object feature) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		agReflAgileGrid = new ExtendedAgileGrid<T>(this,
				SWT.MULTI | SWTX.COLUMN_SELECTION | SWTX.ROW_SELECTION | SWT.V_SCROLL | SWT.H_SCROLL, modifyListener);

		agReflAgileGrid.setLayout(new GridLayout(1, false));
		agReflAgileGrid.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		buttonComposite = new Composite(this, SWT.NONE);
		buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		buttonComposite.setLayout(new GridLayout(5, false));

		btnAdd = new Button(buttonComposite, SWT.NONE);
		btnAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (elementSelectorOnAdd) {
					Object o = CreateElementReferenceWizardFactory.createElementReference(presenter.getReflectedClass(),
							addDialogTitle, addDialogDescription);
					if (o != null) {
						presenter.AddItem(o);
					}
				} else {
					presenter.AddItem();
				}
				agReflAgileGrid.redraw();
				if (modifyListener != null)
					modifyListener.handleEvent(null);
			}
		});
		btnAdd.setText("Add");

		btnOpen = new Button(buttonComposite, SWT.NONE);
		btnOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				presenter.openItem(agReflAgileGrid.getSelectedIndex(), -1);
			}
		});
		btnOpen.setEnabled(false);
		btnOpen.setText("Open");

		agReflAgileGrid.addCellDoubleClickListener(new ICellDoubleClickListener() {
			@Override
			public void cellDoubleClicked(CellDoubleClickEvent event) {
				int col = -1;
				if (event.getSource() instanceof Cell) {
					col = ((Cell) event.getSource()).column;
				}
				presenter.openItem(agReflAgileGrid.getSelectedIndex(), col);
			}
		});

		btnDelete = new Button(buttonComposite, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				// Confirm Delete action
				int result = messageBox("Confirm action", "Are you sure you want to delete the selected item(s)?",
						SWT.YES | SWT.NO | SWT.ICON_INFORMATION);

				if (result == SWT.NO) {
					return;
				}

				presenter.DeleteItem(agReflAgileGrid.getSelectedIndex());

				agReflAgileGrid.redraw();
				if (modifyListener != null)
					modifyListener.handleEvent(null);

				agReflAgileGrid.selectCells(new Cell[] {});

				updateDeleteButton();
			}
		});
		btnDelete.setText("Delete");

		chkRecursiveView = new Button(buttonComposite, SWT.CHECK);
		GridData gd_chkRecursiveView = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_chkRecursiveView.exclude = true;
		chkRecursiveView.setLayoutData(gd_chkRecursiveView);
		chkRecursiveView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (chkRecursiveView.getSelection()) {
					presenter.setDataSource(presenter.getDataSource(), true);
				} else {
					presenter.setDataSource(presenter.getDataSource(), false);
				}

				agReflAgileGrid.setDataSource();

				// presenter.
			}
		});
		chkRecursiveView.setText("Sub Folder Content");
		chkRecursiveView.setSelection(true);

		composite_1 = new Composite(buttonComposite, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		GridLayout gl_composite_1 = new GridLayout(2, false);
		gl_composite_1.verticalSpacing = 0;
		gl_composite_1.marginWidth = 0;
		gl_composite_1.marginHeight = 0;
		gl_composite_1.horizontalSpacing = 0;
		composite_1.setLayout(gl_composite_1);

		sumWidget1 = new EAGSumWidget(composite_1, SWT.NONE);
		sumWidget2 = new EAGSumWidget(composite_1, SWT.NONE);

		new Label(buttonComposite, SWT.NONE);

		if (presenter == null)
			setPresenter(new EAGPresenter<T>());

		if (element != null) {
			element.eAdapters().add(new AdapterImpl() {

				@Override
				public void notifyChanged(Notification msg) {
					if (msg.getFeature().equals(feature)) {
						presenter.updateDataSourceFilter();
						agReflAgileGrid.redraw();
					}

					super.notifyChanged(msg);
				}
			});
		}
	}

	public void setDataSource(final List<T> dataSource) {
		if (columns == null)
			throw new IllegalArgumentException(
					"Columns need to be provided, either by invoking autoGenerate columns, or by providing them");
		presenter.setDataSource(dataSource, chkRecursiveView.getSelection());
		agReflAgileGrid.setDataSource();
	}

	public EAGComposite<T> setPresenter(EAGPresenter<T> presenter) {
		this.presenter = presenter;
		agReflAgileGrid.setPresenter(presenter);
		sumWidget1.setPresenter(presenter);
		sumWidget2.setPresenter(presenter);
		return this;
	}

	public EAGComposite<T> setColumns(ElementColumnDefinition[] columns) {
		this.columns = columns;
		this.presenter.setColumnDefinitions(columns);
		return this;
	}

	public EAGComposite<T> setCanSetRecursive(boolean value) {
		((GridData) chkRecursiveView.getLayoutData()).exclude = !value;
		return this;
	}

	public EAGComposite<T> setSumWidget(boolean enabled) {
		sumWidget1.setEnabled(enabled);
		sumWidget2.setEnabled(enabled);
		return this;
	}

	private boolean hasSelection(List<T> dataSource) {
		return agReflAgileGrid.getCellSelection().length > 0
				&& agReflAgileGrid.getCellSelection()[0].row < dataSource.size();

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	// Parent widgets can optionally provide a modify listener
	public EAGComposite<T> setModifyListener(Listener modifyListener) {
		return this;
	}

	public void updateButtonState(boolean rowSelected, int selectedIndex) {
		btnDelete.setEnabled(rowSelected);
		btnOpen.setEnabled(rowSelected && presenter.canOpenSelection(agReflAgileGrid.getSelectedIndex()));

		sumWidget1.displaySumForValues(getSumValuesFromSelection(sumWidget1));
		sumWidget2.displaySumForValues(getSumValuesFromSelection(sumWidget2));
	}

	private List<Double> getSumValuesFromSelection(EAGSumWidget eagSumWidget) {
		int colIndex = eagSumWidget.getCmbSumColumn().getSelectionIndex();
		List<Double> selectedValues = new ArrayList<Double>();
		if (colIndex != -1) {
			for (int index : agReflAgileGrid.getSelectedIndex()) {
				Object val = agReflAgileGrid.getContentAt(index, colIndex);
				Double d = Double.parseDouble(val.toString());
				selectedValues.add(d);
			}
		}

		return selectedValues;
	}

	public EAGComposite<T> generateColumnsByReflection(Class<?> fromClass) {
		if (presenter == null)
			throw new IllegalArgumentException("Presenter has not been set");
		columns = presenter.getColumnDefinitionsByReflection(fromClass);
		return this;
	}

	public EAGComposite<T> generateDefaultColumns() {
		if (presenter == null)
			throw new IllegalArgumentException("Presenter has not been set");
		columns = presenter.getDefaultColumnDefinition();
		return this;
	}

	public EAGComposite<T> generateSpecificColumns(Class<?> instanceClass) {
		if (presenter == null)
			throw new IllegalArgumentException("Presenter has not been set");
		columns = presenter.getSpecificColumnDefinition(instanceClass);
		return this;
	}

	public EAGComposite<T> setElementSelectorOnAdd(boolean elementSelectorOnAdd) {
		this.elementSelectorOnAdd = elementSelectorOnAdd;
		return this;
	}

	public void setAddDialogTitle(String addDialogTitle) {
		this.addDialogTitle = addDialogTitle;
	}

	public void setAddDialogDescription(String addDialogDescription) {
		this.addDialogDescription = addDialogDescription;
	}

	/**
	 * Helper method for displaying Message box.
	 *
	 * @param text
	 *            the header text
	 * @param message
	 *            the message
	 */
	protected int messageBox(String text, String message, int style) {
		MessageBox dialog = new MessageBox(Display.getDefault().getActiveShell(), style);
		dialog.setText(text);
		dialog.setMessage(message);
		return dialog.open();
	}

	protected void updateDeleteButton() {
		boolean hasSelection = hasSelection(presenter.getDataSourceView());
		btnDelete.setEnabled(hasSelection);
		btnOpen.setEnabled(hasSelection && presenter.canOpenSelection(agReflAgileGrid.getSelectedIndex()));
	}
}
