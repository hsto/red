package dk.dtu.imm.red.core.ui.internal.element.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.ui.handlers.HandlerUtil;

public abstract class ClipboardHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		//Create a clipboard object
		Clipboard clipboard = 
				new Clipboard(HandlerUtil.getActiveShell(event).getDisplay());

		try {
			execute(event, clipboard);
		} finally {
			clipboard.dispose();
		}

		return null;
	}
	
	protected abstract void execute(ExecutionEvent event, Clipboard clipboard)
		throws ExecutionException;

}
