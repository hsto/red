package dk.dtu.imm.red.core.ui.internal.comment.views.impl;

import java.text.Collator;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ColumnSortComparator;

public class CommentTableSortComparator extends ColumnSortComparator {

	public CommentTableSortComparator(AgileGrid agileGrid, int columnIndex,
			int direction) {
		super(agileGrid, columnIndex, direction);
	}

	@Override
	public int doCompare(Object o1, Object o2, int row1, int row2) {
		
		Collator collator = Collator.getInstance();
		collator.setStrength(Collator.PRIMARY);
		return collator.compare(o1, o2);
		
	}

}
