package dk.dtu.imm.red.core.ui.element.wizards.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage; 
import org.eclipse.ui.IWorkbench; 
import dk.dtu.imm.red.core.ui.wizards.BaseWizard;
import dk.dtu.imm.red.core.element.QuantityItem;
import dk.dtu.imm.red.core.element.QuantityLabel;
import dk.dtu.imm.red.core.ui.element.wizards.CreateQuantityItemWizard;
import dk.dtu.imm.red.core.ui.element.wizards.CreateQuantityItemWizardPresenter;

public class CreateQuantityItemWizardImpl extends BaseWizard
		implements CreateQuantityItemWizard {
	
	protected CreateQuantityItemPageImpl createQuantityItemPage;
	protected EList<QuantityItem> items;
	
	public CreateQuantityItemWizardImpl(EList<QuantityItem> items) {
		presenter = new CreateQuantityItemWizardPresenterImpl();
		this.items = items;
	} 
	
	@Override
	public boolean performFinish() {

		QuantityLabel selectedLabel = 
				createQuantityItemPage.getSelectedLabel();
		
		if(selectedLabel == null){
			return true;
		}else{
			((CreateQuantityItemWizardPresenter) presenter)
				.setSelectedLabel(selectedLabel);
			return true;
		}
			
	}

	@Override
	public void pageOpenedOrClosed(IWizardPage fromPage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		createQuantityItemPage = new CreateQuantityItemPageImpl(items);
		presenter = new CreateQuantityItemWizardPresenterImpl();
		
	}
	@Override
	public void addPages() {
		addPage(createQuantityItemPage);
	}

	
}
