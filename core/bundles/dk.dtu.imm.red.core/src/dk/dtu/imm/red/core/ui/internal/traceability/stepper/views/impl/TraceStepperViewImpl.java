package dk.dtu.imm.red.core.ui.internal.traceability.stepper.views.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.Activator;
import dk.dtu.imm.red.core.ui.internal.traceability.stepper.views.TraceStepperContentProvider;
import dk.dtu.imm.red.core.ui.internal.traceability.views.TraceView;
import dk.dtu.imm.red.core.ui.views.BaseView;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;

/**
 * 
 * @author Sebastian Dittmann
 *
 */
public class TraceStepperViewImpl extends BaseView implements TraceView, IResourceChangeListener {

	String ID = "dk.dtu.imm.red.core.traceability.view.table";

	private Composite composite;
	private TraceStepperContentProvider provider;
	private Combo fieldElement, fieldRelationship;
	private Text focusElementText;
	private Button applyButton, radioElement[], radioRelationship[];
	// versionCheckBox;
	private TraceStepperTableEAGComposite sourceContainer, targetContainer;
	private String historyOldURL = "icons/history_arrow_left.png", historyNewURL = "icons/history_arrow_right.png";
	private List<Element> history = null;
	// private Text versionDateText;
	private int historyPointer = 0;
	private boolean test = false;

	@Override
	public void updateTraceabilityView() {
		// TODO
	}

	@Override
	public void tracingRequested() {
		// TODO: function not used yet
	}

	public Combo getFieldElement() {
		return fieldElement;
	}

	public Combo getFieldRelationship() {
		return fieldRelationship;
	}

	public Button[] getRadioElement() {
		return radioElement;
	}

	public Button[] getRadioRelationship() {
		return radioRelationship;
	}
	
	public void setTest(boolean test) {
		this.test = test;
	}

	@Override
	public void createPartControl(Composite parent) {
		composite = new Composite(parent, SWT.NONE);
		provider = new TraceStepperContentProviderImpl();

		GridLayout layout = new GridLayout(1, false);
		layout.verticalSpacing = 0;
		composite.setLayout(layout);

		setUpTables();
		setUpFilter();
	}

	public void setUpNavigation() {
		Composite navigationContainer = new Composite(composite, SWT.NONE);
		GridData navigationGridData = new GridData();
		navigationGridData.grabExcessHorizontalSpace = true;
		navigationGridData.horizontalAlignment = SWT.FILL;
		navigationContainer.setLayoutData(navigationGridData);

		GridLayout navigationLayout = new GridLayout(5, false);
		navigationContainer.setLayout(navigationLayout);

		Composite focusElementComposite = new Composite(navigationContainer, SWT.NONE);
		GridLayout focusElementLayout = new GridLayout(2, false);
		focusElementLayout.marginLeft = 0;
		focusElementLayout.marginTop = 0;
		focusElementComposite.setLayout(focusElementLayout);

		// Add element text field
		focusElementText = new Text(focusElementComposite, SWT.BORDER | SWT.LEFT);
		GridData searchBoxData = new GridData();
		searchBoxData.horizontalSpan = 1;
		searchBoxData.grabExcessHorizontalSpace = true;
		searchBoxData.minimumWidth = 250;
		focusElementText.setLayoutData(searchBoxData);
		focusElementText.setEditable(false);

		// Add "search directories" button
		Button searchButton = new Button(focusElementComposite, SWT.NONE);
		searchButton.setText("...");
		searchButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				history.clear();
				historyPointer = 0;
				provider.setFocusElement(provider.getContent());
				history.add(provider.getFocusElement());
				updateView(provider.getFocusElement());
			}

		});

		// Add navigation buttons
		Composite historyComposite = new Composite(navigationContainer, SWT.NONE);
		GridLayout historyLayout = new GridLayout(2, true);
		historyLayout.marginTop = 0;
		historyLayout.marginBottom = 0;
		historyComposite.setLayout(historyLayout);

		history = new ArrayList<>();

		Button historyOldButton = new Button(historyComposite, SWT.NONE);
		if (!test)
			historyOldButton.setImage(Activator.getDefault().getImageRegistry().get(Activator.HISTORY_OLD_ICON));
		historyOldButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				if (historyPointer > 0 && !history.isEmpty()) {
					historyPointer--;
					updateView(history.get(historyPointer));
				}
			}
		});

		Button historyNewButton = new Button(historyComposite, SWT.NONE);
		if (!test)
			historyNewButton.setImage(Activator.getDefault().getImageRegistry().get(Activator.HISTORY_NEW_ICON));
		historyNewButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (!history.isEmpty() && (historyPointer + 1) < history.size()) {
					historyPointer++;
					updateView(history.get(historyPointer));
				}
			}
		});

		// Add help button
		Button helpButton = new Button(navigationContainer, SWT.PUSH);
		GridData helpButtonGridData = new GridData();
		helpButtonGridData.horizontalSpan = 1;
		helpButtonGridData.horizontalAlignment = SWT.RIGHT;
		helpButtonGridData.verticalAlignment = SWT.TOP;
		helpButtonGridData.grabExcessHorizontalSpace = true;
		helpButtonGridData.verticalIndent = 5;
		helpButton.setLayoutData(helpButtonGridData);
		if (!test)
			helpButton.setImage(Activator.getDefault().getImageRegistry().get(Activator.HELP_ICON));
		helpButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				PlatformUI.getWorkbench().getHelpSystem().displayHelpResource(getHelpResourceURL());
			}
		});
	}

	public void setUpTables() {

		// Set up source table
		sourceContainer = new TraceStepperTableEAGComposite(composite, SWT.NONE, this);
		// Name column
		sourceContainer.setPresenter(new EAGPresenter<Element>());
		sourceContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		sourceContainer.setSourceCellRightClickListener();
		sourceContainer.setColumns(provider.getFromColumnDefinition()).setDataSource(provider.getDataForSourceTable());

		setUpNavigation();

		// Set up target table
		targetContainer = new TraceStepperTableEAGComposite(composite, SWT.NONE, this);
		// Name column
		targetContainer.setPresenter(new EAGPresenter<Element>());
		targetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		targetContainer.setTargetCellRightClickListener();
		// Initialize content provider
		provider.setElementFilter("", true);
		provider.setRelationshipFilter("", true);
		targetContainer.setColumns(provider.getToColumnDefinition()).setDataSource(provider.getDataForTargetTable());

	}

	public void setUpFilter() {
		Composite filterComposite = new Composite(composite, SWT.NONE);
		GridLayout filterLayout = new GridLayout();
		filterLayout.numColumns = 2;
		filterComposite.setLayout(filterLayout);
		filterComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// Set up filter selector
		Composite filterSubComposite = new Composite(filterComposite, SWT.NONE);

		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		filterSubComposite.setLayout(layout);

		GridData textFieldData = new GridData();
		textFieldData.minimumWidth = 250;

		// Element filter
		Label elemLabel = new Label(filterSubComposite, SWT.NONE);
		elemLabel.setText("Elements:");

		// Set up show/hide selector
		Composite radioElementComposite = new Composite(filterSubComposite, SWT.NONE);
		GridLayout radioLayout = new GridLayout();
		radioLayout.numColumns = 2;
		radioElementComposite.setLayout(radioLayout);

		radioElement = new Button[2];
		radioElement[0] = new Button(radioElementComposite, SWT.RADIO);
		radioElement[0].setText("Show");
		radioElement[0].setSelection(true);
		radioElement[1] = new Button(radioElementComposite, SWT.RADIO);
		radioElement[1].setText("Hide");

		fieldElement = new Combo(filterSubComposite, SWT.BORDER | SWT.FILL);
		fieldElement.setItems(provider.getElementKindList());
		fieldElement.setLayoutData(textFieldData);

		// Relationship filter
		Label relatLabel = new Label(filterSubComposite, SWT.NONE);
		relatLabel.setText("Relationships:");

		// Set up show/hide selector
		Composite radioRelatComposite = new Composite(filterSubComposite, SWT.NONE);
		radioRelatComposite.setLayout(radioLayout);

		radioRelationship = new Button[2];
		radioRelationship[0] = new Button(radioRelatComposite, SWT.RADIO);
		radioRelationship[0].setText("Show");
		radioRelationship[0].setSelection(true);
		radioRelationship[1] = new Button(radioRelatComposite, SWT.RADIO);
		radioRelationship[1].setText("Hide");

		fieldRelationship = new Combo(filterSubComposite, SWT.BORDER | SWT.FILL);
		fieldRelationship.setItems(provider.getRelationshipKindList());
		fieldRelationship.setLayoutData(textFieldData);

		GridData buttonData = new GridData();
		buttonData.grabExcessVerticalSpace = true;
		buttonData.verticalAlignment = SWT.FILL;
		applyButton = new Button(filterComposite, SWT.NONE);
		applyButton.setText("Apply filter");
		applyButton.setLayoutData(buttonData);
		applyButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.getSource().getClass().equals(Button.class)) {
					// versionChecked = versionCheckBox.getSelection();
					System.out.println("Apply filters:\nElement type: " + fieldElement.getText()
							+ "\nRelationship type: " + fieldRelationship.getText());
					// + "\nVersion check: " + versionChecked);
					updateView(provider.getFocusElement());
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	// Workaround for content reloading of table
	public void setFocusOnSearchBox() {
		focusElementText.setFocus();
	}

	protected String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/TraceabilityStepper.html";
	}

	/**
	 * Refreshes the traceability view when changes in the project have been
	 * made
	 */
	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		System.out.println(event.getSource().getClass().getName() + " " + event.getType());
	}

	private void updateView(Element focusElement) {
		if (focusElement != null) {
			provider.setFocusElement(focusElement);
			EcoreUtil.copy(focusElement.getChangeList());
			focusElementText.setText(focusElement.getName());
			provider.setElementFilter(fieldElement.getText(), radioElement[0].getSelection());
			provider.setRelationshipFilter(fieldRelationship.getText(), radioRelationship[0].getSelection());
			sourceContainer.setDataSource(provider.getDataForSourceTable());
			sourceContainer.update();
			targetContainer.setDataSource(provider.getDataForTargetTable());
			targetContainer.update();
		}
	}

	public void addElementToHistory(Element element) {
		// Check if element is last element in history, otherwise override the
		// following entries
		if (historyPointer < history.size() - 1) {
			history = history.subList(0, historyPointer + 1);
		}
		history.add(element);
		historyPointer++;
		updateView(element);
	}
}
