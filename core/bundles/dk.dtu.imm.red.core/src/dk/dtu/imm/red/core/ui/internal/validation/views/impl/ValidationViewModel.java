package dk.dtu.imm.red.core.ui.internal.validation.views.impl;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.imm.red.core.validation.ErrorMessage;

public class ValidationViewModel {

	private List<ErrorMessage> validationMessages = new ArrayList<ErrorMessage>();

	public List<ErrorMessage> getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(List<ErrorMessage> validationMessages) {
		this.validationMessages = validationMessages;
	}
	
}
