package dk.dtu.imm.red.core.ui.internal.comment.views;

import org.eclipse.ui.IViewPart;

public interface CommentView extends IViewPart {

	String ID = "dk.dtu.imm.red.core.comment.view";
	
	void setCommentLabel(String text);
	void setButtonsEnabled(boolean enabled);
	void clearTable();

}
