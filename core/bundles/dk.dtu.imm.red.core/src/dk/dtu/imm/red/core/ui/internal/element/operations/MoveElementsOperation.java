package dk.dtu.imm.red.core.ui.internal.element.operations;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class MoveElementsOperation extends AbstractOperation {

	/**
	 * The target of the movement.
	 */
	protected Group targetGroup;

	/**
	 * The elements to be moved.
	 */
	protected Element[] movedElements;

	/**
	 * The list of the elements' original parents. To be used when undoing.
	 */
	protected Group[] originalParents;
	
	protected int index;

	/**
	 * Constructs a new Move Elements operation.
	 * 
	 * @param target
	 *            The target for the movement. If null, the workspace is used as
	 *            default
	 * @param elements
	 *            A list of elements to be moved
	 */
	public MoveElementsOperation(final Group target,
			final List<Element> elements, int index) {
		super("Move elements");

		// Default target to workspace if the given target is null
		if (target != null) {
			this.targetGroup = target;
		} else {
			this.targetGroup = WorkspaceFactory.eINSTANCE
					.getWorkspaceInstance();
		}
		this.movedElements = elements.toArray(new Element[elements.size()]);

		this.originalParents = new Group[elements.size()];
		
		this.index = index;
	}

	@Override
	public IStatus execute(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		return redo(monitor, info);
	}

	@Override
	public IStatus redo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		if (monitor != null) {
			monitor.beginTask("Moving elements", movedElements.length);
		}
		
		String message = checkChangeParentMove();
		if (!message.isEmpty()) {
			MessageDialog md = new MessageDialog(Display.getCurrent().getActiveShell(), "Warning!", null, message, MessageDialog.QUESTION, 
					new String[]{"Yes", "No"}, 1);
			
			if (md.open() == 1) { // Cancel
				return Status.CANCEL_STATUS;
			}
		}

		//for (int i = 0; i < movedElements.length; i++) {
		for (int i = movedElements.length-1; i >= 0; i--) { // Issue # 190 added by Luai
			Element element = movedElements[i];
			originalParents[i] = element.getParent();

			if (targetGroup instanceof Workspace) {
				FileDialog dialog = new FileDialog(Display.getCurrent()
						.getActiveShell(), SWT.SAVE);
				dialog.setFilterExtensions(new String[] {"*.red"});
				String filePath = dialog.open();
				if (filePath == null) {
					return Status.CANCEL_STATUS;
				}
				
				ResourceSet resourceSet = element.getResourceSet();
				URI uri = URI.createFileURI(filePath);
				Resource resource = resourceSet.createResource(uri);
				File file = FileFactory.eINSTANCE.createFile();
				file.getContents().add(element);
				file.setUri(filePath);
				file.setName(filePath.substring(
						filePath.lastIndexOf(java.io.File.separator) + 1));
				WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
					.addOpenedFile(file);
			} else {
				if (targetGroup.getContents().contains(element)) {
					if (targetGroup.getContents().indexOf(element) < index) {
						index--;
					}
					targetGroup.getContents().move(index,  element);
				} else {
					element.setParent(null);
					targetGroup.getContents().add(index, element);
				}
			}
			originalParents[i].save();
			element.save();

			if (monitor != null) {
				monitor.worked(1);
			}
		}

		if (monitor != null) {
			monitor.done();
		}

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(final IProgressMonitor monitor, final IAdaptable info)
			throws ExecutionException {

		//for (int i = 0; i < movedElements.length; i++) {
		for (int i = movedElements.length-1; i >= 0; i--) { // Issue # 190 added by Luai
			Group oldParent = movedElements[i].getParent();
			movedElements[i].setParent(originalParents[i]);
			movedElements[i].save();
			if (oldParent != null) {
				oldParent.save();
			}
		}

		return Status.OK_STATUS;
	}
	
	private String checkChangeParentMove() {
		if (targetGroup instanceof Workspace) return "";
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < movedElements.length; i++) {
			if (!targetGroup.getContents().contains(movedElements[i])) {
				sb.append(movedElements[i].getName() + ", ");
			}
		}
		
		if (sb.toString().isEmpty()) {
			return "";
		}
		else {
			return "Dragging this selection will lead the following element(s): " + sb.toString() + 
					"to get moved to the file: " + targetGroup.getName() + "\n" +
					"Are you sure you want to continue?";
		}
	}

}
