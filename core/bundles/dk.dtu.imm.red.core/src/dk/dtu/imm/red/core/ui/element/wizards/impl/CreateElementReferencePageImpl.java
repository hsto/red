package dk.dtu.imm.red.core.ui.element.wizards.impl;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;

public class CreateElementReferencePageImpl extends WizardPage
		implements IWizardPage{

	protected Label selectElementLabel;
	protected ISelection selection;
	protected ElementTreeViewWidget treeView;
	protected Class[] typesShown;
	
	protected CreateElementReferencePageImpl(Class[] typesShown) {
		this(typesShown, null, null);
	}
	
	protected CreateElementReferencePageImpl(Class[] typesShown, String title, String description) {
		super(title != null ? title : "Create Element Reference");
		
		setTitle(getName());
		setDescription((description != null ? description : 
			"Select an element in the tree in order to make a reference to it.")
				+ "\nIf there is nothing to select, there are no elements of the right type.");
		
		this.typesShown = typesShown;
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		
		GridLayout layout = new GridLayout(2, true);
		composite.setLayout(layout);
		
		GridData descriptionLayoutData = new GridData();
		descriptionLayoutData.horizontalSpan = 2;


		selectElementLabel = new Label(composite, SWT.NONE);
		selectElementLabel.setLayoutData(descriptionLayoutData);
		selectElementLabel
				.setText("Select the element to refer to");

		GridData treeViewerLayoutData = new GridData(SWT.FILL, SWT.FILL, true,
				true);
		treeViewerLayoutData.horizontalSpan = 2;

		Composite treeContainer = new Composite(composite, SWT.NONE);

		GridLayout treeContainerLayout = new GridLayout();
		treeContainerLayout.numColumns = 1;

		treeContainer.setLayout(treeContainerLayout);
		treeContainer.setLayoutData(treeViewerLayoutData);
		
		selection = 
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();

		treeView = new ElementTreeViewWidget(
				false, treeContainer, SWT.BORDER, typesShown);
		if (selection != null) {
			treeView.setSelection(selection);
		}

		treeView.getTree().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(final MouseEvent e) {
				TreeItem item = treeView.getTree().getItem(new Point(e.x, e.y));
				if (item == null) {
					treeView.setSelection(null);
				}
			}
		});
		
		treeView.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				getContainer().updateButtons();
			}
		});
		
		setPageComplete(true);
		setControl(composite);
	}
	
	public Element getSelectedElement(){
		IStructuredSelection selection = (IStructuredSelection) treeView.getSelection(); 
		return (Element) selection.getFirstElement();
	}

}
