package dk.dtu.imm.red.core.ui.wizards;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.file.handlers.CreateFileHandler;
import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;

public class DisplayInfoPage extends WizardPage
		implements IWizardPage{

	protected Label enterDisplayNameLabel;
	protected Label displayNameLabel;
	protected Text displayNameText;
	protected Label selectParentLabel;
	protected ElementTreeViewWidget treeView;
	protected ISelection selection;
	protected String elementType;

	protected boolean askDisplayName;

	protected boolean displayNameModified;
	//button to create a new file on the fly
	protected Button newFileButton;


	public DisplayInfoPage(String elementType, boolean askDisplayName) {
		super("Display Information");
		setTitle("Place the element");
		setDescription("Select an internal location"
				+ " where this " + elementType
				+ " should be placed.\n");
//				+ "If no selection is made, the " + elementType
//				+ " will not be placed in an existing project"
//				+ " or folder, and you will need to specify a "
//				+ " save location.");
		this.elementType = elementType;
		displayNameModified = false;
		this.askDisplayName = askDisplayName;
	}


	public DisplayInfoPage(String elementType) {
		this(elementType, false);
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		GridData descriptionLayoutData = new GridData();
		descriptionLayoutData.horizontalSpan = 2;


		selectParentLabel = new Label(composite, SWT.NONE);
		selectParentLabel.setLayoutData(descriptionLayoutData);
		selectParentLabel
				.setText("Select the position of the new " + elementType);

		GridData treeViewerLayoutData = new GridData(SWT.FILL, SWT.FILL, true,
				true);
		treeViewerLayoutData.horizontalSpan = 2;

		Composite treeContainer = new Composite(composite, SWT.NONE);

		GridLayout treeContainerLayout = new GridLayout();
		treeContainerLayout.numColumns = 1;


		treeContainer.setLayout(treeContainerLayout);
		treeContainer.setLayoutData(treeViewerLayoutData);

		selection =
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		String g = "file";

		if(elementType==g){
			treeView = new ElementTreeViewWidget(true, treeContainer, SWT.BORDER,false,true);
		}
       else{
    	   treeView = new ElementTreeViewWidget(true, treeContainer, SWT.BORDER,true,false);
		}

		if (selection != null) {
			if(selection.isEmpty()){
				//default to select the first file in the project
				TreeItem[] items = treeView.getTree().getItems();
				for(int i=0;i<items.length;i++){
					if(items[i].getData() instanceof File){
						selection = new StructuredSelection(items[i].getData());
						break;
					}
				}
			}

			treeView.setSelection(selection);

		}

		treeView.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				getContainer().updateButtons();
			}
		});


		if (askDisplayName) {
			GridData displayNameLayoutData = new GridData();
			displayNameLayoutData.heightHint = 15;
			displayNameLayoutData.widthHint = 80;

			enterDisplayNameLabel = new Label(composite, SWT.NONE);
			enterDisplayNameLabel.setLayoutData(descriptionLayoutData);
			enterDisplayNameLabel
				.setText("Enter the display name of the new " + elementType);

			displayNameLabel = new Label(composite, SWT.NONE);
			displayNameLabel.setText("Display name: ");
			displayNameLabel.setLayoutData(displayNameLayoutData);

			displayNameText = new Text(composite, SWT.BORDER);
			displayNameText.setLayoutData(displayNameLayoutData);
			displayNameText.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(final ModifyEvent e) {
					displayNameModified = true;
					if (displayNameText.getText().trim().isEmpty()) {
						setPageComplete(false);

					} else {
						setPageComplete(true);
					}
				}
			});
			setPageComplete(false);
		} else {
			setPageComplete(true);
		}

		newFileButton = new Button(composite, SWT.NONE);

		newFileButton.setText("New File");
		newFileButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				IWorkbenchWindow window = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow();

				IHandlerService handlerService = (IHandlerService) window
						.getService(IHandlerService.class);

				try {
					handlerService.executeCommand(CreateFileHandler.ID, null);
				} catch (Exception e) {
					e.printStackTrace(); LogUtil.logError(e);
				}
			}
		});


		setControl(composite);

	}

	@Override
	public IWizardPage getNextPage() {
		if (isPageComplete() && isCurrentPage()
				&& getWizard() instanceof IBaseWizard) {

			((IBaseWizard) getWizard()).pageOpenedOrClosed(this);
		}
		return super.getNextPage();
	}



	public void setDisplayName(String text) {
		if (!displayNameModified && displayNameText != null) {
			displayNameText.setText(text);
			displayNameText.notifyListeners(SWT.Modify, null);
			displayNameModified = false;
		}

	}

	public String getDisplayName() {
		if (displayNameText != null) {
			return displayNameText.getText();
		} else {
			return "";
		}
	}

	public ISelection getSelection() {
		return treeView.getSelection();
	}


	public Button getNewContainerButton() {
		return newFileButton;
	}



}
