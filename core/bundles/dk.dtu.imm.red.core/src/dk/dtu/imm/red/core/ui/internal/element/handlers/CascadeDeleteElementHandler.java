package dk.dtu.imm.red.core.ui.internal.element.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.element.operations.CascadeDeleteElementsOperation;

/**
 * 
 *  Issue #119 @author Luai
 *
 */
public class CascadeDeleteElementHandler extends AbstractHandler implements IHandler {

	

	/**
	 * Creates the two dialog boxes used for confirming the delete.
	 */
	public CascadeDeleteElementHandler() {
		super();
	}

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		
		List<Element> selectedElements = new ArrayList<Element>();
		boolean triggeredFromVisualModeling = false;
		
		if (event.getTrigger() instanceof List) { // Triggered from visualmodelling
			selectedElements = (List<Element>) event.getTrigger();
			//selectedElements.add((Element) event.getTrigger());
			triggeredFromVisualModeling = true;
		} else {
			ISelection selectedObjects = HandlerUtil.getCurrentSelection(event);
			if (selectedObjects instanceof IStructuredSelection) {
				
				Object[] selectedObjectsArray =	((IStructuredSelection) selectedObjects).toArray();
				for (Object object : selectedObjectsArray) {
					if (object instanceof Element) 
					{
						selectedElements.add((Element) object);
					}
				}
			}
			
		}		
		
		Object res = null;
		if (!selectedElements.isEmpty()) {
			CascadeDeleteElementsOperation operation = 
					new CascadeDeleteElementsOperation(selectedElements, triggeredFromVisualModeling);
			operation.addContext(PlatformUI.getWorkbench().getOperationSupport()
					.getUndoContext());

			OperationHistoryFactory.getOperationHistory()
				.execute(operation, null, null);
			res = operation.visualModelingcommands;
		}
		
		return res;
	}

}
