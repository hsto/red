package dk.dtu.imm.red.core.ui.internal.element.handlers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.ui.internal.LogUtil;

public class CopyElementsHandler extends ClipboardHandler {


	@Override
	protected void execute(final ExecutionEvent event, 
			final Clipboard clipboard) throws ExecutionException {
		
		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		
		if (currentSelection instanceof IStructuredSelection) {
			Object[] selectedObjects = 
					((IStructuredSelection) currentSelection).toArray();

			
			Folder pseudoFolder = FolderFactory.eINSTANCE.createFolder();

			for (int i = 0; i < selectedObjects.length; i++) {
				// we can only drag/drop EObjects (domain objects)
				if (!(selectedObjects[i] instanceof EObject)) {
					continue;
				}
				Element copiedElement = 
						(Element) EcoreUtil.copy((EObject) selectedObjects[i]);
				copiedElement.setUniqueID(UUID.randomUUID().toString());
				pseudoFolder.getContents()
					.add(copiedElement);
			}

			Resource resource = new XMLResourceImpl();
			resource.getContents().add(pseudoFolder);
			
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			try {
				resource.save(outputStream, null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); LogUtil.logError(e);
			}

			clipboard.setContents(new Object[] {outputStream.toString()}, 
					new Transfer[] {TextTransfer.getInstance()});
		}
	}

}
