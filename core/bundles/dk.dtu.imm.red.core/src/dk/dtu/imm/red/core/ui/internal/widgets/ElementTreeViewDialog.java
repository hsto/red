package dk.dtu.imm.red.core.ui.internal.widgets;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;

/**
 * 
 * @author Jakob Kragelund
 *
 */
public class ElementTreeViewDialog extends Dialog {
	
	private Class[] type;
	private ElementTreeViewWidget elementExplorer;
	private IStructuredSelection selection;

	public ElementTreeViewDialog(Shell parentShell, Class[] type) {
		super(parentShell);
		this.type = type;
	}
	
	@Override
	protected boolean isResizable() {
		return true;
	}
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		Composite container = new Composite(
				(Composite) super.createDialogArea(parent), SWT.NONE);
		
		GridLayout layout = new GridLayout(1, true);
		container.setLayout(layout);
		container.setLayoutData(new GridData(300, 400));
		
		elementExplorer = 
				new ElementTreeViewWidget(false, container, SWT.BORDER, type);
		elementExplorer.setLayoutData(
				new GridData(SWT.FILL, SWT.FILL, true, true));
		elementExplorer.setSelection(getSelection());
		
		return container;
	}
	
	@Override
	protected void buttonPressed(int buttonId) {
		ISelection selection = elementExplorer.getSelection();
		if (selection instanceof IStructuredSelection) {
			this.selection = (IStructuredSelection) selection;
		} else {
			this.selection = null;
		}
		
		super.buttonPressed(buttonId);
	}
	
	public IStructuredSelection getSelection() {
		return selection;
	}

}
