package dk.dtu.imm.red.core.ui.internal.widgets;

import java.util.Date;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Shell;

public class DateSelectionDialog extends Dialog {
	
	private Point initialLocation;
	private DateTime calendar;
	private Date selectedDate;

	public DateSelectionDialog(Shell parentShell, Point initialLocation) {
		super(parentShell);
		setShellStyle(SWT.BORDER | SWT.APPLICATION_MODAL);
		this.initialLocation = initialLocation;
	}
	
	@Override
	protected boolean isResizable() {
		return false;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		
		calendar = new DateTime(composite, SWT.CALENDAR | SWT.BORDER);
		return composite;
	}
	
	public Date getSelectedDate() {
		return selectedDate;
	}
	
	@Override
	protected void okPressed() {
		selectedDate = new Date(calendar.getYear()-1900, calendar.getMonth(), 
				calendar.getDay()+1, calendar.getHours()-1, 
				calendar.getMinutes()-1, calendar.getSeconds()-1);
		super.okPressed();
	}	
	@Override
	protected void configureShell(Shell newShell) {
		// TODO Auto-generated method stub
		newShell.setLocation(100,100);
		super.configureShell(newShell);
	}
	
	@Override
	protected Point getInitialLocation(Point initialSize) {
		initialLocation.y += initialSize.y/2 + 10;
		return initialLocation;
	}

}
