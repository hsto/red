package dk.dtu.imm.red.core.ui.wizards;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;

public class ElementBasicInfoPageComposite extends Composite {
	private Label lblLabel;
	private Label lblName;
	private Label lblDescription;
	private Text txtLabel;
	private Text txtName;
	private Text txtDescription;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ElementBasicInfoPageComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(5, false));

		Label lblLabel = new Label(this, SWT.NONE);
		lblLabel.setText("Label");

		txtLabel = new Text(this, SWT.BORDER);
		GridData gd_txtLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtLabel.widthHint = 54;
		txtLabel.setLayoutData(gd_txtLabel);
		new Label(this, SWT.NONE);

		Label lblName = new Label(this, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name");

		txtName = new Text(this, SWT.BORDER);
		GridData gd_txtName = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtName.widthHint = 132;
		txtName.setLayoutData(gd_txtName);

		Label lblDescription = new Label(this, SWT.WRAP);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblDescription.setText("Description");

		txtDescription = new Text(this, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gd_txtDescription = new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1);
		gd_txtDescription.heightHint = 56;
		txtDescription.setLayoutData(gd_txtDescription);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public String getTxtLabel(){
		return txtLabel.getText();
	}

	public Text getLabelTextField(){
		return txtLabel;
	}

	public String getTxtName(){
		return txtName.getText();
	}

	public Text getNameTextField(){
		return txtName;
	}

	public String getTxtDescription(){
		return txtDescription.getText();
	}

	public Label getLblLabel() {
		return lblLabel;
	}

	public Label getLblName() {
		return lblName;
	}

	public Label getLblDescription() {
		return lblDescription;
	}



}
