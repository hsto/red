package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.internal.element.handlers.LoadFileHandler;
import dk.dtu.imm.red.core.ui.internal.element.handlers.OpenElementHandler;
import dk.dtu.imm.red.core.ui.internal.file.editors.FileEditorImpl;

import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

/**
* Provider for the file page in project editor
* @author Ahmed Fikry
* 
*/
public class ModelProvide 
{
	private static ModelProvide INSTANC;
	private List<TableRow> allFiles;
	ArrayList<TableRow> localAllFiles = new ArrayList<TableRow>();
	String status;
	String desc;
	
	public static ModelProvide getInstance()
	{
		if(INSTANC == null)
			INSTANC = new ModelProvide();
		 		
		return INSTANC;
	}
		  
	private ModelProvide()
	{
		allFiles = new ArrayList<TableRow>();
		//UpdateAllFilesInfo();
	}
	 
	public void UpdateAllFilesInfo() 
	{
		OpenElementHandler oeh = new OpenElementHandler();
		oeh.fileNamesFunc(filesListData.ProjectNameAndPath);
		
		LoadFileHandler lf = new LoadFileHandler();
		for(int i=0; i< filesListData.fileList.size(); i++ )
		{
			status = lf.FilesLockStatus(filesListData.path+filesListData.fileList.get(i));
			String path=filesListData.path+filesListData.fileList.get(i); 
			desc = lf.FilesDescription(filesListData.path+filesListData.fileList.get(i));
			
			if(status.isEmpty())
				status = "Lock is not set";
			else if(status == "File has been moved"){
				path = "";
				desc = "";}
			
			TableRow line = new TableRow(new String[] {filesListData.fileList.get(i),path,desc, status, filesListData.ProjectNameAndPath });
			
			boolean add=true;
			
			for(int x=0; x<allFiles.size(); x++)
			{
				if(allFiles.get(x).getData(4).equals(filesListData.ProjectNameAndPath) && allFiles.get(x).getData(0).equals(filesListData.fileList.get(i)))
				{
				
					if(!allFiles.get(x).getData(3).equals(status)||!allFiles.get(x).getData(2).equals(desc) )
					{
						allFiles.remove(x);
						allFiles.add(x, line);
					}
					
					add=false;
					break;
				}
			}					
			
			if(add==true)
			{
				allFiles.add(line);
			}
		}
		
		updateLocalList();
		
	}

	private void updateLocalList()
	{
		localAllFiles.clear();
		for(int i=0; i<allFiles.size(); i++)
		{
			
			if(allFiles.get(i).getData(4).equals(filesListData.ProjectNameAndPath))
				localAllFiles.add(allFiles.get(i));
		
		}
	}
	
	public List<TableRow> GetProjectFiles() 
	{
		UpdateAllFilesInfo();
		updateLocalList();
		
		return localAllFiles;
	}
}

class TableRow
{
    private String[]    data;
    private String description;
  
    
    private PropertyChangeSupport propertychangesupport = new PropertyChangeSupport(this);
    
    public TableRow(String[] input)
    {
        data = input;
    }
    
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) 
    {
  	    propertychangesupport.addPropertyChangeListener(propertyName, listener);
  	}

  	  public void removePropertyChangeListener(PropertyChangeListener listener) 
  	  {
  		  propertychangesupport.removePropertyChangeListener(listener);
  	  }
  	  
  	  public String getDescription() 
  	  {
  		  SaveDescription(data);
  		  
  		  return data[2];  	  
  	  }
  	  
  	  public void setDescription(String description) 
  	  {
  		  //propertychangesupport.firePropertyChange("description", this.description, this.description = description);  		  
  		  propertychangesupport.firePropertyChange("description", this.data[2], this.data[2] = description); 
  	  }

  	  public String getData(int index)
  	  {
        if (index < 0 || index > data.length - 1)
            throw new IllegalArgumentException("Invalid index: " + index + ". Minimum: 0, Maximum: " + (data.length - 1));
        return data[index];
  	  }        

  	protected File file;
  	
  	private void SaveDescription(String[] data)
	{
		WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedFiles();
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		for(int y = 0; y<workspace.getCurrentlyOpenedFiles().size();y++)
		{
			if(workspace.getCurrentlyOpenedFiles().get(y).getName().equals(data[0]))
			{
				file = (File) workspace.getCurrentlyOpenedFiles().get(y);
				try 
				{
					if(!file.getDescription().toString().trim().equals(data[2].trim()))
					{
						file.setLongDescription(TextFactory.eINSTANCE.createText(data[2]));
						file.save();
					}						
				} catch (Exception e) {return;}				
			}
		}  		  
  	}
}
	

class TrimLabelProvider extends CellLabelProvider
{

    @Override
    public void update(ViewerCell cell)
    {
        /*
         * This method is called by the TableViewer to get
         * the String to display in the cell
         */
        Object element = cell.getElement();
        if (element instanceof TableRow)
        {
            int columnIndex = cell.getColumnIndex();

            TableRow row = (TableRow) cell.getElement();

            /* Here we trim the text */
            cell.setText(shortenText(row.getData(columnIndex)));
            
        }
    }

    private String shortenText(String text)
    {
        /* This is the maximal length we allow */
        int value = 100;

        if (text.length() > value)
        {
            int index = text.indexOf(" ", value);
            if (index != -1)
                return text.substring(0, index) + " [...]";
        }
        return text;
    }
}