package dk.dtu.imm.red.core.ui.editors;

import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.ui.internal.element.dialog.CheckResultBubble;

public class ElementBasicInfoContainer extends Composite {
	private Text txtLabel;
	private Text txtName;
	private Combo cbKind;
	private RichTextEditor rteDescription;
	
	private Element element;
	private Text txtStructeredId;

	public ElementBasicInfoContainer(Composite parent, int style) {
		super(parent, style);
	}
	
	/**
	 * Create the composite.
	 *
	 * @param parent
	 * @param style
	 */
	public ElementBasicInfoContainer(Composite parent, int style, final BaseEditor editor, final Element specElement) {
		super(parent, style);
		this.element = specElement;
		setLayout(new GridLayout(1, false));

		Group grpBasicinfo = new Group(this, SWT.NONE);
		GridData gd_grpBasicinfo = new GridData(SWT.FILL, SWT.CENTER, true,	false, 1, 1);
		gd_grpBasicinfo.heightHint = 317;
		gd_grpBasicinfo.widthHint = 560;
		grpBasicinfo.setLayoutData(gd_grpBasicinfo);
		grpBasicinfo.setText("Basics");
		grpBasicinfo.setLayout(new GridLayout(6, false));

		Label lblLabel = new Label(grpBasicinfo, SWT.NONE);
		lblLabel.setText("Label");

		txtLabel = new Text(grpBasicinfo, SWT.BORDER);
		GridData gd_txtLabel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtLabel.widthHint = 80;
		gd_txtLabel.minimumWidth = 50;
		txtLabel.setLayoutData(gd_txtLabel);
		txtLabel.addListener(SWT.Modify, editor.getModifyListener());

		Label lblName = new Label(grpBasicinfo, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name");

		txtName = new Text(grpBasicinfo, SWT.BORDER);
		GridData gd_txtName = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtName.widthHint = 350;
		gd_txtName.minimumWidth = 100;
		txtName.setLayoutData(gd_txtName);
		txtName.addListener(SWT.Modify, editor.getModifyListener());

		Label lblKind = new Label(grpBasicinfo, SWT.NONE);
		lblKind.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,	1, 1));
		lblKind.setText("Kind");

		cbKind = new Combo(grpBasicinfo, SWT.NONE);
		GridData gd_cbKind = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,	1);
		gd_cbKind.widthHint = 20;
		cbKind.setLayoutData(gd_cbKind);
		cbKind.addListener(SWT.Modify, editor.getModifyListener());
		
		Label structeredId = new Label(grpBasicinfo, SWT.NONE);
		structeredId.setText("Structured ID");
		
		txtStructeredId = new Text(grpBasicinfo, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_txtStructeredId = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtStructeredId.widthHint = 80;
		gd_txtStructeredId.minimumWidth = 50;
		txtStructeredId.setLayoutData(gd_txtStructeredId);
		txtStructeredId.setText(getStructeredId());
		txtStructeredId.setToolTipText(txtStructeredId.getText());
		
		new Label(grpBasicinfo, SWT.NONE);
		
		SashForm sashButtons = new SashForm(grpBasicinfo, SWT.NONE);
		GridData gd_buttons = new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1);
		sashButtons.setLayoutData(gd_buttons);
		
		Button btnExplain = new Button(sashButtons, SWT.NONE);
		btnExplain.setText("Explain");
		btnExplain.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				IWorkbench wb = PlatformUI.getWorkbench();
				IWorkbenchWindow window = wb.getActiveWorkbenchWindow();
				IWorkbenchPage page = window.getActivePage();
				IEditorPart editor = page.getActiveEditor();
				PlatformUI.getWorkbench().getHelpSystem().displayHelpResource(((BaseEditor) editor).getHelpResourceURL());
			}
		});
		sashButtons.layout();
		

		final Button btnCheck = new Button(sashButtons, SWT.NONE);
		btnCheck.setText("Check Element");
		final CheckResultBubble resultBubble = new CheckResultBubble(btnCheck, element);
		btnCheck.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				element.checkStructure();
				element.checkChildren();
				resultBubble.show();
			}
		});
		
		
		Label lblDescription = new Label(grpBasicinfo, SWT.NONE);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,	false, 1, 1));
		lblDescription.setText("Description");

		rteDescription = new RichTextEditor(grpBasicinfo, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL, editor.getEditorSite() , editor.commandListener);
		GridData gd_txtDescription = new GridData(SWT.FILL, SWT.FILL, true, true, 5, 5);
		gd_txtDescription.widthHint = 550;
		gd_txtDescription.heightHint = 70;
		rteDescription.setLayoutData(gd_txtDescription);
		//rteDescription.addListener(SWT.Modify, editor.getModifyListener());
		ModifyListener modifyList = new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				editor.getModifyListener().handleEvent(null);
			}
		};
		rteDescription.addModifyListener(modifyList);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public void setLabel(String label) {
		txtLabel.setText(label);
	}

	public String getLabel() {
		return txtLabel.getText();
	}

	public Text getLabelTextBox() {
		return txtLabel;
	}

	public void setName(String name) {
		txtName.setText(name);
	}

	public String getName() {
		return txtName.getText();
	}

	public Text getNameTextBox() {
		return txtName;
	}

	public void setKindItems(String[] items) {
		cbKind.setItems(items);
	}

	public void setKind(String item) {
		String[] items = cbKind.getItems();
		for (int i = 0; i < items.length; i++) {
			if (item.equals(items[i])) {
				cbKind.select(i);
			}
		}
	}

	public String getKind() {
		if (cbKind.getSelectionIndex() >= 0) {
			String[] items = cbKind.getItems();
			return items[cbKind.getSelectionIndex()];
		}
		return "";
	}

	public Combo getKindComboBox() {
		return cbKind;
	}

	public void setDescription(String description) {
		rteDescription.setText(description);
	}

	public String getDescription() {
		return rteDescription.getText();
	}

	public RichTextEditor getDescriptionRTE() {
		return rteDescription;
	}

	public void fillInitialData(Element spElement, String[] elementKinds) {

		if (elementKinds.length > 0) {
			cbKind.setEnabled(true);
			setKindItems(elementKinds);
		} else {
			setKindItems(new String[] { "unspecified" });
			cbKind.setEnabled(false);
		}

		// fill basic info
		if (spElement.getLabel() != null) {
			setLabel(spElement.getLabel());
		}
		if (spElement.getName() != null) {
			setName(spElement.getName());
		}
		if (spElement.getElementKind() != null
				&& !spElement.getElementKind().isEmpty()) {
			setKind(spElement.getElementKind());
		} else {
			setKind("unspecified");
		}
		if (spElement.getDescription() != null) {
			setDescription(spElement.getDescription());
		}
	}
	
	/**
	 *  T[.DOM[.SEC[.PART]]].LAB
	 * @return
	 */
	private String getStructeredId() {
		// T - Type
		String str = element.getStructeredIdType();
		
		Element folder1 = null, folder2 = null;
		
		//DOM/SEC/PART - the (simple) labels of the 1st/2nd/3rd level of folder counting from top in which a given element resides
		String folders = "";
		if (element.getParent() instanceof Folder) {
			folder1 = element.getParent();
			if (folder1.getLabel() != null && !folder1.getLabel().isEmpty()) {
				folders += "." + folder1.getLabel().replaceAll(" ", "");
			} else {
				folders += "." + folder1.getName().replaceAll(" ", "");				
			}
		}		
		if (folder1 != null && folder1.getParent() instanceof Folder) {
			folder2 = folder1.getParent();
			if (folder2.getLabel() != null && !folder2.getLabel().isEmpty()) {
				folders = "." + folder2.getLabel().replaceAll(" ", "") + folders;
			} else {
				folders = "." + folder2.getName().replaceAll(" ", "") + folders;				
			}
		}
		if (folder2 != null && folder2.getParent() instanceof Folder) {
			if (folder2.getParent().getLabel() != null && !folder2.getParent().getLabel().isEmpty()) {
				folders = "." + folder2.getParent().getLabel().replaceAll(" ", "") + folders;
			} else {
				folders = "." + folder2.getParent().getName().replaceAll(" ", "") + folders;				
			}
		}
		str += folders;
		
		// LAB - the (simple) label of the element
		if (element.getLabel() != null && !element.getLabel().isEmpty()) {
			str += "." + element.getLabel();
		} else { 
			str += "." + element.getName();
		}
		
		
		return str;
	}
}
