package dk.dtu.imm.red.core.ui.internal.preferences;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ScalableColumnLayoutAdvisor;

public class ServersTableLayoutAdviser extends ScalableColumnLayoutAdvisor{

	public static final int NAME_COLUMN = 0;
	public static final int SERVICE_COLUMN = 1;
	public static final int SERVER_COLUMN = 2;
	public static final int USERNAME_COLUMN = 3;
	public static final int PASSWORD_COLUMN = 4;
	public static final int EXTRA_PARAMS_COLUMN = 5;
	public static final int NUM_COLUMNS = 6;
	private int rowCount;
	
	public ServersTableLayoutAdviser(AgileGrid agileGrid) {
		super(agileGrid);

		rowCount = 0;
	}
	
	@Override
	public int getColumnCount() {
		return NUM_COLUMNS;
	}

	@Override
	public String getTopHeaderLabel(final int col) {
		switch (col) {
		case NAME_COLUMN:
			return "Name";
		case SERVICE_COLUMN:
			return "Service";
		case SERVER_COLUMN:
			return "Server";
		case USERNAME_COLUMN:
			return "Username";
		case PASSWORD_COLUMN:
			return "Password";
		case EXTRA_PARAMS_COLUMN:
			return "ExtraParams";			
		default:
			return super.getTopHeaderLabel(col);
		}
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	@Override
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	@Override
	public int getInitialColumnWidth(int column) {
		switch (column) {
			case NAME_COLUMN:
				return 10;		
			case SERVICE_COLUMN:
				return 8;
			case SERVER_COLUMN:
				return 20;
			case USERNAME_COLUMN:
				return 10;
			case PASSWORD_COLUMN:
				return 10;
			case EXTRA_PARAMS_COLUMN:
				return 20;				
			default:
				return 0;
		}
	}
}
