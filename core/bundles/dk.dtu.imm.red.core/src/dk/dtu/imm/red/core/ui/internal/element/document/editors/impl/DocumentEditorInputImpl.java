package dk.dtu.imm.red.core.ui.internal.element.document.editors.impl;

import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;

public class DocumentEditorInputImpl extends BaseEditorInput{

	public DocumentEditorInputImpl(Document document) {
		super(document);
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public String getName() {
		return "Document editor";
	}

	@Override
	public String getToolTipText() {
		return "Editor for document " + element.getName();
	}
}
