package dk.dtu.imm.red.core.ui;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

public final class SWTUtils {
	
	private SWTUtils () {
		// prevent instantiation
	}

	public static ScrolledComposite wrapInScrolledComposite(Composite parent, Composite composite) {
	
		final ScrolledComposite sc = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		
		if (!composite.setParent(sc)) {
			return null;
		}
		
		sc.setContent(composite);
		sc.setExpandVertical(true);
		sc.setExpandHorizontal(true);
		sc.setMinSize(composite.computeSize(SWT.DEFAULT,
	            SWT.DEFAULT));
		
		return sc;
		
	}
	
	public static byte[] convertImageToByteArrayData(Image image){
		ImageLoader imgSaver = new ImageLoader();
		if (image != null && image.getImageData() != null) {
			imgSaver.data = new ImageData[] {image.getImageData()};
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			imgSaver.save(outputStream, SWT.IMAGE_PNG);
			return outputStream.toByteArray();
		}
		return null;
	}
	
	public static Image convertByteImageDataToImage(byte[] imageData){
		if (imageData != null 
				&& imageData.length > 0) {
			ImageLoader imageLoader = new ImageLoader();
			ByteArrayInputStream inputStream = 
					new ByteArrayInputStream(imageData);
			ImageData[] imageDataArray = imageLoader.load(inputStream);
			
			if (imageDataArray != null && imageDataArray.length > 0) {
				Image image = new Image(Display.getCurrent(), imageDataArray[0]);
				return image;
			}
		}
		return null;
	}
}
