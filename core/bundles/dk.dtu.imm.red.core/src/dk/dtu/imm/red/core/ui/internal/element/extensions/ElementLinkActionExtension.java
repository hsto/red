package dk.dtu.imm.red.core.ui.internal.element.extensions;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.epf.richtext.actions.ILinkActionExtension;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.internal.LogUtil;

public class ElementLinkActionExtension implements ILinkActionExtension {
	
	public static void elementClicked(final Element target) {
		IConfigurationElement[] configurationElements = Platform
				.getExtensionRegistry().getConfigurationElementsFor(
						"dk.dtu.imm.red.core.element");

		try {
			for (IConfigurationElement configurationElement 
					: configurationElements) {

				final Object extension = configurationElement
						.createExecutableExtension("class");

				if (extension instanceof IElementExtensionPoint) {
					ISafeRunnable runnable = new ISafeRunnable() {

						@Override
						public void handleException(final Throwable exception) {
							exception.printStackTrace();
						}

						@Override
						public void run() throws Exception {
							((IElementExtensionPoint) extension)
								.openElement(target);
						}
					};
					runnable.run();
				}
			}
		} catch (CoreException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (Exception e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}
	
	@Override
	public void linkClicked(final String target) {
	}

}
