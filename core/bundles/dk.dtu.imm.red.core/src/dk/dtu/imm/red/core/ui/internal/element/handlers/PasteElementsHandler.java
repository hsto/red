package dk.dtu.imm.red.core.ui.internal.element.handlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class PasteElementsHandler extends ClipboardHandler {

	@Override
	protected void execute(ExecutionEvent event, Clipboard clipboard)
			throws ExecutionException {
		
		Group targetParent = null;
		
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection.isEmpty()) {
			targetParent = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		} else if (selection instanceof IStructuredSelection) {
			Object pasteInto = 
					((IStructuredSelection) selection).getFirstElement();
			
			if (pasteInto instanceof Group) {
				targetParent = (Group) pasteInto;
			} else {
				return;
			}
		}
		
		Transfer transferType = TextTransfer.getInstance();
		
		Object pasteContents = clipboard.getContents(transferType);
		if (pasteContents instanceof String) {
			String pasteString = (String) pasteContents;
			
			InputStream inputStream = new ByteArrayInputStream(
					pasteString.getBytes());

			Resource resource = new XMLResourceImpl();
			try {
				resource.load(inputStream, null);
			} catch (IOException e) {
				// Probably invalid content, so just ignore it
				return;
			}
			
			Folder pseudoFolder = (Folder) resource.getContents().get(0);
			
			EObject[] pastedObjects = 
					pseudoFolder.getContents().toArray(new EObject[0]);
			
			for (int i = 0; i < pastedObjects.length; i++) {
				EObject pastedObject = pastedObjects[i];
				if (pastedObject instanceof Element) {
					if (targetParent instanceof Workspace) {
						FileDialog dialog = new FileDialog(Display.getCurrent()
										.getActiveShell(), SWT.SAVE);
						dialog.setFilterExtensions(new String[] {".red"});
						String fileName = dialog.open();
						
						File file = FileFactory.eINSTANCE.createFile();
						file.getContents().add((Element) pastedObject);
						file.setUri(fileName);
						file.setName(fileName.substring(fileName.lastIndexOf(
								java.io.File.separator) + 1));
						WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
							.addOpenedFile(file);
					} else {
						targetParent.getContents().add((Element) pastedObject);
					}
				}
				((Element) pastedObject).save();
			}
		}
	}

}
