package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.DefaultCellEditorProvider;

public class TableCellEditorProvider extends DefaultCellEditorProvider {
	

	public TableCellEditorProvider(AgileGrid agileGrid) {
		super(agileGrid);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.agilemore.agilegrid.DefaultCellEditorProvider#canEdit(int, int)
	 */
	@Override
	public boolean canEdit(int row, int col) {
		return true;
	}
	
	

}
