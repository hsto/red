package dk.dtu.imm.red.core.ui.widgets;

import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.widgets.ElementTreeViewPresenter;

/**
 * A generic element treeview widget, which can be used in a number of
 * situations where navigation and selection of the created elements in the
 * workspace is needed.
 * 
 * @author Jakob Kragelund
 * 
 */
public class ElementTreeViewWidget extends Composite implements
		ISelectionProvider {

	/**
	 * Internal treeviewer, which is the visual component of this widget.
	 */
	protected TreeViewer treeViewer;

	/**
	 * Content provider for the treeviewer, which provides the elements to be
	 * shown.
	 */
	protected ElementContentProvider contentProvider;

	/**
	 * Label provider for the treeviewer, which provides labels and images for
	 * the elements in the treeviewer.
	 */
	protected ElementLabelProvider labelProvider;

	/**
	 * Presenter for this widget, which handles all default logic. Clients may
	 * add their own logic by adding listeners.
	 */
	protected ElementTreeViewPresenter presenter;
	
	public ElementTreeViewWidget(Composite parent, int style) {
		this(false, parent, style, null);
	}
	
	public ElementTreeViewWidget(final boolean showOnlyGroups,
			final Composite parent, final int style) {
		this(showOnlyGroups, parent, style, null);
	}

	public ElementTreeViewWidget(final boolean showOnlyGroups,
			final Composite parent, final int style, final Class<Element>[] typesShown) {
		this(showOnlyGroups, parent, style, typesShown, null, null, false, false);
	}
	
	public ElementTreeViewWidget(final boolean showOnlyGroups, final Composite parent, 
			final int style, final Class<Element>[] typesShown, final Class<Element>[] typesExcluded) {
		this(showOnlyGroups, parent, style, typesShown, typesExcluded, null, false, false);
	}
	
	public ElementTreeViewWidget(final boolean showOnlyGroups,
			final Composite parent, final int style, final Class<Element>[] typesShown, 
			final Element[] excludedElements) {
		this(showOnlyGroups, parent, style, typesShown, null, excludedElements, false,false);
	}
	
	public ElementTreeViewWidget(boolean showOnlyGroups, Composite parent, int style,
			boolean showFiles,boolean showProjects) {
		this(showOnlyGroups, parent, style, null, null, null, showFiles,showProjects);
	}
	
	
	public ElementTreeViewWidget(final boolean showOnlyGroups, final Composite parent, 
			final int style, final Class<Element>[] typesShown, final Class<Element>[] typesExcluded, 
			final Element[] excludedElements, final boolean showFiles,final boolean showProjects) {
		super(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		setLayout(layout);
		setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		contentProvider = 
				new ElementContentProvider(showOnlyGroups, typesShown, typesExcluded, excludedElements, showFiles,showProjects);
		labelProvider = new ElementLabelProvider();
		treeViewer = new TreeViewer(this, style);
		treeViewer.getTree().setLayoutData(
				new GridData(GridData.FILL, GridData.FILL, true, true));

		treeViewer.setLabelProvider(labelProvider);
		treeViewer.setContentProvider(contentProvider);
		
		presenter = new ElementTreeViewPresenter(this);
		
		getTree().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(final MouseEvent e) {
				TreeItem item = getTree().getItem(new Point(e.x, e.y));
				if (item == null) {
					setSelection(null);
				}
			}
		});

	}

	

	public void addDoubleClickListener(final IDoubleClickListener listener) {
		treeViewer.addDoubleClickListener(listener);
	}

	public void removeDoubleClickListener(final IDoubleClickListener listener) {
		treeViewer.removeDoubleClickListener(listener);
	}

	public void addDragSupportListener(final int operations,
			final Transfer[] transferTypes, final DragSourceListener listener) {
		treeViewer.addDragSupport(operations, transferTypes, listener);
	}

	public void addDropSupportListener(final int operations,
			final Transfer[] transferTypes, final DropTargetListener listener) {
		treeViewer.addDropSupport(operations, transferTypes, listener);
	}

	@Override
	public ISelection getSelection() {
		return treeViewer.getSelection();
	}

	@Override
	public void removeSelectionChangedListener(
			final ISelectionChangedListener listener) {
		treeViewer.removeSelectionChangedListener(listener);
	}

	@Override
	public void setSelection(final ISelection selection) {
		treeViewer.setSelection(selection);
	}

	@Override
	public void addSelectionChangedListener(
			final ISelectionChangedListener listener) {
		treeViewer.addSelectionChangedListener(listener);
	}

	public Tree getTree() {	
		return treeViewer.getTree();
	}

	public TreeViewer getViewer() {
		return treeViewer;
	}
	
	public void setSorter(ViewerSorter sorter) {
		treeViewer.setSorter(sorter);
	}
	

}
