package dk.dtu.imm.red.core.ui.internal.widgets;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;

import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class ElementTreeViewPresenter {

	protected ElementTreeViewWidget widget;

	/**
	 * Listener on the workspace, which is shown in the element explorer.
	 */
	private EContentAdapter workspaceListener = new EContentAdapter() {
		@Override
		public void notifyChanged(final Notification msg) {
			super.notifyChanged(msg);
			if (msg.getEventType() == Notification.ADD
					|| msg.getEventType() == Notification.REMOVE
					|| msg.getEventType() == Notification.ADD_MANY
					|| msg.getEventType() == Notification.REMOVE_MANY
					|| msg.getEventType() == Notification.SET
					|| msg.getEventType() == Notification.UNSET
					|| msg.getEventType() == Notification.MOVE) {
				if (!widget.isDisposed()) {
					// TODO: Find out why the first refresh doesn't work
//					widget.treeViewer.refresh((Element) msg.getNewValue());
					widget.getViewer().refresh();
				}
			}
			
			if (msg.getEventType() == Notification.ADD 
					&& msg.getNewValue() != null) {
				IStructuredSelection selection = 
						new StructuredSelection(msg.getNewValue());
				widget.setSelection(selection);
			}
		}
	};

	public ElementTreeViewPresenter(ElementTreeViewWidget widget) {
		this.widget = widget;

		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		workspace.eAdapters().add(workspaceListener);
		widget.getViewer().setInput(workspace);
	}

}
