package dk.dtu.imm.red.core.ui.internal.element.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.element.operations.DeleteElementsOperation;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class DeleteElementHandler extends AbstractHandler implements IHandler {

	

	/**
	 * Creates the two dialog boxes used for confirming the delete.
	 */
	public DeleteElementHandler() {
		super();
	}

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		
		ISelection selectedObjects = HandlerUtil.getCurrentSelection(event);
		if (selectedObjects instanceof IStructuredSelection) {
			List<Element> selectedElements = new ArrayList<Element>();
			
			Object[] selectedObjectsArray = 
					((IStructuredSelection) selectedObjects).toArray();
			for (Object object : selectedObjectsArray) {
				if(object instanceof Project)
				{
					selectedElements.add((Project) object);
					selectedElements.addAll(WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedFiles());
					
			   }
				else if (object instanceof Element) 
				{
					selectedElements.add((Element) object);
				}
			}
			
			DeleteElementsOperation operation = 
					new DeleteElementsOperation(selectedElements);
			operation.addContext(PlatformUI.getWorkbench().getOperationSupport()
					.getUndoContext());

			OperationHistoryFactory.getOperationHistory()
				.execute(operation, null, null);
			
		}
		
		return null;
	}

}
