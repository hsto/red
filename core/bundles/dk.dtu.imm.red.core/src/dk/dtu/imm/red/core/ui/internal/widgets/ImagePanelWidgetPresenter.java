package dk.dtu.imm.red.core.ui.internal.widgets;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.widgets.operations.ChangeImageOperation;
import dk.dtu.imm.red.core.ui.presenters.BasePresenter;
import dk.dtu.imm.red.core.ui.widgets.ImagePanelWidget;

public class ImagePanelWidgetPresenter extends BasePresenter {
	
	protected Image image;
	protected Image originalImage;
	protected ImagePanelWidget imagePanelWidget;
	
	public ImagePanelWidgetPresenter(ImagePanelWidget imagePanelWidget, Image image) {
		this.imagePanelWidget = imagePanelWidget;
		this.image = image;
	}

	public Image getOriginallImage()
	{
		return originalImage;
	}
	
	public Image getImage() {
		return image;
	}

	public void loadNewImage() {
		changeImage(true);
		imagePanelWidget.refreshImage();
	}

	public void switchToDefaultImage() {
		changeImage(false);
		imagePanelWidget.refreshImage();
	}
	
	/**
	 * Change the image, either to a new image loaded from the
	 * file system, or the default image.
	 * @param openNewImage True if a new image should be loaded, false if the
	 * default image should be used.
	 */
	private void changeImage(final boolean openNewImage) {
		ChangeImageOperation operation = 
				new ChangeImageOperation(this, openNewImage);
		operation.addContext(getUndoContext());

		try {
			OperationHistoryFactory.getOperationHistory()
				.execute(operation, null, null);
		} catch (ExecutionException e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

	/**
	 * Sets the image in the widget to the specified image.
	 * @param newImage the new image to be shown in the widget
	 */
	public void setImage(final Image newImage) {
		this.image = scaleImageToBounds(newImage);
		this.originalImage = newImage;	
		imagePanelWidget.refreshImage();
	}
	
	/**
	 * Scales the given image to fit inside the bounds of the widget.
	 * @param image the image to scale to the widget
	 * @return the scaled image
	 */
	private Image scaleImageToBounds(Image image) {
		if (image == null) {
			return image;
		}
		
		int width = image.getBounds().width;
		int height = image.getBounds().height;
		int canvasWidth = imagePanelWidget.getImageContainer().getBounds().width;
		int canvasHeight = imagePanelWidget.getImageContainer().getBounds().height;
		
		if (width > canvasWidth || height > canvasHeight) {
			int newWidth = width;
			int newHeight = height;
			float ratio = ((float) width) / ((float) height);

			if (width > canvasWidth) {
				newWidth = canvasWidth;
				newHeight = (int) (((float) newWidth) / ratio);
			}
			if (newHeight > canvasHeight) {
				newHeight = canvasHeight;
				newWidth = (int) (((float) newHeight) * ratio);
			}

			image = new Image(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell().getDisplay(), image
					.getImageData().scaledTo(newWidth, newHeight));
		}
		return image;
	}
	
	public int centerImageHorizontal(){
		
		int xCoordinat = 0;
		
		if (image != null){

			int imageWidth = image.getBounds().width;
			int canvasWidth = imagePanelWidget.getImageCanvas().getBounds().width;

			if (imageWidth < canvasWidth){
				xCoordinat = (canvasWidth - imageWidth) / 2;
			}
		}
		return xCoordinat;
	}
	
	public int centerImageVertical() {
		
		int yCoordinat = 0;
		
		if (image != null){
		
			int imageHeight = image.getBounds().height;
			int canvasHeight = imagePanelWidget.getImageCanvas().getBounds().height;

			if (imageHeight < canvasHeight){
				yCoordinat = (canvasHeight - imageHeight) / 2;
			}
		}
		
		return yCoordinat;
	}

	public boolean isDisposed() {
		return imagePanelWidget.isDisposed();
	}
}
