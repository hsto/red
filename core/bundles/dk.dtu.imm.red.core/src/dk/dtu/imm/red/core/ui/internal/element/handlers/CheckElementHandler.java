package dk.dtu.imm.red.core.ui.internal.element.handlers;


import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.element.dialog.CheckResultBubble;
 

public class CheckElementHandler extends AbstractHandler implements IHandler {
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ArrayList<Element> checkedElements = new ArrayList<Element>();
		
		ISelection selectedObjects = HandlerUtil.getCurrentSelection(event);
		if (selectedObjects instanceof IStructuredSelection) {
			
			Object[] selectedObjectsArray = ((IStructuredSelection) selectedObjects).toArray();
			for (Object object : selectedObjectsArray) {
				if (object instanceof Element) {
					((Element) object).checkStructure();
					checkedElements.add((Element) object);
				}
			}			
		}
		
		CheckResultBubble resultBubble = new CheckResultBubble(null, checkedElements.toArray(new Element[0]));
		resultBubble.show();
		
		return null;
	}
}
