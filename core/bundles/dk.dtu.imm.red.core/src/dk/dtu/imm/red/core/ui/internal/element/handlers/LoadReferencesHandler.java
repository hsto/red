package dk.dtu.imm.red.core.ui.internal.element.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.IHandler;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.util.FileHelper;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.file.FileFactory;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.core.ui.internal.project.editors.impl.ModelProvide;
/**
* Handler for the load references. Opens a window, which guides the
* user to load references.
* 
* @author Ahmed Fikry
* 
*/
public class LoadReferencesHandler extends AbstractHandler implements IHandler {

	@SuppressWarnings("resource")
	public Object execute(ExecutionEvent event) {
		ISelection selectedObject = HandlerUtil.getCurrentSelection(event);
		if (selectedObject instanceof IStructuredSelection) {

			IStructuredSelection selection = (IStructuredSelection) selectedObject;
			Element selectedElement = (Element) selection.getFirstElement();

			BaseEditorInput input = new BaseEditorInput(selectedElement);
			
			File file = FileFactory.eINSTANCE.createFile();
			file.getContents().add(selectedElement);
			IEditorPart editor = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.findEditor(input);

			String fileName = FileHelper.getFileName(selectedElement);
			String directoryPath = FileHelper.getDirectoryPath(selectedElement);
			
			LoadFileHandler loadfile = new LoadFileHandler();
			loadfile.list_References.clear();
			loadfile.FilesReferences(fileName, directoryPath);
			int index = loadfile.list_References.indexOf(fileName);
			loadfile.list_References.remove(index);
			if (loadfile.list_References.size() != 0) {
				loadfile.ListUi(loadfile.list_References, directoryPath);
			} else {
				MessageBox messageBox = new MessageBox(
						HandlerUtil.getActiveShell(event), SWT.ICON_INFORMATION
								| SWT.OK);

				messageBox
						.setMessage("The selected file has no references to other files in this project!");
				messageBox.setText("Load References");
				messageBox.open();
			}
		}
		return null;
	}

}
