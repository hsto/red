package dk.dtu.imm.red.core.ui.internal.search.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import dk.dtu.imm.red.core.element.SearchResultWrapper;

public class SearchContentProvider implements ITreeContentProvider {

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof SearchResultWrapper) {
			return getChildren(inputElement);
		}
		return null;
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		List<Object> children = new ArrayList<Object>();
		
		if (parentElement instanceof SearchResultWrapper) {
			SearchResultWrapper results = (SearchResultWrapper) parentElement;
			
			children.addAll(results.getElementResults());
			children.addAll(results.getChildResults());
		}
		
		return children.toArray();
	}

	@Override
	public Object getParent(Object element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		return element instanceof SearchResultWrapper;
	}
}
