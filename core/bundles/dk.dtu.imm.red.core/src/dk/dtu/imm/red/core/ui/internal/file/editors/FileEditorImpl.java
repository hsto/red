/*
 * @author Ahmed Fikry
 */
package dk.dtu.imm.red.core.ui.internal.file.editors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.ElementBasicInfoContainer;
import dk.dtu.imm.red.core.ui.internal.project.editors.impl.ProjectEditorImpl;

public class FileEditorImpl extends BaseEditor implements FileEditor {

	protected int filePageIndex=0;

	protected ElementBasicInfoContainer basicInfoContainer;
	public RichTextEditor longDescriptionEditor;


	@Override
	public void doSave(IProgressMonitor monitor)
	{
		FileEditorPresenter presenter = (FileEditorPresenter) this.presenter;
		//set the spec elements basic info into the presenter
		presenter.setSpecElementBasicInfo(basicInfoContainer.getLabel(),
				basicInfoContainer.getName(), basicInfoContainer.getKind(),
				basicInfoContainer.getDescription());
		presenter.setLongDescription(longDescriptionEditor.getText());

		super.doSave(monitor);

		//super.doSave(monitor);
	   // update file table in the project editior
		ProjectEditorImpl.refreshTable();
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		super.init(site, input);

		presenter = new FileEditorPresenterImpl(this, (File) element);
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void fillInitialData()
	{

		File file = (File) element;
		basicInfoContainer.fillInitialData(file, new String[]{});
		if (file.getDescription() != null) {
			longDescriptionEditor.setText(file.getLongDescription().toString());
			} else {
			longDescriptionEditor.setText("");
		}
		isDirty = false;
		firePropertyChange(PROP_DIRTY);

	}

	@Override
	protected void createPages() {


		filePageIndex = addPage(createFileInfoPage(getContainer()));
		setPageText(filePageIndex, "Summary");
		super.createPages();

		fillInitialData();
	}

	private Composite createFileInfoPage(Composite parent) {

		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		//add the basic info fields
		GridData basicInfoLayoutData = new GridData();
		basicInfoLayoutData.horizontalAlignment = SWT.FILL;
		basicInfoLayoutData.grabExcessHorizontalSpace = true;
		basicInfoLayoutData.horizontalSpan=1;
		basicInfoLayoutData.verticalSpan=1;

		basicInfoContainer = new ElementBasicInfoContainer(composite,SWT.NONE, this, (Element) element);
		basicInfoContainer.setLayoutData(basicInfoLayoutData);
		basicInfoContainer.getLabelTextBox().setEnabled(false);

		Label longDescLabel = new Label(composite, SWT.NONE);
		longDescLabel.setText("Long Description:");

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalSpan = 2;
		editorLayoutData.widthHint = 400;
		editorLayoutData.heightHint = 200;
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessVerticalSpace = true;

		longDescriptionEditor = new RichTextEditor(composite, SWT.BORDER |
				SWT.WRAP | SWT.V_SCROLL, getEditorSite(), commandListener);
		longDescriptionEditor.setLayoutData(editorLayoutData);

		longDescriptionEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getEditorID() {
		return "dk.dtu.imm.red.core.file.fileeditor";
	}

	@Override
	public String getHelpResourceURL() {
		// TODO Auto-generated method stub
		return null;
	}

}
