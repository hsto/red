package dk.dtu.imm.red.core.ui.internal.element.document.wizards;

import dk.dtu.imm.red.core.ui.wizards.IBaseWizard;

public interface NewDocumentWizard extends IBaseWizard {

	String ID = "dk.dtu.imm.red.core.element.document.newwizard";
}
