package dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid; 
import org.agilemore.agilegrid.DefaultContentProvider;   
import org.eclipse.swt.widgets.Listener;

public class EAGContentProvider<T> extends DefaultContentProvider {  
	private Listener modifyListener;
	private ExtendedAgileGrid<?> reflAgileGrid; 
	
	public EAGContentProvider(ExtendedAgileGrid<?> reflAgileGrid, EAGPresenter<T> presenter, Listener modifyListener) {
		super();  
		this.modifyListener = modifyListener;
		this.reflAgileGrid = reflAgileGrid;
	}

	@Override
	public void doSetContentAt(final int row, final int col, final Object value) {  
		if(((ExtendedAgileGrid<T>)this.reflAgileGrid).getPresenter().doSetContentAt(row, col, value)) {
			modifyListener.handleEvent(null);;  
		}
	}  
	
	@Override
	public Object doGetContentAt(final int row, final int col) {
		EAGPresenter<T> presenter = ((ExtendedAgileGrid<T>)this.reflAgileGrid).getPresenter();
		if(presenter.getDataSourceView() == null || presenter.getColumnDefinitions() == null ) return null;  
		
		Object content = presenter.doGetContentAt(row, col); 
		return content;
	}  
}
