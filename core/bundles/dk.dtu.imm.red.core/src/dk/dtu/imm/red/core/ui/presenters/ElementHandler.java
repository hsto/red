package dk.dtu.imm.red.core.ui.presenters;
 
import org.eclipse.emf.ecore.util.EcoreUtil; 
import dk.dtu.imm.red.core.element.Element; 

public class ElementHandler<T extends Element> {
	
	T element;
	//T elementBeforeEdit;
	
	public T getElement() {
		return element;
	}
	
	public void setElement(T value) {
		//T cop = (T) EcoreUtil.copy(value);
		element =  value;
		//elementBeforeEdit = value; 
	} 
	 
	public void save() { 
		element.save();
//		EcoreUtil.replace(elementBeforeEdit, element); 
//		element.save();
//		T cop = (T) EcoreUtil.copy(element); 		
//		elementBeforeEdit = (T) cop;
	} 
	
	public T getElementBeforeEdit() {
		return element;
	}

	public Element getParent() {
		return element.getParent();
	}
	
}
