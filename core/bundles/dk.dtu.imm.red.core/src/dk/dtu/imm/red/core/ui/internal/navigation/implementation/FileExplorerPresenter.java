package dk.dtu.imm.red.core.ui.internal.navigation.implementation;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.views.BaseViewPresenter;
import dk.dtu.imm.red.core.ui.views.IViewPresenter;

public class FileExplorerPresenter extends BaseViewPresenter
		implements IViewPresenter {

	public void elementDoubleClicked(final List<Element> elements) {
		IConfigurationElement[] configurationElements = Platform
				.getExtensionRegistry().getConfigurationElementsFor(
						"dk.dtu.imm.red.core.element");
		try {
			for (IConfigurationElement configurationElement 
					: configurationElements) {

				final Object extension = configurationElement
						.createExecutableExtension("class");

				if (extension instanceof IElementExtensionPoint) {
					ISafeRunnable runnable = new ISafeRunnable() {

						@Override
						public void handleException(final Throwable exception) {
							exception.printStackTrace();
						}

						@Override
						public void run() throws Exception {
							for (Element element : elements) {
								((IElementExtensionPoint) extension)
										.openElement(element);
							}
						}
					};
					runnable.run();
				}
			}
		} catch (CoreException e) {
			e.printStackTrace(); LogUtil.logError(e);
		} catch (Exception e) {
			e.printStackTrace(); LogUtil.logError(e);
		}
	}

}
