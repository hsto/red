package dk.dtu.imm.red.core.ui.internal.navigation.implementation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.IHandlerService;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.element.handlers.MoveElementsHandler;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class ElementDropListener extends ViewerDropAdapter {

	private Object target;
	private int location;

	protected ElementDropListener(Viewer viewer) {
		super(viewer);
		this.setFeedbackEnabled(true);
	}

	@Override
	public boolean performDrop(final Object data) {
		
		// The dragged data is converted to a string
		if (!(data instanceof String)) {
			return false;
		}
		
		// get the location and target.
		// location is LOCATION_BEFORE, _AFTER or _ON
		location = getCurrentLocation();
		target = getCurrentTarget();

		// convert the dragged data (which is XML format) to an object
		InputStream inputStream = new ByteArrayInputStream(
				((String) data).getBytes());

		Resource resource = new XMLResourceImpl();
		try {
			resource.load(inputStream, null);
		} catch (IOException e) {
			e.printStackTrace(); LogUtil.logError(e);
			return false;
		}
		
		Folder pseudoFolder = (Folder) resource.getContents().get(0);

		List<Element> draggedElements = new ArrayList<Element>();
		for (EObject object : pseudoFolder.getContents()) {
			if (object instanceof Element) {
				Element realElement = WorkspaceFactory.eINSTANCE
						.getWorkspaceInstance().findDescendantByUniqueID(
								((Element) object).getUniqueID());
				
				if (realElement instanceof Group && target instanceof Element) {
					Group realElementGroup = (Group) realElement;
					Element targetElement = (Element) target;
					if (realElementGroup.findDescendantByUniqueID(
							targetElement.getUniqueID()) != null) {
						continue;
					}
				}
				
				if (realElement != target && !(target instanceof Project)) {
					draggedElements.add(realElement);
				}
			}
		}

		IHandlerService handlerService = (IHandlerService) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getService(IHandlerService.class);
		ICommandService commandService = (ICommandService) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getService(ICommandService.class);

		Command command = commandService.getCommand(MoveElementsHandler.ID);

		Event event = new Event();
		event.data = draggedElements;

		Map<String, String> parameters = new HashMap<String, String>();

		if (target instanceof Element) {
			parameters.put(MoveElementsHandler.TARGET_PARAM_ID,
					((Element) target).getUniqueID());
			parameters.put(MoveElementsHandler.LOCATION_PARAM_ID, ""+location);
		}
		

		ParameterizedCommand parameterizedCommand = ParameterizedCommand
				.generateCommand(command, parameters);

		try {
			handlerService.executeCommand(parameterizedCommand, event) ;
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public boolean validateDrop(final Object target, final int operation,
			final TransferData transferType) {
//		return (target == null || target instanceof Group);
		return true;
	}

}
