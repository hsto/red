package dk.dtu.imm.red.core.ui.internal.traceability.stepper.views;

import java.util.List;

import org.agilemore.agilegrid.IContentProvider;
import org.eclipse.emf.common.util.EList;
import org.eclipse.swt.graphics.Image;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;

public interface TraceStepperContentProvider extends IContentProvider{
	public Element getContent();
	public void setContent(EList<ElementRelationship> content);
	
	/**
	 * Returns a String array containing all '<em><b>RelationshipKind</b></em>' literals by name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the String array.
	 */
	public String[] getRelationshipKindList();
	
	/**
	 * Returns a String array containing all '<em><b>ElementKind</b></em>' literals by name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the String array.
	 */
	public String[] getElementKindList();
	
	public void setFocusElement(Element focusElement);
	
	public Element getFocusElement();
	
	public List<Element> getDataForSourceTable();
	
	public List<Element> getDataForTargetTable();
	
	public ElementColumnDefinition[] getToColumnDefinition();

	public ElementColumnDefinition[] getFromColumnDefinition();
	
	public void setElementFilter(String filter, boolean show);
	
	public void setRelationshipFilter(String filter, boolean show);
}
