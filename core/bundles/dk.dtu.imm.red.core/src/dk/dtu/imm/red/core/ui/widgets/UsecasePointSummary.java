package dk.dtu.imm.red.core.ui.widgets;

import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.project.editors.impl.UseCasePointsCompositeBackup;
import dk.dtu.imm.red.core.usecasepoint.UsecasePoint;

public class UsecasePointSummary extends Composite { 
	private Text txtRequirementFactor; 
	private Text txtManagementFactor;
	private Text txtTechnologyFactor;
	private Text txtProductivityFactor;
	private Text txtSum;
	private Text txtCost;
	private Text txtEffort;
	private Text txtManualCost;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public UsecasePointSummary(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Composite cmpCalculated = new Composite(this, SWT.NONE);
	 
		cmpCalculated.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		cmpCalculated.setLayout(new GridLayout(2, true));
		
		Group grpCalculatedFactors = new Group(cmpCalculated, SWT.NONE);
		grpCalculatedFactors.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		 
		grpCalculatedFactors.setLayout(new GridLayout(2, true));
		grpCalculatedFactors.setText("Calculated Factors");
		
		Label lblRequirementFactor = new Label(grpCalculatedFactors, SWT.NONE);
		lblRequirementFactor.setText("Requirement Factor (R)");
		
		Label lblManagementFactor = new Label(grpCalculatedFactors, SWT.NONE);
		lblManagementFactor.setText("Management Factor (M)");
		
		txtRequirementFactor = new Text(grpCalculatedFactors, SWT.BORDER | SWT.READ_ONLY);
		txtRequirementFactor.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		txtManagementFactor = new Text(grpCalculatedFactors, SWT.BORDER | SWT.READ_ONLY);
		txtManagementFactor.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		Label lblTechnologyFactor = new Label(grpCalculatedFactors, SWT.NONE);
		lblTechnologyFactor.setText("Technology Factor (T)");
		
		Label lblProductivityFactor = new Label(grpCalculatedFactors, SWT.NONE);
		lblProductivityFactor.setText("Productivity Factor (P)");
		
		txtTechnologyFactor = new Text(grpCalculatedFactors, SWT.BORDER | SWT.READ_ONLY);
		txtTechnologyFactor.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		txtProductivityFactor = new Text(grpCalculatedFactors, SWT.BORDER | SWT.READ_ONLY);
		txtProductivityFactor.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1)); 
		
		Group grpUseCasePoints = new Group(cmpCalculated, SWT.NONE);
		grpUseCasePoints.setText("Effort");
		grpUseCasePoints.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1)); 
		grpUseCasePoints.setLayout(new GridLayout(1, false));
		
		Composite cmpUCPandEffort = new Composite(grpUseCasePoints, SWT.NONE);
		cmpUCPandEffort.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		cmpUCPandEffort.setLayout(new GridLayout(2, true));
		
		Label lblCalculatedSum = new Label(cmpUCPandEffort, SWT.NONE);
	 
		lblCalculatedSum.setText("Use Case Points ( R×T×M)");
		
		Label lblEffort = new Label(cmpUCPandEffort, SWT.NONE);
	 
		lblEffort.setText("Effort (UCP×P)");
		
		txtSum = new Text(cmpUCPandEffort, SWT.BORDER | SWT.READ_ONLY);
		txtSum.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtSum.setSize(308, 21);
		
		txtEffort = new Text(cmpUCPandEffort, SWT.BORDER | SWT.READ_ONLY);
		txtEffort.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtEffort.setBounds(0, 0, 76, 21);
		
		Composite cmpCost = new Composite(grpUseCasePoints, SWT.NONE);
		cmpCost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		cmpCost.setLayout(new GridLayout(2, true));
		
		Label lblCost = new Label(cmpCost, SWT.NONE);
		lblCost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblCost.setText("Estimated Cost");
		
		Label lblManualCost = new Label(cmpCost, SWT.NONE);
		lblManualCost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
 
		lblManualCost.setText("Estimated Cost (Manual)");
		
		txtCost = new Text(cmpCost, SWT.BORDER | SWT.READ_ONLY);
		txtCost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		txtManualCost = new Text(cmpCost, SWT.BORDER | SWT.READ_ONLY);
		txtManualCost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		 
	}
	
	public void refreshValues(UsecasePoint up, Map<String, List<Element>> requirementElementMap) {  
		
		String precisionFormat = "%.2f";
		  
		txtManagementFactor.setText(String.format(precisionFormat, up.getManagementFactor().getSum()));
		txtTechnologyFactor.setText(String.format(precisionFormat, up.getTechnologyFactor().getSum()));
		txtProductivityFactor.setText(String.format(precisionFormat, up.getProductivityFactor().getSum()));  
		txtRequirementFactor.setText(String.format(precisionFormat, up.getRequirementFactor().getSum(requirementElementMap)));
		
		txtEffort.setText(String.format(precisionFormat, up.getSum(requirementElementMap)));  
		txtSum.setText(String.format(precisionFormat, up.getUsecasePoints()));
		txtCost.setText(String.format("%.2f %s", up.getTotalCost(), up.getCostPerUsecasePoint().getMonetaryUnit().name()));
		txtManualCost.setText(String.format("%.2f %s", up.getTotalCostManual(), up.getCostPerUsecasePoint().getMonetaryUnit().name()));
	} 

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	public Text getTxtRequirementFactor() {
		return txtRequirementFactor;
	}

	public void setTxtRequirementFactor(Text txtRequirementFactor) {
		this.txtRequirementFactor = txtRequirementFactor;
	}

	public Text getTxtManagementFactor() {
		return txtManagementFactor;
	}

	public void setTxtManagementFactor(Text txtManagementFactor) {
		this.txtManagementFactor = txtManagementFactor;
	}

	public Text getTxtTechnologyFactor() {
		return txtTechnologyFactor;
	}

	public void setTxtTechnologyFactor(Text txtTechnologyFactor) {
		this.txtTechnologyFactor = txtTechnologyFactor;
	}

	public Text getTxtProductivityFactor() {
		return txtProductivityFactor;
	}

	public void setTxtProductivityFactor(Text txtProductivityFactor) {
		this.txtProductivityFactor = txtProductivityFactor;
	}

	public Text getTxtSum() {
		return txtSum;
	}

	public void setTxtSum(Text txtSum) {
		this.txtSum = txtSum;
	}

	public Text getTxtCost() {
		return txtCost;
	}

	public void setTxtCost(Text txtCost) {
		this.txtCost = txtCost;
	}
}
