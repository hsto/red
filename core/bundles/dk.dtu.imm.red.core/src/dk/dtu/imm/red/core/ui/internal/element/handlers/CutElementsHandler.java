package dk.dtu.imm.red.core.ui.internal.element.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;

public class CutElementsHandler extends AbstractHandler implements IHandler {

	protected CopyElementsHandler copyHandler = new CopyElementsHandler();
	protected DeleteElementHandler deleteHandler = new DeleteElementHandler();
	
	@Override
	public Object execute(final ExecutionEvent event) 
			throws ExecutionException {
		
		copyHandler.execute(event);
		deleteHandler.execute(event);
		
		return null;
	}

}
