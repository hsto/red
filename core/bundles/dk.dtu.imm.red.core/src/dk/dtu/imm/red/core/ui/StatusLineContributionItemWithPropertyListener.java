package dk.dtu.imm.red.core.ui;

import org.eclipse.jface.action.StatusLineContributionItem;
import org.eclipse.jface.util.IPropertyChangeListener;

public class StatusLineContributionItemWithPropertyListener
		extends
			StatusLineContributionItem {

	public StatusLineContributionItemWithPropertyListener(String id) {
		super(id);
	}
	
	public void addPropertyChangeListener(IPropertyChangeListener listener) {
		
	}
	
	public void removePropertyChangeListener(IPropertyChangeListener listener) {
		
	}
	
}
