package dk.dtu.imm.red.core.ui.internal.element.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLParserPoolImpl;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.ElementPackageImpl;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.project.editors.impl.filesListData;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.core.workspace.impl.WorkspacePackageImpl;

public class OpenElementHandler extends AbstractHandler implements IHandler {
	public ResourceSet resourceSet;
	protected Project project;
	protected Folder folder;
	protected Element element;
	private String projectPath;

	public OpenElementHandler() {

	}

	public void setProjectPath(String projectPath) {
		this.projectPath = projectPath;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if ((WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects().size() > 0)) {

			MessageDialog md = new MessageDialog(Display.getCurrent().getActiveShell(), "Existing open project", null,
				"There is already an open project. Do you want to close it and open a new one?", MessageDialog.CONFIRM,
				new String[] { "OK", "Cancel" }, 1);

			int result = md.open();

			if (result == Dialog.OK) {
				CloseElementHandler closeHandler = new CloseElementHandler();
				closeHandler.execute(event);
			}
		}

		if (!(WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects().size() > 0)) {
			try {
				WorkspacePackageImpl.init();
				String filePath;
				if (projectPath == null) {
					DirectoryDialog dialog = new DirectoryDialog(HandlerUtil.getActiveShell(event), SWT.OPEN);

					filePath = dialog.open();
				} else {
					filePath = projectPath;
				}

				if (filePath == null)
					return null;

				filePath += java.io.File.separator;

				java.io.File fpath = new java.io.File(filePath);
				java.io.File[] allfilesindirec = fpath.listFiles();
				String fileName = "";

				for (int z = 0; z < allfilesindirec.length; z++) {
					if (allfilesindirec[z].getName().endsWith(".redproject")) {
						fileName = allfilesindirec[z].getName();
						break;
					}
				}

				filesListData.path = filePath;
				filesListData.ProjectNameAndPath = filePath + fileName;

				openprojectOrFile(filePath, fileName, event);

			} catch (Exception e) {
				return null;
			}
		}

		return null;
	}

	// ResourceSet resourceSet;

	private void openprojectOrFile(String directory, String fileName, ExecutionEvent event) {

		Map<EObject, Collection<EStructuralFeature.Setting>> brokenReferences = new HashMap<EObject, Collection<EStructuralFeature.Setting>>();
		try {
			String filePath = directory + fileName;
			ElementPackageImpl.init();
			ResourceSet resourceSet = new ResourceSetImpl();

			Resource resource = resourceSet.getResource(URI.createFileURI(filePath), true);
			if (resource.getContents().size() == 0)
				return;

			project = (Project) resource.getContents().get(0);
			WorkspaceFactory.eINSTANCE.getWorkspaceInstance().addOpenedProject(project);


			try {

				Map<Object, Object> loadOptions = new HashMap<Object, Object>();
				loadOptions.put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, Boolean.TRUE);

				resource.load(loadOptions);

			} catch (IOException e) {
			}
			ArrayList<Element> loadedElements = new ArrayList<Element>();
			Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();

			for (EObject content : resource.getContents()) {
				if (content instanceof Element) {
					if (!workspace.getContents().contains(content)) {
						loadedElements.add((Element) content);
					}
				}
			}

			workspace.getContents().addAll(loadedElements);

			TreeIterator<EObject> iterator = resource.getAllContents();

			// Now inform all listening plugins that elements have been opened
			while (iterator.hasNext()) {
				EObject object = iterator.next();
				if (!(object instanceof Element)) {
					continue;
				}
				final Element element = (Element) object;

				// Set the URI of the file, in case it has been moved from where
				// it was saved.
				element.setUri(filePath);
			}

			for (int i = 0; i < project.getListofopenedfilesinproject().size(); i++) {
				ElementPackageImpl.init();
				// getting all files in project
				File f = (File) project.getListofopenedfilesinproject().get(i);

				java.io.File localFile = new java.io.File(directory + f.getName());
				// check if file exists in the directory
				if (localFile.exists()) {
					// adding the file to the resource set
					Resource resourc = resourceSet.getResource(URI.createFileURI(directory + f.getName()), true);
					resourc.eAdapters().add(new EContentAdapter() {
						@Override
						public void notifyChanged(Notification notification) {
							// TODO Auto-generated method stub
							super.notifyChanged(notification);
						}
					});

					try {

						Map<Object, Object> loadOptions = new HashMap<Object, Object>();
						loadOptions.put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, Boolean.TRUE);

						resourc.load(loadOptions);
					} catch (IOException e) {
						// TODO Auto-generated catch block

					}

					f = (File) resourc.getContents().get(0);
					WorkspaceFactory.eINSTANCE.getWorkspaceInstance().addOpenedFile(f);

					TreeIterator<EObject> iterat = resourc.getAllContents();

					// Now inform all listening plugins that elements have been
					// opened
					while (iterat.hasNext()) {
						EObject object = iterat.next();

						if (!(object instanceof Element)) {
							continue;
						}
						final Element element = (Element) object;
						element.setUri(directory + f.getName());

						// getting all the relationships in elements
						for (int p = 0; p < element.getRelatesTo().size(); p++) {
							element.getRelatesTo().get(p).getToElement();
							element.getRelatesTo();
						}
						for (int p = 0; p < element.getRelatedBy().size(); p++) {
							element.getRelatedBy().get(p).getFromElement();
							element.getRelatedBy();
						}
					}

//					Map<EObject, Collection<EStructuralFeature.Setting>> unresolvedProxies = EcoreUtil.UnresolvedProxyCrossReferencer
//							.find(resource);
//					brokenReferences.putAll(unresolvedProxies);
//
//					System.out.println("OPENING " + f);
//					for(EObject ref : brokenReferences.keySet()){
//						System.out.println(ref); //dk.dtu.imm.red.core.file.impl.FileImpl@134e772 (eProxyURI: file:/C:/Users/HenryLie/Desktop/RED_Sample/LMS_REDV3_20150419%20With%20Conflict/WeaveTempFile.red#9ae92c23-9938-460d-a054-4bc2a8dbc4f2)
//						for(EStructuralFeature.Setting setting : brokenReferences.get(ref)){
//							EObject objSetting = setting.getEObject();	//project
//							EStructuralFeature objFeature = setting.getEStructuralFeature(); //listOfOpenedfilesinproject
//							System.out.println(objSetting);
//							System.out.println(objFeature);
//						}
//					}

				} else {
					continue;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}


//		try{
			//clean up all brokenReferences
//			for(EObject ref : brokenReferences.keySet()){
//				System.out.println(ref); //dk.dtu.imm.red.core.file.impl.FileImpl@134e772 (eProxyURI: file:/C:/Users/HenryLie/Desktop/RED_Sample/LMS_REDV3_20150419%20With%20Conflict/WeaveTempFile.red#9ae92c23-9938-460d-a054-4bc2a8dbc4f2)
//				for(EStructuralFeature.Setting setting : brokenReferences.get(ref)){
//					EObject objSetting = setting.getEObject();	//project
//					EStructuralFeature objFeature = setting.getEStructuralFeature(); //listOfOpenedfilesinproject
//
//					EcoreUtil.delete(ref,true);
////					objFeature.eUnset((EReference)ref);
////					setting.unset();
////					setting.unset();
//					System.out.println(setting);
//				}
//			}
//			WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedProjects().get(0).save();
//			for (File file : WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedFiles()) {
//				file.save();
//			}
//		}
//		catch(Exception ex){
//			ex.printStackTrace();
//		}

	}

	public ArrayList<String> fileNamesFunc(String path) {
		ArrayList<String> returnValue = new ArrayList();
		try {
			java.io.File XMLfile = new java.io.File(path);
			DocumentBuilderFactory db = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuild;
			dbuild = db.newDocumentBuilder();
			Document doc = dbuild.parse(XMLfile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("Listofopenedfilesinproject");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
					returnValue.add(eElement.getAttribute("href").replace("#/", "").replace("%20", " "));
				}
			}
		} catch (Exception e) {
			return null;
		}

		filesListData.fileList.clear();
		filesListData.fileList = returnValue;
		return returnValue;
	}

}
