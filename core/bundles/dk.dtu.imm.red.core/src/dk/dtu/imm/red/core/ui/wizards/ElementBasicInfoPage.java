package dk.dtu.imm.red.core.ui.wizards;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;


public class ElementBasicInfoPage extends WizardPage implements IWizardPage {

	ElementBasicInfoPageComposite baseComposite;
	public ElementBasicInfoPage(String elementTypeToCreate) {
		super("Define " + elementTypeToCreate);
		setTitle("Create New " + elementTypeToCreate);
		setDescription("Use this wizard to create a new " + elementTypeToCreate);
	}

	@Override
	public void createControl(Composite parent) {

		baseComposite = new ElementBasicInfoPageComposite(parent,SWT.NULL);
		baseComposite.getLabelTextField().addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if (baseComposite.getTxtLabel().trim().isEmpty()
					&&	baseComposite.getTxtName().trim().isEmpty()
					) {
					setPageComplete(false);

				} else {
					setPageComplete(true);
				}
			}
		});
		baseComposite.getNameTextField().addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if (baseComposite.getTxtLabel().trim().isEmpty()
					&&	baseComposite.getTxtName().trim().isEmpty()
					) {
					setPageComplete(false);

				} else {
					setPageComplete(true);
				}
			}
		});
		setControl(baseComposite);
	}
	@Override
	public IWizardPage getNextPage() {
		return super.getNextPage();
	}


	public String getLabel(){
		return baseComposite.getTxtLabel();
	}

	public String getName(){
		return baseComposite.getTxtName();
	}

	public String getDescription(){
		return baseComposite.getTxtDescription();
	}

	public void replaceLblLabel(String newLabelLbl){
		baseComposite.getLblLabel().setText(newLabelLbl);
	}

	public void replaceLblName(String newNameLbl){
		baseComposite.getLblName().setText(newNameLbl);
	}

	public void replaceLblDescrption(String newDescriptionLbl){
		baseComposite.getLblDescription().setText(newDescriptionLbl);
	}

	@Override
	public boolean isPageComplete() {
		if(!getLabel().isEmpty() && !getName().isEmpty()){
			return true;
		}
		return false;
	}
}
