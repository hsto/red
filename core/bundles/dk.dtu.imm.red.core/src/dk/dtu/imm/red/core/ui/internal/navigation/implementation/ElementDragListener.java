package dk.dtu.imm.red.core.ui.internal.navigation.implementation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.TextTransfer;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.folder.Folder;
import dk.dtu.imm.red.core.folder.FolderFactory;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.LogUtil;

public class ElementDragListener implements DragSourceListener {

	protected TreeViewer viewer;
	protected Group[] oldParents;
	protected Element[] selectedElements;

	public ElementDragListener(TreeViewer viewer) {
		this.viewer = viewer;
	}
	
	protected boolean canDragElement(Element draggedElement) {
		return !(draggedElement instanceof File);
	}
	 protected boolean canDragProject(Element draggedElement) {
			
			return !(draggedElement instanceof Project);
		}
	protected Resource createDraggableResource(Element[] elements) {
		Resource resource = new XMLResourceImpl();
		
		Folder pseudoFolder = FolderFactory.eINSTANCE.createFolder();
		for (Element element : elements) {
			if (element != null) {
				pseudoFolder.getContents()
					.add((Element) EcoreUtil.copy(element));
			}
		}
		
		if (pseudoFolder.getContents().size() > 0) {
			resource.getContents().add(pseudoFolder);
			return resource;
		} else {
			return null;
		}
	}

	@Override
	public void dragStart(final DragSourceEvent event) {
		IStructuredSelection selection = 
				(IStructuredSelection) viewer.getSelection();
		Object[] selectedObjects = selection.toArray();
		selectedElements = new Element[selectedObjects.length];
		oldParents = new Group[selectedObjects.length];

		for (int i = 0; i < selectedObjects.length; i++) {
			// we can only drag/drop EObjects (domain objects)
			if (!(selectedObjects[i] instanceof Element) 
					|| !canDragElement((Element) selectedObjects[i]) || !canDragProject((Element) selectedObjects[i])) {
				continue;
			}
			selectedElements[i] = (Element) selectedObjects[i];
			oldParents[i] = selectedElements[i].getParent();
		}
	}
	

	@Override
	public void dragSetData(final DragSourceEvent event) {
	
		Resource resource = createDraggableResource(selectedElements);
		if (resource == null) {
			event.doit = false;
			return;
		}
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			resource.save(outputStream, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

		// Support for transfer of actual Element objects as a ISelection
		if(LocalSelectionTransfer.getTransfer().isSupportedType(event.dataType))
		{
			LocalSelectionTransfer.getTransfer().setSelection(viewer.getSelection());
		}
		
		// Support for transfer through XML serialization 
		if (TextTransfer.getInstance().isSupportedType(event.dataType)) {
			event.data = outputStream.toString();
		}
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
	
	}

}
