package dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.CellEditor;
import org.agilemore.agilegrid.DefaultCellEditorProvider;
import org.agilemore.agilegrid.editors.ComboBoxCellEditor;
import org.eclipse.swt.SWT;

import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.ui.AgileTableTextCellEditor;

public class EAGCellEditorProvider extends DefaultCellEditorProvider {  

	public EAGCellEditorProvider(AgileGrid agileGrid, EAGPresenter<?> presenter) {
		super(agileGrid); 
	 
	}
	
	@Override
	@SuppressWarnings({"rawtypes","unchecked"})
	public CellEditor getCellEditor(int row, int col) {
		EAGPresenter presenter = ((ExtendedAgileGrid)this.agileGrid).getPresenter();
		ElementColumnDefinition defCol = presenter.getColumnDefinitions()[col];
		
		Object subject = presenter.getDataSource().get(row);
		
		if(presenter.handleCellEdit(row, col) == true) return null;
		
		if(defCol.getColumnBehaviour() == ColumnBehaviour.ReferenceSelector) {
			presenter.openItem(((ExtendedAgileGrid)this.agileGrid).getSelectedIndex(), col);
			return null;
		} 
		else if(defCol.getCellItems().length > 0) { 
			return new ComboBoxCellEditor(agileGrid, defCol.getCellItems(), SWT.NONE);
		}
		else if(defCol.getCellHandler() != null && defCol.getCellHandler().getComboValues(subject) != null) { 
			return new ComboBoxCellEditor(agileGrid, defCol.getCellHandler().getComboValues(subject), SWT.NONE);
		}
		else {
			return new AgileTableTextCellEditor(agileGrid);
		} 
	} 
	
	@Override
	@SuppressWarnings("rawtypes")
	public boolean canEdit(final int row, final int col) {  
		EAGPresenter presenter = ((ExtendedAgileGrid)this.agileGrid).getPresenter();
		return presenter.getColumnDefinitions()[col].isEditable();
	} 
	
	

}
