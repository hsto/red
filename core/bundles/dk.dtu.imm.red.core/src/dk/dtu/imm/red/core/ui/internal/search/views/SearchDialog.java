package dk.dtu.imm.red.core.ui.internal.search.views;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class SearchDialog extends Dialog {
	
	protected Label searchStringLabel;
	protected Text searchStringTextBox;
	protected String searchString;

	public SearchDialog(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(2, true);
		composite.setLayout(layout);
		
		searchStringLabel = new Label(composite, SWT.NONE);
		searchStringLabel.setText("Search string:");
		
		searchStringTextBox = new Text(composite, SWT.BORDER);
		
		return composite;
	}
	
	@Override
	protected void okPressed() {
		searchString = searchStringTextBox.getText();
		super.okPressed();
	}
	
	public String getSearchString() {
		return searchString;
	}
	
}
