package dk.dtu.imm.red.core.ui.internal.preferences;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.jface.preference.IPreferenceNode;
import org.eclipse.jface.preference.IPreferencePage;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class ManualResizePreferenceDialog extends PreferenceDialog {

	//This class is created to override the showPage method to prevent it from resizing
	//when showing the project preference page
	
	public ManualResizePreferenceDialog(Shell parentShell,
			PreferenceManager manager) {
		super(parentShell, manager);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean showPage(IPreferenceNode node) {
		if (node == null) {
			return false;
		}
		// Create the page if nessessary
		if (node.getPage() == null) {
			createPage(node);
		}
		if (node.getPage() == null) {
			return false;
		}
		IPreferencePage newPage = getPage(node);
		if (newPage == getCurrentPage()) {
			return true;
		}
		if (getCurrentPage() != null) {
			if (!getCurrentPage().okToLeave()) {
				return false;
			}
		}
		IPreferencePage oldPage = getCurrentPage();
		setCurrentPage(newPage);
		// Set the new page's container
		getCurrentPage().setContainer(this);
		// Ensure that the page control has been created
		// (this allows lazy page control creation)
		if (getCurrentPage().getControl() == null) {
			final boolean[] failed = { false };
			SafeRunnable.run(new ISafeRunnable() {
				public void handleException(Throwable e) {
					failed[0] = true;
				}

				public void run() {
					createPageControl(getCurrentPage(), getPageContainer());
				}
			});
			if (failed[0]) {
				return false;
			}
			// the page is responsible for ensuring the created control is
			// accessable
			// via getControl.
			Assert.isNotNull(getCurrentPage().getControl());
		}
		// Force calculation of the page's description label because
		// label can be wrapped.
		final Point[] size = new Point[1];
		final Point failed = new Point(-1, -1);
		SafeRunnable.run(new ISafeRunnable() {
			public void handleException(Throwable e) {
				size[0] = failed;
			}

			public void run() {
				size[0] = getCurrentPage().computeSize();
			}
		});
		if (size[0].equals(failed)) {
			return false;
		}
		Point contentSize = size[0];
		// Do we need resizing. Computation not needed if the
		// first page is inserted since computing the dialog's
		// size is done by calling dialog.open().
		// Also prevent auto resize if the user has manually resized
		Shell shell = getShell();
		Point shellSize = shell.getSize();
		if (oldPage != null) {
			Rectangle rect = getPageContainer().getClientArea();
			Point containerSize = new Point(rect.width, rect.height);
//			Commented the calculation that do the resizing			
//			int hdiff = contentSize.x - containerSize.x;
//			int vdiff = contentSize.y - containerSize.y;
//			if ((hdiff > 0 || vdiff > 0) && shellSize.equals(lastShellSize)) {
//					hdiff = Math.max(0, hdiff);
//					vdiff = Math.max(0, vdiff);
//					setShellSize(shellSize.x + hdiff, shellSize.y + vdiff);
//					lastShellSize = shell.getSize();
//					if (getCurrentPage().getControl().getSize().x == 0) {
//						getCurrentPage().getControl().setSize(containerSize);
//					}
//				
//			} else {
				getCurrentPage().getControl().setSize(containerSize);
//			}
		}
		
//		scrolled.setMinSize(contentSize);
		
		// Ensure that all other pages are invisible
		// (including ones that triggered an exception during
		// their creation).
		Control[] children = getPageContainer().getChildren();
		Control currentControl = getCurrentPage().getControl();
		for (int i = 0; i < children.length; i++) {
			if (children[i] != currentControl) {
				children[i].setVisible(false);
			}
		}
		// Make the new page visible
		getCurrentPage().setVisible(true);
		if (oldPage != null) {
			oldPage.setVisible(false);
		}
		// update the dialog controls
		update();
		return true;
	}

	
}
