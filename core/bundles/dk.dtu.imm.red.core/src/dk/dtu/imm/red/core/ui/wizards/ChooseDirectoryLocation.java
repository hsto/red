package dk.dtu.imm.red.core.ui.wizards;

import java.io.File;
import java.nio.file.Files;

import org.eclipse.core.runtime.Path;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceConstants;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;


public class ChooseDirectoryLocation extends WizardPage
		implements IWizardPage {

	protected Label locationLabel;
	protected Text locationText;
	protected Button browseButton;
	protected Text titleTextBox;

	protected Label validationLabel;

	protected String initialDirectoryName;


	public ChooseDirectoryLocation() {
		super("Choose Directory Location");
		setTitle("Choose a  location for the new project");
		setDescription("Using the Browse-button, select a location"
				+ " on your computer where this project"
				+ " should be stored.");

	}
	public void setInitialDirectoryName(String directoryName){
		this.initialDirectoryName = directoryName;
	}



	//@Override
	public void createControl(final Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(3, false);
		container.setLayout(layout);

		locationLabel = new Label(container, SWT.NONE);
		locationLabel.setText("Directory:");

		GridData locationTextLayout = new GridData();
		locationTextLayout.grabExcessHorizontalSpace = true;
		locationTextLayout.horizontalAlignment = SWT.FILL;
		locationTextLayout.minimumWidth = 150;
		locationText  = new Text(container, SWT.BORDER);
		locationText.setLayoutData(locationTextLayout);
		locationText.addListener(SWT.Modify, new Listener() {
			public void handleEvent(Event event) {
				if (locationText.getText() != null
						&& !locationText.getText().trim().isEmpty())
				{
					boolean validDirectoryPath = false;

					File file = new File(locationText.getText());

					if ((file.isDirectory())&&(file.listFiles().length == 0)) {


						validDirectoryPath = true;

					}
					if (validDirectoryPath)
					{
						setPageComplete(true);
						validationLabel.setVisible(false);
					} else
					{
						setPageComplete(false);
						validationLabel.setVisible(true);
					}
				}
			}
		});

		GridData validationLabelLayout = new GridData();
		validationLabelLayout.horizontalSpan = 3;
		validationLabel = new Label(container, SWT.NONE);
		validationLabel.setText("Path is invalid, please choose new destination!");
		validationLabel.setLayoutData(validationLabelLayout);
		validationLabel.setVisible(false);


		browseButton = new Button(container, SWT.NONE);
		browseButton.setText("Browse");
		browseButton.addListener(SWT.Selection, new Listener() {

			public void handleEvent(final Event event) {
				//DirectoryDialog dlg = new DirectoryDialog(shell);
				DirectoryDialog dlg = new DirectoryDialog(ChooseDirectoryLocation.this.getShell(), SWT.SAVE);
				// Set the initial filter path according
				// to anything they've selected or typed in
//				dlg.setFilterPath(locationText.getText());

				String workspacePath = PreferenceUtil.getPreferenceString(PreferenceConstants.PROJECT_PREF_WORKSPACE_PATH);
				if(workspacePath!=null && !workspacePath.isEmpty()){
					File f = new File(workspacePath);
					if(f.exists()){
						dlg.setFilterPath(workspacePath);
					}
				}

				// Title bar text
				dlg.setText("Select project location");

				//  message displayed in the dialog
				dlg.setMessage("Select a directory for the project");

				// Calling open() will open and run the dialog.
				// It will return the selected directory, or
				// null if user cancels
				String dir = dlg.open();
				if (dir != null) {
					// Set the text box to the new selection
					locationText.setText(dir);
					locationText.notifyListeners(SWT.Modify, null);
				}
			}
		});




		this.setPageComplete(false);
		this.setControl(container);



	}

	public String getDirectoryName() {
		if (isPageComplete()) {
			return locationText.getText();
		} else {
			return null;
		}
	}

}

























