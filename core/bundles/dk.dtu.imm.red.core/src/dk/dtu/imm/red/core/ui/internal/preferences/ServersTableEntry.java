package dk.dtu.imm.red.core.ui.internal.preferences;

public class ServersTableEntry {
	private String name;
	private String service;
	private String server;
	private String username;
	private String password;
	private String extraParams;
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getExtraParams() {
		return extraParams;
	}
	public void setExtraParams(String extraParams) {
		this.extraParams = extraParams;
	}
	
}
