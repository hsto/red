package dk.dtu.imm.red.core.ui.internal.file.wizards.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.INewWizard;

import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.file.wizards.CreateFileWizard;
import dk.dtu.imm.red.core.ui.internal.project.operations.CreateProjectOperation;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.workspace.Workspace;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
//import com.sun.org.apache.xml.internal.resolver.helpers.FileURL;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.element.util.FileHelper;

public class CreateFileWizardImpl extends BaseNewWizard implements
	CreateFileWizard, INewWizard {
	protected CreateProjectOperation projectOperation;

	public CreateFileWizardImpl() {
		super("file", File.class, true);

		presenter = new CreateFileWizardPresenterImpl();
	}

	@Override
	public void addPages() {
		// do not need element basic info page, instead only have the
		// inputfilename page.
		// super.addPages();
		addPage(inputFileNamePage);
	}

	@Override
	public boolean performFinish() {

		// no location selected, use the path for a new filename
		String name = inputFileNamePage.getDisplayName();
		Workspace workspace = WorkspaceFactory.eINSTANCE.getWorkspaceInstance();
		if (workspace.getCurrentlyOpenedProjects().size() == 0) {
			// The user should have been prompted about needing to have a
			// project open before this.
			return false;
		}
		Project currentProject = workspace.getCurrentlyOpenedProjects().get(0);
		String directoryPath = FileHelper.getDirectoryPath(currentProject);
		String filePath = FileHelper.join(directoryPath, name + ".red");

		((CreateFileWizardPresenterImpl) presenter).wizardFinished(name,
			currentProject, filePath);

		return true;
	}

	@Override
	public boolean canFinish() {
		return inputFileNamePage.isPageComplete();

	}

}
