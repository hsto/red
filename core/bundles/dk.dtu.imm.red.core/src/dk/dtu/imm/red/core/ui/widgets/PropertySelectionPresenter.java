package dk.dtu.imm.red.core.ui.widgets;

import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.emf.common.util.Enumerator;

import dk.dtu.imm.red.core.properties.DurationUnit;
import dk.dtu.imm.red.core.properties.Kind;
import dk.dtu.imm.red.core.properties.MeasurementKind;
import dk.dtu.imm.red.core.properties.MonetaryUnit;
import dk.dtu.imm.red.core.properties.Property;
import dk.dtu.imm.red.core.properties.ValueKind;
import dk.dtu.imm.red.core.ui.presenters.IPresenter;

public class PropertySelectionPresenter implements IPresenter { 
	
	private Property property;
	private Kind kind;
	

	public PropertySelectionPresenter(Property property) {
		this.property = property;
	}
	
	@Override
	public void setUndoContext(IUndoContext newContext) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IUndoContext getUndoContext() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public boolean updateMonetaryUnit(Enumerator newValue ) {
		Enumerator oldVal = property.getMonetaryUnit();
		property.setMonetaryUnit(MonetaryUnit.valueOf(newValue.getName().toUpperCase()));
		return !oldVal.equals(property.getMonetaryUnit());
	}
	
	public boolean updateDurationUnit(Enumerator newUnit) {
		Enumerator oldVal = property.getDurationUnit();
		property.setDurationUnit(DurationUnit.valueOf(newUnit.getName().toUpperCase()));
		return !oldVal.equals(property.getDurationUnit());
	}
	
	public boolean updateValue(double newValue) {
		double oldVal = property.getValue();
		property.setValue(newValue);
		return oldVal != property.getValue();
	}
	
	public boolean updateMeasurement(MeasurementKind newUnit) {
		MeasurementKind oldVal = property.getMeasurementKind();
		property.setMeasurementKind(newUnit); 
		return oldVal != property.getMeasurementKind();
	}  
	
	public boolean updateValue(ValueKind newUnit) {
		ValueKind oldVal = property.getValueKind(); 
		property.setValueKind(newUnit); 
		return oldVal != property.getValueKind();
	}  
}
