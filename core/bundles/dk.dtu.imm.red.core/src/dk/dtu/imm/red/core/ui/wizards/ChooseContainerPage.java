package dk.dtu.imm.red.core.ui.wizards;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;

public class ChooseContainerPage extends WizardPage
implements IWizardPage{

	protected Label selectContainerLabel;
	protected ElementTreeViewWidget treeView;
	protected ISelection selection;

	protected boolean folderChosen;

	public ChooseContainerPage(String actionName) {
		super(actionName);
		setTitle("Choose a container");
		setDescription("Select the container" 
				+ " from where the "+ actionName
				+ " should be generated from.\n" 
				+ "All elements within the folder will be included for the action");
		folderChosen = false;
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		GridData descriptionLayoutData = new GridData();
		descriptionLayoutData.horizontalSpan = 2;


		selectContainerLabel = new Label(composite, SWT.NONE);
		selectContainerLabel.setLayoutData(descriptionLayoutData);
		selectContainerLabel
		.setText("Select a folder");

		GridData treeViewerLayoutData = new GridData(SWT.FILL, SWT.FILL, true,
				true);
		treeViewerLayoutData.horizontalSpan = 2;

		Composite treeContainer = new Composite(composite, SWT.NONE);

		GridLayout treeContainerLayout = new GridLayout();
		treeContainerLayout.numColumns = 1;

		treeContainer.setLayout(treeContainerLayout);
		treeContainer.setLayoutData(treeViewerLayoutData);

		setPageComplete(false);
		
		selection = 
				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();

		treeView = new ElementTreeViewWidget(true, treeContainer, SWT.BORDER, true,true);
		if (selection != null && !selection.isEmpty()) {
			treeView.setSelection(selection);
			setPageComplete(true);
		}

		treeView.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				if(treeView.getSelection().isEmpty()){
					setPageComplete(false);
				}else{
					setPageComplete(true);
				}
				getContainer().updateButtons();
			}
		});

		setControl(composite);

	}

	@Override
	public IWizardPage getNextPage() {
		if (isPageComplete() && isCurrentPage() 
				&& getWizard() instanceof IBaseWizard) {

			((IBaseWizard) getWizard()).pageOpenedOrClosed(this);
		}
		return super.getNextPage();
	}

	public ISelection getSelection() {
		return treeView.getSelection();
	}


}
