package dk.dtu.imm.red.core.ui.editors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.Cell;
import org.agilemore.agilegrid.CellDoubleClickEvent;
import org.agilemore.agilegrid.DefaultCellEditorProvider;
import org.agilemore.agilegrid.ICellDoubleClickListener;
import org.agilemore.agilegrid.ISelectionChangedListener;
import org.agilemore.agilegrid.SWTX;
import org.agilemore.agilegrid.SelectionChangedEvent;
import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.databinding.EMFDataBindingContext;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.epf.richtext.RichTextCommand;
import org.eclipse.epf.richtext.RichTextCommandEvent;
import org.eclipse.epf.richtext.RichTextCommandListener;
import org.eclipse.epf.richtext.RichTextEditor;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.StatusLineContributionItem;
import org.eclipse.jface.databinding.util.JFaceProperties;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.operations.RedoActionHandler;
import org.eclipse.ui.operations.UndoActionHandler;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.part.MultiPageSelectionProvider;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.ElementPackage;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.element.ManagementDecision;
import dk.dtu.imm.red.core.element.Priority;
import dk.dtu.imm.red.core.element.QAAssessment;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.element.relationship.ElementReference;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.element.relationship.RelationshipFactory;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.file.File;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.properties.DurationUnit;
import dk.dtu.imm.red.core.properties.Kind;
import dk.dtu.imm.red.core.properties.MonetaryUnit;
import dk.dtu.imm.red.core.properties.Property;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.SWTUtils;
import dk.dtu.imm.red.core.ui.StatusLineContributionItemWithPropertyListener;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizard;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardPresenter;
import dk.dtu.imm.red.core.ui.element.wizards.impl.CreateElementReferenceWizardImpl;
import dk.dtu.imm.red.core.ui.internal.Activator;
import dk.dtu.imm.red.core.ui.internal.dependency.DependencyTableCellEditorProvider;
import dk.dtu.imm.red.core.ui.internal.dependency.DependencyTableCellRendererProvider;
import dk.dtu.imm.red.core.ui.internal.dependency.DependencyTableContentProvider;
import dk.dtu.imm.red.core.ui.internal.dependency.DependencyTableLayoutAdvisor;
import dk.dtu.imm.red.core.ui.internal.editors.changelog.ChangelogTableContentProvider;
import dk.dtu.imm.red.core.ui.internal.editors.changelog.ChangelogTableLayoutAdviser;
import dk.dtu.imm.red.core.ui.internal.widgets.DateSelectionDialog;
import dk.dtu.imm.red.core.ui.internal.widgets.ElementLifeCycleDialog;
import dk.dtu.imm.red.core.ui.internal.widgets.ElementLifeCycleDialog.Trigger;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.core.user.User;
import dk.dtu.imm.red.core.user.UserFactory;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;
import dk.dtu.imm.red.core.element.QuantityItem;

/**
 * This is a basic multi-page editor in the RED project, containing a Management
 * & Tracing-page, which contains useful information about the element being
 * editor. This editor is meant to be subclassed by other editors which require
 * this Management & Tracing-page, as well as other common functionality.
 *
 * @author Jakob Kragelund
 *
 */
public abstract class BaseEditor extends MultiPageEditorPart implements IEditorPart, IBaseEditor {

	// status bar item ids
	public static final String AUTHOR = "dk.dtu.imm.red.core.statusbar.author";
	public static final String RESPONSIBLE = "dk.dtu.imm.red.core.statusbar.responsible";
	public static final String ELEMENT_STATUS = "dk.dtu.imm.red.core.statusbar.elementstatus";
	public static final String LAST_MODIFIED = "dk.dtu.imm.red.core.statusbar.lastmodified";
	public static final String CREATED = "dk.dtu.imm.red.core.statusbar.created";
	public static final String PRIORITY = "dk.dtu.imm.red.core.statusbar.priority";

	private static final String RED_MASTER_PASSWORD = "02264_dlaraH!";

	public class EditorModifyListener implements Listener {
		@Override
		public void handleEvent(final Event event) {
			isDirty = true;
			firePropertyChange(PROP_DIRTY);
		}
	}

	private static final int FIELD_LENGTH = 100;

	protected Button addCommentButton;

	protected Label author;

	protected Text authorText;

	protected CommentList changelog;

	protected AgileGrid changelogTable;

	protected DefaultCellEditorProvider changelogTableCellEditorProvider;

	protected ChangelogTableContentProvider changelogTableContentProvider;

	protected ChangelogTableLayoutAdviser changelogTableLayoutAdviser;

	protected MultiPageSelectionProvider selectionProvider;

	public RichTextCommandListener commandListener = new RichTextCommandListener() {
		@Override
		public void commandExecuted(final RichTextCommandEvent event) {
			if (event.commandType == RichTextCommandEvent.LINK_COMMAND) {
				if (event.data instanceof String && element != null) {
					String targetUniqueID = (String) event.data;

					Element targetElement = WorkspaceFactory.eINSTANCE.getWorkspaceInstance()
							.findDescendantByUniqueID(targetUniqueID);

					if (targetElement != null) {
						for (Dependency dependency : dependencies) {
							// If the persona already has a relationship
							// to this element, do nothing.
							if (dependency.targetElement.getUniqueID().equals(targetElement.getUniqueID())) {
								return;
							}
						}
						Dependency dependency = new Dependency(RelationshipKind.REFERS_TO, targetElement.getIcon(),
								targetElement, "");
						dependencies.add(dependency);
						firePartPropertyChanged("dependencies", null, null);
						markAsDirty();
						if (dependencyTable != null) {
							dependencyTableLayoutAdvisor
									.setRowCount(dependencies.size() + dependenciesRelatedBy.size());
							dependencyTable.redraw();
						}
					}
				}
			}
		}
	};

	protected Label creationDateLabel;

	protected Text creationDateText;

	protected Label deadlineLabel;

	protected Text deadlineText;

	protected Button deleteCommentButton;

	/**
	 * The element edited by this editor.
	 */
	protected Element element;

	/**
	 * The help button for persona.
	 */
	protected Button helpButton;

	protected boolean isDirty;

	protected Label lastSaveDateLabel;

	protected Text lastSaveDateText;

	protected SimpleDateFormat longSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	protected EditorModifyListener modifyListener;

	public EditorModifyListener getModifyListener() {
		return modifyListener;
	}

	/**
	 * A potential parent editor, if this editor is used as a tab in another
	 * editor.
	 */
	protected BaseEditor parentEditor;

	/**
	 * Base editor presenter.
	 */
	protected IEditorPresenter presenter;

	protected Combo priorityCombo;

	protected Label priorityLabel;

	/**
	 * Handler for the redo-action.
	 */
	protected RedoActionHandler redoHandler;

	protected Label responsibleLabel;

	protected Text responsibleText;

	private SimpleDateFormat shortSDF = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Handler for the undo-action.
	 */
	protected UndoActionHandler undoHandler;

	protected Label versionLabel;

	protected Text versionText;

	protected Label workPackageLabel;

	protected Text workPackageText;

	private DataBindingContext context;

	private Label lockLabel;

	private Scale lockScale;

	private Text lockStatus;

	/**
	 * #issue 231 @Luai
	 */
	protected Label stateLabel;
	protected Text stateField;
	protected Label lifeCyclePhaseLabel;
	protected Text lifeCyclePhaseField;
	protected Label managementDecisionLabel;
	protected Combo managementDecisionCombo;
	protected Label qaAssessmentLabel;
	protected Combo qaAssessmentCombo;
	protected Button advanceStateButton;
	protected Button advanceLifeCyclePhaseButton;

	protected RichTextEditor discussionEditor;
	protected List<Dependency> dependencies;
	protected List<Dependency> dependenciesRelatedBy;
	protected AgileGrid dependencyTable;
	protected DependencyTableLayoutAdvisor dependencyTableLayoutAdvisor;
	protected DependencyTableContentProvider dependencyTableContentProvider;
	protected DependencyTableCellEditorProvider dependencyTableCellEditorProvider;
	protected DependencyTableCellRendererProvider dependencyTableRendererProvider;
	protected Button addDependencyButton;
	protected Button openDependencyButton;
	protected Button deleteDependencyButton;
	/**
	 * Column property names for the dependency table.
	 */
	private final String ICON_COLUMN_NAME = "";
	private final String DEPENDENCY_KIND = "Kind";
	private final String ELEMENT_NAME_COLUMN_NAME = "Element ref";
	private final String COMMENT_COLUMN_NAME = "Addtion";
	private final String[] columnNames = new String[] { ICON_COLUMN_NAME, DEPENDENCY_KIND, ELEMENT_NAME_COLUMN_NAME,
			COMMENT_COLUMN_NAME };

	protected Spinner spinnerCost;
	protected Spinner spinnerBenefit;
	protected Spinner spinnerRisk;
	protected Combo cmbUnitCost;
	protected Combo cmbUnitBenefit;
	protected EAGComposite<QuantityItem> gdQuantities;

	public BaseEditor() {
		this(null);
	}

	public BaseEditor(BaseEditor parent) {
		parentEditor = parent;
		selectionProvider = new MultiPageSelectionProvider(this);
	}

	private Composite createChangelogContainer(final Composite parent) {
		Group tcContainer = new Group(parent, SWT.NONE);
		tcContainer.setText("Changelog");

		changelog = EcoreUtil.copy(element.getChangeList());
		if (changelog == null) {
			changelog = CommentFactory.eINSTANCE.createCommentList();
		}

		GridLayout tcLayout = new GridLayout(3, false);
		tcContainer.setLayout(tcLayout);

		GridData tableData = new GridData();
		tableData.grabExcessHorizontalSpace = true;
		tableData.grabExcessVerticalSpace = true;
		tableData.verticalAlignment = SWT.FILL;
		tableData.horizontalAlignment = SWT.FILL;
		tableData.horizontalSpan = 3;

		changelogTable = new AgileGrid(tcContainer, SWT.MULTI | SWTX.ROW_SELECTION | SWT.V_SCROLL);
		changelogTable.setLayoutData(tableData);

		changelogTableLayoutAdviser = new ChangelogTableLayoutAdviser(changelogTable);
		changelogTableContentProvider = new ChangelogTableContentProvider(this);
		changelogTableCellEditorProvider = new DefaultCellEditorProvider(changelogTable);

		changelogTableContentProvider.setContent(changelog);

		changelogTable.setLayoutAdvisor(changelogTableLayoutAdviser);
		changelogTable.setContentProvider(changelogTableContentProvider);
		changelogTable.setCellEditorProvider(changelogTableCellEditorProvider);

		changelogTableLayoutAdviser.setRowCount(changelog.getComments().size());
		changelogTable.redraw();

		changelogTable.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(final SelectionChangedEvent arg0) {
				if (changelogTable.getCellSelection().length > 0) {
					deleteCommentButton.setEnabled(true);
				} else {
					deleteCommentButton.setEnabled(false);
				}
			}
		});

		addCommentButton = new Button(tcContainer, SWT.NONE);
		addCommentButton.setText("Add");
		addCommentButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				addChangelogComment(null);
			}
		});

		deleteCommentButton = new Button(tcContainer, SWT.NONE);
		deleteCommentButton.setText("Delete");
		deleteCommentButton.setEnabled(false);
		deleteCommentButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				Cell[] selectedCells = changelogTable.getCellSelection();

				for (Cell cell : selectedCells) {
					changelog.getComments().remove(cell.row);
				}

				markAsDirty();
				changelogTableLayoutAdviser.setRowCount(changelog.getComments().size());
				changelogTable.clearSelection();
				deleteCommentButton.setEnabled(false);
				changelogTable.redraw();
			}

		});
		return tcContainer;
	}

	private void addChangelogComment(String text) {
		Comment comment = CommentFactory.eINSTANCE.createComment();
		comment.setVersion(versionText.getText());
		User user = UserFactory.eINSTANCE.createUser();
		user.setName(authorText.getText());
		comment.setAuthor(user);
		comment.setTimeCreated(new Date());
		if (text != null) {
			dk.dtu.imm.red.core.text.Text textValue = TextFactory.eINSTANCE.createText(text);
			comment.setText(textValue);
		}

		changelog.getComments().add(comment);

		markAsDirty();
		changelogTableLayoutAdviser.setRowCount(changelog.getComments().size());
		changelogTable.redraw();
	}

	/**
	 * Creates a container for the help button, which opens help for this type
	 * of element (persona).
	 *
	 * @param parent
	 *            The parent composite for this container
	 * @param style
	 *            the style for this container
	 * @return the created container
	 */
	protected Composite createHelpContainer(final Composite parent, final int style) {
		Composite container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		container.setSize(40, 400);
		layout.marginHeight = 5;
		layout.marginWidth = 5;
		container.setLayout(layout);

		helpButton = new Button(container, SWT.PUSH);
		helpButton.setSize(20, 20);
		helpButton.setImage(Activator.getDefault().getImageRegistry().get(Activator.HELP_ICON));
		helpButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				PlatformUI.getWorkbench().getHelpSystem().displayHelpResource(getHelpResourceURL());
			}
		});

		return container;
	}

	/**
	 * Creates the
	 *
	 * @param parent
	 * @return
	 */
	protected Composite createStatusContainer(final Composite parent) {
		final Group composite = new Group(parent, SWT.NONE);
		composite.setText("Status");

		GridLayout layout = new GridLayout(5, false);
		composite.setLayout(layout);

		GridLayout containerLayout = new GridLayout(3, false);

		/**
		 * Container 1 (left column)
		 */
		Composite container1 = new Composite(composite, SWT.NONE);
		container1.setLayout(containerLayout);

		author = new Label(container1, SWT.NONE);
		author.setText("Author");

		authorText = new Text(container1, SWT.BORDER);
		authorText.addListener(SWT.Modify, modifyListener);
		authorText.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) authorText.getLayoutData()).horizontalSpan = 2;
		authorText.setTextLimit(presenter.getAuthorMaxLength());

		responsibleLabel = new Label(container1, SWT.NONE);
		responsibleLabel.setText("Responsible User");

		responsibleText = new Text(container1, SWT.BORDER);
		responsibleText.addListener(SWT.Modify, modifyListener);
		responsibleText.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) responsibleText.getLayoutData()).horizontalSpan = 2;
		responsibleText.setTextLimit(presenter.getResponsibleUserMaxLength());

		workPackageLabel = new Label(container1, SWT.NONE);
		workPackageLabel.setText("Work package");

		workPackageText = new Text(container1, SWT.BORDER);
		workPackageText.addListener(SWT.Modify, modifyListener);
		workPackageText.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) workPackageText.getLayoutData()).horizontalSpan = 2;
		workPackageText.setTextLimit(presenter.getWorkpackageMaxLength());

		/**
		 * Container 2 (middle left column)
		 */
		Composite container2 = new Composite(composite, SWT.NONE);
		container2.setLayout(containerLayout);

		versionLabel = new Label(container2, SWT.NONE);
		versionLabel.setText("Version");

		versionText = new Text(container2, SWT.BORDER);
		versionText.addListener(SWT.Modify, modifyListener);
		versionText.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) versionText.getLayoutData()).horizontalSpan = 2;
		versionText.setTextLimit(presenter.getVersionMaxLength());
		versionText.addListener(SWT.Verify, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('0' <= chars[i] && chars[i] <= '9' || chars[i] == '.')) {
						e.doit = false;
						return;
					}
				}
			}
		});

		managementDecisionLabel = new Label(container2, SWT.NONE);
		managementDecisionLabel.setText("Mngmt. Decision");
		managementDecisionLabel.setToolTipText("Management Decision");

		managementDecisionCombo = new Combo(container2, SWT.READ_ONLY);
		managementDecisionCombo.add("");
		for (ManagementDecision decission : ManagementDecision.values()) {
			managementDecisionCombo.add(decission.getLiteral());
		}
		managementDecisionCombo.addListener(SWT.Modify, modifyListener);
		managementDecisionCombo.setLayoutData(new GridData(FIELD_LENGTH - 14, SWT.DEFAULT));
		((GridData) managementDecisionCombo.getLayoutData()).horizontalSpan = 2;

		priorityLabel = new Label(container2, SWT.NONE);
		priorityLabel.setText("Priority");

		priorityCombo = new Combo(container2, SWT.READ_ONLY);
		priorityCombo.add("");
		for (Priority priority : Priority.values()) {
			priorityCombo.add(priority.getLiteral());
		}
		priorityCombo.addListener(SWT.Modify, modifyListener);
		priorityCombo.setLayoutData(new GridData(FIELD_LENGTH - 14, SWT.DEFAULT));
		((GridData) priorityCombo.getLayoutData()).horizontalSpan = 2;

		/**
		 * Container3
		 */
		Composite container3 = new Composite(composite, SWT.NONE);
		container3.setLayout(containerLayout);

		lifeCyclePhaseLabel = new Label(container3, SWT.NONE);
		lifeCyclePhaseLabel.setText("Life Cycle Phase");

		lifeCyclePhaseField = new Text(container3, SWT.READ_ONLY | SWT.BORDER);
		lifeCyclePhaseField.addListener(SWT.Modify, modifyListener);
		lifeCyclePhaseField.setLayoutData(new GridData(FIELD_LENGTH - 18, SWT.DEFAULT));

		advanceLifeCyclePhaseButton = new Button(container3, SWT.NONE);
		advanceLifeCyclePhaseButton.setToolTipText("Advance to next Life Cycle Phase");
		advanceLifeCyclePhaseButton
				.setImage(Activator.getDefault().getImageRegistry().get(Activator.NAVIGATE_FORWARD_ICON));
		advanceLifeCyclePhaseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				ElementLifeCycleDialog dialog = new ElementLifeCycleDialog(composite.getShell(), element,
						Trigger.ADVANCE_LIFE_CYCLE_PHASE, lifeCyclePhaseField, stateField);
				if (dialog.open() == Window.OK) {
					String message = dialog.getChangelogMessage();
					if (message != null && !message.isEmpty()) {
						addChangelogComment(message);
					}
				}
			}
		});

		stateLabel = new Label(container3, SWT.NONE);
		stateLabel.setText("State");

		stateField = new Text(container3, SWT.READ_ONLY | SWT.BORDER);
		stateField.addListener(SWT.Modify, modifyListener);
		stateField.setLayoutData(new GridData(FIELD_LENGTH - 18, SWT.DEFAULT));

		advanceStateButton = new Button(container3, SWT.NONE);
		advanceStateButton.setToolTipText("Advance to next State");
		advanceStateButton.setImage(Activator.getDefault().getImageRegistry().get(Activator.NAVIGATE_FORWARD_ICON));
		advanceStateButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				ElementLifeCycleDialog dialog = new ElementLifeCycleDialog(composite.getShell(), element,
						Trigger.ADVANCE_STATE, lifeCyclePhaseField, stateField);
				if (dialog.open() == Window.OK) {
					String message = dialog.getChangelogMessage();
					if (message != null && !message.isEmpty()) {
						addChangelogComment(message);
					}
				}
			}
		});

		qaAssessmentLabel = new Label(container3, SWT.NONE);
		qaAssessmentLabel.setText("QA Assessment");

		qaAssessmentCombo = new Combo(container3, SWT.READ_ONLY);
		qaAssessmentCombo.add("");
		for (QAAssessment assessment : QAAssessment.values()) {
			qaAssessmentCombo.add(assessment.getLiteral());
		}
		qaAssessmentCombo.addListener(SWT.Modify, modifyListener);
		qaAssessmentCombo.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) qaAssessmentCombo.getLayoutData()).horizontalSpan = 2;

		/**
		 * Container4 (middle right column)
		 */

		Composite container4 = new Composite(composite, SWT.NONE);
		container4.setLayout(containerLayout);

		creationDateLabel = new Label(container4, SWT.NONE);
		creationDateLabel.setText("Creation Date");

		creationDateText = new Text(container4, SWT.BORDER);
		creationDateText.setEditable(false);
		creationDateText.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) creationDateText.getLayoutData()).horizontalSpan = 2;

		lastSaveDateLabel = new Label(container4, SWT.NONE);
		lastSaveDateLabel.setText("Last Save");

		lastSaveDateText = new Text(container4, SWT.BORDER);
		lastSaveDateText.setEditable(false);
		lastSaveDateText.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) lastSaveDateText.getLayoutData()).horizontalSpan = 2;

		deadlineLabel = new Label(container4, SWT.NONE);
		deadlineLabel.setText("Deadline");

		deadlineText = new Text(container4, SWT.BORDER);
		deadlineText.setToolTipText("Date format: yyyy-mm-dd");
		deadlineText.addListener(SWT.Modify, modifyListener);
		deadlineText.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		deadlineText.setTextLimit(presenter.getDeadlineMaxLength());
		deadlineText.addListener(SWT.Verify, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('0' <= chars[i] && chars[i] <= '9' || chars[i] == '-')) {
						e.doit = false;
						return;
					}
				}
			}
		});
		deadlineText.addListener(SWT.FocusOut, new Listener() {
			@Override
			public void handleEvent(Event event) {
				try {
					shortSDF.parse(deadlineText.getText());
				} catch (ParseException e) {
					deadlineText.setFocus();
					deadlineText.setSelection(0);
				}
			}
		});

		final Button calButton = new Button(container4, SWT.NONE);

		calButton.setImage(Activator.getDefault().getImageRegistry().get(Activator.CALENDAR_ICON));
		calButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				calButton.toDisplay(calButton.getLocation());
				DateSelectionDialog dialog = new DateSelectionDialog(composite.getShell(),
						new Point(calButton.toDisplay(calButton.getLocation()).x, calButton.getLocation().y));
				if (dialog.open() == Dialog.OK) {
					deadlineText.setText(shortSDF.format(dialog.getSelectedDate()));
				}
			}
		});

		/**
		 * Container5 (right column)
		 */

		Composite container5 = new Composite(composite, SWT.NONE);
		container5.setLayout(layout);

		lockLabel = new Label(container5, SWT.NONE);
		lockLabel.setText("Lock Status");
		lockLabel.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) lockLabel.getLayoutData()).horizontalSpan = 3;

		lockScale = new Scale(container5, SWT.NONE);
		lockScale.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) lockScale.getLayoutData()).horizontalSpan = 3;
		lockScale.setMinimum(0);
		lockScale.setMaximum(3);
		lockScale.setIncrement(1);

		lockStatus = new Text(container5, SWT.BORDER);
		lockStatus.setLayoutData(new GridData(FIELD_LENGTH, SWT.DEFAULT));
		((GridData) lockStatus.getLayoutData()).horizontalSpan = 3;
		lockStatus.setEditable(false);
		lockStatus.addListener(SWT.Modify, modifyListener);

		lockScale.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				switch (lockScale.getSelection()) {
				// Modified for Issue #17 to rename the lock status to better
				// name
				case LockType.LOCK_LIKE_CONTAINER_VALUE:
					lockStatus.setText(LockType.LOCK_LIKE_CONTAINER.getLiteral());
					break;
				case LockType.LOCK_NOTHING_VALUE:
					lockStatus.setText(LockType.LOCK_NOTHING.getLiteral());
					break;
				case LockType.LOCK_COMMENTING_ONLY_VALUE:
					lockStatus.setText(LockType.LOCK_COMMENTING_ONLY.getLiteral());
					break;
				case LockType.LOCK_CONTENTS_VALUE:
					lockStatus.setText(LockType.LOCK_CONTENTS.getLiteral());
					break;
				}
				// End Modified for Issue #17 to rename the lock status to
				// better name
			}
		});

		return composite;

	}

	private Composite createDiscussionContainer(final Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		Label discussionTextLabel = new Label(composite, SWT.NONE);
		discussionTextLabel.setText("Discussion:");

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalSpan = 2;
		editorLayoutData.widthHint = 400;
		editorLayoutData.heightHint = 200;
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessVerticalSpace = true;

		discussionEditor = new RichTextEditor(composite, SWT.BORDER, getEditorSite(), commandListener);
		discussionEditor.setLayoutData(editorLayoutData);
		discussionEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				modifyListener.handleEvent(null);
			}
		});

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	@Override
	protected void createPages() {
		int index = -1;
		index = addPage(createManagementPage(getContainer()));
		setPageText(index, "Management");

		index = addPage(createTracingPage(getContainer()));
		setPageText(index, "Tracing");

		index = addPage(createQuantitiesPage(getContainer()));
		setPageText(index, "Quantities");

		fillManagementData();
		fillCostAndRiskData();

		isDirty = false;
		firePropertyChange(PROP_DIRTY);
	}

	private Composite createManagementPage(final Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(1, true);
		composite.setLayout(layout);

		GridData managementGridData = new GridData();
		managementGridData.grabExcessHorizontalSpace = true;
		managementGridData.horizontalAlignment = SWT.FILL;
		managementGridData.widthHint = 400;
		Composite managementContainer = createStatusContainer(composite);
		managementContainer.setLayoutData(managementGridData);

		GridData tableGridData = new GridData();
		tableGridData.grabExcessHorizontalSpace = true;
		tableGridData.horizontalAlignment = SWT.FILL;
		tableGridData.widthHint = 400;
		tableGridData.heightHint = 250;

		Composite tableContainer = createChangelogContainer(composite);
		tableContainer.setLayoutData(tableGridData);

		GridData editorLayoutData = new GridData();
		editorLayoutData.horizontalSpan = 2;
		editorLayoutData.widthHint = 400;
		editorLayoutData.heightHint = 200;
		editorLayoutData.horizontalAlignment = SWT.FILL;
		editorLayoutData.grabExcessHorizontalSpace = true;
		editorLayoutData.verticalAlignment = SWT.FILL;
		editorLayoutData.grabExcessVerticalSpace = true;
		Composite discussionContainer = createDiscussionContainer(composite);
		discussionContainer.setLayoutData(editorLayoutData);

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	private Composite createTracingPage(final Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(1, true);
		composite.setLayout(layout);

		GridData layoutData = new GridData();
		layoutData.horizontalSpan = 2;
		layoutData.widthHint = 400;
		layoutData.heightHint = 200;
		layoutData.horizontalAlignment = SWT.FILL;
		layoutData.grabExcessHorizontalSpace = true;
		layoutData.verticalAlignment = SWT.FILL;
		layoutData.grabExcessVerticalSpace = true;
		Composite container = createTracingPageContainer(composite);
		container.setLayoutData(layoutData);

		return SWTUtils.wrapInScrolledComposite(parent, composite);
	}

	private Composite createTracingPageContainer(final Composite parent) {
		Group container = new Group(parent, SWT.NONE);
		container.setText("Dependencies");

		GridLayout layout = new GridLayout(3, false);
		container.setLayout(layout);

		GridData tableData = new GridData();
		tableData.grabExcessHorizontalSpace = true;
		tableData.grabExcessVerticalSpace = true;
		tableData.verticalAlignment = SWT.FILL;
		tableData.horizontalAlignment = SWT.FILL;
		tableData.horizontalSpan = 3;

		// dependencies = new ArrayList<Dependency>();

		dependencyTable = new AgileGrid(container, SWT.MULTI | SWT.V_SCROLL | SWTX.ROW_SELECTION | SWT.WRAP);
		dependencyTable.setLayoutData(tableData);
		dependencyTableLayoutAdvisor = new DependencyTableLayoutAdvisor(dependencyTable, columnNames);
		dependencyTableContentProvider = new DependencyTableContentProvider(dependencies, dependenciesRelatedBy);
		dependencyTableCellEditorProvider = new DependencyTableCellEditorProvider(dependencyTable, element.eClass().getName());
		dependencyTableRendererProvider = new DependencyTableCellRendererProvider(dependencyTable);

		dependencyTableContentProvider.setContent(dependencies, dependenciesRelatedBy);
		dependencyTableCellEditorProvider.setRelatedByContent(dependenciesRelatedBy);
		dependencyTableLayoutAdvisor.setRowCount(dependencies.size() + dependenciesRelatedBy.size());
		dependencyTable.redraw();

		dependencyTable.setLayoutAdvisor(dependencyTableLayoutAdvisor);
		dependencyTable.setContentProvider(dependencyTableContentProvider);
		dependencyTable.setCellEditorProvider(dependencyTableCellEditorProvider);
		dependencyTable.setCellRendererProvider(dependencyTableRendererProvider);

		Composite buttonContainer = new Composite(container, SWT.NONE);
		GridData buttonGridData = new GridData();
		buttonGridData.horizontalSpan = 3;
		buttonContainer.setLayoutData(buttonGridData);
		GridLayout buttonLayout = new GridLayout(3, false);
		buttonContainer.setLayout(buttonLayout);

		addDependencyButton = new Button(buttonContainer, SWT.NONE);
		addDependencyButton.setText("Add Dependency");
		addDependencyButton.setEnabled(true);
		addDependencyButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				presenter.addDependency(BaseEditor.this);
				dependencyTableLayoutAdvisor.setRowCount(dependencies.size() + dependenciesRelatedBy.size());
				dependencyTable.redraw();
			}

		});

		openDependencyButton = new Button(buttonContainer, SWT.NONE);
		openDependencyButton.setText("Open");
		openDependencyButton.setEnabled(false);
		openDependencyButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Cell[] selectedCells = dependencyTable.getCellSelection();
				Set<Dependency> selectedDependencies = new HashSet<Dependency>();

				for (Cell cell : selectedCells) {
					// selectedDependencies.add(dependencies.get(cell.row));
					if (cell.row < dependenciesRelatedBy.size()) {
						selectedDependencies.add(dependenciesRelatedBy.get(cell.row));
					} else {
						selectedDependencies.add(dependencies.get(cell.row - dependenciesRelatedBy.size()));
					}
				}

				for (Dependency dependency : selectedDependencies) {
					((BaseEditorPresenter) presenter).openDependencyElement(BaseEditor.this, dependency);
				}
			}
		});

		deleteDependencyButton = new Button(buttonContainer, SWT.NONE);
		deleteDependencyButton.setText("Delete");
		deleteDependencyButton.setEnabled(false);
		deleteDependencyButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Cell[] selectedCells = dependencyTable.getCellSelection();
				Set<Dependency> selectedDependencies = new HashSet<Dependency>();
				Set<Dependency> selectedDependenciesRelatedBy = new HashSet<Dependency>();

				for (Cell cell : selectedCells) {
					// selectedDependencies.add(dependencies.get(cell.row));
					if (cell.row < dependenciesRelatedBy.size()) {
						selectedDependenciesRelatedBy.add(dependenciesRelatedBy.get(cell.row));
					} else {
						selectedDependencies.add(dependencies.get(cell.row - dependenciesRelatedBy.size()));
					}
				}

				for (Dependency dependency : selectedDependenciesRelatedBy) {
					((BaseEditorPresenter) presenter).deleteDependency(BaseEditor.this, dependency, true);
				}
				for (Dependency dependency : selectedDependencies) {
					((BaseEditorPresenter) presenter).deleteDependency(BaseEditor.this, dependency, false);
				}

				dependencyTableLayoutAdvisor.setRowCount(dependencies.size() + dependenciesRelatedBy.size());
				dependencyTable.clearSelection();
				deleteDependencyButton.setEnabled(false);
				openDependencyButton.setEnabled(false);
				dependencyTable.redraw();
			}
		});

		dependencyTable.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				if (event.getNewSelections().size() > 0) {
					deleteDependencyButton.setEnabled(true);
					openDependencyButton.setEnabled(true);
				} else {
					deleteDependencyButton.setEnabled(false);
					openDependencyButton.setEnabled(false);
				}
			}
		});

		dependencyTable.addCellDoubleClickListener(new ICellDoubleClickListener() {
			@Override
			public void cellDoubleClicked(CellDoubleClickEvent event) {
				Cell[] selectedCell = dependencyTable.getCellSelection();
				// Dependency dependency =
				// dependencies.get(selectedCell[0].row);
				Dependency dependency = null;
				if (selectedCell[0].row < dependenciesRelatedBy.size()) {
					dependency = dependenciesRelatedBy.get(selectedCell[0].row);
				} else {
					dependency = dependencies.get(selectedCell[0].row - dependenciesRelatedBy.size());
				}
				((BaseEditorPresenter) presenter).openDependencyElement(BaseEditor.this, dependency);
			}
		});

		return container;
	}

	private Composite createQuantitiesPage(final Composite parent) {
		Composite verticalComp = new Composite(parent, SWT.NONE);
		verticalComp.setLayout(new GridLayout(1, true));
		Composite topComposite = new Composite(verticalComp, SWT.NONE);

		GridLayout layout = new GridLayout(3, true);
		topComposite.setLayout(layout);

		Group gpCost = new Group(topComposite, SWT.NONE);
		gpCost.setSize(181, 61);
		gpCost.setText("Assigned Cost");
		gpCost.setLayout(new GridLayout(2, false));

		GridData gd_spinner = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_spinner.widthHint = 30;

		GridData gd_cmbUnit = new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1);
		gd_cmbUnit.heightHint = 50;
		gd_cmbUnit.minimumWidth = 50;
		gd_cmbUnit.widthHint = 20;

		spinnerCost = new Spinner(gpCost, SWT.BORDER);
		spinnerCost.setIncrement(1);
		spinnerCost.setMaximum(99999999);
		spinnerCost.addListener(SWT.Modify, modifyListener);
		spinnerCost.setLayoutData(gd_spinner);

		cmbUnitCost = new Combo(gpCost, SWT.READ_ONLY);
		cmbUnitCost.addListener(SWT.Modify, modifyListener);
		cmbUnitCost.setLayoutData(gd_cmbUnit);

		Group gpBenefit = new Group(topComposite, SWT.NONE);
		gpBenefit.setSize(181, 61);
		gpBenefit.setText("Assigned Benefit");
		gpBenefit.setLayout(new GridLayout(2, false));

		spinnerBenefit = new Spinner(gpBenefit, SWT.BORDER);
		spinnerBenefit.setIncrement(1);
		spinnerBenefit.setMaximum(99999999);
		spinnerBenefit.addListener(SWT.Modify, modifyListener);
		spinnerBenefit.setLayoutData(gd_spinner);

		cmbUnitBenefit = new Combo(gpBenefit, SWT.READ_ONLY);
		cmbUnitBenefit.addListener(SWT.Modify, modifyListener);
		cmbUnitBenefit.setLayoutData(gd_cmbUnit);

		Group gpRisk = new Group(topComposite, SWT.NONE);
		gpRisk.setSize(181, 61);
		gpRisk.setText("Risk");
		gpRisk.setLayout(new GridLayout(2, false));

		spinnerRisk = new Spinner(gpRisk, SWT.BORDER);
		spinnerRisk.setIncrement(1);
		spinnerRisk.setMaximum(100);
		spinnerRisk.addListener(SWT.Modify, modifyListener);
		spinnerRisk.setLayoutData(gd_spinner);

		Label label = new Label(gpRisk, SWT.None);
		label.setText("%");

		// Setup quantity column
		SashForm sashQuantities = new SashForm(verticalComp, SWT.VERTICAL);
		Composite compQuantities = new Composite(sashQuantities, SWT.NONE);
		compQuantities.setLayout(new GridLayout(2, true));
		gdQuantities = new EAGComposite<QuantityItem>(compQuantities, SWT.NONE, modifyListener)
				.setModifyListener(modifyListener).setPresenter(((BaseEditorPresenter) presenter).quantityPresenter);
		gdQuantities.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		sashQuantities.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		new Composite(sashQuantities, SWT.NONE);
		sashQuantities.setWeights(new int[] { 1, 1 });

		return SWTUtils.wrapInScrolledComposite(parent, verticalComp);
	}

	/**
	 * Implements the save functionality.
	 *
	 * @param monitor
	 *            progress monitor
	 */
	public void doSave(final IProgressMonitor monitor) {

		boolean NeedToClearAllPassword = false;
		if (element.getLockStatus() != LockType.LOCK_LIKE_CONTAINER
				&& element.getLockStatus() != LockType.LOCK_NOTHING) {

			String elementPassword = element.getLockPassword();
			String workspacePassword = WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getPassword();

			boolean passwordMatch = false;

			if (elementPassword != null && workspacePassword != null && elementPassword.equals(workspacePassword)) {
				passwordMatch = true;
			}

			// Added for Issue #17 check if password is empty and lock slider is
			// not moved, prompt for unlock
			else if (elementPassword != null && elementPassword.isEmpty()
					&& LockType.get(lockScale.getSelection()) == element.getLockStatus()) {
				MessageDialog dialog = new MessageDialog(Display.getCurrent().getActiveShell(), "Element locked", null,
						"This element is locked from editing. Do you want to unlock it?", MessageDialog.CONFIRM,
						new String[] { "OK", "Cancel" }, 0);
				int result = dialog.open();
				if (result == Dialog.OK) {
					presenter.setLockType(LockType.LOCK_NOTHING);
					lockScale.setSelection(LockType.LOCK_NOTHING_VALUE);
					lockStatus.setText(LockType.LOCK_NOTHING.getLiteral());
				} else {
					return;
				}
			}

			else if (!passwordMatch && !elementPassword.isEmpty()) {
				// End Added for Issue #17 check if password is empty, prompt
				// for unlock

				InputDialog dialog = new InputDialog(Display.getCurrent().getActiveShell(), "Element locked",
						"This element is locked for editing.\n" + "That means that you are not able to save "
								+ "any changes made in this editor, unless you enter the correct" + " password.\n",
						"", null);
				if (dialog.open() == Dialog.OK) {
					if (dialog.getValue().equals(RED_MASTER_PASSWORD)) {
						// remove the lock
						NeedToClearAllPassword = true;
					} else if (!dialog.getValue().equals(elementPassword)) {
						MessageDialog.openError(null, "Wrong password", "The password you entered is incorrect.");
						return;
					}
				} else {
					return;
				}
			}
		}

		if (NeedToClearAllPassword) {

			boolean resetAllPassword = MessageDialog.openConfirm(Display.getCurrent().getActiveShell(),
					"Master Password",
					"You have entered a master password. Do you want to reset all password associated with this file?");
			if (resetAllPassword) {
				MasterPasswordUnlocker unlocker = new MasterPasswordUnlocker();
				EObject rootObject = EcoreUtil.getRootContainer(element);
				if (rootObject != null && rootObject instanceof Element) {
					Element rootElement = (Element) rootObject;
					rootElement.acceptVisitor(unlocker);
				}
			}
		}

		if (monitor != null) {
			monitor.beginTask("Save", IProgressMonitor.UNKNOWN);
		}

		presenter.setAuthor(authorText.getText());
		presenter.setDeadline(deadlineText.getText());
		presenter.setPriority(priorityCombo.getItems()[priorityCombo.getSelectionIndex()]);
		presenter.setResponsibleUser(responsibleText.getText());
		presenter.setState(stateField.getText());
		presenter.setLifeCyclePhase(lifeCyclePhaseField.getText());
		presenter
				.setManagementDecision(managementDecisionCombo.getItems()[managementDecisionCombo.getSelectionIndex()]);
		presenter.setQAAssessment(qaAssessmentCombo.getItems()[qaAssessmentCombo.getSelectionIndex()]);
		presenter.setVersion(versionText.getText());
		presenter.setWorkpackage(workPackageText.getText());
		presenter.setChangelog(changelog);

		switch (lockScale.getSelection()) {
		case LockType.LOCK_LIKE_CONTAINER_VALUE:
			presenter.setLockType(LockType.LOCK_LIKE_CONTAINER);
			break;
		case LockType.LOCK_NOTHING_VALUE:
			presenter.setLockType(LockType.LOCK_NOTHING);
			break;
		case LockType.LOCK_COMMENTING_ONLY_VALUE:
			presenter.setLockType(LockType.LOCK_COMMENTING_ONLY);
			break;
		case LockType.LOCK_CONTENTS_VALUE:
			presenter.setLockType(LockType.LOCK_CONTENTS);
			break;
		}

		// synchronize associations
		List<ElementRelationship> deletedRelationships = new ArrayList<ElementRelationship>();
		List<ElementRelationship> deletedRelatedByRelationships = new ArrayList<ElementRelationship>();
		List<Dependency> addedRelationships = new ArrayList<Dependency>();

		for (ElementRelationship relationship : element.getRelatesTo()) {
			if (!(relationship instanceof ElementReference)) {
				continue;
			}
			boolean foundRelationship = false;
			for (Dependency dependency : getDependencies()) {
				if (relationship.getToElement() == dependency.targetElement) {
					((ElementReference) relationship).setRelationshipKind(dependency.dependencyKind);
					relationship.setRelevance(dependency.comment);
					foundRelationship = true;
					break;
				}
			}

			if (foundRelationship) {
				continue;
			} else {
				deletedRelationships.add(relationship);
			}
		}

		for (ElementRelationship relationship : element.getRelatedBy()) {
			if (!(relationship instanceof ElementReference)) {
				continue;
			}
			boolean foundRelationship = false;
			for (Dependency dependency : getDependenciesRelatedBy()) {
				if (relationship.getFromElement() == dependency.targetElement) {
					((ElementReference) relationship).setRelationshipKind(
							RelationshipKind.getReverseRelationshipKind(dependency.dependencyKind));
					relationship.setRelevance(dependency.comment);
					foundRelationship = true;
					break;
				}
			}

			if (foundRelationship) {
				continue;
			} else {
				deletedRelatedByRelationships.add(relationship);
			}
		}

		for (Dependency dependencies : getDependencies()) {
			boolean foundRelationship = false;
			for (ElementRelationship relationship : element.getRelatesTo()) {
				if (relationship.getToElement() == dependencies.targetElement) {
					foundRelationship = true;
					break;
				}
			}

			if (foundRelationship) {
				continue;
			} else {
				addedRelationships.add(dependencies);
			}
		}

		for (ElementRelationship relationship : deletedRelationships) {
			element.getRelatesTo().remove(relationship);
			relationship.delete();
		}

		for (ElementRelationship relationship : deletedRelatedByRelationships) {
			element.getRelatedBy().remove(relationship);
			relationship.delete();
		}

		for (Dependency dependency : addedRelationships) {
			ElementReference newRelationship = RelationshipFactory.eINSTANCE.createElementReference();
			newRelationship.setFromElement(element);
			newRelationship.setToElement(dependency.targetElement);
			newRelationship.setRelationshipKind(dependency.dependencyKind);
			newRelationship.setRelevance(dependency.comment);

			validateActivePassivePattern(newRelationship);
		}

		presenter.setCost(spinnerCost.getSelection());
		presenter.setBenefit(spinnerBenefit.getSelection());
		presenter.setRisk(spinnerRisk.getSelection());

		presenter.save();

		if (element != null) {
			if (element.getLastModified() != null)
				lastSaveDateText.setText(longSDF.format(element.getLastModified()));
			setPartName(formatPartName(element));
		}
		if (!(element instanceof Project)) {
			for (File file : WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getCurrentlyOpenedFiles()) {
				file.save();
			}
		}
		isDirty = false;
		firePropertyChange(IEditorPart.PROP_DIRTY);

		if (monitor != null) {
			monitor.done();
		}
	}

	@Override
	public abstract void doSaveAs();

	public abstract String getEditorID();

	/**
	 * Fills the initial management data when the editor is opened.
	 */
	protected void fillManagementData() {
		if (element.getCreator() != null && element.getCreator().getName() != null) {
			authorText.setText(element.getCreator().getName());
		}

		if (element.getResponsibleUser() != null && element.getResponsibleUser().getName() != null) {
			responsibleText.setText(element.getResponsibleUser().getName());
		}

		if (element.getTimeCreated() != null) {
			creationDateText.setText(longSDF.format(element.getTimeCreated()));
		}

		if (element.getLastModified() != null) {
			lastSaveDateText.setText(longSDF.format(element.getLastModified()));
		}

		if (element.getDeadline() != null) {
			deadlineText.setText(shortSDF.format(element.getDeadline()));
		}

		if (element.getVersion() != null) {
			versionText.setText(element.getVersion());
		}

		if (element.getState() != null) {
			stateField.setText(element.getState().getLiteral());
		}
		if (element.getLifeCyclePhase() != null) {
			lifeCyclePhaseField.setText(element.getLifeCyclePhase().getLiteral());
		}

		if (element.getManagementDecision() != null) {
			managementDecisionCombo.select(element.getManagementDecision().ordinal() + 1);
		}

		if (element.getQaAssessment() != null) {
			qaAssessmentCombo.select(element.getQaAssessment().ordinal() + 1);
		}

		if (element.getPriority() != null) {
			priorityCombo.select(element.getPriority().ordinal() + 1);
		}

		if (element.getWorkPackage() != null) {
			workPackageText.setText(element.getWorkPackage());
		}

		switch (element.getLockStatus().getValue()) {
		case LockType.LOCK_LIKE_CONTAINER_VALUE:
			lockScale.setSelection(LockType.LOCK_LIKE_CONTAINER_VALUE);
			break;
		case LockType.LOCK_NOTHING_VALUE:
			lockScale.setSelection(LockType.LOCK_NOTHING_VALUE);
			break;
		case LockType.LOCK_COMMENTING_ONLY_VALUE:
			lockScale.setSelection(LockType.LOCK_COMMENTING_ONLY_VALUE);
			break;
		case LockType.LOCK_CONTENTS_VALUE:
			lockScale.setSelection(LockType.LOCK_CONTENTS_VALUE);
			break;
		}
		lockScale.notifyListeners(SWT.Selection, new Event());
	}

	private void fillCostAndRiskData() {

		if (element.getCost() != null) {
			spinnerCost.setSelection((int) element.getCost().getValue());
		}
		if (element.getBenefit() != null) {
			spinnerBenefit.setSelection((int) element.getBenefit().getValue());
		}
		if (element.getRisk() >= 0) {
			spinnerRisk.setSelection(element.getRisk());
		}

		fillUnitCombo(cmbUnitCost, element.getCost());
		fillUnitCombo(cmbUnitBenefit, element.getBenefit());

		gdQuantities.setColumns(((BaseEditorPresenter) presenter).getQuantityColumnDefinition())
				.setDataSource(element.getQuantities());
	}

	private void fillUnitCombo(Combo cmbUnit, Property property) {
		cmbUnit.setItems(new String[] {});
		Object[] values = property.getKind() == Kind.MEASUREMENT ? DurationUnit.values() : MonetaryUnit.values();

		for (Object unit : values) {
			String name = ((Enumerator) unit).getName();
			cmbUnit.add(name);
		}

		String name = property.getKind() == Kind.MEASUREMENT ? property.getDurationUnit().getName()
				: property.getMonetaryUnit().getName();
		cmbUnit.select(cmbUnit.indexOf(name));
	}

	@Override
	public List<Dependency> getDependencies() {
		return dependencies;
	}

	@Override
	public List<Dependency> getDependenciesRelatedBy() {
		return dependenciesRelatedBy;
	}

	@Override
	public Element getEditorElement() {
		return element;
	}

	/**
	 * Get a reference to a user-selected element. Opens a wizard, which lets
	 * the user pick an element of any type.
	 *
	 * @return the selected element.
	 */
	Element getElementReference() {
		return getElementReference(null);
	}

	/**
	 * Get a reference to a user-selected element. Opens a wizard, which lets
	 * the user pick an element of one of the types in the allowedTypes
	 * parameter.
	 *
	 * @param allowedTypes
	 *            list of allowed types which the user can select.
	 * @return the selected element.
	 */
	private Element getElementReference(final Class[] allowedTypes) {
		CreateElementReferenceWizard wizard = new CreateElementReferenceWizardImpl(allowedTypes);
		wizard.init(PlatformUI.getWorkbench(), (IStructuredSelection) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService().getSelection());

		WizardDialog dialog = new WizardDialog(Display.getCurrent().getActiveShell(), wizard);

		dialog.open();

		Element selectedElement = ((CreateElementReferenceWizardPresenter) wizard.getPresenter()).getselectedElement();

		return selectedElement;
	}

	@Override
	public IEditorPresenter getPresenter() {
		return presenter;
	}

	private static UpdateValueStrategy createUpdateStrategy(final String label) {
		return new UpdateValueStrategy() {
			@Override
			public Object convert(final Object value) {
				if (value != null) {
					return label + ": " + value;
				} else {
					return "";
				}
			}
		};

	}

	private void bindValues(StatusLineContributionItem item, EAttribute literal, Object propertySource,
			UpdateValueStrategy strategy) {
		IObservableValue widgetValue = JFaceProperties
				.value(StatusLineContributionItemWithPropertyListener.class, "text", "text").observe(item);
		IObservableValue modelValue = EMFProperties.value(literal).observe(propertySource);

		context.bindValue(widgetValue, modelValue, null, strategy);
	}

	@Override
	public void setFocus() {
		super.setFocus();

		if (element == null) {
			return;
		}

		IStatusLineManager statusManager = getEditorSite().getActionBars().getStatusLineManager();

		bindValues((StatusLineContributionItem) statusManager.find(AUTHOR), ElementPackage.Literals.ELEMENT__NAME,
				element.getCreator(), createUpdateStrategy("Author"));
		bindValues((StatusLineContributionItem) statusManager.find(RESPONSIBLE), ElementPackage.Literals.ELEMENT__NAME,
				element.getResponsibleUser(), createUpdateStrategy("Responsible"));
		// bindValues((StatusLineContributionItem)
		// statusManager.find(ELEMENT_STATUS),
		// ElementPackage.Literals.ELEMENT__LIFECYCLE_STATUS, element,
		// createUpdateStrategy("Status"));
		bindValues((StatusLineContributionItem) statusManager.find(ELEMENT_STATUS),
				ElementPackage.Literals.ELEMENT__STATE, element, createUpdateStrategy("Status"));
		bindValues((StatusLineContributionItem) statusManager.find(PRIORITY), ElementPackage.Literals.ELEMENT__PRIORITY,
				element, createUpdateStrategy("Priority"));
		bindValues((StatusLineContributionItem) statusManager.find(LAST_MODIFIED),
				ElementPackage.Literals.ELEMENT__LAST_MODIFIED, element, new UpdateValueStrategy() {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

					@Override
					public Object convert(final Object value) {
						if (value != null) {
							return "Last save: " + sdf.format(value);
						} else {
							return "";
						}
					}
				});

	}

	@Override
	public void init(final IEditorSite site, final IEditorInput input) throws PartInitException {

		isDirty = false;
		modifyListener = new EditorModifyListener();

		dependencies = new ArrayList<Dependency>();
		dependenciesRelatedBy = new ArrayList<Dependency>();

		if (input instanceof BaseEditorInput) {
			element = ((BaseEditorInput) input).getElement();
		} else {
			throw new PartInitException("Editor input is of invalid type. " + "Must be BaseEditorInput");
		}

		setSite(site);
		setInput(input);

		context = new EMFDataBindingContext();

		undoHandler = new UndoActionHandler(getSite(), IOperationHistory.GLOBAL_UNDO_CONTEXT);
		redoHandler = new RedoActionHandler(getSite(), IOperationHistory.GLOBAL_UNDO_CONTEXT);
		final IActionBars actionBars = getEditorSite().getActionBars();

		actionBars.setGlobalActionHandler(ActionFactory.UNDO.getId(), undoHandler);
		actionBars.setGlobalActionHandler(ActionFactory.REDO.getId(), redoHandler);
		actionBars.updateActionBars();

		if (element != null) {

			setPartName(formatPartName(element));

			for (ElementRelationship tempRelationship : element.getRelatesTo()) {
				if (tempRelationship.getToElement() == null || !(tempRelationship instanceof ElementReference)) {
					continue;
				}
				dependencies.add(new Dependency(((ElementReference) tempRelationship).getRelationshipKind(),
						tempRelationship.getToElement().getIcon(), tempRelationship.getToElement(),
						tempRelationship.getRelevance()));
			}

			for (ElementRelationship tempRelationship : element.getRelatedBy()) {
				if (tempRelationship.getFromElement() == null || tempRelationship.getToElement() == null
						|| !(tempRelationship instanceof ElementReference)) {
					continue;
				}
				RelationshipKind kind = RelationshipKind
						.getReverseRelationshipKind(((ElementReference) tempRelationship).getRelationshipKind());
				if (kind != null) {
					dependenciesRelatedBy.add(new Dependency(kind, tempRelationship.getFromElement().getIcon(),
							tempRelationship.getFromElement(), tempRelationship.getRelevance()));
				}
			}
		}

		getSite().setSelectionProvider(selectionProvider);

	}

	private String formatPartName(Element element) {
		String partName = "";
		if (element.getLabel() != null && !element.getLabel().isEmpty()) {
			partName += element.getLabel();
		}
		if (element.getName() != null && !element.getName().isEmpty()) {
			partName += "-" + element.getName();
		}

		if (partName.length() > 20)
			partName = partName.substring(0, 20) + "..";
		return partName;
	}

	@Override
	public boolean isDirty() {
		return isDirty;
	}

	@Override
	public void markAsDirty() {
		this.modifyListener.handleEvent(null);
		if (parentEditor != null) {
			parentEditor.markAsDirty();
		}
	}

	@Override
	public void refresh() {
		fillInitialData();
		if (element != null) {
			setPartName(formatPartName(element));
		}
	}

	/**
	 * Fills in initial data in various text fields.
	 */
	protected abstract void fillInitialData();

	@Override
	public void selectText(EStructuralFeature feature, int startIndex, int endIndex) {
		// TODO Auto-generated method stub

	}

	public void selectTextBasicInfo(EStructuralFeature feature, int startIndex, int endIndex, int basicInfoPage,
			ElementBasicInfoContainer basicInfoContainer) {
		if (feature == ElementPackage.Literals.ELEMENT__NAME) {
			setActivePage(basicInfoPage);
			basicInfoContainer.getNameTextBox().setSelection(startIndex, endIndex);
		} else if (feature == ElementPackage.Literals.ELEMENT__LABEL) {
			setActivePage(basicInfoPage);
			basicInfoContainer.getLabelTextBox().setSelection(startIndex, endIndex);
		} else if (feature == ElementPackage.Literals.ELEMENT__ELEMENT_KIND) {
			setActivePage(basicInfoPage);
			basicInfoContainer.getKindComboBox().select(startIndex);
		} else if (feature == ElementPackage.Literals.ELEMENT__DESCRIPTION) {
			setActivePage(basicInfoPage);
			basicInfoContainer.getDescriptionRTE().executeCommand(RichTextCommand.SELECT_TEXT,
					new String[] { "" + startIndex, "" + endIndex });
		}
	}

	@Override
	public SearchResultWrapper search(SearchParameter param) {
		// TODO Auto-generated method stub
		return null;
	}

	public void addBasicAttributesToSearch(ElementBasicInfoContainer basicInfoContainer,
			Map<EStructuralFeature, String> attributesToSearch) {
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__NAME, basicInfoContainer.getName());
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__LABEL, basicInfoContainer.getLabel());
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__ELEMENT_KIND, basicInfoContainer.getKind());
		attributesToSearch.put(ElementPackage.Literals.ELEMENT__DESCRIPTION, basicInfoContainer.getDescription());
	}

	public void doSaveFile(final IProgressMonitor monitor) {

		boolean NeedToClearAllPassword = false;
		if (element.getLockStatus() != LockType.LOCK_LIKE_CONTAINER
				&& element.getLockStatus() != LockType.LOCK_NOTHING) {

			String elementPassword = element.getLockPassword();
			String workspacePassword = WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getPassword();

			boolean passwordMatch = false;

			if (elementPassword != null && workspacePassword != null && elementPassword.equals(workspacePassword)) {
				passwordMatch = true;
			}

			// Added for Issue #17 check if password is empty and lock slider is
			// not moved, prompt for unlock
			else if (elementPassword != null && elementPassword.isEmpty()
					&& LockType.get(lockScale.getSelection()) == element.getLockStatus()) {
				MessageDialog dialog = new MessageDialog(Display.getCurrent().getActiveShell(), "Element locked", null,
						"This element is locked from editing. Do you want to unlock it?", MessageDialog.CONFIRM,
						new String[] { "OK", "Cancel" }, 0);
				int result = dialog.open();
				if (result == Dialog.OK) {
					presenter.setLockType(LockType.LOCK_NOTHING);
					lockScale.setSelection(LockType.LOCK_NOTHING_VALUE);
					lockStatus.setText(LockType.LOCK_NOTHING.getLiteral());
				} else {
					return;
				}
			}

			else if (!passwordMatch && !elementPassword.isEmpty()) {
				// End Added for Issue #17 check if password is empty, prompt
				// for unlock

				InputDialog dialog = new InputDialog(Display.getCurrent().getActiveShell(), "Element locked",
						"This element is locked for editing.\n" + "That means that you are not able to save "
								+ "any changes made in this editor, unless you enter the correct" + " password.\n",
						"", null);
				if (dialog.open() == Dialog.OK) {
					if (dialog.getValue().equals(RED_MASTER_PASSWORD)) {
						// remove the lock
						NeedToClearAllPassword = true;
					} else if (!dialog.getValue().equals(elementPassword)) {
						MessageDialog.openError(null, "Wrong password", "The password you entered is incorrect.");
						return;
					}
				} else {
					return;
				}
			}
		}

		if (NeedToClearAllPassword) {

			boolean resetAllPassword = MessageDialog.openConfirm(Display.getCurrent().getActiveShell(),
					"Master Password",
					"You have entered a master password. Do you want to reset all password associated with this file?");
			if (resetAllPassword) {
				MasterPasswordUnlocker unlocker = new MasterPasswordUnlocker();
				EObject rootObject = EcoreUtil.getRootContainer(element);
				if (rootObject != null && rootObject instanceof Element) {
					Element rootElement = (Element) rootObject;
					rootElement.acceptVisitor(unlocker);
				}
			}
		}

		if (monitor != null) {
			monitor.beginTask("Save", IProgressMonitor.UNKNOWN);
		}

		presenter.setAuthor(authorText.getText());
		presenter.setDeadline(deadlineText.getText());
		presenter.setPriority(priorityCombo.getItems()[priorityCombo.getSelectionIndex()]);
		presenter.setResponsibleUser(responsibleText.getText());
		presenter.setState(stateField.getText());
		presenter.setLifeCyclePhase(lifeCyclePhaseField.getText());
		presenter
				.setManagementDecision(managementDecisionCombo.getItems()[managementDecisionCombo.getSelectionIndex()]);
		presenter.setQAAssessment(qaAssessmentCombo.getItems()[qaAssessmentCombo.getSelectionIndex()]);
		presenter.setVersion(versionText.getText());
		presenter.setWorkpackage(workPackageText.getText());
		presenter.setChangelog(changelog);

		switch (lockScale.getSelection()) {

		case LockType.LOCK_LIKE_CONTAINER_VALUE:
			presenter.setLockType(LockType.LOCK_LIKE_CONTAINER);
			break;
		case LockType.LOCK_NOTHING_VALUE:
			presenter.setLockType(LockType.LOCK_NOTHING);
			break;
		case LockType.LOCK_COMMENTING_ONLY_VALUE:
			presenter.setLockType(LockType.LOCK_COMMENTING_ONLY);
			break;
		case LockType.LOCK_CONTENTS_VALUE:
			presenter.setLockType(LockType.LOCK_CONTENTS);
			break;
		}

		if (element != null) {
			lastSaveDateText.setText(longSDF.format(element.getLastModified()));
			setPartName(formatPartName(element));
		}

		presenter.setCost(spinnerCost.getSelection());
		presenter.setBenefit(spinnerBenefit.getSelection());
		presenter.setRisk(spinnerRisk.getSelection());

		presenter.save();

		isDirty = false;
		firePropertyChange(IEditorPart.PROP_DIRTY);

		if (monitor != null) {
			monitor.done();

		}

	}

	private void validateActivePassivePattern(ElementReference reference) {
		int value = reference.getRelationshipKind().getValue();

		// Check if reference is not an active type
		if (value % 2 != 1 && value < RelationshipKind.CONFLICTS_WITH_VALUE) {
			// then change source and target, and the relationship type to
			// active
			Element source = reference.getFromElement();
			reference.setFromElement(reference.getToElement());
			reference.setToElement(source);
			reference.setRelationshipKind(RelationshipKind.getReverseRelationshipKind(reference.getRelationshipKind()));
		
			Element target = reference.getToElement();
			source = reference.getFromElement();
			
			source.getRelatedBy().remove(reference);
			source.getRelatesTo().add(reference);
			
			target.getRelatesTo().remove(reference);
			target.getRelatedBy().add(reference);
		}
	}
}
