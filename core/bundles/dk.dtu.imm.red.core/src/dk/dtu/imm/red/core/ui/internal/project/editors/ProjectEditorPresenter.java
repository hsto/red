package dk.dtu.imm.red.core.ui.internal.project.editors;

import java.util.List;

import dk.dtu.imm.red.core.project.ProjectDate;
import dk.dtu.imm.red.core.project.UserTableEntry;
import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;
import dk.dtu.imm.red.core.usecasepoint.Factor; 
import dk.dtu.imm.red.core.usecasepoint.WeightedFactor;

public interface ProjectEditorPresenter extends IEditorPresenter{

//	void setName(String text);

	void setUsers(List<UserTableEntry> users);

	void setDates(List<ProjectDate> dates);

	void setManagementFactorAspectValue(WeightedFactor mfa,
			int selectionIndex);

	void setManagementFactorAspectWeight(WeightedFactor mfa,
			double val);

	void setFactorValue(Factor f, double val);

	void createDefaultUsecasePoint();
}
