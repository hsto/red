package dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid;

import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;

import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;

public class EAGSumWidget extends Composite {
	private Text txtSumValue; 
	private Label lblSum;
	private Combo cmbSumColumn;
	private boolean enabled;
	private EAGPresenter<?> presenter;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public EAGSumWidget(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.marginHeight = 0;
		setLayout(gridLayout);
		
		GridData gdLayout = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdLayout.exclude = true; 
		this.setLayoutData(gdLayout);
		 
		lblSum = new Label(this, SWT.NONE);
		lblSum.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSum.setText("Sum");
		
		cmbSumColumn = new Combo(this, SWT.READ_ONLY);
		cmbSumColumn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		txtSumValue = new Text(this, SWT.BORDER | SWT.RIGHT);
		GridData gd_txtSumValue = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtSumValue.widthHint = 80;
		txtSumValue.setLayoutData(gd_txtSumValue);
		txtSumValue.setEditable(false);  
	}
	
	

	public void setPresenter(EAGPresenter<?> presenter) {
		this.presenter = presenter;
	}



	public void setEnabled(boolean enabled) { 
		
		this.enabled = enabled;
		((GridData)this.getLayoutData()).exclude = !enabled; 
		
		if(enabled) {
			cmbSumColumn.removeAll();
			
			HashMap<String, Integer> columnMap = new HashMap<String, Integer>(); 
			
			int i = 0;
			for(ElementColumnDefinition c : presenter.getColumnDefinitions()) {
				columnMap.put(c.getColumnName(), i);  
				cmbSumColumn.add(c.getColumnName()); 
			}  
		}  
	}
	
	public void displaySumForValues(List<Double> values) {
		try { 
			if(enabled) {
				int col = cmbSumColumn.getSelectionIndex();
				
				//Calculate sum
				if(col != -1) { 
					double sum = presenter.calculateSumForColumn(values);
					txtSumValue.setText(String.valueOf(sum));
				}
				else {
					txtSumValue.setText(String.valueOf(0));
				}
			} 
		}
		catch(Exception e) {
			
		} //Todo. Temp fix. Find out why canOpenSlection fails sometimes.
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public Text getTxtSumValue() {
		return txtSumValue;
	}

	public Label getLblSum() {
		return lblSum;
	}



	public Combo getCmbSumColumn() {
		return cmbSumColumn;
	} 
	
	public EAGPresenter<?> getPresenter() {
		return presenter;
	}

}
