package dk.dtu.imm.red.core.ui.internal.comment.views.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.Cell;
import org.agilemore.agilegrid.ColumnSortComparator;
import org.agilemore.agilegrid.ColumnSortOnClick;
import org.agilemore.agilegrid.DefaultCompositorStrategy;
import org.agilemore.agilegrid.ICompositorStrategy;
import org.agilemore.agilegrid.ISelectionChangedListener;
import org.agilemore.agilegrid.SWTX;
import org.agilemore.agilegrid.SelectionChangedEvent;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentCategory;
import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.comment.CommentPackage;
import dk.dtu.imm.red.core.ui.editors.IBaseEditor;
import dk.dtu.imm.red.core.ui.internal.Activator;
import dk.dtu.imm.red.core.ui.internal.PartAdapter;
import dk.dtu.imm.red.core.ui.internal.comment.views.CommentView;
import dk.dtu.imm.red.core.ui.internal.comment.views.CommentViewPresenter;
import dk.dtu.imm.red.core.ui.views.BaseView;

/**
 * View used for creating, displaying, editing and deleting comments.
 *
 * @author Anders Friis
 *
 */
public final class CommentViewImpl extends BaseView implements CommentView{

	/**
	 * Adapter which listens for changes in activated/openened and closed
	 * parts in the workspace. This changes the input of the comment view,
	 * depending on which editor is active.
	 * @author Jakob Kragelund
	 *
	 */
	private final class PartActivationAdapter extends PartAdapter {

		//Add code to fix Issue #10
		private BaseView view=null;
		public PartActivationAdapter(BaseView view){
			super();
			this.view = view;
		}
		//End Add code to fix Issue #10

		@Override
		public void partActivated(final IWorkbenchPartReference partRef) {

			//Add code to fix Issue #10
			if(!getViewSite().getPage().isPartVisible(view)){
				//return if the comment view is not visible
				return;
			}


			IEditorPart part = getSite().getPage().getActiveEditor();
			if(part instanceof IBaseEditor){
				IBaseEditor activeEditor = (IBaseEditor) part;
				if (activeEditor.getEditorElement() != null) {
					((CommentViewPresenter) presenter).inputChanged(activeEditor
							.getEditorElement());

					commentList = activeEditor.getEditorElement()
							.getCommentlist();
					commentTableContentProvider.setContent(commentList);
					commentTableLayoutAdviser.setRowCount(commentList
							.getComments().size());
					commentsTable.redraw();
					
					commentList.eAdapters().add(new AdapterImpl(){

						@Override
						public void notifyChanged(Notification msg) {
							if (msg.getFeature().equals(CommentPackage.eINSTANCE.getCommentList_Comments())) {
								commentTableLayoutAdviser.setRowCount(commentList.getComments().size());
								commentsTable.redraw();
							}
							super.notifyChanged(msg);
						}
					});
				}
			}
		}

		@Override
		public void partClosed(final IWorkbenchPartReference partRef) {

			//Add code to fix Issue #10
			if(!getViewSite().getPage().isPartVisible(view)){
				//return if the comment view is not visible
				return;
			}
			//End add code to fix Issue #10

			IWorkbenchPart activatedPart = partRef.getPart(false);
			if (activatedPart != null && activatedPart instanceof IBaseEditor) {
				IBaseEditor activatedEditor = (IBaseEditor) activatedPart;

				if (((CommentViewPresenter) presenter).getInput()
						== activatedEditor.getEditorElement()) {

					((CommentViewPresenter) presenter).inputChanged(null);
				}
			}
		}

	}

	protected void showLockedMessage() {
		MessageDialog.openError(null, "Element locked",
				"The element is locked for commenting.");

	}

	/**
	 * Column property names for the table viewer.
	 */
	private static final String DATE_COLUMN = "date";
	private static final String AUTHOR_COLUMN = "author";
	private static final String COMMENT_COLUMN = "comment";
	private static final String STATUS_COLUMN = "status";

	/**
	 * Array containing column property names.
	 */
	private final String[] columnNames = new String[] {
			DATE_COLUMN,
			AUTHOR_COLUMN,
			COMMENT_COLUMN,
			STATUS_COLUMN
	};

	/**
	 * The model element shown in the comment view.
	 */
	protected CommentList commentList;

	/**
	 * The label for comment view.
	 */
	protected Label commentViewLabel;

	/**
	 * The table that shows comments for the marked element.
	 */
	protected AgileGrid commentsTable;

	/**
	 * The tableviewer for the comments table.
	 */
	protected TableViewer commentsTableViewer;

	/**
	 * The button that deletes the comment(s) currently marked
	 * in the comment table.
	 */
	protected Button deleteCommentButton;

	/**
	 * The button to add a new comment to the comment table for
	 * the marked element.
	 */
	protected Button addCommentButton;

	/**
	 * The button that opens the help menu for comments.
	 */
	protected Button helpButton;

	/**
	 * The composite containing view elements.
	 */
	protected Composite composite;

	protected CommentTableCellEditorProvider commentTableCellEditorProvider;

	/**
	 * Content provider for the comment table.
	 */
	protected CommentTableContentProvider commentTableContentProvider;

	/**
	 * Layout adviser for the comment table.
	 */
	protected CommentTableLayoutAdviser commentTableLayoutAdviser;

	/**
	 * Instantiates necessary objects for this view, such as the presenter.
	 */
	public CommentViewImpl() {
		presenter = new CommentViewPresenterImpl(this);
	}

	/**
	 * Create the comment table with layout.
	 * @param parent parent container for the comment table
	 */
	private void createTable(final Composite parent) {
		Composite tcContainer = new Composite(parent, SWT.NONE);

		FillLayout tcLayout = new FillLayout();
		tcContainer.setLayout(tcLayout);

		GridData tableGridData = new GridData();
		tableGridData.horizontalSpan = 3;
		tableGridData.widthHint = 400;
		tableGridData.heightHint = 250;
		tableGridData.horizontalAlignment = SWT.FILL;
		tableGridData.grabExcessHorizontalSpace = true;

		tcContainer.setLayoutData(tableGridData);

		commentsTable = new AgileGrid(tcContainer,
				SWT.MULTI | SWTX.ROW_SELECTION | SWT.V_SCROLL);

		commentTableLayoutAdviser =
				new CommentTableLayoutAdviser(commentsTable);
		final ICompositorStrategy compositorStrategy =
				new DefaultCompositorStrategy(commentTableLayoutAdviser);
		commentTableLayoutAdviser.setCompositorStrategy(compositorStrategy);


		commentTableContentProvider = new CommentTableContentProvider();
		commentTableCellEditorProvider =
				new CommentTableCellEditorProvider(commentsTable, this);

		commentsTable.setLayoutAdvisor(commentTableLayoutAdviser);
		commentsTable.setContentProvider(commentTableContentProvider);
		commentsTable.setCellEditorProvider(commentTableCellEditorProvider);
		commentsTable.addMouseListener(new ColumnSortOnClick(commentsTable,
				new CommentTableSortComparator(commentsTable, Integer.MIN_VALUE,
						ColumnSortComparator.SORT_NONE)));
		commentTableContentProvider.addPropertyChangeListener(
				(CommentViewPresenter) presenter);
		commentsTable.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				if (commentsTable.getCellSelection().length > 0) {
					deleteCommentButton.setEnabled(true);
				} else {
					deleteCommentButton.setEnabled(false);
				}
			}
		});

	}

	@Override
	public void createPartControl(Composite parent) {
		composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		Composite tableContainer = new Composite(composite, SWT.NONE);
		GridData tableGridData = new GridData();
		tableGridData.grabExcessHorizontalSpace = true;
		tableGridData.horizontalAlignment = SWT.FILL;
		tableContainer.setLayoutData(tableGridData);

		GridLayout tableLayout = new GridLayout(2, false);
		tableContainer.setLayout(tableLayout);

		commentViewLabel = new Label(tableContainer, SWT.NONE);
		GridData commentLabelGridData = new GridData();
		commentLabelGridData.horizontalSpan = 1;
		commentLabelGridData.grabExcessHorizontalSpace = true;
		commentLabelGridData.minimumWidth = 250;
		commentViewLabel.setText("No element chosen");
		commentViewLabel.setLayoutData(commentLabelGridData);

		helpButton = new Button(tableContainer, SWT.PUSH);
		GridData helpButtonGridData = new GridData();
		helpButtonGridData.horizontalSpan = 1;
		helpButtonGridData.horizontalAlignment = SWT.RIGHT;
		helpButtonGridData.verticalAlignment = SWT.TOP;
		helpButtonGridData.grabExcessHorizontalSpace = true;
		helpButton.setLayoutData(helpButtonGridData);
		helpButton.setImage(Activator.getDefault().getImageRegistry()
				.get(Activator.HELP_ICON));
		helpButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				PlatformUI.getWorkbench().getHelpSystem()
					.displayHelpResource(getHelpResourceURL());
			}
		});

		createTable(tableContainer);

		Composite buttonContainer = new Composite(composite, SWT.NONE);
		GridData buttonGridData = new GridData();
		buttonGridData.horizontalSpan = 2;
		buttonContainer.setLayoutData(buttonGridData);

		GridLayout buttonLayout = new GridLayout(3, false);
		buttonContainer.setLayout(buttonLayout);


		addCommentButton = new Button(buttonContainer, SWT.NONE);
		addCommentButton.setText("Add Comment");
		addCommentButton.setEnabled(false);
		addCommentButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {

				if (((CommentViewPresenter) presenter).isLocked()) {
					showLockedMessage();
					return;
				}

				((CommentViewPresenter) presenter).addComment();
				commentTableLayoutAdviser.setRowCount(
						commentList.getComments().size());
				commentsTable.redraw();
			}
		});

		deleteCommentButton = new Button(buttonContainer, SWT.NONE);
		deleteCommentButton.setText("Delete");
		deleteCommentButton.setEnabled(false);
		deleteCommentButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {

				if (((CommentViewPresenter) presenter).isLocked()) {
					showLockedMessage();
					return;
				}

				Cell[] selectedCells = commentsTable.getCellSelection();
				Set<Comment> selectedComments = new HashSet<Comment>();

				for (Cell cell : selectedCells) {
					selectedComments.add(
							commentList.getComments().get(cell.row));
				}

				((CommentViewPresenter) presenter)
				.deleteComment(selectedComments);

				commentTableLayoutAdviser.setRowCount(
						commentList.getComments().size());
				commentsTable.clearSelection();
				deleteCommentButton.setEnabled(false);
				commentsTable.redraw();
			}
		});

		super.createPartControl(parent);

		if (getSite().getPage() != null) {
			//Modify code to fix Issue #10
			getSite().getPage().addPartListener(new PartActivationAdapter(this));
			//End Modify code to fix Issue #10

		}
	}

	protected String getHelpResourceURL() {
		return "/dk.dtu.imm.red.help/html/gettingstarted/Comments.html";
	}

	public List<String> getColumnNames() {
		return Arrays.asList(columnNames);
	}

	public List<String> getStatusTypes() {
		CommentCategory[] commentCategories = CommentCategory.values();
		String[] commentCategoryLiterals = new String[commentCategories.length];
		for (int i = 0; i < commentCategories.length; i++) {
			commentCategoryLiterals[i] = commentCategories[i].getLiteral();
		}
		return Arrays.asList(commentCategoryLiterals);
	}

	public void updateCommentsTable(Comment comment) {
		commentsTableViewer.update(comment, null);
	}

	@Override
	public void setCommentLabel(String text) {
		commentViewLabel.setText(text);
	}

	@Override
	public void setButtonsEnabled(boolean enabled) {
		addCommentButton.setEnabled(enabled);
		if (!enabled) {
			deleteCommentButton.setEnabled(false);
		}
	}

	@Override
	public void clearTable() {
		commentTableContentProvider.setContent(null);
		commentTableLayoutAdviser.setRowCount(0);
		commentsTable.redraw();
	}

	protected boolean isLocked() {
		return ((CommentViewPresenter) presenter).isLocked();
	}

}
