package dk.dtu.imm.red.core.ui.internal.element.dialog;

import java.util.List;

import dk.dtu.imm.red.core.comment.IssueComment;
import dk.dtu.imm.red.core.comment.IssueSeverity;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGPresenter;

public class CheckingToolEAGPresenter extends EAGPresenter<IssueComment> {
	
	private IssueSeverity minimumSeverity = IssueSeverity.OK;
	
	@Override
	public void DeleteItem(List<Integer> selectedIndexes) {
		List<Object> items = getSelectedItems(selectedIndexes);
		
		for (Object item : items) {
			getDataSource().remove(item);
			getDataSourceView().remove(item);
			
			if (item instanceof Element) {
				Element element = (Element) item;
				
				if (element.eContainer() instanceof Element) {
					Element parent = (Element) element.eContainer();

					Object containerObject = parent.eGet(element.eContainmentFeature());
					
					if (containerObject instanceof List<?>) {
						((List<?>) containerObject).remove(element);
						parent.save();
					}
				}
			}
		}
	}
	
	public void setMinimumSeverity(IssueSeverity minimumSeverity) {
		this.minimumSeverity = minimumSeverity;
		
		updateDataSourceFilter();
	}

	@Override
	public void getFilterDataSourceRec(List<IssueComment> inp, List<IssueComment> currentFilteredList) {
		for (IssueComment issue : inp) {
			if (minimumSeverity.getValue() <= issue.getSeverity().getValue()) {
				currentFilteredList.add(issue);
			}
		}
	}
}
