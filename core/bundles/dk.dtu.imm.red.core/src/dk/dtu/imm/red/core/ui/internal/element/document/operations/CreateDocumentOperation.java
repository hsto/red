package dk.dtu.imm.red.core.ui.internal.element.document.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.element.document.DocumentFactory;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.element.extensions.CoreElementExtension;
import dk.dtu.imm.red.core.ui.internal.preferences.PreferenceUtil;

public class CreateDocumentOperation extends AbstractOperation{

	protected String label;
	protected Group parent;
//	protected String path;
	protected String name;
	protected String description;
	protected CoreElementExtension extension;

	protected Document document;

	public CreateDocumentOperation(String label, String name, String description, Group parent, String path){
		super("Create Document");

		this.label = label;
		this.name = name;
		this.description=description;
		this.parent = parent;
//		this.path = path;

		extension = new CoreElementExtension();
	}

	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}

	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		document = DocumentFactory.eINSTANCE.createDocument();
		document.setCreator(PreferenceUtil.getUserPreference_User());
		document.setLabel(label);
		document.setName(name);
		document.setDescription(description);
		if (parent != null) {
			document.setParent(parent);
		}

		document.save();
		extension.openElement(document);

		return Status.OK_STATUS;
	}

	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		if(parent != null) {
			parent.getContents().remove(document);
			extension.deleteElement(document);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}
}
