package dk.dtu.imm.red.core.ui.internal.file.wizards.impl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.operations.IWorkbenchOperationSupport;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.file.operations.NewFileOperation;
import dk.dtu.imm.red.core.ui.internal.file.wizards.CreateFileWizardPresenter;
import dk.dtu.imm.red.core.ui.wizards.BaseWizardPresenter;

public class CreateFileWizardPresenterImpl extends BaseWizardPresenter
		implements CreateFileWizardPresenter {

	@Override
	public void wizardFinished(final String name, final Group parent, final String path) {
		IWorkbenchOperationSupport operationSupport = PlatformUI.getWorkbench()
				.getOperationSupport();
		
		NewFileOperation operation = 
				new NewFileOperation(name, parent, path);

		operation.addContext(operationSupport.getUndoContext());
		
		IAdaptable info = new IAdaptable() {
			@Override
			public Object getAdapter(final Class adapter) {
				if (Shell.class.equals(adapter)) {
					return Display.getCurrent().getActiveShell();
				}
				return null;
			}
		};

		try {
			OperationHistoryFactory.getOperationHistory().execute(
					operation, null, info);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}

	}
}
