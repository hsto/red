package dk.dtu.imm.red.core.ui.internal.element.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.core.ui.editors.IBaseEditor;
import dk.dtu.imm.red.core.ui.internal.element.operations.RenameElementOperation;

public class RenameElementHandler extends AbstractHandler implements IHandler{

	
	public RenameElementHandler() {
		super();
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		ISelection selectedObject = HandlerUtil.getCurrentSelection(event);
		if (selectedObject instanceof IStructuredSelection) {
			
			IStructuredSelection selection = (IStructuredSelection) selectedObject;
			Element selectedElement = (Element) selection.getFirstElement();
			
			BaseEditorInput input = new BaseEditorInput(selectedElement);
            
			IEditorPart editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage().findEditor(input);
            
			if (editor != null && editor.isDirty()){
				MessageDialog saveDialog = new MessageDialog(Display
				.getCurrent().getActiveShell(), "Save "+selectedElement.getName() +"?", 
				null, "The element must be saved before renaming." +
				" Save "+selectedElement.getName() +"?", MessageDialog.QUESTION, 
				new String[] { "Yes", "No" }, 1);
				
				if (saveDialog.open() != Window.OK){
					return null;
				}else {
					editor.doSave(new NullProgressMonitor());
				}
			}
			

			if(selectedElement!=null){
				RenameElementOperation operation =
						new RenameElementOperation(selectedElement);
				operation.addContext(PlatformUI.getWorkbench().getOperationSupport()
						.getUndoContext());
				OperationHistoryFactory.getOperationHistory().execute(operation, null, null);
				if (editor != null){
					((IBaseEditor) editor).refresh();
				}
			}
		}
		
		return null;
	}

}
