package dk.dtu.imm.red.core.ui.internal.comment.views;

import java.beans.PropertyChangeListener;
import java.util.Set;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.views.IViewPresenter;

public interface CommentViewPresenter extends IViewPresenter, PropertyChangeListener {
	
	void inputChanged(Element elementShown);

	void addComment();

	void deleteComment(Set<Comment> selectedComments);
	
	Element getInput();

	boolean isLocked();

}
