package dk.dtu.imm.red.core.ui.editors;

import dk.dtu.imm.red.core.comment.CommentList;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.properties.Property;
import dk.dtu.imm.red.core.ui.editors.IBaseEditor.Dependency;
import dk.dtu.imm.red.core.ui.presenters.IPresenter;

public interface IEditorPresenter extends IPresenter {

	void save();
	void saveFileData();
	void dispose();

	void setAuthor(String name);

	void setResponsibleUser(String name);

	void setDeadline(String deadline);

	void setVersion(String version);

	void setState(String state);
	
	void setLifeCyclePhase(String lifeCyclePhase);
	
	void setManagementDecision(String managementDecision);
	
	void setQAAssessment(String qaAssessment);

	void setPriority(String state);

	void setWorkpackage(String state);

	void setChangelog(CommentList changelog);

	int getAuthorMaxLength();

	int getResponsibleUserMaxLength();

	int getWorkpackageMaxLength();

	int getVersionMaxLength();

	int getDeadlineMaxLength();

	void setLockType(LockType lockType);

	void setSpecElementBasicInfo(String label, String name, String kind, String description);

	void setElementIconURI(String URI);
	
	// #231
	void addDependency(BaseEditor editor);
	void deleteDependency(BaseEditor editor, Dependency dependency, boolean relatedBy);
	void openDependencyElement(BaseEditor editor, Dependency dependency);
	
	void setCost(int cost);
	void setBenefit(int benefit);
	void setRisk(int risk);
}
