package dk.dtu.imm.red.core.ui.internal.search.handlers;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.element.ElementFactory;
import dk.dtu.imm.red.core.element.SearchParameter;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.internal.search.views.SearchDialog;
import dk.dtu.imm.red.core.ui.internal.search.views.SearchView;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class OpenSearchDialog extends AbstractHandler implements IHandler {
	
	private static IProgressMonitor monitor;
	
	public static boolean isSearchCanceled() {
		if (monitor != null) {
			return monitor.isCanceled();
		} return false;
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		SearchDialog dialog = new SearchDialog(HandlerUtil.getActiveShell(event));
		
		int retValue = dialog.open();
		if (retValue == Dialog.CANCEL) {
			return null;
		}
		
		final SearchParameter param = ElementFactory.eINSTANCE.createSearchParameter();
		param.setSearchString(dialog.getSearchString());
		if (param.getSearchString().isEmpty()) {
			return null;
		}
		
		final Display display = Display.getCurrent();
		
		ProgressMonitorDialog progressDialog = 
				new ProgressMonitorDialog(Display.getDefault().getActiveShell());
		try {
			progressDialog.run(true, true, new IRunnableWithProgress() {
				
				@Override
				public void run(IProgressMonitor monitor) {
					monitor.beginTask("Search", IProgressMonitor.UNKNOWN);
					OpenSearchDialog.monitor = monitor;
					final SearchResultWrapper result = WorkspaceFactory.eINSTANCE
							.getWorkspaceInstance().search(param);
					display.asyncExec(new Runnable() {
						@Override
						public void run() {
							IEditorReference[] editors = PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getActivePage().getEditorReferences();
							SearchResultWrapper[] editorResults = new SearchResultWrapper[editors.length];
							int i = 0;
							for (IEditorReference editorRef : editors) {
								IEditorPart editor = editorRef.getEditor(false);
								if (editor instanceof BaseEditor) {
									editorResults[i] = ((BaseEditor) editor).search(param);
									i++;
								}
							}
							
							SearchView searchView = null;
							try {
								searchView = (SearchView) PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow().getActivePage()
										.showView(SearchView.ID);
								searchView.setInput(result);
							} catch (PartInitException e) {
								// TODO Auto-generated catch block
								e.printStackTrace(); LogUtil.logError(e);
							}
							
							
						}
					});
					OpenSearchDialog.monitor = null;
					monitor.done();
					
				}
			});
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); LogUtil.logError(e);
		}
		
		return null;
	}
}
