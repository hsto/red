package dk.dtu.imm.red.core.ui.internal.comment.views.impl;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ScalableColumnLayoutAdvisor;

public class CommentTableLayoutAdviser extends ScalableColumnLayoutAdvisor {

	protected static final int STATUS_COLUMN = 3;
	protected static final int COMMENT_COLUMN = 2;
	protected static final int AUTHOR_COLUMN = 1;
	protected static final int DATE_COLUMN = 0;
	protected static final int NUM_COLUMNS = 4;
	
	private int rowCount;

	public CommentTableLayoutAdviser(AgileGrid agileGrid) {
		super(agileGrid);
		
		rowCount = 0;
	}
	
	@Override
	public String getTooltip(int row, int col) {
		Object object = agileGrid.getContentAt(row, col);
		
		if (object != null) {
			return object.toString();
		} else {
			return null;
		}
	}
	
	@Override
	public int getColumnCount() {
		// Date, author, comment and status
		return NUM_COLUMNS;
	}
	
	@Override
	public String getTopHeaderLabel(final int col) {
		switch (col) {
			case DATE_COLUMN:
				return "Date";
			case AUTHOR_COLUMN:
				return "Author";
			case COMMENT_COLUMN:
				return "Comment";
			case STATUS_COLUMN:
				return "Status";
			default:
				return super.getTopHeaderLabel(col);
		}
	}
	
	@Override
	public int getRowCount() {
		return rowCount;
	}
	
	@Override
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
}
