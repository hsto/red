package dk.dtu.imm.red.core.ui.internal.element.handlers;


import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.handlers.HandlerUtil;

import dk.dtu.imm.red.core.ui.internal.element.dialog.CheckingToolDialog;
 

public class CheckingToolHandler extends AbstractHandler implements IHandler {
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		CheckingToolDialog dialog = new CheckingToolDialog(HandlerUtil.getActiveShell(event));
		
		System.out.println("Opened dialog");

		dialog.create();
		dialog.open();
		
		return null;
	}
}
