package dk.dtu.imm.red.core.ui.internal.comment.views.impl;

import java.beans.PropertyChangeEvent;
import java.util.Date;
import java.util.Set;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.CommentCategory;
import dk.dtu.imm.red.core.comment.CommentFactory;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.text.TextFactory;
import dk.dtu.imm.red.core.ui.internal.comment.views.CommentView;
import dk.dtu.imm.red.core.ui.internal.comment.views.CommentViewPresenter;
import dk.dtu.imm.red.core.ui.views.BaseViewPresenter;
import dk.dtu.imm.red.core.user.User;
import dk.dtu.imm.red.core.user.UserFactory;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

/**
 * The presenter for the comment view.
 * @author Friis 
 */

public final class CommentViewPresenterImpl extends BaseViewPresenter 
	implements CommentViewPresenter {

	/**
	 * the view of this presenter.
	 */
	protected CommentView view;
	protected Element shownElement;

	public CommentViewPresenterImpl(final CommentView view) {
		super();
		this.view = view;
	}

	@Override
	public void inputChanged(Element elementShown) {
		
		this.shownElement = elementShown;
		
		if (elementShown == null) {
			view.setCommentLabel("No element chosen");
			view.setButtonsEnabled(false);
			view.clearTable();
		} else {
			view.setCommentLabel("Comments for " + elementShown.getName());
			view.setButtonsEnabled(true);
		}
	}

	@Override
	public void addComment() {
		Comment comment = CommentFactory.eINSTANCE.createComment();
		
		comment.setTimeCreated(new Date());
		
		User user = UserFactory.eINSTANCE.createUser();
		comment.setAuthor(user);
		
		comment.setText(TextFactory.eINSTANCE.createText());
		
		comment.setCategory(CommentCategory.UNRESOLVED);
		
		shownElement.getCommentlist().getComments().add(comment);
		
		shownElement.save();
	}
	
	@Override
	public void deleteComment(final Set<Comment> selectedComments) {
		shownElement.getCommentlist().getComments().removeAll(selectedComments);
		
		shownElement.save();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (shownElement != null) {
			shownElement.save();
		}
	}
	
	@Override
	public Element getInput() {
		return shownElement;
	}
	
	@Override
	public boolean isLocked() {
		LockType lock = shownElement.getLockStatus();
		String elementPassword = shownElement.getLockPassword();
		String workspacePassword = WorkspaceFactory.eINSTANCE
				.getWorkspaceInstance().getPassword();
		
		if (lock == LockType.LOCK_CONTENTS) {
			if (elementPassword != null 
					&& elementPassword.equals(workspacePassword) == false) {
				return true;
			}
		}
		return false;
	}
	

}
