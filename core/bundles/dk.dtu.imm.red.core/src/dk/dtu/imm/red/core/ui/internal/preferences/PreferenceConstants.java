package dk.dtu.imm.red.core.ui.internal.preferences;

public class PreferenceConstants {
	    public static final String USER_PREF_FIRST_NAME = "preferences.user.firstname";
	    public static final String USER_PREF_MIDDLE_NAME = "preferences.user.middlename";
	    public static final String USER_PREF_LAST_NAME = "preferences.user.lastname";
	    public static final String USER_PREF_INITIALS = "preferences.user.initials";
	    public static final String USER_PREF_ID = "preferences.user.id";
	    public static final String USER_PREF_EMAIL = "preferences.user.email";
	    public static final String USER_PREF_ROLE = "preferences.user.role";
	    public static final String USER_PREF_REVIEW_COLOR = "preferences.user.reviewcolor";
	    public static final String USER_PREF_PASSWORD = "preferences.user.password";

	    public static final String PROJECT_PREF_LABELS = "preferences.project.labels";
	    public static final String PROJECT_PREF_LOG_LEVEL = "preferences.project.loglevel";
	    public static final String PROJECT_PREF_WORKSPACE_PATH = "preferences.project.workspacepath";
	    public static final String PROJECT_PREF_LOG_PATH = "preferences.project.logpath";
	    public static final String PROJECT_PREF_AUTOSAVE = "preferences.project.autosave";
	    
	    public static final String PROJECT_PREF_AUTOSAVE_OFF = "0";
	    public static final String PROJECT_PREF_AUTOSAVE_2MIN = "2";
	    public static final String PROJECT_PREF_AUTOSAVE_5MIN = "5";
	    public static final String PROJECT_PREF_AUTOSAVE_10MIN = "10";
	    public static final String PROJECT_PREF_AUTOSAVE_20MIN = "20";
	    public static final String PROJECT_PREF_AUTOSAVE_40MIN = "40";
	    public static final String PROJECT_PREF_AUTOSAVE_60MIN = "60";

	    //these are array type
	    public static final String SERVERS_PREF_NAME = "preferences.servers.name";
	    public static final String SERVERS_PREF_SERVICES = "preferences.servers.service";
	    public static final String SERVERS_PREF_SERVER = "preferences.servers.server";
	    public static final String SERVERS_PREF_USERNAME = "preferences.servers.username";
	    public static final String SERVERS_PREF_PASSWORD = "preferences.servers.password";
	    public static final String SERVERS_PREF_EXTRA_PARAMS = "preferences.servers.extraparams";
	    public static final String SERVERS_PREF_ARRAYSIZE = "preferences.servers.arraysize";

	    public static final String STITCH_PREF_EDIT_DISTANCE_THRESHOLD = "preferences.stitch.threshold";
	    public static final String STITCH_PREF_AUTO_LAYOUT= "preferences.stitch.autolayout";
	    public static final String STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS = "preferences.stitch.cascadeupdate";
	    public static final String STITCH_PREF_ALLOW_CONFLICTING_SUBELEMENTS = "preferences.stitch.allowconflictsubelements";
	    public static final String STITCH_PREF_MERGE_LOG_LEVEL = "preferences.stitch.mergeloglevel";
	    public static final String STITCH_PREF_SEPARATE_MULTISTITCH = "preferences.stitch.separatemultistitch";
	    public static final String STITCH_PREF_SEPARATE_MULTISTITCH_BYPACKAGE = "ByPackage";
	    public static final String STITCH_PREF_SEPARATE_MULTISTITCH_BYDIAGRAMTYPE = "ByDiagramType";
	    public static final String STITCH_PREF_STITCHING_SEQUENCE = "preferences.stitch.stitchingsequence";
	    public static final String STITCH_PREF_STITCHING_SEQUENCE_SEQUENTIAL = "sequential";
	    public static final String STITCH_PREF_STITCHING_SEQUENCE_TREE = "tree";

}
