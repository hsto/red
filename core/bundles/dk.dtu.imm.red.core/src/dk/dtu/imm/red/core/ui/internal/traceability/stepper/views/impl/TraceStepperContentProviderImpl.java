package dk.dtu.imm.red.core.ui.internal.traceability.stepper.views.impl;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.agilemore.agilegrid.DefaultContentProvider;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.impl.CellHandler;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition;
import dk.dtu.imm.red.core.element.impl.ElementColumnDefinition.ColumnBehaviour;
import dk.dtu.imm.red.core.element.relationship.ElementReference;
import dk.dtu.imm.red.core.element.relationship.ElementRelationship;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizard;
import dk.dtu.imm.red.core.ui.element.wizards.CreateElementReferenceWizardPresenter;
import dk.dtu.imm.red.core.ui.element.wizards.impl.CreateElementReferenceWizardImpl;
import dk.dtu.imm.red.core.ui.internal.Activator;
import dk.dtu.imm.red.core.ui.internal.traceability.stepper.views.TraceStepperContentProvider;
import dk.dtu.imm.red.core.ui.internal.traceability.util.SpecificationElementType;
import dk.dtu.imm.red.core.ui.internal.traceability.util.TraceabilityQueryEngine;

public class TraceStepperContentProviderImpl extends DefaultContentProvider implements TraceStepperContentProvider {

	private HashMap<String, ElementReference> targetRelationships, sourceRelationships;
	private Element focusElement;
	private String elementFilter = "", relationFilter = "";
	private boolean elementShow = true, relationshipShow = true;
	private TraceabilityQueryEngine engine;

	public TraceStepperContentProviderImpl() {
		engine = new TraceabilityQueryEngine();
	}

	@Override
	public void setContent(EList<ElementRelationship> relationships) {
	}

	@Override
	public Element getContent() {
		CreateElementReferenceWizard wizard = new CreateElementReferenceWizardImpl(null, "Select element",
				"Select the element from the workspace which you want to get shown in the traceability stepper.");
		wizard.init(PlatformUI.getWorkbench(), (IStructuredSelection) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService().getSelection());

		WizardDialog dialog = new WizardDialog(Display.getCurrent().getActiveShell(), wizard);
		dialog.open();

		Element selectedElement = ((CreateElementReferenceWizardPresenter) wizard.getPresenter()).getselectedElement();
		return selectedElement;
	}

	public String[] getRelationshipKindList() {
		List<String> list = new ArrayList<String>();

		for (RelationshipKind kind : RelationshipKind.values()) {
			list.add(kind.getLiteral());
		}

		return list.toArray(new String[0]);
	}

	public String[] getElementKindList() {
		List<String> list = new ArrayList<String>();

		for (SpecificationElementType kind : SpecificationElementType.values()) {
			list.add(kind.getLiteral());
		}

		return list.toArray(new String[0]);
	}

	public List<Element> getDataForSourceTable() {

		// Workaround to avoid rewriting the whole filter algorithm
		List<ElementRelationship> list = new ArrayList<>();
		List<Element> resourceData = new ArrayList<>();
		EList<ElementRelationship> origin;

		sourceRelationships = new HashMap<>();

		if (focusElement != null) {
			origin = focusElement.getRelatedBy();
			for (ElementRelationship relationship : origin) {
				list.add(relationship);
			}
			for (ElementRelationship relationship : focusElement.getRelatesTo()) {
				if (RelationshipKind.getSymmetricalKinds()
						.contains(((ElementReference) relationship).getRelationshipKind()))
					list.add(relationship);
			}

			list = applyFilter(list, new ArrayList<ElementRelationship>(), true);

			for (ElementRelationship relationship : list) {

				if (RelationshipKind.getSymmetricalKinds()
						.contains(((ElementReference) relationship).getRelationshipKind())
						&& relationship.getFromElement().equals(focusElement)) {
					if (relationship.getToElement() == null)
						continue;
					sourceRelationships.put(relationship.getToElement().getElementHashId(),
							(ElementReference) relationship);
					resourceData.add(relationship.getToElement());

				} else {
					sourceRelationships.put(relationship.getFromElement().getElementHashId(),
							(ElementReference) relationship);
					resourceData.add(relationship.getFromElement());
				}
			}
		}

		return resourceData;

	}

	public List<Element> getDataForTargetTable() {
		List<ElementRelationship> list = new ArrayList<>();
		List<Element> resourceData = new ArrayList<>();
		EList<ElementRelationship> origin;

		targetRelationships = new HashMap<>();

		try {
			if (focusElement != null) {
				origin = focusElement.getRelatesTo();
				for (ElementRelationship relationship : origin) {
					if (!RelationshipKind.getSymmetricalKinds()
							.contains(((ElementReference) relationship).getRelationshipKind()))
						list.add(relationship);
				}

				list = applyFilter(list, new ArrayList<ElementRelationship>(), false);

				for (ElementRelationship relationship : list) {
					targetRelationships.put(relationship.getToElement().getElementHashId(),
							(ElementReference) relationship);
					resourceData.add(relationship.getToElement());
				}
			}

			return resourceData;
		} catch (NullPointerException e) {

			return resourceData;
		}
	}

	public void setFocusElement(Element focusElement) {
		this.focusElement = focusElement;
	}

	public Element getFocusElement() {
		return focusElement;
	}

	public List<ElementRelationship> applyFilter(List<ElementRelationship> unfilteredRelationships,
			List<ElementRelationship> filteredRelationships, boolean relatedBy) {

		ElementReference reference;
		Date versionTimestamp = null;

		// Return if list is empty
		if (unfilteredRelationships.isEmpty()) {
			return filteredRelationships;
		} else {
			// Check if filter is set, otherwise skip filtering
			if (elementFilter.isEmpty() && relationFilter.isEmpty() && versionTimestamp == null) {
				return unfilteredRelationships;
			} else {

				// Initialize new element to check
				reference = (ElementReference) unfilteredRelationships.get(0);
				unfilteredRelationships.remove(0);

				// Check for timestamp if check is enabled
				if (versionTimestamp == null
						|| (versionTimestamp != null && reference.getTimeCreated().before(versionTimestamp))) {

					if (elementFilter.isEmpty() && relationFilter.isEmpty()) {
						filteredRelationships.add(reference);

					} else {

						// Filter for Element class type
						if (!elementFilter.isEmpty()) {

							// Check which element is relevant for the filter
							if (relatedBy) {

								// Check if element is to hide or to show
								if (elementShow) {
									if (reference.getFromElement().eClass().getName().equals(elementFilter)) {
										filteredRelationships.add(reference);
										// Check for symmetrical relationship
										// types
									} else if (RelationshipKind.getSymmetricalKinds()
											.contains((reference.getRelationshipKind()))
											&& (reference.getFromElement().equals(focusElement))) {
										if ((reference.getToElement() != null) && (reference.getToElement().eClass()
												.getName().equals(elementFilter))) {
											filteredRelationships.add(reference);
										}
									}
								} else {
									if ((!reference.getFromElement().eClass().getName().equals(elementFilter))
											&& !(RelationshipKind.getSymmetricalKinds()
													.contains((reference.getRelationshipKind()))
													&& (reference.getFromElement().equals(focusElement)))) {
										filteredRelationships.add(reference);

									} else if (RelationshipKind.getSymmetricalKinds()
											.contains((reference.getRelationshipKind()))
											&& (reference.getFromElement().equals(focusElement))) {
										if ((reference.getToElement() != null) && (!reference.getToElement().eClass()
												.getName().equals(elementFilter))) {
											filteredRelationships.add(reference);
										}
									}
								}
							} else {
								if (elementShow) {
									if (reference.getToElement().eClass().getName().equals(elementFilter)) {
										filteredRelationships.add(reference);
									}
								} else {
									if (!reference.getToElement().eClass().getName().equals(elementFilter)) {
										filteredRelationships.add(reference);
									}
								}
							}
						}

						// Filter for ElementRelationship class type
						if (!relationFilter.isEmpty()) {

							// If filter is supposed to show element, add it to
							// the
							// filtered list
							if (relationshipShow) {
								// If not relatedBy, get reverse
								// RelationshipKind
								if (reference.getRelationshipKind().getLiteral().equals(relationFilter)) {
									filteredRelationships.add(reference);
								} else if (RelationshipKind
										.getReverseRelationshipKind(RelationshipKind.get(relationFilter)) != null
										&& reference.getRelationshipKind().getLiteral().equals(RelationshipKind
												.getReverseRelationshipKind(RelationshipKind.get(relationFilter))
												.getLiteral())) {
									filteredRelationships.add(reference);
								}
							} else { // Otherwise add every other element
								if (!reference.getRelationshipKind().getLiteral().equals(relationFilter)
										&& RelationshipKind.getReverseRelationshipKind(
												RelationshipKind.get(relationFilter)) != null
										&& !reference.getRelationshipKind().getLiteral().equals(RelationshipKind
												.getReverseRelationshipKind(RelationshipKind.get(relationFilter))
												.getLiteral())) {
									filteredRelationships.add(reference);
								}
							}
						}
					}
				}
			}
		}
		return

		applyFilter(unfilteredRelationships, filteredRelationships, relatedBy);
	}

	@Override
	public ElementColumnDefinition[] getToColumnDefinition() {
		return new ElementColumnDefinition[] {
				new ElementColumnDefinition().setHeaderName("Element").setColumnType(String.class).setColumnWidth(70)
						.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
						.setGetterMethod(new String[] { "getName" })
						.setReferenceGetterMethod(new String[] { "eContainer", "eContainer" }),

				// Type column
				new ElementColumnDefinition().setHeaderName("Type").setColumnType(String.class).setColumnWidth(15)
						.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
						.setCellHandler(new CellHandler<Element>() {

							@Override
							public Object getAttributeValue(Element item) {
								if (item != null) {
									return item.eClass().getName();
								} else {
									return "";
								}
							}

							@Override
							public void setAttributeValue(Element item, Object value) {
								// This column cannot be edited
							}
						}),

				new ElementColumnDefinition().setHeaderName("Relationship").setColumnType(String.class)
						.setColumnWidth(35).setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
						.setCellHandler(new CellHandler<Element>() {

							@Override
							public Object getAttributeValue(Element item) {
								if (item != null) {
									return targetRelationships.get(item.getElementHashId()).getRelationshipKind()
											.getLiteral();
								} else
									return null;
							}

							@Override
							public void setAttributeValue(Element item, Object value) {
								// not editable

							}
						}) };
	}

	@Override
	public ElementColumnDefinition[] getFromColumnDefinition() {

		return new ElementColumnDefinition[] {
				new ElementColumnDefinition().setHeaderName("Element").setColumnType(String.class).setColumnWidth(70)
						.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
						.setGetterMethod(new String[] { "getName" })
						.setReferenceGetterMethod(new String[] { "eContainer", "eContainer" }),

				// Type column
				new ElementColumnDefinition().setHeaderName("Type").setColumnType(String.class).setColumnWidth(15)
						.setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
						.setCellHandler(new CellHandler<Element>() {

							@Override
							public Object getAttributeValue(Element item) {
								if (item != null) {
									return item.eClass().getName();
								} else {
									return "";
								}
							}

							@Override
							public void setAttributeValue(Element item, Object value) {
								// This column cannot be edited
							}
						}),

				new ElementColumnDefinition().setHeaderName("Relationship").setColumnType(String.class)
						.setColumnWidth(35).setColumnBehaviour(ColumnBehaviour.ReferenceSelector)
						.setCellHandler(new CellHandler<Element>() {

							@Override
							public Object getAttributeValue(Element item) {
								if (item != null) {
									return RelationshipKind.getReverseRelationshipKind(
											sourceRelationships.get(item.getElementHashId()).getRelationshipKind())
											.getLiteral();
								} else
									return null;
							}

							@Override
							public void setAttributeValue(Element item, Object value) {
								// not editable

							}
						}) };
	}

	public void setElementFilter(String filter, boolean show) {
		elementFilter = filter;
		elementShow = show;
	}

	public void setRelationshipFilter(String filter, boolean show) {
		relationFilter = filter;
		relationshipShow = show;
	}
}
