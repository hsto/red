package dk.dtu.imm.red.core.ui.internal.traceability.stepper.views.impl;

import org.agilemore.agilegrid.Cell;
import org.agilemore.agilegrid.CellDoubleClickEvent;
import org.agilemore.agilegrid.ICellDoubleClickListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.EAGComposite;
import dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid.ExtendedAgileGrid;

public class TraceStepperTableEAGComposite extends EAGComposite<Element> {
	private TraceStepperViewImpl referencedView;

	public TraceStepperTableEAGComposite(Composite parent, int style, TraceStepperViewImpl referencedView) {
		super(parent, style, null);

		// Hide unneeded controls
		GridData hideGrid = new GridData(0, 0);
		hideGrid.exclude = true;
		btnAdd.setVisible(false);
		btnAdd.setLayoutData(hideGrid);
		chkRecursiveView.setVisible(false);
		chkRecursiveView.setLayoutData(hideGrid);
		composite_1.setVisible(false);
		composite_1.setLayoutData(hideGrid);

		btnDelete.setVisible(false);
		btnDelete.setLayoutData(hideGrid);
		btnOpen.setVisible(false);
		btnOpen.setLayoutData(hideGrid);
		
		buttonComposite.setVisible(false);
		buttonComposite.setLayoutData(hideGrid);

		this.referencedView = referencedView;
	}

	public void setSourceCellRightClickListener() {
		ExtendedAgileGrid<Element> eAgileGrid = getAgReflAgileGrid();
		eAgileGrid.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseDown(MouseEvent e) {
				// Detect right click
				if (e.button == 3) {
					// Filters clicks on empty table rows
					if (!agReflAgileGrid.getSelectedIndex().isEmpty()) {
						Element selection = presenter.getDataSourceView()
								.get(agReflAgileGrid.getSelectedIndex().get(0));
						referencedView.addElementToHistory(selection);
						referencedView.setFocusOnSearchBox();
					}
				}
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	public void setTargetCellRightClickListener() {
		ExtendedAgileGrid<Element> eAgileGrid = getAgReflAgileGrid();
		eAgileGrid.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseDown(MouseEvent e) {
				// Detect right click
				if (e.button == 3) {
					// Filters clicks on empty table rows
					if (!agReflAgileGrid.getSelectedIndex().isEmpty()) {
						Element selection = (Element) presenter.getDataSourceView()
								.get(agReflAgileGrid.getSelectedIndex().get(0));
						referencedView.addElementToHistory(selection);
						referencedView.setFocusOnSearchBox();
					}
				}
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}
}
