package dk.dtu.imm.red.core.ui.internal.navigation.implementation;

import java.util.List;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchActionConstants;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.ui.internal.navigation.ElementExplorer;
import dk.dtu.imm.red.core.ui.internal.widgets.filters.ElementExplorerFilter;
import dk.dtu.imm.red.core.ui.internal.widgets.sorters.AlphabeticElementSorter;
import dk.dtu.imm.red.core.ui.views.BaseView;
import dk.dtu.imm.red.core.ui.widgets.ElementTreeViewWidget;

/**
 * The Element Explorer provides a tree-view of the logical structure of the
 * element hierachy. The root of the tree (which is not shown in the view) is a
 * workspace.
 * 
 * @author Jakob Kragelund
 * 
 */
public class ElementExplorerImpl extends BaseView implements ElementExplorer {

	protected ElementTreeViewWidget treeView;
	protected ViewerSorter alphabeticElementSorter;
	protected ElementExplorerFilter elementFilter;
	protected Button sortButton;
	protected Text filterText;
	protected Label filterLabel;

	/**
	 * Creates an element explorer with a content- and labelprovider.
	 */
	public ElementExplorerImpl() {
		alphabeticElementSorter = new AlphabeticElementSorter();
		elementFilter = new ElementExplorerFilter();
	}

	@Override
	public void createPartControl(final Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(1, true);
		container.setLayout(layout);
		
		Composite topContainer = new Composite(container, SWT.NONE);
		topContainer.setLayout(new RowLayout(SWT.HORIZONTAL));
		sortButton = new Button(topContainer, SWT.CHECK);
		sortButton.setText("Sort");
		sortButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if(sortButton.getSelection()) {
					treeView.setSorter(alphabeticElementSorter);
				} else {
					treeView.setSorter(null);
				}
			}
		});
		
		new Label(topContainer, SWT.NONE).setText("          ");
		
		
		filterLabel = new Label(topContainer, SWT.NONE);
		filterLabel.setText("Filter:");
		filterText = new Text(topContainer, SWT.BORDER);
		filterText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				elementFilter.setFilterString(filterText.getText());
				treeView.getViewer().refresh();
			}
		});
		
		treeView = new ElementTreeViewWidget(false, container, SWT.MULTI
				| SWT.BORDER,false,true);
		treeView.getViewer().addFilter(elementFilter);

		treeView.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(final DoubleClickEvent event) {
				ITreeSelection selection = (ITreeSelection) treeView
						.getSelection();
				List<Element> selectedElements = selection.toList();

				((ElementExplorerPresenter) presenter)
					.elementDoubleClicked(selectedElements);
			}
		});
		
		// Set up drag and drop listeners
		int operations = DND.DROP_MOVE | DND.DROP_COPY;
		// Only drag supports the LocalSelectionTransfer
		Transfer[] dragTransferTypes = 
				new Transfer[] { TextTransfer.getInstance() , LocalSelectionTransfer.getTransfer()};
		treeView.addDragSupportListener(operations, dragTransferTypes,
				new ElementDragListener(treeView.getViewer()));
		Transfer[] dropTransferTypes = new Transfer[] {TextTransfer.getInstance()};
		treeView.addDropSupportListener(operations, dropTransferTypes, 
				new ElementDropListener(treeView.getViewer()));

		// Set up context menu
		MenuManager contextMenuManager = new MenuManager();
		contextMenuManager.add(new Separator(
				IWorkbenchActionConstants.MB_ADDITIONS));
		treeView.getTree().setMenu(
				contextMenuManager.createContextMenu(treeView.getTree()));

		getSite().registerContextMenu(contextMenuManager, treeView);
		getSite().setSelectionProvider(treeView);

		presenter = new ElementExplorerPresenter(this);

		super.createPartControl(parent);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
	
	public void refreshViewParts(){
		treeView.getViewer().refresh();		
	}
	
	public ElementTreeViewWidget getTreeView() {
		return treeView;
	}
	
}
