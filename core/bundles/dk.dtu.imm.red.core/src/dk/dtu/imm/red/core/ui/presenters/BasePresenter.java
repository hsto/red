package dk.dtu.imm.red.core.ui.presenters;

import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.ui.PlatformUI;

public abstract class BasePresenter implements IPresenter {

	/**
	 * The undo-context of the presenter.
	 */
	protected IUndoContext undoContext;

	/**
	 * Construct a basic presenter with a default undo context.
	 */
	public BasePresenter() {
		// Default undo-context is the global one
		undoContext = PlatformUI.getWorkbench().getOperationSupport()
				.getUndoContext();
	}

	/**
	 * Returns the undo context.
	 * 
	 * @return the presenter's undo context
	 */
	@Override
	public IUndoContext getUndoContext() {
		return undoContext;
	}

	/**
	 * Set the undo context.
	 * 
	 * @param newContext
	 *            the new undo context
	 */
	@Override
	public void setUndoContext(final IUndoContext newContext) {
		undoContext = newContext;
	}

}
