package dk.dtu.imm.red.core.ui.internal.element.document.wizards.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredSelection;

import dk.dtu.imm.red.core.element.document.Document;
import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.project.Project;
import dk.dtu.imm.red.core.ui.internal.element.document.wizards.NewDocumentWizard;
import dk.dtu.imm.red.core.ui.internal.element.document.wizards.NewDocumentWizardPresenter;
import dk.dtu.imm.red.core.ui.wizards.BaseNewWizard;
import dk.dtu.imm.red.core.ui.wizards.ElementBasicInfoPage;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class NewDocumentWizardImpl extends BaseNewWizard implements NewDocumentWizard{

//	protected ElementBasicInfoPage defineDocumentPage;

	protected NewDocumentWizardPresenter presenter;

	public NewDocumentWizardImpl() {
		super("Document", Document.class);
		presenter = new NewDocumentWizardPresenterImpl(this);
//		defineDocumentPage = new ElementBasicInfoPage("Document");
	}

	@Override
	public void addPages() {
//		addPage(defineDocumentPage);
		super.addPages();
	}

	@Override
	public boolean performFinish() {

		String label = elementBasicInfoPage.getLabel();
		String name = elementBasicInfoPage.getName();
		String description = elementBasicInfoPage.getDescription(); 

		Group parent = null;

		if (!displayInfoPage.getSelection().isEmpty() 
				&& displayInfoPage.getSelection() 
				instanceof IStructuredSelection) {
			IStructuredSelection selection = 
					(IStructuredSelection) displayInfoPage.getSelection();

			if (selection.getFirstElement() instanceof Group) {
				parent = (Group) selection.getFirstElement();
			}
		}

		presenter.wizardFinished(label, name, description, parent, "");

		return true;
	}

	@Override
	public boolean canFinish() {
		return super.canFinish() 
				&& elementBasicInfoPage.isPageComplete() 
				&& displayInfoPage.isPageComplete() 
				&& !displayInfoPage.getSelection().isEmpty();
	}
}
