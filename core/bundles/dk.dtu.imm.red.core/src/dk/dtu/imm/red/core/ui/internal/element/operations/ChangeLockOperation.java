package dk.dtu.imm.red.core.ui.internal.element.operations;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.LockType;
import dk.dtu.imm.red.core.workspace.WorkspaceFactory;

public class ChangeLockOperation extends AbstractOperation {

	protected LockType oldLock;
	protected LockType newLock;
	protected Element element;
	
	public ChangeLockOperation(LockType oldLock, LockType newLock, Element element) {
		super("Change lock");
		
		this.oldLock = oldLock;
		this.newLock = newLock;
		this.element = element;
	}
	
	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		return redo(monitor, info);
	}
	
	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {
		
		element.setLockStatus(newLock);
		element.setLockPassword(
				WorkspaceFactory.eINSTANCE.getWorkspaceInstance().getPassword());
		element.save();
		return Status.OK_STATUS;
	}
	
	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info)
			throws ExecutionException {

		element.setLockStatus(oldLock);
		element.save();
		return Status.OK_STATUS;
	}

}
