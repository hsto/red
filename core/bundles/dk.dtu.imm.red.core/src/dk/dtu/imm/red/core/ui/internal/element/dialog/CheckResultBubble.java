package dk.dtu.imm.red.core.ui.internal.element.dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.comment.Comment;
import dk.dtu.imm.red.core.comment.IssueComment;
import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.group.Group;

public class CheckResultBubble {
	
	private static final int TOOLTIP_LINKED_HIDE_DELAY = 700;
	private static final int TOOLTIP_UNLINKED_HIDE_DELAY = 4000;
	
	private ToolTip toolTip;
	private Control control;
	private Element[] checkedElements;
	
	public CheckResultBubble(Control control, Element checkedElement) {
		this(control, new Element[]{checkedElement});
	}

	public CheckResultBubble(Control control, Element[] checkedElements) {

		this.control = control;
		this.checkedElements = checkedElements;
	}

	public void show() {
		int issuesFound = 0;
		
		for (Element element : checkedElements) {
			issuesFound += countIssues(element);
		}
		
		int tipSettings;
		String tipText;
		
		if (issuesFound == 0) {
			tipSettings = SWT.ICON_INFORMATION;
			tipText = "No issues found";
		}
		else {
			tipSettings = SWT.ICON_ERROR;
			tipText = issuesFound + " issues found.";
		}
		
		instantiateTooltip(tipSettings, tipText);
		addHideDelay();
		
		this.toolTip.setVisible(true);
	}

	private int countIssues(Element element) {
		int issuesFound = 0;
		
		for (Comment comment : element.getCommentlist().getComments()) {
			if (comment instanceof IssueComment) {
				issuesFound++;
			}
		}
		
		if (element instanceof Group) {
			Group group = (Group) element;
			for (Element childElement : group.getContents()) {
				issuesFound += countIssues(childElement);
			}
		}
		
		return issuesFound;
	}
	
	private void instantiateTooltip(int tipSettings, String tipText) {
		int finalTipSettings = tipSettings;
		Shell shell;
		Point location;
		
		if (this.control == null) {
			shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
			location = shell.toDisplay(shell.getSize());
			location.x = 30;
			location.y -= 150;
		}
		else {
			finalTipSettings |= SWT.BALLOON; 
			shell = control.getShell();
			location = control.toDisplay(control.getSize());
		}
		
		this.toolTip = new ToolTip(shell, finalTipSettings);

		toolTip.setText(tipText);
		toolTip.setLocation(location);
	}
	
	private void addHideDelay() {
		if (this.control == null) {toolTip.getDisplay().timerExec(TOOLTIP_UNLINKED_HIDE_DELAY, new Runnable() {
                public void run() {
                	toolTip.setVisible(false);
                }
            });
		}
		else {
			control.addListener(SWT.MouseExit, new Listener() {
	            public void handleEvent(Event event) {
	            	toolTip.getDisplay().timerExec(TOOLTIP_LINKED_HIDE_DELAY, new Runnable() {
	                    public void run() {
	                    	toolTip.setVisible(false);
	                    }
	                });
	            }
	        });
		}
	}
}
