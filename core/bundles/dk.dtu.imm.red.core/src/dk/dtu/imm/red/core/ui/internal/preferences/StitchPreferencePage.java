package dk.dtu.imm.red.core.ui.internal.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class StitchPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	private IntegerFieldEditor editDistanceThresholdText;
	private BooleanFieldEditor autoLayoutBox;
	// private BooleanFieldEditor allowStepTracingBox;
	private BooleanFieldEditor cascadeUpdateBox;
	private BooleanFieldEditor allowConflictingSubElementsBox;


	private ComboFieldEditor mergeLogLevelCombo;
	private RadioGroupFieldEditor separateMultiStitchRadioBtn;
	private RadioGroupFieldEditor stitchingSequenceBtn;;

	/**
	 * Create the preference page.
	 */
	public StitchPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		setTitle("Stitch Preference");
	}

	/**
	 * Create contents of the preference page.
	 */
	@Override
	protected void createFieldEditors() {

		Composite composite = getFieldEditorParent();
		{
			editDistanceThresholdText = new IntegerFieldEditor(PreferenceConstants.STITCH_PREF_EDIT_DISTANCE_THRESHOLD, "Max Edit Distance Threshold", getFieldEditorParent());
			editDistanceThresholdText.setValidRange(0, 999);
			editDistanceThresholdText.setEmptyStringAllowed(true);
			addField(editDistanceThresholdText);
		}
		autoLayoutBox = new BooleanFieldEditor(PreferenceConstants.STITCH_PREF_AUTO_LAYOUT, "Auto layout output diagram", BooleanFieldEditor.DEFAULT, getFieldEditorParent());
		addField(autoLayoutBox);

		cascadeUpdateBox = new BooleanFieldEditor(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS, "Cascade updates of woven elements", BooleanFieldEditor.DEFAULT, getFieldEditorParent());

		allowConflictingSubElementsBox= new BooleanFieldEditor(PreferenceConstants.STITCH_PREF_ALLOW_CONFLICTING_SUBELEMENTS, "Allow conflicting sub-element names", BooleanFieldEditor.DEFAULT, getFieldEditorParent());
		addField(allowConflictingSubElementsBox);

		mergeLogLevelCombo = new ComboFieldEditor(PreferenceConstants.STITCH_PREF_MERGE_LOG_LEVEL, "Log Level", new String[][]{{"INFO", "INFO"}, {"DEBUG", "DEBUG"}}, getFieldEditorParent());
		addField(mergeLogLevelCombo);

		addField(cascadeUpdateBox);
		separateMultiStitchRadioBtn = new RadioGroupFieldEditor(
				PreferenceConstants.STITCH_PREF_SEPARATE_MULTISTITCH, "Separate multi stitch",
			1,new String[][]{{
						"By package", PreferenceConstants.STITCH_PREF_SEPARATE_MULTISTITCH_BYPACKAGE},
						{"By diagram type", PreferenceConstants.STITCH_PREF_SEPARATE_MULTISTITCH_BYDIAGRAMTYPE}}, getFieldEditorParent(), false);
		addField(separateMultiStitchRadioBtn);

		stitchingSequenceBtn =new RadioGroupFieldEditor(PreferenceConstants.STITCH_PREF_STITCHING_SEQUENCE, "Stitching sequence",
				1, new String[][]{
						{"Sequential", PreferenceConstants.STITCH_PREF_STITCHING_SEQUENCE_SEQUENTIAL},
						{"Tree", PreferenceConstants.STITCH_PREF_STITCHING_SEQUENCE_TREE}},
		getFieldEditorParent(), false);

		addField(stitchingSequenceBtn);
	}

	/**
	 * Initialize the preference page.
	 */
	public void init(IWorkbench workbench) {
		// Initialize the preference page
	}



}
