package dk.dtu.imm.red.core.ui.widgets.extendedAgileGrid;

import java.util.ArrayList;
import java.util.List;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.ColumnSortComparator;
import org.agilemore.agilegrid.ColumnSortOnClick;
import org.agilemore.agilegrid.DefaultCompositorStrategy;
import org.agilemore.agilegrid.ICompositorStrategy;
import org.agilemore.agilegrid.ISelectionChangedListener;
import org.agilemore.agilegrid.SelectionChangedEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;

public class ExtendedAgileGrid<T> extends AgileGrid {

	private boolean rowSelected = false;
	private int selectedIndex;
	private Composite compositeParent;
	private EAGPresenter<T> presenter;

	public EAGPresenter<T> getPresenter() {
		return presenter;
	}

	private Listener modifyListener;

	public ExtendedAgileGrid(Composite parent, int style, Listener modifyListener) {
		super(parent, style);
		this.compositeParent = parent;
		this.modifyListener = modifyListener;

	}

	public ExtendedAgileGrid<T> setPresenter(EAGPresenter<T> presenter) {
		this.presenter = presenter;
		return this;
	}

	public void setDataSource() {

		EAGColumnLayoutAdvisor<T> layoutAdvisor = new EAGColumnLayoutAdvisor<T>(this, presenter);
		final ICompositorStrategy compositorStrategy = new DefaultCompositorStrategy(layoutAdvisor);
		layoutAdvisor.setCompositorStrategy(compositorStrategy);

		this.setLayoutAdvisor(layoutAdvisor);
		this.setContentProvider(new EAGContentProvider<T>(this, presenter, modifyListener));
		this.setCellEditorProvider(new EAGCellEditorProvider(this, presenter));
		this.setCellRendererProvider(new EAGCellRendererProvider(this, presenter));
		this.addMouseListener(new ColumnSortOnClick(this,
				new EAGTableSortComparator(this, Integer.MIN_VALUE, ColumnSortComparator.SORT_UP)));

		this.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				updateRowSelectedState(presenter.getDataSourceView().size());
			}
		});

		clearSelection();
		redraw();
	}

	@Override
	public void redraw() {
		try {
			super.redraw();
			if (presenter.getDataSourceView() != null && presenter != null)
				updateRowSelectedState(presenter.getDataSourceView().size());
		} catch (Exception ex) {
		}
	}

	private void updateRowSelectedState(int dataSourceItemCount) {
		rowSelected = getCellSelection().length > 0 && dataSourceItemCount > 0;

		selectedIndex = getCellSelection().length > 0 ? getCellSelection()[0].row : -1;

		// If parent is CRUD handler, update button state
		if (compositeParent instanceof EAGComposite<?>) {
			((EAGComposite<?>) compositeParent).updateButtonState(rowSelected, selectedIndex);
		}

		presenter.rowSelectionUpdated(rowSelected, selectedIndex);
	}

	public boolean isRowSelected() {
		return rowSelected;
	}

	public List<Integer> getSelectedIndex() {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();

		for (org.agilemore.agilegrid.Cell c : getCellSelection()) {
			arrayList.add(c.row);
		}
		return arrayList;
	}
}
