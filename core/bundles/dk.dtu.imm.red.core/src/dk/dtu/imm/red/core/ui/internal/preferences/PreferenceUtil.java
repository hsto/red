package dk.dtu.imm.red.core.ui.internal.preferences;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceStore;

import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.user.User;
import dk.dtu.imm.red.core.user.UserFactory;

public class PreferenceUtil {

	private static String preferenceStoreLocation =".";
	private static final String PREFERENCE_STORE_FILENAME = "redconfig.pref";
	private static PreferenceStore defaultPrefStore=null;

	public static PreferenceStore getPreferenceStore(){
		if(defaultPrefStore!=null){
			return defaultPrefStore;
		}

		try{
			if(Platform.isRunning()){
				URL platformURL = Platform.getInstanceLocation().getURL();
				String platformPath = FileLocator.toFileURL(platformURL).getPath();
				String storeLocation = platformPath+PREFERENCE_STORE_FILENAME;
				File f = new File(storeLocation);
				//this store the preferences in /runtime-dk.dtu.imm.red.product/redconfig.pref
				System.out.println("storeLocation is in:"+storeLocation);
				if(!f.exists()){
					f.createNewFile();
				}
				defaultPrefStore = new PreferenceStore(storeLocation);
				defaultPrefStore.load();
			}
		}catch(IOException e){
			e.printStackTrace(); LogUtil.logError(e);
		}


		return defaultPrefStore;
	}


	public static void storePreference(String name, String defaultValue, String value){
		if(getPreferenceStore()!=null){
			getPreferenceStore().setDefault(name, defaultValue);
			getPreferenceStore().setValue(name, value);
		}
	}

	public static String getPreferenceString(String name){

		if(getPreferenceStore()!=null){
			return getPreferenceStore().getString(name);
		}
		return "";
	}

	public static int getPreferenceInt(String name){
		if(getPreferenceStore()!=null){
			return getPreferenceStore().getInt(name);
		}
		return 0;
	}

	public static boolean getPreferenceBoolean(String name){
		if(getPreferenceStore()!=null){
			return getPreferenceStore().getBoolean(name);
		}
		return false;
	}

	public static String getPreference(IPreferenceStore prefStore, String name){
		if(getPreferenceStore()!=null){
			return getPreferenceStore().getString(name);
		}
		return "";
	}


	public static User getUserPreference_User(){
		String firstName = getPreferenceString(PreferenceConstants.USER_PREF_FIRST_NAME);
		String middleName = getPreferenceString(PreferenceConstants.USER_PREF_MIDDLE_NAME);
		String lastName = getPreferenceString(PreferenceConstants.USER_PREF_LAST_NAME);
		String id = getPreferenceString(PreferenceConstants.USER_PREF_ID);
		String initials = getPreferenceString(PreferenceConstants.USER_PREF_INITIALS);
		String email = getPreferenceString(PreferenceConstants.USER_PREF_EMAIL);
		User user = UserFactory.eINSTANCE.createUser();
		user.setName((firstName + " " + middleName + " " + lastName).trim());
		user.setId(id);
		user.setInitials(initials);
		user.setEmail(email);
		return user;
	}

	public static void setDefaultPreferenceValue(){
		PreferenceStore store = PreferenceUtil.getPreferenceStore();
	    store.setDefault(PreferenceConstants.STITCH_PREF_EDIT_DISTANCE_THRESHOLD, 0);
	    store.setDefault(PreferenceConstants.STITCH_PREF_AUTO_LAYOUT, true);
	    store.setDefault(PreferenceConstants.STITCH_PREF_CASCADE_UPDATE_OF_WOVEN_ELEMENTS,true);
	    store.setDefault(PreferenceConstants.STITCH_PREF_ALLOW_CONFLICTING_SUBELEMENTS,true);
	    store.setDefault(PreferenceConstants.STITCH_PREF_SEPARATE_MULTISTITCH, PreferenceConstants.STITCH_PREF_SEPARATE_MULTISTITCH_BYDIAGRAMTYPE);
	    store.setDefault(PreferenceConstants.STITCH_PREF_STITCHING_SEQUENCE, PreferenceConstants.STITCH_PREF_STITCHING_SEQUENCE_SEQUENTIAL);
	    store.setDefault(PreferenceConstants.PROJECT_PREF_AUTOSAVE, PreferenceConstants.PROJECT_PREF_AUTOSAVE_10MIN);
	}

}
