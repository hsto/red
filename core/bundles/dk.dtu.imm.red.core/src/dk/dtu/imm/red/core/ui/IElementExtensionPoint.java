package dk.dtu.imm.red.core.ui;

import dk.dtu.imm.red.core.element.Element;


/**
 * Extension point interface for opening elements, which plug-ins have extended.
 * 
 * @author Jakob Kragelund
 * 
 */
public interface IElementExtensionPoint {

	/**
	 * Used to tell a plug-in that an element wants to be opened. If the plug-in
	 * knows how to open the element, it may react on this.
	 * 
	 * @param element
	 *            The element to be opened
	 */
	void openElement(Element element);

	/**
	 * Used to tell a plug-in that an element has been deleted. It is up to all
	 * plug-ins to handle this appropriately, for example by deleting the
	 * element from internal data structures.
	 * @param element
	 */
	void deleteElement(Element element);
	
	/**
	 * Used to tell a plug-in that an element has been restored (for example, 
	 * by undoing a delete). It is up to all plug-ins to handle this 
	 * appropriately, for example by restoring the element in an internal 
	 * data structure.
	 * @param element The restored element
	 */
	void restoreElement(Element element);
	

}
