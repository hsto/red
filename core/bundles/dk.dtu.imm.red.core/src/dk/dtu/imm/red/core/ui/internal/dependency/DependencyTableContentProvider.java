package dk.dtu.imm.red.core.ui.internal.dependency;

import java.util.List;

import org.agilemore.agilegrid.DefaultContentProvider;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.relationship.ElementReference;
import dk.dtu.imm.red.core.element.relationship.RelationshipKind;
import dk.dtu.imm.red.core.ui.editors.IBaseEditor.Dependency;

/**
 * Content provider for the associations table.
 * 
 * @author Jakob Kragelund
 *
 */
public class DependencyTableContentProvider extends DefaultContentProvider {

	/**
	 * The various columns and their indices in the table.
	 */
	private static final int COMMENT_COLUMN = 3;
	private static final int ELEMENT_NAME_COLUMN = 2;
	private static final int DEPENDENCY_KIND_COLUMN = 1;
	private static final int ICON_COLUMN = 0;

	/**
	 * The relations shown in the table.
	 */
	protected List<Dependency> relations;
	protected List<Dependency> relationsRelatedBy;

	/**
	 * Creates a new content provider designed for the associations table in the
	 * persona editor.
	 * 
	 * @param relations
	 *            the associations of the persona
	 * @param editor
	 *            the editor containing the table
	 */
	public DependencyTableContentProvider(final List<Dependency> relations, final List<Dependency> relationsRelatedBy) {
		super();

		this.relations = relations;
		this.relationsRelatedBy = relationsRelatedBy;
	}

	public void setContent(final List<Dependency> relations, final List<Dependency> relationsRelatedBy) {
		this.relations = relations;
		this.relationsRelatedBy = relationsRelatedBy;
	}

	@Override
	public void doSetContentAt(final int row, final int col, final Object value) {

		Dependency relationship = null;

		// Always keeps the relatedBy relations on the top of the table
		if (row < relationsRelatedBy.size()) {
			relationship = relationsRelatedBy.get(row);
		} else {
			relationship = relations.get(row - relationsRelatedBy.size());
		}

		if (relationship == null || relationship.targetElement == null) {
			super.doSetContentAt(row, col, value);
		}

		switch (col) {
		case ICON_COLUMN: // not editable
			break;
		case DEPENDENCY_KIND_COLUMN:
			Object oldKind = doGetContentAt(row, col);
			if (oldKind == null || !oldKind.equals(value)) {
				// TODO: Change to define graph top-down direction
				relationship.dependencyKind = RelationshipKind.get((String) value);
				IWorkbench wb = PlatformUI.getWorkbench();
				IWorkbenchWindow window = wb.getActiveWorkbenchWindow();
				IWorkbenchPage page = window.getActivePage();
				IEditorPart editor = page.getActiveEditor();
				editor.doSave(new NullProgressMonitor());
				super.doSetContentAt(row, col, value);
			}
			break;
		case ELEMENT_NAME_COLUMN: // not editable
			break;
		case COMMENT_COLUMN:
			Object oldValue = doGetContentAt(row, col);
			// if the new value really is different from the old
			if (oldValue == null || !oldValue.equals(value)) {
				// TODO comments must be saved in some way but
				// not by marking the editor as dirty.
				relationship.comment = (String) value;
				IWorkbench wb = PlatformUI.getWorkbench();
				IWorkbenchWindow window = wb.getActiveWorkbenchWindow();
				IWorkbenchPage page = window.getActivePage();
				IEditorPart editor = page.getActiveEditor();
				editor.doSave(new NullProgressMonitor());
				super.doSetContentAt(row, col, value);
			}
			break;
		default:
			super.doSetContentAt(row, col, value);
		}
	}

	@Override
	public Object doGetContentAt(final int row, final int col) {
		Dependency relationship = null;

		// Always keeps the relatedBy relations on the top of the table
		if (row < relationsRelatedBy.size()) {
			relationship = relationsRelatedBy.get(row);
		} else {
			relationship = relations.get(row - relationsRelatedBy.size());
		}

		if (relationship == null || relationship.targetElement == null) {
			return super.doGetContentAt(row, col);
		}

		switch (col) {
		case ICON_COLUMN:
			return relationship.icon;
		case DEPENDENCY_KIND_COLUMN:
			if (relationship.dependencyKind != null)
				return relationship.dependencyKind.getLiteral();
			else
				return super.doGetContentAt(row, col);
		case ELEMENT_NAME_COLUMN:
			return relationship.targetElement.getName();
		case COMMENT_COLUMN:
			return relationship.comment;
		// return "abc\ndef";
		default:
			return super.doGetContentAt(row, col);
		}
	}

}
