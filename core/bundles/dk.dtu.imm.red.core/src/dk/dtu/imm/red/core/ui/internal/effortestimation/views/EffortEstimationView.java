package dk.dtu.imm.red.core.ui.internal.effortestimation.views;

import org.eclipse.ui.IViewPart;

import dk.dtu.imm.red.core.ui.editors.IBaseEditor;

public interface EffortEstimationView extends IViewPart { 
	String ID = "dk.dtu.imm.red.core.effortestimation.view"; 
	void updateValues(); 
} 