package dk.dtu.imm.red.core.ui.internal.search.views;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

import dk.dtu.imm.red.core.element.Element;
import dk.dtu.imm.red.core.element.SearchResult;
import dk.dtu.imm.red.core.element.SearchResultWrapper;
import dk.dtu.imm.red.core.ui.IElementExtensionPoint;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;
import dk.dtu.imm.red.core.ui.editors.BaseEditorInput;
import dk.dtu.imm.red.core.ui.internal.LogUtil;
import dk.dtu.imm.red.core.ui.views.BaseView;

public class SearchView extends BaseView {
	
	public static final String ID = "dk.dtu.imm.red.core.search.view";
	
	protected TreeViewer searchResultViewer;
	
	@Override
	public void createPartControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		
		FillLayout layout = new FillLayout();
		composite.setLayout(layout);
		
		searchResultViewer = new TreeViewer(composite, SWT.NONE);
		searchResultViewer.setContentProvider(new SearchContentProvider());
		searchResultViewer.setLabelProvider(new SearchLabelProvider());
		
		searchResultViewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				IStructuredSelection selection = 
						(IStructuredSelection) event.getSelection();
				Element selectedElement = null;
				
				if (selection.getFirstElement() instanceof SearchResultWrapper) {
					selectedElement = ((SearchResultWrapper) selection
							.getFirstElement()).getElement();
				} else if (selection.getFirstElement() instanceof SearchResult) {
					selectedElement = ((SearchResult) selection
							.getFirstElement()).getElement();
				}
				
				final Element finalElement = selectedElement;
				
				IConfigurationElement[] configurationElements = Platform
						.getExtensionRegistry().getConfigurationElementsFor(
								"dk.dtu.imm.red.core.element");
				try {
					for (IConfigurationElement configurationElement 
							: configurationElements) {

						final Object extension = configurationElement
								.createExecutableExtension("class");

						if (extension instanceof IElementExtensionPoint) {
							ISafeRunnable runnable = new ISafeRunnable() {

								@Override
								public void handleException(final Throwable exception) {
									exception.printStackTrace();
								}

								@Override
								public void run() throws Exception {
									((IElementExtensionPoint) extension)
									.openElement(finalElement);
								}
							};
							runnable.run();
						}
					}
				} catch (CoreException e) {
					e.printStackTrace(); LogUtil.logError(e);
				} catch (Exception e) {
					e.printStackTrace(); LogUtil.logError(e);
				}
				
				BaseEditorInput editorInput = new BaseEditorInput(finalElement);
				IEditorPart editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findEditor(editorInput);
				if (editor instanceof BaseEditor && selection.getFirstElement() instanceof SearchResult) {
					SearchResult result = (SearchResult) selection.getFirstElement();
					((BaseEditor) editor).selectText(result.getAttribute(), result.getStartIndex(), result.getEndIndex());
				}
			}
		});
	}
	
	public void setInput(SearchResultWrapper searchResult) {
		searchResultViewer.setInput(searchResult);
	}

}
