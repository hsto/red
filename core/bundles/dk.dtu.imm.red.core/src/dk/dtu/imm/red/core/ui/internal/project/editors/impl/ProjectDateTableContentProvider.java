package dk.dtu.imm.red.core.ui.internal.project.editors.impl;

import java.text.SimpleDateFormat;
import java.util.List;

import org.agilemore.agilegrid.DefaultContentProvider;

import dk.dtu.imm.red.core.project.ProjectDate;
import dk.dtu.imm.red.core.ui.editors.BaseEditor;

public class ProjectDateTableContentProvider extends DefaultContentProvider {

	private static final int COMMENT_COLUMN = 1;
	private static final int DATE_COLUMN = 0;

	private List<ProjectDate> dates;
	
	private SimpleDateFormat sdf;
	
	private BaseEditor editor;


	public ProjectDateTableContentProvider(BaseEditor baseEditor) {
		super();
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		this.editor = baseEditor;
	}

	public void setContent(List<ProjectDate> dates) {
		this.dates = dates;
	}

	@Override
	public void doSetContentAt(int row, int col, Object value) {
		ProjectDate date = dates.get(row);

		if (date == null) {
			super.doSetContentAt(row, col, value);
		}

		switch (col) {
			case DATE_COLUMN:
				date.setDate((String) value);
				break;
			case COMMENT_COLUMN:
				date.setComment((String) value);
				break;
			default:
				doSetContentAt(row, col, value);
		}
		
		editor.markAsDirty();
	}

	@Override
	public Object doGetContentAt(int row, int col) {
		ProjectDate date = dates.get(row);

		if (date == null) {
			return super.doGetContentAt(row, col);
		}

		switch (col) {
			case DATE_COLUMN:
				return date.getDate();
			case COMMENT_COLUMN:
				return date.getComment();
			default:
				return doGetContentAt(row, col);
		}
	}
}
