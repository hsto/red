package dk.dtu.imm.red.core.ui.element.wizards.impl;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import dk.dtu.imm.red.core.element.QuantityItem;
import dk.dtu.imm.red.core.element.QuantityLabel;

public class CreateQuantityItemPageImpl extends WizardPage
		implements IWizardPage{

	protected Label selectElementLabel;
	protected ISelection selection;
	protected Combo comboBox;
	protected EList<QuantityItem> items;
	
	protected CreateQuantityItemPageImpl(EList<QuantityItem> items) {
		super("Create Quantity Item");
		
		setDescription("Select the type of quantity to create");
		setTitle("Create Quantity Item");
		
		this.items = items;
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		
		GridLayout layout = new GridLayout(2, true);
		composite.setLayout(layout);
		
		GridData descriptionLayoutData = new GridData();
		descriptionLayoutData.horizontalSpan = 2;


		selectElementLabel = new Label(composite, SWT.NONE);
		selectElementLabel.setLayoutData(descriptionLayoutData);
		selectElementLabel
				.setText("Type of quantity:");

		Composite comboContainer = new Composite(composite, SWT.NONE);
		GridData comboLayoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		comboLayoutData.horizontalSpan = 2;
		GridLayout comboContainerLayout = new GridLayout();
		comboContainerLayout.numColumns = 1;

		comboContainer.setLayout(comboContainerLayout);
		comboContainer.setLayoutData(comboLayoutData);

		comboBox = new Combo(comboContainer, SWT.READ_ONLY);
		for (QuantityLabel label : getAvailableLabels()) {
			comboBox.add(label.getLiteral());
		}
		comboBox.select(0);
		
		if (comboBox.getItems().length == 0) {
			comboBox.add("None available");
			comboBox.select(0);
			comboBox.setEnabled(false);
			setPageComplete(false);
		}
		else {
			setPageComplete(true);
		}
		
		setControl(composite);
	}
	
	public QuantityLabel getSelectedLabel(){
		String selectedLiteral = comboBox.getItem(comboBox.getSelectionIndex());
		return QuantityLabel.get(selectedLiteral);
	}

	private QuantityLabel[] getAvailableLabels() {
		ArrayList<QuantityLabel> availableList = new ArrayList<QuantityLabel>(Arrays.asList(QuantityLabel.values()));
		
		for (QuantityItem item : items) {
			availableList.remove(item.getLabel());
		}
		
		return availableList.toArray(new QuantityLabel[availableList.size()]);
	}
}
