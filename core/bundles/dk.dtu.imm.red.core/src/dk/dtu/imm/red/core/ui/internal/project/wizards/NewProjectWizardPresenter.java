package dk.dtu.imm.red.core.ui.internal.project.wizards;

import dk.dtu.imm.red.core.element.group.Group;
import dk.dtu.imm.red.core.ui.wizards.IWizardPresenter;

public interface NewProjectWizardPresenter extends IWizardPresenter{

	void wizardFinished(String name, String projectDescription, String path);
}
