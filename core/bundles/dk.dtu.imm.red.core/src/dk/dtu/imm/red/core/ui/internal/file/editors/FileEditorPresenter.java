package dk.dtu.imm.red.core.ui.internal.file.editors;

import dk.dtu.imm.red.core.ui.editors.IEditorPresenter;

public interface FileEditorPresenter extends IEditorPresenter{

	void setLongDescription(String text);

	
}