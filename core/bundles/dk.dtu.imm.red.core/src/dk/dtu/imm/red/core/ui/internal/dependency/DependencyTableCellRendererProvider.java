package dk.dtu.imm.red.core.ui.internal.dependency;

import org.agilemore.agilegrid.AgileGrid;
import org.agilemore.agilegrid.DefaultCellRendererProvider;
import org.agilemore.agilegrid.ICellRenderer;
import org.agilemore.agilegrid.renderers.TextCellRenderer;

import dk.dtu.imm.red.core.ui.ImageCellRenderer;

public class DependencyTableCellRendererProvider
		extends	DefaultCellRendererProvider {
	
	private static final int ICON_COLUM = 0;
	
	ICellRenderer imageRenderer;
	ICellRenderer textCellRenderer;
	
	public DependencyTableCellRendererProvider(AgileGrid grid) {
		super(grid);
		
		imageRenderer = new ImageCellRenderer(grid);
		textCellRenderer = new TextCellRenderer(grid);
	}
	
	@Override
	public ICellRenderer getCellRenderer(int row, int col) {
		if (col == ICON_COLUM) {
			return imageRenderer;
		} else {
			return textCellRenderer;
		}
	}
}
